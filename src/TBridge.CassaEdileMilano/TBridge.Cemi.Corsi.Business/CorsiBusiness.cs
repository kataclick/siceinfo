using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Data;
using TBridge.Cemi.Corsi.Type.Collections;
using TBridge.Cemi.Corsi.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters;

namespace TBridge.Cemi.Corsi.Business
{
    public class CorsiBusiness
    {
        #region Semafori

        public const string URLSEMAFOROGIALLO = "~/images/semaforoGiallo.png";
        public const string URLSEMAFOROROSSO = "~/images/semaforoRosso.png";
        public const string URLSEMAFOROVERDE = "~/images/semaforoVerde.png";

        #endregion

        private readonly CorsiDataAccess dataAccess = new CorsiDataAccess();

        public LocazioneCollection GetLocazioni()
        {
            return dataAccess.GetLocazioni();
        }

        public Boolean InsertLocazione(Locazione locazione)
        {
            return dataAccess.InsertLocazione(locazione);
        }

        public CorsoCollection GetCorsiAll()
        {
            return dataAccess.GetCorsiAll(false, false);
        }

        public CorsoCollection GetCorsiPianificati()
        {
            return dataAccess.GetCorsiAll(true, false);
        }

        public CorsoCollection GetCorsiPianificabili()
        {
            return dataAccess.GetCorsiAll(false, true);
        }

        public Corso GetCorsoByKey(Int32 idCorso)
        {
            return dataAccess.GetCorsoByKey(idCorso);
        }

        public Boolean InsertCorso(Corso corso)
        {
            return dataAccess.InsertCorso(corso);
        }

        public ProgrammazioneModuloCollection GetProgrammazioni(ProgrammazioneModuloFilter filtro)
        {
            return dataAccess.GetProgrammazioni(filtro);
        }

        public Boolean InsertUpdateProgrammazione(Programmazione programmazione)
        {
            if (!programmazione.IdProgrammazione.HasValue)
            {
                return dataAccess.InsertProgrammazione(programmazione);
            }
            else
            {
                return dataAccess.UpdateProgrammazione(programmazione);
            }
        }

        public ProgrammazioneModuloCollection GetProgrammazione(Int32 idProgrammazione)
        {
            return dataAccess.GetProgrammazione(idProgrammazione);
        }

        public ModuloCollection GetModuliConProgrammazioniFuture(Int32 idCorso, DateTime dataInizio, DateTime dataFine,
                                                                 Boolean? prenotabili, Boolean overBooking)
        {
            return dataAccess.GetModuliConProgrammazioniFuture(idCorso, dataInizio, dataFine, prenotabili, overBooking);
        }

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            return dataAccess.GetLavoratoriSiceNew(filtro);
        }

        public LavoratoreCollection GetLavoratoriAnagraficaCondivisa(LavoratoreFilter filtro)
        {
            return dataAccess.GetLavoratoriAnagraficaCondivisa(filtro);
        }

        public ImpresaCollection GetImpreseAnagraficaCondivisa(ImpresaFilter filtro)
        {
            return dataAccess.GetImpreseAnagraficaCondivisa(filtro);
        }

        public Lavoratore GetLavoratoreAnagraficaCondivisaByKey(Int32 idLavoratore)
        {
            return dataAccess.GetLavoratoreAnagraficaCondivisaByKey(idLavoratore);
        }

        public Impresa GetImpresaAnagraficaCondivisaByKey(Int32 idImpresa)
        {
            return dataAccess.GetImpresaAnagraficaCondivisaByKey(idImpresa);
        }

        public Lavoratore GetLavoratoreSiceNewByKey(Int32 idLavoratore)
        {
            return dataAccess.GetLavoratoreSiceNewByKey(idLavoratore);
        }

        public bool InsertPartecipazione(Partecipazione partecipazione, Boolean overBooking)
        {
            return dataAccess.InsertPartecipazione(partecipazione, overBooking);
        }

        public LavoratoreCollection GetPartecipanti(PartecipanteFilter filtro)
        {
            return dataAccess.GetPartecipanti(filtro);
        }

        public ImpresaCollection GetImpreseSiceNewEAnagrafica(ImpresaFilter filtro)
        {
            return dataAccess.GetImpreseSiceNewEAnagrafica(filtro);
        }

        public Impresa GetImpresaSiceNewByKey(int idImpresa)
        {
            return dataAccess.GetImpresaSiceNewByKey(idImpresa);
        }

        public Impresa GetImpresaCorsiByKey(int idImpresa)
        {
            return dataAccess.GetImpresaCorsiByKey(idImpresa);
        }

        public ProgrammazioneModuloCollection GetPartecipazioni(PartecipazioneFilter filtro)
        {
            return dataAccess.GetPartecipazione(filtro);
        }

        public ProgrammazioneModuloCollection GetPartecipazioni(int idPartecipazione)
        {
            return dataAccess.GetPartecipazione(idPartecipazione);
        }

        public Boolean UpdatePartecipazioneModuloPresenza(Int32 idPartecipazioneModulo, Boolean presente)
        {
            return dataAccess.UpdatePartecipazioneModuloPresenza(idPartecipazioneModulo, presente);
        }

        public String ConvertiBoolInSemaforo(Boolean? stato)
        {
            if (!stato.HasValue)
            {
                return URLSEMAFOROGIALLO;
            }
            else
            {
                if (stato.Value)
                {
                    return URLSEMAFOROVERDE;
                }
                else
                {
                    return URLSEMAFOROROSSO;
                }
            }
        }

        public Boolean UpdateProgrammazioneModuloConfermaPresenze(Int32 idProgrammazioneModulo)
        {
            return dataAccess.UpdateProgrammazioneModuloConfermaPresenze(idProgrammazioneModulo);
        }

        public Boolean DeleteProgrammazione(Int32 idProgrammazione)
        {
            return dataAccess.DeleteProgrammazione(idProgrammazione);
        }

        public Boolean DeleteCorso(int idCorso)
        {
            return dataAccess.DeleteCorso(idCorso);
        }

        public PartecipazioneModuloCollection GetPartecipazioniByLavoratoreImpresa(
            PartecipazioneLavoratoreImpresaFilter filtro)
        {
            return dataAccess.GetPartecipazioniByLavoratoreImpresa(filtro);
        }

        public Int32? GetUltimaImpresaLavoratore(int idLavoratore)
        {
            return dataAccess.GetUltimaImpresaLavoratore(idLavoratore);
        }

        public Boolean DeletePartecipazione(int idPartecipazioneModulo)
        {
            return dataAccess.DeletePartecipazione(idPartecipazioneModulo);
        }

        public Locazione GetLocazioneByKey(int idLocazione)
        {
            return dataAccess.GetLocazioneByKey(idLocazione);
        }

        public TitoloDiStudioCollection GetTitoliDiStudioAll()
        {
            return dataAccess.GetTitoliDiStudioAll();
        }

        public TipoIscrizioneCollection GetTipiIscrizioneAll()
        {
            return dataAccess.GetTipiIscrizioneAll();
        }

        public TipoContrattoCollection GetTipiContrattoAll()
        {
            return dataAccess.GetTipiContrattoAll();
        }

        public Lavoratore GetCorsiLavoratore(Int32 idLavoratore)
        {
            return dataAccess.GetCorsiLavoratore(idLavoratore);
        }

        public void GetDatiAggiuntiviPartecipazione(Int32 idPartecipazione, out Boolean? primaEsperienza, out DateTime? dataAssunzione)
        {
            dataAccess.GetDatiAggiuntiviPartecipazione(idPartecipazione, out primaEsperienza, out dataAssunzione);
        }

        public bool UpdateCorsiLavoratore(Lavoratore lavoratore, Int32 idPartecipazione)
        {
            return dataAccess.UpdateCorsiLavoratore(lavoratore, idPartecipazione);
        }

        public bool UpdatePartecipazione(Int32 idPartecipazione, Boolean? primaEsperienza, DateTime? dataAssunzione)
        {
            return dataAccess.UpdatePartecipazione(idPartecipazione, primaEsperienza, dataAssunzione, null);
        }

        public EstrazioneFormedilImpresaCollection EstrazioneFormedilImprese(EstrazioneFormedilFilter filtro)
        {
            return dataAccess.EstrazioneFormedilImprese(filtro);
        }

        public EstrazioneFormedilLavoratoreCollection EstrazioneFormedilLavoratori(EstrazioneFormedilFilter filtro)
        {
            return dataAccess.EstrazioneFormedilLavoratori(filtro);
        }

        public Boolean EsisteImpresaConStessaIvaFisc(String partitaIva, String codiceFiscale)
        {
            return dataAccess.EsisteImpresaConStessaIvaFisc(partitaIva, codiceFiscale);
        }

        public Boolean OraValida(String oraFornita, out DateTime oraTrovata)
        {
            Boolean res = false;
            oraTrovata = new DateTime(1900, 1, 1);

            String[] oraSplitted = oraFornita.Split(':');
            if (oraSplitted.Length == 2)
            {
                Int32 ora;
                Int32 minuti;

                if (Int32.TryParse(oraSplitted[0], out ora)
                    && Int32.TryParse(oraSplitted[1], out minuti))
                {
                    if (ora >= 0 && ora <= 23
                        && minuti >= 0 && minuti <= 59)
                    {
                        res = true;
                        oraTrovata = new DateTime(1900, 1, 1, ora, minuti, 0);
                    }
                }
            }

            return res;
        }

        public Boolean EsisteLavoratoreConStessoCodiceFiscale(String codiceFiscale)
        {
            return dataAccess.EsisteLavoratoreConStessoCodiceFiscale(codiceFiscale);
        }

        public Boolean DeleteLocazione(Int32 idLocazione)
        {
            return dataAccess.DeleteLocazione(idLocazione);
        }

        public Boolean LavoratoreSeguito16Ore(String codiceFiscale)
        {
            return dataAccess.LavoratoreSeguito16Ore(codiceFiscale);
        }

        #region Gestione prestazioni domande corsi

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public PrestazioniDomandeCorsiCollection GetPrestazioniDomandeCorsi(PrestazioneDomandaCorsoFilter filtro)
        {
            return dataAccess.GetPrestazioniDomandeCorsi(filtro);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public StatoDomandaCollection GetStatiDomanda()
        {
            return dataAccess.GetStatiDomanda();
        }

        public void AccogliPrestazioneDomandaCorso(Int32 idPrestazioneDomandaCorso, Int32 idUtente)
        {
            dataAccess.AggiornaStatoPrestazioneDomandaCorso(idPrestazioneDomandaCorso, "C", idUtente);
        }

        public void RespingiPrestazioneDomandaCorso(Int32 idPrestazioneDomandaCorso, Int32 idUtente)
        {
            dataAccess.AggiornaStatoPrestazioneDomandaCorso(idPrestazioneDomandaCorso, "R", idUtente);
        }

        public void AnnullaPrestazioneDomandaCorso(Int32 idPrestazioneDomandaCorso, Int32 idUtente)
        {
            dataAccess.AggiornaStatoPrestazioneDomandaCorso(idPrestazioneDomandaCorso, "A", idUtente);
        }

        public Boolean AggiornaLavoratoreBeneficiarioDomandaPrestazione(int idCorsiPrestazioneDomanda, int idLavoratore,
                                                                        int idUtente)
        {
            return dataAccess.AggiornaLavoratoreBeneficiarioDomandaPrestazione(idCorsiPrestazioneDomanda, idLavoratore,
                                                                               idUtente);
        }

        #endregion

        #region Anagrafica Unica
        public Type.DataSets.Corsi AnagraficaUnicaRicercaCorsi(AnagraficaUnicaCorsiFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaCorsi(filtro);
        }

        public Type.DataSets.Iscrizioni AnagraficaUnicaRicercaIscrizioni(AnagraficaUnicaIscrizioniFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaIscrizioni(filtro);
        }

        public Type.DataSets.Lavoratori AnagraficaUnicaRicercaLavoratori(AnagraficaUnicaLavoratoriFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaLavoratori(filtro);
        }

        public Type.DataSets.Imprese AnagraficaUnicaRicercaImprese(AnagraficaUnicaImpreseFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaImprese(filtro);
        }
        #endregion

        #region PrestazioniDTA
        public DateTime? DataErogazionePrestazioniDTA(Int32 anno)
        {
            return dataAccess.DataErogazionePrestazioniDTA(anno);
        }

        public List<PrestazioneDTA> GetPrestazioniDTALavoratori(Int32 anno, DTAFilter filtro)
        {
            return dataAccess.GetPrestazioniDTALavoratori(anno, filtro);
        }

        public void SalvaPrestazioniDTALavoratori(List<PrestazioneDTALavoratore> prestazioni)
        {
            dataAccess.SalvaPrestazioniDTALavoratori(prestazioni);
        }

        public void SalvaPrestazioneDTALavoratore(PrestazioneDTALavoratore prestazione)
        {
            dataAccess.SalvaPrestazioneDTALavoratore(prestazione, null);
        }

        public void GeneraDomandePrestazioniDTALavoratori(Int32 anno)
        {
            dataAccess.GeneraDomandePrestazioniDTALavoratori(anno);
        }

        public Dictionary<Int32, String> GetPrestazioniDTAAnni()
        {
            return dataAccess.GetPrestazioniDTAAnni();
        }
        #endregion
    }
}