﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Data;
using TBridge.Cemi.Corsi.Type.Entities.Esem;
using TBridge.Cemi.Corsi.Type.Filters.Esem;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.Corsi.Type.Filters;

namespace TBridge.Cemi.Corsi.Business
{
    public class CorsiEsemBusiness
    {
        private readonly CorsiEsemDataAccess dataAccess = new CorsiEsemDataAccess();

        public List<TipoCorso> GetTipiCorso(Utente utente)
        {
            return dataAccess.GetTipiCorso(utente);
        }

        public List<Sede> GetSedi(Utente utente)
        {
            return dataAccess.GetSedi(utente);
        }

        public List<Corso> GetCorsi(Utente utente, CorsoFilter filter)
        {
            return dataAccess.GetCorsi(utente, filter);
        }

        public List<LavoratoreCorsi> GetCorsiScadenzeImpresa(Utente utente, String codiceFiscaleImpresa, Int32? idImpresa)
        {
            return dataAccess.GetCorsiScadenzeImpresa(utente, codiceFiscaleImpresa, idImpresa);
        }

        public LavoratoreCorsi GetCorsiScadenzeLavoratore(Utente utente, String codiceFiscaleLavoratore, Int32? idLavoratore)
        {
            return dataAccess.GetCorsiScadenzeLavoratore(utente, codiceFiscaleLavoratore, idLavoratore);
        }

        public List<LavoratoreCorsi> GetIscrizioniImpresa(Utente utente, PartecipazioneLavoratoreImpresaFilter filtro)
        {
            List<LavoratoreCorsi> iscrizioni = dataAccess.GetIscrizioniImpresa(utente, filtro.ImpresaIvaFisc, filtro.IdImpresa);
            List<LavoratoreCorsi> res = new List<LavoratoreCorsi>();

            foreach (LavoratoreCorsi lc in iscrizioni)
            {
                Boolean add = true;

                if (!String.IsNullOrEmpty(filtro.LavoratoreCognome) && !lc.Lavoratore.Cognome.Contains(filtro.LavoratoreCognome))
                {
                    add = false;
                }

                if (!String.IsNullOrEmpty(filtro.LavoratoreNome) && !lc.Lavoratore.Nome.Contains(filtro.LavoratoreNome))
                {
                    add = false;
                }

                if (!String.IsNullOrEmpty(filtro.LavoratoreCodiceFiscale) && !lc.Lavoratore.CodiceFiscale.Contains(filtro.LavoratoreCodiceFiscale))
                {
                    add = false;
                }

                if (add)
                    res.Add(lc);
            }

            return res;
        }

        public LavoratoreCorsi GetIscrizioniLavoratore(Utente utente, Int32 idLavoratore, String codiceFiscaleLavoratore)
        {
            return dataAccess.GetIscrizioniLavoratore(utente, idLavoratore, codiceFiscaleLavoratore);
        }

        public Boolean IscriviLavoratore(out String messaggio, Utente utente, Iscrizione iscrizione)
        {
            return dataAccess.IscriviLavoratore(out messaggio, utente, iscrizione);
        }

        public Boolean CorsiGratuita(Int32 idLavoratore, DateTime dataIscrizione)
        {
            return dataAccess.CorsiGratuita(idLavoratore, dataIscrizione);
        }

        public Boolean CorsiTicket(DateTime dataCorso, DateTime dataAssunzione, Boolean primaEsperienza)
        {
            return dataAccess.CorsiTicket(dataCorso, dataAssunzione, primaEsperienza);
        }

        public Boolean CancellaIscrizione(out String messaggio, Utente utente, Iscrizione iscrizione)
        {
            return dataAccess.CancellaIscrizione(out messaggio, utente, iscrizione);
        }
    }
}
