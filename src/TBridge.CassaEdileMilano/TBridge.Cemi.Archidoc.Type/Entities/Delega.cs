﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public class Delega : Documento
    {
        public int Barcode { get; set; }
        public String CodiceSindacato { get; set; }
        public String CodiceComprensorio { get; set; }
        public DateTime? DataConferma { get; set; }
        public DateTime? DataAdesione { get; set; }

        public String NomeAllegatoLettera { get; set; }
        public String NomeAllegatoBusta { get; set; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            switch (archidocKey)
            {                
                default:
                    return base.Get(archidocKey, valoreOriginale);
                    break;
            }
        }

        public override void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    Nome = valore;
                    break;
                case "svIfKey13":
                case "svwsIfKey13":
                    DateTime data;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    int id;
                    if (Int32.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    int id2;
                    if (Int32.TryParse(valore, out id2))
                        Barcode = id2;
                    break;
                case "svIfKey23":
                case "svwsIfKey23":
                    CodiceFiscale = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    CodiceSindacato = valore;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    CodiceComprensorio = valore;
                    break;
                case "svIfKey33":
                case "svwsIfKey33":
                    DateTime data2;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data2))
                        DataConferma = data2;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    DateTime data3;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data3))
                        DataAdesione = data3;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}