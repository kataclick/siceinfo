﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public class Fattura : Documento
    {
        public Int32? IdPrestazioniFattura { get; set; }
        public DateTime? Data { get; set; }
        public Decimal? Totale { get; set; }
        public String Numero { get; set; }
        public bool? Valida { get; set; }
        public DateTime? DataAssociazione { get; set; }
        public DateTime? DataValidazione { get; set; }
        public bool? Originale { get; set; }
        public String IdTipoPrestazione { get; set; }
        public Int32? NumeroProtocolloPrestazione { get; set; }
        public Int32? ProtocolloPrestazione { get; set; }
        public DateTime? DataAggiornamentoArchidoc { get; set; }

        public Int32? IdPrestazioniFatturaDichiarata { set; get; }   // Vedi tabella [dbo].[PrestazioniFattureDichiarate]
        public Int32? IdPrestazioniDomanda { set; get; }
        public Int32? IdFamiliare { set; get; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            string ret;

            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    ret = Cognome;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    ret = Nome;
                    break;
                case "svIfKey13":
                case "svwsIfKey13":
                    if (DataNascita.HasValue)
                        ret = DataNascita.Value.ToString("dd/MM/yyyy");
                    else 
                        ret = valoreOriginale;
                    break;
                case "svIfKey14":
                case "svwsIfKey14":
                    ret = IdPrestazioniFatturaDichiarata.HasValue ? IdPrestazioniFatturaDichiarata.ToString() : valoreOriginale;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    if (IdLavoratore.HasValue)
                        ret = IdLavoratore.Value.ToString();
                    else 
                        ret = valoreOriginale;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    if (NumeroProtocolloPrestazione.HasValue)
                        ret = NumeroProtocolloPrestazione.Value.ToString();
                    else
                        ret = valoreOriginale;
                    break;
                case "svIfKey23":
                case "svwsIfKey23":
                    if (ProtocolloPrestazione.HasValue)
                        ret = ProtocolloPrestazione.Value.ToString();
                    else 
                        ret = valoreOriginale;
                    break;
                case "svIfKey24":
                case "svwsIfKey24":
                    ret = ConvertIdTipoPrestazioneToArchidoc(IdTipoPrestazione);
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    ret =  Numero;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    ret = Data.HasValue ? Data.Value.ToString("dd/MM/yyyy") : valoreOriginale;
                    break;
                case "svIfKey33":
                case "svwsIfKey33":
                    ret = Totale.HasValue ? Totale.Value.ToString("0.00") : valoreOriginale;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    ret = IdPrestazioniDomanda.HasValue ? IdPrestazioniDomanda.Value.ToString() : valoreOriginale;
                    break;
                case "svIfKey35":
                case "svwsIfKey35":
                    ret = IdPrestazioniFatturaDichiarata.HasValue ? IdPrestazioniFatturaDichiarata.Value.ToString() : valoreOriginale;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    ret = Originale.HasValue && Originale.Value ? "SI" : "NO";;
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    ret = IdFamiliare.HasValue ? IdFamiliare.Value.ToString() : valoreOriginale;
                    break;
                case "svIfKey43":
                case "svwsIfKey43":
                    ret = CodiceFiscale;
                    break;
                default:
                    ret = base.Get(archidocKey, valoreOriginale);
                    break;
            }

          
            return ret;
        }

        public override void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    Nome = valore;
                    break;
                case "svIfKey13":
                case "svwsIfKey13":
                    DateTime data;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;
                    break;
                case "svIfKey14":
                case "svwsIfKey14":
                    Int32 idFattura;
                    if (Int32.TryParse(valore, out idFattura))
                    {
                        IdPrestazioniFatturaDichiarata = idFattura;
                    }
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    int id;
                    if (Int32.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    int numerop;
                    if (Int32.TryParse(valore, out numerop))
                        NumeroProtocolloPrestazione = numerop;
                    break;
                case "svIfKey23":
                case "svwsIfKey23":
                    int prot;
                    if (Int32.TryParse(valore, out prot))
                        ProtocolloPrestazione = prot;
                    break;
                case "svIfKey24":
                case "svwsIfKey24":
                    IdTipoPrestazione = ConvertIdTipoPrestazioneFromArchidoc(valore);
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    Numero = valore;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    DateTime datafattura;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out datafattura))
                        Data = datafattura;
                    break;
                case "svIfKey33":
                case "svwsIfKey33":
                    Decimal totale;
                    if (Decimal.TryParse(valore, out totale))
                        Totale = totale;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    int idPrest;
                    if (Int32.TryParse(valore, out idPrest))
                    {
                        IdPrestazioniDomanda = idPrest;
                    }
                    break;
                case "svIfKey35":
                case "svwsIfKey35":
                    int idFatDic;
                    if (Int32.TryParse(valore, out idFatDic))
                    {
                        IdPrestazioniFatturaDichiarata = idFatDic;
                    }
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    Originale = (valore == "SI" ? true : false);
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    int idFam;
                    if (!String.IsNullOrEmpty(valore) &&  int.TryParse(valore, out idFam))
                    {
                        IdFamiliare = idFam;
                    }
                    break;
                case "svIfKey43":
                case "svwsIfKey43":
                    CodiceFiscale = valore;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}