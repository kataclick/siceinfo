﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    public class MalattiaTelematica : Documento
    {
        public int IdMalattiaTelematica { get; set; }
        public int IdImpresa { get; set; }
        public String Tipo { get; set; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            String ret;
            switch (archidocKey)
            {
                case "svIfKey12":
                case "svwsIfKey12":
                    ret = IdMalattiaTelematica.ToString();
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    ret = Tipo;
                    break;
                case "svIfKey11":
                case "svwsIfKey11":
                    ret = Cognome;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    ret = Nome;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    if (DataNascita.HasValue)
                        ret = DataNascita.Value.ToString("dd/MM/yyyy");
                    else 
                        ret = valoreOriginale;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    if (IdLavoratore.HasValue)
                        ret = IdLavoratore.Value.ToString();
                    else 
                        ret = valoreOriginale;
                    break;
                case "svIfDateDoc":
                case "svwsIfDateDoc":
                    if (DataInserimentoRecord.HasValue)
                        ret = DataInserimentoRecord.Value.ToString("dd/MM/yyyy");
                    else
                        ret = valoreOriginale;
                    break;
                default:
                    ret = base.Get(archidocKey, valoreOriginale);
                    break;
            }

            return ret;     
        }

        public override void Set(string archidocKey, string valore)
        {
            DateTime data;
            int id;
            switch (archidocKey)
            {
                case "svIfKey12":
                case "svwsIfKey12":
                    if (Int32.TryParse(valore, out id))
                    {
                        IdMalattiaTelematica = id;
                    }
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    Tipo = valore;
                    break;
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    Nome = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":            
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;              
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    
                    if (Int32.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfDateDoc":
                case "svwsIfDateDoc":
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataInserimentoRecord = data; 
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}
