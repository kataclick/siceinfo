﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public abstract class Documento
    {
        public Int32? IdDocumento { get; set; }     //identity autoincrementale sulle nostre tabelle di appoggio non viene mai usato
        public String IdArchidoc { get; set; }

        public Int32? IdLavoratore { get; set; }

        public String Cognome { get; set; }
        public String Nome { get; set; }
        public String CodiceFiscale { get; set; }
        public String ProtocolloScansione { get; set; }    // Progressivo annuo univoco

        public DateTime? DataNascita { get; set; }
        public DateTime? DataScansione { get; set; }
        public DateTime? DataInserimentoRecord { get; set; }
        public DateTime? DataModificaRecord { get; set; }

        public byte[] FileByteArray { get; set; }
        public String FileNome { get; set; }
        public String FileEstensione { get; set; }

        public String ConvertIdTipoCertificatoToArchidoc(Int32 tipo)
        {
            switch (tipo)
            {
                case 1:
                    return "01 PIANO DI STUDI";
                case 2:
                    return "02 CERTIFICATO DEGLI ESAMI SOSTENUTI CON VOTAZIONI";
                case 3:
                    return "03 CERTIFICATO DI IMMATRICOLAZIONE ALL UNIVERSITA";
                case 4:
                    return "04 ATTESTATO DI ISCRIZIONE E FREQUENZA ALLA 1A CLASSE ";
                case 5:
                    return "05 CERTIFICATO DI PROMOZIONE ORIGINALE O AUTENTICATO DALLA SCUOLA ";
                case 6:
                    return "06 COPIA DEL VERBALE DELLA COMMISSIONE ASL ATTESTANTE LO STATO DI INVALIDITA";
                case 7:
                    return "07 COPIA VERBALE DELLE AUTORITA INTERVENUTE SUL LUOGO DELL INFORTUNIO IN CASO DI MORTE DEL LAVORATORE";
                case 8:
                    return "08 DICHIARAZIONE RELATIVA ALLA SITUAZIONE FISCALE ";
                case 9:
                    return "09 DICHIARAZIONE SOSTITUTIVA DELL ATTO DI NOTORIETA AUTENTICATA DAL COMUNE DI RESIDENZA";
                case 10:
                    return "10 MODELLO INPS HAND2 O HAND3 IN CASO DI PERMESSI AI SENSI DELLA LEGGE 104/92";
                case 11:
                    return "11 ORIGINALE DEL CERTIFICATO DI MORTE O FOTOCOPIA AUTENTICATA DA PUBBLICO UFFICIALE ";
               default:
                    return tipo.ToString().PadLeft(2,'0');
            }
        }


        public String ConvertIdTipoPrestazioneToArchidoc(String tipo)
        {
            switch (tipo)
            {
                case "C002":
                    return "C002 / ASSEGNO FUNERARIO";
                case "C002-L":
                    return "C002 L / ASSEGNO FUNERARIO LAVORATORE";
                case "C003-1":
                    return "C003 1 / BORSA STUDIO SCUOLE SEC. 2  GRADO O RIMB.SPESE DID";
                case "C003-2":
                    return "C003 2 / BORSA DI STUDIO UNIVERSITARIA";
                case "C003-3":
                    return "C003 3 / BORSA DI STUDIO UNIVERSITARIE,INGEGNERIA CIVILE";
                case "C004":
                    return "C004 / PROTESI E CURE DENTARIE";
                case "C004-1":
                    return "C004 1 / PROTESI E CURE DENTARIE PER FAMILIARI";
                case "C004CP":
                    return "C004 CP / CONTRIBUTO PER PROTESI E CURE DENTARIE";
                case "C005":
                    return "C005 / PROTESI ACUSTICHE";
                case "C005-1":
                    return "C005 1 / PROTESI ACUSTICHE PER FAMILIARE";
                case "C006":
                    return "C006 / ACCESSO FIGLI SCUOLA SEC. 1  GRADO";
                case "C007-1":
                    return "C007 1 / LENTI OCULISTICHE CURATIVE";
                case "C007-2":
                    return "C007 2 / LENTI OCULISTICHE CURATIVE PER FAMILIARI";
                case "C007-3":
                    return "C007 3 / LENTI OCULISTICHE CURATIVE PER FAMILIARI";
                case "C008":
                    return "C008 / PROTESI ORTODONTICHE PER FIGLI";
                case "C009":
                    return "C009 / PROTESI ORTOPEDICHE";
                case "C009-1":
                    return "C009 1 / PROTESI ORTOPEDICHE PER FAMILIARI";
                case "C010":
                    return "C010 / VISITE MED. SPEC. E ACCERT. DIAGN";
                case "C010-F":
                    return "C010 F / VISITE MED.SPEC.E ACCERT.DIAGN. FAMILIAR";
                case "C014":
                    return "C014 / CONTRIBUTO PORTATORI DI HANDICAP LAVORATORE";
                case "C014-C":
                    return "C014 C / CONTRIBUTO PORTATORI DI HANDICAP PER CONIUGE";
                case "C014-F":
                    return "C014 F / CONTRIBUTO PORTATORI DI HANDICAP PER FAMILIARI";
                // non gestite da SiceInfo
                case "B002":
                    return "B002 / ASSEGNO FUNERARIO";
                case "B014":
                    return "B014 / CONTRIBUTO PORTATORI DI HANDICAP LAVORATORE";
                case "B014-C":
                    return "B014 C / CONTRIBUTO PORTATORI DI HANDICAP PER CONIUGE";
                case "B014-F":
                    return "B014 F / CONTRIBUTO PORTATORI DI HANDICAP PER FAMILIARI";
                case "B015":
                    return "B015 / ASSEGNO STRAORDINARIO";
                case "B020":
                    return "B020 / ABBANDONO DEL SETTORE A CAUSA DI GRAVE INFORTUNIO";
                case "B021":
                    return "B021 / UNA TANTUM PER LA FORM. DEI LAV. IN DISOC.SPECIALE";
                case "C-ANID":
                    return "C ANID / CONTRIB. FREQUENZA FIGLI ASILO NIDO";
                case "CARENZ":
                    return "CARENZ / CARENZA MALATTIA";
                case "SETBIA":
                    return "SETBIA / SETTIMANA BIANCA";
                case "VILLAG":
                    return "VILLAG / VILLAGGI VACANZE";
                case "Z001":
                    return "Z001 / EDILCARD";
                case "Z002":
                    return "Z002 / CURE TERMALI";
                case "PRENAT":
                    return "PRENAT / PREMIO NATALITA'";
                default:
                    return tipo;
            }
        }

        public String ConvertIdTipoPrestazioneFromArchidoc(String tipo)
        {            
            switch (tipo)
            {
                case "C002 / ASSEGNO FUNERARIO":
                    return "C002";
                    break;
                case "C002 L / ASSEGNO FUNERARIO LAVORATORE":
                    return "C002-L";
                    break;
                case "C003 1 / BORSA STUDIO SCUOLE SEC. 2  GRADO O RIMB.SPESE DID":
                    return  "C003-1";
                    break;
                case "C003 2 / BORSA DI STUDIO UNIVERSITARIA":
                    return  "C003-2";
                    break;
                case "C003 3 / BORSA DI STUDIO UNIVERSITARIE,INGEGNERIA CIVILE":
                    return  "C003-3";
                    break;
                case "C004 / PROTESI E CURE DENTARIE":
                    return  "C004";
                    break;
                case "C004 1 / PROTESI E CURE DENTARIE PER FAMILIARI":
                    return  "C004-1";
                case "C004 CP / CONTRIBUTO PER PROTESI E CURE DENTARIE":
                    return "C004CP";
                case "C005 / PROTESI ACUSTICHE":
                    return  "C005";
                    break;
                case "C005 1 / PROTESI ACUSTICHE PER FAMILIARE":
                    return  "C005-1";
                    break;
                case "C006 / ACCESSO FIGLI SCUOLA SEC. 1  GRADO":
                    return  "C006";
                    break;
                case "C007 1 / LENTI OCULISTICHE CURATIVE":
                    return  "C007-1";
                    break;
                case "C007 2 / LENTI OCULISTICHE CURATIVE PER FAMILIARI":
                    return  "C007-2";
                    break;
                case "C007 3 / LENTI OCULISTICHE CURATIVE PER FAMILIARI":
                    return  "C007-3";
                    break;
                case "C008 / PROTESI ORTODONTICHE PER FIGLI":
                    return  "C008";
                    break;
                case "C009 / PROTESI ORTOPEDICHE":
                    return  "C009";
                    break;
                case "C009 1 / PROTESI ORTOPEDICHE PER FAMILIARI":
                    return  "C009-1";
                    break;
                case "C010 / VISITE MED. SPEC. E ACCERT. DIAGN":
                    return  "C010";
                    break;
                case "C010 F / VISITE MED.SPEC.E ACCERT.DIAGN. FAMILIAR":
                    return  "C010-F";
                    break;
                case "C014 / CONTRIBUTO PORTATORI DI HANDICAP LAVORATORE":
                    return  "C014";
                    break;
                case "C014 C / CONTRIBUTO PORTATORI DI HANDICAP PER CONIUGE":
                    return  "C014-C";
                    break;
                case "C014 F / CONTRIBUTO PORTATORI DI HANDICAP PER FAMILIARI":
                    return  "C014-F";
                    break;
                case "PRENAT / PREMIO NATALITA'":
                    return "PRENAT";
                    break;
                default:
                    if (tipo.IndexOf("/") > 0)
                        return (tipo.Substring(0, tipo.IndexOf("/"))).TrimEnd().Replace(" ", "-");
                    else return tipo;
                    break;
            }
        }

        /// <summary>
        /// ritorna il nome della classe privo del namespace
        /// </summary>
        /// <returns></returns>
        public string ToShortClassName()
        {
            int ultimoPunto = ToString().LastIndexOf(".");
            return (ToString().Substring(ultimoPunto + 1));
        }

        public virtual string Get(string archidocKey, string valoreOriginale)
        {
            String ret;
            switch (archidocKey)
            {
                case "svIfReference":
                case "svwsIfReference":
                    ret = (this).ProtocolloScansione;
                    break;
                //case "svwsIfProtocol":
                //    return (this).ProtocolloScansione;
                //    break;
                case "svIfDateReg":
                case "svwsIfDateReg":
                    if (DataScansione.HasValue)
                        ret = (this).DataScansione.Value.ToString("dd/MM/yyyy");
                    else
                        ret = valoreOriginale;
                    break;
                default:
                    ret = valoreOriginale;
                    break;
            }

            return ret;
        }


        public virtual void SetIdArchidoc(string idArchidoc)
        {
            IdArchidoc = idArchidoc;
        }

        public virtual void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfReference":
                case "svwsIfReference":
                    ProtocolloScansione = valore;
                    break;
                //case "svwsIfProtocol": --- Non viene usato, non è chiave in archidoc
                //    ProtocolloScansione = valore;
                //    break;
                case "svIfDateReg":
                case "svwsIfDateReg":
                    DateTime data;
                    if (DateTime.TryParseExact(valore,"dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataScansione = data;
                    break;
                default:
                    break;
            }
        }

        public virtual String ParseIdTipoPrestazione(String valore)
        {
            String result = String.Empty;

            if (!String.IsNullOrEmpty(valore))
            {
                String[] valori = valore.Split(' ');
                if (valore.Length > 0)
                    result = valori[0].Trim();
            }

            return result;
        }

        public virtual DateTime? ParseData(DateTime? data)
        {
            if (data.HasValue)
            {
                if (DateTime.Compare(data.Value, new DateTime(1972, 1, 1)) >=0
                    && DateTime.Compare(data.Value, new DateTime(2079, 6, 6)) <= 0)
                {
                    return data;
                }
            }
            
            return null;
        }
    }
}