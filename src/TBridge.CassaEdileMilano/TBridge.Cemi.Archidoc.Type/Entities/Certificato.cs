﻿using System;
using System.Globalization;
using System.Threading;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public class Certificato : Documento
    {
        public Int32? ProtocolloPrestazione { get; set; }
        public Int32? NumeroProtocolloPrestazione { get; set; }
        public Int32? IdPrestazioniDomanda { get; set; }
        public Int32? IdTipoCertificatoArchidoc { get; set; }
        public bool? Originale { get; set; }
        public Int32? IdFamiliare { get; set; }
        public String IdTipoPrestazione { get; set; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    return Cognome;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    return Nome;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    if (DataNascita.HasValue)
                        return DataNascita.Value.ToString("dd/MM/yyyy");
                    else return valoreOriginale;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    if (IdLavoratore.HasValue)
                        return IdLavoratore.Value.ToString();
                    else return valoreOriginale;
                    break;
                case "svIfKey23":
                case "svwsIfKey23":
                    return NumeroProtocolloPrestazione.ToString();
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    return ProtocolloPrestazione.ToString();
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    return ConvertIdTipoPrestazioneToArchidoc(IdTipoPrestazione);
                    break;
                case "svIfKey33":
                case "svwsIfKey33":
                    if (IdTipoCertificatoArchidoc.HasValue)
                        return ConvertIdTipoCertificatoToArchidoc(IdTipoCertificatoArchidoc.Value);
                    else return valoreOriginale;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    return IdPrestazioniDomanda.HasValue ? IdPrestazioniDomanda.Value.ToString() : valoreOriginale;
                    break;
                //case "svIfKey41":
                //case "svwsIfKey41":
                //    return Originale.HasValue && Originale.Value ? "SI" : "NO";
                //    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    return IdFamiliare.HasValue ? IdFamiliare.Value.ToString() : valoreOriginale;
                    break;
                case "svIfKey43":
                case "svwsIfKey43":
                    return CodiceFiscale;
                    break;
                default:
                    return base.Get(archidocKey, valoreOriginale);
                    break;
            }
        }

        public override void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    Nome = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    DateTime data;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    int id;
                    if (Int32.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfKey23":
                case "svwsIfKey23":
                    int numerop;
                    if (Int32.TryParse(valore, out numerop))
                        NumeroProtocolloPrestazione = numerop;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    int prot;
                    if (Int32.TryParse(valore, out prot))
                        ProtocolloPrestazione = prot;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    IdTipoPrestazione = ConvertIdTipoPrestazioneFromArchidoc(valore);
                    break;
                case "svIfKey33":
                case "svwsIfKey33":
                    int certificato;
                    if (valore.Length > 2)
                    {
                        if (Int32.TryParse(valore.Substring(0, 2), out certificato))
                            IdTipoCertificatoArchidoc = certificato;
                    }
                    else
                    {
                        if (Int32.TryParse(valore, out certificato))
                            IdTipoCertificatoArchidoc = certificato;
                    }
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    int idPrest;
                    if (Int32.TryParse(valore, out idPrest))
                    {
                        IdPrestazioniDomanda = idPrest;
                    }
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    Originale = (valore=="SI"? true: false);
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    int idFam;
                    if (!String.IsNullOrEmpty(valore) && Int32.TryParse(valore, out idFam))
                        IdFamiliare = idFam;
                    break;
                case "svIfKey43":
                case "svwsIfKey43":
                    CodiceFiscale = valore;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}