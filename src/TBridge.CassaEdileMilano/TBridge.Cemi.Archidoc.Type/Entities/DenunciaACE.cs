﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    public class DenunciaACE : Documento
    {
        public int IdRichiesta { get; set; }
        public int IdImpresa { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String CodiceFiscaleIVAImpresa { get; set; }
        public String AllegatoNome { get; set; }
        public String Tipo { get; set; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            String ret = null;
            switch (archidocKey)
            {
                //    case "svIfKey12":
                //    case "svwsIfKey12":
                //        ret = IdMalattiaTelematica.ToString();
                //        break;
                //    case "svIfKey22":
                //    case "svwsIfKey22":
                //        ret = Tipo;
                //        break;
                case "svIfKey11":
                case "svwsIfKey11":
                    ret = RagioneSocialeImpresa;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    ret = CodiceFiscaleIVAImpresa;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    ret = IdImpresa.ToString();
                    break;
                case "svwsIfKey41":
                    ret = IdRichiesta.ToString();
                    break;
                case "svIfDateDoc":
                case "svwsIfDateDoc":
                    if (DataInserimentoRecord.HasValue)
                        ret = DataInserimentoRecord.Value.ToString("dd/MM/yyyy");
                    else
                        ret = valoreOriginale;
                    break;
                default:
                    ret = base.Get(archidocKey, valoreOriginale);
                    break;
            }

            return ret;
        }

        public override void Set(string archidocKey, string valore)
        {
            DateTime data;
            int id;
            switch (archidocKey)
            {
                //    case "svIfKey12":
                //    case "svwsIfKey12":
                //        if (Int32.TryParse(valore, out id))
                //        {
                //            IdMalattiaTelematica = id;
                //        }
                //        break;
                //    case "svIfKey22":
                //    case "svwsIfKey22":
                //        Tipo = valore;
                //        break;
                case "svIfKey11":
                case "svwsIfKey11":
                    RagioneSocialeImpresa = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    CodiceFiscaleIVAImpresa = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    if (Int32.TryParse(valore, out id))
                        IdImpresa = id;
                    break;
                case "svwsIfKey41":
                    if (Int32.TryParse(valore, out id))
                        IdRichiesta = id;
                    break;
                case "svIfDateDoc":
                case "svwsIfDateDoc":
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataInserimentoRecord = data;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}
