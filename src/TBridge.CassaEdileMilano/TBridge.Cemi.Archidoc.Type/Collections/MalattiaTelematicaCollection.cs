﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TBridge.Cemi.Archidoc.Type.Entities;

namespace TBridge.Cemi.Archidoc.Type.Collection
{
    [Serializable]
    public class MalattiaTelematicaCollection : List<MalattiaTelematica>
    {
    }
}
