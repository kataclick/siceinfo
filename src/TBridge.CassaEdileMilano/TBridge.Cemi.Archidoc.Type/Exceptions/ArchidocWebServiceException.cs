﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Archidoc.Type.Exceptions
{
    public class ArchidocWebServiceException : Exception
    {
        public ArchidocWebServiceException(String message)
            :base(message)
        {
        }
    }
}
