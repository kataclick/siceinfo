﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Archidoc.Type.Exceptions
{
    public class ArchidocUpdateSiceInfoException : Exception
    {
        public ArchidocUpdateSiceInfoException(String message)
            :base(message)
        {
        }
    }
}
