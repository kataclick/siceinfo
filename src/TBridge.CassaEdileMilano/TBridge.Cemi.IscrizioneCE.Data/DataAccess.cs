using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Cantieri.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Type.Entities;
using TBridge.Cemi.IscrizioneCE.Type.Collections;
using TBridge.Cemi.IscrizioneCE.Type.Entities;
using TBridge.Cemi.IscrizioneCE.Type.Enums;
using TBridge.Cemi.IscrizioneCE.Type.Exceptions;
using TBridge.Cemi.IscrizioneCE.Type.Filters;
using Lavoratore = TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore;

namespace TBridge.Cemi.IscrizioneCE.Data
{
    public class IscrizioneCEDataAccess
    {
        private Database databaseCemi;

        public IscrizioneCEDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        #region Metodi per i controlli

        public bool PartitaIvaEsistente(string partitaIva)
        {
            bool res = true;

            int dbRes;
            using (
                DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloPresenzaPartitaIva")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);

                dbRes = (int) databaseCemi.ExecuteScalar(comando);
            }
            if (dbRes == 0)
                res = false;

            return res;
        }

        public bool CodiceFiscaleEsistente(string codiceFiscale)
        {
            bool res = true;

            int dbRes;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloPresenzaCodiceFiscale"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                dbRes = (int) databaseCemi.ExecuteScalar(comando);
            }
            if (dbRes == 0)
                res = false;

            return res;
        }

        public bool PartitaIvaEsistenteDomande(string partitaIva, Int32? idDomanda)
        {
            bool res = true;

            int dbRes;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloPresenzaPartitaIvaDomande"))
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);
                if (idDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                dbRes = (int) databaseCemi.ExecuteScalar(comando);
            }
            if (dbRes == 0)
                res = false;

            return res;
        }

        public bool CodiceFiscaleEsistenteDomande(string codiceFiscale, Int32? idDomanda)
        {
            bool res = true;

            int dbRes;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloPresenzaCodiceFiscaleDomande"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                if (idDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                dbRes = (int) databaseCemi.ExecuteScalar(comando);
            }
            if (dbRes == 0)
                res = false;

            return res;
        }

        public int? LegaleRappresentanteInAnagrafica(string cognome, string nome, DateTime dataNascita,
                                                     string codiceFiscale)
        {
            int? res = null;

            object iRes;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloPresenzaLegaleRappresentante"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
                databaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);

                databaseCemi.ExecuteNonQuery(comando);
                iRes = databaseCemi.GetParameterValue(comando, "@idLavoratore");
            }

            if (iRes != DBNull.Value)
                res = (int) iRes;

            return res;
        }

        public bool BancaEsistente(Banca banca)
        {
            bool res = true;

            int dbRes;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloPresenzaBanca"))
            {
                DatabaseCemi.AddInParameter(comando, "@abi", DbType.Int32, banca.Abi);
                DatabaseCemi.AddInParameter(comando, "@cab", DbType.Int32, banca.Cab);

                dbRes = (int) databaseCemi.ExecuteScalar(comando);
            }
            if (dbRes == 0)
                res = false;

            return res;
        }

        public bool CapInAnagrafica(string codiceCatastale, string frazione, string cap)
        {
            bool res;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEControlloCapInAnagrafica")
                )
            {
                DatabaseCemi.AddInParameter(comando, "@codiceCatastale", DbType.String, codiceCatastale);
                if (!string.IsNullOrEmpty(frazione))
                    DatabaseCemi.AddInParameter(comando, "@frazione", DbType.String, frazione);
                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cap);
                DatabaseCemi.AddOutParameter(comando, "@res", DbType.Boolean, 1);

                databaseCemi.ExecuteNonQuery(comando);

                res = (bool) databaseCemi.GetParameterValue(comando, "@res");
            }

            return res;
        }

        #endregion

        #region Metodi per l'inserimento della domanda

        public bool InsertUpdateIscrizione(ModuloIscrizione iscrizione, Boolean modificaAbilitata)
        {
            bool res;

            using (DbConnection connection = databaseCemi.CreateConnection())
            {
                connection.Open();
                using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        if (InsertUpdateCantiere(iscrizione.Cantiere, transaction))
                        {
                            if (InsertUpdateLegaleRappresentante(iscrizione.LegaleRappresentante, transaction))
                            {
                                if (InsertUpdateIndirizzo(iscrizione.SedeLegale, transaction))
                                {
                                    if (InsertUpdateIndirizzo(iscrizione.SedeAmministrativa, transaction))
                                    {
                                        // Inserisco la domanda vera e propria...
                                        if (InsertUpdateDomanda(iscrizione, modificaAbilitata, transaction))
                                        {
                                            transaction.Commit();
                                            res = true;
                                        }
                                        else
                                            throw new Exception(
                                                "Iscrizione CE: L'inserimento della domanda non � andato a buon fine");
                                    }
                                    else
                                        throw new Exception(
                                            "Iscrizione CE: L'inserimento della sede amministrativa non � andato a buon fine");
                                }
                                else
                                    throw new Exception(
                                        "Iscrizione CE: L'inserimento della sede legale non � andato a buon fine");
                            }
                            else
                                throw new Exception(
                                    "Iscrizione CE: L'inserimento del legale rappresentante non � andato a buon fine");
                        }
                        else
                            throw new Exception("Iscrizione CE: L'inserimento del cantiere non � andato a buon fine");
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private bool InsertUpdateDomanda(ModuloIscrizione domanda, Boolean modificaAbilitata, DbTransaction transaction)
        {
            bool res = false;

            int iRres;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandeInsertUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, domanda.IdCassaEdile);
                if (domanda.IdDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, domanda.RagioneSociale);
                if (!string.IsNullOrEmpty(domanda.Impresa.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, domanda.Impresa.CodiceFiscale);
                if (!string.IsNullOrEmpty(domanda.Impresa.PartitaIVA))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, domanda.Impresa.PartitaIVA);
                if (!string.IsNullOrEmpty(domanda.Impresa.CodiceINPS))
                    DatabaseCemi.AddInParameter(comando, "@matricolaINPS", DbType.String, domanda.Impresa.CodiceINPS);
                if (!string.IsNullOrEmpty(domanda.Impresa.CodiceINAIL))
                    DatabaseCemi.AddInParameter(comando, "@matricolaINAIL", DbType.String, domanda.Impresa.CodiceINAIL);
                if (!string.IsNullOrEmpty(domanda.Impresa.ControCodiceINAIL))
                    DatabaseCemi.AddInParameter(comando, "@controCodiceINAIL", DbType.String,
                                                domanda.Impresa.ControCodiceINAIL);
                if (domanda.DataRichiestaIscrizione.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataRichiestaIscrizione", DbType.DateTime,
                                                domanda.DataRichiestaIscrizione.Value);
                if (!string.IsNullOrEmpty(domanda.CodiceOrganizzazioneImprenditoriale))
                    DatabaseCemi.AddInParameter(comando, "@codiceOrganizzazioneImprenditoriale", DbType.String,
                                                domanda.CodiceOrganizzazioneImprenditoriale);
                if (!string.IsNullOrEmpty(domanda.DescrizioneOrganizzazioneImprenditoriale))
                    DatabaseCemi.AddInParameter(comando, "@descrizioneOrganizzazioneImprenditoriale", DbType.String,
                                                domanda.DescrizioneOrganizzazioneImprenditoriale);
                if (!string.IsNullOrEmpty(domanda.PosizioneOrganizzazioneImprenditoriale))
                    DatabaseCemi.AddInParameter(comando, "@posizioneOrganizzazioneImprenditoriale", DbType.String,
                                                domanda.PosizioneOrganizzazioneImprenditoriale);
                if (!string.IsNullOrEmpty(domanda.NumeroIscrizioneCameraCommercio))
                    DatabaseCemi.AddInParameter(comando, "@numeroIscrizioneCameraCommercio", DbType.String,
                                                domanda.NumeroIscrizioneCameraCommercio);
                if (!string.IsNullOrEmpty(domanda.NumeroIscrizioneImpreseArtigiane))
                    DatabaseCemi.AddInParameter(comando, "@numeroIscrizioneImpreseArtigiane", DbType.String,
                                                domanda.NumeroIscrizioneImpreseArtigiane);
                if (domanda.DataIscrizioneCommercioArtigiane.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataIscrizioneCommercioArtigiane", DbType.DateTime,
                                                domanda.DataIscrizioneCommercioArtigiane.Value);
                if (!string.IsNullOrEmpty(domanda.CodiceTipoImpresa))
                    DatabaseCemi.AddInParameter(comando, "@idTipoImpresa", DbType.String, domanda.CodiceTipoImpresa);
                if (!string.IsNullOrEmpty(domanda.DescrizioneTipoImpresa))
                    DatabaseCemi.AddInParameter(comando, "@descrizioneTipoImpresa", DbType.String,
                                                domanda.DescrizioneTipoImpresa);
                if (!string.IsNullOrEmpty(domanda.CodiceNaturaGiuridica))
                    DatabaseCemi.AddInParameter(comando, "@idNaturaGiuridica", DbType.String,
                                                domanda.CodiceNaturaGiuridica);
                if (!string.IsNullOrEmpty(domanda.DescrizioneNaturaGiuridica))
                    DatabaseCemi.AddInParameter(comando, "@descrizioneNaturaGiuridica", DbType.String,
                                                domanda.DescrizioneNaturaGiuridica);
                if (!string.IsNullOrEmpty(domanda.CodiceAttivitaISTAT))
                    DatabaseCemi.AddInParameter(comando, "@idAttivitaISTAT", DbType.String, domanda.CodiceAttivitaISTAT);
                if (!string.IsNullOrEmpty(domanda.DescrizioneAttivitaISTAT))
                    DatabaseCemi.AddInParameter(comando, "@descrizioneAttivitaISTAT", DbType.String,
                                                domanda.DescrizioneAttivitaISTAT);
                if (!string.IsNullOrEmpty(domanda.CorrispondenzaPresso))
                    DatabaseCemi.AddInParameter(comando, "@corrispondenzaPresso", DbType.String,
                                                domanda.CorrispondenzaPresso);
                //DatabaseCemi.AddInParameter(comando, "@idBanca", DbType.Int32, domanda.Banca.IdBanca.Value);
                DatabaseCemi.AddInParameter(comando, "@idIndirizzo1", DbType.Int32, domanda.SedeLegale.IdIndirizzo.Value);
                DatabaseCemi.AddInParameter(comando, "@idIndirizzo2", DbType.Int32,
                                            domanda.SedeAmministrativa.IdIndirizzo.Value);
                DatabaseCemi.AddInParameter(comando, "@idLegaleRappresentante", DbType.Int32,
                                            domanda.LegaleRappresentante.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, domanda.Cantiere.IdCantiere.Value);
                DatabaseCemi.AddInParameter(comando, "@confermata", DbType.Boolean, domanda.Confermata);
                if (domanda.IdConsulente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, domanda.IdConsulente.Value);
                if (domanda.NumeroOpe.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroOperai", DbType.Int32, domanda.NumeroOpe.Value);
                if (domanda.NumeroOpeImp.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroImpiegati", DbType.Int32, domanda.NumeroOpeImp.Value);
                DatabaseCemi.AddInParameter(comando, "@geoOkSedeLegale", DbType.Boolean, domanda.GeocodificaOkSedeLegale);
                DatabaseCemi.AddInParameter(comando, "@geoOkSedeAmministrativa", DbType.Boolean,
                                            domanda.GeocodificaOkSedeAmministrativa);
                DatabaseCemi.AddInParameter(comando, "@geoOkCantiere", DbType.Boolean, domanda.GeocodificaOkCantiere);
                DatabaseCemi.AddInParameter(comando, "@cantiereInNotifiche", DbType.Boolean, domanda.CantiereInNotifiche);
                if (domanda.Stato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, domanda.Stato.Value);
                if (domanda.IdUtente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, domanda.IdUtente);
                DatabaseCemi.AddInParameter(comando, "@compilazioneConsulente", DbType.Boolean,
                                            domanda.CompilazioneConsulente);

                //aggiunta campi richiesti da elena: tipoInvioDenuncia
                if (!string.IsNullOrEmpty(domanda.IdTipoInvioDenuncia))
                    DatabaseCemi.AddInParameter(comando, "@idTipoInvioDenuncia", DbType.String,
                                                domanda.IdTipoInvioDenuncia);
                if (!string.IsNullOrEmpty(domanda.IdTipoIscrizione))
                    DatabaseCemi.AddInParameter(comando, "@idTipoIscrizione", DbType.String, domanda.IdTipoIscrizione);

                // Campo per distinguere la tipologia di modulo usato
                DatabaseCemi.AddInParameter(comando, "@tipoModulo", DbType.Int16, domanda.TipoModulo);

                if (!string.IsNullOrEmpty(domanda.IBAN))
                    DatabaseCemi.AddInParameter(comando, "@iban", DbType.String, domanda.IBAN);
                if (!string.IsNullOrEmpty(domanda.IntestatarioConto))
                    DatabaseCemi.AddInParameter(comando, "@intestatarioConto", DbType.String, domanda.IntestatarioConto);

                if (domanda.CodiceConsulente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@codiceConsulente", DbType.Int32,
                                                domanda.CodiceConsulente.Value);
                if (!string.IsNullOrEmpty(domanda.RagSocConsulente))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSocialeConsulente", DbType.String,
                                                domanda.RagSocConsulente);
                if (!string.IsNullOrEmpty(domanda.ComuneConsulente))
                    DatabaseCemi.AddInParameter(comando, "@comuneConsulente", DbType.String,
                                                domanda.ComuneConsulente);
                if (!string.IsNullOrEmpty(domanda.CodiceFiscaleConsulente))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscaleConsulente", DbType.String,
                                                domanda.CodiceFiscaleConsulente);
                if (!string.IsNullOrEmpty(domanda.IndirizzoConsulente))
                    DatabaseCemi.AddInParameter(comando, "@indirizzoConsulente", DbType.String,
                                                domanda.IndirizzoConsulente);
                if (!string.IsNullOrEmpty(domanda.ProvinciaConsulente))
                    DatabaseCemi.AddInParameter(comando, "@provinciaConsulente", DbType.String,
                                                domanda.ProvinciaConsulente);
                if (!string.IsNullOrEmpty(domanda.CodiceCatastaleConsulente))
                    DatabaseCemi.AddInParameter(comando, "@codiceCatastaleConsulente", DbType.String,
                                                domanda.CodiceCatastaleConsulente);
                if (!string.IsNullOrEmpty(domanda.CapConsulente))
                    DatabaseCemi.AddInParameter(comando, "@capConsulente", DbType.String,
                                                domanda.CapConsulente);
                if (!string.IsNullOrEmpty(domanda.TelefonoConsulente))
                    DatabaseCemi.AddInParameter(comando, "@telefonoConsulente", DbType.String,
                                                domanda.TelefonoConsulente);
                if (!string.IsNullOrEmpty(domanda.FaxConsulente))
                    DatabaseCemi.AddInParameter(comando, "@faxConsulente", DbType.String,
                                                domanda.FaxConsulente);
                if (!string.IsNullOrEmpty(domanda.EmailConsulente))
                    DatabaseCemi.AddInParameter(comando, "@emailConsulente", DbType.String,
                                                domanda.EmailConsulente);
                if (!string.IsNullOrEmpty(domanda.PecConsulente))
                    DatabaseCemi.AddInParameter(comando, "@pecConsulente", DbType.String,
                                                domanda.PecConsulente);

                if (!string.IsNullOrEmpty(domanda.PEC))
                    DatabaseCemi.AddInParameter(comando, "@pec", DbType.String,
                                                domanda.PEC);

                DatabaseCemi.AddInParameter(comando, "@guid", DbType.Guid,
                                                domanda.guid);

                DatabaseCemi.AddInParameter(comando, "@modificaAbilitata", DbType.Boolean,
                                                modificaAbilitata);

                iRres = Int32.Parse(databaseCemi.ExecuteScalar(comando, transaction).ToString());
            }
            if (iRres > 0)
            {
                domanda.IdDomanda = iRres;
                res = true;
            }
            else
            {
                if (iRres == -3)
                {
                    throw new DomandaGiaInseritaException();
                }
                else
                {
                    if (iRres == -2)
                    {
                        throw new PartitaIvaCodiceFiscaleGiaPresenteException();
                    }
                }
            }

            return res;
        }

        private bool InsertUpdateLegaleRappresentante(Lavoratore legaleRappresentante, DbTransaction transaction)
        {
            bool res = false;

            int iRres;
            using (
                DbCommand comando =
                    databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCELegaliRappresentantiInsertUpdate"))
            {
                if (legaleRappresentante.IdLavoratore > 0)
                    DatabaseCemi.AddInParameter(comando, "@idLegaleRappresentante", DbType.Int32,
                                                legaleRappresentante.IdLavoratore);
                if (!string.IsNullOrEmpty(legaleRappresentante.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, legaleRappresentante.Cognome);
                if (!string.IsNullOrEmpty(legaleRappresentante.Nome))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, legaleRappresentante.Nome);
                if (!string.IsNullOrEmpty(legaleRappresentante.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String,
                                                legaleRappresentante.CodiceFiscale);
                if (legaleRappresentante.DataNascita != new DateTime())
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime,
                                                legaleRappresentante.DataNascita);
                if (!string.IsNullOrEmpty(legaleRappresentante.LuogoNascita))
                    DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String,
                                                legaleRappresentante.LuogoNascita);
                if (!string.IsNullOrEmpty(legaleRappresentante.PaeseNascita))
                    DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String,
                                                legaleRappresentante.PaeseNascita);
                if (!string.IsNullOrEmpty(legaleRappresentante.IndirizzoComune))
                    DatabaseCemi.AddInParameter(comando, "@comuneResidenza", DbType.String,
                                                legaleRappresentante.IndirizzoComune);
                if (!string.IsNullOrEmpty(legaleRappresentante.IndirizzoFrazione))
                    DatabaseCemi.AddInParameter(comando, "@frazioneResidenza", DbType.String,
                                                legaleRappresentante.IndirizzoFrazione);
                if (!string.IsNullOrEmpty(legaleRappresentante.IndirizzoPreIndirizzo))
                    DatabaseCemi.AddInParameter(comando, "@preIndirizzoResidenza", DbType.String,
                                                legaleRappresentante.IndirizzoPreIndirizzo);
                if (!string.IsNullOrEmpty(legaleRappresentante.IndirizzoDenominazione))
                    DatabaseCemi.AddInParameter(comando, "@indirizzoResidenza", DbType.String,
                                                legaleRappresentante.IndirizzoDenominazione);
                if (!string.IsNullOrEmpty(legaleRappresentante.IndirizzoCAP))
                    DatabaseCemi.AddInParameter(comando, "@capResidenza", DbType.String,
                                                legaleRappresentante.IndirizzoCAP);
                if (!string.IsNullOrEmpty(legaleRappresentante.IndirizzoProvincia))
                    DatabaseCemi.AddInParameter(comando, "@provinciaResidenza", DbType.String,
                                                legaleRappresentante.IndirizzoProvincia);
                if (!string.IsNullOrEmpty(legaleRappresentante.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefonoResidenza", DbType.String,
                                                legaleRappresentante.Telefono);
                if (!string.IsNullOrEmpty(legaleRappresentante.Sesso))
                    DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, legaleRappresentante.Sesso);

                iRres = Int32.Parse(databaseCemi.ExecuteScalar(comando, transaction).ToString());
            }
            if (iRres != -1)
            {
                legaleRappresentante.IdLavoratore = iRres;
                res = true;
            }

            return res;
        }

        private bool InsertUpdateIndirizzo(Sede indirizzo, DbTransaction transaction)
        {
            bool res = false;

            int iRres;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEIndirizziInsertUpdate"))
            {
                if (indirizzo.IdIndirizzo.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idIndirizzo", DbType.Int32, indirizzo.IdIndirizzo.Value);
                if (!string.IsNullOrEmpty(indirizzo.PreIndirizzo))
                    DatabaseCemi.AddInParameter(comando, "@preIndirizzo", DbType.String, indirizzo.PreIndirizzo);
                if (!string.IsNullOrEmpty(indirizzo.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzo.Indirizzo);
                if (!string.IsNullOrEmpty(indirizzo.Comune))
                    DatabaseCemi.AddInParameter(comando, "@localita", DbType.String, indirizzo.Comune);
                if (!string.IsNullOrEmpty(indirizzo.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, indirizzo.Provincia);
                if (!string.IsNullOrEmpty(indirizzo.Frazione))
                    DatabaseCemi.AddInParameter(comando, "@frazione", DbType.String, indirizzo.Frazione);
                if (!string.IsNullOrEmpty(indirizzo.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, indirizzo.Cap);
                if (!string.IsNullOrEmpty(indirizzo.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, indirizzo.Telefono);
                if (!string.IsNullOrEmpty(indirizzo.Fax))
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, indirizzo.Fax);
                if (!string.IsNullOrEmpty(indirizzo.EMail))
                    DatabaseCemi.AddInParameter(comando, "@email", DbType.String, indirizzo.EMail);

                iRres = Int32.Parse(databaseCemi.ExecuteScalar(comando, transaction).ToString());
            }
            if (iRres != -1)
            {
                indirizzo.IdIndirizzo = iRres;
                res = true;
            }

            return res;
        }

        private bool InsertUpdateCantiere(Cantiere cantiere, DbTransaction transaction)
        {
            bool res = false;

            int iRres;
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCECantieriInsertUpdate"))
            {
                if (cantiere.IdCantiere.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32, cantiere.IdCantiere.Value);
                if (!string.IsNullOrEmpty(cantiere.PreIndirizzo))
                    DatabaseCemi.AddInParameter(comando, "@preIndirizzo", DbType.String, cantiere.PreIndirizzo);
                if (!string.IsNullOrEmpty(cantiere.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, cantiere.Indirizzo);
                if (!string.IsNullOrEmpty(cantiere.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, cantiere.Comune);
                if (!string.IsNullOrEmpty(cantiere.Frazione))
                    DatabaseCemi.AddInParameter(comando, "@frazione", DbType.String, cantiere.Frazione);
                if (!string.IsNullOrEmpty(cantiere.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, cantiere.Provincia);
                if (!string.IsNullOrEmpty(cantiere.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cantiere.Cap);
                if (!string.IsNullOrEmpty(cantiere.CommittenteTrovato))
                    DatabaseCemi.AddInParameter(comando, "@committente", DbType.String, cantiere.CommittenteTrovato);

                iRres = Int32.Parse(databaseCemi.ExecuteScalar(comando, transaction).ToString());
            }
            if (iRres != -1)
            {
                cantiere.IdCantiere = iRres;
                res = true;
            }

            return res;
        }

        /*
                private bool InsertUpdateBanca(Banca banca, DbTransaction transaction)
                {
                    bool res = false;

                    int iRres;
                    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEBancheInsertUpdate"))
                    {
                        if (banca.IdBanca.HasValue)
                            DatabaseCemi.AddInParameter(comando, "@idBanca", DbType.Int32, banca.IdBanca.Value);
                        if (!string.IsNullOrEmpty(banca.Cin))
                            DatabaseCemi.AddInParameter(comando, "@cin", DbType.String, banca.Cin);
                        if (!string.IsNullOrEmpty(banca.Abi))
                            DatabaseCemi.AddInParameter(comando, "@abi", DbType.String, banca.Abi);
                        if (!string.IsNullOrEmpty(banca.Cab))
                            DatabaseCemi.AddInParameter(comando, "@cab", DbType.String, banca.Cab);
                        if (!string.IsNullOrEmpty(banca.Banca1))
                            DatabaseCemi.AddInParameter(comando, "@descrizioneBanca", DbType.String, banca.Banca1);
                        if (!string.IsNullOrEmpty(banca.Agenzia))
                            DatabaseCemi.AddInParameter(comando, "@descrizioneAgenzia", DbType.String, banca.Agenzia);
                        if (!string.IsNullOrEmpty(banca.PreIndirizzo))
                            DatabaseCemi.AddInParameter(comando, "@preIndirizzo", DbType.String, banca.PreIndirizzo);
                        if (!string.IsNullOrEmpty(banca.Indirizzo))
                            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, banca.Indirizzo);
                        if (!string.IsNullOrEmpty(banca.Civico))
                            DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, banca.Civico);
                        if (!string.IsNullOrEmpty(banca.Comune))
                            DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, banca.Comune);
                        if (!string.IsNullOrEmpty(banca.Frazione))
                            DatabaseCemi.AddInParameter(comando, "@frazione", DbType.String, banca.Frazione);
                        if (!string.IsNullOrEmpty(banca.Provincia))
                            DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, banca.Provincia);
                        if (!string.IsNullOrEmpty(banca.Cap))
                            DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, banca.Cap);
                        if (!string.IsNullOrEmpty(banca.Telefono))
                            DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, banca.Telefono);
                        if (!string.IsNullOrEmpty(banca.Fax))
                            DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, banca.Fax);
                        if (!string.IsNullOrEmpty(banca.Cc))
                            DatabaseCemi.AddInParameter(comando, "@numeroCC", DbType.String, banca.Cc);
                        if (!string.IsNullOrEmpty(banca.Intestatario))
                            DatabaseCemi.AddInParameter(comando, "@intestatario", DbType.String, banca.Intestatario);
                        if (!string.IsNullOrEmpty(banca.CinPaese))
                            DatabaseCemi.AddInParameter(comando, "@cinPaese", DbType.String, banca.CinPaese);
                        if (!string.IsNullOrEmpty(banca.Paese))
                            DatabaseCemi.AddInParameter(comando, "@paese", DbType.String, banca.Paese);

                        iRres = Int32.Parse(databaseCemi.ExecuteScalar(comando, transaction).ToString());
                    }
                    if (iRres != -1)
                    {
                        banca.IdBanca = iRres;
                        res = true;
                    }

                    return res;
                }
        */

        #endregion

        #region Metodi per la ricerca delle domade

        public ModuloIscrizioneCollection GetDomande(ModuloIscrizioneFilter filtro, TipoOrdinamento? ordinamento)
        {
            ModuloIscrizioneCollection domande = new ModuloIscrizioneCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandeSelectPerRicerca"))
            {
                if (filtro.IdConsulente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente.Value);
                if (filtro.StatoDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, filtro.StatoDomanda.Value);
                if (filtro.Confermate.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@confermata", DbType.Boolean, filtro.Confermate.Value);
                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                if (filtro.CompilateConsulente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@compilateConsulente", DbType.Boolean,
                                                filtro.CompilateConsulente);
                if (filtro.Dal.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                if (filtro.Al.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);
                if (filtro.DataRichiestaDal.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataRichiestaDal", DbType.DateTime, filtro.DataRichiestaDal);
                if (filtro.DataRichiestaAl.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataRichiestaAl", DbType.DateTime, filtro.DataRichiestaAl);
                if (ordinamento.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@ordinamento", DbType.Int32, ordinamento);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        ModuloIscrizione modulo = new ModuloIscrizione();
                        domande.Add(modulo);

                        modulo.IdDomanda = reader.GetInt32(reader.GetOrdinal("idIscrizioneCEDomanda"));
                        modulo.Impresa.RagioneSociale = reader.GetString(reader.GetOrdinal("ragioneSociale"));

                        int tempOrdinal = reader.GetOrdinal("partitaIVA");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.Impresa.PartitaIVA = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("codiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.Impresa.CodiceFiscale = reader.GetString(tempOrdinal);

                        modulo.Confermata = reader.GetBoolean(reader.GetOrdinal("confermata"));

                        tempOrdinal = reader.GetOrdinal("stato");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.Stato = (StatoDomanda) reader.GetInt32(tempOrdinal);

                        modulo.CompilazioneConsulente = reader.GetBoolean(reader.GetOrdinal("compilazioneConsulente"));

                        tempOrdinal = reader.GetOrdinal("idUtente");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.IdUtente = reader.GetInt32(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("utente");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.Utente = reader.GetString(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("idConsulente");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.IdConsulente = reader.GetInt32(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("consulenteRagioneSociale");
                        if (!reader.IsDBNull(tempOrdinal))
                            modulo.RagioneSocialeConsulente = reader.GetString(tempOrdinal);

                        modulo.TipoModulo = (TipoModulo) ((short) reader["tipoModulo"]);

                        if (!Convert.IsDBNull(reader["idImpresa"]))
                            modulo.Impresa.IdImpresa = (int) reader["idImpresa"];

                        if (!Convert.IsDBNull(reader["dataPresentazione"]))
                            modulo.DataPresentazione = (DateTime) reader["dataPresentazione"];
                    }
                }
            }

            return domande;
        }

        public ModuloIscrizione GetDomanda(int idDomanda)
        {
            ModuloIscrizione domanda = new ModuloIscrizione();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandaSelectPerId"))
            {
                databaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        // DATI GENERALI
                        domanda.IdDomanda = reader.GetInt32(reader.GetOrdinal("idIscrizioneCEDomanda"));
                        domanda.Impresa.RagioneSociale = reader.GetString(reader.GetOrdinal("ragioneSociale"));
                        domanda.CompilazioneConsulente = reader.GetBoolean(reader.GetOrdinal("compilazioneConsulente"));

                        int tempOrdinal = reader.GetOrdinal("codiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Impresa.CodiceFiscale = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("partitaIVA");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Impresa.PartitaIVA = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("matricolaINPS");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Impresa.CodiceINPS = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("matricolaINAIL");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Impresa.CodiceINAIL = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("controCodiceINAIL");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Impresa.ControCodiceINAIL = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("dataRichiestaIscrizione");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.DataRichiestaIscrizione = reader.GetDateTime(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("codiceOrganizzazioneImprenditoriale");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.CodiceOrganizzazioneImprenditoriale = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("descrizioneOrganizzazioneImprenditoriale");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.DescrizioneOrganizzazioneImprenditoriale = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("posizioneOrganizzazioneImprenditoriale");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.PosizioneOrganizzazioneImprenditoriale = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("numeroIscrizioneCameraCommercio");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.NumeroIscrizioneCameraCommercio = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("numeroIscrizioneImpreseArtigiane");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.NumeroIscrizioneImpreseArtigiane = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("dataIscrizioneCommercioArtigiane");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.DataIscrizioneCommercioArtigiane = reader.GetDateTime(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("idTipoImpresa");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.CodiceTipoImpresa = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("descrizioneTipoImpresa");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.DescrizioneTipoImpresa = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("idNaturaGiuridica");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.CodiceNaturaGiuridica = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("descrizioneNaturaGiuridica");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.DescrizioneNaturaGiuridica = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("idAttivitaISTAT");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.CodiceAttivitaISTAT = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("descrizioneAttivitaISTAT");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.DescrizioneAttivitaISTAT = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("corrispondenzaPresso");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.CorrispondenzaPresso = reader.GetString(tempOrdinal);
                        domanda.Confermata = reader.GetBoolean(reader.GetOrdinal("confermata"));
                        tempOrdinal = reader.GetOrdinal("stato");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Stato = (StatoDomanda) reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("numeroOperai");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.NumeroOpe = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("numeroImpiegati");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.NumeroOpeImp = reader.GetInt32(tempOrdinal);
                        domanda.GeocodificaOkSedeLegale = reader.GetBoolean(reader.GetOrdinal("geocodificaOkSedeLegale"));
                        domanda.GeocodificaOkSedeAmministrativa =
                            reader.GetBoolean(reader.GetOrdinal("geocodificaOkSedeAmministrativa"));
                        domanda.GeocodificaOkCantiere = reader.GetBoolean(reader.GetOrdinal("geocodificaOkCantiere"));
                        domanda.CantiereInNotifiche = reader.GetBoolean(reader.GetOrdinal("cantiereInNotifiche"));
                        if (reader["idTipoInvioDenuncia"] != DBNull.Value)
                            domanda.IdTipoInvioDenuncia = (string) reader["idTipoInvioDenuncia"];
                        if (reader["codiceConsulente"] != DBNull.Value)
                            domanda.CodiceConsulente = (int) reader["codiceConsulente"];
                        if (reader["ragioneSocialeConsulente"] != DBNull.Value)
                            domanda.RagSocConsulente = (string) reader["ragioneSocialeConsulente"];
                        if (reader["comuneConsulente"] != DBNull.Value)
                            domanda.ComuneConsulente = (string) reader["comuneConsulente"];
                        if (reader["codiceFiscaleConsulente"] != DBNull.Value)
                            domanda.CodiceFiscaleConsulente = (string) reader["codiceFiscaleConsulente"];
                        if (!Convert.IsDBNull(reader["indirizzoConsulente"]))
                            domanda.IndirizzoConsulente = (string) reader["indirizzoConsulente"];
                        if (!Convert.IsDBNull(reader["provinciaConsulente"]))
                            domanda.ProvinciaConsulente = (string) reader["provinciaConsulente"];
                        if (!Convert.IsDBNull(reader["codiceCatastaleConsulente"]))
                            domanda.CodiceCatastaleConsulente = (string) reader["codiceCatastaleConsulente"];
                        if (!Convert.IsDBNull(reader["capConsulente"]))
                            domanda.CapConsulente = (string) reader["capConsulente"];
                        if (!Convert.IsDBNull(reader["telefonoConsulente"]))
                            domanda.TelefonoConsulente = (string) reader["telefonoConsulente"];
                        if (!Convert.IsDBNull(reader["faxConsulente"]))
                            domanda.FaxConsulente = (string) reader["faxConsulente"];
                        if (!Convert.IsDBNull(reader["emailConsulente"]))
                            domanda.EmailConsulente = (string) reader["emailConsulente"];
                        if (!Convert.IsDBNull(reader["pecConsulente"]))
                            domanda.PecConsulente = (string) reader["pecConsulente"];

                        if (!Convert.IsDBNull(reader["pec"]))
                            domanda.PEC = (string) reader["pec"];

                        // CONTO CORRENTE
                        if (reader["iban"] != DBNull.Value)
                            domanda.IBAN = (string) reader["iban"];
                        if (reader["intestatarioConto"] != DBNull.Value)
                            domanda.IntestatarioConto = (string) reader["intestatarioConto"];

                        // CANTIERE
                        domanda.Cantiere.IdCantiere = reader.GetInt32(reader.GetOrdinal("idIscrizioneCECantiere"));
                        tempOrdinal = reader.GetOrdinal("cantierePreIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.PreIndirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cantiereIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.Indirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cantiereComune");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.Comune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cantiereFrazione");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.Frazione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cantiereProvincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.Provincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cantiereCap");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cantiereCommittente");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.Cantiere.CommittenteTrovato = reader.GetString(tempOrdinal);

                        // LEGALE RAPPRESENTANTE
                        domanda.LegaleRappresentante.IdLavoratore =
                            reader.GetInt32(reader.GetOrdinal("idIscrizioneCELegaleRappresentante"));
                        tempOrdinal = reader.GetOrdinal("rapprCognome");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.Cognome = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprNome");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.Nome = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprCodiceFiscale");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.CodiceFiscale = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprDataNascita");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.DataNascita = reader.GetDateTime(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprLuogoNascita");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.LuogoNascita = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprProvinciaNascita");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.ProvinciaNascita = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprPaeseNascita");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.PaeseNascita = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprComuneResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.IndirizzoComune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprFrazioneResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.IndirizzoFrazione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprPreIndirizzoResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.IndirizzoPreIndirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprIndirizzoResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.IndirizzoDenominazione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprCapResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.IndirizzoCAP = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprProvinciaResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.IndirizzoProvincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("rapprTelefonoResidenza");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.LegaleRappresentante.Telefono = reader.GetString(tempOrdinal);
                        if (reader["rapprSesso"] != DBNull.Value)
                            domanda.LegaleRappresentante.Sesso = (string) reader["rapprSesso"];

                        // INDIRIZZO1
                        domanda.SedeLegale.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("indirizzo1Id"));
                        tempOrdinal = reader.GetOrdinal("indirizzo1PreIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.PreIndirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Indirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Indirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Localita");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Comune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Frazione");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Frazione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Provincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Cap");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Telefono");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Telefono = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Fax");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.Fax = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo1Email");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeLegale.EMail = reader.GetString(tempOrdinal);

                        // INDIRIZZO2
                        domanda.SedeAmministrativa.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("indirizzo2Id"));
                        tempOrdinal = reader.GetOrdinal("indirizzo2PreIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.PreIndirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Indirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Indirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Localita");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Comune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Frazione");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Frazione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Provincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Cap");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Telefono");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Telefono = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Fax");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.Fax = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo2Email");
                        if (!reader.IsDBNull(tempOrdinal))
                            domanda.SedeAmministrativa.EMail = reader.GetString(tempOrdinal);

                        // CONSULENTE
                        tempOrdinal = reader.GetOrdinal("idConsulente");
                        if (!reader.IsDBNull(tempOrdinal))
                        {
                            domanda.IdConsulente = reader.GetInt32(tempOrdinal);
                            domanda.Consulente = new Consulente();
                            domanda.Consulente.IdConsulente = reader.GetInt32(tempOrdinal);

                            tempOrdinal = reader.GetOrdinal("consulenteRagioneSociale");
                            if (!reader.IsDBNull(tempOrdinal))
                                domanda.Consulente.RagioneSociale = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("consulenteCodiceFiscale");
                            if (!reader.IsDBNull(tempOrdinal))
                                domanda.Consulente.CodiceFiscale = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("consulenteIndirizzo");
                            if (!reader.IsDBNull(tempOrdinal))
                                domanda.Consulente.Indirizzo = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("consulenteLocalita");
                            if (!reader.IsDBNull(tempOrdinal))
                                domanda.Consulente.Comune = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("consulenteProvincia");
                            if (!reader.IsDBNull(tempOrdinal))
                                domanda.Consulente.Provincia = reader.GetString(tempOrdinal);
                        }

                        // TIPO MODULO
                        domanda.TipoModulo = (TipoModulo) ((short) reader["tipoModulo"]);

                        if (!Convert.IsDBNull(reader["dataPresentazione"]))
                            domanda.DataPresentazione = (DateTime) reader["dataPresentazione"];
                    }
                }
            }

            return domanda;
        }

        #endregion

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        public bool CambiaStatoDomanda(int idDomanda, StatoDomanda statoDomanda, string idTipoIscrizione, int idUtente)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCECambioStatoDomanda"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, statoDomanda);
                if (!string.IsNullOrEmpty(idTipoIscrizione))
                    DatabaseCemi.AddInParameter(comando, "@idTipoIscrizione", DbType.String, idTipoIscrizione);

                if (databaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public Banca GetBancaSiceNew(int abi, int cab)
        {
            Banca banca = null;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_BancheSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@abi", DbType.Int32, abi);
                DatabaseCemi.AddInParameter(comando, "@cab", DbType.Int32, cab);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        banca = new Banca();

                        banca.Abi = reader.GetInt32(reader.GetOrdinal("ABI")).ToString("00000");
                        banca.Cab = reader.GetInt32(reader.GetOrdinal("CAB")).ToString("00000");
                        int tempOrdinal = reader.GetOrdinal("descrizioneBanca");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Banca1 = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("descrizioneAgenzia");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Agenzia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("preIndirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.PreIndirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("preIndirizzoDescrizione");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.PreIndirizzoDescrizione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("indirizzo");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Indirizzo = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("civico");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Civico = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("comune");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Comune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("comuneDescrizione");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.ComuneDescrizione = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Provincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("CAP");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("telefono");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("fax");
                        if (!reader.IsDBNull(tempOrdinal))
                            banca.Cap = reader.GetString(tempOrdinal);
                    }
                }
            }

            return banca;
        }

        public bool DeleteDomanda(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandeDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if ((int) databaseCemi.ExecuteScalar(comando) == 1)
                    res = true;
            }

            return res;
        }

        public TipoIscrizioneCollection GetTipiIscrizione(string idTipoImpresa)
        {
            TipoIscrizioneCollection tipiIscrizione = new TipoIscrizioneCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_TipiIscrizioneSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoImpresa", DbType.String, idTipoImpresa);

                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        TipoIscrizione tipoIscrizione = new TipoIscrizione();
                        tipiIscrizione.Add(tipoIscrizione);

                        tipoIscrizione.IdTipoImpresa = reader["idTipoImpresa"] as string;
                        tipoIscrizione.IdTipoIscrizione = reader["idTipoIscrizione"] as string;
                        tipoIscrizione.Descrizione = reader["descrizione"] as string;
                    }
                }
            }

            return tipiIscrizione;
        }

        public string GetPivaByIdDomanda(int idDomanda)
        {
            string piva;
            using (DbCommand command = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandaSelectPivaById"))
            {
                databaseCemi.AddInParameter(command, "@idDomanda", DbType.Int32, idDomanda);
                piva = databaseCemi.ExecuteScalar(command) as string;
            }
            return piva;
        }

        public void UpdateIscrizioneSalvaDocumento(int idDomanda, byte[] moduli)
        {
            using (DbCommand command = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandeUpdatePdf"))
            {
                databaseCemi.AddInParameter(command, "@idDomanda", DbType.Int32, idDomanda);
                databaseCemi.AddInParameter(command, "@pdf", DbType.Binary, moduli);

                DatabaseCemi.ExecuteNonQuery(command);
            }
        }

        public byte[] GetModuloDomanda(int idDomanda)
        {
            byte[] moduli = null;

            using (DbCommand command = databaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneCEDomandeSelectPdf"))
            {
                databaseCemi.AddInParameter(command, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = databaseCemi.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        moduli = (byte[]) reader["pdf"];
                    }
                }
            }

            return moduli;
        }
    }
}