﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Data.SqlClient;
using TBridge.Cemi.Type;
using TBridge.Cemi.Business;
using Cemi.CigoTelematica.Type;
using Cemi.CigoTelematica.Type.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;




namespace Cemi.CigoTelematica.Data
{
    public class CigoTelematicaDataAccess
    {
        private Database databaseCemi;

        public CigoTelematicaDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

    }
}
