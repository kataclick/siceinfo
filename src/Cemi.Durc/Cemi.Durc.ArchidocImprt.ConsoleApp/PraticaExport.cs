﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    public class PraticaExport
    {
        public string Protocollo { get; set; }
        public string CodiceFiscaleImpresaEsecutrice { get; set; }
        public string RagioneSocialeImpresaEsecutrice { get; set; }
        public string IdImpresaEsecutrice { get; set; }
        public DateTime? DataEmissione { get; set; }
        public string Note { get; set; }
        public StatoPratica Stato { get; set; }
        public String PathDurcFile { get; set; }
        public String PathBniFile { get; set; }
        public String Path { get; set; }

        public String PathDurcFileFirmato { set; get; }

        public Boolean DurcFileFirmatoPresente 
        {
            get { return !String.IsNullOrEmpty(PathDurcFileFirmato); }
        }

        public String guidArchidoc { set; get; }

        public String NomeDurcFileFirmato
        {
            get
            {
                if (PathDurcFileFirmato != null)
                {
                    return System.IO.Path.GetFileName(PathDurcFileFirmato);
                }

                return null;
            }
        }

        public String NomeDurcFile
        {
            get
            {
                return CreaNomeFile(PathDurcFile);
            }
        }

        public String NomeBniFile
        {
            get
            {
                return CreaNomeFile(PathBniFile);
            }
        }

        private string CreaNomeFile(String pathFile)
        {
            if (pathFile != null)
            {
                String s = System.IO.Path.GetFileName(pathFile);
                String[] split = s.Split('.');
                if (split.Length == 1)
                {
                    return String.Format("{0}{1}", split[0], Protocollo);
                }
                else if (split.Length > 1)
                {
                    String name = String.Format("{0}{1}", split[0], Protocollo);

                    for (int i = 1; i < split.Length; i++)
                    {
                        name = String.Format("{0}.{1}", name, split[i]);
                    }

                    return name;
                }
            }

            return null;
        }

        public override bool Equals(object obj)
        {
            return this.Protocollo == ((PraticaExport)obj).Protocollo;
        }

        public override int GetHashCode()
        {
            return this.ToString().ToLower().GetHashCode();
        }

    }


    public enum StatoPratica
    {
        Valida,
       // ValidaConAllegato,
        DurcFileMancante,
        NonConsegnata,
        InserimentoArchidocFallito,
        PresenteArchidoc,
       // PresenteArchidocCheckAllegato,
    }
}
