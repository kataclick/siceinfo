﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Configuration;
using System.Threading;
using Cemi.Durc.ArchidocImprt.ConsoleApp;
using Cemi.Durc.ArchidocImprt.ConsoleApp.ArchidocData;
using Cemi.Durc.Export.ConsoleApp;

namespace Cemi.Durc.Export.ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            List<PraticaExport> praticheExport;
            List<PraticaExport> _praticheGiaImportate = new List<PraticaExport>();
            List<PraticaExport> _praticheValide = new List<PraticaExport>();
            List<PraticaExport> _praticheNonValide = new List<PraticaExport>();
            DateTime _dataEmissioneDa;
            DateTime _dataEmissioneA;
            DateTime _oggi;
            int _numeroDocumentiInseriti = 0;

            try
            {
              

                LogManager.InsertDbLog(String.Format("START_{0}", Consts.NOME_PROCEDURA),String.Empty);

                
                _oggi = DateTime.Now;
                _dataEmissioneDa = _oggi.Date.AddDays(-GetGiorniRecupero());
                _dataEmissioneA = _oggi;

                //_dataEmissioneDa = new DateTime(2013, 1, 15);
                //_dataEmissioneA = new DateTime(2013, 1, 18);
               
                

                //using (DurcClientEntities context = new DurcClientEntities())
                //{
                //    //var query = from praticaProtocollo in context.PraticheProtocollo
                //    //            join pratica in context.Pratiche on praticaProtocollo.Protocollo equals pratica.Protocollo
                //    //            where praticaProtocollo.TipoInvio == 1 && praticaProtocollo.StatoPec == 12
                //    //                    && (pratica.DataEmissione >= _dataEmissioneDa && pratica.DataEmissione <= _dataEmissioneA)
                //    //            select
                //    //                new PraticaExport
                //    //                    {
                //    //                        Protocollo = pratica.Protocollo,
                //    //                        CodiceFiscaleImpresaEsecutrice = pratica.CodiceFiscaleImpresaEsecutrice,
                //    //                        RagioneSocialeImpresaEsecutrice = pratica.RagioneSocialeImpresaEsecutrice,
                //    //                        IdImpresaEsecutrice = pratica.IdImpresaEsecutrice,
                //    //                        DataEmissione = pratica.DataEmissione,
                //    //                        Note = pratica.Note
                //    //                    };


                //    var query = from pratica in context.Pratiche join praticaMessaggio in context.PraticheMsg on 
                //                pratica.Protocollo equals praticaMessaggio.protocollo
                //                where (pratica.DataEmissione >= _dataEmissioneDa && pratica.DataEmissione <= _dataEmissioneA)
                //                    && praticaMessaggio.messaggio.StartsWith(ConfigurationManager.AppSettings["StringaConfermaConsegna"])
                //                select
                //                        new PraticaExport
                //                        {
                //                            Protocollo = pratica.Protocollo,
                //                            CodiceFiscaleImpresaEsecutrice = pratica.CodiceFiscaleImpresaEsecutrice,
                //                            RagioneSocialeImpresaEsecutrice = pratica.RagioneSocialeImpresaEsecutrice,
                //                            IdImpresaEsecutrice = pratica.IdImpresaEsecutrice,
                //                            DataEmissione = pratica.DataEmissione,
                //                            Note = pratica.Note
                //                        };

                //    praticheExport = query.ToList().Distinct(new PraticaExportEqualityComparer()).ToList();
                //}

                praticheExport = DurcClientDataProvider.GetPratichePecConfermate(_dataEmissioneDa, _dataEmissioneA);

                if (praticheExport.Count > 0)
                {
                    _praticheGiaImportate = GetPrticheGiaImportate(_dataEmissioneDa, _dataEmissioneA);

                    using (new NetworkConnection(ConfigurationManager.AppSettings["DurcConstantPathSection"],
                                                new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"])))
                    {
                        foreach (PraticaExport praticaExport in praticheExport)
                        {

                            if (ValidaPratica(praticaExport))
                            {
                                PraticaExport p = _praticheGiaImportate.FirstOrDefault(x => x.Equals(praticaExport));
                                if (p != null)
                                {
                                    praticaExport.Stato = StatoPratica.PresenteArchidoc;
                                    praticaExport.guidArchidoc = p.guidArchidoc;
                                }

                                _praticheValide.Add(praticaExport);
                            }
                            else
                            {
                                _praticheNonValide.Add(praticaExport);
                            }
                        }
                    }

                    _praticheGiaImportate.Clear();
                }
                praticheExport.Clear();
                
                

                if (_praticheValide.Count > 0)
                {
                    using (new NetworkConnection(ConfigurationManager.AppSettings["DurcConstantPathSection"],
                                               new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"])))
                    {

                        foreach (PraticaExport pratica in _praticheValide)
                        {
                           
                            if (pratica.Stato == StatoPratica.Valida )
                            {
                                InserisciInArchidoc(pratica);
                                _numeroDocumentiInseriti++;
                            }
                            else if (pratica.Stato == StatoPratica.PresenteArchidoc)
                            {
                                CheckAllegatiPraticheGiaInseriteInArchidoc(pratica);
                            }

                        }
                    }
                }


                LogManager.LogPraticheNonValide(_praticheNonValide);

                LogManager.InsertDbLog(String.Format("END_{0}", Consts.NOME_PROCEDURA),
                                        String.Format("DURC Inseriti in Archidoc: {0}", _numeroDocumentiInseriti));

                try
                {
                    EmailManager.SendMail("Import Completato", String.Format("DURC importati in Archidoc: {0}", _numeroDocumentiInseriti), false);
                }
                catch (Exception ex)
                {

                    throw new Exception("Errore invio mail di notifica", ex);
                }

                
            }
            catch (Exception exc)
            {

                LogManager.InsertDbLog(String.Format("ERROR_{0}", Consts.NOME_PROCEDURA), exc.Message);

                try
                {
                    EmailManager.SendMail("Import Fallito", String.Format("Errore:\n{0}", exc.Message), true);
                }
                catch (Exception )
                {

                   //Non faccio niente
                }
            }
        }

        //static String PraticaWriter(PraticaExport praticaExport)
        //{
        //    String path = PathPratica(praticaExport);

        //    string note = "Durc inviato via PEC";
        //    if (!String.IsNullOrEmpty(praticaExport.Note))
        //        note += " - " + praticaExport.Note;


        //    String row =
        //        String.Format(
        //            "|||||{0}||{1}|||||{2}|||||{3}|||||{4}|||||{5}||{6}|",
        //            praticaExport.DataEmissione.HasValue ? praticaExport.DataEmissione.Value.ToString("dd/MM/yyyy") : String.Empty,
        //            praticaExport.RagioneSocialeImpresaEsecutrice,
        //            praticaExport.CodiceFiscaleImpresaEsecutrice,
        //            praticaExport.IdImpresaEsecutrice,
        //            praticaExport.Protocollo,
        //            note,
        //            String.Format(@"{0}\{1}", path, ConfigurationManager.AppSettings["DurcFileName"])
        //            );
        //    if (praticaExport.Stato == StatoPratica.ValidaConAllegato)
        //    {
        //        string attachment = String.Format(@"{0}\{1}", path, ConfigurationManager.AppSettings["BniFileName"]);
        //        row = String.Format(@"{0}|{1}", row, attachment);
        //    }

        //    return row;
        //}

        static String PathPratica(PraticaExport praticaExport)
        {
            String folder = ((int.Parse(praticaExport.Protocollo) / 100000) * 100000).ToString("00000000");
            return String.Format(
                                @"{0}\{1}\{2}",
                                ConfigurationManager.AppSettings["DurcConstantPathSection"],
                                folder,
                                praticaExport.Protocollo
                                );
        }

        static bool ValidaPratica(PraticaExport praticaExport)
        {
            bool valida = false;
            praticaExport.Path = PathPratica(praticaExport);
            
            praticaExport.PathDurcFile = String.Format(@"{0}\{1}", praticaExport.Path,
                                                     ConfigurationManager.AppSettings["DurcFileName"]);

                
                if (File.Exists(praticaExport.PathDurcFile))
                {
                    valida = true;
                    praticaExport.Stato = StatoPratica.Valida;

                    String file = Path.Combine(praticaExport.Path, 
                                String.Format(ConfigurationManager.AppSettings["DurcFirmatoFileName"], praticaExport.Protocollo));

                    if (File.Exists(file))
                    {
                        praticaExport.PathDurcFileFirmato = file;
                    }

                    file = String.Format(@"{0}\{1}", praticaExport.Path,
                                                           ConfigurationManager.AppSettings["BniFileName"]);
                    if (File.Exists(file))
                    {
                        praticaExport.PathBniFile = file;
                    }

                }
                else
                {
                    //praticaExport.PathDurcFile = null;
                    praticaExport.Stato = StatoPratica.DurcFileMancante;
                }
         

            return valida;
        }


        // Aggiungere reference Microsoft CDO for Windows 2000 Library
        //static Boolean CheckAvvenutaConsegna(PraticaExport praticaExport)
        //{
        //    Boolean result = false;
        //    String[] files = Directory.GetFiles(praticaExport.Path, "*.eml");
        //    if (files.Length == 1)
        //    {
                
        //        praticaExport.PathRicevutaConsegna = Path.Combine(praticaExport.Path, files[0]);
        //        CDO.Message msg = new CDO.Message();
        //        ADODB.Stream stream = new ADODB.Stream();
        //        try
        //        {
        //            String oggetto;
        //            stream.Open(Type.Missing, ADODB.ConnectModeEnum.adModeUnknown, ADODB.StreamOpenOptionsEnum.adOpenStreamUnspecified, String.Empty, String.Empty);
        //            stream.LoadFromFile(praticaExport.PathRicevutaConsegna);
        //            stream.Flush();
        //            msg.DataSource.OpenObject(stream, "_Stream");
        //            msg.DataSource.Save();
        //            oggetto = msg.Subject;
        //            stream.Close();

        //            result = (!String.IsNullOrEmpty(oggetto) && oggetto.Contains(ConfigurationManager.AppSettings["StringaConfermaConsegna"]));
        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //        finally
        //        {
        //            stream.Close();
        //        }
        //    }

        //    return result;
        //}

        static Boolean InserisciInArchidoc(PraticaExport pratica)
        {
            Boolean result = false;
            try
            {
                Byte[] documento =null;
                Byte[] allegato=null;

                PdfToTiff pdfToTiffConverter = PdfToTiff.GetIstance();

                documento = pdfToTiffConverter.GeneraDurcTiff(pratica);
                if (pratica.DurcFileFirmatoPresente)
                {
                    allegato = File.ReadAllBytes(pratica.PathDurcFileFirmato);
                    if (allegato == null)
                    {
                        throw new Exception(String.Format("Lettura del file {0} FALLITA.", pratica.PathDurcFileFirmato));
                    }
                }
                  

                ArchidocDataAccess archidocDataAccess = ArchidocDataAccess.GetIstance();

                archidocDataAccess.InsertDurcWithDocument(pratica, documento, pratica.NomeDurcFile);

                if (pratica.DurcFileFirmatoPresente)
                {
                    //List<PraticaExport> p = archidocDataAccess.GetDurcFromArchidoc(pratica.Protocollo);

                    //if (p.Count == 1 && p[0].guidArchidoc != null)
                    //{
                    //    pratica.guidArchidoc = p[0].guidArchidoc;
                        //archidocDataAccess.InsertAllegatoEsterno(pratica.guidArchidoc, String.Empty, String.Format("BNI_{0}.pdf", pratica.Protocollo), bniFile);

                    archidocDataAccess.InsertAllegatoEsterno(pratica.guidArchidoc, ConfigurationManager.AppSettings["NotaAllegatoFileFirmato"], pratica.NomeDurcFileFirmato, allegato);
                    //}

                }

                result = true;
            }
            catch (Exception )
            {
               // TracciaMessaggioErrore(exc.Message, null);
                throw;
                
            }

            return result;
 
        }


        static void CheckAllegatiPraticheGiaInseriteInArchidoc(PraticaExport pratica)
        {
            Allegato allegato = null;
            Byte[] file = null;

            if (pratica.DurcFileFirmatoPresente)
            {

                ArchidocDataAccess archidocDataAccess = ArchidocDataAccess.GetIstance();
                allegato = archidocDataAccess.GetAllegatoEsternoByNomeFile(pratica.guidArchidoc, pratica.NomeDurcFileFirmato, false);

                if (allegato == null)
                {
                    file = File.ReadAllBytes(pratica.PathDurcFileFirmato);

                    if (file == null)
                    {
                        throw new Exception(String.Format("Lettura del file {0} FALLITA.", pratica.PathBniFile));
                    }

                    archidocDataAccess.InsertAllegatoEsterno(pratica.guidArchidoc, ConfigurationManager.AppSettings["NotaAllegatoFileFirmato"], String.Format(pratica.NomeBniFile, pratica.Protocollo), file);
                }
            }
        }


        static List<PraticaExport> GetPrticheGiaImportate(DateTime dataEmissioneDa, DateTime dataEmissioneA)
        {
            ArchidocDataAccess archidocDataAccess = ArchidocDataAccess.GetIstance();
            return archidocDataAccess.GetDurcFromArchidoc(dataEmissioneDa, dataEmissioneA);
        }

        static int GetGiorniRecupero()
        {
            int giorni = 0;
            if (!int.TryParse(ConfigurationManager.AppSettings["GiorniRecupero"], out giorni))
            {
                throw new ConfigurationErrorsException("Parametro di configurazione \"GiorniRecupero\" errato.");
            }

            return giorni;
        }
       
      

        //static void CreaCartellaOutput(String path)
        //{
        //    List<String> directories = new List<String>();
        //    DirectoryInfo directoryInfo = new DirectoryInfo(path);
        //    while (directoryInfo != null)
        //    {
        //        directories.Add(directoryInfo.FullName);
        //        directoryInfo = Directory.GetParent(directoryInfo.FullName);
        //    }

        //    directories.Reverse();
        //    foreach (string directory in directories)
        //    {
        //        if (!Directory.Exists(directory))
        //        {
        //            Directory.CreateDirectory(directory);
        //        }
        //    }
        //}

    }
}
