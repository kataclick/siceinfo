﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
//using Microsoft.Office.Interop.Word;
//using PdfSharp.Pdf;
//using PdfSharp.Pdf.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace PdfManager
{
    public class PdfManagerTextSharp
    {
        private const int PDF_HEADER_LENGHT = 9;
        //public Stream ConvertWord2Pdf(string wordFilePath)
        //{
        //    // Create a new Microsoft Word application object
        //    Application word = new Application();

        //    // C# doesn't have optional arguments so we'll need a dummy value
        //    object oMissing = Missing.Value;

        //    word.Visible = false;
        //    word.ScreenUpdating = false;

        //    // Cast as Object for word Open method
        //    Object filename = wordFilePath;

        //    // Use the dummy value as a placeholder for optional arguments
        //    Document doc = word.Documents.Open(ref filename, ref oMissing,
        //                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing);
        //    doc.Activate();
            
        //    object outputFileName = wordFilePath.Replace(".docx", ".pdf");
        //    object fileFormat = WdSaveFormat.wdFormatPDF;

        //    // Save document into PDF Format
        //    doc.SaveAs(ref outputFileName,
        //               ref fileFormat, ref oMissing, ref oMissing,
        //               ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //               ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //               ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        //    // Close the Word document, but leave the Word application open.
        //    // doc has to be cast to type _Document so that it will find the
        //    // correct Close method.
        //    object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
        //    (doc).Close(ref saveChanges, ref oMissing, ref oMissing);
        //    doc = null;


        //    // word has to be cast to type _Application so that it will find
        //    // the correct Quit method.
        //    word.NormalTemplate.Saved = true; 
        //    (word).Quit(ref oMissing, ref oMissing, ref oMissing);
        //    word = null;

        //    Stream ms = GetFileStream((string) outputFileName);
        //    if (File.Exists((string) outputFileName))
        //        File.Delete((string) outputFileName);

        //    //Do il tempo a word di chiudersi...
        //    Thread.Sleep(100);

        //    return ms;


        //    //var wordApplication = new Microsoft.Office.Interop.Word.Application();
        //    //try
        //    //{
        //    //    Microsoft.Office.Interop.Word.Document wordDocument = wordApplication.Documents.Open(wordFilePath);

        //    //    if (wordDocument != null)
        //    //        wordDocument.SaveAs(string.Format(@"{0}\temp.pdf", Path.GetTempPath()), Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF);
        //    //    else
        //    //        throw new Exception("CUSTOM ERROR: Cannot Open Word Application");
        //    //    wordDocument.Close();
        //    //    wordApplication.Quit();

        //    //    Stream ms = GetFileStream(string.Format(@"{0}\temp.pdf", Path.GetTempPath()));
        //    //    if (File.Exists(string.Format(@"{0}\temp.pdf", Path.GetTempPath())))
        //    //        File.Delete(string.Format(@"{0}\temp.pdf", Path.GetTempPath()));
        //    //    return ms;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    wordApplication.Quit();
        //    //    throw;
        //    //}
        //}

        public Boolean IsPdf(byte[] fileByteArray)
        {

            return CheckPdfHeader(fileByteArray);
        }

        public Boolean IsPdf(Stream fileStream)
        {
            using (MemoryStream ms = new MemoryStream(PDF_HEADER_LENGHT))
            {
                if (fileStream.Length > PDF_HEADER_LENGHT)
                {
                    fileStream.CopyTo(ms, PDF_HEADER_LENGHT);
                    return CheckPdfHeader(ms.ToArray());
                }
            }

            return false;
        }

        public Stream MergePdf(Stream mainPdf, Stream childPdf)
        {
            Document outputDocument = new Document();
            MemoryStream outputStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(outputDocument, outputStream);
            // Open document to write
            outputDocument.Open();
            PdfContentByte content = writer.DirectContent;

            mainPdf.Seek(0, SeekOrigin.Begin);
            PdfReader readerMainPdf = new PdfReader(mainPdf);
            for (int i = 0; i < readerMainPdf.NumberOfPages; i++)
            {
                AddPage(outputDocument, content, writer, readerMainPdf, i);
            }

            mainPdf.Seek(0, SeekOrigin.Begin);
            PdfReader readerChildPdf = new PdfReader(childPdf);
            for (int i = 0; i < readerChildPdf.NumberOfPages; i++)
            {
                AddPage(outputDocument, content, writer, readerChildPdf, i);
            }

            writer.CloseStream = false;
            outputDocument.Close();
            outputStream.Seek(0, SeekOrigin.Begin);

            return outputStream;
        }

        public void MergePdf(Stream mainPdf, Stream childPdf, string pathFileDest)
        {
            using (MemoryStream ms = (MemoryStream)MergePdf(mainPdf, childPdf))
            {
                File.WriteAllBytes(pathFileDest, ms.ToArray());
            }      
        }

        public Stream MergePdf(string mainPdf, string childPdf)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            return MergePdf(main, child);
        }

        public void MergePdf(string mainPdf, string childPdf, string pathFileDest)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            MergePdf(main, child, pathFileDest);
        }

        public Stream GetPdfPage(Stream pdf, int pageIndex)
        {
            PdfReader inputReader = new PdfReader(pdf);
            if (inputReader.NumberOfPages < pageIndex || pageIndex < 0)
                return null;

            Document outputDocument = new Document();
            MemoryStream outputStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(outputDocument, outputStream);
            // Open document to write
            outputDocument.Open();
            PdfContentByte content = writer.DirectContent;

            AddPage(outputDocument, content, writer, inputReader, pageIndex);

            writer.CloseStream = false;
            outputDocument.Close();
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }
        
        public Stream GetPdfPages(Byte[] pdfByteArray, int pageFrom, int pageTo)
        {
            Stream outputStream = null;
            using (MemoryStream memStream = new MemoryStream(pdfByteArray))
            {
                outputStream = GetPdfPages(memStream, pageFrom, pageTo);
            }

            return outputStream;
        }


        public Stream GetPdfPages(Stream pdf, int pageFrom, int pageTo)
        {
            PdfReader inputReader = new PdfReader(pdf);
            int pageCount = inputReader.NumberOfPages;

            if (pageFrom < 0 || pageFrom > pageCount)
            {
                throw new ArgumentException("pageFrom must be= >= 0 and <= page count", "pageFrom");
            }

            Document outputDocument = new Document();
            MemoryStream outputStream = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(outputDocument, outputStream);
            // Open document to write
            outputDocument.Open();
            PdfContentByte content = writer.DirectContent;

            for (int i = Math.Max(0, pageFrom); i < Math.Min(pageCount, pageTo); i++)
            {
                AddPage(outputDocument, content, writer, inputReader, i);
            }

            writer.CloseStream = false;
            outputDocument.Close();
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        public Stream GetPdfPage(string pdf, int pageIndex)
        {
            Stream pdfStream = GetFileStream(pdf);
            return GetPdfPage(pdfStream, pageIndex);
        }

        public Stream GetFileStream(string filename)
        {
            MemoryStream ms = new MemoryStream();
            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] bytes = new byte[file.Length];
            file.Read(bytes, 0, (int) file.Length);
            ms.Write(bytes, 0, (int) file.Length);
            file.Close();
            return ms;
        }

        public Stream GetByteStream(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            return ms;
        }

        #region private
        private void AddPage(Document document, PdfContentByte content, PdfWriter writer, PdfReader reader, int pageIndex)
        {
            int currentPageIndex = pageIndex + 1;
            document.SetPageSize(
                        reader.GetPageSizeWithRotation(currentPageIndex));

            // Create page
            document.NewPage();
            PdfImportedPage importedPage =
              writer.GetImportedPage(reader, currentPageIndex);


            // Determine page orientation
            int pageOrientation = reader.GetPageRotation(currentPageIndex);
            if ((pageOrientation == 90) || (pageOrientation == 270))
            {
                content.AddTemplate(importedPage, 0, -1f, 1f, 0, 0,
                   reader.GetPageSizeWithRotation(currentPageIndex).Height);
            }
            else
            {
                content.AddTemplate(importedPage, 1f, 0, 0, 1f, 0, 0);
            }

        }

        private Boolean CheckPdfHeader(byte[] buffer)
        {
            //%PDF-1.x
            return (buffer != null && buffer.Length > PDF_HEADER_LENGHT && buffer[0] == 0x25 && buffer[1] == 0x50
            && buffer[2] == 0x44 && buffer[3] == 0x46 && buffer[4] == 0x2d && buffer[4] == 0x31 && buffer[4] == 0x2e);

        }
        #endregion
    }
}