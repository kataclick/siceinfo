﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    public static class EmailManager
    {


        public static void SendMail(String subject, String bodyTxt, Boolean highPriority)
        {

            MailMessage mailMessage = new MailMessage();

            mailMessage.Subject = subject;

            mailMessage.Body = bodyTxt;

            mailMessage.Priority = highPriority ? MailPriority.High : MailPriority.Normal;
           // MailMesaji.BodyEncoding = Encoding.GetEncoding("Windows-1254"); // Turkish Character Encoding

            mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["mailFromAddress"]);

            AddRecipients(mailMessage);

            System.Net.Mail.SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"]);

            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            SetCredentials(smtp);


            //smtp.EnableSsl = true;


            smtp.Send(mailMessage);

        }

        private static void SetCredentials(SmtpClient smtp)
        {
            String usr = ConfigurationManager.AppSettings["accountUsername"];
            String psw = ConfigurationManager.AppSettings["accountPassword"];

            if (!String.IsNullOrEmpty(usr) && !String.IsNullOrEmpty(psw))
            {
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(usr, psw);
            }
        }


        private static void AddRecipients(MailMessage mailMessage)
        {
            String[] recipients = ConfigurationManager.AppSettings["mailToAddress"].Split(';');
            for (int i = 0; i < recipients.Length; i++)
            {
                if (!String.IsNullOrEmpty(recipients[i]))
                {
                    mailMessage.To.Add(new MailAddress(recipients[i]));
                }
            }
        }
    }
}
