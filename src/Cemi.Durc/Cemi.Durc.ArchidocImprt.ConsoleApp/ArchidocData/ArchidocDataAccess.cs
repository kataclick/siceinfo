﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Cemi.Durc.ArchidocImprt.ConsoleApp.ArchidocWsSsd;
using Cemi.Durc.Export.ConsoleApp;
using System.Data;
using System.Globalization;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp.ArchidocData
{
    class ArchidocDataAccess
    {


        private String ArchidocServer { set; get; }
        private String ArchidocMainEm { set; get; }
        private String ArchidocGpEm { set; get; }
        private String ArchidocDatabase { set; get; }
        private String ArchidocUsername { set; get; }
        private String ArchidocPassword { set; get; }
        private String Archivio { set; get; }
        private String TipoDocumento { set; get; }
        private String _noteDefalut;

        private static ArchidocDataAccess _archidocDataAccess;


        public static ArchidocDataAccess GetIstance()
        {
            if (_archidocDataAccess == null)
            {
                _archidocDataAccess = new ArchidocDataAccess();
            }

            return _archidocDataAccess;
        }

        private ArchidocDataAccess()
        {
            ArchidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
            ArchidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
            ArchidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
            ArchidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
            ArchidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
            ArchidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);
            TipoDocumento = ConfigurationManager.AppSettings["NomeTipoDocumentoArchidoc"];
            _noteDefalut = ConfigurationManager.AppSettings["NoteArchidoc"];
            Archivio = Consts.NOME_ARCHIVIO_ARCHIDOC;
        }

        public void InsertDurcWithDocument(PraticaExport pratica, byte[] documento, string nomeFile)
        {
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                String guid = archidocServiceClient.Insert(
                                                            ArchidocServer, ArchidocDatabase, ArchidocGpEm, ArchidocMainEm, ArchidocUsername, ArchidocPassword, Archivio, TipoDocumento,
                                                            null,
                                                            pratica.DataEmissione.Value.ToString("dd/MM/yyyy"),
                                                            pratica.RagioneSocialeImpresaEsecutrice,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            pratica.CodiceFiscaleImpresaEsecutrice,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            pratica.IdImpresaEsecutrice,
                                                            null,
                                                            null,
                                                            null,
                                                            null,
                                                            pratica.Protocollo,
                                                            Consts.IDENTIFICATIVO_DOCUMENTI_INSERITI_IN_ARCHIDOC,
                                                            null,
                                                            null,
                                                            null,
                                                            _noteDefalut,
                                                            documento, 
                                                            (documento == null) ? null : nomeFile,
                                                            (documento == null)

                                                          );
                                            
                                                            //null,
                                                            //documento.FileByteArray,
                                                            //documento.FileNome,
                                                            //(documento.FileByteArray == null)
                                                      //   );

                pratica.guidArchidoc = guid;


            }
        }

        public List<PraticaExport> GetDurcFromArchidoc(String protocolloPratica)
        {
            List<PraticaExport> dichiarazioni = new List<PraticaExport>();



            DataSet cardsDataSet = GetCardsByProtocolloPratica(protocolloPratica);

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        PraticaExport pratica = new PraticaExport();
                        //dichiarazione.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        String valore;
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            valore = card[column].ToString();
                            ChiaviArchidocWsSsd chiave;

                            try
                            {
                                chiave = (ChiaviArchidocWsSsd)Enum.Parse(typeof(ChiaviArchidocWsSsd), column.ColumnName);
                            }
                            catch (Exception)
                            {

                                chiave = ChiaviArchidocWsSsd.Unknown;
                            }

                            switch (chiave)
                            {
                                case ChiaviArchidocWsSsd.svIfReference:
                                    break;
                                case ChiaviArchidocWsSsd.svIfDateReg:
                                    break;
                                case ChiaviArchidocWsSsd.svIfProtocol:
                                    break;
                                case ChiaviArchidocWsSsd.svIfDateDoc:
                                    DateTime data;
                                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                                        pratica.DataEmissione = data;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey11:
                                    pratica.RagioneSocialeImpresaEsecutrice = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey12:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey13:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey14:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey15:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey21:
                                    pratica.CodiceFiscaleImpresaEsecutrice = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey22:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey23:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey24:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey25:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey31:
                                    pratica.IdImpresaEsecutrice = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey32:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey33:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey34:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey35:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey41:
                                    pratica.Protocollo = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey42:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey43:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey44:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey45:
                                    break;
                                case ChiaviArchidocWsSsd.svIfObj:
                                    break;
                                case ChiaviArchidocWsSsd.GUID:
                                    pratica.guidArchidoc = valore;
                                    break;
                                case ChiaviArchidocWsSsd.NrPag:
                                    break;
                                case ChiaviArchidocWsSsd.DocName:
                                    break;
                                case ChiaviArchidocWsSsd.DocExtension:
                                    break;
                                case ChiaviArchidocWsSsd.NrAttachments:
                                    break;
                                case ChiaviArchidocWsSsd.FullPath:
                                    break;
                                default:
                                    break;
                            }
                        }

                        dichiarazioni.Add(pratica);
                    }
                }

            

            return dichiarazioni;
        }


        public List<PraticaExport> GetDurcFromArchidoc(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            List<PraticaExport> dichiarazioni = new List<PraticaExport>();


            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByDataDocumento(
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                                             dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy")
                                             );

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        PraticaExport pratica = new PraticaExport();
                        //dichiarazione.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        String valore;
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            valore = card[column].ToString();
                            ChiaviArchidocWsSsd chiave ;

                            try
                            {
                                chiave = (ChiaviArchidocWsSsd)Enum.Parse(typeof(ChiaviArchidocWsSsd), column.ColumnName);
                            }
                            catch (Exception)
                            {

                                chiave = ChiaviArchidocWsSsd.Unknown;
                            }

                            switch (chiave)
                            {
                                case ChiaviArchidocWsSsd.svIfReference:
                                    break;
                                case ChiaviArchidocWsSsd.svIfDateReg:
                                    break;
                                case ChiaviArchidocWsSsd.svIfProtocol:
                                    break;
                                case ChiaviArchidocWsSsd.svIfDateDoc:
                                         DateTime data;
                                        if (DateTime.TryParseExact(valore,"dd/MM/yyyy", null, DateTimeStyles.None, out data))
                                            pratica.DataEmissione = data;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey11:
                                    pratica.RagioneSocialeImpresaEsecutrice = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey12:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey13:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey14:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey15:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey21:
                                    pratica.CodiceFiscaleImpresaEsecutrice = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey22:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey23:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey24:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey25:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey31:
                                    pratica.IdImpresaEsecutrice = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey32:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey33:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey34:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey35:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey41:
                                    pratica.Protocollo = valore;
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey42:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey43:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey44:
                                    break;
                                case ChiaviArchidocWsSsd.svIfKey45:
                                    break;
                                case ChiaviArchidocWsSsd.svIfObj:
                                    break;
                                case ChiaviArchidocWsSsd.GUID:
                                    pratica.guidArchidoc = valore;
                                    break;
                                case ChiaviArchidocWsSsd.NrPag:
                                    break;
                                case ChiaviArchidocWsSsd.DocName:
                                    break;
                                case ChiaviArchidocWsSsd.DocExtension:
                                    break;
                                case ChiaviArchidocWsSsd.NrAttachments:
                                    break;
                                case ChiaviArchidocWsSsd.FullPath:
                                    break;
                                default:
                                    break;
                            }
                        }

                        dichiarazioni.Add(pratica);
                    }
                }

            }

            return dichiarazioni;
        }

        public void InsertAllegatoEsterno(String cardGuid, String note, String nomeFile, Byte[] fileByteArray)
        {
            String retVal;
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                retVal = archidocServiceClient.InsertAttachment(ArchidocServer, ArchidocDatabase, ArchidocGpEm, ArchidocMainEm, ArchidocUsername, ArchidocPassword, cardGuid, note, fileByteArray, nomeFile);
            }
        }


        public Allegato GetAllegatoEsternoByNomeFile(String cardGuid, String nomeFile, Boolean getDocumento)
        {
            Allegato allegato = null;
            int index = -1;

            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                DataSet dataSet = archidocServiceClient.GetAttachments(ArchidocServer, ArchidocDatabase, ArchidocUsername, ArchidocPassword, cardGuid);
                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count == 1)
                {
                    DataTable attachmentsTable = dataSet.Tables[0];
                    foreach (DataRow attachment in attachmentsTable.Rows)
                    {
                        index++;
                        String file = GetNomeFileAllegatoFromDataRow(attachment);
                        if (file != null && file.Equals(nomeFile, StringComparison.CurrentCultureIgnoreCase))
                        {
                            allegato = DataRowToAllegato(attachment);
                            break;
                        }
                    }
                }

                if (allegato != null && getDocumento)
                {
                    Byte[] buffer;
                    archidocServiceClient.SaveAttachment(ArchidocServer, ArchidocDatabase, ArchidocUsername, ArchidocPassword, cardGuid, index, out buffer);
                    allegato.FileByteArray = buffer;
                }

            }

            return allegato;
        }


        private DataSet GetCardsByDataDocumento(string da, string a)
        {
            DataSet cardsDataSet = null;
            //String sReferenceFrom = null;
            //String sReferenceTo = null;
            //String sDateRegFrom = null;
            //String sDateRegTo = null;
            //String sKey22 = null;

            //switch (tipoRicerca)
            //{
            //    case TipiRicercaArchidoc.DataScansione:
            //        sDateRegFrom = da;
            //        sDateRegTo = a;
            //        break;
            //    case TipiRicercaArchidoc.ProtocolloArchidoc:
            //        sReferenceFrom = da;
            //        sReferenceTo = a;
            //        break;
            //    case TipiRicercaArchidoc.IdDelega:
            //        sKey22 = da;
            //        break;
            //    default:
            //        break;
            //}

            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                cardsDataSet = archidocServiceClient.GetList(ArchidocServer, ArchidocDatabase, ArchidocUsername, ArchidocPassword, Archivio, TipoDocumento, 0, null, null, null, null, null,
                                                                da, a, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, Consts.IDENTIFICATIVO_DOCUMENTI_INSERITI_IN_ARCHIDOC, null, null, null, null);

            }

            return cardsDataSet;
        }


        private DataSet GetCardsByProtocolloPratica(string protocolloPratica)
        {
            DataSet cardsDataSet = null;
            //String sReferenceFrom = null;
            //String sReferenceTo = null;
            //String sDateRegFrom = null;
            //String sDateRegTo = null;
            //String sKey22 = null;

            //switch (tipoRicerca)
            //{
            //    case TipiRicercaArchidoc.DataScansione:
            //        sDateRegFrom = da;
            //        sDateRegTo = a;
            //        break;
            //    case TipiRicercaArchidoc.ProtocolloArchidoc:
            //        sReferenceFrom = da;
            //        sReferenceTo = a;
            //        break;
            //    case TipiRicercaArchidoc.IdDelega:
            //        sKey22 = da;
            //        break;
            //    default:
            //        break;
            //}

            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                cardsDataSet = archidocServiceClient.GetList(ArchidocServer, ArchidocDatabase, ArchidocUsername, ArchidocPassword, Archivio, TipoDocumento, 0, null, null, null, null, null,
                                                                null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, protocolloPratica, Consts.IDENTIFICATIVO_DOCUMENTI_INSERITI_IN_ARCHIDOC, null, null, null, null);

            }

            return cardsDataSet;
        }


        private static Allegato DataRowToAllegato(DataRow attachment)
        {
            Allegato allegato = new Allegato();
            for (int i = 0; i < attachment.ItemArray.Length; i++)
            {
                String value = attachment[i] != null ? attachment[i].ToString() : null;
                switch (i)
                {
                    case 0:
                        allegato.NomeFile = value;
                        break;
                    case 1:
                        allegato.Note = value;
                        break;
                    case 2:
                        allegato.AllegatoGuId = value;
                        break;
                    case 3:
                        allegato.CardGuid = value;
                        break;
                    case 4:
                        allegato.Interno = value == "1" ? true : false;
                        break;
                    default:
                        break;
                }
            }
            return allegato;
        }


        private static String GetNomeFileAllegatoFromDataRow(DataRow attachment)
        {
            return attachment[0] as String;
        }
    }
}
