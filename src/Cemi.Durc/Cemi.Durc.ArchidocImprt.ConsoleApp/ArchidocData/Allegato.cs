﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    public class Allegato
    {
        public String AllegatoGuId { set; get; }
        public String CardGuid { set; get; }
        public Boolean Interno { set; get; }
        public int Codice { set; get; }
        public String NomeFile { set; get; }
        public String Note { set; get; }
        public Byte[] FileByteArray { set; get; }
    }
}
