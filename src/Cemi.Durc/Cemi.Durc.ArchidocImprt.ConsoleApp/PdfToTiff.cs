﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PdfToImage;
using System.IO;
using System.Configuration;
using PdfManager;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    class PdfToTiff
    {
        //const String _nomeDurcPdfTmp = "durcTemp";

        private static  PdfToTiff _pdfToTiff;
        private PDFConvert _converter;
        private String _tempDirectory;
        //private String _durcPdfTmpFullPath;

        public static PdfToTiff GetIstance()
        {
            if (_pdfToTiff == null)
            {
                _pdfToTiff = new PdfToTiff();
            }

            return _pdfToTiff;
        }

        private PdfToTiff()
        {
             _converter = new PDFConvert();
             _tempDirectory = Path.Combine(Directory.GetCurrentDirectory(), ConfigurationManager.AppSettings["CartellaTempTifFile"]);
             //_durcPdfTmpFullPath = Path.Combine(_tempDirectory, _nomeDurcPdfTmp);
        }


        public Byte[] GeneraDurcTiff(PraticaExport pratica)
        {
            Byte[] tiffFile = null;
            String localTempPdfFile = Path.Combine(_tempDirectory, Path.GetFileName(pratica.PathDurcFile));
            String localTempTiffFile = Path.Combine(_tempDirectory, String.Concat(Path.GetFileNameWithoutExtension(pratica.PathDurcFile), Consts.TIFF_ESTENSIONE));
            try
            {
                if (!Directory.Exists(_tempDirectory))
                {
                    Directory.CreateDirectory(_tempDirectory);
                }
                else
                {
                    foreach (FileInfo file in new DirectoryInfo(_tempDirectory).GetFiles())
                    {
                        file.Delete();
                    }

                }

                if (String.IsNullOrEmpty(pratica.PathBniFile))
                {

                    File.Copy(pratica.PathDurcFile, localTempPdfFile, true);
                }
                else
                {
                    ConcatenaSalvaDurcBni(pratica, localTempPdfFile);
                }
                tiffFile = ConvertiPdfToTiff( localTempPdfFile, localTempTiffFile);
                if (tiffFile == null)
                {
                    throw new Exception(String.Format("Conversione da Pdf a Tiff fallita per la pratica N. {0}", pratica.Protocollo));
                }

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (File.Exists(localTempPdfFile))
                {
                    File.Delete(localTempPdfFile);
                }

                if (File.Exists(localTempTiffFile))
                {
                    File.Delete(localTempTiffFile);
                }
            }
          
            return tiffFile;
           // File.Copy(
        }

        private Byte[] ConvertiPdfToTiff( String localTempPdfFile, String localTempTiffFile)
        {
            Byte[] tiffFile = null;
            _converter.OutputFormat = "tiffg4";
            if (_converter.Convert(localTempPdfFile, localTempTiffFile))
            {
                tiffFile = File.ReadAllBytes(localTempTiffFile);
            }
         
            return tiffFile;
        }

        private void ConcatenaSalvaDurcBni(PraticaExport pratica, String destFile)
        {

            PdfManagerTextSharp pdfManager = new PdfManagerTextSharp();
            using (Stream durc = new MemoryStream(File.ReadAllBytes(pratica.PathDurcFile)))
            {
                using (Stream bni = new MemoryStream(File.ReadAllBytes(pratica.PathBniFile) ))
                {
                    pdfManager.MergePdf(durc, bni, destFile);
                }
            }  
        }
    }
}
