﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    class PraticaExportEqualityComparer : IEqualityComparer<PraticaExport>
    {
        public bool Equals(PraticaExport x, PraticaExport y)
        {
            return x.CodiceFiscaleImpresaEsecutrice == y.CodiceFiscaleImpresaEsecutrice
                   && x.DataEmissione == y.DataEmissione
                   && x.IdImpresaEsecutrice == y.IdImpresaEsecutrice
                   && x.Protocollo == y.Protocollo
                   && x.RagioneSocialeImpresaEsecutrice == y.RagioneSocialeImpresaEsecutrice;
        }

        public int GetHashCode(PraticaExport obj)
        {
            return obj.ToString().ToLower().GetHashCode();
        }
    }
}
