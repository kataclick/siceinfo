﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    public static class DurcClientDataProvider
    {
        public static List<PraticaExport> GetPratichePecConfermate(DateTime dataEmisioneda, DateTime dataEmisionea)
        {
            List<PraticaExport> pratiche = new List<PraticaExport>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DurcClient"].ToString()))
            {
                conn.Open();
                StringBuilder query = new StringBuilder();
                //query.Append("SELECT distinct dbo.dmPratica4.protocollo  FROM   dbo.dmPraticaMsg INNER JOIN dbo.dmPratica4 ON dbo.dmPraticaMsg.protocollo = dbo.dmPratica4.protocollo where dbo.dmPraticaMsg.messaggio like 'Ricevuta di ritorno pervenuta per riferimento:%' ");
                //query.Append(" AND (dmPratica4.data_emissione_durc >= @da AND dmPratica4.data_emissione_durc <= @a)");


                query.Append("SELECT t.protocollo, p.codice_impresa_esecutrice, p.impresa_esecutrice, p.codice_fiscale,  p.data_emissione_durc, p.note ");
                query.Append(" from ");
                query.Append(" (SELECT distinct dbo.dmPratica4.protocollo  as protocollo ");
                query.Append(" FROM  dbo.dmPraticaMsg INNER JOIN ");
                query.Append(" dbo.dmPratica4 ON dbo.dmPraticaMsg.protocollo = dbo.dmPratica4.protocollo ");
                query.Append(" where dbo.dmPraticaMsg.messaggio like @stringaConferma ");
                query.Append(" and dbo.dmPraticaMsg.messaggio not like @stringaErrore ");
                query.Append(" and (dmPratica4.data_emissione_durc >= @da AND dmPratica4.data_emissione_durc <= @a)) t ");
                query.Append(" inner join dbo.dmPratica4 p on t.protocollo = p.protocollo ");
                using (SqlCommand command = new SqlCommand(query.ToString(), conn))
                {
                    command.CommandTimeout = 300;
                    command.Parameters.AddWithValue("@da", dataEmisioneda);
                    command.Parameters.AddWithValue("@a", dataEmisionea);
                    command.Parameters.AddWithValue("@stringaConferma", Consts.STRINGA_CONFERMA_RICEVUTA_RITORNO);
                    command.Parameters.AddWithValue("@stringaErrore", Consts.STRINGA_ERRORE_RICEZIONE);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int indexProtocollo = reader.GetOrdinal("protocollo");
                        int indexCodImp = reader.GetOrdinal("codice_impresa_esecutrice");
                        int indexRagSocImp = reader.GetOrdinal("impresa_esecutrice");
                        int indexCodFisImp = reader.GetOrdinal("codice_fiscale");
                        int indexDataEm = reader.GetOrdinal("data_emissione_durc");
                        int indexNote = reader.GetOrdinal("note");
                        while (reader.Read())
                        {
                            PraticaExport pratica = new PraticaExport()
                            {
                                CodiceFiscaleImpresaEsecutrice = reader.IsDBNull(indexCodFisImp) ? null : reader.GetString(indexCodFisImp),
                                RagioneSocialeImpresaEsecutrice = reader.IsDBNull(indexRagSocImp)  ? null : reader.GetString(indexRagSocImp),
                                Protocollo = reader.IsDBNull(indexProtocollo)  ? null : reader.GetString(indexProtocollo),
                                IdImpresaEsecutrice = reader.IsDBNull(indexCodImp) ? null : reader.GetString(indexCodImp),
                                DataEmissione = reader.IsDBNull(indexDataEm)  ? (DateTime?)null : reader.GetDateTime(indexDataEm),
                                Note = reader.IsDBNull(indexNote) ? null : reader.GetString(indexNote),
                            };


                            pratiche.Add(pratica);
                        }
                    }
                }
            }

            return pratiche;
        }

    }
}
