﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using Cemi.Durc.Export.ConsoleApp;
using System.IO;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    public static class  LogManager
    {
        public static void InsertDbLog(string tipoElaborazione, string messaggio)
        {
            String connStr = ConfigurationManager.ConnectionStrings["CEMI"].ToString();
            try
            {
                using (SqlConnection conn = new SqlConnection(connStr))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand("dbo.USP_ArchidocLogInsert", conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@tipoElaborazione", tipoElaborazione);
                        if (messaggio != null)
                        {
                            command.Parameters.AddWithValue("@messaggio", messaggio);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@messaggio", DBNull.Value);
                        }

                        if (command.ExecuteNonQuery() != 1)
                        {
                            //TODO usare l'EVENT Log
                        }
                    }
                }

            }
            catch (Exception)
            {

                //TODO usare l'EVENT Log
            }       
        }

        public static void LogPraticheNonValide(List<PraticaExport> pratiche)
        {
            if (pratiche.Count > 0)
            {
                String filePath = ConfigurationManager.AppSettings["LogFileName"];
                int durcFileMancanti = 0;
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    foreach (PraticaExport praticaExport in pratiche)
                    {
                        if (praticaExport.Stato == StatoPratica.DurcFileMancante)
                        {
                            durcFileMancanti++;
                            String message = String.Format("{0} --- DURC Protocollo {1} : File {2} non trovato", DateTime.Now,
                                                        praticaExport.Protocollo, praticaExport.PathDurcFile);
                            writer.WriteLine(message);
                        }
                    }
                }

                if (durcFileMancanti > 0)
                {
                    InsertDbLog(String.Format("WARNING_{0}", Consts.NOME_PROCEDURA),
                                            String.Format("DURC scartati per pdf mancante: {0}", durcFileMancanti));
                }
            }
        }
    }
}
