﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Durc.ArchidocImprt.ConsoleApp
{
    public static class Consts
    {
        public  const String NOME_PROCEDURA = "DURC";
        public  const String NOME_ARCHIVIO_ARCHIDOC = "Generale";

        public const String TIFF_ESTENSIONE = ".tif";

        public const String IDENTIFICATIVO_DOCUMENTI_INSERITI_IN_ARCHIDOC = "SI";

        public const String STRINGA_CONFERMA_RICEVUTA_RITORNO = "Ricevuta di ritorno pervenuta per riferimento:%";
        public const String STRINGA_ERRORE_RICEZIONE = "%Riscontrato errore nella ricezione%";
    }
}
