﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Durc.Export.ConsoleApp
{
    public class PraticaExport
    {
        public string Protocollo { get; set; }
        public string CodiceFiscaleImpresaEsecutrice { get; set; }
        public string RagioneSocialeImpresaEsecutrice { get; set; }
        public string IdImpresaEsecutrice { get; set; }
        public DateTime? DataEmissione { get; set; }
        public string Note { get; set; }
        public StatoPratica Stato { get; set; }
        public string PathDurcFile { get; set; }
        public String PathAllegato { get; set; }
    }


    public enum StatoPratica
    {
        Valida,
        ValidaConAllegato,
        DurcFileMancante
    }
}
