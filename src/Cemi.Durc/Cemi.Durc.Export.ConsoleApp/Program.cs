﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Configuration;
using System.Threading;

namespace Cemi.Durc.Export.ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            List<PraticaExport> praticheExport;
            bool praticheNonValide = false;
            String outputPath = String.Format(@"{0}\{1}", ConfigurationManager.AppSettings["OutputFolder"],
                                              DateTime.Now.ToString("yyyyMMdd"));

            try
            {
                CreaCartellaOutput(outputPath);

                using (DurcClientEntities context = new DurcClientEntities())
                {
                    var query = from praticaProtocollo in context.PraticheProtocollo
                                join pratica in context.Pratiche on praticaProtocollo.Protocollo equals pratica.Protocollo
                                where praticaProtocollo.TipoInvio == 1 && praticaProtocollo.StatoPec == 12
                                select
                                    new PraticaExport
                                        {
                                            Protocollo = pratica.Protocollo,
                                            CodiceFiscaleImpresaEsecutrice = pratica.CodiceFiscaleImpresaEsecutrice,
                                            RagioneSocialeImpresaEsecutrice = pratica.RagioneSocialeImpresaEsecutrice,
                                            IdImpresaEsecutrice = pratica.IdImpresaEsecutrice,
                                            DataEmissione = pratica.DataEmissione,
                                            Note = pratica.Note
                                        };

                    praticheExport = query.ToList().Distinct(new PraticaExportEqualityComparer()).ToList();
                }


                if (praticheExport.Count > 0)
                {
                    using (new NetworkConnection(ConfigurationManager.AppSettings["DurcConstantPathSection"],
                                                new NetworkCredential(ConfigurationManager.AppSettings["Username"], ConfigurationManager.AppSettings["Password"])))
                    {
                        foreach (PraticaExport praticaExport in praticheExport)
                        {
                            praticheNonValide |= !ValidaPratica(praticaExport);
                        }
                    }
                }
                
                if (praticheExport.Count > 0)
                {
                    String fullPath = String.Format(@"{0}\{1}", outputPath,
                                                    ConfigurationManager.AppSettings["OutputFileName"]);
                    using (StreamWriter writer = new StreamWriter(fullPath))
                    {
                        foreach (PraticaExport praticaExport in praticheExport)
                        {
                            if (praticaExport.Stato != StatoPratica.DurcFileMancante)
                            {
                                writer.WriteLine(PraticaWriter(praticaExport));
                            }
                        }
                    }
                }

                if (praticheNonValide)
                {
                    LogPraticheNonValide(praticheExport, outputPath);
                    throw new Exception(
                        String.Format("Alcune pratiche non sono state processsate vedere il file di log: {0}",
                                      String.Format(@"{0}\{1}", outputPath,
                                                    ConfigurationManager.AppSettings["InvalidPracticeLogName"])));
                }
            }
            catch (Exception exc)
            {
                TracciaMessaggioErrore(exc.Message, outputPath);
            }
        }

        static String PraticaWriter(PraticaExport praticaExport)
        {
            String path = PathPratica(praticaExport);

            string note = "Durc inviato via PEC";
            if (!String.IsNullOrEmpty(praticaExport.Note))
                note += " - " + praticaExport.Note;


            String row =
                String.Format(
                    "|||||{0}||{1}|||||{2}|||||{3}|||||{4}|||||{5}||{6}|",
                    praticaExport.DataEmissione.HasValue ? praticaExport.DataEmissione.Value.ToString("dd/MM/yyyy") : String.Empty,
                    praticaExport.RagioneSocialeImpresaEsecutrice,
                    praticaExport.CodiceFiscaleImpresaEsecutrice,
                    praticaExport.IdImpresaEsecutrice,
                    praticaExport.Protocollo,
                    note,
                    String.Format(@"{0}\{1}", path, ConfigurationManager.AppSettings["DurcFileName"])
                    );
            if (praticaExport.Stato == StatoPratica.ValidaConAllegato)
            {
                string attachment = String.Format(@"{0}\{1}", path, ConfigurationManager.AppSettings["AttachmentName"]);
                row = String.Format(@"{0}|{1}", row, attachment);
            }

            return row;
        }

        static String PathPratica(PraticaExport praticaExport)
        {
            String folder = ((int.Parse(praticaExport.Protocollo) / 100000) * 100000).ToString("00000000");
            return String.Format(
                                @"{0}\{1}\{2}",
                                ConfigurationManager.AppSettings["DurcConstantPathSection"],
                                folder,
                                praticaExport.Protocollo
                                );
        }

        static bool ValidaPratica(PraticaExport praticaExport)
        {
            bool valida;
            String path = PathPratica(praticaExport);
            praticaExport.PathDurcFile = String.Format(@"{0}\{1}", path,
                                                       ConfigurationManager.AppSettings["DurcFileName"]);
            
            if (File.Exists(praticaExport.PathDurcFile))
            {
                valida = true;
                praticaExport.Stato = StatoPratica.Valida;
                praticaExport.PathAllegato = String.Format(@"{0}\{1}", path,
                                                       ConfigurationManager.AppSettings["AttachmentName"]);
                if (File.Exists(praticaExport.PathAllegato))
                {
                    praticaExport.Stato = StatoPratica.ValidaConAllegato;
                }
            }
            else
            {
                praticaExport.Stato = StatoPratica.DurcFileMancante;
                valida = false;
            }

            return valida;
        }


        static void LogPraticheNonValide(List<PraticaExport> pratiche, String outputPath)
        {
            String filePath = String.Format(@"{0}\{1}", outputPath, ConfigurationManager.AppSettings["InvalidPracticeLogName"]);
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                foreach (PraticaExport praticaExport in pratiche)
                {
                    if (praticaExport.Stato == StatoPratica.DurcFileMancante)
                    {
                        String message = String.Format("{0}  Protocollo {1} : File {2} non trovato", DateTime.Now,
                                                    praticaExport.Protocollo, praticaExport.PathDurcFile);
                        writer.WriteLine(message);
                    }
                }
            }
        }

        static void CreaCartellaOutput(String path)
        {
            List<String> directories = new List<String>();
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            while (directoryInfo != null)
            {
                directories.Add(directoryInfo.FullName);
                directoryInfo = Directory.GetParent(directoryInfo.FullName);
            }

            directories.Reverse();
            foreach (string directory in directories)
            {
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
        }

        static void TracciaMessaggioErrore(string messaggio, String outputPath)
        {
            string log = ConfigurationManager.AppSettings["LogDurcExport"];

            Console.WriteLine(messaggio);

            if (log == "true")
            {
                String filePath = String.Format(@"{0}\{1}", outputPath, ConfigurationManager.AppSettings["ErrorLogName"]);
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    writer.WriteLine(String.Format("{0}  {1}", DateTime.Now, messaggio));
                }
            }
        }
    }
}
