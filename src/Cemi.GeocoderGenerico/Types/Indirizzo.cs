﻿using System;

namespace Cemi.GeocoderGenerico.Types
{
    [Serializable]
    public class Indirizzo : IndirizzoSemplice
    {
        public Decimal? Latitudine { get; set; }
        public Decimal? Longitudine { get; set; }

        public String IndirizzoCompletoGeocode
        {
            get
            {
                return String.Format("{0} {1} {2}", IndirizzoBase, Localita, Stato);
            }
        }

        public Boolean Georeferenziato
        {
            get { return Latitudine.HasValue && Longitudine.HasValue; }
        }
    }
}