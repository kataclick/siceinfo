using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using Cemi.GeocoderGenerico.Types;
using TBridge.Cemi.Geocode.Business;

namespace Cemi.GeocoderGenerico.WinApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            InitializeBackgroundWorker();
        }

        private void InitializeBackgroundWorker()
        {
            backgroundWorkerGeo.DoWork += backgroundWorkerGeo_DoWork;
            backgroundWorkerGeo.RunWorkerCompleted += backgroundWorkerGeo_RunWorkerCompleted;
            backgroundWorkerGeo.ProgressChanged += backgroundWorkerGeo_ProgressChanged;
        }

        private void backgroundWorkerGeo_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            IndirizzoCollection indirizzi = (IndirizzoCollection)e.Argument;

            foreach (Indirizzo indirizzo in indirizzi)
            {
                try
                {
                    List<Indirizzo> geocodingResult =
                        GeocodeProvider.Geocode(String.Format("{0}", indirizzo.IndirizzoCompletoGeocode));
                    if (geocodingResult.Count == 0)
                    {
                        worker.ReportProgress(1,
                                              String.Format("Indirizzo '{0}' NON georeferenziato",
                                                            indirizzo.IndirizzoCompletoGeocode));
                    }
                    else
                    {
                        if (!geocodingResult[0].Georeferenziato)
                        {
                            worker.ReportProgress(1,
                                                  String.Format("Indirizzo '{0}' NON georeferenziato",
                                                                indirizzo.IndirizzoCompletoGeocode));
                        }
                        if (geocodingResult.Count == 1 /* && geocodingResult[0].Georeferenziato*/)
                        {
                            UpdateIndirizzo(indirizzo, geocodingResult[0]);
                            worker.ReportProgress(1,
                                                  String.Format("Indirizzo '{0}' georeferenziato correttamente",
                                                                indirizzo.IndirizzoCompletoGeocode));
                        }
                        else
                        {
                            UpdateIndirizzo(indirizzo, geocodingResult[0]);
                            //foreach(Indirizzo indirizzoGeocoded in geocodingResult)
                            //{
                            //    InsertIndirizzi(indirizzo, indirizzoGeocoded);
                            //}
                            worker.ReportProgress(1,
                                                 String.Format("Indirizzo '{0}' georeferenziato correttamente",
                                                               indirizzo.IndirizzoCompletoGeocode));
                        }
                    }
                }

                //if (geocodingResult.Count == 1 && geocodingResult[0].Georeferenziato)
                //    {
                //        UpdateIndirizzo(indirizzo, geocodingResult[0]);
                //        worker.ReportProgress(1,
                //            String.Format("Indirizzo '{0}' georeferenziato correttamente",
                //                            indirizzo.IndirizzoCompletoGeocode));
                //    }
                //    else
                //    {
                //        worker.ReportProgress(1,
                //                              String.Format("Indirizzo '{0}' NON georeferenziato",
                //                                            indirizzo.IndirizzoCompletoGeocode));
                //    }
                //}
                catch (Exception ex)
                {
                    worker.ReportProgress(1,
                                          String.Format(
                                              "Eccezione durante la georeferenziazione dell'indirizzo '{0}' - Eccezione: {1}",
                                              indirizzo.IndirizzoCompletoGeocode, ex.Message));
                }

                Thread.Sleep(2500);
            }
        }

        private void backgroundWorkerGeo_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if ((string)e.UserState != null)
                textBoxGeo.AppendText(Environment.NewLine + (string)e.UserState /* + Environment.NewLine*/);
            progressBarGeo.PerformStep();
            if (progressBarGeo.Value == progressBarGeo.Maximum)
                progressBarGeo.Value = 0;
        }

        private void backgroundWorkerGeo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                textBoxGeo.AppendText(Environment.NewLine + "Errore durante la procedura di georeferenzazione!");
            }
            //else if (e.Cancelled)
            //{
            //    textBoxGeo.AppendText(Environment.NewLine + "Procedura di import annullata!");
            //}
            else
            {
                progressBarGeo.Value = progressBarGeo.Maximum;
                textBoxGeo.AppendText(Environment.NewLine + "Procedura di georeferenzazione completata");
            }
            AbilitaBottoni();
        }

        private void buttonGeoNuovi_Click(object sender, EventArgs e)
        {
            AzzeraTutto();
            try
            {
                DisabilitaBottoni();
                IndirizzoCollection indirizzi = GetIndirizzi();
                progressBarGeo.Maximum = indirizzi.Count;
                backgroundWorkerGeo.RunWorkerAsync(indirizzi);
            }
            catch (Exception ex)
            {
                textBoxGeo.Text = "Eccezione durante la procedura di import" + Environment.NewLine + "Messaggio: " +
                                  ex.Message;
            }
        }

        private void DisabilitaBottoni()
        {
            buttonGeo.Enabled = false;
        }

        private void AbilitaBottoni()
        {
            buttonGeo.Enabled = true;
        }

        private void AzzeraTutto()
        {
            progressBarGeo.Value = 0;
            textBoxGeo.Text = String.Empty;
        }

        private IndirizzoCollection GetIndirizzi()
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                String query =
                    String.Format("SELECT {0}, {1}, {2}, {3}, {4}, {5} FROM {6} {7}",
                        ConfigurationManager.AppSettings["NomeCampoId"],
                        ConfigurationManager.AppSettings["NomeCampoIndirizzo"],
                        ConfigurationManager.AppSettings["NomeCampoCivico"],
                        ConfigurationManager.AppSettings["NomeCampoComune"],
                        ConfigurationManager.AppSettings["NomeCampoProvincia"],
                        ConfigurationManager.AppSettings["NomeCampoCap"],
                        ConfigurationManager.AppSettings["NomeTabella"],
                        ConfigurationManager.AppSettings["FiltroTabella"]);

                using (SqlCommand comando = new SqlCommand(query, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                Indirizzo indirizzo = new Indirizzo();
                                indirizzi.Add(indirizzo);

                                indirizzo.Identificativo = (Int32)reader[ConfigurationManager.AppSettings["NomeCampoId"]];
                                if (reader[ConfigurationManager.AppSettings["NomeCampoIndirizzo"]] != DBNull.Value)
                                {
                                    indirizzo.NomeVia = reader[ConfigurationManager.AppSettings["NomeCampoIndirizzo"]].ToString();
                                }
                                if (reader[ConfigurationManager.AppSettings["NomeCampoCivico"]] != DBNull.Value)
                                {
                                    indirizzo.Civico = reader[ConfigurationManager.AppSettings["NomeCampoCivico"]].ToString();
                                }
                                if (reader[ConfigurationManager.AppSettings["NomeCampoComune"]] != DBNull.Value)
                                {
                                    indirizzo.Comune = reader[ConfigurationManager.AppSettings["NomeCampoComune"]].ToString();
                                }
                                if (reader[ConfigurationManager.AppSettings["NomeCampoProvincia"]] != DBNull.Value)
                                {
                                    indirizzo.Provincia = reader[ConfigurationManager.AppSettings["NomeCampoProvincia"]].ToString();
                                }
                                if (reader[ConfigurationManager.AppSettings["NomeCampoCap"]] != DBNull.Value)
                                {
                                    indirizzo.Cap = reader[ConfigurationManager.AppSettings["NomeCampoCap"]].ToString();
                                }
                            }
                        }
                    }
                }
            }

            return indirizzi;
        }

        private void UpdateIndirizzo(Indirizzo indirizzoOriginale, Indirizzo indirizzoGeo)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                String query =
                    String.Format("UPDATE {0} SET {1} = '{2}', {3} = '{4}', {5} = '{6}', {7} = '{8}', {9} = '{10}', {11} = {12}, {13} = {14} WHERE {15} = {16}",
                        ConfigurationManager.AppSettings["NomeTabella"],
                        ConfigurationManager.AppSettings["NomeCampoIndirizzoDestinazione"],
                        String.Format("{0} {1}", indirizzoGeo.Qualificatore, indirizzoGeo.NomeVia).Trim().Replace("'", "''"),
                        ConfigurationManager.AppSettings["NomeCampoCivicoDestinazione"],
                        indirizzoGeo.Civico,
                        ConfigurationManager.AppSettings["NomeCampoComuneDestinazione"],
                        indirizzoGeo.Comune.Replace("'", "''"),
                        ConfigurationManager.AppSettings["NomeCampoProvinciaDestinazione"],
                        indirizzoGeo.Provincia,
                        ConfigurationManager.AppSettings["NomeCampoCapDestinazione"],
                        indirizzoGeo.Cap,
                        ConfigurationManager.AppSettings["NomeCampoLatitudineDestinazione"],
                        indirizzoGeo.Latitudine.ToString().Replace(',', '.'),
                        ConfigurationManager.AppSettings["NomeCampoLongitudineDestinazione"],
                        indirizzoGeo.Longitudine.ToString().Replace(',', '.'),
                        ConfigurationManager.AppSettings["NomeCampoId"],
                        indirizzoOriginale.Identificativo);

                using (SqlCommand comando = new SqlCommand(query, connection))
                {
                    connection.Open();
                    if (comando.ExecuteNonQuery() != 1)
                    {
                        throw new Exception("UpdateIndirizzo: Aggiornamento non riuscito");
                    }
                }
            }
        }

        private void InsertIndirizzi(Indirizzo indirizzoOriginale, Indirizzo indirizzoGeo)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString))
            {
                String query =
                    String.Format("INSERT INTO {0} ({1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}) VALUES ({9}, '{10}', '{11}', '{12}', '{13}', '{14}', {15}, {16})",
                        ConfigurationManager.AppSettings["NomeTabellaDestinazione"],

                        ConfigurationManager.AppSettings["NomeCampoIdDestinazione"], //1
                        ConfigurationManager.AppSettings["NomeCampoIndirizzoDestinazione"], //2
                        ConfigurationManager.AppSettings["NomeCampoCivicoDestinazione"], //3
                        ConfigurationManager.AppSettings["NomeCampoComuneDestinazione"], //4
                        ConfigurationManager.AppSettings["NomeCampoProvinciaDestinazione"], //5
                        ConfigurationManager.AppSettings["NomeCampoCapDestinazione"],  //6
                        ConfigurationManager.AppSettings["NomeCampoLatitudineDestinazione"], //7
                        ConfigurationManager.AppSettings["NomeCampoLongitudineDestinazione"], //8

                        indirizzoOriginale.Identificativo, //9
                        String.Format("{0} {1}", indirizzoGeo.Qualificatore, indirizzoGeo.NomeVia).Trim().Replace("'", "''"), //10
                        indirizzoGeo.Civico, //11
                        indirizzoGeo.Comune.Replace("'", "''"), //12
                        indirizzoGeo.Provincia, //13
                        indirizzoGeo.Cap, //14
                        indirizzoGeo.Latitudine.ToString().Replace(',', '.'), //15
                        indirizzoGeo.Longitudine.ToString().Replace(',', '.') // 16
                        );

                using (SqlCommand comando = new SqlCommand(query, connection))
                {
                    connection.Open();
                    if (comando.ExecuteNonQuery() != 1)
                    {
                        throw new Exception("InserIndirizzo: Inserimento non riuscito");
                    }
                }
            }
        }
    }
}