﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Cemi.GeocoderGenerico.Types;
using Cemi.GeocoderGenerico.WinApp.BingGeocodeService;

namespace TBridge.Cemi.Geocode.Business
{
    internal class BingProvider
    {
        private static readonly string Key = ConfigurationManager.AppSettings["BingKey"];

        internal static List<Indirizzo> Geocode(String address)
        {
            List<Indirizzo> indirizzi = new List<Indirizzo>();

            GeocodeRequest geocodeRequest = new GeocodeRequest();

            geocodeRequest.Credentials = new Credentials { ApplicationId = Key };
            geocodeRequest.Culture = "it-IT";
            geocodeRequest.Query = address;

            GeocodeServiceClient geocodeService = new GeocodeServiceClient("BasicHttpBinding_IGeocodeService");
            GeocodeResponse geocodeResponse = geocodeService.Geocode(geocodeRequest);

            foreach (GeocodeResult geocodeResult in geocodeResponse.Results)
            {
                Indirizzo indirizzo = new Indirizzo();
                indirizzo.NomeVia = geocodeResult.Address.AddressLine;
                indirizzo.Cap = geocodeResult.Address.PostalCode;
                indirizzo.Comune = geocodeResult.Address.Locality;
                indirizzo.Provincia =
                    geocodeResult.Address.FormattedAddress.Substring(geocodeResult.Address.FormattedAddress.Length - 2);
                indirizzo.Stato = geocodeResult.Address.CountryRegion;
                indirizzo.Latitudine = Convert.ToDecimal(geocodeResult.Locations[0].Latitude);
                indirizzo.Longitudine = Convert.ToDecimal(geocodeResult.Locations[0].Longitude);

                if (!String.IsNullOrEmpty(indirizzo.NomeVia))
                    indirizzi.Add(indirizzo);
            }

            return indirizzi;
        }
    }
}