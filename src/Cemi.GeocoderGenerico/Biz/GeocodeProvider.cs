﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Cemi.GeocoderGenerico.Types;

namespace TBridge.Cemi.Geocode.Business
{
    public class GeocodeProvider
    {
        private static readonly string GeocodeProviderType = ConfigurationManager.AppSettings["GeocodeProviderType"];

        public static List<Indirizzo> Geocode(String address)
        {
            List<Indirizzo> ret;

            switch (GeocodeProviderType)
            {
                case "Google":
                    ret = GoogleProvider.Geocode(address);
                    break;
                case "Bing":
                    ret = BingProvider.Geocode(address);
                    break;
                default:
                    ret = GoogleProvider.Geocode(address);
                    break;
            }

            return ret;
        }
    }
}