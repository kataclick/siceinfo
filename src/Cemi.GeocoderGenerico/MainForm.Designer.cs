namespace Cemi.GeocoderGenerico.WinApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxGeo = new System.Windows.Forms.TextBox();
            this.progressBarGeo = new System.Windows.Forms.ProgressBar();
            this.buttonGeo = new System.Windows.Forms.Button();
            this.backgroundWorkerGeo = new System.ComponentModel.BackgroundWorker();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxGeo);
            this.groupBox1.Controls.Add(this.progressBarGeo);
            this.groupBox1.Controls.Add(this.buttonGeo);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 330);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // textBoxGeo
            // 
            this.textBoxGeo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxGeo.Location = new System.Drawing.Point(6, 77);
            this.textBoxGeo.Multiline = true;
            this.textBoxGeo.Name = "textBoxGeo";
            this.textBoxGeo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxGeo.Size = new System.Drawing.Size(384, 247);
            this.textBoxGeo.TabIndex = 3;
            // 
            // progressBarGeo
            // 
            this.progressBarGeo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarGeo.Location = new System.Drawing.Point(6, 48);
            this.progressBarGeo.Name = "progressBarGeo";
            this.progressBarGeo.Size = new System.Drawing.Size(384, 23);
            this.progressBarGeo.Step = 1;
            this.progressBarGeo.TabIndex = 2;
            // 
            // buttonGeo
            // 
            this.buttonGeo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGeo.Location = new System.Drawing.Point(127, 19);
            this.buttonGeo.Name = "buttonGeo";
            this.buttonGeo.Size = new System.Drawing.Size(128, 23);
            this.buttonGeo.TabIndex = 1;
            this.buttonGeo.Text = "Georeferenzia";
            this.buttonGeo.UseVisualStyleBackColor = true;
            this.buttonGeo.Click += new System.EventHandler(this.buttonGeoNuovi_Click);
            // 
            // backgroundWorkerGeo
            // 
            this.backgroundWorkerGeo.WorkerReportsProgress = true;
            this.backgroundWorkerGeo.WorkerSupportsCancellation = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 330);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(285, 282);
            this.Name = "MainForm";
            this.Text = "Geocoder generico";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxGeo;
        private System.Windows.Forms.ProgressBar progressBarGeo;
        private System.Windows.Forms.Button buttonGeo;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGeo;
    }
}