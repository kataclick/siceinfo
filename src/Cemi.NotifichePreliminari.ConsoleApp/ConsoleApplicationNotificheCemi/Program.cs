﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.NotifichePreliminari.ConsoleApp.ServiceReferenceCantieri;
//using Cemi.NotifichePreliminari.ConsoleApp.DataProvider;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;


namespace Cemi.NotifichePreliminari.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");

            Program prg = new Program();

            if (args.Length == 0)
            {
                prg.Log("-- SPECIFICARE UN COMANDO: load/recovery --");
            }
            else
            {
                SecurityType st = prg.CreateSecurityType();
                Intestazione intest = prg.CreateIntestazione();

                #region CARICAMENTO DA EDILCONNECT
                if (args[0] == "edilconnect")
                {
                    Console.WriteLine("-- INIZIO PROCEDURA CARICAMENTO DA EDILCONNECT --");
                    DataProvider dpCantieri = new DataProvider();
                    dpCantieri.UploadFromEdilconnect();
                    Console.WriteLine("-- FINE PROCEDURA CARICAMENTO DA EDILCONNECT --");
                }
                #endregion

                #region CARICAMENTO NUOVE NOTIFICHE CON INPUT
                if (args[0] == "addinput")
                {
                    #region DA DATA
                    if (args[1] == "data")
                    {
                        DataProvider dataProviderCantieri = new DataProvider();

                        #region DEFINISCO LE DATE PER CARICARE LE NUOVE NOTIFICHE
                        Console.WriteLine("-- DATA DI INIZIO CARICAMENTO NOTIFICHE (gg/mm/aaaa): ");
                        DateTime dataNuove = Convert.ToDateTime(Console.ReadLine());//new DateTime();
                        //dataNuove = dataProviderCantieri.ReadDataNuove();
                        Console.WriteLine("");

                        Console.WriteLine("-- DATA DI FINE CARICAMENTO NOTIFICHE (gg/mm/aaaa): ");
                        DateTime dataFine = Convert.ToDateTime(Console.ReadLine());
                        Console.WriteLine("");
                        
                        /*DateTime dataFine = new DateTime();
                        dataFine = DateTime.Today.AddDays(-2);*/
                        #endregion

                        #region CARICO LE NUOVE NOTIFICHE
                        Console.WriteLine("-- INIZIO PROCEDURA CARICAMENTO NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine);

                        prg.Log(string.Format("-- INIZIO PROCEDURA CARICAMENTO NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine));
                        prg.LogLight(string.Format("-- INIZIO PROCEDURA CARICAMENTO NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine));
                        prg.LogLight(string.Empty);

                        dataProviderCantieri.LoadNew(st, intest, dataNuove, dataFine, null);

                        prg.Log("-- FINE PROCEDURA CARICAMENTO NUOVE NOTIFICHE --");
                        prg.LogLight("-- FINE PROCEDURA CARICAMENTO NUOVE NOTIFICHE --");
                        prg.LogLight(string.Empty);
                        prg.LogLight(string.Empty);
                        Console.WriteLine("-- FINE PROCEDURA CARICAMENTO NUOVE NOTIFICHE --");
                        Console.WriteLine(" ");
                        #endregion

                    }
                    #endregion

                    #region DA NUMERO NOTIFICA
                    if (args[1] == "numeroNotifica")
                    {
                        DataProvider dataProviderCantieri = new DataProvider();

                        #region LEGGO IL NUMERO DELLA NOTIFICA
                        Console.WriteLine("-- NUMERO NOTIFICA (protocollo/anno): ");
                        String numeroNotifica = Console.ReadLine();
                        Console.WriteLine("");
                        #endregion

                        #region CARICO LE NUOVE NOTIFICHE
                        Console.WriteLine("-- INIZIO PROCEDURA CARICAMENTO NUOVA NOTIFICA {0} --", numeroNotifica);

                        prg.Log(string.Format("-- INIZIO PROCEDURA CARICAMENTO NUOVA NOTIFICA {0} --", numeroNotifica));
                        prg.LogLight(string.Format("-- INIZIO PROCEDURA CARICAMENTO NUOVA NOTIFICA {0} --", numeroNotifica));
                        prg.LogLight(string.Empty);

                        dataProviderCantieri.LoadNewSingola(st, intest, numeroNotifica);

                        prg.Log("-- FINE PROCEDURA CARICAMENTO NUOVA NOTIFICA --");
                        prg.LogLight("-- FINE PROCEDURA CARICAMENTO NUOVA NOTIFICA --");
                        prg.LogLight(string.Empty);
                        prg.LogLight(string.Empty);
                        Console.WriteLine("-- FINE PROCEDURA CARICAMENTO NUOVA NOTIFICA --");
                        Console.WriteLine(" ");
                        #endregion
                    }
                    #endregion
                }
                #endregion
                
                #region CARICAMENTO NUOVE NOTIFICHE CON PARAMETRI
                if (args[0] == "add")
                {
                    DataProvider dataProviderCantieri = new DataProvider();

                    #region DEFINISCO LE DATE PER CARICARE LE NUOVE NOTIFICHE
                    DateTime dataNuove = new DateTime();
                    dataNuove = dataProviderCantieri.ReadDataNuove();

                    DateTime dataFine = new DateTime();
                    dataFine = DateTime.Today.AddDays(-2);
                    #endregion

                    #region CARICO LE NUOVE NOTIFICHE
                    Console.WriteLine("-- INIZIO PROCEDURA CARICAMENTO NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine);

                    prg.Log(string.Format("-- INIZIO PROCEDURA CARICAMENTO NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine));
                    prg.LogLight(string.Format("-- INIZIO PROCEDURA CARICAMENTO NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine));
                    prg.LogLight(string.Empty);

                    dataProviderCantieri.LoadNew(st, intest, dataNuove, dataFine, null);

                    prg.Log("-- FINE PROCEDURA CARICAMENTO NUOVE NOTIFICHE --");
                    prg.LogLight("-- FINE PROCEDURA CARICAMENTO NUOVE NOTIFICHE --");
                    prg.LogLight(string.Empty);
                    prg.LogLight(string.Empty);
                    Console.WriteLine("-- FINE PROCEDURA CARICAMENTO NUOVE NOTIFICHE --");
                    Console.WriteLine(" ");
                    #endregion
                }
                #endregion

                #region CARICAMENTO NON CONSECUTIVE
                if (args[0] == "consecutive")
                { 
                    DataProvider DPCantieri = new DataProvider();

                    Console.WriteLine("-- INIZIO CARICAMENTO NON CONSECUTIVE --");
                    prg.Log("-- INIZIO CARICAMENTO NON CONSECUTIVE --");
                    prg.LogLight("-- INIZIO CARICAMENTO NON CONSECUTIVE --");

                    DPCantieri.RecoveryNonConsecutive(st,intest);

                    Console.WriteLine("");
                    Console.WriteLine("-- FINE CARICAMENTO NON CONSECUTIVE --");
                    prg.Log("-- FINE CARICAMENTO NON CONSECUTIVE --");
                    prg.LogLight("-- FINE CARICAMENTO NON CONSECUTIVE --");
                    prg.LogLight(string.Empty);
                    prg.LogLight(string.Empty);
                    //Console.ReadLine();
                }
                #endregion

                #region CARICAMENTO DA TABELLA ERRORI
                if (args[0] == "recovery")
                {
                    DataProvider DPCantieri = new DataProvider();

                    Console.WriteLine("-- INIZIO PROCEDURA RECOVERY --");
                    prg.Log("-- INIZIO PROCEDURA RECOVERY --");
                    prg.LogLight("-- INIZIO PROCEDURA RECOVERY --");

                    //Console.WriteLine("");
                    //Console.WriteLine("-- RECOVERY EDILCONNECT --");
                    //prg.Log("-- RECOVERY EDILCONNECT --");
                    //prg.LogLight(string.Empty);
                    //prg.LogLight("-- RECOVERY EDILCONNECT --");
                    //prg.LogLight(string.Empty);
                    DPCantieri.RecoveryFromEdilconnect(args[1].ToString(), st, intest);
                    //Console.WriteLine("");
                    //Console.WriteLine("-- FINE RECOVERY EDILCONNECT --");
                    //prg.Log("-- FINE RECOVERY EDILCONNECT --");
                    //prg.LogLight(string.Empty);
                    //prg.LogLight("-- FINE RECOVERY EDILCONNECT --");
                    //prg.LogLight(string.Empty);

                    #region RECOVERY TABELLARE
                    /*
                    Console.WriteLine("");
                    Console.WriteLine("-- RECOVERY TABELLARE --");
                    prg.Log("-- RECOVERY TABELLARE --");
                    prg.LogLight(string.Empty);
                    prg.LogLight("-- RECOVERY TABELLARE --");
                    prg.LogLight(string.Empty);
                    DPCantieri.Recovery(st, intest);
                    Console.WriteLine("");
                    Console.WriteLine("-- FINE RECOVERY TABELLARE --");
                    prg.Log("-- FINE RECOVERY TABELLARE --");
                    prg.LogLight(string.Empty);
                    prg.LogLight("-- FINE RECOVERY TABELLARE --");
                    prg.LogLight(string.Empty);
                    */
                    #endregion

                    Console.WriteLine("");
                    Console.WriteLine("-- FINE PROCEDURA RECOVERY --");
                    prg.Log("-- FINE PROCEDURA RECOVERY --");
                    prg.LogLight("-- FINE PROCEDURA RECOVERY --");
                    prg.LogLight(string.Empty);
                    prg.LogLight(string.Empty);
                }
                #endregion

                #region REINSERIMENTO INCOMPLETE
                if (args[0] == "reinsert")
                { 
                    DataProvider DPCantieri = new DataProvider();

                    Console.WriteLine("-- INIZIO PROCEDURA REINSERIMENTO NOTIFICHE --");
                    prg.Log("-- INIZIO PROCEDURA REINSERIMENTO NOTIFICHE --");
                    prg.LogLight("-- INIZIO PROCEDURA REINSERIMENTO NOTIFICHE --");

                    DPCantieri.ReinserimentoIncomplete(st, intest);

                    Console.WriteLine("-- FINE PROCEDURA REINSERIMENTO NOTIFICHE --");
                    prg.Log("-- FINE PROCEDURA REINSERIMENTO NOTIFICHE --");
                    prg.LogLight("-- FINE PROCEDURA REINSERIMENTO NOTIFICHE --");

                }
                #endregion

                #region UPDATE CAP E PROVINCIA DA EDILCONNECT
                if (args[0] == "updatecap")
                {
                    DataProvider DPCantieri = new DataProvider();

                    Console.WriteLine("-- INIZIO PROCEDURA RECUPERO CAP E PROVINCIA DA EDILCONNECT --");
                    prg.Log("-- INIZIO PROCEDURA RECUPERO CAP E PROVINCIA DA EDILCONNECT --");
                    prg.LogLight("-- INIZIO PROCEDURA RECUPERO CAP E PROVINCIA DA EDILCONNECT --");

                    DPCantieri.UpdateCapProvinciaFromEdilconnect(st, intest);

                    Console.WriteLine("-- FINE PROCEDURA RECUPERO CAP E PROVINCIA DA EDILCONNECT --");
                    prg.Log("-- FINE PROCEDURA RECUPERO CAP E PROVINCIA DA EDILCONNECT --");
                    prg.LogLight("-- FINE PROCEDURA RECUPERO CAP E PROVINCIA DA EDILCONNECT --");

                }
                #endregion

                #region AGGIORNAMENTO NOTIFICHE
                if (args[0] == "update")
                {
                    DataProvider DPCantieri = new DataProvider();

                    Console.WriteLine("-- INIZIO PROCEDURA AGGIORNAMENTO NOTIFICHE --");
                    prg.Log("-- INIZIO PROCEDURA AGGIORNAMENTO NOTIFICHE --");
                    prg.LogLight("-- INIZIO PROCEDURA AGGIORNAMENTO NOTIFICHE --");

                    #region CICLO
                    if (args[1] == "ciclo")
                    {
                        #region DEFINISCO LE DATE PER I RECUPERI

                        DateTime dataInizioAggiornamento = new DateTime();
                        dataInizioAggiornamento = DPCantieri.ReadDataInizioAggiornamento();

                        DateTime dataFineAggiornamento = new DateTime();
                        dataFineAggiornamento = DateTime.Today.AddDays(-2);

                        DateTime dataInizio = new DateTime();
                        dataInizio = DPCantieri.ReadDataCursore();

                        DateTime dataFine = new DateTime(2012,12,31);
                        //dataFine = DateTime.Today.AddDays(-2);

                        #endregion

                        #region CARICO LE NUOVE NOTIFICHE (NON USATO)
                        //Console.WriteLine("-- CARICO LE NUOVE NOTIFICHE DA {0} A {1} --", dataNuove, dataFine);
                        //DPCantieri.LoadNew(st, intest, dataNuove, dataFine, null);
                        #endregion

                        #region CICLO PER L'AGGIORNAMENTO DELLE NOTIFICHE

                        while (dataInizioAggiornamento <= dataFineAggiornamento)
                        {
                            Console.WriteLine("-- AGGIORNAMENTO CANTIERI  Giorno: {0} --", dataInizioAggiornamento);
                            while (dataInizio <= dataFine)
                            {
                                try
                                {
                                    DPCantieri.UpdateNew(st, intest, dataInizio, dataFine, null, dataInizioAggiornamento, dataFineAggiornamento);
                                }
                                catch (Exception exc)
                                {
                                    prg.Log(string.Format("ERRORE PER AGGIORNAMENTO {0} DEL GIORNO {1} \n {2}", dataInizioAggiornamento, dataInizio, exc));
                                    prg.LogLight(string.Format("ERRORE PER AGGIORNAMENTO {0} DEL GIORNO {1}", dataInizioAggiornamento, dataInizio));
                                    //DPCantieri.InsertErrore(null, dataInizio, 1, dataInizioAggiornamento);
                                }
                                dataInizio = dataInizio.AddDays(1);
                                DPCantieri.UdpateDateAggiornamento(dataInizio, dataInizioAggiornamento);

                            }
                            dataInizio = DateTime.Today.AddYears(-2);
                            dataInizioAggiornamento = dataInizioAggiornamento.AddDays(1);
                            DPCantieri.UdpateDateAggiornamento(dataInizio, dataInizioAggiornamento);
                        }
                        #endregion

                        //dataProviderCantieri.UdpateUpdateDate(DateTime.Parse("01/01/1889"), DateTime.Today);
                    }
                    #endregion

                    #region DA EDILCONNECT
                    if (args[1] == "edilconnect")
                    {
                        DPCantieri.UpdateFromEdilconnect(st,intest);
                    }
                    #endregion

                    #region DA INPUT - DATA
                    if (args[1] == "input")
                    {
                        #region DEFINISCO LE DATE PER CARICARE LE NUOVE NOTIFICHE
                        Console.WriteLine("-- DATA DI INIZIO CARICAMENTO NOTIFICHE (gg/mm/aaaa): ");
                        DateTime dataInizio = Convert.ToDateTime(Console.ReadLine());
                        Console.WriteLine("");

                        Console.WriteLine("-- DATA DI FINE CARICAMENTO NOTIFICHE (gg/mm/aaaa): ");
                        DateTime dataFine = Convert.ToDateTime(Console.ReadLine());
                        Console.WriteLine("");
                        //dataFine = DateTime.Today.AddDays(-2);

                        DateTime dataInizioAggiornamento = new DateTime();
                        dataInizioAggiornamento = DateTime.MinValue;

                        DateTime dataFineAggiornamento = new DateTime();
                        dataFineAggiornamento = DateTime.Today.AddDays(-2);
                        #endregion

                        #region CICLO PER REINSERIMENTO DELLE NOTIFICHE
                        //Console.WriteLine("-- REINSERIMENTO CANTIERI  Giorno: {0} --", dataInizioAggiornamento);
                        while (dataInizio <= dataFine)
                        {
                            try
                            {
                                DPCantieri.UpdateNew(st, intest, dataInizio, dataFine, null, DateTime.MinValue, DateTime.MinValue);
                            }
                            catch (Exception exc)
                            {
                                prg.Log(string.Format("ERRORE PER REINSERIMENTO DEL GIORNO {0} \n {1}", dataInizio, exc));
                                prg.LogLight(string.Format("ERRORE PER REINSERIMENTO DEL GIORNO {0}", dataInizio));
                                //DPCantieri.InsertErrore(null, dataInizio, 0, DateTime.MaxValue);
                            }
                            dataInizio = dataInizio.AddDays(1);
                            DPCantieri.UdpateDateAggiornamento(dataInizio, dataInizioAggiornamento);

                        }
                        #endregion
                        
                    }
                    #endregion

                    #region DA INPUT - NUMERO NOTIFICA
                    if (args[1] == "numeroNotifica")
                    {

                        /*#region DEFINISCO LE DATE PER CARICARE LE NUOVE NOTIFICHE
                        Console.WriteLine("-- DATA DI INIZIO CARICAMENTO NOTIFICHE (gg/mm/aaaa): ");
                        DateTime dataInizio = Convert.ToDateTime(Console.ReadLine());
                        Console.WriteLine("");

                        Console.WriteLine("-- DATA DI FINE CARICAMENTO NOTIFICHE (gg/mm/aaaa): ");
                        DateTime dataFine = Convert.ToDateTime(Console.ReadLine());
                        Console.WriteLine("");
                        //dataFine = DateTime.Today.AddDays(-2);

                        DateTime dataInizioAggiornamento = new DateTime();
                        dataInizioAggiornamento = DateTime.MinValue;

                        DateTime dataFineAggiornamento = new DateTime();
                        dataFineAggiornamento = DateTime.Today.AddDays(-2);
                        #endregion
                        */
                        Console.WriteLine("-- NUMERO NOTIFICA (protocollo/anno): ");
                        String numeroNotifica = Console.ReadLine().ToString();
                        #region CICLO PER REINSERIMENTO DELLE NOTIFICHE
                        //Console.WriteLine("-- REINSERIMENTO CANTIERI  Giorno: {0} --", dataInizioAggiornamento);
                        
                            try
                            {
                                DPCantieri.UpdateNewSingolo(st, intest, numeroNotifica);
                            }
                            catch (Exception exc)
                            {
                                prg.Log(string.Format("ERRORE PER REINSERIMENTO DELLA NOTIFICA {0} \n {1}", numeroNotifica, exc));
                                prg.LogLight(string.Format("ERRORE PER REINSERIMENTO DELLA NOTIFICA {0}", numeroNotifica));
                                //DPCantieri.InsertErrore(numeroNotifica, null, 0, DateTime.MaxValue);
                            }

                        #endregion

                    }
                    #endregion

                    Console.WriteLine("-- FINE PROCEDURA AGGIORNAMENTO --");
                    prg.Log("-- FINE PROCEDURA AGGIORNAMENTO --");
                    prg.LogLight("-- FINE PROCEDURA AGGIORNAMENTO --");
                    prg.LogLight(string.Empty);
                    prg.LogLight(string.Empty);
                    //Console.WriteLine("-- PREMERE UN TASTO PER CONCLUDERE --");
                    //Console.ReadLine();
                }
                #endregion
            }
        }


        public SecurityType CreateSecurityType()
        {
            SecurityType st;
            st = new SecurityType();
            st.UsernameToken = new UsernameToken();

            st.UsernameToken.Username = "cassaedilemilano";
            st.UsernameToken.Password = "JeRXVcVf";
            st.Timestamp = new Timestamp();
            st.Timestamp.Created = DateTime.Now.AddHours(-12);
            st.Timestamp.Expires = DateTime.Now.AddHours(12);
            return st;
        }

        public Intestazione CreateIntestazione()
        {
            Intestazione intest = new Intestazione();
            intest.IntestazioneMessaggio = new IntestazioneMessaggio();
            intest.IntestazioneMessaggio.Mittente = new IdentificativoParte[1];

            IdentificativoParte ident = new IdentificativoParte();
            intest.IntestazioneMessaggio.Mittente[0] = ident;
            ident.tipo = "CodicePA";
            ident.Value = "Cassa Edile Milano, Lodi, Monza e Brianza";
            ident.indirizzoTelematico = "www.cassaedilemilano.it";

            IdentificativoParte idenDest = new IdentificativoParte();
            intest.IntestazioneMessaggio.Destinatario = new Destinatario();
            intest.IntestazioneMessaggio.Destinatario.IdentificativoParte = idenDest;
            idenDest.tipo = "CodicePA";
            idenDest.Value = "Regione Lombardia";


            intest.IntestazioneMessaggio.ProfiloCollaborazione = new ProfiloCollaborazione();
            intest.IntestazioneMessaggio.ProfiloCollaborazione.Value =
                ProfiloCollaborazioneBaseType.EGOV_IT_ServizioSincrono;

            intest.IntestazioneMessaggio.Servizio = new Servizio();
            intest.IntestazioneMessaggio.Servizio.tipo = "TEST";
            intest.IntestazioneMessaggio.Servizio.Value = "WS - CANTIERI";

            intest.ListaTrasmissioni = new Trasmissione[1];
            intest.ListaTrasmissioni[0] = new Trasmissione();
            intest.ListaTrasmissioni[0].Origine = new Origine();
            intest.ListaTrasmissioni[0].Origine.IdentificativoParte = ident;
            intest.ListaTrasmissioni[0].OraRegistrazione = new OraRegistrazione();
            intest.ListaTrasmissioni[0].OraRegistrazione.Value = DateTime.Now;
            intest.ListaTrasmissioni[0].Destinazione = new Destinazione();
            intest.ListaTrasmissioni[0].Destinazione.IdentificativoParte = idenDest;


            intest.IntestazioneMessaggio.Messaggio = new Messaggio();
            intest.IntestazioneMessaggio.Messaggio.Identificatore = DateTime.Now.ToString("ddMMyyyyHHmmss");
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione = new OraRegistrazione();
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione.tempo = OraRegistrazioneTempo.EGOV_IT_Locale;
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione.Value = DateTime.Now;
            intest.IntestazioneMessaggio.Messaggio.RiferimentoMessaggio = DateTime.Now.ToString("ddMMyyyyHHmmss");
            intest.IntestazioneMessaggio.Messaggio.ScadenzaSpecified = false;

            intest.IntestazioneMessaggio.Azione = "WS - CANTIERI";
            intest.IntestazioneMessaggio.ProfiloTrasmissione = new ProfiloTrasmissione();
            intest.IntestazioneMessaggio.ProfiloTrasmissione.confermaRicezione = false;
            intest.IntestazioneMessaggio.ProfiloTrasmissione.inoltro = ProfiloTrasmissioneInoltro.EGOV_IT_PIUDIUNAVOLTA;

            intest.IntestazioneMessaggio.ProfiloCollaborazione = new ProfiloCollaborazione();
            intest.IntestazioneMessaggio.ProfiloCollaborazione.Value =
                ProfiloCollaborazioneBaseType.EGOV_IT_ServizioSincrono;

            intest.IntestazioneMessaggio.Collaborazione = String.Format("CassaEdileMilanoRegioneLombardia{0}",
                                                                        DateTime.Now.ToString("ddMMyyyyHHmmss"));
            return intest;
        }

        public void Log(String message)
        {
            String msg = String.Format("{0} ---- {1}", DateTime.Now, message);
            using (StreamWriter writer = new StreamWriter("Log.txt", true))
            {
                writer.WriteLine(msg);
                writer.WriteLine();
            }
            //Console.WriteLine();
            int maxlenght = msg.Length;
            if (maxlenght > 255)
            {
                maxlenght = 255;
            }
            Console.WriteLine(msg.Substring(0, maxlenght));
            Console.WriteLine();
        }

        public void LogLight(String message)
        {
            //String msg = string.Empty;
            if (string.IsNullOrEmpty(message))
            {
                using (StreamWriter writer = new StreamWriter("LogLight.txt", true))
                {
                    writer.WriteLine();
                }
            }
            else
            {
                String msg = String.Format("{0} ---- {1}", DateTime.Now, message);
                using (StreamWriter writer = new StreamWriter("LogLight.txt", true))
                {
                    writer.WriteLine(msg);
                }
            }
        }

        private static DateTime WriteEndDate(int anno)
        {
            Program prg = new Program();
            if (anno > 0)
            {
                DateTime endDate = new DateTime();
                if (anno == System.DateTime.Now.Year)
                {
                    endDate = System.DateTime.Now.Date;
                }
                else
                {
                    endDate = new DateTime(anno, 12, 31);
                }
                return endDate;
            }
            else
            {
                prg.Log("-- Lettura anno non riuscita --");
                DateTime error = new DateTime(1900, 01, 01);
                return error;
            }

        }

        public String GetDescrizioneProvincia(String prov)
        {
            String descrizioneProvincia = prov;

            switch (prov)
            {
                case "PV":
                    descrizioneProvincia = "PAVIA";
                    break;
                case "CO":
                    descrizioneProvincia = "COMO";
                    break;
                case "MI":
                    descrizioneProvincia = "MILANO";
                    break;
                case "CR":
                    descrizioneProvincia = "CREMONA";
                    break;
                case "VA":
                    descrizioneProvincia = "VARESE";
                    break;
                case "LO":
                    descrizioneProvincia = "LODI";
                    break;
                case "LC":
                    descrizioneProvincia = "LECCO";
                    break;
                case "SO":
                    descrizioneProvincia = "SONDRIO";
                    break;
                case "MN":
                    descrizioneProvincia = "MANTOVA";
                    break;
                case "MB":
                    descrizioneProvincia = "MONZA E DELLA BRIANZA";
                    break;
                case "BG":
                    descrizioneProvincia = "BERGAMO";
                    break;
                case "BS":
                    descrizioneProvincia = "BRESCIA";
                    break;
            }

            return descrizioneProvincia;
        }
        
    }
}
