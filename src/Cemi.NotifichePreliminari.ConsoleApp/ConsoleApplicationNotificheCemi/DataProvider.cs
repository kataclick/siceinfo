﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.NotifichePreliminari.ConsoleApp.ServiceReferenceCantieri;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//using Cemi.NotifichePreliminari.ConsoleApp.Program;

namespace Cemi.NotifichePreliminari.ConsoleApp
{
    class DataProvider
    {
        System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
        
        //string connstring = "Data Source=SRV-TESTSQL;Initial Catalog=SICE;Integrated Security=SSPI";





        public DateTime ReadDataInizioAggiornamento()
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariSelectDataAggiornamento", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    DateTime data = new DateTime();
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            data = Convert.ToDateTime(dr["aggiornamento"]);
                            return data;
                        }
                        return DateTime.MinValue;
                    }
                }
            }
        }

        public DateTime ReadDataCursore()
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariSelectDataCursore", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    DateTime data = new DateTime();
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            data = Convert.ToDateTime(dr["caricamento"]);
                            return data;
                        }
                        return DateTime.MinValue;
                    }
                }
            }
        }

        public void UdpateDateAggiornamento(DateTime dataCursore, DateTime dataAggiornamento)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariUpdateDateAggiornamento", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    if (dataCursore.Year < 1900 || dataCursore.Year > 2070)
                        comm.Parameters.AddWithValue("@inputCaricamento", DBNull.Value);
                    else
                        comm.Parameters.AddWithValue("@inputCaricamento", dataCursore);

                    if(dataAggiornamento.Year < 1900 || dataAggiornamento.Year > 2070)
                        comm.Parameters.AddWithValue("@inputAggiornamento", DBNull.Value);
                    else
                        comm.Parameters.AddWithValue("@inputAggiornamento", dataAggiornamento);
                    comm.ExecuteNonQuery();
                }
            }
        }

        public DateTime ReadDataNuove()
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariSelectDataNuove", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    DateTime data = new DateTime();
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            data = Convert.ToDateTime(dr["nuove"]);
                            return data;
                        }
                        return DateTime.MinValue;
                    }
                }
            }
        }
        
        public void UdpateDataNuove(DateTime dataNuove)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariUpdateDataNuove", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    if (dataNuove.Year < 1900 || dataNuove.Year > 2070)
                        comm.Parameters.AddWithValue("@inputNuove", DBNull.Value);
                    else
                        comm.Parameters.AddWithValue("@inputNuove", dataNuove);

                    comm.ExecuteNonQuery();
                }
            }
        }

        public int ReadYear()
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariSelectAnno", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    string anno;
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            anno = dr["anno"].ToString();
                            return Convert.ToInt32(anno);
                        }
                        return 0;
                    }
                }
            }
        }

        public void UpdateYear()
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand comm = new SqlCommand("dbo.Marco_NotifichePreliminariUpdateAnno", conn))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    comm.ExecuteNonQuery();

                }
            }
        }
        
        public void DeleteYear()
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariDeleteAnno", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }

        }



        public void UploadFromEdilconnect()
        {
            string dataUpdate = string.Empty;
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectMaxUpdate", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            dataUpdate = dr["dataAggiornamentoNotifica"].ToString();
                        }
                    }
                }
            }
            //TEMPORANEO: PER CARICAMENTO TOTALE
            //dataUpdate = string.Empty;
            //TEMPORANEO: PER CARICAMENTO TOTALE

            string query = string.Empty;
            if (String.IsNullOrEmpty(dataUpdate))
            {
                query = "SELECT dbNUMERO_NOTIFICA, dbDATA_INSERIMENTO, dbDATA_ULTIMA_MODIFICA, dbLAST_CHECKED_DATE FROM dbo.tbASLNotifica";                
            }
            else
            {
                //query = "SELECT dbNUMERO_NOTIFICA, dbDATA_INSERIMENTO, dbDATA_ULTIMA_MODIFICA, dbLAST_CHECKED_DATE FROM dbo.tbASLNotifica WHERE dbLAST_CHECKED_DATE >= '" + dataUpdate + "'";
                query = "SELECT dbNUMERO_NOTIFICA, dbDATA_INSERIMENTO, dbDATA_ULTIMA_MODIFICA, dbLAST_CHECKED_DATE FROM dbo.tbASLNotifica ";
                query = query + "WHERE dateadd(dd, 0, datediff(dd, 0, dbLAST_CHECKED_DATE)) >= '" + dataUpdate + "' ";
                query = query + "AND dateadd(dd,0, datediff(dd, 0, dbLAST_CHECKED_DATE)) = dbDATA_INSERIMENTO ";
                query = query + "UNION ";
                query = query + "SELECT dbNUMERO_NOTIFICA, dbDATA_INSERIMENTO, dbDATA_ULTIMA_MODIFICA, dbLAST_CHECKED_DATE FROM dbo.tbASLNotifica ";
                query = query + "WHERE dateadd(dd, 0, datediff(dd, 0, dbLAST_CHECKED_DATE)) >= '" + dataUpdate + "' ";
                query = query + "AND dbDATA_ULTIMA_MODIFICA >= DATEADD(day, -10, dateadd(dd, 0, datediff(dd, 0, dbLAST_CHECKED_DATE)))";
            }

            System.Configuration.ConnectionStringSettings conedil = ConfigurationManager.ConnectionStrings["EdilConnect"];
            var table = new DataTable();
            Console.WriteLine("-- Carico le notifiche da EdilConnect dal giorno {0:yyyymmdd} --", dataUpdate);
            using (var da = new SqlDataAdapter(query, conedil.ToString()))
            {
                da.Fill(table);
            }
            int numNot = 0;
            foreach (DataRow dr in table.Rows)
            {
                numNot = CheckNumeroNotificaEdilconnect(dr["dbNUMERO_NOTIFICA"].ToString());
                using (SqlConnection conn = new SqlConnection(constr.ToString()))
                {
                    conn.Open();
                    if (numNot == 0)
                    {
                        using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectInsert", conn))
                        {
                            command.CommandType = System.Data.CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@numeroNotifica", dr["dbNUMERO_NOTIFICA"]);
                            command.Parameters.AddWithValue("@dataComunicazioneNotifica", Convert.ToDateTime(dr["dbDATA_INSERIMENTO"]));
                            command.Parameters.AddWithValue("@dataAggiornamentoNotifica", Convert.ToDateTime(dr["dbDATA_ULTIMA_MODIFICA"]));
                            command.Parameters.AddWithValue("@dataUltimoControllo", Convert.ToDateTime(dr["dbLAST_CHECKED_DATE"]));

                            try
                            {
                                command.ExecuteNonQuery();
                                Console.WriteLine("-- Caricata la notifica {0} --", dr["dbNUMERO_NOTIFICA"].ToString());
                            }
                            catch (SqlException exc)
                            {
                                if (exc.Number == 2601)
                                {
                                    Program var = new Program();
                                    var.Log(string.Format("Eccezione sql per il caricamento da EdilConnect"));
                                }

                            }

                        }
                    }
                    else
                    {
                        using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectUpdate", conn))
                        {
                            command.CommandType = System.Data.CommandType.StoredProcedure;

                            command.Parameters.AddWithValue("@numeroNotifica", dr["dbNUMERO_NOTIFICA"]);
                            command.Parameters.AddWithValue("@dataAggiornamentoNotifica", Convert.ToDateTime(dr["dbDATA_ULTIMA_MODIFICA"]));
                            command.Parameters.AddWithValue("@dataUltimoControllo", Convert.ToDateTime(dr["dbLAST_CHECKED_DATE"]));

                            try
                            {
                                command.ExecuteNonQuery();
                                Console.WriteLine("-- Aggiornata la notifica {0} --", dr["dbNUMERO_NOTIFICA"].ToString());
                            }
                            catch (SqlException exc)
                            {
                                if (exc.Number == 2601)
                                {
                                    Program var = new Program();
                                    var.Log(string.Format("Eccezione sql per il caricamento da EdilConnect"));
                                }

                            }

                        }
                    }
                }

            }

        }

        public void RecoveryFromEdilconnect(string mode, SecurityType st, Intestazione intest)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                #region PER GIORNO
                if (mode == "day")
                {
                    using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectSelectGiorni", conn))
                    {
                        //command.Parameters.AddWithValue("@tipo", tipo);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                Program prg = new Program();
                                while (reader.Read())
                                {
                                    Console.WriteLine("-- RECUPERO GIORNO {0} --", reader[0]);
                                    LoadNew(st, intest, Convert.ToDateTime(reader[0]), Convert.ToDateTime(reader[0]), string.Empty);
                                }
                            }
                        }
                    }
                }
                #endregion

                #region PER NOTIFICA
                if (mode == "notifica")
                {
                    using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectSelectNotifiche", conn))
                    {
                        //command.Parameters.AddWithValue("@tipo", tipo);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {
                                Program prg = new Program();
                                while (reader.Read())
                                {
                                    Console.WriteLine("-- RECUPERO NOTIFICA {0} DEL GIORNO {1} --", reader[0], reader[1]);
                                    LoadNew(st, intest, Convert.ToDateTime(reader[1]), Convert.ToDateTime(reader[1]), reader[0].ToString());
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }

        public void UpdateFromEdilconnect(SecurityType st, Intestazione intest)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectSelectUpdate", conn))
                {
                    //command.Parameters.AddWithValue("@tipo", tipo);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            //Program prg = new Program();
                            while (reader.Read())
                            {
                                Console.WriteLine("-- RECUPERO NOTIFICA {0} DEL GIORNO {1} CON AGGIORNAMENTO DEL {2} --", reader[0], reader[1], reader[2]);
                                UpdateNew(st, intest, Convert.ToDateTime(reader[1]), Convert.ToDateTime(reader[1]), reader[0].ToString(), Convert.ToDateTime(reader[2]), Convert.ToDateTime(reader[2]));
                            }
                        }
                    }
                }
            }
        }

        public void UpdateCapProvinciaFromEdilconnect(SecurityType st, Intestazione intest)
        {
            Program var = new Program();
            String query = String.Empty;
            DataTable dt = new DataTable();
            dt.Columns.Add("dbNUMERO_Notifica", typeof(String));
            dt.Columns.Add("dbCAP", typeof(String));
            dt.Columns.Add("dbPROVINCIA", typeof(String));
            System.Configuration.ConnectionStringSettings constredil = ConfigurationManager.ConnectionStrings["EdilConnect"];

            Console.WriteLine("-- RECUPERO ELENCO NOTIFICHE SENZA CAP E PROVINCIA --");

            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("dbo.Marco_NotifichePreliminariUpdateCapProvinciaFromEdilconnect", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            if (dr["numeroNotifica"]!=null)
                            {
                                using (SqlConnection conedil = new SqlConnection(constredil.ToString()))
                                {
                                    conedil.Open();
                                    query = String.Format("SELECT dbNUMERO_NOTIFICA, dbCAP, dbPROVINCIA FROM dbo.tbASLNotifica WHERE dbNUMERO_NOTIFICA = '{0}'", dr["numeroNotifica"].ToString());
                                    Console.WriteLine(String.Format("-- RECUPERO DATI NOTIFICA {0} --",dr["numeroNotifica"].ToString()));
                                    using (SqlCommand cmded = new SqlCommand(query, conedil))
                                    {
                                        cmded.CommandType = System.Data.CommandType.Text;
                                        using (SqlDataReader dred = cmded.ExecuteReader())
                                        {
                                            while (dred.Read())
                                            {
                                                if (dred.HasRows)
                                                {
                                                    if (!String.IsNullOrEmpty(dred["dbCAP"].ToString()))
                                                    {
                                                        dt.Rows.Add(dred["dbNUMERO_NOTIFICA"], var.GetDescrizioneProvincia(dred["dbCAP"].ToString()), dred["dbPROVINCIA"]);
                                                    }
                                                    else 
                                                    {
                                                        dt.Rows.Add(dred["dbNUMERO_NOTIFICA"], dred["dbCAP"], dred["dbPROVINCIA"]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(dt.Rows.Count > 0)
            {
                InsertCapProvinciaFromDataTable(dt);
            }
        }

        public void InsertCapProvinciaFromDataTable(DataTable dt)
        {
            String query = String.Empty;
            Console.WriteLine("");
            Console.WriteLine("-- INIZIO UPDATE INDIRIZZI --");
            Console.WriteLine("");
            foreach (DataRow dr in dt.Rows)
            {
                query = String.Empty;
                query = String.Format("UPDATE dbo.NotifichePreliminariIndirizzi SET CAP = '{0}', provincia = '{1}' WHERE numeroNotifica =  '{2}'", dr["dbCAP"].ToString(), dr["dbPROVINCIA"].ToString(), dr["dbNUMERO_NOTIFICA"].ToString());
                Console.WriteLine("-- UPDATE CAP E PROVINCIA NOTIFICA {0} --", dr["dbNUMERO_NOTIFICA"].ToString());
                using (SqlConnection conn = new SqlConnection(constr.ToString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }


        public void RecoveryNonConsecutive(SecurityType st, Intestazione intest)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariNonConsecutiveSelect", conn))
                {
                    //command.Parameters.AddWithValue("@tipo", tipo);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            Program prg = new Program();
                            while (reader.Read())
                            {
                                Console.WriteLine("-- RECUPERO NOTIFICA {0} --", reader[0]);
                                LoadNew(st, intest, DateTime.MinValue, DateTime.MinValue, reader[0].ToString());
                            }
                        }
                    }
                }
            }
        }

        public void ReinserimentoIncomplete(SecurityType st, Intestazione intest)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariIncomplete", conn))
                {
                    //command.Parameters.AddWithValue("@tipo", tipo);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            Program prg = new Program();
                            while (reader.Read())
                            {
                                Console.WriteLine("-- RECUPERO NOTIFICA {0} DEL GIORNO {1} --", reader[0], reader[1]);
                                prg.LogLight(String.Format("-- RECUPERO NOTIFICA {0} DEL GIORNO {1} --", reader[0], reader[1]));
                                UpdateNew(st, intest, Convert.ToDateTime(reader[1]), Convert.ToDateTime(reader[1]), reader[0].ToString(), DateTime.MinValue, DateTime.MinValue);
                                //LoadNew(st, intest, Convert.ToDateTime(reader[1]), Convert.ToDateTime(reader[1]), reader[0].ToString());
                            }
                        }
                    }
                }
            }
        }

        public void LoadNew(SecurityType st, Intestazione intest, DateTime dataInizio, DateTime dataFine, String numeroNotifica)
        {
            Program prg = new Program();

            if (String.IsNullOrEmpty(numeroNotifica))
                numeroNotifica = null;


            using (CantieriServicePortClient client = new ServiceReferenceCantieri.CantieriServicePortClient())
            {
                CantiereTR4[] cantiereTR4;
                while (dataInizio <= dataFine)
                {
                    try
                    {
                        if (dataInizio == DateTime.MinValue)
                        {
                            if (!String.IsNullOrEmpty(numeroNotifica))
                            {
                                Console.WriteLine("-- INIZIO RECUPERO CANTIERI: notifica {0} --", numeroNotifica);
                                prg.LogLight(string.Empty);
                                prg.LogLight(string.Format("-- INIZIO RECUPERO CANTIERI: notifica {0} --", numeroNotifica));
                            }

                            client.ricercaCantieriTR4(st, ref intest,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                numeroNotifica,
                                                null,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                null,
                                                null,
                                                new String[0],
                                                out cantiereTR4);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(numeroNotifica))
                            {
                                Console.WriteLine("-- INIZIO RECUPERO CANTIERI: Giorno {0} --", dataInizio);
                                prg.LogLight(string.Empty);
                                prg.LogLight(string.Format("-- INIZIO RECUPERO CANTIERI: Giorno {0} --", dataInizio));
                            }
                            else
                            {
                                Console.WriteLine("-- INIZIO RECUPERO CANTIERI: Giorno {0}, notifica {1} --", dataInizio, numeroNotifica);
                                prg.LogLight(string.Empty);
                                prg.LogLight(string.Format("-- INIZIO RECUPERO CANTIERI: Giorno {0}, notifica {1} --", dataInizio, numeroNotifica));
                            }

                            client.ricercaCantieriTR4(st, ref intest,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                numeroNotifica,
                                                dataInizio,
                                                dataInizio,
                                                null,
                                                null,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                null,
                                                string.Empty,
                                                null,
                                                null,
                                                new String[0],
                                                out cantiereTR4);
                        }

                        Console.WriteLine("-- CANTIERI RECUPERATI: {0}", cantiereTR4.Length);
                        int totImprese = 0;
                        int totIndirizzi = 0;
                        int totPersone = 0;
                        int totCantieri = 0;
                        for (int i = 0; i < cantiereTR4.Length; i++)
                        {
                            int numNot = 0;
                            numNot = CheckNumeroNotifica(cantiereTR4[i].nrNotifica);
                            if (numNot == 0)
                            {
                                System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                                using (SqlConnection conn = new SqlConnection(constr.ToString()))
                                {
                                    conn.Open();
                                    using (SqlTransaction tran = conn.BeginTransaction())
                                    {
                                        try
                                        {
                                            #region Carico i cantieri
                                            //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                                            InsertCantiere(cantiereTR4[i], tran, conn);
                                            totCantieri = totCantieri + 1;
                                            #endregion

                                            #region Recupero e carico le imprese per ogni cantere
                                            ImpresaCantTR4[] imprese;

                                            //Console.WriteLine("-- RECUPERO LE IMPRESE PER IL CANTIERE {0}--", i+1);
                                            client.ricercaImpreseTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out imprese);
                                            //Console.WriteLine("-- IMPRESE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, imprese.Length);
                                            totImprese = totImprese + imprese.Length;

                                            if (imprese.Length > 0)
                                            {
                                                for (int y = 0; y < imprese.Length; y++)
                                                {
                                                    //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                                                    InsertImpresa(imprese[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                }
                                            }
                                            #endregion

                                            #region Recupero e carico gli indirizzi per ogni cantiere
                                            IndirizzoCantTR4[] indirizzi;

                                            //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
                                            client.ricercaIndirizziTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out indirizzi);
                                            //Console.WriteLine("-- INDIRIZZI RECUPERATI PER IL CANTIERE {0}: {1}", i + 1, indirizzi.Length);
                                            totIndirizzi = totIndirizzi + indirizzi.Length;

                                            if (indirizzi.Length > 0)
                                            {
                                                for (int y = 0; y < indirizzi.Length; y++)
                                                {
                                                    //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                    InsertIndirizzo(indirizzi[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                }
                                            }
                                            #endregion

                                            #region Recupero e carico le persone per ogni cantiere
                                            PersonaCantTR4[] persone;

                                            //Console.WriteLine("-- RECUPERO LE PERSONE PER IL CANTIERE {0}--", i + 1);
                                            client.ricercaPersoneTR4(st, ref intest, cantiereTR4[i].nrNotifica, out persone);
                                            //Console.WriteLine("-- PERSONE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, persone.Length);
                                            totPersone = totPersone + persone.Length;

                                            if (persone.Length > 0)
                                            {
                                                for (int y = 0; y < persone.Length; y++)
                                                {
                                                    //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                    InsertPersona(persone[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                }
                                            }
                                            #endregion
                                            //Console.WriteLine(" ");
                                            tran.Commit();
                                        }
                                        catch (Exception exc)
                                        {
                                            tran.Rollback();
                                            prg.Log(string.Format("ERRORE INSERIMENTO NOTIFICA {0} DEL GIORNO {1}: \n {2}", cantiereTR4[i].nrNotifica, dataInizio, exc));
                                            prg.LogLight(string.Format("ERRORE INSERIMENTO NOTIFICA {0} DEL GIORNO {1}", cantiereTR4[i].nrNotifica, dataInizio));
                                            //InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 0, DateTime.MaxValue);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("-- NOTIFICA {0} GIA' PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica);
                            }

                        }
                        Console.WriteLine("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, totCantieri);
                        Console.WriteLine(" ");
                        prg.LogLight(string.Format("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, totCantieri));
                        prg.LogLight(string.Empty);

                    }
                    catch (Exception exc)
                    {

                        prg.Log(String.Format("ERRORE per giorno {0} \n {1}", dataInizio, exc));
                        prg.LogLight(String.Format("ERRORE per giorno {0}", dataInizio));
                        prg.LogLight(string.Empty);
                        //InsertErrore(numeroNotifica, dataInizio, 0, DateTime.MaxValue);
                    }

                    dataInizio = dataInizio.AddDays(1);
                }
            }
        }

        public void LoadNewSingola(SecurityType st, Intestazione intest, String numeroNotifica)
        {
            Program prg = new Program();

            if (String.IsNullOrEmpty(numeroNotifica))
                numeroNotifica = null;


            using (CantieriServicePortClient client = new ServiceReferenceCantieri.CantieriServicePortClient())
            {
                try
                {
                    CantiereTR4[] cantiereTR4;
                    if (!String.IsNullOrEmpty(numeroNotifica))
                    {
                        Console.WriteLine("-- INIZIO RECUPERO CANTIERI: notifica {0} --", numeroNotifica);
                        prg.LogLight(string.Empty);
                        prg.LogLight(string.Format("-- INIZIO RECUPERO CANTIERI: notifica {0} --", numeroNotifica));

                        client.ricercaCantieriTR4(st, ref intest,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            numeroNotifica,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            null,
                                            null,
                                            new String[0],
                                            out cantiereTR4);


                        Console.WriteLine("-- CANTIERI RECUPERATI: {0}", cantiereTR4.Length);
                        int totImprese = 0;
                        int totIndirizzi = 0;
                        int totPersone = 0;
                        int totCantieri = 0;
                        for (int i = 0; i < cantiereTR4.Length; i++)
                        {
                            int numNot = 0;
                            numNot = CheckNumeroNotifica(cantiereTR4[i].nrNotifica);
                            if (numNot == 0)
                            {
                                System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                                using (SqlConnection conn = new SqlConnection(constr.ToString()))
                                {
                                    conn.Open();
                                    using (SqlTransaction tran = conn.BeginTransaction())
                                    {
                                        try
                                        {
                                            #region Carico i cantieri
                                            //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                                            InsertCantiere(cantiereTR4[i], tran, conn);
                                            totCantieri = totCantieri + 1;
                                            #endregion

                                            #region Recupero e carico le imprese per ogni cantere
                                            ImpresaCantTR4[] imprese;

                                            //Console.WriteLine("-- RECUPERO LE IMPRESE PER IL CANTIERE {0}--", i+1);
                                            client.ricercaImpreseTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out imprese);
                                            //Console.WriteLine("-- IMPRESE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, imprese.Length);
                                            totImprese = totImprese + imprese.Length;

                                            if (imprese.Length > 0)
                                            {
                                                for (int y = 0; y < imprese.Length; y++)
                                                {
                                                    //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                                                    InsertImpresa(imprese[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                }
                                            }
                                            #endregion

                                            #region Recupero e carico gli indirizzi per ogni cantiere
                                            IndirizzoCantTR4[] indirizzi;

                                            //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
                                            client.ricercaIndirizziTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out indirizzi);
                                            //Console.WriteLine("-- INDIRIZZI RECUPERATI PER IL CANTIERE {0}: {1}", i + 1, indirizzi.Length);
                                            totIndirizzi = totIndirizzi + indirizzi.Length;

                                            if (indirizzi.Length > 0)
                                            {
                                                for (int y = 0; y < indirizzi.Length; y++)
                                                {
                                                    //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                    InsertIndirizzo(indirizzi[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                }
                                            }
                                            #endregion

                                            #region Recupero e carico le persone per ogni cantiere
                                            PersonaCantTR4[] persone;

                                            //Console.WriteLine("-- RECUPERO LE PERSONE PER IL CANTIERE {0}--", i + 1);
                                            client.ricercaPersoneTR4(st, ref intest, cantiereTR4[i].nrNotifica, out persone);
                                            //Console.WriteLine("-- PERSONE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, persone.Length);
                                            totPersone = totPersone + persone.Length;

                                            if (persone.Length > 0)
                                            {
                                                for (int y = 0; y < persone.Length; y++)
                                                {
                                                    //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                    InsertPersona(persone[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                }
                                            }
                                            #endregion
                                            //Console.WriteLine(" ");
                                            tran.Commit();
                                        }
                                        catch (Exception exc)
                                        {
                                            tran.Rollback();
                                            prg.Log(string.Format("ERRORE INSERIMENTO NOTIFICA {0} : \n {2}", cantiereTR4[i].nrNotifica, exc));
                                            prg.LogLight(string.Format("ERRORE INSERIMENTO NOTIFICA {0}", cantiereTR4[i].nrNotifica));
                                            //InsertErrore(cantiereTR4[i].nrNotifica, null, 0, null);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("-- NOTIFICA {0} GIA' PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica);
                            }

                        }
                        Console.WriteLine("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, totCantieri);
                        Console.WriteLine(" ");
                        prg.LogLight(string.Format("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, totCantieri));
                        prg.LogLight(string.Empty);
                    }

                }
                catch (Exception exc)
                {

                    prg.Log(String.Format("ERRORE per notifica {0} \n {1}", numeroNotifica, exc));
                    prg.LogLight(String.Format("ERRORE per notifica {0}", numeroNotifica));
                    prg.LogLight(string.Empty);
                    //InsertErrore(numeroNotifica, null, 0, null);
                }
            }
        }


        public void UpdateNew(SecurityType st, Intestazione intest, DateTime dataInizio, DateTime dataFine, String numeroNotifica, DateTime dataInizioAggiornamento, DateTime dataFineAggiornamento)
        {
            Program prg = new Program();

            //prg.Log("-- INIZIO PROCEDURA AGGIORNAMENTO --");
            //prg.LogLight("-- INIZIO PROCEDURA AGGIORNAMENTO --");

            DateTime? AggiornamentoInizio = dataInizioAggiornamento;
            DateTime? AggiornamentoFine = dataFineAggiornamento;


            if (dataInizioAggiornamento == DateTime.MinValue)
                AggiornamentoInizio = null;
            if (dataFineAggiornamento == DateTime.MinValue) 
                AggiornamentoFine = null;

            using (CantieriServicePortClient client = new ServiceReferenceCantieri.CantieriServicePortClient())
            {
                try
                {
                    CantiereTR4[] cantiereTR4;
              
                    Console.WriteLine("-- INIZIO RECUPERO CANTIERI  Giorno: {0} --", dataInizio);

                        client.ricercaCantieriTR4(st, ref intest,
                            null,
                            null,
                            null,
                            string.Empty,
                            numeroNotifica,
                            dataInizio,
                            dataInizio,
                            null,
                            null,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            string.Empty,
                            string.Empty,
                            string.Empty,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            string.Empty,
                            AggiornamentoInizio,
                            AggiornamentoInizio,
                            new String[0],
                            out cantiereTR4);


                    //Console.WriteLine("-- CANTIERI RECUPERATI: {0}", cantiereTR4.Length);

                    int totImprese = 0;
                    int totIndirizzi = 0;
                    int totPersone = 0;
                    for (int i = 0; i < cantiereTR4.Length; i++)
                    {
                        int numNot = 0;
                        numNot = CheckNumeroNotifica(cantiereTR4[i].nrNotifica);
                        if (numNot != 0)
                        {
                            Console.WriteLine("-- REINSERIMENTO NOTIFICA {0} --", cantiereTR4[i].nrNotifica);
                            System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                            using (SqlConnection conn = new SqlConnection(constr.ToString()))
                            {
                                conn.Open();
                                using (SqlTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        #region Aggiorno il cantiere
                                        //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                                        UpdateCantieri(cantiereTR4[i], tran, conn);
                                        #endregion

                                        #region Cancello Imprese, Indirizzi e Persone con specifico numeroNotifica
                                        DeleteNotificaByNumeroNotifica(cantiereTR4[i].nrNotifica, tran, conn);
                                        #endregion

                                        #region Recupero e carico le imprese per ogni cantere
                                        ImpresaCantTR4[] imprese;

                                        //Console.WriteLine("-- RECUPERO LE IMPRESE PER IL CANTIERE {0}--", i+1);
                                        client.ricercaImpreseTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out imprese);
                                        //Console.WriteLine("-- IMPRESE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, imprese.Length);
                                        totImprese = totImprese + imprese.Length;

                                        if (imprese.Length > 0)
                                        {
                                            for (int y = 0; y < imprese.Length; y++)
                                            {
                                                //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                                                InsertImpresa(imprese[y], cantiereTR4[i].nrNotifica, tran, conn);
                                            }
                                        }
                                        #endregion

                                        #region Recupero e carico gli indirizzi per ogni cantiere
                                        IndirizzoCantTR4[] indirizzi;

                                        //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
                                        client.ricercaIndirizziTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out indirizzi);
                                        //Console.WriteLine("-- INDIRIZZI RECUPERATI PER IL CANTIERE {0}: {1}", i + 1, indirizzi.Length);
                                        totIndirizzi = totIndirizzi + indirizzi.Length;

                                        if (indirizzi.Length > 0)
                                        {
                                            for (int y = 0; y < indirizzi.Length; y++)
                                            {
                                                //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                InsertIndirizzo(indirizzi[y], cantiereTR4[i].nrNotifica, tran, conn);
                                            }
                                        }
                                        #endregion

                                        #region Recupero e carico le persone per ogni cantiere
                                        PersonaCantTR4[] persone;

                                        //Console.WriteLine("-- RECUPERO LE PERSONE PER IL CANTIERE {0}--", i + 1);
                                        client.ricercaPersoneTR4(st, ref intest, cantiereTR4[i].nrNotifica, out persone);
                                        //Console.WriteLine("-- PERSONE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, persone.Length);
                                        totPersone = totPersone + persone.Length;

                                        if (persone.Length > 0)
                                        {
                                            for (int y = 0; y < persone.Length; y++)
                                            {
                                                //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                InsertPersona(persone[y], cantiereTR4[i].nrNotifica, tran, conn);
                                            }
                                        }
                                        #endregion

                                        tran.Commit();
                                    }
                                    catch (Exception exc)
                                    {
                                        tran.Rollback();
                                        prg.Log(string.Format("ERRORE AGGIORNAMENTO DEL {0} PER LA NOTIFICA {0} DEL GIORNO {1}: \n {2}", dataInizioAggiornamento, cantiereTR4[i].nrNotifica, dataInizio, exc));
                                        prg.LogLight(string.Format("ERRORE AGGIORNAMENTO DEL {0} PER LA NOTIFICA {0} DEL GIORNO {1}", dataInizioAggiornamento, cantiereTR4[i].nrNotifica, dataInizio));
                                        //InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 1, dataInizioAggiornamento);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("-- NOTIFICA {0} DEL GIORNO {1} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica, dataInizio);
                            prg.Log(string.Format("-- NOTIFICA {0} DEL GIORNO {1} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica, dataInizio));
                            prg.LogLight(string.Format("-- NOTIFICA {0} DEL GIORNO {1} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica, dataInizio));
                            //InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 0, DateTime.MaxValue);
                        }
                    }
                    Console.WriteLine("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, cantiereTR4.Length);
                    Console.WriteLine(" ");
                    prg.LogLight(string.Format("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone,cantiereTR4.Length));
                    prg.LogLight(string.Empty);
                }
                catch (Exception exc)
                {
                    prg.Log(String.Format("ERRORE per aggiornamento {0} del giorno {1} \n {2}", dataInizioAggiornamento, dataInizio, exc));
                    prg.LogLight(String.Format("ERRORE per aggiornamento {0} del giorno {1}", dataInizioAggiornamento, dataInizio));
                    prg.LogLight(string.Empty);
                    //InsertErrore(numeroNotifica, dataInizio, 1, dataInizioAggiornamento);
                }
            }
        }

        public void UpdateNewSingolo(SecurityType st, Intestazione intest, String numeroNotifica)
        {
            Program prg = new Program();

            using (CantieriServicePortClient client = new ServiceReferenceCantieri.CantieriServicePortClient())
            {
                try
                {
                    CantiereTR4[] cantiereTR4;

                    Console.WriteLine("-- INIZIO RECUPERO CANTIERI  Notifica: {0} --", numeroNotifica);

                    client.ricercaCantieriTR4(st, ref intest,
                        null,
                        null,
                        null,
                        string.Empty,
                        numeroNotifica,
                        null,
                        null,
                        null,
                        null,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        string.Empty,
                        null,
                        null,
                        new String[0],
                        out cantiereTR4);


                    //Console.WriteLine("-- CANTIERI RECUPERATI: {0}", cantiereTR4.Length);

                    int totImprese = 0;
                    int totIndirizzi = 0;
                    int totPersone = 0;
                    for (int i = 0; i < cantiereTR4.Length; i++)
                    {
                        int numNot = 0;
                        numNot = CheckNumeroNotifica(cantiereTR4[i].nrNotifica);
                        if (numNot != 0)
                        {
                            Console.WriteLine("-- REINSERIMENTO NOTIFICA {0} --", cantiereTR4[i].nrNotifica);
                            System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                            using (SqlConnection conn = new SqlConnection(constr.ToString()))
                            {
                                conn.Open();
                                using (SqlTransaction tran = conn.BeginTransaction())
                                {
                                    try
                                    {
                                        #region Aggiorno il cantiere
                                        //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                                        UpdateCantieri(cantiereTR4[i], tran, conn);
                                        #endregion

                                        #region Cancello Imprese, Indirizzi e Persone con specifico numeroNotifica
                                        DeleteNotificaByNumeroNotifica(cantiereTR4[i].nrNotifica, tran, conn);
                                        #endregion

                                        #region Recupero e carico le imprese per ogni cantere
                                        ImpresaCantTR4[] imprese;

                                        //Console.WriteLine("-- RECUPERO LE IMPRESE PER IL CANTIERE {0}--", i+1);
                                        client.ricercaImpreseTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out imprese);
                                        //Console.WriteLine("-- IMPRESE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, imprese.Length);
                                        totImprese = totImprese + imprese.Length;

                                        if (imprese.Length > 0)
                                        {
                                            for (int y = 0; y < imprese.Length; y++)
                                            {
                                                //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                                                InsertImpresa(imprese[y], cantiereTR4[i].nrNotifica, tran, conn);
                                            }
                                        }
                                        #endregion

                                        #region Recupero e carico gli indirizzi per ogni cantiere
                                        IndirizzoCantTR4[] indirizzi;

                                        //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
                                        client.ricercaIndirizziTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out indirizzi);
                                        //Console.WriteLine("-- INDIRIZZI RECUPERATI PER IL CANTIERE {0}: {1}", i + 1, indirizzi.Length);
                                        totIndirizzi = totIndirizzi + indirizzi.Length;

                                        if (indirizzi.Length > 0)
                                        {
                                            for (int y = 0; y < indirizzi.Length; y++)
                                            {
                                                //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                InsertIndirizzo(indirizzi[y], cantiereTR4[i].nrNotifica, tran, conn);
                                            }
                                        }
                                        #endregion

                                        #region Recupero e carico le persone per ogni cantiere
                                        PersonaCantTR4[] persone;

                                        //Console.WriteLine("-- RECUPERO LE PERSONE PER IL CANTIERE {0}--", i + 1);
                                        client.ricercaPersoneTR4(st, ref intest, cantiereTR4[i].nrNotifica, out persone);
                                        //Console.WriteLine("-- PERSONE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, persone.Length);
                                        totPersone = totPersone + persone.Length;

                                        if (persone.Length > 0)
                                        {
                                            for (int y = 0; y < persone.Length; y++)
                                            {
                                                //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                InsertPersona(persone[y], cantiereTR4[i].nrNotifica, tran, conn);
                                            }
                                        }
                                        #endregion

                                        tran.Commit();
                                    }
                                    catch (Exception exc)
                                    {
                                        tran.Rollback();
                                        prg.Log(string.Format("ERRORE AGGIORNAMENTO PER LA NOTIFICA {0} : \n {1}", cantiereTR4[i].nrNotifica, exc));
                                        prg.LogLight(string.Format("ERRORE AGGIORNAMENTO PER LA NOTIFICA {0} ", cantiereTR4[i].nrNotifica));
                                        //InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 1, dataInizioAggiornamento);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("-- NOTIFICA {0} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica);
                            prg.Log(string.Format("-- NOTIFICA {0} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica));
                            prg.LogLight(string.Format("-- NOTIFICA {0} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica));
                            //InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 0, DateTime.MaxValue);
                        }
                    }
                    Console.WriteLine("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, cantiereTR4.Length);
                    Console.WriteLine(" ");
                    prg.LogLight(string.Format("-- INSERITI {3} CANTIERI, {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, cantiereTR4.Length));
                    prg.LogLight(string.Empty);
                }
                catch (Exception)
                {
                    prg.Log(String.Format("ERRORE aggiornamento notifica {0}", numeroNotifica));
                    prg.LogLight(String.Format("ERRORE aggiornamento notifica {0}", numeroNotifica));
                    prg.LogLight(string.Empty);
                    //InsertErrore(numeroNotifica, dataInizio, 1, dataInizioAggiornamento);
                }
            }
        }



        public void Recovery(SecurityType st, Intestazione intest)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariSelectRecoveryDate", conn))
                {
                    //command.Parameters.AddWithValue("@tipo", tipo);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            Program prg = new Program();
                            while (reader.Read())
                            {
                                int numNot;
                                if (Convert.ToInt32(reader[2]) == 0)
                                {
                                    if (System.String.IsNullOrEmpty(reader[1].ToString()))
                                    {
                                        numNot = CheckDay(Convert.ToDateTime(reader[0]));
                                        if (numNot == 0)
                                        {
                                            Console.WriteLine("-- RECUPERO GIORNO {0} --", reader[0]);
                                            LoadNew(st, intest, Convert.ToDateTime(reader[0]), Convert.ToDateTime(reader[0]), string.Empty);
                                        }
                                        else
                                        {
                                            Console.WriteLine("-- GIORNO {0} PRESENTE --", reader[0]);
                                            prg.LogLight(string.Format("-- GIORNO {0} PRESENTE --", reader[0]));
                                            DeleteError(Convert.ToDateTime(reader[0]), null, 0);
                                            Console.WriteLine("-- GIORNO {0} ELIMINATO DAGLI ERRORI --", reader[0]);
                                        }
                                    }
                                    else
                                    {
                                        numNot = CheckNumeroNotifica(reader[1].ToString());
                                        if (numNot == 0)
                                        {
                                            Console.WriteLine("-- RECUPERO GIORNO {0} --", reader[0]);
                                            LoadNew(st, intest, Convert.ToDateTime(reader[0]), Convert.ToDateTime(reader[0]), reader[1].ToString());
                                        }
                                        else
                                        {
                                            Console.WriteLine("-- GIORNO {0} PRESENTE --", reader[0]);
                                            prg.LogLight(string.Format("-- GIORNO {0} PRESENTE --", reader[0]));
                                            DeleteError(Convert.ToDateTime(reader[0]), reader[1].ToString(), 0);
                                            Console.WriteLine("-- GIORNO {0} ELIMINATO DAGLI ERRORI --", reader[0]);
                                        }

                                    }
                                }
                                else
                                {
                                    if (System.String.IsNullOrEmpty(reader[1].ToString()))
                                    {
                                        Console.WriteLine("-- RECUPERO AGGIORNAMENTO {0} DEL {1} --", reader[3], reader[0]);
                                        UpdateNew(st, intest, Convert.ToDateTime(reader[0]), Convert.ToDateTime(reader[0]), string.Empty, Convert.ToDateTime(reader[3]), Convert.ToDateTime(reader[3]));
                                    }
                                    else
                                    {
                                        Console.WriteLine("-- RECUPERO AGGIORNAMENTO {0} DELLA NOTIFICA {1} DEL {2} --", reader[3], reader[1], reader[0]);
                                        UpdateNew(st, intest, Convert.ToDateTime(reader[0]), Convert.ToDateTime(reader[0]), reader[1].ToString(), Convert.ToDateTime(reader[3]), Convert.ToDateTime(reader[3]));
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        
        public void InsertCantiere(CantiereTR4 cantiere, SqlTransaction tran, SqlConnection conn)
        {
            

                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariCantiereInsert", conn, tran))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@numeroNotifica", cantiere.nrNotifica);
                    command.Parameters.AddWithValue("@protocolloNotifica", cantiere.nrProtocollo);

                        DateTime dataNotifica = Convert.ToDateTime(cantiere.dtComunicazione);
                        if (dataNotifica.Year < 1900 || dataNotifica.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                        {
                            command.Parameters.AddWithValue("@dataComunicazioneNotifica", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@dataComunicazioneNotifica", Convert.ToDateTime(cantiere.dtComunicazione));
                        }
                        //command.Parameters.AddWithValue("@dataComunicazioneNotifica", Convert.ToDateTime(cantiere.dtComunicazione));

                        if ((cantiere.dtAggNotifica != null) && (Convert.ToString(cantiere.dtAggNotifica) != ""))
                        {
                            DateTime dataAggiornamento = Convert.ToDateTime(cantiere.dtAggNotifica);
                            if (dataAggiornamento.Year < 1900 || dataAggiornamento.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                            {
                                command.Parameters.AddWithValue("@dataAggiornamentoNotifica", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@dataAggiornamentoNotifica", Convert.ToDateTime(cantiere.dtAggNotifica));
                            }
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@dataAggiornamentoNotifica", DBNull.Value);
                        }

                    command.Parameters.AddWithValue("@numeroLavoratoriAutonomi", cantiere.nrLavAutonomi);
                    command.Parameters.AddWithValue("@numeroImprese", cantiere.nrImprese);
                    command.Parameters.AddWithValue("@idTipoContratto", cantiere.cdContrattoAppalto);
                    command.Parameters.AddWithValue("@descrizioneTipoContratto", cantiere.cdContrattoCIG);
                    command.Parameters.AddWithValue("@idContratto", cantiere.cdContrattoCUI);
                    command.Parameters.AddWithValue("@idTipoOpera", cantiere.cdTipoOpera);
                    command.Parameters.AddWithValue("@descrizioneTipoOpera", cantiere.dsTipoOpera);
                    command.Parameters.AddWithValue("@descrizioneTipoCategoria", cantiere.dsTipoCategoria);
                    command.Parameters.AddWithValue("@descrizioneTipoTipologia", cantiere.dsTipoTipologia);
                    command.Parameters.AddWithValue("@descrizioneAltraCategoria", cantiere.dsAltraCategoria);
                    command.Parameters.AddWithValue("@descrizioneAltraTipologia", cantiere.dsAltraTipologia);
                    command.Parameters.AddWithValue("@ammontareComplessivo", cantiere.ammontareComplessivo);
                    command.Parameters.AddWithValue("@art9011", Convert.ToByte(cantiere.flArt90C11));
                    command.Parameters.AddWithValue("@art9011B", Convert.ToByte(cantiere.flArt99C1B));
                    command.Parameters.AddWithValue("@art9011C", Convert.ToByte(cantiere.flArt99C1C));
                    command.Parameters.AddWithValue("@idCantiereRegione", cantiere.idagp);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2601)
                        {
                            Program var = new Program();
                            var.Log(string.Format("Eccezione sql per la notifica numero {0} di tipo 0: \n {1}",cantiere.nrNotifica,exc.Message));
                        } 

                    }
                }
            

        }

        public void UpdateCantieri(CantiereTR4 cantiere, SqlTransaction tran, SqlConnection conn)
        {
            using (SqlCommand command = new SqlCommand("[dbo].[Marco_NotifichePreliminariCantiereUpdate]",conn,tran))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@numeroNotifica", cantiere.nrNotifica);
                command.Parameters.AddWithValue("@protocolloNotifica", cantiere.nrProtocollo);

                DateTime dataNotifica = Convert.ToDateTime(cantiere.dtComunicazione);
                if (dataNotifica.Year < 1900 || dataNotifica.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                {
                    command.Parameters.AddWithValue("@dataComunicazioneNotifica", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("@dataComunicazioneNotifica", Convert.ToDateTime(cantiere.dtComunicazione));
                }
                //command.Parameters.AddWithValue("@dataComunicazioneNotifica", Convert.ToDateTime(cantiere.dtComunicazione));

                if ((cantiere.dtAggNotifica != null) && (Convert.ToString(cantiere.dtAggNotifica) != ""))
                {
                    DateTime dataAggiornamento = Convert.ToDateTime(cantiere.dtAggNotifica);
                    if (dataAggiornamento.Year < 1900 || dataAggiornamento.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                    {
                        command.Parameters.AddWithValue("@dataAggiornamentoNotifica", DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@dataAggiornamentoNotifica", dataAggiornamento);
                    }
                }
                else
                {
                    command.Parameters.AddWithValue("@dataAggiornamentoNotifica", DBNull.Value);
                }

                command.Parameters.AddWithValue("@numeroLavoratoriAutonomi", cantiere.nrLavAutonomi);
                command.Parameters.AddWithValue("@numeroImprese", cantiere.nrImprese);
                command.Parameters.AddWithValue("@idTipoContratto", cantiere.cdContrattoAppalto);
                command.Parameters.AddWithValue("@descrizioneTipoContratto", cantiere.cdContrattoCIG);
                command.Parameters.AddWithValue("@idContratto", cantiere.cdContrattoCUI);
                command.Parameters.AddWithValue("@idTipoOpera", cantiere.cdTipoOpera);
                command.Parameters.AddWithValue("@descrizioneTipoOpera", cantiere.dsTipoOpera);
                command.Parameters.AddWithValue("@descrizioneTipoCategoria", cantiere.dsTipoCategoria);
                command.Parameters.AddWithValue("@descrizioneTipoTipologia", cantiere.dsTipoTipologia);
                command.Parameters.AddWithValue("@descrizioneAltraCategoria", cantiere.dsAltraCategoria);
                command.Parameters.AddWithValue("@descrizioneAltraTipologia", cantiere.dsAltraTipologia);
                command.Parameters.AddWithValue("@ammontareComplessivo", cantiere.ammontareComplessivo);
                command.Parameters.AddWithValue("@art9011", Convert.ToByte(cantiere.flArt90C11));
                command.Parameters.AddWithValue("@art9011B", Convert.ToByte(cantiere.flArt99C1B));
                command.Parameters.AddWithValue("@art9011C", Convert.ToByte(cantiere.flArt99C1C));
                command.Parameters.AddWithValue("@idCantiereRegione", cantiere.idagp);

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException exc)
                {
                    if (exc.Number == 2601)
                    {
                        Program var = new Program();
                        var.Log(string.Format("Eccezione sql per la notifica numero {0} di tipo 1: \n {1}", cantiere.nrNotifica, exc.Message));
                        //var.LogLight(string.Format("UPDATE: Eccezione sql per la notifica numero {0}", cantiere.nrNotifica));
                    }

                }
            }
        }

        public void InsertImpresa(ImpresaCantTR4 impresa, string numeroNotifica, SqlTransaction tran, SqlConnection conn)
        {
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariImpresaInsert", conn,tran))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    command.Parameters.AddWithValue("@ragioneSociale", impresa.ragioneSociale);
                    command.Parameters.AddWithValue("@codiceFiscale", impresa.codFiscaleId);
                    command.Parameters.AddWithValue("@piva", impresa.PivaImpresaAssoc);
                    command.Parameters.AddWithValue("@indirizzoSede", impresa.indirizzoSede);
                    command.Parameters.AddWithValue("@comune", impresa.dsComune);
                    command.Parameters.AddWithValue("@provincia", impresa.dsSiglaProvincia);
                    command.Parameters.AddWithValue("@nazione", impresa.nazione);
                    command.Parameters.AddWithValue("@CAP", impresa.zipCode);
                    command.Parameters.AddWithValue("@idTipoIncaricoAppalto", impresa.cdTipoIncaricoAppalto);
                    command.Parameters.AddWithValue("@descrizioneTipoIncaricoAppalto", impresa.dsTipoIncaricoAppalto);


                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2601)
                        {
                            Program var = new Program();
                            var.Log(string.Format("Eccezione sql per la notifica numero {0}: {1}", numeroNotifica, exc.Message));
                            //var.LogLight(string.Format("INSERT IMPRESA: Eccezione sql per la notifica numero {0}", numeroNotifica));
                        }

                    }
                }
        }

        public void InsertIndirizzo(IndirizzoCantTR4 indirizzo, string numeroNotifica, SqlTransaction tran, SqlConnection conn)
        {
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariIndirizzoInsert", conn,tran))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    command.Parameters.AddWithValue("@indirizzo", indirizzo.indirizzo);
                    command.Parameters.AddWithValue("@comune", indirizzo.dsComune);
                    command.Parameters.AddWithValue("@CAP", indirizzo.CAPindirCantiere);
                    command.Parameters.AddWithValue("@provincia", indirizzo.dsProvincia);

                        DateTime dataNescita = Convert.ToDateTime(indirizzo.dtInizioLavori);
                        if (dataNescita.Year < 1900 || dataNescita.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                        {
                            command.Parameters.AddWithValue("@dataInizioLavori", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@dataInizioLavori", Convert.ToDateTime(indirizzo.dtInizioLavori));
                        }
                        //command.Parameters.AddWithValue("@dataInizioLavori", Convert.ToDateTime(indirizzo.dtInizioLavori));

                    command.Parameters.AddWithValue("@durataLavori", indirizzo.durataLavori);
                    command.Parameters.AddWithValue("@descrizioneDurataLavori", indirizzo.dsDurataLavori);
                    command.Parameters.AddWithValue("@idArea", indirizzo.cdArea);
                    command.Parameters.AddWithValue("@descrizioneArea", indirizzo.dsArea);
                    command.Parameters.AddWithValue("@noteArea", indirizzo.areaNote);
                    command.Parameters.AddWithValue("@numeroMassimoLavoratori", indirizzo.nrMassimoLavoratori);
                    command.Parameters.AddWithValue("@idIndirizzoRegione", indirizzo.idagp);

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2601)
                        {
                            Program var = new Program();
                            var.Log(string.Format("Eccezione sql per la notifica numero {0}: {1}", numeroNotifica, exc.Message));
                            //var.LogLight(string.Format("INSERT INDIRIZZO: Eccezione sql per la notifica numero {0}", numeroNotifica));
                        }

                    }
                }
        }

        public void InsertPersona(PersonaCantTR4 persona, string numeroNotifica, SqlTransaction tran, SqlConnection conn)
        {
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariPersonaInsert", conn,tran))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    command.Parameters.AddWithValue("@nome", persona.nome);
                    command.Parameters.AddWithValue("@cognome", persona.cognome);
                    command.Parameters.AddWithValue("@codiceFiscale", persona.codFiscaleDocumento);
                    
                    command.Parameters.AddWithValue("@regione", persona.regione);
                    command.Parameters.AddWithValue("@nazione", persona.nazione);
                    command.Parameters.AddWithValue("@indirizzo", persona.indirizzo);
                    command.Parameters.AddWithValue("@comune", persona.dsComune);
                    command.Parameters.AddWithValue("@provincia", persona.dsProvincia);
                    command.Parameters.AddWithValue("@CAP", persona.zipCode);
                    command.Parameters.AddWithValue("@ruolo", persona.ruolo);
                    

                    if (String.IsNullOrWhiteSpace(persona.dtNascita))
                    {
                        command.Parameters.AddWithValue("@dataNascita", DBNull.Value);
                    }
                    else
                    {
                        DateTime dataNascita = DateTime.ParseExact(persona.dtNascita, "dd/MM/yyyy", null);
                        if (dataNascita.Year < 1900 || dataNascita.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                        {
                            command.Parameters.AddWithValue("@dataNascita", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@dataNascita", Convert.ToDateTime(persona.dtNascita));
                        }
                    }

                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2601)
                        {
                            Program var = new Program();
                            var.Log(string.Format("Eccezione sql per la notifica numero {0}: {1}", numeroNotifica, exc.Message));
                            //var.LogLight(string.Format("INSERT PERSONA: Eccezione sql per la notifica numero {0}", numeroNotifica));
                        }

                    }
                }
        }
        
        public void InsertErrore(string numeroNotifica, DateTime? dataNotifica, int tipo, DateTime? dataAggiornamento)
        {
            int numNot;
            if (string.IsNullOrEmpty(numeroNotifica))
            {
                numNot = CheckDayErrore(dataNotifica, tipo);
            }
            else
            {
                numNot = CheckNumeroNotificaErrore(numeroNotifica, tipo);
            }

            if (numNot == 0)
            {
                //System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                using (SqlConnection conn = new SqlConnection(constr.ToString()))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariErroreInsert", conn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        if (string.IsNullOrEmpty(numeroNotifica))
                        {
                            command.Parameters.AddWithValue("@numeroNotifica", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                        }


                        DateTime? dataNot = (dataNotifica == null) ? null : dataNotifica;
                            command.Parameters.AddWithValue("@dataNotifica", dataNot);
                        

                        DateTime? dataAgg = (dataAggiornamento == null) ? null : dataAggiornamento;
                            command.Parameters.AddWithValue("@dataAggiornamento", dataAgg);

                        command.Parameters.AddWithValue("@tipo", tipo);

                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (SqlException exc)
                        {
                            if (exc.Number == 2601)
                            {
                                Program var = new Program();
                                var.Log(string.Format("Eccezione sql per errore notifica numero {0} di tipo {1}: \n {2}", numeroNotifica, tipo, exc.Message));
                                //var.LogLight(string.Format("INSERT ERRORE: Eccezione sql per errore notifica numero {0} di tipo {1}", numeroNotifica, tipo));
                            }

                        }
                    }
                }
            }
            else 
            {
                Console.WriteLine("-- ERRORE PRESENTE. NON INSERITO --");
                Console.WriteLine(" ");
                return;
            }
        }
        

        
        public int CheckDay(DateTime dataNotifica)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariCantiereSelectByDate", conn))
                {
                    SqlDataReader dr;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@dataNotifica", dataNotifica);
                    dr = command.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            return Convert.ToInt32(dr["numNotifiche"].ToString());
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    return 0;
                }
            }
        }

        public int CheckNumeroNotifica(String numeroNotifica)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariCantiereSelectByNumeroNotifica", conn))
                {
                    SqlDataReader dr;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    dr = command.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            return Convert.ToInt32(dr["numNotifiche"].ToString());
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    return 0;
                }
            }
        }

        public int CheckNumeroNotificaEdilconnect(String numeroNotifica)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariEdilconnectSelectByNumeroNotifica", conn))
                {
                    SqlDataReader dr;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    dr = command.ExecuteReader();
                    int numNot = 0;
                    try
                    {
                        while (dr.Read())
                        {
                            numNot = Convert.ToInt32(dr["numNotifiche"].ToString());
                        }
                        dr.Close();
                        return numNot;
                    }
                    catch
                    {
                        return 0;
                    }
                }
            }
        }

        public void DeleteError(DateTime dataNotifica, String numeroNotifica, int tipo)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                if (string.IsNullOrEmpty(numeroNotifica))
                {
                    numeroNotifica = string.Empty;
                }
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariErroreDelete", conn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@dataNotifica", dataNotifica);
                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    command.Parameters.AddWithValue("@tipo", tipo);
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2601)
                        {
                            Program var = new Program();
                            var.Log(string.Format("Eccezione sql per delete errore notifica con data {0} di tipo {1}: {2}", dataNotifica, tipo, exc.Message));
                            //var.LogLight(string.Format("DELETE ERROR: Eccezione sql per delete errore notifica {0} con data {1} di tipo {2}", numeroNotifica, dataNotifica, tipo));
                        }
                    }
                }
            }
        }

        public int CheckDayErrore(DateTime? dataNotifica, int tipo)
        {            
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariErroreSelectByDate", conn))
                {
                    SqlDataReader dr;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    if (dataNotifica == null)
                    {
                        command.Parameters.AddWithValue("@dataNotifica", DBNull.Value);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@dataNotifica", dataNotifica);
                    }
                    command.Parameters.AddWithValue("@tipo", tipo);
                    dr = command.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            return Convert.ToInt32(dr["numNotifiche"].ToString());
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    return 0;
                }
            }
        }
        
        public int CheckNumeroNotificaErrore(String numeroNotifica, int tipo)
        {
            using (SqlConnection conn = new SqlConnection(constr.ToString()))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariErroreSelectByNumeroNotifica", conn))
                {
                    SqlDataReader dr;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);
                    command.Parameters.AddWithValue("@tipo", tipo);
                    dr = command.ExecuteReader();
                    try
                    {
                        while (dr.Read())
                        {
                            return Convert.ToInt32(dr["numNotifiche"].ToString());
                        }
                    }
                    finally
                    {
                        dr.Close();
                    }
                    return 0;
                }
            }
        }

        public void DeleteNotificaByNumeroNotifica(String numeroNotifica, SqlTransaction tran, SqlConnection conn)
        { 
            using (SqlCommand command = new SqlCommand("dbo.Marco_NotifichePreliminariDeleteByNumeroNotifica",conn,tran))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@numeroNotifica", numeroNotifica);

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException exc)
                {
                    if (exc.Number == 2601)
                    {
                        Program var = new Program();
                        var.Log(string.Format("Eccezione sql per la cancellazione della notifica numero {0}: {1}", numeroNotifica, exc.Message));
                        Console.WriteLine("Eccezione sql per la cancellazione della notifica numero {0}: {1}", numeroNotifica, exc.Message);
                    }

                }
            }
        }

        
        /* VECCHIO LOAD
public void Load(DateTime dataInizio, DateTime dataFine, String numeroNotifica)
{
    Program prg = new Program();
    //System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
    //Console.WriteLine(constr.ToString());
    prg.Log("-- INIZIO PROCEDURA --");

    if (String.IsNullOrEmpty(numeroNotifica))
        numeroNotifica = null;


    using (CantieriServicePortClient client = new ServiceReferenceCantieri.CantieriServicePortClient())
    {
        CantiereTR4[] cantiereTR4;
        SecurityType st = prg.CreateSecurityType();
        Intestazione intest = prg.CreateIntestazione();
        //DataProvider dataProviderCantieri = new DataProvider();

        //int numeroGiorni = (dataFine - dataInizio).Days;
        //for (int i = 0; i < numeroGiorni; i++)

        while (dataInizio <= dataFine)
        {
            try
            {
                if (String.IsNullOrEmpty(numeroNotifica))
                    Console.WriteLine("-- INIZIO RECUPERO CANTIERI: Giorno {0} --", dataInizio);
                else
                    Console.WriteLine("-- INIZIO RECUPERO CANTIERI: Giorno {0}, notifica {1} --", dataInizio,numeroNotifica);

                client.ricercaCantieriTR4(st, ref intest,
                                        null,
                                        null,
                                        null,
                                        string.Empty,
                                        numeroNotifica,
                                        dataInizio,
                                        dataInizio,
                                        null,
                                        null,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        string.Empty,
                                        string.Empty,
                                        string.Empty,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        null,
                                        string.Empty,
                                        null,
                                        null,
                                        new String[0],
                                        out cantiereTR4);


                Console.WriteLine("-- CANTIERI RECUPERATI: {0}", cantiereTR4.Length);
                int totImprese = 0;
                int totIndirizzi = 0;
                int totPersone = 0;
                for (int i = 0; i < cantiereTR4.Length; i++)
                {
                    int numNot = 0;
                    numNot = CheckNumeroNotifica(cantiereTR4[i].nrNotifica);
                    if (numNot == 0)
                    {
                        System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                        using (SqlConnection conn = new SqlConnection(constr.ToString()))
                        {
                            conn.Open();
                            using (SqlTransaction tran = conn.BeginTransaction())
                            {
                                try
                                {
                                    #region Carico i cantieri
                                    //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                                    InsertCantiere(cantiereTR4[i], tran, conn);
                                    #endregion

                                    #region Recupero e carico le imprese per ogni cantere
                                    ImpresaCantTR4[] imprese;

                                    //Console.WriteLine("-- RECUPERO LE IMPRESE PER IL CANTIERE {0}--", i+1);
                                    client.ricercaImpreseTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out imprese);
                                    //Console.WriteLine("-- IMPRESE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, imprese.Length);
                                    totImprese = totImprese + imprese.Length;

                                    if (imprese.Length > 0)
                                    {
                                        for (int y = 0; y < imprese.Length; y++)
                                        {
                                            //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                                            InsertImpresa(imprese[y], cantiereTR4[i].nrNotifica, tran, conn);
                                        }
                                    }
                                    #endregion

                                    #region Recupero e carico gli indirizzi per ogni cantiere
                                    IndirizzoCantTR4[] indirizzi;

                                    //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
                                    client.ricercaIndirizziTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out indirizzi);
                                    //Console.WriteLine("-- INDIRIZZI RECUPERATI PER IL CANTIERE {0}: {1}", i + 1, indirizzi.Length);
                                    totIndirizzi = totIndirizzi + indirizzi.Length;

                                    if (indirizzi.Length > 0)
                                    {
                                        for (int y = 0; y < indirizzi.Length; y++)
                                        {
                                            //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                            InsertIndirizzo(indirizzi[y], cantiereTR4[i].nrNotifica, tran, conn);
                                        }
                                    }
                                    #endregion

                                    #region Recupero e carico le persone per ogni cantiere
                                    PersonaCantTR4[] persone;

                                    //Console.WriteLine("-- RECUPERO LE PERSONE PER IL CANTIERE {0}--", i + 1);
                                    client.ricercaPersoneTR4(st, ref intest, cantiereTR4[i].nrNotifica, out persone);
                                    //Console.WriteLine("-- PERSONE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, persone.Length);
                                    totPersone = totPersone + persone.Length;

                                    if (persone.Length > 0)
                                    {
                                        for (int y = 0; y < persone.Length; y++)
                                        {
                                            //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                            InsertPersona(persone[y], cantiereTR4[i].nrNotifica, tran, conn);
                                        }
                                    }
                                    #endregion
                                    //Console.WriteLine(" ");
                                    tran.Commit();
                                }
                                catch (Exception exc)
                                {
                                    tran.Rollback();
                                    prg.Log(string.Format("ERRORE INSERIMENTO NOTIFICA {0} DEL GIORNO {1}: \n {2}", cantiereTR4[i].nrNotifica, dataInizio, exc));
                                    InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 0, DateTime.MaxValue);
                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("-- NOTIFICA {0} GIA' PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica);
                    }

                }
                Console.WriteLine("-- INSERITE {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone);
                Console.WriteLine(" ");

            }
            catch (Exception exc)
            {

                prg.Log(String.Format("ERRORE per giorno {0} \n {1}", dataInizio, exc));
                InsertErrore(numeroNotifica, dataInizio, 0, DateTime.MaxValue);
            }

            dataInizio = dataInizio.AddDays(1);
        }
        prg.Log("-- FINE PROCEDURA --");
        Console.WriteLine(" ");
    }
}
*/

        /* VECCHIO UPDATE
        public void Update(DateTime dataInizioAggiornamento, DateTime dataFineAggiornamento)
        {
            Program prg = new Program();
            //System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
            //Console.WriteLine(constr.ToString());
            prg.Log("-- INIZIO PROCEDURA AGGIORNAMENTO --");

            using (CantieriServicePortClient client = new ServiceReferenceCantieri.CantieriServicePortClient())
            {
                CantiereTR4[] cantiereTR4;
                SecurityType st = prg.CreateSecurityType();
                Intestazione intest = prg.CreateIntestazione();
                //DataProvider dataProviderCantieri = new DataProvider();

                DateTime dataInizio = ReadDataInizioAggiornamento(); //DateTime.Today.AddYears(-2);//new DateTime(2013, 01, 01);
                //DateTime dataFine = new DateTime(anno, 01, 31);
                //*********************
                //DateTime dataInizio = new DateTime(2012, 08, 01);
                //DateTime dataFine = new DateTime(2012, 08, 01);
                //dataInizioAggiornamento = new DateTime(2012, 11, 04);
                //dataFineAggiornamento = new DateTime(2012, 11, 04);
                //*********************

                //int numeroGiorni = (dataFine - dataInizio).Days;
                //for (int i = 0; i < numeroGiorni; i++)

                while (dataInizioAggiornamento <= dataFineAggiornamento)
                {
                    Console.WriteLine("-- INIZIO AGGIORNAMENTO CANTIERI  Giorno: {0} --", dataInizioAggiornamento);
                    while (dataInizio <= dataFineAggiornamento)
                    {
                        try
                        {
                            Console.WriteLine("-- INIZIO RECUPERO CANTIERI  Giorno: {0} --", dataInizio);

                            client.ricercaCantieriTR4(st, ref intest,
                                                    null,
                                                    null,
                                                    null,
                                                    string.Empty,
                                                    null,
                                                    dataInizio,
                                                    dataInizio,
                                                    null,
                                                    null,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    string.Empty,
                                                    string.Empty,
                                                    string.Empty,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    string.Empty,
                                                    dataInizioAggiornamento,
                                                    dataInizioAggiornamento,
                                                    new String[0],
                                                    out cantiereTR4);


                            Console.WriteLine("-- CANTIERI RECUPERATI: {0}", cantiereTR4.Length);
                            int totImprese = 0;
                            int totIndirizzi = 0;
                            int totPersone = 0;
                            for (int i = 0; i < cantiereTR4.Length; i++)
                            {
                                int numNot = 0;
                                numNot = CheckNumeroNotifica(cantiereTR4[i].nrNotifica);
                                if (numNot != 0)
                                {
                                    Console.WriteLine("-- AGGIORNAMENTO NOTIFICA {0} --", cantiereTR4[i].nrNotifica);
                                    System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];
                                    using (SqlConnection conn = new SqlConnection(constr.ToString()))
                                    {
                                        conn.Open();
                                        using (SqlTransaction tran = conn.BeginTransaction())
                                        {
                                            try
                                            {
                                                #region Aggiorno il cantiere
                                                //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                                                UpdateCantieri(cantiereTR4[i], tran, conn);
                                                #endregion

                                                #region Cancello Imprese, Indirizzi e Persone con specifico numeroNotifica
                                                DeleteNotificaByNumeroNotifica(cantiereTR4[i].nrNotifica, tran, conn);
                                                #endregion

                                                #region Recupero e carico le imprese per ogni cantere
                                                ImpresaCantTR4[] imprese;

                                                //Console.WriteLine("-- RECUPERO LE IMPRESE PER IL CANTIERE {0}--", i+1);
                                                client.ricercaImpreseTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out imprese);
                                                //Console.WriteLine("-- IMPRESE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, imprese.Length);
                                                totImprese = totImprese + imprese.Length;

                                                if (imprese.Length > 0)
                                                {
                                                    for (int y = 0; y < imprese.Length; y++)
                                                    {
                                                        //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                                                        InsertImpresa(imprese[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                    }
                                                }
                                                #endregion

                                                #region Recupero e carico gli indirizzi per ogni cantiere
                                                IndirizzoCantTR4[] indirizzi;

                                                //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
                                                client.ricercaIndirizziTR4(st, ref intest, cantiereTR4[i].nrNotifica, string.Empty, out indirizzi);
                                                //Console.WriteLine("-- INDIRIZZI RECUPERATI PER IL CANTIERE {0}: {1}", i + 1, indirizzi.Length);
                                                totIndirizzi = totIndirizzi + indirizzi.Length;

                                                if (indirizzi.Length > 0)
                                                {
                                                    for (int y = 0; y < indirizzi.Length; y++)
                                                    {
                                                        //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                        InsertIndirizzo(indirizzi[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                    }
                                                }
                                                #endregion

                                                #region Recupero e carico le persone per ogni cantiere
                                                PersonaCantTR4[] persone;

                                                //Console.WriteLine("-- RECUPERO LE PERSONE PER IL CANTIERE {0}--", i + 1);
                                                client.ricercaPersoneTR4(st, ref intest, cantiereTR4[i].nrNotifica, out persone);
                                                //Console.WriteLine("-- PERSONE RECUPERATE PER IL CANTIERE {0}: {1}", i + 1, persone.Length);
                                                totPersone = totPersone + persone.Length;

                                                if (persone.Length > 0)
                                                {
                                                    for (int y = 0; y < persone.Length; y++)
                                                    {
                                                        //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                                                        InsertPersona(persone[y], cantiereTR4[i].nrNotifica, tran, conn);
                                                    }
                                                }
                                                #endregion
                                                //Console.WriteLine(" ");
                                                tran.Commit();
                                            }
                                            catch (Exception exc)
                                            {
                                                tran.Rollback();
                                                prg.Log(string.Format("ERRORE AGGIORNAMENTO DEL {0} PER LA NOTIFICA {0} DEL GIORNO {1}: \n {2}", dataInizioAggiornamento, cantiereTR4[i].nrNotifica, dataInizio, exc));
                                                InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 1, dataInizioAggiornamento);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("-- NOTIFICA {0} DEL GIORNO {1} NON PRESENTE A SISTEMA --", cantiereTR4[i].nrNotifica, dataInizio);
                                    InsertErrore(cantiereTR4[i].nrNotifica, dataInizio, 0, DateTime.MaxValue);
                                }

                            }
                            Console.WriteLine("-- INSERITE {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone);
                            Console.WriteLine(" ");

                        }
                        catch (Exception exc)
                        {

                            prg.Log(String.Format("ERRORE PER AGGIORNAMENTO {0} DEL GIORNO {1} \n {2}", dataInizioAggiornamento, dataInizio, exc));
                            InsertErrore(null, dataInizio, 1, dataInizioAggiornamento);
                        }
                        UdpateUpdateDate(dataInizio, dataInizioAggiornamento);
                        dataInizio = dataInizio.AddDays(1);
                        
                    }
                    UdpateUpdateDate(dataInizio, dataInizioAggiornamento);
                    dataInizio = DateTime.Today.AddYears(-2);                    
                    dataInizioAggiornamento = dataInizioAggiornamento.AddDays(1);
                }
                prg.Log("-- FINE PROCEDURA --");
            }
        }
        */
    }
}
