﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Security;
using CEXChange;

namespace Cemi.Edilconnect.AuthenticationService
{
    public class AuthService : IAuthService
    {
        public bool Authenticate(String codiceCe, String guid, String partitaIva, Int32 tipoUtente, String loginEc, String passwordEcCriptata, String loginSice, String passwordSice)
        {
            Boolean authenticated = false;

            string usernameAuthEc = ConfigurationManager.AppSettings["AuthEcUsername"];
            //string passwordAuthEc = ConfigurationManager.AppSettings["AuthEcPassword"];
            string passwordAuthEc = ConfigurationManager.AppSettings["AuthEcPassword"];

            String passwordEcCriptataDecriptata = CEXChangeCryptography.cxAESDecryptToString(passwordEcCriptata, passwordAuthEc);
            String[] splitted = passwordEcCriptataDecriptata.Split(' ');

            if (splitted != null && splitted.Length == 2)
            {
                String pIvaCript = splitted[0];
                String timeUtcStr = splitted[1];

                DateTime timeUtc = DateTime.FromFileTimeUtc(long.Parse(timeUtcStr));
                DateTime adesso = DateTime.UtcNow;

                if (loginEc == usernameAuthEc && !String.IsNullOrWhiteSpace(guid)
                    && pIvaCript == partitaIva
                    && timeUtc.Day == adesso.Day && timeUtc.Month == adesso.Month && timeUtc.Year == adesso.Year)
                {
                    //E' edilconnect che mi sta chiamando...
                    switch (tipoUtente)
                    {
                        case 1:
                            // Consulente
                            using (SICEEntities context = new SICEEntities())
                            {
                                String hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(passwordSice, "sha1");
                                Int32 idConsulente = Int32.Parse(codiceCe);

                                var queryConsulenti = from consulente in context.Consulenti
                                                      where consulente.Id == idConsulente
                                                        && consulente.CodiceFiscale == partitaIva
                                                        && consulente.Utente.Username == loginSice
                                                        && consulente.Utente.Password == hashedPassword
                                                        && (!consulente.Utente.DataScadenzaPassword.HasValue || consulente.Utente.DataScadenzaPassword > DateTime.Now)
                                                      select consulente;

                                try
                                {
                                    Consulente consulente = queryConsulenti.Single();
                                    if (consulente != null)
                                    {
                                        consulente.GuidEdilconnect = new Guid(guid);
                                        context.SaveChanges();

                                        authenticated = true;
                                    }
                                }
                                catch { }
                            }
                            break;

                        case 2:
                            // Impresa
                            using (SICEEntities context = new SICEEntities())
                            {
                                String hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(passwordSice, "sha1");
                                Int32 idImpresa = Int32.Parse(codiceCe);

                                var queryImprese = from impresa in context.Imprese
                                                   where impresa.Id == idImpresa
                                                     && impresa.PartitaIVA == partitaIva
                                                     && impresa.Utente.Username == loginSice
                                                     && impresa.Utente.Password == hashedPassword
                                                     && (!impresa.Utente.DataScadenzaPassword.HasValue || impresa.Utente.DataScadenzaPassword > DateTime.Now)
                                                   select impresa;

                                try
                                {
                                    Impresa impresa = queryImprese.Single();
                                    if (impresa != null)
                                    {
                                        impresa.GuidEdilconnect = new Guid(guid);
                                        context.SaveChanges();

                                        authenticated = true;
                                    }
                                }
                                catch { }
                            }
                            break;
                    }
                }
            }

            return authenticated;
        }
    }
}
