﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Cemi.Edilconnect.AuthenticationService
{
    [ServiceContract]
    public interface IAuthService
    {
        [OperationContract]
        bool Authenticate(string codiceCe, string guid, string partitaIva, int tipoUtente, string loginEc, string passwordEc, string loginSice, string passwordSice);
    }
}
