﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CEXChange
{
    public class CEXChangeCryptography
    {
        public static string cxToHex(byte[] abData)
        {
            if (abData == null || abData.Length == 0)
            {
                return "";
            }
            const string HexFormat = "{0:X2}";
            StringBuilder sb = new StringBuilder();
            foreach (byte b in abData)
            {
                sb.Append(string.Format(HexFormat, b));
            }
            return sb.ToString();
        }


        public static byte[] cxFromHex(string sHEXData)
        {

            if (sHEXData == null || sHEXData.Length == 0)
            {
                return null;
            }
            try
            {
                int l = Convert.ToInt32(sHEXData.Length / 2);
                byte[] b = new byte[l];
                for (int i = 0; i <= l - 1; i++)
                {
                    b[i] = Convert.ToByte(sHEXData.Substring(i * 2, 2), 16);
                }
                return b;
            }
            catch
            {
                throw new Exception("La stringa non è formattata correttamente.");
            }

        }

        public static string cxAESEncryptToString(string sData, string sKey, string sSalt = null)
        {

            string data = sData;
            byte[] utfdata = UTF8Encoding.UTF8.GetBytes(data);
            byte[] abValue = cxAESEncrypt(utfdata, sKey, sSalt);

            //Dim encryptedString As String = Convert.ToBase64String(abValue)
            string encryptedString = cxToHex(abValue);

            return encryptedString;

        }

        public static byte[] cxAESEncrypt(byte[] utfdata, string sKey, string sSalt = null)
        {
            // Test data
            byte[] saltBytes = null;
            if (sSalt == null)
            {
                saltBytes = UTF8Encoding.UTF8.GetBytes(cxReverseString(sKey));
            }
            else
            {
                saltBytes = UTF8Encoding.UTF8.GetBytes(sSalt);
            }

            // Our symmetric encryption algorithm
            AesManaged aes = new AesManaged();

            // We're using the PBKDF2 standard for password-based key generation
            Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(sKey, saltBytes);

            // Setting our parameters
            aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
            aes.KeySize = aes.LegalKeySizes[0].MaxSize;

            aes.Key = rfc.GetBytes(aes.KeySize / 8);
            aes.IV = rfc.GetBytes(aes.BlockSize / 8);

            // Encryption
            ICryptoTransform encryptTransf = aes.CreateEncryptor();

            // Output stream, can be also a FileStream
            MemoryStream encryptStream = new MemoryStream();
            CryptoStream encryptor = new CryptoStream(encryptStream, encryptTransf, CryptoStreamMode.Write);

            encryptor.Write(utfdata, 0, utfdata.Length);
            encryptor.Flush();
            encryptor.Close();

            // Showing our encrypted content
            byte[] encryptBytes = encryptStream.ToArray();

            return encryptBytes;
        }

        public static string cxAESDecryptToString(string base64Input, string sKey, string sSalt = null)
        {

            //Dim encryptBytes As Byte() = Convert.FromBase64String(base64Input)
            byte[] encryptBytes = cxFromHex(base64Input);
            byte[] decryptBytes = cxAESDecrypt(encryptBytes, sKey, sSalt);
            string decryptedString = UTF8Encoding.UTF8.GetString(decryptBytes, 0, decryptBytes.Length);

            return decryptedString;

        }

        public static byte[] cxAESDecrypt(byte[] encryptBytes, string sKey, string sSalt = null)
        {

            //byte[] encryptBytes = UTF8Encoding.UTF8.GetBytes(input);
            byte[] saltBytes = null;

            if (sSalt == null)
            {
                saltBytes = Encoding.UTF8.GetBytes(cxReverseString(sKey));
            }
            else
            {
                saltBytes = Encoding.UTF8.GetBytes(sSalt);
            }

            // Our symmetric encryption algorithm
            AesManaged aes = new AesManaged();

            // We're using the PBKDF2 standard for password-based key generation
            Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(sKey, saltBytes);

            // Setting our parameters
            aes.BlockSize = aes.LegalBlockSizes[0].MaxSize;
            aes.KeySize = aes.LegalKeySizes[0].MaxSize;

            aes.Key = rfc.GetBytes(aes.KeySize / 8);
            aes.IV = rfc.GetBytes(aes.BlockSize / 8);

            // Now, decryption
            ICryptoTransform decryptTrans = aes.CreateDecryptor();

            // Output stream, can be also a FileStream
            MemoryStream decryptStream = new MemoryStream();
            CryptoStream decryptor = new CryptoStream(decryptStream, decryptTrans, CryptoStreamMode.Write);

            decryptor.Write(encryptBytes, 0, encryptBytes.Length);
            decryptor.Flush();
            decryptor.Close();

            // Showing our decrypted content
            byte[] decryptBytes = decryptStream.ToArray();
            return decryptBytes;

        }

        public static string cxReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }
}
