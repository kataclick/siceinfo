﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using Cemi.VariazioneStato.ConsoleApp.EmailServiceReference;
using System.Globalization;
using System.Threading;

namespace Cemi.VariazioneStato.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CEMI"].ToString();
            DateTime dataConfronto = DateTime.Today.AddDays(-1);
            List<VariazioneStato> richieste = new List<VariazioneStato>();

            #region Invio mail cambi stati gestiti
            try
            {                
                const string query = "USP_ImpreseRichiesteVariazioneStatoSelectGestitePerMail";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@dataGestione", dataConfronto);
                        connection.Open();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            #region Indici
                            Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                            Int32 indeceDataRichiesta = reader.GetOrdinal("dataRichiesta");
                            Int32 indiceStato = reader.GetOrdinal("stato");
                            Int32 indiceDataVariazione = reader.GetOrdinal("dataVariazioneSiceNew");
                            Int32 indiceEMailSedeLegale = reader.GetOrdinal("eMailSedeLegale");
                            Int32 indiceEMailSedeAmministrazione = reader.GetOrdinal("eMailSedeAmministrazione");
                            Int32 indiceEMailCorrispondenza = reader.GetOrdinal("eMailCorrispondenza");
                            Int32 indiceEMailConsulente = reader.GetOrdinal("eMailConsulente");
                            Int32 indiceRagioneSocialeInpresa = reader.GetOrdinal("ragioneSocialeInpresa");
                            Int32 indiceRagioneSocialeConsulente = reader.GetOrdinal("ragioneSocialeConsulente");
                            #endregion

                            while (reader.Read())
                            {
                                VariazioneStato richiesta = new VariazioneStato
                                {
                                    DataRichiesta = reader.GetDateTime(indeceDataRichiesta),
                                    IdImpresa = reader.GetInt32(indiceIdImpresa),
                                    Stato = reader.GetString(indiceStato),
                                    DataVariazione = reader.GetDateTime(indiceDataVariazione),
                                    EMailSedeLegale = reader.IsDBNull(indiceEMailSedeLegale) ? null : reader.GetString(indiceEMailSedeLegale),
                                    EMailCorrispondenza = reader.IsDBNull(indiceEMailCorrispondenza) ? null : reader.GetString(indiceEMailCorrispondenza),
                                    EMailSedeAmministrazione = reader.IsDBNull(indiceEMailSedeAmministrazione) ? null : reader.GetString(indiceEMailSedeAmministrazione),
                                    EMailConsulente = reader.IsDBNull(indiceEMailConsulente) ? null : reader.GetString(indiceEMailConsulente),
                                    RagioneSocialeConsulente = reader.IsDBNull(indiceRagioneSocialeConsulente) ? null : reader.GetString(indiceRagioneSocialeConsulente),
                                    RagioneSocialeImpresa = reader.IsDBNull(indiceRagioneSocialeInpresa) ? null : reader.GetString(indiceRagioneSocialeInpresa)
                                };
                                richieste.Add(richiesta);
                            }
                        }
                    }
                }

                foreach (VariazioneStato richiestaGestita in richieste)
                {
                    InviaEMail(richiestaGestita);
                }
            }
            catch (Exception ex)
            {
                TracciaMessaggio(ex.Message);
            }
            #endregion

            richieste = null;
            richieste = new List<VariazioneStato>();

            #region Invio mail cambi stati respinti
            try
            {
                const string query = "USP_ImpreseRichiesteVariazioneStatoSelectRespintePerMail";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@dataGestione", dataConfronto);
                        connection.Open();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            #region Indici
                            Int32 indiceIdImpresa = reader.GetOrdinal("idImpresa");
                            Int32 indeceDataRichiesta = reader.GetOrdinal("dataRichiesta");
                            Int32 indiceStato = reader.GetOrdinal("stato");
                            Int32 indiceEMailSedeLegale = reader.GetOrdinal("eMailSedeLegale");
                            Int32 indiceEMailSedeAmministrazione = reader.GetOrdinal("eMailSedeAmministrazione");
                            Int32 indiceEMailCorrispondenza = reader.GetOrdinal("eMailCorrispondenza");
                            Int32 indiceEMailConsulente = reader.GetOrdinal("eMailConsulente");
                            Int32 indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSocialeImpresa");
                            Int32 indiceRagioneSocialeConsulente = reader.GetOrdinal("ragioneSocialeConsulente");
                            Int32 indiceDescrizioneCausaleRespinta = reader.GetOrdinal("descrizione");
                            #endregion

                            while (reader.Read())
                            {
                                VariazioneStato richiesta = new VariazioneStato
                                {
                                    DataRichiesta = reader.GetDateTime(indeceDataRichiesta),
                                    IdImpresa = reader.GetInt32(indiceIdImpresa),
                                    Stato = reader.IsDBNull(indiceStato) ? null : reader.GetInt32(indiceStato).ToString(),
                                    DataVariazione = DateTime.MinValue,
                                    EMailSedeLegale = reader.IsDBNull(indiceEMailSedeLegale) ? null : reader.GetString(indiceEMailSedeLegale),
                                    EMailCorrispondenza = reader.IsDBNull(indiceEMailCorrispondenza) ? null : reader.GetString(indiceEMailCorrispondenza),
                                    EMailSedeAmministrazione = reader.IsDBNull(indiceEMailSedeAmministrazione) ? null : reader.GetString(indiceEMailSedeAmministrazione),
                                    EMailConsulente = reader.IsDBNull(indiceEMailConsulente) ? null : reader.GetString(indiceEMailConsulente),
                                    RagioneSocialeConsulente = reader.IsDBNull(indiceRagioneSocialeConsulente) ? null : reader.GetString(indiceRagioneSocialeConsulente),
                                    RagioneSocialeImpresa = reader.IsDBNull(indiceRagioneSocialeImpresa) ? null : reader.GetString(indiceRagioneSocialeImpresa),
                                    DescrizioneCausaleRespinta = reader.IsDBNull(indiceDescrizioneCausaleRespinta) ? null : reader.GetString(indiceDescrizioneCausaleRespinta)
                                };
                                richieste.Add(richiesta);
                            }
                        }
                    }
                }

                foreach (VariazioneStato richiestaGestita in richieste)
                {
                    InviaEMailRespinta(richiestaGestita);
                }
            }
            catch (Exception ex)
            {
                TracciaMessaggio(ex.Message);
            }
            #endregion
        }

        private static void InviaEMail(VariazioneStato richiesta)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            bool usaMailTest = true;
            string usaMailTestString = ConfigurationManager.AppSettings["UsaMailTest"];
            if (usaMailTestString == "false")
                usaMailTest = false;

            EmailMessageSerializzabile email = new EmailMessageSerializzabile
                                                   {
                                                       Mittente = new EmailAddress
                                                                      {
                                                                          Nome = "Cassa Edile di Milano - Ufficio Servizi alle Imprese",
                                                                          Indirizzo = "noreply-datorilavoro@cassaedilemilano.it"
                                                                      },
                                                       Oggetto =
                                                           String.Format("Variazione stato impresa {1} - {0} eseguita",
                                                                         richiesta.IdImpresa,
                                                                         richiesta.RagioneSocialeImpresa)
                                                   };


            // HTML
            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("<font face=\"Arial\" size=2>");
            sbTestoHtml.Append("Buongiorno,");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.AppendFormat("si informa che la richiesta di <b>{0}</b> relativa all'impresa {1} (codice iscrizione n. {2}), effettuata in data <b>",
                richiesta.Stato == "SOSPESA" ? "SOSPENSIONE" : (richiesta.Stato == "ATTIVA" ? "RIATTIVAZIONE" : (richiesta.Stato == "CESSATA" ? "CESSAZIONE" : String.Empty)),
                richiesta.RagioneSocialeImpresa,
                richiesta.IdImpresa);
            CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
            sbTestoHtml.Append(richiesta.DataRichiesta.ToShortDateString());
            sbTestoHtml.Append(
                "</b> tramite la funzione ad accesso privato \"<b>Variazione stato impresa</b>\" dell’area \"<b>Servizi on-line</b>\" del sito internet <a href='http://www.cassaedilemilano.it'>www.cassaedilemilano.it</a>, è stata inoltrata correttamente. La variazione è stata, pertanto, registrata sui nostri sistemi informativi.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");

            sbTestoHtml.Append("La posizione dell’impresa risulta:<br />");
            sbTestoHtml.AppendFormat("{0} dal {1}<br />",
                richiesta.Stato.ToLower(),
                richiesta.DataVariazione.ToShortDateString());

            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Si ringrazia per la cortese collaborazione.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cordiali saluti.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Ufficio Servizi alle Imprese");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            sbTestoHtml.Append("</font>");


            // PLAIN
            StringBuilder sbTestoPlain = new StringBuilder();
            sbTestoPlain.Append("Buongiorno,");
            sbTestoPlain.Append("\n");
            sbTestoPlain.AppendFormat("si informa che la richiesta di {0} relativa all'impresa {1} (codice iscrizione n. {2}), effettuata in data ",
                richiesta.Stato == "SOSPESA" ? "SOSPENSIONE" : (richiesta.Stato == "ATTIVA" ? "RIATTIVAZIONE" : (richiesta.Stato == "CESSATA" ? "CESSAZIONE" : String.Empty)),
                richiesta.RagioneSocialeImpresa,
                richiesta.IdImpresa);
            sbTestoPlain.Append(richiesta.DataRichiesta.ToShortDateString());
            sbTestoPlain.Append(
                " tramite la funzione ad accesso privato \"Variazione stato impresa\" dell’area \"Servizi on-line\" del sito internet www.cassaedilemilano.it, è stata inoltrata correttamente. La variazione è stata, pertanto, registrata sui nostri sistemi informativi.");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");

            sbTestoPlain.Append("La posizione dell’impresa risulta: \n");
            sbTestoPlain.AppendFormat("{0} dal {1} \n", richiesta.Stato.ToLower(), richiesta.DataVariazione.ToShortDateString());

            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Si ringrazia per la cortese collaborazione.");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Cordiali saluti.");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Ufficio Servizi alle Imprese");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            if (usaMailTest)
            {
                String[] mailDestinatariTest = ConfigurationManager.AppSettings["MailTest"].Split(';');
                email.Destinatari = new EmailAddress[mailDestinatariTest.Count()];
                for (int i = 0; i < mailDestinatariTest.Count(); i++)
                {
                    email.Destinatari[i] = new EmailAddress
                                               {
                                                   Indirizzo = mailDestinatariTest[i],
                                                   Nome = richiesta.RagioneSocialeImpresa
                                               };
                }
            }
            else
            {
                List<EmailAddress> destinatari = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(richiesta.EMailSedeLegale))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailSedeLegale, Nome = richiesta.RagioneSocialeImpresa });
                }

                if (!String.IsNullOrEmpty(richiesta.EMailSedeAmministrazione))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailSedeAmministrazione, Nome = richiesta.RagioneSocialeImpresa });
                }

                if (!String.IsNullOrEmpty(richiesta.EMailCorrispondenza))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailCorrispondenza, Nome = richiesta.RagioneSocialeImpresa });
                }

                if (!String.IsNullOrEmpty(richiesta.EMailConsulente))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailConsulente, Nome = richiesta.RagioneSocialeConsulente });
                }

                var destinatariUnici = destinatari.GroupBy(d => d.Indirizzo).Select(g => g.First()).ToList();

                email.Destinatari = destinatariUnici.ToArray();
            }

            if (email.Destinatari.Length > 0)
            {
                EmailInfoServiceSoapClient client = new EmailInfoServiceSoapClient();

                NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
                client.ClientCredentials.Windows.ClientCredential = credentials;
                client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                client.InviaEmail(email);
            }
        }

        private static void InviaEMailRespinta(VariazioneStato richiesta)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            bool usaMailTest = true;
            string usaMailTestString = ConfigurationManager.AppSettings["UsaMailTest"];
            if (usaMailTestString == "false")
                usaMailTest = false;

            EmailMessageSerializzabile email = new EmailMessageSerializzabile
            {
                Mittente = new EmailAddress
                {
                    Nome = "Cassa Edile di Milano - Ufficio Servizi alle Imprese",
                    Indirizzo = "noreply-datorilavoro@cassaedilemilano.it"
                },
                Oggetto =
                                                           String.Format("Variazione stato impresa {1} - {0}. Richiesta non accolta",
                                                                         richiesta.IdImpresa,
                                                                         richiesta.RagioneSocialeImpresa)
            };


            // HTML
            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("<font face=\"Arial\" size=2>");
            sbTestoHtml.Append("Buongiorno,");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.AppendFormat("si informa che la richiesta di <b>{0}</b> relativa all'impresa {1} - codice iscrizione n. {2} effettuata in data ",
                richiesta.Stato == "1" ? "sospensione" : (richiesta.Stato == "2" ? "riattivazione" : String.Empty),
                richiesta.RagioneSocialeImpresa,
                richiesta.IdImpresa);
            CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
            sbTestoHtml.Append(richiesta.DataRichiesta.ToShortDateString());
            sbTestoHtml.Append(
                "</b> tramite la funzione ad accesso privato \"<b>Variazione stato impresa</b>\" dell’area \"<b>Servizi on-line</b>\" del sito internet <a href='http://www.cassaedilemilano.it'>www.cassaedilemilano.it</a>, è stata inoltrata correttamente.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("La richiesta non può, tuttavia, essere accolta in quanto:");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.AppendFormat("{0}", richiesta.DescrizioneCausaleRespinta);
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("L’ufficio Servizi alle Imprese (tel. 02.584961 – tasto 1) resta a disposizione per eventuali chiarimenti in merito.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cordiali saluti.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Ufficio Servizi alle Imprese");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            sbTestoHtml.Append("</font>");            


            // PLAIN
            StringBuilder sbTestoPlain = new StringBuilder();
            sbTestoPlain.Append("Buongiorno,");
            sbTestoPlain.Append("\n");
            sbTestoPlain.AppendFormat("si informa che la richiesta di {0} relativa all'impresa {1} (codice iscrizione n. {2}), effettuata in data ",
                richiesta.Stato == "SOSPESA" ? "SOSPENSIONE" : (richiesta.Stato == "ATTIVA" ? "RIATTIVAZIONE" : (richiesta.Stato == "CESSATA" ? "CESSAZIONE" : String.Empty)),
                richiesta.RagioneSocialeImpresa,
                richiesta.IdImpresa);
            sbTestoPlain.Append(richiesta.DataRichiesta.ToShortDateString());
            sbTestoPlain.Append(
                " tramite la funzione ad accesso privato \"Variazione stato impresa\" dell’area \"Servizi on-line\" del sito internet www.cassaedilemilano.it, è stata inoltrata correttamente.");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("La richiesta non può, tuttavia, essere accolta in quanto:");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.AppendFormat("{0}", richiesta.DescrizioneCausaleRespinta);
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("L’ufficio Servizi alle Imprese (tel. 02.584961 – tasto 1) resta a disposizione per eventuali chiarimenti in merito.");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Cordiali saluti.");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Ufficio Servizi alle Imprese");
            sbTestoPlain.Append("\n");
            sbTestoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            if (usaMailTest)
            {
                String[] mailDestinatariTest = ConfigurationManager.AppSettings["MailTest"].Split(';');
                email.Destinatari = new EmailAddress[mailDestinatariTest.Count()];
                for (int i = 0; i < mailDestinatariTest.Count(); i++)
                {
                    email.Destinatari[i] = new EmailAddress
                    {
                        Indirizzo = mailDestinatariTest[i],
                        Nome = richiesta.RagioneSocialeImpresa
                    };
                }
            }
            else
            {
                List<EmailAddress> destinatari = new List<EmailAddress>();
                if (!String.IsNullOrEmpty(richiesta.EMailSedeLegale))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailSedeLegale, Nome = richiesta.RagioneSocialeImpresa });
                }

                if (!String.IsNullOrEmpty(richiesta.EMailSedeAmministrazione))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailSedeAmministrazione, Nome = richiesta.RagioneSocialeImpresa });
                }

                if (!String.IsNullOrEmpty(richiesta.EMailCorrispondenza))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailCorrispondenza, Nome = richiesta.RagioneSocialeImpresa });
                }

                if (!String.IsNullOrEmpty(richiesta.EMailConsulente))
                {
                    destinatari.Add(new EmailAddress { Indirizzo = richiesta.EMailConsulente, Nome = richiesta.RagioneSocialeConsulente });
                }

                var destinatariUnici = destinatari.GroupBy(d => d.Indirizzo).Select(g => g.First()).ToList();

                email.Destinatari = destinatariUnici.ToArray();
            }

            if (email.Destinatari.Length > 0)
            {
                EmailInfoServiceSoapClient client = new EmailInfoServiceSoapClient();

                NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
                client.ClientCredentials.Windows.ClientCredential = credentials;
                client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                client.InviaEmail(email);
            }
        }


        private static void TracciaMessaggio(string messaggio)
        {
            string logVariazione = ConfigurationManager.AppSettings["LogVariazione"];

            Console.WriteLine(messaggio);

            if (logVariazione == "true")
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Eccezione Variazione Stato Invio Mail: {0}", messaggio));
                }
            }
        }
    }
}
