﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.VariazioneStato.ConsoleApp
{
    public class VariazioneStato
    {
        public int IdImpresa { get; set; }
        public DateTime DataRichiesta { get; set; }
        public String Stato { get; set; }
        public DateTime DataVariazione { get; set; }
        public String EMailSedeLegale { get; set; }
        public String EMailSedeAmministrazione { get; set; }
        public String EMailCorrispondenza { get; set; }
        public String EMailConsulente { get; set; }
        public String RagioneSocialeImpresa { get; set; }
        public String RagioneSocialeConsulente { get; set; }
        public String DescrizioneCausaleRespinta { get; set; }
    }
}
