﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace TBridge.Cemi.Cruscotto.CubeBrowserService
{
    // NOTE: If you change the interface name "ICubeService" here, you must also update the reference to "ICubeService" in Web.config.
    [ServiceContract]
    public interface ICubeService
    {
        [OperationContract]
        AdoMdHelper.CellStone GetResult(string strMembers, string strAxis0, string strAxis1, string strMeasure,
                                        string strcube);

        [OperationContract]
        String GetResultToString(string strMembers, string strAxis0, string strAxis1, string strMeasure, string strcube);

        [OperationContract]
        AdoMdHelper.CellStone GetKpi(string Kpi, string strAxis0, string strcube);

        [OperationContract]
        List<AdoMdHelper.DimensionInfo> GetDims(string cubename);

        [OperationContract]
        List<AdoMdHelper.Dimensions> GetDimensions(string cubename);

        [OperationContract]
        List<AdoMdHelper.DimensionInfo> GetHierarchies(string cubename, string dimension);

        [OperationContract]
        List<AdoMdHelper.MemberInfo> GetHierarchyMembers(string cubename, string dimension, string hierarchy);

        [OperationContract]
        List<AdoMdHelper.MeasureInfo> GetMeasures(string cubename);

        [OperationContract]
        List<AdoMdHelper.KpiInfo> GetKpis(string cubename);

        [OperationContract]
        List<AdoMdHelper.CubePInfo> GetCubes();

        [OperationContract]
        List<string> GetDimMembers(string strDim, string strcube);

        [OperationContract]
        AdoMdHelper.DataSeries ReadFromExcel(string fileName);

        [OperationContract]
        AdoMdHelper.DataSeries ReadMassaSalarialeAnnualeFromExcel(string filename);


        [OperationContract]
        AdoMdHelper.DataSeries ReadMassaSalarialeMensileFromExcel(string filename);

        [OperationContract]
        List<AdoMdHelper.Dimensione> GetDimensioni(string cubename);

        [OperationContract]
        string GetDataAggiornamento(string measure,string cubename);
    }
}