﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Security.Permissions;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Microsoft.AnalysisServices.AdomdClient;

namespace TBridge.Cemi.Cruscotto.CubeBrowserService
{
    // NOTE: If you change the class name "CubeService" here, you must also update the reference to "CubeService" in Web.config.
    
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,IncludeExceptionDetailInFaults = true)]
    public class CubeService : ICubeService
    {
        private readonly string strConn = ConfigurationManager.ConnectionStrings["CONN"].ToString();

        #region ICubeService Members

        public AdoMdHelper.CellStone GetResult(string strMembers, string strAxis0, string strAxis1, string strMeasure,
                                               string strcube)
        {
            AdoMdHelper.CellStone result = new AdoMdHelper.CellStone();

            string strMDX = AdoMdHelper.MadeMDX(strMembers, strAxis0, strAxis1, strMeasure, strcube);
            CellSet cs = AdoMdHelper.GetCellSet(strMDX);
            result = AdoMdHelper.ToCellStone(cs);
            return result;
        }

        public String GetResultToString(string strMembers, string strAxis0, string strAxis1, string strMeasure,
                                        string strcube)
        {
            string result = "";

            string strMDX = AdoMdHelper.MadeMDX(strMembers, strAxis0, strAxis1, strMeasure, strcube);
            CellSet cs = AdoMdHelper.GetCellSet(strMDX);
            result = AdoMdHelper.ToString(cs);
            return result;
        }

        public AdoMdHelper.CellStone GetKpi(string Kpi, string strAxis0, string strcube)
        {
            AdoMdHelper.CellStone result = new AdoMdHelper.CellStone();

            string strMDX = AdoMdHelper.MadeMDX(Kpi, strAxis0, strcube);

            CellSet cs = AdoMdHelper.GetCellSet(strMDX);
            result = AdoMdHelper.ToCellStone(cs);
            return result;
        }

        public List<AdoMdHelper.DimensionInfo> GetDims(string cubename)
        {
            List<AdoMdHelper.DimensionInfo> result = new List<AdoMdHelper.DimensionInfo>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;
               

                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                List<Hierarchy> lstHierarchy = CubeHelper.CubesHiberarchies(sepcube);

                foreach (Hierarchy hierarchy in lstHierarchy)
                {
                    result.Add(new AdoMdHelper.DimensionInfo
                                   {
                                       Caption = hierarchy.Name,
                                       Expression = hierarchy.UniqueName,
                                       LevelCount = hierarchy.Levels.Count
                                   });
                }
            }
            return result;
        }

        public List<AdoMdHelper.Dimensions> GetDimensions(string cubename)
        {
            List<AdoMdHelper.Dimensions> result = new List<AdoMdHelper.Dimensions>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;


                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                List<Dimension> lstHierarchy = CubeHelper.CubesDimensions(sepcube);

                foreach (Dimension hierarchy in lstHierarchy)
                {
                    result.Add(new AdoMdHelper.Dimensions
                                   {
                                       Caption = hierarchy.Name,
                                       Expression = hierarchy.UniqueName,
                                   });
                }
            }
            return result;
        }

        public List<AdoMdHelper.DimensionInfo> GetHierarchies(string cubename, string dimension)
        {
            List<AdoMdHelper.DimensionInfo> result = new List<AdoMdHelper.DimensionInfo>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;


                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                List<Hierarchy> lstHierarchy = CubeHelper.CubesHiberarchies(sepcube, dimension);

                foreach (Hierarchy hierarchy in lstHierarchy)
                {
                    result.Add(new AdoMdHelper.DimensionInfo
                                   {
                                       Caption = hierarchy.Name,
                                       Expression = hierarchy.UniqueName,
                                       LevelCount = hierarchy.Levels.Count
                                   });
                }
            }
            return result;
        }

        public List<AdoMdHelper.MemberInfo> GetHierarchyMembers(string cubename, string dimension, string hierarchy)
        {
            List<AdoMdHelper.MemberInfo> result = new List<AdoMdHelper.MemberInfo>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;


                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                List<Member> lstMember = CubeHelper.CubesHiberarchyMembers(sepcube, dimension, hierarchy);

                foreach (Member member in lstMember)
                {
                    result.Add(new AdoMdHelper.MemberInfo
                                   {
                                       Caption = member.Name,
                                       Expression = member.UniqueName
                                   });
                }
            }
            return result;
        }

        public List<AdoMdHelper.Dimensione> GetDimensioni(string cubename)
        {
            List<AdoMdHelper.Dimensione> result = new List<AdoMdHelper.Dimensione>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;


                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                foreach (Dimension dimension in sepcube.Dimensions)
                {
                    if (!(dimension.Caption.Equals("Measures") || dimension.Caption.Contains("Data")))
                    {
                        AdoMdHelper.Dimensione dimensione = new AdoMdHelper.Dimensione();
                        dimensione.Caption = dimension.Name;
                        dimensione.Expression = dimension.UniqueName;
                        foreach (Hierarchy attribute in dimension.AttributeHierarchies)
                        {
                            if (!attribute.Caption.Contains("Id"))
                            {
                                AdoMdHelper.Attributo attributo = new AdoMdHelper.Attributo();
                                attributo.Caption = attribute.Name;
                                attributo.Expression = attribute.UniqueName;

                                MemberCollection memberCollection = attribute.Levels[1].GetMembers();
                                foreach (Member member in memberCollection)
                                    if (!(member.Name.Equals("Unknown") || String.IsNullOrEmpty(member.Name)))
                                        attributo.Members.Add(new AdoMdHelper.MemberInfo
                                                                  {
                                                                      Caption = member.Name,
                                                                      Expression = member.UniqueName
                                                                  });
                                dimensione.Attributes.Add(attributo);
                            }
                        }
                        result.Add(dimensione);
                    }
                }
            }
            return result;
        }

        public string GetDataAggiornamento(string measure, string cubename)
        {
            string mdx = string.Format("SELECT {0} ON COLUMNS FROM [{1}]", measure, cubename);
            return AdoMdHelper.GetDate(mdx);
        }

        public List<AdoMdHelper.MeasureInfo> GetMeasures(string cubename)
        {
            List<AdoMdHelper.MeasureInfo> result = new List<AdoMdHelper.MeasureInfo>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;

                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                List<Measure> lstMeasure = CubeHelper.CubesMeasure(sepcube);

                foreach (Measure item in lstMeasure)
                {
                    result.Add(new AdoMdHelper.MeasureInfo
                                   {
                                       Caption = item.Name,
                                       Expression = item.UniqueName,
                                   });
                }
            }
            return result;
        }

        public List<AdoMdHelper.KpiInfo> GetKpis(string cubename)
        {
            List<AdoMdHelper.KpiInfo> result = new List<AdoMdHelper.KpiInfo>();

            CubeHelper cube = new CubeHelper();
            cube.ConnectionString = strConn;
            //cube.OpenConnection();

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;

                CubeDef sepcube = CubeHelper.GetCubeByName(cubename, connection.Cubes);
                List<Kpi> lstKpi = CubeHelper.Cubeskpi(sepcube);

                connection.Close();

                foreach (Kpi kpi in lstKpi)
                {
                    result.Add(new AdoMdHelper.KpiInfo
                                   {
                                       Name = kpi.Name,
                                       Goal = kpi.Properties["KPI_GOAL"].Value.ToString(),
                                       Status = kpi.Properties["KPI_STATUS"].Value.ToString(),
                                       Trend = kpi.Properties["KPI_TREND"].Value.ToString(),
                                       StatusGraphic = kpi.Properties["KPI_STATUS_GRAPHIC"].Value.ToString(),
                                       TrendGraphic = kpi.Properties["KPI_TREND_GRAPHIC"].Value.ToString(),
                                       Caption = kpi.Properties["KPI_CAPTION"].Value.ToString(),
                                       DisplayFolder = kpi.Properties["KPI_DISPLAY_FOLDER"].Value.ToString(),
                                       Value = kpi.Properties["KPI_VALUE"].Value.ToString(),
                                       Weight = kpi.Properties["KPI_WEIGHT"].Value.ToString(),
                                       Pkn = kpi.Properties["KPI_PARENT_KPI_NAME"].Value.ToString(),
                                       kpiMDX =
                                           @"Select {" + kpi.Properties["KPI_VALUE"].Value + "," +
                                           kpi.Properties["KPI_GOAL"].Value + "," + kpi.Properties["KPI_STATUS"].Value +
                                           "," + kpi.Properties["KPI_TREND"].Value + "} on columns"
                                       ,
                                   });
                }
            }
            return result;
        }

        public List<AdoMdHelper.CubePInfo> GetCubes()
        {
            List<AdoMdHelper.CubePInfo> result = new List<AdoMdHelper.CubePInfo>();

            CubeHelper cube = new CubeHelper();

            //cube.Connection = new AdomdConnection();

            //cube.ConnectionString = strConn;
            //cube.Connection.Open(strConn);

            using (AdomdConnection connection = new AdomdConnection())
            {
                connection.ConnectionString = strConn;
                connection.Open();
                cube.Connection = connection;
                //List<CubeDef> lstCubes = CubeHelper.CubesCube(cube.Connection.Cubes);
                List<CubeDef> lstCubes = CubeHelper.CubesCube(connection.Cubes);

                foreach (CubeDef item in lstCubes)
                {
                    result.Add(new AdoMdHelper.CubePInfo
                                   {
                                       Caption = item.Name,
                                       LastProcess = item.LastProcessed,
                                       LastUpdate = item.LastUpdated
                                   });
                }
            }
            return result;
        }

        public List<string> GetDimMembers(string strDim, string strcube)
        {
            List<string> result = new List<string>();

            string strMDX = "select nonempty(" + strDim + ".children) on columns from " + strcube;
            CellSet cs = AdoMdHelper.GetCellSet(strMDX);

            foreach (Position p in cs.Axes[0].Positions)
            {
                foreach (Member m in p.Members)
                {
                    result.Add(m.Caption);
                }
            }

            return result;
        }

        public AdoMdHelper.DataSeries ReadFromExcel(string fileName)
        {
            AdoMdHelper.DataSeries result = new AdoMdHelper.DataSeries();

            string connString =
                String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", fileName);

            // Create the connection object 
            OleDbConnection oledbConn = new OleDbConnection(connString);
            try
            {
                // Open connection
                oledbConn.Open();

                // Create OleDbCommand object and select data from worksheet Sheet1
                OleDbCommand cmd = new OleDbCommand("SELECT Anno,Mese,Budget FROM [Foglio1$]", oledbConn);

                // Create new OleDbDataAdapter 
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                oleda.SelectCommand = cmd;

                // Create a DataSet which will hold the data extracted from the worksheet.
                DataSet ds = new DataSet();

                // Fill the DataSet from the data extracted from the worksheet.
                oleda.Fill(ds, "Budget");

                int row = 0;

                while (ds.Tables[0].Rows[row][0] != DBNull.Value)
                {
                    AdoMdHelper.DataPoint dp = new AdoMdHelper.DataPoint();
                    dp.LegendLabel = ds.Tables[0].Rows[row][1].ToString();
                    dp.Value = double.Parse(ds.Tables[0].Rows[row][2].ToString());

                    result.Definition.Add(dp);
                    row++;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            finally
            {
                // Close connection
                oledbConn.Close();
            }

            return result;
        }

        public AdoMdHelper.DataSeries ReadMassaSalarialeMensileFromExcel(string fileName)
        {
            AdoMdHelper.DataSeries result = new AdoMdHelper.DataSeries();

            string connString =
                String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", fileName);

            // Create the connection object 
            OleDbConnection oledbConn = new OleDbConnection(connString);
            try
            {
                // Open connection
                oledbConn.Open();

                // Create OleDbCommand object and select data from worksheet Sheet1
                OleDbCommand cmd = new OleDbCommand("SELECT Anno,Mese,MassaSalariale FROM [Foglio1$]", oledbConn);

                // Create new OleDbDataAdapter 
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                oleda.SelectCommand = cmd;

                // Create a DataSet which will hold the data extracted from the worksheet.
                DataSet ds = new DataSet();

                // Fill the DataSet from the data extracted from the worksheet.
                oleda.Fill(ds, "MassaSalariale");

                int row = 0;

                while (ds.Tables[0].Rows[row][0] != DBNull.Value)
                {
                    AdoMdHelper.DataPoint dp = new AdoMdHelper.DataPoint();
                    dp.LegendLabel = ds.Tables[0].Rows[row][1].ToString();
                    dp.Value = double.Parse(ds.Tables[0].Rows[row][2].ToString());

                    result.Definition.Add(dp);
                    row++;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            finally
            {
                // Close connection
                oledbConn.Close();
            }

            return result;
        }

        public AdoMdHelper.DataSeries ReadMassaSalarialeAnnualeFromExcel(string fileName)
        {
            AdoMdHelper.DataSeries result = new AdoMdHelper.DataSeries();

            string connString =
                String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0", fileName);

            // Create the connection object 
            OleDbConnection oledbConn = new OleDbConnection(connString);
            try
            {
                // Open connection
                oledbConn.Open();

                // Create OleDbCommand object and select data from worksheet Sheet1
                OleDbCommand cmd = new OleDbCommand("SELECT Anno,MassaSalariale FROM [Foglio1$]", oledbConn);

                // Create new OleDbDataAdapter 
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                oleda.SelectCommand = cmd;

                // Create a DataSet which will hold the data extracted from the worksheet.
                DataSet ds = new DataSet();

                // Fill the DataSet from the data extracted from the worksheet.
                oleda.Fill(ds, "MassaSalariale");

                int row = 0;

                while (ds.Tables[0].Rows[row][0] != DBNull.Value)
                {
                    AdoMdHelper.DataPoint dp = new AdoMdHelper.DataPoint();
                    dp.LegendLabel = ds.Tables[0].Rows[row][0].ToString();
                    dp.Value = double.Parse(ds.Tables[0].Rows[row][1].ToString());

                    result.Definition.Add(dp);
                    row++;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            finally
            {
                // Close connection
                oledbConn.Close();
            }

            return result;
        }


        #endregion
    }
}