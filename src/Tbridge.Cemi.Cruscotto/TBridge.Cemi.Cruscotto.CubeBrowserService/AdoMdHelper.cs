﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Microsoft.AnalysisServices.AdomdClient;
using Tuple = Microsoft.AnalysisServices.AdomdClient.Tuple;

namespace TBridge.Cemi.Cruscotto.CubeBrowserService
{
    public class AdoMdHelper
    {
        #region EnuCellType enum

        /// <summary>
        /// Enumeratore di tipo
        /// </summary>
        public enum EnuCellType
        {
            Axis,
            CellValue
        }

        #endregion

        private static readonly string connectionString =
            ConfigurationManager.ConnectionStrings["CONN"].ToString();

        public static CellSet GetCellSet(string strMDX)
        {
            CellSet cs;
            using (AdomdConnection conn = new AdomdConnection(connectionString))
            {
                conn.Open();
                AdomdCommand command = new AdomdCommand(strMDX, conn);
                cs = command.ExecuteCellSet();
                conn.Close();
            }
            return cs;
        }

        public static string ToString(CellSet cs)
        {
            StringBuilder result = new StringBuilder();

            result.Append("\t");

            TupleCollection tuplesOnColumns = cs.Axes[0].Set.Tuples;
            foreach (Tuple column in tuplesOnColumns)
            {
                result.Append(column.Members[0].Caption + "\t");
            }
            result.AppendLine();
            TupleCollection tuplesOnRows = cs.Axes[1].Set.Tuples;
            for (int row = 0; row < tuplesOnRows.Count; row++)
            {
                result.Append(tuplesOnRows[row].Members[0].Caption + "\t");
                for (int col = 0; col < tuplesOnColumns.Count; col++)
                {
                    result.Append(cs.Cells[col, row].FormattedValue + "\t");
                }
                result.AppendLine();
            }

            return result.ToString();
        }

        public static CellStone ToCellStone(CellSet cs)
        {
            CellStone result = new CellStone();

            int columnsCount = cs.Axes[0].Positions.Count;
            int rowCount = cs.Axes[1].Positions.Count;

            CellSetRow csr = new CellSetRow();

            csr.Columns.Add(new Cell());
            foreach (Position p in cs.Axes[0].Positions)
            {
                foreach (Member m in p.Members)
                {
                    Cell dc = new Cell();

                    dc.Caption = m.Caption;
                    dc.CellType = EnuCellType.Axis;
                    dc.UniqueName = m.UniqueName;

                    csr.Columns.Add(dc);
                }
            }

            result.Rows.Add(csr);

            int pos = 0;
            foreach (Position py in cs.Axes[1].Positions)
            {
                CellSetRow csrd = new CellSetRow();

                csrd.Columns.Add(new Cell
                                     {
                                         Caption = py.Members[0].Caption,
                                         CellType = EnuCellType.Axis,
                                         UniqueName = py.Members[0].UniqueName
                                     });

                for (int x = 0; x < columnsCount; x++)
                {
                    string _caption = cs.Cells[x, pos].FormattedValue;
                    if (string.IsNullOrEmpty(_caption))
                    {
                        _caption = "0";
                    }

                    csrd.Columns.Add(new Cell
                                         {
                                             Caption = _caption,
                                             CellType = EnuCellType.CellValue,
                                             UniqueName = ""
                                         });
                }

                pos++;
                result.Rows.Add(csrd);
            }

            return result;
        }

        public static string MadeMDX(string strMembers, string strAxis0, string strAxis1, string strMeasure,
                                     string strcube)
        {
            string result = "";

            bool bAxis0 = !string.IsNullOrEmpty(strAxis0);
            bool bAxis1 = !string.IsNullOrEmpty(strAxis1);
            bool bMembers = !string.IsNullOrEmpty(strMembers);

            if (
                !(strAxis0.Contains("&") || strAxis0.EndsWith(".members") || strAxis0.EndsWith("}") ||
                  strAxis0.EndsWith(")")))
                strAxis0 = string.Format("{0}.children", strAxis0);
            if (
                !(strAxis1.Contains("&") || strAxis1.EndsWith(".members") || strAxis0.EndsWith("}") ||
                  strAxis0.EndsWith(")")))
                strAxis1 = string.Format("{0}.children", strAxis1);
            if (bMembers)
                strMembers = string.Format("WITH {0}", strMembers);
            else strMembers = "";

            if (bAxis0 && bAxis1)
            {
                result = strMembers + "select nonempty(" + strAxis0 + ") on columns,nonempty(" + strAxis1 +
                         ") on rows from [" + strcube + "] where (" + strMeasure + ")";
            }
            if (bAxis0 && bAxis1 == false)
            {
                result = strMembers + "select nonempty(" + strAxis0 + ") on columns,(" + strMeasure + ") on rows from [" +
                         strcube + "]";
            }
            if (bAxis0 == false && bAxis1)
            {
                result = strMembers + "select (" + strMeasure + ") on columns,nonempty(" + strAxis1 + ") on rows from [" +
                         strcube + "]";
            }


            return result;
        }

        public static string MadeMDX(string Kpi, string axis0, string strcube)
        {
            string result;
            result = string.Format("{0},nonempty({1}.children) on rows from [{2}]", Kpi, axis0, strcube);
            return result;
        }

        #region Nested type: Attributo

        public class Attributo
        {
            public Attributo()
            {
                Members = new List<MemberInfo>();
            }

            public string Caption { get; set; }
            public string Expression { get; set; }
            public List<MemberInfo> Members { get; set; }
        }

        #endregion

        #region Nested type: Cell

        /// <summary>
        /// Represents a cell in a given cellset. 
        /// </summary>
        public class Cell
        {
            public string Caption { get; set; }
            public string UniqueName { get; set; }
            public EnuCellType CellType { get; set; }
        }

        #endregion

        #region Nested type: CellSetRow

        /// <summary>
        /// Represents a row in a given cellset
        /// </summary>
        public class CellSetRow
        {
            public CellSetRow()
            {
                Columns = new List<Cell>();
                Visible = true;
            }

            public List<Cell> Columns { get; set; }
            public bool Visible { get; set; }
        }

        #endregion

        #region Nested type: CellStone

        /// <summary>
        /// CellSetStone
        /// </summary>
        public class CellStone
        {
            public CellStone()
            {
                Rows = new List<CellSetRow>();
            }

            public List<CellSetRow> Rows { get; set; }
        }

        #endregion

        #region Nested type: CubePInfo

        /// <summary>
        /// Represents a cube
        /// </summary>
        public class CubePInfo
        {
            /// <summary>
            /// Gets or sets the caption for the cube
            /// </summary>
            public string Caption { get; set; }
            public DateTime LastProcess { get; set; }
            public DateTime LastUpdate { get; set; }
            //public string Expression { get; set; }
        }

        #endregion

        #region Nested type: DataPoint

        public class DataPoint
        {
            public string LegendLabel { get; set; }
            public double Value { get; set; }
        }

        #endregion

        #region Nested type: DataSeries

        public class DataSeries
        {
            public DataSeries()
            {
                Definition = new List<DataPoint>();
            }

            public List<DataPoint> Definition { get; set; }
        }

        #endregion

        #region Nested type: Dimensione

        public class Dimensione
        {
            public Dimensione()
            {
                Attributes = new List<Attributo>();
            }

            public string Caption { get; set; }
            public string Expression { get; set; }
            public List<Attributo> Attributes { get; set; }
        }

        #endregion

        #region Nested type: DimensionInfo

        /// <summary>
        /// represents a dimension
        /// </summary>
        public class DimensionInfo
        {
            public string Caption { get; set; }
            public string Expression { get; set; }
            public int LevelCount { get; set; }
        }

        #endregion

        #region Nested type: Dimensions

        /// <summary>
        /// represents a dimension
        /// </summary>
        public class Dimensions
        {
            public string Caption { get; set; }
            public string Expression { get; set; }
        }

        #endregion

        #region Nested type: KpiInfo

        public class KpiInfo
        {
            public string Name { get; set; }
            public string Goal { get; set; }
            public string Status { get; set; }
            public string Trend { get; set; }
            public string Caption { get; set; }
            public string Value { get; set; }
            public string DisplayFolder { get; set; }
            public string Weight { get; set; }
            public string Pkn { get; set; }
            public string StatusGraphic { get; set; }
            public string TrendGraphic { get; set; }
            public string kpiMDX { get; set; }
        }

        #endregion

        #region Nested type: MeasureInfo

        /// <summary>
        /// Represents a Measure
        /// </summary>
        public class MeasureInfo
        {
            public string Caption { get; set; }
            public string Expression { get; set; }
        }

        #endregion

        #region Nested type: MemberInfo

        /// <summary>
        /// Represents a Member
        /// </summary>
        public class MemberInfo
        {
            public string Caption { get; set; }
            public string Expression { get; set; }
        }

        #endregion

        public static string GetDate(string mdx)
        {
            string data = String.Empty;
            
            try
            {
                CellStone cellstone = GetCellStone(mdx);
                if (cellstone.Rows.Count > 1)
                {
                    if (cellstone.Rows[1].Columns.Count > 0)
                    {
                        data = cellstone.Rows[1].Columns[0].Caption;
                    }
                }
            }
            catch (Exception e)
            {
                data = e.Message + mdx;
            }
            
            return data;
        }

        public static CellStone GetCellStone(string mdx)
        {
            CellStone result = new CellStone();
            using (AdomdConnection conn = new AdomdConnection(connectionString))
            {
                conn.Open();
                AdomdCommand command = new AdomdCommand(mdx, conn);
                List<int> ordinals = new List<int>();
                using (AdomdDataReader reader = command.ExecuteReader())
                {
                    CellSetRow header = new CellSetRow();
                    for (int i = 0; i < reader.FieldCount-1; i++)
                    {
                        String currentCaption = reader.GetName(i).Substring(0, reader.GetName(i).LastIndexOf("["));
                        String nextCaption = reader.GetName(i+1).Substring(0, reader.GetName(i+1).LastIndexOf("["));
                        if (!currentCaption.Substring(0, currentCaption.LastIndexOf("[")).Equals(nextCaption.Substring(0, nextCaption.LastIndexOf("["))) || currentCaption=="[Measures].")
                        {
                            ordinals.Add(i);
                            header.Columns.Add(new Cell { Caption = reader.GetName(i), CellType = currentCaption=="[Measures]."?EnuCellType.CellValue:EnuCellType.Axis, UniqueName = reader.GetName(i) });
                        }
                    }
                    ordinals.Add(reader.FieldCount-1);
                    header.Columns.Add(new Cell { Caption = reader.GetName(reader.FieldCount - 1), CellType = EnuCellType.Axis, UniqueName = reader.GetName(reader.FieldCount - 1) });
                        

                    result.Rows.Add(header);
                    
                    while (reader.Read())
                    {
                        CellSetRow row = new CellSetRow();
                        foreach (int i in ordinals)
                        {
                            Cell cell = new Cell { CellType = EnuCellType.CellValue };
                            if (!reader.IsDBNull(i))
                            {
                                cell.Caption = reader.GetString(i);
                                cell.UniqueName = reader.GetString(i);
                            }
                            else
                            {
                                cell.Caption = "0";
                                cell.UniqueName = "0";
                            }
                            row.Columns.Add(cell);
                        }
                        bool containsUnknownMember = row.Columns.Any(cell => cell.Caption.Equals("Unknown"));
                        if (!containsUnknownMember)
                            result.Rows.Add(row);
                    }
                }
                conn.Close();
            }
            return result;
        }

    }
}