﻿using System.Collections.Generic;
using Microsoft.AnalysisServices.AdomdClient;

namespace TBridge.Cemi.Cruscotto.CubeBrowserService
{
    public class CubeHelper
    {
        private readonly AdomdConnection conn;
        private string connString;
        private bool isOpen;

        public CubeHelper()
        {
            conn = new AdomdConnection();
        }

        public CubeHelper(string connectionString)
        {
            connString = connectionString;
            conn = new AdomdConnection();
            conn.ConnectionString = connectionString;
        }

        public AdomdConnection Connection { get; set; }

        public bool IsOpen
        {
            get { return isOpen; }
        }

        public string ConnectionString
        {
            get { return connString; }
            set
            {
                connString = value;
                conn.ConnectionString = connString;
            }
        }

        public void OpenConnection()
        {
            conn.Open();
            isOpen = true;
        }

        public void CloseConnection()
        {
            conn.Close();
        }

        public static List<Dimension> CubesDimensions(CubeDef cube)
        {
            List<Dimension> result = new List<Dimension>();

            foreach (Dimension item in cube.Dimensions)
            {
                result.Add(item);
            }

            return result;
        }

        public static List<Measure> CubesMeasure(CubeDef cube)
        {
            List<Measure> result = new List<Measure>();

            foreach (Measure item in cube.Measures)
            {
                result.Add(item);
            }

            return result;
        }

        public static List<Kpi> Cubeskpi(CubeDef cube)
        {
            List<Kpi> result = new List<Kpi>();

            foreach (Kpi item in cube.Kpis)
            {
                result.Add(item);
            }

            return result;
        }

        public static List<Hierarchy> CubesHiberarchies(CubeDef cube)
        {
            List<Hierarchy> result = new List<Hierarchy>();

            foreach (Dimension item in cube.Dimensions)
            {
                foreach (Hierarchy hierarchy in item.Hierarchies)
                {
                    result.Add(hierarchy);
                }
            }
            return result;
        }

        public static List<Hierarchy> CubesHiberarchies(CubeDef cube, string dimension)
        {
            List<Hierarchy> result = new List<Hierarchy>();

            foreach (Dimension item in cube.Dimensions)
            {
                if (item.Caption.Equals(dimension))
                    foreach (Hierarchy hierarchy in item.Hierarchies)
                    {
                        result.Add(hierarchy);
                    }
            }
            return result;
        }

        public static List<Member> CubesHiberarchyMembers(CubeDef cube, string dimension, string hierarchy)
        {
            List<Member> result = new List<Member>();

            foreach (Dimension item in cube.Dimensions)
            {
                if (item.Caption.Equals(dimension))
                {
                    foreach (Hierarchy hier in item.Hierarchies)
                    {
                        if (hier.Name.Equals(hierarchy))
                        {
                            foreach (Member member in hier.Levels[0].GetMembers()[0].GetChildren())
                            {
                                result.Add(member);
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            return result;
        }

        public static List<CubeDef> CubesCube(CubeCollection cubes)
        {
            List<CubeDef> result = new List<CubeDef>();

            foreach (CubeDef cub in cubes)
            {
                if (cub.Type.Equals(CubeType.Cube))
                {
                    result.Add(cub);
                }
            }
            return result;
        }

        public static CubeDef GetCubeByName(string cubename, CubeCollection cubes)
        {
            foreach (CubeDef cube in CubesCube(cubes))
            {
                if (cube.Caption == cubename)
                {
                    return cube;
                }
            }

            return null;
        }
    }
}