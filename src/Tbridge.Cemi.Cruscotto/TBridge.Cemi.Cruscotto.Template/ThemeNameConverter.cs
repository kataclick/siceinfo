﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ThemeNameConverter : IValueConverter
    {
        // Methods

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (string.IsNullOrEmpty(value as string))
            {
                return "Themes";
            }
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}