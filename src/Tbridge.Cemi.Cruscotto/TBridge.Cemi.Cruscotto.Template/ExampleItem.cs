﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ExampleItem : NotifyPropertyChangedBase, IExampleItem
    {
        // Fields
        [CompilerGenerated] private ExamplesCollection _Children;
        private string _CodicePrestazione; 
        private string _LastUpdate;
        [CompilerGenerated] private string _CommonFilesFolder;
        [CompilerGenerated] private string _IconUrl;
        [CompilerGenerated] private bool _IsDefault;
        [CompilerGenerated] private bool _IsExample;
        [CompilerGenerated] private bool _IsFeatured;
        [CompilerGenerated] private bool _IsHomepage;
        [CompilerGenerated] private string _Keywords;
        [CompilerGenerated] private IExampleItem _Parent;
        [CompilerGenerated] private string _Path;
        [CompilerGenerated] private string _Text;
        private string _TitoloPagina;
        private string filter;
        private bool isExpanded;
        private bool isSelected;
        private bool visible;

        private bool HasVisibleChildren
        {
            get
            {
                foreach (ExampleItem item in Children)
                {
                    if (item.Visible)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        // Properties

        #region IExampleItem Members

        public IExampleItem CategoryFirstLookExample
        {
            get
            {
                foreach (ExampleItem item in Children)
                {
                    if (item.Visible)
                    {
                        return item;
                    }
                }
                return this;
            }
        }

        public ExamplesCollection Children
        {
            [CompilerGenerated]
            get { return _Children; }
            set { _Children = value; }
        }

        public string CommonFilesFolder
        {
            [CompilerGenerated]
            get { return _CommonFilesFolder; }
            set { _CommonFilesFolder = value; }
        }

        public ImageSource ExampleImage
        {
            get
            {
                return
                    new BitmapImage(
                        new Uri(
                            string.Format(CultureInfo.InvariantCulture, "/{0};component/Images/Products/Big/{1}.png",
                                          new object[]
                                              {
                                                  ApplicationHelper.ExampleApplicationAssemblyName,
                                                  Path.Split(new char[] {'/'})[0]
                                              }), UriKind.Relative));
            }
        }


        public string Filter
        {
            get { return filter; }
            set
            {
                if (filter != value)
                {
                    filter = value;
                    OnPropertyChanged("Visible");
                }
            }
        }

        public bool HasElements
        {
            get { return (Children.Count > 0); }
        }

        public ImageSource Icon
        {
            get
            {
                if (!string.IsNullOrEmpty(IconUrl))
                {
                    return new BitmapImage(new Uri(IconUrl, UriKind.Relative));
                }
                return null;
            }
        }

        public string IconUrl
        {
            [CompilerGenerated]
            get { return _IconUrl; }
            set { _IconUrl = value; }
        }

        public bool IsDefault
        {
            [CompilerGenerated]
            get { return _IsDefault; }
            set { _IsDefault = value; }
        }

        public bool IsExample
        {
            [CompilerGenerated]
            get { return _IsExample; }
            set { _IsExample = value; }
        }

        public bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                if (isExpanded != value)
                {
                    if (value && (Parent != null))
                    {
                        Parent.IsExpanded = true;
                    }
                    isExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }
            }
        }

        public bool IsFeatured
        {
            [CompilerGenerated]
            get { return _IsFeatured; }
            set { _IsFeatured = value; }
        }

        public bool IsHomepage
        {
            [CompilerGenerated]
            get { return _IsHomepage; }
            set { _IsHomepage = value; }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected != value)
                {
                    if (value)
                    {
                        IsExpanded = true;
                    }
                    isSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        public string Keywords
        {
            [CompilerGenerated]
            get { return _Keywords; }
            set { _Keywords = value; }
        }

        public IExampleItem Parent
        {
            [CompilerGenerated]
            get { return _Parent; }
            set { _Parent = value; }
        }

        public string CodicePrestazione
        {
            get { return _CodicePrestazione; }
            set { _CodicePrestazione = value; }
        }

        public string LastUpdate
        {
            get { return _LastUpdate; }
            set { _LastUpdate = value; }
        }

        public string TitoloPagina
        {
            get { return _TitoloPagina; }
            set { _TitoloPagina = value; }
        }

        public string Path
        {
            [CompilerGenerated]
            get { return _Path; }
            [CompilerGenerated]
            set { _Path = value; }
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "Used for binding")]
        public string Platform
        {
            get { return "for Silverlight"; }
        }

        public string ShortTypeName
        {
            get
            {
                if (Path.Contains("GridView"))
                {
                    return (ApplicationHelper.ExampleApplicationRootNamespace + "." + Path.Replace('/', '.') + ".Page");
                }
                return (ApplicationHelper.ExampleApplicationRootNamespace + ".Pages" + "." + Path.Replace('/', '.') +
                        ".Page");
            }
        }

        public string Text
        {
            [CompilerGenerated]
            get { return _Text; }
            set { _Text = value; }
        }

        public bool Visible
        {
            get
            {
                if (IsExample)
                {
                    bool flag = (string.IsNullOrEmpty(Filter) ||
                                 (Text.IndexOf(Filter, StringComparison.OrdinalIgnoreCase) > -1)) ||
                                (Keywords.IndexOf(Filter, StringComparison.OrdinalIgnoreCase) > -1);
                    return (visible = flag);
                }
                return (visible = HasVisibleChildren);
            }
            set
            {
                if (value != visible)
                {
                    visible = value;
                }
            }
        }

        #endregion

        internal void ApplyFilter(string value)
        {
            Filter = value;
            foreach (ExampleItem item in Children)
            {
                item.ApplyFilter(value.Trim());
            }
            OnPropertyChanged("Visible");
        }

        private static bool GetAttributeValue(XElement element, string name, bool defaultValue)
        {
            if (element.Attribute((XName) name) != null)
            {
                return Convert.ToBoolean(element.Attribute((XName) name).Value, CultureInfo.InvariantCulture);
            }
            return defaultValue;
        }

        private static string GetAttributeValue(XElement element, string name, string defaultValue)
        {
            if (element.Attribute((XName) name) != null)
            {
                return element.Attribute((XName) name).Value;
            }
            return defaultValue;
        }

        private static bool GetIsHomeExample(XElement element, IExampleItem exampleItem)
        {
            while (((element != null) && (exampleItem != null)) && exampleItem.IsExample)
            {
                element = element.Parent;
                exampleItem = exampleItem.Parent;
            }
            return GetAttributeValue(element, "IsHomeExample", false);
        }

        public static ExamplesCollection SelectAll(XElement root)
        {
            List<IExampleItem> listItems = SelectAll(root, null);
            listItems.Sort(delegate(IExampleItem example1, IExampleItem example2)
                               {
                                   int num = Comparer<string>.Default.Compare(example1.Text, example2.Text);
                                   if (num != 0)
                                   {
                                       if (example1.Text.Equals("Assistenziali", StringComparison.OrdinalIgnoreCase))
                                       {
                                           return -1;
                                       }
                                       if (example2.Text.Equals("Assistenziali", StringComparison.OrdinalIgnoreCase))
                                       {
                                           return 1;
                                       }
                                   }
                                   return num;
                               });
            return new ExamplesCollection(listItems);
        }

        private static List<IExampleItem> SelectAll(XElement root, IExampleItem parent)
        {
            List<IExampleItem> items = new List<IExampleItem>();
            root.Elements().ForEach<XElement>(delegate(XElement element)
                                                  {
                                                      if (GetAttributeValue(element, "visible", true))
                                                      {
                                                          ExampleItem item;
                                                          item = new ExampleItem
                                                                     {
                                                                         Path =
                                                                             GetAttributeValue(element, "path",
                                                                                               (string) null) ??
                                                                             GetAttributeValue(element, "name",
                                                                                               string.Empty),
                                                                         Text =
                                                                             GetAttributeValue(element, "text",
                                                                                               string.Empty),
                                                                         IsFeatured =
                                                                             GetAttributeValue(element, "featured",
                                                                                               false),
                                                                         IsDefault =
                                                                             GetAttributeValue(element, "default", false),
                                                                         IsExample = element.Name.LocalName == "example",
                                                                         CommonFilesFolder =
                                                                             GetAttributeValue(element, "commonFiles",
                                                                                               string.Empty),
                                                                         Parent = parent
                                                                     };
                                                          item.IconUrl = GetAttributeValue(element, "IconUrl",
                                                                                           item.Path + "/Image.png");
                                                          item.IsHomepage = GetIsHomeExample(element, item);
                                                          item.CodicePrestazione = GetAttributeValue(element,
                                                                                                     "codicePrestazione",
                                                                                                     "");
                                                          item.TitoloPagina = GetAttributeValue(element, "titoloPagina",
                                                                                                "");
                                                          item.Keywords = SelectKeyWards(item, element);
                                                          item.Children =
                                                              new ExamplesCollection(SelectAll(element, item));

                                                          items.Add(item);
                                                      }
                                                  });
            return items;
        }


        private static string SelectKeyWards(IExampleItem currentItem, XElement element)
        {
            string str = GetAttributeValue(element, "KeyWords", string.Empty);
            if (currentItem.Parent != null)
            {
                currentItem = currentItem.Parent;
                str = string.Format("{0},{1},{2}", str, currentItem.Keywords, currentItem.Text);
            }
            return str;
        }
    }
}