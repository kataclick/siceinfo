﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Browser;

namespace TBridge.Cemi.Cruscotto.Template
{
    internal class BrowserHelper
    {
        // Methods

        // Properties
        internal static Uri GetCurrentBrowserUri
        {
            get { return Application.Current.Host.Source; }
        }

        internal static string PathToExampleXML
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "/{0};component/Prestazioni.xml",
                                     new object[] {ApplicationHelper.ExampleApplicationAssemblyName});
            }
        }

        internal static string ApplicationAssemblyName
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "/{0};",
                                     new object[] {ApplicationHelper.ExampleApplicationAssemblyName});
            }
        }

        internal static string GetCurrentBookmark(string defaultValue)
        {
            string currentBookmark = HtmlPage.Window.CurrentBookmark;
            if (!string.IsNullOrEmpty(currentBookmark))
            {
                return currentBookmark;
            }
            currentBookmark = HtmlPage.Document.DocumentUri.Query;
            if (string.IsNullOrEmpty(currentBookmark))
            {
                return defaultValue;
            }
            return currentBookmark.Substring(1);
        }

        internal static void SetBrowserTitle(string title)
        {
            string code = string.Format(CultureInfo.InvariantCulture, "window.document.title='{0}'",
                                        new object[] {title});
            HtmlPage.Window.Eval(code);
        }
    }
}