﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ExamplesViewModel : NotifyPropertyChangedBase
    {
        // Fields
        private ExamplesCollection examples;
        private string searchText;
        private IExampleItem selectedExample;

        public string BrowserTitle
        {
            get { return GetTitle(SelectedExample); }
        }

        public ExamplesCollection Examples
        {
            get
            {
                if (examples == null)
                {
                    examples = LoadExamples();
                }
                return examples;
            }
        }

        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (searchText != value)
                {
                    searchText = value;
                    OnPropertyChanged("SearchText");
                    Examples.ExpandedAll(!string.IsNullOrEmpty(searchText));
                    Examples.ApplyFilter(searchText);
                }
            }
        }

        public string PathToXML { get; set; }

        public string LastUpadte { get; set; }

        public IExampleItem SelectedExample
        {
            get { return selectedExample; }
            set
            {
                if (!value.IsExample && (value.Parent == null))
                {
                    value = value.CategoryFirstLookExample;
                }
                if (selectedExample != value)
                {
                    selectedExample = value;
                    if (selectedExample != null)
                    {
                        OnSelectedExampleChanged();
                    }
                    OnPropertyChanged("SelectedExample");
                }
            }
        }


        // Events
        public event EventHandler SelectedExampleChanged;


        private IExampleItem FindExample(IEnumerable<IExampleItem> collection, Predicate<IExampleItem> condition)
        {
            foreach (IExampleItem item in collection)
            {
                if (condition(item))
                {
                    return item;
                }
                IExampleItem item2 = FindExample((IEnumerable<IExampleItem>) item.Children, condition);
                if (item2 != null)
                {
                    return item2;
                }
            }
            return null;
        }

        //Qui!!!
        public IExampleItem FindExampleBypath(string path)
        {
            return FindExample((IEnumerable<IExampleItem>) Examples,
                               delegate(IExampleItem item) { return item.Path == path; });
        }

        public IExampleItem FindExampleByCode(string codicePrestazione)
        {
            return FindExample((IEnumerable<IExampleItem>) Examples,
                               delegate(IExampleItem item) { return item.CodicePrestazione == codicePrestazione; });
        }

        private string GetTitle(IExampleItem item)
        {
            string titleText = string.Empty;
            if (item.IsHomepage)
            {
                return string.Format(CultureInfo.InvariantCulture, "Highlights of RadControls {0}",
                                     new object[] {item.Platform});
            }
            string exampleTitle = string.Empty;
            titleText = GetTitleText(item, ref exampleTitle);
            return string.Format(CultureInfo.InvariantCulture, "{0}", new object[] {titleText});
        }

        private string GetTitleText(IExampleItem item, ref string exampleTitle)
        {
            if (item.Parent != null)
            {
                exampleTitle = string.Format(" - {0}{1}", item.Text, exampleTitle);
                return GetTitleText(item.Parent, ref exampleTitle);
            }
            return item.Text;
        }

        internal ExamplesCollection LoadExamples()
        {
            Uri uriResource = new Uri(String.Format("{0}{1}", BrowserHelper.ApplicationAssemblyName, PathToXML),
                                      UriKind.Relative);
            StreamResourceInfo resourceStream = Application.GetResourceStream(uriResource);
            if (resourceStream == null)
            {
                throw new MissingManifestResourceException("Cannot find Examples.xml");
            }
            return ExampleItem.SelectAll(XDocument.Load(new StreamReader(resourceStream.Stream)).Element("examples"));
        }

        protected virtual void OnSelectedExampleChanged()
        {
            SelectedExampleChanged(this, EventArgs.Empty);
        }

        public void SelectFirstVisibleExample()
        {
            IExampleItem it = FindExample(Examples,
                                          delegate(IExampleItem item) { return item.IsExample && item.Visible; });
            SearchText = string.Empty;
            SelectedExample = it;
        }

        // Properties
    }
}