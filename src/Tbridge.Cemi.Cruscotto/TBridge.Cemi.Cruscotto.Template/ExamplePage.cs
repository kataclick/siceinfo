﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public partial class ExamplePage : BasePage
    {
        // Fields

        public static readonly DependencyProperty BottomRightPanelProperty =
            DependencyProperty.Register("BottomRightPanel", typeof (Panel), typeof (ExamplePage), null);

        public static readonly DependencyProperty CodePanelProperty = DependencyProperty.Register("codeViewerPanel",
                                                                                                  typeof (Panel),
                                                                                                  typeof (ExamplePage),
                                                                                                  null);

        public static readonly DependencyProperty DropDownProperty = DependencyProperty.Register("DropDown",
                                                                                                 typeof (RadComboBox),
                                                                                                 typeof (ExamplePage),
                                                                                                 null);

        public static readonly DependencyProperty ExampleImageProperty = DependencyProperty.Register("ExampleImage",
                                                                                                     typeof (ImageSource
                                                                                                         ),
                                                                                                     typeof (ExamplePage
                                                                                                         ), null);

        public static readonly DependencyProperty ExampleTitleProperty = DependencyProperty.Register("ExampleTitle",
                                                                                                     typeof (string),
                                                                                                     typeof (ExamplePage
                                                                                                         ), null);

        public static readonly DependencyProperty IsDescriptionViewProperty =
            DependencyProperty.Register("IsDescriptionView", typeof (bool), typeof (ExamplePage), null);

        public static readonly DependencyProperty OutputProperty = DependencyProperty.Register("Output", typeof (string),
                                                                                               typeof (ExamplePage),
                                                                                               null);

        public static readonly DependencyProperty PreviewControlProperty = DependencyProperty.Register(
            "PreviewControl", typeof (Panel), typeof (ExamplePage), null);

        public static readonly DependencyProperty RightPanelVisibilityProperty =
            DependencyProperty.Register("RightPanelVisibility", typeof (Visibility), typeof (ExamplePage),
                                        new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty TopRightPanelProperty = DependencyProperty.Register("TopRightPanel",
                                                                                                      typeof (Panel),
                                                                                                      typeof (
                                                                                                          ExamplePage),
                                                                                                      null);


        // Methods
        public ExamplePage()
        {
            string str = "SL";
            string uriString = string.Format("TBridge.Cemi.Cruscotto.Template;component/themes/ExamplePage_{0}.xaml", str);
            Application.LoadComponent(this, new Uri(uriString, UriKind.Relative));
        }

        // Properties
        internal ContentControl BottomPanel
        {
            get
            {
                ContentControl control = base.FindName("bottomPanel") as ContentControl;
                if (control == null)
                {
                    throw new NotImplementedException("not find element with that name");
                }
                return control;
            }
        }

        public Panel BottomRightPanel
        {
            get { return (Panel) base.GetValue(BottomRightPanelProperty); }
            set { base.SetValue(BottomRightPanelProperty, value); }
        }

        public Grid CodeViewer
        {
            get
            {
                Grid viewer = base.FindName("codeViewerPanel") as Grid;
                if (viewer == null)
                {
                    throw new NotImplementedException("not find element with that name");
                }
                return viewer;
            }
        }

        public RadComboBox DropDown
        {
            get { return (RadComboBox) base.GetValue(DropDownProperty); }
            set { base.SetValue(DropDownProperty, value); }
        }

        internal ContentControl ExampleArea
        {
            get
            {
                ContentControl control = base.FindName("exampleArea") as ContentControl;
                if (control == null)
                {
                    throw new NotImplementedException("not find element with that name");
                }
                return control;
            }
        }

        public ImageSource ExampleImage
        {
            get { return (ImageSource) base.GetValue(ExampleImageProperty); }
            set { base.SetValue(ExampleImageProperty, value); }
        }


        internal Border ExamplePanel
        {
            get
            {
                Border border = base.FindName("exampleblock") as Border;
                return (border ?? new Border());
            }
        }

        public string ExampleTitle
        {
            get { return (string) base.GetValue(ExampleTitleProperty); }
            set { base.SetValue(ExampleTitleProperty, value); }
        }

        public bool IsDescriptionView
        {
            get { return (bool) base.GetValue(IsDescriptionViewProperty); }
            set { base.SetValue(IsDescriptionViewProperty, value); }
        }

        public string Output
        {
            get { return (string) base.GetValue(OutputProperty); }
            set { base.SetValue(OutputProperty, value); }
        }

        public Panel PreviewControl
        {
            get { return (Panel) base.GetValue(PreviewControlProperty); }
            set { base.SetValue(PreviewControlProperty, value); }
        }

        internal ContentControl RightPanel
        {
            get
            {
                ContentControl control = base.FindName("rightPanel") as ContentControl;
                if (control == null)
                {
                    throw new NotImplementedException("not find element with that name");
                }
                return control;
            }
        }

        public Visibility RightPanelVisibility
        {
            get { return (Visibility) base.GetValue(RightPanelVisibilityProperty); }
            set { base.SetValue(RightPanelVisibilityProperty, value); }
        }

        //internal RadComboBox ThemesComboBox
        //{
        //    get
        //    {
        //        RadComboBox box = base.FindName("themesComboBox") as RadComboBox;
        //        if (box == null)
        //        {
        //            throw new NotImplementedException("not find element with that name");
        //        }
        //        return box;
        //    }
        //}

        public Panel TopRightPanel
        {
            get { return (Panel) base.GetValue(TopRightPanelProperty); }
            set { base.SetValue(TopRightPanelProperty, value); }
        }

        public Panel CodePanel
        {
            get { return (Panel) base.GetValue(CodePanelProperty); }
            set { base.SetValue(CodePanelProperty, value); }
        }

        private static MainPage GetMainPage(DependencyObject example)
        {
            for (DependencyObject obj2 = VisualTreeHelper.GetParent(example);
                 obj2 != null;
                 obj2 = VisualTreeHelper.GetParent(obj2))
            {
                MainPage page = obj2 as MainPage;
                if (page != null)
                {
                    return page;
                }
            }
            return null;
        }


        protected override void OnCurrentExampleChanged(object oldValue, object newValue)
        {
            Example example = newValue as Example;
            if (example != null)
            {
                Panel topRightPanel = example.TopRightPanel;
                Panel bottomRightPanel = example.BottomRightPanel;
                ExampleArea.Content = example;
                if (topRightPanel != null)
                {
                    RightPanel.Content = topRightPanel;
                    if (example.TopRightPanel.DataContext == null)
                    {
                        example.TopRightPanel.DataContext = example.DataContext;
                    }
                }
                if (bottomRightPanel != null)
                {
                    BottomPanel.Content = bottomRightPanel;
                    if (bottomRightPanel.DataContext == null)
                    {
                        bottomRightPanel.DataContext = example.DataContext;
                    }
                }
                //this.ThemesComboBox.Visibility = example.ThemeComboBoxVisibility;
                ExamplePanel.Visibility = ((bottomRightPanel == null) && (topRightPanel == null))
                                              ? Visibility.Collapsed
                                              : Visibility.Visible;
            }
        }

        protected override void OnExampleItemChanged(object oldItem, object newItem)
        {
            base.NavigationIdentifier = ((ExampleItem) newItem).Path;
            base.DataContext = newItem;
        }

        private void OnPreviewButtonLoaded(object sender, RoutedEventArgs e)
        {
            if (ExampleItem.Text.Equals("Home"))
            {
                ((ToggleButton) sender).Visibility = Visibility.Collapsed;
            }

        }

        private void OnPreviewButtonClick(object sender, RoutedEventArgs e)
        {
            if (!ExampleItem.Text.Equals("Home"))
            {
                IsDescriptionView = !IsDescriptionView;
                string str = IsDescriptionView ? "hideExamplePanel" : "showExamplePanel";
                if (IsDescriptionView)
                {
                    CodeViewer.ToString();
                }
                base.SelectedExample.OnSwitchCodeArea(IsDescriptionView, CodeViewer);
                Storyboard storyboard = base.Resources[str] as Storyboard;
                if (storyboard != null)
                {
                    storyboard.Begin();
                }
                ImageSourceConverter imgs = new ImageSourceConverter();
                //ToggleButton printButton = base.FindName("printButton") as ToggleButton;

                ToggleButton previewButton = base.FindName("previewButton") as ToggleButton;
                Image img = (Image) previewButton.Content;
                ToggleButton exportButton = base.FindName("exportButton") as ToggleButton;
                if (IsDescriptionView)
                {
                    //printButton.Visibility = Visibility.Collapsed;
                    exportButton.Visibility = Visibility.Visible;
                    img.SetValue(Image.SourceProperty,
                                 imgs.ConvertFromString("/TBridge.Cemi.Cruscotto.Template;component/images/chart-icon.png"));
                    previewButton.SetValue(ToolTipService.ToolTipProperty, "Visualizza i grafici");
                }
                else
                {
                    //printButton.Visibility = Visibility.Visible;
                    exportButton.Visibility = Visibility.Collapsed;
                    img.SetValue(Image.SourceProperty,
                                 imgs.ConvertFromString("/TBridge.Cemi.Cruscotto.Template;component/images/GridView.png"));
                    previewButton.SetValue(ToolTipService.ToolTipProperty, "Visualizza le tabelle");
                }
            }
            // /TBridge.Cemi.Cruscotto.Template;component/images/GridView.png;
            //img.SetValue(Image.SourceProperty, "/TBridge.Cemi.Cruscotto.Template;component/images/GridView.png");
        }

        private void OnExportButtonClick(object sender, RoutedEventArgs e)
        {
            base.SelectedExample.OnExport();
        }

        private void OnPrintButtonClick(object sender, RoutedEventArgs e)
        {
            base.SelectedExample.OnPrint();
        }

        public void ThemeChanged(object sender, PropertyChangedEventArgs args)
        {
            GetMainPage(this).Navigate(base.ExampleItem);
        }
    }
}