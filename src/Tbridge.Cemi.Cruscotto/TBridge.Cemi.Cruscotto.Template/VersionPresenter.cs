﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class VersionPresenter : Control
    {
        // Fields
        public static readonly DependencyProperty LoadedExampleProperty;
        public static readonly DependencyProperty VersionTextProperty;

        // Methods
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
        static VersionPresenter()
        {
            int num3;
            int num4;
            string[] strArray =
                typeof (StyleManager).AssemblyQualifiedName.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)[2]
                    .Trim().Split(new[] {'.'});
            int num = Convert.ToInt32(strArray[1], CultureInfo.InvariantCulture);
            int year = Convert.ToInt32(strArray[0].Remove(0, "Version=".Length), CultureInfo.InvariantCulture);
            if (strArray[2].Length > 3)
            {
                num3 = Convert.ToInt32(strArray[2].Substring(0, 2), CultureInfo.InvariantCulture);
                num4 = Convert.ToInt32(strArray[2].Substring(2), CultureInfo.InvariantCulture);
            }
            else
            {
                num3 = Convert.ToInt32(strArray[2].Substring(0, 1), CultureInfo.InvariantCulture);
                num4 = Convert.ToInt32(strArray[2].Substring(1), CultureInfo.InvariantCulture);
            }
            if (num3 > 12)
            {
                num3 -= 12;
                year++;
            }
            string str2 = (Convert.ToInt32(strArray[3], CultureInfo.InvariantCulture) > 0x3e8) ? "Silverlight" : "WPF";
            string defaultValue = string.Format(CultureInfo.InvariantCulture,
                                                "RadControls for {0} Q{1} SP2 {2}, released on {3:dd MMMM yyyy}",
                                                new object[] {str2, num, year, new DateTime(year, num3, num4)});
            VersionTextProperty = DependencyProperty.Register("VersionText", typeof (string), typeof (VersionPresenter),
                                                              new PropertyMetadata(defaultValue));
            LoadedExampleProperty = DependencyProperty.Register("LoadedExample", typeof (string),
                                                                typeof (VersionPresenter),
                                                                new PropertyMetadata(
                                                                    new PropertyChangedCallback(
                                                                        OnLoadExamplePropertyChanged)));
        }

        public VersionPresenter()
        {
            base.DefaultStyleKey = typeof (VersionPresenter);
        }

        // Properties
        public string LoadedExample
        {
            get { return (string) base.GetValue(LoadedExampleProperty); }
            set { base.SetValue(LoadedExampleProperty, value); }
        }

        public string VersionText
        {
            get { return (string) base.GetValue(VersionTextProperty); }
            set { base.SetValue(VersionTextProperty, value); }
        }

        private static void OnLoadExamplePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
        }
    }
}