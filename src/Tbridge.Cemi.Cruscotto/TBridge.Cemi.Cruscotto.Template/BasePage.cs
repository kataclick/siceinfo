﻿using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class BasePage : UserControl, IFrame
    {
        // Fields

        public static readonly DependencyProperty ExampleItemProperty = DependencyProperty.Register("ExampleItem",
                                                                                                    typeof (IExampleItem
                                                                                                        ),
                                                                                                    typeof (BasePage),
                                                                                                    new PropertyMetadata
                                                                                                        (new PropertyChangedCallback
                                                                                                             (ExampleItemChanged)));

        internal static readonly DependencyProperty SelectedtExampleProperty =
            DependencyProperty.Register("SelectedExample", typeof (Example), typeof (BasePage),
                                        new PropertyMetadata(new PropertyChangedCallback(OnItemChanged)));

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof (string),
                                                                                              typeof (ExamplePage), null);

        [CompilerGenerated] private Panel _ParentContainer;
        private string hash;

        public IExampleItem ExampleItem
        {
            get { return (IExampleItem) base.GetValue(ExampleItemProperty); }
            set { base.SetValue(ExampleItemProperty, value); }
        }

        public string Path
        {
            get { return base.GetType().FullName; }
        }

        internal Example SelectedExample
        {
            get { return (Example) base.GetValue(SelectedtExampleProperty); }
            set { base.SetValue(SelectedtExampleProperty, value); }
        }

        #region IFrame Members

        public string NavigationIdentifier
        {
            get
            {
                if (string.IsNullOrEmpty(hash))
                {
                    return Path;
                }
                return hash;
            }
            set { hash = value; }
        }

        public Panel ParentContainer
        {
            [CompilerGenerated]
            get { return _ParentContainer; }
            [CompilerGenerated]
            set { _ParentContainer = value; }
        }

        public string Title
        {
            get { return (string) base.GetValue(TitleProperty); }
            set { base.SetValue(TitleProperty, value); }
        }

        #endregion

        // Methods
        private static void ExampleItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as BasePage).OnExampleItemChanged(e.OldValue, e.NewValue);
        }

        internal static BasePage GetValidPage(IExampleItem exampleItem)
        {
            if (exampleItem.IsHomepage)
            {
                return new Homepage();
            }
            return new ExamplePage();
        }

        protected virtual void OnCurrentExampleChanged(object oldValue, object newValue)
        {
        }

        protected virtual void OnExampleItemChanged(object oldItem, object newItem)
        {
        }

        private static void OnItemChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as BasePage).OnCurrentExampleChanged(e.OldValue, e.NewValue);
        }

        // Properties
    }
}