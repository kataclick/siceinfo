﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        // Methods

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (((bool) value) ? Visibility.Visible : Visibility.Collapsed);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}