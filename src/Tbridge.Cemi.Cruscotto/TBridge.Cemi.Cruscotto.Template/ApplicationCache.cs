﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using TBridge.Cemi.Cruscotto.Template;

namespace TBridge.Cemi.Cruscotto.Template
{
    public static class ApplicationCache
    {
        // Fields
        [CompilerGenerated] private static IDictionary<string, string> _AppCache;

        // Methods
        static ApplicationCache()
        {
            AppCache = new Dictionary<string, string>();
        }

        private static IDictionary<string, string> AppCache
        {
            [CompilerGenerated]
            get { return _AppCache; }
            [CompilerGenerated]
            set { _AppCache = value; }
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "value")]
        public static bool IsLoadedAssembly(string value)
        {
            return
                AppCache.Any(
                    delegate(KeyValuePair<string, string> keyPair) { return string.Equals(keyPair.Value, value, StringComparison.OrdinalIgnoreCase); });
        }

        public static void SaveAssemblyInCache(Assembly assembly)
        {
            if (!AppCache.Keys.Contains(assembly.FullName))
            {
                AppCache.Add(assembly.FullName, assembly.FullName.Split(new[] {','})[0]);
            }
        }

        public static bool TryLoadingType(string type, out Example resultObject)
        {
            resultObject = null;
            Type type2 =
                Type.GetType(
                    ApplicationHelper.ApplicationType.AssemblyQualifiedName.Replace(
                        ApplicationHelper.ApplicationType.FullName, type));
            if (type2 == null)
            {
                foreach (KeyValuePair<string, string> pair in AppCache)
                {
                    type2 = Type.GetType(string.Format("{0},{1}", type, pair.Key));
                    if (type2 != null)
                    {
                        break;
                    }
                }
            }
            if (type2 == null)
            {
                return false;
            }
            resultObject = Activator.CreateInstance(type2) as Example;
            return true;
        }

        // Properties
    }
}