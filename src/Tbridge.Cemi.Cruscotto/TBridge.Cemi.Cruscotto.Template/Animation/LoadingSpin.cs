﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Animation;

namespace TBridge.Cemi.Cruscotto.Template.Animation
{
    public partial class LoadingSpin
    {
        // Fields
        private List<Storyboard> animations;
        private Storyboard intervalTimer;
        private int nextAnimation;

        // Methods
        public LoadingSpin()
        {
            InitializeComponent();
            Storyboard storyboard = new Storyboard();
            storyboard.Duration = new Duration(TimeSpan.FromMilliseconds(100.0));
            intervalTimer = storyboard;
            intervalTimer.Completed += new EventHandler(OnIntervalTimerCompleted);
            List<Storyboard> list = new List<Storyboard>();
            list.Add(CycleAnimation0);
            list.Add(CycleAnimation1);
            list.Add(CycleAnimation2);
            list.Add(CycleAnimation3);
            list.Add(CycleAnimation4);
            list.Add(CycleAnimation5);
            list.Add(CycleAnimation6);
            list.Add(CycleAnimation7);
            list.Add(CycleAnimation8);
            list.Add(CycleAnimation9);
            animations = list;
        }

        public void Begin()
        {
            base.Visibility = Visibility.Visible;
            AppearAnimation.Begin();
            intervalTimer.Begin();
        }


        private void OnIntervalTimerCompleted(object sender, EventArgs e)
        {
            animations[nextAnimation].Begin();
            nextAnimation = (nextAnimation > 8) ? 0 : (nextAnimation + 1);
            intervalTimer.Begin();
        }

        public void Stop()
        {
            base.Visibility = Visibility.Collapsed;
            AppearAnimation.Stop();
            nextAnimation = 0;
            intervalTimer.Stop();
            foreach (Storyboard storyboard in animations)
            {
                storyboard.Stop();
            }
        }
    }
}