﻿using System;
using System.Runtime.CompilerServices;
using System.Windows;

namespace TBridge.Cemi.Cruscotto.Template.Animation
{
    public class TransitionEventArgs : EventArgs
    {
        // Fields
        [CompilerGenerated] private FrameworkElement _TargetExamplePage;

        // Methods
        public TransitionEventArgs(FrameworkElement targetExample)
        {
            TargetExamplePage = targetExample;
        }

        // Properties
        public FrameworkElement TargetExamplePage
        {
            [CompilerGenerated]
            get { return _TargetExamplePage; }
            [CompilerGenerated]
            set { _TargetExamplePage = value; }
        }
    }
}