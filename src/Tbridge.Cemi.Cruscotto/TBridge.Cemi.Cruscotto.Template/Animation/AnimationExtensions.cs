﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace TBridge.Cemi.Cruscotto.Template.Animation
{
    internal static class AnimationExtensions
    {
        // Methods
        internal static AnimationContext Animate(this FrameworkElement target)
        {
            AnimationContext context = new AnimationContext();
            context.Targets.Add(target);
            return context;
        }

        internal static AnimationContext Animate(this AnimationContext target, params FrameworkElement[] newTargets)
        {
            target.Targets.Clear();
            foreach (FrameworkElement element in newTargets)
            {
                target.Targets.Add(element);
            }
            return target;
        }

        internal static AnimationContext EnsureDefaultTransforms(this AnimationContext target)
        {
            foreach (FrameworkElement element in target.Targets)
            {
                TransformGroup renderTransform = element.RenderTransform as TransformGroup;
                renderTransform = new TransformGroup();
                renderTransform.Children.Add(new ScaleTransform());
                renderTransform.Children.Add(new SkewTransform());
                renderTransform.Children.Add(new RotateTransform());
                renderTransform.Children.Add(new TranslateTransform());
                element.RenderTransform = renderTransform;
            }
            return target;
        }

        internal static AnimationContext MoveX(this AnimationContext target, params double[] args)
        {
            return
                target.SingleProperty(
                    "(UIElement.RenderTransform).(TransformGroup.Children)[3].(TranslateTransform.X)", args);
        }

        internal static AnimationContext MoveY(this AnimationContext target, params double[] args)
        {
            return
                target.SingleProperty(
                    "(UIElement.RenderTransform).(TransformGroup.Children)[3].(TranslateTransform.Y)", args);
        }

        internal static AnimationContext Opacity(this AnimationContext target, params double[] args)
        {
            return target.SingleProperty("(UIElement.Opacity)", args);
        }

        internal static AnimationContext Origin(this AnimationContext target, double x1, double x2)
        {
            foreach (FrameworkElement element in target.Targets)
            {
                element.RenderTransformOrigin = new Point(x1, x2);
            }
            return target;
        }

        internal static AnimationContext Scale(this AnimationContext target, params double[] args)
        {
            List<double> list = args.ToList<double>();
            if ((args.Length%2) != 0)
            {
                throw new InvalidOperationException("Params hsould come in a time-value pair");
            }
            foreach (FrameworkElement element in target.Targets)
            {
                DoubleAnimationUsingKeyFrames timeline = new DoubleAnimationUsingKeyFrames();
                Storyboard.SetTarget(timeline, element);
                Storyboard.SetTargetProperty(timeline,
                                             new PropertyPath(
                                                 "(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleX)",
                                                 new object[0]));
                DoubleAnimationUsingKeyFrames frames2 = new DoubleAnimationUsingKeyFrames();
                Storyboard.SetTarget(frames2, element);
                Storyboard.SetTargetProperty(frames2,
                                             new PropertyPath(
                                                 "(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleY)",
                                                 new object[0]));
                for (int i = 0; i < list.Count; i += 2)
                {
                    SplineDoubleKeyFrame frame = new SplineDoubleKeyFrame();
                    frame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(list[i]));
                    frame.Value = list[i + 1];
                    timeline.KeyFrames.Add(frame);
                    SplineDoubleKeyFrame frame2 = new SplineDoubleKeyFrame();
                    frame2.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(list[i]));
                    frame2.Value = list[i + 1];
                    frames2.KeyFrames.Add(frame2);
                }
                target.Instance.Children.Add(timeline);
                target.Instance.Children.Add(frames2);
            }
            target.StartIndex = target.EndIndex;
            target.EndIndex += 2*target.Targets.Count;
            return target;
        }

        private static AnimationContext SingleProperty(this AnimationContext target, string propertyPath,
                                                       params double[] args)
        {
            List<double> list = args.ToList<double>();
            if ((args.Length%2) != 0)
            {
                throw new InvalidOperationException("Params hsould come in a time-value pair");
            }
            foreach (FrameworkElement element in target.Targets)
            {
                DoubleAnimationUsingKeyFrames timeline = new DoubleAnimationUsingKeyFrames();
                Storyboard.SetTarget(timeline, element);
                Storyboard.SetTargetProperty(timeline, new PropertyPath(propertyPath, new object[0]));
                for (int i = 0; i < list.Count; i += 2)
                {
                    SplineDoubleKeyFrame frame = new SplineDoubleKeyFrame();
                    frame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(list[i]));
                    frame.Value = list[i + 1];
                    timeline.KeyFrames.Add(frame);
                }
                target.Instance.Children.Add(timeline);
            }
            target.StartIndex = target.EndIndex;
            target.EndIndex += target.Targets.Count;
            return target;
        }

        internal static AnimationContext Splines(this AnimationContext target, Spline spline)
        {
            return target.Splines(1, spline.Point1.X, spline.Point1.Y, spline.Point2.X, spline.Point2.Y);
        }

        internal static AnimationContext Splines(this AnimationContext target, int index, Spline spline)
        {
            return target.Splines(index, spline.Point1.X, spline.Point1.Y, spline.Point2.X, spline.Point2.Y);
        }

        internal static AnimationContext Splines(this AnimationContext target, int index, double x1, double y1,
                                                 double x2, double y2)
        {
            for (int i = target.StartIndex; i < target.EndIndex; i++)
            {
                KeySpline spline = new KeySpline();
                spline.ControlPoint1 = new Point(x1, y1);
                spline.ControlPoint2 = new Point(x2, y2);
                ((target.Instance.Children[i] as DoubleAnimationUsingKeyFrames).KeyFrames[index] as SplineDoubleKeyFrame)
                    .KeySpline = spline;
            }
            return target;
        }

        internal static AnimationContext With(this AnimationContext target, params FrameworkElement[] newElements)
        {
            foreach (FrameworkElement element in newElements)
            {
                target.Targets.Add(element);
            }
            return target;
        }

        internal static AnimationContext Without(this AnimationContext target, params FrameworkElement[] newElements)
        {
            foreach (FrameworkElement element in newElements)
            {
                target.Targets.Remove(element);
            }
            return target;
        }

        // Nested Types

        #region Nested type: AnimationContext

        internal class AnimationContext
        {
            // Fields
            [CompilerGenerated] private int _EndIndex;
            [CompilerGenerated] private Storyboard _Instance;
            [CompilerGenerated] private int _StartIndex;
            [CompilerGenerated] private ICollection<FrameworkElement> _Targets;

            // Methods
            public AnimationContext()
            {
                Storyboard storyboard = new Storyboard();
                storyboard.FillBehavior = FillBehavior.HoldEnd;
                Instance = storyboard;
                Targets = new List<FrameworkElement>(4);
            }

            // Properties
            public int EndIndex
            {
                [CompilerGenerated]
                get { return _EndIndex; }
                [CompilerGenerated]
                set { _EndIndex = value; }
            }

            internal Storyboard Instance
            {
                [CompilerGenerated]
                get { return _Instance; }
                [CompilerGenerated]
                set { _Instance = value; }
            }

            public int StartIndex
            {
                [CompilerGenerated]
                get { return _StartIndex; }
                [CompilerGenerated]
                set { _StartIndex = value; }
            }

            internal ICollection<FrameworkElement> Targets
            {
                [CompilerGenerated]
                get { return _Targets; }
                [CompilerGenerated]
                set { _Targets = value; }
            }
        }

        #endregion
    }
}