﻿using System.Runtime.CompilerServices;
using System.Windows;

namespace TBridge.Cemi.Cruscotto.Template.Animation
{
    internal class Spline
    {
        // Fields
        [CompilerGenerated] private Point _Point1;
        [CompilerGenerated] private Point _Point2;

        // Methods
        public Spline(double x1, double y1, double x2, double y2)
        {
            Point1 = new Point(x1, y1);
            Point2 = new Point(x2, y2);
        }

        // Properties
        internal Point Point1
        {
            [CompilerGenerated]
            get { return _Point1; }
            [CompilerGenerated]
            set { _Point1 = value; }
        }

        internal Point Point2
        {
            [CompilerGenerated]
            get { return _Point2; }
            [CompilerGenerated]
            set { _Point2 = value; }
        }
    }
}