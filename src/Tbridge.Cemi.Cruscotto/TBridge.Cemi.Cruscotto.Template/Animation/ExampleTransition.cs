﻿using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template.Animation
{
    public class ExampleTransition : ITransition
    {
        // Fields
        [CompilerGenerated] private Grid _Container;
        [CompilerGenerated] private double _PageWidth;
        private Storyboard animation = new Storyboard();
        private bool isRunning;
        private FrameworkElement item1;
        private FrameworkElement item2;
        private FrameworkElement mask1;
        private FrameworkElement mask2;

        // Events

        // Methods
        public ExampleTransition(double pageWidth, Grid panel, FrameworkElement mask1, FrameworkElement mask2)
        {
            PageWidth = pageWidth;
            Container = panel;
            this.mask1 = mask1;
            this.mask2 = mask2;
        }

        public Grid Container
        {
            [CompilerGenerated]
            get { return _Container; }
            [CompilerGenerated]
            set { _Container = value; }
        }

        public double PageWidth
        {
            [CompilerGenerated]
            get { return _PageWidth; }
            [CompilerGenerated]
            set { _PageWidth = value; }
        }

        #region ITransition Members

        public void Begin(IFrame page)
        {
            double pageWidth = PageWidth;
            item2 = page as FrameworkElement;
            if ((Container.Children.Count > 0) && (Container.Children[0] is RadFrame))
            {
                Container.Children.RemoveAt(0);
            }
            if (Container.Children.Count < 1)
            {
                Container.Children.Add(item2);
            }
            else
            {
                item1 = Container.Children[0] as FrameworkElement;
                Container.Children.Add(item2);
                double num2 = 0.8;
                double num3 = (1.0 - num2)/2.0;
                Container.Children.Add(mask1);
                Container.Children.Add(mask2);
                Spline spline = new Spline(0.504000008106232, 0.256000012159348, 0.458999991416931, 1.0);
                Spline spline2 = new Spline(0.247999995946884, 0.0, 1.0, 1.0);
                Spline spline3 = new Spline(0.321000009775162, 0.0, 0.45100000500679, 1.0);
                double num4 = 15.0;
                double[] args = new double[6];
                args[1] = 1.0;
                args[2] = 0.2;
                args[3] = 1.0;
                args[4] = 0.7;
                args[5] = num2;
                double[] numArray2 = new double[6];
                numArray2[0] = 0.7;
                numArray2[2] = 1.365;
                numArray2[3] = (-pageWidth*(num2 - num3)) - num4;
                numArray2[4] = 1.9;
                numArray2[5] = (-pageWidth*num2) - num4;
                double[] numArray3 = new double[6];
                numArray3[2] = 0.2;
                numArray3[4] = 0.7;
                numArray3[5] = 1.0;
                double[] numArray4 = new double[4];
                numArray4[0] = 1.35;
                numArray4[1] = 1.0;
                numArray4[2] = 1.9;
                double[] numArray5 = new double[6];
                numArray5[1] = num2;
                numArray5[2] = 1.365;
                numArray5[3] = num2;
                numArray5[4] = 1.9;
                numArray5[5] = 1.0;
                double[] numArray6 = new double[8];
                numArray6[1] = ((num2 + num3)*pageWidth) + num4;
                numArray6[2] = 0.2;
                numArray6[3] = ((num2 + num3)*pageWidth) + num4;
                numArray6[4] = 0.7;
                numArray6[5] = ((num2 - num3)*pageWidth) + num4;
                numArray6[6] = 1.365;
                animation =
                    item1.Animate().With(new[] {mask1}).EnsureDefaultTransforms().Origin(0.0, 0.5).Scale(args).Splines(
                        2, spline).MoveX(numArray2).Splines(1, spline2).Splines(2, spline3).Without(
                        new FrameworkElement[] {item1}).Opacity(numArray3).Splines(1, spline).Animate(
                        new FrameworkElement[] {mask2}).Opacity(numArray4).Splines(spline3).With(
                        new FrameworkElement[] {item2}).EnsureDefaultTransforms().Origin(0.5, 0.5).Scale(numArray5).
                        Splines(1, spline2).Splines(2, spline3).MoveX(numArray6).Splines(2, spline).Splines(3, spline2).
                        Instance;
                animation.SpeedRatio = (755.0/pageWidth)*1.5;
                isRunning = true;
                animation.Completed += delegate(object sender, EventArgs e)
                                           {
                                               TransitionAnimationCompleted(this, new TransitionEventArgs(item2));
                                               Container.Children.Remove(mask1);
                                               Container.Children.Remove(mask2);
                                               Container.Children.Remove(item1);
                                               item1 = null;
                                               item2 = null;
                                           };
                animation.Begin();
            }
        }

        public void StopTransition()
        {
            animation.Stop();
            Container.Children.Remove(item1);
            Container.Children.Remove(mask1);
            Container.Children.Remove(mask2);
            item1 = null;
            item2 = null;
            isRunning = false;
        }

        // Properties

        public bool IsRunning
        {
            get { return isRunning; }
            set { throw new NotImplementedException(); }
        }

        #endregion

        public event EventHandler<TransitionEventArgs> TransitionAnimationCompleted;
    }
}