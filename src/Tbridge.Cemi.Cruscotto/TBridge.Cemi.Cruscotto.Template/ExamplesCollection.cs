﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ExamplesCollection : ObservableCollection<IExampleItem>
    {
        // Methods
        public ExamplesCollection()
        {
        }

        internal ExamplesCollection(List<IExampleItem> listItems)
            : this()
        {
            Action<IExampleItem> action = null;
            if (action == null)
            {
                action = delegate(IExampleItem exampleItem) { base.Add(exampleItem); };
            }
            listItems.ForEach(action);
        }

        public void AddRange(ExamplesCollection exampleCollection)
        {
            exampleCollection.ForEach<IExampleItem>(delegate(IExampleItem item) { base.Add(item); });
        }

        internal void ApplyFilter(string value)
        {
            this.ForEach<IExampleItem>(delegate(IExampleItem item) { ((ExampleItem) item).ApplyFilter(value); });
        }

        internal void ExpandedAll(bool value)
        {
            this.ForEach<IExampleItem>(delegate(IExampleItem item) { item.IsExpanded = value; });
        }
    }
}