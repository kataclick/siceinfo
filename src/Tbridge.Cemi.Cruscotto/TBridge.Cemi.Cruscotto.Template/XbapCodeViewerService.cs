﻿using System;
using System.IO;
using System.Windows.Resources;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class XbapCodeViewerService : WpfCodeViewerService
    {
        // Methods
        protected override Stream GetResourceStream(string path)
        {
            StreamResourceInfo info = null;
            if (info == null)
            {
                return null;
            }
            return info.Stream;
        }

        public override void GetSourceFilesListAsync(string examplePath, Action<string> callback)
        {
            examplePath = RemoveFolderSeparator(examplePath);
            string str = string.Format("{0}/Description.txt|{0}/Page.xaml|{0}/Page.xaml.cs", examplePath);
            callback(str);
        }
    }
}