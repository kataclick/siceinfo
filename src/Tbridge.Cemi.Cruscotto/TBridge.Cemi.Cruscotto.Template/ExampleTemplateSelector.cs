﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ExampleTemplateSelector : DataTemplateSelector
    {
        // Fields
        [CompilerGenerated] private DataTemplate _CategoryTemplate;
        [CompilerGenerated] private DataTemplate _ExampleTemplate;

        // Methods

        // Properties
        public DataTemplate CategoryTemplate
        {
            [CompilerGenerated]
            get { return _CategoryTemplate; }
            [CompilerGenerated]
            set { _CategoryTemplate = value; }
        }

        public DataTemplate ExampleTemplate
        {
            [CompilerGenerated]
            get { return _ExampleTemplate; }
            [CompilerGenerated]
            set { _ExampleTemplate = value; }
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ExampleItem item2 = item as ExampleItem;
            if (item2 == null)
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture,
                                                              "The type {0} is not supported as data item in ExamplesTree.",
                                                              new object[] {item.GetType()}));
            }
            if (!item2.IsExample)
            {
                return CategoryTemplate;
            }
            return ExampleTemplate;
        }
    }
}