﻿using System.Windows;
using System.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class CategoriesButton : Button
    {
        public static readonly DependencyProperty BookmarkProperty = DependencyProperty.Register("Bookmark",
                                                                                                 typeof (string),
                                                                                                 typeof (
                                                                                                     CategoriesButton),
                                                                                                 new PropertyMetadata(
                                                                                                     "Set Category bookmark"));

        public static readonly DependencyProperty DefaultPathProperty = DependencyProperty.Register("Path",
                                                                                                    typeof (string),
                                                                                                    typeof (
                                                                                                        CategoriesButton
                                                                                                        ),
                                                                                                    new PropertyMetadata
                                                                                                        ("Set XML menu File Path"));

        public string MenuFilePath
        {
            get { return (string) base.GetValue(DefaultPathProperty); }
            set { base.SetValue(DefaultPathProperty, value); }
        }

        public string CategoryBookmark
        {
            get { return (string) base.GetValue(BookmarkProperty); }
            set { base.SetValue(BookmarkProperty, value); }
        }
    }
}