﻿namespace TBridge.Cemi.Cruscotto.Template
{
    public static class StringExtension
    {
        // Methods
        public static string RemoveWhiteSpace(this string value)
        {
            int num;
            while ((num = value.IndexOf(' ')) != -1)
            {
                value = value.Remove(num, 1);
            }
            return value;
        }
    }
}