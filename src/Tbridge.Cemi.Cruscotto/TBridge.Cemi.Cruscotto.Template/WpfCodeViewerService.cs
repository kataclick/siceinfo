﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class WpfCodeViewerService : CodeViewerService
    {
        // Methods
        protected static string DomainBaseDirectory
        {
            get { return string.Empty; }
        }

        protected override string GetDescriptionPath(string examplePath)
        {
            return (examplePath + "/Description.txt");
        }

        private static IEnumerable<string> GetFileName(string directoryName, IEnumerable<string> fileNameCollection)
        {
            List<string> list = new List<string>();
            foreach (string str in fileNameCollection)
            {
                list.Add(directoryName + "/" + Path.GetFileName(str));
            }
            return list;
        }

        protected override Stream GetResourceStream(string path)
        {
            return File.OpenRead(DomainBaseDirectory + path);
        }

        public override void GetSourceFileAsync(string filePath, Action<string> callback)
        {
            callback(base.GetResource("/" + filePath));
        }

        public override void GetSourceFilesListAsync(string examplePath, Action<string> callback)
        {
            examplePath = RemoveFolderSeparator(examplePath);
            string[] strArray = new[] {"*.txt", "*.cs", "*.vb", "*.xaml"};
            List<string> list = new List<string>();
            foreach (string str in examplePath.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries))
            {
                string path = Path.Combine(DomainBaseDirectory, str);
                foreach (string str3 in strArray)
                {
                    list.AddRange(GetFileName(str, Directory.EnumerateFiles(path, str3)));
                }
                list.Remove(str + "/Description.xaml");
            }
            list.Sort(delegate(string x, string y) { return Comparer<string>.Default.Compare(x, y); });
            string str4 = string.Join("|", list.ToArray());
            callback(str4);
        }

        // Properties
    }
}