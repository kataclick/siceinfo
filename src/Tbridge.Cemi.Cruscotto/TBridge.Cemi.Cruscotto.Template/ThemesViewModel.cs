﻿using System.Collections.Generic;
using Telerik.Windows.Controls;
using ThemeManager=TBridge.Cemi.Cruscotto.Template.Common.ThemeManager;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ThemesViewModel : NotifyPropertyChangedBase
    {
        // Fields
        private int selectedThemeIndex;

        // Methods
        public ThemesViewModel()
        {
            if (StyleManager.ApplicationTheme != null)
            {
                selectedThemeIndex = Themes.IndexOf(StyleManager.ApplicationTheme.ToString());
            }
            else
            {
                selectedThemeIndex = -1;
            }
        }

        // Properties
        public int SelectedThemeIndex
        {
            get { return selectedThemeIndex; }
            set
            {
                if (selectedThemeIndex != value)
                {
                    selectedThemeIndex = value;
                    StyleManager.ApplicationTheme = ThemeManager.FromName(Themes[value]);
                    OnPropertyChanged("SelectedThemeIndex");
                }
            }
        }

        public static IList<string> Themes
        {
            get { return ThemeManager.StandardThemeNames; }
        }
    }
}