﻿using System;
using System.IO;

namespace TBridge.Cemi.Cruscotto.Template
{
    public abstract class CodeViewerService
    {
        // Fields
        private static CodeViewerService current;

        // Methods
        protected CodeViewerService()
        {
        }

        public static CodeViewerService Current
        {
            get
            {
                if (current == null)
                {
                    current = new SilverlightCodeViewerService();
                }
                return current;
            }
        }

        public string GetDescription(string examplePath)
        {
            return GetResource(GetDescriptionPath(examplePath));
        }

        protected abstract string GetDescriptionPath(string examplePath);

        protected string GetResource(string path)
        {
            Stream resourceStream = GetResourceStream(path);
            if (resourceStream == null)
            {
                return null;
            }
            using (StreamReader reader = new StreamReader(resourceStream))
            {
                return reader.ReadToEnd();
            }
        }

        protected abstract Stream GetResourceStream(string path);
        public abstract void GetSourceFileAsync(string filePath, Action<string> callback);
        public abstract void GetSourceFilesListAsync(string examplePath, Action<string> callback);

        protected static string RemoveFolderSeparator(string examplePath)
        {
            if (examplePath.IndexOf('|') == (examplePath.Length - 1))
            {
                examplePath = examplePath.Remove(examplePath.IndexOf('|'));
            }
            return examplePath;
        }

        // Properties
    }
}