﻿using System;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using TBridge.Cemi.Cruscotto.Template.AuthenticationService;
using TBridge.Cemi.Cruscotto.Template.Common;

namespace TBridge.Silverlight.Authentication
{
    public partial class LoginControl : UserControl
    {
        public delegate void LoginHandler(object sender, User e);
        public delegate void LogoutHandler(object sender);

        public event LoginHandler LoginEvent;
        public event LogoutHandler LogoutEvent;

        private IAuthenticationService client;
        private LoginWindow LoginWindow;

        public LoginControl()
        {
            InitializeComponent();

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Utils.AuthenticationServiceEndpoint);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<IAuthenticationService> factory =
                new ChannelFactory<IAuthenticationService>(basicHttpBinding, endpointAddress);

            factory.Open();
            client = factory.CreateChannel();

            UpdateLoginState();
        }

        private bool IsAuthenticated { get; set; }
        public string UserName { get; set; }

        private void SignOutCompleted(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           client.EndSignOut(asyncResult);
                                           IsAuthenticated = false;
                                           UpdateLoginState();
                                           OnLogout();
                                       });
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            ShowLoginWindow();
        }

        public void ShowLoginWindow()
        {
            LoginWindow = new LoginWindow();
            LoginWindow.Closed += loginWindow_Closed;
            LoginWindow.ShowDialog();
        }

        private void loginWindow_Closed(object sender, EventArgs e)
        {
            IsAuthenticated = LoginWindow.DialogResult.Value;
            Username.Text = LoginWindow.User.UserName;
            UpdateLoginState();
            OnLogin(LoginWindow.User); 
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            client.BeginSignOut(SignOutCompleted, null);
        }

        private void UpdateLoginState()
        {
            VisualStateManager.GoToState(this, IsAuthenticated ? "loggedIn" : "loggedOut", true);
        }

        void OnLogin(User e)
        {
            if (LoginEvent != null)
            {
                LoginEvent(this, e);
            }
        }

        void OnLogout()
        {
            if (LogoutEvent != null)
            {
                LogoutEvent(this);
            }
        }
    }
}