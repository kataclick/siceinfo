﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TBridge.Silverlight.Authentication
{
    public partial class PasswordControl : UserControl
    {
        public static readonly DependencyProperty PasswordTextProperty =
            DependencyProperty.Register(
                "PasswordText",
                typeof(string),
                typeof(PasswordControl),
                null);

        public PasswordControl()
        {
            InitializeComponent();
        }

        public string PasswordText
        {
            get
            {
                string str = base.GetValue(PasswordTextProperty) as string;
                if (str != null)
                {
                    return str;
                }
                return string.Empty;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                base.SetValue(PasswordTextProperty, value);
            }
        }

        private void Password_LostFocus(object sender, RoutedEventArgs e)
        {
            if (sender is PasswordBox)
            {
                PasswordText = password.Password;
            }
        }
    }
}