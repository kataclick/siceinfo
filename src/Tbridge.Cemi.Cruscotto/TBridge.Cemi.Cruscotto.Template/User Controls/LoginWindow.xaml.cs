﻿using System;
using System.ServiceModel;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Template.AuthenticationService;
using TBridge.Cemi.Cruscotto.Template.Common;
using Telerik.Windows.Controls;

namespace TBridge.Silverlight.Authentication
{
    public partial class LoginWindow : RadWindow
    {
        private readonly IAuthenticationService client;

        private bool autenticato;
        private bool hasRoles;

        public LoginWindow()
        {
            InitializeComponent();

            loginButton.IsEnabled = false;

            SetWindowStyle();

            User = new User();

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Utils.AuthenticationServiceEndpoint);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<IAuthenticationService> factory =
                new ChannelFactory<IAuthenticationService>(basicHttpBinding, endpointAddress);

            factory.Open();
            client = factory.CreateChannel();
        }

        public User User { get; set; }

        public bool Autenticato
        {
            get { return autenticato; }
            set
            {
                if (!value)
                {
                    throw new Exception("Attenzione l'utente non può visualizzare le informazioni richieste.");
                }

                autenticato = value;
            }
        }

        public bool HasRoles
        {
            get { return hasRoles; }
            set
            {
                if (!value)
                {
                    throw new Exception("Attenzione nome utente o password sbagliati.");
                }

                hasRoles = value;
            }
        }


        private void SetWindowStyle()
        {
            Header = "";
            BorderThickness = new Thickness(0, -20, 0, 0);
            BorderBrush = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
            BorderBackground = new SolidColorBrush(Color.FromArgb(0, 31, 73, 125));
            Background = new SolidColorBrush(Color.FromArgb(190, 31, 73, 125));
            CanMove = false;
            CanClose = false;
            
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            SetEditableState(false);

            client.BeginAuthenticate(loginUserNameBox.Text, loginPasswordBox.Password, AuthenticateCompleted, null);
        }

        private void AuthenticateCompleted(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           User =
                                               client.EndAuthenticate(asyncResult);
                                           DialogResult = User.LoggedIn;
                                           String loginError = String.Empty;
                                           if (User.LoggedIn)
                                           {
                                               if (User.Roles.Contains("CruscottoPrestazioni") ||
                                                   User.Roles.Contains("CruscottoCrc") ||
                                                   User.Roles.Contains("CruscottoImprese") ||
                                                 User.Roles.Contains("CruscottoMassaSalariale"))
                                               {
                                                   Close();
                                               }
                                               else
                                               {
                                                   SetEditableState(true);
                                                   loginError =
                                                       "Attenzione l'utente non può visualizzare le informzioni richieste.";
                                               }
                                           }
                                           else
                                           {
                                               SetEditableState(true);
                                               loginError = "Attenzione nome utente o password sbagliati.";
                                           }
                                           ValidationSummary.Visibility = Visibility.Visible;
                                           ValidationSummary.Text = loginError;
                                       });
        }

        private void LoginUserNameBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidationSummary.Visibility = Visibility.Collapsed;
            SetLoginButtonEnabled();
        }

        private void LoginPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            ValidationSummary.Visibility = Visibility.Collapsed;
            SetLoginButtonEnabled();
        }

        private void SetLoginButtonEnabled()
        {
            loginButton.IsEnabled = (loginUserNameBox.Text.Length != 0) && (loginPasswordBox.Password.Length != 0);
        }

        private void SetEditableState(bool enabled)
        {
            loginButton.IsEnabled = enabled;
            loginUserNameBox.IsEnabled = enabled;
            loginPasswordBox.IsEnabled = enabled;
            loginUserNameBox.Focus();
        }

        private void LoginBox_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (loginButton.IsEnabled)
                    {
                        ButtonAutomationPeer peer = new ButtonAutomationPeer(loginButton);

                        IInvokeProvider ip = (IInvokeProvider) peer;
                        ip.Invoke();
                    }
                    break;
            }
        }
    }
}