﻿using System.Windows.Media;

namespace TBridge.Cemi.Cruscotto.Template
{
    public interface IExampleItem
    {
        // Properties
        IExampleItem CategoryFirstLookExample { get; }
        ExamplesCollection Children { get; }
        string CommonFilesFolder { get; }
        ImageSource ExampleImage { get; }
        string Filter { get; set; }
        bool HasElements { get; }
        ImageSource Icon { get; }
        string IconUrl { get; }
        bool IsDefault { get; }
        bool IsExample { get; }
        bool IsExpanded { get; set; }
        bool IsFeatured { get; }
        bool IsHomepage { get; }
        bool IsSelected { get; set; }
        string Keywords { get; }
        IExampleItem Parent { get; }
        string Path { get; set; }
        string Platform { get; }
        string ShortTypeName { get; }
        string Text { get; }
        bool Visible { get; set; }
        string CodicePrestazione { get; set; }
        string LastUpdate { get; set; }
        string TitoloPagina { get; set; }
    }
}