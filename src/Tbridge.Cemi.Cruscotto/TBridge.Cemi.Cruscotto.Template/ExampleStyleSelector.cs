﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class ExampleStyleSelector : StyleSelector
    {
        // Fields
        [CompilerGenerated] private Style _CategoryStyle;
        [CompilerGenerated] private Style _ExampleStyle;
        [CompilerGenerated] private Style _InnerCategoryStyle;

        // Methods

        // Properties
        public Style CategoryStyle
        {
            [CompilerGenerated]
            get { return _CategoryStyle; }
            [CompilerGenerated]
            set { _CategoryStyle = value; }
        }

        public Style ExampleStyle
        {
            [CompilerGenerated]
            get { return _ExampleStyle; }
            [CompilerGenerated]
            set { _ExampleStyle = value; }
        }

        public Style InnerCategoryStyle
        {
            [CompilerGenerated]
            get { return _InnerCategoryStyle; }
            [CompilerGenerated]
            set { _InnerCategoryStyle = value; }
        }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            ExampleItem item2 = item as ExampleItem;
            if (item2 == null)
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture,
                                                              "The type {0} is not supported as data item in ExamplesTree.",
                                                              new object[] {item.GetType()}));
            }
            if (item2.IsExample)
            {
                return ExampleStyle;
            }
            if (item2.Parent != null)
            {
                return InnerCategoryStyle;
            }
            return CategoryStyle;
        }
    }
}