﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using TBridge.Cemi.Cruscotto.Template.Parser;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class BaseCodeViewer : Control
    {
        // Fields
        public static readonly DependencyProperty ExampleItemProperty = DependencyProperty.Register("ExampleItem",
                                                                                                    typeof (ExampleItem),
                                                                                                    typeof (
                                                                                                        BaseCodeViewer),
                                                                                                    null);

        [CompilerGenerated] private string _FilePath;
        [CompilerGenerated] private TextBlock _FormattedText;
        [CompilerGenerated] private ScrollViewer _FormattedTextScrollViewer;
        [CompilerGenerated] private TextBox _Selection;

        // Methods
        public BaseCodeViewer()
        {
            base.DefaultStyleKey = typeof (BaseCodeViewer);
            base.Loaded += new RoutedEventHandler(OnLoaded);
        }

        public ExampleItem ExampleItem
        {
            get { return (ExampleItem) base.GetValue(ExampleItemProperty); }
            set { base.SetValue(ExampleItemProperty, value); }
        }

        private string FilePath
        {
            [CompilerGenerated]
            get { return _FilePath; }
            [CompilerGenerated]
            set { _FilePath = value; }
        }

        protected TextBlock FormattedText
        {
            [CompilerGenerated]
            get { return _FormattedText; }
            [CompilerGenerated]
            set { _FormattedText = value; }
        }

        protected ScrollViewer FormattedTextScrollViewer
        {
            [CompilerGenerated]
            get { return _FormattedTextScrollViewer; }
            [CompilerGenerated]
            set { _FormattedTextScrollViewer = value; }
        }

        protected TextBox Selection
        {
            [CompilerGenerated]
            get { return _Selection; }
            [CompilerGenerated]
            set { _Selection = value; }
        }

        protected static string ConvertTabsToSpaces(string sourceText, int numberOfSpaces)
        {
            return Regex.Replace(sourceText, "\t", string.Empty.PadLeft(numberOfSpaces, ' '));
        }

        protected static void FormatCodeFile(InlineCollection inlineCollection, string code, string extension)
        {
            new Tokenizer().TokenizeCode(code, extension).ForEach(
                delegate(Token token) { inlineCollection.Add(token.GetRun()); });
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "inlineCollection"),
         SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "WPF compatibility"),
         SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "inlines",
             Justification = "Silverlight compatibility")]
        protected void FormatDescription(InlineCollection inlineCollection, string code)
        {
            FormatCodeFile(inlineCollection, code, ".txt");
        }

        private void InitializeFlowDocument()
        {
            FormattedTextScrollViewer = base.GetTemplateChild("ScrollViewer") as ScrollViewer;
            FormattedText = base.GetTemplateChild("FormattedText") as TextBlock;
            Selection = (TextBox) base.GetTemplateChild("Selection");
            Selection.KeyDown += delegate(object sender, KeyEventArgs e) { e.Handled = true; };
        }

        public override void OnApplyTemplate()
        {
            InitializeFlowDocument();
            base.OnApplyTemplate();
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (ExampleItem != null)
            {
                string examplePath = string.Join("|", new string[] {ExampleItem.Path, ExampleItem.CommonFilesFolder});
                CodeViewerService.Current.GetSourceFilesListAsync(examplePath, new Action<string>(OnSourceFilesReceived));
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected virtual void OnSourceFilesLoaded(string descriptionFile, List<string> list)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           CodeViewerService.Current.GetSourceFileAsync(descriptionFile,
                                                                                        delegate(string code)
                                                                                            {
                                                                                                Dispatcher.BeginInvoke(
                                                                                                    delegate
                                                                                                        {
                                                                                                            InlineCollection
                                                                                                                inlineCollection
                                                                                                                    =
                                                                                                                    ResetCodeViewerContent
                                                                                                                        ();
                                                                                                            if (
                                                                                                                !string.
                                                                                                                     IsNullOrEmpty
                                                                                                                     (code))
                                                                                                            {
                                                                                                                code =
                                                                                                                    ConvertTabsToSpaces
                                                                                                                        (code,
                                                                                                                         4);
                                                                                                                FormatDescription
                                                                                                                    (inlineCollection,
                                                                                                                     code);
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                inlineCollection
                                                                                                                    .Add
                                                                                                                    (new Run
                                                                                                                         {
                                                                                                                             Text
                                                                                                                                 =
                                                                                                                                 "Not find description!!!"
                                                                                                                         });
                                                                                                            }
                                                                                                        });
                                                                                            });
                                       });
        }

        protected void OnSourceFilesReceived(string list)
        {
            IEnumerable<string> source =
                list.Split(new char[] {'|'}).Where<string>(
                    delegate(string file) { return Path.GetExtension(file) == ".txt"; });
            string descriptionFile = (source.Count<string>() > 0) ? source.First<string>() : string.Empty;
            IEnumerable<string> enumerable2 =
                list.Split(new char[] {'|'}).Where<string>(
                    delegate(string file) { return Path.GetExtension(file) != ".txt"; });
            OnSourceFilesLoaded(descriptionFile, enumerable2.ToList<string>());
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        protected void RefreshTextArea()
        {
            Selection.Text = FormattedText.Text;
            FormattedTextScrollViewer.ScrollToVerticalOffset(0.0);
            FormattedTextScrollViewer.ScrollToHorizontalOffset(0.0);
        }

        protected InlineCollection ResetCodeViewerContent()
        {
            TextBlock formattedText = FormattedText;
            formattedText.Inlines.Clear();
            return formattedText.Inlines;
        }

        // Properties
    }
}