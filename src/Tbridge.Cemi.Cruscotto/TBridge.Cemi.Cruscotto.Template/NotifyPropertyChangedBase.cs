﻿using System.ComponentModel;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class NotifyPropertyChangedBase : INotifyPropertyChanged
    {
        // Events

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        // Methods
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}