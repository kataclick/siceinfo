﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using TBridge.Cemi.Cruscotto.Template.Animation;

namespace TBridge.Cemi.Cruscotto.Template
{
    public static class ExtensionMethods
    {
        // Methods
        public static bool Contains(this Dictionary<string, object> source, string key)
        {
            return source.ContainsKey(key);
        }

        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters",
            Justification = "WPF compatibility")]
        public static void NavigateToExample(this RadFrame exampleContainer, BasePage page)
        {
            NavigationService navigationService = NavigationService.GetNavigationService();
            Rectangle rectangle3 = new Rectangle();
            rectangle3.Fill = new SolidColorBrush(Color.FromArgb(0x4d, 8, 0x11, 0x30));
            Rectangle rectangle = rectangle3;
            Rectangle rectangle4 = new Rectangle();
            rectangle4.Fill = new SolidColorBrush(Color.FromArgb(0x4d, 8, 0x11, 0x30));
            Rectangle rectangle2 = rectangle4;
            Grid rootPanel = navigationService.RootPanel as Grid;
            ExamplePage page2 = navigationService.RootPanel.Children[0] as ExamplePage;
            ExampleTransition transition = new ExampleTransition(rootPanel.ActualWidth, rootPanel, rectangle, rectangle2);
            navigationService.Transition = transition;
            transition.TransitionAnimationCompleted += OnTransitionAnimationCompleted;
            exampleContainer.Navigate(page, navigationService.Transition);
            if (page2 != null)
            {
                page2.SelectedExample.Unload();
                page2.SelectedExample.StartTransitionAnimation();
            }
        }

        private static void OnTransitionAnimationCompleted(object sender, TransitionEventArgs e)
        {
            ExampleTransition transition = sender as ExampleTransition;
            transition.TransitionAnimationCompleted -= OnTransitionAnimationCompleted;
            if ((e.TargetExamplePage != null) && (e.TargetExamplePage is BasePage))
            {
                BasePage targetExamplePage = e.TargetExamplePage as BasePage;
                targetExamplePage.SelectedExample.AnimationCompleted();
            }
        }
    }
}