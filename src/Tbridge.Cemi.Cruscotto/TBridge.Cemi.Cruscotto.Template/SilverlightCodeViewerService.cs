﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Resources;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class SilverlightCodeViewerService : CodeViewerService
    {
        // Methods
        private static string CodeViewerBaseUrl
        {
            get
            {
                string absoluteUri = HtmlPage.Document.DocumentUri.AbsoluteUri;
                if (!string.IsNullOrEmpty(HtmlPage.Document.DocumentUri.Query))
                {
                    absoluteUri = absoluteUri.Replace(HtmlPage.Document.DocumentUri.Query, "");
                }
                int startIndex = absoluteUri.LastIndexOf("Default.aspx", StringComparison.OrdinalIgnoreCase);
                if (startIndex == -1)
                {
                    startIndex = absoluteUri.LastIndexOf("Default.VB.aspx", StringComparison.OrdinalIgnoreCase);
                }
                if (startIndex == -1)
                {
                    startIndex = absoluteUri.LastIndexOf("#", StringComparison.OrdinalIgnoreCase);
                }
                if (startIndex != -1)
                {
                    absoluteUri = absoluteUri.Remove(startIndex, absoluteUri.Length - startIndex);
                }
                return (absoluteUri +
                        (absoluteUri.EndsWith("/", StringComparison.OrdinalIgnoreCase) ? string.Empty : "/") +
                        "CodeViewer.ashx");
            }
        }

        private static void BeginRequest(string path, bool listFiles, Action<string> callback)
        {
            Uri requestUri = new Uri(CodeViewerBaseUrl + "?path=" + path + (listFiles ? "&list=true" : string.Empty),
                                     UriKind.Absolute);
            WebRequest state = WebRequest.Create(requestUri);
            AsyncCallback callback2 = delegate(IAsyncResult result)
                                          {
                                              using (
                                                  TextReader reader =
                                                      new StreamReader(
                                                          (result.AsyncState as WebRequest).EndGetResponse(result).
                                                              GetResponseStream()))
                                              {
                                                  string str = reader.ReadToEnd();
                                                  callback(str);
                                              }
                                          };
            state.BeginGetResponse(callback2, state);
        }

        protected override string GetDescriptionPath(string examplePath)
        {
            return (examplePath + "/Description.txt");
        }

        protected override Stream GetResourceStream(string path)
        {
            StreamResourceInfo resourceStream =
                Application.GetResourceStream(
                    new Uri(
                        string.Format(CultureInfo.InvariantCulture, "/Pages.{0};component/Pages/{1}",
                                      new object[] {path.Split(new[] {'/'})[0], path}), UriKind.Relative));
            if (resourceStream == null)
            {
                return null;
            }
            return resourceStream.Stream;
        }

        public override void GetSourceFileAsync(string filePath, Action<string> callback)
        {
            BeginRequest(filePath, false, callback);
        }

        public override void GetSourceFilesListAsync(string examplePath, Action<string> callback)
        {
            BeginRequest(examplePath, true, callback);
        }

        // Properties
    }
}