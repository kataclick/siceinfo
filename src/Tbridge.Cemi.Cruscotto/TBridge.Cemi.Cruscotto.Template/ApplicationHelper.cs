﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;

namespace TBridge.Cemi.Cruscotto.Template
{
    internal static class ApplicationHelper
    {
        // Fields
        private static string exampleApplicationAssemblyName;
        private static string exampleApplicationRootNamespace;

        public static string ApplicationAssemblyType
        {
            get { return Path.GetExtension(ExampleApplicationAssemblyName); }
        }

        public static Type ApplicationType
        {
            get { return Application.Current.GetType(); }
        }

        public static string ExampleApplicationAssemblyName
        {
            get
            {
                if (exampleApplicationAssemblyName == null)
                {
                    exampleApplicationAssemblyName =
                        ApplicationType.AssemblyQualifiedName.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)[
                            1].Trim();
                }
                return exampleApplicationAssemblyName;
            }
        }

        public static string ExampleApplicationRootNamespace
        {
            get
            {
                if (exampleApplicationRootNamespace == null)
                {
                    exampleApplicationRootNamespace = ApplicationType.Namespace;
                }
                return exampleApplicationRootNamespace;
            }
        }

        // Methods
        private static void BeginLoadAssembly(string path, string type, Action<object> callBack)
        {
            string pathToAssembly = CurrentLoadedAssemblyName(path);
            string uri = string.Format("{0}.xap", GetPathToXap(pathToAssembly));
            OpenReadCompletedEventHandler downloadHandler = delegate(object sender, OpenReadCompletedEventArgs e)
                                                                {
                                                                    if (!e.Cancelled && (e.Error == null))
                                                                    {
                                                                        Example example;
                                                                        LoadAssembliesFromXAP(e.Result);
                                                                        if (ApplicationCache.TryLoadingType(type,
                                                                                                            out example))
                                                                        {
                                                                            callBack(example);
                                                                        }
                                                                    }
                                                                };
            DownloadResource(uri, downloadHandler);
        }

        public static string CurrentLoadedAssemblyName(string path)
        {
            string str = path;
            str = str.Split(new[] {'/'})[0];
            return string.Format(CultureInfo.InvariantCulture, "Pages.{0}", new object[] { str });
        }

        private static void DownloadResource(string uri, OpenReadCompletedEventHandler downloadHandler)
        {
            WebClient client = new WebClient();
            client.OpenReadAsync(new Uri(uri, UriKind.Relative));
            client.OpenReadCompleted += downloadHandler;
        }

        private static string GetPathToXap(string pathToAssembly)
        {
            return (pathToAssembly + ApplicationAssemblyType);
        }

        [SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults",
            MessageId = "System.Windows.Resources.StreamResourceInfo")]
        private static Stream GetSpecificResourceFromStream(Stream stream, Uri uri)
        {
            StreamResourceInfo zipPackageStreamResourceInfo = new StreamResourceInfo(stream, "application / binary");
            return Application.GetResourceStream(zipPackageStreamResourceInfo, uri).Stream;
        }

        internal static void LoadAssemblies(string path, string type, Action<object> callBack)
        {
            Example example;
            if (ApplicationCache.TryLoadingType(type, out example))
            {
                callBack(example);
            }
            else
            {
                BeginLoadAssembly(path, type, callBack);
            }
        }

        private static void LoadAssembliesFromXAP(Stream xapStream)
        {
            XDocument.Parse(
                new StreamReader(GetSpecificResourceFromStream(xapStream, new Uri("AppManifest.xaml", UriKind.Relative)))
                    .ReadToEnd()).Root.Elements().Elements().ToList().ForEach(delegate(XElement item)
                                                                                  {
                                                                                      string uriString =
                                                                                          item.Attribute("Source").Value;
                                                                                      LoadAssemblyInDomain(
                                                                                          GetSpecificResourceFromStream(
                                                                                              xapStream,
                                                                                              new Uri(uriString,
                                                                                                      UriKind.Relative)));
                                                                                  });
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "resultStram")]
        private static void LoadAssemblyInDomain(Stream resultStram)
        {
            Action a = null;
            if (Deployment.Current.Dispatcher.CheckAccess())
            {
                AssemblyPart part = new AssemblyPart();
                ApplicationCache.SaveAssemblyInCache(part.Load(resultStram));
            }
            else
            {
                if (a == null)
                {
                    a = delegate
                            {
                                AssemblyPart part = new AssemblyPart();
                                ApplicationCache.SaveAssemblyInCache(part.Load(resultStram));
                            };
                }
                Deployment.Current.Dispatcher.BeginInvoke(a);
            }
        }

        // Properties
    }
}