﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class CodeViewer : BaseCodeViewer
    {
        // Fields
        [CompilerGenerated] private RadTabControl _TabControl;
        private Dictionary<string, string> sourceCode = new Dictionary<string, string>();

        // Methods
        public CodeViewer()
        {
            base.DefaultStyleKey = typeof (CodeViewer);
        }

        internal RadTabControl TabControl
        {
            [CompilerGenerated]
            get { return _TabControl; }
            [CompilerGenerated]
            set { _TabControl = value; }
        }

        protected void AddSourceCode(string file, string content)
        {
            if (content == null)
            {
                content = "Cannot find description";
            }
            if (!sourceCode.ContainsKey(file))
            {
                sourceCode.Add(file, ConvertTabsToSpaces(content, 4));
            }
            base.Dispatcher.BeginInvoke(delegate { DisplaySelectedFile(); });
        }

        private void DisplaySelectedFile()
        {
            string selectedFile = TabControl.SelectedItem as string;
            if (selectedFile != null)
            {
                InlineCollection inlines = base.ResetCodeViewerContent();
                if (sourceCode.ContainsKey(Path.GetFileName(selectedFile)))
                {
                    string code = sourceCode[selectedFile];
                    base.Dispatcher.BeginInvoke(delegate
                                                    {
                                                        ResetCodeViewerContent();
                                                        FormatCodeFile(inlines, code,
                                                                       GetSourceFileExtension(selectedFile));
                                                        RefreshTextArea();
                                                    });
                }
                Run run = new Run();
                run.Text = "Loading....";
                run.Foreground = new SolidColorBrush(Colors.Black);
                inlines.Add(run);
                base.RefreshTextArea();
            }
        }

        private static string GetSourceFileExtension(string fileName)
        {
            string extension = Path.GetExtension(fileName);
            if (string.IsNullOrEmpty(extension))
            {
                return ".txt";
            }
            return extension;
        }

        private void InitializeTabControl()
        {
            if (TabControl != null)
            {
                TabControl.SelectionChanged -= new RoutedEventHandler(OnTabControlSelectionChanged);
            }
            TabControl = base.GetTemplateChild("TabControl") as RadTabControl;
            if (TabControl != null)
            {
                TabControl.SelectionChanged += new RoutedEventHandler(OnTabControlSelectionChanged);
            }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            InitializeTabControl();
        }

        [SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
        protected override void OnSourceFilesLoaded(string descriptionFile, List<string> list)
        {
            Action action = delegate
                                {
                                    List<string> fileNames = new List<string>();
                                    list.Sort(new Comparison<string>(SortDescription));
                                    list.ForEach(
                                        delegate(string fileName) { fileNames.Add(Path.GetFileName(fileName)); });
                                    for (int j = 0; j < list.Count; j++)
                                    {
                                        string file = list[j];
                                        CodeViewerService.Current.GetSourceFileAsync(file,
                                                                                     delegate(string content)
                                                                                         {
                                                                                             AddSourceCode(
                                                                                                 Path.GetFileName(file),
                                                                                                 content);
                                                                                         });
                                    }
                                    TabControl.ItemsSource = fileNames;
                                };
            base.Dispatcher.BeginInvoke(action);
        }

        private void OnTabControlSelectionChanged(object sender, RoutedEventArgs e)
        {
            DisplaySelectedFile();
        }

        private int SortDescription(string first, string second)
        {
            first = Path.GetFileName(first);
            second = Path.GetFileName(second);
            int num = Comparer<string>.Default.Compare(first, second);
            if (num != 0)
            {
                if (first.Equals("Page.xaml", StringComparison.OrdinalIgnoreCase))
                {
                    return -1;
                }
                if (second.Equals("Page.xaml", StringComparison.OrdinalIgnoreCase))
                {
                    return 1;
                }
            }
            return num;
        }

        // Properties
    }
}