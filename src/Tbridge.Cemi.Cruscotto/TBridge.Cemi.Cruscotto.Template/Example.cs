﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Resources;
using Telerik.Windows.Controls;
using TBridge.Cemi.Cruscotto.Template.Common;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class Example : UserControl
    {
        // Fields
        public static readonly DependencyProperty BottomRightPanelProperty =
            DependencyProperty.Register("BottomRightPanel", typeof (Panel), typeof (Example), null);

        public static readonly DependencyProperty CodiceProperty = DependencyProperty.Register("CodicePrestazione",
                                                                                               typeof(string),
                                                                                               typeof(Example), null);

        public static readonly DependencyProperty UpadteProperty = DependencyProperty.Register("LastUpdate",
                                                                                               typeof(string),
                                                                                               typeof(Example), null);

        public static readonly DependencyProperty DropDownProperty = DependencyProperty.Register("DropDown",
                                                                                                 typeof (RadComboBox),
                                                                                                 typeof (Example), null);

        public static readonly DependencyProperty OutputProperty = DependencyProperty.Register("Output", typeof (string),
                                                                                               typeof (Example), null);

        public static readonly DependencyProperty ThemeComboBoxVisibilityProperty =
            DependencyProperty.Register("ThemeComboBoxVisibility", typeof (Visibility), typeof (Example),
                                        new PropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty TopRightPanelProperty = DependencyProperty.Register("TopRightPanel",
                                                                                                      typeof (Panel),
                                                                                                      typeof (Example),
                                                                                                      null);

        public Example()
        {
            StreamResourceInfo resourceStream = null;
            try
            {
                Uri uriResource = new Uri("/Telerik.Windows.QuickStart;component/Themes/CommonStyles.xaml",
                                          UriKind.Relative);
                resourceStream = Application.GetResourceStream(uriResource);
                if ((resourceStream != null) && (resourceStream.Stream != null))
                {
                    List<string> keys = new List<string>();
                    ResourceDictionary dictionary = ResourceParser.Parse(resourceStream.Stream, true, keys);
                    foreach (string str in keys)
                    {
                        if (base.Resources.Contains(str))
                        {
                            base.Resources.Remove(str);
                        }
                        object obj2 = dictionary[str];
                        if (obj2 != null)
                        {
                            base.Resources.Add(str, obj2);
                        }
                    }
                }
            }
            catch (KeyNotFoundException)
            {
            }
        }

        public Grid CodeViewer { get; set; }

        public Panel BottomRightPanel
        {
            get { return (Panel) base.GetValue(BottomRightPanelProperty); }
            set { base.SetValue(BottomRightPanelProperty, value); }
        }

        public RadComboBox DropDown
        {
            get { return (RadComboBox) base.GetValue(DropDownProperty); }
            set { base.SetValue(DropDownProperty, value); }
        }

        public string Output
        {
            get { return (string) base.GetValue(OutputProperty); }
            set { base.SetValue(OutputProperty, value); }
        }

        public Visibility ThemeComboBoxVisibility
        {
            get { return (Visibility) base.GetValue(ThemeComboBoxVisibilityProperty); }
            set { base.SetValue(ThemeComboBoxVisibilityProperty, value); }
        }

        public Panel TopRightPanel
        {
            get { return (Panel) base.GetValue(TopRightPanelProperty); }
            set { base.SetValue(TopRightPanelProperty, value); }
        }

        public string CodicePrestazione
        {
            get { return (string) base.GetValue(CodiceProperty); }
            set { base.SetValue(CodiceProperty, value); }
        }

        public string LastUpdate
        {
            get { return (string)base.GetValue(UpadteProperty); }
            set { base.SetValue(UpadteProperty, value); }
        }

        // Events
        public event EventHandler HideExampleArea;

        public event EventHandler ShowExampleArea;

        public event EventHandler ClickExportButton;

        public event EventHandler ClickPrintButton;

        public event EventHandler TransitionAnimationCompleted;

        public event EventHandler TransitionAnimationStarted;

        //public event EventHandler Unloaded;

        public event EventHandler LastUpdateChanged;


        // Methods

        internal void AnimationCompleted()
        {
            OnTransitionAnimationCompleted(EventArgs.Empty);
        }

        protected virtual void OnHideExampleArea(EventArgs e)
        {
            if (HideExampleArea != null)
                HideExampleArea(this, EventArgs.Empty);
        }

        protected virtual void OnClickExportButton(EventArgs e)
        {
            if (ClickExportButton != null)
                ClickExportButton(this, EventArgs.Empty);
        }

        protected virtual void OnClickPrintButton(EventArgs e)
        {
            if (ClickPrintButton != null)
                ClickPrintButton(this, EventArgs.Empty);
        }

        protected virtual void OnShowExampleArea(EventArgs e)
        {
            if (ShowExampleArea != null) ShowExampleArea(this, e);
        }

        internal void OnExport()
        {
            OnClickExportButton(EventArgs.Empty);
        }

        internal void OnPrint()
        {
            OnClickPrintButton(EventArgs.Empty);
        }

        internal void OnSwitchCodeArea(bool isCodeAreaView, Grid grid)
        {
            if (isCodeAreaView)
            {
                CodeViewer = grid;
                OnHideExampleArea(EventArgs.Empty);
            }
            else
            {
                Storyboard popupOpenDelayer = new Storyboard();
                popupOpenDelayer.Duration = TimeSpan.FromSeconds(2.0);
                EventHandler handler = null;
                handler = delegate(object sender, EventArgs e)
                              {
                                  OnShowExampleArea(EventArgs.Empty);
                                  popupOpenDelayer.Completed -= handler;
                              };
                popupOpenDelayer.Completed += handler;
                popupOpenDelayer.Begin();
            }
        }

        protected virtual void OnTransitionAnimationCompleted(EventArgs e)
        {
            if (TransitionAnimationCompleted != null) TransitionAnimationCompleted(this, e);
        }

        protected virtual void OnTransitionAnimationStarted()
        {
            if (TransitionAnimationStarted != null) TransitionAnimationStarted(this, EventArgs.Empty);
        }

        protected virtual void OnUnloaded(EventArgs args)
        {
            //if (Unloaded != null) Unloaded(this, args);
        }

        protected virtual void OnLastUpdateChanged(EventArgs args)
        {
            if (LastUpdateChanged != null) LastUpdateChanged(this, args);
        }

        internal void StartTransitionAnimation()
        {
            OnTransitionAnimationStarted();
        }

        internal void Unload()
        {
            OnUnloaded(EventArgs.Empty);
        }

        // Properties
    }
}