﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class StringToBooleanConverter : IValueConverter
    {
        // Methods

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string str = value as string;
            return string.IsNullOrEmpty(str);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}