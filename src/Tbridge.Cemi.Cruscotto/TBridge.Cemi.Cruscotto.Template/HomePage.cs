﻿namespace TBridge.Cemi.Cruscotto.Template
{
    public partial class Homepage : BasePage
    {
        // Fields
        //private bool _contentLoaded;

        // Methods
        public Homepage()
        {
            InitializeComponent();
        }

        protected override void OnCurrentExampleChanged(object oldValue, object newValue)
        {
            exampleArea.Content = newValue;
        }
    }
}