﻿using System;
using System.Runtime.CompilerServices;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class BrowserHistoryEventArgs : EventArgs
    {
        // Fields
        [CompilerGenerated] private string _Path;

        // Methods
        public BrowserHistoryEventArgs(string path)
        {
            Path = path;
        }

        // Properties
        public string Path
        {
            [CompilerGenerated]
            get { return _Path; }
            [CompilerGenerated]
            set { _Path = value; }
        }
    }
}