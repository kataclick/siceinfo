﻿using System;
using System.Collections.Generic;

namespace TBridge.Cemi.Cruscotto.Template
{
    public static class IEnumerableExtension
    {
        // Methods
        public static void ForEach<TSource>(this IEnumerable<TSource> collection, Action<TSource> action)
        {
            foreach (TSource local in collection)
            {
                action(local);
            }
        }
    }
}