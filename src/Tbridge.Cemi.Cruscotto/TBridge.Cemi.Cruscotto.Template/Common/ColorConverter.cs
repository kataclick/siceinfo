﻿using System.Globalization;
using System.Windows.Media;

namespace TBridge.Cemi.Cruscotto.Template.Common
{
    public static class ColorConverter
    {
        public static Color ConvertFromString(string argb)
        {
            uint num;
            if (uint.TryParse(argb.TrimStart(new[] {'#', '0'}), NumberStyles.HexNumber, null, out num))
            {
                uint num2 = 0xff;
                uint num3 = num >> 0x10;
                uint num4 = (num << 8) >> 0x10;
                uint num5 = (num << 0x10) >> 0x10;
                return Color.FromArgb((byte) num2, (byte) num3, (byte) num4, (byte) num5);
            }
            return Colors.Black;
        }
    }
}