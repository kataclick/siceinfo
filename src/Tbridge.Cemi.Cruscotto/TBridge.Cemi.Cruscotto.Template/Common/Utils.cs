﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Resources;

namespace TBridge.Cemi.Cruscotto.Template.Common
{
    internal class Utils
    {
        // Methods
        public static ResourceDictionary GetResourceDictionary(string url)
        {
            Uri uriResource = new Uri(url, UriKind.Relative);
            StreamResourceInfo resourceStream = Application.GetResourceStream(uriResource);
            if ((resourceStream != null) && (resourceStream.Stream != null))
            {
                using (StreamReader reader = new StreamReader(resourceStream.Stream))
                {
                    return (XamlReader.Load(reader.ReadToEnd()) as ResourceDictionary);
                }
            }
            return null;
        }

        public static Style GetSpecificStyle(string styleKey, string url)
        {
            ResourceDictionary resourceDictionary = GetResourceDictionary(url);
            if (resourceDictionary.Contains(styleKey))
            {
                return (resourceDictionary[styleKey] as Style);
            }
            return new Style();
        }

        public static String AuthenticationServiceEndpoint { get; set; }
    }
}