﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template.Common
{
    public static class ThemeManager
    {
        // Fields
        internal static readonly string DefaultThemeName = "Office_Black";
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")] internal static readonly List<string> StandardThemeNames = new List<string>();
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")] internal static readonly Dictionary<string, Theme> StandardThemes = new Dictionary<string, Theme>();

        // Methods
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
        static ThemeManager()
        {
            RegisterTheme("Office_Black", new Office_BlackTheme(), true);
            RegisterTheme("Summer", new SummerTheme(), true);
            RegisterTheme("Vista", new VistaTheme(), true);
        }

        public static Theme FromName(string themeName)
        {
            if (themeName == null)
            {
                return null;
            }
            if (StandardThemes.ContainsKey(themeName))
            {
                return StandardThemes[themeName];
            }
            return StandardThemes[DefaultThemeName];
        }

        private static void RegisterTheme(string name, Theme theme, bool isCommon)
        {
            StandardThemes.Add(name, theme);
            if (isCommon)
            {
                StandardThemeNames.Add(name);
            }
        }
    }
}