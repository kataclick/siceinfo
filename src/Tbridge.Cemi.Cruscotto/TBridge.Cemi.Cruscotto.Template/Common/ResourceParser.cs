﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;
using Telerik.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template.Common
{
    internal static class ResourceParser
    {
        // Fields
        private const string NewXamlNamespace = "http://schemas.microsoft.com/client/2007";
        private const string OldXamlNamespace = "http://schemas.microsoft.com/winfx/2006/xaml/presentation";

        private const string ResourceDictionaryXamlWithPrefix =
            "<ResourceDictionary xmlns=\"http://schemas.microsoft.com/client/2007\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:{1}=\"{2}\"> <{0} x:Key=\"Key\"/> </ResourceDictionary>";

        private const string StyleXaml =
            "<Style xmlns=\"http://schemas.microsoft.com/client/2007\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" TargetType=\"{0}\" />";

        private const string StyleXamlWithPrefix =
            "<Style xmlns=\"http://schemas.microsoft.com/client/2007\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:{1}=\"{2}\" TargetType=\"{0}\" />";

        private static readonly string[] systemAssemblies = new[]
                                                                {
                                                                    "mscorlib.dll", "system.dll", "System.Core.dll",
                                                                    "System.Net.dll", "System.Windows.dll",
                                                                    "System.Windows.Browser.dll", "System.Xml.dll"
                                                                };

        // Methods
        private static bool CheckElement(XmlReader reader)
        {
            string prefix = reader.Prefix;
            if (!string.IsNullOrEmpty(prefix))
            {
                return ((reader.Name.IndexOf(':') < 0) || CheckIfAssemblyIsLoaded(reader, prefix));
            }
            return true;
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static bool CheckIfAssemblyIsLoaded(XmlReader reader, string prefix)
        {
            string assemblyFromPrefix = GetAssemblyFromPrefix(reader, prefix);
            if (string.IsNullOrEmpty(assemblyFromPrefix))
            {
                return true;
            }
            string[] strArray = assemblyFromPrefix.Split(new[] {','});
            if (strArray.Length > 1)
            {
                assemblyFromPrefix = strArray[0];
            }
            string str2 = string.Format("{0}.{1}", assemblyFromPrefix, "dll");
            bool flag = false;
            if (Array.IndexOf(systemAssemblies, str2) > -1)
            {
                flag = true;
            }
            else
            {
                foreach (AssemblyPart part in Deployment.Current.Parts)
                {
                    if (part.Source == str2)
                    {
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag && RadControl.IsInDesignMode)
            {
                string attribute = null;
                if (IsXamlElement<Style>(reader.Name, reader.NamespaceURI) ||
                    IsXamlElement<ControlTemplate>(reader.Name, reader.NamespaceURI))
                {
                    attribute = reader.GetAttribute("TargetType", null);
                }
                else
                {
                    attribute = reader.Name;
                }
                string xaml = string.Format(CultureInfo.InvariantCulture,
                                            "<Style xmlns=\"http://schemas.microsoft.com/client/2007\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" xmlns:{1}=\"{2}\" TargetType=\"{0}\" />",
                                            new object[] {attribute, prefix, reader.LookupNamespace(prefix)});
                try
                {
                    Style style = XamlReader.Load(xaml) as Style;
                    return (style != null);
                }
                catch
                {
                    return false;
                }
            }
            return flag;
        }

        private static string GetAssemblyFromPrefix(XmlReader reader, string prefix)
        {
            string nameSpaceAndAssemblyFromPrefix = GetNameSpaceAndAssemblyFromPrefix(reader, prefix);
            int startIndex = nameSpaceAndAssemblyFromPrefix.IndexOf('=') + 1;
            int length = nameSpaceAndAssemblyFromPrefix.Length - startIndex;
            return nameSpaceAndAssemblyFromPrefix.Substring(startIndex, length);
        }

        private static string GetKeyAttribute(XmlReader reader)
        {
            string attribute = reader.GetAttribute("Key", "http://schemas.microsoft.com/winfx/2006/xaml");
            if (string.IsNullOrEmpty(attribute) && (reader.LocalName == "Style"))
            {
                string str2 = reader.GetAttribute("TargetType", null);
                string prefix = null;
                int index = str2.IndexOf(':');
                if (index > 0)
                {
                    prefix = str2.Substring(0, index);
                    string namespaceFromPrefix = GetNamespaceFromPrefix(reader, prefix);
                    int length = str2.Length - (index + 1);
                    string str5 = str2.Substring(index + 1, length);
                    return string.Format("{0}.{1}", namespaceFromPrefix, str5);
                }
                Style style =
                    XamlReader.Load(string.Format(CultureInfo.InvariantCulture,
                                                  "<Style xmlns=\"http://schemas.microsoft.com/client/2007\" xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\" TargetType=\"{0}\" />",
                                                  new object[] {str2})) as Style;
                if (style != null)
                {
                    attribute = style.TargetType.FullName;
                }
            }
            return attribute;
        }

        private static string GetNameSpaceAndAssemblyFromPrefix(XmlReader reader, string prefix)
        {
            return reader.LookupNamespace(prefix);
        }

        private static string GetNamespaceFromPrefix(XmlReader reader, string prefix)
        {
            string nameSpaceAndAssemblyFromPrefix = GetNameSpaceAndAssemblyFromPrefix(reader, prefix);
            int startIndex = nameSpaceAndAssemblyFromPrefix.IndexOf(':') + 1;
            int length = nameSpaceAndAssemblyFromPrefix.IndexOf(';') - startIndex;
            return nameSpaceAndAssemblyFromPrefix.Substring(startIndex, length);
        }

        private static bool IsStyleOrControlTemplateTargetTypeLoaded(XmlReader reader)
        {
            if (IsXamlElement<Style>(reader.Name, reader.NamespaceURI) ||
                IsXamlElement<ControlTemplate>(reader.Name, reader.NamespaceURI))
            {
                string attribute = reader.GetAttribute("TargetType", null);
                if (string.IsNullOrEmpty(attribute))
                {
                    return true;
                }
                string prefix = null;
                int index = attribute.IndexOf(':');
                if (index > 0)
                {
                    prefix = attribute.Substring(0, index);
                    return CheckIfAssemblyIsLoaded(reader, prefix);
                }
            }
            return true;
        }

        private static bool IsXamlElement<T>(string name, string ns)
        {
            if (!(name == typeof (T).Name))
            {
                return false;
            }
            if (!(ns == "http://schemas.microsoft.com/winfx/2006/xaml/presentation"))
            {
                return (ns == "http://schemas.microsoft.com/client/2007");
            }
            return true;
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public static ResourceDictionary Parse(Stream stream, bool checkTypes, List<string> keys)
        {
            string str;
            using (MemoryStream stream2 = new MemoryStream())
            {
                using (XmlWriter writer = XmlWriter.Create(stream2))
                {
                    using (XmlReader reader = XmlReader.Create(new StreamReader(stream)))
                    {
                        ParseResources(reader, writer, checkTypes, keys);
                    }
                }
                stream2.Seek(0L, SeekOrigin.Begin);
                str = new StreamReader(stream2).ReadToEnd();
            }
            try
            {
                ContentControl control = XamlReader.Load(str) as ContentControl;
                return control.Resources;
            }
            catch
            {
                return null;
            }
        }

        private static void ParseElement(XmlReader reader, XmlWriter writer, bool checkTypes, List<string> keys)
        {
            if ((checkTypes && (reader.Depth == 1)) && !IsStyleOrControlTemplateTargetTypeLoaded(reader))
            {
                reader.Skip();
            }
            else if (((checkTypes && (reader.Depth == 1)) &&
                      (!IsXamlElement<Style>(reader.Name, reader.NamespaceURI) &&
                       !IsXamlElement<ControlTemplate>(reader.Name, reader.NamespaceURI))) && !CheckElement(reader))
            {
                reader.Skip();
            }
            else
            {
                bool isEmptyElement = reader.IsEmptyElement;
                if (reader.Depth == 0)
                {
                    if (!IsXamlElement<ResourceDictionary>(reader.LocalName, reader.NamespaceURI))
                    {
                        throw new InvalidOperationException(
                            "CanOnlyParseXAMLFilesWithResourceDictionaryAsTheRootElement");
                    }
                    writer.WriteStartElement("ContentControl", reader.NamespaceURI);
                    writer.WriteAttributes(reader, true);
                    writer.WriteStartElement("ContentControl.Resources");
                    if (isEmptyElement)
                    {
                        writer.WriteEndElement();
                        writer.WriteEndElement();
                    }
                }
                else
                {
                    if (reader.Depth == 1)
                    {
                        string keyAttribute = GetKeyAttribute(reader);
                        if (!string.IsNullOrEmpty(keyAttribute))
                        {
                            keys.Add(keyAttribute);
                        }
                    }
                    writer.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
                    writer.WriteAttributes(reader, true);
                    if (isEmptyElement)
                    {
                        writer.WriteEndElement();
                    }
                }
            }
        }

        private static void ParseResources(XmlReader reader, XmlWriter writer, bool checkTypes, List<string> keys)
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        ParseElement(reader, writer, checkTypes, keys);
                        break;

                    case XmlNodeType.Text:
                        writer.WriteString(reader.Value);
                        break;

                    case XmlNodeType.CDATA:
                        writer.WriteCData(reader.Value);
                        break;

                    case XmlNodeType.EntityReference:
                        writer.WriteEntityRef(reader.Name);
                        break;

                    case XmlNodeType.ProcessingInstruction:
                    case XmlNodeType.XmlDeclaration:
                        writer.WriteProcessingInstruction(reader.Name, reader.Value);
                        break;

                    case XmlNodeType.Comment:
                        writer.WriteComment(reader.Value);
                        break;

                    case XmlNodeType.DocumentType:
                        writer.WriteDocType(reader.Name, reader.GetAttribute("PUBLIC"), reader.GetAttribute("SYSTEM"),
                                            reader.Value);
                        break;

                    case XmlNodeType.Whitespace:
                    case XmlNodeType.SignificantWhitespace:
                        writer.WriteWhitespace(reader.Value);
                        break;

                    case XmlNodeType.EndElement:
                        writer.WriteFullEndElement();
                        if (reader.Depth == 0)
                        {
                            writer.WriteFullEndElement();
                        }
                        break;
                }
            }
        }
    }
}