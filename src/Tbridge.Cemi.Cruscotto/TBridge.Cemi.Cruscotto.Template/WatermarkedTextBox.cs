﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class WatermarkedTextBox : TextBox
    {
        // Fields
        public static readonly DependencyProperty DefaultTextProperty = DependencyProperty.Register("DefaultText",
                                                                                                    typeof (string),
                                                                                                    typeof (
                                                                                                        WatermarkedTextBox
                                                                                                        ),
                                                                                                    new PropertyMetadata
                                                                                                        ("Set Default Text!!!"));

        // Events

        // Methods
        public WatermarkedTextBox()
        {
            base.TextChanged += new TextChangedEventHandler(OnWotermarkedTextBoxTextChanged);
        }

        public string DefaultText
        {
            get { return (string) base.GetValue(DefaultTextProperty); }
            set { base.SetValue(DefaultTextProperty, value); }
        }

        public event EventHandler WatermarkedTextChanged;

        public override void OnApplyTemplate()
        {
            base.Text = DefaultText;
            base.OnApplyTemplate();
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (string.Equals(base.Text, DefaultText, StringComparison.OrdinalIgnoreCase))
            {
                base.Text = string.Empty;
            }
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(base.Text))
            {
                base.Text = DefaultText;
            }
            base.OnLostFocus(e);
        }

        private void OnWotermarkedTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.Equals(base.Text, DefaultText, StringComparison.OrdinalIgnoreCase))
            {
                WatermarkedTextChanged(this, e);
            }
        }

        // Properties
    }
}