﻿using System.Collections.ObjectModel;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Template.Common;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class XamlSyntaxParser : SyntaxParser
    {
        // Methods
        protected override Collection<LanguageSyntaxStructure> LoadLanguageSyntax(
            Collection<LanguageSyntaxStructure> languageSyntax)
        {
            string regularExpression = @"\G(?<attribute>[a-zA-Z][a-zA-Z0-9.:*_]*\s*(?==))";
            languageSyntax.Add(new LanguageSyntaxStructure(regularExpression, "attribute",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#FF004E"))));
            string str2 = @"\G(?<element>(?<=(<)|(</))[a-zA-Z][a-zA-Z0-9.:*_]*\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str2, "element",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#000000"))));
            string str3 = @"\G(?<comment><!--\s*[\s\S]*\s*-->\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str3, "comment",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#537D01"))));
            string str4 = @"\G(?<tag>(</|<|/>|>)\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str4, "tag",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            string str5 = "\\G(?<string>=\\s*\"[_=#{}a-zA-Z0-9.:;\\s-/,*]*\\s*\"\\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str5, "string",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            string str6 = @"\G(?<content>[^<]+\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str6, "content",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            return base.LoadLanguageSyntax(languageSyntax);
        }

        protected override void SetFileExtension(string extension)
        {
            base.SetFileExtension(".xaml");
        }
    }
}