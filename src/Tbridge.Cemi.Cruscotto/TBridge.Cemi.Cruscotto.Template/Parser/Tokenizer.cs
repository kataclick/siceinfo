﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class Tokenizer
    {
        // Fields

        #region ParserType enum

        public enum ParserType
        {
            CSharp,
            VisualBasic,
            XAML
        }

        #endregion

        private static Regex codeSyntax;
        private Collection<LanguageSyntaxStructure> languageStructure;
        private SyntaxPicker picker = new SyntaxPicker();
        private List<Token> tokens = new List<Token>();

        // Methods
        private string GenerateLanguageSyntaxRegularExpression()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(@"\s*");
            for (int i = 0; i < (languageStructure.Count - 1); i++)
            {
                builder.AppendFormat("{0}|", new object[] {languageStructure[i].RegexString});
            }
            builder.AppendFormat("{0}", new object[] {languageStructure[languageStructure.Count - 1].RegexString});
            builder.Append(@"\s*");
            return builder.ToString();
        }

        private Token Tokenize(Match match)
        {
            for (int i = 0; i < languageStructure.Count; i++)
            {
                if (match.Groups[languageStructure[i].Description].Success)
                {
                    return new Token(match.Groups[languageStructure[i].Description].Value, languageStructure[i].Color);
                }
            }
            return new Token(null, null);
        }

        public List<Token> TokenizeCode(string code, string extension)
        {
            tokens.Clear();
            SyntaxParser parser = picker.FindParserByExtension(extension);
            if (parser == null)
            {
                throw new ArgumentException("No Syntax Parser found that can parse this file!", "extension");
            }
            languageStructure = parser.GenerateLanguageSyntaxPatterns();
            codeSyntax = new Regex(GenerateLanguageSyntaxRegularExpression(),
                                   RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
            MatchCollection matchs = codeSyntax.Matches(code);
            if (parser is HTMLSyntaxParser)
            {
                tokens.AddRange(HTMLSyntaxParser.ApplyStyle(code));
            }
            else if (matchs.Count != 0)
            {
                for (int i = 0; i < matchs.Count; i++)
                {
                    Token item = Tokenize(matchs[i]);
                    tokens.Add(item);
                }
            }
            return tokens;
        }

        // Nested Types
    }
}