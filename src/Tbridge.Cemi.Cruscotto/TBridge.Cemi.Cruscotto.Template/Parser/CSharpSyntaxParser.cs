﻿using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Template.Common;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class CSharpSyntaxParser : SyntaxParser
    {
        // Fields
        private string[] keywords = new string[]
                                        {
                                            "abstract", "as", "base", "break", "byte", "bool", "char", "catch", "case",
                                            "checked", "class", "continue", "const", "decimal", "default", "delegate",
                                            "double", "do", "event", "explicit", "extern", "else", "enum", "finally",
                                            "false", "fixed", "float", "for", "foreach", "goto", "if", "implicit",
                                            "in", "int", "interface", "is", "internal", "lock", "long", "new",
                                            "namespace", "null", "object", "operator", "out", "override", "params",
                                            "private",
                                            "protected", "public", "partial", "return", "readonly", "ref", "struct",
                                            "switch", "sbyte", "sealed", "sizeof", "short", "stackalloc", "static",
                                            "string", "this",
                                            "throw", "true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe",
                                            "ushort", "using", "virtual", "volatile", "void", "while"
                                        };

        // Methods
        protected override Collection<LanguageSyntaxStructure> LoadLanguageSyntax(
            Collection<LanguageSyntaxStructure> languageSyntax)
        {
            string regularExpression = @"\G(?<comment>(\/\/[ \t\S]*\s*\s*)|(\/\*\s*[\s\S]*\s*\*/\s))";
            languageSyntax.Add(new LanguageSyntaxStructure(regularExpression, "comment",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#537D01"))));
            StringBuilder builder = new StringBuilder();
            builder.Append(@"\G(?<keyword>(");
            for (int i = 0; i < keywords.Length; i++)
            {
                builder.Append(keywords[i] + "|");
            }
            builder.Append(keywords[keywords.Length - 1]);
            builder.Append(@")(?=(\.)|(\s+)))");
            languageSyntax.Add(new LanguageSyntaxStructure(builder.ToString(), "keyword",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            string str2 = "\\G(?<string>\"\\s*((\\\\\")|[^\\\"])*\\s*\"\\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str2, "string",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            string str3 = @"\G(?<identifier>[\w]+\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str3, "identifier",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#000000"))));
            string str4 = @"\G(?<other>[\s\S]\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str4, "other",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#000000"))));
            return base.LoadLanguageSyntax(languageSyntax);
        }

        protected override void SetFileExtension(string extension)
        {
            base.SetFileExtension(".cs");
        }
    }
}