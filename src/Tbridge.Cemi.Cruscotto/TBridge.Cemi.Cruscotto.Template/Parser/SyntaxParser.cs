﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class SyntaxParser : IDisposable
    {
        // Fields
        private string defaultExtension = ".default";
        private Collection<LanguageSyntaxStructure> languageSyntaxList = new Collection<LanguageSyntaxStructure>();

        // Methods
        [SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SyntaxParser()
        {
            SetFileExtension(defaultExtension);
        }

        public string GetExtension
        {
            get { return defaultExtension; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion

        public Collection<LanguageSyntaxStructure> GenerateLanguageSyntaxPatterns()
        {
            return LoadLanguageSyntax(languageSyntaxList);
        }

        protected virtual Collection<LanguageSyntaxStructure> LoadLanguageSyntax(
            Collection<LanguageSyntaxStructure> languageSyntax)
        {
            return languageSyntax;
        }

        protected virtual void SetFileExtension(string extension)
        {
            if (!extension.StartsWith(".", StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException("File Extension must start with a '.'!", "extension");
            }
            defaultExtension = extension;
        }

        // Properties
    }
}