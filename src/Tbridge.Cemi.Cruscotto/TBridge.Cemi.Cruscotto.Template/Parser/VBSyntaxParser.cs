﻿using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Template.Common;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class VBSyntaxParser : SyntaxParser
    {
        // Fields
        private string[] keywords = new string[]
                                        {
                                            "AddHandler", "AddressOf", "Alias", "AndAlso", "And", "As", "Boolean",
                                            "ByRef", "Byte", "ByVal", "Call", "Case", "Catch", "CBool", "CByte", "CChar"
                                            ,
                                            "CDate", "CDec", "CDbl", "Char", "CInt", "Class", "CLng", "CObj", "Const",
                                            "Continue", "CSByte", "CShort", "CSng", "CStr", "CType", "CUInt",
                                            "CULng", "CUShort", "Date", "Decimal", "Declare", "Default", "Delegate",
                                            "Dim", "DirectCast", "Double", "Do", "Each", "ElseIf", "Else", "EndIf",
                                            "End",
                                            "Enum", "Erase", "Error", "Event", "Exit", "False", "Finally", "For",
                                            "Friend", "Function", "GetType", "Get", "Global", "GoSub", "GoTo", "Handles"
                                            ,
                                            "If", "Implements", "Imports", "Inherits", "Integer", "Interface", "In",
                                            "IsNot", "Is", "Let", "Lib", "Like", "Long", "Loop", "Me", "Module",
                                            "Mod", "MustInherit", "MustOverride", "MyBase", "MyClass", "Namespace",
                                            "Narrowing", "New", "Next", "Nothing", "Not", "NotInheritable",
                                            "NotOverridable", "Object", "Of", "On",
                                            "Operator", "Option", "Optional", "OrElse", "Or", "Overloads", "Overridable"
                                            , "Overrides", "ParamArray", "Partial", "Private", "Property", "Protected",
                                            "Public", "RaiseEvent", "ReadOnly",
                                            "ReDim", "REM", "RemoveHandler", "Resume", "Return", "SByte", "Select",
                                            "Set", "Shadows", "Shared", "Short", "Single", "Static", "Step", "Stop",
                                            "String",
                                            "Structure", "Sub", "SyncLock", "Then", "Throw", "To", "True", "Try",
                                            "TryCast", "TypeOf", "Variant", "Wend", "UInteger", "ULong", "UShort",
                                            "Using",
                                            "When", "While", "Widening", "With", "WithEvents", "WriteOnly", "Xor"
                                        };

        // Methods
        protected override Collection<LanguageSyntaxStructure> LoadLanguageSyntax(
            Collection<LanguageSyntaxStructure> languageSyntax)
        {
            string regularExpression = @"\G(?<comment>'[ \t\S]*\s*\s)";
            languageSyntax.Add(new LanguageSyntaxStructure(regularExpression, "comment",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#537D01"))));
            StringBuilder builder = new StringBuilder();
            builder.Append(@"\G(?<keyword>(");
            for (int i = 0; i < keywords.Length; i++)
            {
                builder.Append(keywords[i] + "|");
            }
            builder.Append(keywords[keywords.Length - 1]);
            builder.Append(@")(?=(\.)|(\s+)))");
            languageSyntax.Add(new LanguageSyntaxStructure(builder.ToString(), "keyword",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            string str2 = "\\G(?<string>\"\\s*((\\\\\")|[^\\\"])*\\s*\"\\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str2, "string",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#0160E5"))));
            string str3 = @"\G(?<identifier>[\w]+\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str3, "identifier",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#000000"))));
            string str4 = @"\G(?<other>[\s\S]\s*)";
            languageSyntax.Add(new LanguageSyntaxStructure(str4, "other",
                                                           new SolidColorBrush(
                                                               ColorConverter.ConvertFromString("#000000"))));
            return base.LoadLanguageSyntax(languageSyntax);
        }

        protected override void SetFileExtension(string extension)
        {
            base.SetFileExtension(".vb");
        }
    }
}