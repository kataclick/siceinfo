﻿using System.Windows.Media;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class LanguageSyntaxStructure
    {
        // Fields
        private string description;
        private string regexString;
        private Brush segmentColor;

        // Methods
        public LanguageSyntaxStructure(string regularExpression, string description, Brush color)
        {
            regexString = regularExpression;
            segmentColor = color;
            this.description = description;
        }

        // Properties
        public Brush Color
        {
            get { return segmentColor; }
        }

        public string Description
        {
            get { return description; }
        }

        public string RegexString
        {
            get { return regexString; }
        }
    }
}