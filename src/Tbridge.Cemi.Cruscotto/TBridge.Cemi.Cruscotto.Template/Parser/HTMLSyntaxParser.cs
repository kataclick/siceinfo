﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Media;
using System.Xml;
using TBridge.Cemi.Cruscotto.Template.Common;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class HTMLSyntaxParser : SyntaxParser
    {
        // Methods
        public static IEnumerable<Token> ApplyStyle(string htmlValue)
        {
            Collection<Token> collection = new Collection<Token>();
            using (StringReader reader = new StringReader("<xml>" + htmlValue + "</xml>"))
            {
                bool isBold = false;
                bool isItalic = false;
                using (XmlReader reader2 = XmlReader.Create(reader))
                {
                    while (reader2.Read())
                    {
                        string str = reader2.Name.ToLower();
                        switch (reader2.NodeType)
                        {
                            case XmlNodeType.Element:
                            case XmlNodeType.EndElement:
                                {
                                    string str2 = str.ToLower();
                                    if (str2 != null)
                                    {
                                        if ((!(str2 == "bold") && !(str2 == "strong")) && !(str2 == "b"))
                                        {
                                            if (str2 == "i")
                                            {
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            isBold = !isBold;
                                        }
                                    }
                                    continue;
                                }
                            case XmlNodeType.Attribute:
                            case XmlNodeType.SignificantWhitespace:
                                {
                                    continue;
                                }
                            case XmlNodeType.Text:
                                {
                                    Brush specificBrush = GetSpecificBrush(HtmlTextDecoration.Bold);
                                    collection.Add(new Token(reader2.Value, specificBrush, isBold, isItalic));
                                    continue;
                                }
                            case XmlNodeType.Whitespace:
                                {
                                    collection.Add(new Token("\n", GetSpecificBrush(HtmlTextDecoration.None)));
                                    continue;
                                }
                            default:
                                {
                                    continue;
                                }
                        }
                        isItalic = !isItalic;
                    }
                    return collection;
                }
            }
        }

        public static Brush GetSpecificBrush(HtmlTextDecoration textDecoration)
        {
            switch (textDecoration)
            {
                case HtmlTextDecoration.Bold:
                case HtmlTextDecoration.Italic:
                    return new SolidColorBrush(ColorConverter.ConvertFromString("#ff4fbfe4"));
            }
            return new SolidColorBrush(Colors.Black);
        }

        protected override Collection<LanguageSyntaxStructure> LoadLanguageSyntax(
            Collection<LanguageSyntaxStructure> languageSyntax)
        {
            string regularExpression = @"<br \>";
            languageSyntax.Add(new LanguageSyntaxStructure(regularExpression, "Breaks", new SolidColorBrush(Colors.Red)));
            string str2 = @"<\s*(b|B|strong|STRONG)\s*>.*<\s*/(b|B|strong|STRONG)\s*>";
            languageSyntax.Add(new LanguageSyntaxStructure(str2, "Bold",
                                                           new SolidColorBrush(Color.FromArgb(0xff, 0, 0, 100))));
            string str3 = @"(?<Italic><\s*(i|I)\s*>.*<\s*/(|i|I)\s*>)";
            languageSyntax.Add(new LanguageSyntaxStructure(str3, "Italic", new SolidColorBrush(Colors.Green)));
            return base.LoadLanguageSyntax(languageSyntax);
        }

        protected override void SetFileExtension(string extension)
        {
            base.SetFileExtension(".txt");
        }
    }
}