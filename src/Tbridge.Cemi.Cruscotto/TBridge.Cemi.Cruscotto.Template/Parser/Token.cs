﻿using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class Token
    {
        // Fields

        #region TokenType enum

        public enum TokenType
        {
            KeyWord,
            Comment,
            String,
            Identifier,
            Other,
            None
        }

        #endregion

        [CompilerGenerated] private Brush _Color;
        [CompilerGenerated] private bool _IsBold;
        [CompilerGenerated] private bool _IsItalic;
        [CompilerGenerated] private string _Value;

        // Methods
        public Token(string value, Brush color) : this(value, color, false, false)
        {
        }

        public Token(string value, Brush color, bool isBold, bool isItalic)
        {
            Value = value;
            Color = color;
            IsBold = isBold;
            IsItalic = isItalic;
        }

        // Properties
        public Brush Color
        {
            [CompilerGenerated]
            get { return _Color; }
            [CompilerGenerated]
            set { _Color = value; }
        }

        public bool IsBold
        {
            [CompilerGenerated]
            get { return _IsBold; }
            [CompilerGenerated]
            set { _IsBold = value; }
        }

        public bool IsItalic
        {
            [CompilerGenerated]
            get { return _IsItalic; }
            [CompilerGenerated]
            set { _IsItalic = value; }
        }

        public string Value
        {
            [CompilerGenerated]
            get { return _Value; }
            [CompilerGenerated]
            set { _Value = value; }
        }

        public Run GetRun()
        {
            Run run = new Run();
            run.Text = Value;
            run.Foreground = Color;
            run.FontWeight = IsBold ? FontWeights.ExtraBlack : FontWeights.Normal;
            run.FontStyle = IsItalic ? FontStyles.Italic : FontStyles.Normal;
            return run;
        }

        // Nested Types
    }
}