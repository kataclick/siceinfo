﻿namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    public enum HtmlTextDecoration
    {
        Bold,
        Italic,
        None
    }
}