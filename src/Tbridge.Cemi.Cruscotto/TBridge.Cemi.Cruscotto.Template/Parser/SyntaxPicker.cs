﻿using System.Collections.Generic;

namespace TBridge.Cemi.Cruscotto.Template.Parser
{
    internal class SyntaxPicker
    {
        // Fields
        private List<SyntaxParser> parsers = new List<SyntaxParser>();

        // Methods
        internal SyntaxPicker()
        {
            AddParser(new CSharpSyntaxParser());
            AddParser(new VBSyntaxParser());
            AddParser(new XamlSyntaxParser());
            AddParser(new HTMLSyntaxParser());
        }

        internal void AddParser(SyntaxParser parser)
        {
            parsers.Add(parser);
        }

        internal SyntaxParser FindParserByExtension(string extension)
        {
            foreach (SyntaxParser parser in parsers)
            {
                if (parser.GetExtension == extension)
                {
                    return parser;
                }
            }
            return null;
        }
    }
}