﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Browser;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class BrowserHistoryManager
    {
        // Events

        // Methods
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
        static BrowserHistoryManager()
        {
            BrowserHistoryManager instance = new BrowserHistoryManager();
            HtmlPage.RegisterScriptableObject(instance.GetType().Name, instance);
        }

        public static event EventHandler<BrowserHistoryEventArgs> BrowserHistoryNavigate;

        internal static void AddBrowserHistory(string currentPage, string pageTitle)
        {
            Action a = null;
            if (HtmlPage.Document.GetProperty("addHistoryPoint") != null)
            {
                HtmlPage.Document.Invoke("addHistoryPoint", new object[] {currentPage, pageTitle});
            }
            else
            {
                if (a == null)
                {
                    a = delegate { AddBrowserHistory(currentPage, pageTitle); };
                }
                Deployment.Current.Dispatcher.BeginInvoke(a);
            }
        }

        [ScriptableMember]
        public void HandleNavigate(string state)
        {
            if (BrowserHistoryNavigate != null)
            {
                BrowserHistoryNavigate(this, new BrowserHistoryEventArgs(state));
            }
        }
    }
}