﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using TBridge.Silverlight.Authentication;
using Telerik.Windows.Controls;
using TBridge.Cemi.Cruscotto.Template.Animation;
using TBridge.Cemi.Cruscotto.Template.AuthenticationService;
using SelectionChangedEventArgs=Telerik.Windows.Controls.SelectionChangedEventArgs;
using SelectionChangedEventHandler=Telerik.Windows.Controls.SelectionChangedEventHandler;

namespace TBridge.Cemi.Cruscotto.Template
{
    public class MainPage : Control
    {
        // Fields
        [CompilerGenerated] private bool _AsyncDownloadExamples;
        [CompilerGenerated] private ExamplesViewModel _ViewModel;
        private LoginControl loginControl;
        private CategoriesButton impreseButton;
        private FrameworkElement codePanel;
        private CategoriesButton crcButton;
        private RadFrame exampleContainer;
        private FrameworkElement examplesMask;
        private FrameworkElement examplesPanel;
        private RadTreeView examplesTree;
        private bool firstTime = true;
        private FrameworkElement layoutRoot;
        private LoadingSpin LoadingVisual;
        private CategoriesButton startButton;
        //Messi ora
        private CategoriesButton prestazioniButton;
        private CategoriesButton repButton;
        private FrameworkElement searchBox;
        private Button searchButton;
        //Fine
        private WatermarkedTextBox searchTextBox;
        private FrameworkElement splashLoader;
        private CategoriesButton testButton;
        private FrameworkElement topLeftLogo;
        //private FrameworkElement topRightButtons;
        private FrameworkElement topRightPanel;
        private FrameworkElement treeViewPanel;
        private CategoriesButton winButton;
        private CategoriesButton massaSalarialeButton;

        // Methods
        public MainPage()
        {
            base.DefaultStyleKey = typeof (MainPage);
            base.DataContext = ViewModel = new ExamplesViewModel();
            ViewModel.SelectedExampleChanged += new EventHandler(OnSelectedExampleChanged);
        }

        public void SetAuthenticationServiceEndpoint(String endpoint)
        {
            Common.Utils.AuthenticationServiceEndpoint = endpoint;
        }

        public bool AsyncDownloadExamples
        {
            [CompilerGenerated]
            get { return _AsyncDownloadExamples; }
            [CompilerGenerated]
            set { _AsyncDownloadExamples = value; }
        }

        private RadTreeView ExamplesTree
        {
            get { return examplesTree; }
            set
            {
                if (examplesTree != null)
                {
                    examplesTree.SelectionChanged -= new SelectionChangedEventHandler(OnExamplesTreeSelectionChanged);
                }
                examplesTree = value;
                examplesTree.SelectionChanged += new SelectionChangedEventHandler(OnExamplesTreeSelectionChanged);
            }
        }

        private Button SearchButton
        {
            get { return searchButton; }
            set
            {
                searchButton = value;
                searchButton.Click += delegate(object sender, RoutedEventArgs e) { SearchForExample(); };
            }
        }

        //Inserito ora
        private LoginControl LoginControl
        {
            get { return loginControl; }
            set { loginControl = value; }
        }

        private CategoriesButton SelectedCategory { get; set; }

        private CategoriesButton PrestazioniButton
        {
            get { return prestazioniButton; }
            set
            {
                if (prestazioniButton != null)
                {
                    prestazioniButton.Click -= OnCategoryButtonClick;
                }
                prestazioniButton = value;
                prestazioniButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton MassaSalarialeButton
        {
            get { return massaSalarialeButton; }
            set
            {
                if (massaSalarialeButton != null)
                {
                    massaSalarialeButton.Click -= OnCategoryButtonClick;
                }
                massaSalarialeButton = value;
                massaSalarialeButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton CRCButton
        {
            get { return crcButton; }
            set
            {
                if (crcButton != null)
                {
                    crcButton.Click -= OnCategoryButtonClick;
                }
                crcButton = value;
                crcButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton ImpreseButton
        {
            get { return impreseButton; }
            set
            {
                if (impreseButton != null)
                {
                    impreseButton.Click -= OnCategoryButtonClick;
                }
                impreseButton = value;
                impreseButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton StartButton
        {
            get { return startButton; }
            set
            {
                if (startButton != null)
                {
                    startButton.Click -= OnCategoryButtonClick;
                }
                startButton = value;
                startButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton TestButton
        {
            get { return testButton; }
            set
            {
                if (testButton != null)
                {
                    testButton.Click -= OnCategoryButtonClick;
                }
                testButton = value;
                testButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton WINButton
        {
            get { return winButton; }
            set
            {
                if (winButton != null)
                {
                    winButton.Click -= OnCategoryButtonClick;
                }
                winButton = value;
                winButton.Click += OnCategoryButtonClick;
            }
        }

        private CategoriesButton REPButton
        {
            get { return repButton; }
            set
            {
                if (repButton != null)
                {
                    repButton.Click -= OnCategoryButtonClick;
                }
                repButton = value;
                repButton.Click += OnCategoryButtonClick;
            }
        }

        //FINE
        private WatermarkedTextBox SearchTextBox
        {
            get { return searchTextBox; }
            set
            {
                if (searchTextBox != null)
                {
                    searchTextBox.WatermarkedTextChanged -= new EventHandler(OnSearchTextChanged);
                    searchTextBox.KeyDown -= new KeyEventHandler(OnSearchTextKeyDown);
                }
                searchTextBox = value;
                searchTextBox.WatermarkedTextChanged += new EventHandler(OnSearchTextChanged);
                searchTextBox.KeyDown += new KeyEventHandler(OnSearchTextKeyDown);
            }
        }

        internal ExamplesViewModel ViewModel
        {
            [CompilerGenerated]
            get { return _ViewModel; }
            [CompilerGenerated]
            set { _ViewModel = value; }
        }

        internal void ApplyMainPageTemplate()
        {
            LoadingVisual = (LoadingSpin) base.GetTemplateChild("LoadingVisual");
            splashLoader = base.GetTemplateChild("splashLoader") as FrameworkElement;
            topLeftLogo = base.GetTemplateChild("topLeftLogo") as FrameworkElement;
            layoutRoot = base.GetTemplateChild("layoutRoot") as FrameworkElement;
            //this.topRightButtons = base.GetTemplateChild("topRightButtons") as FrameworkElement;
            topRightPanel = base.GetTemplateChild("topRightPanel") as FrameworkElement;
            codePanel = base.GetTemplateChild("codeViewerPanel") as FrameworkElement;
            searchBox = base.GetTemplateChild("searchBox") as FrameworkElement;
            treeViewPanel = base.GetTemplateChild("treeViewPanel") as FrameworkElement;
            examplesPanel = base.GetTemplateChild("examplesPanel") as FrameworkElement;
            examplesMask = base.GetTemplateChild("examplesMask") as FrameworkElement;
            NavigationService.GetNavigationService().RootPanel =
                (RadFrameContainer) base.GetTemplateChild("radFameContainer");
            BrowserHistoryManager.BrowserHistoryNavigate +=
                delegate(object sender, BrowserHistoryEventArgs e) { OnBrowserHistoryNavigateCompleted(e.Path); };
            if (firstTime)
            {
                base.Dispatcher.BeginInvoke(delegate
                                                {
                                                    layoutRoot.Opacity = 1.0;
                                                    splashLoader.Opacity = 1.0;
                                                    BeginInitialAnimation();
                                                });
                (exampleContainer.Parent as Panel).SizeChanged += delegate(object sender, SizeChangedEventArgs e)
                                                                      {
                                                                          Panel panel = sender as Panel;
                                                                          panel.Clip = new RectangleGeometry
                                                                                           {
                                                                                               Rect =
                                                                                                   new Rect(
                                                                                                   new Point(),
                                                                                                   e.NewSize)
                                                                                           };
                                                                      };
            }
        }

        private void BeginInitialAnimation()
        {
            double actualWidth = topLeftLogo.ActualWidth;
            double width = treeViewPanel.Width;
            double num2 = examplesPanel.ActualWidth;
            //double actualHeight = this.topRightButtons.ActualHeight;
            Spline spline = new Spline(0.321000009775162, 0.0, 0.45100000500679, 1.0);
            double[] args = new double[4];
            args[1] = 1.0;
            args[2] = 1.0;
            double[] numArray2 = new double[6];
            numArray2[2] = 1.0;
            numArray2[4] = 2.0;
            numArray2[5] = 1.0;
            double[] numArray3 = new double[6];
            numArray3[1] = actualWidth;
            numArray3[2] = 1.0;
            numArray3[3] = actualWidth;
            numArray3[4] = 2.0;
            double[] numArray4 = new double[6];
            numArray4[2] = 2.0;
            numArray4[4] = 3.0;
            numArray4[5] = 1.0;
            double[] numArray5 = new double[6];
            numArray5[1] = -actualWidth;
            numArray5[2] = 2.0;
            numArray5[3] = -actualWidth;
            numArray5[4] = 3.0;
            double[] numArray6 = new double[6];
            numArray6[1] = 1.0;
            numArray6[2] = 5.0;
            numArray6[3] = 1.0;
            numArray6[4] = 5.5;
            double[] numArray7 = new double[6];
            numArray7[1] = 0.8;
            numArray7[2] = 5.0;
            numArray7[3] = 0.8;
            numArray7[4] = 5.5;
            numArray7[5] = 1.0;
            double[] numArray8 = new double[6];
            numArray8[1] = num2;
            numArray8[2] = 3.0;
            numArray8[3] = num2;
            numArray8[4] = 5.0;
            //double[] numArray9 = new double[6];
            //numArray9[1] = 0;//-actualHeight;
            //numArray9[2] = 5.5;
            //numArray9[3] = 0;//-actualHeight;
            //numArray9[4] = 6.0;
            Storyboard instance;
            if (firstTime)
                instance =
                    splashLoader.Animate().Opacity(args).Animate(new FrameworkElement[] {topLeftLogo}).
                        EnsureDefaultTransforms().Opacity(numArray2).MoveX(numArray3).Splines(2, spline).Animate(
                        new FrameworkElement[] {treeViewPanel, searchBox}).EnsureDefaultTransforms().Opacity(numArray4).
                        MoveX(numArray5).Splines(2, spline).Animate(new FrameworkElement[] {examplesMask}).Opacity(
                        numArray6).Animate(new FrameworkElement[] {examplesPanel}).EnsureDefaultTransforms().Origin(
                        0.0, 0.5).Scale(numArray7).Splines(2, spline).MoveX(numArray8).Splines(2, spline).Instance;
                    //.Animate(new FrameworkElement[] { /*this.topRightButtons,*/ this.topRightPanel }).EnsureDefaultTransforms().MoveY(numArray9).Splines(2, spline).Instance;
            else
            {
                instance =
                    splashLoader.Animate().Opacity(args).Animate(new FrameworkElement[] {examplesMask}).Opacity(
                        numArray6).Animate(new FrameworkElement[] {examplesPanel}).EnsureDefaultTransforms().Origin(
                        0.0, 0.5).Scale(numArray7).Splines(2, spline).MoveX(numArray8).Splines(2, spline).Instance;
                    //.Animate(new FrameworkElement[] { /*this.topRightButtons,*/ this.topRightPanel }).EnsureDefaultTransforms().MoveY(numArray9).Splines(2, spline).Instance;
            }
            firstTime = false;
            instance.SpeedRatio = 1.5;
            instance.Completed += new EventHandler(OnInitialAnimationCompleted);
            instance.Begin();
        }

        internal void Navigate(IExampleItem item)
        {
            Action<object> callBack = null;
            if (item.IsExample)
            {
                string shortTypeName = item.ShortTypeName;
                LoadingVisual.Begin();
                if (callBack == null)
                {
                    callBack = delegate(object result)
                                   {
                                       NavigateToExample(result as Example, item);
                                       LoadingVisual.Stop();
                                       if (!item.IsSelected)
                                       {
                                           item.IsSelected = !item.IsSelected;
                                       }
                                   };
                }
                ApplicationHelper.LoadAssemblies(item.Path, shortTypeName, callBack);
            }
            BrowserHelper.SetBrowserTitle(ViewModel.BrowserTitle);
        }

        private void NavigateToExample(Example example, IExampleItem item)
        {
            BasePage validPage = BasePage.GetValidPage(item);
            example.CodicePrestazione = item.CodicePrestazione;
            validPage.SelectedExample = example;
            validPage.ExampleItem = item;
            exampleContainer.NavigateToExample(validPage);
        }

        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private void NavigateToPage(IExampleItem item)
        {
            //BrowserHistoryManager.AddBrowserHistory(item.Path, string.Empty);
            BrowserHistoryManager.AddBrowserHistory(item.CodicePrestazione, string.Empty);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            exampleContainer = (RadFrame) base.GetTemplateChild("ExampleContainer");
            ExamplesTree = (RadTreeView) base.GetTemplateChild("ExamplesTree");
            SearchButton = (Button) base.GetTemplateChild("searchButton");
            MassaSalarialeButton = (CategoriesButton)base.GetTemplateChild("massaSalarialeButton");
            PrestazioniButton = (CategoriesButton) base.GetTemplateChild("prestazioniButton");
            CRCButton = (CategoriesButton) base.GetTemplateChild("crcButton");
            ImpreseButton = (CategoriesButton) base.GetTemplateChild("impreseButton");
            StartButton = (CategoriesButton) base.GetTemplateChild("startButton");
            LoginControl = (LoginControl) base.GetTemplateChild("loginControl");
            SearchTextBox = (WatermarkedTextBox) base.GetTemplateChild("searchTextBox");

            //this.WINButton = (CategoriesButton)base.GetTemplateChild("winButton");
            //this.TestButton = (CategoriesButton)base.GetTemplateChild("testButton");
            //this.ORMButton = (CategoriesButton)base.GetTemplateChild("ormButton");
            //this.REPButton = (CategoriesButton)base.GetTemplateChild("repButton");)

            if (firstTime)
            {
                SelectedCategory = StartButton;
                SelectedCategory.Click -= OnCategoryButtonClick;
                LoginControl.LoginEvent += OnLogin;
                LoginControl.LogoutEvent += OnLogout;
            }

            ApplyMainPageTemplate();
            ViewModel.PathToXML = SelectedCategory.MenuFilePath;
            string currentBookmark = BrowserHelper.GetCurrentBookmark(SelectedCategory.CategoryBookmark);
            ViewModel.SelectedExample = this.ViewModel.FindExampleBypath(currentBookmark);
        }

        private void OnLogin(object sender, User e)
        {
            if (e.Roles.Contains("CruscottoMassaSalariale"))
            {
                MassaSalarialeButton.IsEnabled = true;
            }
            if (e.Roles.Contains("CruscottoPrestazioni"))
            {
                PrestazioniButton.IsEnabled = true;
            }
            if (e.Roles.Contains("CruscottoCrc"))
            {
                CRCButton.IsEnabled = true;
            }
            if (e.Roles.Contains("CruscottoImprese"))
            {
                ImpreseButton.IsEnabled = true;
            }

            if (e.Roles.Contains("CruscottoMassaSalariale"))
            {
                OnCategoryButtonClick(MassaSalarialeButton, null);
            }
            else if (e.Roles.Contains("CruscottoPrestazioni"))
            {
                OnCategoryButtonClick(PrestazioniButton, null);
            }
            else if (e.Roles.Contains("CruscottoCrc"))
            {
                OnCategoryButtonClick(CRCButton, null);
            }
            else if (e.Roles.Contains("CruscottoImprese"))
            {
                OnCategoryButtonClick(ImpreseButton, null);
            }
        }

        private void OnLogout(object sender)
        {
            PrestazioniButton.IsEnabled = false;
            CRCButton.IsEnabled = false;
            ImpreseButton.IsEnabled = false;
            MassaSalarialeButton.IsEnabled = false;
            OnCategoryButtonClick(StartButton, null);
        }

        private void OnBrowserHistoryNavigateCompleted(string path)
        {
            // IExampleItem item = this.ViewModel.FindExampleBypath(path);
            IExampleItem item = ViewModel.FindExampleByCode((path));

            if (item != null)
            {
                Navigate(item);
            }
        }

        private void OnExamplesTreeSelectionChanged(object sender, SelectionChangedEventArgs e1)
        {
            ExampleItem selectedItem = ExamplesTree.SelectedItem as ExampleItem;
            if (selectedItem == null)
                selectedItem = (ExampleItem) ExamplesTree.Items[0];
            if ((selectedItem.IsExample || (selectedItem.Parent == null)) && (selectedItem != null))
            {
                ViewModel.SelectedExample = selectedItem;
            }
        }

        private void OnInitialAnimationCompleted(object sender, EventArgs args)
        {
            NavigationService navigationService = NavigationService.GetNavigationService();
            if ((navigationService.RootPanel != null) && (navigationService.RootPanel.Children.Count > 0))
            {
                BasePage page = navigationService.RootPanel.Children[0] as BasePage;
                if (page != null)
                {
                    page.SelectedExample.AnimationCompleted();
                }
            }
        }

        private void OnSearchTextChanged(object sender, EventArgs e)
        {
            ViewModel.SearchText = searchTextBox.Text;
        }

        private void OnSearchTextKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchForExample();
            }
        }

        private void OnSelectedExampleChanged(object sender, EventArgs e)
        {
            IExampleItem selectedExample = ViewModel.SelectedExample;
            if (selectedExample != null)
            {
                NavigateToPage(selectedExample);
            }
        }

        private void SearchForExample()
        {
            ViewModel.SelectFirstVisibleExample();
        }

        private void OnCategoryButtonClick(object sender, RoutedEventArgs e)
        {
            CategoriesButton temp = new CategoriesButton();
            temp.Style = ((CategoriesButton) sender).Style;
            ((CategoriesButton) sender).Style = SelectedCategory.Style;
            SelectedCategory.Style = temp.Style;
            SelectedCategory.Click += OnCategoryButtonClick;
            SelectedCategory = ((CategoriesButton) sender);
            ((CategoriesButton) sender).Click -= OnCategoryButtonClick;

            ExamplesTree.SelectionChanged -= OnExamplesTreeSelectionChanged;

            ViewModel = new ExamplesViewModel();
            ViewModel.PathToXML = SelectedCategory.MenuFilePath;
            ViewModel.SelectedExampleChanged += OnSelectedExampleChanged;

            examplesTree.SelectionChanged += OnExamplesTreeSelectionChanged;

            HtmlPage.Window.CurrentBookmark = "";

            OnApplyTemplate();


            ExamplesTree.ItemsSource = ViewModel.Examples;
        }

        // Properties
    }
}