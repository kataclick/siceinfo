﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebSiteCruscotto
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            SaveSilverlightDeploymentSettings(ParamInitParams);
        }

        private void SaveSilverlightDeploymentSettings(Literal litSettings)
        {
            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            StringBuilder sb = new StringBuilder();
            sb.Append("<param name=\"InitParams\" value=\"");

            int settingCount = appSettings.Count;
            for (int index = 0; index < settingCount; index++)
            {
                sb.Append(appSettings.GetKey(index));
                sb.Append("=");
                sb.Append(appSettings[index]);
                sb.Append(",");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.Append("\" />");

            litSettings.Text = sb.ToString();
        }
    }
}