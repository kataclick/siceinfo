﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebSiteCruscotto.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="height: 100%; overflow: hidden;">
<head id="Head1" runat="server">
    <title>Cruscotto Direzionale</title>
    <link rel="stylesheet" href="style/style.css" type="text/css" />
    <style type="text/css">
        #silverlightControlHost
        {
            height: 100%;
        }
    </style>
    <script type="text/javascript" src="SplashScreen/splashscreen.js"></script>
    <script type="text/javascript" src="Silverlight.js"></script>
    <script type="text/javascript">
        function onSilverlightError(sender, args) {

            var appSource = "";
            if (sender != null && sender != 0) {
                appSource = sender.getHost().Source;
            }
            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            var errMsg = "Unhandled Error in Silverlight Application " + appSource + "\n";

            errMsg += "Code: " + iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " + args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }
    </script>
</head>
<body style="height: 100%; margin: 0; padding: 0; background: #084E85; overflow: hidden;">
    <form id="form1" runat="server" style="height: 100%; background: #084E85;">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableHistory="true">
        <Scripts>
            <asp:ScriptReference Path="BrowserHistoryScript.js" />
        </Scripts>
    </asp:ScriptManager>
    <div id="silverlightControlHost">
        <object data="data:application/x-silverlight-2," id="Xaml1" type="application/x-silverlight-2"
            width="100%" height="100%">
            <%
                string strSourceFile = @"ClientBin/SilverlightCube.xap";
                string param;
                if (System.Diagnostics.Debugger.IsAttached)
                    param = "<param name=\"source\" value=\"" + strSourceFile + "\" />";
                else
                {
                    string xappath = HttpContext.Current.Server.MapPath(@"") + @"\" + strSourceFile;
                    DateTime xapCreationDate = System.IO.File.GetLastWriteTime(xappath);
                    param = "<param name=\"source\" value=\"" + strSourceFile + "?ignore="
                            + xapCreationDate.ToString() + "\" />";
                }
                Response.Write(param);
            %>
            <param name="onerror" value="onSilverlightError" />
            <param name="background" value="#084E85" />
            <param name="windowless" value="false" />
            <param name="minRuntimeVersion" value="4.0.50826.0" />
            <param name="autoUpgrade" value="true" />
            <param name="EnableRedrawRegions" value="false" />
            <param name="HtmlAccessEnabled" value="true" />
            <param name="SplashScreenSource" value="SplashScreen/SplashScreen.xaml" />
            <asp:Literal ID="ParamInitParams" runat="server"></asp:Literal>
            <param name="onsourcedownloadprogresschanged" value="onSourceDownloadProgressChanged" />
            <param name="ScaleMode" value="Stretch" />
            <div class="background" style="margin: 0px; padding: 0px;">
            </div>
        </object>
        <iframe id="__historyFrame" style='visibility: hidden; height: 0; width: 0; border: 0px'>
        </iframe>
    </div>
    </form>
</body>
</html>
