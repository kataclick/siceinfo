﻿function pageLoad() {
    //Called when the bookmark changes.
	onNavigate = function(sender, state) {

		if (state._state.q == undefined) {
			state._state.q = "";
		}

		var plugIn = $get("Xaml1"); 

		if (plugIn && plugIn.type == 'application/x-silverlight-2') {
			plugIn.Content.BrowserHistoryManager.HandleNavigate(state.get_state().q);
		}
	}

    //Adds a history point
    document.addHistoryPoint = function(entry, title) {
    	Sys.Application.addHistoryPoint({ q: entry }, title);
    }

    //Serialzie override, we would just like to display a string.
    Sys.Application._serializeState = function(state) {
        return (state != null) ? state.q : "";
    }

    //The bookmark needs to translate to an object for the internal handling.
    Sys.Application._deserializeState = function(state) {
        return { q: state };
    }

    //Sign up for the navigate event:
    
    Sys.Application.add_navigate(onNavigate);
}
