﻿function OnBackgroundImageDownLoadProgressChanged(sender, args) {
    if (sender.DownloadProgress >= 1) {
        var qsf = document.getElementById("Xaml1");
        if (document.body.SplashScreenAnimationStarted == undefined) {
            document.body.SplashScreenAnimationStarted = true;
            var cycleAnimation = qsf.content.findName("CycleAnimation");
            cycleAnimation.Begin();

            var overlays = qsf.content.findName("overlays");
            overlays.opacity = 1;
        }
    }
}

function onSourceDownloadProgressChanged(sender, eventArgs) {

    var progress = eventArgs.progress;
    var totalWidth = document.body.clientWidth;
    var totalHeight = document.body.clientHeight;

    var loaderWidth = 226;
    var loaderHeight = 222;

    var background = sender.getHost().content.findName("background");
    background.Width = totalWidth;
    background.Height = totalHeight;

    var loader = sender.getHost().content.findName("loader");
    loader.setValue("Canvas.Left", Math.round((totalWidth - loaderWidth) / 2));
    loader.setValue("Canvas.Top", Math.round((totalHeight - loaderHeight) / 2));

    var progressBar = sender.getHost().content.findName("ProgressBar");
    progressBar.width = progress * 201;

    //Set the opacity of the text blocks accordingly:
    var textBlockCount = 4;
    var visibleRange = 1 / textBlockCount;
    for (var i = 1; i <= textBlockCount; i++) {
        var textBlock = sender.getHost().content.findName("text" + i.toString());
        if (progress > (i - 1) * visibleRange && progress < i * visibleRange) {
            textBlock.opacity = Math.max(1 - (i - 1) * visibleRange / progress, 0.9);
        }
        else {
            textBlock.opacity = 0;
        }
    }
}

