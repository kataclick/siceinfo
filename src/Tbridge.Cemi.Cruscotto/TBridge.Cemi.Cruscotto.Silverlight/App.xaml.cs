﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Browser;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;

namespace TBridge.Cemi.Cruscotto.Silverlight
{
    public partial class App : Application
    {
        public App()
        {
            Startup += Application_Startup;
            Exit += Application_Exit;
            UnhandledException += Application_UnhandledException;

            InitializeComponent();
        }

        void App_CheckAndDownloadUpdateCompleted(object sender, CheckAndDownloadUpdateCompletedEventArgs e)
        {
            if (e.UpdateAvailable)
                MessageBox.Show("The new version is downloaded, please restart this application to launch the new version.");
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Common.Common.InizializzaDictionary();

            string endpoint;
            e.InitParams.TryGetValue("CubeBrowserService", out endpoint);
            Common.Common.CompletePath = endpoint;

            e.InitParams.TryGetValue("AuthenticationService", out endpoint);

            CheckAndDownloadUpdateCompleted += App_CheckAndDownloadUpdateCompleted;
            CheckAndDownloadUpdateAsync();

    
            RootVisual = new CubeContainer(false,endpoint);
        }

        private void Application_Exit(object sender, EventArgs e)
        {
        }

        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            // If the app is running outside of the debugger then report the exception using
            // the browser's exception mechanism. On IE this will display it a yellow alert 
            // icon in the status bar and Firefox will display a script error.
            if (!Debugger.IsAttached)
            {
                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });
            }
        }

        private void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e)
        {
            try
            {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg +
                                     "\");");
            }
            catch (Exception)
            {
            }
        }
    }
}