﻿using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using Telerik.Windows.Controls.Charting;

namespace TBridge.Cemi.Cruscotto.Silverlight.Common
{
    public class SeriesExtensions
    {
        public void FillWithSalesData(DataSeries series)
        {
            //foreach (DataViewModel item in GetSalesData())
            //{
            //    DataPoint point = new DataPoint();
            //    point.YValue = item.Data.Volume;
            //    point.XCategory = item.Data.XCategory;
            //    point.DataItem = item;

            //    series.Add(point);
            //}
        }

        public static DataSeries GetSeries(AdoMdHelperCellStone cs, string axisValue, int axis)
        {
            DataSeries result = new DataSeries();

            try
            {
                int countcolumn = cs.Rows[0].Columns.Count;
                int countrow = cs.Rows.Count;

                if (axis == 1)
                {
                    for (int i = 1; i < countrow; i++)
                    {
                        for (int j = 1; j < countcolumn; j++)
                        {
                            if (cs.Rows[i].Columns[0].Caption == axisValue)
                            {
                                if (!cs.Rows[0].Columns[j].Caption.Equals(""))
                                {
                                    if (cs.Rows[0].Columns[j].Caption.Equals("Unknown"))
                                        cs.Rows[0].Columns[j].Caption = "0";
                                    DataPoint dp = new DataPoint { LegendLabel = cs.Rows[0].Columns[j].Caption };
                                    string value = cs.Rows[i].Columns[j].Caption;
                                    value = value.StartsWith("$") ? value.Substring(1).Replace(",", "") : value.Replace(".", ",");
                                    if (value.Equals("1,#INF"))
                                        value = "0";
                                    dp.YValue = double.Parse(value);
                                    dp.XCategory = dp.LegendLabel;
                                    result.Add(dp);
                                }
                            }
                        }
                    }
                }

                if (axis == 0)
                {
                    for (int i = 1; i < countcolumn; i++)
                    {
                        for (int j = 1; j < countrow; j++)
                        {
                            if (cs.Rows[0].Columns[i].Caption == axisValue)
                            {
                                if (!cs.Rows[j].Columns[0].Caption.Equals(""))
                                {
                                    if (cs.Rows[j].Columns[0].Caption.Equals("Unknown"))
                                        cs.Rows[j].Columns[0].Caption = "0";
                                    DataPoint dp = new DataPoint { LegendLabel = cs.Rows[j].Columns[0].Caption };
                                    string value = cs.Rows[j].Columns[i].Caption;
                                    value = value.StartsWith("$") ? value.Substring(1).Replace(",", "") : value.Replace(".", ",");

                                    if (value.Equals("1,#INF"))
                                        value = "0";

                                    dp.YValue = double.Parse(value);
                                    dp.XCategory = dp.LegendLabel;
                                    result.Add(dp);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return result;
        }

    }
}