﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Printing;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using SelectionMode = System.Windows.Controls.SelectionMode;

namespace TBridge.Cemi.Cruscotto.Silverlight.Common
{
    public class Common
    {
        private static readonly Regex Cleanrgx = new Regex(@"[^A-Za-z0-9]");

        public static Double Delay = 5;

        #region Configurazione serie

        public static DataSeries GetSeriesFloating(AdoMdHelperCellStone cs, string axisValue, int axis)
        {
            DataSeries result = new DataSeries();
            string[] ax = axisValue.Split(',');
            List<string> lista = ax.ToList();
            int countcolumn = cs.Rows[0].Columns.Count;
            int countrow = cs.Rows.Count;

            if (axis == 1)
            {
                for (int i = 1; i < countrow; i++)
                {
                    for (int j = 1; j < countcolumn; j++)
                    {
                        if (lista.Contains(cs.Rows[i].Columns[0].Caption))
                        {
                            DataPoint dp = new DataPoint {LegendLabel = cs.Rows[0].Columns[j].Caption};
                            string value = cs.Rows[i].Columns[j].Caption;
                            value = value.StartsWith("$")
                                        ? value.Substring(1).Replace(",", "")
                                        : value.Substring(1).Replace(".", ",");
                            if (value.Equals(",#INF"))
                                value = "2";
                            if (value.Equals(""))
                                value = "0";
                            dp.YValue = double.Parse(value);

                            result.Add(dp);
                        }
                    }
                }
            }

            if (axis == 0)
            {
                for (int i = 1; i < countcolumn; i++)
                {
                    for (int j = 1; j < countrow; j++)
                    {
                        if (cs.Rows[0].Columns[i].Caption == axisValue)
                        {
                            DataPoint dp = new DataPoint {LegendLabel = cs.Rows[j].Columns[0].Caption};
                            string value = cs.Rows[j].Columns[i].Caption;
                            if (value.StartsWith("$"))
                                value = value.Substring(1).Replace(",", "");
                            dp.YValue = double.Parse(value);

                            result.Add(dp);
                        }
                    }
                }
            }

            return result;
        }

        public static void SortDataSeries(DataSeries series, DataSeries appoggio)
        {
            foreach (DataPoint point in from point in appoggio
                                        let dp = point
                                        let count = series.Where(s => s.XCategory == dp.LegendLabel).Count()
                                        where count == 0
                                        select point)
            {
                series.Add(new DataPoint(point.LegendLabel, 0) {LegendLabel = point.LegendLabel});
            }

            for (int i = 0; i < series.Count - 1; i++)
            {
                for (int j = i + 1; j < series.Count; j++)
                {
                    if (Convert.ToInt32(series[j].XCategory) < Convert.ToInt32(series[i].XCategory))
                    {
                        DataPoint dp = series[j];
                        series[j] = series[i];
                        series[i] = dp;
                    }
                }
            }
        }

        public static void InitializeRadChart(RadChart chart)
        {
            foreach (UIElement obj in ((Grid) chart.Content).Children)
            {
                Grid grid = obj as Grid;
                if (grid != null)
                    foreach (UIElement o in grid.Children)
                    {
                        ChartArea area = o as ChartArea;
                        if (area != null)
                            area.DataSeries.Clear();
                    }
            }
        }

        public static DataSeries GetSeries(AdoMdHelperCellStone cs, string axisValue, int axis)
        {
            DataSeries result = new DataSeries();

            try
            {
                int countcolumn = cs.Rows[0].Columns.Count;
                int countrow = cs.Rows.Count;

                if (axis == 1)
                {
                    for (int i = 1; i < countrow; i++)
                    {
                        for (int j = 1; j < countcolumn; j++)
                        {
                            if (cs.Rows[i].Columns[0].Caption == axisValue)
                            {
                                if (!cs.Rows[0].Columns[j].Caption.Equals(""))
                                {
                                    if (cs.Rows[0].Columns[j].Caption.Equals("Unknown"))
                                        cs.Rows[0].Columns[j].Caption = "0";
                                    DataPoint dp = new DataPoint {LegendLabel = cs.Rows[0].Columns[j].Caption};
                                    string value = cs.Rows[i].Columns[j].Caption;
                                    value = value.StartsWith("$")
                                                ? value.Substring(1).Replace(",", "")
                                                : value.Replace(".", ",");
                                    if (value.Equals("1,#INF"))
                                        value = "0";
                                    dp.YValue = double.Parse(value);
                                    dp.XCategory = dp.LegendLabel;
                                    result.Add(dp);
                                }
                            }
                        }
                    }
                }

                if (axis == 0)
                {
                    for (int i = 1; i < countcolumn; i++)
                    {
                        for (int j = 1; j < countrow; j++)
                        {
                            if (cs.Rows[0].Columns[i].Caption == axisValue)
                            {
                                if (!cs.Rows[j].Columns[0].Caption.Equals(""))
                                {
                                    if (cs.Rows[j].Columns[0].Caption.Equals("Unknown"))
                                        cs.Rows[j].Columns[0].Caption = "0";
                                    DataPoint dp = new DataPoint {LegendLabel = cs.Rows[j].Columns[0].Caption};
                                    string value = cs.Rows[j].Columns[i].Caption;
                                    value = value.StartsWith("$")
                                                ? value.Substring(1).Replace(",", "")
                                                : value.Replace(".", ",");

                                    if (value.Equals("1,#INF"))
                                        value = "0";

                                    dp.YValue = double.Parse(value);
                                    dp.XCategory = dp.LegendLabel;
                                    result.Add(dp);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return result;
        }

        public static DataSeries GetAvgSeries(AdoMdHelperCellStone cs, string axisValue, int axis)
        {
            DataSeries result = new DataSeries();

            try
            {
                int countcolumn = cs.Rows[0].Columns.Count;
                int countrow = cs.Rows.Count;

                if (axis == 1)
                {
                    for (int i = 1; i < countrow; i++)
                    {
                        for (int j = 1; j < countcolumn; j++)
                        {
                            if (cs.Rows[i].Columns[0].Caption == axisValue)
                            {
                                if (!cs.Rows[0].Columns[j].Caption.Equals(""))
                                {
                                    if (cs.Rows[0].Columns[j].Caption.Equals("Unknown"))
                                        cs.Rows[0].Columns[j].Caption = "0";
                                    DataPoint dp = new DataPoint { LegendLabel = cs.Rows[0].Columns[j].Caption };
                                    string value = cs.Rows[i].Columns[j].Caption;
                                    value = value.StartsWith("$")
                                                ? value.Substring(1).Replace(",", "")
                                                : value.Replace(".", ",");
                                    if (value.Equals("1,#INF"))
                                        value = "0";
                                    int den = 12;
                                    if (dp.LegendLabel.Equals(DateTime.Today.Year.ToString()))
                                        den = DateTime.Today.Month;
                                    dp.YValue = double.Parse(value)/den;
                                    dp.XCategory = dp.LegendLabel;
                                    result.Add(dp);
                                }
                            }
                        }
                    }
                }

                if (axis == 0)
                {
                    for (int i = 1; i < countcolumn; i++)
                    {
                        for (int j = 1; j < countrow; j++)
                        {
                            if (cs.Rows[0].Columns[i].Caption == axisValue)
                            {
                                if (!cs.Rows[j].Columns[0].Caption.Equals(""))
                                {
                                    if (cs.Rows[j].Columns[0].Caption.Equals("Unknown"))
                                        cs.Rows[j].Columns[0].Caption = "0";
                                    DataPoint dp = new DataPoint { LegendLabel = cs.Rows[j].Columns[0].Caption };
                                    string value = cs.Rows[j].Columns[i].Caption;
                                    value = value.StartsWith("$")
                                                ? value.Substring(1).Replace(",", "")
                                                : value.Replace(".", ",");

                                    if (value.Equals("1,#INF"))
                                        value = "0";

                                    int den = 12;
                                    if (dp.LegendLabel.Equals(DateTime.Today.Year.ToString()))
                                        den = DateTime.Today.Month;
                                    dp.YValue = double.Parse(value) / den;
                                    dp.XCategory = dp.LegendLabel;
                                    result.Add(dp);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return result;
        }

        public static DataSeries GetSeriesTotal(AdoMdHelperCellStone cs, string axisValue, int axis, int lastMonth)
        {
            DataSeries result = new DataSeries();

            try
            {
                int countcolumn = cs.Rows[0].Columns.Count;
                int countrow = cs.Rows.Count;
                DataPoint dp = new DataPoint { LegendLabel = axisValue };
                dp.XCategory = dp.LegendLabel;
               
                    for (int i = 1; i < countcolumn; i++)
                    {
                        for (int j = 1; j < countrow; j++)
                        {
                            if (cs.Rows[0].Columns[i].Caption == axisValue)
                            {
                                if (!cs.Rows[j].Columns[0].Caption.Equals(""))
                                {
                                    if (int.Parse(cs.Rows[j].Columns[0].Caption) <= lastMonth)
                                    {
                                        if (cs.Rows[j].Columns[0].Caption.Equals("Unknown"))
                                            cs.Rows[j].Columns[0].Caption = "0";
                                        string value = cs.Rows[j].Columns[i].Caption;
                                        value = value.StartsWith("$")
                                                    ? value.Substring(1).Replace(",", "")
                                                    : value.Replace(".", ",");

                                        if (value.Equals("1,#INF"))
                                            value = "0";

                                        dp.YValue += double.Parse(value);
                                    }
                                }
                            }
                        }
                    }
                result.Add(dp);
            }
            catch
            {
            }
            return result;
        }

        public static void SetSeriesLabel(DataSeries series)
        {
            series.Definition.ShowItemLabels = false;
            double somma = series.Sum(point => point.YValue);
            foreach (DataPoint point in series)
            {
                double percentuale = Math.Round((100*point.YValue)/somma, 0);
                point.LegendLabel = string.Format("{0}: {1}%", point.LegendLabel, percentuale);
            }
        }

        public static void SetScala(ChartArea chartArea, string axisFormat)
        {
            double max = 0;
            
            List<string> assiSecondari = new List<string>();

            foreach (DataSeries series in chartArea.DataSeries.Where((s=>!String.IsNullOrEmpty(s.Definition.AxisName))))
            {
                if (!assiSecondari.Contains(series.Definition.AxisName))
                    assiSecondari.Add(series.Definition.AxisName);
            }

            //Asse primario
            foreach (DataSeries series in chartArea.DataSeries.Where(s=>String.IsNullOrEmpty(s.Definition.AxisName)))
            {
                    double seriesMax = series.Max(d => d.YValue);
                    if (max < seriesMax)
                    {
                        max = seriesMax;
                    }
            }
            max = max + max * 0.2;
            chartArea.AxisY.DefaultLabelFormat = axisFormat;
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.AddRange(0, max, max / 4);

            foreach (string axis in assiSecondari)
            {
                max = 0;
                foreach (
                    DataSeries series in chartArea.DataSeries.Where(s => s.Definition.AxisName.Equals(axis)))
                {
                    double seriesMax = series.Max(d => d.YValue);
                    if (max < seriesMax)
                    {
                        max = seriesMax;
                    }
                }
                max = max + max*0.2;
            
                if (chartArea.AdditionalYAxes.Where(a => a.AxisName.Equals(axis)).FirstOrDefault() != null)
                {
                    chartArea.AdditionalYAxes.Where(a => a.AxisName.Equals(axis)).FirstOrDefault().DefaultLabelFormat = axisFormat;
                    chartArea.AdditionalYAxes.Where(a => a.AxisName.Equals(axis)).FirstOrDefault().AutoRange = false;
                    chartArea.AdditionalYAxes.Where(a => a.AxisName.Equals(axis)).FirstOrDefault().AddRange(0, max,
                                                                                                            max/4);
                }
            }
        }

        public static void SetScalaPercentage(ChartArea chartArea,string axisFormat)
        {
            chartArea.AxisY.DefaultLabelFormat = axisFormat;
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.AddRange(0,100,25);
        }


        public static void SetScalaStackedChart(ChartArea chartArea, string axisFormat)
        {
            double max = chartArea.DataSeries.Sum(series => series.Max(d => d.YValue));
            max = max + max * 0.2;
            
            chartArea.AxisY.DefaultLabelFormat = axisFormat;
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.AddRange(0, max, max / 4);
        }

        public static string ToString(AdoMdHelperCellStone cellStone)
        {
            string result = "";

            AdoMdHelperCellStone temp = cellStone;

            for (int colum = 1; colum < cellStone.Rows[0].Columns.Count; colum++)
            {
                String value = cellStone.Rows[0].Columns[colum].Caption;
                if (!String.IsNullOrEmpty(value))
                {
                    if (value.Equals("Unknown"))
                    {
                        foreach (AdoMdHelperCellSetRow row in temp.Rows)
                        {
                            row.Columns.RemoveAt(colum);
                        }
                    }
                }
            }
            for (int row = 1; row < cellStone.Rows.Count; row++)
            {
                String value = cellStone.Rows[row].Columns[0].Caption;
                if (!String.IsNullOrEmpty(value))
                {
                    if (value.Equals("Unknown"))
                        temp.Rows.RemoveAt(row);
                }
                else
                    temp.Rows.RemoveAt(row);
            }

            cellStone = temp;

            foreach (AdoMdHelperCellSetRow row in cellStone.Rows)
            {
                foreach (AdoMdHelperCell cell in row.Columns)
                {
                    string value = cell.Caption;
                    if (String.IsNullOrEmpty(value))
                        value = "";
                    result = String.Format("{0}{1}\t", result, value);
                }
                result = String.Format("{0}\n", result);
            }

            return result;
        }

        public static DataSeries GetSeries(AdoMdHelperDataSeries series)
        {
            DataSeries result = new DataSeries();

            foreach (AdoMdHelperDataPoint point in series.Definition)
            {
                DataPoint dp = new DataPoint {LegendLabel = point.LegendLabel, YValue = point.Value, XCategory = point.LegendLabel};
                result.Add(dp);
            }

            return result;
        }

        public static DataSeries GetSeriesTotal(AdoMdHelperDataSeries series, string label, out int lastMonth)
        {
            DataSeries result = new DataSeries();
            double total = series.Definition.Sum(point => point.Value);
            AdoMdHelperDataPoint lastPoint = series.Definition.LastOrDefault();
            lastMonth = 12;
            if(lastPoint!=null)
                int.TryParse(lastPoint.LegendLabel,out lastMonth);
            DataPoint dp = new DataPoint { LegendLabel = label, YValue = total, XCategory = label };
            result.Add(dp);

            return result;
        }

        #endregion

        #region Configurazione grafici

        public static void ApplyOldChartStyle(RadChart chart, ResourceDictionary resources)
        {
            chart.Style = resources["CustomChart"] as Style;
            chart.AxisElementBrush = new SolidColorBrush(Colors.White);
            chart.AxisForeground = new SolidColorBrush(Colors.White);

            chart.DefaultView.ChartLegend.Style = resources["CustomLegend"] as Style;
            chart.DefaultView.ChartTitle.Style = resources["CustomTitle"] as Style;
            chart.DefaultView.ChartLegend.LegendItemStyle = resources["CustomLegendItemTemplate"] as Style;
            chart.DefaultView.ChartArea.AxisX.AxisStyles.AlternateStripLineStyle =
                resources["HorizontalStripLineStyle"] as Style;
            chart.DefaultView.ChartArea.AxisY.AxisStyles.AlternateStripLineStyle =
                resources["VerticalStripLineStyle"] as Style;

            //Old Fills 
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 0, 198, 255)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 27, 255, 0)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 255, 141, 0)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 255, 35, 0)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 169, 0, 255)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 255, 0, 212)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 55, 172, 179)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 180, 191, 53)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 54, 92, 161)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 152, 115, 93)));
        }

        public static void ApplyPrintChartStyle(RadChart chart)
        {
            //Old Fills 
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 0, 198, 255)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 27, 255, 0)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 255, 141, 0)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 255, 35, 0)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 169, 0, 255)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 255, 0, 212)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 55, 172, 179)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 180, 191, 53)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 54, 92, 161)));
            chart.PaletteBrushes.Add(new SolidColorBrush(Color.FromArgb(255, 152, 115, 93)));
        }

        public static void ConfigureAllYearsChart(ChartArea chartArea, AdoMdHelperCellStone cellStone, string format,int startYear,bool isPercentage=false)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            if (cellStone.Rows[0].Columns.Count > 1)
            {
                DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                        1);

                splineAreaSeries.Definition = new SplineAreaSeriesDefinition();

                SetSeriesLabel(splineAreaSeries);

                DataSeries temporanea = new DataSeries {Definition = new SplineAreaSeriesDefinition()};
                foreach (DataPoint t in splineAreaSeries)
                    if (
                        !(t.LegendLabel.StartsWith("0") ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) < DateTime.Today.Year - Delay ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year ||
                          (startYear!=DateTime.Today.Year && Int32.Parse(t.LegendLabel.Substring(0, 4))< startYear)))
                    {
                        temporanea.Add(t);
                    }

                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;
                splineAreaSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(127, 0, 198, 255));
                splineAreaSeries.Definition.Appearance.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 163, 210));
                splineAreaSeries.Definition.ItemToolTipFormat = "#XCAT\r\nNumero: #Y{n0}";

                //Aggiungo un punto alla serie per far vedere l'anno corrente
                //DataPoint point = new DataPoint(splineAreaSeries.Count,
                //                                splineAreaSeries[splineAreaSeries.Count - 1].YValue)
                //                      {LegendLabel = " ", XCategory = " "};
                //splineAreaSeries.Add(point);
                chartArea.DataSeries.Add(splineAreaSeries);

                //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                //{
                //    tickPoint.LabelFormat = format;
                //}
            }
            if(isPercentage)
            {
                SetScalaPercentage(chartArea, format);
            }
            else
            {
                SetScala(chartArea,format);   
            }
        }

        public static void ConfigureMonthlyChart(ChartArea chartArea, AdoMdHelperCellStone cellStone, string format,
                                                 string anno, DataSeries monthNum,bool isPercentage=false)
        {
            chartArea.DataSeries.Clear();

            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;

            DataSeries barSeries = GetSeries(cellStone, (int.Parse(anno) - 1).ToString(), 0);
            barSeries.Definition = new BarSeriesDefinition {ShowItemLabels = false, ShowItemToolTips = true};
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Colors.White);
            barSeries.LegendLabel = (int.Parse(anno) - 1).ToString();
            barSeries.Definition.ItemToolTipFormat = format;

            SortDataSeries(barSeries, monthNum);
            string[] months = new[] {"Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"};

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);


            barSeries = GetSeries(cellStone, anno, 0);
            barSeries.Definition = new BarSeriesDefinition {ShowItemLabels = false, ShowItemToolTips = true};
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255, 65, 146, 243));
            barSeries.LegendLabel = anno;
            barSeries.Definition.ItemToolTipFormat = format;

            SortDataSeries(barSeries, monthNum);

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            //SetScala(chartArea);

            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            if(!isPercentage)
            {
                SetScala(chartArea,format);
            }
            else
            {
                SetScalaPercentage(chartArea,format);
            }
        }

        public static void ConfigureDoubleSeriesChart(ChartArea chartArea, AdoMdHelperCellStone cellStone,
                                                      AdoMdHelperCellStone secondCellStone, string format)
        {
            double numeroAnniVisualizzati = Delay;
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            if (cellStone.Rows[0].Columns.Count > 1)
            {
                DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                        1);

                if (numeroAnniVisualizzati > splineAreaSeries.Count)
                    numeroAnniVisualizzati = splineAreaSeries.Count;


                splineAreaSeries.Definition = new BarSeriesDefinition();
                splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;

                SetSeriesLabel(splineAreaSeries);

                DataSeries temporanea = new DataSeries();
                foreach (DataPoint t in splineAreaSeries)
                    if (
                        !(t.LegendLabel.StartsWith("0") ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) < DateTime.Today.Year -
                          (numeroAnniVisualizzati - 1) ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year))
                    {
                        temporanea.Add(t);
                    }
                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;

                for (int i = 0; i < splineAreaSeries.Count; i++)
                    splineAreaSeries[i].XValue = i;

                chartArea.DataSeries.Add(splineAreaSeries);

                splineAreaSeries = GetSeries(secondCellStone, secondCellStone.Rows[1].Columns[0].Caption, 1);
                splineAreaSeries.Definition = new BarSeriesDefinition();

                splineAreaSeries.LegendLabel = secondCellStone.Rows[1].Columns[0].Caption;

                SetSeriesLabel(splineAreaSeries);

                temporanea = new DataSeries();
                foreach (DataPoint t in splineAreaSeries)
                    if (
                        !(t.LegendLabel.StartsWith("0") ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) < DateTime.Today.Year -
                          (numeroAnniVisualizzati - 1) ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year))
                    {
                        temporanea.Add(t);
                    }
                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;


                for (int i = 0; i < splineAreaSeries.Count; i++)
                    splineAreaSeries[i].XValue = i;

                chartArea.DataSeries.Add(splineAreaSeries);
                chartArea.AxisX.TickPoints.Clear();
                for (int i = 0; i < splineAreaSeries.Count; i++)
                    chartArea.AxisX.TickPoints.Add(new TickPoint
                                                       {
                                                           Value = i,
                                                           Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                                                       });

                //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                //{
                //    tickPoint.LabelFormat = format;
                //}
            }
            SetScala(chartArea,format);
        }

        public static void ConfigureDoubleBarChart(ChartArea chartArea, AdoMdHelperCellStone cellStone,
                                                   AdoMdHelperCellStone secondCellStone, string format)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            try
            {
                if (cellStone.Rows[0].Columns.Count > 1)
                {
                    DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                            1);

                    splineAreaSeries.Definition = new BarSeriesDefinition();
                    splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;

                    SetSeriesLabel(splineAreaSeries);

                    DataSeries temporanea = new DataSeries();
                    foreach (DataPoint t in splineAreaSeries)
                        if (!t.LegendLabel.StartsWith("0"))
                        {
                            temporanea.Add(t);
                        }
                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;

                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);

                    splineAreaSeries = GetSeries(secondCellStone, secondCellStone.Rows[1].Columns[0].Caption, 1);
                    splineAreaSeries.Definition = new BarSeriesDefinition();

                    splineAreaSeries.LegendLabel = secondCellStone.Rows[1].Columns[0].Caption;

                    SetSeriesLabel(splineAreaSeries);

                    temporanea = new DataSeries();
                    foreach (DataPoint t in splineAreaSeries)
                        if (!t.LegendLabel.StartsWith("0"))
                        {
                            temporanea.Add(t);
                        }
                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;


                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);
                    chartArea.AxisX.TickPoints.Clear();
                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        chartArea.AxisX.TickPoints.Add(new TickPoint
                                                           {
                                                               Value = i,
                                                               Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                                                           });

                    //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                    //{
                    //    tickPoint.LabelFormat = format;
                    //}
                }
                SetScala(chartArea,format);
            }
            catch
            {
            }
        }

        public static void ConfigureBarChart(ChartArea chartArea, AdoMdHelperCellStone cellStone, string format)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            try
            {
                if (cellStone.Rows[0].Columns.Count > 1)
                {
                    DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                            1);

                    splineAreaSeries.Definition = new BarSeriesDefinition();
                    splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;

                    SetSeriesLabel(splineAreaSeries);

                    DataSeries temporanea = new DataSeries();
                    foreach (DataPoint t in splineAreaSeries)
                        if (!t.LegendLabel.StartsWith("0"))
                        {
                            temporanea.Add(t);
                        }
                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;

                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);

                    chartArea.AxisX.TickPoints.Clear();
                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        chartArea.AxisX.TickPoints.Add(new TickPoint
                        {
                            Value = i,
                            Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                        });

                    //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                    //{
                    //    tickPoint.LabelFormat = format;
                    //}
                }
                SetScala(chartArea,format);
            }
            catch
            {
            }
        }

        public static void ConfigureBarChart(ChartArea chartArea, AdoMdHelperCellStone cellStone, int startYear, string format)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            try
            {
                if (cellStone.Rows[0].Columns.Count > 1)
                {
                    DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                            1);

                    splineAreaSeries.Definition = new BarSeriesDefinition();
                    splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;

                    SetSeriesLabel(splineAreaSeries);

                    DataSeries temporanea = new DataSeries { Definition = new SplineAreaSeriesDefinition() };
                    foreach (DataPoint t in splineAreaSeries)
                        if (
                            !(t.LegendLabel.StartsWith("0") ||
                              Int32.Parse(t.LegendLabel.Substring(0, 4)) < DateTime.Today.Year - Delay ||
                              Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year ||
                              (startYear != DateTime.Today.Year && Int32.Parse(t.LegendLabel.Substring(0, 4)) < startYear)))
                        {
                            temporanea.Add(t);
                        }

                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;

                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);

                    chartArea.AxisX.TickPoints.Clear();
                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        chartArea.AxisX.TickPoints.Add(new TickPoint
                        {
                            Value = i,
                            Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                        });

                    //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                    //{
                    //    tickPoint.LabelFormat = format;
                    //}
                }
                SetScala(chartArea,format);
            }
            catch
            {
            }
        }

        public static void ConfigureDoubleBarMultipleAxisChart(ChartArea chartArea, AdoMdHelperCellStone cellStone,
                                                               AdoMdHelperCellStone secondCellStone, string format)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            chartArea.AdditionalYAxes.Clear();
            chartArea.AdditionalYAxes.Add(new AxisY());
            chartArea.AdditionalYAxes[0].MinValue = 0;
            chartArea.AdditionalYAxes[0].AxisName = "Importi";
            chartArea.AdditionalYAxes[0].DefaultLabelFormat = format;
            try
            {
                if (cellStone.Rows[0].Columns.Count > 1)
                {
                    DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                            1);

                    splineAreaSeries.Definition = new BarSeriesDefinition();
                    splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;

                    SetSeriesLabel(splineAreaSeries);

                    DataSeries temporanea = new DataSeries();
                    foreach (DataPoint t in splineAreaSeries)
                        if (!t.LegendLabel.StartsWith("0"))
                        {
                            temporanea.Add(t);
                        }
                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;

                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);

                    splineAreaSeries = GetSeries(secondCellStone, secondCellStone.Rows[1].Columns[0].Caption, 1);
                    splineAreaSeries.Definition = new BarSeriesDefinition();

                    splineAreaSeries.LegendLabel = secondCellStone.Rows[1].Columns[0].Caption;

                    SetSeriesLabel(splineAreaSeries);

                    temporanea = new DataSeries();
                    foreach (DataPoint t in splineAreaSeries)
                        if (!t.LegendLabel.StartsWith("0"))
                        {
                            temporanea.Add(t);
                        }
                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.AxisName = "Importi";
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;


                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);
                    chartArea.AxisX.TickPoints.Clear();
                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        chartArea.AxisX.TickPoints.Add(new TickPoint
                                                           {
                                                               Value = i,
                                                               Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                                                           });

                    //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                    //{
                    //    tickPoint.LabelFormat = format;
                    //}
                }
                SetScala(chartArea,format);
            }
            catch
            {
            }
        }


        public static void ConfigureMonthDoubleSeriesChart(ChartArea chartArea, AdoMdHelperCellStone cellStone,
                                                           AdoMdHelperCellStone secondCellStone, string format,
                                                           string anno, DataSeries monthNum, String secondSeriesAxisName)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            chartArea.AdditionalYAxes.Clear();
            if (!String.IsNullOrEmpty(secondSeriesAxisName))
            {
                chartArea.AdditionalYAxes.Add(new AxisY());
                chartArea.AdditionalYAxes[0].AutoRange = true;
                chartArea.AdditionalYAxes[0].MaxValue = 0;
                chartArea.AdditionalYAxes[0].MinValue = 0;
                chartArea.AdditionalYAxes[0].AxisName = secondSeriesAxisName;
            }

            DataSeries barSeries = GetSeries(cellStone, anno, 0);
            barSeries.Definition = new BarSeriesDefinition {ShowItemLabels = false, ShowItemToolTips = true};
            barSeries.LegendLabel = anno;

            DataSeries temp = new DataSeries();
            double c;

            foreach (
                DataPoint t in barSeries.Where(t => Double.TryParse(t.LegendLabel, out c) && !t.LegendLabel.Equals("0"))
                )
            {
                temp.Add(t);
            }
            barSeries.Clear();
            foreach (DataPoint t in temp)
                barSeries.Add(t);


            SortDataSeries(barSeries, monthNum);

            string[] months = new[] {"Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"};

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            double maxValue = 0;
            double value = maxValue;
            foreach (DataPoint t in
                barSeries.Where(t => !t.LegendLabel.Equals("Unknown") && !t.LegendLabel.Equals("")).Where(
                    t => t.YValue > value))
            {
                maxValue = t.YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            barSeries = GetSeries(secondCellStone, anno, 0);
            barSeries.Definition = new BarSeriesDefinition {ShowItemLabels = false, ShowItemToolTips = true};
            barSeries.LegendLabel = anno;

            temp.Clear();

            foreach (
                DataPoint t in barSeries.Where(t => Double.TryParse(t.LegendLabel, out c) && !t.LegendLabel.Equals("0"))
                )
            {
                temp.Add(t);
            }
            barSeries.Clear();
            foreach (DataPoint t in temp)
                barSeries.Add(t);


            SortDataSeries(barSeries, monthNum);

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            foreach (DataPoint t in barSeries)
            {
                if (!t.LegendLabel.Equals("Unknown") && !t.LegendLabel.Equals(""))
                    if (t.YValue > maxValue) maxValue = t.YValue;
            }

            if (barSeries.Count != 0)
            {
                barSeries.Definition.AxisName = secondSeriesAxisName;
                chartArea.DataSeries.Add(barSeries);
            }

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            //chartArea.AxisY.Title = "";


            for (int i = 0; i < barSeries.Count; i++)
            {
                chartArea.AxisX.TickPoints[i].Value = barSeries[i].XValue;
            }

            //SetScala(chartArea);

            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            SetScala(chartArea,format);
        }

        public static void ConfigureDoubleSplineSeriesChart(ChartArea chartArea, AdoMdHelperCellStone cellStone,
                                                            AdoMdHelperCellStone secondCellStone, string format)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            if (cellStone.Rows[0].Columns.Count > 1)
            {
                DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                        1);

                splineAreaSeries.Definition = new SplineSeriesDefinition();

                SetSeriesLabel(splineAreaSeries);

                DataSeries temporanea = new DataSeries {Definition = new SplineSeriesDefinition()};
                foreach (DataPoint t in splineAreaSeries)
                    if (
                        !(t.LegendLabel.StartsWith("0") ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) <
                          DateTime.Today.Year - Delay ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year))
                    {
                        temporanea.Add(t);
                    }

                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;

                //Aggiungo un punto alla serie per far vedere l'anno corrente
                DataPoint point = new DataPoint(0, splineAreaSeries[splineAreaSeries.Count - 1].YValue)
                                      {LegendLabel = " ", XCategory = " "};
                splineAreaSeries.Add(point);
                chartArea.DataSeries.Add(splineAreaSeries);

                splineAreaSeries = GetSeries(secondCellStone, secondCellStone.Rows[1].Columns[0].Caption,
                                             1);

                splineAreaSeries.Definition = new SplineSeriesDefinition();

                SetSeriesLabel(splineAreaSeries);

                temporanea = new DataSeries {Definition = new SplineSeriesDefinition()};
                foreach (DataPoint t in splineAreaSeries)
                    if (
                        !(t.LegendLabel.StartsWith("0") ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) <
                          DateTime.Today.Year - Delay ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year))
                    {
                        temporanea.Add(t);
                    }

                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;

                //Aggiungo un punto alla serie per far vedere l'anno corrente
                point = new DataPoint(0, splineAreaSeries[splineAreaSeries.Count - 1].YValue)
                            {LegendLabel = " ", XCategory = " "};
                splineAreaSeries.Add(point);
                chartArea.DataSeries.Add(splineAreaSeries);


                for (int i = 0; i < chartArea.AxisX.TickPoints.Count; i++)
                {
                    chartArea.AxisX.TickPoints[i].Label = splineAreaSeries[i].LegendLabel.Split(':')[0];
                }

                //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                //{
                //    tickPoint.LabelFormat = "#VAL{N0}";
                //}
            }
            SetScala(chartArea,format);
        }

        public static void ConfigureMultipleAxisChart(ChartArea chartArea, AdoMdHelperCellStone cellStone,
                                                      AdoMdHelperCellStone secondCellStone, string format)
        {
            double numeroAnniVisualizzati = Delay;
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            
            chartArea.AxisX.TickPoints.Clear();
            chartArea.AdditionalYAxes.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AdditionalYAxes.Add(new AxisY());
            chartArea.AdditionalYAxes[0].AxisName = "Importi";
            chartArea.AdditionalYAxes[0].DefaultLabelFormat = format;
            chartArea.AdditionalYAxes[0].MinValue = 0;
            if (cellStone.Rows[0].Columns.Count > 1)
            {
                DataSeries splineAreaSeries = GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                        1);
                if (numeroAnniVisualizzati > splineAreaSeries.Count)
                    numeroAnniVisualizzati = splineAreaSeries.Count;

                splineAreaSeries.Definition = new BarSeriesDefinition();
                splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;


                SetSeriesLabel(splineAreaSeries);

                DataSeries temporanea = new DataSeries();
                int anno = 0;
                foreach (DataPoint t in
                    splineAreaSeries.Where(
                        t => !t.LegendLabel.StartsWith("0") && Int32.TryParse(t.LegendLabel.Substring(0, 4), out anno)).
                        Where(
                            t =>
                            !(anno < DateTime.Today.Year - (numeroAnniVisualizzati - 1) || anno > DateTime.Today.Year)))
                {
                    temporanea.Add(t);
                }
                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;

                for (int i = 0; i < splineAreaSeries.Count; i++)
                    splineAreaSeries[i].XValue = i;

                chartArea.DataSeries.Add(splineAreaSeries);

                splineAreaSeries = GetSeries(secondCellStone, secondCellStone.Rows[1].Columns[0].Caption, 1);
                splineAreaSeries.Definition = new BarSeriesDefinition();

                splineAreaSeries.LegendLabel = secondCellStone.Rows[1].Columns[0].Caption;

                SetSeriesLabel(splineAreaSeries);

                temporanea = new DataSeries();
                foreach (DataPoint t in
                    splineAreaSeries.Where(
                        t => !t.LegendLabel.StartsWith("0") && Int32.TryParse(t.LegendLabel.Substring(0, 4), out anno)).
                        Where(
                            t =>
                            !(anno < DateTime.Today.Year - (numeroAnniVisualizzati - 1) || anno > DateTime.Today.Year)))
                {
                    temporanea.Add(t);
                }

                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.AxisName = "Importi";
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;


                for (int i = 0; i < splineAreaSeries.Count; i++)
                    splineAreaSeries[i].XValue = i;

                chartArea.DataSeries.Add(splineAreaSeries);
                chartArea.AxisX.TickPoints.Clear();
                for (int i = 0; i < splineAreaSeries.Count; i++)
                    chartArea.AxisX.TickPoints.Add(new TickPoint
                                                       {
                                                           Value = i,
                                                           Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                                                       });

                //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                //{
                //    tickPoint.LabelFormat = format;
                //}
            }
            SetScala(chartArea,format);
        }

        #endregion

        #region Export e Tabelle 

        #region Telerik

        public static void CreaGridView(Grid codeViewer, CellSet[] cellSets, String filtro)
        {
            int childrenCount = codeViewer.Children.Count;
            for (int j = childrenCount; j > 0; j -= 2)
                if (codeViewer.Children[j - 1] is RadGridView)
                {
                    codeViewer.Children.RemoveAt(j - 1);
                    codeViewer.Children.RemoveAt(j - 2);
                }
                else break;

            //hideExampleArea = e;
            int row = 1, column = 0;

            for (int i = 0; i < cellSets.Length; i++)
            {
                if(i>0)
                {
                    filtro = null;
                }
                if (cellSets[i].Content != null)
                {
                    BindGridView(codeViewer, cellSets[i].Content, cellSets[i].Title, filtro, row, column);
                    if ((i + 1)%2 == 1)
                        column += 1;
                    else
                    {
                        row += 2;
                        column = 0;
                    }
                }
            }
        }

        private static void BindGridView(Grid codeViewer, String content, String title, String filtro, int row, int column)
        {
            RadGridView myGrid = new RadGridView
                                     {
                                         AutoGenerateColumns = false,
                                         IsFilteringAllowed = false,
                                         ShowGroupPanel = false,
                                         CanUserSortColumns = false,
                                         CanUserReorderColumns = false,
                                         FrozenColumnCount = 1,
                                         SelectionMode = SelectionMode.Extended
                                     };

            myGrid.ElementExporting+= RadGridView_ElementExporting;
            myGrid.ElementExported += RadGridView_ElementExported;
            
            List<string> lines = content.Split(new[] {'\n'}).ToList();

            List<string> headers = lines[0].Split(new[] {'\t'}).ToList();
            headers.RemoveAt(headers.Count - 1);

            for (int i = 0; i < headers.Count; i++)
                headers[i] = CleanPropertyString(headers[i]);

            foreach (string header in headers)
            {
                int annoHeader;
                Int32.TryParse(header.Substring(6), out annoHeader);
                if (annoHeader == 0 || (annoHeader <= DateTime.Today.Year && annoHeader > DateTime.Today.Year - 10))
                {
                    AddColumn(myGrid, header);
                }
            }

            lines.RemoveAt(0);
            myGrid.ItemsSource = GenerateData(headers, lines).ToDataSource();
  
            TextBlock titolo = new TextBlock {Text = title};
            titolo.SetValue(Grid.ColumnProperty, column);
            titolo.SetValue(Grid.RowProperty, row - 1);
            titolo.TextAlignment = TextAlignment.Center;
            titolo.FontSize = 16;
            titolo.FontWeight = FontWeights.Bold;
            titolo.Foreground = new SolidColorBrush(Colors.White);
            
            myGrid.Name = !String.IsNullOrEmpty(filtro) ? String.Format("{0}, filtro: {1}",title,filtro) : title;


            myGrid.SetValue(Grid.ColumnProperty, column);
            myGrid.SetValue(Grid.RowProperty, row);
            myGrid.SetValue(Grid.ColumnSpanProperty, 1);
            codeViewer.Children.Add(titolo);
            try
            {
                codeViewer.Children.Add(myGrid);
            }
            catch{}
        }

        private static void AddColumn(RadGridView grid, string header)
        {
            GridViewDataColumn col = new GridViewDataColumn
                                         {
                                             IsReadOnly = true,
                                             Header = header.Substring(6),
                                             DataMemberBinding = new Binding(header)
                                         };
            grid.Columns.Add(col);
        }

        public static void ToExcel(Grid codeViewer)
        {
            const string extension = "xls";

            List<RadGridView> listaGridView = new List<RadGridView>();

            int childrenCount = codeViewer.Children.Count;
            for (int j = 0; j < childrenCount; j++)
            {
                if (codeViewer.Children[j] is RadGridView)
                {
                    listaGridView.Add((RadGridView) codeViewer.Children[j]);
                }
            }


            SaveFileDialog dialog = new SaveFileDialog
                                        {
                                            DefaultExt = extension,
                                            Filter =
                                                String.Format("Excel files (*.{0})|*.{0}|All files (*.*)|*.*", extension),
                                            FilterIndex = 1
                                        };

            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    foreach (RadGridView grid in listaGridView)
                    {

                        var options = new GridViewExportOptions
                                          {
                                              Format = ExportFormat.Html,
                                              ShowColumnHeaders = true,
                                              ShowColumnFooters = true,
                                              ShowGroupFooters = false,
                                          };

                        grid.Export(stream, options);
                    }

                    stream.Close();
                }
            }
        }

        private static void RadGridView_ElementExporting(object sender, GridViewElementExportingEventArgs e)
        {
            //Change the font size and the background color of the table header only
            if (e.Element == ExportElement.HeaderRow ||
                e.Element == ExportElement.HeaderCell)
            {
                e.FontSize = e.FontSize + 4;
                e.Background = Colors.DarkGray;
            }
            //Change the font size and the background color of the group headers only
            else if (e.Element == ExportElement.GroupHeaderCell ||
                e.Element == ExportElement.GroupHeaderRow)
            {
                e.FontSize = e.FontSize + 2;
                e.Background = Colors.LightGray;
            }
        }

        private static void RadGridView_ElementExported(object sender, GridViewElementExportedEventArgs e)
        {
            if (e.Element == ExportElement.Table)
            {
                e.Writer.Write(String.Format(@"<tr><td style=""background-color:#CCC;"" colspan=""{0}"">",10));
                e.Writer.Write(((RadGridView)sender).Name);
                e.Writer.Write("<br /><br /><br /></td></tr>");
            }
        }

        #endregion

        public static void ExpotToExcel(CellSet[] cellSets, String filtro)
        {
            var s = Application.GetResourceStream(new Uri("excelTemplate.txt", UriKind.Relative));

            var dialog = new SaveFileDialog
                             {
                                 DefaultExt = "*.xml",
                                 Filter = "Excel Xml (*.xml)|*.xml|All files (*.*)|*.*"
                             };


            if (dialog.ShowDialog() == false) return;


            using (var sw = new StreamWriter(dialog.OpenFile()))
            {
                var sr = new StreamReader(s.Stream);

                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();

                    if (line == "***") break;

                    sw.WriteLine(line);
                }
                if (!String.IsNullOrEmpty(filtro))
                {
                    sw.WriteLine("<Row ss:AutoFitHeight=\"0\">");
                    sw.WriteLine(
                        "<Cell ss:StyleID=\"s63\"><Data ss:Type=\"String\">Dati filtrati per: {0}</Data></Cell>",
                        filtro);
                    sw.WriteLine("</Row>");
                }

                foreach (CellSet cellSet in cellSets)
                {
                    sw.WriteLine("<Row ss:AutoFitHeight=\"0\">");
                    sw.WriteLine("<Cell ss:StyleID=\"s63\"><Data ss:Type=\"String\">{0}</Data></Cell>", cellSet.Title);
                    sw.WriteLine("</Row>");
                    if (cellSet.Content != null)
                    {
                        List<string> lines = cellSet.Content.Split(new[] {'\n'}).ToList();
                        for (int x = 0; x < lines.Count; x++)
                        {
                            sw.WriteLine("<Row>");
                            List<string> headers = lines[x].Split(new[] {'\t'}).ToList();
                            headers.RemoveAt(headers.Count - 1);
                            for (int i = 0; i < headers.Count; i++)
                                if (x == 0 || i == 0)
                                    sw.WriteLine("<Cell><Data ss:Type=\"String\">{0}</Data></Cell>", headers[i]);
                                else if (!headers[i].Equals("1.#INF"))
                                {
                                    sw.WriteLine(
                                        headers[i].Contains(".")
                                            ? "<Cell ss:StyleID=\"s81\"><Data ss:Type=\"Number\">{0}</Data></Cell>"
                                            : "<Cell ss:StyleID=\"s80\"><Data ss:Type=\"Number\">{0}</Data></Cell>",
                                        headers[i]);
                                }
                                else
                                    sw.WriteLine("<Cell><Data ss:Type=\"String\">-</Data></Cell>");

                            sw.WriteLine("</Row>");
                        }
                    }
                }

                while (!sr.EndOfStream)
                {
                    sw.WriteLine(sr.ReadLine());
                }
            }
        }

        private static string CleanPropertyString(string oriproperty)
        {
            var ret = Cleanrgx.Replace(oriproperty, "_");
            //if (String.IsNullOrEmpty(ret) || (ret.Length > 0 && ret[0] >= '0' && ret[0] <= '9'))
            ret = String.Format("header{0}", ret);

            return ret;
        }

        private static void BindDg(Grid codeViewer, String content, String title, int row, int column)
        {
            DataGrid myGrid = new DataGrid {AutoGenerateColumns = false};

            List<string> lines = content.Split(new[] {'\n'}).ToList();

            List<string> headers = lines[0].Split(new[] {'\t'}).ToList();
            headers.RemoveAt(headers.Count - 1);

            for (int i = 0; i < headers.Count; i++)
                headers[i] = CleanPropertyString(headers[i]);

            foreach (string header in headers)
            {
                int annoHeader;
                Int32.TryParse(header.Substring(6), out annoHeader);
                if (annoHeader == 0 || (annoHeader <= DateTime.Today.Year && annoHeader > DateTime.Today.Year - 10))
                {
                    AddColumn(myGrid, header);
                }
            }

            lines.RemoveAt(0);
            myGrid.ItemsSource = GenerateData(headers, lines).ToDataSource();

            TextBlock titolo = new TextBlock {Text = title};
            titolo.SetValue(Grid.ColumnProperty, column);
            titolo.SetValue(Grid.RowProperty, row - 1);
            titolo.TextAlignment = TextAlignment.Center;
            titolo.FontSize = 16;
            titolo.FontWeight = FontWeights.Bold;
            titolo.Foreground = new SolidColorBrush(Colors.White);


            myGrid.SetValue(Grid.ColumnProperty, column);
            myGrid.SetValue(Grid.RowProperty, row);
            myGrid.SetValue(Grid.ColumnSpanProperty, 1);
            codeViewer.Children.Add(titolo);
            codeViewer.Children.Add(myGrid);
        }

        private static void AddColumn(DataGrid myGrid, string header)
        {
            DataGridTextColumn col = new DataGridTextColumn
                                         {
                                             IsReadOnly = true,
                                             Header = header.Substring(6),
                                             Binding = new Binding(header),
                                         };
            myGrid.Columns.Add(col);
        }

        private static IEnumerable<IDictionary> GenerateData(List<string> headers, IEnumerable<string> lines)
        {
            foreach (string line in lines)
            {
                if (!String.IsNullOrEmpty(line))
                {
                    List<string> values = line.Split(new[] {'\t'}).ToList();
                    var dict = new Dictionary<string, string>();

                    for (int j = 0; j < headers.Count; j++)
                    {
                        int annoHeader;
                        Int32.TryParse(headers[j].Substring(6), out annoHeader);
                        if (annoHeader == 0 ||
                            (annoHeader <= DateTime.Today.Year && annoHeader > DateTime.Today.Year - 10))
                        {
                            if (values[j].StartsWith("."))
                            {
                                values[j] = values[j].Replace(".", "0.");
                            }
                            if(values[j].StartsWith(","))
                            {
                                values[j] = values[j].Replace(",", "0,");
                            }
                            if(!dict.ContainsKey(headers[j]))
                            {
                                dict.Add(headers[j], !values[j].Equals("1.#INF") ? values[j] : "-");
                            }
                            else
                            {
                                dict.Add(string.Format("{0}{1}",headers[j],j), !values[j].Equals("1.#INF") ? values[j] : "-");
                            }
                        }
                    }

                    yield return dict;
                }
            }
        }

        public static void CreaTabella(Grid codeViewer, CellSet[] cellSets)
        {
            int childrenCount = codeViewer.Children.Count;
            for (int j = childrenCount; j > 0; j -= 2)
                if (codeViewer.Children[j - 1] is DataGrid)
                {
                    codeViewer.Children.RemoveAt(j - 1);
                    codeViewer.Children.RemoveAt(j - 2);
                }
                else break;

            //hideExampleArea = e;
            int row = 1, column = 0;

            for (int i = 0; i < cellSets.Length; i++)
            {
                if (cellSets[i].Content != null)
                {
                    BindDg(codeViewer, cellSets[i].Content, cellSets[i].Title, row, column);
                    if ((i + 1)%2 == 1)
                        column += 1;
                    else
                    {
                        row += 2;
                        column = 0;
                    }
                }
            }
        }

        #endregion

        #region Lookup codici prestazioni

        public static void InizializzaDictionary()
        {
            DictionaryCodici = new Dictionary<string, IList<String>>();

            #region Prestazioni

            DictionaryCodici["AndamentoGenerale"] = new List<String>
                                                        {
                                                            "[B006]",
                                                            "[C004]",
                                                            "[C004-1]",
                                                            "[B004-1]",
                                                            "[B010-F]",
                                                            "[C010-F]",
                                                            "[B010]",
                                                            "[C010]",
                                                            "[B009-1]",
                                                            "[C009-1]",
                                                            "[B009]",
                                                            "[C009]",
                                                            "[B004]",
                                                            "[B006-N]",
                                                            "[C006]",
                                                            "[B008]",
                                                            "[C008]",
                                                            "[B005-1]",
                                                            "[C005-1]",
                                                            "[B005]",
                                                            "[C005]",
                                                            "[B007-3]",
                                                            "[C007-3]",
                                                            "[B002]",
                                                            "[C002]",
                                                            "[B007-1]",
                                                            "[B007-2]",
                                                            "[C007-2]",
                                                            "[C007-1]",
                                                            "[B014-F]",
                                                            "[C014-F]",
                                                            "[ASNIDO]",
                                                            "[B014-C]",
                                                            "[C014-C]",
                                                            "[B014]",
                                                            "[C014]",
                                                            "[B003-2]",
                                                            "[C003-2]",
                                                            "[B003-1]",
                                                            "[C003-1]",
                                                            "[B003-3]",
                                                            "[C003-3]",
                                                            "[C-ANID]",
                                                            "[C002-L]"
                                                        };
            DictionaryCodici["AccessoScuolaSecondaria"] = new List<string> {"[B006]", "[B006-N]", "[C006]"};

            DictionaryCodici["AssegnoFunerario"] = new List<string> {"[B002]", "[C002]", "[C002-L]"};

            DictionaryCodici["BorsaDiStudioUniversitaria"] = new List<string> {"[B003-2]", "[C003-2]"};
            DictionaryCodici["BorsaDiStudioIngegneriaCivile"] = new List<string> {"[B003-3]", "[C003-3]"};
            DictionaryCodici["BorsaDiStudioScuoleSecondarie"] = new List<string> {"[B003-1]", "[C003-1]"};

            DictionaryCodici["AsiloNido"] = new List<string> {"[ASNIDO]", "[C-ANID]"};

            DictionaryCodici["ContributoPortatoriHandicapLavoratore"] = new List<string> {"[B014]", "[C014]"};
            DictionaryCodici["ContributoPortatoriHandicapConiuge"] = new List<string> {"[B014-C]", "[C014-C]"};
            DictionaryCodici["ContributoPortatoriHandicapFamiliare"] = new List<string> {"[B014-F]", "[C014-F]"};

            DictionaryCodici["LentiOculisticheLavoratore"] = new List<string> {"[B007-1]", "[C007-1]"};
            DictionaryCodici["LentiOculisticheConiuge"] = new List<string> {"[B007-3]", "[C007-3]"};
            DictionaryCodici["LentiOculisticheFamiliare"] = new List<string> {"[B007-2]", "[C007-2]"};

            DictionaryCodici["ProtesiAcusticheLavoratori"] = new List<string> {"[B005]", "[C005]"};
            DictionaryCodici["ProtesiAcusticheFamiliari"] = new List<string> {"[B005-1]", "[C005-1]"};

            DictionaryCodici["ProtesiDentarieLavoratori"] = new List<string> {"[C004]", "[B004]"};
            DictionaryCodici["ProtesiDentarieFamiliari"] = new List<string> {"[C004-1]", "[B004-1]"};
            DictionaryCodici["ProtesiOrtodontichePerFiglio"] = new List<string> {"[B008]", "[C008]"};

            DictionaryCodici["ProtesiOrtopedicheLavoratori"] = new List<string> {"[B009]", "[C009]"};
            DictionaryCodici["ProtesiOrtopedicheFamiliari"] = new List<string> {"[B009-1]", "[C009-1]"};

            DictionaryCodici["VisiteMedicheSpecialisticheLavoratori"] = new List<string> {"[B010]", "[C010]"};
            DictionaryCodici["VisiteMedicheSpecialisticheFamiliari"] = new List<string> {"[B010-F]", "[C010-F]"};

            DictionaryCodici["Cartella"] = new List<string> {"[A003]"};

            DictionaryCodici["AnzianitàProfessionaleEdile"] = new List<string> {"[A001]"};

            DictionaryCodici["Colonie"] = new List<string> {"[VILLAG]"};

            DictionaryCodici["16Ore"] = new List<string> {"[16ORE]", "[16ORES]"};

            DictionaryCodici["PremioFedeltà"] = new List<string> {"[PREFED]", "[B022]"};

            #endregion

            #region Recupero Crediti

            DictionaryCodici["FasePrelegale"] = new List<String> {"[PRELEG]"};
            DictionaryCodici["Insolvenze"] = new List<String>
                                                 {
                                                     "[DI]",
                                                     "[PRE]",
                                                     "[PIG]",
                                                     "[IF]",
                                                     "[DIC]",
                                                     "[DIL]",
                                                     "[APA]",
                                                     "[AM]",
                                                     "[APL]",
                                                     "[CP]",
                                                     "[LCA]",
                                                     "[PR1]"
                                                 };
            DictionaryCodici["Fallimenti"] = new List<String> {"[FAL]"};

            #endregion
        }

        #endregion

        #region Print

        public static void Print(UIElement source, String documentName)
        {
            var doc = new PrintDocument();

            var offsetY = 0d;
            var totalHeight = 0d;

            doc.PrintPage += (s, e) =>
                                 {
                                     var canvas = new Canvas();
                                     canvas.Children.Add(source);
                                     e.PageVisual = canvas;

                                     if (totalHeight == 0)
                                     {
                                         totalHeight = source.DesiredSize.Height;
                                     }

                                     Canvas.SetTop(source, -offsetY);

                                     offsetY += e.PrintableArea.Height;
                                     e.HasMorePages = offsetY <= totalHeight;
                                 };
            doc.Print(documentName);
        }

        public static RadChart Clone(RadChart baseChart)
        {
            RadChart cloneChart = new RadChart();

            IList<ChartArea> charts = baseChart.ChildrenOfType<ChartArea>();
            IList<ChartTitle> titles = baseChart.ChildrenOfType<ChartTitle>();

            #region Griglia

            Grid baseContainer =
                baseChart.ChildrenOfType<Grid>().Where(g => g.Parent != null).Where(
                    gs => gs.Parent.GetType().Name == "RadChart").Single();
            baseContainer = baseContainer.ChildrenOfType<Grid>().First();

            Grid grid = new Grid();

            double fillFactorX = baseContainer.ColumnDefinitions.Where(c => c.Width.IsStar).Sum(s => s.ActualWidth)/
                                 (800 -
                                  baseContainer.ColumnDefinitions.Where(c => c.Width.IsAuto || c.Width.IsAbsolute).Sum(
                                      s => s.ActualWidth));
            double fillFactorY = baseContainer.RowDefinitions.Where(r => r.Height.IsStar).Sum(s => s.ActualHeight)/
                                 (400 -
                                  baseContainer.RowDefinitions.Where(r => r.Height.IsAuto || r.Height.IsAbsolute).Sum(
                                      s => s.ActualHeight));

            foreach (RowDefinition rowDefinition in baseContainer.RowDefinitions)
            {
                grid.RowDefinitions.Add(rowDefinition.Height.IsStar
                                            ? new RowDefinition
                                                  {Height = new GridLength(rowDefinition.ActualHeight/fillFactorY)}
                                            : new RowDefinition {Height = new GridLength(rowDefinition.ActualHeight)});
            }

            foreach (ColumnDefinition columnDefinition in baseContainer.ColumnDefinitions)
            {
                grid.ColumnDefinitions.Add(columnDefinition.Width.IsStar
                                               ? new ColumnDefinition
                                                     {Width = new GridLength(columnDefinition.ActualWidth/fillFactorX)}
                                               : new ColumnDefinition
                                                     {Width = new GridLength(columnDefinition.ActualWidth)});
            }

            cloneChart.UseDefaultLayout = false;
            cloneChart.Content = grid;

            #endregion

            #region ChartArea

            foreach (ChartArea chartArea in charts)
            {
                ChartArea chart = new ChartArea
                                      {
                                          EnableAnimations = false,
                                          EnableStripLinesAnimation = false,
                                          LegendName = String.Format("{0}print", chartArea.LegendName),
                                          AxisX =
                                              {
                                                  LayoutMode = chartArea.AxisX.LayoutMode,
                                                  StripLinesVisibility = chartArea.AxisX.StripLinesVisibility
                                              },
                                          AxisY =
                                              {
                                                  ExtendDirection = chartArea.AxisY.ExtendDirection,
                                                  Title = chartArea.AxisY.Title
                                              }
                                      };

                chart.AxisY.AxisStyles.TitleStyle = chartArea.AxisY.AxisStyles.TitleStyle;

                for (int i = 0; i < chartArea.DataSeries.Count; i++)
                {
                    DataSeries dataSeries = chartArea.DataSeries[i];

                    chart.DataSeries.Add(dataSeries);
                }

                int row = Grid.GetRow(chartArea);
                int col = Grid.GetColumn(chartArea);
                int colSpan = Grid.GetColumnSpan(chartArea);
                int rowSpan = Grid.GetRowSpan(chartArea);


                Grid.SetRow(chart, row);
                Grid.SetColumn(chart, col);
                Grid.SetColumnSpan(chart, colSpan);
                Grid.SetRowSpan(chart, rowSpan);
                grid.Children.Add(chart);

                #region Legende

                String legendName = String.Format("{0}print", chartArea.LegendName);
                if (!String.IsNullOrEmpty(chartArea.LegendName))
                    if (grid.ChildrenOfType<ChartLegend>().Count(c => c.Name == legendName) == 0)
                    {
                        ChartLegend legend = new ChartLegend
                                                 {
                                                     UseAutoGeneratedItems = true,
                                                     Header = chartArea.Legend.Header,
                                                     Name = String.Format("{0}print", chartArea.LegendName)
                                                 };

                        row = Grid.GetRow(chartArea.Legend);
                        col = Grid.GetColumn(chartArea.Legend);
                        colSpan = Grid.GetColumnSpan(chartArea.Legend);
                        rowSpan = Grid.GetRowSpan(chartArea.Legend);

                        Grid.SetRow(legend, row);
                        Grid.SetColumn(legend, col);
                        Grid.SetColumnSpan(legend, colSpan);
                        Grid.SetRowSpan(legend, rowSpan);

                        grid.Children.Add(legend);
                    }

                #endregion

                if (chartArea.AxisY.TickPoints.Count > 0)
                {
                    String format = chartArea.AxisY.TickPoints[0].LabelFormat;
                    foreach (TickPoint tickPoint in chart.AxisY.TickPoints)
                    {
                        tickPoint.LabelFormat = format;
                    }
                }
            }

            #endregion

            #region Titoli

            ////Chart Title
            foreach (ChartTitle chartTitle in titles)
            {
                ChartTitle title = new ChartTitle
                                       {
                                           Content = chartTitle.Content,
                                           HorizontalAlignment = chartTitle.HorizontalAlignment,
                                           OuterBorderBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)),
                                           BorderBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)),
                                           Background = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0)),
                                           Foreground = new SolidColorBrush(Color.FromArgb(255, 42, 42, 42)),
                                       };

                int row = Grid.GetRow(chartTitle);
                int col = Grid.GetColumn(chartTitle);
                int colSpan = Grid.GetColumnSpan(chartTitle);
                int rowSpan = Grid.GetRowSpan(chartTitle);


                Grid.SetRow(title, row);
                Grid.SetColumn(title, col);
                Grid.SetColumnSpan(title, colSpan);
                Grid.SetRowSpan(title, rowSpan);
                if (chartTitle.Parent.GetType().Name.Equals("Grid"))
                {
                    grid.Children.Add(title);
                }
            }

            #endregion

            ApplyPrintChartStyle(cloneChart);

            return cloneChart;
        }

        public static RadChart GetTestChart()
        {
            RadChart chart = new RadChart();
            chart.DefaultSeriesDefinition = new BarSeriesDefinition();

            //Chart Title
            chart.DefaultView.ChartTitle.Content = "Year 2009";
            chart.DefaultView.ChartTitle.HorizontalAlignment = HorizontalAlignment.Center;

            //Chart Legend
            chart.DefaultView.ChartLegend.UseAutoGeneratedItems = true;


            Random rand = new Random(DateTime.Now.Millisecond);

            DataSeries series = new DataSeries();

            for (int i = 2000; i < 2010; i++)
            {
                DataPoint dataPoint = new DataPoint {YValue = rand.Next(29, 91), XCategory = i.ToString()};
                series.Add(dataPoint);
            }
            series.Definition = new BarSeriesDefinition();
            chart.DefaultView.ChartArea.EnableAnimations = false;
            chart.DefaultView.ChartArea.DataSeries.Add(series);
            ApplyPrintChartStyle(chart);
            return chart;
        }

        #endregion

        public static String CompletePath { get; set; }

        public static Dictionary<string, IList<String>> DictionaryCodici { get; set; }

    }
}