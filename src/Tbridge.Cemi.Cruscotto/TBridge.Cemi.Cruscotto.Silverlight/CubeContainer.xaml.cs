﻿using TBridge.Cemi.Cruscotto.Template;

namespace TBridge.Cemi.Cruscotto.Silverlight
{
    public partial class CubeContainer : MainPage
    {
        public CubeContainer(bool asynckDownloadXAPs, string endpoint)
        {
            InitializeComponent();

            //InitializeComponent();
            base.SetAuthenticationServiceEndpoint(endpoint);
            base.AsyncDownloadExamples = asynckDownloadXAPs;
        }
    }
}