﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs=System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.Anomalie
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        private ComboItem allItem;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool Initialize;

        public bool InizializzazioneInCorso;
        public ICubeService m_Client;

        public string maxYear;
        public BackgroundWorker worker = new BackgroundWorker();
        private bool LoadImpreseIscritte;
        public AdoMdHelperCellStone cellStone { get; set; }
        public AdoMdHelperCellStone secondCellStone { get; set; }

        public string Members { set; get; }

        public string MembersMonth { set; get; }

        public string Anno { set; get; }

        public string StatoImpresa { get; set; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            //string completePath = App.Current.Host.Source.Scheme
            //     + "://" + App.Current.Host.Source.Host
            //     + ":" + App.Current.Host.Source.Port + "/CubeService.svc";

            string completePath = Application.Current.Host.Source.Scheme
                                  + "://" + Application.Current.Host.Source.Host
                                  + ":" + Application.Current.Host.Source.Port + "/TBridge.Cemi.Cruscotto.Silverlight.Web/CubeService.svc";

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            //ChannelFactory<ICubeService> factory =
            //    new ChannelFactory<ICubeService>("BasicHttpBinding_ICubeService");
            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);


            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = maxYear = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = maxYear = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }


            Members =
                "member [Measures].[TotalUsed] as sum({[Data].[TimeHierarchy].&[1900]:[Data].[TimeHierarchy].currentmember}, [Measures].[Imprese Totali])";
            MembersMonth =
                string.Format("member [Measures].[TotalUsed] as CASE WHEN ([Data].[TimeHierarchy].currentmember.parent.membervalue <= {0} OR [Data].[TimeHierarchy].currentmember.membervalue <= {1}) THEN sum({2}[Data].[TimeHierarchy].&[1900].&[1]:[Data].[TimeHierarchy].currentmember{3}, [Measures].[Imprese Totali])ELSE 0 END ",DateTime.Today.Year-1,DateTime.Today.Month,"{","}");

            ((ComboItem) dimCombo.Items[0]).Caption = "[Imprese Iscritte].[Stato].&[ATTIVA]";
            ((ComboItem) dimCombo.Items[1]).Caption = "[Imprese Iscritte].[Stato].&[SOSPESA]";
            ((ComboItem) dimCombo.Items[2]).Caption = "[Imprese Iscritte].[Stato].&[CESSATA]";

            ((ComboItem) dimCombo.Items[3]).Name = "TotalUsed";
            ((ComboItem) dimCombo.Items[0]).Name = "ImpreseAttive";
            ((ComboItem) dimCombo.Items[1]).Name = "ImpreseSospese";
            ((ComboItem) dimCombo.Items[2]).Name = "ImpreseCessate";

            ((ComboItem)dimComboTipoImpresa.Items[0]).Caption = "[Imprese].[Tipo Impresa].[All]";
            ((ComboItem)dimComboTipoImpresa.Items[1]).Caption = "[Imprese].[Tipo Impresa].&[Artigiana]";
            ((ComboItem)dimComboTipoImpresa.Items[2]).Caption = "[Imprese].[Tipo Impresa].&[Industria]";
            ((ComboItem)dimComboTipoImpresa.Items[3]).Caption = "[Imprese].[Tipo Impresa].&[Cooperativa]";

            allItem = ((ComboItem) dimCombo.Items[3]);

            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;
            Misura = ((ComboItem) dimCombo.SelectedItem).Name;
            StatoImpresa = "[Imprese Iscritte].[Stato].&[ATTIVA]";

            titoloNuoveIscrizioniMensili.Content = String.Format("Nuove imprese iscritte nel {0}", Anno);
            titoloIscrizioniMensili.Content = String.Format("Imprese iscritte nel {0}", Anno);

            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;
            dimComboTipoImpresa.SelectionChanged += ComboBox_SelectionChanged;

            Initialize = true;

            SetSlider();

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();

                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
                m_Client.BeginGetDataAggiornamento("[Measures].[Data aggiornamento imprese]",
                                                "CuboImprese",
                                                OnEndGetDataAggiornamento,
                                                null);
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;
            Slider3.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;
            Slider3.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
            Slider3.ValueChanged += Slider_ValueChanged;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                            String.Format("[Measures].[Imprese Totali],{0}", Dimensione), "CuboImprese",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members,
                                            String.Format("([Data].[Anno].&[2004]:[Data].[Anno].&[{0}])", maxYear), "",
                                            String.Format("[Measures].[TotalUsed]"), "CuboImprese",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                            String.Format("[Measures].[Importo Debito Denunciato],{0}", Dimensione),
                                            "SICE",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                    
            }
        }

        private void LoadDataSecondSeriesAllYear()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 2:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                            String.Format("[Measures].[Importo Debito Versato],{0}", Dimensione), "SICE",
                                            OnEndGetSecondSeriesResult,
                                            null);
                    break;
                default:
                    break;
            }
        }

        private void LoadDataPerDimensioneImpresa()
        {
            switch (TabIndex)
            {
                case 2:
                    m_Client.BeginGetResult("", "[Imprese].[Id Dimensione]", "",
                                            String.Format(
                                                "[Measures].[Importo Debito Denunciato],{0},[Data Competenza].[Anno].&[{1}]",
                                                Dimensione, Anno),
                                            "SICE",
                                            OnEndGetPerDimensione,
                                            null);
                    break;
            }
        }

        private void LoadDataPerDimensioneSecondSeries()
        {
            switch (TabIndex)
            {
                case 2:
                    m_Client.BeginGetResult("", "[Imprese].[Id Dimensione]", "",
                                            String.Format(
                                                "[Measures].[Importo Debito Versato],{0},[Data Competenza].[Anno].&[{1}]",
                                                Dimensione, Anno),
                                            "SICE",
                                            OnEndGetPerDimensioneSecondSeries,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedYear(bool loadImpreseIscritte = true)
        {
            InizializzazioneInCorso = true;
            LoadImpreseIscritte = loadImpreseIscritte;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format("[Measures].[Imprese Totali],{0}", Dimensione), "CuboImprese",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members,
                                            String.Format("([Data].[Anno].&[1900]:[Data].[Anno].&[{0}])", maxYear),
                                            "([Imprese Iscritte].[Stato].children)",
                                            String.Format("[Measures].[TotalUsed]"), "CuboImprese",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataYearDetails()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 2:
                    m_Client.BeginGetResult("", "[Imprese].[Tipo Impresa]", "",
                                            String.Format(
                                                "[Measures].[Importo Debito Denunciato],[Data Competenza].[Anno].&[{0}]",
                                                Anno),
                                            "SICE",
                                            OnEndGetDettaglioResult,
                                            null);
                    break;
                default:
                    break;
            }
        }

        private void LoadDataYearSecondSeriesDetails()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 2:
                    m_Client.BeginGetResult("", "[Imprese].[Tipo Impresa]", "",
                                            String.Format(
                                                "[Measures].[Importo Debito Versato],[Data Competenza].[Anno].&[{0}]",
                                                Anno),
                                            "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);

                    break;
                default:
                    break;
            }
        }

        private void LoadDataSelectedYearIscrizioni()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult(MembersMonth,
                                            String.Format("([Data].[Anno].&[2004]:[Data].[Anno].&[{0}])", maxYear),
                                            "([Data].[Mese].children)",
                                            String.Format("[Measures].[{0}]", Misura), "CuboImprese",
                                            OnEndGetMonthlyIscrizioniResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data].[Anno]",
                                            "[Data].[Mese]",
                                            String.Format("[Measures].[{0}Percentuali]", Misura), "CuboImprese",
                                            OnEndGetMonthlyIscrizioniResult, null);
                    break;
                    //case 1:
                    //    m_Client.BeginGetResult(
                    //        "",
                    //        String.Format("([TimeIscrizione].[Anno Iscrizione].&[2004]:[TimeIscrizione].[Anno Iscrizione].&[{0}])",maxYear),
                    //        "{[TimeIscrizione].[Mese Iscrizione].children}",
                    //        String.Format("([Measures].[{0}Percentuali])",Misura), "Imprese",
                    //        OnEndGetMonthlyIscrizioniResult,
                    //        null);
                    //    break;
            }
        }

        private void LoadDataAllYearsIscrizioni()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    if (!Misura.Equals("TotalUsed"))
                    {
                        m_Client.BeginGetResult(string.Format("MEMBER [Measures].[AvgVal] AS Avg( Descendants([Data].[TimeHierarchy].currentmember, [Data].[TimeHierarchy].[Mese]), [Measures].[{0}])", Misura),
                        "[Data].[Anno]", "",
                        "[Measures].[AvgVal]", "CuboImprese",
                        OnEndGetAllYearsIscrizioniResult,
                        null);
                    }
                    else
                    {
                        m_Client.BeginGetResult(Members,
                                            String.Format("([Data].[Anno].&[2004]:[Data].[Anno].&[{0}])", maxYear), "",
                                            String.Format("[Measures].[{0}]", Misura), "CuboImprese",
                                            OnEndGetAllYearsIscrizioniResult,
                                            null);}
                    break;
                case 1:
                    LoadDataSelectedYear();
                    break;
            }
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        public static void ConfigureAllYearsAvgChart(ChartArea chartArea, AdoMdHelperCellStone cellStone, string format, int startYear, bool isPercentage = false)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            if (cellStone.Rows[0].Columns.Count > 1)
            {
                DataSeries splineAreaSeries = Common.Common.GetAvgSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                        1);

                splineAreaSeries.Definition = new SplineAreaSeriesDefinition();

                Common.Common.SetSeriesLabel(splineAreaSeries);

                DataSeries temporanea = new DataSeries { Definition = new SplineAreaSeriesDefinition() };
                foreach (DataPoint t in splineAreaSeries)
                    if (
                        !(t.LegendLabel.StartsWith("0") ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) < DateTime.Today.Year - Common.Common.Delay ||
                          Int32.Parse(t.LegendLabel.Substring(0, 4)) > DateTime.Today.Year ||
                          (startYear != DateTime.Today.Year && Int32.Parse(t.LegendLabel.Substring(0, 4)) < startYear)))
                    {
                        temporanea.Add(t);
                    }

                splineAreaSeries = temporanea;
                splineAreaSeries.Definition.ShowItemLabels = false;
                splineAreaSeries.Definition.ShowItemToolTips = true;
                splineAreaSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(127, 0, 198, 255));
                splineAreaSeries.Definition.Appearance.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 163, 210));
                splineAreaSeries.Definition.ItemToolTipFormat = "#XCAT\r\nNumero: #Y{n0}";

                //Aggiungo un punto alla serie per far vedere l'anno corrente
                //DataPoint point = new DataPoint(splineAreaSeries.Count,
                //                                splineAreaSeries[splineAreaSeries.Count - 1].YValue) { LegendLabel = " ", XCategory = " " };
                //splineAreaSeries.Add(point);
                chartArea.DataSeries.Add(splineAreaSeries);

                //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
                //{
                //    tickPoint.LabelFormat = format;
                //}
            }
            if (isPercentage)
            {
                Common.Common.SetScalaPercentage(chartArea, "#VAL{N2}");
            }
            else
            {
                Common.Common.SetScala(chartArea,format);
            }
        }


        private void ConfigureMonthlyPercentageChart(ChartArea chartArea, ChartTitle title)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, (int.Parse(Anno) - 1).ToString(), 0);
            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Colors.White);
            barSeries.LegendLabel = (int.Parse(Anno) - 1).ToString();
            Common.Common.SortDataSeries(barSeries, monthNum);
            string[] months = new[] { "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" };

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }
            double maxValue = 0;
            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            barSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255, 65, 146, 243));
            barSeries.LegendLabel = Anno;

            Common.Common.SortDataSeries(barSeries, monthNum);

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            //chartArea.AxisY.Title = "";

            //string format = "n2";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            Common.Common.SetScalaPercentage(chartArea, "#VAL{N2}");
        }

        private void ConfigureAnnualChart(ChartArea chartArea, ChartTitle title)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Iscrizioni annuali";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            doughnutSeries.Definition.ShowItemLabels = true;
            doughnutSeries.Definition.ShowItemToolTips = true;
            Common.Common.SetSeriesLabel(doughnutSeries);

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   if(dimCombo.SelectedIndex==0)
                                                   {
                                                       ConfigureAllYearsAvgChart(chartNuoveIscrittePerAnno, cellStone,
                                                                                    "#VAL{N0}", DateTime.Today.Year);
         
                                                   }
                                                   else
                                                   {
                                                       Common.Common.ConfigureAllYearsChart(chartNuoveIscrittePerAnno, cellStone,
                                                                                 "#VAL{N0}", DateTime.Today.Year);

                                                   }
                                                   CellsetArray[0].Title = "Nuove imprese iscritte";
                                                   break;
                                               case 1:
                                                   titoloStatoImpreseAnnuale.Content =
                                                       String.Format("Stato imprese al {0}", Anno);
                                                   CellsetArray[0].Title = "Imprese iscritte";
                                                   //ConfigureAllYearsChart(chartStatisticaIscrittePerAnno);
                                                   Common.Common.ConfigureAllYearsChart(chartStatisticaIscrittePerAnno,
                                                                                 cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   break;
                                               case 2:
                                                   CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                                   CellsetArray[0].Title = "Importo Dovuto";
                                                   break;
                                           }
                                       });
            if(TabIndex==2)
            {
                LoadDataSecondSeriesAllYear();
            }
            else
            {
                LoadDataAllYearsIscrizioni();
            }
        }

        private void OnEndGetSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                secondCellStone =
                    m_Client.EndGetResult(asyncResult);
                CellsetArray[1].Content = Common.Common.ToString(secondCellStone);

                switch (TabIndex)
                {
                    case 2:
                        CellsetArray[1].Title = "Importo versato";
                        Common.Common.ConfigureDoubleSeriesChart(chartYearsGen, cellStone,
                                                          secondCellStone, "c0");
                        chartYearsGen.DataSeries[0].LegendLabel = "Denunciato";
                        chartYearsGen.DataSeries[1].LegendLabel = "Versato";
                        break;
                    default:
                        break;
                }
            });
            LoadDataYearDetails();
        }

        private void OnEndGetDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                cellStone =
                    m_Client.EndGetResult(asyncResult);
                CellsetArray[2].Content = Common.Common.ToString(cellStone);
            });

            LoadDataYearSecondSeriesDetails();
        }

        private void OnEndGetSecondSeriesDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                secondCellStone =
                    m_Client.EndGetResult(asyncResult);
                CellsetArray[3].Content = Common.Common.ToString(secondCellStone);

                switch (TabIndex)
                {
                    case 2:
                        YearTitle.Content =
                            String.Format("Dettaglio del {0} per dim. impresa", Anno);

                        Common.Common.ConfigureDoubleBarChart(chartMonthDetailGen, cellStone,
                                                       secondCellStone, "c0");

                        MonthlyTitle.Content = String.Format("Importi per Tipologia Contrattuale del {0}", Anno);
                        break;
                }
            });
                LoadDataPerDimensioneImpresa();
        }

        private void OnEndGetPerDimensione(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                cellStone =
                    m_Client.EndGetResult(asyncResult);
            });
            LoadDataPerDimensioneSecondSeries();
        }

        private void OnEndGetPerDimensioneSecondSeries(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                secondCellStone =
                    m_Client.EndGetResult(asyncResult);
                switch (TabIndex)
                {
                    default:
                        break;
                    case 2:
                        Common.Common.ConfigureDoubleBarChart(chartYearDetail, cellStone,
                                                       secondCellStone, "c0");
                        break;
                }
            });
            InizializzazioneCompletata();
        }


        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   CellsetArray[1].Title = "Nuove imprese iscritte mensilmente";
                                                   Common.Common.ConfigureMonthlyChart(chartNuoveIscrittePerMese, cellStone,
                                                                                "#VAL{N0}", Anno, monthNum);
                                                   //ConfigureMonthlyChart(chartNuoveIscrittePerMese, titoloNuoveIscrizioniMensili); 
                                                   break;
                                               case 1:
                                                   CellsetArray[1].Title = titoloStatoImpreseAnnuale.Content.ToString();
                                                   ConfigureAnnualChart(chartDettaglioIscrittePerAnno,
                                                                        titoloNuoveIscrizioniMensili);
                                                   break;
                                           }
                                       });
            if (Initialize && (TabIndex==0 && LoadImpreseIscritte))
                LoadDataSelectedYearIscrizioni();
            else InizializzazioneCompletata();
        }

        private void OnEndGetAllYearsIscrizioniResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);

                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   //ConfigureAllYearsChart(chartIscrittePerAnno);
                                                   if(dimCombo.SelectedIndex==0)
                                                   {
                                                       if (Misura.Equals("TotalUsed"))
                                                       {
                                                           ConfigureAllYearsAvgChart(chartIscrittePerAnno, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                       }
                                                       else
                                                       {
                                                           Common.Common.ConfigureAllYearsChart(chartIscrittePerAnno, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                       }
                                                   }
                                                   else
                                                   {
                                                       Common.Common.ConfigureAllYearsChart(chartIscrittePerAnno, cellStone, "#VAL{N0}", DateTime.Today.Year);    
                                                   }
                                                   CellsetArray[2].Title = "Imprese iscritte";
                                                   break;
                                           }
                                       });


            LoadDataSelectedYear();
        }

        private void OnEndGetMonthlyIscrizioniResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   CellsetArray[3].Title = "Imprese iscritte mensilmente";
                                                   Common.Common.ConfigureMonthlyChart(chartIscrittePerMese, cellStone, "#VAL{N0}",
                                                                                Anno, monthNum);
                                                   //ConfigureMonthlyChart(chartIscrittePerMese, titoloIscrizioniMensili);
                                                   break;
                                               case 1:
                                                   CellsetArray[3].Title = (string) titoloPercentuali.Content;
                                                   ConfigureMonthlyPercentageChart(chartDettaglioIscrittePerMese,
                                                                                   titoloIscrizioniMensili);
                                                   break;
                                           }
                                       });
            Initialize = false;
            InizializzazioneCompletata();
        }

        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}",
                                                (e.DataPoint.LegendLabel.Substring(0,
                                                                                   e.DataPoint.LegendLabel.LastIndexOf(
                                                                                       ":"))), e.DataPoint.YValue);
            }
        }

        private void ChartDebitoItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                Anno = e.DataPoint.LegendLabel.Split(':')[0];
                LoadDataYearDetails();
            }
        }

        private void MonthItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            if (TabIndex == 1)
                tooltip.Content = string.Format("{0}\rNumero: {1:n2}%", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                                e.DataPoint.YValue);
            else
                tooltip.Content = string.Format("{0}\r{1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                                e.DataPoint.YValue);
        }

        private void ChartIscrittePerMeseClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            titoloIscrizioniMensili.Content = String.Format("Imprese iscritte nel {0}", Anno);
            
                LoadDataSelectedYearIscrizioni();
            }
        }

        private void ChartNuoveIscrittePerMeseYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            titoloNuoveIscrizioniMensili.Content = String.Format("Nuove imprese iscritte nel {0}", Anno);
            

                LoadDataSelectedYear(false);
            }
        }

        private void EuroToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0} {1:c}",
                                                e.DataPoint.LegendLabel.Substring(0,
                                                                                  e.DataPoint.LegendLabel.IndexOf(":")),
                                                e.DataPoint.YValue);
            }
        }

        private void ChartIscrittePerMeseStatisticaClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            Initialize = true;
            titoloStatoImpreseAnnuale.Content = String.Format("Stato imprese al {0}", Anno);
            

                LoadDataSelectedYear();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;
                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();

                Common.Common.InitializeRadChart(RadChart1);
                Common.Common.InitializeRadChart(RadChart2);
                Common.Common.InitializeRadChart(RadChart3);

                dimCombo.Visibility = Visibility.Visible;
                dimComboTipoImpresa.Visibility = Visibility.Collapsed;
                ComboBoxTitle.Text = "Stato impresa:";

                dimCombo.SelectionChanged -= ComboBox_SelectionChanged;
                dimComboTipoImpresa.SelectionChanged -= ComboBox_SelectionChanged;
                if (TabIndex == 1)
                {
                    dimCombo.Items.RemoveAt(3);
                    dimCombo.SelectedIndex = 0;
                    titoloPercentuali.Content = "Percentuale imprese attive";
                    Misura = ((ComboItem)dimCombo.SelectedItem).Name;
                    Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                    Misura = ((ComboItem)dimCombo.SelectedItem).Name;
                }
                else if(TabIndex==0)
                {
                    dimCombo.Items.Add(allItem);
                    dimCombo.SelectedIndex = 3;
                    Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                    Misura = ((ComboItem)dimCombo.SelectedItem).Name;
                }
                else
                {
                    ComboBoxTitle.Text = "Tipo di impresa:";
                    dimCombo.Visibility = Visibility.Collapsed;
                    dimComboTipoImpresa.Visibility = Visibility.Visible;
                    dimComboTipoImpresa.SelectedIndex = 0;
                    Dimensione = ((ComboItem)dimComboTipoImpresa.SelectedItem).Caption;
                }

                dimCombo.SelectionChanged += ComboBox_SelectionChanged;
                dimComboTipoImpresa.SelectionChanged += ComboBox_SelectionChanged;

                LoadDataAllYears();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            Initialize = true;

            busyIndicator.IsBusy = false;
        }


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(TabIndex>1)
            {
                Dimensione = ((ComboItem)dimComboTipoImpresa.SelectedItem).Caption;
            }
            else
            {
                Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                Misura = ((ComboItem)dimCombo.SelectedItem).Name;    
            }
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                if (TabIndex == 1)
                {
                    switch (dimCombo.SelectedIndex)
                    {
                        case 0:
                            titoloPercentuali.Content = "Percentuale imprese attive";
                            break;
                        case 1:
                            titoloPercentuali.Content = "Percentuale imprese sospese";
                            break;
                        case 2:
                            titoloPercentuali.Content = "Percentuale imprese cessate";
                            break;
                    }
                    LoadDataSelectedYearIscrizioni();
                }
                else
                {
                    Initialize = true;
                    LoadDataAllYears();
                }
            }
            else
            {
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray, filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n = 0;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion
    }
}