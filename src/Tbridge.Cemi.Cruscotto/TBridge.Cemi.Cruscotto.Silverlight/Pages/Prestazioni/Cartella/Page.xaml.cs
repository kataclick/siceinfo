﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using TBridge.Cemi.Cruscotto.Template;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.Prestazioni.Cartella
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public ComboItem esitoPagamentoItem = new ComboItem();
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool InizializzazioneInCorso;
        public ICubeService m_Client;
        public ComboItem modalitaPagamentoItem = new ComboItem();
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }
        public AdoMdHelperCellStone secondCellStone { get; set; }


        public string Members { set; get; }

        public string Anno { set; get; }

        public Int32 Semestre { set; get; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            Mese = "Primo Semestre";
            Semestre = 1;

            Misura = "[Measures].[Numero Prestazioni],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]";

            Members = "Member [Tipi Prestazione].[Tipo Prestazione].[OldAndNew] AS'Aggregate({";
            for (int i = 0; i < Common.Common.DictionaryCodici[CodicePrestazione].Count; i++)
                Members = String.Format("{0}[Tipi Prestazione].[Tipo Prestazione].&{1},", Members, Common.Common.DictionaryCodici[CodicePrestazione][i]);

            Members = String.Format("{0}{1})'", Members.Substring(0, Members.Length - 1), "}");

            Dimensione = "[Lavoratori].[Provenienza]";


            modalitaPagamentoItem.Caption = "[Prestazioni Liquidate].[Modalita Pagamento]";
            modalitaPagamentoItem.Content = "Modalità di Liquidazione";
            modalitaPagamentoItem.Margin = ((ComboItem) dimCombo.Items[0]).Margin;

            esitoPagamentoItem.Caption = "[Prestazioni Liquidate].[Descrizione Stato Assegno]";
            esitoPagamentoItem.Content = "Stato pagamento";
            esitoPagamentoItem.Margin = ((ComboItem) dimCombo.Items[0]).Margin;

            SetSlider();

            dimCombo.SelectedIndex = 1;
            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += dimSelectionChanged;

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;
            
            TextBlockNetto.Visibility = System.Windows.Visibility.Collapsed;
            TextBlockImporto.Visibility = System.Windows.Visibility.Collapsed;
            
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure = String.Empty;
            switch (TabIndex)
            {
                case 0:
                    measure = "[Measures].[Data aggiornamento domande]";
                    break;
                case 1:
                    measure = "[Measures].[Data aggiornamento domande]";//"[Measures].[Data aggiornamento prestazioni liquidate]";
                    break;
                case 2:
                    measure = "[Measures].[Data aggiornamento domande]";//"[Measures].[Data aggiornamento prestazioni liquidate]";
                    break;
            }
            if (measure != String.Empty)
                m_Client.BeginGetDataAggiornamento(measure,
                                                "Prestazioni",
                                                OnEndGetDataAggiornamento,
                                                null);
            else
            {
                LoadDataAllYears();
            }
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }



        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            "[Measures].[Lavoratori Interessati]", "Cartella",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;

                case 1:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            "[Measures].[Numero Lavoratori Liquidati]",
                                            "Cartella",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            "[Measures].[Importo Erogato]",
                                            "Cartella",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
            }
        }

        private void LoadDataAllYearsSecondSeries()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            "[Measures].[Numero Lavoratori Liquidati]", "Cartella",
                                            OnEndGetAllYearsSecondSeriesResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            "[Measures].[Importo Erogato]", "Cartella",
                                            OnEndGetAllYearsSecondSeriesResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedYear()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Semestri].[Id Semestre]", "",
                                            String.Format("[Measures].[Lavoratori Interessati],[Time].[Anno].&[{0}]",
                                                          Anno), "Cartella",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Semestri].[Id Semestre]", "",
                                            String.Format(
                                                "[Measures].[Numero Lavoratori Liquidati],[Time].[Anno].&[{0}]", Anno),
                                            "Cartella",
                                            OnEndGetAnnualResult,
                                            null);

                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "[Time].[Mese]",
                                            "[Measures].[Importo Erogato]",
                                            "Cartella",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedYearSecondSeries()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Semestri].[Id Semestre]", "",
                                            String.Format(
                                                "[Measures].[Numero Lavoratori Liquidati],[Time].[Anno].&[{0}]", Anno),
                                            "Cartella",
                                            OnEndGetAnnualSecondSeriesResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Semestri].[Id Semestre]", "",
                                            String.Format("[Measures].[Importo Erogato],[Time].[Anno].&[{0}]", Anno),
                                            "Cartella",
                                            OnEndGetAnnualSecondSeriesResult,
                                            null);

                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "[Time].[Mese]",
                                            "[Measures].[Importo Erogato]",
                                            "Cartella",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthly()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", Dimensione, "",
                                            String.Format(
                                                "[Measures].[Lavoratori Interessati],[Semestri].[Id Semestre].&[{0}],[Time].[Anno].&[{1}]",
                                                Semestre, Anno), "Cartella",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", Dimensione, "",
                                            String.Format(
                                                "[Measures].[Numero Lavoratori Liquidati],[Semestri].[Id Semestre].&[{0}],[Time].[Anno].&[{1}]",
                                                Semestre, Anno), "Cartella",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;

                case 2:
                    m_Client.BeginGetResult("", string.Format("[Time].[Anno].&[{0}]", Anno), Dimensione,
                                            String.Format("[Measures].[Importo Erogato],[Time].[Mese].&[{0}]",
                                                          NumMese), "Cartella",
                                            OnEndGetMonthlyResult,
                                            null);

                    break;
            }
            //LoadDataMonthDetails();
        }

        private void LoadDataMonthlySecondSeries()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    String dim = Dimensione;
                    if (Dimensione.Equals("[Cartella].[Fascia Età]"))
                    {
                        dim = "[Prestazioni Liquidate].[Fascia Età]";
                    }
                    m_Client.BeginGetResult("", dim, "",
                                            String.Format(
                                                "[Measures].[Numero Lavoratori Liquidati],[Semestri].[Id Semestre].&[{0}],[Time].[Anno].&[{1}]",
                                                Semestre, Anno), "Cartella",
                                            OnEndGetMonthlySecondSeriesResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", Dimensione, "",
                                            String.Format(
                                                "[Measures].[Importo Erogato],[Semestri].[Id Semestre].&[{0}],[Time].[Anno].&[{1}]",
                                                Semestre, Anno), "Cartella",
                                            OnEndGetMonthlySecondSeriesResult,
                                            null);
                    break;
            }
            //LoadDataMonthDetails();
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   //ConfigureAllYearsChart(chartYearsGen, AnnualTitle);
                                                   //Common.ConfigureAllYearsChart(chartLavoratoriInteressati, cellStone,
                                                   //                              "#VAL{N0}");
                                                   CellsetArray[0].Title = "Lavoratori interessati";
                                                   break;
                                               case 1:
                                                   //ConfigureAllYearsChart(chartYearsGen, AnnualTitle);
                                                   //Common.ConfigureAllYearsChart(chartLavoratoriLiquidati, cellStone,
                                                   //                              "#VAL{N0}");
                                                   CellsetArray[0].Title = "Lavoratori Liquidati";
                                                   CellsetArray[1].Title = String.Format(
                                                       "Lavoratori Liquidati nel {0}", Anno);
                                                   break;
                                           }
                                       });
            if (TabIndex == 2)
                LoadDataSelectedYear();
            else
            {
                LoadDataAllYearsSecondSeries();
            }
        }

        private void OnEndGetAllYearsSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   //ConfigureAllYearsChart(chartYearsGen, AnnualTitle);
                                                   Common.Common.ConfigureDoubleSplineSeriesChart(chartLavoratoriInteressati,
                                                                                           cellStone, secondCellStone,
                                                                                           "#VAL{N0}");
                                                   try
                                                   {
                                                       chartLavoratoriInteressati.DataSeries[0].LegendLabel =
                                                           "Lav. liquidabili";
                                                       chartLavoratoriInteressati.DataSeries[1].LegendLabel =
                                                           "Lav. liquidati";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }
                                                   break;
                                               case 1:
                                                   //ConfigureAllYearsChart(chartYearsGen, AnnualTitle);
                                                   Common.Common.ConfigureMultipleAxisChart(chartLavoratoriLiquidati,
                                                                                     cellStone, secondCellStone,
                                                                                     "#VAL{N0}");
                                                   try
                                                   {
                                                       chartLavoratoriLiquidati.DataSeries[0].LegendLabel =
                                                           "Lav. liquidati";
                                                       chartLavoratoriLiquidati.DataSeries[1].LegendLabel =
                                                           "Imp. liquidati";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }
                                                   break;
                                           }
                                       });
            LoadDataSelectedYear();
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   titoloLavoratoriInteressati.Content =
                                                       String.Format("Lavoratori interessati nel {0}",
                                                                     Anno);
                                                   CellsetArray[1].Title = (string) titoloLavoratoriInteressati.Content;
                                                   //Common.ConfigureMonthlyChart(chartLavoratoriInteressatiDettaglio,
                                                   //                             cellStone, "#VAL{N0}", Anno, monthNum);

                                                   break;
                                               case 1:
                                                   titoloLavoratoriLiquidati.Content =
                                                       String.Format("Lavoratori liquidati nel {0}",
                                                                     Anno);
                                                   CellsetArray[1].Title = (string) titoloLavoratoriLiquidati.Content;
                                                   //Common.ConfigureMonthlyChart(chartLavoratoriLiquidatiPerMese,
                                                   //                             cellStone, "#VAL{N0}", Anno, monthNum);
                                                   break;
                                           }
                                       });
            if (TabIndex == 2)
                LoadDataMonthly();
            else LoadDataSelectedYearSecondSeries();
        }

        private void OnEndGetAnnualSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureDoubleBarChart(
                                                       chartLavoratoriInteressatiDettaglio,
                                                       cellStone, secondCellStone, "#VAL{N0}");
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureDoubleBarMultipleAxisChart(
                                                       chartLavoratoriLiquidatiPerMese,
                                                       cellStone, secondCellStone, "#VAL{N0}");
                                                   try
                                                   {
                                                       chartLavoratoriLiquidatiPerMese.DataSeries[0].LegendLabel =
                                                           "Lav. liquidati";
                                                       chartLavoratoriLiquidatiPerMese.DataSeries[1].LegendLabel =
                                                           "Imp. liquidati";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }
                                                   break;
                                           }
                                       });
            LoadDataMonthly();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                           }
                                       });
            LoadDataMonthlySecondSeries();
        }

        private void OnEndGetMonthlySecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 0:
                                                   Common.Common.ConfigureDoubleBarChart(
                                                       chartLavoratoriInteressatiPerAnno,
                                                       cellStone, secondCellStone, "#VAL{N0}");

                                                   titoloLavoratoriPerAnno.Content =
                                                       String.Format("Dettaglio del {0} {1}", Mese, Anno);
                                                   CellsetArray[2].Title = titoloLavoratoriPerAnno.Content.ToString();
                                                   InizializzazioneCompletata();
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureDoubleBarMultipleAxisChart(chartDettaglioLavoratori,
                                                                                              cellStone, secondCellStone,
                                                                                              "#VAL{N0}");

                                                   titoloDettaglioLiquidati.Content =
                                                       String.Format("Dettaglio del {0} {1}", Mese, Anno);
                                                   CellsetArray[2].Title = titoloDettaglioLiquidati.Content.ToString();

                                                   try
                                                   {
                                                       chartDettaglioLavoratori.DataSeries[0].LegendLabel =
                                                           "Lav. liquidati";
                                                       chartDettaglioLavoratori.DataSeries[1].LegendLabel =
                                                           "Imp. liquidati";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }

                                                   InizializzazioneCompletata();
                                                   break;
                                           }
                                       });
        }

        private void OnEndGetDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 1:
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }


        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                if (e.DataSeries.LegendLabel != null)
                {
                    tooltip.Content = string.Format(e.DataSeries.LegendLabel.Equals("Imp. liquidati") ? "{0}\r\n{1:c0}" : "{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel.Substring(0,
                                                                                                                                                                                      e.DataPoint.LegendLabel.
                                                                                                                                                                                          LastIndexOf(
                                                                                                                                                                                              ":"))), e.DataPoint.YValue);
                }
                else
                {


                    tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}",
                                                    (e.DataPoint.LegendLabel.Substring(0,
                                                                                       e.DataPoint.LegendLabel.
                                                                                           LastIndexOf(
                                                                                               ":"))),
                                                    e.DataPoint.YValue);
                }
            }
        }

        private void ChartItemYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                switch (TabIndex)
                {
                    default:
                        LoadDataSelectedYear();
                        break;
                }
            }
        }

        private void ChartItemSemestreClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            Mese = e.DataPoint.LegendLabel.Split(':')[0];
            if (Mese.Equals("Primo Semestre"))
                Semestre = 1;
            else Semestre = 2;

            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataMonthly();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            Common.Common.InitializeRadChart(RadChart2);

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;
                if (TabIndex != 1)
                {
                    TextBlockNetto.Visibility = Visibility.Collapsed;
                    TextBlockImporto.Visibility = Visibility.Collapsed;
                }
                else
                {

                    TextBlockNetto.Visibility = Visibility.Visible;
                    TextBlockImporto.Visibility = Visibility.Visible;
                }
                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();
                filterContainer.Visibility = Visibility.Visible;
                if (dimCombo.Items.Contains(esitoPagamentoItem))
                    dimCombo.Items.Remove(esitoPagamentoItem);
                if (dimCombo.Items.Contains(modalitaPagamentoItem))
                    dimCombo.Items.Remove(modalitaPagamentoItem);
                switch (TabIndex)
                {
                    default:
                        filterContainer.Visibility = Visibility.Collapsed;
                        break;
                    case 0:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Cartella].[Fascia Età]";
                        break;
                    case 1:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Prestazioni Liquidate].[Fascia Età]";
                        dimCombo.Items.Add(esitoPagamentoItem);
                        dimCombo.Items.Add(modalitaPagamentoItem);
                        break;
                    case 2:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Prestazioni Liquidate].[Fascia Età]";
                        dimCombo.Items.Add(esitoPagamentoItem);
                        dimCombo.Items.Add(modalitaPagamentoItem);
                        break;
                }

                dimCombo.SelectionChanged -= dimSelectionChanged;

                Dimensione = "[Lavoratori].[Provenienza]";
                dimCombo.SelectedIndex = 1;

                dimCombo.SelectionChanged += dimSelectionChanged;

                CaricaDateAggiornamentoDati();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void dimSelectionChanged(object sender, SelectionChangedEventArgs changedEventArgs)
        {
            if (!worker.IsBusy)
            {
            if (dimCombo.SelectedItem != null)
            {
                Dimensione = ((ComboItem) (dimCombo.SelectedItem)).Caption;

                
                    InizializzazioneInCorso = true;
                    busyIndicator.IsBusy = true;
                    worker.RunWorkerAsync();
                    switch (TabIndex)
                    {
                        default:
                            LoadDataSelectedYear();
                            break;
                        case 1:
                            LoadDataMonthly();
                            break;
                    }
                }
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            

            busyIndicator.IsBusy = false;
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion
    }
}