﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Template;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.Prestazioni
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        public bool InizializzazioneInCorso;
        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public ObservableCollection<AdoMdHelperCubePInfo> list = new ObservableCollection<AdoMdHelperCubePInfo>();
        public ICubeService m_Client;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }
        public AdoMdHelperCellStone secondCellStone { get; set; }

        public string Members { set; get; }

        public string Anno { set; get; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        public bool IsSliderSet { get; set; }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month-1;
            }
            else
            {
                Anno = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            Misura = "[Measures].[Numero Prestazioni],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]";

            Members = "Member [Tipi Prestazione].[Tipo Prestazione].[OldAndNew] AS'Aggregate({";
            //string[] appoggio = CodicePrestazione.Split(',');
            for (int i = 0; i < Common.Common.DictionaryCodici[CodicePrestazione].Count; i++)
                Members = String.Format("{0}[Tipi Prestazione].[Tipo Prestazione].&{1},", Members,
                                        Common.Common.DictionaryCodici[CodicePrestazione][i]);

            Members = String.Format("{0}{1})'", Members.Substring(0, Members.Length - 1), "}");

            Dimensione = "[Stato Domanda].[Stato Domanda]";
            //[Time].[1997]:[Time].[1999]
            dimCombo.SelectedIndex = 0;
            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += dimSelectionChanged;

            SetSlider();
            
            TextBlockNetto.Visibility = System.Windows.Visibility.Collapsed;
            TextBlockImporto.Visibility = System.Windows.Visibility.Collapsed;

            worker.WorkerSupportsCancellation = true;
            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
            //LoadDataAllYears();
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure = String.Empty;
            switch (TabIndex)
            {
                default:
                    measure = "[Measures].[Data aggiornamento domande]";
                    break;
                
                //case 0:
                //    measure = "[Measures].[Data aggiornamento prestazioni liquidate]";
                //    break;
                //case 1:
                //    measure = "[Measures].[Data aggiornamento domande]";
                //    break;
                //case 2:
                //    measure = "[Measures].[Data aggiornamento prestazioni liquidate]";
                //    break;
                //case 3:
                //    measure = "[Measures].[Data aggiornamento prestazioni liquidate]";
                //    break;
                //case 4:
                //    measure = "[Measures].[Data aggiornamento prestazioni liquidate]";
                //    break;
        }
            if (measure != String.Empty)
                m_Client.BeginGetDataAggiornamento(measure,
                                                "Prestazioni",
                                                OnEndGetDataAggiornamento,
                                                null);
            else
            {
                LoadDataAllYears();
            }
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider3.ValueChanged -= Slider_ValueChanged;
            Slider4.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider3.Value = Common.Common.Delay;
            Slider4.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider3.ValueChanged += Slider_ValueChanged;
            Slider4.ValueChanged += Slider_ValueChanged;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult(Members, "[Data Domanda].[Anno]", "",
                                            Misura, "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members, "[Data Domanda].[Anno]", "",
                                            "[Measures].[Numero Domande],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;

                case 2:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                            "[Measures].[Numero Prestazioni Liquidate],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 4:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                            "[Measures].[TempoMedioDiLiquidazione],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 5:
                    m_Client.BeginGetResult(Members, "[Data Domanda].[Anno]", "",
                                            "[Measures].[RapportoLavoratoriRichiedentiAttivi],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
            }
        }

        private void LoadTempoMedioAnnuale()
        {
            m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                           "[Measures].[TempoMedioDiLiquidazioneNew],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                           "Prestazioni",
                                           OnEndGetTempoMedioAnnuale,
                                           null);
        }

        private void OnEndGetTempoMedioAnnuale(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                secondCellStone =
                    m_Client.EndGetResult(asyncResult);
                CellsetArray[2].Content = Common.Common.ToString(secondCellStone);

                        CellsetArray[0].Title = "";
                Common.Common.ConfigureDoubleBarChart(chartYearsAvg, cellStone, secondCellStone, "#VAL{N0}");
                
                    chartYearsAvg.DataSeries[0].LegendLabel = "Da data domanda";
                    chartYearsAvg.DataSeries[1].LegendLabel = "Da data gestione";
            });
            LoadDataSelectedYear();
        }

        private void LoadDataSelectedYear()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult(Members, string.Format("[Data Domanda].[Anno].&[{0}]", Anno), Dimensione,
                                            Misura, "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members, "[Domande Pervenute].[Tipo Inserimento]", "[Data Domanda].[Mese]",
                                            string.Format(
                                                "[Measures].[Numero Domande],[Data Domanda].[Anno].&[{0}],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                                Anno), "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;

                case 2:
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                            "[Measures].[Numero Prestazioni Liquidate],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 4:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", Dimensione,
                                            "[Measures].[TempoMedioDiLiquidazione],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 5:
                    m_Client.BeginGetResult(Members, "[Data Domanda].[Anno]", "",
                                            "[Measures].[RapportoLavoratoriRichiedentiAttivi],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthDetails()
        {
            InizializzazioneInCorso = true;
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            string mis = String.Empty;
            Mese = months[NumMese - 1];

            switch (TabIndex)
            {
                case 0:
                    mis = string.Format("{1},[Data Domanda].[Mese].&[{0}]", NumMese, Misura);
                    m_Client.BeginGetResult(Members, string.Format("[Data Domanda].[Anno].&[{0}]", Anno), Dimensione,
                                            mis, "Prestazioni",
                                            OnEndGetDettaglioResult,
                                            null);
                    break;
                case 1:
                    break;
                case 2:
                    mis =
                        string.Format(
                            "[Measures].[Numero Prestazioni Liquidate],[Data Liquidazione].[Mese].&[{0}],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                            NumMese);
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                            Dimensione, mis,
                                            "Prestazioni",
                                            OnEndGetDettaglioResult,
                                            null);

                    break;
                case 3:
                    mis =
                        string.Format(
                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew],[Data Liquidazione].[Mese].&[{0}]",
                            NumMese);
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                            Dimensione, mis,
                                            "Prestazioni",
                                            OnEndGetDettaglioResult,
                                            null);

                    break;
            }
        }

        private void LoadDataMonthly()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult(Members, string.Format("[Data Domanda].[Anno]"), "[Data Domanda].[Mese]",
                                            Misura,
                                            "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members, "[Domande Pervenute].[Autenticazione]",
                                            String.Format("[Data Domanda].[Anno].&[{0}]", Anno),
                                            "[Measures].[Numero Domande],[Domande Pervenute].[Tipo Inserimento].&[Domanda Telematica],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAutenticazioneDomande,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno]"),
                                            "[Data Liquidazione].[Mese]",
                                            "[Measures].[Numero Prestazioni Liquidate],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 4:
                    m_Client.BeginGetResult(Members, String.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                            "[Data Liquidazione].[Mese]",
                                            "[Measures].[TempoMedioDiLiquidazione],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;

                case 3:
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno]"),
                                            "[Data Liquidazione].[Mese]",
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 5:
                    m_Client.BeginGetResult(Members, "[Data Domanda].[Anno]", "",
                                            "[Measures].[RapportoPrestazioniLavoratoriAttivi],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
            }
            LoadDataMonthDetails();
        }

        private void LoadTempoMedioMensile()
        {
            m_Client.BeginGetResult(Members, String.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                           "[Data Liquidazione].[Mese]",
                                           "[Measures].[TempoMedioDiLiquidazioneNew],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                           "Prestazioni",
                                           OnEndGetTempoMedioMensile,
                                           null);
        }

        private void OnEndGetTempoMedioMensile(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(secondCellStone);
                                          CellsetArray[3].Title =
                                               String.Format("Tempo medio di liquidazione nel {0}", Anno);
                                           ConfigureTempoMedioMonthlyChart(chartMonthTime);
                                       });
            InizializzazioneCompletata();
        }

        private void LoadDomande()
        {
            InizializzazioneInCorso = true;
            m_Client.BeginGetResult(Members, "[Domande Pervenute].[Autenticazione]", "[Data Domanda].[Anno]",
                                    "[Measures].[Numero Domande],[Domande Pervenute].[Tipo Inserimento].&[Domanda Telematica],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                    "Prestazioni",
                                    OnEndGetAutenticazioneDomande,
                                    null);
        }

        private void LoadDimensionMembers()
        {
            m_Client.BeginGetDimMembers(Dimensione, "Prestazioni",
                                        OnEndGetDimMembers,
                                        null);
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        private void ConfigureAnnualChart(ChartArea chartArea, ChartTitle title)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();
            switch (TabIndex)
            {
                case 0:
                    title.Content = String.Format("Numero di prestazioni nel {0}", Anno);
                    break;
                case 1:
                    title.Content = String.Format("Domande pervenute nel {0}", Anno);
                    break;
                case 2:
                    title.Content = String.Format("Prestazioni liquidate nel {0}", Anno);
                    break;
                case 3:
                    title.Content = String.Format("Importi erogati nel {0}", Anno);
                    break;
            }

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Prestazioni annuali";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            doughnutSeries.Definition.ShowItemLabels = true;
            doughnutSeries.Definition.ShowItemToolTips = true;
            Common.Common.SetSeriesLabel(doughnutSeries);

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
        }

        private void ConfigureDettaglioChart(ChartArea chartArea, ChartTitle title)
        {
            title.Content = String.Format("Dettaglio di {0} {1}", Mese, Anno);
            CellsetArray[3].Title = (string) title.Content;

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Dettaglio mensile";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            Common.Common.SetSeriesLabel(doughnutSeries);
            doughnutSeries.Definition.ShowItemToolTips = true;
            chartArea.DataSeries.Clear();

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
            foreach (DataPoint item in chartArea.DataSeries[0])
            {
                if (item.YValue == 0)
                    item.Label = " ";
            }
            ChartLegend2.Header = "Legenda:";
        }

        private void ConfigureTempoMedioMonthlyChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            chartArea.AxisY.TickPoints.Clear();
            
            if (cellStone.Rows[0].Columns.Count > 1)
            {
                TempoMedioTitle.Content = String.Format("Tempo medio di liquidazione nel {0}",
                                                        cellStone.Rows[0].Columns[1].Caption);

                string[] months = new[]
                                      {
                                          "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov",
                                          "Dic"
                                      };

                DataSeries barSeries = Common.Common.GetSeries(cellStone, cellStone.Rows[0].Columns[1].Caption, 0);
                barSeries.Definition = new LineSeriesDefinition();
                barSeries.Definition.ShowItemLabels = false;
                barSeries.Definition.ShowItemToolTips = true;
                barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255, 65, 146, 243));
                Common.Common.SortDataSeries(barSeries, monthNum);

                foreach (DataPoint point in barSeries)
                {
                    point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
                }
                if (barSeries.Count != 0)
                    chartArea.DataSeries.Add(barSeries);

                DataSeries secondBarSeries = Common.Common.GetSeries(secondCellStone, secondCellStone.Rows[0].Columns[1].Caption, 0);
                secondBarSeries.Definition = new LineSeriesDefinition();
                secondBarSeries.Definition.ShowItemLabels = false;
                secondBarSeries.Definition.ShowItemToolTips = true;
                secondBarSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255, 65, 146, 243));
                Common.Common.SortDataSeries(secondBarSeries, monthNum);

                foreach (DataPoint point in secondBarSeries)
                {
                    point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
                }
                if (secondBarSeries.Count != 0)
                    chartArea.DataSeries.Add(secondBarSeries);



                chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
                chartArea.AxisY.Title = "(Giorni)";

                Common.Common.SetScala(chartArea, "#VAL{N0}");
            }
        }

        private void ConfigureDomandePervenuteChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            DomandeTitle.Content = String.Format("Domande pervenute nel {0}", Anno);

            DataSeries series = Common.Common.GetSeries(cellStone, "Domanda Cartacea", 0);
            series.Definition = new LineSeriesDefinition();
            series.LegendLabel = "Domande Cartacee";
            series.Definition.ShowItemLabels = false;
            series.Definition.ShowItemToolTips = true;
            series.Definition.Appearance.Stroke = new SolidColorBrush(Colors.LightGray);

            Common.Common.SortDataSeries(series, monthNum);
            string[] months = new[] {"Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"};

            foreach (DataPoint point in series)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            chartArea.DataSeries.Add(series);

            series = Common.Common.GetSeries(cellStone, "Domanda Telematica", 0);
            series.Definition = new LineSeriesDefinition();
            series.LegendLabel = "Domande Telematiche";
            series.Definition.ShowItemLabels = false;
            series.Definition.ShowItemToolTips = true;

            series.Definition.Appearance.Stroke = new SolidColorBrush(Colors.Orange);

            Common.Common.SortDataSeries(series, monthNum);

            foreach (DataPoint point in series)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            chartArea.DataSeries.Add(series);
            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            Common.Common.SetScala(chartArea, "#VAL{N0}");
        }

        private void ConfigureAutenticazioneChart(ChartArea chartArea)
        {
            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 1);
            doughnutSeries.LegendLabel = "Dettaglio mensile";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            Common.Common.SetSeriesLabel(doughnutSeries);
            doughnutSeries.Definition.ShowItemToolTips = true;
            chartArea.DataSeries.Clear();
            chartArea.DataSeries.Add(doughnutSeries);
            foreach (DataPoint item in chartArea.DataSeries[0])
            {
                if (item.YValue == 0)
                    item.Label = " ";
            }
            ChartLegend2.Header = "Prestazioni mensili";
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureAllYearsChart(chartYearsGen, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Numero annuale di prestazioni";
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureAllYearsChart(chartYearsDomande, cellStone,
                                                                                        "#VAL{N0}",2008);
                                                   CellsetArray[0].Title = "Numero Annuale di Domande";
                                                   break;
                                               case 2:
                                                   Common.Common.ConfigureAllYearsChart(chartYearsLiq, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Numero Annuale di prestazioni liquidate";
                                                   break;
                                               case 3:
                                                   Common.Common.ConfigureAllYearsChart(chartYearsImp, cellStone, "c0", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Importi liquidati annualmente";
                                                   break;
                                               case 4:
                                                   CellsetArray[0].Title = "Tempo medio di liquidazione";
                                                   //Common.Common.ConfigureAllYearsChart(chartYearsAvg, cellStone, "#VAL{N0}");
                                                   break;
                                           }
                                       });
            if(TabIndex==4)
            {
                LoadTempoMedioAnnuale();
            }
            else
            {
                LoadDataSelectedYear();
            }    
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   AnnualTitle.Content = String.Format("Numero di prestazioni nel {0}",
                                                                                       Anno);
                                                   CellsetArray[1].Title = (string) AnnualTitle.Content;
                                                   ConfigureAnnualChart(chartYearDetailGen, AnnualTitle);
                                                   break;
                                               case 1:
                                                   AnnualTitle.Content = String.Format("Domande pervenute nel {0}", Anno);
                                                   CellsetArray[1].Title = (string) AnnualTitle.Content;
                                                   ConfigureDomandePervenuteChart(chartDomandeYear);
                                                   break;
                                               case 2:
                                                   AnnualTitleb.Content = String.Format(
                                                       "Prestazioni liquidate nel {0}", Anno);
                                                   CellsetArray[1].Title = (string) AnnualTitleb.Content;
                                                   ConfigureAnnualChart(chartYearDetailLiq, AnnualTitleb);
                                                   break;
                                               case 3:
                                                   AnnualTitlee.Content = String.Format("Importi erogati nel {0}", Anno);
                                                   CellsetArray[1].Title = (string) AnnualTitlee.Content;
                                                   ConfigureAnnualChart(chartYearDetailImp, AnnualTitlee);
                                                   break;
                                           }
                                       });
            LoadDataMonthly();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           if(TabIndex!=4)
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   CellsetArray[2].Title = "Numero mensile di prestazioni";
                                                   Common.Common.ConfigureMonthlyChart(chartMonthGen, cellStone, "#VAL{N0}",
                                                                                       Anno,
                                                                                       monthNum);
                                                   break;
                                               case 2:
                                                   CellsetArray[2].Title = "Numero mensile di Domande";
                                                   Common.Common.ConfigureMonthlyChart(chartMonthLiq, cellStone, "#VAL{N0}",
                                                                                       Anno,
                                                                                       monthNum);
                                                   break;
                                               case 3:
                                                   CellsetArray[2].Title = "Importi mensilmente erogati";
                                                   Common.Common.ConfigureMonthlyChart(chartMonthImp, cellStone, "c0",
                                                                                       Anno,
                                                                                       monthNum);
                                                   break;
                                               case 4:
                                                   CellsetArray[1].Content = Common.Common.ToString(cellStone);
                                                   CellsetArray[1].Title =
                                                       String.Format("Tempo medio di liquidazione nel {0}", Anno);
                                                   break;
                                               case 5:
                                                   break;
                                           }
                                       });
            if (TabIndex == 1)
                LoadDomande();
            if (TabIndex == 4)
            {
                LoadTempoMedioMensile();
            }
        }

        private void OnEndGetAutenticazioneDomande(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           switch (TabIndex)
                                           {
                                               default:
                                                   ConfigureAutenticazioneChart(chartAutenticazione);
                                                   break;
                                               case 5:
                                                   //ConfigureIndicatoriMonthlyChart(chartMediaLav);
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        private void OnEndGetDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   ConfigureDettaglioChart(chartMonthDetailGen, DetailsTitle);
                                                   break;
                                               case 2:
                                                   ConfigureDettaglioChart(chartMonthDetailLiq, DetailsTitleb);
                                                   break;
                                               case 3:
                                                   ConfigureDettaglioChart(chartMonthDetailImp, DetailsTitlee);
                                                   break;
                                               case 4:
                                                   break;
                                               case 5:
                                                   //ConfigureIndicatoriMonthlyChart(chartMediaPrs);
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        private void OnEndGetDimMembers(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate { DimensionMembers = m_Client.EndGetDimMembers(asyncResult); });
        }

        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}",
                                                (e.DataPoint.LegendLabel.Substring(0,
                                                                                   e.DataPoint.LegendLabel.LastIndexOf(
                                                                                       ":"))), e.DataPoint.YValue);
            }
        }

        private void MonthItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            if (TabIndex != 3)
                tooltip.Content = string.Format("{0}\rNumero: {1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                                e.DataPoint.YValue);
            else
                tooltip.Content = string.Format("{0}\r{1:c}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                                e.DataPoint.YValue);
        }

        private void DayToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nGiorni: {1:n0}",
                                                (e.DataPoint.LegendLabel.Substring(0,
                                                                                   e.DataPoint.LegendLabel.IndexOf(":"))),
                                                e.DataPoint.YValue);
            }
        }

        private void DayMonthToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            tooltip.Content = string.Format("{0}\r\nGiorni: {1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                            e.DataPoint.YValue);
        }

        private void ChartItemYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataSelectedYear();
            }
        }

        private void MonthGenItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            string mis = string.Format("{1},[Data Domanda].[Mese].&[{0}]", e.DataPoint.LegendLabel, Misura);
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            NumMese = Int32.Parse(e.DataPoint.LegendLabel);
            Mese = months[Int32.Parse(e.DataPoint.LegendLabel) - 1];
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                m_Client.BeginGetResult(Members, string.Format("[Data Domanda].[Anno].&[{0}]", Anno), Dimensione, mis,
                                        "Prestazioni",
                                        OnEndGetDettaglioResult,
                                        null);
            }
        }

        private void MonthLiqItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            Mese = months[Int32.Parse(e.DataPoint.LegendLabel) - 1];
            NumMese = Int32.Parse(e.DataPoint.LegendLabel);
            string mis =
                string.Format(
                    "[Measures].[Numero Prestazioni Liquidate],[Data Liquidazione].[Mese].&[{0}],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                    e.DataPoint.LegendLabel);
           
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                        Dimensione, mis,
                                        "Prestazioni",
                                        OnEndGetDettaglioResult,
                                        null);
            }
        }

        private void MonthImpItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            Mese = months[Int32.Parse(e.DataPoint.LegendLabel) - 1];
            NumMese = Int32.Parse(e.DataPoint.LegendLabel);
            string mis =
                string.Format(
                    "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew],[Data Liquidazione].[Mese].&[{0}]",
                    e.DataPoint.LegendLabel);
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                        Dimensione, mis,
                                        "Prestazioni",
                                        OnEndGetDettaglioResult,
                                        null);
            }
        }

        private void EuroToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0} {1:c}", e.DataPoint.LegendLabel.Substring(0, 5), e.DataPoint.YValue);
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs routedEventArgs)
        {
            Common.Common.InitializeRadChart(RadChart1);
            Common.Common.InitializeRadChart(RadChart2d);
            Common.Common.InitializeRadChart(RadChart2);
            Common.Common.InitializeRadChart(RadChart1e);
            Common.Common.InitializeRadChart(RadChart3);

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;
                if (TabIndex != 3)
                {
                    TextBlockNetto.Visibility = System.Windows.Visibility.Collapsed;
                    TextBlockImporto.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    TextBlockNetto.Visibility = System.Windows.Visibility.Visible;
                    TextBlockImporto.Visibility = System.Windows.Visibility.Visible;
                }

                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();

                ((ComboItem)dimCombo.Items[3]).Caption = "[Prestazioni].[Fascia Età]";

                switch (TabIndex)
                {
                    default:
                        break;
                    case 0:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Stato Domanda].[Stato Domanda]";
                        ((ComboItem)dimCombo.Items[0]).Content = "Stato Prestazione";
                        break;
                    case 2:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Prestazioni Liquidate].[Modalita Pagamento]";
                        ((ComboItem)dimCombo.Items[0]).Content = "Modalità di Liquidazione";
                        ((ComboItem)dimCombo.Items[3]).Caption = "[Prestazioni Liquidate].[Fascia Età]";
                        break;
                    case 3:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Prestazioni Liquidate].[Modalita Pagamento]";
                        ((ComboItem)dimCombo.Items[0]).Content = "Modalità di Liquidazione";
                        ((ComboItem)dimCombo.Items[3]).Caption = "[Prestazioni Liquidate].[Fascia Età]";
                        break;
                }

                Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                dimCombo.SelectedIndex = 0;

            

                CaricaDateAggiornamentoDati();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void dimSelectionChanged(object sender, SelectionChangedEventArgs changedEventArgs)
        {
            if (dimCombo.SelectedItem != null)
            {
                Dimensione = ((ComboItem) (dimCombo.SelectedItem)).Caption;
                if (!worker.IsBusy)
                {
                    InizializzazioneInCorso = true;
                    busyIndicator.IsBusy = true;
                    worker.RunWorkerAsync();
                    LoadDimensionMembers();
                    LoadDataSelectedYear();
                }
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            //Il ToggleButton per la griglia è nel ExamplePage_SL.xaml

            //Common.CreaTabella(CodeViewer, CellsetArray);
            Common.Common.CreaGridView(CodeViewer, CellsetArray,null);

            base.OnHideExampleArea(e);
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            busyIndicator.IsBusy = false;
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion

        #region Print

        protected override void OnClickPrintButton(EventArgs e)
        {
            switch (TabIndex)
            {
                case 0:

                    Common.Common.Print(Common.Common.Clone(RadChart1), "");
                    break;
                case 1:
                    Common.Common.Print(Common.Common.Clone(RadChart2d), "");
                    break;
                case 2:
                    Common.Common.Print(Common.Common.Clone(RadChart2), "");
                    break;
                case 3:
                    Common.Common.Print(Common.Common.Clone(RadChart1e), "");
                    break;
                case 4:
                    Common.Common.Print(Common.Common.Clone(RadChart3), "");
                    break;
            }
            base.OnClickPrintButton(e);
        }

        #endregion
    }
}