﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using TBridge.Cemi.Cruscotto.Template;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.Prestazioni.APE
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public ComboItem esitoPagamentoItem = new ComboItem();
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool InizializzazioneInCorso;
        public ICubeService m_Client;
        public ComboItem modalitaPagamentoItem = new ComboItem();
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }
        public AdoMdHelperCellStone secondCellStone { get; set; }

        public string Members { set; get; }

        public string Anno { set; get; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            //string completePath = App.Current.Host.Source.Scheme
            //     + "://" + App.Current.Host.Source.Host
            //     + ":" + App.Current.Host.Source.Port + "/CubeService.svc";

            string completePath = Application.Current.Host.Source.Scheme
                                  + "://" + Application.Current.Host.Source.Host
                                  + ":" + Application.Current.Host.Source.Port + "/TBridge.Cemi.Cruscotto.Silverlight.Web/CubeService.svc";

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            //ChannelFactory<ICubeService> factory =
            //    new ChannelFactory<ICubeService>("BasicHttpBinding_ICubeService");
            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 4)
            {
                Anno = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto",
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            Mese = months[NumMese - 1];
            Misura = "[Measures].[Numero Prestazioni],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]";

            Members = "Member [Tipi Prestazione].[Tipo Prestazione].[OldAndNew] AS'Aggregate({";

            for (int i = 0; i < Common.Common.DictionaryCodici[CodicePrestazione].Count; i++)
                Members = String.Format("{0}[Tipi Prestazione].[Tipo Prestazione].&{1},", Members, Common.Common.DictionaryCodici[CodicePrestazione][i]);

            Members = String.Format("{0}{1})'", Members.Substring(0, Members.Length - 1), "}");

            Dimensione = "[Lavoratori].[Provenienza]";
            ((ComboItem)dimCombo.Items[0]).Caption = "[APE].[Fascia Età]";
            modalitaPagamentoItem.Caption = "[Prestazioni Liquidate].[Modalita Pagamento]";
            modalitaPagamentoItem.Content = "Modalità di Liquidazione";
            modalitaPagamentoItem.Margin = ((ComboItem) dimCombo.Items[0]).Margin;

            

            esitoPagamentoItem.Caption = "[Prestazioni Liquidate].[Descrizione Stato Assegno]";
            esitoPagamentoItem.Content = "Stato pagamento";
            esitoPagamentoItem.Margin = ((ComboItem) dimCombo.Items[0]).Margin;

            dimCombo.SelectedIndex = 1;
            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += dimSelectionChanged;

            SetSlider();

            TextBlockNetto.Visibility = System.Windows.Visibility.Collapsed;
            TextBlockImporto.Visibility = System.Windows.Visibility.Collapsed;

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            m_Client.BeginGetDataAggiornamento("[Measures].[Data aggiornamento domande]",
                                                "Prestazioni",
                                                OnEndGetDataAggiornamento,
                                                null);
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            "[Measures].[Lavoratori Interessati]", "APE",
                                            OnEndGetAllYearsSecondSeriesResult,
                                            null);
                    //m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                    //                        "[Measures].[Lavoratori Liquidati],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                    //                        "Prestazioni",
                    //                        OnEndGetAllYearsResult,
                    //                        null);
                    break;

                case 1:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                            "[Measures].[Lavoratori Liquidati],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
            }
        }

        private void LoadDataAllYearsSecondSeries()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    break;

                case 1:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "",
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAllYearsSecondSeriesResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedYear()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", Dimensione, "",
                                            String.Format("[Measures].[Lavoratori Interessati],[Time].[Anno].&[{0}]",
                                                          Anno), "APE",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "[Data Liquidazione].[Mese]",
                                            "[Measures].[Lavoratori Liquidati],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "[Data Liquidazione].[Mese]",
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedYearSecondSeries()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    //Non serve più
                    m_Client.BeginGetResult(Members,
                                            Dimensione, "",
                                            String.Format(
                                                "[Measures].[Lavoratori Liquidati],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew],[Data Liquidazione].[Anno].&[{0}]",
                                                Anno), "Prestazioni",
                                            OnEndGetSecondSeriesAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult(Members, "[Data Liquidazione].[Anno]", "[Data Liquidazione].[Mese]",
                                            "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]",
                                            "Prestazioni",
                                            OnEndGetSecondSeriesAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthly()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 1:
                    m_Client.BeginGetResult(Members,
                                            Dimensione, "",
                                            String.Format(
                                                "[Measures].[Lavoratori Liquidati],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew],[Data Liquidazione].[Anno].&[{0}],[Data Liquidazione].[Mese].&[{1}]",
                                                Anno, NumMese), "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;

                case 2:
                    m_Client.BeginGetResult(Members, string.Format("[Data Liquidazione].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                            String.Format(
                                                "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew],[Data Liquidazione].[Mese].&[{0}]",
                                                NumMese), "Prestazioni",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthlySecondSeries()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 1:
                    m_Client.BeginGetResult(Members,
                                            Dimensione, "",
                                            String.Format(
                                                "[Measures].[Importo Erogato],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew],[Data Liquidazione].[Anno].&[{0}],[Data Liquidazione].[Mese].&[{1}]",
                                                Anno, NumMese), "Prestazioni",
                                            OnEndGetMonthlySecondSeriesResult,
                                            null);
                    break;
            }
        }

        private void LoadDimensionMembers()
        {
            m_Client.BeginGetDimMembers(Dimensione, "Prestazioni",
                                        OnEndGetDimMembers,
                                        null);
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 1:
                                                   CellsetArray[0].Title = "Lavoratori Liquidati";
                                                   break;
                                           }
                                       });
            if(TabIndex!=0)
            {
                LoadDataAllYearsSecondSeries();
            }
            else
            {
                LoadDataSelectedYear();
            }
        }

        private void OnEndGetAllYearsSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(secondCellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   titoloLavoratoriPerAnno.Content =
                                                       String.Format("Lavoratori interessati nel {0}", Anno);
                                                   CellsetArray[0].Title = "Lavoratori interessati";
                                                   CellsetArray[1].Title = (string) titoloLavoratoriPerAnno.Content;

                                                   Common.Common.ConfigureAllYearsChart(chartLavoratoriInteressati, secondCellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   chartLavoratoriInteressati.DataSeries[0].LegendLabel =
                                                       "Lavoratori interessati";
                                                   
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureMultipleAxisChart(chartLavoratoriLiquidati, cellStone,
                                                                                     secondCellStone,
                                                                                     "#VAL{N0}");
                                                   CellsetArray[0].Title = "Lavoratori Liquidati";
                                                   try
                                                   {
                                                       chartLavoratoriLiquidati.DataSeries[0].LegendLabel =
                                                           "Lavoratori";
                                                       chartLavoratoriLiquidati.DataSeries[1].LegendLabel =
                                                           "Importi";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }
                                                   break;
                                           }
                                       });
            LoadDataSelectedYear();
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   titoloLavoratoriPerAnno.Content = String.Format("Dettaglio del {0}",
                                                                                                   Anno);
                                                   ConfigureBarChart(chartLavoratoriInteressatiPerAnno);
                                                   break;
                                               case 1:
                                                   titoloLavoratoriLiquidati.Content =
                                                       String.Format("Lavoratori liquidati nel {0}",
                                                                     Anno);
                                                   CellsetArray[1].Title = (string) titoloLavoratoriLiquidati.Content;
                                                   break;
                                           }
                                       });
            if(TabIndex!=0)
            {
                LoadDataSelectedYearSecondSeries();
            }
            else
            {
                InizializzazioneCompletata();
            }
        }

        private void OnEndGetSecondSeriesAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(secondCellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureDoubleBarChart(chartLavoratoriInteressatiPerAnno,
                                                                                  cellStone, secondCellStone, "#VAL{N0}");
                                                   titoloLavoratoriPerAnno.Content = String.Format("Dettaglio del {0}",
                                                                                                   Anno);

                                                   InizializzazioneCompletata();
                                                   break;
                                               case 1:
                                                   titoloLavoratoriLiquidati.Content =
                                                       String.Format("Lavoratori liquidati nel {0}",
                                                                     Anno);
                                                   CellsetArray[1].Title = (string) titoloLavoratoriLiquidati.Content;
                                                   Common.Common.ConfigureMonthDoubleSeriesChart(
                                                       chartLavoratoriLiquidatiPerMese,
                                                       cellStone, secondCellStone, "#VAL{N0}", Anno, monthNum, "Importi");
                                                   try
                                                   {
                                                       chartLavoratoriLiquidatiPerMese.DataSeries[0].LegendLabel =
                                                           "Lavoratori liquidati";
                                                       chartLavoratoriLiquidatiPerMese.DataSeries[1].LegendLabel =
                                                           "Importi liquidati";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }
                                                   break;
                                           }
                                       });
            LoadDataMonthly();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 1:
                                                   titoloDettaglioLiquidati.Content =
                                                       String.Format("Dettaglio di {0} {1}", Mese, Anno);
                                                   CellsetArray[2].Title = titoloDettaglioLiquidati.Content.ToString();
                                                   break;
                                           }
                                       });
            LoadDataMonthlySecondSeries();
        }

        private void OnEndGetMonthlySecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(secondCellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureDoubleBarMultipleAxisChart(chartDettaglioLavoratori,
                                                                                              cellStone, secondCellStone,
                                                                                              "#VAL{N0}");
                                                   try
                                                   {
                                                       chartDettaglioLavoratori.DataSeries[0].LegendLabel =
                                                           "Lavoratori liquidati";
                                                       chartDettaglioLavoratori.DataSeries[1].LegendLabel =
                                                           "Importi liquidati";
                                                   }
                                                   catch (Exception)
                                                   {
                                                   }
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        private void OnEndGetDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                                   //case 0:
                                                   //    ConfigureDettaglioChart(chartMonthDetailGen, DetailsTitle); break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        private void OnEndGetDimMembers(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate { DimensionMembers = m_Client.EndGetDimMembers(asyncResult); });
        }

        #endregion

        private void ConfigureBarChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisX.AutoRange = true;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            try
            {
                if (cellStone.Rows[0].Columns.Count > 1)
                {
                    DataSeries splineAreaSeries = Common.Common.GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption,
                                                                   1);

                    splineAreaSeries.Definition = new BarSeriesDefinition();
                    splineAreaSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;

                    Common.Common.SetSeriesLabel(splineAreaSeries);

                    DataSeries temporanea = new DataSeries();
                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        if (!splineAreaSeries[i].LegendLabel.StartsWith("0"))
                        {
                            temporanea.Add(splineAreaSeries[i]);
                        }
                    splineAreaSeries = temporanea;
                    splineAreaSeries.Definition.ShowItemLabels = false;
                    splineAreaSeries.Definition.ShowItemToolTips = true;

                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        splineAreaSeries[i].XValue = i;

                    chartArea.DataSeries.Add(splineAreaSeries);
                    chartArea.AxisX.TickPoints.Clear();
                    for (int i = 0; i < splineAreaSeries.Count; i++)
                        chartArea.AxisX.TickPoints.Add(new TickPoint
                                                           {
                                                               Value = i,
                                                               Label = splineAreaSeries[i].LegendLabel.Split(':')[0]
                                                           });

                    Common.Common.SetScala(chartArea, "#VAL{N0}");
                }
            }
            catch
            {
            }
            }

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {if(e.DataSeries.LegendLabel!=null)
                tooltip.Content = string.Format(e.DataSeries.LegendLabel.Equals("Importi liquidati") ? "{0}\r\n{1:c0}" : "{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel.Substring(0,
                                                                                                                                                                                     e.DataPoint.LegendLabel.
                                                                                                                                                                                         LastIndexOf(
                                                                                                                                                                                             ":"))), e.DataPoint.YValue);
            else
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel.Substring(0,
                                                                                                                                                                                        e.DataPoint.LegendLabel.
                                                                                                                                                                                            LastIndexOf(
                                                                                                                                                                                                ":"))), e.DataPoint.YValue);
               
            }
            }
        }

        private void MonthItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                if (e.DataSeries.LegendLabel != null)
                    tooltip.Content = string.Format(e.DataSeries.LegendLabel.Equals("Importi liquidati") ? "{0}\r{1:c0}" : "{0}\r{1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1], e.DataPoint.YValue);
                else
                {
                    tooltip.Content = string.Format("{0}\rNumero: {1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1], e.DataPoint.YValue);
                }
            }
        }

        private void ChartItemYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                switch (TabIndex)
                {
                    default:
                        LoadDataSelectedYear();
                        break;
                }
            }
        }

        private void MonthGenItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto",
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            NumMese = Int32.Parse(e.DataPoint.LegendLabel);
            Mese = months[Int32.Parse(e.DataPoint.LegendLabel) - 1];
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataMonthly();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                TabIndex = mStrip.SelectedIndex;
                if(TabIndex!=1)
                {
                    TextBlockNetto.Visibility = System.Windows.Visibility.Collapsed;
                    TextBlockImporto.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {

                    TextBlockNetto.Visibility = System.Windows.Visibility.Visible;
                    TextBlockImporto.Visibility = System.Windows.Visibility.Visible;
                }
                worker.RunWorkerAsync();

                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();

                filterContainer.Visibility = Visibility.Visible;
                if (dimCombo.Items.Contains(esitoPagamentoItem))
                    dimCombo.Items.Remove(esitoPagamentoItem);
                if (dimCombo.Items.Contains(modalitaPagamentoItem))
                    dimCombo.Items.Remove(modalitaPagamentoItem);
                switch (TabIndex)
                {
                    default:
                        filterContainer.Visibility = Visibility.Collapsed;
                        break;
                    case 0:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[APE].[Fascia Età]";
                        ((ComboItem)dimCombo.Items[0]).Content = "Fascia d'eta";
                        break;
                    case 1:
                        ((ComboItem)dimCombo.Items[0]).Caption = "[Prestazioni Liquidate].[Fascia Età]";
                        ((ComboItem)dimCombo.Items[0]).Content = "Fascia d'età";
                        dimCombo.Items.Add(esitoPagamentoItem);
                        dimCombo.Items.Add(modalitaPagamentoItem);
                        break;
                    case 2:
                        //((MyRadComboBoxItem) dimCombo.Items[0]).Caption = "[Prestazioni Liquidate].[Fascia Età]";
                        //((MyRadComboBoxItem)dimCombo.Items[0]).Content = "Modalità di Liquidazione";
                        dimCombo.Items.Add(esitoPagamentoItem);
                        dimCombo.Items.Add(modalitaPagamentoItem);
                        break;
                }
                if (dimCombo.SelectedItem != null)
                    Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                else
                {
                    Dimensione = ((ComboItem)dimCombo.Items[0]).Caption;
                }
                dimCombo.SelectedIndex = 0;



                CaricaDateAggiornamentoDati();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void dimSelectionChanged(object sender, SelectionChangedEventArgs changedEventArgs)
        {
            if (dimCombo.SelectedItem != null)
            {
                Dimensione = ((ComboItem) (dimCombo.SelectedItem)).Caption;
                //ConfigureCharts();
                if (!worker.IsBusy)
                {
                    InizializzazioneInCorso = true;
                    busyIndicator.IsBusy = true;
                    worker.RunWorkerAsync();
                    TabIndex = mStrip.SelectedIndex;
                    LoadDimensionMembers();
                    switch (TabIndex)
                    {
                        default:
                            LoadDataSelectedYear();
                            break;
                        case 1:
                            LoadDataMonthly();
                            break;
                    }
                }
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string) ((ComboItem) dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            
            busyIndicator.IsBusy = false;
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            //String filtro = null;
            //if (filterContainer.Visibility == Visibility.Visible)
            //    filtro = (string) ((ComboItem) dimCombo.SelectedItem).Content;
            //Common.Common.ExpotToExcel(CellsetArray, filtro);
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion
    }
}