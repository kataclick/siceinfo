﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.Prestazioni.Colonie
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool InizializzazioneInCorso;
        public ICubeService m_Client;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }

        public string Members { set; get; }

        public string Anno { set; get; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public string CassaEdile { get; set; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
           
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            dimCombo.SelectedIndex = 0;
            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += dimSelectionChanged;

            ((ComboItem)comboBoxCassaEdile.Items[1]).Caption = "[Colonie Domande].[Cassa Edile].&[Milano]";
            ((ComboItem)comboBoxCassaEdile.Items[2]).Caption = "[Colonie Domande].[Cassa Edile].&[Altro]";
            comboBoxCassaEdile.SelectedIndex = 0;
            comboBoxCassaEdile.SelectionChanged += comboBoxCassaEdile_SelectionChanged;
            Dimensione = ((ComboItem)(dimCombo.SelectedItem)).Caption;
            CassaEdile = "[Colonie Domande].[Cassa Edile].[All]";

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
                m_Client.BeginGetDataAggiornamento("[Measures].[Data aggiornamento colonie]",
                                                "Colonie",
                                                OnEndGetDataAggiornamento,
                                                null);
           
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

       
        private void comboBoxCassaEdile_SelectionChanged(object sender, SelectionChangedEventArgs changedEventArgs)
        {
            CassaEdile = ((ComboItem) comboBoxCassaEdile.SelectedItem).Caption;
            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;

            
                    m_Client.BeginGetResult("", "[Data Domanda].[Anno]", "",
                                            String.Format("[Measures].[Numero di Domande],{0}", CassaEdile), "Colonie",
                                            OnEndGetAllYearsResult,
                                            null);
        }

        private void LoadDataSelectedYear()
        {
            m_Client.BeginGetResult("", String.Format("[Data Domanda].[Anno].&[{0}]",Anno), Dimensione,
                                    String.Format("[Measures].[Numero di Domande],{0}", CassaEdile), "Colonie",
                                            OnEndGetAnnualResult,
                                            null);
        }

        private void LoadDataMonthly()
        {
                    m_Client.BeginGetResult("", "[Casse Edili].[Id Cassa Edile]", "",
                                            String.Format(
                                                "[Measures].[Numero di Domande],[Data Domanda].[Anno].&[{0}]", Anno),
                                            "Colonie",
                                            OnEndGetMonthlyResult,
                                            null);
        }

        private void LoadDimensionMembers()
        {
            m_Client.BeginGetDimMembers(Dimensione, "Colonie",
                                        OnEndGetDimMembers,
                                        null);
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        private void ConfigureAnnualChart(ChartArea chartArea, ChartTitle title)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            title.Content = string.Format("Domande nel {0}", Anno);

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Domande annuali";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition)doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            doughnutSeries.Definition.ShowItemLabels = true;
            doughnutSeries.Definition.ShowItemToolTips = true;
            Common.Common.SetSeriesLabel(doughnutSeries);

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
        }

        private void ConfigureBarChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.LabelRotationAngle = 45;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption, 1);

            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255, 65, 246, 43));


            double maxValue = 0;

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            //chartArea.AxisY.Title = "";


            for (int i = 0; i < barSeries.Count; i++)
            {
                chartArea.AxisX.TickPoints[i].Value = barSeries[i].XValue;
                chartArea.AxisX.TickPoints[i].Label = barSeries[i].LegendLabel;
            }

            string format = "#VAL{N0}";
            if (TabIndex > 0) format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            Common.Common.SetScala(chartArea,format);
        }

        private void ConfigureDettaglioChart(ChartArea chartArea, ChartTitle title)
        {
            title.Content = String.Format("Dettaglio del {0}", Anno);

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Dettaglio annuale";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            Common.Common.SetSeriesLabel(doughnutSeries);
            doughnutSeries.Definition.ShowItemToolTips = true;
            chartArea.DataSeries.Clear();

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
            foreach (DataPoint item in chartArea.DataSeries[0])
            {
                if (item.YValue == 0)
                    item.Label = " ";
            }
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);

                                                   Common.Common.ConfigureAllYearsChart(chartDomandeRicevute, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Numero annuale di prestazioni";
                                                   CellsetArray[1].Title = (string) AnnualTitle.Content;
                                       });
            LoadDataSelectedYear();
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                               cellStone = m_Client.EndGetResult(asyncResult);
                                               CellsetArray[1].Content = Common.Common.ToString(cellStone);
                                               ConfigureAnnualChart(chartDomandeRicevutePerAnno, AnnualTitle);
                                               //Common.Common.ConfigureAllYearsChart(chartDomandeRicevutePerAnno, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                           AnnualTitle.Content = string.Format("Domande nel {0}", Anno);
                                       });
            LoadDataMonthly();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);

                                                   MonthlyTitle.Content = String.Format("Dettaglio del {0}", Anno);
                                                   CellsetArray[2].Title = "Numero mensile di prestazioni";
                                                   ConfigureBarChart(chartDomandeMensili);
                                       });
            InizializzazioneCompletata();
        }


        private void OnEndGetDimMembers(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate { DimensionMembers = m_Client.EndGetDimMembers(asyncResult); });
        }

        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}",
                                                (e.DataPoint.LegendLabel.Substring(0,
                                                                                   e.DataPoint.LegendLabel.LastIndexOf(
                                                                                       ":"))), e.DataPoint.YValue);
            }
        }

        public void CassaEdileTooltip(ItemToolTip2D toolTip, ItemToolTipEventArgs e)
        {
            toolTip.Content = String.Format("{0}: {1}", e.DataPoint.LegendLabel, e.DataPoint.YValue);
        }

        private void ChartItemYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataSelectedYear();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;

                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();


                filterContainer.Visibility = Visibility.Visible;

                dimCombo.SelectionChanged -= dimSelectionChanged;
                Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                dimCombo.SelectedIndex = 0;
                dimCombo.SelectionChanged += dimSelectionChanged;

                LoadDataAllYears();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void dimSelectionChanged(object sender, SelectionChangedEventArgs changedEventArgs)
        {
            if (dimCombo.SelectedItem != null)
            {
                Dimensione = ((ComboItem) (dimCombo.SelectedItem)).Caption;
                if (!worker.IsBusy)
                {
                    InizializzazioneInCorso = true;
                    busyIndicator.IsBusy = true;
                    worker.RunWorkerAsync();
                    LoadDimensionMembers();
                    LoadDataSelectedYear();
                }
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            busyIndicator.IsBusy = false;
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion
    }
}