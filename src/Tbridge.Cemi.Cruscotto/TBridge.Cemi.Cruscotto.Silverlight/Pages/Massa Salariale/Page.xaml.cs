﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.ServiceModel;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Media;
using System.Xml.Linq;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs=System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.MassaSalariale
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        public AdoMdHelperDataSeries excelSeries { get; set; }
        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool InizializzazioneInCorso;

        public ICubeService m_Client;

        public string maxYear;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }

        public AdoMdHelperCellStone secondCellStone { get; set; }


        public string Members { set; get; }

        public string Anno { set; get; }

        public string StatoImpresa { get; set; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;


            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month - 1;
            }
            else
            {
                Anno = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            for (int i = 2009; i <= DateTime.Today.Year; i++)
            {
                annoCombo.Items.Add(new ComboItem { Caption = i.ToString(), Content = i.ToString(), Margin = new Thickness(0, -5, 0, 0) });
            }

            annoCombo.SelectedIndex = annoCombo.Items.Count-2;

            dimCombo.SelectedIndex = 0;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;
            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;

            SetSlider();

            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;
            
            if(!worker.IsBusy)
            {
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure = String.Empty;
            switch (TabIndex)
            {
                case 1:
                    measure = "[Measures].[Data Aggiornamento Ore Denunciate]";
                    break;
                case 3:
                    measure = "[Measures].[Data aggiornamento imprese con denuncie]";
                    break;
                case 2:
                    measure = "[Measures].[Data Aggiornamento lavoratori in denuncia]";
                    break;
            }
            if(measure!=String.Empty)
            m_Client.BeginGetDataAggiornamento(measure,
                                            "Massa Salariale",
                                            OnEndGetDataAggiornamento,
                                            null);
            else
            {
                LoadDataAllYears();
            }
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           string data = m_Client.EndGetDataAggiornamento(ar);
                                           DataAggiornamento.Text = String.Format("Dati aggiornati al {0}",data);
                                       });
            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider3.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider3.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider3.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void CaricaDaExcel()
        {
            if (annoCombo.SelectedItem != null)
            {
                Anno = ((ComboItem)annoCombo.SelectedItem).Caption;
                InizializzazioneInCorso = true;
                m_Client.BeginReadMassaSalarialeMensileFromExcel(String.Format(@"D:\Cruscotto\MassaSalariale{0}.xls", Anno), OnEndGetExcel, null);
            }
        }

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 1:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            String.Format("[Measures].[Ore Dichiarate],{0},[Tipi Ora].[Id Tipo Ora].&[001]", Dimensione),
                                            "Massa Salariale",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            String.Format("[Measures].[Numero Imprese con  Denunce],{0}", Dimensione), "Massa Salariale",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            String.Format("[Measures].[Numero lavoratori in denuncia],{0}", Dimensione), "Massa Salariale",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 0:
                    CaricaDaExcel();
                    break;
            }
        }

        private void LoadDataSelectedYear()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 1:
                    m_Client.BeginGetResult("", String.Format("[Time].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                            String.Format("[Measures].[Ore Dichiarate],[Tipi Ora].[Id Tipo Ora].&[001]"),
                                            "Massa Salariale",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult("", String.Format("[Time].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                           "[Measures].[Numero Imprese con  Denunce]", "Massa Salariale",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", String.Format("[Time].[Anno].&[{0}]", Anno),
                                             Dimensione,
                                            String.Format("[Measures].[Numero lavoratori in denuncia]"), "Massa Salariale",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedMonth()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 1:
                    m_Client.BeginGetResult("", String.Format("[Time].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                            String.Format(
                                                "[Measures].[Ore Dichiarate],[Tipi Ora].[Id Tipo Ora].&[001],[Time].[Mese].&[{0}]",
                                                NumMese), "Massa Salariale",
                                            OnEndGetMonthResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult("", String.Format("[Time].[Anno].&[{0}]", Anno),
                                            Dimensione,
                                            String.Format("[Measures].[Numero Imprese con  Denunce],[Time].[Mese].&[{0}]", NumMese
                                                          ), "Massa Salariale",
                                            OnEndGetMonthResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", String.Format("[Time].[Anno].&[{0}]", Anno),
                                           Dimensione,
                                            String.Format("[Measures].[Numero lavoratori in denuncia],[Time].[Mese].&[{0}]", NumMese
                                                          ), "Massa Salariale",
                                            OnEndGetMonthResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthly()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 1:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "[Time].[Mese]",
                                            String.Format("[Measures].[Ore Dichiarate],{0},[Tipi Ora].[Id Tipo Ora].&[001]", Dimensione),
                                            "Massa Salariale",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "[Time].[Mese]",
                                            String.Format("[Measures].[Numero Imprese con  Denunce],{0}", Dimensione), "Massa Salariale",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "[Time].[Mese]",
                                            String.Format("[Measures].[Numero lavoratori in denuncia],{0}", Dimensione), "Massa Salariale",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
            }
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        private void ConfigureBudgetChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(excelSeries);
            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.LegendLabel = "Denunciato a budget";
            //Mi becca un datap oint conn Improtorecuperatocrc, allora lo tolgo...verifica
            DataSeries temp = new DataSeries();
            double c;
            for (int i = 0; i < barSeries.Count; i++)
                if (!Double.TryParse(barSeries[i].LegendLabel, out c))
                    temp.Add(barSeries[i]);

            for (int i = 0; i < temp.Count; i++)
                barSeries.Remove(temp[i]);

            string[] months = new[] { "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" };
            //Common.Common.SortDataSeries(barSeries, monthNum);
            //for (int i = 0; i < barSeries.Count; i++)
            //{
            //    if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
            //        barSeries[i].XValue = int.Parse(barSeries[i].LegendLabel) - 1;
            //}
            
            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            double maxValue = 0;
            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

           
            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            //Common.Common.SetScala(chartArea);

            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}

            Common.Common.SetScala(chartArea, "#VAL{N0}");
        }


        public void ConfigureAllYearsChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();
            DataSeries splineAreaSeries = Common.Common.GetSeries(excelSeries);
            
            splineAreaSeries.Definition = new SplineAreaSeriesDefinition();

            Common.Common.SetSeriesLabel(splineAreaSeries);

            //DataPoint point = new DataPoint(splineAreaSeries.Count,
            //                                   splineAreaSeries[splineAreaSeries.Count - 1].YValue) { LegendLabel = " ", XCategory = " " };
            //splineAreaSeries.Add(point);

            splineAreaSeries.Definition.ShowItemLabels = false;
            splineAreaSeries.Definition.ShowItemToolTips = true;
            splineAreaSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(127, 0, 198, 255));
            splineAreaSeries.Definition.Appearance.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 163, 210));
            splineAreaSeries.Definition.ItemToolTipFormat = "#XCAT\r\nNumero: #Y{n0}";

            chartArea.DataSeries.Add(splineAreaSeries);

            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = "#VAL{N0}";
            //}
            Common.Common.SetScala(chartArea, "#VAL{N0}");
        }
        
        #endregion

        #region OnEndGetResult

        private void OnEndGetExcel(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                try
                {

                excelSeries =
                    m_Client.EndReadMassaSalarialeMensileFromExcel(asyncResult);
                ConfigureBudgetChart(chartMassaSalarialePerMese);

                }
                catch (Exception)
                {
                    InizializzazioneCompletata();
                }
            });
            CaricaMassaAnnualeDaExcel();
            InizializzazioneCompletata();
        }

        private void OnEndGetMassaAnnualeExcel(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                excelSeries =
                    m_Client.EndReadMassaSalarialeAnnualeFromExcel(asyncResult);
                ConfigureAllYearsChart(chartMassaSalarialeAnnaule);
            });
            annoCombo.SelectionChanged -= OnSelectedIndexChanged;
            annoCombo.SelectionChanged += OnSelectedIndexChanged;
            InizializzazioneCompletata();
        }

        private void CaricaMassaAnnualeDaExcel()
        {
            m_Client.BeginReadMassaSalarialeAnnualeFromExcel(String.Format(@"D:\Cruscotto\MassaSalariale.xls"), OnEndGetMassaAnnualeExcel, null);
        }

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 1:
                                                   Common.Common.ConfigureAllYearsChart(chartOreLavorateAnnuali, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   titoloOreLavorateAnnuali.Content = String.Format("Ore lavorate nel {0}", Anno);
                                                   CellsetArray[0].Title = "Ore lavorate";
                                                   break;
                                               case 3:
                                                   titoloImpConDenunciaAnnuali.Content = String.Format("Imp. in denuncia nel {0}", Anno);
                                                   CellsetArray[0].Title = "Lavoratori in denuncia";
                                                   Common.Common.ConfigureAllYearsChart(chartAndamentoAnnualeImpConDenuncia, cellStone,
                                                                                 "#VAL{N0}", DateTime.Today.Year);
                                                   break;
                                               case 2:
                                                   Common.Common.ConfigureAllYearsChart(chartAndamentoAnnualeLavoratoriInDenuncia, cellStone, "#VAL{N0}", DateTime.Today.Year);
                                                   titoloLavoratoriInDenunciaAnnuali.Content =
                                                       String.Format("Lav. in denuncia nel {0}", Anno);
                                                   CellsetArray[0].Title = "Imprese con denuncia conf.";
                                                   break;
                                           }
                                       });
            LoadDataMonthly();
        }

        private void ConfigureAnnualChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Prestazioni annuali";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition)doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            doughnutSeries.Definition.ShowItemLabels = true;
            doughnutSeries.Definition.ShowItemToolTips = true;
            Common.Common.SetSeriesLabel(doughnutSeries);

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                               CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 1:
                                                   CellsetArray[1].Title = String.Format("Ore lavorate nel {0}", Anno);
                                                   ConfigureAnnualChart(chartOreLavoratePerAnno);
                                                   break;
                                               case 3:
                                                   CellsetArray[1].Title = String.Format("Lavoratori in denuncia nel {0}", Anno);
                                                   ConfigureAnnualChart(chartImpConDenunciaPerAnno);
                                                   break;
                                               case 2:
                                                   CellsetArray[1].Title = String.Format("Imp. con denuncia nel {0}", Anno);
                                                   ConfigureAnnualChart(chartLavoratoriInDenunciaPerAnno);
                                                   break;
                                           }
                                       });
                LoadDataSelectedMonth();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);

                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 1:
                                                   Common.Common.ConfigureMonthlyChart(chartOreLavorateMensili, cellStone, "#VAL{N0}",
                                                                              Anno, monthNum);
                                                   CellsetArray[2].Title = "Ore lavorate mensili";
                                                   break;
                                               case 3:
                                                   Common.Common.ConfigureMonthlyChart(chartAndamentoMensileImpConDenuncia, cellStone,
                                                                                "#VAL{N0}", Anno, monthNum);
                                                   CellsetArray[2].Title = "Lavoratori in denuncia mensili";
                                                   break;
                                               case 2:
                                                   Common.Common.ConfigureMonthlyChart(chartAndamentoMensileLavoratoriInDenuncia, cellStone,
                                                                                "#VAL{N0}", Anno, monthNum);
                                                   CellsetArray[2].Title = "Imprese con denuncia conf. mensili";
                                                   break;
                                           }
                                       });
                LoadDataSelectedYear();
        }

        private void OnEndGetMonthResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           string[] months = new[]
                                                                 {
                                                                     "Gennaio", "Febbraio", "Marzo", "Aprile",
                                                                     "Maggio", "Giugno", "Luglio",
                                                                     "Agosto", "Settembre", "Ottobre", "Novembre",
                                                                     "Dicembre"
                                                                 };

                                           switch (TabIndex)
                                           {
                                               case 1:
                                                   titoloOreLavorateMensili.Content = String.Format("Dettaglio di {0}",
                                                                                                months[
                                                                                                    NumMese -
                                                                                                    1]);
                                                   CellsetArray[3].Title =
                                                       titoloOreLavorateMensili.Content.ToString();
                                                   ConfigureAnnualChart(chartOreLavoratePerMese);
                                                   break;
                                               case 3:
                                                   titoloImpConDenunciaMensili.Content = String.Format("Dettaglio di {0}",
                                                                                                months[
                                                                                                    NumMese -
                                                                                                    1]);
                                                   CellsetArray[3].Title = titoloLavoratoriInDenunciaMensili.Content.ToString();
                                                   ConfigureAnnualChart(chartImpConDenunciaPerMese);
                                                   break;
                                               case 2:
                                                   titoloLavoratoriInDenunciaMensili.Content =
                                                      String.Format("Dettaglio di {0}", months[NumMese - 1]);
                                                   CellsetArray[3].Title =
                                                       titoloImpConDenunciaMensili.Content.ToString();
                                                   ConfigureAnnualChart(chartLavoratoriInDenunciaPerMese);
                                                   break;
                                           }
                                       });
        InizializzazioneCompletata();
        }

        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel.Substring(0,
                                                                                                                                            e.DataPoint.LegendLabel.LastIndexOf(
                                                                                                                                                ":"))), e.DataPoint.YValue);
            }
        }


        private void ItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel), e.DataPoint.YValue);
            }
        }

        private void MonthItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            
                tooltip.Content = string.Format("{0}\rNumero: {1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                            e.DataPoint.YValue);
            
        }

        private void ChartItemMeseClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            NumMese = Int32.Parse(e.DataPoint.LegendLabel.Split(':')[0]);
            
            
                busyIndicator.IsBusy = true;
                InizializzazioneInCorso = true;
                worker.RunWorkerAsync();
                LoadDataSelectedMonth();
            }
        }

        private void ChartItemYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
            Anno = e.DataPoint.LegendLabel.Split(':')[0];

            switch (TabIndex)
            {
                case 1:
                    titoloOreLavorateAnnuali.Content = String.Format("Ore lavorate nel {0}", Anno);
                    break;
                case 3:
                    titoloImpConDenunciaAnnuali.Content = String.Format("Imp. in denuncia nel {0}", Anno);
                    break;
                case 2:
                    titoloLavoratoriInDenunciaAnnuali.Content =
                        String.Format("Lav. in denuncia nel {0}", Anno);
                    break;
            }
            
            
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataMonthly();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            Common.Common.InitializeRadChart(RadChart2);
            Common.Common.InitializeRadChart(RadChart3);

            if(!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                TabIndex = mStrip.SelectedIndex;
                worker.RunWorkerAsync();
                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();
                CaricaDateAggiornamentoDati();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;   
            }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            busyIndicator.IsBusy = false;
        }


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataAllYears();
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n = 0;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion

        private void OnSelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDaExcel();
            }
        }
    }
}