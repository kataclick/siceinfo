﻿using System.ComponentModel;

namespace TBridge.Cemi.Cruscotto.Silverlight.Entities
{
    public class Data
    {
        private double volume;

        public Data(double volume, string category)
        {
            Volume = volume;
            XCategory = category;
        }

        public double Volume
        {
            get { return volume; }
            set
            {
                volume = value;
                OnPropertyChanged("Volume");
            }
        }

        public string XCategory { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}