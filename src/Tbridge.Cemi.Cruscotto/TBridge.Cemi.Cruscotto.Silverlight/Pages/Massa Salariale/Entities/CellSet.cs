﻿namespace TBridge.Cemi.Cruscotto.Silverlight.Entities
{
    public class CellSet
    {
        public string Content { get; set; }
        public string Title { get; set; }
    }
}