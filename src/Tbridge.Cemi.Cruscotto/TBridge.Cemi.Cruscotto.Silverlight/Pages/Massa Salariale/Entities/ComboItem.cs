﻿using System.Windows.Controls;

namespace TBridge.Cemi.Cruscotto.Silverlight.Entities
{
    public class ComboItem : ComboBoxItem
    {
        public string Caption { get; set; }
    }
}