﻿using System;
using System.ComponentModel;
using System.Windows.Media;

namespace TBridge.Cemi.Cruscotto.Silverlight.Entities
{
    public class DataViewModel : INotifyPropertyChanged
    {
        public DataViewModel(Data data)
        {
            Data = data;
            data.PropertyChanged += HandleSalesDataPropertyChanged;
        }

        public Data Data { get; private set; }

        public Brush SalesVolumeColor { get; private set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void HandleSalesDataPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //if (e.PropertyName == "Volume")
            //    this.UpdateSalesVolumeColor();
        }


        private LinearGradientBrush CreateGradientBrush(string color)
        {
            return
                new LinearGradientBrush(
                    new GradientStopCollection
                        {
                            new GradientStop {Color = GetColorFromHexString("#fffff8dc"), Offset = 1},
                            new GradientStop {Color = GetColorFromHexString(color), Offset = 0}
                        }, 90);
        }

        private Color GetColorFromHexString(string s)
        {
            s = s.Replace("#", string.Empty);

            byte a = Convert.ToByte(s.Substring(0, 2), 16);
            byte r = Convert.ToByte(s.Substring(2, 2), 16);
            byte g = Convert.ToByte(s.Substring(4, 2), 16);
            byte b = Convert.ToByte(s.Substring(6, 2), 16);
            return Color.FromArgb(a, r, g, b);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}