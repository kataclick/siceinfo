﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

//Ricordare che da qualche parte c'è Prestazioni nel quickStart

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.CRC
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        private bool initialize;
        public bool InizializzazioneInCorso;
        public ICubeService m_Client;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }

        public AdoMdHelperCellStone secondCellStone { get; set; }

        public AdoMdHelperCellStone thirdCellStone { get; set; }

        public AdoMdHelperCellStone fourthCellStone { get; set; }

        public AdoMdHelperDataSeries excelSeries { get; set; }

        public Brush SeriesColor { get; set; }

        public string Anno { set; get; }

        public string AnnoCassa { set; get; }

        public string AnnoCompetenza { set; get; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = DateTime.Today.Year.ToString();
                AnnoCassa = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = (DateTime.Today.Year - 1).ToString();
                AnnoCassa = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto",
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            Mese = months[NumMese - 1];
            Misura = "[Measures].[Numero Prestazioni],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]";

            ((ComboItem) dimCombo.Items[0]).Caption = "[Imprese].[Tipo Impresa].[All]";
            ((ComboItem) dimCombo.Items[1]).Caption = "[Imprese].[Tipo Impresa].&[Artigiana]";
            ((ComboItem) dimCombo.Items[2]).Caption = "[Imprese].[Tipo Impresa].&[Industria]";
            ((ComboItem) dimCombo.Items[3]).Caption = "[Imprese].[Tipo Impresa].&[Cooperativa]";
            for (int i = 2009; i <= DateTime.Today.Year;i++ )
            {
                annoCombo.Items.Add(new ComboItem { Caption = i.ToString(), Content = i.ToString(), Margin = new Thickness(0,-5,0,0) });
            }
            annoCombo.SelectedIndex = 0;
            dimCombo.SelectedIndex = 0;
            
            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += dimSelectionChanged;

            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;

            SetSlider();

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure = String.Empty;
            switch (TabIndex)
            {
                case 0:
                    measure = "[Measures].[Data aggiornamento dovuto-versato]";
                    break;
                case 1:
                    measure = "[Measures].[Data aggiornamento bollettinifreccia]";
                    break;
                case 2:
                    measure = "[Measures].[Data aggiornamento bollettinifreccia]";
                    break;
                case 3:
                    measure = "[Measures].[Data aggiornamento dovuto-versato]";
                    break;
            }
            if (measure != String.Empty)
                m_Client.BeginGetDataAggiornamento(measure,
                                                "SICE",
                                                OnEndGetDataAggiornamento,
                                                null);
            else
            {
                LoadDataAllYears();
            }
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }
        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider3.ValueChanged -= Slider_ValueChanged;
            Slider4.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider3.Value = Common.Common.Delay;
            Slider4.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider3.ValueChanged += Slider_ValueChanged;
            Slider4.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
        }


        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;
            initialize = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                            String.Format("[Measures].[Debito Residuo],{0}", Dimensione),
                                            "SICE",
                                            OnEndGetSecondSeriesResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                            String.Format("[Measures].[Importo Dovuto - Importi Dovuti CRC],{0}",
                                                          Dimensione), "SICE",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Data Valuta].[Anno]", "",
                                            String.Format("[Measures].[Importo Recuperato],{0}", Dimensione), "SICE",
                                            OnEndGetSecondSeriesResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                            String.Format("[Measures].[RapportoPercentualeDovutoVersato],{0}",
                                                          Dimensione), "SICE",
                                            OnEndGetSecondSeriesResult,
                                            null);
                    break;

                default:
                    break;
            }
        }

        private void LoadDataSecondSeriesAllYear()
        {
            InizializzazioneInCorso = true;

            switch (TabIndex)
            {
                case 0:
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                            String.Format("[Measures].[Importo Recuperato],{0}", Dimensione), "SICE",
                                            OnEndGetSecondSeriesResult,
                                            null);
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }

        private void LoadDataYearDetails()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Imprese].[Tipo Impresa]", "",
                                            String.Format(
                                                "[Measures].[Debito Residuo],[Data Competenza].[Anno].&[{0}]",
                                                Anno),
                                            "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Imprese].[Tipo Impresa]", "",
                                            String.Format(
                                                "[Measures].[Importo Dovuto - Importi Dovuti CRC],[Data Competenza].[Anno].&[{0}]",
                                                Anno),
                                            "SICE",
                                            OnEndGetDettaglioResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Data Valuta].[Anno]",
                                            "[Data Valuta].[Mese]",
                                            String.Format("[Measures].[Importo Recuperato],{0}", Dimensione), "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);
                    break;
                case 3:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                            "[Data Competenza].[Mese]",
                                            String.Format("[Measures].[RapportoPercentualeDovutoVersato],{0}",
                                                          Dimensione), "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);
                    break;
                case 4:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                            "[Data Competenza].[Mese]",
                                            String.Format("[Measures].[Importo Debito Versato],{0}", Dimensione),
                                            "SICE",
                                            OnEndGetDettaglioResult,
                                            null);
                    break;
                default:
                    break;
            }
        }

        private void LoadDataBudgetAnnual()
        {
            m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                    "[Data Competenza].[Mese]",
                                            "[Measures].[Importo Debito Versato]",
                                            "SICE",
                                            OnEndGetAnnualBudgetResult,
                                            null);
        }

        private void LoadDataBudgetAnnualSeconSeries()
        {
            m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                    "[Data Competenza].[Mese]",
                                           "[Measures].[Importo Debito Denunciato]",
                                           "SICE",
                                           OnEndGetSecondSeriesAnnualBudgetResult,
                                           null);
        }

        private void OnEndGetSecondSeriesAnnualBudgetResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                fourthCellStone =
                    m_Client.EndGetResult(asyncResult);
                ConfigureBudgetChartAnnual(chartBudgetAnnual);
            });
            InizializzazioneCompletata();
        }

        private void OnEndGetAnnualBudgetResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                thirdCellStone =
                    m_Client.EndGetResult(asyncResult);
            });
            LoadDataBudgetAnnualSeconSeries();
        }

        private void LoadDataYearSecondSeriesDetails()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    //m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                    //                        "[Data Competenza].[Mese]",
                    //                        String.Format("[Measures].[Importo Debito Versato],{0}", Dimensione), "SICE",
                    //                        OnEndGetSecondSeriesDettaglioResult,
                    //                        null);
                    m_Client.BeginGetResult("", "[Imprese].[Tipo Impresa]", "",
                                            String.Format(
                                                "[Measures].[Importo Debito Versato],[Data Competenza].[Anno].&[{0}]",
                                                Anno),
                                            "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);

                    break;
                case 1:
                    //m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                    //                        "[Data Competenza].[Mese]",
                    //                        String.Format("[Measures].[Importo Recuperato],{0}", Dimensione), "SICE",
                    //                        OnEndGetSecondSeriesDettaglioResult,
                    //                        null);
                    m_Client.BeginGetResult("", "[Imprese].[Tipo Impresa]", "",
                                            String.Format(
                                                "[Measures].[Importo Recuperato],[Data Competenza].[Anno].&[{0}]",
                                                Anno),
                                            "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);
                    break;
                case 4:
                    m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                            "[Data Competenza].[Mese]",
                                            String.Format("[Measures].[Importo Debito Denunciato],{0}", Dimensione),
                                            "SICE",
                                            OnEndGetSecondSeriesDettaglioResult,
                                            null);
                    break;
                default:
                    break;
            }
        }

        private void LoadataRecuperatoPerCassa()
        {
            InizializzazioneInCorso = true;
            if (initialize)
            {
                switch (TabIndex)
                {
                    case 2:

                        m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                                "",
                                                String.Format(
                                                    "[Measures].[Importo Recuperato],[Data Valuta].[Anno].&[{0}],[Data Valuta].[Mese].&[{1}],{2}",
                                                    Anno, NumMese, Dimensione), "SICE",
                                                OnEndGetDataRecuperatoPerCassa,
                                                null);
                        break;
                    case 3:
                        m_Client.BeginGetResult("", "[Data Competenza].[Anno]", "",
                                                String.Format("[Measures].[RapportoPercentualeDovutoRecuperato],{0}",
                                                              Dimensione), "SICE",
                                                OnEndGetDataRecuperatoPerCassa,
                                                null);
                        break;
                    case 4:
                        LoadDataBudgetAnnual();
                        break;
                }
            }
            else
            {
                InizializzazioneCompletata();
            }
        }

        private void LoadDataPercentualiMensili()
        {
            InizializzazioneInCorso = true;
            m_Client.BeginGetResult("", "[Data Competenza].[Anno]",
                                    "[Data Competenza].[Mese]",
                                    String.Format("[Measures].[RapportoPercentualeDovutoRecuperato],{0}", Dimensione),
                                    "SICE",
                                    OnEndGetDataRecuperatoPerCompetenza,
                                    null);
        }

        private void CaricamentoDatiBudget()
        {
            if (annoCombo.SelectedItem != null)
            {
                Anno = ((ComboItem) annoCombo.SelectedItem).Caption;
                InizializzazioneInCorso = true;
                m_Client.BeginReadFromExcel(String.Format(@"D:\Cruscotto\Budget{0}.xls", Anno), OnEndGetExcel, null);
            }
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        private void LoadDataPerDimensioneImpresa()
        {
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Imprese].[Id Dimensione]", "",
                                            String.Format(
                                                "[Measures].[Debito Residuo],{0},[Data Competenza].[Anno].&[{1}]",
                                                Dimensione, Anno),
                                            "SICE",
                                            OnEndGetPerDimensioneSecondSeries,
                                            null);
                    break;

                case 1:
                    m_Client.BeginGetResult("", "[Imprese].[Id Dimensione]", "",
                                            String.Format(
                                                "[Measures].[Importo Dovuto - Importi Dovuti CRC],{0},[Data Competenza].[Anno].&[{1}]",
                                                Dimensione, Anno),
                                            "SICE",
                                            OnEndGetPerDimensione,
                                            null);
                    break;
            }
        }

        private void LoadDataPerDimensioneSecondSeries()
        {
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Imprese].[Id Dimensione]", "",
                                            String.Format(
                                                "[Measures].[Importo Debito Versato],{0},[Data Competenza].[Anno].&[{1}]",
                                                Dimensione, Anno),
                                            "SICE",
                                            OnEndGetPerDimensioneSecondSeries,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Imprese].[Id Dimensione]", "",
                                            String.Format(
                                                "[Measures].[Importo Recuperato],{0},[Data Competenza].[Anno].&[{1}]",
                                                Dimensione, Anno),
                                            "SICE",
                                            OnEndGetPerDimensioneSecondSeries,
                                            null);
                    break;
            }
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonthChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, AnnoCompetenza, 0);
            barSeries.Definition = new BarSeriesDefinition {ShowItemLabels = false, ShowItemToolTips = true};
            barSeries.Definition.Appearance.Fill = SeriesColor;
            barSeries.LegendLabel = Anno;
            Common.Common.SortDataSeries(barSeries, monthNum);
            string[] months = new[] { "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" };

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            double maxValue = 0;
            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            Common.Common.SetScala(chartArea, "#VAL{C0}");

            //string format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
        }

        private void ConfigureMonthChartCassa(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, AnnoCassa, 0);
            barSeries.Definition = new BarSeriesDefinition { ShowItemLabels = false, ShowItemToolTips = true };
            barSeries.Definition.Appearance.Fill = SeriesColor;
            barSeries.LegendLabel = Anno;
            Common.Common.SortDataSeries(barSeries, monthNum);
            string[] months = new[] { "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" };

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            double maxValue = 0;
            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            //chartArea.AxisY.Title = "";

            Common.Common.SetScala(chartArea, "#VAL{C0}");

            
            //string format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
        }

        private void ConfigureBudgetChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(excelSeries);
            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.LegendLabel = "Denunciato a budget";
            
            DataSeries temp = new DataSeries();
            double c;
            for (int i = 0; i < barSeries.Count; i++)
                if (!Double.TryParse(barSeries[i].LegendLabel, out c))
                    temp.Add(barSeries[i]);

            for (int i = 0; i < temp.Count; i++)
                barSeries.Remove(temp[i]);

            string[] months = new[] { "Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic" };
            //Common.Common.SortDataSeries(barSeries, monthNum);
            //for (int i = 0; i < barSeries.Count; i++)
            //{
            //    if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
            //        barSeries[i].XValue = int.Parse(barSeries[i].LegendLabel) - 1;
            //}

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            double maxValue = 0;
            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            barSeries = Common.Common.GetSeries(secondCellStone, Anno, 0);
            barSeries.Definition = new BarSeriesDefinition {ShowItemLabels = false, ShowItemToolTips = true};
            barSeries.LegendLabel = "Denunciato";
            //Common.Common.SortDataSeries(barSeries, monthNum);
            

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            
            barSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            barSeries.Definition = new BarSeriesDefinition { ShowItemLabels = false, ShowItemToolTips = true };
            barSeries.LegendLabel = "Versato";
            //Common.Common.SortDataSeries(barSeries, monthNum);
            

            foreach (DataPoint point in barSeries)
            {
                point.XCategory = months[Convert.ToInt32(point.XCategory) - 1];
            }

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            
            //Common.Common.SetScala(chartArea);

            //string format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            Common.Common.SetScala(chartArea, "#VAL{C0}");
        }

        private void ConfigureBudgetChartAnnual(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            int lastMonth;
            DataSeries barSeries = Common.Common.GetSeriesTotal(excelSeries,Anno,out lastMonth);
            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.LegendLabel = "Denunciato a budget";

            DataSeries temp = new DataSeries();
            double c;
            for (int i = 0; i < barSeries.Count; i++)
                if (!Double.TryParse(barSeries[i].LegendLabel, out c))
                    temp.Add(barSeries[i]);

            for (int i = 0; i < temp.Count; i++)
                barSeries.Remove(temp[i]);

            double maxValue = 0;
            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            barSeries = Common.Common.GetSeriesTotal(fourthCellStone, Anno, 1,lastMonth);
            barSeries.Definition = new BarSeriesDefinition { ShowItemLabels = false, ShowItemToolTips = true };
            barSeries.LegendLabel = "Denunciato";
            //Common.Common.SortDataSeries(barSeries, monthNum);


            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            barSeries = Common.Common.GetSeriesTotal(thirdCellStone, Anno, 1,lastMonth);
            barSeries.Definition = new BarSeriesDefinition { ShowItemLabels = false, ShowItemToolTips = true };
            barSeries.LegendLabel = "Versato";
            //Common.Common.SortDataSeries(barSeries, monthNum);

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            //Common.Common.SetScala(chartArea);

            //string format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            Common.Common.SetScala(chartArea, "#VAL{C0}");
        }

        private void ConfigureRecuperatoPerCassaChart(ChartArea chartArea)
        {
            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption, 1);
            doughnutSeries.LegendLabel = "Dettaglio mensile";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            Common.Common.SetSeriesLabel(doughnutSeries);
            doughnutSeries.Definition.ShowItemToolTips = true;
            chartArea.DataSeries.Clear();

            DataSeries temporanea = new DataSeries();
            temporanea.Definition = new DoughnutSeriesDefinition();
            for (int i = 0; i < doughnutSeries.Count; i++)
                if (!(doughnutSeries[i].LegendLabel.StartsWith("0") || doughnutSeries[i].YValue == 0 ||
                      Int32.Parse(doughnutSeries[i].LegendLabel.Substring(0, 4)) < /*DateTime.Today.Year*/
                      Int32.Parse(cellStone.Rows[0].Columns[cellStone.Rows[0].Columns.Count - 1].Caption) - 5 ||
                      Int32.Parse(doughnutSeries[i].LegendLabel.Substring(0, 4)) > /*DateTime.Today.Year*/
                      Int32.Parse(cellStone.Rows[0].Columns[cellStone.Rows[0].Columns.Count - 1].Caption)))
                    temporanea.Add(doughnutSeries[i]);

            doughnutSeries = temporanea;
            doughnutSeries.Definition.ShowItemLabels = false;
            if (doughnutSeries.Count > 0)
            {
                chartArea.DataSeries.Add(doughnutSeries);
                foreach (DataPoint item in chartArea.DataSeries[0])
                {
                    if (item.YValue == 0)
                        item.Label = " ";
                }
            }
        }

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                           CellsetArray[0].Title = "Importo Dovuto";
                                       });
            LoadDataSecondSeriesAllYear();
        }

        private void OnEndGetSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(secondCellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   CellsetArray[0].Title = "Delta Importo denunciato -Importo versato";
                                                   CellsetArray[0].Content = CellsetArray[1].Content;
                                                   CellsetArray[1].Content = null;
                                                   Common.Common.ConfigureBarChart(chartYearsGen, secondCellStone,DateTime.Today.Year, "c0");
                                                   chartYearsGen.DataSeries[0].LegendLabel = "Delta Denunciato - Versato";
                                                   break;
                                               case 1:
                                                   CellsetArray[1].Title = "Importo recuperato";
                                                   Common.Common.ConfigureDoubleSeriesChart(chartDaVersareYears, cellStone,
                                                                                     secondCellStone, "c0");
                                                   chartDaVersareYears.DataSeries[0].LegendLabel = "Dovuto";
                                                   chartDaVersareYears.DataSeries[1].LegendLabel = "Recuperato";
                                                   //ConfigureDoubleSeriesChart(chartDaVersareYears);
                                                   break;

                                               case 2:
                                                   cellStone = secondCellStone;
                                                   secondCellStone = null;
                                                   CellsetArray[0].Content = CellsetArray[1].Content;
                                                   CellsetArray[0].Title = String.Format("Recuperato per cassa");
                                                   RecuperatoAnnualeTitle.Content = String.Format("Recuperato per cassa nel {0}",Anno);
                                                   //ConfigureAllYearsChart(chartRecuperatoYears);
                                                   Common.Common.ConfigureAllYearsChart(chartRecuperatoYears, cellStone, "c0", 2009);
                                                   break;
                                               case 3:
                                                   cellStone = secondCellStone;
                                                   secondCellStone = null;
                                                   CellsetArray[0].Title = "Rapporto Denunciato/Versato";
                                                   titoloDovutoVersatoMensile.Content =
                                                       String.Format("Dettaglio del {0}", AnnoCassa);
                                                   CellsetArray[0].Content = CellsetArray[1].Content;
                                                   CellsetArray[1].Title = "Dettaglio mensile";
                                                   //ConfigureAllYearsChart(chartDovutoVersato);
                                                   Common.Common.ConfigureAllYearsChart(chartDovutoVersato, cellStone, "", DateTime.Today.Year,true);
                                                   break;
                                               default:
                                                   break;
                                           }
                                       });
            LoadDataYearDetails();
        }

        private void OnEndGetDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);
                                       });

            LoadDataYearSecondSeriesDetails();
        }

        private void OnEndGetSecondSeriesDettaglioResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(secondCellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   YearTitle.Content =
                                                       String.Format("Dettaglio del {0} per dim. impresa", Anno);
                                                   CellsetArray[1].Content = CellsetArray[3].Content;
                                                   CellsetArray[3].Content = null;
                                                   CellsetArray[1].Title = String.Format("Importi per Tipologia Contrattuale del {0}", Anno);
                                                   Common.Common.ConfigureBarChart(chartMonthDetailGen, secondCellStone, "c0");

                                                   MonthlyTitle.Content = String.Format("Importi per Tipologia Contrattuale del {0}", Anno);
                                                   break;
                                               case 1:
                                                   DaVersareMonthlyTitle.Content =
                                                       String.Format("Dettaglio mensile del {0}", Anno);
                                                   DaVersareYearTitle.Content =
                                                       String.Format("Dettaglio del {0} per dim. impresa", Anno);
                                                   Common.Common.ConfigureDoubleBarChart(chartDaVersareMonthly, cellStone,
                                                                                  secondCellStone, "c0");

                                                   //Common.ConfigureMonthDoubleSeriesChart(chartDaVersareMonthly,
                                                   //                                   cellStone, secondCellStone,
                                                   //                                   "c0", Anno, monthNum);
                                                   //ConfigureMonthDoubleSeriesChart(chartDaVersareMonthly, DaVersareMonthlyTitle);
                                                   break;
                                               case 2:
                                                   cellStone = secondCellStone;
                                                   secondCellStone = null;
                                                   CellsetArray[1].Content = CellsetArray[3].Content;
                                                   CellsetArray[3].Content = null;
                                                   DettaglioRecuperatoTitle.Content =
                                                       String.Format("Competenza nel {0}", AnnoCompetenza);
                                                   CellsetArray[1].Title = String.Format("Recuperato per cassa a {0}",
                                                                                         AnnoCassa);
                                                   Common.Common.ConfigureMonthlyChart(chartRecuperatoMonthly,
                                                                                      cellStone, "c0", AnnoCassa, monthNum);
                                                   //ConfigureMonthChartFirst(chartRecuperatoMonthly);
                                                   break;
                                               case 3:
                                                   cellStone = secondCellStone;
                                                   secondCellStone = null;
                                                   CellsetArray[1].Content = CellsetArray[3].Content;
                                                   Common.Common.ConfigureMonthlyChart(chartDovutoVersatoMonthly,
                                                                                       cellStone, "n0", AnnoCassa, monthNum,true);
                                                   //ConfigureMonthChartFirst(chartDovutoVersatoMonthly);
                                                   break;
                                               case 4:
                                                   ConfigureBudgetChart(chartBudget);
                                                   break;
                                           }
                                       });
            if (TabIndex > 1)
            {
                LoadataRecuperatoPerCassa();
            }
            else
            {
                LoadDataPerDimensioneImpresa();
            }
        }

        private void OnEndGetDataRecuperatoPerCassa(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 2:
                                                   RecuperatoMonthlyTitle.Content =
                                                       String.Format("Incassato a {0} {1}", Mese, AnnoCassa);
                                                   CellsetArray[2].Title = String.Format("Incassato a {0} {1}", Mese,
                                                                                         AnnoCassa);
                                                   ConfigureRecuperatoPerCassaChart(chartRecuperatoMonthlyDetails);
                                                   break;
                                               case 3:
                                                   CellsetArray[2].Title = "Rapporto Dovuto/Recuperato";
                                                   titoloDovutoRecuperatoMensile.Content =
                                                       String.Format("Dettaglio del {0}", AnnoCassa);
                                                   CellsetArray[3].Title =
                                                       (string) titoloDovutoRecuperatoMensile.Content;
                                                   //ConfigureAllYearsChart(chartDovutoRecuperato);
                                                   Common.Common.ConfigureAllYearsChart(chartDovutoRecuperato, cellStone, "", DateTime.Today.Year,true);
                                                   break;
                                           }
                                       });
            if (TabIndex == 3)
                LoadDataPercentualiMensili();
            else InizializzazioneCompletata();
        }

        private void OnEndGetDataRecuperatoPerCompetenza(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 2:
                                                   CellsetArray[3].Title = String.Format(
                                                       "Dettaglio competenza del {0}", AnnoCompetenza);
                                                   DettaglioRecuperatoTitle.Content =
                                                       String.Format("Competenza del {0}", AnnoCompetenza);
                                                   ConfigureMonthChart(chartRecuperatoMonthDetail);
                                                   break;
                                               case 3:
                                                   Common.Common.ConfigureMonthlyChart(chartDovutoRecuperatoMonthly,
                                                                                       cellStone, "n0", AnnoCassa,
                                                                                       monthNum,true);
                                                   CellsetArray[3].Title = "Dettaglio mensile";
                                                   
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        private void OnEndGetExcel(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           try
                                           {

                                           excelSeries =
                                               m_Client.EndReadFromExcel(asyncResult);

                                           }
                                           catch (Exception)
                                           {
                                                InizializzazioneCompletata();
                                           }
                                       });
            LoadDataYearDetails();
        }

        private void OnEndGetPerDimensione(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                       });
            LoadDataPerDimensioneSecondSeries();
        }

        private void OnEndGetPerDimensioneSecondSeries(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           switch (TabIndex)
                                           {
                                               default:
                                                   break;
                                               case 0:
                                                   CellsetArray[2].Title = String.Format("Dettaglio del {0} per dim. impresa", Anno);;
                                                   CellsetArray[2].Content = Common.Common.ToString(secondCellStone);
                                                   Common.Common.ConfigureBarChart(chartYearDetail, secondCellStone, "c0");
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureDoubleBarChart(chartDaVersareYearsDetail, cellStone,
                                                                                  secondCellStone, "c0");
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        #endregion

        #region Eventi Grafici

        private void ChartItemYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            AnnoCassa = e.DataPoint.LegendLabel.Split(':')[0];
                switch (TabIndex)
                {
                    default:
                        ConfigureMonthChartCassa(chartMonthDetailGen);
                        break;
                    case 1:
                    case 2:
                        Anno = AnnoCassa;
                        //DaVersareMonthlyTitle.Content = String.Format("Dettaglio mensile del {0}", Anno);
                        LoadDataYearDetails();
                        break;
                }
            }
        }

        private void ChartDebitoItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            initialize = false;
                LoadDataYearDetails();
            }
        }

        private void chartDovutoRecuperatoItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            AnnoCassa = e.DataPoint.LegendLabel.Split(':')[0];
            titoloDovutoRecuperatoMensile.Content = String.Format("Dettaglio del {0}", AnnoCassa);
                LoadDataPercentualiMensili();
            }
        }

        private void chartDovutoVersatoItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                AnnoCassa = e.DataPoint.LegendLabel.Split(':')[0];
                titoloDovutoVersatoMensile.Content = String.Format("Dettaglio del {0}", AnnoCassa);
                initialize = false;
                LoadDataYearDetails();
            }
        }

        private void MonthGenItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                string[] months = new[]
                                      {
                                          "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                          "Agosto",
                                          "Settembre", "Ottobre", "Novembre", "Dicembre"
                                      };
                NumMese = Int32.Parse(e.DataPoint.LegendLabel);
                Mese = months[Int32.Parse(e.DataPoint.LegendLabel) - 1];
                if (TabIndex > 1)
                {
                    LoadataRecuperatoPerCassa();
                }
            }
        }

        private void MonthImpItemClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                AnnoCompetenza = e.DataPoint.LegendLabel.Substring(0, 4);
                SeriesColor = e.DataSeries.Definition.Appearance.Fill;
                DettaglioRecuperatoTitle.Content =
                    String.Format("Competenza nel {0}", AnnoCompetenza);
                m_Client.BeginGetResult("", String.Format("[Data Competenza].[Anno].&[{0}]", AnnoCompetenza),
                                        "[Data Competenza].[Mese]",
                                        String.Format(
                                            "[Measures].[Importo Recuperato],[Data Valuta].[Anno].&[{0}],[Data Valuta].[Mese].&[{1}],{2}",
                                            AnnoCassa, NumMese, Dimensione), "SICE",
                                        OnEndGetDataRecuperatoPerCompetenza,
                                        null);
            }
        }

        private void EuroToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0} {1:c}",
                                                e.DataPoint.LegendLabel.Substring(0,
                                                                                  e.DataPoint.LegendLabel.IndexOf(":")),
                                                e.DataPoint.YValue);
            }
        }

        private void AnnualEuroToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0} {1:c}",
                                                e.DataPoint.LegendLabel,
                                                e.DataPoint.YValue);
            }
        }


        private void EuroMonthToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto",
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}: {1:c}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                                e.DataPoint.YValue);
            }
        }

        private void PercentageToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}: {1:0.00}%",
                                                e.DataPoint.LegendLabel.Substring(0,
                                                                                  e.DataPoint.LegendLabel.IndexOf(":")),
                                                e.DataPoint.YValue);
            }
        }

        private void PercentageMonthToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto",
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}: {1:0.00}%", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                                e.DataPoint.YValue);
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            Common.Common.InitializeRadChart(RadChart2);
            Common.Common.InitializeRadChart(RadChart3);
            Common.Common.InitializeRadChart(RadChart4);
            Common.Common.InitializeRadChart(RadChart5);
            
                if (!worker.IsBusy)
                {
                    TabIndex = mStrip.SelectedIndex;
                    CellsetArray[0] = new CellSet();
                    CellsetArray[1] = new CellSet();
                    CellsetArray[2] = new CellSet();
                    CellsetArray[3] = new CellSet();
                    if (TabIndex != 4)
                    {
                        InizializzazioneInCorso = true;
                        busyIndicator.IsBusy = true;
                        worker.RunWorkerAsync();
                        
                        Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
                        dimCombo.SelectedIndex = 0;
                        CaricaDateAggiornamentoDati();
                    }
                }
                else
                {
                    mStrip.SelectedIndex = TabIndex;
                }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            filterContainer.Visibility = Visibility.Visible;

            switch (TabIndex)
            {
                case 4:
                    filterContainer.Visibility = Visibility.Collapsed;
                    break;
            }

            busyIndicator.IsBusy = false;
        }

        private void dimSelectionChanged(object sender, SelectionChangedEventArgs e1)
        {
            if (dimCombo.SelectedItem != null)
            {
                Dimensione = ((ComboItem) (dimCombo.SelectedItem)).Caption;
                switch (TabIndex)
                {
                    case 0:
                        FiltroGrafico1.Text = String.Format("Filtro: {0}",
                                                            ((ComboItem) (dimCombo.SelectedItem)).Content);
                        break;
                    case 1:
                        FiltroGrafico2.Text = String.Format("Filtro: {0}",
                                                            ((ComboItem) (dimCombo.SelectedItem)).Content);
                        break;
                    case 2:
                        FiltroGrafico3.Text = String.Format("Filtro: {0}",
                                                            ((ComboItem) (dimCombo.SelectedItem)).Content);
                        break;
                    case 3:
                        FiltroGrafico4.Text = String.Format("Filtro: {0}",
                                                            ((ComboItem) (dimCombo.SelectedItem)).Content);
                        break;
                    case 4:
                        FiltroGrafico5.Text = String.Format("Filtro: {0}",
                                                            ((ComboItem) (dimCombo.SelectedItem)).Content);
                        break;
                }
                if (!worker.IsBusy)
                {
                    InizializzazioneInCorso = true;
                    busyIndicator.IsBusy = true;
                    worker.RunWorkerAsync();
                    LoadDataAllYears();
                }
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        public Decimal ToDecimal(string num)
        {
            decimal n;
            num = num.Replace('.', ',');
            Decimal.TryParse(num, out n);
            return n;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CaricamentoDatiBudget();
        }

        #endregion

    }
}