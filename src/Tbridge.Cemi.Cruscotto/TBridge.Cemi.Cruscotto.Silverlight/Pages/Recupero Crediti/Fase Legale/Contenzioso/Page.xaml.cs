﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs=System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.CRC_Contenzioso
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool InizializzazioneInCorso;

        public ICubeService m_Client;

        public string maxYear;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }

        public AdoMdHelperCellStone secondCellStone { get; set; }


        public string Members { set; get; }

        public string Anno { set; get; }

        public string StatoImpresa { get; set; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            //string completePath = App.Current.Host.Source.Scheme
            //     + "://" + App.Current.Host.Source.Host
            //     + ":" + App.Current.Host.Source.Port + "/CubeService.svc";


            string completePath = Application.Current.Host.Source.Scheme
                                  + "://" + Application.Current.Host.Source.Host
                                  + ":" + Application.Current.Host.Source.Port + "/TBridge.Cemi.Cruscotto.Silverlight.Web/CubeService.svc";

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;


            //ChannelFactory<ICubeService> factory =
            //    new ChannelFactory<ICubeService>("BasicHttpBinding_ICubeService");
            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = maxYear = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = maxYear = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            dimCombo.SelectedIndex = 0;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;
            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;

            titoloPraticheLegaliAnnuali.Content = String.Format("Pratiche legali nel {0}", Anno);

            SetSlider();

            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;
            chartAffidatiPerAnno.AxisX.LabelRotationAngle = 45;
            chartImportiDettaglio.AxisX.LabelRotationAngle = 45;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();

                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure = "[Measures].[Data aggiornamento pratiche fallimentari]";

            m_Client.BeginGetDataAggiornamento(measure,
                                                "CRC",
                                                OnEndGetDataAggiornamento,
                                                null);
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider3.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider3.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider3.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                            String.Format("[Measures].[Numero Pratiche con importi],{0}", Dimensione),
                                            "CRC",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                            String.Format("[Measures].[Dovuto],{0}", Dimensione), "CRC",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                            String.Format("[Measures].[Dovuto],{0}", Dimensione), "CRC",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
            }
        }

        private void LoadDataAllYearsSecondSeries()
        {
            InizializzazioneInCorso = true;
            m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                    String.Format("[Measures].[Versato],{0}", Dimensione), "CRC",
                                    OnEndGetSecondSeriesResult,
                                    null);
        }

        private void LoadDataSelectedYear()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Con Importi].[Tipo Fase]",
                                            String.Format("[Measures].[Numero Pratiche con importi],{0}", Dimensione),
                                            "CRC",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Con Importi].[Tipo Fase]",
                                            String.Format("[Measures].[Dovuto],{0}", Dimensione), "CRC",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Con Importi].[Codice Ente]",
                                            String.Format("[Measures].[Dovuto],{0}", Dimensione), "CRC",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedYearSecondSeries()
        {
            InizializzazioneInCorso = true;
            m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                    "[Pratiche Con Importi].[Codice Ente]",
                                    String.Format("[Measures].[Versato],{0}", Dimensione), "CRC",
                                    OnEndGetAnnualSecondSeriesResult,
                                    null);
        }

        private void LoadDataSelectedMonth()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Con Importi].[Tipo Fase]",
                                            String.Format(
                                                "[Measures].[Numero Pratiche con importi],[Data].[Mese].&[{0}],{1}",
                                                NumMese, Dimensione), "CRC",
                                            OnEndGetMonthResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Con Importi].[Tipo Fase]",
                                            String.Format("[Measures].[Dovuto],[Data].[Mese].&[{0}],{1}", NumMese,
                                                          Dimensione), "CRC",
                                            OnEndGetMonthResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Con Importi].[Codice Ente]",
                                            String.Format("[Measures].[Dovuto],[Data].[Mese].&[{0}],{1}", NumMese,
                                                          Dimensione), "CRC",
                                            OnEndGetMonthResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSelectedMonthSecondSeries()
        {
            InizializzazioneInCorso = true;
            m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                    "[Pratiche Con Importi].[Codice Ente]",
                                    String.Format("[Measures].[Versato],[Data].[Mese].&[{0}],{1}", NumMese, Dimensione),
                                    "CRC",
                                    OnEndGetMonthSecondSeriesResult,
                                    null);
        }

        private void LoadDataMonthly()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format("[Measures].[Numero Pratiche con importi],{0}", Dimensione),
                                            "CRC",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format("[Measures].[Dovuto],{0}", Dimensione), "CRC",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format("[Measures].[Dovuto],{0}", Dimensione), "CRC",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
            }
        }

        private void LoadDataSecondSeriesMonthly()
        {
            InizializzazioneInCorso = true;
            m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                    String.Format("[Measures].[Versato],{0}", Dimensione), "CRC",
                                    OnEndGetSecondSeriesMonthlyResult,
                                    null);
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        private void ConfigureBarChart(ChartArea chartArea, ChartTitle title)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, Anno, 0);

            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255, 65, 246, 43));


            double maxValue = 0;

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            //chartArea.AxisY.Title = "";


            for (int i = 0; i < barSeries.Count; i++)
            {
                chartArea.AxisX.TickPoints[i].Value = barSeries[i].XValue;
                chartArea.AxisX.TickPoints[i].Label = barSeries[i].LegendLabel;
            }

            //Common.Common.SetScala(chartArea);


            string format = "#VAL{N0}";
            if (TabIndex > 0) format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            Common.Common.SetScala(chartArea,format);
        }

        private void ConfigureDoubleSeriesChartAxis0(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.TickPoints.Clear();
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, Anno, 0);

            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.LegendLabel = "Dovuto";

            double maxValue = 0;

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            DataSeries barSeries1 = Common.Common.GetSeries(secondCellStone, Anno, 0);

            barSeries1.Definition = new BarSeriesDefinition();
            barSeries1.Definition.ShowItemLabels = false;
            barSeries1.Definition.ShowItemToolTips = true;
            barSeries1.LegendLabel = "Recuperato";
            for (int i = 0; i < barSeries1.Count; i++)
            {
                if (!barSeries1[i].LegendLabel.Equals("Unknown") && !barSeries1[i].LegendLabel.Equals(""))
                    if (barSeries1[i].YValue > maxValue) maxValue = barSeries1[i].YValue;
            }
            if (barSeries1.Count != 0)
                chartArea.DataSeries.Add(barSeries1);

            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            //chartArea.AxisY.Title = "";


            for (int i = 0; i < barSeries.Count; i++)
            {
                chartArea.AxisX.TickPoints[i].Value = barSeries[i].XValue;
                chartArea.AxisX.TickPoints[i].Label = barSeries[i].LegendLabel;
            }

            //Common.Common.SetScala(chartArea);


            string format = "#VAL{N0}";

            if (TabIndex > 0) format = "c0";
            foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            {
                tickPoint.LabelFormat = format;
            }
            Common.Common.SetScala(chartArea,format);
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   //ConfigureAllYearsChart(chartNuoveIscrittePerAnno);
                                                   Common.Common.ConfigureAllYearsChart(chartNuoveIscrittePerAnno, cellStone,
                                                                                 "#VAL{N0}", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Pratiche legali da contenzioso";
                                                   CellsetArray[1].Title = String.Format("Pratiche nel {0}", Anno);
                                                   break;
                                               case 1:
                                                   titoloImportiAnnuali.Content = String.Format("Importi nel {0}", Anno);
                                                   CellsetArray[0].Title = "Importi per pratiche legali";
                                                   //ConfigureAllYearsChart(chartAndamentoAnnualeImporti);
                                                   Common.Common.ConfigureAllYearsChart(chartAndamentoAnnualeImporti, cellStone,
                                                                                 "c0", DateTime.Today.Year);
                                                   break;
                                               case 2:
                                                   titoloImportiAnnualiAffidati.Content =
                                                       String.Format("Importi nel {0}", Anno);
                                                   CellsetArray[0].Title = "Importi per pratiche legali";
                                                   //ConfigureAllYearsChart(chartImportiAffidati);
                                                   break;
                                           }
                                       });
            if (TabIndex != 2)
                LoadDataMonthly();
            else
                LoadDataAllYearsSecondSeries();
        }

        private void OnEndGetSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           Common.Common.ConfigureDoubleSeriesChart(chartImportiAffidati, cellStone,
                                                                             secondCellStone, "c0");
                                           //ConfigureDoubleSeriesChart(chartImportiAffidati);
                                       });
            LoadDataMonthly();
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           if (TabIndex != 2)
                                               CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   ConfigureBarChart(chartNuoveIscrittePerMese,
                                                                     titoloPraticheLegaliAnnuali);
                                                   break;
                                               case 1:
                                                   CellsetArray[1].Title = titoloImportiAnnuali.Content.ToString();
                                                   ConfigureBarChart(chartImportiPerAnno, titoloPraticheLegaliAnnuali);
                                                   break;
                                                   //case 2:
                                                   //    CellsetArray[1].Title = titoloImportiAnnuali.Content.ToString();
                                                   //    //ConfigureAnnualChart(chartAffidatiPerAnno, titoloPraticheLegaliAnnuali);
                                                   //    break;
                                           }
                                       });
            if (TabIndex != 2)
                LoadDataSelectedMonth();
            else LoadDataSelectedYearSecondSeries();
        }

        private void OnEndGetAnnualSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           ConfigureDoubleSeriesChartAxis0(chartAffidatiPerAnno);
                                       });
            LoadDataSelectedMonth();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);

                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureMonthlyChart(chartIscrittePerAnno, cellStone, "#VAL{N0}",
                                                                                Anno, monthNum);
                                                   //ConfigureMonthlyChart(chartIscrittePerAnno,titoloPraticheLegaliMensili);
                                                   CellsetArray[2].Title = "Pratiche legali mensili";
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureMonthlyChart(chartAndamentoMensileImporti, cellStone,
                                                                                "c0", Anno, monthNum);
                                                   //ConfigureMonthlyChart(chartAndamentoMensileImporti,titoloImportiMensili);
                                                   CellsetArray[2].Title = "Importi mensili";
                                                   break;
                                           }
                                       });
            if (TabIndex != 2)
                LoadDataSelectedYear();
            else LoadDataSecondSeriesMonthly();
        }

        private void OnEndGetSecondSeriesMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           Common.Common.ConfigureMonthDoubleSeriesChart(chartImportiAffidatiMensili, cellStone,
                                                                                  secondCellStone, "c0", Anno, monthNum,
                                                                                  String.Empty);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                           CellsetArray[1].Content = Common.Common.ToString(secondCellStone);
                                           CellsetArray[0].Title = "Importi Dovuti";
                                           CellsetArray[1].Title = "Importi Affidati";
                                           //ConfigureDoubleSeriesChartMonths(chartImportiAffidatiMensili);
                                       });
            LoadDataSelectedYear();
        }

        private void OnEndGetMonthResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           string[] months = new[]
                                                                 {
                                                                     "Gennaio", "Febbraio", "Marzo", "Aprile",
                                                                     "Maggio", "Giugno", "Luglio",
                                                                     "Agosto", "Settembre", "Ottobre", "Novembre",
                                                                     "Dicembre"
                                                                 };

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   titoloPraticheLegaliMensili.Content =
                                                       String.Format("Pratiche legali di {0} ",
                                                                     months[NumMese - 1]);
                                                   CellsetArray[3].Title =
                                                       titoloPraticheLegaliMensili.Content.ToString();
                                                   ConfigureBarChart(chartIscrittePerMese, titoloPraticheLegaliMensili);
                                                   break;
                                               case 1:
                                                   titoloImportiMensili.Content = String.Format("Dettaglio di {0}",
                                                                                                months[NumMese -
                                                                                                    1]);
                                                   CellsetArray[3].Title = titoloImportiMensili.Content.ToString();
                                                   ConfigureBarChart(chartImportiPerMese, titoloPraticheLegaliMensili);
                                                   break;
                                               case 2:
                                                   titoloImportiAffidatiMensili.Content =
                                                       String.Format("Dettaglio di {0}", months[NumMese - 1]);
                                                   CellsetArray[3].Title =
                                                       titoloImportiAffidatiMensili.Content.ToString();
                                                   //ConfigureAnnualChart(chartImportiDettaglio, titoloPraticheLegaliMensili); 
                                                   break;
                                           }
                                       });
            if (TabIndex == 2)
                LoadDataSelectedMonthSecondSeries();
            else InizializzazioneCompletata();
        }

        private void OnEndGetMonthSecondSeriesResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           ConfigureDoubleSeriesChartAxis0(chartImportiDettaglio);
                                           CellsetArray[2] = new CellSet();
                                           CellsetArray[3] = new CellSet();
                                       });
            InizializzazioneCompletata();
        }

        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format(TabIndex==2 ? "{0}\r\n{1:c0}" : "{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel.Substring(0,
                                                                                                                                            e.DataPoint.LegendLabel.LastIndexOf(
                                                                                                                                                ":"))), e.DataPoint.YValue);
            }
        }


        private void ItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format(TabIndex==2 ? "{0}\r\n{1:c0}" : "{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel), e.DataPoint.YValue);
            }
        }

        private void MonthItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            if(TabIndex==2){
                tooltip.Content = string.Format("{0}\r{1:c0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                            e.DataPoint.YValue);
            }
            else
            {
                tooltip.Content = string.Format("{0}\rNumero: {1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                            e.DataPoint.YValue);
            }
        }

        private void ChartIscrittePerMeseClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            NumMese = Int32.Parse(e.DataPoint.LegendLabel.Split(':')[0]);
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };
            if (TabIndex == 0)
                titoloPraticheLegaliMensili.Content = String.Format("Pratiche legali di {0} ",
                                                                    months[NumMese - 1]);
            else titoloImportiMensili.Content = String.Format("Importi di {0} ", months[NumMese - 1]);

            
                LoadDataSelectedMonth();
            }
        }

        private void ChartNuoveIscrittePerMeseYearClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            titoloPraticheLegaliAnnuali.Content = String.Format("Pratiche legali nel {0}", Anno);
            titoloImportiAnnuali.Content = String.Format("Importi nel {0}", Anno);
            titoloImportiAnnualiAffidati.Content = String.Format("Importi nel {0}", Anno);
           
                LoadDataMonthly();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            Common.Common.InitializeRadChart(RadChart2);
            Common.Common.InitializeRadChart(RadChart3);

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;

                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();
                LoadDataAllYears();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            CambiaLegenda();
            busyIndicator.IsBusy = false;
        }


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataAllYears();
            }
        }

        private void CambiaLegenda()
        {
            if (TabIndex == 2)
            {
                Testo0.Text = "A1: MANZOLI PAOLO";
                Testo1.Text = "A2: MENICHINO FILIPPO";
                Testo2.Text = "A4: GORRASI FRANCESO";
                Testo3.Text = "A5: VIGILANTE PAOLILLO";
                Testo4.Text = "A6: LOALDI ANGELA";
                Testo5.Text = "A7: GUAGLIONE MARCUCCI";
                Testo6.Text = "A8: ZANCHETTA RICCARDO";
                Testo7.Text = "A9: ANDRIA  LUIGI";
                Testo8.Text = "";
                Testo9.Text = "";
                Testo10.Text = "";
                Testo11.Text = "";
            }
            else
            {
                Testo0.Text = "DI: DECRETO INGIUNTIVO";
                Testo1.Text = "PRE: PRECETTO";
                Testo2.Text = "PIG: PIGNORAMENTO";
                Testo3.Text = "IF: ISTANZA DI FALLIMENTO";
                Testo4.Text = "DIC: DIFFIDA DA CONTENZIOSO";
                Testo5.Text = "DIL: DIFFIDA DA LEGALE";
                Testo6.Text = "APA: DITTA AGLI ATTI ACCORDO SEDE";
                Testo7.Text = "AM: AMMINISTRAZIONE CONTROLLATA";
                Testo8.Text = "APL: ACCORDO DI PAGAMENTO DA LEGALE";
                Testo9.Text = "CP: CONCORDATO PREVENTIVO";
                Testo10.Text = "LCA: LIQUIDAZIONE COATTA AMMINISTRATIVA";
                Testo11.Text = "PR1: PIANO DI RIENTRO LEGALE";
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n = 0;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion
    }
}