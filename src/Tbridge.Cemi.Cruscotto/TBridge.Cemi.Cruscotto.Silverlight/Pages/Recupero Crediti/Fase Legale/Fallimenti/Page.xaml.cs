﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using TBridge.Cemi.Cruscotto.Silverlight.FunzioniComuni;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs=System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.CRC_Legale
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// </summary>
    public partial class Page : Example
    {
        #region Variabili e Proprietà

        private CellSet[] _elements = {new CellSet(), new CellSet(), new CellSet(), new CellSet()};
        public string _filtro;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool initialize;
        public bool InizializzazioneInCorso;

        public ICubeService m_Client;

        public string maxYear;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }

        public AdoMdHelperCellStone secondCellStone { get; set; }

        public string Members { set; get; }

        public string Anno { set; get; }

        public string StatoImpresa { get; set; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            //string completePath = App.Current.Host.Source.Scheme
            //     + "://" + App.Current.Host.Source.Host
            //     + ":" + App.Current.Host.Source.Port + "/CubeService.svc";

            string completePath = Application.Current.Host.Source.Scheme
                                  + "://" + Application.Current.Host.Source.Host
                                  + ":" + Application.Current.Host.Source.Port + "/TBridge.Cemi.Cruscotto.Silverlight.Web/CubeService.svc";

            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);

            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;


            //ChannelFactory<ICubeService> factory =
            //    new ChannelFactory<ICubeService>("BasicHttpBinding_ICubeService");
            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = maxYear = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = maxYear = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            dimCombo.SelectedIndex = 0;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;
            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;

            titoloNumPraticheAttiveMensile.Content = String.Format("Pratiche attive nel {0}", Anno);
            titoloimportiMensiliPraticheAttive.Content = String.Format("Importi nel {0}", Anno);

            SetSlider();

            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure = "[Measures].[Data aggiornamento pratiche fallimentari]";
             
            m_Client.BeginGetDataAggiornamento(measure,
                                                "CRC",
                                                OnEndGetDataAggiornamento,
                                                null);
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

        private void SetSlider()
        {
            Slider1.ValueChanged -= Slider_ValueChanged;
            Slider2.ValueChanged -= Slider_ValueChanged;

            Slider1.Value = Common.Common.Delay;
            Slider2.Value = Common.Common.Delay;

            Slider1.ValueChanged += Slider_ValueChanged;
            Slider2.ValueChanged += Slider_ValueChanged;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Common.Common.Delay = e.NewValue;

            SetSlider();

            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;
            initialize = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                            String.Format(
                                                "[Measures].[Numero pratiche fallimentari],[Pratiche Fallimentari].[Risultato Pratica].&[Attiva],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "",
                                            String.Format(
                                                "[Measures].[Numero pratiche fallimentari],[Pratiche Fallimentari].[Risultato Pratica].&[Archiviata],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetAllYearsResult,
                                            null);
                    break;
                case 2:
                    LoadDataImporti();
                    break;
            }
        }

        private void LoadDataSelectedYear()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno), "[Data].[Mese]",
                                            String.Format(
                                                "[Measures].[Numero pratiche fallimentari],[Pratiche Fallimentari].[Risultato Pratica].&[Attiva],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                                            "[Pratiche Fallimentari].[Stato]",
                                            String.Format(
                                                "[Measures].[Numero pratiche fallimentari],[Pratiche Fallimentari].[Risultato Pratica].&[Archiviata],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetAnnualResult,
                                            null);
                    break;
                case 2:
                    LoadDataMonthly();
                    break;
            }
        }

        private void LoadDataImporti()
        {
            InizializzazioneInCorso = true;
            if (initialize)
            {
                switch (TabIndex)
                {
                    case 0:
                        m_Client.BeginGetResult("", String.Format("[Data].[Anno]"), "",
                                                String.Format(
                                                    "[Measures].[Dovuto - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Attiva],{0}",
                                                    Dimensione), "CRC",
                                                OnEndGetMonthResult,
                                                null);
                        break;
                    case 1:
                        //m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno), "[Data].[Mese]",
                        //                        String.Format(
                        //                            "[Measures].[Numero pratiche fallimentari],[Pratiche Fallimentari].[Risultato Pratica].&[Archiviata],{0}",
                        //                            Dimensione), "CRC",
                        //                        OnEndGetMonthSecondResult,
                        //                        null);
                        m_Client.BeginGetResult("", String.Format("[Data].[Anno]"), "",
                                                String.Format(
                                                    "[Measures].[Dovuto - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Archiviata],{0}",
                                                    Dimensione), "CRC",
                                                OnEndGetMonthResult,
                                                null);
                        break;
                    case 2:
                        m_Client.BeginGetResult("", String.Format("[Data].[Anno]"), "",
                                                String.Format(
                                                    "[Measures].[Dovuto - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Archiviata],{0}",
                                                    Dimensione), "CRC",
                                                OnEndGetMonthResult,
                                                null);

                        break;
                }
            }
            else
            {
                InizializzazioneCompletata();
            }
        }

        private void LoadDataImportiSecondSeries()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno]"), "",
                                            String.Format(
                                                "[Measures].[Versato - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Attiva],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetMonthSecondResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno]"), "",
                                            String.Format(
                                                "[Measures].[Versato - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Archiviata],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetMonthSecondResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthly()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno), "[Data].[Mese]",
                                            String.Format(
                                                "[Measures].[Dovuto - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Attiva],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 1:
                    //m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno),
                    //                        "[Pratiche Fallimentari].[Stato]",
                    //                        String.Format(
                    //                            "[Measures].[Numero pratiche fallimentari],[Pratiche Fallimentari].[Risultato Pratica].&[Archiviata],{0},[Data].[Mese].&[{1}]",
                    //                            Dimensione, Mese), "CRC",
                    //                        OnEndGetSecondMonthlyResult,
                    //                        null);
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format(
                                                "[Measures].[Dovuto - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Archiviata],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
                case 2:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format(
                                                "[Measures].[Dovuto - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Archiviata],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetMonthlyResult,
                                            null);
                    break;
            }
        }

        private void LoadDataMonthlySecondSeries()
        {
            InizializzazioneInCorso = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", String.Format("[Data].[Anno].&[{0}]", Anno), "[Data].[Mese]",
                                            String.Format(
                                                "[Measures].[Versato - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Attiva],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetSecondMonthlyResult,
                                            null);
                    break;
                case 1:
                    m_Client.BeginGetResult("", "[Data].[Anno]", "[Data].[Mese]",
                                            String.Format(
                                                "[Measures].[Versato - Pratiche Fallimentari Con Importi],[Pratiche Fallimentari Con Importi].[Risultato Pratica].&[Archiviata],{0}",
                                                Dimensione), "CRC",
                                            OnEndGetSecondMonthlyResult,
                                            null);
                    break;
            }
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        private void ConfigureAnnualChart(ChartArea chartArea, ChartTitle title)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisX.TickPoints.Clear();

            DataSeries doughnutSeries = Common.Common.GetSeries(cellStone, Anno, 0);
            doughnutSeries.LegendLabel = "Iscrizioni annuali";
            doughnutSeries.Definition = new DoughnutSeriesDefinition();
            ((DoughnutSeriesDefinition) doughnutSeries.Definition).LabelSettings.LabelOffset = 0.7d;
            doughnutSeries.Definition.ItemLabelFormat = "#%{p0}";
            doughnutSeries.Definition.ShowItemLabels = true;
            doughnutSeries.Definition.ShowItemToolTips = true;
            Common.Common.SetSeriesLabel(doughnutSeries);

            DataSeries temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].LegendLabel.StartsWith("0"))
                    doughnutSeries.RemoveAt(i);

            temporanea = doughnutSeries;
            for (int i = 0; i < temporanea.Count; i++)
                if (temporanea[i].YValue == 0)
                    doughnutSeries.RemoveAt(i);

            chartArea.DataSeries.Add(doughnutSeries);
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetAllYearsResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   //ConfigureAllYearsChart(chartPraticheAttiveAnnuali);
                                                   Common.Common.ConfigureAllYearsChart(chartPraticheAttiveAnnuali, cellStone,
                                                                                 "#VAL{N0}", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Pratiche legali attive";
                                                   break;
                                               case 1:
                                                   titoloArchiviatePerAnno.Content =
                                                       String.Format("Pratiche archiviate nel {0}", Anno);
                                                   //ConfigureAllYearsChart(chartArchiviateAnnualmente);
                                                   Common.Common.ConfigureAllYearsChart(chartArchiviateAnnualmente, cellStone,
                                                                                 "#VAL{N0}", DateTime.Today.Year);
                                                   CellsetArray[0].Title = "Pratiche archiviate";
                                                   CellsetArray[1].Title = titoloArchiviatePerAnno.Content.ToString();
                                                   CellsetArray[2].Title = "Pratiche archiviate mensilmente";
                                                   break;
                                           }
                                       });
            LoadDataSelectedYear();
        }

        private void OnEndGetAnnualResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[1].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureMonthlyChart(chartPraticheAttiveMensili, cellStone,
                                                                                "#VAL{N0}", Anno, monthNum);
                                                   //ConfigureMonthlyChart(chartPraticheAttiveMensili, titoloNumPraticheAttiveMensile); 
                                                   CellsetArray[1].Title =
                                                       titoloNumPraticheAttiveMensile.Content.ToString();
                                                   break;
                                               case 1:
                                                   ConfigureAnnualChart(chartArchiviatePerAnno,
                                                                        titoloNumPraticheAttiveMensile);
                                                   break;
                                           }
                                       });
            LoadDataImporti();
        }

        private void OnEndGetMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           if (TabIndex != 2)
                                           {
                                               CellsetArray[2].Content = Common.Common.ToString(cellStone);
                                               CellsetArray[2].Title = String.Format("Importo Dovuto nel {0}", Anno);
                                           }
                                       });
            LoadDataMonthlySecondSeries();
        }

        private void OnEndGetSecondMonthlyResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);

                                           CellsetArray[3].Content = Common.Common.ToString(secondCellStone);
                                           string[] months = new[]
                                                                 {
                                                                     "Gennaio", "Febbraio", "Marzo", "Aprile",
                                                                     "Maggio", "Giugno", "Luglio",
                                                                     "Agosto", "Settembre", "Ottobre", "Novembre",
                                                                     "Dicembre"
                                                                 };
                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureMonthDoubleSeriesChart(
                                                       chartImportiMensiliPraticheAttive, cellStone, secondCellStone,
                                                       "c0", Anno, monthNum, String.Empty);
                                                   //ConfigureDoubleSeriesChartMonths(chartImportiMensiliPraticheAttive);
                                                   chartImportiMensiliPraticheAttive.DataSeries[0].LegendLabel =
                                                       "Dovuto";
                                                   chartImportiMensiliPraticheAttive.DataSeries[1].LegendLabel =
                                                       "Versato";
                                                   CellsetArray[3].Title = String.Format("Importo Versato nel {0}", Anno);
                                                   break;
                                           }
                                       });
            InizializzazioneCompletata();
        }

        private void OnEndGetMonthResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                       });
            LoadDataImportiSecondSeries();
        }

        private void OnEndGetMonthSecondResult(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Content = Common.Common.ToString(cellStone);

                                           switch (TabIndex)
                                           {
                                               case 0:
                                                   Common.Common.ConfigureDoubleSeriesChart(chartImportiAnnualiPraticheAttive,
                                                                                     cellStone, secondCellStone, "c0");
                                                   //ConfigureDoubleSeriesChart(chartImportiAnnualiPraticheAttive);
                                                   break;
                                               case 1:
                                                   Common.Common.ConfigureDoubleSeriesChart(chartImportiArchiviati, cellStone,
                                                                                     secondCellStone, "c0");
                                                   chartImportiArchiviati.DataSeries[0].LegendLabel = "Dovuto";
                                                   chartImportiArchiviati.DataSeries[1].LegendLabel = "Versato";
                                                   //cellStone = secondCellStone;
                                                   //secondCellStone = null;
                                                   //Common.ConfigureMonthlyChart(chartArchiviateMensili, cellStone, "#VAL{N0}",
                                                   //                             Anno, monthNum);
                                                   //CellsetArray[2].Content = Common.ToString(cellStone);
                                                   //ConfigureMonthlyChart(chartArchiviateMensili,titoloArchiviatePerMese);
                                                   break;
                                               case 2:
                                                   Common.Common.ConfigureDoubleSeriesChart(chartImportiArchiviati, cellStone,
                                                                                     secondCellStone, "c0");
                                                   chartImportiArchiviati.DataSeries[0].LegendLabel = "Dovuto";
                                                   chartImportiArchiviati.DataSeries[1].LegendLabel = "Versato";

                                                   //ConfigureDoubleSeriesChart(chartImportiArchiviati); 
                                                   break;
                                           }
                                       });
            LoadDataMonthly();
        }

        #endregion

        #region Eventi Grafici

        private void NumItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}",
                                                (e.DataPoint.LegendLabel.Substring(0,
                                                                                   e.DataPoint.LegendLabel.LastIndexOf(
                                                                                       ":"))), e.DataPoint.YValue);
            }
        }

        private void ItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel), e.DataPoint.YValue);
            }
        }

        private void MonthItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio",
                                      "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            tooltip.Content = string.Format("{0}\rNumero: {1:n0}", months[Int32.Parse(e.DataPoint.LegendLabel) - 1],
                                            e.DataPoint.YValue);
        }

        private void ChartPraticheMensiliClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            switch (TabIndex)
            {
                default:
                    NumMese = Int32.Parse(e.DataPoint.LegendLabel.Split(':')[0]);
                    break;
                case 0:
                    Anno = e.DataPoint.LegendLabel.Split(':')[0];
                    titoloimportiMensiliPraticheAttive.Content = String.Format("Importi nel {0}", Anno);
                    break;
            }
           
                LoadDataMonthly();
            }
        }

        private void ChartPraticheAnnualiClick(object sender, ChartItemClickEventArgs e)
        {
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
            Anno = e.DataPoint.LegendLabel.Split(':')[0];
            titoloNumPraticheAttiveMensile.Content = String.Format("Pratiche attive nel {0}", Anno);
            titoloArchiviatePerAnno.Content = String.Format("Pratiche archiviate nel {0}", Anno);
            if (TabIndex == 0) initialize = false;

                LoadDataSelectedYear();
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            Common.Common.InitializeRadChart(RadChart1);
            Common.Common.InitializeRadChart(RadChart3);

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;
                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();
                LoadDataAllYears();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {


            busyIndicator.IsBusy = false;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Dimensione = ((ComboItem) dimCombo.SelectedItem).Caption;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataAllYears();
            }
        }


        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;
            Common.Common.CreaGridView(CodeViewer, CellsetArray,filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion
    }
}