﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ServiceModel;
using System.Windows;
using System.Windows.Media;
using TBridge.Cemi.Cruscotto.Silverlight.CubeBrowserService;
using TBridge.Cemi.Cruscotto.Silverlight.Entities;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;
using TBridge.Cemi.Cruscotto.Template;
using SelectionChangedEventArgs=System.Windows.Controls.SelectionChangedEventArgs;

namespace TBridge.Cemi.Cruscotto.Silverlight.Pages.PreLegale
{
    /// <summary>
    /// Interaction logic for Example.xaml
    /// Rinominato case 0 --> case 4
    ///            case 2 --> case 0
    /// Togliere commenti metodi OnEnd, ComboBox_SelectionChanged, SelezioneAttivita, TabSelectionChanged
    /// Per ripristinare
    /// </summary>
    public partial class Page : Example
    {


        #region Variabili e Proprietà

        private CellSet[] _elements = { new CellSet(), new CellSet(), new CellSet(), new CellSet() };
        public string _filtro;
        public String AttivitaSelezionata;
        public Brush brush;
        public String ComunicazioneSelezionata;
        public RadGridView gridView;
        public EventArgs hideExampleArea;
        public bool initialize;

        public bool InizializzazioneInCorso;

        public ICubeService m_Client;

        public string maxYear;
        public BackgroundWorker worker = new BackgroundWorker();
        public AdoMdHelperCellStone cellStone { get; set; }

        public AdoMdHelperCellStone secondCellStone { get; set; }

        public AdoMdHelperCellStone thirdCellStone { get; set; }

        public string Members { set; get; }

        public string Anno { set; get; }

        public string StatoImpresa { get; set; }

        public string CellsetString { get; set; }

        public string Misura { set; get; }

        public string Dimensione { set; get; }

        public new int TabIndex { get; set; }

        public DataSeries monthNum { get; set; }

        public ObservableCollection<string> DimensionMembers { get; set; }

        public string Mese { get; set; }

        public int NumMese { get; set; }

        public CellSet[] CellsetArray
        {
            get { return _elements; }
            set { _elements = value; }
        }

        #endregion

        public Page()
        {
            InitializeComponent();

            foreach (RadTabItem item in mStrip.Items)
            {
                RadChart chart = item.Content as RadChart;
                if (chart != null)
                {
                    Common.Common.ApplyOldChartStyle(chart, LayoutRoot.Resources);
                }
            }

            Loaded += Example_Loaded;
            Unloaded += Example_Unloaded;
        }

        private void Example_Unloaded(object sender, EventArgs e)
        {
            worker.CancelAsync();
        }

        private void Example_Loaded(object sender, RoutedEventArgs e)
        {
            BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
            EndpointAddress endpointAddress = new EndpointAddress(Common.Common.CompletePath);


            basicHttpBinding.MaxBufferSize = 2147483647;
            basicHttpBinding.MaxReceivedMessageSize = 2147483647;

            ChannelFactory<ICubeService> factory =
                new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

            factory.Open();
            m_Client = factory.CreateChannel();

            ConfigureMonth();

            if (DateTime.Today.Month - 1 > 0)
            {
                Anno = maxYear = DateTime.Today.Year.ToString();
                NumMese = DateTime.Today.Month -1;
            }
            else
            {
                Anno = maxYear = (DateTime.Today.Year - 1).ToString();
                NumMese = 12;
            }

            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            Mese = months[NumMese-1];

            dimCombo.SelectedIndex = 0;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;
            Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;

            mStrip.SelectionChanged += TabSelectionChanged;
            dimCombo.SelectionChanged += ComboBox_SelectionChanged;

            worker.DoWork += BackgroundWorkerDoWork; worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += BackgroundWorkerRunWorkerCompleted;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                CaricaDateAggiornamentoDati();
            }
        }

        private void CaricaDateAggiornamentoDati()
        {
            string measure =  "[Measures].[Data aggiornamento Comunicazioni]";
                    
            m_Client.BeginGetDataAggiornamento(measure,
                                                "CRM",
                                                OnEndGetDataAggiornamento,
                                                null);
        }

        private void OnEndGetDataAggiornamento(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(delegate
            {
                string data = m_Client.EndGetDataAggiornamento(ar);
                DataAggiornamento.Text = String.Format("Dati aggiornati al {0}", data);
            });
            LoadDataAllYears();
        }

        #region Caricamento Dati

        private void LoadDataAllYears()
        {
            InizializzazioneInCorso = true;
            initialize = true;
            switch (TabIndex)
            {
                case 0:
                    m_Client.BeginGetResult("", "[Time].[Anno]", "",
                                            String.Format("[Measures].[Imprese contattate],{0}", Dimensione), "CRM",
                                            OnEndGetImpreseContattateAnnualmente,
                                            null);
                    break;
            }
        }

        private void LoadDataImpreseContattateMensilmente()
        {
            m_Client.BeginGetResult("", "[Time].[Anno]", "[Time].[Mese]",
                                            String.Format("[Measures].[Imprese contattate],{0}", Dimensione), "CRM",
                                            OnEndGetImpreseContattateMensilmente,
                                            null);
        }

        private void LoadDataImpreseContattate()
        {
            m_Client.BeginGetResult("", "[Cruscotto Comunicazioni Stato Inviata].[Tipo Comunicazione]", "",
                                            String.Format("[Measures].[Imprese contattate],{0},[Time].[Anno].&[{1}],[Time].[Mese].&[{2}]", Dimensione, Anno, NumMese), "CRM",
                                            OnEndGetImpreseContattate,
                                            null);
        }

        private void LoadDataImportiOriginari()
        {
            m_Client.BeginGetResult("", "[Cruscotto Comunicazioni Stato Inviata].[Tipo Comunicazione]", "",
                                    String.Format("[Measures].[Importo Debito Iniziale],{0},[Time].[Anno].&[{1}],[Time].[Mese].&[{2}]", Dimensione, Anno, NumMese), "CRM",
                                    OnEndGetImportiOriginari,
                                    null);
        }

        private void LoadDataImportiOdierni()
        {
            m_Client.BeginGetResult("", "[Cruscotto Comunicazioni Stato Inviata].[Tipo Comunicazione]", "",
                                    String.Format("[Measures].[Importo Debito Odierno],{0},[Time].[Anno].&[{1}],[Time].[Mese].&[{2}]", Dimensione, Anno, NumMese), "CRM",
                                    OnEndGetImportiOdierni,
                                    null);
        }

        private void InizializzazioneCompletata()
        {
            InizializzazioneInCorso = false;
        }

        #endregion

        #region Configurazione Grafici

        private void ConfigureMonth()
        {
            monthNum = new DataSeries();
            for (int i = 1; i < 13; i++)
            {
                monthNum.Add(new DataPoint(i, 0));
                monthNum[i - 1].LegendLabel = i.ToString();
            }
        }

        private void ConfigureDoubleBarChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            chartArea.AxisY.TickPoints.Clear();

            DataSeries barSeries = Common.Common.GetSeries(cellStone, cellStone.Rows[1].Columns[0].Caption, 1);

            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;

            barSeries.LegendLabel = cellStone.Rows[1].Columns[0].Caption;
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255,140,50,190));
            double maxValue = 0;

            foreach (DataPoint t in barSeries)
            {
                if (!t.LegendLabel.Equals("Unknown") && !t.LegendLabel.Equals(""))
                    if (t.YValue > maxValue) maxValue = t.YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);

            //
            barSeries = Common.Common.GetSeries(secondCellStone, secondCellStone.Rows[1].Columns[0].Caption, 1);

            barSeries.Definition = new BarSeriesDefinition();
            barSeries.Definition.ShowItemLabels = false;
            barSeries.Definition.ShowItemToolTips = true;
            barSeries.Definition.Appearance.Fill = new SolidColorBrush(Color.FromArgb(255,120,154,200));

            barSeries.LegendLabel = secondCellStone.Rows[1].Columns[0].Caption;
            maxValue = 0;

            for (int i = 0; i < barSeries.Count; i++)
            {
                if (!barSeries[i].LegendLabel.Equals("Unknown") && !barSeries[i].LegendLabel.Equals(""))
                    if (barSeries[i].YValue > maxValue) maxValue = barSeries[i].YValue;
            }
            if (barSeries.Count != 0)
                chartArea.DataSeries.Add(barSeries);


            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;

            for (int i = 0; i < barSeries.Count; i++)
            {
                chartArea.AxisX.TickPoints[i].Value = barSeries[i].XValue;
                chartArea.AxisX.TickPoints[i].Label = barSeries[i].LegendLabel;
            }

            const string format = "c0";
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}
            Common.Common.SetScala(chartArea,format);
        }

        private void ConfigureStackedbarChart(ChartArea chartArea)
        {
            chartArea.DataSeries.Clear();
            chartArea.AxisY.AutoRange = false;
            chartArea.AxisY.MinValue = 0;
            chartArea.AxisX.TickPoints.Clear();
            chartArea.AxisY.TickPoints.Clear();


            for (int x = 1; x < cellStone.Rows[0].Columns.Count; x++)
            {
                DataSeries barSeries = Common.Common.GetSeries(cellStone, cellStone.Rows[0].Columns[x].Caption, 0);

                barSeries.Definition = new StackedBarSeriesDefinition
                                           {
                                               ShowItemLabels = false,
                                               ShowItemToolTips = true
                                           };
                
                barSeries.LegendLabel = cellStone.Rows[0].Columns[x].Caption;

                if (barSeries.Count != 0)
                    chartArea.DataSeries.Add(barSeries);
            }
            chartArea.AxisX.StripLinesVisibility = Visibility.Collapsed;
            chartArea.AxisX.Visibility = Visibility.Collapsed;
            chartArea.ItemWidthPercent = 10;
            //foreach (TickPoint tickPoint in chartArea.AxisY.TickPoints)
            //{
            //    tickPoint.LabelFormat = format;
            //}

            Common.Common.SetScalaStackedChart(chartArea,"#VAL{N0}");
        }

        #endregion

        #region OnEndGetResult

        private void OnEndGetImpreseContattateAnnualmente(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[0].Title = "Imprese contattate annualmente";
                                           CellsetArray[0].Content = Common.Common.ToString(cellStone);

                                           Common.Common.ConfigureAllYearsChart(chartImpreseContattateAnnualmente, cellStone, "#VAL{N0}",DateTime.Today.Year);
                                       });
            LoadDataImpreseContattateMensilmente();
        }

        private void OnEndGetImpreseContattateMensilmente(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                cellStone =
                    m_Client.EndGetResult(asyncResult);
                CellsetArray[1].Title = "Imprese contattate mensilmente";
                CellsetArray[1].Content = Common.Common.ToString(cellStone);

                Common.Common.ConfigureMonthlyChart(chartImpreseContattateMensilmente, cellStone, "#VAL{N0}", Anno, monthNum);
            });
            LoadDataImpreseContattate();
        }

        private void OnEndGetImpreseContattate(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
            {
                cellStone =
                    m_Client.EndGetResult(asyncResult);
                CellsetArray[1].Title = "Imprese contattate mensilmente";
                CellsetArray[1].Content = Common.Common.ToString(cellStone);


                MonthlyTitle.Content = String.Format("Dettaglio di {0} {1}", Mese, Anno);
                ConfigureStackedbarChart(chartImpreseContattate);
            });
            LoadDataImportiOriginari();
        }

        private void OnEndGetImportiOriginari(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           cellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[2].Title = "Importo debito iniziale";
                                           CellsetArray[2].Content = Common.Common.ToString(cellStone);
                                       });
            LoadDataImportiOdierni();
        }

        private void OnEndGetImportiOdierni(IAsyncResult asyncResult)
        {
            Dispatcher.BeginInvoke(delegate
                                       {
                                           secondCellStone =
                                               m_Client.EndGetResult(asyncResult);
                                           CellsetArray[3].Title = "Importo debito attuale";
                                           CellsetArray[3].Content = Common.Common.ToString(secondCellStone);

                                           ConfigureDoubleBarChart(chartImportiRecuperati);
                                       });
            InizializzazioneCompletata();
        }

        #endregion

        #region Eventi Grafici

        private void EuroToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0} {1:c}", e.DataPoint.LegendLabel, e.DataPoint.YValue);
            }
        }

        private void ItemToolTip(ItemToolTip2D tooltip, ItemToolTipEventArgs e)
        {
            tooltip.Content = null;

            if (!e.DataPoint.LegendLabel.Equals(" "))
            {
                tooltip.Content = string.Format("{0}\r\nNumero: {1:n0}", (e.DataPoint.LegendLabel), e.DataPoint.YValue);
            }
        }

        #endregion

        #region Eventi

        private void TabSelectionChanged(object sender, RoutedEventArgs e)
        {
            //Common.InitializeRadChart(RadChart1);
            //Common.InitializeRadChart(RadChart3);
            Common.Common.InitializeRadChart(RadChart4);
            TopRightPanel.Visibility = Visibility.Collapsed;

            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                TabIndex = mStrip.SelectedIndex;
                CellsetArray[0] = new CellSet();
                CellsetArray[1] = new CellSet();
                CellsetArray[2] = new CellSet();
                CellsetArray[3] = new CellSet();
                LoadDataAllYears();
            }
            else
            {
                mStrip.SelectedIndex = TabIndex;
            }
        }

        private void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (!InizializzazioneInCorso) break;
            }
        }

        private void BackgroundWorkerRunWorkerCompleted(object sender,
                                                        RunWorkerCompletedEventArgs e)
        {
            if (TabIndex == 1)
                TopRightPanel.Visibility = Visibility.Visible;



            busyIndicator.IsBusy = false;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Dimensione = ((ComboItem)dimCombo.SelectedItem).Caption;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataAllYears();
            }
        }

        protected override void OnHideExampleArea(EventArgs e)
        {
            String filtro = null;
            if (filterContainer.Visibility == Visibility.Visible)
                filtro = (string)((ComboItem)dimCombo.SelectedItem).Content;

            Common.Common.CreaGridView(CodeViewer, CellsetArray, filtro);
            base.OnHideExampleArea(e);
        }

        protected override void OnShowExampleArea(EventArgs e)
        {
            base.OnShowExampleArea(e);
        }

        #endregion

        #region Export to Excel

        protected override void OnClickExportButton(EventArgs e)
        {
            Common.Common.ToExcel(CodeViewer);
            base.OnClickExportButton(e);
        }

        public int ToInt32(string num)
        {
            int n;
            Int32.TryParse(num, out n);
            return n;
        }

        #endregion

        private void chartImpreseContattateAnnualmente_ItemClick(object sender, ChartItemClickEventArgs e)
        {
            Anno = e.DataPoint.XCategory;
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataImpreseContattateMensilmente();
            }
        }

        private void chartImpreseContattateMensilmente_ItemClick(object sender, ChartItemClickEventArgs e)
        {
            NumMese = Int32.Parse(e.DataPoint.LegendLabel);
            string[] months = new[]
                                  {
                                      "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto"
                                      ,
                                      "Settembre", "Ottobre", "Novembre", "Dicembre"
                                  };

            Mese = months[NumMese-1];
            if (!worker.IsBusy)
            {
                InizializzazioneInCorso = true;
                busyIndicator.IsBusy = true;
                worker.RunWorkerAsync();
                LoadDataImpreseContattate();
            }
        }
    }
}