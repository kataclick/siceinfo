﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using TestConsole.AuthenticationService;
using TestConsole.CubeBrowserService;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
           // AuthenticationService.IAuthenticationService authenticationService;
           // BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
           // EndpointAddress endpointAddress = new EndpointAddress("http://localhost:1700/CubeService.svc");
            
           // //basicHttpBinding.Security.Mode = BasicHttpSecurityMode.TransportWithMessageCredential;
           // basicHttpBinding.MaxBufferSize = 2147483647;
           // basicHttpBinding.MaxReceivedMessageSize = 2147483647;
           //Dictionary<string ,IList<String>> DictionaryCodici = new Dictionary<string, IList<String>>();
           // DictionaryCodici["PremioFedeltà"] = new List<string> { "[PREFED]", "[B022]" };
           // string Members = "Member [Tipi Prestazione].[Tipo Prestazione].[OldAndNew] AS'Aggregate({";

           // for (int i = 0; i < DictionaryCodici["PremioFedeltà"].Count; i++)
           //     Members = String.Format("{0}[Tipi Prestazione].[Tipo Prestazione].&{1},", Members, DictionaryCodici["PremioFedeltà"][i]);

           // Members = String.Format("{0}{1})'", Members.Substring(0, Members.Length - 1), "}");
           // ChannelFactory<ICubeService> factory =
           //     new ChannelFactory<ICubeService>(basicHttpBinding, endpointAddress);

           // factory.Open();
           // ICubeService client = factory.CreateChannel();
            
           //AdoMdHelperCellStone cellStone = client.GetResult(Members, string.Format("[Data Domanda].[Anno].&[2011]"), "[Lavoratori].[Provenienza]",
           //                                 string.Format("{0},{1}", "[Measures].[Numero Prestazioni],[Tipi Prestazione].[Tipo Prestazione].[OldAndNew]", Members.Contains("[PREFED]") ? ",[Stato Domanda].[Stato Domanda].&[L]" : string.Empty), "Prestazioni");


           
           BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
           EndpointAddress endpointAddress = new EndpointAddress("http://localhost:1506/AuthenticationService.svc");
           ChannelFactory<IAuthenticationService> factory =
                   new ChannelFactory<IAuthenticationService>(basicHttpBinding, endpointAddress);

           factory.Open();
            IAuthenticationService client = factory.CreateChannel();
            User user = client.Authenticate("admin", "admin");
        }
    }
}
