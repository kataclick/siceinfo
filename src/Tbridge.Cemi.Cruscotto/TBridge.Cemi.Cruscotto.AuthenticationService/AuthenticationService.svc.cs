﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web.Security;

namespace TBridge.Cemi.Cruscotto.AuthenticationService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class AuthenticationService : IAuthenticationService
    {
        #region IAuthenticationService Members

        public User Authenticate(string Username, string Password)
        {
            User user = new User();
            user.LoggedIn = false;
            if (Membership.ValidateUser(Username, Password))
            {
                FormsAuthentication.SetAuthCookie(Username, false);

                MembershipUser userInfo = Membership.GetUser(Username);

                user.UserName = userInfo.UserName;
                user.Roles = Roles.GetRolesForUser(Username);
                user.LoggedIn = true;
            }
            return user;
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        #endregion
    }
}