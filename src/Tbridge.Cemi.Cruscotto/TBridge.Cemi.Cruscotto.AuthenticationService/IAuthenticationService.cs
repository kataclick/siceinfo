﻿using System.ServiceModel;

namespace TBridge.Cemi.Cruscotto.AuthenticationService
{
    // NOTE: If you change the interface name "ICubeService" here, you must also update the reference to "ICubeService" in Web.config.
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        User Authenticate(string Username, string Password);

        [OperationContract]
        void SignOut();
    }
}