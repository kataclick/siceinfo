﻿namespace TBridge.Cemi.Cruscotto.AuthenticationService
{
    public class User
    {
        public bool LoggedIn { get; set; }
        public string UserName { get; set; }
        public string[] Roles { get; set; }
    }
}