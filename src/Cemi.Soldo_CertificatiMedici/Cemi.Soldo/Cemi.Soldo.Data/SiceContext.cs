// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61

using Cemi.Soldo.Data.Mapping;
using Cemi.Soldo.Type.Domain;

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Cemi.Soldo.Data
{


    [System.CodeDom.Compiler.GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "2.19.1.0")]
    public partial class SiceContext : System.Data.Entity.DbContext
    {
        public System.Data.Entity.DbSet<Impresa> Imprese { get; set; } // Imprese
        public System.Data.Entity.DbSet<Lavoratore> Lavoratori { get; set; } // Lavoratori
        public System.Data.Entity.DbSet<MalattiaTelematicaAssenza> MalattiaTelematicaAssenze { get; set; } // MalattiaTelematicaAssenze
        public System.Data.Entity.DbSet<MalattiaTelematicaCertificatoMedico> MalattiaTelematicaCertificatiMedici { get; set; } // MalattiaTelematicaCertificatiMedici
        public System.Data.Entity.DbSet<MalattiaTelematicaCertificatoMedicoSoldo> MalattiaTelematicaCertificatiMediciSoldo { get; set; } // MalattiaTelematicaCertificatiMediciSoldo
        
        static SiceContext()
        {
            System.Data.Entity.Database.SetInitializer<SiceContext>(null);
        }

        public SiceContext()
            : base("Name=SICE")
        {
            InitializePartial();
        }

        public SiceContext(string connectionString)
            : base(connectionString)
        {
            InitializePartial();
        }

        public SiceContext(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model)
            : base(connectionString, model)
        {
            InitializePartial();
        }

        public SiceContext(System.Data.Common.DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
            InitializePartial();
        }

        public SiceContext(System.Data.Common.DbConnection existingConnection, System.Data.Entity.Infrastructure.DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
            InitializePartial();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public bool IsSqlParameterNull(System.Data.SqlClient.SqlParameter param)
        {
            var sqlValue = param.SqlValue;
            var nullableValue = sqlValue as System.Data.SqlTypes.INullable;
            if (nullableValue != null)
                return nullableValue.IsNull;
            return (sqlValue == null || sqlValue == System.DBNull.Value);
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ImpreseMap());
            modelBuilder.Configurations.Add(new LavoratoriMap());
            modelBuilder.Configurations.Add(new MalattiaTelematicaAssenzeMap());
            modelBuilder.Configurations.Add(new MalattiaTelematicaCertificatiMediciMap());
            modelBuilder.Configurations.Add(new MalattiaTelematicaCertificatiMediciSoldoMap());

            OnModelCreatingPartial(modelBuilder);
        }

        public static System.Data.Entity.DbModelBuilder CreateModel(System.Data.Entity.DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new ImpreseMap(schema));
            modelBuilder.Configurations.Add(new LavoratoriMap(schema));
            modelBuilder.Configurations.Add(new MalattiaTelematicaAssenzeMap(schema));
            modelBuilder.Configurations.Add(new MalattiaTelematicaCertificatiMediciMap(schema));
            modelBuilder.Configurations.Add(new MalattiaTelematicaCertificatiMediciSoldoMap(schema));
            return modelBuilder;
        }

        partial void InitializePartial();
        partial void OnModelCreatingPartial(System.Data.Entity.DbModelBuilder modelBuilder);
    }
}
// </auto-generated>
