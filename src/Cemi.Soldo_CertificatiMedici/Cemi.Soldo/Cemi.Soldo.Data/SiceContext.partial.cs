﻿namespace Cemi.Soldo.Data
{
    public partial class SiceContext
    {
        partial void InitializePartial()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
    }
}