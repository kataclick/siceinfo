﻿using AutoMapper;

namespace Cemi.Soldo.Utils
{
    public static class MyMapper<TSource, TDestination>
    {
        public static TDestination Map(TSource obj)
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<TSource, TDestination>());
            IMapper mapper = config.CreateMapper();

            TDestination ret = mapper.Map<TDestination>(obj);

            return ret;
        }
    }
}
