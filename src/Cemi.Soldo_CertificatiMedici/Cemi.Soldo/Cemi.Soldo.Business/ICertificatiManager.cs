﻿using System.Collections.Generic;
using Cemi.Soldo.Type.Dto;
using Cemi.Soldo.Type.Service;

namespace Cemi.Soldo.Business
{
    public interface ICertificatiManager
    {
        bool InsertCertificato(CertificatoMedicoSoldo certificatoMedicoSoldo);
        List<CertificatoSoldoAndAssenza> GetCertificatiSoldoAndAssenzaToMove();
        int MoveCertificatiSoldi(List<CertificatoSoldoAndAssenza> certificatiAssenzeToMove);
        int CheckAssenze(List<CertificatoSoldoAndAssenza> certificatiAssenze);
    }
}