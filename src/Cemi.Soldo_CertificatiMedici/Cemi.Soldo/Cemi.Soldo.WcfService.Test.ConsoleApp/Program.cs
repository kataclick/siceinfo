﻿using System;
using Cemi.Soldo.WcfService.Test.ConsoleApp.SoldoService;

namespace Cemi.Soldo.WcfService.Test.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine($"{DateTime.Now} - Inizio Procedura");

            CertificatiMediciSoldoServiceClient client = new CertificatiMediciSoldoServiceClient();
            bool ret = client.InsertCertificato(new CertificatoMedicoSoldo
            {
                CodiceFiscaleLavoratore = "BLKKML65L27Z352H",
                DataInizioMalattia = new DateTime(2015, 9, 11),
                DataFineMalattia = new DateTime(2015, 9, 15),
                DataRilascioCertificato = new DateTime(2015, 9, 11),
                IdImpresa = "00034834",
                IdLavoratore = "00815611",
                NumeroCertificato = "146606406",
                ScopertoDal = null,
                ScopertoAl = null,
                TipoEvento = TipoEvento.Inizio,
                TipoVisita = TipoVisita.Ambulatoriale
            });

            Console.WriteLine($"{DateTime.Now} - Risposta: {ret}");
            Console.WriteLine($"{DateTime.Now} - Fine Procedura");

            Console.ReadLine();
        }
    }
}