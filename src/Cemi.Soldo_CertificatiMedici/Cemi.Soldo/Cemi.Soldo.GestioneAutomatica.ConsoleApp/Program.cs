﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Cemi.Soldo.Business;
using Cemi.Soldo.Type.Dto;
using SimpleInjector;

namespace Cemi.Soldo.GestioneAutomatica.ConsoleApp
{
    class Program
    {
        private static Container _container;

        static void Main(string[] args)
        {
            Console.WriteLine($"{DateTime.Now} - Inizio procedura...");

            InitializeContainer();

            ICertificatiManager certificatiManager = _container.GetInstance<ICertificatiManager>();
            List<CertificatoSoldoAndAssenza> certificatiSoldoAndAssenza = certificatiManager.GetCertificatiSoldoAndAssenzaToMove();
            Console.WriteLine($"{DateTime.Now} - Certificati Soldo con Assenze: {certificatiSoldoAndAssenza.Count}");

            int numRec = certificatiManager.MoveCertificatiSoldi(certificatiSoldoAndAssenza);
            Console.WriteLine($"{DateTime.Now} - Record modificati con Certificati medici creati a partire da Soldo: {numRec}");

            numRec = certificatiManager.CheckAssenze(certificatiSoldoAndAssenza);
            Console.WriteLine($"{DateTime.Now} - Assenze verificate a partire da Soldo: {numRec}");

            Console.WriteLine($"{DateTime.Now} - Fine procedura");
        }

        private static void InitializeContainer()
        {
            _container = new Container();

            string name = string.Empty;
            var diConfig = ConfigurationManager.GetSection("MyDIConfig") as NameValueCollection;
            if (diConfig != null)
            {
                name = diConfig["ImplementationAssemblyList"];
            }
            List<string> listName = name.Split(';').ToList();
            foreach (string n in listName)
            {
                var impAssembly = Assembly.Load(n);

                if (impAssembly != null)
                {
                    var registrations =
                        from type in impAssembly.GetExportedTypes()
                        where type.GetInterfaces().Any()
                        select new { Services = type.GetInterfaces().ToList(), Implementation = type };

                    foreach (var reg in registrations)
                    {
                        foreach (var serv in reg.Services)
                        {
                            _container.Register(serv, reg.Implementation, Lifestyle.Singleton);
                        }
                    }
                }
            }
        }
    }
}
