﻿using System;
using System.Linq;
using Cemi.Soldo.Data;

namespace Cemi.MalattiaTelematica.Business.Common
{
    public class AssenzeManager
    {
        public bool IsAssenzaPrecedenteNonAccolta(int idAssenza, int idLavoratore, int idImpresa, DateTime inizioMalattia)
        {
            bool ret = false;

            using (var context = new SiceContext())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.IdImpresa == idImpresa
                        &&
                        mta.IdMalattiaTelematicaAssenza != idAssenza
                        &&
                        mta.DataInizioMalattia.Value == inizioMalattia
                        &&
                        mta.DataInizio.Value <= inizioMalattia
                        &&
                        (mta.Stato == "I" || mta.Stato == "9" || mta.Stato == "6" || mta.Stato == "7" ||
                         mta.Stato == "8")
                    select mta.Stato;

                if (query.Any())
                    ret = true;
            }

            return ret;
        }

        public bool IsAssenzaMancante(int idAssenza, int idLavoratore, DateTime inizioMalattia, DateTime inizio,
            bool ricaduta, string tipo)
        {
            bool ret = true;

            DateTime inizio2 = ricaduta ? inizio.AddDays(-30) : inizio.AddDays(-1);

            using (var context = new SiceContext())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.IdMalattiaTelematicaAssenza != idAssenza
                        &&
                        mta.DataInizioMalattia.Value == inizioMalattia
                        &&
                        mta.DataFine.Value >= inizio2
                        &&
                        mta.DataInizio < inizio
                        &&
                        mta.Tipo == tipo
                    select mta;

                if (query.Any())
                    ret = false;
            }

            return ret;
        }

        public bool IsPeriodiNonCongruenti(int idAssenza, DateTime inizio)
        {
            bool ret = false;

            using (var context = new SiceContext())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where
                        cert.MalattiaTelematicaAssenze.Any(ass => ass.IdMalattiaTelematicaAssenza == idAssenza)
                        &&
                        cert.DataInizio < inizio
                    select cert;

                if (query.Any())
                    ret = true;
            }

            return ret;
        }
    }
}