﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Cemi.Soldo.Type.Domain;

namespace Cemi.MalattiaTelematica.Business.Common
{
    public class CertificatiInpsManager
    {
        private static string PulisciRiga(string line)
        {
            return
                line.Replace("<TD>", string.Empty)
                    .Replace("</TD>", string.Empty)
                    .Replace("value=\"", string.Empty)
                    .Replace("\"", string.Empty)
                    .Replace("\t", string.Empty);
        }

        public MalattiaTelematicaCertificatoMedico GetCertificatoMedicoFromInps(string codiceFiscale, string numero)
        {
            MalattiaTelematicaCertificatoMedico cert = new MalattiaTelematicaCertificatoMedico();

            HttpWebRequest webRequest =
                (HttpWebRequest)
                    WebRequest.Create(
                        "https://serviziweb2.inps.it/AttestatiCittadinoWeb/attivaMain?cmd=immessoCertificato");


            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";

            using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                string corpo = $"codicefisc={codiceFiscale}&numerocert={numero}";
                //string corpo = String.Format("codicefisc={0}&numerocert={1}", "NDRTMS71P21H383I", "29547847");
                requestWriter.Write(corpo);
                requestWriter.Close();
            }

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            string responseData;
            using (StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                responseData = reader.ReadToEnd();
                reader.Close();
            }


            List<string> lines = responseData.Split(new[] {"\r\n"}, StringSplitOptions.None).ToList();

            bool valido = false;
            string tipo = "";

            if (PulisciRiga(lines[24]).Contains("Certificato di malattia telematico"))
            {
                if (PulisciRiga(lines[148]) != string.Empty)
                {
                    valido = true;

                    cert.DataRilascio = Convert.ToDateTime(PulisciRiga(lines[148]));
                    cert.DataInizio = Convert.ToDateTime(PulisciRiga(lines[139]));
                    cert.DataFine = Convert.ToDateTime(PulisciRiga(lines[157]));

                    if (PulisciRiga(lines[183]).Contains("checked"))
                    {
                        tipo = "I";
                    }
                    else if (PulisciRiga(lines[185]).Contains("checked"))
                    {
                        tipo = "C";
                    }
                    else if (PulisciRiga(lines[187]).Contains("checked"))
                    {
                        tipo = "R";
                    }
                }
            }
            else
            {
                if (PulisciRiga(lines[150]) != string.Empty)
                {
                    valido = true;

                    cert.DataRilascio = Convert.ToDateTime(PulisciRiga(lines[150]));
                    cert.DataInizio = Convert.ToDateTime(PulisciRiga(lines[116]));
                    cert.DataFine = Convert.ToDateTime(PulisciRiga(lines[136]));

                    if (PulisciRiga(lines[156]).Contains("checked"))
                    {
                        tipo = "I";
                    }
                    else if (PulisciRiga(lines[158]).Contains("checked"))
                    {
                        tipo = "C";
                    }
                    else if (PulisciRiga(lines[160]).Contains("checked"))
                    {
                        tipo = "R";
                    }
                }
            }

            if (valido)
            {
                cert.Tipo = tipo;
                cert.Numero = numero;
                cert.TipoAssenza = "MA";
                cert.NomeFile = "attestato.html";

                // estrapolo solamente la parte relativa al certificato
                int startIndex = responseData.IndexOf("<form method", StringComparison.Ordinal);
                int stopIndex = responseData.LastIndexOf("</form>", StringComparison.Ordinal);

                //calcolo la lunghezza
                int length = stopIndex + 7 - startIndex;

                var encoding = Encoding.UTF8;
                cert.Immagine = encoding.GetBytes(responseData.Substring(startIndex, length));
            }

            return cert;
        }

        public byte[] GetDocumentFromInps(string codiceFiscale, string numero)
        {
            return GetCertificatoMedicoFromInps(codiceFiscale, numero).Immagine;
        }
    }
}