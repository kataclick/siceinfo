// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61


#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Cemi.Soldo.Type.Domain
{

    // Lavoratori
    [System.CodeDom.Compiler.GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "2.19.1.0")]
    public partial class Lavoratore
    {
        public int IdLavoratore { get; set; } // idLavoratore (Primary key)
        public int? IdUtente { get; set; } // idUtente
        public string Cognome { get; set; } // cognome (length: 30)
        public string Nome { get; set; } // nome (length: 30)
        public string CodiceFiscale { get; set; } // codiceFiscale (length: 16)
        public System.DateTime? DataNascita { get; set; } // dataNascita
        public string LuogoNascita { get; set; } // luogoNascita (length: 35)
        public string IdNazionalita { get; set; } // idNazionalita (length: 3)
        public string Sesso { get; set; } // sesso (length: 1)
        public string IdStatoCivile { get; set; } // idStatoCivile (length: 1)
        public string IndirizzoDenominazione { get; set; } // indirizzoDenominazione (length: 67)
        public string IndirizzoCap { get; set; } // indirizzoCAP (length: 5)
        public string IndirizzoProvincia { get; set; } // indirizzoProvincia (length: 2)
        public string Telefono { get; set; } // telefono (length: 20)
        public string Cellulare { get; set; } // cellulare (length: 20)
        public string EMail { get; set; } // eMail (length: 50)
        public System.DateTime? DataDecesso { get; set; } // dataDecesso
        public string IdTipoDecesso { get; set; } // idTipoDecesso (length: 2)
        public string IdTipoPagamento { get; set; } // idTipoPagamento (length: 1)
        public string Pin { get; set; } // PIN (length: 8)
        public System.DateTime? DataGenerazionePin { get; set; } // dataGenerazionePIN
        public System.DateTime? DataInserimentoRecord { get; set; } // dataInserimentoRecord
        public string IndirizzoComune { get; set; } // indirizzoComune (length: 66)
        public string IdCassaEdile { get; set; } // idCassaEdile (length: 4)
        public System.DateTime? DataModificaRecord { get; set; } // dataModificaRecord
        public string IndirizzoComuneCodiceCatastale { get; set; } // indirizzoComuneCodiceCatastale (length: 4)
        public string CartaPrepagata { get; set; } // cartaPrepagata (length: 1)
        public string Bonifico { get; set; } // bonifico (length: 1)
        public string ProvinciaNascita { get; set; } // provinciaNascita (length: 2)
        public string CivicoIndirizzo { get; set; } // civicoIndirizzo (length: 10)
        public string CodiceAbi { get; set; } // codiceABI (length: 5)
        public string CodiceCab { get; set; } // codiceCAB (length: 5)
        public string CodiceCin { get; set; } // codiceCIN (length: 1)
        public string CodiceCinPaese { get; set; } // codiceCINPaese (length: 2)
        public string CodicePaese { get; set; } // codicePaese (length: 2)
        public System.DateTime? DataFineValiditaIndirizzo { get; set; } // dataFineValiditaIndirizzo
        public System.DateTime? DataInizioValiditaIndirizzo { get; set; } // dataInizioValiditaIndirizzo
        public string DenominazioneIndirizzo { get; set; } // denominazioneIndirizzo (length: 40)
        public string Fax { get; set; } // fax (length: 20)
        public string IdTipoVia { get; set; } // idTipoVia (length: 3)
        public string NumeroCc { get; set; } // numeroCC (length: 15)
        public string PosizioneValida { get; set; } // posizioneValida (length: 1)
        public string Presso { get; set; } // presso (length: 30)
        public string SegnaleCodiceFiscale { get; set; } // segnaleCodiceFiscale (length: 1)
        public System.DateTime? DataInvioSmsPin { get; set; } // dataInvioSmsPin
        public string IbanEstero { get; set; } // ibanEstero (length: 40)

        // Reverse navigation
        public virtual System.Collections.Generic.ICollection<MalattiaTelematicaCertificatoMedico> MalattiaTelematicaCertificatiMedicis { get; set; } // MalattiaTelematicaCertificatiMedici.FK_MalattiaTelematicaCertificatiMedici_Lavoratori
        
        public Lavoratore()
        {
            DataInserimentoRecord = System.DateTime.Now;
            MalattiaTelematicaCertificatiMedicis = new System.Collections.Generic.List<MalattiaTelematicaCertificatoMedico>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
