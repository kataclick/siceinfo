﻿namespace Cemi.Soldo.Type.Dto
{
    public class CertificatoSoldoAndAssenza
    {
        public int IdCertificatoSoldo { get; set; }
        public int IdAssenza { get; set; }
    }
}
