﻿namespace Cemi.Soldo.Type.Enum
{
    public enum TipoEvento
    {
        Inizio = 0,
        Continuazione,
        Ricaduta
    }
}
