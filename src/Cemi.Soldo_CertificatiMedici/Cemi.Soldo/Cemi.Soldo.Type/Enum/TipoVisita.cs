﻿namespace Cemi.Soldo.Type.Enum
{
    public enum TipoVisita
    {
        Ambulatoriale = 0,
        Domiciliare,
        ProntoSoccorso
    }
}
