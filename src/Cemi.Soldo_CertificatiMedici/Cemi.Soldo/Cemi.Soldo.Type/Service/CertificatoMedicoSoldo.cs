﻿using System;
using System.Runtime.Serialization;
using Cemi.Soldo.Type.Enum;

namespace Cemi.Soldo.Type.Service
{
    [DataContract]
    public class CertificatoMedicoSoldo
    {
        [DataMember]
        public string IdLavoratore { get; set; }

        [DataMember]
        public string CodiceFiscaleLavoratore { get; set; }

        [DataMember]
        public string IdImpresa { get; set; }

        [DataMember]
        public string NumeroCertificato { get; set; }

        [DataMember]
        public DateTime DataInizioMalattia { get; set; }

        [DataMember]
        public DateTime DataRilascioCertificato { get; set; }

        [DataMember]
        public DateTime DataFineMalattia { get; set; }

        [DataMember]
        public TipoVisita TipoVisita { get; set; }

        [DataMember]
        public TipoEvento TipoEvento { get; set; }

        [DataMember]
        public DateTime? ScopertoDal { get; set; }

        [DataMember]
        public DateTime? ScopertoAl { get; set; }
    }
}