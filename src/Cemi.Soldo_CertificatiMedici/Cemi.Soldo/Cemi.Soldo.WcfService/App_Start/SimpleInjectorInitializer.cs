using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Cemi.Soldo.WcfService;
using SimpleInjector;
using SimpleInjector.Integration.Wcf;
using WebActivator;

[assembly: PostApplicationStartMethod(typeof(SimpleInjectorInitializer), "Initialize")]

namespace Cemi.Soldo.WcfService
{
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it for the WCF ServiceHostFactory.</summary>
        public static void Initialize()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WcfOperationLifestyle();

            InitializeContainer(container);

            container.RegisterWcfServices(Assembly.GetExecutingAssembly());

            container.Verify();

            SimpleInjectorServiceHostFactory.SetContainer(container);

            // TODO: Add the following attribute to all .svc files:
            // Factory="SimpleInjector.Integration.Wcf.SimpleInjectorServiceHostFactory, SimpleInjector.Integration.Wcf"
        }

        private static void InitializeContainer(Container container)
        {
            string name = string.Empty;
            var diConfig = ConfigurationManager.GetSection("MyDIConfig") as NameValueCollection;
            if (diConfig != null)
            {
                name = diConfig["ImplementationAssemblyList"];
            }
            List<string> listName = name.Split(';').ToList();
            foreach (string n in listName)
            {
                var impAssembly = Assembly.Load(n);

                if (impAssembly != null)
                {
                    var registrations =
                        from type in impAssembly.GetExportedTypes()
                        where type.GetInterfaces().Any()
                        select new {Services = type.GetInterfaces().ToList(), Implementation = type};

                    foreach (var reg in registrations)
                    {
                        foreach (var serv in reg.Services)
                        {
                            container.Register(serv, reg.Implementation, Lifestyle.Transient);
                        }
                    }
                }
            }
        }
    }
}