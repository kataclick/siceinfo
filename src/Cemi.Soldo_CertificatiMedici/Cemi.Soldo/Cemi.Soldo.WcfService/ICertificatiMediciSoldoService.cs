﻿using System.ServiceModel;
using Cemi.Soldo.Type.Service;

namespace Cemi.Soldo.WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICertificatiMediciSoldoService" in both code and config file together.
    [ServiceContract]
    public interface ICertificatiMediciSoldoService
    {
        [OperationContract]
        bool InsertCertificato(CertificatoMedicoSoldo certificatoMedicoSoldo);
    }
}