﻿using System;
using System.ServiceModel.Activation;
using Cemi.Soldo.Business;
using Cemi.Soldo.Type.Service;
using Cemi.Soldo.WcfService.Elmah;

namespace Cemi.Soldo.WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CertificatiMediciSoldoService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CertificatiMediciSoldoService.svc or CertificatiMediciSoldoService.svc.cs at the Solution Explorer and start debugging.
    [ServiceErrorBehavior(typeof(ElmahErrorHandler))]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CertificatiMediciSoldoService : ICertificatiMediciSoldoService
    {
        private readonly ICertificatiManager _certificatiManager;

        public CertificatiMediciSoldoService(ICertificatiManager certificatiManager)
        {
            _certificatiManager = certificatiManager;
        }

        public bool InsertCertificato(CertificatoMedicoSoldo certificatoMedicoSoldo)
        {
            bool response = _certificatiManager.InsertCertificato(certificatoMedicoSoldo);
            return response;
        }
    }
}