﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cemi.MalattiaTelematica.Business.Common;
using Cemi.Soldo.Data;
using Cemi.Soldo.Type.Domain;
using Cemi.Soldo.Type.Dto;
using Cemi.Soldo.Type.Enum;
using Cemi.Soldo.Type.Service;
using Cemi.Soldo.Utils;

namespace Cemi.Soldo.Business.Imp
{
    public class CertificatiManager : ICertificatiManager
    {
        public bool InsertCertificato(CertificatoMedicoSoldo certificatoMedicoSoldo)
        {
            int numChanges;

            MalattiaTelematicaCertificatoMedicoSoldo certificatoMedicoToStore =
                MyMapper<CertificatoMedicoSoldo, MalattiaTelematicaCertificatoMedicoSoldo>.Map(certificatoMedicoSoldo);
            //certificatoMedicoToStore.DateLastUpdate = DateTime.Now;

            using (var context = new SiceContext())
            {
                context.MalattiaTelematicaCertificatiMediciSoldo.Add(certificatoMedicoToStore);

                numChanges = context.SaveChanges();
            }

            return numChanges > 0;
        }

        public List<CertificatoSoldoAndAssenza> GetCertificatiSoldoAndAssenzaToMove()
        {
            List<CertificatoSoldoAndAssenza> certificatiAssenzeToMove =
                new List<CertificatoSoldoAndAssenza>();

            using (var context = new SiceContext())
            {
                List<MalattiaTelematicaCertificatoMedicoSoldo> certificatiSoldo =
                    (from certSoldo in context.MalattiaTelematicaCertificatiMediciSoldo
                        where certSoldo.DataGestioneSiceInfo == null
                        orderby certSoldo.IdImpresa, certSoldo.CodiceFiscaleLavoratore,
                            certSoldo.DataInizioMalattia ascending
                        select certSoldo).ToList();

                foreach (MalattiaTelematicaCertificatoMedicoSoldo medicoSoldo in certificatiSoldo)
                {
                    //verifico Lav e nel caso lo setto
                    List<int> idLavList = (from lav in context.Lavoratori
                        where lav.CodiceFiscale == medicoSoldo.CodiceFiscaleLavoratore
                        select lav.IdLavoratore).ToList();

                    if (idLavList.Count == 1)
                    {
                        medicoSoldo.IdLavoratore = idLavList[0];

                        List<int> assenzeId = (from mtAssenza in context.MalattiaTelematicaAssenze
                                               where
                                                   mtAssenza.IdImpresa == medicoSoldo.IdImpresa &&
                                                   mtAssenza.IdLavoratore == medicoSoldo.IdLavoratore &&
                                                   mtAssenza.DataInizio <= medicoSoldo.DataInizioMalattia &&
                                                   mtAssenza.DataFine >= medicoSoldo.DataInizioMalattia
                                               select mtAssenza.IdMalattiaTelematicaAssenza).ToList();

                        if (assenzeId.Count > 0)
                        {
                            certificatiAssenzeToMove.Add(new CertificatoSoldoAndAssenza
                            {
                                IdAssenza = assenzeId.First(),
                                IdCertificatoSoldo = medicoSoldo.Id
                            });
                        }
                    }
                }

                int numRec = context.SaveChanges();

                /*
                var query = from certSoldo in context.MalattiaTelematicaCertificatiMediciSoldo
                    join lav in context.Lavoratori on certSoldo.CodiceFiscaleLavoratore equals lav.CodiceFiscale
                    where certSoldo.DataGestioneSiceInfo == null
                    orderby certSoldo.IdImpresa, certSoldo.CodiceFiscaleLavoratore, certSoldo.DataInizioMalattia ascending
                    select
                        new
                        {
                            certSoldo.Id,
                            certSoldo.IdImpresa,
                            certSoldo.CodiceFiscaleLavoratore,
                            lav.IdLavoratore,
                            certSoldo.DataInizioMalattia
                        };

                var certificatiSoldo = query.Distinct().ToList();

                foreach (var certificatoSoldo in certificatiSoldo)
                {
                    List<int> assenzeId = (from mtAssenza in context.MalattiaTelematicaAssenze
                        where
                            mtAssenza.IdImpresa == certificatoSoldo.IdImpresa &&
                            mtAssenza.IdLavoratore == certificatoSoldo.IdLavoratore &&
                            mtAssenza.DataInizio <= certificatoSoldo.DataInizioMalattia &&
                            mtAssenza.DataFine >= certificatoSoldo.DataInizioMalattia
                        select mtAssenza.IdMalattiaTelematicaAssenza).ToList();

                    if (assenzeId.Count > 0)
                    {
                        certificatiAssenzeToMove.Add(new CertificatoSoldoAndAssenza
                        {
                            IdAssenza = assenzeId.First(),
                            IdCertificatoSoldo = certificatoSoldo.Id
                        });
                    }
                }
                */
            }

            return certificatiAssenzeToMove;
        }

        public int MoveCertificatiSoldi(List<CertificatoSoldoAndAssenza> certificatiAssenzeToMove)
        {
            int numRec;
            CertificatiInpsManager certificatiInpsManager = new CertificatiInpsManager();

            using (var context = new SiceContext())
            {
                foreach (CertificatoSoldoAndAssenza certificatoSoldoAndAssenzaToMove in certificatiAssenzeToMove)
                {
                    //var certSoldoAndLav =
                    //    (from medicoSoldo in context.MalattiaTelematicaCertificatiMediciSoldo
                    //     join lav in context.Lavoratori on medicoSoldo.CodiceFiscaleLavoratore equals lav.CodiceFiscale
                    //     where medicoSoldo.Id == certificatoSoldoAndAssenzaToMove.IdCertificatoSoldo
                    //     select new {medicoSoldo, lav.IdLavoratore}).Single();

                    MalattiaTelematicaCertificatoMedicoSoldo certSoldo =
                        context.MalattiaTelematicaCertificatiMediciSoldo.Single(
                            x => x.Id == certificatoSoldoAndAssenzaToMove.IdCertificatoSoldo);

                    MalattiaTelematicaAssenza assenza =
                        context.MalattiaTelematicaAssenze.Single(
                            x => x.IdMalattiaTelematicaAssenza == certificatoSoldoAndAssenzaToMove.IdAssenza);

                    MalattiaTelematicaCertificatoMedico certificatoMedico =
                        (from certificato in context.MalattiaTelematicaCertificatiMedici
                            where
                                certificato.Numero == certSoldo.NumeroCertificato &&
                                certificato.IdLavoratore == certSoldo.IdLavoratore.Value
                            select certificato).SingleOrDefault() ?? new MalattiaTelematicaCertificatoMedico();

                    certificatoMedico.IdImpresa = certSoldo.IdImpresa;
                    certificatoMedico.IdLavoratore = certSoldo.IdLavoratore.Value;
                    certificatoMedico.DataInizio = certSoldo.DataInizioMalattia;
                    certificatoMedico.DataFine = certSoldo.DataFineMalattia;
                    certificatoMedico.DataRilascio = certSoldo.DataRilascioCertificato;
                    certificatoMedico.Numero = certSoldo.NumeroCertificato;
                    certificatoMedico.TipoAssenza = "MA";
                    certificatoMedico.Ricovero = null;
                    certificatoMedico.Tipo = certSoldo.TipoEvento.ToString().First().ToString();

                    certificatoMedico.MalattiaTelematicaAssenze.Add(assenza);

                    try
                    {
                        certificatoMedico.Immagine =
                            certificatiInpsManager.GetDocumentFromInps(certSoldo.CodiceFiscaleLavoratore,
                                certSoldo.NumeroCertificato);
                        certificatoMedico.NomeFile = "attestato.html";
                    }
                    catch (Exception)
                    {
                        certificatoMedico.Immagine = null;
                        certificatoMedico.NomeFile = string.Empty;
                    }

                    certificatoMedico.DataModificaRecord = DateTime.Now;


                    context.MalattiaTelematicaCertificatiMedici.Add(certificatoMedico);

                    certSoldo.DataGestioneSiceInfo = DateTime.Now;
                }

                numRec = context.SaveChanges();
            }

            return numRec;
        }

        public int CheckAssenze(List<CertificatoSoldoAndAssenza> certificatiAssenze)
        {
            int numRec;
            AssenzeManager assenzeManager = new AssenzeManager();

            using (var context = new SiceContext())
            {
                foreach (CertificatoSoldoAndAssenza certificatoAssenza in certificatiAssenze)
                {
                    MalattiaTelematicaAssenza assenza =
                        context.MalattiaTelematicaAssenze.Single(x => x.IdMalattiaTelematicaAssenza == certificatoAssenza.IdAssenza);

                    MalattiaTelematicaCertificatoMedicoSoldo certificatoSoldo =
                        context.MalattiaTelematicaCertificatiMediciSoldo.Single(
                            x => x.Id == certificatoAssenza.IdCertificatoSoldo);
                    
                    bool assenzaPrecedenteNonAccolta = assenzeManager.IsAssenzaPrecedenteNonAccolta(assenza.IdMalattiaTelematicaAssenza, assenza.IdLavoratore,
                        assenza.IdImpresa, assenza.DataInizioMalattia.Value);

                    bool assenzaMancante = false;
                    if (certificatoSoldo.DataInizioMalattia != assenza.DataInizio)
                    {
                        bool ricaduta = certificatoSoldo.TipoEvento == TipoEvento.Ricaduta;
                        assenzaMancante = assenzeManager.IsAssenzaMancante(assenza.IdMalattiaTelematicaAssenza,
                            assenza.IdLavoratore, assenza.DataInizioMalattia.Value, assenza.DataInizio.Value, ricaduta,
                            assenza.Tipo);
                    }

                    bool periodiCongruenti = assenzeManager.IsPeriodiNonCongruenti(assenza.IdMalattiaTelematicaAssenza,
                        assenza.DataInizioMalattia.Value);

                    if (assenzaPrecedenteNonAccolta || periodiCongruenti || assenzaMancante)
                    {
                        assenza.Stato = "9";
                    }
                    else
                    {
                        assenza.Stato = "C";
                    }
                }

                numRec = context.SaveChanges();
            }

            return numRec;
        }
    }
}