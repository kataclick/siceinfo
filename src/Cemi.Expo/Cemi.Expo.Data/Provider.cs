﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Cemi.Expo.Type.Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.Expo.Data
{
    public class Provider
    {
        public Provider()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public List<CorsoLavoratore> GetCorsiByCf(string codiceFiscale)
        {
            List<CorsoLavoratore> listaCorsi = new List<CorsoLavoratore>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ExpoLavoratoriCorsiSelectByCf"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        CorsoLavoratore corso = new CorsoLavoratore
                            {
                                CodiceFiscale = codiceFiscale,
                                Ente = (string) reader["ente"],
                                Titolo = (string) reader["titoloCorso"],
                                DataAttestazione = (DateTime) reader["dataAttestazione"]
                            };

                        listaCorsi.Add(corso);
                    }
                }
            }

            return listaCorsi;
        }

        public List<InfoLavoratore> GetInfoLavoratoreByCf(string codiceFiscale)
        {
            List<InfoLavoratore> listaLav = new List<InfoLavoratore>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ExpoStatoLavoratoreSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        InfoLavoratore lav = new InfoLavoratore
                        {
                            CodiceFiscale = codiceFiscale,
                            CassaEdileIscrizione = reader["cassaIscrizione"] as string,
                            IdLavoratore = reader["idLavoratore"] as int?,
                            DataNascita = reader["dataNascita"] as DateTime?,
                            Cognome = reader["cognome"] as string,
                            Nome = reader["nome"] as string,
                            Sesso = reader["sesso"] as string,
                            ComuneNascita = reader["comuneNascita"] as string,
                            ComuneNascitaCodCatastale = reader["comuneNascitaCodCatastale"] as string
                        };

                        listaLav.Add(lav);
                    }
                }
            }

            return listaLav;
        }

        public List<InfoImpresa> GetInfoImpresaByPiva(string partitaIva)
        {
            List<InfoImpresa> listaImp = new List<InfoImpresa>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ExpoStatoImpresaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        InfoImpresa imp = new InfoImpresa
                        {
                            PartitaIva = partitaIva,
                            IdImpresa = reader["idImpresa"] as int?,
                            Stato = reader["stato"] as string,
                            RichiestaIscrizione = (bool) reader["richiestaIscrizione"]
                        };

                        listaImp.Add(imp);
                    }
                }
            }

            return listaImp;
        }
    }
}
