﻿using System.Configuration;
using Cemi.Expo.Business;

namespace Cemi.Expo.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (ConfigurationManager.AppSettings["GestioneCorsi"] == "true")
            {
                CorsiManager corsiManager = new CorsiManager();
                corsiManager.GestisciRichieste();
            }

            if (ConfigurationManager.AppSettings["GestioneImprese"] == "true")
            {
                ImpreseManager impreseManager = new ImpreseManager();
                impreseManager.GestisciRichieste();
            }

            if (ConfigurationManager.AppSettings["GestioneLavoratori"] == "true")
            {
                LavoratoriManager lavoratoriManager = new LavoratoriManager();
                lavoratoriManager.GestisciRichieste();
            }
        }
    }
}