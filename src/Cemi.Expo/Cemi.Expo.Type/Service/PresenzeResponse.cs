﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class PresenzeResponse
    {
        [DataMember]
        public bool Corretto
        {
            get { return (CfErrori.Count == 0); }
            set { }
        }

        [DataMember]
        public List<string> CfErrori { get; set; }

        public PresenzeResponse()
        {
            CfErrori = new List<string>();
        }
    }
}
