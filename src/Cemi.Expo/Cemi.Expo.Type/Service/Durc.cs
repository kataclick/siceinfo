﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class Durc
    {
        [DataMember]
        public string Cip { get; set; }

        [DataMember]
        public string PartivaIvaEsecutrice { get; set; }

        [DataMember]
        public string CodiceDurc { get; set; }  //NumeroProtocollo

        [DataMember]
        public DateTime DataEmissione { get; set; }

        [DataMember]
        public bool Regolare { get; set; }
    }
}
