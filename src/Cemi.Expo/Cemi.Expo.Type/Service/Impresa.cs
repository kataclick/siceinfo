﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class Impresa
    {
        [DataMember]
        public String PartitaIva { get; set; }

        [DataMember]
        public String RagioneSociale { get; set; }

        [DataMember]
        public Indirizzo Indirizzo { get; set; }
    }
}
