﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class Indirizzo
    {
        [DataMember]
        public String Via { get; set; }

        [DataMember]
        public String Comune { get; set; }

        [DataMember]
        public String Cap { get; set; }

        [DataMember]
        public String Provincia { get; set; }

        [DataMember]
        public String Stato { get; set; }
    }
}
