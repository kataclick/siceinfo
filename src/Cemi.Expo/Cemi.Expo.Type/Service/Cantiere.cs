﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class Cantiere
    {
        [DataMember]
        public Impresa ImpresaAffidataria { get; set; }

        [DataMember]
        public Impresa ImpresaEsecutrice { get; set; }

        [DataMember]
        public Impresa ImpresaSubappaltatrice { get; set; }

        [DataMember]
        public DateTime DataInizioAttivita { get; set; }

        [DataMember]
        public DateTime DataFineAttivita { get; set; }

        [DataMember]
        public String ContrattoEsecutore { get; set; }

        [DataMember]
        public String TipologiaLavori { get; set; }

        [DataMember]
        public String NumeroNotificaPreliminare { get; set; }

    }
}
