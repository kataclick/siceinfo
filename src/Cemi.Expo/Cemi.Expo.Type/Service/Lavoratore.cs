﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class Lavoratore
    {
        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String Nome { get; set; }

        [DataMember]
        public String Cognome { get; set; }

        [DataMember]
        public DateTime DataNascita { get; set; }

        [DataMember]
        public String ComuneNascita { get; set; }

        [DataMember]
        public String Sesso { get; set; }

        [DataMember]
        public DateTime DataAssunzione { get; set; }

        [DataMember]
        public String PartitaIvaAffidatario { get; set; }

        [DataMember]
        public String PartitaIvaImpresa { get; set; }

        [DataMember]
        public String GateAccesso { get; set; }

        [DataMember]
        public String TipologiaLavori { get; set; }

        [DataMember]
        public String ContrattoCcnl { get; set; }

        [DataMember]
        public String Qualifica { get; set; }

        [DataMember]
        public String Mansione { get; set; }

        [DataMember]
        public String Categoria { get; set; }

        [DataMember]
        public List<DateTime> ListaPresenze { get; set; }
    }
}
