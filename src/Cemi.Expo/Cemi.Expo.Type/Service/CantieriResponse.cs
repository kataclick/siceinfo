﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cemi.Expo.Type.Service
{
    [DataContract]
    public class CantieriResponse
    {
        [DataMember]
        public bool Corretto
        {
            get { return (PivaErrori.Count == 0); }
            set {}
        }

        [DataMember]
        public List<string> PivaErrori { get; set; }

        public CantieriResponse()
        {
            PivaErrori = new List<string>();
        }
    }
}
