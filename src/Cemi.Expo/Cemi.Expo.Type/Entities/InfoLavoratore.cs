﻿using System;

namespace Cemi.Expo.Type.Entities
{
    public class InfoLavoratore
    {
        public string CodiceFiscale { get; set; }

        public string CassaEdileIscrizione { get; set; }

        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string ComuneNascita { get; set; }

        public string ComuneNascitaCodCatastale { get; set; }

        public string Sesso { get; set; }
    }
}