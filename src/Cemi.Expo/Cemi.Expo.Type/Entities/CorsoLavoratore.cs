﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Expo.Type.Entities
{
    public class CorsoLavoratore
    {
        public string CodiceFiscale { get; set; }

        public string Ente { get; set; }

        public string Titolo { get; set; }

        public DateTime DataAttestazione { get; set; }
    }
}
