﻿namespace Cemi.Expo.Type.Entities
{
    public class InfoImpresa
    {
        public string PartitaIva { get; set; }

        public int? IdImpresa { get; set; }

        public string Stato { get; set; }

        public bool RichiestaIscrizione { get; set; }
    }
}
