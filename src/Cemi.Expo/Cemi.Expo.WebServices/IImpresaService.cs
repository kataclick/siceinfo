﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Cemi.Expo.WebServices
{
    [ServiceContract]
    public interface IImpresaService
    {
        [OperationContract]
        int SetInfoIscrizioneImpresa(List<String> partiteIva);
    }
}