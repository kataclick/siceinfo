﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.WebServices
{
    [ServiceContract]
    public interface IPresenzeService
    {
        [OperationContract]
        PresenzeResponse SetPresenze(List<Lavoratore> listaLavoratoriWs);
    }

    
}
