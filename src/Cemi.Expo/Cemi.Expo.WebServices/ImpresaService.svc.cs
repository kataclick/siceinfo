﻿using System;
using System.Collections.Generic;
using Cemi.Expo.Business;

namespace Cemi.Expo.WebServices
{
    public class ImpresaService : IImpresaService
    {
        private readonly ImpreseManager _impreseManager = new ImpreseManager();

        public int SetInfoIscrizioneImpresa(List<string> partiteIva)
        {
            return _impreseManager.InsertRequest(partiteIva);
        }
    }
}
