﻿using System;
using System.Collections.Generic;
using Cemi.Expo.Business;

namespace Cemi.Expo.WebServices
{
    public class LavoratoreService : ILavoratoreService
    {
        private readonly LavoratoriManager _lavoratoriManager = new LavoratoriManager();

        public int SetInfoLavoratore(List<string> codiciFiscaliLavoratori)
        {
            return _lavoratoriManager.InsertRequest(codiciFiscaliLavoratori);
        }
    }
}