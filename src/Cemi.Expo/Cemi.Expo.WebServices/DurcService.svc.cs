﻿using System.Collections.Generic;
using Cemi.Expo.Business;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.WebServices
{
    public class DurcService : IDurcService
    {
        private readonly DurcManager _durcManager = new DurcManager();

        #region IDurcService Members

        public List<Durc> GetDurcInfo(List<string> listaCip)
        {
            return _durcManager.GetDurc(listaCip);
        }

        #endregion
    }
}