﻿using System.Collections.Generic;
using Cemi.Expo.Business;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.WebServices
{
    public class PresenzeService : IPresenzeService
    {
        private readonly PresenzeManager _presenzeManager = new PresenzeManager();

        #region IPresenzeService Members

        public PresenzeResponse SetPresenze(List<Lavoratore> listaLavoratoriWs)
        {
            return _presenzeManager.InsertPresenze(listaLavoratoriWs);
        }

        #endregion
    }
}