﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.WebServices
{
    [ServiceContract]
    public interface ICantieriService
    {
        [OperationContract]
        CantieriResponse SetCantieri(List<Cantiere> listaCantieriWs);
    }
}
