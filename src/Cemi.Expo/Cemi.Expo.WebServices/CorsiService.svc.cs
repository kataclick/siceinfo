﻿using System;
using System.Collections.Generic;
using Cemi.Expo.Business;

namespace Cemi.Expo.WebServices
{
    public class CorsiService : ICorsiService
    {
        private readonly CorsiManager _corsiManager = new CorsiManager();

        #region ICorsiService Members

        public int SetInfoCorso(List<string> codiciFiscaleLavoratori)
        {
            return _corsiManager.InsertRequest(codiciFiscaleLavoratori);
        }

        #endregion
    }
}
