﻿using System.Collections.Generic;
using Cemi.Expo.Business;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.WebServices
{
    public class CantieriService : ICantieriService
    {
        private readonly CantieriManager _cantieriManager = new CantieriManager();

        #region ICantieriService Members

        public CantieriResponse SetCantieri(List<Cantiere> listaCantieriWs)
        {
            return _cantieriManager.InsertCantieri(listaCantieriWs);
        }

        #endregion
    }
}
