﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Cemi.Expo.WebServices
{
    [ServiceContract]
    public interface ICorsiService
    {
        [OperationContract]
        int SetInfoCorso(List<String> codiciFiscaleLavoratori);
    }
}
