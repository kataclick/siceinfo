﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Cemi.Expo.WebServices
{
    [ServiceContract]
    public interface ILavoratoreService
    {
        [OperationContract]
        int SetInfoLavoratore(List<String> codiciFiscaliLavoratori);
    }
}