﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Runtime.Serialization;
using Cemi.Expo.Type.Service;
using Cemi.Expo.WebService.TestApp.CantieriServiceReference;

namespace Cemi.Expo.WebService.TestApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            /*
            CantieriServiceClient clientCantieri = new CantieriServiceClient();

            Cantiere cantiereProva = new Cantiere();

            cantiereProva.ContrattoEsecutore = "068";
            cantiereProva.DataFineAttivita = new DateTime(2000, 1, 1);
            cantiereProva.DataInizioAttivita = new DateTime(2000, 1, 1);

            cantiereProva.ImpresaAffidataria = new Impresa();
            cantiereProva.ImpresaAffidataria.Indirizzo = new Indirizzo();
            cantiereProva.ImpresaAffidataria.Indirizzo.Cap = "20123";
            cantiereProva.ImpresaAffidataria.Indirizzo.Comune = "Milano";
            cantiereProva.ImpresaAffidataria.Indirizzo.Provincia = "MI";
            cantiereProva.ImpresaAffidataria.Indirizzo.Stato = "IT";
            cantiereProva.ImpresaAffidataria.Indirizzo.Via = "Piazza degli Affari2";

            cantiereProva.ImpresaAffidataria.PartitaIva = "02682060120";
            cantiereProva.ImpresaAffidataria.RagioneSociale = "Telecom Italia S.p.a.";

            cantiereProva.ImpresaEsecutrice = new Impresa();
            cantiereProva.ImpresaEsecutrice.Indirizzo = new Indirizzo();
            cantiereProva.ImpresaEsecutrice.Indirizzo.Cap = "20123";
            cantiereProva.ImpresaEsecutrice.Indirizzo.Comune = "Milano";
            cantiereProva.ImpresaEsecutrice.Indirizzo.Provincia = "MI";
            cantiereProva.ImpresaEsecutrice.Indirizzo.Stato = "IT";
            cantiereProva.ImpresaEsecutrice.Indirizzo.Via = "Piazza degli Affari2";

            cantiereProva.ImpresaEsecutrice.PartitaIva = "02682060120";
            cantiereProva.ImpresaEsecutrice.RagioneSociale = "Telecom Italia S.p.a.";

            //cantiereProva.ImpresaSubappaltatrice = new Impresa();
            //cantiereProva.ImpresaSubappaltatrice.Indirizzo = new Indirizzo();
            //cantiereProva.ImpresaSubappaltatrice.Indirizzo.Cap = "20123";
            //cantiereProva.ImpresaSubappaltatrice.Indirizzo.Comune = "Milano";
            //cantiereProva.ImpresaSubappaltatrice.Indirizzo.Provincia = "MI";
            //cantiereProva.ImpresaSubappaltatrice.Indirizzo.Stato = "IT";
            //cantiereProva.ImpresaSubappaltatrice.Indirizzo.Via = "Piazza degli Affari2";

            //cantiereProva.ImpresaSubappaltatrice.PartitaIva = "02682060120";
            //cantiereProva.ImpresaSubappaltatrice.RagioneSociale = "Telecom Italia S.p.a.";

            cantiereProva.NumeroNotificaPreliminare = "50/2012";

            cantiereProva.TipologiaLavori = "23.61.00";


            Cantiere cantiereProva2 = new Cantiere();

            cantiereProva2.ContrattoEsecutore = "068";
            cantiereProva2.DataFineAttivita = new DateTime(2000, 1, 1);
            cantiereProva2.DataInizioAttivita = new DateTime(2000, 1, 1);
            
            cantiereProva2.ImpresaAffidataria = new Impresa();
            cantiereProva2.ImpresaAffidataria.Indirizzo = new Indirizzo();
            cantiereProva2.ImpresaAffidataria.Indirizzo.Cap = "20123";
            cantiereProva2.ImpresaAffidataria.Indirizzo.Comune = "Milano";
            cantiereProva2.ImpresaAffidataria.Indirizzo.Provincia = "MI";
            cantiereProva2.ImpresaAffidataria.Indirizzo.Stato = "IT";
            cantiereProva2.ImpresaAffidataria.Indirizzo.Via = "Piazza degli Affari2";
            
            cantiereProva2.ImpresaAffidataria.PartitaIva = "02682060120";
            cantiereProva2.ImpresaAffidataria.RagioneSociale = "Telecom Italia S.p.a.";
            
            cantiereProva2.ImpresaEsecutrice = new Impresa();
            cantiereProva2.ImpresaEsecutrice.Indirizzo = new Indirizzo();
            cantiereProva2.ImpresaEsecutrice.Indirizzo.Cap = "20123";
            cantiereProva2.ImpresaEsecutrice.Indirizzo.Comune = "Milano";
            cantiereProva2.ImpresaEsecutrice.Indirizzo.Provincia = "MI";
            cantiereProva2.ImpresaEsecutrice.Indirizzo.Stato = "IT";
            cantiereProva2.ImpresaEsecutrice.Indirizzo.Via = "Piazza degli Affari2";
            
            cantiereProva2.ImpresaEsecutrice.PartitaIva = "02682060120";
            cantiereProva2.ImpresaEsecutrice.RagioneSociale = "Telecom Italia S.p.a.";
            
            cantiereProva2.ImpresaSubappaltatrice = new Impresa();
            cantiereProva2.ImpresaSubappaltatrice.Indirizzo = new Indirizzo();
            cantiereProva2.ImpresaSubappaltatrice.Indirizzo.Cap = "20123";
            cantiereProva2.ImpresaSubappaltatrice.Indirizzo.Comune = "Milano";
            cantiereProva2.ImpresaSubappaltatrice.Indirizzo.Provincia = "MI";
            cantiereProva2.ImpresaSubappaltatrice.Indirizzo.Stato = "IT";
            cantiereProva2.ImpresaSubappaltatrice.Indirizzo.Via = "Piazza degli Affari2";
            
            cantiereProva2.ImpresaSubappaltatrice.PartitaIva = "02682060120";
            cantiereProva2.ImpresaSubappaltatrice.RagioneSociale = "Telecom Italia S.p.a.";
            
            cantiereProva2.NumeroNotificaPreliminare = "50/2012";
            
            cantiereProva2.TipologiaLavori = "23.61.00";


            List<Cantiere> listaCantieri = new List<Cantiere>();
            listaCantieri.Add(cantiereProva);
            listaCantieri.Add(cantiereProva2);

            ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

            var retCant = clientCantieri.SetCantieri(listaCantieri.ToArray());

            Console.WriteLine(retCant.Corretto ? "OK" : "NOK");

            Console.ReadLine();*/



            /*PresenzeServiceClient clientPresenze = new PresenzeServiceClient();

            Lavoratore lavoratore = new Lavoratore();

            lavoratore.Categoria = "A";
            lavoratore.CodiceFiscale = "BRGDTL63A10Z600S";
            lavoratore.Cognome = "BORGO";
            lavoratore.ComuneNascita = "MILANO";
            lavoratore.ContrattoCcnl = "068X";
            lavoratore.DataAssunzione = new DateTime(2012, 9, 5);
            lavoratore.DataNascita = new DateTime(1973, 3, 23);
            lavoratore.GateAccesso = "GX";
            lavoratore.ListaPresenze = new List<DateTime>();
            lavoratore.ListaPresenze.Add(new DateTime(2012, 8, 27));
            lavoratore.ListaPresenze.Add(new DateTime(2012, 8, 29));
            lavoratore.Mansione = "00";
            lavoratore.Nome = "DANTE ALBERTO";
            lavoratore.PartitaIvaAffidatario = "01806840128";
            lavoratore.PartitaIvaImpresa = "01903090189";
            lavoratore.Qualifica = "1A";
            lavoratore.Sesso = "M";

            Lavoratore lavoratore2 = new Lavoratore();

            lavoratore2.Categoria = "A";
            lavoratore2.CodiceFiscale = "BRGDTL63A10Z600S";
            lavoratore2.Cognome = "BORGO";
            lavoratore2.ComuneNascita = "MILANO";
            lavoratore2.ContrattoCcnl = "068";
            lavoratore2.DataAssunzione = new DateTime(2012, 9, 5);
            lavoratore2.DataNascita = new DateTime(1973, 3, 23);
            lavoratore2.GateAccesso = "GX";
            lavoratore2.ListaPresenze = new List<DateTime>();
            lavoratore2.ListaPresenze.Add(new DateTime(2012, 8, 27));
            lavoratore2.ListaPresenze.Add(new DateTime(2012, 8, 29));
            lavoratore2.Mansione = "00";
            lavoratore2.Nome = "DANTE ALBERTO";
            lavoratore2.PartitaIvaAffidatario = "0000000000";
            lavoratore2.PartitaIvaImpresa = "01903090189";
            lavoratore2.Qualifica = "1A";
            lavoratore2.Sesso = "M";

            Lavoratore lavoratore3 = new Lavoratore();

            lavoratore3.Categoria = "A";
            lavoratore3.CodiceFiscale = "PIRIPI";
            lavoratore3.Cognome = "BORGO";
            lavoratore3.ComuneNascita = "MILANO";
            lavoratore3.ContrattoCcnl = "068";
            lavoratore3.DataAssunzione = new DateTime(2012, 9, 5);
            lavoratore3.DataNascita = new DateTime(1973, 3, 23);
            lavoratore3.GateAccesso = "GX";
            lavoratore3.ListaPresenze = new List<DateTime>();
            lavoratore3.ListaPresenze.Add(new DateTime(2012, 8, 27));
            lavoratore3.ListaPresenze.Add(new DateTime(2012, 8, 29));
            lavoratore3.Mansione = "00";
            lavoratore3.Nome = "PIRIPACCHIO GIULIO";
            lavoratore3.PartitaIvaAffidatario = "0000000000";
            lavoratore3.PartitaIvaImpresa = "01903090189";
            lavoratore3.Qualifica = "QQ";
            lavoratore3.Sesso = "M";

            List<Lavoratore> listaLavoratori = new List<Lavoratore>();
            listaLavoratori.Add(lavoratore);
            listaLavoratori.Add(lavoratore2);
            listaLavoratori.Add(lavoratore3);

            var retPres = clientPresenze.SetPresenze(listaLavoratori.ToArray());

            Console.WriteLine(retPres.Corretto ? "OK" : "NOK");*/



            try
            {
                List<Cantiere> listaCantieri = new List<Cantiere>();

                DataContractSerializer dcs = new DataContractSerializer(typeof(List<Cantiere>));

                try
                {
                    Stream fs = new FileStream(@".\20150319.xml", FileMode.Open, FileAccess.Read);
                    listaCantieri = (List<Cantiere>) dcs.ReadObject(fs);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                    if (e.InnerException != null)
                    {
                        Console.WriteLine(e.InnerException.Message);
                    }
                }

                Console.WriteLine("Letto XML");

                //try
                //{
                //    XmlSerializer xmlSer = new XmlSerializer(typeof(List<Cantiere>));
                //    Stream fs = new FileStream(@".\testReale.xml", FileMode.Open, FileAccess.Read);
                //    List<Cantiere> listaCantieriXml = (List<Cantiere>) xmlSer.Deserialize(fs);
                //}
                //catch (Exception e)
                //{

                //}

                ServicePointManager.ServerCertificateValidationCallback =
                new RemoteCertificateValidationCallback(
                    delegate
                    { return true; }
                );

                CantieriServiceClient clientCantieri = new CantieriServiceClient();

                //while (true)
                //{
                CantieriResponse cantieriResponse = clientCantieri.SetCantieri(listaCantieri.ToArray());

                if (cantieriResponse != null)
                {
                    Console.WriteLine(cantieriResponse.Corretto ? "OK" : "NOK");
                    foreach (String piva in cantieriResponse.PivaErrori)
                    {
                        Console.WriteLine(piva);
                    }
                }
                else
                {
                    Console.WriteLine("NOK: NULL RESPONSE");
                }
                //}
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.WriteLine(exception.StackTrace);

                if (exception.InnerException != null)
                {
                    Console.WriteLine(exception.InnerException.Message);
                }
            }




            //try
            //{
            //    List<Lavoratore> listaLavoratori = new List<Lavoratore>();

            //    DataContractSerializer dcs = new DataContractSerializer(typeof(List<Lavoratore>));

            //    try
            //    {
            //        Stream fs = new FileStream(@".\presenze.xml", FileMode.Open, FileAccess.Read);
            //        listaLavoratori = (List<Lavoratore>) dcs.ReadObject(fs);
            //    }
            //    catch (Exception e)
            //    {
            //        Console.WriteLine(e.Message);
            //    }

            //    ServicePointManager.CertificatePolicy = new AcceptAllCertificatesPolicy();

            //    PresenzeServiceClient clientPresenze = new PresenzeServiceClient();

            //    PresenzeResponse presenzeResponse = clientPresenze.SetPresenze(listaLavoratori.ToArray());

            //    Console.WriteLine(presenzeResponse.Corretto ? "OK" : "NOK");
            //}
            //catch (Exception exception)
            //{
            //    Console.WriteLine(exception.Message);
            //}



            //try
            //{
            //    DurcServiceReference.DurcServiceClient clientDurc = new DurcServiceClient();
            //    List<string> listaCip = new List<string> { "20120382792865", "20120496180050", "20120697903320", "20120588053666", "20120678950510" };
            //    Durc[] durcInfo = clientDurc.GetDurcInfo(listaCip.ToArray());

            //    Console.WriteLine(durcInfo.Length);
            //}
            //catch (Exception exception)
            //{
            //    Console.WriteLine(exception.Message);
            //}


            //CorsiServiceReference.CorsiServiceClient clientCorsi = new CorsiServiceClient();

            //List<string> listaCf = new List<string> { "MSTLSS81H12D969D", "BBGNRC55A05G273S", "PNTFRC68D09C893D", "BBNVTR72E21B639N", "BBTLSN88B07D286W" };
            //int numeroRichiesteSalvate = clientCorsi.SetInfoCorso(listaCf.ToArray());

            Console.ReadLine();
        }
    }

    public class AcceptAllCertificatesPolicy : System.Net.ICertificatePolicy
    {
        public AcceptAllCertificatesPolicy()
        { }

        public bool CheckValidationResult(ServicePoint sp,
                  System.Security.Cryptography.X509Certificates.X509Certificate cert,
                  WebRequest req,
                  int problem)
        {
            return true;
        }
    }
}