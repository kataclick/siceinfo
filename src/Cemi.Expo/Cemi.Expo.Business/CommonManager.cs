﻿using System;
using System.Text.RegularExpressions;

namespace Cemi.Expo.Business
{
    public class CommonManager
    {
        public static string NormalizzaCampoTesto(string campo)
        {
            if (!String.IsNullOrEmpty(campo))
            {
                // Il Regex.Replace serve per sostituire i caratteri non ASCII 
                // con la stringa vuota
                return Regex.Replace(campo, @"[^\u0000-\u007F]", string.Empty).Trim().Replace('&', 'e').Replace('-', ' ');
            }

            return String.Empty;
        }
    }
}
