﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Cemi.Expo.Data;
using Cemi.Expo.Type.Domain;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.Business
{
    public class PresenzeManager
    {
        private static readonly string LogWsExpo = ConfigurationManager.AppSettings["LogWsExpo"];

        public PresenzeResponse InsertPresenze(List<Lavoratore> listaLavoratoriWs)
        {
            PresenzeResponse ret = new PresenzeResponse();
            DateTime dtDef = new DateTime(1900, 1, 1);

            foreach (var lavoratore in listaLavoratoriWs)
            {
                using (ExpoEntities context = new ExpoEntities())
                {
                    ExpoLavoratore expoLavoratore = null;

                    #region Pulizia dei dati in arrivo
                    // Nel caso in cui arrivi una mansione 21 la modifichiamo in 19
                    if (lavoratore.Mansione == "21")
                    {
                        lavoratore.Mansione = "19";
                    }

                    // Nel caso in cui arrivi una data assunzione strana la modifichiamo in 1/1/1900
                    if (lavoratore.DataAssunzione < dtDef)
                    {
                        lavoratore.DataAssunzione = dtDef;
                    }
                    #endregion

                    try
                    {
                        var lavoratoreTmp = (from lav in context.ExpoLavoratori
                                             where lav.CodiceFiscale == lavoratore.CodiceFiscale
                                             select lav).ToList();

                        if (lavoratoreTmp.Count == 0)
                        {
                            expoLavoratore = new ExpoLavoratore
                            {
                                CodiceFiscale = lavoratore.CodiceFiscale,
                                Cognome = lavoratore.Cognome,
                                Nome = lavoratore.Nome,
                                DataNascita = lavoratore.DataNascita,
                                Sesso = (lavoratore.Sesso == "M"),
                                ComuneNascita = lavoratore.ComuneNascita,
                                DataInserimentoRecord = DateTime.Now,
                                DataModificaRecord = null
                            };

                            ExpoLavoratoreRapporto expoLavoratoreRapporto = new ExpoLavoratoreRapporto
                            {
                                PivaImpresa = lavoratore.PartitaIvaImpresa,
                                PivaAffidatario = lavoratore.PartitaIvaAffidatario,
                                DataAssunzione = lavoratore.DataAssunzione,
                                GateAccesso = lavoratore.GateAccesso,
                                IdCategoria = lavoratore.Categoria,
                                IdMansione = lavoratore.Mansione,
                                IdQualifica = lavoratore.Qualifica,
                                IdCcnl = lavoratore.ContrattoCcnl,
                                DataInserimentoRecord = DateTime.Now
                            };

                            // SPOSTATO A MONTE PERCHE' GENERAVA DUPLICAZIONI DEI RAPPORTI DI LAVORO
                            //// Nel caso in cui arrivi una mansione 21 la modifichiamo in 19
                            //if (expoLavoratoreRapporto.IdMansione == "21")
                            //{
                            //    expoLavoratoreRapporto.IdMansione = "19";
                            //}

                            //// Nel caso in cui arrivi una data assunzione strana la modifichiamo in 1/1/1900
                            //if (expoLavoratoreRapporto.DataAssunzione < dtDef)
                            //{
                            //    expoLavoratoreRapporto.DataAssunzione = dtDef;
                            //}

                            foreach (var presenza in lavoratore.ListaPresenze)
                            {
                                expoLavoratoreRapporto.ExpoPresenze.Add(new ExpoPresenza { DataAccesso = presenza, DataInserimentoRecord = DateTime.Now });
                            }

                            expoLavoratore.ExpoLavoratoriRapporti.Add(expoLavoratoreRapporto);

                            context.ExpoLavoratori.AddObject(expoLavoratore);
                        }
                        else
                        {
                            expoLavoratore = lavoratoreTmp[0];

                            expoLavoratore.CodiceFiscale = lavoratore.CodiceFiscale;
                            expoLavoratore.Cognome = lavoratore.Cognome;
                            expoLavoratore.Nome = lavoratore.Nome;
                            expoLavoratore.DataNascita = lavoratore.DataNascita;
                            expoLavoratore.Sesso = (lavoratore.Sesso == "M");
                            expoLavoratore.ComuneNascita = lavoratore.ComuneNascita;
                            expoLavoratore.DataModificaRecord = DateTime.Now;

                            var rapporti = (from rapporto in expoLavoratore.ExpoLavoratoriRapporti
                                            where
                                                rapporto.PivaImpresa == lavoratore.PartitaIvaImpresa
                                                && rapporto.PivaAffidatario == lavoratore.PartitaIvaAffidatario
                                                && rapporto.DataAssunzione == lavoratore.DataAssunzione
                                                && rapporto.GateAccesso == lavoratore.GateAccesso
                                                && rapporto.IdCategoria == lavoratore.Categoria
                                                && rapporto.IdMansione == lavoratore.Mansione
                                                && rapporto.IdQualifica == lavoratore.Qualifica
                                                && rapporto.IdCcnl == lavoratore.ContrattoCcnl
                                            select rapporto).ToList();

                            if (rapporti.Count == 0)
                            {
                                ExpoLavoratoreRapporto expoLavoratoreRapporto = new ExpoLavoratoreRapporto
                                {
                                    PivaImpresa = lavoratore.PartitaIvaImpresa,
                                    PivaAffidatario = lavoratore.PartitaIvaAffidatario,
                                    DataAssunzione = lavoratore.DataAssunzione,
                                    GateAccesso = lavoratore.GateAccesso,
                                    IdCategoria = lavoratore.Categoria,
                                    IdMansione = lavoratore.Mansione,
                                    IdQualifica = lavoratore.Qualifica,
                                    IdCcnl = lavoratore.ContrattoCcnl,
                                    DataInserimentoRecord = DateTime.Now
                                };

                                // SPOSTATO A MONTE PERCHE' GENERAVA DUPLICAZIONI DEI RAPPORTI DI LAVORO
                                //// Nel caso in cui arrivi una mansione 21 la modifichiamo in 19
                                //if (expoLavoratoreRapporto.IdMansione == "21")
                                //{
                                //    expoLavoratoreRapporto.IdMansione = "19";
                                //}

                                //// Nel caso in cui arrivi una data assunzione strana la modifichiamo in 1/1/1900
                                //if (expoLavoratoreRapporto.DataAssunzione < dtDef)
                                //{
                                //    expoLavoratoreRapporto.DataAssunzione = dtDef;
                                //}

                                foreach (var presenza in lavoratore.ListaPresenze)
                                {
                                    expoLavoratoreRapporto.ExpoPresenze.Add(new ExpoPresenza { DataAccesso = presenza, DataInserimentoRecord = DateTime.Now });
                                }

                                expoLavoratore.ExpoLavoratoriRapporti.Add(expoLavoratoreRapporto);
                            }
                            else
                            {
                                var expoPresenze = rapporti[0].ExpoPresenze;

                                foreach (var presenza in lavoratore.ListaPresenze.Distinct())
                                {
                                    var presenze = (from pres in expoPresenze
                                                    where pres.DataAccesso == presenza
                                                    select pres).ToList();

                                    if (presenze.Count == 0)
                                    {
                                        expoPresenze.Add(new ExpoPresenza { DataAccesso = presenza, DataInserimentoRecord = DateTime.Now });
                                    }
                                    else
                                    {
                                        presenze[0].DataModificaRecord = DateTime.Now;
                                    }
                                }
                            }
                        }

                        int num = context.SaveChanges();

                        if (num <= 0)
                        {
                            ret.CfErrori.Add(expoLavoratore.CodiceFiscale);
                        }
                    }
                    catch (Exception ex)
                    {
                        ret.CfErrori.Add(expoLavoratore.CodiceFiscale);

                        if (LogWsExpo == "true")
                        {
                            using (EventLog appLog = new EventLog())
                            {
                                appLog.Source = "SiceInfo";
                                appLog.WriteEntry(String.Format("Eccezione WS Expo Presenze: {0} - {1} - {2}",
                                                                expoLavoratore.CodiceFiscale, ex.Message,
                                                                ex.InnerException));
                            }
                        }

                        try
                        {
                            using (ExpoEntities context2 = new ExpoEntities())
                            {
                                // Memorizzo il log dell'errore sulla tabella apposita
                                foreach (DateTime dataAccesso in lavoratore.ListaPresenze)
                                {
                                    var logErrori = (from errori in context2.ExpoPresenzeErrori
                                                     where errori.CodiceFiscale == lavoratore.CodiceFiscale
                                                         && errori.PIvaImpresa == lavoratore.PartitaIvaImpresa
                                                         && errori.Data == dataAccesso
                                                     select errori).ToList();


                                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(Lavoratore));
                                    StringWriter textWriter = new StringWriter();
                                    xmlSerializer.Serialize(textWriter, lavoratore);
                                    String lavoratoreSerializzato = textWriter.ToString();
                                    DateTime adesso = DateTime.Now;

                                    String messaggioErrore = String.Format("{0} - {1}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : String.Empty);

                                    if (logErrori.Count == 1)
                                    {
                                        ExpoPresenzaErrore presErrore = logErrori[0];

                                        presErrore.DataModificaRecord = adesso;
                                        presErrore.DatiAggiuntivi = lavoratoreSerializzato;
                                        presErrore.MessaggioErrore = messaggioErrore;
                                    }
                                    else
                                    {
                                        ExpoPresenzaErrore presErrore = new ExpoPresenzaErrore();

                                        presErrore.CodiceFiscale = lavoratore.CodiceFiscale;
                                        presErrore.PIvaImpresa = lavoratore.PartitaIvaImpresa;
                                        presErrore.Data = dataAccesso;
                                        presErrore.DatiAggiuntivi = lavoratoreSerializzato;
                                        presErrore.DataInserimentoRecord = adesso;
                                        presErrore.DataModificaRecord = adesso;
                                        presErrore.MessaggioErrore = messaggioErrore;

                                        context2.ExpoPresenzeErrori.AddObject(presErrore);
                                    }

                                    context2.SaveChanges();
                                }
                            }
                        }
                        catch { }
                    }
                }
            }

            return ret;
        }
    }
}
