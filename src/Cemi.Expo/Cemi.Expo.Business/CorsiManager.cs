﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Cemi.Expo.Business.ExpoCorsiServiceReference;
using Cemi.Expo.Data;
using Cemi.Expo.Type.Domain;
using Cemi.Expo.Type.Entities;

namespace Cemi.Expo.Business
{
    public class CorsiManager
    {
        private static readonly string LogWsExpo = ConfigurationManager.AppSettings["LogWsExpo"];
        private readonly Provider _dataProvider = new Provider();

        public int InsertRequest(List<string> codiciFiscaleLavoratori)
        {
            int ret;

            try
            {
                using (ExpoEntities context = new ExpoEntities())
                {
                    foreach (string codiceFiscale in codiciFiscaleLavoratori)
                    {
                        context.ExpoRichiesteCorsi.AddObject(new ExpoRichiestaCorsi
                            {
                                CodiceFiscale = codiceFiscale,
                                DataRichiesta = DateTime.Now,
                                Gestita = false,
                                DataGestione = null
                            });
                    }

                    ret = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ret = 0;

                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Corsi: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }

            return ret;
        }

        public void GestisciRichieste()
        {
            try
            {
                CorsiServiceClient client = new CorsiServiceClient();
                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["WsExpoUsername"];
                    client.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["WsExpoPassword"];
                }

                ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

                List<getCorsiRequestListaCorsi> listaCorsiWs = new List<getCorsiRequestListaCorsi>();

                using (ExpoEntities context = new ExpoEntities())
                {
                    var richeste = (from cf in context.ExpoRichiesteCorsi
                                    where cf.Gestita == false
                                    select cf).ToList();

                    foreach (ExpoRichiestaCorsi richiestaCorsi in richeste)
                    {
                        List<CorsoLavoratore> corsiLavoratore = _dataProvider.GetCorsiByCf(richiestaCorsi.CodiceFiscale);

                        if (corsiLavoratore.Count > 0)
                        {
                            foreach (CorsoLavoratore corsoLavoratore in corsiLavoratore)
                            {
                                getCorsiRequestListaCorsi corsoWs = new getCorsiRequestListaCorsi
                                {
                                    CodiceFiscale = richiestaCorsi.CodiceFiscale,
                                    Ente = corsoLavoratore.Ente,
                                    DenominazioneCorso = corsoLavoratore.Titolo,
                                    DataAttestazioneCorso = corsoLavoratore.DataAttestazione
                                };

                                listaCorsiWs.Add(corsoWs);
                            }
                        }

                        richiestaCorsi.Gestita = true;
                        richiestaCorsi.DataGestione = DateTime.Now;
                    }

                    if (listaCorsiWs.Count > 0)
                    {
                        getCorsiResponse corsiResponse = client.CorsiService(listaCorsiWs.ToArray());
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Corsi: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }
        }
    }
}