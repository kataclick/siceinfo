﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Cemi.Expo.Business.ExpoImpreseServiceReference;
using Cemi.Expo.Data;
using Cemi.Expo.Type.Domain;
using Cemi.Expo.Type.Entities;

namespace Cemi.Expo.Business
{
    public class ImpreseManager
    {
        private static readonly string LogWsExpo = ConfigurationManager.AppSettings["LogWsExpo"];
        private readonly Provider _dataProvider = new Provider();

        public int InsertRequest(List<string> partiteIvaImprese)
        {
            int ret;

            try
            {
                using (ExpoEntities context = new ExpoEntities())
                {
                    foreach (string partitaIva in partiteIvaImprese)
                    {
                        context.ExpoRichiesteImprese.AddObject(new ExpoRichiestaImpresa
                        {
                            PartitaIva = partitaIva,
                            DataRichiesta = DateTime.Now,
                            Gestita = false,
                            DataGestione = null
                        });
                    }

                    ret = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ret = 0;

                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Imprese: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }

            return ret;
        }

        public void GestisciRichieste()
        {
            try
            {
                getVerificaIscrizioneClient client = new getVerificaIscrizioneClient();

                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["WsExpoUsername"];
                    client.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["WsExpoPassword"];
                }

                ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

                List<getVerificaIscrizioneRequestImpresa> listaImpreseWs = new List<getVerificaIscrizioneRequestImpresa>();

                using (ExpoEntities context = new ExpoEntities())
                {
                    var richeste = (from cf in context.ExpoRichiesteImprese
                                    where cf.Gestita == false
                                    select cf).ToList();

                    foreach (ExpoRichiestaImpresa richiestaCorsi in richeste)
                    {
                        List<InfoImpresa> infoImprese = _dataProvider.GetInfoImpresaByPiva(richiestaCorsi.PartitaIva);

                        if (infoImprese.Count > 0)
                        {
                            foreach (InfoImpresa imp in infoImprese)
                            {
                                getVerificaIscrizioneRequestImpresa impWs = new getVerificaIscrizioneRequestImpresa
                                {
                                    PartitaIVA = imp.PartitaIva,
                                    NumeroIscrizione = imp.IdImpresa.ToString(),
                                    Richiesta = imp.RichiestaIscrizione.ToString(),
                                    Stato = imp.Stato ?? String.Empty
                                };

                                listaImpreseWs.Add(impWs);
                            }
                        }

                        richiestaCorsi.Gestita = true;
                        richiestaCorsi.DataGestione = DateTime.Now;
                    }

                    if (listaImpreseWs.Count > 0)
                    {
                        getVerificaIscrizioneResponse response = client.getVerificaIscrizione(listaImpreseWs.ToArray());
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Imprese: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }

        }
    }
}
