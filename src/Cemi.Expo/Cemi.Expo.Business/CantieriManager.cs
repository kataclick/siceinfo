﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Cemi.Expo.Data;
using Cemi.Expo.Type.Domain;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.Business
{
    public class CantieriManager
    {
        private static readonly string LogWsExpo = ConfigurationManager.AppSettings["LogWsExpo"];

        public CantieriResponse InsertCantieri(List<Cantiere> cantieri)
        {
            CantieriResponse ret = new CantieriResponse();

            try
            {
                using (ExpoEntities context = new ExpoEntities())
                {
                    context.ContextOptions.LazyLoadingEnabled = false;

                    foreach (var cantiere in cantieri)
                    {
                        //using (ExpoEntities context = new ExpoEntities())
                        //{
                        ExpoFiliera filiera = null;

                        //if (cantiere.ImpresaSubappaltatrice != null && (cantiere.ImpresaSubappaltatrice.PartitaIva == "01335800429" || cantiere.ImpresaSubappaltatrice.PartitaIva == "DE212467546"))
                        //{
                        //    Int32 aaaaa = 0;
                        //}

                        try
                        {
                            ExpoImpresa impresaAffidataria;
                            ExpoImpresa impresaEsecutrice;
                            ExpoImpresa impresaAppaltante = null;

                            #region Impresa Affidataria

                            String pivaAffidataria = cantiere.ImpresaAffidataria.PartitaIva;
                            String ragSocAffidataria = cantiere.ImpresaAffidataria.RagioneSociale.Trim();

                            var impresaAffidatariaTmp = (from impresa in context.ExpoImprese
                                                         where (impresa.PartitaIva == pivaAffidataria)
                                                            && (impresa.RagioneSociale.Trim().Equals(ragSocAffidataria))
                                                         select impresa).ToList();

                            if (impresaAffidatariaTmp.Count == 0)
                            {
                                impresaAffidataria = new ExpoImpresa
                                {
                                    RagioneSociale = cantiere.ImpresaAffidataria.RagioneSociale,
                                    PartitaIva = cantiere.ImpresaAffidataria.PartitaIva,
                                    Via = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Via),
                                    Comune = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Comune),
                                    Cap = cantiere.ImpresaAffidataria.Indirizzo.Cap,
                                    Provincia = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Provincia),
                                    Stato = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Stato),
                                    DataInserimentoRecord = DateTime.Now,
                                    DataModificaRecord = null
                                };
                            }
                            else
                            {
                                impresaAffidataria = impresaAffidatariaTmp[0];

                                impresaAffidataria.RagioneSociale = cantiere.ImpresaAffidataria.RagioneSociale;
                                impresaAffidataria.PartitaIva = cantiere.ImpresaAffidataria.PartitaIva;
                                impresaAffidataria.Via = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Via);
                                impresaAffidataria.Comune = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Comune);
                                impresaAffidataria.Cap = cantiere.ImpresaAffidataria.Indirizzo.Cap;
                                impresaAffidataria.Provincia = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Provincia);
                                impresaAffidataria.Stato = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaAffidataria.Indirizzo.Stato);
                                impresaAffidataria.DataModificaRecord = DateTime.Now;

                                //context.SaveChanges();
                            }

                            #endregion

                            #region Impresa Esecutrice

                            String pivaEsecutrice = cantiere.ImpresaEsecutrice.PartitaIva;
                            String ragSocEsecutrice = cantiere.ImpresaEsecutrice.RagioneSociale.Trim();

                            if (pivaEsecutrice == pivaAffidataria && ragSocEsecutrice == ragSocAffidataria)
                            {
                                impresaEsecutrice = impresaAffidataria;
                            }
                            else
                            {
                                var impresaEsecutriceTmp = (from impresa in context.ExpoImprese
                                                            where (impresa.PartitaIva == pivaEsecutrice)
                                                                && (impresa.RagioneSociale.Trim().Equals(ragSocEsecutrice))
                                                            select impresa).ToList();

                                if (impresaEsecutriceTmp.Count == 0)
                                {
                                    impresaEsecutrice = new ExpoImpresa
                                    {
                                        RagioneSociale = cantiere.ImpresaEsecutrice.RagioneSociale,
                                        PartitaIva = cantiere.ImpresaEsecutrice.PartitaIva,
                                        Via = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Via),
                                        Comune = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Comune),
                                        Cap = cantiere.ImpresaEsecutrice.Indirizzo.Cap,
                                        Provincia = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Provincia),
                                        Stato = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Stato),
                                        DataInserimentoRecord = DateTime.Now,
                                        DataModificaRecord = null
                                    };
                                }
                                else
                                {
                                    impresaEsecutrice = impresaEsecutriceTmp[0];

                                    impresaEsecutrice.RagioneSociale = cantiere.ImpresaEsecutrice.RagioneSociale;
                                    impresaEsecutrice.PartitaIva = cantiere.ImpresaEsecutrice.PartitaIva;
                                    impresaEsecutrice.Via = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Via);
                                    impresaEsecutrice.Comune = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Comune);
                                    impresaEsecutrice.Cap = cantiere.ImpresaEsecutrice.Indirizzo.Cap;
                                    impresaEsecutrice.Provincia = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Provincia);
                                    impresaEsecutrice.Stato = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaEsecutrice.Indirizzo.Stato);
                                    impresaEsecutrice.DataModificaRecord = DateTime.Now;

                                    //context.SaveChanges();
                                }
                            }

                            #endregion

                            #region Impresa Appaltante

                            if (cantiere.ImpresaSubappaltatrice != null)
                            {
                                String pivaAppaltante = cantiere.ImpresaSubappaltatrice.PartitaIva;
                                String ragSocAppaltante = cantiere.ImpresaSubappaltatrice.RagioneSociale.Trim();

                                if (pivaAppaltante == pivaAffidataria && ragSocAppaltante == ragSocAffidataria)
                                {
                                    impresaAppaltante = impresaAffidataria;
                                }
                                else if (pivaAppaltante == pivaEsecutrice && ragSocAppaltante == ragSocEsecutrice)
                                {
                                    impresaAppaltante = impresaEsecutrice;
                                }
                                else
                                {
                                    var impresaAppaltanteTmp = (from impresa in context.ExpoImprese
                                                                where (impresa.PartitaIva == pivaAppaltante)
                                                                    && (impresa.RagioneSociale.Trim().Equals(ragSocAppaltante))
                                                                select impresa).ToList();

                                    if (impresaAppaltanteTmp.Count == 0)
                                    {
                                        impresaAppaltante = new ExpoImpresa
                                        {
                                            RagioneSociale = cantiere.ImpresaSubappaltatrice.RagioneSociale,
                                            PartitaIva = cantiere.ImpresaSubappaltatrice.PartitaIva,
                                            Via = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Via),
                                            Comune = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Comune),
                                            Cap = cantiere.ImpresaSubappaltatrice.Indirizzo.Cap,
                                            Provincia = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Provincia),
                                            Stato = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Stato),
                                            DataInserimentoRecord = DateTime.Now,
                                            DataModificaRecord = null
                                        };
                                    }
                                    else
                                    {
                                        impresaAppaltante = impresaAppaltanteTmp[0];

                                        impresaAppaltante.RagioneSociale = cantiere.ImpresaSubappaltatrice.RagioneSociale;
                                        impresaAppaltante.PartitaIva = cantiere.ImpresaSubappaltatrice.PartitaIva;
                                        impresaAppaltante.Via = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Via);
                                        impresaAppaltante.Comune = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Comune);
                                        impresaAppaltante.Cap = cantiere.ImpresaSubappaltatrice.Indirizzo.Cap;
                                        impresaAppaltante.Provincia = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Provincia);
                                        impresaAppaltante.Stato = CommonManager.NormalizzaCampoTesto(cantiere.ImpresaSubappaltatrice.Indirizzo.Stato);
                                        impresaAppaltante.DataModificaRecord = DateTime.Now;
                                    }
                                }
                            }

                            #endregion

                            filiera = new ExpoFiliera
                            {
                                ExpoImpresaAffidataria = impresaAffidataria,
                                ExpoImpresaEsecutrice = impresaEsecutrice,
                                ExpoImpresaAppaltante = impresaAppaltante,
                                DataInizioAttivita = cantiere.DataInizioAttivita,
                                DataFineAttivita = cantiere.DataFineAttivita,
                                DataRicezione = DateTime.Now,
                                IdAttivitaIstat = cantiere.TipologiaLavori.Trim(),
                                NotificaProtocolloRegione = cantiere.NumeroNotificaPreliminare,
                                IdCcnl = cantiere.ContrattoEsecutore.Trim()
                            };

                            if (String.IsNullOrWhiteSpace(filiera.IdCcnl))
                            {
                                filiera.IdCcnl = "ND";
                            }

                            context.ExpoFiliere.AddObject(filiera);
                            int num = context.SaveChanges();

                            if (num <= 0)
                            {
                                string piva = cantiere.ImpresaEsecutrice != null
                                                  ? cantiere.ImpresaEsecutrice.PartitaIva
                                                  : "000000000";
                                ret.PivaErrori.Add(piva);
                            }
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                string piva = cantiere.ImpresaEsecutrice != null
                                                  ? cantiere.ImpresaEsecutrice.PartitaIva
                                                  : "000000000";
                                ret.PivaErrori.Add(piva);

                                if (LogWsExpo == "true")
                                {
                                    using (EventLog appLog = new EventLog())
                                    {
                                        appLog.Source = "SiceInfo";
                                        appLog.WriteEntry(String.Format("Eccezione WS Expo Cantieri: {0} - {1} - {2}",
                                                                        filiera.ExpoImpresaEsecutrice.PartitaIva, ex.Message,
                                                                        ex.InnerException));
                                    }
                                }
                            }
                            catch { }
                        }
                    }

                    //int num = context.SaveChanges();

                    //if (num <= 0)
                    //{
                    //    ret.PivaErrori.Add("Errori presenti");
                    //}
                }
            }
            catch (Exception ex)
            {
                try
                {
                    if (LogWsExpo == "true")
                    {
                        using (EventLog appLog = new EventLog())
                        {
                            appLog.Source = "SiceInfo";
                            appLog.WriteEntry(String.Format("Eccezione WS Expo Cantieri EF: {0} - {1}",
                                                            ex.Message,
                                                            ex.InnerException));
                        }
                    }
                }
                catch { }
            }

            return ret;
        }
    }
}
