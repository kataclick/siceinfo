﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Cemi.Expo.Data;
using Cemi.Expo.Type.Service;

namespace Cemi.Expo.Business
{
    public class DurcManager
    {
        private static readonly string LogWsExpo = ConfigurationManager.AppSettings["LogWsExpo"];

        public List<Durc> GetDurc(List<string> listaCip)
        {
            List<Durc> listaDurc = new List<Durc>();

            try
            {
                foreach (string cip in listaCip)
                {
                    using (ExpoEntities context = new ExpoEntities())
                    {
                        listaDurc.AddRange((from d in context.DurcCompleti
                                            where d.Cip == cip
                                            select new Durc
                                                {
                                                    Cip = d.Cip,
                                                    CodiceDurc = d.Protocollo,
                                                    DataEmissione = d.DataEmissione.Value,
                                                    PartivaIvaEsecutrice = d.CodiceFiscale,
                                                    Regolare =
                                                        (d.RegolareCe.Value && d.RegolareInail.Value &&
                                                         d.RegolareInps.Value)
                                                }).ToList());
                    }
                }
            }
            catch (Exception ex)
            {
                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Durc: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }

            return listaDurc;
        }
    }
}