﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Cemi.Expo.Business.ExpoLavoratoriServiceReference;
using Cemi.Expo.Data;
using Cemi.Expo.Type.Domain;
using Cemi.Expo.Type.Entities;

namespace Cemi.Expo.Business
{
    public class LavoratoriManager
    {
        private static readonly string LogWsExpo = ConfigurationManager.AppSettings["LogWsExpo"];
        private readonly Provider _dataProvider = new Provider();

        public int InsertRequest(List<string> codiciFiscaliLavoratori)
        {
            int ret;

            try
            {
                using (ExpoEntities context = new ExpoEntities())
                {
                    foreach (string codiceFiscale in codiciFiscaliLavoratori)
                    {
                        context.ExpoRichiesteLavoratori.AddObject(new ExpoRichiestaLavoratore
                                                                      {
                                                                          CodiceFiscale = codiceFiscale,
                                                                          DataRichiesta = DateTime.Now,
                                                                          Gestita = false,
                                                                          DataGestione = null
                                                                      });
                    }

                    ret = context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ret = 0;

                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Lavoratori: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }

            return ret;
        }

        public void GestisciRichieste()
        {
            try
            {
                getStatoClient client = new getStatoClient();
                if (client.ClientCredentials != null)
                {
                    client.ClientCredentials.UserName.UserName =
                        ConfigurationManager.AppSettings["WsExpoUsername"];
                    client.ClientCredentials.UserName.Password =
                        ConfigurationManager.AppSettings["WsExpoPassword"];
                }

                ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

                List<getStatoRequestLavoratore> listaLavoratoriWs = new List<getStatoRequestLavoratore>();

                using (ExpoEntities context = new ExpoEntities())
                {
                    var richeste = (from cf in context.ExpoRichiesteLavoratori
                                    where cf.Gestita == false
                                    select cf).ToList();

                    foreach (ExpoRichiestaLavoratore richiestaLavoratore in richeste)
                    {
                        List<InfoLavoratore> infoLavoratori =
                            _dataProvider.GetInfoLavoratoreByCf(richiestaLavoratore.CodiceFiscale);

                        if (infoLavoratori.Count > 0)
                        {
                            foreach (InfoLavoratore infoLavoratore in infoLavoratori)
                            {
                                getStatoRequestLavoratore lavoratoreWs = new getStatoRequestLavoratore
                                {
                                    CodiceFiscale = infoLavoratore.CodiceFiscale,
                                    CassaEdileIscrizione = infoLavoratore.CassaEdileIscrizione,
                                    CodiceCatastale = infoLavoratore.ComuneNascitaCodCatastale ?? String.Empty,
                                    CodiceIscrizione = infoLavoratore.IdLavoratore.ToString(),
                                    Cognome = infoLavoratore.Cognome,
                                    Nome = infoLavoratore.Nome,
                                    ComuneNascita = infoLavoratore.ComuneNascita,
                                    DataNascita = infoLavoratore.DataNascita.HasValue ? infoLavoratore.DataNascita.Value : DateTime.MinValue,
                                    Sesso = infoLavoratore.Sesso
                                };

                                listaLavoratoriWs.Add(lavoratoreWs);
                            }
                        }

                        richiestaLavoratore.Gestita = true;
                        richiestaLavoratore.DataGestione = DateTime.Now;
                    }

                    if (listaLavoratoriWs.Count > 0)
                    {
                        getStatoResponse lavoratoriResponse = client.getStato(listaLavoratoriWs.ToArray());
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (LogWsExpo == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione WS Expo Lavoratori: {0} - {1}",
                                                        ex.Message,
                                                        ex.InnerException));
                    }
                }
            }
        }
    }
}