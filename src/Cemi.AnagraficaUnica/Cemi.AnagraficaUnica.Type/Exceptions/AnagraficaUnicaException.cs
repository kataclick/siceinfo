﻿using System;

namespace Cemi.AnagraficaUnica.Type.Exceptions
{
    public class AnagraficaUnicaException : Exception
    {
        public AnagraficaUnicaException(String messaggio)
            : base(messaggio)
        {
        }
    }
}
