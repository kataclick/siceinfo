﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using Cemi.AnagraficaUnica.Business;

namespace Cemi.AnagraficaUnica.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            LeggiDaWebServiceAggiornaAnagrafiche();

            Console.ReadLine();
        }

        private static void LeggiDaWebServiceAggiornaAnagrafiche()
        {
            Manager manager = new Manager();

            Console.WriteLine("-- Inizio dell'allineamento Anagrafiche");
            Console.WriteLine();

            //try
            //{
            //    AnagraficaCalendario();
            //}
            //catch (SqlException sqlExcCalendario)
            //{
            //    Manager.MemorizzaErrore(sqlExcCalendario.Message);
            //    Console.WriteLine("!!  Errore Sql Anagrafica Calendario: {0}", sqlExcCalendario.Message);
            //}
            //catch (Exception excCalendario)
            //{
            //    Manager.MemorizzaErrore(excCalendario.Message);
            //    Console.WriteLine("!!  Errore Anagrafica Calendario: {0}", excCalendario.Message);
            //}

            //try
            //{
            //    AnagraficaCorsi();
            //}
            //catch (SqlException sqlExcCorsi)
            //{
            //    Manager.MemorizzaErrore(sqlExcCorsi.Message);
            //    Console.WriteLine("!!  Errore Sql Anagrafica Corsi: {0}", sqlExcCorsi.Message);
            //}
            //catch (Exception excCorsi)
            //{
            //    Manager.MemorizzaErrore(excCorsi.Message);
            //    Console.WriteLine("!!  Errore Anagrafica Corsi: {0}", excCorsi.Message);
            //}

            Boolean eseguiEdizioni = true;
            if (manager.Interattiva)
            {
                Console.Write("Eseguire caricamento edizioni (S/N)?  ");
                String letto = Console.ReadLine();

                if (letto.Trim().ToUpper() == "N")
                {
                    eseguiEdizioni = false;
                }
            }

            if (eseguiEdizioni)
            {
                try
                {
                    AnagraficaEdizioni();
                }
                catch (SqlException sqlExcEdizioni)
                {
                    Manager.MemorizzaErrore(sqlExcEdizioni.Message);
                    Console.WriteLine("!!  Errore Sql Anagrafica Edizioni: {0}", sqlExcEdizioni.Message);
                }
                catch (Exception excEdizioni)
                {
                    Manager.MemorizzaErrore(excEdizioni.Message);
                    Console.WriteLine("!!  Errore Anagrafica Edizioni: {0}", excEdizioni.Message);

                    if (excEdizioni.InnerException != null)
                    {
                        Console.WriteLine("!!  Errore Anagrafica Edizioni (INNER): {0}", excEdizioni.InnerException);
                    }
                }
            }

            Boolean eseguiImprese = true;
            if (manager.Interattiva)
            {
                Console.Write("Eseguire caricamento imprese (S/N)?  ");
                String letto = Console.ReadLine();

                if (letto.Trim().ToUpper() == "N")
                {
                    eseguiImprese = false;
                }
            }

            if (eseguiImprese)
            {
                try
                {
                    AnagraficaImprese();
                }
                catch (SqlException sqlExcImprese)
                {
                    Manager.MemorizzaErrore(sqlExcImprese.Message);
                    Console.WriteLine("!!  Errore Sql Anagrafica Imprese: {0}", sqlExcImprese.Message);
                }
                catch (Exception excImprese)
                {
                    Manager.MemorizzaErrore(excImprese.Message);
                    Console.WriteLine("!!  Errore Anagrafica Imprese: {0}", excImprese.Message);

                    if (excImprese.InnerException != null)
                    {
                        Console.WriteLine("!!  Errore Anagrafica Edizioni (INNER): {0}", excImprese.InnerException);
                    }
                }
            }

            Boolean eseguiLavoratori = true;
            if (manager.Interattiva)
            {
                Console.Write("Eseguire caricamento lavoratori (S/N)?  ");
                String letto = Console.ReadLine();

                if (letto.Trim().ToUpper() == "N")
                {
                    eseguiLavoratori = false;
                }
            }

            if (eseguiLavoratori)
            {
                try
                {
                    AnagraficaLavoratori();
                }
                catch (SqlException sqlExcLavoratori)
                {
                    Manager.MemorizzaErrore(sqlExcLavoratori.Message);
                    Console.WriteLine("!!  Errore Sql Anagrafica Lavoratori: {0}", sqlExcLavoratori.Message);
                }
                catch (Exception excLavoratori)
                {
                    Manager.MemorizzaErrore(excLavoratori.Message);
                    Console.WriteLine("!!  Errore Anagrafica Lavoratori: {0}", excLavoratori.Message);

                    if (excLavoratori.InnerException != null)
                    {
                        Console.WriteLine("!!  Errore Anagrafica Edizioni (INNER): {0}", excLavoratori.InnerException);
                    }
                }
            }

            Boolean eseguiStorico = true;
            if (manager.Interattiva)
            {
                Console.Write("Eseguire caricamento storico (S/N)?  ");
                String letto = Console.ReadLine();

                if (letto.Trim().ToUpper() == "N")
                {
                    eseguiStorico = false;
                }
            }

            if (eseguiStorico)
            {
                try
                {
                    AnagraficaStorico();
                }
                catch (SqlException sqlExcStorico)
                {
                    Manager.MemorizzaErrore(sqlExcStorico.Message);
                    Console.WriteLine("!!  Errore Sql Anagrafica Storico: {0}", sqlExcStorico.Message);
                }
                catch (Exception excStorico)
                {
                    Manager.MemorizzaErrore(excStorico.Message);
                    Console.WriteLine("!!  Errore Anagrafica Storico: {0}", excStorico.Message);

                    if (excStorico.InnerException != null)
                    {
                        Console.WriteLine("!!  Errore Anagrafica Edizioni (INNER): {0}", excStorico.InnerException);
                    }
                }
            }

            //try
            //{
            //    AnagraficaStoricoCorsi();
            //}
            //catch (SqlException sqlExcStoricoCorsi)
            //{
            //    Manager.MemorizzaErrore(sqlExcStoricoCorsi.Message);
            //    Console.WriteLine("!!  Errore Sql Anagrafica Storico Corsi: {0}", sqlExcStoricoCorsi.Message);
            //}
            //catch (Exception excStoricoCorsi)
            //{
            //    Manager.MemorizzaErrore(excStoricoCorsi.Message);
            //    Console.WriteLine("!!  Errore Anagrafica Storico Corsi: {0}", excStoricoCorsi.Message);
            //}

            Console.WriteLine();
            Console.WriteLine("-- Fine dell'allineamento Anagrafiche");
        }

        private static void AnagraficaEdizioni()
        {
            Console.WriteLine("----- Anagrafica Edizioni");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Chiamata WS");
            DataSet dsEdizioniEsem = clientEsem.AnaEdizioni(null);
            Console.WriteLine("Esem: Fine Chiamata WS");
            Console.WriteLine("Cpt: Chiamata WS");
            DataSet dsEdizioniCpt = clientCpt.AnaEdizioni(null);
            Console.WriteLine("Cpt: Fine Chiamata WS");

            Console.WriteLine("Esem: Inizio caricamento DB");
            try
            {
                biz.AnagraficaEdizioniEsem(dsEdizioniEsem);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Edizioni: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Edizioni: {0}", excStorico.Message);
            }
            Console.WriteLine("Esem: Fine caricamento DB");
            Console.WriteLine("Cpt: Inizio caricamento DB");
            try
            {
                biz.AnagraficaEdizioniCpt(dsEdizioniCpt);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Edizioni: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Edizioni: {0}", excStorico.Message);
            }
            Console.WriteLine("Cpt: Fine caricamento DB");

            Console.WriteLine("----- Fine Anagrafica Edizioni");
        }

        private static void AnagraficaStoricoCorsi()
        {
            Console.WriteLine("----- Anagrafica Storico Corsi");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Recupero Corsi non presenti");
            StringCollection dsStoricoCorsiEsem = biz.GetCorsiNonPresentiEsem();
            Console.WriteLine("Esem: Fine Recupero Corsi non presenti");

            Console.WriteLine("Esem: Inizio recupero Corsi tramite WS");
            foreach (String codiceCorso in dsStoricoCorsiEsem)
            {
                DataSet dsCorso = clientEsem.AnaEdizioni(codiceCorso);
                biz.AnagraficaStoricoCorsiEsem(dsCorso);
            }
            Console.WriteLine("Esem: Fine recupero Corsi tramite WS");

            Console.WriteLine("Cpt: Recupero Corsi non presenti");
            StringCollection dsStoricoCorsiCpt = biz.GetCorsiNonPresentiCpt();
            Console.WriteLine("Cpt: Fine Recupero Corsi non presenti");

            Console.WriteLine("Cpt: Inizio recupero Corsi tramite WS");
            foreach (String codiceCorso in dsStoricoCorsiCpt)
            {
                DataSet dsCorso = clientEsem.AnaEdizioni(codiceCorso);
                biz.AnagraficaStoricoCorsiCpt(dsCorso);
            }
            Console.WriteLine("Cpt: Fine recupero Corsi tramite WS");

            Console.WriteLine("----- Fine Anagrafica Storico Corsi");
        }

        private static void AnagraficaStorico()
        {
            Console.WriteLine("----- Anagrafica Storico");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Chiamata WS");
            DataSet dsLavoratoriEsem = clientEsem.AnaStorico(null, null, null);
            Console.WriteLine("Esem: Fine Chiamata WS");
            Console.WriteLine("Cpt: Chiamata WS");
            DataSet dsLavoratoriCpt = clientCpt.AnaStorico(null, null, null);
            Console.WriteLine("Cpt: Fine Chiamata WS");

            Console.WriteLine("Esem: Inizio caricamento DB");
            try
            {
                biz.AnagraficaStoricoEsem(dsLavoratoriEsem);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Storico: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Storico: {0}", excStorico.Message);
            }
            Console.WriteLine("Esem: Fine caricamento DB");
            Console.WriteLine("Cpt: Inizio caricamento DB");
            try
            {
                biz.AnagraficaStoricoCpt(dsLavoratoriCpt);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Storico: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Storico: {0}", excStorico.Message);
            }
            Console.WriteLine("Cpt: Fine caricamento DB");

            Console.WriteLine("----- Fine Anagrafica Storico");
        }

        private static void AnagraficaLavoratori()
        {
            Console.WriteLine("----- Anagrafica Lavoratori");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Chiamata WS");
            DataSet dsLavoratoriEsem = clientEsem.AnaLavoratori(null);
            Console.WriteLine("Esem: Fine Chiamata WS");
            Console.WriteLine("Cpt: Chiamata WS");
            DataSet dsLavoratoriCpt = clientCpt.AnaLavoratori(null);
            Console.WriteLine("Cpt: Fine Chiamata WS");

            Console.WriteLine("Esem: Inizio caricamento DB");
            try
            {
                biz.AnagraficaLavoratoriEsem(dsLavoratoriEsem);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Lavoratori: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Lavoratori: {0}", excStorico.Message);
            }
            Console.WriteLine("Esem: Fine caricamento DB");
            Console.WriteLine("Cpt: Inizio caricamento DB");
            try
            {
                biz.AnagraficaLavoratoriCpt(dsLavoratoriCpt);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Lavoratori: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Lavoratori: {0}", excStorico.Message);
            }
            Console.WriteLine("Cpt: Fine caricamento DB");

            Console.WriteLine("----- Fine Anagrafica Lavoratori");
        }

        private static void AnagraficaImprese()
        {
            Console.WriteLine("----- Anagrafica Imprese");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Chiamata WS");
            DataSet dsImpreseEsem = clientEsem.AnaImprese(null);
            Console.WriteLine("Esem: Fine Chiamata WS");
            Console.WriteLine("Cpt: Chiamata WS");
            DataSet dsImpreseCpt = clientCpt.AnaImprese(null);
            Console.WriteLine("Cpt: Fine Chiamata WS");

            Console.WriteLine("Esem: Inizio caricamento DB");
            try
            {
                biz.AnagraficaImpreseEsem(dsImpreseEsem);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Imprese: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Imprese: {0}", excStorico.Message);
            }
            Console.WriteLine("Esem: Fine caricamento DB");
            Console.WriteLine("Cpt: Inizio caricamento DB");
            try
            {
                biz.AnagraficaImpreseCpt(dsImpreseCpt);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Imprese: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Imprese: {0}", excStorico.Message);
            }
            Console.WriteLine("Cpt: Fine caricamento DB");

            Console.WriteLine("----- Fine Anagrafica Imprese");
        }

        private static void AnagraficaCorsi()
        {
            Console.WriteLine("----- Anagrafica Corsi");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Chiamata WS");
            DataSet dsCorsiEsem = clientEsem.AnaCorsi(null);
            Console.WriteLine("Esem: Fine Chiamata WS");
            Console.WriteLine("Cpt: Chiamata WS");
            DataSet dsCorsiCpt = clientCpt.AnaCorsi(null);
            Console.WriteLine("Cpt: Fine Chiamata WS");

            Console.WriteLine("Esem: Inizio caricamento DB");
            try
            {
                biz.AnagraficaCorsiEsem(dsCorsiEsem);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Corsi: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Corsi: {0}", excStorico.Message);
            }
            Console.WriteLine("Esem: Fine caricamento DB");
            Console.WriteLine("Cpt: Inizio caricamento DB");
            try
            {
                biz.AnagraficaCorsiCpt(dsCorsiCpt);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Corsi: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Corsi: {0}", excStorico.Message);
            }
            Console.WriteLine("Cpt: Fine caricamento DB");

            Console.WriteLine("----- Fine Anagrafica Corsi");
        }

        private static void AnagraficaCalendario()
        {
            Console.WriteLine("----- Anagrafica Calendario");

            Manager biz = new Manager();
            EsemService.ServiceSoapClient clientEsem = new EsemService.ServiceSoapClient();
            CptService.ServiceSoapClient clientCpt = new CptService.ServiceSoapClient();

            Console.WriteLine("Esem: Chiamata WS");
            DataSet dsCalendarioEsem = clientEsem.AnaCalendario(null, null);
            Console.WriteLine("Esem: Fine Chiamata WS");
            Console.WriteLine("Cpt: Chiamata WS");
            DataSet dsCalendarioCpt = clientCpt.AnaCalendario(null, null);
            Console.WriteLine("Cpt: Fine Chiamata WS");

            Console.WriteLine("Esem: Inizio caricamento DB");
            try
            {
                biz.AnagraficaCalendarioEsem(dsCalendarioEsem);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Storico: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Storico: {0}", excStorico.Message);
            }
            Console.WriteLine("Esem: Fine caricamento DB");
            Console.WriteLine("Cpt: Inizio caricamento DB");
            try
            {
                biz.AnagraficaCalendarioCpt(dsCalendarioCpt);
            }
            catch (SqlException sqlExcStorico)
            {
                Manager.MemorizzaErrore(sqlExcStorico.Message);
                Console.WriteLine("!!  Errore Sql Anagrafica Calendario: {0}", sqlExcStorico.Message);
            }
            catch (Exception excStorico)
            {
                Manager.MemorizzaErrore(excStorico.Message);
                Console.WriteLine("!!  Errore Anagrafica Calendario: {0}", excStorico.Message);
            }
            Console.WriteLine("Cpt: Fine caricamento DB");

            Console.WriteLine("----- Fine Anagrafica Calendario");
        }
    }
}
