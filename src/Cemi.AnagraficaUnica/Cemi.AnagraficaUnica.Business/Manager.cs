﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Cemi.AnagraficaUnica.DataAccess;
using Cemi.AnagraficaUnica.Type.Exceptions;

namespace Cemi.AnagraficaUnica.Business
{
    public class Manager
    {
        public DataProvider DataProvider { get; set; }

        public Boolean Interattiva
        {
            get
            {
                return Boolean.Parse(ConfigurationManager.AppSettings["Interattiva"]);
            }
        }

        public Manager()
        {
            this.DataProvider = new DataProvider();
        }

        #region Metodi di utilità
        public static void MemorizzaErrore(String messaggio)
        {
            using (EventLog appLog = new EventLog())
            {
                try
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Errore nella lettura e aggiornamento Anagrafica Unica: {0}", messaggio));
                }
                catch { }
            }
        }
        #endregion

        #region Anagrafica Lavoratori
        public void AnagraficaLavoratoriEsem(DataSet dsLavoratoriEsem)
        {
            this.DataProvider.AnagraficaLavoratoriEsemDelete();

            if (dsLavoratoriEsem != null && dsLavoratoriEsem.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsLavoratoriEsem.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaLavoratoriEsemInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }

        public void AnagraficaLavoratoriCpt(DataSet dsLavoratoriCpt)
        {
            this.DataProvider.AnagraficaLavoratoriCptDelete();

            if (dsLavoratoriCpt != null && dsLavoratoriCpt.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsLavoratoriCpt.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaLavoratoriCptInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }
        #endregion

        #region Anagrafica Imprese
        public void AnagraficaImpreseEsem(DataSet dsImpreseEsem)
        {
            this.DataProvider.AnagraficaImpreseEsemDelete();

            if (dsImpreseEsem != null && dsImpreseEsem.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsImpreseEsem.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaImpreseEsemInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }

        public void AnagraficaImpreseCpt(DataSet dsImpreseCpt)
        {
            this.DataProvider.AnagraficaImpreseCptDelete();

            if (dsImpreseCpt != null && dsImpreseCpt.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsImpreseCpt.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaImpreseCptInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }
        #endregion

        #region Anagrafica Corsi
        public void AnagraficaCorsiEsem(DataSet dsCorsiEsem)
        {
            this.DataProvider.AnagraficaCorsiEsemDelete();

            if (dsCorsiEsem != null && dsCorsiEsem.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsCorsiEsem.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaCorsiEsemInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }

        public void AnagraficaCorsiCpt(DataSet dsCorsiCpt)
        {
            this.DataProvider.AnagraficaCorsiCptDelete();

            if (dsCorsiCpt != null && dsCorsiCpt.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsCorsiCpt.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaCorsiCptInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }
        #endregion

        #region Anagrafica Calendario
        public void AnagraficaCalendarioEsem(DataSet dsCalendarioEsem)
        {
            this.DataProvider.AnagraficaCalendarioEsemDelete();

            if (dsCalendarioEsem != null && dsCalendarioEsem.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsCalendarioEsem.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaCalendarioEsemInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }

        public void AnagraficaCalendarioCpt(DataSet dsCalendarioCpt)
        {
            this.DataProvider.AnagraficaCalendarioCptDelete();

            if (dsCalendarioCpt != null && dsCalendarioCpt.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsCalendarioCpt.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaCalendarioCptInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }
        #endregion

        #region Anagrafica Storico
        public void AnagraficaStoricoEsem(DataSet dsStoricoEsem)
        {
            this.DataProvider.AnagraficaStoricoEsemDelete();

            if (dsStoricoEsem != null && dsStoricoEsem.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsStoricoEsem.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaStoricoEsemInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2627)
                        {
                            tuttoOk = false;
                            Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB: chiave doppia", dr.ItemArray.ToString()));
                        }
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }

        public void AnagraficaStoricoCpt(DataSet dsStoricoCpt)
        {
            this.DataProvider.AnagraficaStoricoCptDelete();

            if (dsStoricoCpt != null && dsStoricoCpt.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsStoricoCpt.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaStoricoCptInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2627)
                        {
                            tuttoOk = false;
                            Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB: chiave doppia", dr.ItemArray.ToString()));
                        }
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }
        #endregion

        #region Anagrafica Edizioni
        public void AnagraficaEdizioniEsem(DataSet dsEdizioniEsem)
        {
            this.DataProvider.AnagraficaEdizioniEsemDelete();

            if (dsEdizioniEsem != null && dsEdizioniEsem.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsEdizioniEsem.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaStoricoCorsiEsemInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2627)
                        {
                            tuttoOk = false;
                            Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB: chiave doppia", dr.ItemArray.ToString()));
                        }
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }

        public void AnagraficaEdizioniCpt(DataSet dsEdizioniCpt)
        {
            this.DataProvider.AnagraficaEdizioniCptDelete();

            if (dsEdizioniCpt != null && dsEdizioniCpt.Tables.Count == 1)
            {
                Boolean tuttoOk = true;

                foreach (DataRow dr in dsEdizioniCpt.Tables[0].Rows)
                {
                    try
                    {
                        this.DataProvider.AnagraficaStoricoCorsiCptInsert(dr);
                    }
                    catch (AnagraficaUnicaException)
                    {
                        tuttoOk = false;
                        Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB", dr.ItemArray.ToString()));
                    }
                    catch (SqlException exc)
                    {
                        if (exc.Number == 2627)
                        {
                            tuttoOk = false;
                            Manager.MemorizzaErrore(String.Format("Riga {0} non inserita nel DB: chiave doppia", dr.ItemArray.ToString()));
                        }
                    }
                }

                if (!tuttoOk)
                {
                    throw new AnagraficaUnicaException("Caricamento completato con errori");
                }
            }
        }
        #endregion

        #region Anagrafica Storico Corsi
        public void AnagraficaStoricoCorsiEsem(DataSet dsCorso)
        {
            if (dsCorso != null && dsCorso.Tables.Count == 1)
            {
                try
                {
                    this.DataProvider.AnagraficaStoricoCorsiEsemInsert(dsCorso.Tables[0].Rows[0]);
                }
                catch (AnagraficaUnicaException)
                {
                    Manager.MemorizzaErrore(String.Format("Corso {0} non inserito nel DB", dsCorso.Tables[0].Rows[0]["CodiceCatalogoSessione"]));
                    throw;
                }
            }
        }

        public void AnagraficaStoricoCorsiCpt(DataSet dsCorso)
        {
            if (dsCorso != null && dsCorso.Tables.Count == 1)
            {
                try
                {
                    this.DataProvider.AnagraficaStoricoCorsiCptInsert(dsCorso.Tables[0].Rows[0]);
                }
                catch (AnagraficaUnicaException)
                {
                    Manager.MemorizzaErrore(String.Format("Corso {0} non inserito nel DB", dsCorso.Tables[0].Rows[0]["CodiceCatalogoSessione"]));
                    throw;
                }
            }
        }
        #endregion

        public StringCollection GetCorsiNonPresentiEsem()
        {
            return this.DataProvider.GetCorsiNonPresentiEsem();
        }

        public StringCollection GetCorsiNonPresentiCpt()
        {
            return this.DataProvider.GetCorsiNonPresentiCpt();
        }
    }
}
