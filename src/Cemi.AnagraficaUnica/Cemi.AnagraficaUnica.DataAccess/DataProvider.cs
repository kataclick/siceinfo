﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using Cemi.AnagraficaUnica.Type.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.AnagraficaUnica.DataAccess
{
    public class DataProvider
    {
        private Database databaseCemi;

        public DataProvider()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        #region Anagrafica Lavoratori
        public void AnagraficaLavoratoriEsemDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaLavoratoriEsemDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaLavoratoriEsemInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaLavoratoriEsemInsert"))
            {
                AggiungiParametriPerLavoratoriEsem(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaLavoratoriEsemInsert: Riga non inserita");
                }
            }
        }

        public void AnagraficaLavoratoriCptDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaLavoratoriCptDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaLavoratoriCptInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaLavoratoriCptInsert"))
            {
                AggiungiParametriPerLavoratoriCpt(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaLavoratoriCptInsert: Riga non inserita");
                }
            }
        }

        private void AggiungiParametriPerLavoratoriEsem(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCassaEdile"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCassaEdile"].ToString()))
            {
                Int32 codice = -1;
                if (Int32.TryParse(dr["codiceCassaEdile"].ToString(), out codice))
                {
                    this.databaseCemi.AddInParameter(comando, "@codiceCassaEdile", DbType.Int32, codice);
                }
            }
            if (dr["cognome"] != Convert.DBNull && !String.IsNullOrEmpty(dr["cognome"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@cognome", DbType.String, dr["cognome"] as String);
            }
            if (dr["nome"] != Convert.DBNull && !String.IsNullOrEmpty(dr["nome"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@nome", DbType.String, dr["nome"] as String);
            }
            if (dr["sesso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sesso"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sesso", DbType.String, dr["sesso"] as String);
            }
            if (dr["dataNascita"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataNascita"].ToString()))
            {
                String dataNascitaS = dr["dataNascita"].ToString();
                String[] splitted = dataNascitaS.Split('/');
                DateTime dataNascita = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
            }
            if (dr["provinciaNascita"] != Convert.DBNull && !String.IsNullOrEmpty(dr["provinciaNascita"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String, dr["provinciaNascita"] as String);
            }
            if (dr["luogoNascita"] != Convert.DBNull && !String.IsNullOrEmpty(dr["luogoNascita"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String, dr["luogoNascita"] as String);
            }
            if (dr["codiceFiscale"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceFiscale"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, dr["codiceFiscale"] as String);
            }
            if (dr["cittadinanza"] != Convert.DBNull && !String.IsNullOrEmpty(dr["cittadinanza"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, dr["cittadinanza"] as String);
            }
            if (dr["indirizzoProvincia"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoProvincia"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoProvincia", DbType.String, dr["indirizzoProvincia"] as String);
            }
            if (dr["indirizzoLuogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoLuogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoLuogo", DbType.String, dr["indirizzoLuogo"] as String);
            }
            if (dr["indirizzoDenominazione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoDenominazione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoDenominazione", DbType.String, dr["indirizzoDenominazione"] as String);
            }
            if (dr["indirizzoCap"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoCap"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoCap", DbType.String, dr["indirizzoCap"] as String);
            }
            if (dr["telefono"] != Convert.DBNull && !String.IsNullOrEmpty(dr["telefono"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@telefono", DbType.String, dr["telefono"] as String);
            }
            if (dr["cellulare"] != Convert.DBNull && !String.IsNullOrEmpty(dr["cellulare"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@cellulare", DbType.String, dr["cellulare"] as String);
            }
            if (dr["email"] != Convert.DBNull && !String.IsNullOrEmpty(dr["email"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@email", DbType.String, dr["email"] as String);
            }
            if (dr["partitaIvaImpresa"] != Convert.DBNull && !String.IsNullOrEmpty(dr["partitaIvaImpresa"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, dr["partitaIvaImpresa"] as String);
            }
            if (dr["titoloDiStudio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["titoloDiStudio"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@titoloDiStudio", DbType.String, dr["titoloDiStudio"] as String);
            }
            if (dr["dataUltimoAggiornamento"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataUltimoAggiornamento"].ToString()))
            {
                String dataUltimoAggiornamentoS = dr["dataUltimoAggiornamento"].ToString();
                String[] splitted = dataUltimoAggiornamentoS.Split('/');
                DateTime dataUltimoAggiornamento = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataUltimoAggiornamento", DbType.DateTime, dataUltimoAggiornamento);
            }
        }

        private void AggiungiParametriPerLavoratoriCpt(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCassaEdile"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCassaEdile"].ToString()))
            {
                Int32 codice = -1;
                if (Int32.TryParse(dr["codiceCassaEdile"].ToString(), out codice))
                {
                    this.databaseCemi.AddInParameter(comando, "@codiceCassaEdile", DbType.Int32, codice);
                }
            }
            if (dr["cognome"] != Convert.DBNull && !String.IsNullOrEmpty(dr["cognome"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@cognome", DbType.String, dr["cognome"] as String);
            }
            if (dr["nome"] != Convert.DBNull && !String.IsNullOrEmpty(dr["nome"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@nome", DbType.String, dr["nome"] as String);
            }
            if (dr["sesso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sesso"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sesso", DbType.String, dr["sesso"] as String);
            }
            if (dr["dataNascita"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataNascita"].ToString()))
            {
                String dataNascitaS = dr["dataNascita"].ToString();
                String[] splitted = dataNascitaS.Split('/');
                DateTime dataNascita = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
            }
            if (dr["provinciaNascita"] != Convert.DBNull && !String.IsNullOrEmpty(dr["provinciaNascita"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String, dr["provinciaNascita"] as String);
            }
            if (dr["luogoNascita"] != Convert.DBNull && !String.IsNullOrEmpty(dr["luogoNascita"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String, dr["luogoNascita"] as String);
            }
            if (dr["codiceFiscale"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceFiscale"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, dr["codiceFiscale"] as String);
            }
            if (dr["cittadinanza"] != Convert.DBNull && !String.IsNullOrEmpty(dr["cittadinanza"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, dr["cittadinanza"] as String);
            }
            if (dr["indirizzoProvincia"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoProvincia"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoProvincia", DbType.String, dr["indirizzoProvincia"] as String);
            }
            if (dr["indirizzoLuogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoLuogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoLuogo", DbType.String, dr["indirizzoLuogo"] as String);
            }
            if (dr["indirizzoDenominazione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoDenominazione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoDenominazione", DbType.String, dr["indirizzoDenominazione"] as String);
            }
            if (dr["indirizzoCap"] != Convert.DBNull && !String.IsNullOrEmpty(dr["indirizzoCap"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@indirizzoCap", DbType.String, dr["indirizzoCap"] as String);
            }
            if (dr["cellulare"] != Convert.DBNull && !String.IsNullOrEmpty(dr["cellulare"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@cellulare", DbType.String, dr["cellulare"] as String);
            }
            if (dr["email"] != Convert.DBNull && !String.IsNullOrEmpty(dr["email"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@email", DbType.String, dr["email"] as String);
            }
            if (dr["partitaIvaImpresa"] != Convert.DBNull && !String.IsNullOrEmpty(dr["partitaIvaImpresa"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@partitaIvaImpresa", DbType.String, dr["partitaIvaImpresa"] as String);
            }
            if (dr["titoloDiStudio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["titoloDiStudio"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@titoloDiStudio", DbType.String, dr["titoloDiStudio"] as String);
            }
            if (dr["dataUltimoAggiornamento"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataUltimoAggiornamento"].ToString()))
            {
                String dataUltimoAggiornamentoS = dr["dataUltimoAggiornamento"].ToString();
                String[] splitted = dataUltimoAggiornamentoS.Split('/');
                DateTime dataUltimoAggiornamento = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataUltimoAggiornamento", DbType.DateTime, dataUltimoAggiornamento);
            }
        }
        #endregion

        #region Anagrafica Imprese
        public void AnagraficaImpreseEsemDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaImpreseEsemDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaImpreseEsemInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaImpreseEsemInsert"))
            {
                AggiungiParametriPerImpreseEsem(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaImpreseEsemInsert: Riga non inserita");
                }
            }
        }

        public void AnagraficaImpreseCptDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaImpreseCptDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaImpreseCptInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaImpreseCptInsert"))
            {
                AggiungiParametriPerImpreseCpt(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaImpreseEsemInsert: Riga non inserita");
                }
            }
        }

        private void AggiungiParametriPerImpreseEsem(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCassaEdile"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCassaEdile"].ToString()))
            {
                Int32 codice = -1;
                if (Int32.TryParse(dr["codiceCassaEdile"].ToString(), out codice))
                {
                    this.databaseCemi.AddInParameter(comando, "@codiceCassaEdile", DbType.Int32, codice);
                }
            }
            if (dr["ragioneSociale"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ragioneSociale"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, dr["ragioneSociale"] as String);
            }
            if (dr["partitaIvaCodiceFiscale"] != Convert.DBNull && !String.IsNullOrEmpty(dr["partitaIvaCodiceFiscale"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@partitaIvaCodiceFiscale", DbType.String, dr["partitaIvaCodiceFiscale"] as String);
            }
            if (dr["sedeLegaleProvincia"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleProvincia"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleProvincia", DbType.String, dr["sedeLegaleProvincia"] as String);
            }
            if (dr["sedeLegaleLuogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleLuogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleLuogo", DbType.String, dr["sedeLegaleLuogo"] as String);
            }
            if (dr["sedeLegaleDenominazione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleDenominazione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleDenominazione", DbType.String, dr["sedeLegaleDenominazione"] as String);
            }
            if (dr["sedeLegaleCap"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleCap"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleCap", DbType.String, dr["sedeLegaleCap"] as String);
            }
            if (dr["sedeLegaleTelefono"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleTelefono"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleTelefono", DbType.String, dr["sedeLegaleTelefono"] as String);
            }
            if (dr["sedeLegaleFax"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleFax"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleFax", DbType.String, dr["sedeLegaleFax"] as String);
            }
            if (dr["sedeLegaleEmail"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleEmail"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleEmail", DbType.String, dr["sedeLegaleEmail"] as String);
            }
            if (dr["sedeAmministrativaProvincia"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaProvincia"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaProvincia", DbType.String, dr["sedeAmministrativaProvincia"] as String);
            }
            if (dr["sedeAmministrativaLuogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaLuogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaLuogo", DbType.String, dr["sedeAmministrativaLuogo"] as String);
            }
            if (dr["sedeAmministrativaDenominazione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaDenominazione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaDenominazione", DbType.String, dr["sedeAmministrativaDenominazione"] as String);
            }
            if (dr["sedeAmministrativaCap"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaCap"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaCap", DbType.String, dr["sedeAmministrativaCap"] as String);
            }
            //if (dr["sedeAmministrativaTelefono"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaTelefono"].ToString()))
            //{
            //    this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaTelefono", DbType.String, dr["sedeAmministrativaTelefono"] as String);
            //}
            //if (dr["sedeAmministrativaFax"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaFax"].ToString()))
            //{
            //    this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaFax", DbType.String, dr["sedeAmministrativaFax"] as String);
            //}
            //if (dr["sedeAmministrativaEmail"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaEmail"].ToString()))
            //{
            //    this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaEmail", DbType.String, dr["sedeAmministrativaEmail"] as String);
            //}
            if (dr["numeroDipendenti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroDipendenti"].ToString()))
            {
                Int32 numeroDipendenti = -1;
                if (Int32.TryParse(dr["numeroDipendenti"].ToString(), out numeroDipendenti))
                {
                    this.databaseCemi.AddInParameter(comando, "@numeroDipendenti", DbType.Int32, numeroDipendenti);
                }
            }
            if (dr["dataUltimoAggiornamento"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataUltimoAggiornamento"].ToString()))
            {
                String dataUltimoAggiornamentoS = dr["dataUltimoAggiornamento"].ToString();
                String[] splitted = dataUltimoAggiornamentoS.Split('/');
                DateTime dataUltimoAggiornamento = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataUltimoAggiornamento", DbType.DateTime, dataUltimoAggiornamento);
            }
        }

        private void AggiungiParametriPerImpreseCpt(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCassaEdile"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCassaEdile"].ToString()))
            {
                Int32 codice = -1;
                if (Int32.TryParse(dr["codiceCassaEdile"].ToString(), out codice))
                {
                    this.databaseCemi.AddInParameter(comando, "@codiceCassaEdile", DbType.Int32, codice);
                }
            }
            if (dr["ragioneSociale"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ragioneSociale"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, dr["ragioneSociale"] as String);
            }
            if (dr["partitaIvaCodiceFiscale"] != Convert.DBNull && !String.IsNullOrEmpty(dr["partitaIvaCodiceFiscale"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@partitaIvaCodiceFiscale", DbType.String, dr["partitaIvaCodiceFiscale"] as String);
            }
            if (dr["sedeLegaleProvincia"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleProvincia"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleProvincia", DbType.String, dr["sedeLegaleProvincia"] as String);
            }
            if (dr["sedeLegaleLuogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleLuogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleLuogo", DbType.String, dr["sedeLegaleLuogo"] as String);
            }
            if (dr["sedeLegaleDenominazione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleDenominazione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleDenominazione", DbType.String, dr["sedeLegaleDenominazione"] as String);
            }
            if (dr["sedeLegaleCap"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleCap"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleCap", DbType.String, dr["sedeLegaleCap"] as String);
            }
            if (dr["sedeLegaleTelefono"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleTelefono"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleTelefono", DbType.String, dr["sedeLegaleTelefono"] as String);
            }
            if (dr["sedeLegaleFax"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleFax"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleFax", DbType.String, dr["sedeLegaleFax"] as String);
            }
            if (dr["sedeLegaleEmail"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeLegaleEmail"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeLegaleEmail", DbType.String, dr["sedeLegaleEmail"] as String);
            }
            if (dr["sedeAmministrativaProvincia"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaProvincia"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaProvincia", DbType.String, dr["sedeAmministrativaProvincia"] as String);
            }
            if (dr["sedeAmministrativaLuogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaLuogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaLuogo", DbType.String, dr["sedeAmministrativaLuogo"] as String);
            }
            if (dr["sedeAmministrativaDenominazione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaDenominazione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaDenominazione", DbType.String, dr["sedeAmministrativaDenominazione"] as String);
            }
            if (dr["sedeAmministrativaCap"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sedeAmministrativaCap"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sedeAmministrativaCap", DbType.String, dr["sedeAmministrativaCap"] as String);
            }
            if (dr["numeroDipendenti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroDipendenti"].ToString()))
            {
                Int32 numeroDipendenti = -1;
                if (Int32.TryParse(dr["numeroDipendenti"].ToString(), out numeroDipendenti))
                {
                    this.databaseCemi.AddInParameter(comando, "@numeroDipendenti", DbType.Int32, numeroDipendenti);
                }
            }
            if (dr["dataUltimoAggiornamento"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataUltimoAggiornamento"].ToString()))
            {
                String dataUltimoAggiornamentoS = dr["dataUltimoAggiornamento"].ToString();
                String[] splitted = dataUltimoAggiornamentoS.Split('/');
                DateTime dataUltimoAggiornamento = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataUltimoAggiornamento", DbType.DateTime, dataUltimoAggiornamento);
            }
        }
        #endregion

        #region Anagrafica Corsi
        public void AnagraficaCorsiEsemDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiEsemDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaCorsiEsemInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiEsemInsert"))
            {
                AggiungiParametriPerCorsi(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaCorsiEsemInsert: Riga non inserita");
                }
            }
        }

        public void AnagraficaCorsiCptDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiCptDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaCorsiCptInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiCptInsert"))
            {
                AggiungiParametriPerCorsi(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaCorsiCptInsert: Riga non inserita");
                }
            }
        }

        private void AggiungiParametriPerCorsi(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCatalogo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogo"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@codiceCatalogo", DbType.String, dr["codiceCatalogo"] as String);
            }
            if (dr["area"] != Convert.DBNull && !String.IsNullOrEmpty(dr["area"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@area", DbType.String, dr["area"] as String);
            }
            if (dr["sottoArea"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sottoArea"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sottoArea", DbType.String, dr["sottoArea"] as String);
            }
            if (dr["titoloCorso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["titoloCorso"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@titoloCorso", DbType.String, dr["titoloCorso"] as String);
            }
            if (dr["durataOre"] != Convert.DBNull && !String.IsNullOrEmpty(dr["durataOre"].ToString()))
            {
                Int32 durataOre = -1;
                if (Int32.TryParse(dr["durataOre"].ToString(), out durataOre))
                {
                    this.databaseCemi.AddInParameter(comando, "@durataOre", DbType.Int32, durataOre);
                }
            }
            if (dr["durataGiorni"] != Convert.DBNull && !String.IsNullOrEmpty(dr["durataGiorni"].ToString()))
            {
                Int32 durataGiorni = -1;
                if (Int32.TryParse(dr["durataGiorni"].ToString(), out durataGiorni))
                {
                    this.databaseCemi.AddInParameter(comando, "@durataGiorni", DbType.Int32, durataGiorni);
                }
            }
            if (dr["numeroMaxCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMaxCorsisti"].ToString()))
            {
                Int32 numeroMaxCorsisti = -1;
                if (Int32.TryParse(dr["numeroMaxCorsisti"].ToString(), out numeroMaxCorsisti))
                {
                    this.databaseCemi.AddInParameter(comando, "@numeroMaxCorsisti", DbType.Int32, numeroMaxCorsisti);
                }
            }
            if (dr["costo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["costo"].ToString()))
            {
                Decimal costo = -1;
                if (Decimal.TryParse(dr["costo"].ToString(), out costo))
                {
                    this.databaseCemi.AddInParameter(comando, "@costo", DbType.Int32, costo);
                }
            }
            if (dr["tipoAttestato"] != Convert.DBNull && !String.IsNullOrEmpty(dr["tipoAttestato"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@tipoAttestato", DbType.String, dr["tipoAttestato"] as String);
            }
        }
        #endregion

        #region Anagrafica Calendario
        public void AnagraficaCalendarioEsemDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCalendarioEsemDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaCalendarioEsemInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCalendarioEsemInsert"))
            {
                AggiungiParametriPerCalendario(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaCalendarioEsemInsert: Riga non inserita");
                }
            }
        }

        public void AnagraficaCalendarioCptDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCalendarioCptDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaCalendarioCptInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCalendarioCptInsert"))
            {
                AggiungiParametriPerCalendario(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaCalendarioCptInsert: Riga non inserita");
                }
            }
        }

        private void AggiungiParametriPerCalendario(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCatalogoCorso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogoCorso"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@codiceCatalogoCorso", DbType.String, dr["codiceCatalogoCorso"] as String);
            }
            if (dr["codiceCatalogoSessione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogoSessione"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@codiceCatalogoSessione", DbType.String, dr["codiceCatalogoSessione"] as String);
            }
            if (dr["sede"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sede"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@sede", DbType.String, dr["sede"] as String);
            }
            if (dr["aula"] != Convert.DBNull && !String.IsNullOrEmpty(dr["aula"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@aula", DbType.String, dr["aula"] as String);
            }
            if (dr["oraInizio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oraInizio"].ToString()))
            {
                String oraInizioS = dr["oraInizio"].ToString();
                String[] splitted = oraInizioS.Split(':');
                DateTime oraInizio = new DateTime(1900, 1, 1, Int32.Parse(splitted[1]), Int32.Parse(splitted[0]), 0);

                this.databaseCemi.AddInParameter(comando, "@oraInizio", DbType.DateTime, oraInizio);
            }
            if (dr["oraFine"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oraFine"].ToString()))
            {
                String oraFineS = dr["oraFine"].ToString();
                String[] splitted = oraFineS.Split(':');
                DateTime oraFine = new DateTime(1900, 1, 1, Int32.Parse(splitted[1]), Int32.Parse(splitted[0]), 0);

                this.databaseCemi.AddInParameter(comando, "@oraFine", DbType.DateTime, oraFine);
            }
            if (dr["oraInizioInt"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oraInizioInt"].ToString()))
            {
                String oraInizioIntS = dr["oraInizioInt"].ToString();
                String[] splitted = oraInizioIntS.Split(':');
                DateTime oraInizioInt = new DateTime(1900, 1, 1, Int32.Parse(splitted[1]), Int32.Parse(splitted[0]), 0);

                this.databaseCemi.AddInParameter(comando, "@oraInizioInt", DbType.DateTime, oraInizioInt);
            }
            if (dr["oraFineInt"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oraFineInt"].ToString()))
            {
                String oraFineIntS = dr["oraFineInt"].ToString();
                String[] splitted = oraFineIntS.Split(':');
                DateTime oraFineInt = new DateTime(1900, 1, 1, Int32.Parse(splitted[1]), Int32.Parse(splitted[0]), 0);

                this.databaseCemi.AddInParameter(comando, "@oraFineInt", DbType.DateTime, oraFineInt);
            }
            if (dr["dataInizio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataInizio"].ToString()))
            {
                String dataInizioS = dr["dataInizio"].ToString();
                String[] splitted = dataInizioS.Split('/');
                DateTime dataInizio = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, dataInizio);
            }
            if (dr["dataFine"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataInizio"].ToString()))
            {
                String dataFineS = dr["dataFine"].ToString();
                String[] splitted = dataFineS.Split('/');
                DateTime dataFine = new DateTime(Int32.Parse(splitted[2].Substring(0, 4)), Int32.Parse(splitted[1]), Int32.Parse(splitted[0]));

                this.databaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataFine);
            }
            if (dr["oreLezione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oreLezione"].ToString()))
            {
                Int32 oreLezione = -1;
                if (Int32.TryParse(dr["oreLezione"].ToString(), out oreLezione))
                {
                    this.databaseCemi.AddInParameter(comando, "@oreLezione", DbType.Int32, oreLezione);
                }
            }
            if (dr["durataGiorni"] != Convert.DBNull && !String.IsNullOrEmpty(dr["durataGiorni"].ToString()))
            {
                Int32 durataGiorni = -1;
                if (Int32.TryParse(dr["durataGiorni"].ToString(), out durataGiorni))
                {
                    this.databaseCemi.AddInParameter(comando, "@durataGiorni", DbType.Int32, durataGiorni);
                }
            }
            if (dr["numeroMinCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMinCorsisti"].ToString()))
            {
                Int32 numeroMinCorsisti = -1;
                if (Int32.TryParse(dr["numeroMinCorsisti"].ToString(), out numeroMinCorsisti))
                {
                    this.databaseCemi.AddInParameter(comando, "@numeroMinCorsisti", DbType.Int32, numeroMinCorsisti);
                }
            }
            if (dr["numeroMaxCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMaxCorsisti"].ToString()))
            {
                Int32 numeroMaxCorsisti = -1;
                if (Int32.TryParse(dr["numeroMaxCorsisti"].ToString(), out numeroMaxCorsisti))
                {
                    this.databaseCemi.AddInParameter(comando, "@numeroMaxCorsisti", DbType.Int32, numeroMaxCorsisti);
                }
            }
            if (dr["numeroMaxCorsistiPre"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMaxCorsistiPre"].ToString()))
            {
                Int32 numeroMaxCorsistiPre = -1;
                if (Int32.TryParse(dr["numeroMaxCorsistiPre"].ToString(), out numeroMaxCorsistiPre))
                {
                    this.databaseCemi.AddInParameter(comando, "@numeroMaxCorsistiPre", DbType.Int32, numeroMaxCorsistiPre);
                }
            }
            if (dr["costo"] != Convert.DBNull && !String.IsNullOrEmpty(dr["costo"].ToString()))
            {
                Decimal costo = -1;
                if (Decimal.TryParse(dr["costo"].ToString(), out costo))
                {
                    this.databaseCemi.AddInParameter(comando, "@costo", DbType.Int32, costo);
                }
            }
            if (dr["tipoAttestato"] != Convert.DBNull && !String.IsNullOrEmpty(dr["tipoAttestato"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@tipoAttestato", DbType.String, dr["tipoAttestato"] as String);
            }
        }
        #endregion

        #region Anagrafica Storico
        public void AnagraficaStoricoEsemDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaStoricoEsemDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaStoricoEsemInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaStoricoEsemInsert"))
            {
                AggiungiParametriPerStoricoEsem(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaStoricoEsemInsert: Riga non inserita");
                }
            }
        }

        public void AnagraficaStoricoCptDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaStoricoCptDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaStoricoCptInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaStoricoCptInsert"))
            {
                AggiungiParametriPerStoricoCpt(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaStoricoCptInsert: Riga non inserita");
                }
            }
        }

        private void AggiungiParametriPerStoricoEsem(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guidLavoratore", DbType.Guid, new Guid(dr["id"] as String));
            this.databaseCemi.AddInParameter(comando, "@guidImpresa", DbType.Guid, new Guid(dr["idImpresa"] as String));
            this.databaseCemi.AddInParameter(comando, "@codiceCatalogoSessione", DbType.Guid, new Guid(dr["idSessione"] as String));
            this.databaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, dr["codiceFiscale"] as String);
            if (dr["partitaIVA"] != Convert.DBNull && !String.IsNullOrEmpty(dr["partitaIVA"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@partitaIvaCodiceFiscaleImpresa", DbType.String, dr["partitaIVA"] as String);
            }
            if (dr["iscrizione_congiunta"] != Convert.DBNull && !String.IsNullOrEmpty(dr["iscrizione_congiunta"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@iscrizioneCongiunta", DbType.Boolean, (!String.IsNullOrEmpty(dr["iscrizione_congiunta"].ToString()) && dr["iscrizione_congiunta"].ToString() == "1") ? true : false);
            }
            else
            {
                this.databaseCemi.AddInParameter(comando, "@iscrizioneCongiunta", DbType.Boolean, false);
            }
            if (dr["frequenza"] != Convert.DBNull && !String.IsNullOrEmpty(dr["frequenza"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@frequenza", DbType.String, dr["frequenza"] as String);
            }
            if (dr["num_attestato"] != Convert.DBNull && !String.IsNullOrEmpty(dr["num_attestato"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@numeroAttestato", DbType.String, dr["num_attestato"] as String);
            }
        }

        private void AggiungiParametriPerStoricoCpt(DbCommand comando, DataRow dr)
        {
            this.databaseCemi.AddInParameter(comando, "@guidLavoratore", DbType.Guid, new Guid(dr["id"] as String));
            this.databaseCemi.AddInParameter(comando, "@guidImpresa", DbType.Guid, new Guid(dr["idImpresa"] as String));
            this.databaseCemi.AddInParameter(comando, "@codiceCatalogoSessione", DbType.Guid, new Guid(dr["idSessione"] as String));
            this.databaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String, dr["codiceFiscale"] as String);
            if (dr["partitaIVA"] != Convert.DBNull && !String.IsNullOrEmpty(dr["partitaIVA"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@partitaIvaCodiceFiscaleImpresa", DbType.String, dr["partitaIVA"] as String);
            }
            if (dr["iscrizione_congiunta"] != Convert.DBNull && !String.IsNullOrEmpty(dr["iscrizione_congiunta"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@iscrizioneCongiunta", DbType.Boolean, (!String.IsNullOrEmpty(dr["iscrizione_congiunta"].ToString()) && dr["iscrizione_congiunta"].ToString() == "1") ? true : false);
            }
            else
            {
                this.databaseCemi.AddInParameter(comando, "@iscrizioneCongiunta", DbType.Boolean, false);
            }
            if (dr["frequenza"] != Convert.DBNull && !String.IsNullOrEmpty(dr["frequenza"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@frequenza", DbType.String, dr["frequenza"] as String);
            }
            if (dr["num_attestato"] != Convert.DBNull && !String.IsNullOrEmpty(dr["num_attestato"].ToString()))
            {
                this.databaseCemi.AddInParameter(comando, "@numeroAttestato", DbType.String, dr["num_attestato"] as String);
            }
        }
        #endregion

        #region Anagrafica Storico Corsi
        public void AnagraficaStoricoCorsiEsemInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("USP_AnagraficaUnicaCalendarioEsemInsert"))
            {
                AggiungiParametriPerStoricoCorsiEsem(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaStoricoCorsiEsemInsert: Riga non inserita");
                }
            }
        }

        public void AnagraficaStoricoCorsiCptInsert(DataRow dr)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("USP_AnagraficaUnicaCalendarioCptInsert"))
            {
                AggiungiParametriPerStoricoCorsiCpt(comando, dr);

                if (this.databaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new AnagraficaUnicaException("AnagraficaStoricoCorsiCptInsert: Riga non inserita");
                }
            }
        }

        private void AggiungiParametriPerStoricoCorsiEsem(DbCommand comando, DataRow dr)
        {
            databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCatalogoCorso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogoCorso"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@codiceCatalogoCorso", DbType.String, dr["codiceCatalogoCorso"] as String);
            }
            if (dr["codiceCatalogoSessione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogoSessione"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@codiceCatalogoSessione", DbType.String, dr["codiceCatalogoSessione"] as String);
            }
            if (dr["sede"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sede"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@sede", DbType.String, dr["sede"] as String);
            }
            if (dr["aula"] != Convert.DBNull && !String.IsNullOrEmpty(dr["aula"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@aula", DbType.String, dr["aula"] as String);
            }
            if (dr["titoloCorso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["titoloCorso"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@titoloCorso", DbType.String, dr["titoloCorso"] as String);
            }
            if (dr["ora_inizio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_inizio"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraInizio", DbType.DateTime, DateTime.Parse(dr["ora_inizio"].ToString().Replace('.', ':')));
            }
            if (dr["ora_fine"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_fine"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraFine", DbType.DateTime, DateTime.Parse(dr["ora_fine"].ToString().Replace('.', ':')));
            }
            if (dr["ora_inizio_int"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_inizio_int"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraInizioInt", DbType.DateTime, DateTime.Parse(dr["ora_inizio_int"].ToString().Replace('.', ':')));
            }
            if (dr["ora_fine_int"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_fine_int"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraFineInt", DbType.DateTime, DateTime.Parse(dr["ora_fine_int"].ToString().Replace('.', ':')));
            }
            if (dr["dataInizio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataInizio"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, DateTime.Parse(dr["dataInizio"].ToString().Replace('.', ':')));
            }
            if (dr["dataFine"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataFine"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, DateTime.Parse(dr["dataInizio"].ToString().Replace('.', ':')));
            }
            //if (dr["oreLezione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oreLezione"].ToString()))
            //{
            //    databaseCemi.AddInParameter(comando, "@oreLezione", DbType.Int32, Int32.Parse(dr["oreLezione"].ToString()));
            //}
            if (dr["durataGiorni"] != Convert.DBNull && !String.IsNullOrEmpty(dr["durataGiorni"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@durataGiorni", DbType.Int32, Int32.Parse(dr["durataGiorni"].ToString()));
            }
            if (dr["numeroMinCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMinCorsisti"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@numeroMinCorsisti", DbType.Int32, Int32.Parse(dr["numeroMinCorsisti"].ToString()));
            }
            if (dr["numeroMaxCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMaxCorsisti"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@numeroMaxCorsisti", DbType.Int32, Int32.Parse(dr["numeroMaxCorsisti"].ToString()));
            }
            if (dr["tipoAttestato"] != Convert.DBNull && !String.IsNullOrEmpty(dr["tipoAttestato"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@tipoAttestato", DbType.String, dr["tipoAttestato"] as String);
            }
        }

        private void AggiungiParametriPerStoricoCorsiCpt(DbCommand comando, DataRow dr)
        {
            databaseCemi.AddInParameter(comando, "@guid", DbType.Guid, new Guid(dr["id"] as String));
            if (dr["codiceCatalogoCorso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogoCorso"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@codiceCatalogoCorso", DbType.String, dr["codiceCatalogoCorso"] as String);
            }
            if (dr["codiceCatalogoSessione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["codiceCatalogoSessione"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@codiceCatalogoSessione", DbType.String, dr["codiceCatalogoSessione"] as String);
            }
            if (dr["sede"] != Convert.DBNull && !String.IsNullOrEmpty(dr["sede"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@sede", DbType.String, dr["sede"] as String);
            }
            if (dr["aula"] != Convert.DBNull && !String.IsNullOrEmpty(dr["aula"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@aula", DbType.String, dr["aula"] as String);
            }
            if (dr["titoloCorso"] != Convert.DBNull && !String.IsNullOrEmpty(dr["titoloCorso"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@titoloCorso", DbType.String, dr["titoloCorso"] as String);
            }
            if (dr["ora_inizio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_inizio"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraInizio", DbType.DateTime, DateTime.Parse(dr["ora_inizio"].ToString().Replace('.', ':')));
            }
            if (dr["ora_fine"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_fine"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraFine", DbType.DateTime, DateTime.Parse(dr["ora_fine"].ToString().Replace('.', ':')));
            }
            if (dr["ora_inizio_int"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_inizio_int"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraInizioInt", DbType.DateTime, DateTime.Parse(dr["ora_inizio_int"].ToString().Replace('.', ':')));
            }
            if (dr["ora_fine_int"] != Convert.DBNull && !String.IsNullOrEmpty(dr["ora_fine_int"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@oraFineInt", DbType.DateTime, DateTime.Parse(dr["ora_fine_int"].ToString().Replace('.', ':')));
            }
            if (dr["dataInizio"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataInizio"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, DateTime.Parse(dr["dataInizio"].ToString().Replace('.', ':')));
            }
            if (dr["dataFine"] != Convert.DBNull && !String.IsNullOrEmpty(dr["dataFine"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, DateTime.Parse(dr["dataInizio"].ToString().Replace('.', ':')));
            }
            //if (dr["oreLezione"] != Convert.DBNull && !String.IsNullOrEmpty(dr["oreLezione"].ToString()))
            //{
            //    databaseCemi.AddInParameter(comando, "@oreLezione", DbType.Int32, Int32.Parse(dr["oreLezione"].ToString()));
            //}
            if (dr["durataGiorni"] != Convert.DBNull && !String.IsNullOrEmpty(dr["durataGiorni"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@durataGiorni", DbType.Int32, Int32.Parse(dr["durataGiorni"].ToString()));
            }
            if (dr["numeroMinCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMinCorsisti"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@numeroMinCorsisti", DbType.Int32, Int32.Parse(dr["numeroMinCorsisti"].ToString()));
            }
            if (dr["numeroMaxCorsisti"] != Convert.DBNull && !String.IsNullOrEmpty(dr["numeroMaxCorsisti"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@numeroMaxCorsisti", DbType.Int32, Int32.Parse(dr["numeroMaxCorsisti"].ToString()));
            }
            if (dr["tipoAttestato"] != Convert.DBNull && !String.IsNullOrEmpty(dr["tipoAttestato"].ToString()))
            {
                databaseCemi.AddInParameter(comando, "@tipoAttestato", DbType.String, dr["tipoAttestato"] as String);
            }
        }
        #endregion

        #region Anagrafica Edizioni
        public void AnagraficaEdizioniEsemDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCalendarioEsemDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }

        public void AnagraficaEdizioniCptDelete()
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCalendarioCptDelete"))
            {
                this.databaseCemi.ExecuteNonQuery(comando);
            }
        }
        #endregion

        public StringCollection GetCorsiNonPresentiEsem()
        {
            return GetCorsiMancanti("dbo.USP_AnagraficaUnicaStoricoCorsiEsemSelectNonPresenti");
        }

        public StringCollection GetCorsiNonPresentiCpt()
        {
            return GetCorsiMancanti("dbo.USP_AnagraficaUnicaStoricoCorsiCptSelectNonPresenti");
        }

        private StringCollection GetCorsiMancanti(string sp)
        {
            StringCollection corsiMancanti = new StringCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand(sp))
            {
                using (IDataReader reader = databaseCemi.ExecuteReader(comando))
                {
                    Int32 indiceCodiceCorso = reader.GetOrdinal("codiceCatalogoSessione");

                    while (reader.Read())
                    {
                        corsiMancanti.Add(reader.GetGuid(indiceCodiceCorso).ToString());
                    }
                }
            }

            return corsiMancanti;
        }
    }
}
