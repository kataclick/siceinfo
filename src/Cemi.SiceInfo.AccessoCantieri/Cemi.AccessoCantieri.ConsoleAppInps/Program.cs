﻿using System;
using System.Configuration;
using ActiveUp.Net.Mail;

namespace Cemi.AccessoCantieri.ConsoleAppInps
{
    internal class Program
    {
        private static readonly string PopAddress = ConfigurationManager.AppSettings["PopAddress"];
        private static readonly string Username = ConfigurationManager.AppSettings["Username"];
        private static readonly string Password = ConfigurationManager.AppSettings["Password"];
        private static readonly string FolderPath = ConfigurationManager.AppSettings["FolderPath"];

        private static void Main(string[] args)
        {
            using (Pop3Client pop3Client = new Pop3Client())
            {
                pop3Client.ConnectSsl(PopAddress, Username, Password);

                for (int i = 1; i <= pop3Client.MessageCount; i++)
                {
                    Message message = pop3Client.RetrieveMessageObject(i);

                    foreach (MimePart attachment in message.Attachments)
                    {
                        attachment.StoreToFile(String.Format(@"{0}{1} [{2}] {3}", FolderPath, DateTime.Now.ToString("yyyyMMdd hhmmss"), i, " inps.xls"));
                    }

                    //pop3Client.DeleteMessage(i);
                }

                pop3Client.Disconnect();
            }
        }
    }
}