//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.AnagraficaCondivisa.Type.Domain
{
    public partial class Indirizzo
    {
        #region Primitive Properties
    
        public virtual int IdIndirizzo
        {
            get;
            set;
        }
    
        public virtual int IdLavoratore
        {
            get { return _idLavoratore; }
            set
            {
                if (_idLavoratore != value)
                {
                    if (Lavoratori != null && Lavoratori.IdLavoratore != value)
                    {
                        Lavoratori = null;
                    }
                    _idLavoratore = value;
                }
            }
        }
        private int _idLavoratore;
    
        public virtual int IdEnteGestore
        {
            get { return _idEnteGestore; }
            set
            {
                if (_idEnteGestore != value)
                {
                    if (Ente != null && Ente.IdEnte != value)
                    {
                        Ente = null;
                    }
                    _idEnteGestore = value;
                }
            }
        }
        private int _idEnteGestore;
    
        public virtual int IdTipoIndirizzo
        {
            get { return _idTipoIndirizzo; }
            set
            {
                if (_idTipoIndirizzo != value)
                {
                    if (TipoIndirizzo != null && TipoIndirizzo.IdTipoIndirizzo != value)
                    {
                        TipoIndirizzo = null;
                    }
                    _idTipoIndirizzo = value;
                }
            }
        }
        private int _idTipoIndirizzo;
    
        public virtual string Denominazione
        {
            get;
            set;
        }
    
        public virtual string Civico
        {
            get;
            set;
        }
    
        public virtual string CodiceCatastaleComune
        {
            get;
            set;
        }
    
        public virtual string Cap
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataInserimento
        {
            get;
            set;
        }
    
        public virtual string UtenteInserimento
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataUltimaModifica
        {
            get;
            set;
        }
    
        public virtual string UtenteUltimaModifica
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual Ente Ente
        {
            get { return _ente; }
            set
            {
                if (!ReferenceEquals(_ente, value))
                {
                    var previousValue = _ente;
                    _ente = value;
                    FixupEnte(previousValue);
                }
            }
        }
        private Ente _ente;
    
        public virtual TipoIndirizzo TipoIndirizzo
        {
            get { return _tipoIndirizzo; }
            set
            {
                if (!ReferenceEquals(_tipoIndirizzo, value))
                {
                    var previousValue = _tipoIndirizzo;
                    _tipoIndirizzo = value;
                    FixupTipoIndirizzo(previousValue);
                }
            }
        }
        private TipoIndirizzo _tipoIndirizzo;
    
        public virtual Lavoratore Lavoratori
        {
            get { return _lavoratori; }
            set
            {
                if (!ReferenceEquals(_lavoratori, value))
                {
                    var previousValue = _lavoratori;
                    _lavoratori = value;
                    FixupLavoratori(previousValue);
                }
            }
        }
        private Lavoratore _lavoratori;

        #endregion
        #region Association Fixup
    
        private void FixupEnte(Ente previousValue)
        {
            if (previousValue != null && previousValue.Indirizzi.Contains(this))
            {
                previousValue.Indirizzi.Remove(this);
            }
    
            if (Ente != null)
            {
                if (!Ente.Indirizzi.Contains(this))
                {
                    Ente.Indirizzi.Add(this);
                }
                if (IdEnteGestore != Ente.IdEnte)
                {
                    IdEnteGestore = Ente.IdEnte;
                }
            }
        }
    
        private void FixupTipoIndirizzo(TipoIndirizzo previousValue)
        {
            if (previousValue != null && previousValue.Indirizzi.Contains(this))
            {
                previousValue.Indirizzi.Remove(this);
            }
    
            if (TipoIndirizzo != null)
            {
                if (!TipoIndirizzo.Indirizzi.Contains(this))
                {
                    TipoIndirizzo.Indirizzi.Add(this);
                }
                if (IdTipoIndirizzo != TipoIndirizzo.IdTipoIndirizzo)
                {
                    IdTipoIndirizzo = TipoIndirizzo.IdTipoIndirizzo;
                }
            }
        }
    
        private void FixupLavoratori(Lavoratore previousValue)
        {
            if (previousValue != null && previousValue.Indirizzi.Contains(this))
            {
                previousValue.Indirizzi.Remove(this);
            }
    
            if (Lavoratori != null)
            {
                if (!Lavoratori.Indirizzi.Contains(this))
                {
                    Lavoratori.Indirizzi.Add(this);
                }
                if (IdLavoratore != Lavoratori.IdLavoratore)
                {
                    IdLavoratore = Lavoratori.IdLavoratore;
                }
            }
        }

        #endregion
    }
}
