//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.AnagraficaCondivisa.Type.Domain
{
    public partial class Imprese
    {
        #region Primitive Properties
    
        public virtual int idImpresa
        {
            get;
            set;
        }
    
        public virtual Nullable<int> codiceCassaEdile
        {
            get;
            set;
        }
    
        public virtual string ragioneSociale
        {
            get;
            set;
        }
    
        public virtual string codiceFiscale
        {
            get;
            set;
        }
    
        public virtual string partitaIVA
        {
            get;
            set;
        }
    
        public virtual string codiceINPS
        {
            get;
            set;
        }
    
        public virtual string codiceINAIL
        {
            get;
            set;
        }
    
        public virtual string iscrizioneCCIAA
        {
            get;
            set;
        }
    
        public virtual int idContratto
        {
            get { return _idContratto; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idContratto != value)
                    {
                        if (Contratti != null && Contratti.idContratto != value)
                        {
                            Contratti = null;
                        }
                        _idContratto = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idContratto;
    
        public virtual string idTipoImpresa
        {
            get { return _idTipoImpresa; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idTipoImpresa != value)
                    {
                        if (TipiImpresa != null && TipiImpresa.idTipoImpresa != value)
                        {
                            TipiImpresa = null;
                        }
                        _idTipoImpresa = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private string _idTipoImpresa;
    
        public virtual string idAttivitaISTAT
        {
            get { return _idAttivitaISTAT; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idAttivitaISTAT != value)
                    {
                        if (AttivitaISTAT != null && AttivitaISTAT.idAttivitaISTAT != value)
                        {
                            AttivitaISTAT = null;
                        }
                        _idAttivitaISTAT = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private string _idAttivitaISTAT;
    
        public virtual Nullable<int> idNaturaGiuridica
        {
            get { return _idNaturaGiuridica; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idNaturaGiuridica != value)
                    {
                        if (NatureGiuridiche != null && NatureGiuridiche.idNaturaGiuridica != value)
                        {
                            NatureGiuridiche = null;
                        }
                        _idNaturaGiuridica = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _idNaturaGiuridica;
    
        public virtual string sitoWeb
        {
            get;
            set;
        }
    
        public virtual bool lavoratoreAutonomo
        {
            get;
            set;
        }
    
        public virtual Nullable<int> idStatoImpresa
        {
            get { return _idStatoImpresa; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idStatoImpresa != value)
                    {
                        if (StatiImpresa != null && StatiImpresa.idStatoImpresa != value)
                        {
                            StatiImpresa = null;
                        }
                        _idStatoImpresa = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _idStatoImpresa;
    
        public virtual Nullable<System.DateTime> dataUltimaVariazioneStato
        {
            get;
            set;
        }
    
        public virtual Nullable<bool> regolarita
        {
            get;
            set;
        }
    
        public virtual int idEnteGestore
        {
            get { return _idEnteGestore; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idEnteGestore != value)
                    {
                        if (Enti != null && Enti.idEnte != value)
                        {
                            Enti = null;
                        }
                        _idEnteGestore = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idEnteGestore;
    
        public virtual int idEnteInserimento
        {
            get { return _idEnteInserimento; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idEnteInserimento != value)
                    {
                        if (Enti1 != null && Enti1.idEnte != value)
                        {
                            Enti1 = null;
                        }
                        _idEnteInserimento = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idEnteInserimento;
    
        public virtual System.DateTime dataInserimento
        {
            get;
            set;
        }
    
        public virtual string utenteInserimento
        {
            get;
            set;
        }
    
        public virtual int idEnteUltimaModifica
        {
            get { return _idEnteUltimaModifica; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idEnteUltimaModifica != value)
                    {
                        if (Enti2 != null && Enti2.idEnte != value)
                        {
                            Enti2 = null;
                        }
                        _idEnteUltimaModifica = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idEnteUltimaModifica;
    
        public virtual System.DateTime dataUltimaModifica
        {
            get;
            set;
        }
    
        public virtual string utenteUltimaModifica
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual AttivitaIstat AttivitaISTAT
        {
            get { return _attivitaISTAT; }
            set
            {
                if (!ReferenceEquals(_attivitaISTAT, value))
                {
                    var previousValue = _attivitaISTAT;
                    _attivitaISTAT = value;
                    FixupAttivitaISTAT(previousValue);
                }
            }
        }
        private AttivitaIstat _attivitaISTAT;
    
        public virtual ICollection<Contatti> Contatti
        {
            get
            {
                if (_contatti == null)
                {
                    var newCollection = new FixupCollection<Contatti>();
                    newCollection.CollectionChanged += FixupContatti;
                    _contatti = newCollection;
                }
                return _contatti;
            }
            set
            {
                if (!ReferenceEquals(_contatti, value))
                {
                    var previousValue = _contatti as FixupCollection<Contatti>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupContatti;
                    }
                    _contatti = value;
                    var newValue = value as FixupCollection<Contatti>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupContatti;
                    }
                }
            }
        }
        private ICollection<Contatti> _contatti;
    
        public virtual Contratti Contratti
        {
            get { return _contratti; }
            set
            {
                if (!ReferenceEquals(_contratti, value))
                {
                    var previousValue = _contratti;
                    _contratti = value;
                    FixupContratti(previousValue);
                }
            }
        }
        private Contratti _contratti;
    
        public virtual Enti Enti
        {
            get { return _enti; }
            set
            {
                if (!ReferenceEquals(_enti, value))
                {
                    var previousValue = _enti;
                    _enti = value;
                    FixupEnti(previousValue);
                }
            }
        }
        private Enti _enti;
    
        public virtual Enti Enti1
        {
            get { return _enti1; }
            set
            {
                if (!ReferenceEquals(_enti1, value))
                {
                    var previousValue = _enti1;
                    _enti1 = value;
                    FixupEnti1(previousValue);
                }
            }
        }
        private Enti _enti1;
    
        public virtual Enti Enti2
        {
            get { return _enti2; }
            set
            {
                if (!ReferenceEquals(_enti2, value))
                {
                    var previousValue = _enti2;
                    _enti2 = value;
                    FixupEnti2(previousValue);
                }
            }
        }
        private Enti _enti2;
    
        public virtual NatureGiuridiche NatureGiuridiche
        {
            get { return _natureGiuridiche; }
            set
            {
                if (!ReferenceEquals(_natureGiuridiche, value))
                {
                    var previousValue = _natureGiuridiche;
                    _natureGiuridiche = value;
                    FixupNatureGiuridiche(previousValue);
                }
            }
        }
        private NatureGiuridiche _natureGiuridiche;
    
        public virtual StatiImpresa StatiImpresa
        {
            get { return _statiImpresa; }
            set
            {
                if (!ReferenceEquals(_statiImpresa, value))
                {
                    var previousValue = _statiImpresa;
                    _statiImpresa = value;
                    FixupStatiImpresa(previousValue);
                }
            }
        }
        private StatiImpresa _statiImpresa;
    
        public virtual TipiImpresa TipiImpresa
        {
            get { return _tipiImpresa; }
            set
            {
                if (!ReferenceEquals(_tipiImpresa, value))
                {
                    var previousValue = _tipiImpresa;
                    _tipiImpresa = value;
                    FixupTipiImpresa(previousValue);
                }
            }
        }
        private TipiImpresa _tipiImpresa;
    
        public virtual ICollection<Riferimenti> Riferimenti
        {
            get
            {
                if (_riferimenti == null)
                {
                    var newCollection = new FixupCollection<Riferimenti>();
                    newCollection.CollectionChanged += FixupRiferimenti;
                    _riferimenti = newCollection;
                }
                return _riferimenti;
            }
            set
            {
                if (!ReferenceEquals(_riferimenti, value))
                {
                    var previousValue = _riferimenti as FixupCollection<Riferimenti>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupRiferimenti;
                    }
                    _riferimenti = value;
                    var newValue = value as FixupCollection<Riferimenti>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupRiferimenti;
                    }
                }
            }
        }
        private ICollection<Riferimenti> _riferimenti;
    
        public virtual ICollection<Sedi> Sedi
        {
            get
            {
                if (_sedi == null)
                {
                    var newCollection = new FixupCollection<Sedi>();
                    newCollection.CollectionChanged += FixupSedi;
                    _sedi = newCollection;
                }
                return _sedi;
            }
            set
            {
                if (!ReferenceEquals(_sedi, value))
                {
                    var previousValue = _sedi as FixupCollection<Sedi>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupSedi;
                    }
                    _sedi = value;
                    var newValue = value as FixupCollection<Sedi>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupSedi;
                    }
                }
            }
        }
        private ICollection<Sedi> _sedi;

        #endregion
        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupAttivitaISTAT(AttivitaIstat previousValue)
        {
            if (previousValue != null && previousValue.Imprese.Contains(this))
            {
                previousValue.Imprese.Remove(this);
            }
    
            if (AttivitaISTAT != null)
            {
                if (!AttivitaISTAT.Imprese.Contains(this))
                {
                    AttivitaISTAT.Imprese.Add(this);
                }
                if (idAttivitaISTAT != AttivitaISTAT.idAttivitaISTAT)
                {
                    idAttivitaISTAT = AttivitaISTAT.idAttivitaISTAT;
                }
            }
            else if (!_settingFK)
            {
                idAttivitaISTAT = null;
            }
        }
    
        private void FixupContratti(Contratti previousValue)
        {
            if (previousValue != null && previousValue.Imprese.Contains(this))
            {
                previousValue.Imprese.Remove(this);
            }
    
            if (Contratti != null)
            {
                if (!Contratti.Imprese.Contains(this))
                {
                    Contratti.Imprese.Add(this);
                }
                if (idContratto != Contratti.idContratto)
                {
                    idContratto = Contratti.idContratto;
                }
            }
        }
    
        private void FixupEnti(Enti previousValue)
        {
            if (previousValue != null && previousValue.Imprese.Contains(this))
            {
                previousValue.Imprese.Remove(this);
            }
    
            if (Enti != null)
            {
                if (!Enti.Imprese.Contains(this))
                {
                    Enti.Imprese.Add(this);
                }
                if (idEnteGestore != Enti.idEnte)
                {
                    idEnteGestore = Enti.idEnte;
                }
            }
        }
    
        private void FixupEnti1(Enti previousValue)
        {
            if (previousValue != null && previousValue.Imprese1.Contains(this))
            {
                previousValue.Imprese1.Remove(this);
            }
    
            if (Enti1 != null)
            {
                if (!Enti1.Imprese1.Contains(this))
                {
                    Enti1.Imprese1.Add(this);
                }
                if (idEnteInserimento != Enti1.idEnte)
                {
                    idEnteInserimento = Enti1.idEnte;
                }
            }
        }
    
        private void FixupEnti2(Enti previousValue)
        {
            if (previousValue != null && previousValue.Imprese2.Contains(this))
            {
                previousValue.Imprese2.Remove(this);
            }
    
            if (Enti2 != null)
            {
                if (!Enti2.Imprese2.Contains(this))
                {
                    Enti2.Imprese2.Add(this);
                }
                if (idEnteUltimaModifica != Enti2.idEnte)
                {
                    idEnteUltimaModifica = Enti2.idEnte;
                }
            }
        }
    
        private void FixupNatureGiuridiche(NatureGiuridiche previousValue)
        {
            if (previousValue != null && previousValue.Imprese.Contains(this))
            {
                previousValue.Imprese.Remove(this);
            }
    
            if (NatureGiuridiche != null)
            {
                if (!NatureGiuridiche.Imprese.Contains(this))
                {
                    NatureGiuridiche.Imprese.Add(this);
                }
                if (idNaturaGiuridica != NatureGiuridiche.idNaturaGiuridica)
                {
                    idNaturaGiuridica = NatureGiuridiche.idNaturaGiuridica;
                }
            }
            else if (!_settingFK)
            {
                idNaturaGiuridica = null;
            }
        }
    
        private void FixupStatiImpresa(StatiImpresa previousValue)
        {
            if (previousValue != null && previousValue.Imprese.Contains(this))
            {
                previousValue.Imprese.Remove(this);
            }
    
            if (StatiImpresa != null)
            {
                if (!StatiImpresa.Imprese.Contains(this))
                {
                    StatiImpresa.Imprese.Add(this);
                }
                if (idStatoImpresa != StatiImpresa.idStatoImpresa)
                {
                    idStatoImpresa = StatiImpresa.idStatoImpresa;
                }
            }
            else if (!_settingFK)
            {
                idStatoImpresa = null;
            }
        }
    
        private void FixupTipiImpresa(TipiImpresa previousValue)
        {
            if (previousValue != null && previousValue.Imprese.Contains(this))
            {
                previousValue.Imprese.Remove(this);
            }
    
            if (TipiImpresa != null)
            {
                if (!TipiImpresa.Imprese.Contains(this))
                {
                    TipiImpresa.Imprese.Add(this);
                }
                if (idTipoImpresa != TipiImpresa.idTipoImpresa)
                {
                    idTipoImpresa = TipiImpresa.idTipoImpresa;
                }
            }
            else if (!_settingFK)
            {
                idTipoImpresa = null;
            }
        }
    
        private void FixupContatti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Contatti item in e.NewItems)
                {
                    item.Imprese = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Contatti item in e.OldItems)
                {
                    if (ReferenceEquals(item.Imprese, this))
                    {
                        item.Imprese = null;
                    }
                }
            }
        }
    
        private void FixupRiferimenti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Riferimenti item in e.NewItems)
                {
                    item.Imprese = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Riferimenti item in e.OldItems)
                {
                    if (ReferenceEquals(item.Imprese, this))
                    {
                        item.Imprese = null;
                    }
                }
            }
        }
    
        private void FixupSedi(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Sedi item in e.NewItems)
                {
                    item.Imprese = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Sedi item in e.OldItems)
                {
                    if (ReferenceEquals(item.Imprese, this))
                    {
                        item.Imprese = null;
                    }
                }
            }
        }

        #endregion
    }
}
