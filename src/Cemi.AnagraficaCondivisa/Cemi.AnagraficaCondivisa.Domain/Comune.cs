//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.AnagraficaCondivisa.Type.Domain
{
    public partial class Comune
    {
        #region Primitive Properties
    
        public virtual string CodiceCatastale
        {
            get;
            set;
        }
    
        public virtual string Denominazione
        {
            get;
            set;
        }
    
        public virtual string Provincia
        {
            get;
            set;
        }

        #endregion
    }
}
