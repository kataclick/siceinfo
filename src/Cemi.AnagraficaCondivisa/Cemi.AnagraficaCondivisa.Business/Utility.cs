﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Elmah;
namespace Cemi.AnagraficaCondivisa.Business
{
    public class Utility
    {
        public static Boolean VerificaCodiceFiscale(String codiceFiscale)
        {
            Boolean res = false;

            try
            {
                const String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                int[,] matricecod = GetMatriceControlloCf();

                codiceFiscale = codiceFiscale.ToUpper();
                Int32 codcontrollo = 0;
                // richiede una using System.Text;
                ASCIIEncoding ascii = new ASCIIEncoding();
                for (int i = 0; i <= 14; i++)
                {
                    String carattere = codiceFiscale[i].ToString();
                    Byte[] asciibytes = ascii.GetBytes(carattere);
                    Int32 asciicode = Int32.Parse(asciibytes[0].ToString());
                    codcontrollo = codcontrollo + matricecod[asciicode, i % 2];
                }

                if (alfabeto[codcontrollo % 26] == codiceFiscale[15])
                {
                    res = true;
                }
            }
            catch { }

            return res;
        }

        public static Boolean VerificaPartitaIva(String partitaIva)
        {
            Boolean res = true;

            try
            {
                int i;
                int s = 0;
                for (i = 0; i <= 9; i += 2)
                    s += partitaIva[i] - '0';
                for (i = 1; i <= 9; i += 2)
                {
                    int c = 2 * (partitaIva[i] - '0');
                    if (c > 9)
                        c = c - 9;
                    s += c;
                }
                if ((10 - s % 10) % 10 != partitaIva[10] - '0')
                    res = false;
            }
            catch
            {
                res = false;
            }

            return res;
        }

        public static Boolean VerificaTelefono(String telefono)
        {
            return Regex.IsMatch(telefono, @"^(\u002B\u0033\u0039)?(\d){5,}$");
        }

        public static Boolean VerificaEmail(String email)
        {
            return Regex.IsMatch(email, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }

        public static String NormalizzaStringa(String stringa)
        {
            if (String.IsNullOrWhiteSpace(stringa))
            {
                return null;
            }
            else
            {
                return stringa.Trim().ToUpper();
            }
        }

        public static void LogError(Exception exc)
        {
            try
            {
                Dictionary<String, String> config = new Dictionary<String, String>();
                config["connectionStringName"] = "ElmahConnection";
                config["applicationName"] = "WS AnagraficaCondivisa";

                SqlErrorLog sqlErrorLog = new SqlErrorLog(config);
                Error err = new Error(exc);

                sqlErrorLog.Log(err);
            }
            catch { }
        }

        #region Metodi privati
        private static int[,] GetMatriceControlloCf()
        {
            int[,] matricecod = new int[100, 2];
            //matrice per calcolare il carattere di controllo
            //il primo indice è il codice ascii del carattere il secondo indice la posizione pari o dispari
            // "A"
            matricecod[65, 0] = 1;
            matricecod[65, 1] = 0;
            // "B"
            matricecod[66, 0] = 0;
            matricecod[66, 1] = 1;
            // "C"
            matricecod[67, 0] = 5;
            matricecod[67, 1] = 2;
            // "D"
            matricecod[68, 0] = 7;
            matricecod[68, 1] = 3;
            // "E"
            matricecod[69, 0] = 9;
            matricecod[69, 1] = 4;
            // "F"
            matricecod[70, 0] = 13;
            matricecod[70, 1] = 5;
            // "G"
            matricecod[71, 0] = 15;
            matricecod[71, 1] = 6;
            // "H"
            matricecod[72, 0] = 17;
            matricecod[72, 1] = 7;
            // "I"
            matricecod[73, 0] = 19;
            matricecod[73, 1] = 8;
            // "J"
            matricecod[74, 0] = 21;
            matricecod[74, 1] = 9;
            // "K"
            matricecod[75, 0] = 2;
            matricecod[75, 1] = 10;
            // "L"
            matricecod[76, 0] = 4;
            matricecod[76, 1] = 11;
            // "M"
            matricecod[77, 0] = 18;
            matricecod[77, 1] = 12;
            // "N"
            matricecod[78, 0] = 20;
            matricecod[78, 1] = 13;
            // "O"
            matricecod[79, 0] = 11;
            matricecod[79, 1] = 14;
            // "P"
            matricecod[80, 0] = 3;
            matricecod[80, 1] = 15;
            // "Q"
            matricecod[81, 0] = 6;
            matricecod[81, 1] = 16;
            // "R"
            matricecod[82, 0] = 8;
            matricecod[82, 1] = 17;
            // "S"
            matricecod[83, 0] = 12;
            matricecod[83, 1] = 18;
            // "T"
            matricecod[84, 0] = 14;
            matricecod[84, 1] = 19;
            // "U"
            matricecod[85, 0] = 16;
            matricecod[85, 1] = 20;
            // "V"
            matricecod[86, 0] = 10;
            matricecod[86, 1] = 21;
            // "W"
            matricecod[87, 0] = 22;
            matricecod[87, 1] = 22;
            // "X"
            matricecod[88, 0] = 25;
            matricecod[88, 1] = 23;
            // "Y"
            matricecod[89, 0] = 24;
            matricecod[89, 1] = 24;
            // "Z"
            matricecod[90, 0] = 23;
            matricecod[90, 1] = 25;
            // "0"
            matricecod[48, 0] = 1;
            matricecod[48, 1] = 0;
            // "1"
            matricecod[49, 0] = 0;
            matricecod[49, 1] = 1;
            // "2"
            matricecod[50, 0] = 5;
            matricecod[50, 1] = 2;
            // "3"
            matricecod[51, 0] = 7;
            matricecod[51, 1] = 3;
            // "4"
            matricecod[52, 0] = 9;
            matricecod[52, 1] = 4;
            // "5"
            matricecod[53, 0] = 13;
            matricecod[53, 1] = 5;
            // "6"
            matricecod[54, 0] = 15;
            matricecod[54, 1] = 6;
            // "7"
            matricecod[55, 0] = 17;
            matricecod[55, 1] = 7;
            // "8"
            matricecod[56, 0] = 19;
            matricecod[56, 1] = 8;
            // "9"
            matricecod[57, 0] = 21;
            matricecod[57, 1] = 9;

            return matricecod;
        }
        #endregion
    }
}
