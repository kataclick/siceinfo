﻿
using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
namespace Cemi.AnagraficaCondivisa.Business
{
    public class LavoratoreServiceManager
    {
        private LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoLavoratore(Lavoratore lavoratore, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (lavoratore == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro lavoratore non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati del lavoratore
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckLavoratore(lavoratore, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        LavoratoreManager manager = new LavoratoreManager();
                        manager.InserisciLavoratore(lavoratore, idEnte, userName);

                        risultato.Codice = lavoratore.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (LavoratoreGiaPresenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.DatoGiaPresente,
                            Descrizione = "Il codice fiscale fornito è già presente nella base dati"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoLavoratore(Lavoratore lavoratore, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (lavoratore == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro lavoratore non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (lavoratore.Codice == null
                    || !lavoratore.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice del lavoratore non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati del lavoratore
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckLavoratore(lavoratore, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        LavoratoreManager manager = new LavoratoreManager();

                        try
                        {
                            manager.AggiornaLavoratore(lavoratore, idEnte, userName);

                            risultato.Codice = lavoratore.Codice.Value;
                            risultato.Eseguita = true;
                        }
                        catch (LavoratoreNonTrovatoException)
                        {
                            RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                            {
                                TipoErrore = TipoErrore.ErroreDatabase,
                                Descrizione = "Il codice lavoratore passato non corrisponde ad un lavoratore esistente"
                            };

                            risultato.Eseguita = false;
                            risultato.Errori.Add(errore);
                        }
                        catch (GestoreDifferenteException)
                        {
                            RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                            {
                                TipoErrore = TipoErrore.ErroreAutorizzazione,
                                Descrizione = "Il gestore del lavoratore non corrisponde a quello che sta effettuando l'operazione"
                            };

                            risultato.Eseguita = false;
                            risultato.Errori.Add(errore);
                        }
                        catch (LavoratoreEliminatoException)
                        {
                            RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                            {
                                TipoErrore = TipoErrore.ErroreDatabase,
                                Descrizione = "Il lavoratore di cui si vuole effettuare l'aggiornamento è stato cancellato"
                            };

                            risultato.Eseguita = false;
                            risultato.Errori.Add(errore);
                        }
                    }
                    catch (LavoratoreGiaPresenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.DatoGiaPresente,
                            Descrizione = "Il codice fiscale fornito è già presente nella base dati"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaLavoratore(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    LavoratoreManager manager = new LavoratoreManager();
                    manager.CancellaLavoratore(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (LavoratoreNonTrovatoException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice lavoratore passato non corrisponde ad un lavoratore esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore del lavoratore non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (LavoratoreEliminatoException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il lavoratore è già stato cancellato"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per il controllo dei dati
        private void CheckLavoratore(Lavoratore lavoratore, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            lavoratore.Cognome = Utility.NormalizzaStringa(lavoratore.Cognome);
            lavoratore.Nome = Utility.NormalizzaStringa(lavoratore.Nome);
            lavoratore.CodiceFiscale = Utility.NormalizzaStringa(lavoratore.CodiceFiscale);
            lavoratore.Sesso = Utility.NormalizzaStringa(lavoratore.Sesso);

            // COGNOME
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(lavoratore.Cognome))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Cognome non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (lavoratore.Cognome.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Cognome non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // NOME
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(lavoratore.Nome))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Nome non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (lavoratore.Nome.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Nome non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // CODICE FISCALE
            // - Stringa non vuota;
            // - Codice fiscale formalmente valido (primi 11 caratteri);
            if (String.IsNullOrWhiteSpace(lavoratore.CodiceFiscale))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceFiscale non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (!Utility.VerificaCodiceFiscale(lavoratore.CodiceFiscale))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro CodiceFiscale non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // SESSO
            // - Stringa non vuota;
            // - Valorizzato con M o F.
            if (String.IsNullOrWhiteSpace(lavoratore.Sesso))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Sesso non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (lavoratore.Sesso != "M" && lavoratore.Sesso != "F")
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Sesso non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // DATA NASCITA
            // - Data > 1/1/1900.
            if (lavoratore.DataNascita <= new DateTime(1900, 1, 1))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro DataNascita non è valido"
                };
                errori.Add(errore);
            }

            // LUOGO NASCITA
            // ???

            // TIPO LAVORATORE
            // - Tipologia presente in anagrafica.
            if (lavoratore.CodiceTipologia < 1 ||
                !lookupManager.ControllaTipoLavoratore(lavoratore.CodiceTipologia))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceTipologia non è valido"
                };
                errori.Add(errore);
            }

            // NAZIONALITA
            // - Nazionalità presente in anagrafica.
            if (!String.IsNullOrEmpty(lavoratore.CodiceNazionalita)
                && !lookupManager.ControllaNazionalita(lavoratore.CodiceNazionalita))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceNazionalita non è valido"
                };
                errori.Add(errore);
            }
        }
        #endregion
    }
}
