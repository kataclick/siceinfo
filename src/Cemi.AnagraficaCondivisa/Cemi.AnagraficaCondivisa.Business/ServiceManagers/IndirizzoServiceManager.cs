﻿
using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Business.Managers;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
namespace Cemi.AnagraficaCondivisa.Business
{
    public class IndirizzoServiceManager
    {
        private readonly LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoIndirizzo(Indirizzo indirizzo, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (indirizzo == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro indirizzo non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckIndirizzo(indirizzo, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    IndirizzoManager manager = new IndirizzoManager();
                    manager.InserisciIndirizzo(indirizzo, idEnte, userName);

                    risultato.Codice = indirizzo.Codice.Value;
                    risultato.Eseguita = true;
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoIndirizzo(Indirizzo indirizzo, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (indirizzo == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro indirizzo non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (indirizzo.Codice == null
                    || !indirizzo.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice dell'indirizzo non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckIndirizzo(indirizzo, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        IndirizzoManager manager = new IndirizzoManager();
                        manager.AggiornaIndirizzo(indirizzo, idEnte, userName);

                        risultato.Codice = indirizzo.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (IndirizzoNonTrovatoException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreDatabase,
                            Descrizione = "Il codice indirizzo passato non corrisponde ad un indirizzo esistente"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                    catch (GestoreDifferenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = "Il gestore dell'indirizzo non corrisponde a quello che sta effettuando l'operazione"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaIndirizzo(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    IndirizzoManager manager = new IndirizzoManager();
                    manager.CancellaIndirizzo(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (IndirizzoNonTrovatoException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice indirizzo passato non corrisponde ad un indirizzo esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore dell'indirizzo non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per i controlli
        private void CheckIndirizzo(Indirizzo indirizzo, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            indirizzo.Civico = Utility.NormalizzaStringa(indirizzo.Civico);
            indirizzo.Denominazione = Utility.NormalizzaStringa(indirizzo.Denominazione);

            // TIPO INDIRIZZO
            // - Tipologia presente in anagrafica.
            if (!lookupManager.ControllaTipoIndirizzo(indirizzo.CodiceTipoIndirizzo))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceTipoIndirizzo non è valido"
                };
                errori.Add(errore);
            }

            // LAVORATORE
            // - Lavoratore presente in anagrafica.
            if (!lookupManager.ControllaLavoratore(indirizzo.CodiceLavoratore))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceLavoratore non è valido"
                };
                errori.Add(errore);
            }

            // DENOMINAZIONE
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(indirizzo.Denominazione))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Denominazione non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (indirizzo.Denominazione.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Denominazione non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // COMUNE
            // - Presenza in anagrafica
            if (!String.IsNullOrEmpty(indirizzo.CodiceComune))
            {
                if (!lookupManager.ControllaComune(indirizzo.CodiceComune))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro CodiceComune non è valido"
                    };
                    errori.Add(errore);
                }
            }
            else
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceComune non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // CAP
            // - Presenza in anagrafica
            if (!String.IsNullOrEmpty(indirizzo.Cap))
            {
                //if (!lookupManager.ControllaCap(indirizzo.CodiceComune, indirizzo.Cap))
                //{
                //    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                //    {
                //        TipoErrore = TipoErrore.ParametroErrato,
                //        Descrizione = "Il parametro Cap non è valido"
                //    };
                //    errori.Add(errore);
                //}
            }
            else
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Cap non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
        }
        #endregion
    }
}
