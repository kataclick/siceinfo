﻿
using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
namespace Cemi.AnagraficaCondivisa.Business
{
    public class RiferimentoServiceManager
    {
        private readonly LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoRiferimento(Riferimento riferimento, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (riferimento == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro riferimento non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckRiferimento(riferimento, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    RiferimentoManager manager = new RiferimentoManager();
                    manager.InserisciRiferimento(riferimento, idEnte, userName);

                    risultato.Codice = riferimento.Codice.Value;
                    risultato.Eseguita = true;
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoRiferimento(Riferimento riferimento, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (riferimento == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro riferimento non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (riferimento.Codice == null
                    || !riferimento.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice del riferimento non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckRiferimento(riferimento, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        RiferimentoManager manager = new RiferimentoManager();
                        manager.AggiornaRiferimento(riferimento, idEnte, userName);

                        risultato.Codice = riferimento.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (RiferimentoNonTrovatoException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreDatabase,
                            Descrizione = "Il codice riferimento passato non corrisponde ad un riferimento esistente"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                    catch (GestoreDifferenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = "Il gestore del riferimento non corrisponde a quello che sta effettuando l'operazione"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaRiferimento(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    RiferimentoManager manager = new RiferimentoManager();
                    manager.CancellaRiferimento(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (RiferimentoNonTrovatoException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice riferimento passato non corrisponde ad un riferimento esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore del riferimento non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per i controlli
        private void CheckRiferimento(Riferimento riferimento, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            riferimento.Cognome = Utility.NormalizzaStringa(riferimento.Cognome);
            riferimento.Nome = Utility.NormalizzaStringa(riferimento.Nome);

            // COGNOME
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(riferimento.Cognome))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Cognome non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (riferimento.Cognome.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Cognome non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // NOME
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(riferimento.Nome))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Nome non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (riferimento.Nome.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Nome non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // TIPOLOGIA RIFERIMENTO
            // - Tipologia presente in anagrafica.
            if (!lookupManager.ControllaTipoRiferimento(riferimento.CodiceTipoRiferimento))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceTipoRiferimento non è valido"
                };
                errori.Add(errore);
            }

            // IMPRESA
            // - Impresa presente in anagrafica.
            if (!lookupManager.ControllaImpresa(riferimento.CodiceImpresa))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceImpresa non è valido"
                };
                errori.Add(errore);
            }

            // TELEFONO
            // - Stringa numerica di almeno 5 caratteri.
            if (!String.IsNullOrWhiteSpace(riferimento.Telefono)
                && !Utility.VerificaTelefono(riferimento.Telefono))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Telefono non è valido"
                };
                errori.Add(errore);
            }

            // FAX
            // - Stringa numerica di almeno 5 caratteri.
            if (!String.IsNullOrWhiteSpace(riferimento.Fax)
                && !Utility.VerificaTelefono(riferimento.Fax))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Fax non è valido"
                };
                errori.Add(errore);
            }

            // Email
            // - Email valida.
            if (!String.IsNullOrWhiteSpace(riferimento.Email)
                && !Utility.VerificaEmail(riferimento.Email))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Email non è valido"
                };
                errori.Add(errore);
            }

            // PEC
            // - Pec valida.
            if (!String.IsNullOrWhiteSpace(riferimento.PEC)
                && !Utility.VerificaEmail(riferimento.PEC))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Pec non è valido"
                };
                errori.Add(errore);
            }
        }
        #endregion
    }
}
