﻿using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Business.Managers;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class SedeServiceManager
    {
        private readonly LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoSede(Sede sede, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (sede == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro sede non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckSede(sede, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    SedeManager manager = new SedeManager();
                    manager.InserisciSede(sede, idEnte, userName);

                    risultato.Codice = sede.Codice.Value;
                    risultato.Eseguita = true;
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoSede(Sede sede, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (sede == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro sede non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (sede.Codice == null
                    || !sede.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice della sede non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckSede(sede, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        SedeManager manager = new SedeManager();
                        manager.AggiornaSede(sede, idEnte, userName);

                        risultato.Codice = sede.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (SedeNonTrovataException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreDatabase,
                            Descrizione = "Il codice sede passato non corrisponde ad una sede esistente"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                    catch (GestoreDifferenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = "Il gestore della sede non corrisponde a quello che sta effettuando l'operazione"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaSede(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    SedeManager manager = new SedeManager();
                    manager.CancellaSede(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (SedeNonTrovataException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice sede passato non corrisponde ad una sede esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore della sede non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per i controlli
        private void CheckSede(Sede sede, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            sede.Civico = Utility.NormalizzaStringa(sede.Civico);
            sede.Denominazione = Utility.NormalizzaStringa(sede.Denominazione);
            sede.Presso = Utility.NormalizzaStringa(sede.Presso);

            // TIPO SEDE
            // - Tipologia presente in anagrafica.
            if (!lookupManager.ControllaTipoSede(sede.CodiceTipoSede))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceTipoSede non è valido"
                };
                errori.Add(errore);
            }

            // IMPRESA
            // - Impresa presente in anagrafica.
            if (!lookupManager.ControllaImpresa(sede.CodiceImpresa))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceImpresa non è valido"
                };
                errori.Add(errore);
            }

            // DENOMINAZIONE
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(sede.Denominazione))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Denominazione non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (sede.Denominazione.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Denominazione non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // COMUNE
            // - Presenza in anagrafica
            if (!String.IsNullOrEmpty(sede.CodiceComune))
            {
                if (!lookupManager.ControllaComune(sede.CodiceComune))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro CodiceComune non è valido"
                    };
                    errori.Add(errore);
                }
            }
            else
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceComune non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // CAP
            // - Presenza in anagrafica
            if (!String.IsNullOrEmpty(sede.Cap))
            {
                //if (!lookupManager.ControllaCap(sede.CodiceComune, sede.Cap))
                //{
                //    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                //    {
                //        TipoErrore = TipoErrore.ParametroErrato,
                //        Descrizione = "Il parametro Cap non è valido"
                //    };
                //    errori.Add(errore);
                //}
            }
            else
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Cap non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // TELEFONO
            // - Stringa numerica di almeno 5 caratteri.
            if (!String.IsNullOrWhiteSpace(sede.Telefono)
                && !Utility.VerificaTelefono(sede.Telefono))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Telefono non è valido"
                };
                errori.Add(errore);
            }

            // FAX
            // - Stringa numerica di almeno 5 caratteri.
            if (!String.IsNullOrWhiteSpace(sede.Fax)
                && !Utility.VerificaTelefono(sede.Fax))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Fax non è valido"
                };
                errori.Add(errore);
            }

            // Email
            // - Email valida.
            if (!String.IsNullOrWhiteSpace(sede.Email)
                && !Utility.VerificaEmail(sede.Email))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Email non è valido"
                };
                errori.Add(errore);
            }

            // PEC
            // - Pec valida.
            if (!String.IsNullOrWhiteSpace(sede.Pec)
                && !Utility.VerificaEmail(sede.Pec))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Pec non è valido"
                };
                errori.Add(errore);
            }
        }
        #endregion
    }
}
