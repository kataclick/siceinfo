﻿using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Business.Managers;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class CorsoServiceManager
    {
        private readonly LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoCorso(Corso corso, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (corso == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro corso non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckCorso(corso, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        CorsoManager manager = new CorsoManager();
                        manager.InserisciCorso(corso, idEnte, userName);

                        risultato.Codice = corso.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (CorsoGiaPresenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.DatoGiaPresente,
                            Descrizione = "Il corso fornito è già presente nella base dati"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoCorso(Corso corso, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (corso == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro corso non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (corso.Codice == null
                    || !corso.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice del corso non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckCorso(corso, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        CorsoManager manager = new CorsoManager();
                        manager.AggiornaCorso(corso, idEnte, userName);

                        risultato.Codice = corso.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (CorsoNonTrovatoException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreDatabase,
                            Descrizione = "Il codice corso passato non corrisponde ad un corso esistente"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                    catch (GestoreDifferenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = "Il gestore del corso non corrisponde a quello che sta effettuando l'operazione"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaCorso(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    CorsoManager manager = new CorsoManager();
                    manager.CancellaCorso(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (CorsoNonTrovatoException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice corso passato non corrisponde ad un corso esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore del corso non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per i controlli
        private void CheckCorso(Corso corso, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            corso.CodiceCorso = Utility.NormalizzaStringa(corso.CodiceCorso);
            corso.Denominazione = Utility.NormalizzaStringa(corso.Denominazione);

            // CODICE CORSO
            // - Stringa non vuota.
            if (String.IsNullOrWhiteSpace(corso.CodiceCorso))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceCorso non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // DENOMINAZIONE CORSO
            // - Stringa non vuota.
            if (String.IsNullOrWhiteSpace(corso.Denominazione))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro Denominazione non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // DURATA
            // - Durata > 0.
            if (corso.Durata <= 0)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Durata non è valido"
                };
                errori.Add(errore);
            }
        }
        #endregion
    }
}
