﻿using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Business.Managers;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class CorsoLavoratoreServiceManager
    {
        private readonly LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoCorsoLavoratore(CorsoLavoratore corsoLavoratore, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (corsoLavoratore == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro corsoLavoratore non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckCorsoLavoratore(corsoLavoratore, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        CorsoLavoratoreManager manager = new CorsoLavoratoreManager();
                        manager.InserisciCorsoLavoratore(corsoLavoratore, idEnte, userName);

                        risultato.Codice = corsoLavoratore.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (CorsoLavoratoreGiaPresenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.DatoGiaPresente,
                            Descrizione = "Il corso fornito è già presente nella base dati"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoCorsoLavoratore(CorsoLavoratore corsoLavoratore, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (corsoLavoratore == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro corsoLavoratore non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (corsoLavoratore.Codice == null
                    || !corsoLavoratore.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice del corsoLavoratore non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckCorsoLavoratore(corsoLavoratore, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        CorsoLavoratoreManager manager = new CorsoLavoratoreManager();
                        manager.AggiornaCorsoLavoratore(corsoLavoratore, idEnte, userName);

                        risultato.Codice = corsoLavoratore.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (CorsoLavoratoreNonTrovatoException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreDatabase,
                            Descrizione = "Il codice corso passato non corrisponde ad un corso esistente"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                    catch (GestoreDifferenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = "Il gestore del corso non corrisponde a quello che sta effettuando l'operazione"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaCorsoLavoratore(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    CorsoLavoratoreManager manager = new CorsoLavoratoreManager();
                    manager.CancellaCorsoLavoratore(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (CorsoLavoratoreNonTrovatoException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice corso passato non corrisponde ad un corso esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore del corso non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per i controlli
        private void CheckCorsoLavoratore(CorsoLavoratore corsoLavoratore, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            corsoLavoratore.CodiceCorso = Utility.NormalizzaStringa(corsoLavoratore.CodiceCorso);
            corsoLavoratore.Cognome = Utility.NormalizzaStringa(corsoLavoratore.Cognome);
            corsoLavoratore.Nome = Utility.NormalizzaStringa(corsoLavoratore.Nome);
            corsoLavoratore.CodiceFiscale = Utility.NormalizzaStringa(corsoLavoratore.CodiceFiscale);
            corsoLavoratore.NumeroAttestato = Utility.NormalizzaStringa(corsoLavoratore.NumeroAttestato);
            corsoLavoratore.Sede = Utility.NormalizzaStringa(corsoLavoratore.Sede);
            corsoLavoratore.DenominazioneCorso = Utility.NormalizzaStringa(corsoLavoratore.DenominazioneCorso);

            // LAVORATORE
            // - Lavoratore presente in anagrafica.
            if (!lookupManager.ControllaLavoratore(corsoLavoratore.CodiceLavoratore))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceLavoratore non è valido"
                };
                errori.Add(errore);
            }

            // IMPRESA
            // - Impresa presente in anagrafica.
            if (corsoLavoratore.CodiceImpresa.HasValue && !lookupManager.ControllaImpresa(corsoLavoratore.CodiceImpresa.Value))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceImpresa non è valido"
                };
                errori.Add(errore);
            }

            // CODICE CORSO
            // - Stringa non vuota.
            if (String.IsNullOrWhiteSpace(corsoLavoratore.CodiceCorso))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceCorso non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // DATA NASCITA
            // - Data > 1/1/1900.
            if (corsoLavoratore.DataNascita.HasValue && corsoLavoratore.DataNascita.Value <= new DateTime(1900, 1, 1))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro DataNascita non è valido"
                };
                errori.Add(errore);
            }

            // DENOMINAZIONE CORSO
            // - Stringa non vuota.
            if (String.IsNullOrWhiteSpace(corsoLavoratore.DenominazioneCorso))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro DenominazioneCorso non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }

            // DATA INIZIO CORSO
            // - Data > 1/1/1900.
            if (corsoLavoratore.DataInizioCorso <= new DateTime(1900, 1, 1))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro DataInizioCorso non è valido"
                };
                errori.Add(errore);
            }

            // DATA FINE CORSO
            // - Data > 1/1/1900.
            if (corsoLavoratore.DataFineCorso.HasValue && corsoLavoratore.DataFineCorso.Value <= new DateTime(1900, 1, 1))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro DataFineCorso non è valido"
                };
                errori.Add(errore);
            }

            // DURATA
            // - Durata > 0.
            if (corsoLavoratore.Durata.HasValue && corsoLavoratore.Durata.Value <= 0)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro Durata non è valido"
                };
                errori.Add(errore);
            }
        }
        #endregion
    }
}
