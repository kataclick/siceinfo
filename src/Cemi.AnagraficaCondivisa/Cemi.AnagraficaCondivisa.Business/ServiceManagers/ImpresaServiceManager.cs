﻿using System;
using System.Collections.Generic;
using System.Net;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Exceptions;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class ImpresaServiceManager
    {
        private LookupManager lookupManager = new LookupManager();

        public RisultatoOperazione InserimentoImpresa(Impresa impresa, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (impresa == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro impresa non può essere null"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckImpresa(impresa, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        ImpresaManager manager = new ImpresaManager();
                        manager.InserisciImpresa(impresa, idEnte, userName);

                        risultato.Codice = impresa.Codice.Value;
                        risultato.Eseguita = true;
                    }
                    catch (ImpresaGiaPresenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.DatoGiaPresente,
                            Descrizione = "La partita IVA o il codice fiscale forniti sono già presenti nella base dati"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoImpresa(Impresa impresa, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (impresa == null)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro impresa non può essere null"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                if (impresa.Codice == null
                    || !impresa.Codice.HasValue)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro codice dell'impresa non può essere null"
                    };
                    risultato.Errori.Add(errore);
                }
            }

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                CheckImpresa(impresa, risultato.Errori);

                if (risultato.Errori.Count > 0)
                {
                    risultato.Eseguita = false;
                }
                else
                {
                    try
                    {
                        ImpresaManager manager = new ImpresaManager();

                        try
                        {
                            manager.AggiornaImpresa(impresa, idEnte, userName);

                            risultato.Codice = impresa.Codice.Value;
                            risultato.Eseguita = true;
                        }
                        catch (ImpresaNonTrovataException)
                        {
                            RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                            {
                                TipoErrore = TipoErrore.ErroreDatabase,
                                Descrizione = "Il codice impresa passato non corrisponde ad un'impresa esistente"
                            };

                            risultato.Eseguita = false;
                            risultato.Errori.Add(errore);
                        }
                        catch (GestoreDifferenteException)
                        {
                            RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                            {
                                TipoErrore = TipoErrore.ErroreAutorizzazione,
                                Descrizione = "Il gestore dell'impresa non corrisponde a quello che sta effettuando l'operazione"
                            };

                            risultato.Eseguita = false;
                            risultato.Errori.Add(errore);
                        }
                        catch (ImpresaEliminataException)
                        {
                            RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                            {
                                TipoErrore = TipoErrore.ErroreDatabase,
                                Descrizione = "L'impresa di cui si vuole effettuare l'aggiornamento è stata cancellata"
                            };

                            risultato.Eseguita = false;
                            risultato.Errori.Add(errore);
                        }
                    }
                    catch (ImpresaGiaPresenteException)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.DatoGiaPresente,
                            Descrizione = "La partita IVA o il codice fiscale forniti sono già presenti nella base dati"
                        };

                        risultato.Eseguita = false;
                        risultato.Errori.Add(errore);
                    }
                }
            }

            return risultato;
        }

        public RisultatoOperazione CancellaImpresa(Int32 codice, String userName, String IP)
        {
            RisultatoOperazione risultato = new RisultatoOperazione();
            risultato.Codice = -1;
            Int32 idEnte = -1;

            if (String.IsNullOrWhiteSpace(userName))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro userName non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }

            if (String.IsNullOrWhiteSpace(IP))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro IP non può essere null o stringa vuota"
                };
                risultato.Errori.Add(errore);
            }
            else
            {
                try
                {
                    IPAddress address = IPAddress.Parse(IP);

                    AuthorizationManager authManager = new AuthorizationManager();
                    idEnte = authManager.VerificaAutorizzazione(address);

                    if (idEnte == -1)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ErroreAutorizzazione,
                            Descrizione = String.Format("IP di chiamata {0} non riconosciuto", IP)
                        };
                        risultato.Errori.Add(errore);
                    }
                }
                catch (Exception exc)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = String.Format("L'indirizzo IP {0} non è valido", IP)
                    };
                    risultato.Errori.Add(errore);

                    Utility.LogError(exc);
                }
            }

            // Controllo dei dati dell'impresa
            if (risultato.Errori.Count > 0)
            {
                risultato.Eseguita = false;
            }
            else
            {
                try
                {
                    ImpresaManager manager = new ImpresaManager();
                    manager.CancellaImpresa(codice, idEnte, userName);

                    risultato.Eseguita = true;
                }
                catch (ImpresaNonTrovataException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "Il codice impresa passato non corrisponde ad un'impresa esistente"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (GestoreDifferenteException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreAutorizzazione,
                        Descrizione = "Il gestore dell'impresa non corrisponde a quello che sta effettuando l'operazione"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
                catch (ImpresaEliminataException)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ErroreDatabase,
                        Descrizione = "L'impresa è già stata cancellata"
                    };

                    risultato.Eseguita = false;
                    risultato.Errori.Add(errore);
                }
            }

            return risultato;
        }

        #region Metodi per il controllo dei dati
        private void CheckImpresa(Impresa impresa, List<RisultatoOperazioneErrore> errori)
        {
            // Normalizzo tutte le stringhe
            impresa.RagioneSociale = Utility.NormalizzaStringa(impresa.RagioneSociale);
            impresa.CodiceFiscale = Utility.NormalizzaStringa(impresa.CodiceFiscale);
            impresa.PartitaIva = Utility.NormalizzaStringa(impresa.PartitaIva);
            impresa.CodiceAttivitaISTAT = Utility.NormalizzaStringa(impresa.CodiceAttivitaISTAT);
            impresa.CodiceINAIL = Utility.NormalizzaStringa(impresa.CodiceINAIL);
            impresa.CodiceINPS = Utility.NormalizzaStringa(impresa.CodiceINPS);
            impresa.CodiceTipoImpresa = Utility.NormalizzaStringa(impresa.CodiceTipoImpresa);
            impresa.IscrizioneCCIAA = Utility.NormalizzaStringa(impresa.IscrizioneCCIAA);
            impresa.SitoWeb = Utility.NormalizzaStringa(impresa.SitoWeb);
            if (impresa.SedeLegale != null)
            {
                impresa.SedeLegale.Civico = Utility.NormalizzaStringa(impresa.SedeLegale.Civico);
                impresa.SedeLegale.CodiceComune = Utility.NormalizzaStringa(impresa.SedeLegale.CodiceComune);
                impresa.SedeLegale.CodiceToponimo = Utility.NormalizzaStringa(impresa.SedeLegale.CodiceToponimo);
                impresa.SedeLegale.Denominazione = Utility.NormalizzaStringa(impresa.SedeLegale.Denominazione);
                impresa.SedeLegale.Presso = Utility.NormalizzaStringa(impresa.SedeLegale.Presso);
            }

            // RAGIONE SOCIALE
            // - Stringa non vuota;
            // - Presenza di almeno due caratteri (che non siano spazi).
            if (String.IsNullOrWhiteSpace(impresa.RagioneSociale))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro RagioneSociale non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (impresa.RagioneSociale.Length < 3)
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro RagioneSociale non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // CODICE FISCALE
            // - Stringa non vuota;
            // - Codice fiscale formalmente valido (primi 11 caratteri);
            // - Partita Iva valida (con verifica codice controllo).
            if (String.IsNullOrWhiteSpace(impresa.CodiceFiscale))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceFiscale non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (!Utility.VerificaCodiceFiscale(impresa.CodiceFiscale)
                    && !Utility.VerificaPartitaIva(impresa.CodiceFiscale))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro CodiceFiscale non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // PARTITA IVA
            // - Stringa non vuota;
            // - Partita Iva valida (con verifica codice controllo).
            if (String.IsNullOrWhiteSpace(impresa.PartitaIva))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro PartitaIva non può essere null o stringa vuota"
                };
                errori.Add(errore);
            }
            else
            {
                if (!Utility.VerificaPartitaIva(impresa.PartitaIva))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro PartitaIva non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // CONTRATTO
            // - Codice contratto presente in anagrafica.
            if (impresa.CodiceContratto == null || !impresa.CodiceContratto.HasValue)
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroMancante,
                    Descrizione = "Il parametro CodiceContratto non può essere null"
                };
                errori.Add(errore);
            }
            else
            {
                if (!lookupManager.ControllaContratto(impresa.CodiceContratto.Value))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro CodiceContratto non è valido"
                    };
                    errori.Add(errore);
                }
            }

            // TIPO IMPRESA
            // - Tipo impresa presente in anagrafica.
            if (!String.IsNullOrWhiteSpace(impresa.CodiceTipoImpresa)
                && !lookupManager.ControllaTipoImpresa(impresa.CodiceTipoImpresa))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceTipoImpresa non è valido"
                };
                errori.Add(errore);
            }

            // ATTIVITA ISTAT
            // - Attività ISTAT presente in anagrafica.
            if (!String.IsNullOrWhiteSpace(impresa.CodiceAttivitaISTAT)
                && !lookupManager.ControllaAttivitaISTAT(impresa.CodiceAttivitaISTAT))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceAttivitaISTAT non è valido"
                };
                errori.Add(errore);
            }

            // NATURA GIURIDICA
            // - Natura giuridica presente in anagrafica.
            if (impresa.CodiceNaturaGiuridica.HasValue
                && !lookupManager.ControllaNaturaGiuridica(impresa.CodiceNaturaGiuridica.Value))
            {
                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ParametroErrato,
                    Descrizione = "Il parametro CodiceNaturaGiuridica non è valido"
                };
                errori.Add(errore);
            }

            // SEDE LEGALE
            if (impresa.SedeLegale != null)
            {
                // DENOMINAZIONE
                // - Stringa non vuota;
                // - Presenza di almeno due caratteri (che non siano spazi).
                if (String.IsNullOrWhiteSpace(impresa.SedeLegale.Denominazione))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroMancante,
                        Descrizione = "Il parametro Denominazione della SedeLegale non può essere null o stringa vuota"
                    };
                    errori.Add(errore);
                }
                else
                {
                    if (impresa.SedeLegale.Denominazione.Length < 3)
                    {
                        RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                        {
                            TipoErrore = TipoErrore.ParametroErrato,
                            Descrizione = "Il parametro Denominazione della SedeLegale non è valido"
                        };
                        errori.Add(errore);
                    }
                }

                // COMUNE
                // - Comune presente in anagrafica.
                if (String.IsNullOrWhiteSpace(impresa.SedeLegale.CodiceComune)
                    || !lookupManager.ControllaComune(impresa.SedeLegale.CodiceComune))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro CodiceComune della SedeLegale non è valido"
                    };
                    errori.Add(errore);
                }

                // CAP
                // - Cap presente in anagrafica.
                //if (String.IsNullOrWhiteSpace(impresa.SedeLegale.Cap)
                //    || String.IsNullOrWhiteSpace(impresa.SedeLegale.CodiceComune)
                //    || !lookupManager.ControllaCap(impresa.SedeLegale.CodiceComune, impresa.SedeLegale.Cap))
                //{
                //    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                //    {
                //        TipoErrore = TipoErrore.ParametroErrato,
                //        Descrizione = "Il parametro Cap della SedeLegale non è valido"
                //    };
                //    errori.Add(errore);
                //}

                // TELEFONO
                // - Stringa numerica di almeno 5 caratteri.
                if (!String.IsNullOrWhiteSpace(impresa.SedeLegale.Telefono)
                    && !Utility.VerificaTelefono(impresa.SedeLegale.Telefono))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Telefono della SedeLegale non è valido"
                    };
                    errori.Add(errore);
                }

                // FAX
                // - Stringa numerica di almeno 5 caratteri.
                if (!String.IsNullOrWhiteSpace(impresa.SedeLegale.Fax)
                    && !Utility.VerificaTelefono(impresa.SedeLegale.Fax))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Fax della SedeLegale non è valido"
                    };
                    errori.Add(errore);
                }

                // Email
                // - Email valida.
                if (!String.IsNullOrWhiteSpace(impresa.SedeLegale.Email)
                    && !Utility.VerificaEmail(impresa.SedeLegale.Email))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Email della SedeLegale non è valido"
                    };
                    errori.Add(errore);
                }

                // PEC
                // - Pec valida.
                if (!String.IsNullOrWhiteSpace(impresa.SedeLegale.Pec)
                    && !Utility.VerificaEmail(impresa.SedeLegale.Pec))
                {
                    RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                    {
                        TipoErrore = TipoErrore.ParametroErrato,
                        Descrizione = "Il parametro Pec della SedeLegale non è valido"
                    };
                    errori.Add(errore);
                }
            }
        }
        #endregion
    }
}
