﻿using System;
using System.Linq;
using System.Net;
using Cemi.AnagraficaCondivisa.Data;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class LookupManager
    {
        public Boolean ControllaContratto(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from contratto in context.ContrattoSet
                            where contratto.IdContratto == codice
                            select contratto;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaTipoImpresa(String codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from tipoImpresa in context.TipoImpresaSet
                            where tipoImpresa.IdTipoImpresa == codice
                            select tipoImpresa;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaAttivitaISTAT(String codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from attivitaISTAT in context.AttivitaIstatSet
                            where attivitaISTAT.IdAttivitaIstat == codice
                            select attivitaISTAT;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaNaturaGiuridica(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from naturaGiuridica in context.NaturaGiuridicaSet
                            where naturaGiuridica.IdNaturaGiuridica == codice
                            select naturaGiuridica;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaComune(String codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from comune in context.ComuneSet
                            where comune.CodiceCatastale == codice
                            select comune;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaCap(String codiceComune, String cap)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from capD in context.CapSet
                            where capD.CodiceCatastale == codiceComune && capD.Capp == cap
                            select capD;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaTipoRiferimento(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from tipoRiferimento in context.TipoRiferimentoSet
                            where tipoRiferimento.IdTipoRiferimento == codice
                            select tipoRiferimento;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaImpresa(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from impresa in context.ImpresaSet
                            where impresa.IdImpresa == codice
                            select impresa;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaLavoratore(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from lavoratore in context.LavoratoreSet
                            where lavoratore.IdLavoratore == codice
                            select lavoratore;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaTipoSede(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from tipoSede in context.TipoSedeSet
                            where tipoSede.IdTipoSede == codice
                            select tipoSede;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Int32 RecuperaIdEnteDaIP(IPAddress indirizzoIP)
        {
            Int32 idEnte = -1;
            String indirizzoIPStringa = indirizzoIP.ToString();

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from enteIp in context.EntiIPSet
                            where enteIp.IP == indirizzoIPStringa
                            select enteIp;

                if (query.Count() == 1)
                {
                    idEnte = query.Single().IdEnte;
                }
            }

            return idEnte;
        }

        public Boolean ControllaTipoIndirizzo(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from tipoIndirizzo in context.TipoIndirizzoSet
                            where tipoIndirizzo.IdTipoIndirizzo == codice
                            select tipoIndirizzo;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaTipoLavoratore(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from tipoLavoratore in context.TipoLavoratoreSet
                            where tipoLavoratore.IdTipoLavoratore == codice
                            select tipoLavoratore;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaNazionalita(String codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from nazionalita in context.NazionalitaSet
                            where nazionalita.IdNazionalita == codice
                            select nazionalita;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean ControllaCorso(Int32 codice)
        {
            Boolean res = false;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                var query = from corso in context.CorsoSet
                            where corso.IdCorso == codice
                            select corso;

                if (query.Count() == 1)
                {
                    res = true;
                }
            }

            return res;
        }
    }
}
