﻿using System;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using RiferimentoDomain = Cemi.AnagraficaCondivisa.Type.Domain.Riferimento;
using RiferimentoService = Cemi.AnagraficaCondivisa.Type.Entities.Riferimento;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class RiferimentoManager
    {
        public void InserisciRiferimento(RiferimentoService riferimentoService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione del riferimento passato al servizio
            // nel riferimento di dominio
            RiferimentoDomain riferimentoDomain = new RiferimentoDomain();

            riferimentoDomain.Cellulare = riferimentoService.Cellulare;
            riferimentoDomain.Cognome = riferimentoService.Cognome;
            riferimentoDomain.Email = riferimentoService.Email;
            riferimentoDomain.Fax = riferimentoService.Fax;
            riferimentoDomain.IdImpresa = riferimentoService.CodiceImpresa;
            riferimentoDomain.IdTipoRiferimento = riferimentoService.CodiceTipoRiferimento;
            riferimentoDomain.Nome = riferimentoService.Nome;
            riferimentoDomain.Pec = riferimentoService.PEC;
            riferimentoDomain.Telefono = riferimentoService.Telefono;
            riferimentoDomain.IdEnteGestore = idEnteInserimento;
            riferimentoDomain.DataInserimento = DateTime.Now;
            riferimentoDomain.UtenteInserimento = utenteInserimento;
            riferimentoDomain.DataUltimaModifica = DateTime.Now;
            riferimentoDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.RiferimentoSet.AddObject(riferimentoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("RiferimentoManager.InserisciRiferimento", "L'inserimento del riferimento è fallito");
                }

                riferimentoService.Codice = riferimentoDomain.IdRiferimento;
            }
        }

        public void AggiornaRiferimento(RiferimentoService riferimentoService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                RiferimentoDomain riferimentoDomain;
                var query = from riferimento in context.RiferimentoSet
                            where riferimento.IdRiferimento == riferimentoService.Codice.Value
                            select riferimento;

                try
                {
                    riferimentoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new RiferimentoNonTrovatoException();
                }

                if (riferimentoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                riferimentoDomain.Cellulare = riferimentoService.Cellulare;
                riferimentoDomain.Cognome = riferimentoService.Cognome;
                riferimentoDomain.Email = riferimentoService.Email;
                riferimentoDomain.Fax = riferimentoService.Fax;
                riferimentoDomain.IdImpresa = riferimentoService.CodiceImpresa;
                riferimentoDomain.IdTipoRiferimento = riferimentoService.CodiceTipoRiferimento;
                riferimentoDomain.Nome = riferimentoService.Nome;
                riferimentoDomain.Pec = riferimentoService.PEC;
                riferimentoDomain.Telefono = riferimentoService.Telefono;
                riferimentoDomain.DataUltimaModifica = DateTime.Now;
                riferimentoDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("RiferimentoManager.AggiornaAggiornaRiferimento", "L'aggiornamento del riferimento è fallito");
                }
            }
        }

        public void CancellaRiferimento(Int32 codice, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                RiferimentoDomain riferimentoDomain;
                var query = from riferimento in context.RiferimentoSet
                            where riferimento.IdRiferimento == codice
                            select riferimento;

                try
                {
                    riferimentoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new RiferimentoNonTrovatoException();
                }

                if (riferimentoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.RiferimentoSet.DeleteObject(riferimentoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("RiferimentoManager.CancellaRiferimento", "La cancellazione del riferimento è fallita");
                }
            }
        }
    }
}
