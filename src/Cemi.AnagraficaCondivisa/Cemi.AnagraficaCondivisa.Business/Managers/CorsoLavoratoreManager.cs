﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using CorsoLavoratoreDomain = Cemi.AnagraficaCondivisa.Type.Domain.LavoratoreCorso;
using CorsoLavoratoreService = Cemi.AnagraficaCondivisa.Type.Entities.CorsoLavoratore;

namespace Cemi.AnagraficaCondivisa.Business.Managers
{
    public class CorsoLavoratoreManager
    {
        public void InserisciCorsoLavoratore(CorsoLavoratoreService corsoService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione del corso passato al servizio
            // nel corso di dominio
            CorsoLavoratoreDomain corsoDomain = new CorsoLavoratoreDomain();

            corsoDomain.CodiceCorso = corsoService.CodiceCorso;
            corsoDomain.CodiceFiscale = corsoService.CodiceFiscale;
            corsoDomain.Cognome = corsoService.Cognome;
            corsoDomain.DataFineCorso = corsoService.DataFineCorso;
            corsoDomain.DataInizioCorso = corsoService.DataInizioCorso;
            corsoDomain.DataNascita = corsoService.DataNascita;
            corsoDomain.DenominazioneCorso = corsoService.DenominazioneCorso;
            corsoDomain.Durata = corsoService.Durata;
            corsoDomain.IdImpresa = corsoService.CodiceImpresa;
            corsoDomain.IdLavoratore = corsoService.CodiceLavoratore;
            corsoDomain.IscrizioneCongiunta = corsoService.IscrizioneCongiunta;
            corsoDomain.Nome = corsoService.Nome;
            corsoDomain.NumeroAttestato = corsoService.NumeroAttestato;
            corsoDomain.Sede = corsoService.Sede;
            corsoDomain.IdEnteGestore = idEnteInserimento;
            corsoDomain.DataInserimento = DateTime.Now;
            corsoDomain.UtenteInserimento = utenteInserimento;
            corsoDomain.DataUltimaModifica = DateTime.Now;
            corsoDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.LavoratoreCorsoSet.AddObject(corsoDomain);

                try
                {
                    if (context.SaveChanges() != 1)
                    {
                        throw new ImpresaServiceException("CorsoLavoratoreManager.InserisciCorso", "L'inserimento del corso lavoratore è fallito");
                    }
                }
                catch (Exception exc)
                {
                    if (exc.InnerException != null
                        && exc.InnerException.GetType() == typeof(SqlException))
                    {
                        SqlException sqlExc = (SqlException) exc.InnerException;

                        if (sqlExc.Number == 2627)
                        {
                            throw new CorsoLavoratoreGiaPresenteException();
                        }
                        else
                        {
                            throw exc;
                        }
                    }
                    else
                    {
                        throw exc;
                    }
                }

                corsoService.Codice = corsoDomain.IdLavoratoreCorso;
            }
        }

        public void AggiornaCorsoLavoratore(CorsoLavoratoreService corsoService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                CorsoLavoratoreDomain corsoDomain;
                var query = from corso in context.LavoratoreCorsoSet
                            where corso.IdLavoratoreCorso == corsoService.Codice.Value
                            select corso;

                try
                {
                    corsoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new CorsoLavoratoreNonTrovatoException();
                }

                if (corsoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                corsoDomain.CodiceCorso = corsoService.CodiceCorso;
                corsoDomain.CodiceFiscale = corsoService.CodiceFiscale;
                corsoDomain.Cognome = corsoService.Cognome;
                corsoDomain.DataFineCorso = corsoService.DataFineCorso;
                corsoDomain.DataInizioCorso = corsoService.DataInizioCorso;
                corsoDomain.DataNascita = corsoService.DataNascita;
                corsoDomain.DenominazioneCorso = corsoService.DenominazioneCorso;
                corsoDomain.Durata = corsoService.Durata;
                corsoDomain.IdImpresa = corsoService.CodiceImpresa;
                corsoDomain.IdLavoratore = corsoService.CodiceLavoratore;
                corsoDomain.IscrizioneCongiunta = corsoService.IscrizioneCongiunta;
                corsoDomain.Nome = corsoService.Nome;
                corsoDomain.NumeroAttestato = corsoService.NumeroAttestato;
                corsoDomain.Sede = corsoService.Sede;
                corsoDomain.DataUltimaModifica = DateTime.Now;
                corsoDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new CorsoServiceException("CorsoLavoratoreManager.AggiornaCorso", "L'aggiornamento del corso lavoratore è fallito");
                }
            }
        }

        public void CancellaCorsoLavoratore(Int32 codice, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                CorsoLavoratoreDomain corsoDomain;
                var query = from corso in context.LavoratoreCorsoSet
                            where corso.IdLavoratoreCorso == codice
                            select corso;

                try
                {
                    corsoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new CorsoLavoratoreNonTrovatoException();
                }

                if (corsoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.LavoratoreCorsoSet.DeleteObject(corsoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new CorsoServiceException("CorsoLavoratoreManager.CancellaCorso", "La cancellazione del corso lavoratore è fallita");
                }
            }
        }
    }
}
