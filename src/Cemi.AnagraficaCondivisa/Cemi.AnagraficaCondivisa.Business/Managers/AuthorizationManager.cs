﻿
using System;
using System.Net;
namespace Cemi.AnagraficaCondivisa.Business
{
    public class AuthorizationManager
    {
        public Int32 VerificaAutorizzazione(IPAddress ip)
        {
            Int32 idEnte = -1;

            LookupManager lookupManager = new LookupManager();
            idEnte = lookupManager.RecuperaIdEnteDaIP(ip);

            return idEnte;
        }
    }
}
