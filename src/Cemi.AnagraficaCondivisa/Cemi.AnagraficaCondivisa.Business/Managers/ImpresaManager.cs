﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using ImpresaDomain = Cemi.AnagraficaCondivisa.Type.Domain.Impresa;
using ImpresaService = Cemi.AnagraficaCondivisa.Type.Entities.Impresa;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class ImpresaManager
    {
        public void InserisciImpresa(ImpresaService impresaService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione dell'impresa passata al servizio
            // nell'impresa di dominio
            ImpresaDomain impresaDomain = new ImpresaDomain();

            impresaDomain.RagioneSociale = impresaService.RagioneSociale;
            impresaDomain.CodiceFiscale = impresaService.CodiceFiscale;
            impresaDomain.PartitaIVA = impresaService.PartitaIva;
            impresaDomain.CodiceINPS = impresaService.CodiceINPS;
            impresaDomain.CodiceINAIL = impresaService.CodiceINAIL;
            impresaDomain.IscrizioneCCIAA = impresaService.IscrizioneCCIAA;
            impresaDomain.IdContratto = impresaService.CodiceContratto.Value;
            impresaDomain.IdTipoImpresa = impresaService.CodiceTipoImpresa;
            impresaDomain.IdAttivitaISTAT = impresaService.CodiceAttivitaISTAT;
            impresaDomain.IdNaturaGiuridica = impresaService.CodiceNaturaGiuridica;
            impresaDomain.SitoWeb = impresaService.SitoWeb;
            impresaDomain.LavoratoreAutonomo = impresaService.LavoratoreAutonomo;
            impresaDomain.IdEnteGestore = idEnteInserimento;
            impresaDomain.IdEnteInserimento = idEnteInserimento;
            impresaDomain.DataInserimento = DateTime.Now;
            impresaDomain.UtenteInserimento = utenteInserimento;
            impresaDomain.IdEnteUltimaModifica = idEnteInserimento;
            impresaDomain.DataUltimaModifica = DateTime.Now;
            impresaDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.ImpresaSet.AddObject(impresaDomain);

                try
                {
                    if (context.SaveChanges() != 1)
                    {
                        throw new ImpresaServiceException("ImpresaManager.InserisciImpresa", "L'inserimento dell'impresa è fallito");
                    }
                }
                catch (Exception exc)
                {
                    if (exc.InnerException != null
                        && exc.InnerException.GetType() == typeof(SqlException))
                    {
                        SqlException sqlExc = (SqlException) exc.InnerException;

                        if (sqlExc.Number == 547)
                        {
                            throw new ImpresaGiaPresenteException();
                        }
                        else
                        {
                            throw exc;
                        }
                    }
                    else
                    {
                        throw exc;
                    }
                }

                impresaService.Codice = impresaDomain.IdImpresa;
            }
        }

        public void AggiornaImpresa(ImpresaService impresaService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                ImpresaDomain impresaDomain;
                var query = from impresa in context.ImpresaSet
                            where impresa.IdImpresa == impresaService.Codice.Value
                            select impresa;

                try
                {
                    impresaDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new ImpresaNonTrovataException();
                }

                if (impresaDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                if (impresaDomain.Cancellata)
                {
                    throw new ImpresaEliminataException();
                }

                impresaDomain.RagioneSociale = impresaService.RagioneSociale;
                impresaDomain.CodiceFiscale = impresaService.CodiceFiscale;
                impresaDomain.PartitaIVA = impresaService.PartitaIva;
                impresaDomain.CodiceINPS = impresaService.CodiceINPS;
                impresaDomain.CodiceINAIL = impresaService.CodiceINAIL;
                impresaDomain.IscrizioneCCIAA = impresaService.IscrizioneCCIAA;
                impresaDomain.IdContratto = impresaService.CodiceContratto.Value;
                impresaDomain.IdTipoImpresa = impresaService.CodiceTipoImpresa;
                impresaDomain.IdAttivitaISTAT = impresaService.CodiceAttivitaISTAT;
                impresaDomain.IdNaturaGiuridica = impresaService.CodiceNaturaGiuridica;
                impresaDomain.SitoWeb = impresaService.SitoWeb;
                impresaDomain.LavoratoreAutonomo = impresaService.LavoratoreAutonomo;
                impresaDomain.IdEnteUltimaModifica = idEnteAggiornamento;
                impresaDomain.DataUltimaModifica = DateTime.Now;
                impresaDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("ImpresaManager.AggiornaImpresa", "L'aggiornamento dell'impresa è fallito");
                }
            }
        }

        public void CancellaImpresa(int codice, int idEnteCancellazione, string userName)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                ImpresaDomain impresaDomain;
                var query = from impresa in context.ImpresaSet
                            where impresa.IdImpresa == codice
                            select impresa;

                try
                {
                    impresaDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new ContattoNonTrovatoException();
                }

                if (impresaDomain.IdEnteGestore != idEnteCancellazione)
                {
                    throw new GestoreDifferenteException();
                }

                if (impresaDomain.Cancellata)
                {
                    throw new ImpresaEliminataException();
                }

                impresaDomain.Cancellata = true;
                impresaDomain.DataCancellazione = DateTime.Now;
                impresaDomain.UtenteCancellazione = userName;

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("ImpresaManager.CancellaImpresa", "La cancellazione dell'impresa è fallita");
                }
            }
        }
    }
}
