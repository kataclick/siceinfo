﻿
using System;
using System.Data.SqlClient;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using LavoratoreDomain = Cemi.AnagraficaCondivisa.Type.Domain.Lavoratore;
using LavoratoreService = Cemi.AnagraficaCondivisa.Type.Entities.Lavoratore;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class LavoratoreManager
    {
        public void InserisciLavoratore(LavoratoreService lavoratoreService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione del lavoratore passato al servizio
            // nel lavoratore di dominio
            LavoratoreDomain lavoratoreDomain = new LavoratoreDomain();

            lavoratoreDomain.Cognome = lavoratoreService.Cognome;
            lavoratoreDomain.Nome = lavoratoreService.Nome;
            lavoratoreDomain.CodiceFiscale = lavoratoreService.CodiceFiscale;
            lavoratoreDomain.Sesso = lavoratoreService.Sesso;
            lavoratoreDomain.DataNascita = lavoratoreService.DataNascita;
            if (!String.IsNullOrEmpty(lavoratoreService.LuogoNascita))
            {
                lavoratoreDomain.LuogoNascita = lavoratoreService.LuogoNascita;
            }
            if (!String.IsNullOrEmpty(lavoratoreService.CodiceNazionalita))
            {
                lavoratoreDomain.IdNazionalita = lavoratoreService.CodiceNazionalita;
            }
            lavoratoreDomain.IdTipoLavoratore = lavoratoreService.CodiceTipologia;
            lavoratoreDomain.IdEnteGestore = idEnteInserimento;
            lavoratoreDomain.IdEnteInserimento = idEnteInserimento;
            lavoratoreDomain.DataInserimento = DateTime.Now;
            lavoratoreDomain.UtenteInserimento = utenteInserimento;
            lavoratoreDomain.IdEnteUltimaModifica = idEnteInserimento;
            lavoratoreDomain.DataUltimaModifica = DateTime.Now;
            lavoratoreDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.LavoratoreSet.AddObject(lavoratoreDomain);

                try
                {
                    if (context.SaveChanges() != 1)
                    {
                        throw new LavoratoreServiceException("LavoratoreManager.InserisciLavoratore", "L'inserimento del lavoratore è fallito");
                    }
                }
                catch (Exception exc)
                {
                    if (exc.InnerException != null
                        && exc.InnerException.GetType() == typeof(SqlException))
                    {
                        SqlException sqlExc = (SqlException) exc.InnerException;

                        if (sqlExc.Number == 547)
                        {
                            throw new LavoratoreGiaPresenteException();
                        }
                        else
                        {
                            throw exc;
                        }
                    }
                    else
                    {
                        throw exc;
                    }
                }

                lavoratoreService.Codice = lavoratoreDomain.IdLavoratore;
            }
        }

        public void AggiornaLavoratore(LavoratoreService lavoratoreService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                LavoratoreDomain lavoratoreDomain;
                var query = from lavoratore in context.LavoratoreSet
                            where lavoratore.IdLavoratore == lavoratoreService.Codice.Value
                            select lavoratore;

                try
                {
                    lavoratoreDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new LavoratoreNonTrovatoException();
                }

                if (lavoratoreDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                if (lavoratoreDomain.Cancellato)
                {
                    throw new LavoratoreEliminatoException();
                }

                lavoratoreDomain.Cognome = lavoratoreService.Cognome;
                lavoratoreDomain.Nome = lavoratoreService.Nome;
                lavoratoreDomain.CodiceFiscale = lavoratoreService.CodiceFiscale;
                lavoratoreDomain.Sesso = lavoratoreService.Sesso;
                lavoratoreDomain.DataNascita = lavoratoreService.DataNascita;
                if (!String.IsNullOrEmpty(lavoratoreService.LuogoNascita))
                {
                    lavoratoreDomain.LuogoNascita = lavoratoreService.LuogoNascita;
                }
                if (!String.IsNullOrEmpty(lavoratoreService.CodiceNazionalita))
                {
                    lavoratoreDomain.IdNazionalita = lavoratoreService.CodiceNazionalita;
                }
                lavoratoreDomain.IdTipoLavoratore = lavoratoreService.CodiceTipologia;
                lavoratoreDomain.IdEnteUltimaModifica = idEnteAggiornamento;
                lavoratoreDomain.DataUltimaModifica = DateTime.Now;
                lavoratoreDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("LavoratoreManager.AggiornaLavoratore", "L'aggiornamento del lavoratore è fallito");
                }
            }
        }

        public void CancellaLavoratore(Int32 codice, Int32 idEnteCancellazione, string userName)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                LavoratoreDomain lavoratoreDomain;
                var query = from lavoratore in context.LavoratoreSet
                            where lavoratore.IdLavoratore == codice
                            select lavoratore;

                try
                {
                    lavoratoreDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new LavoratoreNonTrovatoException();
                }

                if (lavoratoreDomain.IdEnteGestore != idEnteCancellazione)
                {
                    throw new GestoreDifferenteException();
                }

                if (lavoratoreDomain.Cancellato)
                {
                    throw new LavoratoreEliminatoException();
                }

                lavoratoreDomain.Cancellato = true;
                lavoratoreDomain.DataCancellazione = DateTime.Now;
                lavoratoreDomain.UtenteCancellazione = userName;

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("LavoratoreManager.CancellaLavoratore", "La cancellazione del lavoratore è fallita");
                }
            }
        }
    }
}
