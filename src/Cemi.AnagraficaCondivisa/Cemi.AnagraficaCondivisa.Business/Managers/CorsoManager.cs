﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using CorsoDomain = Cemi.AnagraficaCondivisa.Type.Domain.Corso;
using CorsoService = Cemi.AnagraficaCondivisa.Type.Entities.Corso;

namespace Cemi.AnagraficaCondivisa.Business.Managers
{
    public class CorsoManager
    {
        public void InserisciCorso(CorsoService corsoService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione del corso passato al servizio
            // nel corso di dominio
            CorsoDomain corsoDomain = new CorsoDomain();

            corsoDomain.Codice = corsoService.CodiceCorso;
            corsoDomain.Denominazione = corsoService.Denominazione;
            corsoDomain.Durata = corsoService.Durata;
            corsoDomain.IdEnteGestore = idEnteInserimento;
            corsoDomain.DataInserimento = DateTime.Now;
            corsoDomain.UtenteInserimento = utenteInserimento;
            corsoDomain.DataUltimaModifica = DateTime.Now;
            corsoDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.CorsoSet.AddObject(corsoDomain);

                try
                {
                    if (context.SaveChanges() != 1)
                    {
                        throw new CorsoServiceException("CorsoManager.InserisciCorso", "L'inserimento del corso è fallito");
                    }
                }
                catch (Exception exc)
                {
                    if (exc.InnerException != null
                        && exc.InnerException.GetType() == typeof(SqlException))
                    {
                        SqlException sqlExc = (SqlException) exc.InnerException;

                        if (sqlExc.Number == 2627)
                        {
                            throw new CorsoGiaPresenteException();
                        }
                        else
                        {
                            throw exc;
                        }
                    }
                    else
                    {
                        throw exc;
                    }
                }

                corsoService.Codice = corsoDomain.IdCorso;
            }
        }

        public void AggiornaCorso(CorsoService corsoService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                CorsoDomain corsoDomain;
                var query = from corso in context.CorsoSet
                            where corso.IdCorso == corsoService.Codice.Value
                            select corso;

                try
                {
                    corsoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new CorsoNonTrovatoException();
                }

                if (corsoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                corsoDomain.Codice = corsoService.CodiceCorso;
                corsoDomain.Denominazione = corsoService.Denominazione;
                corsoDomain.Durata = corsoService.Durata;
                corsoDomain.DataUltimaModifica = DateTime.Now;
                corsoDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new CorsoServiceException("CorsoManager.AggiornaCorso", "L'aggiornamento del corso è fallito");
                }
            }
        }

        public void CancellaCorso(Int32 codice, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                CorsoDomain corsoDomain;
                var query = from corso in context.CorsoSet
                            where corso.IdCorso == codice
                            select corso;

                try
                {
                    corsoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new CorsoNonTrovatoException();
                }

                if (corsoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.CorsoSet.DeleteObject(corsoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new CorsoServiceException("CorsoManager.CancellaCorso", "La cancellazione del corso è fallita");
                }
            }
        }
    }
}
