﻿using System;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using ContattoDomain = Cemi.AnagraficaCondivisa.Type.Domain.ContattoLavoratore;
using ContattoService = Cemi.AnagraficaCondivisa.Type.Entities.ContattoLavoratore;

namespace Cemi.AnagraficaCondivisa.Business.Managers
{
    public class ContattoLavoratoreManager
    {
        public void InserisciContatto(ContattoService contattoService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione del contatto passato al servizio
            // nel contatto di dominio
            ContattoDomain contattoDomain = new ContattoDomain();

            contattoDomain.Cellulare = contattoService.Cellulare;
            contattoDomain.Email = contattoService.Email;
            contattoDomain.Fax = contattoService.Fax;
            contattoDomain.IdLavoratore = contattoService.CodiceLavoratore;
            contattoDomain.Nota = contattoService.Nota;
            contattoDomain.Pec = contattoService.PEC;
            contattoDomain.Telefono = contattoService.Telefono;
            contattoDomain.IdEnteGestore = idEnteInserimento;
            contattoDomain.DataInserimento = DateTime.Now;
            contattoDomain.UtenteInserimento = utenteInserimento;
            contattoDomain.DataUltimaModifica = DateTime.Now;
            contattoDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.ContattoLavoratoreSet.AddObject(contattoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("ContattoLavoratoreManager.InserisciContatto", "L'inserimento del contatto è fallito");
                }

                contattoService.Codice = contattoDomain.IdContattoLavoratore;
            }
        }

        public void AggiornaContatto(ContattoService contattoService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                ContattoDomain contattoDomain;
                var query = from contatto in context.ContattoLavoratoreSet
                            where contatto.IdContattoLavoratore == contattoService.Codice.Value
                            select contatto;

                try
                {
                    contattoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new ContattoNonTrovatoException();
                }

                if (contattoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                contattoDomain.Cellulare = contattoService.Cellulare;
                contattoDomain.Email = contattoService.Email;
                contattoDomain.Fax = contattoService.Fax;
                contattoDomain.IdLavoratore = contattoService.CodiceLavoratore;
                contattoDomain.Nota = contattoService.Nota;
                contattoDomain.Pec = contattoService.PEC;
                contattoDomain.Telefono = contattoService.Telefono;
                contattoDomain.DataUltimaModifica = DateTime.Now;
                contattoDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("ContattoLavoratoreManager.AggiornaContatto", "L'aggiornamento del contatto è fallito");
                }
            }
        }

        public void CancellaContatto(Int32 codice, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                ContattoDomain contattoDomain;
                var query = from contatto in context.ContattoLavoratoreSet
                            where contatto.IdContattoLavoratore == codice
                            select contatto;

                try
                {
                    contattoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new ContattoNonTrovatoException();
                }

                if (contattoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.ContattoLavoratoreSet.DeleteObject(contattoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("ContattoLavoratoreManager.CancellaContatto", "La cancellazione del contatto è fallita");
                }
            }
        }
    }
}
