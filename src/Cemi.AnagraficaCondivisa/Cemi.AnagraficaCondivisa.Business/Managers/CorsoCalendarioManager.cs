﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using CorsoCalendarioDomain = Cemi.AnagraficaCondivisa.Type.Domain.CorsoCalendario;
using CorsoCalendarioService = Cemi.AnagraficaCondivisa.Type.Entities.CorsoCalendario;

namespace Cemi.AnagraficaCondivisa.Business.Managers
{
    public class CorsoCalendarioManager
    {
        public void InserisciCorsoCalendario(CorsoCalendarioService corsoCalendarioService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione del corso passato al servizio
            // nel corso di dominio
            CorsoCalendarioDomain corsoCalendarioDomain = new CorsoCalendarioDomain();

            corsoCalendarioDomain.Fine = corsoCalendarioService.Fine;
            corsoCalendarioDomain.IdCorso = corsoCalendarioService.CodiceCorso;
            corsoCalendarioDomain.Inizio = corsoCalendarioService.Inizio;
            corsoCalendarioDomain.Progressivo = corsoCalendarioService.Progressivo;
            corsoCalendarioDomain.Sede = corsoCalendarioService.Sede;
            corsoCalendarioDomain.IdEnteGestore = idEnteInserimento;
            corsoCalendarioDomain.DataInserimento = DateTime.Now;
            corsoCalendarioDomain.UtenteInserimento = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.CorsoCalendarioSet.AddObject(corsoCalendarioDomain);

                try
                {
                    if (context.SaveChanges() != 1)
                    {
                        throw new CorsoServiceException("CorsoCalendarioManager.InserisciCorsoCalendario", "L'inserimento del corso calendario è fallito");
                    }
                }
                catch (Exception exc)
                {
                    if (exc.InnerException != null
                        && exc.InnerException.GetType() == typeof(SqlException))
                    {
                        SqlException sqlExc = (SqlException) exc.InnerException;

                        if (sqlExc.Number == 2627)
                        {
                            throw new CorsoCalendarioGiaPresenteException();
                        }
                        else
                        {
                            throw exc;
                        }
                    }
                    else
                    {
                        throw exc;
                    }
                }

                //corsoCalendarioService.Codice = corsoCalendarioDomain.IdCorsoCalendario;
            }
        }

        public void CancellaCorsoCalendario(Int32 codiceCorso, DateTime inizio, Int32 progressivo, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                CorsoCalendarioDomain corsoCalendarioDomain;
                var query = from corsoCalendario in context.CorsoCalendarioSet
                            where corsoCalendario.IdCorso == codiceCorso
                                && corsoCalendario.Inizio == inizio
                                && corsoCalendario.Progressivo == progressivo
                            select corsoCalendario;

                try
                {
                    corsoCalendarioDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new CorsoCalendarioNonTrovatoException();
                }

                if (corsoCalendarioDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.CorsoCalendarioSet.DeleteObject(corsoCalendarioDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new CorsoServiceException("CorsoCalendarioManager.CancellaCorsoCalendario", "La cancellazione del corso calendario è fallita");
                }
            }
        }
    }
}
