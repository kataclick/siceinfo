﻿using System;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using IndirizzoDomain = Cemi.AnagraficaCondivisa.Type.Domain.Indirizzo;
using IndirizzoService = Cemi.AnagraficaCondivisa.Type.Entities.Indirizzo;

namespace Cemi.AnagraficaCondivisa.Business.Managers
{
    public class IndirizzoManager
    {
        public void InserisciIndirizzo(IndirizzoService indirizzoService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione dell'indirizzo passato al servizio
            // nell'indirizzo di dominio
            IndirizzoDomain indirizzoDomain = new IndirizzoDomain();

            indirizzoDomain.Cap = indirizzoService.Cap;
            indirizzoDomain.Civico = indirizzoService.Civico;
            indirizzoDomain.CodiceCatastaleComune = indirizzoService.CodiceComune;
            indirizzoDomain.Denominazione = indirizzoService.Denominazione;
            indirizzoDomain.IdLavoratore = indirizzoService.CodiceLavoratore;
            indirizzoDomain.IdTipoIndirizzo = indirizzoService.CodiceTipoIndirizzo;
            indirizzoDomain.IdEnteGestore = idEnteInserimento;
            indirizzoDomain.DataInserimento = DateTime.Now;
            indirizzoDomain.UtenteInserimento = utenteInserimento;
            indirizzoDomain.DataUltimaModifica = DateTime.Now;
            indirizzoDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.IndirizzoSet.AddObject(indirizzoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("IndirizzoManager.InserisciIndirizzo", "L'inserimento dell'indirizzo è fallito");
                }

                indirizzoService.Codice = indirizzoDomain.IdIndirizzo;
            }
        }

        public void AggiornaIndirizzo(IndirizzoService indirizzoService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                IndirizzoDomain indirizzoDomain;
                var query = from indirizzo in context.IndirizzoSet
                            where indirizzo.IdIndirizzo == indirizzoService.Codice.Value
                            select indirizzo;

                try
                {
                    indirizzoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new IndirizzoNonTrovatoException();
                }

                if (indirizzoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                indirizzoDomain.Cap = indirizzoService.Cap;
                indirizzoDomain.Civico = indirizzoService.Civico;
                indirizzoDomain.CodiceCatastaleComune = indirizzoService.CodiceComune;
                indirizzoDomain.Denominazione = indirizzoService.Denominazione;
                indirizzoDomain.IdLavoratore = indirizzoService.CodiceLavoratore;
                indirizzoDomain.IdTipoIndirizzo = indirizzoService.CodiceTipoIndirizzo;
                indirizzoDomain.DataUltimaModifica = DateTime.Now;
                indirizzoDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("IndirizzoManager.AggiornaIndirizzo", "L'aggiornamento dell'indirizzo è fallito");
                }
            }
        }

        public void CancellaIndirizzo(Int32 codice, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                IndirizzoDomain indirizzoDomain;
                var query = from indirizzo in context.IndirizzoSet
                            where indirizzo.IdIndirizzo == codice
                            select indirizzo;

                try
                {
                    indirizzoDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new IndirizzoNonTrovatoException();
                }

                if (indirizzoDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.IndirizzoSet.DeleteObject(indirizzoDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new LavoratoreServiceException("IndirizzoManager.CancellaIndirizzo", "La cancellazione dell'indirizzo è fallita");
                }
            }
        }
    }
}
