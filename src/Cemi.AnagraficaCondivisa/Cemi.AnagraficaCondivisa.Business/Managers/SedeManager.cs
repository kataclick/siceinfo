﻿using System;
using System.Linq;
using Cemi.AnagraficaCondivisa.Data;
using Cemi.AnagraficaCondivisa.Type.Exceptions;
using SedeDomain = Cemi.AnagraficaCondivisa.Type.Domain.Sede;
using SedeService = Cemi.AnagraficaCondivisa.Type.Entities.Sede;

namespace Cemi.AnagraficaCondivisa.Business.Managers
{
    public class SedeManager
    {
        public void InserisciSede(SedeService sedeService, Int32 idEnteInserimento, String utenteInserimento)
        {
            // Trasformazione della sede passata al servizio
            // nella sede di dominio
            SedeDomain sedeDomain = new SedeDomain();

            sedeDomain.Cap = sedeService.Cap;
            sedeDomain.Civico = sedeService.Civico;
            sedeDomain.CodiceCatastaleComune = sedeService.CodiceComune;
            sedeDomain.Denominazione = sedeService.Denominazione;
            sedeDomain.Email = sedeService.Email;
            sedeDomain.Fax = sedeService.Fax;
            sedeDomain.IdImpresa = sedeService.CodiceImpresa;
            sedeDomain.IdTipoSede = sedeService.CodiceTipoSede;
            sedeDomain.Pec = sedeService.Pec;
            sedeDomain.Presso = sedeService.Presso;
            sedeDomain.Telefono = sedeService.Telefono;
            sedeDomain.IdEnteGestore = idEnteInserimento;
            sedeDomain.DataInserimento = DateTime.Now;
            sedeDomain.UtenteInserimento = utenteInserimento;
            sedeDomain.DataUltimaModifica = DateTime.Now;
            sedeDomain.UtenteUltimaModifica = utenteInserimento;

            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                context.SedeSet.AddObject(sedeDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("SedeManager.InserisciSede", "L'inserimento della sede è fallito");
                }

                sedeService.Codice = sedeDomain.IdSede;
            }
        }

        public void AggiornaSede(SedeService sedeService, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                SedeDomain sedeDomain;
                var query = from sede in context.SedeSet
                            where sede.IdSede == sedeService.Codice.Value
                            select sede;

                try
                {
                    sedeDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new SedeNonTrovataException();
                }

                if (sedeDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                sedeDomain.Cap = sedeService.Cap;
                sedeDomain.Civico = sedeService.Civico;
                sedeDomain.CodiceCatastaleComune = sedeService.CodiceComune;
                sedeDomain.Denominazione = sedeService.Denominazione;
                sedeDomain.Email = sedeService.Email;
                sedeDomain.Fax = sedeService.Fax;
                sedeDomain.IdImpresa = sedeService.CodiceImpresa;
                sedeDomain.IdTipoSede = sedeService.CodiceTipoSede;
                sedeDomain.Pec = sedeService.Pec;
                sedeDomain.Presso = sedeService.Presso;
                sedeDomain.Telefono = sedeService.Telefono;
                sedeDomain.DataUltimaModifica = DateTime.Now;
                sedeDomain.UtenteUltimaModifica = utenteAggiornamento;

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("SedeManager.AggiornaSede", "L'aggiornamento della sede è fallita");
                }
            }
        }

        public void CancellaSede(Int32 codice, Int32 idEnteAggiornamento, String utenteAggiornamento)
        {
            using (AnagraficaCondivisaEntities context = new AnagraficaCondivisaEntities())
            {
                SedeDomain sedeDomain;
                var query = from sede in context.SedeSet
                            where sede.IdSede == codice
                            select sede;

                try
                {
                    sedeDomain = query.Single();
                }
                catch (Exception)
                {
                    throw new SedeNonTrovataException();
                }

                if (sedeDomain.IdEnteGestore != idEnteAggiornamento)
                {
                    throw new GestoreDifferenteException();
                }

                context.SedeSet.DeleteObject(sedeDomain);

                if (context.SaveChanges() != 1)
                {
                    throw new ImpresaServiceException("RiferimentoManager.CancellaRiferimento", "La cancellazione del riferimento è fallita");
                }
            }
        }
    }
}
