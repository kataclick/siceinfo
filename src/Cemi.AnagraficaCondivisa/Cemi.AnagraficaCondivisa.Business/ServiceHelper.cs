﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Cemi.AnagraficaCondivisa.Type.Exceptions;

namespace Cemi.AnagraficaCondivisa.Business
{
    public class ServiceHelper
    {
        public static String RecuperaIPChiamante()
        {
            String IP = null;

            OperationContext context = OperationContext.Current;
            if (context != null)
            {
                MessageProperties messageProperties = context.IncomingMessageProperties;
                if (messageProperties != null)
                {
                    RemoteEndpointMessageProperty endpointProperty =
                        messageProperties[RemoteEndpointMessageProperty.Name]
                        as RemoteEndpointMessageProperty;

                    IP = endpointProperty.Address;
                }
                else
                {
                    throw new ImpresaServiceException("ServiceHelper.RecuperaIPChiamante", "Impossibile recuperare l'indirizzo IP del chiamante");
                }
            }
            else
            {
                throw new ImpresaServiceException("ServiceHelper.RecuperaIPChiamante", "Impossibile recuperare l'indirizzo IP del chiamante");
            }

            return IP;
        }
    }
}
