﻿using System;
using Cemi.AnagraficaCondivisa.TestApp.CorsoService;

namespace Cemi.AnagraficaCondivisa.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //ImpresaServiceClient impresaClient = new ImpresaServiceClient();

            //Impresa impresa = new Impresa();
            //impresa.RagioneSociale = "INFINITY";
            //impresa.CodiceFiscale = "01767360991";
            //impresa.PartitaIva = "01767360991";
            //impresa.CodiceContratto = 1;

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO IMPRESA");
            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoIns = impresaClient.InserimentoImpresa(impresa, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoIns.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoIns.Codice));
            //if (risultatoIns.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoIns.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoIns.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoIns.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO IMPRESA");

            //impresa.Codice = 12;
            //impresa.RagioneSociale = String.Format("INFINITY {0}", DateTime.Now.ToString("yyyyMMddHHmm"));

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoAgg = impresaClient.AggiornamentoImpresa(impresa, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAgg.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAgg.Codice));
            //if (risultatoAgg.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAgg.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAgg.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAgg.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS CANCELLAZIONE IMPRESA");
            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoCanc = impresaClient.CancellazioneImpresa(12, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanc.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanc.Codice));
            //if (risultatoIns.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanc.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanc.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanc.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO RIFERIMENTO");

            //Riferimento riferimento = new Riferimento();

            //riferimento.Cognome = "mura";
            //riferimento.Nome = "alessio";
            //riferimento.Email = "alessio.mura@itsinfinity.com";
            //riferimento.Telefono = "564655";
            //riferimento.CodiceTipoRiferimento = 1;
            //riferimento.CodiceImpresa = 12;

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoInsRif = impresaClient.InserimentoRiferimento(riferimento, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsRif.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsRif.Codice));
            //if (risultatoInsRif.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsRif.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsRif.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsRif.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS CANCELLAZIONE RIFERIMENTO");

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoCanRif = impresaClient.CancellazioneRiferimento(risultatoInsRif.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanRif.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanRif.Codice));
            //if (risultatoCanRif.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanRif.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanRif.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanRif.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO RIFERIMENTO");

            //riferimento.Cognome = String.Format("mura {0}", DateTime.Now.ToString("yyyyMMddHHmm"));
            //riferimento.Codice = 7;

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoAggRif = impresaClient.AggiornamentoRiferimento(riferimento, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggRif.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggRif.Codice));
            //if (risultatoAggRif.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggRif.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggRif.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggRif.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO CONTATTO");

            //ContattoImpresa contatto = new ContattoImpresa();

            //contatto.Nota = "prova";
            //contatto.Telefono = "5555555";
            //contatto.CodiceImpresa = 12;

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoInsCon = impresaClient.InserimentoContatto(contatto, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsCon.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsCon.Codice));
            //if (risultatoInsCon.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsCon.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsCon.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsCon.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS CANCELLAZIONE CONTATTO");

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoCanCon = impresaClient.CancellazioneContatto(risultatoInsCon.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanCon.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanCon.Codice));
            //if (risultatoCanCon.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanCon.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanCon.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanCon.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO CONTATTO");

            //contatto.Codice = 1;
            //contatto.Nota = String.Format("prova {0}", DateTime.Now.ToString("yyyyMMddHHmm"));
            //contatto.Telefono = "5555555";
            //contatto.CodiceImpresa = 12;

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoAggCon = impresaClient.AggiornamentoContatto(contatto, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggCon.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggCon.Codice));
            //if (risultatoInsCon.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggCon.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggCon.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggCon.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO SEDE");

            //Sede sede = new Sede();
            //sede.CodiceImpresa = 12;
            //sede.CodiceTipoSede = 2;
            //sede.Denominazione = "PROVA";
            //sede.CodiceComune = "D969";
            //sede.Cap = "16157";

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoInsSed = impresaClient.InserimentoSede(sede, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsSed.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsSed.Codice));
            //if (risultatoInsSed.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsSed.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsSed.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsSed.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS CANCELLAZIONE SEDE");

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoCanSed = impresaClient.CancellazioneSede(risultatoInsSed.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanSed.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanSed.Codice));
            //if (risultatoCanSed.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanSed.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanSed.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanSed.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO SEDE");

            //sede.Codice = 3;
            //sede.Denominazione = String.Format("PROVA {0}", DateTime.Now.ToString("yyyyMMddHHmm"));

            //Cemi.AnagraficaCondivisa.TestApp.ImpresaService.RisultatoOperazione risultatoAggSed = impresaClient.AggiornamentoSede(sede, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggSed.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggSed.Codice));
            //if (risultatoAggSed.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggSed.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggSed.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggSed.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //LavoratoreServiceClient lavoratoreClient = new LavoratoreServiceClient();

            //Console.WriteLine("--------------- CHIAMATA WS CANCELLAZIONE LAVORATORE");

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoCanLav = lavoratoreClient.CancellazioneLavoratore(1, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanLav.Codice));
            //if (risultatoCanLav.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Lavoratore lavoratore = new Lavoratore();
            //lavoratore.Cognome = "mura";
            //lavoratore.Nome = "alessio";
            //lavoratore.CodiceFiscale = "mrulss81l25d969a";
            //lavoratore.Sesso = "m";
            //lavoratore.DataNascita = new DateTime(1981, 7, 25);
            //lavoratore.CodiceTipologia = 1;

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO LAVORATORE");
            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoLavIns = lavoratoreClient.InserimentoLavoratore(lavoratore, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoLavIns.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoLavIns.Codice));
            //if (risultatoLavIns.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoLavIns.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoLavIns.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoLavIns.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //lavoratore.Codice = 1;
            //lavoratore.Cognome = String.Format("mura {0}", DateTime.Now.ToShortTimeString());

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO LAVORATORE");
            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoLavAgg = lavoratoreClient.AggiornamentoLavoratore(lavoratore, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoLavAgg.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoLavAgg.Codice));
            //if (risultatoLavIns.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoLavAgg.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoLavAgg.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoLavAgg.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS LAV INSERIMENTO CONTATTO");

            //ContattoLavoratore contattoLav = new ContattoLavoratore();

            //contattoLav.Nota = "prova";
            //contattoLav.Telefono = "5555555";
            //contattoLav.CodiceLavoratore = 1;

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoInsConLav = lavoratoreClient.InserimentoContatto(contattoLav, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsConLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsConLav.Codice));
            //if (risultatoInsCon.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsConLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsConLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsConLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS LAV CANCELLAZIONE CONTATTO");

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoCanConLav = lavoratoreClient.CancellazioneContatto(risultatoInsConLav.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanConLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanConLav.Codice));
            //if (risultatoCanConLav.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanConLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanConLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanConLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS LAV AGGIORNAMENTO CONTATTO");

            //contattoLav.Codice = 1;
            //contattoLav.Nota = String.Format("prova {0}", DateTime.Now.ToShortTimeString());

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoAggConLav = lavoratoreClient.AggiornamentoContatto(contattoLav, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggConLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggConLav.Codice));
            //if (risultatoAggConLav.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggConLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggConLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggConLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO INDIRIZZO");

            //Indirizzo indirizzo = new Indirizzo();
            //indirizzo.CodiceLavoratore = 1;
            //indirizzo.CodiceTipoIndirizzo = 1;
            //indirizzo.Denominazione = "PROVA";
            //indirizzo.CodiceComune = "D969";
            //indirizzo.Cap = "16157";

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoInsInd = lavoratoreClient.InserimentoIndirizzo(indirizzo, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsInd.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsInd.Codice));
            //if (risultatoInsInd.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsInd.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsInd.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsInd.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS LAV CANCELLAZIONE INDIRIZZO");

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoCanInd = lavoratoreClient.CancellazioneIndirizzo(risultatoInsInd.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanInd.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanInd.Codice));
            //if (risultatoCanInd.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanInd.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanInd.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanInd.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO INDIRIZZO");

            //indirizzo.Codice = 1;
            //indirizzo.Denominazione = String.Format("PROVA {0}", DateTime.Now.ToShortTimeString());
            //indirizzo.CodiceComune = "D969";
            //indirizzo.Cap = "16157";

            //Cemi.AnagraficaCondivisa.TestApp.LavoratoreService.RisultatoOperazione risultatoAggInd = lavoratoreClient.AggiornamentoIndirizzo(indirizzo, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggInd.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggInd.Codice));
            //if (risultatoAggInd.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggInd.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggInd.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggInd.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            CorsoService.CorsoServiceClient corsoClient = new CorsoServiceClient();

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO CORSO LAVORATORE");

            //CorsoLavoratore corsoLav = new CorsoLavoratore();
            //corsoLav.CodiceCorso = "16ORE";
            //corsoLav.CodiceImpresa = 12;
            //corsoLav.CodiceLavoratore = 206889;
            //corsoLav.Cognome = "MURA";
            //corsoLav.DataInizioCorso = new DateTime(2014, 1, 1);
            //corsoLav.DenominazioneCorso = "16 ORE";
            //corsoLav.Durata = 5;
            //corsoLav.IscrizioneCongiunta = true;
            //corsoLav.NumeroAttestato = "2345/2014";
            //corsoLav.Sede = "VIA DINO COL 4N GENOVA";

            //Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoInsCorLav = corsoClient.InserimentoCorsoLavoratore(corsoLav, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsCorLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsCorLav.Codice));
            //if (risultatoInsCorLav.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsCorLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsCorLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsCorLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO CORSO LAVORATORE");

            //corsoLav.Codice = 1;
            //corsoLav.CodiceCorso = "16ORE AGG";

            //Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoAggCorLav = corsoClient.AggiornamentoCorsoLavoratore(corsoLav, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggCorLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggCorLav.Codice));
            //if (risultatoAggCorLav.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggCorLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggCorLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggCorLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS LAV CANCELLAZIONE CORSO LAVORATORE");

            //Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoCanCorLav = corsoClient.CancellazioneCorsoLavoratore(risultatoInsCorLav.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanCorLav.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanCorLav.Codice));
            //if (risultatoCanCorLav.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanCorLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanCorLav.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanCorLav.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO CORSO");

            //Corso corso = new Corso();
            //corso.CodiceCorso = "16ORETRIS";
            //corso.Denominazione = "16 ORE DESCRIZIONE";
            //corso.Durata = 5;

            //Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoInsCor = corsoClient.InserimentoCorso(corso, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsCor.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsCor.Codice));
            //if (risultatoInsCor.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoInsCor.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoInsCor.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoInsCor.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS AGGIORNAMENTO CORSO");

            //corso.Codice = risultatoInsCor.Codice;
            //corso.Denominazione = "16 ORE AGG";

            //Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoAggCor = corsoClient.AggiornamentoCorso(corso, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoAggCor.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoAggCor.Codice));
            //if (risultatoAggCor.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoAggCorLav.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoAggCor.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoAggCor.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            //Console.WriteLine("--------------- CHIAMATA WS LAV CANCELLAZIONE CORSO");

            //Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoCanCor = corsoClient.CancellazioneCorso(risultatoInsCor.Codice, "test");

            //Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanCor.Eseguita));
            //Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanCor.Codice));
            //if (risultatoCanCor.Errori.Length > 0)
            //{
            //    Console.WriteLine(" --- ERRORI ---");

            //    for (Int32 i = 0; i < risultatoCanCor.Errori.Length; i++)
            //    {
            //        Console.WriteLine(" Tipo    : {0}", risultatoCanCor.Errori[i].TipoErrore);
            //        Console.WriteLine(" Desc.   : {0}", risultatoCanCor.Errori[i].Descrizione);
            //        Console.WriteLine(" --------------");
            //    }
            //}
            //Console.WriteLine("--------------- FINE CHIAMATA WS");

            Console.WriteLine("--------------- CHIAMATA WS INSERIMENTO CORSO CALENDARIO");

            CorsoCalendario corsoCal = new CorsoCalendario();
            corsoCal.CodiceCorso = 0;
            corsoCal.Inizio = new DateTime(2014, 6, 1, 8, 0, 0);
            corsoCal.Progressivo = 5;
            corsoCal.Fine = new DateTime(2014, 6, 1, 17, 0, 0);
            corsoCal.Sede = "genova";

            Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoInsCorCal = corsoClient.InserimentoCorsoCalendario(corsoCal, "test");

            Console.WriteLine(String.Format(" - Stato   : {0}", risultatoInsCorCal.Eseguita));
            Console.WriteLine(String.Format(" - Cod     : {0}", risultatoInsCorCal.Codice));
            if (risultatoInsCorCal.Errori.Length > 0)
            {
                Console.WriteLine(" --- ERRORI ---");

                for (Int32 i = 0; i < risultatoInsCorCal.Errori.Length; i++)
                {
                    Console.WriteLine(" Tipo    : {0}", risultatoInsCorCal.Errori[i].TipoErrore);
                    Console.WriteLine(" Desc.   : {0}", risultatoInsCorCal.Errori[i].Descrizione);
                    Console.WriteLine(" --------------");
                }
            }
            Console.WriteLine("--------------- FINE CHIAMATA WS");

            Console.WriteLine("--------------- CHIAMATA WS CANCELLAZIONE CORSO CALENDARIO");

            Cemi.AnagraficaCondivisa.TestApp.CorsoService.RisultatoOperazione risultatoCanCorCal = corsoClient.CancellazioneCorsoCalendario(corsoCal.CodiceCorso, corsoCal.Inizio, corsoCal.Progressivo, "test");

            Console.WriteLine(String.Format(" - Stato   : {0}", risultatoCanCorCal.Eseguita));
            Console.WriteLine(String.Format(" - Cod     : {0}", risultatoCanCorCal.Codice));
            if (risultatoInsCorCal.Errori.Length > 0)
            {
                Console.WriteLine(" --- ERRORI ---");

                for (Int32 i = 0; i < risultatoCanCorCal.Errori.Length; i++)
                {
                    Console.WriteLine(" Tipo    : {0}", risultatoCanCorCal.Errori[i].TipoErrore);
                    Console.WriteLine(" Desc.   : {0}", risultatoCanCorCal.Errori[i].Descrizione);
                    Console.WriteLine(" --------------");
                }
            }
            Console.WriteLine("--------------- FINE CHIAMATA WS");

            Console.ReadLine();
        }
    }
}
