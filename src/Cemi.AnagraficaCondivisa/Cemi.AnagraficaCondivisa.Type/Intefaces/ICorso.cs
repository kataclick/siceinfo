﻿using System;
using System.ServiceModel;
using Cemi.AnagraficaCondivisa.Type.Entities;

namespace Cemi.AnagraficaCondivisa.Type.Intefaces
{
    [ServiceContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPTCorsi")]
    public interface ICorsoService
    {
        #region Metodi per l'inserimento
        [OperationContract]
        RisultatoOperazione InserimentoCorsoLavoratore(CorsoLavoratore corsoLavoratore, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoCorso(Corso corso, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoCorsoCalendario(CorsoCalendario corsoCalendario, String userName);
        #endregion

        #region Metodi per l'aggiornamento
        [OperationContract]
        RisultatoOperazione AggiornamentoCorsoLavoratore(CorsoLavoratore corsoLavoratore, String userName);

        [OperationContract]
        RisultatoOperazione AggiornamentoCorso(Corso corso, String userName);
        #endregion

        #region Metodi per la cancellazione
        [OperationContract]
        RisultatoOperazione CancellazioneCorsoLavoratore(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneCorso(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneCorsoCalendario(Int32 codiceCorso, DateTime inizio, Int32 progressivo, String userName);
        #endregion
    }
}
