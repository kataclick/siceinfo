﻿using System;
using System.ServiceModel;
using Cemi.AnagraficaCondivisa.Type.Entities;

namespace Cemi.AnagraficaCondivisa.Type.Intefaces
{
    [ServiceContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPTImprese")]
    public interface IImpresaService
    {
        #region Metodi per l'inserimento
        [OperationContract]
        RisultatoOperazione InserimentoImpresa(Impresa impresa, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoContatto(ContattoImpresa contatto, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoRiferimento(Riferimento riferimento, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoSede(Sede sede, String userName);
        #endregion

        #region Metodi per l'aggiornamento
        [OperationContract]
        RisultatoOperazione AggiornamentoImpresa(Impresa impresa, String userName);

        [OperationContract]
        RisultatoOperazione AggiornamentoContatto(ContattoImpresa contatto, String userName);

        [OperationContract]
        RisultatoOperazione AggiornamentoRiferimento(Riferimento riferimento, String userName);

        [OperationContract]
        RisultatoOperazione AggiornamentoSede(Sede sede, String userName);
        #endregion

        #region Metodi per la cancellazione
        [OperationContract]
        RisultatoOperazione CancellazioneImpresa(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneContatto(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneRiferimento(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneSede(Int32 codice, String userName);
        #endregion
    }
}
