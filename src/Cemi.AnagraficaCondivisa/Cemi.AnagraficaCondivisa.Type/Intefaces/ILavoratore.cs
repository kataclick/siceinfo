﻿using System;
using System.ServiceModel;
using Cemi.AnagraficaCondivisa.Type.Entities;

namespace Cemi.AnagraficaCondivisa.Type.Intefaces
{
    [ServiceContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPTLavoratori")]
    public interface ILavoratoreService
    {
        #region Metodi per l'inserimento
        [OperationContract]
        RisultatoOperazione InserimentoLavoratore(Lavoratore lavoratore, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoIndirizzo(Indirizzo indirizzo, String userName);

        [OperationContract]
        RisultatoOperazione InserimentoContatto(ContattoLavoratore contatto, String userName);
        #endregion

        #region Metodi per l'aggiornamento
        [OperationContract]
        RisultatoOperazione AggiornamentoLavoratore(Lavoratore lavoratore, String userName);

        [OperationContract]
        RisultatoOperazione AggiornamentoIndirizzo(Indirizzo indirizzo, String userName);

        [OperationContract]
        RisultatoOperazione AggiornamentoContatto(ContattoLavoratore contatto, String userName);
        #endregion

        #region Metodi per la cancellazione
        [OperationContract]
        RisultatoOperazione CancellazioneLavoratore(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneIndirizzo(Int32 codice, String userName);

        [OperationContract]
        RisultatoOperazione CancellazioneContatto(Int32 codice, String userName);
        #endregion
    }
}
