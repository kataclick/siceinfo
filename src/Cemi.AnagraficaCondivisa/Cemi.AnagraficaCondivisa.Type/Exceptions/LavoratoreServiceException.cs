﻿using System;

namespace Cemi.AnagraficaCondivisa.Type.Exceptions
{
    public class LavoratoreServiceException : Exception
    {
        private String metodo;

        public LavoratoreServiceException(String metodo)
            : base()
        {
            this.metodo = metodo;
        }

        public LavoratoreServiceException(String metodo, String message)
            : base(message)
        {
            this.metodo = metodo;
        }

        public LavoratoreServiceException(String metodo, String message, Exception innerException)
            : base(message, innerException)
        {
            this.metodo = metodo;
        }
    }
}
