﻿using System;

namespace Cemi.AnagraficaCondivisa.Type.Exceptions
{
    public class CorsoServiceException : Exception
    {
        private String metodo;

        public CorsoServiceException(String metodo)
            : base()
        {
            this.metodo = metodo;
        }

        public CorsoServiceException(String metodo, String message)
            : base(message)
        {
            this.metodo = metodo;
        }

        public CorsoServiceException(String metodo, String message, Exception innerException)
            : base(message, innerException)
        {
            this.metodo = metodo;
        }
    }
}
