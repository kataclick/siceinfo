﻿using System;

namespace Cemi.AnagraficaCondivisa.Type.Exceptions
{
    public class ImpresaServiceException : Exception
    {
        private String metodo;

        public ImpresaServiceException(String metodo)
            : base()
        {
            this.metodo = metodo;
        }

        public ImpresaServiceException(String metodo, String message)
            : base(message)
        {
            this.metodo = metodo;
        }

        public ImpresaServiceException(String metodo, String message, Exception innerException)
            : base(message, innerException)
        {
            this.metodo = metodo;
        }
    }
}
