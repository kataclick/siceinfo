﻿using System;

namespace Cemi.AnagraficaCondivisa.Type.Exceptions
{
    public class RiferimentoServiceException : Exception
    {
        private String metodo;

        public RiferimentoServiceException(String metodo)
            : base()
        {
            this.metodo = metodo;
        }

        public RiferimentoServiceException(String metodo, String message)
            : base(message)
        {
            this.metodo = metodo;
        }

        public RiferimentoServiceException(String metodo, String message, Exception innerException)
            : base(message, innerException)
        {
            this.metodo = metodo;
        }
    }
}
