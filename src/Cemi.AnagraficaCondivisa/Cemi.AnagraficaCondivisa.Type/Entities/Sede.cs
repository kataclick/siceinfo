﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class Sede
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public String CodiceToponimo { get; set; }

        [DataMember]
        public Int32 CodiceTipoSede { get; set; }

        [DataMember]
        public Int32 CodiceImpresa { get; set; }

        [DataMember]
        public String Denominazione { get; set; }

        [DataMember]
        public String Civico { get; set; }

        [DataMember]
        public String CodiceComune { get; set; }

        [DataMember]
        public String Cap { get; set; }

        [DataMember]
        public String Presso { get; set; }

        [DataMember]
        public String Telefono { get; set; }

        [DataMember]
        public String Fax { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public String Pec { get; set; }
    }
}
