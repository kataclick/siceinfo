﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class CorsoCalendario
    {
        [DataMember]
        public Int32 CodiceCorso { get; set; }

        [DataMember]
        public DateTime Inizio { get; set; }

        [DataMember]
        public Int32 Progressivo { get; set; }

        [DataMember]
        public DateTime Fine { get; set; }

        [DataMember]
        public String Sede { get; set; }
    }
}
