﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class Impresa
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public Int32? CodiceCassaEdile { get; set; }

        [DataMember]
        public String RagioneSociale { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String PartitaIva { get; set; }

        [DataMember]
        public String CodiceINPS { get; set; }

        [DataMember]
        public String CodiceINAIL { get; set; }

        [DataMember]
        public String IscrizioneCCIAA { get; set; }

        [DataMember]
        public Int32? CodiceContratto { get; set; }

        [DataMember]
        public String CodiceTipoImpresa { get; set; }

        [DataMember]
        public String CodiceAttivitaISTAT { get; set; }

        [DataMember]
        public Int32? CodiceNaturaGiuridica { get; set; }

        [DataMember]
        public String SitoWeb { get; set; }

        [DataMember]
        public Boolean LavoratoreAutonomo { get; set; }

        [DataMember]
        public Sede SedeLegale { get; set; }
    }
}
