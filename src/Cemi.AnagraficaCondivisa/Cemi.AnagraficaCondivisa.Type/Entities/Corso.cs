﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class Corso
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public String CodiceCorso { get; set; }

        [DataMember]
        public String Denominazione { get; set; }

        [DataMember]
        public Decimal Durata { get; set; }
    }
}
