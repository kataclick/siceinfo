﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class ContattoImpresa
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public Int32 CodiceImpresa { get; set; }

        [DataMember]
        public String Nota { get; set; }

        [DataMember]
        public String Telefono { get; set; }

        [DataMember]
        public String Fax { get; set; }

        [DataMember]
        public String Cellulare { get; set; }

        [DataMember]
        public String Email { get; set; }

        [DataMember]
        public String PEC { get; set; }
    }
}
