﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class Indirizzo
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public Int32 CodiceTipoIndirizzo { get; set; }

        [DataMember]
        public Int32 CodiceLavoratore { get; set; }

        [DataMember]
        public String Denominazione { get; set; }

        [DataMember]
        public String Civico { get; set; }

        [DataMember]
        public String CodiceComune { get; set; }

        [DataMember]
        public String Cap { get; set; }
    }
}
