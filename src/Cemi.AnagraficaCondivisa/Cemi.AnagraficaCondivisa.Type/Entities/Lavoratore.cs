﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class Lavoratore
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public Int32? CodiceCassaEdile { get; set; }

        [DataMember]
        public String Cognome { get; set; }

        [DataMember]
        public String Nome { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String Sesso { get; set; }

        [DataMember]
        public DateTime DataNascita { get; set; }

        [DataMember]
        public String LuogoNascita { get; set; }

        [DataMember]
        public String CodiceNazionalita { get; set; }

        [DataMember]
        public Int32 CodiceTipologia { get; set; }
    }
}
