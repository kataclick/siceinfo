﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class RisultatoOperazione
    {
        public RisultatoOperazione()
        {
            this.Errori = new List<RisultatoOperazioneErrore>();
        }

        [DataMember]
        public Boolean Eseguita { get; set; }

        [DataMember]
        public Int32 Codice { get; set; }

        [DataMember]
        public List<RisultatoOperazioneErrore> Errori { get; set; }
    }
}
