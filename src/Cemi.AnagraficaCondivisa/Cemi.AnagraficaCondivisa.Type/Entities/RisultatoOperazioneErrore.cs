﻿using System;
using System.Runtime.Serialization;
using Cemi.AnagraficaCondivisa.Type.Enums;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class RisultatoOperazioneErrore
    {
        [DataMember]
        public TipoErrore TipoErrore { get; set; }

        [DataMember]
        public String Descrizione { get; set; }
    }
}
