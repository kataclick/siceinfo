﻿using System;
using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Entities
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public class CorsoLavoratore
    {
        [DataMember]
        public Int32? Codice { get; set; }

        [DataMember]
        public Int32 CodiceLavoratore { get; set; }

        [DataMember]
        public String CodiceCorso { get; set; }

        [DataMember]
        public Int32? CodiceImpresa { get; set; }

        [DataMember]
        public String Cognome { get; set; }

        [DataMember]
        public String Nome { get; set; }

        [DataMember]
        public DateTime? DataNascita { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String DenominazioneCorso { get; set; }

        [DataMember]
        public DateTime DataInizioCorso { get; set; }

        [DataMember]
        public DateTime? DataFineCorso { get; set; }

        [DataMember]
        public Decimal? Durata { get; set; }

        [DataMember]
        public String Sede { get; set; }

        [DataMember]
        public String NumeroAttestato { get; set; }

        [DataMember]
        public Boolean IscrizioneCongiunta { get; set; }
    }
}
