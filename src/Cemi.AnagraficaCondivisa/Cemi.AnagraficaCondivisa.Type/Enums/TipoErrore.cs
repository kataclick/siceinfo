﻿using System.Runtime.Serialization;

namespace Cemi.AnagraficaCondivisa.Type.Enums
{
    [DataContract(Namespace = "www.cassaedilemilano.it/AnagraficaCondivisaCEMIESEMCPT")]
    public enum TipoErrore
    {
        [EnumMember]
        ParametroMancante = 0,
        [EnumMember]
        ParametroErrato,
        [EnumMember]
        ErroreAutorizzazione,
        [EnumMember]
        ErroreDatabase,
        [EnumMember]
        ErroreGenerico,
        [EnumMember]
        DatoGiaPresente
    }
}
