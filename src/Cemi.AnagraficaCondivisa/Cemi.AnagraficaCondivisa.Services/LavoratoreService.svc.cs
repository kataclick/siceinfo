﻿using System;
using System.Collections.Generic;
using Cemi.AnagraficaCondivisa.Business;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Intefaces;

namespace Cemi.AnagraficaCondivisa.Services
{
    public class LavoratoreService : ILavoratoreService
    {
        LavoratoreServiceManager lavoratoreManager = new LavoratoreServiceManager();
        ContattoLavoratoreServiceManager contattoManager = new ContattoLavoratoreServiceManager();
        IndirizzoServiceManager indirizzoManager = new IndirizzoServiceManager();

        #region Metodi per l'inserimento
        public RisultatoOperazione InserimentoLavoratore(Lavoratore lavoratore, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = lavoratoreManager.InserimentoLavoratore(lavoratore, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoContatto(ContattoLavoratore contatto, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = contattoManager.InserimentoContatto(contatto, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoIndirizzo(Indirizzo indirizzo, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = indirizzoManager.InserimentoIndirizzo(indirizzo, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion

        #region Metodi per l'aggiornamento
        public RisultatoOperazione AggiornamentoLavoratore(Lavoratore lavoratore, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = lavoratoreManager.AggiornamentoLavoratore(lavoratore, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoContatto(ContattoLavoratore contatto, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = contattoManager.AggiornamentoContatto(contatto, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoIndirizzo(Indirizzo indirizzo, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = indirizzoManager.AggiornamentoIndirizzo(indirizzo, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion

        #region Metodi per la cancellazione
        public RisultatoOperazione CancellazioneLavoratore(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = lavoratoreManager.CancellaLavoratore(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneContatto(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = contattoManager.CancellaContatto(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneIndirizzo(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = indirizzoManager.CancellaIndirizzo(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion
    }
}
