﻿
using System;
using System.Collections.Generic;
using Cemi.AnagraficaCondivisa.Business;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Intefaces;
namespace Cemi.AnagraficaCondivisa.Services
{
    public class ImpresaService : IImpresaService
    {
        ImpresaServiceManager impresaManager = new ImpresaServiceManager();
        RiferimentoServiceManager riferimentoManager = new RiferimentoServiceManager();
        ContattoImpresaServiceManager contattoManager = new ContattoImpresaServiceManager();
        SedeServiceManager sedeManager = new SedeServiceManager();

        #region Metodi per l'inserimento
        public RisultatoOperazione InserimentoImpresa(Impresa impresa, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = impresaManager.InserimentoImpresa(impresa, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoContatto(ContattoImpresa contatto, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = contattoManager.InserimentoContatto(contatto, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoRiferimento(Riferimento riferimento, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = riferimentoManager.InserimentoRiferimento(riferimento, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoSede(Sede sede, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = sedeManager.InserimentoSede(sede, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion

        #region Metodi per l'aggiornamento
        public RisultatoOperazione AggiornamentoImpresa(Impresa impresa, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = impresaManager.AggiornamentoImpresa(impresa, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoContatto(ContattoImpresa contatto, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = contattoManager.AggiornamentoContatto(contatto, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoRiferimento(Riferimento riferimento, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = riferimentoManager.AggiornamentoRiferimento(riferimento, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoSede(Sede sede, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = sedeManager.AggiornamentoSede(sede, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion

        #region Metodi per la cancellazione
        public RisultatoOperazione CancellazioneImpresa(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = impresaManager.CancellaImpresa(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneContatto(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = contattoManager.CancellaContatto(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneRiferimento(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = riferimentoManager.CancellaRiferimento(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneSede(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = sedeManager.CancellaSede(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion
    }
}
