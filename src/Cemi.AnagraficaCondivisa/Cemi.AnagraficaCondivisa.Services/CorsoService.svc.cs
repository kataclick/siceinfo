﻿using System;
using System.Collections.Generic;
using Cemi.AnagraficaCondivisa.Business;
using Cemi.AnagraficaCondivisa.Type.Entities;
using Cemi.AnagraficaCondivisa.Type.Enums;
using Cemi.AnagraficaCondivisa.Type.Intefaces;

namespace Cemi.AnagraficaCondivisa.Services
{
    public class CorsoService : ICorsoService
    {
        CorsoLavoratoreServiceManager corsoLavoratoreManager = new CorsoLavoratoreServiceManager();
        CorsoServiceManager corsoManager = new CorsoServiceManager();
        CorsoCalendarioServiceManager corsoCalendarioManager = new CorsoCalendarioServiceManager();

        #region Metodi per l'inserimento
        public RisultatoOperazione InserimentoCorsoLavoratore(CorsoLavoratore corsoLavoratore, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoLavoratoreManager.InserimentoCorsoLavoratore(corsoLavoratore, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoCorso(Corso corso, string userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoManager.InserimentoCorso(corso, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione InserimentoCorsoCalendario(CorsoCalendario corsoCalendario, string userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoCalendarioManager.InserimentoCorsoCalendario(corsoCalendario, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion

        #region Metodi per l'aggiornamento
        public RisultatoOperazione AggiornamentoCorsoLavoratore(CorsoLavoratore corsoLavoratore, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoLavoratoreManager.AggiornamentoCorsoLavoratore(corsoLavoratore, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione AggiornamentoCorso(Corso corso, string userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoManager.AggiornamentoCorso(corso, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion

        #region Metodi per la cancellazione
        public RisultatoOperazione CancellazioneCorsoLavoratore(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoLavoratoreManager.CancellaCorsoLavoratore(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneCorso(Int32 codice, String userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoManager.CancellaCorso(codice, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }

        public RisultatoOperazione CancellazioneCorsoCalendario(Int32 codiceCorso, DateTime inizio, Int32 progressivo, string userName)
        {
            RisultatoOperazione risultato = null;

            try
            {
                // Recupero l'indirizzo IP del chiamante (serve per individuare l'ente che sta effettuando la richiesta)
                String IP = ServiceHelper.RecuperaIPChiamante();

                // Effettuo l'operazione, tutti i controlli e autorizzazioni sono effettuati nel business
                risultato = corsoCalendarioManager.CancellaCorsoCalendario(codiceCorso, inizio, progressivo, userName, IP);
            }
            catch (Exception ecc)
            {
                risultato = new RisultatoOperazione()
                {
                    Codice = -1,
                    Eseguita = false,
                    Errori = new List<RisultatoOperazioneErrore>()
                };

                RisultatoOperazioneErrore errore = new RisultatoOperazioneErrore()
                {
                    TipoErrore = TipoErrore.ErroreGenerico,
                    Descrizione = ecc.Message
                };

                risultato.Errori.Add(errore);
            }

            return risultato;
        }
        #endregion
    }
}
