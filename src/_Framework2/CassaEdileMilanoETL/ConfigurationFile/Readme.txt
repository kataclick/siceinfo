questo file serve per illustrare il funzionamento dei file di configurazione (*.dtsconfig)

Tutti i packages possiedono delle variabili che possono essere modificate con questi file di configurazione. 
Possono avere una certa rilevanza nel caso in cui bisogna modificare la propriet� connectionstring di una connessione. 
E' sufficiente modificare il valore della propriet� corrispondente e il package cambia stringa di connessione.

I file .dtsconfig presenti sono quelli utilizzati dai vari processi. Per i processi di scrittura (da SICE verso AS400) � stato 
creato un unico file di configurazione comune a tutti. Idem per i processi "manuali" SICENEW (da AS400 verso ambiente di appoggio SQL) e 
SICEINFO (da ambiente di appoggio verso SICE)