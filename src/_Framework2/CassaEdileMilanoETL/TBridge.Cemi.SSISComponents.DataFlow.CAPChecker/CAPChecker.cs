﻿//
// By James Howey
// Copyright (c) Microsoft Corporation.  All rights reserved.
 using System;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using Microsoft.SqlServer.Dts;
using System.Globalization;
using Microsoft.SqlServer.Dts.ManagedMsg;
using System.Diagnostics;
using System.Text.RegularExpressions;



namespace TBridge.Cemi.SSISComponents.DataFlow
{
    [DtsPipelineComponent(DisplayName = "CAPChecker", ComponentType = ComponentType.Transform, 
        Description = "Controlla che i campi di tipo DT_I4, DT_STR, DT_WSTR siano dei CAP corretti.")]
	public class CAPChecker : PipelineComponent
    {
        private string dateExpression;
        private string CAPRegularExpression = "^\\d{1,5}$";

        #region helper methods and objects

        private void PostError(string message)
        {
            bool cancel = false;
            this.ComponentMetaData.FireError(0, this.ComponentMetaData.Name, message, "", 0, out cancel);
        }

        private DTSValidationStatus promoteStatus(ref DTSValidationStatus currentStatus, DTSValidationStatus newStatus)
        {
            // statuses are ranked in order of increasing severity, from
            // valid to broken to needsnewmetadata to corrupt.
            // bad status, if any, is result of programming error
            switch (currentStatus)
            {
                case DTSValidationStatus.VS_ISVALID:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                case DTSValidationStatus.VS_ISBROKEN:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                case DTSValidationStatus.VS_ISCORRUPT:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                default:
                    throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
            }
            return currentStatus;
        } 

        #endregion

        #region design time functionality
        
        public override void ProvideComponentProperties()
        {
            base.ProvideComponentProperties();
            IDTSCustomPropertyCollection90 customProperties = this.ComponentMetaData.CustomPropertyCollection;
        }

        public override DTSValidationStatus Validate()
        {
            // if scale doesn't match output column, or extra output columns, or insufficient
            // output columns, this component is corrupt. We can say this because in our design
            // time methods, we have compelled the output columns to remain in lockstep with 
            // input column selections

            //  if component is corrupt, we are permitted to return without further checks
            DTSValidationStatus status = base.Validate();
            if (status == DTSValidationStatus.VS_ISCORRUPT)
            {
                return status;
            }

            IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            IDTSCustomProperty90 customProperty;
            int lineageID;

            
            IDTSInputCollection90 inputCollection = metadata.InputCollection;
            IDTSInput90 input = inputCollection[0];
            IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            IDTSInputColumn90 inputColumn;

            IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            IDTSOutput90 output = outputCollection[0];
            IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            IDTSOutputColumn90 outputColumn;

            if (inputColumnCollection.Count != outputColumnCollection.Count)
            {
                PostError("Input and output columns don't match up.");
                return DTSValidationStatus.VS_ISCORRUPT;
            }

            for (int j = 0; j < outputColumnCollection.Count; j++)
            {
                outputColumn = outputColumnCollection[j];

                //TODO controllo sull'outputcolumn
                //if (!isUnicode)
                //{
                //    if (outputColumn.CodePage != codePage)
                //    {
                //        PostError("Output column code page doesn't match component custome property CodePage.");
                //        return DTSValidationStatus.VS_ISCORRUPT;
                //    }

                //    if (outputColumn.DataType != DataType.DT_STR)
                //    {
                //        PostError("If unicode not selected, output column data type must be DT_STR");
                //        return DTSValidationStatus.VS_ISCORRUPT;
                //    }
                //}
                //else
                //{
                //    if (outputColumn.DataType != DataType.DT_WSTR)
                //    {
                //        PostError("If unicode selected, output column data type must be DT_WSTR");
                //        return DTSValidationStatus.VS_ISCORRUPT;
                //    }
                //}

                IDTSCustomPropertyCollection90 customPropertyCollection;
                lineageID = outputColumn.LineageID;
                customPropertyCollection = outputColumn.CustomPropertyCollection;
                try
                {
                    customProperty = customPropertyCollection["InputColumnID"];
                }
                catch (Exception)
                {
                    PostError("Output column [" + outputColumn.Name + "] has no InputColumnID custom property.");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }

                int inputColumnID = (int)customProperty.Value;
                try
                {
                    inputColumn = inputColumnCollection.FindObjectByID(inputColumnID);
                }
                catch (Exception)
                {
                    PostError("InputColumnID [" + inputColumnID + "] not found in selected input columns.");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }
                // if input column is orphaned, we already have reinit status, and cleanup will 
                // eliminate any following errors. 
                if (inputColumn.IsValid)
                {
                    if (inputColumn.DataType != DataType.DT_STR && inputColumn.DataType != DataType.DT_WSTR 
                        && inputColumn.DataType != DataType.DT_I4)
                    {
                        PostError("La colonna [" + inputColumn.Name + "] non ha il tipo di dato corretto. Il tipo deve essere: DT_STR, DT_STR o DT_I4.");
                        //todo togliere commento
                        promoteStatus(ref status, DTSValidationStatus.VS_ISBROKEN);
                    }
                }
            }
            return status;
        }

        #region controlli per aggiunta di input e output
        public override void ReinitializeMetaData()
        {
            // This should delete orphaned columns for us, calling OnDeletingInputColumn
            // If data type changes to non-string, component is broken and user must fix manually
            base.ReinitializeMetaData();
        }

        public override IDTSInput90 InsertInput(DTSInsertPlacement insertPlacement, int inputID)
        {
            PostError("Component requires exactly one input. New input is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDINPUT);
        }

        public override void DeleteInput(int inputID)
        {
            PostError("Component requires exactly one input. Deleted input is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTDELETEINPUT);
        }

        public override IDTSOutput90 InsertOutput(DTSInsertPlacement insertPlacement, int outputID)
        {
            PostError("Component requires exactly one output. New output is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDOUTPUT);
        }

        public override void DeleteOutput(int outputID)
        {
            PostError("Component requires exactly one output. Deleted output is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTDELETEOUTPUT);
        }

        public override IDTSOutputColumn90 InsertOutputColumnAt(int outputID, int outputColumnIndex, string name, string description)
        {
            PostError("Component forbids adding output columns. Check input columnn to configure.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDCOLUMN);
        }

        public override void DeleteOutputColumn(int outputID, int outputColumnID)
        {
            PostError("Component forbids deleting output columns. Uncheck input columnn to configure.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDCOLUMN);
        }
        
        public override void  OnDeletingInputColumn(int inputID, int inputColumnID)
        {
			IDTSComponentMetaData90 metadata = this.ComponentMetaData;

			// An input column is being deleted. This may be because it no longer is 
			// present upstream, or because the user has unchecked it.
			// We have to retrieve the column, then remove any 
			// reference to it in the output column collection
			IDTSInputCollection90 inputCollection = metadata.InputCollection;
    		IDTSInput90 input = inputCollection[0];
			// Get input column collection and retrieve interesting column
			IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            IDTSInputColumn90 inputColumn = inputColumnCollection.FindObjectByID(inputColumnID);

			// Iterate over output columns looking for matching id in custom property.
			IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
			IDTSOutput90 output = outputCollection[0];
			IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
			for (int j = 0; j < outputColumnCollection.Count; j++)
			{
				IDTSOutputColumn90 outputColumn = outputColumnCollection[j];
				IDTSCustomPropertyCollection90 customPropertyCollection = outputColumn.CustomPropertyCollection;
				IDTSCustomProperty90 customProperty = customPropertyCollection["InputColumnID"];
				int columnID = (int)customProperty.Value;
				if (columnID == inputColumnID)
				{
                    // we just delete the output column
                    base.DeleteOutputColumn(output.ID, outputColumn.ID);
				}
			}
        }
        #endregion

        public override IDTSCustomProperty90 SetComponentProperty(string propertyName, object propertyValue)
        {
            IDTSCustomProperty90 ret = base.SetComponentProperty(propertyName, propertyValue);

            IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            IDTSCustomProperty90 customProperty;
            IDTSCustomPropertyCollection90 customPropertyCollection = metadata.CustomPropertyCollection;
            
            // Iterate over output columns setting code pages.
            IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            IDTSOutput90 output = outputCollection[0];
            IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            IDTSInputCollection90 inputCollection = metadata.InputCollection;
            IDTSInput90 input = inputCollection[0];
            IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            for (int j = 0; j < outputColumnCollection.Count; j++)
            {
                IDTSOutputColumn90 outputColumn = outputColumnCollection[j];
                customPropertyCollection = outputColumn.CustomPropertyCollection;
                customProperty = customPropertyCollection["InputColumnID"];
                int columnID = (int)customProperty.Value;
                IDTSInputColumn90 inputColumn = inputColumnCollection.FindObjectByID(columnID);
                //if (convertToUnicode)
                {
                    outputColumn.SetDataTypeProperties(DataType.DT_WSTR, 5, 0, 0, 0);
                }
                //else
                //{
                //    outputColumn.SetDataTypeProperties(DataType.DT_STR, inputColumn.Length * 2, 0, 0, codePage);
                //}
            }
            return ret;
        }

        public override void SetOutputColumnDataTypeProperties(int iOutputID, int iOutputColumnID, DataType eDataType, int iLength, int iPrecision, int iScale, int iCodePage)
        {
            PostError("ToDateConvert does not permit changes to individual output column data types.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETOUTPUTCOLUMNDATATYPEPROPERTIES);
            
            //commento perchè non permettiamo 
            
            //// We allow users to change length of string
            //IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            //IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            //IDTSOutput90 output = outputCollection[0];
            //IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            //IDTSOutputColumn90 outputColumn = outputColumnCollection.FindObjectByID(iOutputColumnID);

            //if (eDataType != outputColumn.DataType)
            //{
            //    PostError("ToDateConvert does not permit changes to individual output column data types.");
            //    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETOUTPUTCOLUMNDATATYPEPROPERTIES);
            //}
            //else
            //{
            //    // implicit && eDataType == DataType.DT_STR here
            //    if (outputColumn.CodePage != iCodePage)
            //    {
            //        PostError("ConvertCodePage does not permit changes to individual output column code pages.");
            //        throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETOUTPUTCOLUMNDATATYPEPROPERTIES);
            //    }
            //    else
            //    {
            //        outputColumn.SetDataTypeProperties(outputColumn.DataType, iLength, 0, 0, outputColumn.CodePage);
            //    }
            //}
        }

        public override IDTSInputColumn90 SetUsageType(int inputID, IDTSVirtualInput90 virtualInput, int lineageID, DTSUsageType usageType)
        {
            IDTSInputColumn90 inputColumn;
            IDTSComponentMetaData90 metadata = this.ComponentMetaData;

            switch (usageType)
            {
                case DTSUsageType.UT_READONLY:
			        IDTSVirtualInputColumn90 column = virtualInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(lineageID);
                    if (column.DataType != DataType.DT_WSTR && column.DataType != DataType.DT_STR 
                        && column.DataType != DataType.DT_I4)
                    {
                        PostError("Component operates only on string or int input. Other types are forbidden.");
                        throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
                    }
                    else
                    {
                        inputColumn = base.SetUsageType(inputID, virtualInput, lineageID, usageType);
                        IDTSCustomProperty90 customProperty;
                        IDTSCustomPropertyCollection90 customPropertyCollection = metadata.CustomPropertyCollection;
                        
                        // validate will catch non-existent custom property
                        //customProperty = customPropertyCollection["DateExpression"];
                        //string dateExpression = (string)customProperty.Value;
                        
                        //customProperty = customPropertyCollection["ConvertToUnicode"];
                        //bool convertToUnicode = (bool)customProperty.Value;

                        IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
                        IDTSOutput90 output = outputCollection[0];
                        IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
                        IDTSOutputColumn90 newColumn;
                        newColumn = base.InsertOutputColumnAt(output.ID, outputColumnCollection.Count,
                            /*inputColumn.UpstreamComponentName + "." + */inputColumn.Name + ".CAPChecked", "");
                       //if (!convertToUnicode)
                       // {
                       //     // multiply by 2 to account for dbcs. If using utf-8 or gb18030, etc. this may not be enough
                       //     newColumn.SetDataTypeProperties(DataType.DT_STR, inputColumn.Length * 2, 0, 0, codePage);
                       // }
                        //else
                        {
                            newColumn.SetDataTypeProperties(DataType.DT_WSTR, 5, 0, 0, 0);
                        }
                        customPropertyCollection = newColumn.CustomPropertyCollection;
                        customProperty = customPropertyCollection.New();
                        customProperty.Name = "InputColumnID"; // do not localize
                        // support cut and paste
                        customProperty.ContainsID = true;
                        customProperty.Value = inputColumn.ID;

                        return inputColumn;
                    }
                case DTSUsageType.UT_READWRITE:
                    PostError("Component requires that input columns be marked read only.");
                    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
                case DTSUsageType.UT_IGNORED:
                    IDTSInputCollection90 inputCollection = metadata.InputCollection;
                    IDTSInput90 input = inputCollection[0];
                    IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
                    inputColumn = inputColumnCollection.GetInputColumnByLineageID(lineageID);
					this.OnDeletingInputColumn(inputID, inputColumn.ID);
                    inputColumn = base.SetUsageType(inputID, virtualInput, lineageID, usageType);
                    return inputColumn;
                default:
                    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
            }
        }


        
#endregion

        #region private member variables

        private int[] outColumnWriteIDs;
        private int[] outColumnSourceIDs;
        private int numOutColumnWrites = 0;

        #endregion

        #region runtime functionality
        public override void PreExecute()
        {
            IDTSCustomProperty90 customProperty;
            IDTSCustomPropertyCollection90 customPropertyCollection;

            int lineageID;

            IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            
            IDTSInput90 input = this.ComponentMetaData.InputCollection[0];
            int inputBufferID = input.Buffer;
            IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            IDTSInputColumn90 inputColumn;
            IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            IDTSOutput90 output = outputCollection[0];
            IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            IDTSOutputColumn90 outputColumn;

            // get output columns to write
            this.outColumnWriteIDs = new int[outputColumnCollection.Count];
            this.outColumnSourceIDs = new int[outputColumnCollection.Count];
            this.numOutColumnWrites = 0;
            for (int j = 0; j < outputColumnCollection.Count; j++)
            {
                outputColumn = outputColumnCollection[j];
                lineageID = outputColumn.LineageID;
                // this.numOutColumnWrites index is incremented farther below.
                this.outColumnWriteIDs[this.numOutColumnWrites] = this.BufferManager.FindColumnByLineageID(inputBufferID, lineageID);
                customPropertyCollection = outputColumn.CustomPropertyCollection;
                customProperty = customPropertyCollection["InputColumnID"];
                
                int inputID = (int)customProperty.Value;
                inputColumn = inputColumnCollection.FindObjectByID(inputID);
                this.outColumnSourceIDs[this.numOutColumnWrites] = this.BufferManager.FindColumnByLineageID(inputBufferID, inputColumn.LineageID);
                this.numOutColumnWrites++;
            }
        }

        public override void ProcessInput(int inputID, PipelineBuffer buffer)
        {
            String source;
            if (!buffer.EndOfRowset)
            {
                while (buffer.NextRow())
                {
                    for (int j = 0; j < this.numOutColumnWrites; j++)
                    {
                        Object o = buffer[this.outColumnSourceIDs[j]];

                        Type t = o.GetType();
                        if(t == typeof(Int32))
                            source = (buffer.GetInt32(this.outColumnSourceIDs[j])).ToString();
                        else
                            source = buffer.GetString(this.outColumnSourceIDs[j]);

                        //togliamo gli spazi
                        source = source.Trim();

                        try
                        {
                            if(this.ValidateCAPValue(ref source))
                            {
                                //controlliamo che non sia un CAP finto; la validazione fatta prima permette ci catturare 
                                //i cAP sbagliati scritti come: 0 , 00 , 000 etc. o stringa vuota
                                if (source != "00000")
                                    buffer[this.outColumnWriteIDs[j]] = source;
                                else buffer.SetNull(this.outColumnWriteIDs[j]);
                            }
                            else buffer.SetNull(this.outColumnWriteIDs[j]);
                        }
                        catch (Exception e)
                        {
                            string g = e.Message;
                            PostError("Errore nella correzione del cap; colonna " + this.outColumnSourceIDs[j] + ".");
                            throw new PipelineComponentHResultException(HResults.DTS_E_INDUCEDTRANSFORMFAILUREONTRUNCATION);
                        }
                    }
                }
            }
        }

  
        #endregion

        /// <summary>
        /// Valida che il cap sia composto da numeri, se mancano degli 0 li corregge automaticamente
        /// </summary>
        /// <param name="CAP">passato per riferimento</param>
        /// <returns></returns>
        private bool ValidateCAPValue(ref string CAP)
        {
            Match match;
            Regex regex;

            regex = new Regex(this.CAPRegularExpression);
            match = regex.Match(CAP);
            if (match.Success)
            {
                if (CAP.Length < 5)
                    for (int i = CAP.Length; i<5; i++)
                        CAP = "0" + CAP;
                
                return true;
            }
            else
                return false;           
        }
    }
}