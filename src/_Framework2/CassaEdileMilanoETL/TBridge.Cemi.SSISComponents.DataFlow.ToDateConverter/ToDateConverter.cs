﻿//
// By James Howey
// Copyright (c) Microsoft Corporation.  All rights reserved.
 using System;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using Microsoft.SqlServer.Dts;
using System.Globalization;
using Microsoft.SqlServer.Dts.ManagedMsg;
using System.Diagnostics;
using System.Text.RegularExpressions;



namespace TBridge.Cemi.SSISComponents.DataFlow
{
    [DtsPipelineComponent(DisplayName = "ToDateConverter", ComponentType = ComponentType.Transform, 
        Description = "Converte DT_I4, DT_STR, DT_WSTR in date in base al formato dato dalla data definito lato utente.")]
	public class ToDateConverter : PipelineComponent
    {
        private string dateExpression;
        private DateTime? dateDefaultValue;
        private DateTime? dateMinValue;
        private DateTime? dateMaxValue;
        private string dateRegularExpression = "^\\d{4}\\d{2}\\d{2}$";
        private string dateRegularFormat = "yyyyMMdd";

        #region helper methods and objects

        private void PostError(string message)
        {
            bool cancel = false;
            this.ComponentMetaData.FireError(0, this.ComponentMetaData.Name, message, "", 0, out cancel);
        }

        private DTSValidationStatus promoteStatus(ref DTSValidationStatus currentStatus, DTSValidationStatus newStatus)
        {
            // statuses are ranked in order of increasing severity, from
            // valid to broken to needsnewmetadata to corrupt.
            // bad status, if any, is result of programming error
            switch (currentStatus)
            {
                case DTSValidationStatus.VS_ISVALID:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                case DTSValidationStatus.VS_ISBROKEN:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                case DTSValidationStatus.VS_ISCORRUPT:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            break;
                        default:
                            throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
                    }
                    break;
                default:
                    throw new System.ApplicationException("Internal Error: A value outside the scope of the status enumeration was found.");
            }
            return currentStatus;
        } 

        #endregion

        #region design time functionality
        
        public override void ProvideComponentProperties()
        {
            base.ProvideComponentProperties();
            IDTSCustomPropertyCollection90 customProperties = this.ComponentMetaData.CustomPropertyCollection;
            
            IDTSCustomProperty90 customProperty = customProperties.New();
            customProperty.Name = "DateExpression"; // do not localize
            customProperty.Value = "yyyyMMdd"; // default is us english 
            
            customProperty = customProperties.New();
            customProperty.Name = "MinValue";
            customProperty.Value = "NULL";
            
            customProperty = customProperties.New();
            customProperty.Name = "MaxValue";
            customProperty.Value = "NULL"; 
            
            customProperty = customProperties.New();
            customProperty.Name = "DefaultValue"; 
            customProperty.Value = "NULL"; // I valori possono essere: NULL, MINDATE, MAXDATE oppure una data in formato universale (yyyy-MM-dd)

        }

        public override DTSValidationStatus Validate()
        {
            // if scale doesn't match output column, or extra output columns, or insufficient
            // output columns, this component is corrupt. We can say this because in our design
            // time methods, we have compelled the output columns to remain in lockstep with 
            // input column selections

            //  if component is corrupt, we are permitted to return without further checks
            DTSValidationStatus status = base.Validate();
            if (status == DTSValidationStatus.VS_ISCORRUPT)
            {
                return status;
            }

            IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            IDTSCustomProperty90 customProperty;
            int lineageID;

            IDTSCustomPropertyCollection90 customPropertyCollection = metadata.CustomPropertyCollection;
            try
            {
                customProperty = customPropertyCollection["DateExpression"];
            }
            catch (Exception)
            {
                PostError("Il compomente non ha definita la proprietà custom DateExpression.");
                return DTSValidationStatus.VS_ISCORRUPT;
            }
            string dateExpression = (string)customProperty.Value;
            // TODO controllo sulla dateExpression: come farlo???

            //controlliamo la proprietà default value
            try
            {
                //validiamo il default value
                customProperty = customPropertyCollection["DefaultValue"];
                string defaultValue = (string)customProperty.Value;

                //imposta il valore della data
                if (!ValidateDateDefaultValue(out this.dateDefaultValue, defaultValue))
                {
                    PostError("Il componente non ha definita correttamente la proprietà custom DefaultValue. Il formato corretto è yyyyMMdd oppure NULL");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }
            }
            catch (Exception)
            {
                PostError("Il compomente non ha definita la proprietà custom DefaultValue.");
                return DTSValidationStatus.VS_ISCORRUPT;
            }

            //controlliamo la proprietà MIN value
            try
            {
                //validiamo il default value
                customProperty = customPropertyCollection["MinValue"];
                string minValue = (string)customProperty.Value;

                //imposta il valore della data
                if (!ValidateDateDefaultValue(out this.dateDefaultValue, minValue))
                {
                    PostError("Il componente non ha definita correttamente la proprietà custom MinValue. Il formato corretto è yyyyMMdd oppure NULL");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }
            }
            catch (Exception)
            {
                PostError("Il compomente non ha definita la proprietà custom MinValue.");
                return DTSValidationStatus.VS_ISCORRUPT;
            }

            //controlliamo la proprietà MAX value
            try
            {
                //validiamo il default value
                customProperty = customPropertyCollection["MaxValue"];
                string maxValue = (string)customProperty.Value;

                //imposta il valore della data
                if (!ValidateDateDefaultValue(out this.dateDefaultValue, maxValue))
                {
                    PostError("Il componente non ha definita correttamente la proprietà custom MaxnValue. Il formato corretto è yyyyMMdd oppure NULL");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }
            }
            catch (Exception)
            {
                PostError("Il compomente non ha definita la proprietà custom MaxValue.");
                return DTSValidationStatus.VS_ISCORRUPT;
            }

            IDTSInputCollection90 inputCollection = metadata.InputCollection;
            IDTSInput90 input = inputCollection[0];
            IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            IDTSInputColumn90 inputColumn;

            IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            IDTSOutput90 output = outputCollection[0];
            IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            IDTSOutputColumn90 outputColumn;

            if (inputColumnCollection.Count != outputColumnCollection.Count)
            {
                PostError("Input and output columns don't match up.");
                return DTSValidationStatus.VS_ISCORRUPT;
            }

            for (int j = 0; j < outputColumnCollection.Count; j++)
            {
                outputColumn = outputColumnCollection[j];

                //TODO controllo sull'outputcolumn
                //if (!isUnicode)
                //{
                //    if (outputColumn.CodePage != codePage)
                //    {
                //        PostError("Output column code page doesn't match component custome property CodePage.");
                //        return DTSValidationStatus.VS_ISCORRUPT;
                //    }

                //    if (outputColumn.DataType != DataType.DT_STR)
                //    {
                //        PostError("If unicode not selected, output column data type must be DT_STR");
                //        return DTSValidationStatus.VS_ISCORRUPT;
                //    }
                //}
                //else
                //{
                //    if (outputColumn.DataType != DataType.DT_WSTR)
                //    {
                //        PostError("If unicode selected, output column data type must be DT_WSTR");
                //        return DTSValidationStatus.VS_ISCORRUPT;
                //    }
                //}

                lineageID = outputColumn.LineageID;
                customPropertyCollection = outputColumn.CustomPropertyCollection;
                try
                {
                    customProperty = customPropertyCollection["InputColumnID"];
                }
                catch (Exception)
                {
                    PostError("Output column [" + outputColumn.Name + "] has no InputColumnID custom property.");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }

                int inputColumnID = (int)customProperty.Value;
                try
                {
                    inputColumn = inputColumnCollection.FindObjectByID(inputColumnID);
                }
                catch (Exception)
                {
                    PostError("InputColumnID [" + inputColumnID + "] not found in selected input columns.");
                    return DTSValidationStatus.VS_ISCORRUPT;
                }
                // if input column is orphaned, we already have reinit status, and cleanup will 
                // eliminate any following errors. 
                if (inputColumn.IsValid)
                {
                    if (inputColumn.DataType != DataType.DT_STR && inputColumn.DataType != DataType.DT_WSTR 
                        && inputColumn.DataType != DataType.DT_I4)
                    {
                        PostError("La colonna [" + inputColumn.Name + "] non ha il tipo di dato corretto. Il tipo deve essere: DT_STR, DT_STR o DT_I4.");
                        //todo togliere commento
                        promoteStatus(ref status, DTSValidationStatus.VS_ISBROKEN);
                    }
                }
            }
            return status;
        }

        #region controlli per aggiunta di input e output
        public override void ReinitializeMetaData()
        {
            // This should delete orphaned columns for us, calling OnDeletingInputColumn
            // If data type changes to non-string, component is broken and user must fix manually
            base.ReinitializeMetaData();
        }

        public override IDTSInput90 InsertInput(DTSInsertPlacement insertPlacement, int inputID)
        {
            PostError("Component requires exactly one input. New input is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDINPUT);
        }

        public override void DeleteInput(int inputID)
        {
            PostError("Component requires exactly one input. Deleted input is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTDELETEINPUT);
        }

        public override IDTSOutput90 InsertOutput(DTSInsertPlacement insertPlacement, int outputID)
        {
            PostError("Component requires exactly one output. New output is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDOUTPUT);
        }

        public override void DeleteOutput(int outputID)
        {
            PostError("Component requires exactly one output. Deleted output is forbidden.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTDELETEOUTPUT);
        }

        public override IDTSOutputColumn90 InsertOutputColumnAt(int outputID, int outputColumnIndex, string name, string description)
        {
            PostError("Component forbids adding output columns. Check input columnn to configure.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDCOLUMN);
        }

        public override void DeleteOutputColumn(int outputID, int outputColumnID)
        {
            PostError("Component forbids deleting output columns. Uncheck input columnn to configure.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDCOLUMN);
        }
        
        public override void  OnDeletingInputColumn(int inputID, int inputColumnID)
        {
			IDTSComponentMetaData90 metadata = this.ComponentMetaData;

			// An input column is being deleted. This may be because it no longer is 
			// present upstream, or because the user has unchecked it.
			// We have to retrieve the column, then remove any 
			// reference to it in the output column collection
			IDTSInputCollection90 inputCollection = metadata.InputCollection;
    		IDTSInput90 input = inputCollection[0];
			// Get input column collection and retrieve interesting column
			IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            IDTSInputColumn90 inputColumn = inputColumnCollection.FindObjectByID(inputColumnID);

			// Iterate over output columns looking for matching id in custom property.
			IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
			IDTSOutput90 output = outputCollection[0];
			IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
			for (int j = 0; j < outputColumnCollection.Count; j++)
			{
				IDTSOutputColumn90 outputColumn = outputColumnCollection[j];
				IDTSCustomPropertyCollection90 customPropertyCollection = outputColumn.CustomPropertyCollection;
				IDTSCustomProperty90 customProperty = customPropertyCollection["InputColumnID"];
				int columnID = (int)customProperty.Value;
				if (columnID == inputColumnID)
				{
                    // we just delete the output column
                    base.DeleteOutputColumn(output.ID, outputColumn.ID);
				}
			}
        }
        #endregion

        public override IDTSCustomProperty90 SetComponentProperty(string propertyName, object propertyValue)
        {
            IDTSCustomProperty90 ret = base.SetComponentProperty(propertyName, propertyValue);

            IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            IDTSCustomProperty90 customProperty;
            IDTSCustomPropertyCollection90 customPropertyCollection = metadata.CustomPropertyCollection;

            // validate will catch non-existent custom property
            //customProperty = customPropertyCollection["DateExpression"];
            //this.dateExpression = (string)customProperty.Value;

            //customProperty = customPropertyCollection["DefaultValue"];
            //this.dateDefaultValue = (string)customProperty.Value;

            // Iterate over output columns setting code pages.
            IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            IDTSOutput90 output = outputCollection[0];
            IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            IDTSInputCollection90 inputCollection = metadata.InputCollection;
            IDTSInput90 input = inputCollection[0];
            IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            for (int j = 0; j < outputColumnCollection.Count; j++)
            {
                IDTSOutputColumn90 outputColumn = outputColumnCollection[j];
                customPropertyCollection = outputColumn.CustomPropertyCollection;
                customProperty = customPropertyCollection["InputColumnID"];
                int columnID = (int)customProperty.Value;
                IDTSInputColumn90 inputColumn = inputColumnCollection.FindObjectByID(columnID);
                //if (convertToUnicode)
                {
                    outputColumn.SetDataTypeProperties(DataType.DT_DBDATE, 0, 0, 0, 0);
                }
                //else
                //{
                //    outputColumn.SetDataTypeProperties(DataType.DT_STR, inputColumn.Length * 2, 0, 0, codePage);
                //}
            }
            return ret;
        }

        public override void SetOutputColumnDataTypeProperties(int iOutputID, int iOutputColumnID, DataType eDataType, int iLength, int iPrecision, int iScale, int iCodePage)
        {
            PostError("ToDateConvert does not permit changes to individual output column data types.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETOUTPUTCOLUMNDATATYPEPROPERTIES);
            
            //commento perchè non permettiamo 
            
            //// We allow users to change length of string
            //IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            //IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            //IDTSOutput90 output = outputCollection[0];
            //IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            //IDTSOutputColumn90 outputColumn = outputColumnCollection.FindObjectByID(iOutputColumnID);

            //if (eDataType != outputColumn.DataType)
            //{
            //    PostError("ToDateConvert does not permit changes to individual output column data types.");
            //    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETOUTPUTCOLUMNDATATYPEPROPERTIES);
            //}
            //else
            //{
            //    // implicit && eDataType == DataType.DT_STR here
            //    if (outputColumn.CodePage != iCodePage)
            //    {
            //        PostError("ConvertCodePage does not permit changes to individual output column code pages.");
            //        throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETOUTPUTCOLUMNDATATYPEPROPERTIES);
            //    }
            //    else
            //    {
            //        outputColumn.SetDataTypeProperties(outputColumn.DataType, iLength, 0, 0, outputColumn.CodePage);
            //    }
            //}
        }


        public override IDTSInputColumn90 SetUsageType(int inputID, IDTSVirtualInput90 virtualInput, int lineageID, DTSUsageType usageType)
        {
            IDTSInputColumn90 inputColumn;
            IDTSComponentMetaData90 metadata = this.ComponentMetaData;

            switch (usageType)
            {
                case DTSUsageType.UT_READONLY:
			        IDTSVirtualInputColumn90 column = virtualInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(lineageID);
                    if (column.DataType != DataType.DT_WSTR && column.DataType != DataType.DT_STR 
                        && column.DataType != DataType.DT_I4)
                    {
                        PostError("Component operates only on string or int input. Other types are forbidden.");
                        throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
                    }
                    else
                    {
                        inputColumn = base.SetUsageType(inputID, virtualInput, lineageID, usageType);
                        IDTSCustomProperty90 customProperty;
                        IDTSCustomPropertyCollection90 customPropertyCollection = metadata.CustomPropertyCollection;
                        
                        // validate will catch non-existent custom property
                        //customProperty = customPropertyCollection["DateExpression"];
                        //string dateExpression = (string)customProperty.Value;
                        
                        //customProperty = customPropertyCollection["ConvertToUnicode"];
                        //bool convertToUnicode = (bool)customProperty.Value;

                        IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
                        IDTSOutput90 output = outputCollection[0];
                        IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
                        IDTSOutputColumn90 newColumn;
                        newColumn = base.InsertOutputColumnAt(output.ID, outputColumnCollection.Count,
                            /*inputColumn.UpstreamComponentName + "." + */inputColumn.Name + ".Date", "");
                       //if (!convertToUnicode)
                       // {
                       //     // multiply by 2 to account for dbcs. If using utf-8 or gb18030, etc. this may not be enough
                       //     newColumn.SetDataTypeProperties(DataType.DT_STR, inputColumn.Length * 2, 0, 0, codePage);
                       // }
                        //else
                        {
                            newColumn.SetDataTypeProperties(DataType.DT_DBDATE, 0, 0, 0, 0);
                        }
                        customPropertyCollection = newColumn.CustomPropertyCollection;
                        customProperty = customPropertyCollection.New();
                        customProperty.Name = "InputColumnID"; // do not localize
                        // support cut and paste
                        customProperty.ContainsID = true;
                        customProperty.Value = inputColumn.ID;

                        return inputColumn;
                    }
                case DTSUsageType.UT_READWRITE:
                    PostError("Component requires that input columns be marked read only.");
                    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
                case DTSUsageType.UT_IGNORED:
                    IDTSInputCollection90 inputCollection = metadata.InputCollection;
                    IDTSInput90 input = inputCollection[0];
                    IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
                    inputColumn = inputColumnCollection.GetInputColumnByLineageID(lineageID);
					this.OnDeletingInputColumn(inputID, inputColumn.ID);
                    inputColumn = base.SetUsageType(inputID, virtualInput, lineageID, usageType);
                    return inputColumn;
                default:
                    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
            }
        }


        
#endregion

        #region private member variables

        private int[] outColumnWriteIDs;
        private int[] outColumnSourceIDs;
        private int numOutColumnWrites = 0;

        #endregion

        #region runtime functionality
        public override void PreExecute()
        {
            IDTSCustomProperty90 customProperty;
            IDTSCustomPropertyCollection90 customPropertyCollection;

            int lineageID;

            IDTSComponentMetaData90 metadata = this.ComponentMetaData;
            
            IDTSInput90 input = this.ComponentMetaData.InputCollection[0];
            int inputBufferID = input.Buffer;
            IDTSInputColumnCollection90 inputColumnCollection = input.InputColumnCollection;
            IDTSInputColumn90 inputColumn;
            IDTSOutputCollection90 outputCollection = metadata.OutputCollection;
            IDTSOutput90 output = outputCollection[0];
            IDTSOutputColumnCollection90 outputColumnCollection = output.OutputColumnCollection;
            IDTSOutputColumn90 outputColumn;

            // get output columns to write
            this.outColumnWriteIDs = new int[outputColumnCollection.Count];
            this.outColumnSourceIDs = new int[outputColumnCollection.Count];
            this.numOutColumnWrites = 0;
            for (int j = 0; j < outputColumnCollection.Count; j++)
            {
                outputColumn = outputColumnCollection[j];
                lineageID = outputColumn.LineageID;
                // this.numOutColumnWrites index is incremented farther below.
                this.outColumnWriteIDs[this.numOutColumnWrites] = this.BufferManager.FindColumnByLineageID(inputBufferID, lineageID);
                customPropertyCollection = outputColumn.CustomPropertyCollection;
                customProperty = customPropertyCollection["InputColumnID"];
                
                int inputID = (int)customProperty.Value;
                inputColumn = inputColumnCollection.FindObjectByID(inputID);
                this.outColumnSourceIDs[this.numOutColumnWrites] = this.BufferManager.FindColumnByLineageID(inputBufferID, inputColumn.LineageID);
                this.numOutColumnWrites++;
            }
        }

        public override void ProcessInput(int inputID, PipelineBuffer buffer)
        {
            #region Carichiamo le customproperty
            IDTSCustomProperty90 customProperty;
            IDTSCustomPropertyCollection90 customPropertyCollection;
            IDTSComponentMetaData90 metadata = this.ComponentMetaData;

            //carichiamo le customproperty
            customPropertyCollection = metadata.CustomPropertyCollection;

            customProperty = customPropertyCollection["DateExpression"];
            this.dateExpression = (string)customProperty.Value;
            
            customProperty = customPropertyCollection["DefaultValue"];
            string defaultValue = (string)customProperty.Value;
            ValidateDateDefaultValue(out this.dateDefaultValue, defaultValue);

            customProperty = customPropertyCollection["MinValue"];
            string minValue = (string)customProperty.Value;
            ValidateDateDefaultValue(out this.dateMinValue, minValue);

            customProperty = customPropertyCollection["MaxValue"];
            string maxValue = (string)customProperty.Value;
            ValidateDateDefaultValue(out this.dateMaxValue, maxValue);
            #endregion

            String source;
            if (!buffer.EndOfRowset)
            {
                while (buffer.NextRow())
                {
                    for (int j = 0; j < this.numOutColumnWrites; j++)
                    {
                        Object o = buffer[this.outColumnSourceIDs[j]];

                        Type t = o.GetType();
                        if(t == typeof(Int32))
                            source = (buffer.GetInt32(this.outColumnSourceIDs[j])).ToString();
                        else
                            source = buffer.GetString(this.outColumnSourceIDs[j]);

                        try
                        {
                            DateTime data;

                            if (DateTime.TryParseExact(source, this.dateExpression, null, DateTimeStyles.None, out data))
                            {
                                //controlliamo che la data sia compresa nel range
                                if (
                                    (this.dateMinValue == null || (this.dateMinValue != null && this.dateMinValue <= data))
                                    &&
                                    (this.dateMaxValue == null || (this.dateMaxValue != null && this.dateMaxValue >= data))
                                    )
                                {
                                    buffer[this.outColumnWriteIDs[j]] = data;
                                }
                                else
                                {
                                    //se valido impostiamo il valore di default
                                    if (this.dateDefaultValue != null)
                                        buffer[this.outColumnWriteIDs[j]] = this.dateDefaultValue;
                                    else buffer.SetNull(this.outColumnWriteIDs[j]);
                                }
                            }
                            else
                            {
                                ////se valido impostiamo il valore di default
                                if (this.dateDefaultValue != null)
                                    buffer[this.outColumnWriteIDs[j]] = this.dateDefaultValue;
                                else buffer.SetNull(this.outColumnWriteIDs[j]);
                            }
                        }
                        catch (Exception e)
                        {
                            string g = e.Message;
                            PostError("Errore nella creazione del formato data nella colonna: " + this.outColumnSourceIDs[j] 
                                + "."+ Environment.NewLine + "Error message:" + e.Message);
                            throw new PipelineComponentHResultException(HResults.DTS_E_INDUCEDTRANSFORMFAILUREONTRUNCATION);
                        }
                    }
                }
            }
        }

  
        #endregion

        /// <summary>
        /// Valida la data impostata come default value
        /// </summary>
        /// <param name="dataDefault"></param>
        /// <returns></returns>
        private bool ValidateDateDefaultValue(out DateTime? dataDefault, string dataStringValue)
        {
            

            if (dataStringValue == "NULL")
            {
                dataDefault = null;
                return true;
            }
            else
            {
                //impostiamo a null la data, così nel caso in cui non matchi sovrascriviamo valori precedenti
                dataDefault = null;

                Match match;
                Regex regex;
                regex = new Regex(this.dateRegularExpression);
                match = regex.Match(dataStringValue);
                if (match.Success)
                {
                    DateTime data;
                    if (DateTime.TryParseExact(dataStringValue, this.dateRegularFormat, null, DateTimeStyles.None, out data))
                    {
                        dataDefault = data;
                        return true;
                    }
                    else return false;
                }
                else
                    return false;
            }
        }
    }
}