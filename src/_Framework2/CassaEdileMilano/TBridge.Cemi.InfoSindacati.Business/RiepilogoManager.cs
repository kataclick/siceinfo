using System.Collections.Generic;
using TBridge.Cemi.InfoSindacati.Data;
using TBridge.Cemi.InfoSindacati.Type.Entities;

namespace TBridge.Cemi.InfoSindacati.Business
{
    public class RiepilogoManager
    {
        private readonly DataProvider dataProvider = new DataProvider();

        public List<string> GetElencoComprensori()
        {
            List<string> ret = new List<string>();
            ret.Add("Tutti");
            List<string> comp = dataProvider.GetElencoComprensori();
            ret.AddRange(comp);
            return ret;
        }

        public Riepilogo GetRiepilogo(RiepilogoFilter filter)
        {
            return dataProvider.GetRiepilogo(filter);
        }
    }
}