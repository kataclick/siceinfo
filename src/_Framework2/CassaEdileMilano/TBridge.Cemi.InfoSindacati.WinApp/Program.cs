using System;
using System.Windows.Forms;

namespace TBridge.Cemi.InfoSindacati.WinApp
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new FormMain());
            MyApplicationStart myAppStart = new MyApplicationStart();
            myAppStart.Run(new string[] { });
        }
    }
}