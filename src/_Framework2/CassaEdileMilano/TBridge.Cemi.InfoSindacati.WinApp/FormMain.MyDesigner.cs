using System.Windows.Forms;

namespace TBridge.Cemi.InfoSindacati.WinApp
{
    partial class FormMain
    {
        #region Lavoratori

        private readonly DataGridViewTextBoxColumn codiceDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn cognomeDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn codiceFiscaleDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn dataNascitaDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn indirizzoDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn codiceDittaDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn ragioneSocialeDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn sindacatoDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn tipoCessazioneDataGridViewTextBoxColumnLavoratore =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewButtonColumn ColumnSelectLavoratore = new DataGridViewButtonColumn();

        #endregion

        #region Imprese

        private readonly DataGridViewTextBoxColumn codiceDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn ragioneSocialeDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn codiceFiscaleDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn partitaIvaDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn numeroDipendentiDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewTextBoxColumn dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa =
            new DataGridViewTextBoxColumn();

        private readonly DataGridViewButtonColumn ColumnSelectImpresa = new DataGridViewButtonColumn();

        #endregion

        private void InitializeMyComponent()
        {
            #region Lavoratori

            // 
            // codiceDataGridViewTextBoxColumnLavoratore
            // 
            codiceDataGridViewTextBoxColumnLavoratore.DataPropertyName = "idLavoratore";
            codiceDataGridViewTextBoxColumnLavoratore.HeaderText = "Cod";
            codiceDataGridViewTextBoxColumnLavoratore.Name = "codiceDataGridViewTextBoxColumnLavoratore";
            codiceDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            codiceDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // cognomeDataGridViewTextBoxColumnLavoratore
            // 
            cognomeDataGridViewTextBoxColumnLavoratore.DataPropertyName = "Cognome";
            cognomeDataGridViewTextBoxColumnLavoratore.HeaderText = "Cognome";
            cognomeDataGridViewTextBoxColumnLavoratore.Name = "cognomeDataGridViewTextBoxColumnLavoratore";
            cognomeDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            cognomeDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // nomeDataGridViewTextBoxColumnLavoratore
            // 
            nomeDataGridViewTextBoxColumnLavoratore.DataPropertyName = "Nome";
            nomeDataGridViewTextBoxColumnLavoratore.HeaderText = "Nome";
            nomeDataGridViewTextBoxColumnLavoratore.Name = "nomeDataGridViewTextBoxColumnLavoratore";
            nomeDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            nomeDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // codiceFiscaleDataGridViewTextBoxColumnLavoratore
            // 
            codiceFiscaleDataGridViewTextBoxColumnLavoratore.DataPropertyName = "CodiceFiscale";
            codiceFiscaleDataGridViewTextBoxColumnLavoratore.HeaderText = "C.F.";
            codiceFiscaleDataGridViewTextBoxColumnLavoratore.Name = "codiceFiscaleDataGridViewTextBoxColumnLavoratore";
            codiceFiscaleDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            codiceFiscaleDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // dataNascitaDataGridViewTextBoxColumnLavoratore
            // 
            dataNascitaDataGridViewTextBoxColumnLavoratore.DataPropertyName = "DataNascita";
            dataNascitaDataGridViewTextBoxColumnLavoratore.HeaderText = "Data Nascita";
            dataNascitaDataGridViewTextBoxColumnLavoratore.Name = "dataNascitaDataGridViewTextBoxColumnLavoratore";
            dataNascitaDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            dataNascitaDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // indirizzoDataGridViewTextBoxColumnLavoratore
            // 
            indirizzoDataGridViewTextBoxColumnLavoratore.DataPropertyName = "IndirizzoCompleto";
            indirizzoDataGridViewTextBoxColumnLavoratore.HeaderText = "Indirizzo Lav";
            indirizzoDataGridViewTextBoxColumnLavoratore.Name = "indirizzoDataGridViewTextBoxColumnLavoratore";
            indirizzoDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            indirizzoDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // codiceDittaDataGridViewTextBoxColumnLavoratore
            // 
            codiceDittaDataGridViewTextBoxColumnLavoratore.DataPropertyName = "idImpresa";
            codiceDittaDataGridViewTextBoxColumnLavoratore.HeaderText = "Cod Ditta";
            codiceDittaDataGridViewTextBoxColumnLavoratore.Name = "codiceDittaDataGridViewTextBoxColumnLavoratore";
            codiceDittaDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            codiceDittaDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // ragioneSocialeDataGridViewTextBoxColumnLavoratore
            // 
            ragioneSocialeDataGridViewTextBoxColumnLavoratore.DataPropertyName = "RagioneSociale";
            ragioneSocialeDataGridViewTextBoxColumnLavoratore.HeaderText = "R. Sociale";
            ragioneSocialeDataGridViewTextBoxColumnLavoratore.Name = "ragioneSocialeDataGridViewTextBoxColumnLavoratore";
            ragioneSocialeDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            ragioneSocialeDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // sindacatoDataGridViewTextBoxColumnLavoratore
            // 
            sindacatoDataGridViewTextBoxColumnLavoratore.DataPropertyName = "Sindacato";
            sindacatoDataGridViewTextBoxColumnLavoratore.HeaderText = "Sind";
            sindacatoDataGridViewTextBoxColumnLavoratore.Name = "sindacatoDataGridViewTextBoxColumnLavoratore";
            sindacatoDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            sindacatoDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // tipoCessazioneDataGridViewTextBoxColumnLavoratore
            // 
            tipoCessazioneDataGridViewTextBoxColumnLavoratore.DataPropertyName = "TipoFineRapporto";
            tipoCessazioneDataGridViewTextBoxColumnLavoratore.HeaderText = "Ces.";
            tipoCessazioneDataGridViewTextBoxColumnLavoratore.Name = "tipoCessazioneDataGridViewTextBoxColumnLavoratore";
            tipoCessazioneDataGridViewTextBoxColumnLavoratore.ReadOnly = true;
            tipoCessazioneDataGridViewTextBoxColumnLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // ColumnSelectLavoratore
            // 
            ColumnSelectLavoratore.HeaderText = "";
            ColumnSelectLavoratore.Name = "ColumnSelectLavoratore";
            ColumnSelectLavoratore.ReadOnly = true;
            ColumnSelectLavoratore.Text = "OK";
            ColumnSelectLavoratore.UseColumnTextForButtonValue = true;
            ColumnSelectLavoratore.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            #endregion

            #region Imprese

            // 
            // codiceDataGridViewTextBoxColumnImpresa
            // 
            codiceDataGridViewTextBoxColumnImpresa.DataPropertyName = "idImpresa";
            codiceDataGridViewTextBoxColumnImpresa.HeaderText = "Codice";
            codiceDataGridViewTextBoxColumnImpresa.Name = "codiceDataGridViewTextBoxColumnImpresa";
            codiceDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            codiceDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // ragioneSocialeDataGridViewTextBoxColumnImpresa
            // 
            ragioneSocialeDataGridViewTextBoxColumnImpresa.DataPropertyName = "RagioneSociale";
            ragioneSocialeDataGridViewTextBoxColumnImpresa.HeaderText = "Ragione Sociale";
            ragioneSocialeDataGridViewTextBoxColumnImpresa.Name = "ragioneSocialeDataGridViewTextBoxColumnImpresa";
            ragioneSocialeDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            ragioneSocialeDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa
            // 
            indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa.DataPropertyName = "IndirizzoCompletoSedeLegale";
            indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa.HeaderText = "Indirizzo Sede Legale";
            indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa.Name =
                "indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa";
            indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // codiceFiscaleDataGridViewTextBoxColumnImpresa
            // 
            codiceFiscaleDataGridViewTextBoxColumnImpresa.DataPropertyName = "codiceFiscaleImpresa";
            codiceFiscaleDataGridViewTextBoxColumnImpresa.HeaderText = "C.F.";
            codiceFiscaleDataGridViewTextBoxColumnImpresa.Name = "codiceFiscaleDataGridViewTextBoxColumnImpresa";
            codiceFiscaleDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            codiceFiscaleDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // partitaIvaDataGridViewTextBoxColumnImpresa
            // 
            partitaIvaDataGridViewTextBoxColumnImpresa.DataPropertyName = "PartitaIVA";
            partitaIvaDataGridViewTextBoxColumnImpresa.HeaderText = "P. IVA";
            partitaIvaDataGridViewTextBoxColumnImpresa.Name = "partitaIvaDataGridViewTextBoxColumnImpresa";
            partitaIvaDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            partitaIvaDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // numeroDipendentiDataGridViewTextBoxColumnImpresa
            // 
            numeroDipendentiDataGridViewTextBoxColumnImpresa.DataPropertyName = "NumeroDipendenti";
            numeroDipendentiDataGridViewTextBoxColumnImpresa.HeaderText = "Numero dipendenti";
            numeroDipendentiDataGridViewTextBoxColumnImpresa.Name = "numeroDipendentiDataGridViewTextBoxColumnImpresa";
            numeroDipendentiDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            numeroDipendentiDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa
            // 
            dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa.DataPropertyName = "DataDenuncia";
            dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa.HeaderText = "Data ultima denuncia";
            dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa.Name =
                "dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa";
            dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa.ReadOnly = true;
            dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            // 
            // ColumnSelectImpresa
            // 
            ColumnSelectImpresa.HeaderText = "Dettagli";
            ColumnSelectImpresa.Name = "ColumnSelectImpresa";
            ColumnSelectImpresa.ReadOnly = true;
            ColumnSelectImpresa.Text = "OK";
            ColumnSelectImpresa.UseColumnTextForButtonValue = true;
            ColumnSelectImpresa.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            #endregion
        }
    }
}