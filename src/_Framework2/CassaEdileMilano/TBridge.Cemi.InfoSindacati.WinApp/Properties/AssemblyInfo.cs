﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly : AssemblyTitle("Informativa ai Sindacati")]
[assembly : AssemblyDescription("Applicativo di supporto per i Sindacati")]
[assembly : AssemblyConfiguration("")]
[assembly : AssemblyCompany("T Bridge s.p.a.")]
[assembly : AssemblyProduct("TBridge.Cemi.InfoSindacati.WinApp")]
[assembly : AssemblyCopyright("Copyright ©  2007")]
[assembly : AssemblyTrademark("")]
[assembly : AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly : ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly : Guid("ea00bc0d-0609-4ec4-a3ac-020e79abe9cc")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//

[assembly : AssemblyVersion("1.0.0.0")]
[assembly : AssemblyFileVersion("1.0.0.0")]