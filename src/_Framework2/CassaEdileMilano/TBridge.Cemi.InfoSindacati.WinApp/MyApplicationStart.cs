using Microsoft.VisualBasic.ApplicationServices;

namespace TBridge.Cemi.InfoSindacati.WinApp
{
    public class MyApplicationStart : WindowsFormsApplicationBase
    {
        protected override void OnCreateMainForm()
        {
            MainForm = new FormMain();
        }

        protected override void OnCreateSplashScreen()
        {
            SplashScreen = new SplashForm();
        }
    }
}