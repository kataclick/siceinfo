using System;
using System.Collections.Generic;

namespace TBridge.Cemi.WebServices.CPT
{
    [Serializable]
    public class CantiereCPT
    {
        private decimal ammontareLavori;
        private string cantiereCap;
        private string cantiereCivico;
        private string cantiereComune;
        private string cantiereIndirizzo;
        private string cantiereInfoAggiuntiva;
        private decimal cantiereLatitudine;
        private decimal cantiereLongitudine;
        private string cantiereProvincia;
        private int codiceCantiere;
        private string committenteRagioneSociale;
        private string coordinatoreEsecuzioneNominativo;
        private string coordinatoreEsecuzioneRagioneSociale;
        private DateTime dataFineLavori;
        private DateTime dataInizioLavori;
        private int durataLavori;
        private string naturaLavori;
        private string numeroAppalto;
        private List<SubappaltoCPT> subappalti = new List<SubappaltoCPT>();

        public int CodiceCantiere
        {
            get { return codiceCantiere; }
            set { codiceCantiere = value; }
        }

        public string CommittenteRagioneSociale
        {
            get { return committenteRagioneSociale; }
            set { committenteRagioneSociale = value; }
        }

        public string CantiereIndirizzo
        {
            get { return cantiereIndirizzo; }
            set { cantiereIndirizzo = value; }
        }

        public string CantiereCivico
        {
            get { return cantiereCivico; }
            set { cantiereCivico = value; }
        }

        public string CantiereInfoAggiuntiva
        {
            get { return cantiereInfoAggiuntiva; }
            set { cantiereInfoAggiuntiva = value; }
        }

        public string CantiereComune
        {
            get { return cantiereComune; }
            set { cantiereComune = value; }
        }

        public string CantiereProvincia
        {
            get { return cantiereProvincia; }
            set { cantiereProvincia = value; }
        }

        public string CantiereCap
        {
            get { return cantiereCap; }
            set { cantiereCap = value; }
        }

        public decimal CantiereLatitudine
        {
            get { return cantiereLatitudine; }
            set { cantiereLatitudine = value; }
        }

        public decimal CantiereLongitudine
        {
            get { return cantiereLongitudine; }
            set { cantiereLongitudine = value; }
        }

        public string CoordinatoreEsecuzioneNominativo
        {
            get { return coordinatoreEsecuzioneNominativo; }
            set { coordinatoreEsecuzioneNominativo = value; }
        }

        public string CoordinatoreEsecuzioneRagioneSociale
        {
            get { return coordinatoreEsecuzioneRagioneSociale; }
            set { coordinatoreEsecuzioneRagioneSociale = value; }
        }

        public string NaturaLavori
        {
            get { return naturaLavori; }
            set { naturaLavori = value; }
        }

        public decimal AmmontareLavori
        {
            get { return ammontareLavori; }
            set { ammontareLavori = value; }
        }

        public DateTime DataInizioLavori
        {
            get { return dataInizioLavori; }
            set { dataInizioLavori = value; }
        }

        public DateTime DataFineLavori
        {
            get { return dataFineLavori; }
            set { dataFineLavori = value; }
        }

        public int DurataLavori
        {
            get { return durataLavori; }
            set { durataLavori = value; }
        }

        public string NumeroAppalto
        {
            get { return numeroAppalto; }
            set { numeroAppalto = value; }
        }

        public List<SubappaltoCPT> Subappalti
        {
            get { return subappalti; }
            set { subappalti = value; }
        }
    }
}