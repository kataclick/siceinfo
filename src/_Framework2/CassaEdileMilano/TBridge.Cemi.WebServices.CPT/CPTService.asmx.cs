using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.WebServices.CPT
{
    /// <summary>
    /// Summary description for CPTService
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/CPT/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class CPTService : WebService
    {
        private readonly Database databaseCemi = DatabaseFactory.CreateDatabase("CEMI");

        [WebMethod]
        public List<CantiereCPT> GetCantieri()
        {
            List<CantiereCPT> ret = new List<CantiereCPT>();

            try
            {
                using (DbCommand dbCommandCantiere = databaseCemi.GetStoredProcCommand("dbo.USP_CptCantieriSelectAll"))
                {
                    //dbCommand.CommandTimeout = 900;
                    //ret = databaseCemi.ExecuteDataSet(dbCommand);
                    using (IDataReader drCantiere = databaseCemi.ExecuteReader(dbCommandCantiere))
                    {
                        int ammontareLavoriIndex = drCantiere.GetOrdinal("ammontareLavori");
                        int cantiereCapIndex = drCantiere.GetOrdinal("cantiereCap");
                        int cantiereCivicoIndex = drCantiere.GetOrdinal("cantiereCivico");
                        int cantiereComuneIndex = drCantiere.GetOrdinal("cantiereComune");
                        int cantiereIndirizzoIndex = drCantiere.GetOrdinal("cantiereIndirizzo");
                        int cantiereInfoAggiuntivaIndex = drCantiere.GetOrdinal("cantiereInfoAggiuntiva");
                        int cantiereLatitudineIndex = drCantiere.GetOrdinal("cantiereLatitudine");
                        int cantiereLongitudineIndex = drCantiere.GetOrdinal("cantiereLongitudine");
                        int cantiereProvinciaIndex = drCantiere.GetOrdinal("cantiereProvincia");
                        int codiceCantiereIndex = drCantiere.GetOrdinal("codiceCantiere");
                        int committenteRagioneSocialeIndex = drCantiere.GetOrdinal("committenteRagioneSociale");
                        int coordinatoreEsecuzioneNominativoIndex =
                            drCantiere.GetOrdinal("coordinatoreEsecuzioneNominativo");
                        int coordinatoreEsecuzioneRagioneSocialeIndex =
                            drCantiere.GetOrdinal("coordinatoreEsecuzioneRagioneSociale");
                        int dataFineLavoriIndex = drCantiere.GetOrdinal("dataFineLavori");
                        int dataInizioLavoriIndex = drCantiere.GetOrdinal("dataInizioLavori");
                        int durataLavoriIndex = drCantiere.GetOrdinal("durataLavori");
                        int naturaLavoriIndex = drCantiere.GetOrdinal("naturaLavori");
                        int numeroAppaltoIndex = drCantiere.GetOrdinal("numeroAppalto");

                        while (drCantiere.Read())
                        {
                            CantiereCPT cantiere = new CantiereCPT();

                            cantiere.CodiceCantiere = drCantiere.GetInt32(codiceCantiereIndex);

                            if (!drCantiere.IsDBNull(ammontareLavoriIndex))
                                cantiere.AmmontareLavori = drCantiere.GetDecimal(ammontareLavoriIndex);
                            if (!drCantiere.IsDBNull(cantiereCapIndex))
                                cantiere.CantiereCap = drCantiere.GetString(cantiereCapIndex);
                            if (!drCantiere.IsDBNull(cantiereCivicoIndex))
                                cantiere.CantiereCivico = drCantiere.GetString(cantiereCivicoIndex);
                            if (!drCantiere.IsDBNull(cantiereComuneIndex))
                                cantiere.CantiereComune = drCantiere.GetString(cantiereComuneIndex);
                            if (!drCantiere.IsDBNull(cantiereIndirizzoIndex))
                                cantiere.CantiereIndirizzo = drCantiere.GetString(cantiereIndirizzoIndex);
                            if (!drCantiere.IsDBNull(cantiereInfoAggiuntivaIndex))
                                cantiere.CantiereInfoAggiuntiva = drCantiere.GetString(cantiereInfoAggiuntivaIndex);
                            if (!drCantiere.IsDBNull(cantiereLatitudineIndex))
                                cantiere.CantiereLatitudine = drCantiere.GetDecimal(cantiereLatitudineIndex);
                            if (!drCantiere.IsDBNull(cantiereLongitudineIndex))
                                cantiere.CantiereLongitudine = drCantiere.GetDecimal(cantiereLongitudineIndex);
                            if (!drCantiere.IsDBNull(cantiereProvinciaIndex))
                                cantiere.CantiereProvincia = drCantiere.GetString(cantiereProvinciaIndex);
                            if (!drCantiere.IsDBNull(committenteRagioneSocialeIndex))
                                cantiere.CommittenteRagioneSociale = drCantiere.GetString(committenteRagioneSocialeIndex);
                            if (!drCantiere.IsDBNull(coordinatoreEsecuzioneNominativoIndex))
                                cantiere.CoordinatoreEsecuzioneNominativo =
                                    drCantiere.GetString(coordinatoreEsecuzioneNominativoIndex);
                            if (!drCantiere.IsDBNull(coordinatoreEsecuzioneRagioneSocialeIndex))
                                cantiere.CoordinatoreEsecuzioneRagioneSociale =
                                    drCantiere.GetString(coordinatoreEsecuzioneRagioneSocialeIndex);
                            if (!drCantiere.IsDBNull(dataFineLavoriIndex))
                                cantiere.DataFineLavori = drCantiere.GetDateTime(dataFineLavoriIndex);
                            if (!drCantiere.IsDBNull(dataInizioLavoriIndex))
                                cantiere.DataInizioLavori = drCantiere.GetDateTime(dataInizioLavoriIndex);
                            if (!drCantiere.IsDBNull(durataLavoriIndex))
                                cantiere.DurataLavori = drCantiere.GetInt32(durataLavoriIndex);
                            if (!drCantiere.IsDBNull(naturaLavoriIndex))
                                cantiere.NaturaLavori = drCantiere.GetString(naturaLavoriIndex);
                            if (!drCantiere.IsDBNull(numeroAppaltoIndex))
                                cantiere.NumeroAppalto = drCantiere.GetString(numeroAppaltoIndex);

                            using (
                                DbCommand dbCommandSubappalto =
                                    databaseCemi.GetStoredProcCommand("dbo.USP_CptSubappaltiSelectByIdCantiere"))
                            {
                                databaseCemi.AddInParameter(dbCommandSubappalto, "@idCantiere", DbType.Int32,
                                                            cantiere.CodiceCantiere);
                                using (IDataReader drSubappalto = databaseCemi.ExecuteReader(dbCommandSubappalto))
                                {

                                    int impresaAppaltataDacodiceSubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDacodice");
                                    int impresaAppaltataDaPartitaIVASubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDaPartitaIVA");
                                    int impresaAppaltataDaRagioneSocialeSubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDaRagioneSociale");
                                    int impresaSelezionatacodiceSubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionatacodice");
                                    int impresaSelezionataPartitaIVASubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionataPartitaIVA");
                                    int impresaSelezionataRagioneSocialeSubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionataRagioneSociale");

                                    while (drSubappalto.Read())
                                    {
                                        SubappaltoCPT subappalto = new SubappaltoCPT();

                                        subappalto.CodiceCantiere = cantiere.CodiceCantiere;

                                        if (!drSubappalto.IsDBNull(impresaAppaltataDacodiceSubIndex))
                                            subappalto.ImpresaAppaltataDaCodice =
                                                drSubappalto.GetInt32(impresaAppaltataDacodiceSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaAppaltataDaPartitaIVASubIndex))
                                            subappalto.ImpresaAppaltataDaPartitaIVA =
                                                drSubappalto.GetString(impresaAppaltataDaPartitaIVASubIndex);
                                        if (!drSubappalto.IsDBNull(impresaAppaltataDaRagioneSocialeSubIndex))
                                            subappalto.ImpresaAppaltataDaRagioneSociale =
                                                drSubappalto.GetString(impresaAppaltataDaRagioneSocialeSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionatacodiceSubIndex))
                                            subappalto.ImpresaSelezionataCodice =
                                                drSubappalto.GetInt32(impresaSelezionatacodiceSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionataPartitaIVASubIndex))
                                            subappalto.ImpresaSelezionataPartitaIVA =
                                                drSubappalto.GetString(impresaSelezionataPartitaIVASubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionataRagioneSocialeSubIndex))
                                            subappalto.ImpresaSelezionataRagioneSociale =
                                                drSubappalto.GetString(impresaSelezionataRagioneSocialeSubIndex);

                                        cantiere.Subappalti.Add(subappalto);
                                    }
                                }
                            }

                            ret.Add(cantiere);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }

            return ret;
        }
    }
}