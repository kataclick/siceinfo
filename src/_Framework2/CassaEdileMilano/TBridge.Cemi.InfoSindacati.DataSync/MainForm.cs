using System;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;

namespace TBridge.Cemi.InfoSindacati.DataSync
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            InitializeBackgroundWorker();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadConnString();
        }

        private void LoadConnString()
        {
            textBoxSource.Text = ConfigurationManager.ConnectionStrings["SiceSource"].ConnectionString;
            textBoxDest.Text = ConfigurationManager.ConnectionStrings["SiceCompactDest"].ConnectionString;
        }

        private void buttonSync_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxSource.Text) || String.IsNullOrEmpty(textBoxDest.Text))
            {
                MessageBox.Show("Connection string non definite!", "Attenzione", MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
            }
            else
            {
                textBoxLog.Text = string.Empty; // svuoto la textbox del log
                textBoxLog.AppendText(DateTime.Now.ToShortTimeString() + ": Inizio procedura di allineamento");
                buttonSync.Enabled = false;
                SyncData(textBoxSource.Text, textBoxDest.Text);
            }
        }

        private void SyncData(string connStringSource, string connStringDest)
        {
            SyncParam param = new SyncParam();
            param.ConnStringSource = connStringSource;
            param.ConnStringDest = connStringDest;

            backgroundWorkerSync.RunWorkerAsync(param);
        }

        #region BackgroundWorkers

        private void InitializeBackgroundWorker()
        {
            backgroundWorkerSync.RunWorkerCompleted += backgroundWorkerSync_RunWorkerCompleted;
            backgroundWorkerSync.ProgressChanged += backgroundWorkerSync_ProgressChanged;
            backgroundWorkerSync.DoWork += backgroundWorkerSync_DoWork;
        }

        private void backgroundWorkerSync_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            SyncParam param = (SyncParam) e.Argument;

            Syncronizer.SvuotaTabelle(param.ConnStringDest);

            worker.ReportProgress(1,
                                  String.Format("{0}: Inizio Sincronizzazione Lavoratori",
                                                DateTime.Now.ToShortTimeString()));
            int lavRet = Syncronizer.SyncLavoratori(param.ConnStringSource, param.ConnStringDest);
            worker.ReportProgress(1,
                                  String.Format("{0}: Sincronizzati {1} Lavoratori", DateTime.Now.ToShortTimeString(),
                                                lavRet));

            worker.ReportProgress(1,
                                  String.Format("{0}: Inizio Sincronizzazione Imprese", DateTime.Now.ToShortTimeString()));
            int impRet = Syncronizer.SyncImprese(param.ConnStringSource, param.ConnStringDest);
            worker.ReportProgress(1,
                                  String.Format("{0}: Sincronizzate {1} Imprese", DateTime.Now.ToShortTimeString(),
                                                impRet));


            worker.ReportProgress(1,
                                  String.Format("{0}: Inizio Sincronizzazione Sindacati",
                                                DateTime.Now.ToShortTimeString()));
            int sinRet = Syncronizer.SyncSindacati(param.ConnStringSource, param.ConnStringDest);
            worker.ReportProgress(1,
                                  String.Format("{0}: Sincronizzati {1} Sindacati", DateTime.Now.ToShortTimeString(),
                                                sinRet));

            worker.ReportProgress(1,
                                  String.Format("{0}: Inizio Sincronizzazione Cessazioni",
                                                DateTime.Now.ToShortTimeString()));
            int cesRet = Syncronizer.SyncTipiCessazione(param.ConnStringSource, param.ConnStringDest);
            worker.ReportProgress(1,
                                  String.Format("{0}: Sincronizzati {1} Cessazioni", DateTime.Now.ToShortTimeString(),
                                                cesRet));

            worker.ReportProgress(1,
                                  String.Format("{0}: Inizio Sincronizzazione Comprensori",
                                                DateTime.Now.ToShortTimeString()));
            int comRet = Syncronizer.SyncComprensori(param.ConnStringSource, param.ConnStringDest);
            worker.ReportProgress(1,
                                  String.Format("{0}: Sincronizzati {1} Comprensori", DateTime.Now.ToShortTimeString(),
                                                comRet));
        }

        private void backgroundWorkerSync_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if ((string) e.UserState != null)
                textBoxLog.AppendText(Environment.NewLine + (string) e.UserState /* + Environment.NewLine*/);
            progressBarSync.PerformStep();
            if (progressBarSync.Value == progressBarSync.Maximum)
                progressBarSync.Value = 0;
        }

        private void backgroundWorkerSync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonSync.Enabled = true;
            if (e.Error != null)
            {
                textBoxLog.AppendText(Environment.NewLine + DateTime.Now.ToShortTimeString() +
                                      ": Errore durante la procedura di allineamento!");
            }
                //else if (e.Cancelled)
                //{
                //    textBoxLog.AppendText(Environment.NewLine + "Procedura di import annullata!");
                //}
            else
            {
                progressBarSync.Value = progressBarSync.Maximum;
                textBoxLog.AppendText(Environment.NewLine + DateTime.Now.ToShortTimeString() +
                                      ": Procedura di allineamento completata");
            }
        }

        #endregion
    }
}