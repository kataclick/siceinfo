namespace TBridge.Cemi.InfoSindacati.DataSync
{
    internal class SyncParam
    {
        private string connStringDest;
        private string connStringSource;

        public string ConnStringSource
        {
            get { return connStringSource; }
            set { connStringSource = value; }
        }

        public string ConnStringDest
        {
            get { return connStringDest; }
            set { connStringDest = value; }
        }
    }
}