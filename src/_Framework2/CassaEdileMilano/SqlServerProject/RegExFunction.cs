using System.Data.SqlTypes;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    [SqlFunction]
    public static SqlString RegExSplitSms(string testoSms)
    {
        Regex r = new Regex(@"(?s)\b.{1,149}\b", RegexOptions.Compiled);
        string m = r.Match(testoSms).Value;
        if (m.Length < testoSms.Length)
            m += " <continua>";
        return new SqlString(m);
    }

    //public static readonly RegexOptions Options =
    //    RegexOptions.IgnorePatternWhitespace |
    //    RegexOptions.Singleline;

    //[SqlFunction]
    //public static SqlBoolean RegexMatch(
    //    SqlChars input, SqlString pattern)
    //{
    //    Regex regex = new Regex(pattern.Value, Options);
    //    return regex.IsMatch(new string(input.Value));
    //}
};

