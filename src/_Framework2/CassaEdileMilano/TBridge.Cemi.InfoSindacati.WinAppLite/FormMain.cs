using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GridViewExtensions.GridFilterFactories;
using TBridge.Cemi.InfoSindacati.Business;
using TBridge.Cemi.InfoSindacati.Type.Entities;

namespace TBridge.Cemi.InfoSindacati.WinAppLite
{
    public partial class FormMain : FormBase
    {
        private readonly LavoratoriManager lavoratoriManager;
        private readonly BindingSource source;

        public FormMain()
        {
            InitializeComponent();

            lavoratoriManager = new LavoratoriManager();
            source = new BindingSource();
            //(dgExtender.FilterFactory as GridViewExtensions.GridFilterFactories.DefaultGridFilterFactory).CreateDistinctGridFilters = true;
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            DataSet ds = lavoratoriManager.GetLavoratori();
            
            source.DataSource = ds.Tables[0];
            dataGridViewLav.DataSource = source;
        }
    }
}