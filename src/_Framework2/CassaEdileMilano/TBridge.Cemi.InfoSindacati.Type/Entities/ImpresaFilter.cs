using TBridge.Cemi.InfoSindacati.Type.Enums;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class ImpresaFilter
    {
        private int? idImpresa;
        private Operatori idImpresaOperatore = Operatori.Uguale;
        private string ragioneSociale;
        private string codiceFiscaleImpresa;
        private string partitaIVA;
        private string indirizzoSedeLegale;
        private string capSedeLegale;
        private string localitaSedeLegale;
        private string provinciaSedeLegale;

        public int? IdImpresa
        {
            get { return idImpresa; }
            set { idImpresa = value; }
        }

        public Operatori IdImpresaOperatore
        {
            get { return idImpresaOperatore; }
            set { idImpresaOperatore = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string CodiceFiscaleImpresa
        {
            get { return codiceFiscaleImpresa; }
            set { codiceFiscaleImpresa = value; }
        }

        public string PartitaIVA
        {
            get { return partitaIVA; }
            set { partitaIVA = value; }
        }

        public string IndirizzoSedeLegale
        {
            get { return indirizzoSedeLegale; }
            set { indirizzoSedeLegale = value; }
        }

        public string CapSedeLegale
        {
            get { return capSedeLegale; }
            set { capSedeLegale = value; }
        }

        public string LocalitaSedeLegale
        {
            get { return localitaSedeLegale; }
            set { localitaSedeLegale = value; }
        }

        public string ProvinciaSedeLegale
        {
            get { return provinciaSedeLegale; }
            set { provinciaSedeLegale = value; }
        }
    }
}