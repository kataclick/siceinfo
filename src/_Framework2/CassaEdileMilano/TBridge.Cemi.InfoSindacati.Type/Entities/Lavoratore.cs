using System;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class Lavoratore
    {
        private int idLavoratore;
        private string sindacato;
        private string cognome;
        private string nome;
        private string codiceFiscale;
        private DateTime dataNascita;
        private string indirizzoDenominazione;
        private string indirizzoCAP;
        private string indirizzoProvincia;
        private string indirizzoComune;
        private int idImpresa;
        private string ragioneSociale;
        private string tipoFineRapporto;

        public int IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public string Sindacato
        {
            get { return sindacato; }
            set { sindacato = value; }
        }

        public string Cognome
        {
            get { return cognome; }
            set { cognome = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public DateTime DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = value; }
        }

        public string IndirizzoDenominazione
        {
            get { return indirizzoDenominazione; }
            set { indirizzoDenominazione = value; }
        }

        public string IndirizzoCAP
        {
            get { return indirizzoCAP; }
            set { indirizzoCAP = value; }
        }

        public string IndirizzoProvincia
        {
            get { return indirizzoProvincia; }
            set { indirizzoProvincia = value; }
        }

        public string IndirizzoComune
        {
            get { return indirizzoComune; }
            set { indirizzoComune = value; }
        }

        public string IndirizzoCompleto
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoDenominazione) && !String.IsNullOrEmpty(indirizzoComune) &&
                    !String.IsNullOrEmpty(indirizzoCAP) && !String.IsNullOrEmpty(indirizzoProvincia))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoDenominazione, indirizzoComune, indirizzoCAP,
                                      indirizzoProvincia);
                return ret;
            }
        }

        public int IdImpresa
        {
            get { return idImpresa; }
            set { idImpresa = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string TipoFineRapporto
        {
            get
            {
                string ret = tipoFineRapporto;
                if (!string.IsNullOrEmpty(tipoFineRapporto))
                    ret = tipoFineRapporto.Substring(0, 3);
                return ret;
            }
            set { tipoFineRapporto = value; }
        }
    }
}