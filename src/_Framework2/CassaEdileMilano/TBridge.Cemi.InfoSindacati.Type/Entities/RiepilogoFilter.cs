using System;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class RiepilogoFilter
    {
        private string comprensorio;
        private DateTime dataIscrizioneDa;
        private DateTime dataIscrizioneA;

        public string Comprensorio
        {
            get { return comprensorio; }
            set { comprensorio = value; }
        }

        public DateTime DataIscrizioneDa
        {
            get { return dataIscrizioneDa; }
            set { dataIscrizioneDa = value; }
        }

        public DateTime DataIscrizioneA
        {
            get { return dataIscrizioneA; }
            set { dataIscrizioneA = value; }
        }
    }
}