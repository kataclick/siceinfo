using System;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class LavoratoreDettaglio : Lavoratore
    {
        private int anno;
        private string comprensorioSindacale;
        private DateTime dataIscrizioneSindacato;
        private string ultimoNrErogazione;
        private string annoUltimaErogazioneAPE;
        private DateTime dataRevocaTesseraSindacale;
        private string sindacatoPrecedente;
        private DateTime inizioPeriodo;
        private DateTime finePeriodo;
        private decimal imponibileSalariale;
        private decimal importoCartella;
        private decimal importoDelega;
        private string luogoNascita;
        private string sesso;
        private string codiceFiscaleImpresa;
        private string partitaIVA;
        private string tipoImpresa;
        private string codiceINAIL;
        private string codiceINPS;
        private string attivitaIstat;
        private string naturaGiuridica;
        private string indirizzoSedeLegale;
        private string capSedeLegale;
        private string localitaSedeLegale;
        private string provinciaSedeLegale;
        private string indirizzoSedeAmministrazione;
        private string capSedeAmministrazione;
        private string localitaSedeAmministrazione;
        private string provinciaSedeAmministrazione;
        private string telefonoSedeAmministrazione;
        private string faxSedeAmministrazione;
        private string eMailSedeAmministrazione;
        private string telefonoSedeLegale;
        private string faxSedeLegale;
        private string eMailSedeLegale;
        private DateTime dataInizioValiditaRapporto;
        private DateTime dataFineValiditaRapporto;
        private string tipoContratto;
        private string tipoCategoria;
        private string tipoQualifica;
        private string tipoMansione;
        private bool partTime;

        public int Anno
        {
            get { return anno; }
            set { anno = value; }
        }

        public string ComprensorioSindacale
        {
            get { return comprensorioSindacale; }
            set { comprensorioSindacale = value; }
        }

        public DateTime DataIscrizioneSindacato
        {
            get { return dataIscrizioneSindacato; }
            set { dataIscrizioneSindacato = value; }
        }

        public string UltimoNrErogazione
        {
            get { return ultimoNrErogazione; }
            set { ultimoNrErogazione = value; }
        }

        public string AnnoUltimaErogazioneAPE
        {
            get { return annoUltimaErogazioneAPE; }
            set { annoUltimaErogazioneAPE = value; }
        }

        public DateTime DataRevocaTesseraSindacale
        {
            get { return dataRevocaTesseraSindacale; }
            set { dataRevocaTesseraSindacale = value; }
        }

        public string SindacatoPrecedente
        {
            get { return sindacatoPrecedente; }
            set { sindacatoPrecedente = value; }
        }

        public DateTime InizioPeriodo
        {
            get { return inizioPeriodo; }
            set { inizioPeriodo = value; }
        }

        public DateTime FinePeriodo
        {
            get { return finePeriodo; }
            set { finePeriodo = value; }
        }

        public decimal ImponibileSalariale
        {
            get { return imponibileSalariale; }
            set { imponibileSalariale = value; }
        }

        public decimal ImportoCartella
        {
            get { return importoCartella; }
            set { importoCartella = value; }
        }

        public decimal ImportoDelega
        {
            get { return importoDelega; }
            set { importoDelega = value; }
        }

        public string LuogoNascita
        {
            get { return luogoNascita; }
            set { luogoNascita = value; }
        }

        public string Sesso
        {
            get { return sesso; }
            set { sesso = value; }
        }

        public string CodiceFiscaleImpresa
        {
            get { return codiceFiscaleImpresa; }
            set { codiceFiscaleImpresa = value; }
        }

        public string PartitaIVA
        {
            get { return partitaIVA; }
            set { partitaIVA = value; }
        }

        public string TipoImpresa
        {
            get { return tipoImpresa; }
            set { tipoImpresa = value; }
        }

        public string CodiceINAIL
        {
            get { return codiceINAIL; }
            set { codiceINAIL = value; }
        }

        public string CodiceINPS
        {
            get { return codiceINPS; }
            set { codiceINPS = value; }
        }

        public string AttivitaIstat
        {
            get { return attivitaIstat; }
            set { attivitaIstat = value; }
        }

        public string NaturaGiuridica
        {
            get { return naturaGiuridica; }
            set { naturaGiuridica = value; }
        }

        public string IndirizzoSedeLegale
        {
            get { return indirizzoSedeLegale; }
            set { indirizzoSedeLegale = value; }
        }

        public string CapSedeLegale
        {
            get { return capSedeLegale; }
            set { capSedeLegale = value; }
        }

        public string LocalitaSedeLegale
        {
            get { return localitaSedeLegale; }
            set { localitaSedeLegale = value; }
        }

        public string ProvinciaSedeLegale
        {
            get { return provinciaSedeLegale; }
            set { provinciaSedeLegale = value; }
        }

        public string IndirizzoCompletoSedeLegale
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoSedeLegale) && !String.IsNullOrEmpty(localitaSedeLegale) &&
                    !String.IsNullOrEmpty(capSedeLegale) && !String.IsNullOrEmpty(provinciaSedeLegale))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoSedeLegale, localitaSedeLegale, capSedeLegale,
                                      provinciaSedeLegale);
                return ret;
            }
        }

        public string IndirizzoSedeAmministrazione
        {
            get { return indirizzoSedeAmministrazione; }
            set { indirizzoSedeAmministrazione = value; }
        }

        public string CapSedeAmministrazione
        {
            get { return capSedeAmministrazione; }
            set { capSedeAmministrazione = value; }
        }

        public string LocalitaSedeAmministrazione
        {
            get { return localitaSedeAmministrazione; }
            set { localitaSedeAmministrazione = value; }
        }

        public string ProvinciaSedeAmministrazione
        {
            get { return provinciaSedeAmministrazione; }
            set { provinciaSedeAmministrazione = value; }
        }

        public string IndirizzoCompletoSedeAmministrativa
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoSedeAmministrazione) &&
                    !String.IsNullOrEmpty(localitaSedeAmministrazione) && !String.IsNullOrEmpty(capSedeAmministrazione) &&
                    !String.IsNullOrEmpty(provinciaSedeAmministrazione))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoSedeAmministrazione, localitaSedeAmministrazione,
                                      capSedeAmministrazione, provinciaSedeAmministrazione);
                return ret;
            }
        }

        public string TelefonoSedeAmministrazione
        {
            get { return telefonoSedeAmministrazione; }
            set { telefonoSedeAmministrazione = value; }
        }

        public string FaxSedeAmministrazione
        {
            get { return faxSedeAmministrazione; }
            set { faxSedeAmministrazione = value; }
        }

        public string EMailSedeAmministrazione
        {
            get { return eMailSedeAmministrazione; }
            set { eMailSedeAmministrazione = value; }
        }

        public string TelefonoSedeLegale
        {
            get { return telefonoSedeLegale; }
            set { telefonoSedeLegale = value; }
        }

        public string FaxSedeLegale
        {
            get { return faxSedeLegale; }
            set { faxSedeLegale = value; }
        }

        public string EMailSedeLegale
        {
            get { return eMailSedeLegale; }
            set { eMailSedeLegale = value; }
        }

        public DateTime DataInizioValiditaRapporto
        {
            get { return dataInizioValiditaRapporto; }
            set { dataInizioValiditaRapporto = value; }
        }

        public DateTime DataFineValiditaRapporto
        {
            get { return dataFineValiditaRapporto; }
            set { dataFineValiditaRapporto = value; }
        }

        public string TipoContratto
        {
            get { return tipoContratto; }
            set { tipoContratto = value; }
        }

        public string TipoCategoria
        {
            get { return tipoCategoria; }
            set { tipoCategoria = value; }
        }

        public string TipoQualifica
        {
            get { return tipoQualifica; }
            set { tipoQualifica = value; }
        }

        public string TipoMansione
        {
            get { return tipoMansione; }
            set { tipoMansione = value; }
        }

        public bool PartTime
        {
            get { return partTime; }
            set { partTime = value; }
        }
    }
}