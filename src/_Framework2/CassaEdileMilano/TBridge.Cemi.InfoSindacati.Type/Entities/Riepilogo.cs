namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class Riepilogo
    {
        private decimal imponibileSalariale;
        private decimal importoCartella;
        private decimal importoDelega;
        private int iscrittiCGIL;
        private int iscrittiCISL;
        private int iscrittiUIL;
        private int iscrittiNessuno;

        public decimal ImponibileSalariale
        {
            get { return imponibileSalariale; }
            set { imponibileSalariale = value; }
        }

        public decimal ImportoCartella
        {
            get { return importoCartella; }
            set { importoCartella = value; }
        }

        public decimal ImportoDelega
        {
            get { return importoDelega; }
            set { importoDelega = value; }
        }

        public int IscrittiCGIL
        {
            get { return iscrittiCGIL; }
            set { iscrittiCGIL = value; }
        }

        public int IscrittiCISL
        {
            get { return iscrittiCISL; }
            set { iscrittiCISL = value; }
        }

        public int IscrittiUIL
        {
            get { return iscrittiUIL; }
            set { iscrittiUIL = value; }
        }

        public int IscrittiNessuno
        {
            get { return iscrittiNessuno; }
            set { iscrittiNessuno = value; }
        }
    }
}