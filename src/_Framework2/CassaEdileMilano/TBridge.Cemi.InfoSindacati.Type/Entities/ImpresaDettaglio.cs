using System;

namespace TBridge.Cemi.InfoSindacati.Type.Entities
{
    public class ImpresaDettaglio : Impresa
    {
        private string statoAmministrativo;
        private DateTime dataSospensioneAttivita;
        private DateTime dataRipresaAttivita;
        private DateTime dataCessazioneAttivita;
        private string tipoImpresa;
        private string codiceINAIL;
        private string codiceINPS;
        private string attivitaIstat;
        private string naturaGiuridica;
        private string indirizzoSedeAmministrazione;
        private string capSedeAmministrazione;
        private string localitaSedeAmministrazione;
        private string provinciaSedeAmministrazione;
        private string telefonoSedeAmministrazione;
        private string faxSedeAmministrazione;
        private string eMailSedeAmministrazione;
        private string telefonoSedeLegale;
        private string faxSedeLegale;
        private string eMailSedeLegale;
        private DateTime dataIscrizione;
        private int idConsulente;
        private string ragioneSocialeConsulente;
        private int idLavoratore;
        private string rappresentanteLegale;
        private string telefonoConsulente;
        private string associazioneImprenditoriale;
        private int numeroDipendentiPartTime;

        public string StatoAmministrativo
        {
            get { return statoAmministrativo; }
            set { statoAmministrativo = value; }
        }

        public DateTime DataSospensioneAttivita
        {
            get { return dataSospensioneAttivita; }
            set { dataSospensioneAttivita = value; }
        }

        public DateTime DataRipresaAttivita
        {
            get { return dataRipresaAttivita; }
            set { dataRipresaAttivita = value; }
        }

        public DateTime DataCessazioneAttivita
        {
            get { return dataCessazioneAttivita; }
            set { dataCessazioneAttivita = value; }
        }

        public string TipoImpresa
        {
            get { return tipoImpresa; }
            set { tipoImpresa = value; }
        }

        public string CodiceINAIL
        {
            get { return codiceINAIL; }
            set { codiceINAIL = value; }
        }

        public string CodiceINPS
        {
            get { return codiceINPS; }
            set { codiceINPS = value; }
        }

        public string AttivitaIstat
        {
            get { return attivitaIstat; }
            set { attivitaIstat = value; }
        }

        public string NaturaGiuridica
        {
            get { return naturaGiuridica; }
            set { naturaGiuridica = value; }
        }

        public string IndirizzoSedeAmministrazione
        {
            get { return indirizzoSedeAmministrazione; }
            set { indirizzoSedeAmministrazione = value; }
        }

        public string CapSedeAmministrazione
        {
            get { return capSedeAmministrazione; }
            set { capSedeAmministrazione = value; }
        }

        public string LocalitaSedeAmministrazione
        {
            get { return localitaSedeAmministrazione; }
            set { localitaSedeAmministrazione = value; }
        }

        public string ProvinciaSedeAmministrazione
        {
            get { return provinciaSedeAmministrazione; }
            set { provinciaSedeAmministrazione = value; }
        }

        public string TelefonoSedeAmministrazione
        {
            get { return telefonoSedeAmministrazione; }
            set { telefonoSedeAmministrazione = value; }
        }

        public string IndirizzoCompletoSedeAmministrativa
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoSedeAmministrazione) &&
                    !String.IsNullOrEmpty(localitaSedeAmministrazione) && !String.IsNullOrEmpty(capSedeAmministrazione) &&
                    !String.IsNullOrEmpty(provinciaSedeAmministrazione))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoSedeAmministrazione, localitaSedeAmministrazione,
                                      capSedeAmministrazione, provinciaSedeAmministrazione);
                return ret;
            }
        }

        public string FaxSedeAmministrazione
        {
            get { return faxSedeAmministrazione; }
            set { faxSedeAmministrazione = value; }
        }

        public string EMailSedeAmministrazione
        {
            get { return eMailSedeAmministrazione; }
            set { eMailSedeAmministrazione = value; }
        }

        public string TelefonoSedeLegale
        {
            get { return telefonoSedeLegale; }
            set { telefonoSedeLegale = value; }
        }

        public string FaxSedeLegale
        {
            get { return faxSedeLegale; }
            set { faxSedeLegale = value; }
        }

        public string EMailSedeLegale
        {
            get { return eMailSedeLegale; }
            set { eMailSedeLegale = value; }
        }

        public DateTime DataIscrizione
        {
            get { return dataIscrizione; }
            set { dataIscrizione = value; }
        }

        public int IdConsulente
        {
            get { return idConsulente; }
            set { idConsulente = value; }
        }

        public string RagioneSocialeConsulente
        {
            get { return ragioneSocialeConsulente; }
            set { ragioneSocialeConsulente = value; }
        }

        public int IdLavoratore
        {
            get { return idLavoratore; }
            set { idLavoratore = value; }
        }

        public string RappresentanteLegale
        {
            get { return rappresentanteLegale; }
            set { rappresentanteLegale = value; }
        }

        public string TelefonoConsulente
        {
            get { return telefonoConsulente; }
            set { telefonoConsulente = value; }
        }

        public string AssociazioneImprenditoriale
        {
            get { return associazioneImprenditoriale; }
            set { associazioneImprenditoriale = value; }
        }

        public int NumeroDipendentiPartTime
        {
            get { return numeroDipendentiPartTime; }
            set { numeroDipendentiPartTime = value; }
        }
    }
}