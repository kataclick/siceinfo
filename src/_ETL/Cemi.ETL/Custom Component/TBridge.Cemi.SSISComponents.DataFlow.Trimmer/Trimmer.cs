﻿//RTrimPlus
//===========

//SSIS expression evaluator provides an RTRIM function, but it does not recognize the Japanese
//ideographic space.

//This transform takes a string or unicode column, and removes trailing spaces, whether ASCII, or 
//Japanese. 

//Configure in advanced editor by clicking input columns containing string values. usage type must 
//be set to READWRITE. 

//Included in this install is a file containing ideographic spaces. (spaces2trim.txt) Use a flat
//file source adapter to read from it. Make sure the code page is set to 932 (Japanese). In code page
//932, the ideographic space is represented by two bytes 0x81, 0x40. Run this data through the 
//RTrimPlus transform and see output without trailing spaces of either sort.

//Interesting Features
//====================

//This component is part of a series of components that illustrate increasingly complex 
//behavior, each one exercising a greater proportion of the SSIS object model. If studying 
//in order, this component follows UnDouble, and precedes ConfigureUnDouble. 

//This component was built to provide an introduction to component metadata validation. Also 
//illustrated are:

//- Use SetUsageType to validate selected input columns
//- What are orphaned input columns?
//- IsValid IsCorrupt IsBroken NeedsNewMetadata– when to use?
//- Pipeline data types – Issue: need runtime wrapper in using
//- What about changed upstream data types?
//- How to fire an error.
//- The design time interface
//- You are guaranteed a call to validate before execution, so don’t sweat validation in preexecute
//- Throwing and hresults
// 
// By James Howey
// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Runtime.Wrapper;
using Microsoft.SqlServer.Dts;
using Microsoft.SqlServer.Dts.ManagedMsg;

using System.Collections;
using System.Diagnostics;

namespace TBridge.Cemi.SSISComponents.DataFlow
{
    [DtsPipelineComponent(DisplayName = "Trimmer", ComponentType = ComponentType.Transform, Description = "Elimina gli spazi iniziali e finali dalle stringhe in input")]
    public class Trimmer : PipelineComponent
    {
        private void PostError(string message)
        {
            bool cancel = false;
            this.ComponentMetaData.FireError(0, this.ComponentMetaData.Name, message, "", 0, out cancel);
        }

        private DTSValidationStatus promoteStatus(ref DTSValidationStatus currentStatus, DTSValidationStatus newStatus)
        {
            // statuses are ranked in order of increasing severity, from
            //   valid to broken to needsnewmetadata to corrupt.
            // bad status, if any, is result of programming error
            switch (currentStatus)
            {
                case DTSValidationStatus.VS_ISVALID:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                            break;
                        default:
                            throw new System.ApplicationException("Errore interno al componente: è stato rilevato uno stato non gestibile.");
                    }
                    break;
                case DTSValidationStatus.VS_ISBROKEN:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                            break;
                        default:
                            throw new System.ApplicationException("Errore interno al componente: è stato rilevato uno stato non gestibile.");
                    }
                    break;
                case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                            currentStatus = newStatus;
                            break;
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            break;
                        default:
                            throw new System.ApplicationException("Errore interno al componente: è stato rilevato uno stato non gestibile.");
                    }
                    break;
                case DTSValidationStatus.VS_ISCORRUPT:
                    switch (newStatus)
                    {
                        case DTSValidationStatus.VS_ISCORRUPT:
                        case DTSValidationStatus.VS_ISVALID:
                        case DTSValidationStatus.VS_ISBROKEN:
                        case DTSValidationStatus.VS_NEEDSNEWMETADATA:
                            break;
                        default:
                            throw new System.ApplicationException("Errore interno al componente: è stato rilevato uno stato non gestibile.");
                    }
                    break;
                default:
                    throw new System.ApplicationException("Errore interno al componente: è stato rilevato uno stato non gestibile.");
            }
            return currentStatus;
        }

        public override DTSValidationStatus Validate()
        {
            // check for:
            //  basic layout validation
            //  orphaned input columns
            //  input columns must be text
            //  base validate behavior; return corrupt immediately;

            //  if component is corrupt, we are permitted to return without further checks
            DTSValidationStatus status = base.Validate();
            if (status == DTSValidationStatus.VS_ISCORRUPT)
            {
                return status;
            }

            // we are permitted to return corrupt only if it is impossible to arrive at
            // this state using design-time interfaces. This check therefore necessitates
            // an override of InsertInputAt and DeleteInput
            if (this.ComponentMetaData.InputCollection.Count != 1)
            {
                PostError("Il componente richiede un solo input");
                return DTSValidationStatus.VS_ISCORRUPT;
            }

            IDTSInput100 input = this.ComponentMetaData.InputCollection[0];
            IDTSInputColumnCollection100 inputColumns = input.InputColumnCollection;
            // no input columns selected is ok
            for (int j = 0; j < inputColumns.Count; j++)
            {
                IDTSInputColumn100 column = inputColumns[j];
                // if column has no upstream referent, base component validation has
                // already detected that and status is VS_NEEDSNEWMETADATA. There is no
                // need for us to continue validation efforts on this column.
                if (column.IsValid)
                {
                    // allow only string columns, ANSI or unicode
                    // you get in this state by changing an upstream data source type
                    // we opt to return broken, forcing user to either fix source, or 
                    // deselect column on this component. 
                    DataType dataType = column.DataType;
                    if (dataType != DataType.DT_STR && dataType != DataType.DT_WSTR)
                    {
                        promoteStatus(ref status, DTSValidationStatus.VS_ISBROKEN);
                        PostError("Il componente processa solamente colonne in input di tipo DT_STR e DT_WSTR. Deleseziona le colonne che hanno tipi non consentiti.");
                    }
                    // usage type must be UT_READWRITE. There is no way to get in a non-READWRITE
                    // state without hacking package source, so we call this corrupt.
                    DTSUsageType usageType = column.UsageType;
                    if (usageType != DTSUsageType.UT_READWRITE)
                    {
                        PostError("Le colonne in input devono essere impostate in modalità READWRITE.");
                        return DTSValidationStatus.VS_ISCORRUPT;
                    }
                }
            }
            return status;
        }

        public override IDTSInput100 InsertInput(DTSInsertPlacement insertPlacement, int inputID)
        {
            PostError("Il componente richiede un solo input.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDINPUT);
        }

        public override void DeleteInput(int inputID)
        {
            PostError("Il componente richiede un solo input. Eliminarlo non è consentito.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTDELETEINPUT);
        }

        public override IDTSOutput100 InsertOutput(DTSInsertPlacement insertPlacement, int outputID)
        {
            PostError("Il componente richiede un solo output.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDOUTPUT);
        }

        public override void DeleteOutput(int outputID)
        {
            PostError("Il componente richiede un solo output. Eliminarlo non è consentito.");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTDELETEOUTPUT);
        }

        public override IDTSOutputColumn100 InsertOutputColumnAt(int outputID, int outputColumnIndex, string name, string description)
        {
            // component allow deletion of output columns tho only hacked xml would get you in that state
            PostError("Non è consentito aggiungere colonne di output");
            throw new PipelineComponentHResultException(HResults.DTS_E_CANTADDCOLUMN);
        }

        public override IDTSInputColumn100 SetUsageType(int inputID, IDTSVirtualInput100 virtualInput, int lineageID, DTSUsageType usageType)
        {
            IDTSInputColumn100 inputColumn = null;
            switch (usageType)
            {
                case DTSUsageType.UT_READONLY:
                    PostError("Le colonne in input devono essere impostate in modalità READWRITE.");
                    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
                case DTSUsageType.UT_READWRITE:
                    IDTSVirtualInputColumn100 column = virtualInput.VirtualInputColumnCollection.GetVirtualInputColumnByLineageID(lineageID);
                    if (column.DataType != DataType.DT_STR && column.DataType != DataType.DT_WSTR)
                    {
                        PostError("Il componente processa solamente colonne in input di tipo DT_STR e DT_WSTR");
                        throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
                    }
                    else
                    {
                        // no need to set error dispositions as default is "not used".
                        // no custom properties to add for this component
                        inputColumn = base.SetUsageType(inputID, virtualInput, lineageID, usageType);
                        return inputColumn;
                    }
                case DTSUsageType.UT_IGNORED:
                    // this is the delete input column
                    inputColumn = base.SetUsageType(inputID, virtualInput, lineageID, usageType);
                    return inputColumn;
                default:
                    throw new PipelineComponentHResultException(HResults.DTS_E_CANTSETUSAGETYPE);
            }
        }

        private int[] columnId;
        //private char[] workArea;
        private int numberOfStrings;

        
        public override void PreExecute()
        {
            IDTSInput100 input = this.ComponentMetaData.InputCollection[0];
            int bufferId = input.Buffer;
            IDTSInputColumnCollection100 inputColumnCollection = input.InputColumnCollection;
            numberOfStrings = inputColumnCollection.Count;
            columnId = new int[numberOfStrings];
            int longestString = 0;
            for (int j = 0; j < numberOfStrings; j++)
            {
                IDTSInputColumn100 column = inputColumnCollection[j];
                int lineageId = column.LineageID;
                columnId[j] = this.BufferManager.FindColumnByLineageID(bufferId, lineageId);
                longestString = column.Length > longestString ? column.Length : longestString;
            }
            //workArea = new char[longestString];
        }

        public override void ProcessInput(int inputID, PipelineBuffer buffer)
        {
            string result;
            string source;
            if (!buffer.EndOfRowset)
            {
                while (buffer.NextRow())
                {
                    for (int j = 0; j < numberOfStrings; j++)
                    {
                        source = buffer.GetString(columnId[j]);
                        if (source != null)
                        {
                            result = TrimString(source);
                            if (result.Length != 0)
                                buffer.SetString(columnId[j], result);
                            else buffer.SetNull(columnId[j]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Vengono eliminati gli spazi
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private string TrimString(string source)
        {
            return source.TrimEnd(' ').TrimStart(' ');
            //throw new Exception("The method or operation is not implemented.");
        }

        // Spaces we might trim
        // We will content ourselves with the the ansi and japanese ones, i.e. 0020 and 3000

        // U+0020 SPACE 
        // U+00A0 NO-BREAK SPACE
        // U+2000 EN QUAD
        // U+2001 EM QUAD
        // U+2002 EN SPACE
        // U+2003 EM SPACE
        // U+2004 THREE-PER-EM SPACE
        // U+2005 FOUR-PER-EM SPACE
        // U+2006 SIX-PER-EM SPACE
        // U+2007 FIGURE SPACE
        // U+2008 PUNCTUATION SPACE
        // U+2009 THIN SPACE
        // U+200A HAIR SPACE
        // U+200B ZERO WIDTH SPACE
        // U+202F NARROW NO-BREAK SPACE
        // U+205F MEDIUM MATHEMATICAL SPACE
        // U+3000 IDEOGRAPHIC SPACE
        // U+FEFF ZERO WIDTH NO-BREAK SPACE

	
        //public string ProcessString(char[] target, string source)
        //{
        //    int j;
        //    for (j = source.Length - 1; j >= 0; j--)
        //    {
        //        char c = source[j];
        //        if (!(c == '\x0020' || c == '\x3000'))
        //        {
        //            break;
        //        }
        //    }
        //    int length = j + 1;
        //    for (; j >= 0; j-- )
        //    {
        //        target[j] = source[j];
        //    }
        //    return new string(target, 0, length);
        //}
    }
}