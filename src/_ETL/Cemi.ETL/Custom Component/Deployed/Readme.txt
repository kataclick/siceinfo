Ho incluso le dll dei componenti e il cmd per registrarle. 

Il cmd oltre ad installare le librerie nella gac li copia nella directory pipeline di SQL, 
quindi controllate di avere i diritti di accesso e che il percorso sia corretto.

Le librerie da installare sono quelle presenti in questa directory e non le dll dei progetti presenti nella solution. questo perch� ogni 
ssis package fa riferimento a quelle versioni. Usarne altri comporta modifiche ai package di etl.

Ricordatevi di aprire il path per fare in modo di poter lanciare gacutil da qualsiasi directory. 

Nel caso abbiate Vista, lanciate la shell con diritti si amministratore ;-)