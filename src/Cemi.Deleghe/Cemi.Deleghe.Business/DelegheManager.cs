﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Cemi.Deleghe.Business.SmsServiceReference;
using Cemi.Deleghe.Business.WordServiceReference;
using Cemi.Deleghe.Data;
using Cemi.Deleghe.Type;

namespace Cemi.Deleghe.Business
{
    public class DelegheManager
    {
        readonly Provider _provider = new Provider();

        private static readonly string LogDeleghe = ConfigurationManager.AppSettings["LogDeleghe"];

        public List<Delega> GetDeleghe()
        {
            return _provider.GetDeleghe();
        }

        public List<Delega> GetDelegheSms()
        {
            return _provider.GetDelegheSms();
        }

        public List<Delega> GetDelegheById(int idDelega)
        {
            return _provider.GetDelegheById(idDelega);
        }

        public bool InviaSms(List<Delega> listaDelegheSms)
        {
            bool ret = false;

            try
            {
                SmsServiceClient client = new SmsServiceClient();

                List<SmsDaInviare> listaSms = new List<SmsDaInviare>();

                foreach (Delega delega in listaDelegheSms)
                {
                    SmsDaInviare sms = new SmsDaInviare();
                    sms.Data = DateTime.Today.AddDays(1).AddHours(10);
                    sms.Testo =
                        String.Format(
                            "Cassa Edile di Milano La informa che è stata attivata la Sua delega al sindacato {0}",
                            delega.Sindacato);
                    sms.Destinatari = new SmsDaInviare.Destinatario[1];
                    sms.Destinatari[0] = new SmsDaInviare.Destinatario();
                    sms.Destinatari[0].IdUtenteSiceInfo = delega.IdUtente;
                    sms.Destinatari[0].Numero = delega.Cellulare;
                    sms.Tipologia = TipologiaSms.Deleghe;

                    listaSms.Add(sms);
                }

                if (listaSms.Count > 0)
                {
                    ret = client.InviaListaSms(listaSms.ToArray());
                }
            }
            catch (Exception exception)
            {
                if (LogDeleghe == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione Deleghe SMS: {0} - {1}", exception.Message, exception.InnerException));
                    }
                }
            }


            return ret;
        }

        public bool GeneraLettere(List<Delega> listaDelegheLettere, string pathTemplateLettere, string pathLettere, string pathLetterePdf, string pathLog)
        {
            bool ret = false;

            PdfManager pdfManager = new PdfManager();
            ArchidocConnector archidocConnector = new ArchidocConnector();
            List<LogAnomalia> listaAnomalie = new List<LogAnomalia>();

            try
            {
                //svuoto le cartelle
                DirectoryInfo directoryInfoWord = new DirectoryInfo(pathLettere);
                foreach (FileInfo fileInfo in directoryInfoWord.GetFiles())
                {
                    fileInfo.Delete();
                }

                DirectoryInfo directoryInfoPdf = new DirectoryInfo(pathLetterePdf);
                foreach (FileInfo fileInfo in directoryInfoPdf.GetFiles())
                {
                    fileInfo.Delete();
                }

                WordServiceSoapClient client = new WordServiceSoapClient();

                foreach (Delega delega in listaDelegheLettere)
                {
                    List<WordField> listaCampi = new List<WordField>();

                    WordField wfDataLettera = new WordField { Campo = "DataLettera", Valore = DateTime.Today.ToShortDateString() };
                    //WordField wfProtocollo = new WordField {Campo = "Protocollo", Valore = "???"};
                    WordField wfCodiceCe = new WordField { Campo = "CodiceCe", Valore = delega.IdLavoratore.ToString(CultureInfo.InvariantCulture) };
                    WordField wfCognomeNome = new WordField { Campo = "CognomeNome", Valore = String.Format("{0} {1}", delega.Cognome, delega.Nome) };
                    WordField wfIndirizzo = new WordField { Campo = "Indirizzo", Valore = delega.Residenza.Via };
                    WordField wfCap = new WordField { Campo = "Cap", Valore = delega.Residenza.Cap };
                    WordField wfComune = new WordField { Campo = "Comune", Valore = delega.Residenza.Comune };
                    WordField wfProvincia = new WordField { Campo = "Provincia", Valore = delega.Residenza.Provincia };
                    WordField wfSindacato = new WordField { Campo = "Sindacato", Valore = delega.Sindacato };

                    listaCampi.Add(wfDataLettera);
                    //listaCampi.Add(wfProtocollo);
                    listaCampi.Add(wfCodiceCe);
                    listaCampi.Add(wfCognomeNome);
                    listaCampi.Add(wfIndirizzo);
                    listaCampi.Add(wfCap);
                    listaCampi.Add(wfComune);
                    listaCampi.Add(wfProvincia);
                    listaCampi.Add(wfSindacato);

                    string wordGeneratedPath = client.CreateWord(pathTemplateLettere, listaCampi.ToArray(), pathLettere);

                    Stream pdfStream = pdfManager.ConvertWord2Pdf(wordGeneratedPath);

                    string fileName = String.Format("{0}Lettera-{1}.pdf", pathLetterePdf, delega.IdLavoratore);

                    Stream archidocFirstPage = null;

                    //archidocFirstPage = pdfManager.GetPdfPage(@"C:\Users\alessio.mosto\Desktop\Deleghe\Esempio delega.pdf", 0);

                    if (!String.IsNullOrEmpty(delega.IdArchidoc))
                    {
                        byte[] archidocDocument = archidocConnector.GetDocument(delega.IdArchidoc);
                        if (archidocDocument != null && archidocDocument.Length > 0)
                        {
                            Stream archidocDocumentStream = pdfManager.GetByteStream(archidocDocument);
                            archidocFirstPage = pdfManager.GetPdfPage(archidocDocumentStream, 0);

                            pdfManager.MergePdf(pdfStream, archidocFirstPage, fileName);

                            byte[] pdfByte = null;

                            using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
                            {
                                pdfByte = new byte[fileStream.Length];
                                fileStream.Read(pdfByte, 0, (int) fileStream.Length);
                                fileStream.Flush();
                                fileStream.Close();
                            }

                            string nomeAllegato = String.Format("Lettera-{0}.pdf", delega.IdLavoratore);
                            archidocConnector.InsertAllegatoEsterno(delega.IdArchidoc, "Lettera Delega", nomeAllegato, pdfByte);

                            _provider.AggiornaDelegaAllegato(delega.IdDelega, nomeAllegato);
                        }
                        else
                        {
                            //traccio l'anomalia...
                            listaAnomalie.Add(new LogAnomalia
                                {
                                    IdDelega = delega.IdDelega,
                                    IdLavoratore = delega.IdLavoratore,
                                    IdArchidoc = delega.IdArchidoc,
                                    Cognome = delega.Cognome,
                                    Nome = delega.Nome
                                });
                        }
                    }
                }

                //salvo file di log
                using (StreamWriter sw = File.CreateText(pathLog))
                {
                    foreach (LogAnomalia logAnomalia in listaAnomalie)
                    {
                        sw.WriteLine(logAnomalia.ToString());
                    }

                    sw.Flush();
                    sw.Close();
                }

                ret = true;
            }
            catch (Exception exception)
            {
                if (LogDeleghe == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione Deleghe Lettere: {0} - {1}", exception.Message, exception.InnerException));
                    }
                }
            }

            return ret;
        }

        public bool GeneraLetteraSpecifica(Delega delega)
        {
            bool ret = false;

            string pathLettere = ConfigurationManager.AppSettings["PathLettere"];
            string pathLetterePdf = ConfigurationManager.AppSettings["PathLetterePdf"];
            string pathTemplateLettere = ConfigurationManager.AppSettings["PathTemplateLettere"];
            string pathLog = ConfigurationManager.AppSettings["PathLog"];

            PdfManager pdfManager = new PdfManager();
            Business.ArchidocConnector archidocConnector = new Business.ArchidocConnector();

            try
            {
                WordServiceSoapClient client = new WordServiceSoapClient();

                List<WordField> listaCampi = new List<WordField>();

                WordField wfDataLettera = new WordField { Campo = "DataLettera", Valore = DateTime.Today.ToShortDateString() };
                //WordField wfProtocollo = new WordField {Campo = "Protocollo", Valore = "???"};
                WordField wfCodiceCe = new WordField { Campo = "CodiceCe", Valore = delega.IdLavoratore.ToString(CultureInfo.InvariantCulture) };
                WordField wfCognomeNome = new WordField { Campo = "CognomeNome", Valore = $"{delega.Cognome} {delega.Nome}" };
                WordField wfIndirizzo = new WordField { Campo = "Indirizzo", Valore = delega.Residenza.Via };
                WordField wfCap = new WordField { Campo = "Cap", Valore = delega.Residenza.Cap };
                WordField wfComune = new WordField { Campo = "Comune", Valore = delega.Residenza.Comune };
                WordField wfProvincia = new WordField { Campo = "Provincia", Valore = delega.Residenza.Provincia };
                WordField wfSindacato = new WordField { Campo = "Sindacato", Valore = delega.Sindacato };

                listaCampi.Add(wfDataLettera);
                //listaCampi.Add(wfProtocollo);
                listaCampi.Add(wfCodiceCe);
                listaCampi.Add(wfCognomeNome);
                listaCampi.Add(wfIndirizzo);
                listaCampi.Add(wfCap);
                listaCampi.Add(wfComune);
                listaCampi.Add(wfProvincia);
                listaCampi.Add(wfSindacato);

                string wordGeneratedPath = client.CreateWord(pathTemplateLettere, listaCampi.ToArray(), pathLettere);

                //var pdfFromWord = pdfManager.ConvertWord2PdfTelerik(wordGeneratedPath);
                //Stream pdfStream = new MemoryStream(pdfFromWord);
                var pdfStream = pdfManager.ConvertWord2Pdf(wordGeneratedPath);


                string fileName = $"{pathLetterePdf}Lettera-{delega.IdLavoratore}_TIF.pdf";

                Stream archidocFirstPage = null;

                //archidocFirstPage = pdfManager.GetPdfPage($@".\DelegheToFix\{delega.IdLavoratore}.tif", 0);

                #region Prova marco

                if (!String.IsNullOrEmpty(delega.IdArchidoc))
                {
                    byte[] archidocDocument = archidocConnector.GetDocument(delega.IdArchidoc);
                    if (archidocDocument != null && archidocDocument.Length > 0)
                    {
                        Stream archidocDocumentStream = pdfManager.GetByteStream(archidocDocument);
                        archidocFirstPage = pdfManager.GetPdfPage(archidocDocumentStream, 0);

                        pdfManager.MergePdf(pdfStream, archidocFirstPage, fileName);

                        //byte[] pdfByte = null;

                        //using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
                        //{
                        //    pdfByte = new byte[fileStream.Length];
                        //    fileStream.Read(pdfByte, 0, (int)fileStream.Length);
                        //    fileStream.Flush();
                        //    fileStream.Close();
                        //}

                        //string nomeAllegato = String.Format("Lettera-{0}.pdf", delega.IdLavoratore);
                        //archidocConnector.InsertAllegatoEsterno(delega.IdArchidoc, "Lettera Delega", nomeAllegato, pdfByte);

                        //_provider.AggiornaDelegaAllegato(delega.IdDelega, nomeAllegato);
                    }                    
                }

                fileName = $"{pathLetterePdf}Lettera-{delega.IdLavoratore}.pdf";

                archidocFirstPage = null;

                //archidocFirstPage = pdfManager.GetPdfPage($@".\DelegheToFix\{delega.IdLavoratore}.tif", 0);

                string delegaPdfPath = $@".\DelegheToFix\{delega.IdLavoratore}.pdf";

                if (File.Exists(delegaPdfPath))
                {
                    archidocFirstPage = pdfManager.GetPdfPage(delegaPdfPath, 0);
                    pdfManager.MergePdf(pdfStream, archidocFirstPage, fileName);
                }

                #endregion

                ret = true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //throw;
            }
            return ret;
        }

        public List<string> GeneraLettereUnicoPdf(string pathLetterePdf, int numeroPdfMassimo)
        {
            List<string> listaPdfUniciGenerati = new List<string>();

            PdfManager pdfManager = new PdfManager();

            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(pathLetterePdf);

                FileInfo[] fileInfo = directoryInfo.GetFiles("*.pdf");

                List<List<FileInfo>> lista = fileInfo.Select((x, i) => new { Index = i, Value = x }).GroupBy(x => x.Index / numeroPdfMassimo).Select(x => x.Select(v => v.Value).ToList()).ToList();

                for (int i = 0; i < lista.Count; i++)
                {
                    List<FileInfo> listaFile = lista[i];
                    string pathFileUnico = string.Format("{0}LetteraUnica-{1}.pdf", pathLetterePdf, i);
                    listaFile[0].CopyTo(pathFileUnico);

                    for (int j = 1; j < listaFile.Count; j++)
                    {
                        pdfManager.MergePdf(pathFileUnico, listaFile[j].FullName, pathFileUnico);
                    }

                    listaPdfUniciGenerati.Add(pathFileUnico);
                }
            }
            catch (Exception exception)
            {
                if (LogDeleghe == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione Deleghe Unico PDF: {0} - {1}", exception.Message, exception.InnerException));
                    }
                }
            }

            return listaPdfUniciGenerati;
        }

        public bool EliminaAllegati(List<Delega> listaDeleghe)
        {
            bool ret = false;

            ArchidocConnector archidocConnector = new ArchidocConnector();

            try
            {
                foreach (Delega delega in listaDeleghe)
                {
                    archidocConnector.DeleteAllegato(delega.IdArchidoc, delega.NomeAllegatoLettera);

                    _provider.AggiornaDelegaAllegato(delega.IdDelega, null);
                }

                ret = true;
            }
            catch (Exception exception)
            {
                if (LogDeleghe == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione Deleghe Cancellazione: {0} - {1}", exception.Message, exception.InnerException));
                    }
                }
            }

            return ret;
        }
    }
}
