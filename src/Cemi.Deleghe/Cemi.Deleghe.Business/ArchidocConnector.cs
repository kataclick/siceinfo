﻿using System;
using System.Configuration;
using Cemi.Deleghe.Business.ArchidocServiceReference;
using Cemi.Deleghe.Business.ArchidocStdServiceReference;

namespace Cemi.Deleghe.Business
{
    class ArchidocConnector
    {
        private String ArchidocServer { set; get; }
        private String ArchidocMainEm { set; get; }
        private String ArchidocGpEm { set; get; }
        private String ArchidocDatabase { set; get; }
        private String ArchidocUsername { set; get; }
        private String ArchidocPassword { set; get; }

        public ArchidocConnector()
        {
            ArchidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
            ArchidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
            ArchidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
            ArchidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
            ArchidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
            ArchidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);
        }

        public byte[] GetDocument(string guid, Boolean tiffToPdf)
        {
            String documentExtention;
            Byte[] document;
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                String documentName;
                document = archidocServiceClient.GetDocumentByGUID(ArchidocServer, ArchidocDatabase, ArchidocGpEm, ArchidocMainEm, ArchidocUsername, ArchidocPassword, guid, out documentName, out documentExtention);
            }

            if (tiffToPdf && document != null)
            {
                TiffToPdfConverter converter = new TiffToPdfConverter();
                if ((documentExtention.Equals("tif", StringComparison.CurrentCultureIgnoreCase)
                    || documentExtention.Equals("tiff", StringComparison.CurrentCultureIgnoreCase))
                    && converter.IsTiff(document))
                {
                    //TODO converti
                    document = converter.TiffToPdf(document);
                }
            }

            return document;
        }

        public byte[] GetDocument(string guid)
        {
            return GetDocument(guid, true);
        }

        //public byte[] GetDocument(string guid)
        //{
        //    SvAolWSServiceSoapClient archidocService = new SvAolWSServiceSoapClient();
        //    svwsstSession archidocSession = new svwsstSession();
        //    string sessione = string.Empty;

        //    try
        //    {
        //        #region LOGIN

        //        archidocSession.Language = svwsLanguage.svwsLgItalian;

        //        sessione = archidocService.Login(ArchidocUsername, ArchidocPassword, ref archidocSession);

        //        #endregion

        //        svwsstDocument doc = archidocService.Card_Document(sessione, guid, 0, true, true, true);

        //        archidocService.Logout(sessione);

        //        #region to file
        //        /*
        //        string fileName = "pippo.tif";
        //        string filePathComplete = @"c:\temp\" + fileName;

        //        System.IO.File.CreateText(filePathComplete).Close();

        //        FileStream fs = new FileStream(filePathComplete, FileMode.OpenOrCreate, FileAccess.Write);
        //        fs.Write(doc.DocumentFile, 0, doc.DocumentFile.Length);
        //        fs.Close();
                
        //        //System.Diagnostics.Process.Start(filePathComplete);
        //        */

        //        #endregion

        //        return doc.DocumentFile;
        //    }
        //    catch (Exception ex)
        //    {
        //        archidocService.Logout(sessione);
        //        throw;
        //    }
        //}

        public void InsertAllegatoEsterno(String cardGuid, String note, String nomeFile, Byte[] fileByteArray)
        {
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                archidocServiceClient.InsertAttachment(ArchidocServer, ArchidocDatabase, ArchidocGpEm, ArchidocMainEm, ArchidocUsername, ArchidocPassword, cardGuid, note, fileByteArray, nomeFile);
            }
        }

        public void DeleteAllegato(String cardGuid, String nomeAllegato)
        {
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                archidocServiceClient.DeleteAttachment(ArchidocUsername, ArchidocPassword, cardGuid, nomeAllegato);
            }
        }
    }
}
