﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Threading;
using iTextSharp.text.pdf;
using Microsoft.Office.Interop.Word;
using PdfSharp.Pdf.IO;
using Telerik.Windows.Documents.Common.FormatProviders;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using Telerik.Windows.Documents.Flow.Model;
using PdfDocument = PdfSharp.Pdf.PdfDocument;
using PdfPage = PdfSharp.Pdf.PdfPage;
using PdfReader = PdfSharp.Pdf.IO.PdfReader;

namespace Cemi.Deleghe.Business
{
    public class PdfManager
    {
        public Stream ConvertWord2Pdf(string wordFilePath)
        {
            // Create a new Microsoft Word application object
            Application word = new Application();

            // C# doesn't have optional arguments so we'll need a dummy value
            object oMissing = Missing.Value;

            word.Visible = false;
            word.ScreenUpdating = false;

            // Cast as Object for word Open method
            Object filename = wordFilePath;

            // Use the dummy value as a placeholder for optional arguments
            Document doc = word.Documents.Open(ref filename, ref oMissing,
                                               ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                                               ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                                               ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            doc.Activate();
            
            object outputFileName = wordFilePath.Replace(".docx", ".pdf");
            object fileFormat = WdSaveFormat.wdFormatPDF;

            // Save document into PDF Format
            doc.SaveAs(ref outputFileName,
                       ref fileFormat, ref oMissing, ref oMissing,
                       ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                       ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                       ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Close the Word document, but leave the Word application open.
            // doc has to be cast to type _Document so that it will find the
            // correct Close method.
            object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
            (doc).Close(ref saveChanges, ref oMissing, ref oMissing);
            doc = null;


            // word has to be cast to type _Application so that it will find
            // the correct Quit method.
            word.NormalTemplate.Saved = true; 
            (word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;

            Stream ms = GetFileStream((string) outputFileName);
            if (File.Exists((string) outputFileName))
                File.Delete((string) outputFileName);

            //Do il tempo a word di chiudersi...
            Thread.Sleep(100);

            return ms;


            //var wordApplication = new Microsoft.Office.Interop.Word.Application();
            //try
            //{
            //    Microsoft.Office.Interop.Word.Document wordDocument = wordApplication.Documents.Open(wordFilePath);

            //    if (wordDocument != null)
            //        wordDocument.SaveAs(string.Format(@"{0}\temp.pdf", Path.GetTempPath()), Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF);
            //    else
            //        throw new Exception("CUSTOM ERROR: Cannot Open Word Application");
            //    wordDocument.Close();
            //    wordApplication.Quit();

            //    Stream ms = GetFileStream(string.Format(@"{0}\temp.pdf", Path.GetTempPath()));
            //    if (File.Exists(string.Format(@"{0}\temp.pdf", Path.GetTempPath())))
            //        File.Delete(string.Format(@"{0}\temp.pdf", Path.GetTempPath()));
            //    return ms;
            //}
            //catch (Exception ex)
            //{
            //    wordApplication.Quit();
            //    throw;
            //}
        }

        public byte[] ConvertWord2PdfTelerik(string wordFilePath)
        {
            MemoryStream ms = new MemoryStream();
            FileStream fileStream = new FileStream(wordFilePath, FileMode.Open);
            fileStream.CopyTo(ms);
            return GetPdfByWordWithTelerik(ms);
        }

        public static byte[] GetPdfByWordWithTelerik(MemoryStream wordMemoryStream)
        {
            IFormatProvider<RadFlowDocument> docxFormatProvider = new DocxFormatProvider();
            RadFlowDocument document = docxFormatProvider.Import(wordMemoryStream);

            IFormatProvider<RadFlowDocument> pdfFormatProvider = new PdfFormatProvider();

            using (var stream = new MemoryStream())
            {
                pdfFormatProvider.Export(document, stream);
                return stream.ToArray();
            }
        }

        public static byte[] MergePdf(List<byte[]> pdfByteContent)
        {
            using (var ms = new MemoryStream())
            {
                using (var doc = new iTextSharp.text.Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {
                            //Create a PdfReader bound to that byte array
                            using (var reader = new iTextSharp.text.pdf.PdfReader(p))
                            {
                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                            }
                        }

                        doc.Close();
                    }
                }

                //Return just before disposing
                return ms.ToArray();
            }
        }

        public Stream MergePdf(Stream mainPdf, Stream childPdf)
        {
            MemoryStream output = new MemoryStream();
            PdfDocument outputDocument = PdfReader.Open(mainPdf, PdfDocumentOpenMode.Modify);
            // Open the document to import pages from it.
            if (childPdf != null)
            {
                PdfDocument inputDocument = PdfReader.Open(childPdf, PdfDocumentOpenMode.Import);
                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            outputDocument.Save(output, false);
            byte[] buffer = new byte[output.Length];
            output.Seek(0, SeekOrigin.Begin);
            output.Flush();
            output.Read(buffer, 0, (int) output.Length);
            return output;
        }

        public void MergePdf(Stream mainPdf, Stream childPdf, string pathFileDest)
        {
            PdfDocument outputDocument = PdfReader.Open(mainPdf, PdfDocumentOpenMode.Modify);
            // Open the document to import pages from it.
            if (childPdf != null)
            {
                PdfDocument inputDocument = PdfReader.Open(childPdf, PdfDocumentOpenMode.Import);
                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            outputDocument.Save(pathFileDest);
        }

        public Stream MergePdf(string mainPdf, string childPdf)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            return MergePdf(main, child);
        }

        public void MergePdf(string mainPdf, string childPdf, string pathFileDest)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            MergePdf(main, child, pathFileDest);
        }

        public Stream GetPdfPage(Stream pdf, int pageIndex)
        {
            PdfDocument outputDocument = new PdfDocument();
            PdfDocument inputDocument = PdfReader.Open(pdf, PdfDocumentOpenMode.Import);
            if (inputDocument.PageCount < pageIndex || pageIndex < 0)
                return null;

            // Get the page from the external document...
            PdfPage page = inputDocument.Pages[pageIndex];
            // ...and add it to the output document.
            outputDocument.AddPage(page);

            MemoryStream output = new MemoryStream();
            outputDocument.Save(output, false);
            //byte[] buffer = new byte[output.Length];
            output.Seek(0, SeekOrigin.Begin);
            //output.Flush();
            //output.Read(buffer, 0, (int) output.Length);
            return output;
        }

        public Stream GetPdfPage(string pdf, int pageIndex)
        {
            Stream pdfStream = GetFileStream(pdf);
            return GetPdfPage(pdfStream, pageIndex);
        }

        public Stream GetFileStream(string filename)
        {
            MemoryStream ms = new MemoryStream();
            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] bytes = new byte[file.Length];
            file.Read(bytes, 0, (int) file.Length);
            ms.Write(bytes, 0, (int) file.Length);
            file.Close();
            return ms;
        }

        public Stream GetByteStream(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            return ms;
        }

        public static byte[] ImageToPdf(byte[] image)
        {
            if (image == null || image.Length == 0)
            {
                return null;
            }

            Bitmap bitmap = null;

            try
            {
                using (var msB = new MemoryStream())
                {
                    msB.Write(image, 0, image.Length);
                    try
                    {
                        bitmap = (Bitmap) System.Drawing.Image.FromStream(msB);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }


                    byte[] result = null;

                    iTextSharp.text.Document newDoc = new iTextSharp.text.Document();
                    iTextSharp.text.Document.Compress = true;

                    using (var ms = new MemoryStream())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(newDoc, ms);
                        writer.CompressionLevel = PdfStream.BEST_COMPRESSION;
                        newDoc.Open();

                        PdfContentByte cb = writer.DirectContent;

                        int count = bitmap.GetFrameCount(FrameDimension.Page);

                        for (int idx = 0; idx < count; idx++)
                        {
                            bitmap.SelectActiveFrame(FrameDimension.Page, idx);

                            using (MemoryStream frameStream = new MemoryStream())
                            {
                                // save each frame to a bytestream                                                
                                bitmap.Save(frameStream, ImageFormat.Tiff);

                                // and then create a new Image from it
                                var pageImage = System.Drawing.Image.FromStream(frameStream);

                                var rectangle =
                                    new iTextSharp.text.Rectangle(0, 0, pageImage.Width, pageImage.Height);
                                newDoc.SetPageSize(rectangle);

                                newDoc.NewPage();

                                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(pageImage,
                                    System.Drawing.Imaging.ImageFormat.Tiff);
                                img.SetAbsolutePosition(0, 0);

                                cb.AddImage(img);

                                pageImage = null;
                            }
                        }

                        newDoc.Close();

                        return ms.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}