﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Cemi.Deleghe.Type;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.Deleghe.Data
{
    public class Provider
    {
        public Provider()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public List<Delega> GetDeleghe()
        {
            List<Delega> listaDeleghe = new List<Delega>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectSmsLettere"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Delega delega = new Delega();

                        delega.IdDelega = (int) reader["idDelega"];
                        delega.IdLavoratore = (int) reader["idLavoratore"];
                        delega.Sindacato = (string) reader["sindacato"];
                        if (reader["numero"] != DBNull.Value)
                            delega.Cellulare = (string) reader["numero"];
                        delega.IdUtente = (int) reader["idUtente"];
                        if (reader["cognome"] != DBNull.Value)
                            delega.Cognome = (string) reader["cognome"];
                        if (reader["nome"] != DBNull.Value)
                            delega.Nome = (string) reader["nome"];
                        delega.Residenza = new Indirizzo();
                        if (reader["indirizzoCAP"] != DBNull.Value)
                            delega.Residenza.Cap = (string) reader["indirizzoCAP"];
                        if (reader["indirizzoComune"] != DBNull.Value)
                            delega.Residenza.Comune = (string) reader["indirizzoComune"];
                        if (reader["indirizzoProvincia"] != DBNull.Value)
                            delega.Residenza.Provincia = (string) reader["indirizzoProvincia"];
                        if (reader["indirizzoDenominazione"] != DBNull.Value)
                            delega.Residenza.NomeVia = (string) reader["indirizzoDenominazione"];
                        if (reader["idArchidoc"] != DBNull.Value)
                            delega.IdArchidoc = (string) reader["idArchidoc"];
                        if (reader["nomeAllegatoLettera"] != DBNull.Value)
                            delega.NomeAllegatoLettera = (string)reader["nomeAllegatoLettera"];

                        listaDeleghe.Add(delega);
                    }
                }
            }

            return listaDeleghe;
        }

        public List<Delega> GetDelegheSms()
        {
            List<Delega> listaDeleghe = new List<Delega>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheInvioSms"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Delega delega = new Delega();

                        delega.Sindacato = (string)reader["sindacato"];
                        if (reader["cellulare"] != DBNull.Value)
                            delega.Cellulare = (string)reader["cellulare"];
                        delega.IdUtente = (int)reader["idUtente"];

                        listaDeleghe.Add(delega);
                    }
                }
            }

            return listaDeleghe;
        }

        public List<Delega> GetDelegheById(int idDelega)
        {
            List<Delega> listaDeleghe = new List<Delega>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DelegheSelectSmsLettereById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, idDelega);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Delega delega = new Delega();

                        delega.IdDelega = (int)reader["idDelega"];
                        delega.IdLavoratore = (int)reader["idLavoratore"];
                        delega.Sindacato = (string)reader["sindacato"];
                        if (reader["numero"] != DBNull.Value)
                            delega.Cellulare = (string)reader["numero"];
                        delega.IdUtente = (int)reader["idUtente"];
                        if (reader["cognome"] != DBNull.Value)
                            delega.Cognome = (string)reader["cognome"];
                        if (reader["nome"] != DBNull.Value)
                            delega.Nome = (string)reader["nome"];
                        delega.Residenza = new Indirizzo();
                        if (reader["indirizzoCAP"] != DBNull.Value)
                            delega.Residenza.Cap = (string)reader["indirizzoCAP"];
                        if (reader["indirizzoComune"] != DBNull.Value)
                            delega.Residenza.Comune = (string)reader["indirizzoComune"];
                        if (reader["indirizzoProvincia"] != DBNull.Value)
                            delega.Residenza.Provincia = (string)reader["indirizzoProvincia"];
                        if (reader["indirizzoDenominazione"] != DBNull.Value)
                            delega.Residenza.NomeVia = (string)reader["indirizzoDenominazione"];
                        if (reader["idArchidoc"] != DBNull.Value)
                            delega.IdArchidoc = (string)reader["idArchidoc"];
                        if (reader["nomeAllegatoLettera"] != DBNull.Value)
                            delega.NomeAllegatoLettera = (string)reader["nomeAllegatoLettera"];

                        listaDeleghe.Add(delega);
                    }
                }
            }

            return listaDeleghe;
        }

        public void AggiornaDelegaAllegato(int idDelega, string nomeAllegato)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DocumentiDelegheUpdateAllegatoLettera"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDelega", DbType.Int32, idDelega);
                DatabaseCemi.AddInParameter(comando, "@nomeAllegatoLettera", DbType.String, nomeAllegato);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }
    }
}
