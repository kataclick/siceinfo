﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Odbc;
using System.Text;
using System.Configuration;
using System.IO;
using System.Web.Hosting;

namespace WcfServiceArchidoc
{
    public static class ArchidocRdmServerDataProvider
    {

        public static List<Card> GetCardWithModifiedAttachment(String documentType, DateTime dateFromUtc, DateTime dateToUtc)
        {

            String dsn = ConfigurationManager.AppSettings["OdbcDSN"];
            String username = ConfigurationManager.AppSettings["RDMServerUsername"];
            String password = ConfigurationManager.AppSettings["RDMServerPassword"];
            List<Card> cards = new List<Card>();

            String connString = String.Format("DSN={0};Uid={1};Pwd={2};", dsn, username, password);
            try
            {
                using (OdbcConnection conn = new OdbcConnection(connString))
                {
                    conn.Open();

                    // LOG_MODIFACATOALLEGATO          1040
                    // LOG_AGGIUNTOALLEGATO            1014
                    // LOG_TOLTOALLEGATO               1015 

                    StringBuilder query = new StringBuilder();
                    query.Append("SELECT DISTINCT archivio.Anno AS Anno, archivio.progrAnnuo AS progrAnnuo FROM  archivio JOIN personalizzaarchivio ON archivio.tipodocumento  = personalizzaarchivio.progpersonalizza JOIN logstoria on archivio.progressivo = logstoria.progressivo ");
                    query.AppendFormat("WHERE personalizzaarchivio.nomearchivio = '{0}' AND ( logstoria.tipoAzione = 1014 OR logstoria.tipoAzione = 1040 OR logstoria.tipoAzione = 1015 )  AND logstoria.dataoraoperazione  >= {1} AND logstoria.dataoraoperazione <= {2}",
                        documentType, DateTimeToArchidocLogDateTimeNumber(dateFromUtc), DateTimeToArchidocLogDateTimeNumber(dateToUtc));

                    using (OdbcCommand command = new OdbcCommand(query.ToString(), conn))
                    {
                        using (OdbcDataReader reader = command.ExecuteReader())
                        {

                           
                            int indexProgressivo = reader.GetOrdinal("progrAnnuo");
                            int indexAnno = reader.GetOrdinal("Anno");

                            int anno = 0;
                            int progressivo = 0;

                            while (reader.Read())
                            {
                                //Console.WriteLine(String.Format("{0}/{1}", reader.GetInt32(indexProgressivoAnnuo), String.Format("{0:00}", (reader.GetInt32(indexAnno) % 100))));
                                anno = reader.GetInt32(indexAnno);
                                progressivo = reader.GetInt32(indexProgressivo);
                                cards.Add(new Card { ProgressivoAnnuo = String.Format("{0}/{1}", progressivo, String.Format("{0:00}", (anno % 100))) });
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
          

            return cards;
        }

        /// <summary>
        /// Abilita la modalità Dirty Read che consente di eseguire query solo in lettura senza mettere lock sulle tabelle
        /// </summary>
        /// <param name="connection"></param>
        private static void EnableDirtyRead(OdbcConnection connection)
        {
            String script = "set transaction isolation off";

            using (OdbcCommand command = new OdbcCommand(script, connection))
            {
                int result = command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Disabilita la modalità Dirty Read
        /// </summary>
        /// <param name="connection"></param>
        private static void DisableDirtyRead(OdbcConnection connection)
        {
            String script = "set transaction isolation on";

            using (OdbcCommand command = new OdbcCommand(script, connection))
            {
                int result = command.ExecuteNonQuery();
            }
        }

        private static long DateTimeToArchidocLogDateTimeNumber(DateTime utcDate)
        {
            return (utcDate.Ticks - Const.ARCHIDO_LOG_ORADATA_MAGIC_NUMBER) / (long)1000000000 * (long)100;
        }

        private static DateTime ArchidocLogDateTimeNumberToDateTime(long ticks)
        {
            long tickArchi = ((long)ticks * (long)1000000000 / (long)100) + Const.ARCHIDO_LOG_ORADATA_MAGIC_NUMBER;

            DateTime dateUtc = new DateTime(tickArchi, DateTimeKind.Utc);

            return dateUtc.ToLocalTime();

        }

        //private static void WriteLog(String message)
        //{
        //    String filePath = HostingEnvironment.ApplicationPhysicalPath;
        //    using (StreamWriter writer = new StreamWriter(Path.Combine(filePath,"Log.txt"), true))
        //    {

        //        writer.WriteLine(String.Format("{0} --- {1}", DateTime.Now, message));
        //    }

        //}
    }
}