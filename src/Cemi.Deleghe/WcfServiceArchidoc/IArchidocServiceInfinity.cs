﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceArchidoc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IArchidocServiceInfinity
    {

        [OperationContract]
        List<Card> GetCardsWithModifiedAttachment(String documentType, DateTime dateFromUtc, DateTime dateToUtc);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Card
    {
        String _progressivoAnnuo;
       // int _anno;


        [DataMember]
        public String ProgressivoAnnuo
        {
            get { return _progressivoAnnuo; }
            set { _progressivoAnnuo = value; }
        }

        //[DataMember]
        //public int Anno
        //{
        //    get { return _anno; }
        //    set { _anno = value; }
        //}

        //[DataMember]
        //public String Progressivo
        //{
        //    get { return String.Format("{0}/{1}", _progressivoAnnuo, String.Format("{0:00}", (_anno % 100))); }
        //}
    }
}
