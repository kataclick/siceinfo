﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceArchidoc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class ArchidocService : IArchidocServiceInfinity
    {

        #region IArchidocService Members

        
        /// <summary>
        /// Restituisce le schede di tipo documentType  per cui è stato inserito almeno un allegato nel periodo tra dateFromUtc e dateToUtc
        /// </summary>
        /// <param name="documentType">tipo dcumento</param>
        /// <param name="dateFromUtc"></param>
        /// <param name="dateToUtc"></param>
        /// <returns></returns>
        public List<Card> GetCardsWithModifiedAttachment(string documentType, DateTime dateFromUtc, DateTime dateToUtc)
        {
            return ArchidocRdmServerDataProvider.GetCardWithModifiedAttachment(documentType, dateFromUtc, dateToUtc);
        }

        #endregion
    }
}
