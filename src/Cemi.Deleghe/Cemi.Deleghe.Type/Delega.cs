﻿namespace Cemi.Deleghe.Type
{
    public class Delega
    {
        private string _sindacato;
        
        public string Sindacato
        {
            get
            {
                string ret;

                switch (_sindacato)
                {
                    case "CGIL":
                        ret = "FILLEA CGIL";
                        break;
                    case "CISL":
                        ret = "FILCA CISL";
                        break;
                    case "UIL":
                        ret = "FENEAL UIL";
                        break;
                    default:
                        ret = _sindacato;
                        break;
                }

                return ret;
            }
            set { _sindacato = value; }
        }

        public string Cellulare { get; set; }

        public int IdDelega { get; set; }

        public int IdLavoratore { get; set; }

        public int IdUtente { get; set ; }

        public string Nome { get; set; }

        public string Cognome { get; set; }

        public Indirizzo Residenza { get; set; }

        public string IdArchidoc { get; set; }

        public string NomeAllegatoLettera { get; set; }
    }
}