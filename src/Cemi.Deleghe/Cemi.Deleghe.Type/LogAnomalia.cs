﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.Deleghe.Type
{
    public class LogAnomalia
    {
        public int IdDelega { get; set; }

        public int IdLavoratore { get; set; }

        public string IdArchidoc { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }

        public override string ToString()
        {
            return String.Format("Id Delega: {0} - Id Lavoratore: {1} - Id Archidoc: {2} - Cognome: {3} - Nome: {4}{5}",
                                 IdDelega, IdLavoratore, IdArchidoc, Cognome, Nome, Environment.NewLine);
        }
    }
}
