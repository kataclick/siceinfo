﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using Cemi.Deleghe.Business;
using Cemi.Deleghe.Business.WordServiceReference;
using Cemi.Deleghe.Type;

namespace Cemi.Deleghe.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("{0}: Inizio procedura", DateTime.Now);

            //if (args.Length > 0 && args[0].ToUpper() == "FIX")
            //{
            //    GeneraLettereSenzaArchidoc();
            //}
            //else
            //{
                DelegheManager delegheManager = new DelegheManager();
                List<Delega> listaDeleghe = new List<Delega>();
                List<Delega> listaDelegheSms = new List<Delega>();

                //Recupero Deleghe da gestire
                Console.WriteLine("{0}: Specificare un idDelega? (Y/N)", DateTime.Now);
                string specifyDelega = Console.ReadLine();
                if (specifyDelega != null && specifyDelega.ToUpper() == "Y")
                {
                    Console.WriteLine("{0}: idDelega da gestire: ", DateTime.Now);
                    string idDelegaChar = Console.ReadLine();
                    int idDelega = 0;
                    try
                    {
                        idDelega = Convert.ToInt32(idDelegaChar);
                        listaDeleghe = delegheManager.GetDelegheById(idDelega);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        //throw;
                    }

                }
                else
                {
                    listaDeleghe = delegheManager.GetDeleghe();
                    listaDelegheSms = delegheManager.GetDelegheSms();
                }
                    

                //Console.WriteLine("{0}: Deleghe recuperate: {1}", DateTime.Now, listaDeleghe.Count);

                if (args.Length > 0)
                {
                    switch (args[0].ToUpper())
                    {
                        case "DELETE":
                            Console.WriteLine("{0}: Deleghe recuperate: {1}", DateTime.Now, listaDeleghe.Count);
                            EliminazioneAllegati(listaDeleghe);
                            break;
                        case "MERGE":
                            GenerazioneUnicoPdf();
                            break;
                        case "FIX":
                            Console.WriteLine("{0}: Deleghe recuperate: {1}", DateTime.Now, listaDeleghe.Count);
                            GeneraLettereSenzaArchidoc(listaDeleghe);
                            break;
                        default:
                            Console.WriteLine("Parametro non riconosciuto");
                            break;
                    }
                }
                else
                {
                    //Invio SMS
                    InvioSms(listaDelegheSms);

                    //Generazione lettere
                    GenerazioneLettere(listaDeleghe);
                }
            //}

            Console.WriteLine("{0}: Fine procedura", DateTime.Now);
            Console.WriteLine("-- Premere invio per chiudere --");
            Console.ReadLine();
        }

        private static void InvioSms(IEnumerable<Delega> listaDeleghe)
        {
            DelegheManager delegheManager = new DelegheManager();
            Console.WriteLine("{0}: Inviare gli SMS? (Y/N)", DateTime.Now);
            string invioSms = Console.ReadLine();
            if (invioSms != null && invioSms.ToUpper() == "Y")
            {
                List<Delega> listaDelegheSms = (from delega in listaDeleghe
                                                where !String.IsNullOrEmpty(delega.Cellulare)
                                                select delega).ToList();

                bool invioSmsRes = delegheManager.InviaSms(listaDelegheSms);

                if (invioSmsRes)
                {
                    Console.WriteLine("{0}: SMS inviati per numero deleghe: {1}", DateTime.Now, listaDelegheSms.Count);
                }
                else
                {
                    Console.WriteLine("{0}: PROBLEMA durante l'invio degli SMS - Verificare su Event Viewer", DateTime.Now);
                }
            }
        }

        private static void GenerazioneLettere(IEnumerable<Delega> listaDeleghe)
        {
            DelegheManager delegheManager = new DelegheManager();
            Console.WriteLine("{0}: Generare le lettere? (Y/N)", DateTime.Now);
            string generazioneLettere = Console.ReadLine();
            if (generazioneLettere != null && generazioneLettere.ToUpper() == "Y")
            {
                string pathLettere = ConfigurationManager.AppSettings["PathLettere"];
                string pathLetterePdf = ConfigurationManager.AppSettings["PathLetterePdf"];
                string pathTemplateLettere = ConfigurationManager.AppSettings["PathTemplateLettere"];
                string pathLog = ConfigurationManager.AppSettings["PathLog"];
                
                List<Delega> listaDelegheLettere = (from delega in listaDeleghe
                                                    where
                                                        !String.IsNullOrEmpty(delega.Residenza.Via)
                                                        && !String.IsNullOrEmpty(delega.Residenza.Comune)
                                                        && !String.IsNullOrEmpty(delega.Residenza.Cap)
                                                        && !String.IsNullOrEmpty(delega.Residenza.Provincia)
                                                        && !String.IsNullOrEmpty(delega.IdArchidoc)
                                                    select delega).ToList();


                bool generazioneLettereRes = delegheManager.GeneraLettere(listaDelegheLettere, pathTemplateLettere,
                                                                          pathLettere, pathLetterePdf, pathLog);
                if (generazioneLettereRes)
                {
                    Console.WriteLine("{0}: Lettere generate per numero deleghe: {1}", DateTime.Now,
                                      listaDelegheLettere.Count);

                    GenerazioneUnicoPdf();
                }
                else
                {
                    Console.WriteLine("{0}: PROBLEMA durante la generazione delle lettere - Verificare su Event Viewer",
                                      DateTime.Now);
                }
            }
        }

        private static void GenerazioneUnicoPdf()
        {
            DelegheManager delegheManager = new DelegheManager();

            string pathLetterePdf = ConfigurationManager.AppSettings["PathLetterePdf"];
            int numeroMaxPdf = Convert.ToInt32(ConfigurationManager.AppSettings["NumeroMaxPdf"]);
            
            Console.WriteLine("{0}: Generare un unico PDF? (Y/N)", DateTime.Now);
            string generazioneUnicoPdf = Console.ReadLine();
            if (generazioneUnicoPdf != null && generazioneUnicoPdf.ToUpper() == "Y")
            {
                List<string> lettereUnicoPdf = delegheManager.GeneraLettereUnicoPdf(pathLetterePdf, numeroMaxPdf);

                Console.WriteLine("{0}: Lettere uniche generate: {1}", DateTime.Now, lettereUnicoPdf.Count);
                if (lettereUnicoPdf.Count > 0)
                    Console.WriteLine("{0}: Prima lettera unica generata: {1}", DateTime.Now, lettereUnicoPdf[0]);
            }
        }

        private static void EliminazioneAllegati(List<Delega> listaDeleghe)
        {
            DelegheManager delegheManager = new DelegheManager();
            Console.WriteLine("{0}: Eliminare da Archidoc le lettere allegate? (Y/N)", DateTime.Now);
            string eliminazioneLettere = Console.ReadLine();
            if (eliminazioneLettere != null && eliminazioneLettere.ToUpper() == "Y")
            {
                bool eliminazioneLettereRes = delegheManager.EliminaAllegati(listaDeleghe);

                if (eliminazioneLettereRes)
                {
                    Console.WriteLine("{0}: Lettere allegate eliminate per numero deleghe: {1}", DateTime.Now, listaDeleghe.Count());
                }
                else
                {
                    Console.WriteLine("{0}: PROBLEMA durante la cancellazione su Archidoc - Verificare su Event Viewer", DateTime.Now);
                }
            }
        }

        private static void GeneraLettereSenzaArchidoc(List<Delega> listaDeleghe)
        {
            DelegheManager delegheManager = new DelegheManager();

            if (listaDeleghe.Count() > 0)
            {
                Delega delegaToFix;
                foreach (Delega d in listaDeleghe)
                { 
                    delegaToFix = new Delega
                    {
                        IdLavoratore = d.IdLavoratore,
                        Sindacato = d.Sindacato,

                        Cognome = d.Cognome,
                        Nome = d.Nome,
                        Residenza = new Deleghe.Type.Indirizzo
                        {
                            NomeVia = d.Residenza.IndirizzoBase,
                            Cap = d.Residenza.Cap,
                            Comune = d.Residenza.Comune,
                            Provincia = d.Residenza.Provincia
                        },
                        IdArchidoc = d.IdArchidoc
                    };

                    delegheManager.GeneraLetteraSpecifica(delegaToFix);
                }
                /*
                    delegaToFix = new Delega
                    {
                        IdLavoratore = 849962,
                        Sindacato = "CISL",

                        Cognome = "GUARDADO MENJIVAR",
                        Nome = "JOSUE JONATHAN",
                        Residenza = new Deleghe.Type.Indirizzo
                        {
                            NomeVia = "VIA ANTONIO ALDINI 57",
                            Cap = "20157",
                            Comune = "MILANO",
                            Provincia = "MI"
                        }
                    };

                    GeneraLetteraSpecifica(delegaToFix);

                    delegaToFix = new Delega
                    {
                        IdLavoratore = 847993,
                        Sindacato = "CISL",

                        Cognome = "SIMONI",
                        Nome = "PARLIND",
                        Residenza = new Deleghe.Type.Indirizzo
                        {
                            NomeVia = "VIA SAN GIOVANNI BOSCO 5",
                            Cap = "21056",
                            Comune = "INDUNO OLONA",
                            Provincia = "VA"
                        }
                    };

                    GeneraLetteraSpecifica(delegaToFix);
                */

            }
        }
        
        private static void GeneraLetteraSpecifica(Delega delega)
        {
            string pathLettere = ConfigurationManager.AppSettings["PathLettere"];
            string pathLetterePdf = ConfigurationManager.AppSettings["PathLetterePdf"];
            string pathTemplateLettere = ConfigurationManager.AppSettings["PathTemplateLettere"];
            string pathLog = ConfigurationManager.AppSettings["PathLog"];
            
            PdfManager pdfManager = new PdfManager();

            try
            {
                //svuoto le cartelle
                /*
                DirectoryInfo directoryInfoWord = new DirectoryInfo(pathLettere);
                if (directoryInfoWord.Exists)
                {
                    foreach (FileInfo fileInfo in directoryInfoWord.GetFiles())
                    {
                        fileInfo.Delete();
                    }
                }

                DirectoryInfo directoryInfoPdf = new DirectoryInfo(pathLetterePdf);
                if (directoryInfoPdf.Exists)
                {
                    foreach (FileInfo fileInfo in directoryInfoPdf.GetFiles())
                    {
                        fileInfo.Delete();
                    }
                }
                */

                WordServiceSoapClient client = new WordServiceSoapClient();

                List<WordField> listaCampi = new List<WordField>();

                WordField wfDataLettera = new WordField { Campo = "DataLettera", Valore = DateTime.Today.ToShortDateString() };
                //WordField wfProtocollo = new WordField {Campo = "Protocollo", Valore = "???"};
                WordField wfCodiceCe = new WordField { Campo = "CodiceCe", Valore = delega.IdLavoratore.ToString(CultureInfo.InvariantCulture) };
                WordField wfCognomeNome = new WordField { Campo = "CognomeNome", Valore = $"{delega.Cognome} {delega.Nome}" };
                WordField wfIndirizzo = new WordField { Campo = "Indirizzo", Valore = delega.Residenza.Via };
                WordField wfCap = new WordField { Campo = "Cap", Valore = delega.Residenza.Cap };
                WordField wfComune = new WordField { Campo = "Comune", Valore = delega.Residenza.Comune };
                WordField wfProvincia = new WordField { Campo = "Provincia", Valore = delega.Residenza.Provincia };
                WordField wfSindacato = new WordField { Campo = "Sindacato", Valore = delega.Sindacato };

                listaCampi.Add(wfDataLettera);
                //listaCampi.Add(wfProtocollo);
                listaCampi.Add(wfCodiceCe);
                listaCampi.Add(wfCognomeNome);
                listaCampi.Add(wfIndirizzo);
                listaCampi.Add(wfCap);
                listaCampi.Add(wfComune);
                listaCampi.Add(wfProvincia);
                listaCampi.Add(wfSindacato);

                string wordGeneratedPath = client.CreateWord(pathTemplateLettere, listaCampi.ToArray(), pathLettere);

                //var pdfFromWord = pdfManager.ConvertWord2PdfTelerik(wordGeneratedPath);
                //Stream pdfStream = new MemoryStream(pdfFromWord);
                var pdfStream = pdfManager.ConvertWord2Pdf(wordGeneratedPath);
                

                string fileName = $"{pathLetterePdf}Lettera-{delega.IdLavoratore}_TIF.pdf";

                Stream archidocFirstPage = null;

                //archidocFirstPage = pdfManager.GetPdfPage($@".\DelegheToFix\{delega.IdLavoratore}.tif", 0);

                string delegaTiffPath = $@".\DelegheToFix\{delega.IdLavoratore}.tif";

                if (File.Exists(delegaTiffPath))
                {
                    byte[] archidocDocumentTiff = File.ReadAllBytes(delegaTiffPath);
                    if (archidocDocumentTiff.Length > 0)
                    {
                        //Convert to PDF...
                        byte[] archidocDocument = PdfManager.ImageToPdf(archidocDocumentTiff);

                        Stream archidocDocumentStream = pdfManager.GetByteStream(archidocDocument);
                        archidocFirstPage = pdfManager.GetPdfPage(archidocDocumentStream, 0);

                        pdfManager.MergePdf(pdfStream, archidocFirstPage, fileName);

                        //using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
                        //{
                        //    byte[] pdfByte = new byte[fileStream.Length];
                        //    fileStream.Read(pdfByte, 0, (int)fileStream.Length);
                        //    fileStream.Flush();
                        //    fileStream.Close();
                        //}

                        //string nomeAllegato = $"Lettera-{delega.IdLavoratore}.pdf";
                    }
                }

                fileName = $"{pathLetterePdf}Lettera-{delega.IdLavoratore}.pdf";

                archidocFirstPage = null;

                //archidocFirstPage = pdfManager.GetPdfPage($@".\DelegheToFix\{delega.IdLavoratore}.tif", 0);

                string delegaPdfPath = $@".\DelegheToFix\{delega.IdLavoratore}.pdf";

                if (File.Exists(delegaPdfPath))
                {
                    archidocFirstPage = pdfManager.GetPdfPage(delegaPdfPath, 0);
                    pdfManager.MergePdf(pdfStream, archidocFirstPage, fileName);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //throw;
            }
        }
    }
}