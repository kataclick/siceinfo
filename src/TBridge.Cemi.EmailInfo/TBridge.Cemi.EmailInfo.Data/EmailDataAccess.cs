using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Net.Mail;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.EmailInfo.Type.Collections;
using TBridge.Cemi.EmailInfo.Type.Entities;
using TBridge.Cemi.EmailInfo.Type.Filters;

namespace TBridge.Cemi.EmailInfo.Data
{
    public class EmailDataAccess
    {
        private Database databaseCemi;

        public EmailDataAccess()
        {
            databaseCemi = DatabaseFactory.CreateDatabase("CEMIEmail");
        }

        public Database DatabaseCemi
        {
            get { return databaseCemi; }
            set { databaseCemi = value; }
        }

        public bool InviaEmail(EmailMessageSerializzabile email, string utenteInvio)
        {
            bool res = false;

            if (email != null)
            {
                using (DbConnection connection = databaseCemi.CreateConnection())
                {
                    if (connection != null)
                    {
                        connection.Open();
                        using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                        {
                            try
                            {
                                // Per prima cosa viene inserita l'email
                                if (InsertEmailDaInviare(email, utenteInvio, transaction))
                                {
                                    bool tempRes = true;

                                    // Inserimento destinatari (TO)
                                    foreach (EmailAddress address in email.Destinatari)
                                    {
                                        if (!InsertEmailDestinatario(address, email.IdEmail.Value, transaction))
                                        {
                                            tempRes = false;
                                            break;
                                        }
                                    }

                                    if (tempRes)
                                    {
                                        // Inserimento destinatari (CC)
                                        foreach (EmailAddress address in email.DestinatariCC)
                                        {
                                            if (!InsertEmailDestinatarioCC(address, email.IdEmail.Value, transaction))
                                            {
                                                tempRes = false;
                                                break;
                                            }
                                        }

                                        if (tempRes)
                                        {
                                            // Inserimento destinatari (BCC)
                                            foreach (EmailAddress address in email.DestinatariBCC)
                                            {
                                                if (
                                                    !InsertEmailDestinatarioBCC(address, email.IdEmail.Value,
                                                                                transaction))
                                                {
                                                    tempRes = false;
                                                    break;
                                                }
                                            }

                                            if (tempRes)
                                            {
                                                foreach (EmailAttachment attachment in email.Allegati)
                                                {
                                                    if (
                                                        !InsertEmailAttachment(attachment, email.IdEmail.Value,
                                                                               transaction))
                                                    {
                                                        tempRes = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (tempRes)
                                    {
                                        res = true;
                                        transaction.Commit();
                                    }
                                    else
                                        throw new Exception("Errore durante l'invio della email.");
                                }
                            }
                            catch
                            {
                                transaction.Rollback();
                                throw;
                            }
                        }
                    }
                }
            }

            return res;
        }

        private bool InsertEmailDaInviare(EmailMessageSerializzabile email, string utenteInvio,
                                          DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDaInviareInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@mittenteEmail", DbType.String, email.Mittente.Indirizzo);
                if (!string.IsNullOrEmpty(email.Mittente.Nome))
                    DatabaseCemi.AddInParameter(comando, "@mittenteDescrizione", DbType.String, email.Mittente.Nome);
                if (!string.IsNullOrEmpty(email.Oggetto))
                    DatabaseCemi.AddInParameter(comando, "@oggetto", DbType.String, email.Oggetto);
                if (!string.IsNullOrEmpty(email.BodyPlain))
                    DatabaseCemi.AddInParameter(comando, "@testo", DbType.String, email.BodyPlain);
                if (!string.IsNullOrEmpty(email.BodyHTML))
                    DatabaseCemi.AddInParameter(comando, "@testoHTML", DbType.String, email.BodyHTML);
                DatabaseCemi.AddInParameter(comando, "@dataSchedulata", DbType.DateTime, email.DataSchedulata);
                DatabaseCemi.AddInParameter(comando, "@priorita", DbType.Int32, email.Priorita);
                if (email.ReplyTo != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@rispondiAEmail", DbType.String, email.ReplyTo.Indirizzo);
                    if (!string.IsNullOrEmpty(email.ReplyTo.Nome))
                        DatabaseCemi.AddInParameter(comando, "@rispondiADescrizione", DbType.String, email.ReplyTo.Nome);
                }
                DatabaseCemi.AddInParameter(comando, "@deliveryOptions", DbType.Int32, email.OpzioniNotifica);
                DatabaseCemi.AddInParameter(comando, "@utenteInvio", DbType.String, utenteInvio);

                DatabaseCemi.AddOutParameter(comando, "@idEmail", DbType.Int32, 4);
                DatabaseCemi.ExecuteNonQuery(comando, transaction);

                int idEmail = (int) databaseCemi.GetParameterValue(comando, "@idEmail");
                if (idEmail != -1)
                {
                    email.IdEmail = idEmail;
                    res = true;
                }
            }

            return res;
        }

        public EmailMessageCollection GetEMailDaInviare(EmailMessageFilter filtro)
        {
            EmailMessageCollection listaMail = new EmailMessageCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDaInviareSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    EmailMessage emailPrec = null;

                    while (reader.Read())
                    {
                        int idEmail = (int) reader["idEmail"];

                        // Guardo se il record appartiene ad una nuova email oppure � un destinatario diverso
                        if (emailPrec == null || emailPrec.IdEmail != idEmail)
                        {
                            emailPrec = new EmailMessage();
                            listaMail.Add(emailPrec);

                            emailPrec.IdEmail = idEmail;
                            emailPrec.From = new MailAddress(reader["mittenteEmail"] as string,
                                                             reader["mittenteDescrizione"] as string);
                            emailPrec.Subject = reader["oggetto"] as string;
                            emailPrec.Body = reader["testo"] as string;
                            emailPrec.BodyHTML = reader["testoHTML"] as string;
                            emailPrec.Priority = (MailPriority) reader["priorita"];
                            if (reader["rispondiAEmail"] != DBNull.Value)
                            {
                                emailPrec.ReplyTo = new MailAddress(reader["rispondiAEmail"] as string,
                                                                    reader["rispondiADescrizione"] as string);
                            }
                            emailPrec.DataSchedulata = (DateTime) reader["dataSchedulata"];
                            emailPrec.DeliveryNotificationOptions =
                                (DeliveryNotificationOptions) reader["deliveryOptions"];
                            if (reader["esistonoAllegati"] != DBNull.Value)
                                emailPrec.EsistonoAllegati = true;
                        }

                        // To
                        if (reader["idEmailDestinatario"] != DBNull.Value)
                        {
                            MailAddress address = null;
                            try
                            {
                                address = new MailAddress(reader["destEmail"] as string,
                                            reader["destDescrizione"] as string);
                            }
                            catch
                            {
                                address = new MailAddress("etlcemi@tbridge.it", String.Format("DESTINATARIO: {0}", reader["destEmail"]));
                            }

                            if (!emailPrec.To.Contains(address))
                                emailPrec.To.Add(address);
                        }

                        // Cc
                        if (reader["idEmailDestinatarioCC"] != DBNull.Value)
                        {
                            MailAddress address = null;
                            try
                            {
                                address = new MailAddress(reader["destCCEmail"] as string,
                                                                  reader["destCCDescrizione"] as string);
                            }
                            catch
                            {
                                address = new MailAddress("etlcemi@tbridge.it", String.Format("DESTINATARIOCC: {0}", reader["destCCEmail"]));
                            }

                            if (!emailPrec.CC.Contains(address))
                                emailPrec.CC.Add(address);
                        }

                        // Bcc
                        if (reader["idEmailDestinatarioBCC"] != DBNull.Value)
                        {
                            MailAddress address = null;
                            try
                            {
                                address = new MailAddress(reader["destBCCEmail"] as string,
                                                          reader["destBCCDescrizione"] as string);
                            }
                            catch
                            {
                                address = new MailAddress("etlcemi@tbridge.it", String.Format("DESTINATARIOBCC: {0}", reader["destBCCEmail"]));
                            }

                            if (!emailPrec.Bcc.Contains(address))
                                emailPrec.Bcc.Add(address);
                        }
                    }
                }
            }

            return listaMail;
        }

        public EmailMessageSerializzabileCollection GetEMailDaInviareSerializzabile(EmailMessageFilter filtro,
                                                                                    string utenteInvio)
        {
            EmailMessageSerializzabileCollection listaMail = new EmailMessageSerializzabileCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDaInviareSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    EmailMessageSerializzabile emailPrec = null;

                    while (reader.Read())
                    {
                        int idEmail = (int) reader["idEmail"];

                        // Guardo se il record appartiene ad una nuova email oppure � un destinatario diverso
                        if (emailPrec == null || emailPrec.IdEmail != idEmail)
                        {
                            emailPrec = new EmailMessageSerializzabile();
                            listaMail.Add(emailPrec);

                            emailPrec.IdEmail = idEmail;
                            emailPrec.Mittente = new EmailAddress(reader["mittenteDescrizione"] as string,
                                                                  reader["mittenteEmail"] as string);
                            emailPrec.Oggetto = reader["oggetto"] as string;
                            emailPrec.BodyPlain = reader["testo"] as string;
                            emailPrec.BodyHTML = reader["testoHTML"] as string;
                            emailPrec.Priorita = (MailPriority) reader["priorita"];
                            if (reader["rispondiAEmail"] != DBNull.Value)
                            {
                                emailPrec.ReplyTo = new EmailAddress(reader["rispondiADescrizione"] as string,
                                                                     reader["rispondiAEmail"] as string);
                            }
                            emailPrec.DataSchedulata = (DateTime) reader["dataSchedulata"];
                            emailPrec.OpzioniNotifica = (DeliveryNotificationOptions) reader["deliveryOptions"];
                            if (reader["esistonoAllegati"] != DBNull.Value)
                                emailPrec.EsistonoAllegati = true;
                        }

                        // To
                        if (reader["idEmailDestinatario"] != DBNull.Value)
                        {
                            EmailAddress address = new EmailAddress(reader["destDescrizione"] as string,
                                                                    reader["destEmail"] as string);

                            if (!emailPrec.Destinatari.Contains(address))
                                emailPrec.Destinatari.Add(address);
                        }

                        // CC
                        if (reader["idEmailDestinatarioCC"] != DBNull.Value)
                        {
                            EmailAddress address = new EmailAddress(reader["destCCDescrizione"] as string,
                                                                    reader["destCCEmail"] as string);

                            if (!emailPrec.DestinatariCC.Contains(address))
                                emailPrec.DestinatariCC.Add(address);
                        }

                        // Bcc
                        if (reader["idEmailDestinatarioBCC"] != DBNull.Value)
                        {
                            EmailAddress address = new EmailAddress(reader["destBCCDescrizione"] as string,
                                                                    reader["destBCCEmail"] as string);

                            if (!emailPrec.DestinatariBCC.Contains(address))
                                emailPrec.DestinatariBCC.Add(address);
                        }
                    }
                }
            }

            return listaMail;
        }

        public bool UpdateEmailInviata(EmailMessage email)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDaInviareUpdateInviata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, email.IdEmail.Value);
                DatabaseCemi.AddOutParameter(comando, "@res", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = (bool) DatabaseCemi.GetParameterValue(comando, "@res");
            }

            return res;
        }

        public void GetEmailAttachment(EmailMessage email)
        {
            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailAttachmentSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, email.IdEmail.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        string nomeAttach = reader["nomeAttachment"] as string;
                        byte[] attach = (byte[]) reader["attachment"];
                        MemoryStream stream = new MemoryStream(attach);

                        Attachment attachment = new Attachment(stream, nomeAttach);
                        email.Attachments.Add(attachment);
                    }
                }
            }
        }

        public EmailMessageCollection GetEmailInviate(EmailMessageFilter filtro)
        {
            EmailMessageCollection listaMail = new EmailMessageCollection();

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailInviateSelect"))
            {
                if (filtro.GiornoInvio.HasValue)
                    databaseCemi.AddInParameter(comando, "@dataInvio", DbType.DateTime, filtro.GiornoInvio.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    EmailMessage emailPrec = null;

                    while (reader.Read())
                    {
                        int idEmail = (int) reader["idEmail"];

                        // Guardo se il record appartiene ad una nuova email oppure � un destinatario diverso
                        if (emailPrec == null || emailPrec.IdEmail != idEmail)
                        {
                            emailPrec = new EmailMessage();
                            listaMail.Add(emailPrec);

                            emailPrec.IdEmail = idEmail;
                            emailPrec.From = new MailAddress(reader["mittenteEmail"] as string,
                                                             reader["mittenteDescrizione"] as string);
                            emailPrec.Subject = reader["oggetto"] as string;
                            emailPrec.Body = reader["testo"] as string;
                            emailPrec.BodyHTML = reader["testoHTML"] as string;
                            emailPrec.Priority = (MailPriority) reader["priorita"];
                            if (reader["rispondiAEmail"] != DBNull.Value)
                            {
                                emailPrec.ReplyTo = new MailAddress(reader["rispondiAEmail"] as string,
                                                                    reader["rispondiADescrizione"] as string);
                            }
                            emailPrec.DataSchedulata = (DateTime) reader["dataSchedulata"];
                            emailPrec.DataInvio = (DateTime) reader["dataInvio"];
                            emailPrec.DeliveryNotificationOptions =
                                (DeliveryNotificationOptions) reader["deliveryOptions"];
                            if (reader["esistonoAllegati"] != DBNull.Value)
                                emailPrec.EsistonoAllegati = true;
                        }

                        // To
                        if (reader["idEmailDestinatario"] != DBNull.Value)
                        {
                            MailAddress address = new MailAddress(reader["destEmail"] as string,
                                                                  reader["destDescrizione"] as string);

                            emailPrec.To.Add(address);
                        }

                        // CC
                        if (reader["idEmailDestinatarioCC"] != DBNull.Value)
                        {
                            MailAddress address = new MailAddress(reader["destCCEmail"] as string,
                                                                  reader["destCCDescrizione"] as string);

                            emailPrec.CC.Add(address);
                        }

                        // Bcc
                        if (reader["idEmailDestinatarioBCC"] != DBNull.Value)
                        {
                            MailAddress address = new MailAddress(reader["destBCCEmail"] as string,
                                                                  reader["destBCCDescrizione"] as string);

                            emailPrec.Bcc.Add(address);
                        }
                    }
                }
            }

            return listaMail;
        }

        #region Email, inserimento indirizzi

        private bool InsertEmailDestinatarioBCC(EmailAddress address, int idEmail, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDestinatariBCCInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, idEmail);
                DatabaseCemi.AddInParameter(comando, "@email", DbType.String, address.Indirizzo);
                if (!string.IsNullOrEmpty(address.Nome))
                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, address.Nome);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertEmailDestinatarioCC(EmailAddress address, int idEmail, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDestinatariCCInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, idEmail);
                DatabaseCemi.AddInParameter(comando, "@email", DbType.String, address.Indirizzo);
                if (!string.IsNullOrEmpty(address.Nome))
                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, address.Nome);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertEmailDestinatario(EmailAddress address, int idEmail, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailDestinatariInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, idEmail);
                DatabaseCemi.AddInParameter(comando, "@email", DbType.String, address.Indirizzo);
                if (!string.IsNullOrEmpty(address.Nome))
                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, address.Nome);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Email, inserimento allegati

        private bool InsertEmailAttachment(EmailAttachment attachment, int idEmail, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_EmailAttachmentInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idEmail", DbType.Int32, idEmail);
                DatabaseCemi.AddInParameter(comando, "@nomeAttachment", DbType.String, attachment.NomeFile);
                DatabaseCemi.AddInParameter(comando, "@attachment", DbType.Binary, attachment.File);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        #endregion
    }
}