using System;
using System.Collections.Generic;
using TBridge.Cemi.EmailInfo.Type.Entities;

namespace TBridge.Cemi.EmailInfo.Type.Collections
{
    [Serializable]
    public class EmailAddressCollection : List<EmailAddress>
    {
    }
}