using System;

namespace TBridge.Cemi.EmailInfo.Type.Filters
{
    public class EmailMessageFilter
    {
        public bool? DaInviare { get; set; }

        public DateTime? GiornoInvio { get; set; }
    }
}