using System;

namespace TBridge.Cemi.EmailInfo.Type.Entities
{
    [Serializable]
    public class EmailAttachment
    {
        private byte[] file;
        private string nomeFile;

        public EmailAttachment(string nomeFile, byte[] file)
        {
            this.nomeFile = nomeFile;
            this.file = file;
        }

        public EmailAttachment()
        {
        }

        public string NomeFile
        {
            get { return nomeFile; }
            set { nomeFile = value; }
        }

        public byte[] File
        {
            get { return file; }
            set { file = value; }
        }
    }
}