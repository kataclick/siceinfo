using System;
using System.Net.Mail;
using TBridge.Cemi.EmailInfo.Type.Collections;

namespace TBridge.Cemi.EmailInfo.Type.Entities
{
    [Serializable]
    public class EmailMessageSerializzabile
    {
        private EmailAttachmentCollection allegati;
        private string bodyHTML;
        private string bodyPlain;
        private DateTime dataInvio;
        private DateTime dataSchedulata;
        private EmailAddressCollection destinatari;
        private EmailAddressCollection destinatariBCC;
        private EmailAddressCollection destinatariCC;
        private bool esistonoAllegati;
        private int? idEmail;
        private EmailAddress mittente;
        private string oggetto;
        private DeliveryNotificationOptions opzioniNotifica;
        private MailPriority priorita;
        private EmailAddress replyTo;
        private string utenteInvio;

        public EmailMessageSerializzabile()
        {
            destinatari = new EmailAddressCollection();
            destinatariCC = new EmailAddressCollection();
            destinatariBCC = new EmailAddressCollection();
            allegati = new EmailAttachmentCollection();
            //this.erroreInvio = null;
        }

        public int? IdEmail
        {
            get { return idEmail; }
            set { idEmail = value; }
        }

        public string BodyPlain
        {
            get { return bodyPlain; }
            set { bodyPlain = value; }
        }

        public string BodyHTML
        {
            get { return bodyHTML; }
            set { bodyHTML = value; }
        }

        public DateTime DataSchedulata
        {
            get { return dataSchedulata; }
            set { dataSchedulata = value; }
        }

        public DateTime DataInvio
        {
            get { return dataInvio; }
            set { dataInvio = value; }
        }

        public bool EsistonoAllegati
        {
            get { return esistonoAllegati; }
            set { esistonoAllegati = value; }
        }

        public EmailAddress Mittente
        {
            get { return mittente; }
            set { mittente = value; }
        }

        public EmailAddressCollection Destinatari
        {
            get { return destinatari; }
            set { destinatari = value; }
        }

        public EmailAddressCollection DestinatariCC
        {
            get { return destinatariCC; }
            set { destinatariCC = value; }
        }

        public EmailAddressCollection DestinatariBCC
        {
            get { return destinatariBCC; }
            set { destinatariBCC = value; }
        }

        public string Oggetto
        {
            get { return oggetto; }
            set { oggetto = value; }
        }

        public MailPriority Priorita
        {
            get { return priorita; }
            set { priorita = value; }
        }

        public EmailAddress ReplyTo
        {
            get { return replyTo; }
            set { replyTo = value; }
        }

        public DeliveryNotificationOptions OpzioniNotifica
        {
            get { return opzioniNotifica; }
            set { opzioniNotifica = value; }
        }

        public EmailAttachmentCollection Allegati
        {
            get { return allegati; }
            set { allegati = value; }
        }

        public string UtenteInvio
        {
            get { return utenteInvio; }
        }

        //private Exception erroreInvio;
        //public Exception ErroreInvio
        //{
        //    get { return erroreInvio; }
        //    set { erroreInvio = value; }
        //}
    }
}