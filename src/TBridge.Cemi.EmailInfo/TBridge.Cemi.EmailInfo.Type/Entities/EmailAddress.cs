using System;

namespace TBridge.Cemi.EmailInfo.Type.Entities
{
    [Serializable]
    public class EmailAddress
    {
        private string indirizzo;
        private string nome;

        public EmailAddress(string nome, string indirizzo)
        {
            this.nome = nome;
            this.indirizzo = indirizzo;
        }

        public EmailAddress()
        {
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Indirizzo
        {
            get { return indirizzo; }
            set { indirizzo = value; }
        }
    }
}