using System;
using System.Net.Mail;

namespace TBridge.Cemi.EmailInfo.Type.Entities
{
    public class EmailMessage : MailMessage
    {
        private string body;

        public int? IdEmail { get; set; }

        public string Body
        {
            get { return body; }
            set { body = value; }
        }

        public string BodyHTML { get; set; }

        public DateTime DataSchedulata { get; set; }

        public DateTime DataInvio { get; set; }

        public bool EsistonoAllegati { get; set; }
    }
}