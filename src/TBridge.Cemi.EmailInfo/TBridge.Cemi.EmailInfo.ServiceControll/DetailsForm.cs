using TBridge.Cemi.EmailInfo.Type.Entities;

namespace TBridge.Cemi.EmailInfo.ServiceController
{
    public partial class DetailsForm : BaseForm
    {
        public DetailsForm(EmailMessage email)
        {
            InitializeComponent();

            CaricaEmail(email);
        }

        private void CaricaEmail(EmailMessage email)
        {
            labelSchedulated.Text = email.DataSchedulata.ToString("dd/MM/yyyy hh:mm");
            labelFrom.Text = email.From.ToString();
            labelTo.Text = email.To.ToString();
            labelCC.Text = email.CC.ToString();
            labelBCC.Text = email.Bcc.ToString();
            if (email.ReplyTo != null)
                labelReplyTo.Text = email.ReplyTo.ToString();
            labelPriority.Text = email.Priority.ToString();
            labelSubject.Text = email.Subject;
            labelPlain.Text = email.Body;
            labelHTML.Text = email.BodyHTML;
            checkBoxAllegati.Checked = email.EsistonoAllegati;
            labelDeliveryOptions.Text = email.DeliveryNotificationOptions.ToString();
        }
    }
}