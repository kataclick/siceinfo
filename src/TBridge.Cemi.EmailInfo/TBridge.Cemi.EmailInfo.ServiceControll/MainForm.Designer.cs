namespace TBridge.Cemi.EmailInfo.ServiceController
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControlEmail = new System.Windows.Forms.TabControl();
            this.tabPageEmailDaInviare = new System.Windows.Forms.TabPage();
            this.buttonAggiornaEmailDaInviare = new System.Windows.Forms.Button();
            this.dataGridViewEmailDaInviare = new System.Windows.Forms.DataGridView();
            this.tabPageEmailInviate = new System.Windows.Forms.TabPage();
            this.timerInvioEmail = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorkerInvioEmail = new System.ComponentModel.BackgroundWorker();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.dataGridViewEmailInviate = new System.Windows.Forms.DataGridView();
            this.buttonAggiornaEmailInviate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerGiornoInvio = new System.Windows.Forms.DateTimePicker();
            this.tabControlEmail.SuspendLayout();
            this.tabPageEmailDaInviare.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmailDaInviare)).BeginInit();
            this.tabPageEmailInviate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmailInviate)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlEmail
            // 
            this.tabControlEmail.Controls.Add(this.tabPageEmailDaInviare);
            this.tabControlEmail.Controls.Add(this.tabPageEmailInviate);
            this.tabControlEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlEmail.Location = new System.Drawing.Point(0, 0);
            this.tabControlEmail.Name = "tabControlEmail";
            this.tabControlEmail.SelectedIndex = 0;
            this.tabControlEmail.Size = new System.Drawing.Size(700, 451);
            this.tabControlEmail.TabIndex = 0;
            // 
            // tabPageEmailDaInviare
            // 
            this.tabPageEmailDaInviare.Controls.Add(this.buttonAggiornaEmailDaInviare);
            this.tabPageEmailDaInviare.Controls.Add(this.dataGridViewEmailDaInviare);
            this.tabPageEmailDaInviare.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmailDaInviare.Name = "tabPageEmailDaInviare";
            this.tabPageEmailDaInviare.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmailDaInviare.Size = new System.Drawing.Size(692, 425);
            this.tabPageEmailDaInviare.TabIndex = 0;
            this.tabPageEmailDaInviare.Text = "Email Da Inviare";
            this.tabPageEmailDaInviare.UseVisualStyleBackColor = true;
            // 
            // buttonAggiornaEmailDaInviare
            // 
            this.buttonAggiornaEmailDaInviare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAggiornaEmailDaInviare.Location = new System.Drawing.Point(577, 394);
            this.buttonAggiornaEmailDaInviare.Name = "buttonAggiornaEmailDaInviare";
            this.buttonAggiornaEmailDaInviare.Size = new System.Drawing.Size(109, 23);
            this.buttonAggiornaEmailDaInviare.TabIndex = 1;
            this.buttonAggiornaEmailDaInviare.Text = "Aggiorna";
            this.buttonAggiornaEmailDaInviare.UseVisualStyleBackColor = true;
            this.buttonAggiornaEmailDaInviare.Click += new System.EventHandler(this.buttonAggiornaEmailDaInviare_Click);
            // 
            // dataGridViewEmailDaInviare
            // 
            this.dataGridViewEmailDaInviare.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEmailDaInviare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEmailDaInviare.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewEmailDaInviare.Name = "dataGridViewEmailDaInviare";
            this.dataGridViewEmailDaInviare.ReadOnly = true;
            this.dataGridViewEmailDaInviare.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEmailDaInviare.Size = new System.Drawing.Size(692, 387);
            this.dataGridViewEmailDaInviare.TabIndex = 0;
            this.dataGridViewEmailDaInviare.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewEmailDaInviare_CellDoubleClick);
            // 
            // tabPageEmailInviate
            // 
            this.tabPageEmailInviate.Controls.Add(this.groupBox1);
            this.tabPageEmailInviate.Controls.Add(this.buttonAggiornaEmailInviate);
            this.tabPageEmailInviate.Controls.Add(this.dataGridViewEmailInviate);
            this.tabPageEmailInviate.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmailInviate.Name = "tabPageEmailInviate";
            this.tabPageEmailInviate.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmailInviate.Size = new System.Drawing.Size(692, 425);
            this.tabPageEmailInviate.TabIndex = 1;
            this.tabPageEmailInviate.Text = "Email inviate";
            this.tabPageEmailInviate.UseVisualStyleBackColor = true;
            // 
            // timerInvioEmail
            // 
            this.timerInvioEmail.Tick += new System.EventHandler(this.timerInvioEmail_Tick);
            // 
            // backgroundWorkerInvioEmail
            // 
            this.backgroundWorkerInvioEmail.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerInvioEmail_RunWorkerCompleted);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "Email Sservice";
            this.notifyIcon.Visible = true;
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // dataGridViewEmailInviate
            // 
            this.dataGridViewEmailInviate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEmailInviate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEmailInviate.Location = new System.Drawing.Point(0, 90);
            this.dataGridViewEmailInviate.Name = "dataGridViewEmailInviate";
            this.dataGridViewEmailInviate.ReadOnly = true;
            this.dataGridViewEmailInviate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewEmailInviate.Size = new System.Drawing.Size(692, 297);
            this.dataGridViewEmailInviate.TabIndex = 1;
            // 
            // buttonAggiornaEmailInviate
            // 
            this.buttonAggiornaEmailInviate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAggiornaEmailInviate.Location = new System.Drawing.Point(577, 393);
            this.buttonAggiornaEmailInviate.Name = "buttonAggiornaEmailInviate";
            this.buttonAggiornaEmailInviate.Size = new System.Drawing.Size(109, 23);
            this.buttonAggiornaEmailInviate.TabIndex = 2;
            this.buttonAggiornaEmailInviate.Text = "Cerca";
            this.buttonAggiornaEmailInviate.UseVisualStyleBackColor = true;
            this.buttonAggiornaEmailInviate.Click += new System.EventHandler(this.buttonAggiornaEmailInviate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimePickerGiornoInvio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(692, 83);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parametri di ricerca";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Giorno Invio";
            // 
            // dateTimePickerGiornoInvio
            // 
            this.dateTimePickerGiornoInvio.Location = new System.Drawing.Point(78, 21);
            this.dateTimePickerGiornoInvio.Name = "dateTimePickerGiornoInvio";
            this.dateTimePickerGiornoInvio.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerGiornoInvio.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 451);
            this.Controls.Add(this.tabControlEmail);
            this.MinimumSize = new System.Drawing.Size(244, 180);
            this.Name = "MainForm";
            this.Text = "EmailInfo Control";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.tabControlEmail.ResumeLayout(false);
            this.tabPageEmailDaInviare.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmailDaInviare)).EndInit();
            this.tabPageEmailInviate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmailInviate)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlEmail;
        private System.Windows.Forms.TabPage tabPageEmailDaInviare;
        private System.Windows.Forms.TabPage tabPageEmailInviate;
        private System.Windows.Forms.DataGridView dataGridViewEmailDaInviare;
        private System.Windows.Forms.Button buttonAggiornaEmailDaInviare;
        private System.Windows.Forms.Timer timerInvioEmail;
        private System.ComponentModel.BackgroundWorker backgroundWorkerInvioEmail;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Button buttonAggiornaEmailInviate;
        private System.Windows.Forms.DataGridView dataGridViewEmailInviate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerGiornoInvio;
        private System.Windows.Forms.Label label1;
    }
}