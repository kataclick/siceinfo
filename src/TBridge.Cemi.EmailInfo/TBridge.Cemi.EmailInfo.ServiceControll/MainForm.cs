using System;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
using TBridge.Cemi.EmailInfo.Business;
using TBridge.Cemi.EmailInfo.Type.Collections;
using TBridge.Cemi.EmailInfo.Type.Entities;
using TBridge.Cemi.EmailInfo.Type.Filters;

namespace TBridge.Cemi.EmailInfo.ServiceController
{
    public partial class MainForm : Form
    {
        private readonly EmailBusiness biz;
        private readonly EmailInvio bizInvio;
        private string smtpServer = null;

        public MainForm()
        {
            InitializeComponent();

            biz = new EmailBusiness();
            bizInvio = new EmailInvio();

            InizializzaSmtpServer();
            InizializzaTimer();
            InizializzaBackgroundWorker();

            CaricaEmailDaInviare();
        }

        #region Metodi per l'inizializzazione

        private void InizializzaTimer()
        {
            timerInvioEmail.Interval = Int32.Parse(ConfigurationManager.AppSettings["IntervalloInvio"]);
        }

        private void InizializzaBackgroundWorker()
        {
            backgroundWorkerInvioEmail.DoWork += new DoWorkEventHandler(backgroundWorkerInvioEmail_DoWork);
        }

        private void InizializzaSmtpServer()
        {
            smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
        }

        #endregion

        private void buttonAggiornaEmailDaInviare_Click(object sender, EventArgs e)
        {
            CaricaEmailDaInviare();
        }

        private void CaricaEmailDaInviare()
        {
            EmailMessageFilter filtro = new EmailMessageFilter();
            EmailMessageCollection listaMail = biz.GetEMailDaInviare(filtro);
            dataGridViewEmailDaInviare.DataSource = listaMail;
        }

        private void CaricaEmailInviate()
        {
            dataGridViewEmailInviate.Enabled = false;

            EmailMessageFilter filtro = new EmailMessageFilter();
            filtro.GiornoInvio = dateTimePickerGiornoInvio.Value;
            EmailMessageCollection listaMail = biz.GetEmailInviate(filtro);
            dataGridViewEmailInviate.DataSource = listaMail;

            dataGridViewEmailInviate.Enabled = true;
        }

        private void backgroundWorkerInvioEmail_DoWork(object sender, DoWorkEventArgs e)
        {
            EmailMessageFilter filtro = new EmailMessageFilter();
            filtro.DaInviare = true;
            EmailMessageCollection listaMail = biz.GetEMailDaInviare(filtro);

            bizInvio.InviaEmail(smtpServer, listaMail);
        }

        private void timerInvioEmail_Tick(object sender, EventArgs e)
        {
            timerInvioEmail.Stop();
            backgroundWorkerInvioEmail.RunWorkerAsync();
        }

        private void backgroundWorkerInvioEmail_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerInvioEmail.Start();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            timerInvioEmail.Start();
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
                Hide();
        }

        private void dataGridViewEmailDaInviare_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EmailMessage email = (EmailMessage) dataGridViewEmailDaInviare.Rows[e.RowIndex].DataBoundItem;
            DetailsForm formDettagli = new DetailsForm(email);
            formDettagli.ShowDialog();
        }

        private void buttonAggiornaEmailInviate_Click(object sender, EventArgs e)
        {
            CaricaEmailInviate();
        }
    }
}