namespace TBridge.Cemi.EmailInfo.ServiceController
{
    partial class DetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxHeader = new System.Windows.Forms.GroupBox();
            this.labelDeliveryOptions = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBoxAllegati = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.labelSubject = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelPriority = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelReplyTo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.labelBCC = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelCC = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelFrom = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxBody = new System.Windows.Forms.GroupBox();
            this.groupBoxBodyHtml = new System.Windows.Forms.GroupBox();
            this.labelHTML = new System.Windows.Forms.Label();
            this.groupBoxBodyPlain = new System.Windows.Forms.GroupBox();
            this.labelPlain = new System.Windows.Forms.Label();
            this.labelSchedulated = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBoxHeader.SuspendLayout();
            this.groupBoxBody.SuspendLayout();
            this.groupBoxBodyHtml.SuspendLayout();
            this.groupBoxBodyPlain.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxHeader
            // 
            this.groupBoxHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxHeader.Controls.Add(this.labelDeliveryOptions);
            this.groupBoxHeader.Controls.Add(this.label2);
            this.groupBoxHeader.Controls.Add(this.checkBoxAllegati);
            this.groupBoxHeader.Controls.Add(this.label10);
            this.groupBoxHeader.Controls.Add(this.labelSubject);
            this.groupBoxHeader.Controls.Add(this.label8);
            this.groupBoxHeader.Controls.Add(this.labelPriority);
            this.groupBoxHeader.Controls.Add(this.label7);
            this.groupBoxHeader.Controls.Add(this.labelReplyTo);
            this.groupBoxHeader.Controls.Add(this.label6);
            this.groupBoxHeader.Controls.Add(this.labelBCC);
            this.groupBoxHeader.Controls.Add(this.label5);
            this.groupBoxHeader.Controls.Add(this.labelCC);
            this.groupBoxHeader.Controls.Add(this.label4);
            this.groupBoxHeader.Controls.Add(this.labelTo);
            this.groupBoxHeader.Controls.Add(this.label3);
            this.groupBoxHeader.Controls.Add(this.labelFrom);
            this.groupBoxHeader.Controls.Add(this.label1);
            this.groupBoxHeader.Location = new System.Drawing.Point(4, 23);
            this.groupBoxHeader.Name = "groupBoxHeader";
            this.groupBoxHeader.Size = new System.Drawing.Size(519, 160);
            this.groupBoxHeader.TabIndex = 0;
            this.groupBoxHeader.TabStop = false;
            this.groupBoxHeader.Text = "Header";
            // 
            // labelDeliveryOptions
            // 
            this.labelDeliveryOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDeliveryOptions.AutoSize = true;
            this.labelDeliveryOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDeliveryOptions.Location = new System.Drawing.Point(307, 141);
            this.labelDeliveryOptions.Name = "labelDeliveryOptions";
            this.labelDeliveryOptions.Size = new System.Drawing.Size(0, 13);
            this.labelDeliveryOptions.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(203, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Delivery Optionsi:";
            // 
            // checkBoxAllegati
            // 
            this.checkBoxAllegati.AutoSize = true;
            this.checkBoxAllegati.Enabled = false;
            this.checkBoxAllegati.Location = new System.Drawing.Point(79, 141);
            this.checkBoxAllegati.Name = "checkBoxAllegati";
            this.checkBoxAllegati.Size = new System.Drawing.Size(15, 14);
            this.checkBoxAllegati.TabIndex = 15;
            this.checkBoxAllegati.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Allegati:";
            // 
            // labelSubject
            // 
            this.labelSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSubject.AutoSize = true;
            this.labelSubject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubject.Location = new System.Drawing.Point(76, 124);
            this.labelSubject.Name = "labelSubject";
            this.labelSubject.Size = new System.Drawing.Size(0, 13);
            this.labelSubject.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Subject:";
            // 
            // labelPriority
            // 
            this.labelPriority.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPriority.AutoSize = true;
            this.labelPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPriority.Location = new System.Drawing.Point(76, 106);
            this.labelPriority.Name = "labelPriority";
            this.labelPriority.Size = new System.Drawing.Size(0, 13);
            this.labelPriority.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Priority:";
            // 
            // labelReplyTo
            // 
            this.labelReplyTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelReplyTo.AutoSize = true;
            this.labelReplyTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReplyTo.Location = new System.Drawing.Point(76, 88);
            this.labelReplyTo.Name = "labelReplyTo";
            this.labelReplyTo.Size = new System.Drawing.Size(0, 13);
            this.labelReplyTo.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Reply To:";
            // 
            // labelBCC
            // 
            this.labelBCC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBCC.AutoSize = true;
            this.labelBCC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBCC.Location = new System.Drawing.Point(76, 70);
            this.labelBCC.Name = "labelBCC";
            this.labelBCC.Size = new System.Drawing.Size(0, 13);
            this.labelBCC.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "BCC:";
            // 
            // labelCC
            // 
            this.labelCC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCC.AutoSize = true;
            this.labelCC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCC.Location = new System.Drawing.Point(76, 52);
            this.labelCC.Name = "labelCC";
            this.labelCC.Size = new System.Drawing.Size(0, 13);
            this.labelCC.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "CC:";
            // 
            // labelTo
            // 
            this.labelTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTo.AutoSize = true;
            this.labelTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTo.Location = new System.Drawing.Point(76, 34);
            this.labelTo.Name = "labelTo";
            this.labelTo.Size = new System.Drawing.Size(0, 13);
            this.labelTo.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "To:";
            // 
            // labelFrom
            // 
            this.labelFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFrom.AutoSize = true;
            this.labelFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFrom.Location = new System.Drawing.Point(76, 16);
            this.labelFrom.Name = "labelFrom";
            this.labelFrom.Size = new System.Drawing.Size(0, 13);
            this.labelFrom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "From:";
            // 
            // groupBoxBody
            // 
            this.groupBoxBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxBody.Controls.Add(this.groupBoxBodyHtml);
            this.groupBoxBody.Controls.Add(this.groupBoxBodyPlain);
            this.groupBoxBody.Location = new System.Drawing.Point(4, 189);
            this.groupBoxBody.Name = "groupBoxBody";
            this.groupBoxBody.Size = new System.Drawing.Size(519, 325);
            this.groupBoxBody.TabIndex = 1;
            this.groupBoxBody.TabStop = false;
            this.groupBoxBody.Text = "Body";
            // 
            // groupBoxBodyHtml
            // 
            this.groupBoxBodyHtml.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxBodyHtml.Controls.Add(this.labelHTML);
            this.groupBoxBodyHtml.Location = new System.Drawing.Point(6, 170);
            this.groupBoxBodyHtml.Name = "groupBoxBodyHtml";
            this.groupBoxBodyHtml.Size = new System.Drawing.Size(506, 148);
            this.groupBoxBodyHtml.TabIndex = 1;
            this.groupBoxBodyHtml.TabStop = false;
            this.groupBoxBodyHtml.Text = "HTML";
            // 
            // labelHTML
            // 
            this.labelHTML.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHTML.Location = new System.Drawing.Point(6, 25);
            this.labelHTML.Name = "labelHTML";
            this.labelHTML.Size = new System.Drawing.Size(494, 110);
            this.labelHTML.TabIndex = 0;
            // 
            // groupBoxBodyPlain
            // 
            this.groupBoxBodyPlain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxBodyPlain.Controls.Add(this.labelPlain);
            this.groupBoxBodyPlain.Location = new System.Drawing.Point(5, 15);
            this.groupBoxBodyPlain.Name = "groupBoxBodyPlain";
            this.groupBoxBodyPlain.Size = new System.Drawing.Size(508, 150);
            this.groupBoxBodyPlain.TabIndex = 0;
            this.groupBoxBodyPlain.TabStop = false;
            this.groupBoxBodyPlain.Text = "Plain Text";
            // 
            // labelPlain
            // 
            this.labelPlain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPlain.Location = new System.Drawing.Point(6, 25);
            this.labelPlain.Name = "labelPlain";
            this.labelPlain.Size = new System.Drawing.Size(496, 112);
            this.labelPlain.TabIndex = 0;
            // 
            // labelSchedulated
            // 
            this.labelSchedulated.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSchedulated.AutoSize = true;
            this.labelSchedulated.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSchedulated.Location = new System.Drawing.Point(97, 7);
            this.labelSchedulated.Name = "labelSchedulated";
            this.labelSchedulated.Size = new System.Drawing.Size(0, 13);
            this.labelSchedulated.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Schedulated:";
            // 
            // DetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 520);
            this.Controls.Add(this.labelSchedulated);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBoxBody);
            this.Controls.Add(this.groupBoxHeader);
            this.Name = "DetailsForm";
            this.Text = "EmailInfo Control";
            this.groupBoxHeader.ResumeLayout(false);
            this.groupBoxHeader.PerformLayout();
            this.groupBoxBody.ResumeLayout(false);
            this.groupBoxBodyHtml.ResumeLayout(false);
            this.groupBoxBodyPlain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxHeader;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelCC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelBCC;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelReplyTo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelPriority;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelSubject;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBoxBody;
        private System.Windows.Forms.GroupBox groupBoxBodyPlain;
        private System.Windows.Forms.Label labelPlain;
        private System.Windows.Forms.GroupBox groupBoxBodyHtml;
        private System.Windows.Forms.Label labelHTML;
        private System.Windows.Forms.Label labelSchedulated;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBoxAllegati;
        private System.Windows.Forms.Label labelDeliveryOptions;
        private System.Windows.Forms.Label label2;
    }
}