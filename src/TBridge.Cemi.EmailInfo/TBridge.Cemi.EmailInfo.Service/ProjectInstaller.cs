using System.ComponentModel;
using System.Configuration.Install;

namespace TBridge.Cemi.EmailInfo.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}