using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

namespace TBridge.Cemi.EmailInfo.Service
{
    internal partial class EmailService : ServiceBase
    {
        private readonly Timer timer;

        public EmailService()
        {
            InitializeComponent();

            timer = new Timer();
            timer.Elapsed += timer_Elapsed;
        }

        protected override void OnStart(string[] args)
        {
            KillExistingProcesses();

            try
            {
                StartController();


                timer.Interval = Int32.Parse(ConfigurationManager.AppSettings["timer"]);
                timer.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected override void OnStop()
        {
            KillExistingProcesses();
        }

        private static void StartController()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.WorkingDirectory = ConfigurationManager.AppSettings["working_directory"];
            startInfo.FileName = ConfigurationManager.AppSettings["executable_name"];
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            Process.Start(startInfo);
        }

        private static void KillExistingProcesses()
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }

        private static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            if (processes.Length == 0)
            {
                StartController();
            }
        }
    }
}