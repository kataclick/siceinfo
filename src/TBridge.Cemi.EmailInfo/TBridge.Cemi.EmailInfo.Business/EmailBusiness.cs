using System;
using TBridge.Cemi.EmailInfo.Data;
using TBridge.Cemi.EmailInfo.Type.Collections;
using TBridge.Cemi.EmailInfo.Type.Entities;
using TBridge.Cemi.EmailInfo.Type.Filters;

namespace TBridge.Cemi.EmailInfo.Business
{
    public class EmailBusiness
    {
        private readonly EmailDataAccess dataAccess = new EmailDataAccess();

        /// <summary>
        /// Invia la mail specificata come parametro
        /// </summary>
        /// <param name="email">Email da inviare</param>
        /// <param name="utenteInvio"></param>
        /// <returns>Stato dell'invio</returns>
        public bool InviaEmail(EmailMessageSerializzabile email, string utenteInvio)
        {
            if (email == null)
                throw new ArgumentNullException();

            if (email.Mittente == null || string.IsNullOrEmpty(email.Mittente.Indirizzo))
                throw new ArgumentNullException("Mittente");

            if (string.IsNullOrEmpty(email.BodyPlain) && string.IsNullOrEmpty(email.BodyHTML))
                throw new ArgumentNullException("BodyPlain e BodyHTML");

            if (email.Destinatari.Count == 0 || string.IsNullOrEmpty(email.Destinatari[0].Indirizzo))
                throw new ArgumentNullException("Destinatari");

            if (email.DataSchedulata == new DateTime())
                throw new ArgumentException("Data schedulata non valorizzato");

            return dataAccess.InviaEmail(email, utenteInvio);
        }

        /// <summary>
        /// Invia le mail specificate come parametro
        /// </summary>
        /// <param name="listaMail">Email da inviare</param>
        /// <param name="utenteInvio"></param>
        /// <returns>Email che non sono state inviate</returns>
        public EmailMessageSerializzabileCollection InviaEmail(EmailMessageSerializzabileCollection listaMail,
                                                               string utenteInvio)
        {
            EmailMessageSerializzabileCollection errori = new EmailMessageSerializzabileCollection();

            foreach (EmailMessageSerializzabile email in listaMail)
            {
                try
                {
                    if (!InviaEmail(email, utenteInvio))
                    {
                        errori.Add(email);
                    }
                }
                catch (Exception exc)
                {
                    //email.ErroreInvio = exc;
                    errori.Add(email);
                }
            }

            return errori;
        }

        /// <summary>
        /// Recupera le Email ancora da inviare che soddisfano il filtro di ricerca passato come parametro
        /// </summary>
        /// <param name="filtro">Filtro per la ricerca</param>
        /// <returns>Email trovate nel DB</returns>
        public EmailMessageCollection GetEMailDaInviare(EmailMessageFilter filtro)
        {
            return dataAccess.GetEMailDaInviare(filtro);
        }

        /// <summary>
        /// Recupera le Email ancora da inviare che soddisfano il filtro di ricerca passato come parametro
        /// </summary>
        /// <param name="filtro">Filtro per la ricerca</param>
        /// <param name="utenteInvio"></param>
        /// <returns>Email trovate nel DB</returns>
        public EmailMessageSerializzabileCollection GetEMailDaInviareSerializzabile(EmailMessageFilter filtro,
                                                                                    string utenteInvio)
        {
            return dataAccess.GetEMailDaInviareSerializzabile(filtro, utenteInvio);
        }

        /// <summary>
        /// Recupera le Email gi� inviate che soddisfano il filtro di ricerca passato come parametro
        /// </summary>
        /// <param name="filtro">Filtro per la ricerca</param>
        /// <returns>Email trovate nel DB</returns>
        public EmailMessageCollection GetEmailInviate(EmailMessageFilter filtro)
        {
            return dataAccess.GetEmailInviate(filtro);
        }
    }
}