using System.Net.Mail;
using System.Net.Mime;
using TBridge.Cemi.EmailInfo.Data;
using TBridge.Cemi.EmailInfo.Type.Collections;
using TBridge.Cemi.EmailInfo.Type.Entities;

namespace TBridge.Cemi.EmailInfo.Business
{
    /// <summary>
    /// Classe per l'invio, mediante SMTP server di un messaggio di posta
    /// </summary>
    public class EmailInvio
    {
        private readonly EmailDataAccess dataAccess = new EmailDataAccess();

        /// <summary>
        /// Invia la email passata come parametro mediante il server Smtp specificato
        /// </summary>
        /// <param name="smtpServer">Smtp server</param>
        /// <param name="email">Email da inviare</param>
        public void InviaEmail(string smtpServer, EmailMessage email)
        {
            SmtpClient smptClient = new SmtpClient(smtpServer);

            CreaDoppiaVisualizzazioneMail(email);
            CompletaMailConAllegati(email);

            try
            {
                smptClient.Send(email);
                dataAccess.UpdateEmailInviata(email);
            }
            catch
            {
                // Capire che fare. Propagare o loggare l'eccezione?
                throw;
            }
        }

        /// <summary>
        /// Invia le email passate come parametro mediante il server Smtp specificato
        /// </summary>
        /// <param name="smtpServer">Smtp server</param>
        /// <param name="listaEmail">Email da inviare</param>
        public void InviaEmail(string smtpServer, EmailMessageCollection listaEmail)
        {
            foreach (EmailMessage email in listaEmail)
            {
                try
                {
                    InviaEmail(smtpServer, email);
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Recupera gli allegati della email dal DB
        /// </summary>
        /// <param name="email">Email di cui repurerare gli allegati</param>
        private void CompletaMailConAllegati(EmailMessage email)
        {
            dataAccess.GetEmailAttachment(email);
        }

        /// <summary>
        /// Crea la visualizzazione "Plain" e "HTML"
        /// </summary>
        /// <param name="email">Email di cui creare la doppia visualizzazione</param>
        private static void CreaDoppiaVisualizzazioneMail(EmailMessage email)
        {
            // Create the Plain Text part
            if (!string.IsNullOrEmpty(email.Body))
            {
                AlternateView plain = AlternateView.CreateAlternateViewFromString(
                    email.Body,
                    null,
                    "text/plain");
                plain.TransferEncoding = TransferEncoding.QuotedPrintable;

                email.AlternateViews.Add(plain);
            }

            // Create the Html part
            if (!string.IsNullOrEmpty(email.BodyHTML))
            {
                AlternateView html = AlternateView.CreateAlternateViewFromString(
                    email.BodyHTML,
                    null,
                    "text/html");
                html.TransferEncoding = TransferEncoding.QuotedPrintable;

                email.AlternateViews.Add(html);
            }
        }
    }
}