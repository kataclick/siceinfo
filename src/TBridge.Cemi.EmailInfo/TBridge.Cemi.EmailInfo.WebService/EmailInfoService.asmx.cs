using System.ComponentModel;
using System.Security.Permissions;
using System.Web.Services;
using TBridge.Cemi.EmailInfo.Business;
using TBridge.Cemi.EmailInfo.Type.Collections;
using TBridge.Cemi.EmailInfo.Type.Entities;
using TBridge.Cemi.EmailInfo.Type.Filters;

namespace TBridge.Cemi.EmailInfo.WebService
{
    /// <summary>
    /// Summary description for EmailInfoService
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [PrincipalPermission(SecurityAction.Demand, Role = @"EmailSenders")]
    [PrincipalPermission(SecurityAction.Demand, Role = @"BUILTIN\Administrators")]
    public class EmailInfoService : System.Web.Services.WebService
    {
        /// <summary>
        /// Invia la mail specificata come parametro
        /// </summary>
        /// <param name="email">Email da inviare</param>
        /// <returns>Stato dell'invio</returns>
        [WebMethod]
        public bool InviaEmail(EmailMessageSerializzabile email)
        {
            EmailBusiness biz = new EmailBusiness();
            return biz.InviaEmail(email, User.Identity.Name);
        }

        /// <summary>
        /// Invia le mail specificate come parametro
        /// </summary>
        /// <param name="listaMail">Email da inviare</param>
        /// <returns>Email che non sono state inviate</returns>
        [WebMethod]
        public EmailMessageSerializzabileCollection InviaEmailCollection(EmailMessageSerializzabileCollection listaMail)
        {
            EmailBusiness biz = new EmailBusiness();
            return biz.InviaEmail(listaMail, User.Identity.Name);
        }

        /// <summary>
        /// Recupera le Email ancora da inviare che soddisfano il filtro di ricerca passato come parametro
        /// </summary>
        /// <param name="filtro">Filtro per la ricerca</param>
        /// <returns>Email trovate nel DB</returns>
        [WebMethod]
        public EmailMessageSerializzabileCollection GetEMailDaInviare(EmailMessageFilter filtro)
        {
            EmailBusiness biz = new EmailBusiness();
            return biz.GetEMailDaInviareSerializzabile(filtro, User.Identity.Name);
        }
    }
}