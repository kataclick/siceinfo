﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Timer = System.Timers.Timer;

namespace Cemi.OpenCloseSite.ConsoleApp
{
    internal class Program
    {
        private static Timer _timer;

        private static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["SICE"].ConnectionString;

        private static readonly string SiteRoot = ConfigurationManager.AppSettings["SiteRoot"];
        private static readonly string Services = ConfigurationManager.AppSettings["Services"];
        private static readonly int HourDeadline = Convert.ToInt32(ConfigurationManager.AppSettings["HourDeadline"]);

        private static readonly long TimerIntervalMilliseconds =
            Convert.ToInt64(ConfigurationManager.AppSettings["TimerMilliseconds"]);

        private static bool _checkEtl = true;
        private static bool _openSite;

        private static void Main(string[] args)
        {
            Console.WriteLine($"{DateTime.Now} - Funzione di apertura/chiusura sito e servizi - START");

            Console.WriteLine($"{DateTime.Now} - Chiusura sito... app_offline");

            SetAppOffline(true);

            Console.WriteLine($"{DateTime.Now} - Chiusura sito... servizi");

            foreach (string serviceName in Services.Split(';'))
            {
                StartStopService(serviceName, true);
            }

            SetTimer(TimerIntervalMilliseconds);

            while (_checkEtl)
            {
                //aspetto...
                Thread.Sleep(100);
            }
            
            if (_openSite)
            {
                //posso riaprire...
                Console.WriteLine($"{DateTime.Now} - Apertura sito... app_offline");

                SetAppOffline();

                Console.WriteLine($"{DateTime.Now} - Apertura sito... servizi");
                foreach (string serviceName in Services.Split(';'))
                {
                    StartStopService(serviceName);
                }
            }

            _timer.Dispose();
            Console.WriteLine($"{DateTime.Now} - Funzione di apertura/chiusura sito e servizi - STOP");
        }

        private static void SetTimer(long timerIntervalMilliseconds)
        {
            _timer = new Timer(timerIntervalMilliseconds);
            _timer.Elapsed += OnTimedEvent;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            //oltre limite temporale?
            if (DateTime.Now.Hour > HourDeadline)
            {
                Console.WriteLine($"{DateTime.Now} - Apertura sito... Superata ora limite");

                bool forDeadline = true;
                var countCheckSql = GetCountCheckSql(forDeadline);

                if (countCheckSql == 0)
                {
                    //Fallisce prima ancora di iniziare a caricare i dati, ad esempio perchè AS/400 NON PRONTO
                    ((Timer) source).Stop();
                    _openSite = true;
                    _checkEtl = false;
                    _timer.Stop();
                }
                else
                {
                    //Ha scritto solo inizio - ha cominciato a caricare ma errore nel mentre
                    ((Timer) source).Stop();
                    _openSite = false;
                    _checkEtl = false;
                    _timer.Stop();
                }
            }
            else
            {
                Console.WriteLine($"{DateTime.Now} - Apertura sito... CHECK ETL on DB");

                bool forDeadline = false;
                var countCheckSql = GetCountCheckSql(forDeadline);

                if (countCheckSql == 1)
                {
                    //c'è un record, quindi posso riaprire...
                    ((Timer) source).Stop();
                    _openSite = true;
                    _checkEtl = false;
                    _timer.Stop();
                }
            }
        }

        private static int GetCountCheckSql(bool forDeadline = false)
        {
            int ret = -1;

            var sqlCommand = forDeadline
                ? "SELECT COUNT(*) AS num from dbo.ETL WHERE idTipoETL = 1 AND CAST(dataInizio as date) = CAST(SYSDATETIME() as date) AND dataFine IS NULL"
                : "SELECT COUNT(*) AS num from dbo.ETL WHERE idTipoETL = 1 AND CAST(dataInizio as date) = CAST(SYSDATETIME() as date) AND CAST(dataFine as date) = CAST(SYSDATETIME() as date)";


            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlCommand, connection))
                {
                    connection.Open();
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ret = (int) reader["num"];
                    }
                }
            }

            return ret;
        }

        private static void SetAppOffline(bool close = false)
        {
            string oldNameFullPath = $"{SiteRoot}\\_app_offline.htm";
            string newNameFullPath = $"{SiteRoot}\\app_offline.htm";

            if (close)
            {
                Console.WriteLine($"{DateTime.Now} - Chiusura sito... app_offline");
                if (File.Exists(oldNameFullPath))
                {
                    File.Move(oldNameFullPath, newNameFullPath);
                }
            }
            else
            {
                Console.WriteLine($"{DateTime.Now} - Apertura sito... app_offline");
                if (File.Exists(newNameFullPath))
                {
                    File.Move(newNameFullPath, oldNameFullPath);
                }
            }
        }

        private static void StartStopService(string serviceName, bool stop = false)
        {
            ServiceController service = new ServiceController(serviceName);
            Console.WriteLine($"{DateTime.Now} - Stato Servizio: {serviceName} - {service.Status}");

            if (stop)
            {
                if (service.Status == ServiceControllerStatus.Running)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped);
                }
            }
            else
            {
                if (service.Status == ServiceControllerStatus.Stopped)
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running);
                }
            }

            Console.WriteLine($"{DateTime.Now} - Stato Servizio: {serviceName} - {service.Status}");
        }
    }
}