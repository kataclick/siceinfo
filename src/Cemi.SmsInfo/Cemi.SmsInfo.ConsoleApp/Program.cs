﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using Cemi.SmsInfo.Business;
using System.Globalization;

namespace Cemi.SmsInfo.ConsoleApp
{
    internal class Program
    {
        private static readonly ISmsManager _smsManager = new SmsManager();

        private static void Main(string[] args)
        {
            Console.WriteLine("-- Inizio procedura --");
            Console.WriteLine(DateTime.Now);

            try
            {
                if (args.Length == 0)
                {
                    while (true)
                    {
                        CicloRicezioneInvio();
                    }
                }
                else
                {
                    int defaultDelay = Convert.ToInt32(ConfigurationManager.AppSettings["VerificaInvioGiorni"]);
                    DateTime dataVerifica = DateTime.Today.AddDays(-defaultDelay);
                    if (args.Length > 1)
                    {
                        //CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                        DateTime date;
                        if (DateTime.TryParse(args[1], out date))
                            dataVerifica = date;
                    }

                    switch (args[0])
                    {
                        case "VerificaInvio":
                            VerificaInvio(dataVerifica, false);
                            break;
                        case "VerificaInvioChiusura":
                            VerificaInvio(dataVerifica, true);
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                TracciaErrore(exception.Message + " - Inner: " + exception.InnerException + " - Data: " + exception.Data);
            }

            Console.WriteLine("-- Fine procedura --");
            Console.WriteLine(DateTime.Now);
        }

        private static void CicloRicezioneInvio()
        {
            Console.WriteLine("-- Inizio ciclo --");
            Console.WriteLine(DateTime.Now);

            //Verifica richieste da carrier
            Console.WriteLine("* Get richieste *");
            Console.WriteLine(DateTime.Now);
            RiceviSmsCarrier();

            //Gestione richieste
            Console.WriteLine("* Gestione richieste *");
            Console.WriteLine(DateTime.Now);
            GestisciSmsRicevuti();

            //Verifica SMS in uscita
            Console.WriteLine("* Invio *");
            Console.WriteLine(DateTime.Now);
            InviaSmsInScadenza();

            Console.WriteLine("-- Fine ciclo --");
            Console.WriteLine(DateTime.Now);

            Thread.Sleep(Int32.Parse(ConfigurationManager.AppSettings["IntervalloControlliMillisecondi"]));
        }

        private static void GestisciSmsRicevuti()
        {
            _smsManager.GestisciSmsRicevuti();
        }

        private static void RiceviSmsCarrier()
        {
            _smsManager.RiceviSmsCarrier();
        }

        private static void InviaSmsInScadenza()
        {
            _smsManager.InviaSmsInScadenza();
        }

        private static void VerificaInvio(DateTime data, bool chiusuraAutomatica)
        {
            _smsManager.VerificaInfioSms(data, chiusuraAutomatica);
        }

        private static void TracciaErrore(string messaggioErrore)
        {
            string logSms = ConfigurationManager.AppSettings["LogSms"];

            Console.WriteLine(messaggioErrore);

            if (logSms == "true")
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry($"Eccezione SmsInfo: {messaggioErrore}");
                }
            }
        }
    }
}