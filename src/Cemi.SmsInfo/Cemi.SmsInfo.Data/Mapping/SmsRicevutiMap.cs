// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61

using Cemi.SmsInfo.Type.Domain;

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Cemi.SmsInfo.Data.Mapping
{

    // SmsRicevuti
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.21.1.0")]
    public partial class SmsRicevutiMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<SmsRicevuto>
    {
        public SmsRicevutiMap()
            : this("dbo")
        {
        }

        public SmsRicevutiMap(string schema)
        {
            ToTable(schema + ".SmsRicevuti");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName(@"idSmsRicevuto").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.IdSmsCarrier).HasColumnName(@"idSmsCarrier").IsRequired().HasColumnType("int");
            Property(x => x.NumeroMittente).HasColumnName(@"numeroMittente").IsRequired().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.Testo).HasColumnName(@"testo").IsRequired().HasColumnType("nvarchar").HasMaxLength(255);
            Property(x => x.DataRicezioneCarrier).HasColumnName(@"dataRicezioneCarrier").IsRequired().HasColumnType("smalldatetime");
            Property(x => x.Gestito).HasColumnName(@"gestito").IsRequired().HasColumnType("bit");
            Property(x => x.IdTipoEsitoGestioneSms).HasColumnName(@"idTipoEsitoGestioneSms").IsOptional().HasColumnType("int");
            Property(x => x.IdUtente).HasColumnName(@"idUtente").IsOptional().HasColumnType("int");
            Property(x => x.IdSmsInvioRisposta).HasColumnName(@"idSmsInvioRisposta").IsOptional().HasColumnType("int");
            Property(x => x.DataInserimentoRecord).HasColumnName(@"dataInserimentoRecord").IsOptional().HasColumnType("smalldatetime");

            // Foreign keys
            HasOptional(a => a.SmsInvii).WithMany(b => b.SmsRicevuti).HasForeignKey(c => c.IdSmsInvioRisposta).WillCascadeOnDelete(false); // FK_SmsRicevuti_SmsInvii
            HasOptional(a => a.TipiEsitoGestioneSms).WithMany(b => b.SmsRicevuti).HasForeignKey(c => c.IdTipoEsitoGestioneSms).WillCascadeOnDelete(false); // FK_SmsRicevuti_TipiEsitoGestioneSms
            HasOptional(a => a.Utenti).WithMany(b => b.SmsRicevuti).HasForeignKey(c => c.IdUtente).WillCascadeOnDelete(false); // FK_SmsRicevuti_Utenti
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
