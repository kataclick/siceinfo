﻿using System;

namespace Cemi.SmsInfo.Data
{
    public partial class SiceContext
    {
        private static void SiceContextStaticPartial()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");
        }

        partial void InitializePartial()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
    }
}
