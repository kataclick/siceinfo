﻿using Cemi.SmsInfo.Type.Enum;

namespace Cemi.SmsInfo.Type.Entity
{
    public class EsitoInvio
    {
        public TipoEsitoInvioStatus Status { get; set; }
        public TipoEsitoInvioReason Reason { get; set; }
        public string Numero { get; set; }
    }
}
