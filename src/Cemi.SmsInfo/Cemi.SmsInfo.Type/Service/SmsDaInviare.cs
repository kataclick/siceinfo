﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cemi.SmsInfo.Type.Service
{
    [DataContract]
    public class SmsDaInviare
    {
        [DataMember]
        public string Testo { get; set; }

        [DataMember]
        public TipologiaSms Tipologia { get; set; }

        [DataMember]
        public List<Destinatario> Destinatari { get; set; }

        [DataMember]
        public DateTime Data { get; set; }

        [DataContract]
        public class Destinatario
        {
            [DataMember]
            public string Numero { get; set; }

            [DataMember]
            public int? IdUtenteSiceInfo { get; set; }
        }
    }
}