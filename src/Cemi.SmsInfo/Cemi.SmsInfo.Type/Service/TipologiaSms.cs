﻿namespace Cemi.SmsInfo.Type.Service
{
    public enum TipologiaSms
    {
        Generico = 1,
        Prestazioni,
        Colonie,
        TuteScarpe,
        Ore,
        Pin,
        AccessoCantieri,
        Deleghe,
        Username
    }
}