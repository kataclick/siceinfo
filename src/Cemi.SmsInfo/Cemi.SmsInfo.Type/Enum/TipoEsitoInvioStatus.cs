﻿namespace Cemi.SmsInfo.Type.Enum
{
    public enum TipoEsitoInvioStatus
    {
        Inviato = 1,
        Bad = 3,
        Consegnato = 4
    }
}
