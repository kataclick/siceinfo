﻿namespace Cemi.SmsInfo.Type.Enum
{
    public enum TipoEsitoInvioReason
    {
        DaInviare = 0,
        Inviato = 1,                        // Il messaggio è stato inviato correttamente
        InElaborazione = 2,                 // Inviato al gateway, in attesa di presa in carico
        Consegnato = 4,                     // Consegnato
        NonInviatoErrPermanente = -1,       // Errore permanente non meglio specificato
        NonInviatoErrTemporaneo = -2,       // Errore temporaneo
        NonInviatoErrUnknown = -7,          // Errore sconosciuto
        NonInviatoErrLogin = -3,            // Errore di autenticazione presso il provider
        NonInviatoErrPlafond = -4,          // Credito esaurito
        NonInviatoErrParametri = -5,        // Parametri errati o mancanti
        NonInviatoErrWebService = -6,       // Errore di rete
        Na = -1000                          // Not Applicable 
    }
}
