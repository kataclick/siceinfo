﻿namespace Cemi.SmsInfo.Type.Enum
{
    public enum TipoEsitoGestioneSms
    {
        ErroreGenerico = 1,
        NumeroGiaAttivo,
        RegistrazioneServizio,
        CancellazioneServizio,
        AggiornamentoRegistrazioneServizio,
        UsernameAssente,
        UtenteSconosciuto,
        PinLavoratore = 8
    }
}