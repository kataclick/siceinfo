﻿using System;

namespace Cemi.SmsInfo.Type.Crm
{
    public class SmsNotifica
    {
        public string Numero { get; set; }
        public int IdLavoratore { get; set; }
        public DateTime Data { get; set; }
        public string Testo { get; set; }
    }
}
