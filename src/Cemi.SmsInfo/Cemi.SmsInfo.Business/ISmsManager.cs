﻿using System;
using System.Collections.Generic;
using Cemi.SmsInfo.Type.Service;

namespace Cemi.SmsInfo.Business
{
    public interface ISmsManager
    {
        void GestisciSmsRicevuti();
        int InviaSmsInScadenza();
        bool RiceviSmsCarrier();
        bool SchedulaInvioSms(SmsDaInviare smsDaInviare);
        bool SchedulaInvioSms(List<SmsDaInviare> listaSmsDaInviare);
        void VerificaInfioSms(DateTime dataVerifica, bool chiusuraAutomatica);
    }
}