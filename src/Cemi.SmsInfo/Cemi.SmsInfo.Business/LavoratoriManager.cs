﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cemi.SmsInfo.Data;
using Cemi.SmsInfo.Type.Domain;

namespace Cemi.SmsInfo.Business
{
    internal class LavoratoriManager
    {
        public int GetIdUtenteLavoratoreByCodiceFiscale(string codiceFiscale)
        {
            int ret = -1;

            using (var context = new SiceContext())
            {
                List<int?> listaIdUtenti = (from lavoratore in context.Lavoratori
                    where
                        lavoratore.CodiceFiscale == codiceFiscale && !lavoratore.DataDecesso.HasValue &&
                        lavoratore.PosizioneValida == "S"
                    select lavoratore.IdUtente).ToList();

                if (listaIdUtenti.Count == 1 && listaIdUtenti[0].HasValue)
                {
                    ret = listaIdUtenti[0].Value;
                }
            }

            return ret;
        }

        public int GetIdUtenteLavoratoreByCellulare(string cellulare)
        {
            int ret = -1;
            cellulare = SmsManager.PulisciNumeroCellulare(cellulare);

            using (var context = new SiceContext())
            {
                List<int?> listaIdUtenti = (from lavoratore in context.Lavoratori
                                            from sms in context.SmsRecapitiUtenti.Where(x => lavoratore.IdUtente == x.IdUtente && x.DataFineValidita == null).DefaultIfEmpty()
                                            //join sms in context.SmsRecapitiUtenti
                                            //on lavoratore.IdUtente equals new { sms.IdUtente, sms.DataFineValidita == DBNull }
                                            where
                                                sms.Numero == cellulare
                                                && !lavoratore.DataDecesso.HasValue &&
                                                lavoratore.PosizioneValida == "S"
                                            select lavoratore.IdUtente).ToList();

                if (listaIdUtenti.Count == 1 && listaIdUtenti[0].HasValue)
                {
                    ret = listaIdUtenti[0].Value;
                }
            }

            return ret;
        }

        public bool NumeroDisponibile(string numero)
        {
            bool ret = false;
            using (var context = new SiceContext())
            {
                var queryNumeroNonDisponibile = from smsRecapitoUtente in context.SmsRecapitiUtenti
                    where
                        smsRecapitoUtente.Numero == numero &&
                        smsRecapitoUtente.DataFineValidita == null
                    select smsRecapitoUtente;

                var recapitiStessoNumeroAttivi = queryNumeroNonDisponibile.ToList();

                if (recapitiStessoNumeroAttivi.Count == 0)
                    ret = true;
            }
            return ret;
        }

        public bool SaveRecapitoSmsLavoratore(int idUtente, string numero)
        {
            int num = -1;

            numero = SmsManager.PulisciNumeroCellulare(numero);

            if (SmsManager.NumeroCellulareValido(numero))
            {
                using (var context = new SiceContext())
                {
                    var queryNumeroNonDisponibile = from smsRecapitoUtente in context.SmsRecapitiUtenti
                        where
                            smsRecapitoUtente.Numero == numero &&
                            smsRecapitoUtente.DataFineValidita == null
                        select smsRecapitoUtente;

                    var recapitiStessoNumeroAttivi = queryNumeroNonDisponibile.ToList();

                    if (recapitiStessoNumeroAttivi.Count == 0)
                    {
                        var queryRecapitoAttivo = from recapitoUtente in context.SmsRecapitiUtenti
                            where
                                recapitoUtente.IdUtente == idUtente &&
                                recapitoUtente.DataFineValidita == null
                            select recapitoUtente;

                        SmsRecapitoUtente smsRecapitoUtenteAttivo = queryRecapitoAttivo.SingleOrDefault();

                        if (smsRecapitoUtenteAttivo == null || smsRecapitoUtenteAttivo.Numero != numero)
                        {
                            if (smsRecapitoUtenteAttivo != null && smsRecapitoUtenteAttivo.Numero != numero)
                            {
                                smsRecapitoUtenteAttivo.DataFineValidita = DateTime.Now;
                            }

                            SmsRecapitoUtente nuovoRecapito = new SmsRecapitoUtente
                            {
                                IdUtente = idUtente,
                                Numero = numero,
                                DataInizioValidita = DateTime.Now,
                                DataFineValidita = null,
                                DataInserimentoRecord = DateTime.Now
                            };

                            context.SmsRecapitiUtenti.Add(nuovoRecapito);
                        }
                    }

                    num = context.SaveChanges();
                }
            }

            return num > 0;
        }

        public bool NumeroAttivo(int idUtente, string numero)
        {
            bool ret = false;

            numero = SmsManager.PulisciNumeroCellulare(numero);

            if (SmsManager.NumeroCellulareValido(numero))
            {
                using (var context = new SiceContext())
                {
                    var queryRecapiti = from smsRecapitoUtente in context.SmsRecapitiUtenti
                        where
                            smsRecapitoUtente.Numero == numero &&
                            smsRecapitoUtente.DataFineValidita == null &&
                            smsRecapitoUtente.IdUtente == idUtente
                        select smsRecapitoUtente;

                    var recapitiAttivi = queryRecapiti.ToList();

                    if (recapitiAttivi.Count == 1)
                    {
                        ret = true;
                    }
                }
            }

            return ret;
        }

        public bool DisabilitaRecapito(int idUtente)
        {
            int num = 0;

            using (var context = new SiceContext())
            {
                var query = from recapito in context.SmsRecapitiUtenti
                    where recapito.IdUtente == idUtente && recapito.DataFineValidita == null
                    select recapito;

                SmsRecapitoUtente smsRecapitoUtente = query.SingleOrDefault();
                if (smsRecapitoUtente != null)
                {
                    smsRecapitoUtente.DataFineValidita = DateTime.Now;

                    num = context.SaveChanges();
                }
            }

            return num > 0;
        }

        public string GeneraPin(int idUtente)
        {
            string pin = string.Empty;

            using (var context = new SiceContext())
            {
                var lavoratore = (from lav in context.Lavoratori
                    where lav.IdUtente == idUtente
                    select lav).SingleOrDefault();

                if (lavoratore != null)
                {
                    lavoratore.Pin = GeneraPin();
                    lavoratore.DataGenerazionePin = DateTime.Now;

                    int num = context.SaveChanges();
                    if (num > 0)
                        pin = lavoratore.Pin;
                }
            }

            return pin;
        }

        private string GeneraPin()
        {
            return Guid.NewGuid().ToString().ToUpper().Substring(0, 8);
        }

        public bool SaveNotificaInvioSmsPin(int idUtente)
        {
            int num;
            using (var context = new SiceContext())
            {
                var queryLavoratore = from lavoratore in context.Lavoratori
                    where lavoratore.IdUtente == idUtente
                    select lavoratore;

                Lavoratore lav = queryLavoratore.SingleOrDefault();

                if (lav != null)
                    lav.DataInvioSmsPin = DateTime.Now;

                num = context.SaveChanges();
            }

            return num > 0;
        }
    }
}