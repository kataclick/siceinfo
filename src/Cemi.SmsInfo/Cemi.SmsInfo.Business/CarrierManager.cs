﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Cemi.SmsInfo.Business.SmsCarrierService;
using Cemi.SmsInfo.Type.Domain;
using Cemi.SmsInfo.Type.Entity;
using Cemi.SmsInfo.Type.Enum;

namespace Cemi.SmsInfo.Business
{
    internal class CarrierManager
    {
        public CarrierManager()
        {
            Username = ConfigurationManager.AppSettings["SmsUsername"];
            Password = ConfigurationManager.AppSettings["SmsPassword"];
            CarrierSmsSender = new SmsSenderSoapClient();
        }

        private string Username { get; }
        private string Password { get; }
        public SmsSenderSoapClient CarrierSmsSender { get; set; }

        public int RegistraSms(string testo, List<string> destinatari)
        {
            ArrayOfString destinatariArray = new ArrayOfString();
            destinatariArray.AddRange(destinatari);

            for (int index = 0; index < destinatariArray.Count; index++)
            {
                string destinatario = destinatariArray[index];
                if (!destinatario.StartsWith("+39") && !destinatario.StartsWith("0039"))
                    destinatariArray[index] = "+39" + destinatario;
            }

            int idCarrier = CarrierSmsSender.InserisciMessaggio(Username, Password, 1, "Cassa Edile", testo, null, null,
                destinatariArray, false);

            return idCarrier;
        }

        public int InviaSms(int idSmsCarrier)
        {
            return CarrierSmsSender.InviaMessaggio(Username, Password, idSmsCarrier);
        }

        public List<SmsRicevuto> GetSmsRicevuti()
        {
            List<SmsRicevuto> listaSmsRicevuti = new List<SmsRicevuto>();

            ArrayOfSt_MessaggioRicevuto messaggiRicevuti = CarrierSmsSender.MessaggiRicevuti(Username, Password);
            foreach (st_MessaggioRicevuto messaggioRicevuto in messaggiRicevuti)
            {
                SmsRicevuto smsRicevuto = new SmsRicevuto
                {
                    IdSmsCarrier = messaggioRicevuto.IDMessaggio,
                    NumeroMittente = SmsManager.PulisciNumeroCellulare(messaggioRicevuto.Mittente),
                    DataRicezioneCarrier = messaggioRicevuto.DataRicezione,
                    Testo = messaggioRicevuto.Testo,
                    Gestito = false,
                    DataInserimentoRecord = DateTime.Now
                };

                listaSmsRicevuti.Add(smsRicevuto);
            }

            return listaSmsRicevuti;
        }

        public bool SetAck(int idSmsCarrier)
        {
            return CarrierSmsSender.SetAck(idSmsCarrier) != -1;
        }

        public List<EsitoInvio> GetEsitoInvio(int idSmsCarrier)
        {
            List<EsitoInvio> esiti = new List<EsitoInvio>();

            ArrayOfSt_LogInvio logInvioArray = CarrierSmsSender.LogInvio(Username, Password, idSmsCarrier);

            if (logInvioArray.Count > 0)
            {
                foreach (st_LogInvio logInvio in logInvioArray)
                {
                    EsitoInvio esito = new EsitoInvio
                    {
                        Status = (TipoEsitoInvioStatus) logInvio.StatoContatto,
                        Reason = (TipoEsitoInvioReason) logInvio.SottoStato,
                        Numero = logInvio.Telefono
                    };

                    esiti.Add(esito);
                }
            }

            return esiti;
        }
    }
}