﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using Cemi.SmsInfo.CrmConnector;
using Cemi.SmsInfo.Data;
using Cemi.SmsInfo.Type.Crm;
using Cemi.SmsInfo.Type.Domain;
using Cemi.SmsInfo.Type.Entity;
using Cemi.SmsInfo.Type.Enum;
using Cemi.SmsInfo.Type.Service;
using TipoEsitoGestioneSms = Cemi.SmsInfo.Type.Enum.TipoEsitoGestioneSms;

namespace Cemi.SmsInfo.Business
{
    public class SmsManager : ISmsManager
    {
        private readonly CarrierManager _carrierManager = new CarrierManager();
        private readonly LavoratoriManager _lavoratoriManager = new LavoratoriManager();

        private readonly List<string> _listaNumeriCellTest =
            new List<string>(ConfigurationManager.AppSettings["NumeroCellTest"].Split(';'));

        private readonly bool _notificaCrm = ConfigurationManager.AppSettings["NotificaCrm"] == "true";

        private readonly bool _test = ConfigurationManager.AppSettings["Test"] == "true";

        public int InviaSmsInScadenza()
        {
            int numeroRecordAggiornati = 0;

            List<SmsNotifica> listaSmsNotifica = new List<SmsNotifica>();

            //recupero gli sms scaduti
            using (var context = new SiceContext())
            {
                var query = from smsInvio in context.SmsInvii
                    where !smsInvio.DataInvio.HasValue && smsInvio.DataSchedulata <= DateTime.Now
                    select smsInvio;

                if (_test)
                {
                    query = from smsInvio in query
                        where _listaNumeriCellTest.Contains(smsInvio.Numero)
                        select smsInvio;
                }

                var queryGroup = from invio in query
                    group invio by invio.Sms
                    into g
                    select g;

                List<IGrouping<Sms, SmsInvio>> listGroup = queryGroup.ToList();

                foreach (IGrouping<Sms, SmsInvio> groupSmsInvio in listGroup)
                {
                    Sms smsDaInviare = groupSmsInvio.Key;
                    List<SmsInvio> listaSmsInvio = groupSmsInvio.ToList();

                    var queryNumeri = from invio in listaSmsInvio
                        select invio.Numero;
                    List<string> numeri = queryNumeri.ToList();

                    int idCarrierSms = _carrierManager.RegistraSms(smsDaInviare.Testo, numeri);
                    if (idCarrierSms >= 0)
                    {
                        smsDaInviare.IdSmsCarrier = idCarrierSms;

                        int idCarrierInvio = _carrierManager.InviaSms(idCarrierSms);
                        if (idCarrierInvio >= 0)
                        {
                            foreach (SmsInvio smsInvio in listaSmsInvio)
                            {
                                smsInvio.DataInvio = DateTime.Now;

                                SmsInvio smsInviato = smsInvio;
                                int idLavoratore = (from lavoratore in context.Lavoratori
                                    where lavoratore.IdUtente == smsInviato.IdUtente
                                    select lavoratore.Id).SingleOrDefault();

                                if (idLavoratore > 0)
                                {
                                    listaSmsNotifica.Add(new SmsNotifica
                                    {
                                        Data = smsInvio.DataInvio.Value,
                                        IdLavoratore = idLavoratore,
                                        Numero = smsInvio.Numero,
                                        Testo = smsInvio.Sms.Testo
                                    });
                                }
                            }

                            numeroRecordAggiornati += context.SaveChanges();
                        }
                    }
                }
            }

            #region Notifica CRM

            if (_notificaCrm && numeroRecordAggiornati > 0 && listaSmsNotifica.Count > 0)
            {
                NotificationManager.NotificaSms(listaSmsNotifica);
            }

            #endregion

            return numeroRecordAggiornati;
        }

        public bool SchedulaInvioSms(SmsDaInviare smsDaInviare)
        {
            //per inviare un sms bisogna schedularlo
            //le tabelle di riferimento sono SmsInvii e Sms
            int numRecord;

            using (var context = new SiceContext())
            {
                Sms sms = new Sms
                {
                    Testo = smsDaInviare.Testo,
                    IdTipoSms = (int) smsDaInviare.Tipologia,
                    DataInserimentoRecord = DateTime.Now,
                    IdSmsCarrier = -1,
                    SmsInvii = (from destinatario in smsDaInviare.Destinatari
                        where NumeroCellulareValido(PulisciNumeroCellulare(destinatario.Numero))
                        select new SmsInvio
                        {
                            Numero = PulisciNumeroCellulare(destinatario.Numero),
                            IdUtente = destinatario.IdUtenteSiceInfo,
                            DataSchedulata = smsDaInviare.Data,
                            DataInserimentoRecord = DateTime.Now
                        }).ToList()
                };

                context.Sms.Add(sms);

                numRecord = context.SaveChanges();
            }

            return numRecord > 0;
        }

        public bool SchedulaInvioSms(List<SmsDaInviare> listaSmsDaInviare)
        {
            //per inviare un sms bisogna schedularlo
            //le tabelle di riferimento sono SmsInvii e Sms
            int numRecord;

            using (var context = new SiceContext())
            {
                foreach (SmsDaInviare smsDaInviare in listaSmsDaInviare)
                {
                    Sms sms = new Sms
                    {
                        Testo = smsDaInviare.Testo,
                        IdTipoSms = (int) smsDaInviare.Tipologia,
                        DataInserimentoRecord = DateTime.Now,
                        IdSmsCarrier = -1,
                        SmsInvii = (from destinatario in smsDaInviare.Destinatari
                            where NumeroCellulareValido(PulisciNumeroCellulare(destinatario.Numero))
                            select new SmsInvio
                            {
                                Numero = PulisciNumeroCellulare(destinatario.Numero),
                                IdUtente = destinatario.IdUtenteSiceInfo,
                                DataSchedulata = smsDaInviare.Data,
                                DataInserimentoRecord = DateTime.Now
                            }).ToList()
                    };

                    context.Sms.Add(sms);
                }

                numRecord = context.SaveChanges();
            }

            return numRecord > 0;
        }

        public bool RiceviSmsCarrier()
        {
            int numRecord = 0;

            List<SmsRicevuto> listaSmsRicevuti = _carrierManager.GetSmsRicevuti();

            if (_test)
            {
                listaSmsRicevuti = (from smsRicevuto in listaSmsRicevuti
                    where _listaNumeriCellTest.Contains(smsRicevuto.NumeroMittente)
                    select smsRicevuto).ToList();
            }

            if (listaSmsRicevuti.Count > 0)
            {
                using (var context = new SiceContext())
                {
                    foreach (SmsRicevuto smsRicevuto in listaSmsRicevuti)
                    {
                        context.SmsRicevuti.Add(smsRicevuto);
                        int num = context.SaveChanges();
                        if (num > 0)
                        {
                            _carrierManager.SetAck(smsRicevuto.IdSmsCarrier);
                            numRecord++;
                        }
                    }
                }
            }

            return numRecord > 0;
        }

        public void GestisciSmsRicevuti()
        {
            //recupero gli sms da db non ancora gestiti
            List<SmsRicevuto> listaSmsRicevutiNonGestiti;
            using (var context = new SiceContext())
            {
                var querySmsRicevuti = from smsRicevuto in context.SmsRicevuti
                    where !smsRicevuto.Gestito
                    select smsRicevuto;

                listaSmsRicevutiNonGestiti = querySmsRicevuti.ToList();
            }

            //li gestisco
            foreach (SmsRicevuto smsRicevuto in listaSmsRicevutiNonGestiti)
            {
                GestisciSms(smsRicevuto);
            }
        }

        public void VerificaInfioSms(DateTime dataVerifica, bool chiusuraAutomatica)
        {
            using (var context = new SiceContext())
            {
                var querySmsInviati = from smsInvio in context.SmsInvii.Include("Sms")
                    where
                        smsInvio.DataInvio.HasValue
                        && DbFunctions.TruncateTime(smsInvio.DataInvio.Value) == dataVerifica
                    select smsInvio;

                List<SmsInvio> smsInviatiList = querySmsInviati.ToList();

                List<SmsInvio> smsInviatiListDistinct =
                    smsInviatiList.GroupBy(smsInviati => smsInviati.Sms.IdSmsCarrier).Select(g => g.First()).ToList();

                foreach (SmsInvio smsInvio in smsInviatiListDistinct)
                {
                    List<EsitoInvio> esitiInvio = _carrierManager.GetEsitoInvio(smsInvio.Sms.IdSmsCarrier);

                    foreach (EsitoInvio esitoInvio in esitiInvio)
                    {
                        SmsInviatoEsito esito = new SmsInviatoEsito
                        {
                            IdSms = smsInvio.Sms.Id,
                            IdSmsCarrier = smsInvio.Sms.IdSmsCarrier,
                            Status = (int) esitoInvio.Status,
                            Reason = (int) esitoInvio.Reason,
                            DataInserimentoRecord = DateTime.Now,
                            Numero = esitoInvio.Numero
                        };

                        context.SmsInviatiEsiti.Add(esito);


                        if (chiusuraAutomatica)
                        {
                            if (esitoInvio.Status == TipoEsitoInvioStatus.Bad &&
                                esitoInvio.Reason == TipoEsitoInvioReason.NonInviatoErrPermanente)
                            {
                                var recapito = (from recapitoUtente in context.SmsRecapitiUtenti
                                    where
                                        !recapitoUtente.DataFineValidita.HasValue
                                        && recapitoUtente.Numero == smsInvio.Numero
                                    select recapitoUtente).SingleOrDefault();

                                if (recapito != null)
                                {
                                    recapito.DataFineValidita = DateTime.Now;
                                }
                            }
                        }
                    }
                }

                context.SaveChanges();
            }
        }

        private void GestisciSms(SmsRicevuto smsRicevuto)
        {
            // divido il testo in base al *
            // *codicefiscale = registrazione
            // *codicefiscale*STOP = cancellazione
            // *codicefiscale*PIN = pin
            // *codicefiscale*LOGIN = credenziali
            // *TS*numero_taglia = richiesta scarpe - OLD: NON PIU' SUPPORTATO
            // *TS*SI o *TS*NO = feedback servizio TS
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());

            if (partiMessaggio.Length == 2)
            {
                if (partiMessaggio[1].ToUpper() == "STOP")
                {
                    //cancellazione
                    CancellaLavoratoreSms(smsRicevuto, "numero");
                }
                else if (partiMessaggio[1].ToUpper() == "PIN")
                {
                    //richiesta pin
                    FornisciPinLavoratore(smsRicevuto, "numero");
                }
                else
                {
                    //registrazione
                    RegistraLavoratoreSms(smsRicevuto);
                }
                
            }
            else if (partiMessaggio.Length == 3)
            {
                /*
                if (partiMessaggio[1].ToUpper() == "TS")
                {
                    //TS
                    //RegistraRichiestaTs(smsRicevuto);
                }
                else */
                if (partiMessaggio[2].ToUpper() == "STOP")
                {
                    //cancellazione
                    CancellaLavoratoreSms(smsRicevuto, "codice");
                }
                else if (partiMessaggio[2].ToUpper() == "PIN")
                {
                    //richiesta pin
                    FornisciPinLavoratore(smsRicevuto, "codice");
                }
                //else if ((partiMessaggio[2]).ToUpper() == "LOGIN")
                //{
                //richiesta login
                //FornisciLoginLavoratore(smsRicevuto);
                //}
            }
            else
            {
                //Formato non corretto
                RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
            }
        }

        private void RegistraLavoratoreSms(SmsRicevuto smsRicevuto)
        {
            string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
            string codiceFiscale = partiMessaggio[1].Trim();
            if (codiceFiscale.Length == 16 && CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codiceFiscale))
            {
                //Il CF sembra corretto. Faccio gli ulteriori controlli

                //Utente presente come lavoratore?
                int idUtente = _lavoratoriManager.GetIdUtenteLavoratoreByCodiceFiscale(codiceFiscale);
                if (idUtente > 0)
                {
                    //verifico che il numero non sia già attivo per altri...
                    if (_lavoratoriManager.NumeroDisponibile(smsRicevuto.NumeroMittente))
                    {
                        //Nuovo recapito o aggiornamento
                        if (_lavoratoriManager.SaveRecapitoSmsLavoratore(idUtente, smsRicevuto.NumeroMittente))
                        {
                            //tutto ok
                            RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.RegistrazioneServizio);
                        }
                        else
                        {
                            //errore imprevisto
                            RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                        }
                    }
                    else
                    {
                        //Numero già attivo (per qualcuno)
                        RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.NumeroGiaAttivo);
                    }
                }
                else
                {
                    //Non si riesce ad associare in maniera univoca ad un lavoratore
                    RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.UtenteSconosciuto);
                }
            }
            else
            {
                //CF non valido
                RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
            }
        }

        private void CancellaLavoratoreSms(SmsRicevuto smsRicevuto, string tipoSms)
        {            
            int idUtente = 0;

            // Ricerco l'idUtente in base al tipo di SMS ricevuto (Con codice fiscale o senza)
            if (tipoSms == "codice")
            {
                string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
                string codiceFiscale = partiMessaggio[1].Trim();
                if (codiceFiscale.Length == 16 && CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codiceFiscale))
                {
                    idUtente = _lavoratoriManager.GetIdUtenteLavoratoreByCodiceFiscale(codiceFiscale);
                }
                else
                {
                    //CF non valido
                    RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                }
            }
            else if (tipoSms == "numero")
            {
                idUtente = _lavoratoriManager.GetIdUtenteLavoratoreByCellulare(smsRicevuto.NumeroMittente);
            }

            if (idUtente > 0)
            {
                //verifico che il numero sia registrato e attivo per tale utente
                if (_lavoratoriManager.NumeroAttivo(idUtente, smsRicevuto.NumeroMittente))
                {
                    //Disabilito il recapito
                    if (_lavoratoriManager.DisabilitaRecapito(idUtente))
                    {
                        //tutto ok
                        RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.CancellazioneServizio);
                    }
                    else
                    {
                        //errore imprevisto
                        RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                    }
                }
                else
                {
                    //Associazione numero-utente errata
                    RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                }
            }
            else
            {
                //Non si riesce ad associare in maniera univoca ad un lavoratore
                RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.UtenteSconosciuto);
            }            
        }

        private void FornisciPinLavoratore(SmsRicevuto smsRicevuto, string tipoSms)
        {
            int idUtente = 0;

            if (tipoSms == "codice")
            {
                string[] partiMessaggio = smsRicevuto.Testo.Split("*".ToCharArray());
                string codiceFiscale = partiMessaggio[1].Trim();
                if (codiceFiscale.Length == 16 && CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codiceFiscale))
                {
                    idUtente = _lavoratoriManager.GetIdUtenteLavoratoreByCodiceFiscale(codiceFiscale);
                }
                else
                {
                    //CF non valido
                    RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                }
            }
            else if (tipoSms == "numero")
            {
                idUtente = _lavoratoriManager.GetIdUtenteLavoratoreByCellulare(smsRicevuto.NumeroMittente);
            }
            
            if (idUtente > 0)
            {
                //verifico che il numero sia registrato e attivo per tale utente
                if (_lavoratoriManager.NumeroAttivo(idUtente, smsRicevuto.NumeroMittente))
                {
                    //Invio il PIN
                    string pin = _lavoratoriManager.GeneraPin(idUtente);
                    if (!string.IsNullOrEmpty(pin))
                    {
                        //Pin generato - lo invio
                        if (RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.PinLavoratore, pin))
                            _lavoratoriManager.SaveNotificaInvioSmsPin(idUtente);
                    }
                    else
                    {
                        //errore imprevisto
                        RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                    }
                }
                else
                {
                    //Associazione numero-utente errata
                    RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.ErroreGenerico);
                }
            }
            else
            {
                //Non si riesce ad associare in maniera univoca ad un lavoratore
                RispostaGestioneSms(smsRicevuto, TipoEsitoGestioneSms.UtenteSconosciuto);
            }
            
        }

        private bool RispostaGestioneSms(SmsRicevuto smsRicevuto, TipoEsitoGestioneSms tipoEsitoGestioneSms, string pin = null)
        {
            bool ret = false;

            string messaggioRisposta;
            using (var context = new SiceContext())
            {
                var query = from tipoEsitoGestioneSmse in context.TipiEsitoGestioneSms
                    where tipoEsitoGestioneSmse.Id == (int) tipoEsitoGestioneSms
                    select tipoEsitoGestioneSmse.TemplateMessaggio;

                messaggioRisposta = query.Single();
            }

            if (tipoEsitoGestioneSms == TipoEsitoGestioneSms.PinLavoratore && pin != null)
            {
                messaggioRisposta = string.Format(messaggioRisposta, pin);
            }

            SmsDaInviare smsDaInviare = new SmsDaInviare
            {
                Tipologia = TipologiaSms.Generico,
                Testo = messaggioRisposta,
                Data = DateTime.Now,
                Destinatari =
                    new List<SmsDaInviare.Destinatario>
                    {
                        new SmsDaInviare.Destinatario
                        {
                            Numero = smsRicevuto.NumeroMittente,
                            IdUtenteSiceInfo = smsRicevuto.IdUtente
                        }
                    }
            };

            bool invio = SchedulaInvioSms(smsDaInviare);

            if (invio)
            {
                using (var context = new SiceContext())
                {
                    context.SmsRicevuti.Attach(smsRicevuto);

                    smsRicevuto.Gestito = true;
                    smsRicevuto.IdTipoEsitoGestioneSms = (int) tipoEsitoGestioneSms;
                    //smsRicevuto.IdUtente
                    //smsRicevuto.IdSmsInvioRisposta

                    int num = context.SaveChanges();
                    ret = num > 0;
                }
            }

            return ret;
        }

        public static string PulisciNumeroCellulare(string numero)
        {
            string numeroPulito = numero.Trim();
            if (numeroPulito.StartsWith("00393"))
                numeroPulito = $"+{numeroPulito.Substring(2)}";
            if (!numeroPulito.StartsWith("+393"))
                numeroPulito = $"+39{numeroPulito}";
            return numeroPulito;
        }

        public static bool NumeroCellulareValido(string numero)
        {
            //numero = PulisciNumeroCellulare(numero);
            return Regex.IsMatch(numero, @"^([+]39)?3\d{8,11}$");
        }
    }
}