﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

namespace Cemi.SmsInfo.Service
{
    public partial class SmsService : ServiceBase
    {
        private readonly Timer _timer;

        public SmsService()
        {
            InitializeComponent();

            _timer = new Timer();
            _timer.Elapsed += TimerElapsed;
        }

        protected override void OnStart(string[] args)
        {
            KillExistingProcesses();

            try
            {
                StartController();

                _timer.Interval = Int32.Parse(ConfigurationManager.AppSettings["timerMilliseconds"]);
                _timer.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void StartController()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
                                             {
                                                 CreateNoWindow = false,
                                                 WorkingDirectory =
                                                     ConfigurationManager.AppSettings["working_directory"],
                                                 FileName = ConfigurationManager.AppSettings["executable_name"],
                                                 WindowStyle = ProcessWindowStyle.Normal
                                             };
            Process.Start(startInfo);
        }

        protected override void OnStop()
        {
            KillExistingProcesses();
        }

        private static void KillExistingProcesses()
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            foreach (Process process in processes)
            {
                process.Kill();
            }
        }

        private static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Process[] processes =
                Process.GetProcessesByName(ConfigurationManager.AppSettings["executable_name"].Replace(".exe",
                                                                                                       String.Empty));
            if (processes.Length != 1)
            {
                KillExistingProcesses();
                StartController();
            }
        }
    }
}