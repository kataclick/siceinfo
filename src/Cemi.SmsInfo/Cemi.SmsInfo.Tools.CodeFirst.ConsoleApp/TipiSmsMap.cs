// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Cemi.SmsInfo.Tools.CodeFirst.ConsoleApp
{

    // TipiSms
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.21.1.0")]
    public partial class TipiSmsMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<TipiSms>
    {
        public TipiSmsMap()
            : this("dbo")
        {
        }

        public TipiSmsMap(string schema)
        {
            ToTable(schema + ".TipiSms");
            HasKey(x => x.IdTipoSms);

            Property(x => x.IdTipoSms).HasColumnName(@"idTipoSms").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Descrizione).HasColumnName(@"descrizione").IsRequired().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.DataInserimentoRecord).HasColumnName(@"dataInserimentoRecord").IsRequired().HasColumnType("smalldatetime");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
