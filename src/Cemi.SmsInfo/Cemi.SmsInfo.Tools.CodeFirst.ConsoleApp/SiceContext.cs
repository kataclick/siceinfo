// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Cemi.SmsInfo.Tools.CodeFirst.ConsoleApp
{


    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.21.1.0")]
    public partial class SiceContext : System.Data.Entity.DbContext, ISiceContext
    {
        public System.Data.Entity.DbSet<Lavoratori> Lavoratori { get; set; } // Lavoratori
        public System.Data.Entity.DbSet<Sms> Sms { get; set; } // Sms
        public System.Data.Entity.DbSet<SmsInviatiEsiti> SmsInviatiEsiti { get; set; } // SmsInviatiEsiti
        public System.Data.Entity.DbSet<SmsInvii> SmsInvii { get; set; } // SmsInvii
        public System.Data.Entity.DbSet<SmsRecapitiUtenti> SmsRecapitiUtenti { get; set; } // SmsRecapitiUtenti
        public System.Data.Entity.DbSet<SmsRicevuti> SmsRicevuti { get; set; } // SmsRicevuti
        public System.Data.Entity.DbSet<TipiEsitoGestioneSms> TipiEsitoGestioneSms { get; set; } // TipiEsitoGestioneSms
        public System.Data.Entity.DbSet<TipiSms> TipiSms { get; set; } // TipiSms
        public System.Data.Entity.DbSet<Utenti> Utenti { get; set; } // Utenti

        static SiceContext()
        {
            System.Data.Entity.Database.SetInitializer<SiceContext>(null);
        }

        public SiceContext()
            : base("Name=SICE")
        {
            InitializePartial();
        }

        public SiceContext(string connectionString)
            : base(connectionString)
        {
            InitializePartial();
        }

        public SiceContext(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model)
            : base(connectionString, model)
        {
            InitializePartial();
        }

        public SiceContext(System.Data.Common.DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
            InitializePartial();
        }

        public SiceContext(System.Data.Common.DbConnection existingConnection, System.Data.Entity.Infrastructure.DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
            InitializePartial();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public bool IsSqlParameterNull(System.Data.SqlClient.SqlParameter param)
        {
            var sqlValue = param.SqlValue;
            var nullableValue = sqlValue as System.Data.SqlTypes.INullable;
            if (nullableValue != null)
                return nullableValue.IsNull;
            return (sqlValue == null || sqlValue == System.DBNull.Value);
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new LavoratoriMap());
            modelBuilder.Configurations.Add(new SmsMap());
            modelBuilder.Configurations.Add(new SmsInviatiEsitiMap());
            modelBuilder.Configurations.Add(new SmsInviiMap());
            modelBuilder.Configurations.Add(new SmsRecapitiUtentiMap());
            modelBuilder.Configurations.Add(new SmsRicevutiMap());
            modelBuilder.Configurations.Add(new TipiEsitoGestioneSmsMap());
            modelBuilder.Configurations.Add(new TipiSmsMap());
            modelBuilder.Configurations.Add(new UtentiMap());

            OnModelCreatingPartial(modelBuilder);
        }

        public static System.Data.Entity.DbModelBuilder CreateModel(System.Data.Entity.DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new LavoratoriMap(schema));
            modelBuilder.Configurations.Add(new SmsMap(schema));
            modelBuilder.Configurations.Add(new SmsInviatiEsitiMap(schema));
            modelBuilder.Configurations.Add(new SmsInviiMap(schema));
            modelBuilder.Configurations.Add(new SmsRecapitiUtentiMap(schema));
            modelBuilder.Configurations.Add(new SmsRicevutiMap(schema));
            modelBuilder.Configurations.Add(new TipiEsitoGestioneSmsMap(schema));
            modelBuilder.Configurations.Add(new TipiSmsMap(schema));
            modelBuilder.Configurations.Add(new UtentiMap(schema));
            return modelBuilder;
        }

        partial void InitializePartial();
        partial void OnModelCreatingPartial(System.Data.Entity.DbModelBuilder modelBuilder);
    }
}
// </auto-generated>
