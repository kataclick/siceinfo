﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.SmsInfo.WebService.Test.SmsService;

namespace Cemi.SmsInfo.WebService.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-- Inizio TEST --");

            bool testImmediato = TestInvioSmsImmediato();
            Console.WriteLine("Test immediato: {0}", testImmediato);

//            bool testImmediatoLista = TestInvioListaSmsImmediato();
  //          Console.WriteLine("Test immediato lista: {0}", testImmediatoLista);

            Console.WriteLine("-- Fine TEST --");

            Console.ReadLine();
        }

        private static bool TestInvioSmsImmediato()
        {
            SmsServiceClient client = new SmsServiceClient();
            SmsDaInviare sms = new SmsDaInviare
                                   {
                                       Tipologia = TipologiaSms.Generico,
                                       Testo = "Test Immediato",
                                       Data = DateTime.Now,
                                       Numero = "+393402788089"
                                   };
            return client.InviaSms(sms);
        }

        private static bool TestInvioListaSmsImmediato()
        {
            SmsServiceClient client = new SmsServiceClient();
            List<SmsDaInviare> listaSms = new List<SmsDaInviare>();
            SmsDaInviare sms1 = new SmsDaInviare
            {
                Tipologia = TipologiaSms.Generico,
                Testo = "Test Immediato",
                Data = DateTime.Now,
                Numero = "+393459461342"
            };
            SmsDaInviare sms2 = new SmsDaInviare
            {
                Tipologia = TipologiaSms.Generico,
                Testo = "Test Immediato",
                Data = DateTime.Now,
                Numero = "+393402788089"
            };
            listaSms.Add(sms1);
            listaSms.Add(sms2);
            return client.InviaListaSms(listaSms.ToArray());
        }
    }
}
