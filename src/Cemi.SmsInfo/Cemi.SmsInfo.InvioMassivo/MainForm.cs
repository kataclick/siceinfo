﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using Cemi.SmsInfo.InvioMassivo.SmsServiceReference;

namespace Cemi.SmsInfo.InvioMassivo
{
    public partial class MainForm : BaseForm
    {
        private readonly string _connectionString;
        private readonly int _minutiRitardoInvio;
        private readonly int _numeroMaxDestinatariPerInvio;
        private string _query;
        private string _testo;

        public MainForm()
        {
            InitializeComponent();

            _connectionString = ConfigurationManager.ConnectionStrings["SICE"].ConnectionString;
            _minutiRitardoInvio = Convert.ToInt32(ConfigurationManager.AppSettings["MinutiRitardoInvio"]);
            _numeroMaxDestinatariPerInvio = Convert.ToInt32(ConfigurationManager.AppSettings["NumeroMaxDestinatari"]);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CaricaTesto();
            CaricaQuery();
        }

        private void CaricaTesto()
        {
            _testo = ConfigurationManager.AppSettings["Testo"].Replace('’', '\'');
            textBoxTesto.Text = _testo;
        }

        private void CaricaQuery()
        {
            _query = ConfigurationManager.AppSettings["Query"];
            textBoxQuery.Text = _query;
        }

        private void buttonVerifica_Click(object sender, EventArgs e)
        {
            List<SmsDaInviare.Destinatario> destinatari = new List<SmsDaInviare.Destinatario>();
            richTextBoxLog.Clear();
            richTextBoxLog.AppendText(String.Format("{0} - Inizio verifica{1}", DateTime.Now, Environment.NewLine));

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand command = new SqlCommand(_query, connection))
                    {
                        connection.Open();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SmsDaInviare.Destinatario destinatario = new SmsDaInviare.Destinatario();
                                string numero = PulisciNumeroCellulare((string)reader["numero"]);
                                if (NumeroCellulareValido(numero))
                                {
                                    destinatario.Numero = numero;
                                    if (reader["idUtente"] != DBNull.Value)
                                        destinatario.IdUtenteSiceInfo = (int?) reader["idUtente"];
                                    destinatari.Add(destinatario);
                                }
                            }
                        }
                    }
                }

                richTextBoxLog.AppendText(String.Format("{0} - Recapiti trovati: {1}{2}", DateTime.Now,
                                                        destinatari.Count, Environment.NewLine));
            }
            catch (Exception exception)
            {
                richTextBoxLog.AppendText(String.Format("{0} - Eccezione: {1}{2}", DateTime.Now, exception.Message,
                                                        Environment.NewLine));
            }
        }

        private void buttonInvia_Click(object sender, EventArgs e)
        {
            List<SmsDaInviare.Destinatario> destinatari = new List<SmsDaInviare.Destinatario>();
            richTextBoxLog.Clear();
            richTextBoxLog.AppendText(String.Format("{0} - Inizio Invio{1}", DateTime.Now, Environment.NewLine));

            try
            {
                using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    using (SqlCommand command = new SqlCommand(_query, connection))
                    {
                        connection.Open();
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SmsDaInviare.Destinatario destinatario = new SmsDaInviare.Destinatario();
                                string numero = PulisciNumeroCellulare((string)reader["numero"]);
                                if (NumeroCellulareValido(numero))
                                {
                                    destinatario.Numero = numero;
                                    if (reader["idUtente"] != DBNull.Value)
                                        destinatario.IdUtenteSiceInfo = (int?)reader["idUtente"];
                                    destinatari.Add(destinatario);
                                }
                            }
                        }
                    }
                }

                richTextBoxLog.AppendText(String.Format("{0} - Recapiti caricati: {1}{2}", DateTime.Now,
                                                        destinatari.Count, Environment.NewLine));

                var listeDestinatari = destinatari
                    .Select((x, i) => new {Index = i, Value = x})
                    .GroupBy(x => x.Index/_numeroMaxDestinatariPerInvio)
                    .Select(x => x.Select(v => v.Value).ToList())
                    .ToList();

                List<SmsDaInviare> listaSmsDaInviare = listeDestinatari
                    .Select(elencoDestinatari => new SmsDaInviare
                                                     {
                                                         Data = DateTime.Now.AddMinutes(_minutiRitardoInvio),
                                                         Testo =_testo,
                                                         Tipologia = TipologiaSms.Generico,
                                                         Destinatari = elencoDestinatari.ToArray()
                                                     }).ToList();

                richTextBoxLog.AppendText(String.Format("{0} - SMS predisposti: {1}{2}", DateTime.Now,
                                                        listaSmsDaInviare.Count, Environment.NewLine));

                if (listaSmsDaInviare.Count > 0)
                {
                    SmsServiceClient client = new SmsServiceClient();

                    for (int i = 0; i < listaSmsDaInviare.Count; i++)
                    {
                        bool invio = client.InviaSms(listaSmsDaInviare[i]);

                        string messaggio = invio
                                               ? String.Format("{0} - SMS n° {2} schedulato correttamente{1}", DateTime.Now,
                                                               Environment.NewLine, i)
                                               : String.Format("{0} - Errore nella schedulazione dell'SMS n° {2}{1}", DateTime.Now,
                                                               Environment.NewLine, i);
                        richTextBoxLog.AppendText(messaggio);
                    }
                }
            }
            catch (Exception exception)
            {
                richTextBoxLog.AppendText(String.Format("{0} - Eccezione: {1}{2}", DateTime.Now, exception.Message,
                                                        Environment.NewLine));
            }
        }

        public static string PulisciNumeroCellulare(string numero)
        {
            string numeroPulito = numero.Trim();
            if (numeroPulito.StartsWith("00393"))
                numeroPulito = String.Format("+{0}", numeroPulito.Substring(2));
            if (!numeroPulito.StartsWith("+393"))
                numeroPulito = String.Format("+39{0}", numeroPulito);
            return numeroPulito;
        }

        public static bool NumeroCellulareValido(string numero)
        {
            //numero = PulisciNumeroCellulare(numero);
            return Regex.IsMatch(numero, @"^([+]39)?\d{8,11}$");
        }
    }
}