﻿namespace Cemi.SmsInfo.InvioMassivo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxTesto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxQuery = new System.Windows.Forms.TextBox();
            this.buttonVerifica = new System.Windows.Forms.Button();
            this.buttonInvia = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // textBoxTesto
            // 
            this.textBoxTesto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTesto.Location = new System.Drawing.Point(12, 29);
            this.textBoxTesto.Multiline = true;
            this.textBoxTesto.Name = "textBoxTesto";
            this.textBoxTesto.ReadOnly = true;
            this.textBoxTesto.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxTesto.Size = new System.Drawing.Size(544, 47);
            this.textBoxTesto.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Testo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Query (numero, idUtente)";
            // 
            // textBoxQuery
            // 
            this.textBoxQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxQuery.Location = new System.Drawing.Point(12, 99);
            this.textBoxQuery.Multiline = true;
            this.textBoxQuery.Name = "textBoxQuery";
            this.textBoxQuery.ReadOnly = true;
            this.textBoxQuery.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxQuery.Size = new System.Drawing.Size(544, 47);
            this.textBoxQuery.TabIndex = 2;
            // 
            // buttonVerifica
            // 
            this.buttonVerifica.Location = new System.Drawing.Point(12, 152);
            this.buttonVerifica.Name = "buttonVerifica";
            this.buttonVerifica.Size = new System.Drawing.Size(135, 23);
            this.buttonVerifica.TabIndex = 4;
            this.buttonVerifica.Text = "Verifica Query";
            this.buttonVerifica.UseVisualStyleBackColor = true;
            this.buttonVerifica.Click += new System.EventHandler(this.buttonVerifica_Click);
            // 
            // buttonInvia
            // 
            this.buttonInvia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInvia.Location = new System.Drawing.Point(421, 152);
            this.buttonInvia.Name = "buttonInvia";
            this.buttonInvia.Size = new System.Drawing.Size(135, 23);
            this.buttonInvia.TabIndex = 5;
            this.buttonInvia.Text = "Invia SMS";
            this.buttonInvia.UseVisualStyleBackColor = true;
            this.buttonInvia.Click += new System.EventHandler(this.buttonInvia_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxLog.Location = new System.Drawing.Point(12, 181);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(544, 193);
            this.richTextBoxLog.TabIndex = 6;
            this.richTextBoxLog.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 386);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.buttonInvia);
            this.Controls.Add(this.buttonVerifica);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxQuery);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxTesto);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxTesto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxQuery;
        private System.Windows.Forms.Button buttonVerifica;
        private System.Windows.Forms.Button buttonInvia;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
    }
}