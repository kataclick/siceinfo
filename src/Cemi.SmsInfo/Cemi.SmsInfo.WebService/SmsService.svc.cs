﻿using System.Collections.Generic;
using Cemi.SmsInfo.Business;
using Cemi.SmsInfo.Type.Service;

namespace Cemi.SmsInfo.WebService
{
    public class SmsService : ISmsService
    {
        readonly ISmsManager _smsManager = new SmsManager();

        #region ISmsService Members

        public bool InviaSms(SmsDaInviare sms)
        {
            return _smsManager.SchedulaInvioSms(sms);
        }

        public bool InviaListaSms(List<SmsDaInviare> sms)
        {
            return _smsManager.SchedulaInvioSms(sms);
        }

        #endregion
    }
}