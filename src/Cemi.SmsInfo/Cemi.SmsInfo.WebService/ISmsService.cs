﻿using System.Collections.Generic;
using System.ServiceModel;
using Cemi.SmsInfo.Type.Service;

namespace Cemi.SmsInfo.WebService
{
    [ServiceContract]
    public interface ISmsService
    {
        [OperationContract]
        bool InviaSms(SmsDaInviare sms);

        [OperationContract]
        bool InviaListaSms(List<SmsDaInviare> sms);
    }
}