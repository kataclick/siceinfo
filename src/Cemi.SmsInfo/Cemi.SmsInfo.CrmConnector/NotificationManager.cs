﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Principal;
using Cemi.SmsInfo.CrmConnector.CrmSmsService;
using Cemi.SmsInfo.Type.Crm;

namespace Cemi.SmsInfo.CrmConnector
{
    public class NotificationManager
    {
        private static readonly string Username = ConfigurationManager.AppSettings["UsernameWSCrm"];
        private static readonly string Password = ConfigurationManager.AppSettings["PasswordWSCrm"];
        private static readonly string Domain = ConfigurationManager.AppSettings["DomainWSCrm"];

        public static void NotificaSms(List<SmsNotifica> listaSms)
        {
            if (listaSms.Count > 0)
            {
                try
                {
                    SmsCemiSoapClient client = new SmsCemiSoapClient();

                    NetworkCredential credential = new NetworkCredential(Username, Password) {Domain = Domain};
                    if (client.ClientCredentials != null)
                    {
                        client.ClientCredentials.Windows.ClientCredential = credential;
                        client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;

                        List<cemisms> listaSmsCrm = (from smsNotifica in listaSms
                                                     select new cemisms
                                                                {
                                                                    cellulare = smsNotifica.Numero,
                                                                    codicecontatto = smsNotifica.IdLavoratore.ToString(),
                                                                    data = smsNotifica.Data.ToString(@"dd/MM/yyyy"),
                                                                    testo = smsNotifica.Testo
                                                                }).ToList();

                        ArrayOfCemisms arraySmsCrm = new ArrayOfCemisms();
                        arraySmsCrm.AddRange(listaSmsCrm);

                        string result = client.CreateSms(arraySmsCrm);

                        string message = $"{DateTime.Now} - CRMSMS risposta : {result}";

                        TracciaNotifica(message);
                    }
                }
                catch (Exception e)
                {
                    string message = $"Eccezione CRMSMS: {e.Message}";
                    TracciaNotifica(message);
                }
            }
        }

        private static void TracciaNotifica(string messaggioErrore)
        {
            string logSms = ConfigurationManager.AppSettings["LogSms"];

            Console.WriteLine(messaggioErrore);

            if (logSms == "true")
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry($"Eccezione SmsInfo: {messaggioErrore}");
                }
            }
        }
    }
}
