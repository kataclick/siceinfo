﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Cemi.NotifichePreliminari.Business;
using SimpleInjector;
using System.Reflection;
using System.Collections.Specialized;

namespace Cemi.NotifichePreliminari.ConsoleApp
{
    class Program
    {
        private static Container _container;

        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
            //Program prg = new Program();

            InitializeContainer();

            IDataProvider dpCantieri = _container.GetInstance<IDataProvider>();

            if (args.Length == 0)
            {
                dpCantieri.TracciaErrore("-- Nessun comando specificato --","Lancio senza parametri");
                Console.WriteLine("-- NESSUN COMANDO SPECIFICATO. LANCIARE SPECIFICANDO I SEGUENTI PARAMETRI:");
                Console.WriteLine("- \"edilconnect update\": Carica le testate delle notifiche da EdilConnect");
                Console.WriteLine("- \"regione insert\": Carica le nuove notifiche non presenti a sistema");
                Console.WriteLine("");
                Console.WriteLine("Premere un tasto per chiudere");
                Console.ReadLine();
            }
            else
            {
                #region EDILCONNECT
                if (args[0] == "edilconnect")
                {
                    if (args[1] == "upload")
                    { 
                        Console.WriteLine("-- INIZIO PROCEDURA CARICAMENTO DA EDILCONNECT --");
                    
                        dpCantieri.UploadFromEdilconnect();

                        Console.WriteLine("-- FINE PROCEDURA CARICAMENTO DA EDILCONNECT --");
                    }
                }
                #endregion


                #region Regione Lombardia
                if (args[0] == "regione")
                {
                    if (args[1] == "insert")
                    {
                        Console.WriteLine("-- INIZIO PROCEDURA INSERIMENTO NUOVE NOTIFICHE --");

                        dpCantieri.InsertNewNotifiche();

                        Console.WriteLine("-- FINE PROCEDURA INSERIMENTO NUOVE NOTIFICHE --");
                    }

                    if (args[1] == "update")
                    {
                        Console.WriteLine("-- INIZIO PROCEDURA AGGIORNAMENTO NOTIFICHE --");

                        dpCantieri.UpdateFromEdilconnect();

                        Console.WriteLine("-- FINE PROCEDURA AGGIORNAMENTO NOTIFICHE --");
                    }

                    if (args[1] == "incomplete")
                    {
                        Console.WriteLine("-- INIZIO PROCEDURA AGGIORNAMENTO NOTIFICHE INCOMPLETE --");

                        dpCantieri.UpdateIncomplete();

                        Console.WriteLine("-- FINE PROCEDURA AGGIORNAMENTO NOTIFICHE INCOMPLETE --");
                    }
                }
                #endregion

            }
        }

        private static void InitializeContainer()
        {
            _container = new Container();

            string name = string.Empty;
            var diConfig = ConfigurationManager.GetSection("MyDIConfig") as NameValueCollection;
            if (diConfig != null)
            {
                name = diConfig["ImplementationAssemblyList"];
            }
            List<string> listName = name.Split(';').ToList();
            foreach (string n in listName)
            {
                var impAssembly = Assembly.Load(n);

                if (impAssembly != null)
                {
                    var registrations =
                        from type in impAssembly.GetExportedTypes()
                        where type.GetInterfaces().Any()
                        select new { Services = type.GetInterfaces().ToList(), Implementation = type };

                    foreach (var reg in registrations)
                    {
                        foreach (var serv in reg.Services)
                        {
                            _container.Register(serv, reg.Implementation, Lifestyle.Singleton);
                        }
                    }
                }
            }
        }

        

        public String GetDescrizioneProvincia(String prov)
        {
            String descrizioneProvincia = prov;

            switch (prov)
            {
                case "PV":
                    descrizioneProvincia = "PAVIA";
                    break;
                case "CO":
                    descrizioneProvincia = "COMO";
                    break;
                case "MI":
                    descrizioneProvincia = "MILANO";
                    break;
                case "CR":
                    descrizioneProvincia = "CREMONA";
                    break;
                case "VA":
                    descrizioneProvincia = "VARESE";
                    break;
                case "LO":
                    descrizioneProvincia = "LODI";
                    break;
                case "LC":
                    descrizioneProvincia = "LECCO";
                    break;
                case "SO":
                    descrizioneProvincia = "SONDRIO";
                    break;
                case "MN":
                    descrizioneProvincia = "MANTOVA";
                    break;
                case "MB":
                    descrizioneProvincia = "MONZA E DELLA BRIANZA";
                    break;
                case "BG":
                    descrizioneProvincia = "BERGAMO";
                    break;
                case "BS":
                    descrizioneProvincia = "BRESCIA";
                    break;
            }

            return descrizioneProvincia;
        }


    }

    
}
