﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cemi.NotifichePreliminari.ServiceReferenceCantieri.ServiceReferenceCantieri;


namespace Cemi.NotifichePreliminari.Business
{
    public interface IDataProvider
    {
        void UploadFromEdilconnect();

        void InsertNewNotifiche();

        void UpdateFromEdilconnect();

        void UpdateIncomplete();

        void TracciaErrore(string messaggioErrore, string sottoCategoria);

        List<CantiereTR4> RecuperaCantieri(string numeroNotifica);



    }
}
