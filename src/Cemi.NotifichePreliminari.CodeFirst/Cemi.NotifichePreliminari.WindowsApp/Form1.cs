﻿using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Cemi.NotifichePreliminari.Business.Imp;
using Cemi.NotifichePreliminari.ServiceReferenceCantieri.ServiceReferenceCantieri;
//using SimpleInjector;
using System.Reflection;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace Cemi.NotifichePreliminari.WindowsApp
{
    public partial class Form1 : Form
    {
       

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");



            //DataProvider dpCantieri = new DataProvider();

            List<CantiereTR4> cantieri = RecuperaCantieri(txtNumeroNotifica.Text);
            List<ImpresaCantTR4> imprese = RecuperaImprese(txtNumeroNotifica.Text);
            List<IndirizzoCantTR4> indirizzi = RecuperaIndirizzi(txtNumeroNotifica.Text);
            List<PersonaCantTR4> persone = RecuperaPersone(txtNumeroNotifica.Text);

            DataGridViewAutoSizeColumnsMode mode = (DataGridViewAutoSizeColumnsMode)6;



            GridViewCantiere.DataSource = cantieri;
            GridViewImprese.DataSource = imprese;
            GridViewIndirizzi.DataSource = indirizzi;
            GridViewPersone.DataSource = persone;

            GridViewCantiere.AutoResizeColumns(mode);
            GridViewImprese.AutoResizeColumns(mode);
            GridViewIndirizzi.AutoResizeColumns(mode);
            GridViewPersone.AutoResizeColumns(mode);



        }

        public List<CantiereTR4> RecuperaCantieri(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            CantiereTR4[] cantiereTR4;
            List<CantiereTR4> canList = new List<CantiereTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();
            try
            {
                client.ricercaCantieriTR4(st, ref intest,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            numeroNotifica,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            null,
                                            null,
                                            new String[0],
                                            out cantiereTR4);
                canList = cantiereTR4.ToList();
            }
            catch (Exception ex)
            {
                //TracciaErrore(ex.Message, "Collegamento WS, metodo Cantieri");
            }

            return canList;
        }

        public List<ImpresaCantTR4> RecuperaImprese(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            ImpresaCantTR4[] imprese;
            List<ImpresaCantTR4> impList = new List<ImpresaCantTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();
            try
            {
                client.ricercaImpreseTR4(st, ref intest, numeroNotifica, string.Empty, out imprese);
                impList = imprese.ToList();
            }
            catch (Exception ex)
            {
                //TracciaErrore(ex.Message, "Collegamento WS, metodo Imprese");
            }

            return impList;
        }

        public List<IndirizzoCantTR4> RecuperaIndirizzi(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            IndirizzoCantTR4[] indirizzi;
            List<IndirizzoCantTR4> indList = new List<IndirizzoCantTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();

            //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
            try
            {
                client.ricercaIndirizziTR4(st, ref intest, numeroNotifica, string.Empty, out indirizzi);
                indList = indirizzi.ToList();
            }
            catch (Exception ex)
            {
                //TracciaErrore(ex.Message, "Collegamento WS, metodo Indirizzi");
            }

            return indList;
        }

        public List<PersonaCantTR4> RecuperaPersone(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            PersonaCantTR4[] persone;
            List<PersonaCantTR4> perList = new List<PersonaCantTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();
            try
            {
                client.ricercaPersoneTR4(st, ref intest, numeroNotifica, out persone);
                perList = persone.ToList();
            }
            catch (Exception ex)
            {
               // TracciaErrore(ex.Message, "Collegamento WS, metodo Persone");
            }

            return perList;
        }

        public SecurityType CreateSecurityType()
        {
            SecurityType st;
            st = new SecurityType();
            st.UsernameToken = new UsernameToken();

            st.UsernameToken.Username = "cassaedilemilano";
            st.UsernameToken.Password = "JeRXVcVf";
            st.Timestamp = new Timestamp();
            st.Timestamp.Created = DateTime.Now.AddHours(-12);
            st.Timestamp.Expires = DateTime.Now.AddHours(12);
            return st;
        }

        public Intestazione CreateIntestazione()
        {
            Intestazione intest = new Intestazione();
            intest.IntestazioneMessaggio = new IntestazioneMessaggio();
            intest.IntestazioneMessaggio.Mittente = new IdentificativoParte[1];

            IdentificativoParte ident = new IdentificativoParte();
            intest.IntestazioneMessaggio.Mittente[0] = ident;
            ident.tipo = "CodicePA";
            ident.Value = "Cassa Edile Milano, Lodi, Monza e Brianza";
            ident.indirizzoTelematico = "www.cassaedilemilano.it";

            IdentificativoParte idenDest = new IdentificativoParte();
            intest.IntestazioneMessaggio.Destinatario = new Destinatario();
            intest.IntestazioneMessaggio.Destinatario.IdentificativoParte = idenDest;
            idenDest.tipo = "CodicePA";
            idenDest.Value = "Regione Lombardia";


            intest.IntestazioneMessaggio.ProfiloCollaborazione = new ProfiloCollaborazione();
            intest.IntestazioneMessaggio.ProfiloCollaborazione.Value =
                ProfiloCollaborazioneBaseType.EGOV_IT_ServizioSincrono;

            intest.IntestazioneMessaggio.Servizio = new Servizio();
            intest.IntestazioneMessaggio.Servizio.tipo = "TEST";
            intest.IntestazioneMessaggio.Servizio.Value = "WS - CANTIERI";

            intest.ListaTrasmissioni = new Trasmissione[1];
            intest.ListaTrasmissioni[0] = new Trasmissione();
            intest.ListaTrasmissioni[0].Origine = new Origine();
            intest.ListaTrasmissioni[0].Origine.IdentificativoParte = ident;
            intest.ListaTrasmissioni[0].OraRegistrazione = new OraRegistrazione();
            intest.ListaTrasmissioni[0].OraRegistrazione.Value = DateTime.Now;
            intest.ListaTrasmissioni[0].Destinazione = new Destinazione();
            intest.ListaTrasmissioni[0].Destinazione.IdentificativoParte = idenDest;


            intest.IntestazioneMessaggio.Messaggio = new Messaggio();
            intest.IntestazioneMessaggio.Messaggio.Identificatore = DateTime.Now.ToString("ddMMyyyyHHmmss");
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione = new OraRegistrazione();
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione.tempo = OraRegistrazioneTempo.EGOV_IT_Locale;
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione.Value = DateTime.Now;
            intest.IntestazioneMessaggio.Messaggio.RiferimentoMessaggio = DateTime.Now.ToString("ddMMyyyyHHmmss");
            intest.IntestazioneMessaggio.Messaggio.ScadenzaSpecified = false;

            intest.IntestazioneMessaggio.Azione = "WS - CANTIERI";
            intest.IntestazioneMessaggio.ProfiloTrasmissione = new ProfiloTrasmissione();
            intest.IntestazioneMessaggio.ProfiloTrasmissione.confermaRicezione = false;
            intest.IntestazioneMessaggio.ProfiloTrasmissione.inoltro = ProfiloTrasmissioneInoltro.EGOV_IT_PIUDIUNAVOLTA;

            intest.IntestazioneMessaggio.ProfiloCollaborazione = new ProfiloCollaborazione();
            intest.IntestazioneMessaggio.ProfiloCollaborazione.Value =
                ProfiloCollaborazioneBaseType.EGOV_IT_ServizioSincrono;

            intest.IntestazioneMessaggio.Collaborazione = String.Format("CassaEdileMilanoRegioneLombardia{0}",
                                                                        DateTime.Now.ToString("ddMMyyyyHHmmss"));
            return intest;
        }

    }
}
