﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Threading;
using System.Globalization;
using Cemi.NotifichePreliminari.Business;
using System.Reflection;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace Cemi.NotifichePreliminari.WindowsApp
{
    static class Program
    {


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }


    }


}
