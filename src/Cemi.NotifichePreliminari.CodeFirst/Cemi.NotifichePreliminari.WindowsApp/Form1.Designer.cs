﻿namespace Cemi.NotifichePreliminari.WindowsApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumeroNotifica = new System.Windows.Forms.TextBox();
            this.btnRicerca = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GridViewCantiere = new System.Windows.Forms.DataGridView();
            this.cdContrattoAppaltoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdContrattoCIGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdContrattoCUIDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrNotificaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrProtocolloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdTipoOperaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsTipoOperaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtComunicazioneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrLavAutonomiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrImpreseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsTipoCategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsTipoTipologiaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsAltraCategoriaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsAltraTipologiaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ammontareComplessivoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flArt90C11DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flArt99C1BDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flArt99C1CDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtAggNotificaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idagpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantiereTR4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.GridViewImprese = new System.Windows.Forms.DataGridView();
            this.impresaCantTR4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.codFiscaleIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ragioneSocialeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indirizzoSedeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsComuneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsSiglaProvinciaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazioneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zipCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdTipoIncaricoAppaltoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsTipoIncaricoAppaltoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pivaImpresaAssocDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridViewIndirizzi = new System.Windows.Forms.DataGridView();
            this.GridViewPersone = new System.Windows.Forms.DataGridView();
            this.indirizzoCantTR4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsComuneDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsProvinciaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtInizioLavoriDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.durataLavoriDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsDurataLavoriDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indirizzoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdAreaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsAreaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.areaNoteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrMassimoLavoratoriDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAPindirCantiereDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idagpDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personaCantTR4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ruoloDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codFiscaleDocumentoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cognomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtNascitaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazioneDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.regioneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsProvinciaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsComuneDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.indirizzoDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zipCodeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewCantiere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cantiereTR4BindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewImprese)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.impresaCantTR4BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewIndirizzi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewPersone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.indirizzoCantTR4BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaCantTR4BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero Notifica: ";
            // 
            // txtNumeroNotifica
            // 
            this.txtNumeroNotifica.Location = new System.Drawing.Point(111, 19);
            this.txtNumeroNotifica.Name = "txtNumeroNotifica";
            this.txtNumeroNotifica.Size = new System.Drawing.Size(129, 20);
            this.txtNumeroNotifica.TabIndex = 1;
            // 
            // btnRicerca
            // 
            this.btnRicerca.Location = new System.Drawing.Point(263, 19);
            this.btnRicerca.Name = "btnRicerca";
            this.btnRicerca.Size = new System.Drawing.Size(65, 20);
            this.btnRicerca.TabIndex = 2;
            this.btnRicerca.Text = "Ricerca";
            this.btnRicerca.UseVisualStyleBackColor = true;
            this.btnRicerca.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.GridViewCantiere);
            this.groupBox1.Location = new System.Drawing.Point(20, 57);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(915, 158);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cantiere";
            // 
            // GridViewCantiere
            // 
            this.GridViewCantiere.AllowUserToDeleteRows = false;
            this.GridViewCantiere.AutoGenerateColumns = false;
            this.GridViewCantiere.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewCantiere.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cdContrattoAppaltoDataGridViewTextBoxColumn,
            this.cdContrattoCIGDataGridViewTextBoxColumn,
            this.cdContrattoCUIDataGridViewTextBoxColumn,
            this.nrNotificaDataGridViewTextBoxColumn,
            this.nrProtocolloDataGridViewTextBoxColumn,
            this.cdTipoOperaDataGridViewTextBoxColumn,
            this.dsTipoOperaDataGridViewTextBoxColumn,
            this.dtComunicazioneDataGridViewTextBoxColumn,
            this.nrLavAutonomiDataGridViewTextBoxColumn,
            this.nrImpreseDataGridViewTextBoxColumn,
            this.dsTipoCategoriaDataGridViewTextBoxColumn,
            this.dsTipoTipologiaDataGridViewTextBoxColumn,
            this.dsAltraCategoriaDataGridViewTextBoxColumn,
            this.dsAltraTipologiaDataGridViewTextBoxColumn,
            this.ammontareComplessivoDataGridViewTextBoxColumn,
            this.flArt90C11DataGridViewTextBoxColumn,
            this.flArt99C1BDataGridViewTextBoxColumn,
            this.flArt99C1CDataGridViewTextBoxColumn,
            this.dtAggNotificaDataGridViewTextBoxColumn,
            this.idagpDataGridViewTextBoxColumn});
            this.GridViewCantiere.DataSource = this.cantiereTR4BindingSource;
            this.GridViewCantiere.Location = new System.Drawing.Point(17, 23);
            this.GridViewCantiere.Name = "GridViewCantiere";
            this.GridViewCantiere.ReadOnly = true;
            this.GridViewCantiere.Size = new System.Drawing.Size(885, 113);
            this.GridViewCantiere.TabIndex = 0;
            // 
            // cdContrattoAppaltoDataGridViewTextBoxColumn
            // 
            this.cdContrattoAppaltoDataGridViewTextBoxColumn.DataPropertyName = "cdContrattoAppalto";
            this.cdContrattoAppaltoDataGridViewTextBoxColumn.HeaderText = "cdContrattoAppalto";
            this.cdContrattoAppaltoDataGridViewTextBoxColumn.Name = "cdContrattoAppaltoDataGridViewTextBoxColumn";
            this.cdContrattoAppaltoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cdContrattoCIGDataGridViewTextBoxColumn
            // 
            this.cdContrattoCIGDataGridViewTextBoxColumn.DataPropertyName = "cdContrattoCIG";
            this.cdContrattoCIGDataGridViewTextBoxColumn.HeaderText = "cdContrattoCIG";
            this.cdContrattoCIGDataGridViewTextBoxColumn.Name = "cdContrattoCIGDataGridViewTextBoxColumn";
            this.cdContrattoCIGDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cdContrattoCUIDataGridViewTextBoxColumn
            // 
            this.cdContrattoCUIDataGridViewTextBoxColumn.DataPropertyName = "cdContrattoCUI";
            this.cdContrattoCUIDataGridViewTextBoxColumn.HeaderText = "cdContrattoCUI";
            this.cdContrattoCUIDataGridViewTextBoxColumn.Name = "cdContrattoCUIDataGridViewTextBoxColumn";
            this.cdContrattoCUIDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nrNotificaDataGridViewTextBoxColumn
            // 
            this.nrNotificaDataGridViewTextBoxColumn.DataPropertyName = "nrNotifica";
            this.nrNotificaDataGridViewTextBoxColumn.HeaderText = "nrNotifica";
            this.nrNotificaDataGridViewTextBoxColumn.Name = "nrNotificaDataGridViewTextBoxColumn";
            this.nrNotificaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nrProtocolloDataGridViewTextBoxColumn
            // 
            this.nrProtocolloDataGridViewTextBoxColumn.DataPropertyName = "nrProtocollo";
            this.nrProtocolloDataGridViewTextBoxColumn.HeaderText = "nrProtocollo";
            this.nrProtocolloDataGridViewTextBoxColumn.Name = "nrProtocolloDataGridViewTextBoxColumn";
            this.nrProtocolloDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cdTipoOperaDataGridViewTextBoxColumn
            // 
            this.cdTipoOperaDataGridViewTextBoxColumn.DataPropertyName = "cdTipoOpera";
            this.cdTipoOperaDataGridViewTextBoxColumn.HeaderText = "cdTipoOpera";
            this.cdTipoOperaDataGridViewTextBoxColumn.Name = "cdTipoOperaDataGridViewTextBoxColumn";
            this.cdTipoOperaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsTipoOperaDataGridViewTextBoxColumn
            // 
            this.dsTipoOperaDataGridViewTextBoxColumn.DataPropertyName = "dsTipoOpera";
            this.dsTipoOperaDataGridViewTextBoxColumn.HeaderText = "dsTipoOpera";
            this.dsTipoOperaDataGridViewTextBoxColumn.Name = "dsTipoOperaDataGridViewTextBoxColumn";
            this.dsTipoOperaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtComunicazioneDataGridViewTextBoxColumn
            // 
            this.dtComunicazioneDataGridViewTextBoxColumn.DataPropertyName = "dtComunicazione";
            this.dtComunicazioneDataGridViewTextBoxColumn.HeaderText = "dtComunicazione";
            this.dtComunicazioneDataGridViewTextBoxColumn.Name = "dtComunicazioneDataGridViewTextBoxColumn";
            this.dtComunicazioneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nrLavAutonomiDataGridViewTextBoxColumn
            // 
            this.nrLavAutonomiDataGridViewTextBoxColumn.DataPropertyName = "nrLavAutonomi";
            this.nrLavAutonomiDataGridViewTextBoxColumn.HeaderText = "nrLavAutonomi";
            this.nrLavAutonomiDataGridViewTextBoxColumn.Name = "nrLavAutonomiDataGridViewTextBoxColumn";
            this.nrLavAutonomiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nrImpreseDataGridViewTextBoxColumn
            // 
            this.nrImpreseDataGridViewTextBoxColumn.DataPropertyName = "nrImprese";
            this.nrImpreseDataGridViewTextBoxColumn.HeaderText = "nrImprese";
            this.nrImpreseDataGridViewTextBoxColumn.Name = "nrImpreseDataGridViewTextBoxColumn";
            this.nrImpreseDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsTipoCategoriaDataGridViewTextBoxColumn
            // 
            this.dsTipoCategoriaDataGridViewTextBoxColumn.DataPropertyName = "dsTipoCategoria";
            this.dsTipoCategoriaDataGridViewTextBoxColumn.HeaderText = "dsTipoCategoria";
            this.dsTipoCategoriaDataGridViewTextBoxColumn.Name = "dsTipoCategoriaDataGridViewTextBoxColumn";
            this.dsTipoCategoriaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsTipoTipologiaDataGridViewTextBoxColumn
            // 
            this.dsTipoTipologiaDataGridViewTextBoxColumn.DataPropertyName = "dsTipoTipologia";
            this.dsTipoTipologiaDataGridViewTextBoxColumn.HeaderText = "dsTipoTipologia";
            this.dsTipoTipologiaDataGridViewTextBoxColumn.Name = "dsTipoTipologiaDataGridViewTextBoxColumn";
            this.dsTipoTipologiaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsAltraCategoriaDataGridViewTextBoxColumn
            // 
            this.dsAltraCategoriaDataGridViewTextBoxColumn.DataPropertyName = "dsAltraCategoria";
            this.dsAltraCategoriaDataGridViewTextBoxColumn.HeaderText = "dsAltraCategoria";
            this.dsAltraCategoriaDataGridViewTextBoxColumn.Name = "dsAltraCategoriaDataGridViewTextBoxColumn";
            this.dsAltraCategoriaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsAltraTipologiaDataGridViewTextBoxColumn
            // 
            this.dsAltraTipologiaDataGridViewTextBoxColumn.DataPropertyName = "dsAltraTipologia";
            this.dsAltraTipologiaDataGridViewTextBoxColumn.HeaderText = "dsAltraTipologia";
            this.dsAltraTipologiaDataGridViewTextBoxColumn.Name = "dsAltraTipologiaDataGridViewTextBoxColumn";
            this.dsAltraTipologiaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ammontareComplessivoDataGridViewTextBoxColumn
            // 
            this.ammontareComplessivoDataGridViewTextBoxColumn.DataPropertyName = "ammontareComplessivo";
            this.ammontareComplessivoDataGridViewTextBoxColumn.HeaderText = "ammontareComplessivo";
            this.ammontareComplessivoDataGridViewTextBoxColumn.Name = "ammontareComplessivoDataGridViewTextBoxColumn";
            this.ammontareComplessivoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // flArt90C11DataGridViewTextBoxColumn
            // 
            this.flArt90C11DataGridViewTextBoxColumn.DataPropertyName = "flArt90C11";
            this.flArt90C11DataGridViewTextBoxColumn.HeaderText = "flArt90C11";
            this.flArt90C11DataGridViewTextBoxColumn.Name = "flArt90C11DataGridViewTextBoxColumn";
            this.flArt90C11DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // flArt99C1BDataGridViewTextBoxColumn
            // 
            this.flArt99C1BDataGridViewTextBoxColumn.DataPropertyName = "flArt99C1B";
            this.flArt99C1BDataGridViewTextBoxColumn.HeaderText = "flArt99C1B";
            this.flArt99C1BDataGridViewTextBoxColumn.Name = "flArt99C1BDataGridViewTextBoxColumn";
            this.flArt99C1BDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // flArt99C1CDataGridViewTextBoxColumn
            // 
            this.flArt99C1CDataGridViewTextBoxColumn.DataPropertyName = "flArt99C1C";
            this.flArt99C1CDataGridViewTextBoxColumn.HeaderText = "flArt99C1C";
            this.flArt99C1CDataGridViewTextBoxColumn.Name = "flArt99C1CDataGridViewTextBoxColumn";
            this.flArt99C1CDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtAggNotificaDataGridViewTextBoxColumn
            // 
            this.dtAggNotificaDataGridViewTextBoxColumn.DataPropertyName = "dtAggNotifica";
            this.dtAggNotificaDataGridViewTextBoxColumn.HeaderText = "dtAggNotifica";
            this.dtAggNotificaDataGridViewTextBoxColumn.Name = "dtAggNotificaDataGridViewTextBoxColumn";
            this.dtAggNotificaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idagpDataGridViewTextBoxColumn
            // 
            this.idagpDataGridViewTextBoxColumn.DataPropertyName = "idagp";
            this.idagpDataGridViewTextBoxColumn.HeaderText = "idagp";
            this.idagpDataGridViewTextBoxColumn.Name = "idagpDataGridViewTextBoxColumn";
            this.idagpDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cantiereTR4BindingSource
            // 
            this.cantiereTR4BindingSource.DataSource = typeof(Cemi.NotifichePreliminari.WindowsApp.ServiceReferenceCantieri.CantiereTR4);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.GridViewImprese);
            this.groupBox2.Location = new System.Drawing.Point(21, 226);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(913, 181);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Imprese";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.GridViewIndirizzi);
            this.groupBox3.Location = new System.Drawing.Point(20, 419);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(914, 172);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Indirizzi";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.GridViewPersone);
            this.groupBox4.Location = new System.Drawing.Point(20, 607);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(914, 172);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Persone";
            // 
            // GridViewImprese
            // 
            this.GridViewImprese.AllowUserToDeleteRows = false;
            this.GridViewImprese.AutoGenerateColumns = false;
            this.GridViewImprese.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewImprese.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codFiscaleIdDataGridViewTextBoxColumn,
            this.ragioneSocialeDataGridViewTextBoxColumn,
            this.indirizzoSedeDataGridViewTextBoxColumn,
            this.dsComuneDataGridViewTextBoxColumn,
            this.dsSiglaProvinciaDataGridViewTextBoxColumn,
            this.nazioneDataGridViewTextBoxColumn,
            this.zipCodeDataGridViewTextBoxColumn,
            this.cdTipoIncaricoAppaltoDataGridViewTextBoxColumn,
            this.dsTipoIncaricoAppaltoDataGridViewTextBoxColumn,
            this.pivaImpresaAssocDataGridViewTextBoxColumn});
            this.GridViewImprese.DataSource = this.impresaCantTR4BindingSource;
            this.GridViewImprese.Location = new System.Drawing.Point(16, 19);
            this.GridViewImprese.Name = "GridViewImprese";
            this.GridViewImprese.ReadOnly = true;
            this.GridViewImprese.Size = new System.Drawing.Size(885, 151);
            this.GridViewImprese.TabIndex = 0;
            // 
            // impresaCantTR4BindingSource
            // 
            this.impresaCantTR4BindingSource.DataSource = typeof(Cemi.NotifichePreliminari.WindowsApp.ServiceReferenceCantieri.ImpresaCantTR4);
            // 
            // codFiscaleIdDataGridViewTextBoxColumn
            // 
            this.codFiscaleIdDataGridViewTextBoxColumn.DataPropertyName = "codFiscaleId";
            this.codFiscaleIdDataGridViewTextBoxColumn.HeaderText = "codFiscaleId";
            this.codFiscaleIdDataGridViewTextBoxColumn.Name = "codFiscaleIdDataGridViewTextBoxColumn";
            this.codFiscaleIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ragioneSocialeDataGridViewTextBoxColumn
            // 
            this.ragioneSocialeDataGridViewTextBoxColumn.DataPropertyName = "ragioneSociale";
            this.ragioneSocialeDataGridViewTextBoxColumn.HeaderText = "ragioneSociale";
            this.ragioneSocialeDataGridViewTextBoxColumn.Name = "ragioneSocialeDataGridViewTextBoxColumn";
            this.ragioneSocialeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // indirizzoSedeDataGridViewTextBoxColumn
            // 
            this.indirizzoSedeDataGridViewTextBoxColumn.DataPropertyName = "indirizzoSede";
            this.indirizzoSedeDataGridViewTextBoxColumn.HeaderText = "indirizzoSede";
            this.indirizzoSedeDataGridViewTextBoxColumn.Name = "indirizzoSedeDataGridViewTextBoxColumn";
            this.indirizzoSedeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsComuneDataGridViewTextBoxColumn
            // 
            this.dsComuneDataGridViewTextBoxColumn.DataPropertyName = "dsComune";
            this.dsComuneDataGridViewTextBoxColumn.HeaderText = "dsComune";
            this.dsComuneDataGridViewTextBoxColumn.Name = "dsComuneDataGridViewTextBoxColumn";
            this.dsComuneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsSiglaProvinciaDataGridViewTextBoxColumn
            // 
            this.dsSiglaProvinciaDataGridViewTextBoxColumn.DataPropertyName = "dsSiglaProvincia";
            this.dsSiglaProvinciaDataGridViewTextBoxColumn.HeaderText = "dsSiglaProvincia";
            this.dsSiglaProvinciaDataGridViewTextBoxColumn.Name = "dsSiglaProvinciaDataGridViewTextBoxColumn";
            this.dsSiglaProvinciaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nazioneDataGridViewTextBoxColumn
            // 
            this.nazioneDataGridViewTextBoxColumn.DataPropertyName = "nazione";
            this.nazioneDataGridViewTextBoxColumn.HeaderText = "nazione";
            this.nazioneDataGridViewTextBoxColumn.Name = "nazioneDataGridViewTextBoxColumn";
            this.nazioneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // zipCodeDataGridViewTextBoxColumn
            // 
            this.zipCodeDataGridViewTextBoxColumn.DataPropertyName = "zipCode";
            this.zipCodeDataGridViewTextBoxColumn.HeaderText = "zipCode";
            this.zipCodeDataGridViewTextBoxColumn.Name = "zipCodeDataGridViewTextBoxColumn";
            this.zipCodeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cdTipoIncaricoAppaltoDataGridViewTextBoxColumn
            // 
            this.cdTipoIncaricoAppaltoDataGridViewTextBoxColumn.DataPropertyName = "cdTipoIncaricoAppalto";
            this.cdTipoIncaricoAppaltoDataGridViewTextBoxColumn.HeaderText = "cdTipoIncaricoAppalto";
            this.cdTipoIncaricoAppaltoDataGridViewTextBoxColumn.Name = "cdTipoIncaricoAppaltoDataGridViewTextBoxColumn";
            this.cdTipoIncaricoAppaltoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsTipoIncaricoAppaltoDataGridViewTextBoxColumn
            // 
            this.dsTipoIncaricoAppaltoDataGridViewTextBoxColumn.DataPropertyName = "dsTipoIncaricoAppalto";
            this.dsTipoIncaricoAppaltoDataGridViewTextBoxColumn.HeaderText = "dsTipoIncaricoAppalto";
            this.dsTipoIncaricoAppaltoDataGridViewTextBoxColumn.Name = "dsTipoIncaricoAppaltoDataGridViewTextBoxColumn";
            this.dsTipoIncaricoAppaltoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pivaImpresaAssocDataGridViewTextBoxColumn
            // 
            this.pivaImpresaAssocDataGridViewTextBoxColumn.DataPropertyName = "PivaImpresaAssoc";
            this.pivaImpresaAssocDataGridViewTextBoxColumn.HeaderText = "PivaImpresaAssoc";
            this.pivaImpresaAssocDataGridViewTextBoxColumn.Name = "pivaImpresaAssocDataGridViewTextBoxColumn";
            this.pivaImpresaAssocDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // GridViewIndirizzi
            // 
            this.GridViewIndirizzi.AllowUserToDeleteRows = false;
            this.GridViewIndirizzi.AutoGenerateColumns = false;
            this.GridViewIndirizzi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewIndirizzi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dsComuneDataGridViewTextBoxColumn1,
            this.dsProvinciaDataGridViewTextBoxColumn,
            this.dtInizioLavoriDataGridViewTextBoxColumn,
            this.durataLavoriDataGridViewTextBoxColumn,
            this.dsDurataLavoriDataGridViewTextBoxColumn,
            this.indirizzoDataGridViewTextBoxColumn,
            this.cdAreaDataGridViewTextBoxColumn,
            this.dsAreaDataGridViewTextBoxColumn,
            this.areaNoteDataGridViewTextBoxColumn,
            this.nrMassimoLavoratoriDataGridViewTextBoxColumn,
            this.cAPindirCantiereDataGridViewTextBoxColumn,
            this.idagpDataGridViewTextBoxColumn1});
            this.GridViewIndirizzi.DataSource = this.indirizzoCantTR4BindingSource;
            this.GridViewIndirizzi.Location = new System.Drawing.Point(17, 19);
            this.GridViewIndirizzi.Name = "GridViewIndirizzi";
            this.GridViewIndirizzi.ReadOnly = true;
            this.GridViewIndirizzi.Size = new System.Drawing.Size(885, 147);
            this.GridViewIndirizzi.TabIndex = 0;
            // 
            // GridViewPersone
            // 
            this.GridViewPersone.AllowUserToDeleteRows = false;
            this.GridViewPersone.AutoGenerateColumns = false;
            this.GridViewPersone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewPersone.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ruoloDataGridViewTextBoxColumn,
            this.codFiscaleDocumentoDataGridViewTextBoxColumn,
            this.cognomeDataGridViewTextBoxColumn,
            this.nomeDataGridViewTextBoxColumn,
            this.dtNascitaDataGridViewTextBoxColumn,
            this.nazioneDataGridViewTextBoxColumn1,
            this.regioneDataGridViewTextBoxColumn,
            this.dsProvinciaDataGridViewTextBoxColumn1,
            this.dsComuneDataGridViewTextBoxColumn2,
            this.indirizzoDataGridViewTextBoxColumn1,
            this.zipCodeDataGridViewTextBoxColumn1});
            this.GridViewPersone.DataSource = this.personaCantTR4BindingSource;
            this.GridViewPersone.Location = new System.Drawing.Point(17, 19);
            this.GridViewPersone.Name = "GridViewPersone";
            this.GridViewPersone.ReadOnly = true;
            this.GridViewPersone.Size = new System.Drawing.Size(885, 147);
            this.GridViewPersone.TabIndex = 0;
            // 
            // indirizzoCantTR4BindingSource
            // 
            this.indirizzoCantTR4BindingSource.DataSource = typeof(Cemi.NotifichePreliminari.WindowsApp.ServiceReferenceCantieri.IndirizzoCantTR4);
            // 
            // dsComuneDataGridViewTextBoxColumn1
            // 
            this.dsComuneDataGridViewTextBoxColumn1.DataPropertyName = "dsComune";
            this.dsComuneDataGridViewTextBoxColumn1.HeaderText = "dsComune";
            this.dsComuneDataGridViewTextBoxColumn1.Name = "dsComuneDataGridViewTextBoxColumn1";
            this.dsComuneDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dsProvinciaDataGridViewTextBoxColumn
            // 
            this.dsProvinciaDataGridViewTextBoxColumn.DataPropertyName = "dsProvincia";
            this.dsProvinciaDataGridViewTextBoxColumn.HeaderText = "dsProvincia";
            this.dsProvinciaDataGridViewTextBoxColumn.Name = "dsProvinciaDataGridViewTextBoxColumn";
            this.dsProvinciaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtInizioLavoriDataGridViewTextBoxColumn
            // 
            this.dtInizioLavoriDataGridViewTextBoxColumn.DataPropertyName = "dtInizioLavori";
            this.dtInizioLavoriDataGridViewTextBoxColumn.HeaderText = "dtInizioLavori";
            this.dtInizioLavoriDataGridViewTextBoxColumn.Name = "dtInizioLavoriDataGridViewTextBoxColumn";
            this.dtInizioLavoriDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // durataLavoriDataGridViewTextBoxColumn
            // 
            this.durataLavoriDataGridViewTextBoxColumn.DataPropertyName = "durataLavori";
            this.durataLavoriDataGridViewTextBoxColumn.HeaderText = "durataLavori";
            this.durataLavoriDataGridViewTextBoxColumn.Name = "durataLavoriDataGridViewTextBoxColumn";
            this.durataLavoriDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsDurataLavoriDataGridViewTextBoxColumn
            // 
            this.dsDurataLavoriDataGridViewTextBoxColumn.DataPropertyName = "dsDurataLavori";
            this.dsDurataLavoriDataGridViewTextBoxColumn.HeaderText = "dsDurataLavori";
            this.dsDurataLavoriDataGridViewTextBoxColumn.Name = "dsDurataLavoriDataGridViewTextBoxColumn";
            this.dsDurataLavoriDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // indirizzoDataGridViewTextBoxColumn
            // 
            this.indirizzoDataGridViewTextBoxColumn.DataPropertyName = "indirizzo";
            this.indirizzoDataGridViewTextBoxColumn.HeaderText = "indirizzo";
            this.indirizzoDataGridViewTextBoxColumn.Name = "indirizzoDataGridViewTextBoxColumn";
            this.indirizzoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cdAreaDataGridViewTextBoxColumn
            // 
            this.cdAreaDataGridViewTextBoxColumn.DataPropertyName = "cdArea";
            this.cdAreaDataGridViewTextBoxColumn.HeaderText = "cdArea";
            this.cdAreaDataGridViewTextBoxColumn.Name = "cdAreaDataGridViewTextBoxColumn";
            this.cdAreaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsAreaDataGridViewTextBoxColumn
            // 
            this.dsAreaDataGridViewTextBoxColumn.DataPropertyName = "dsArea";
            this.dsAreaDataGridViewTextBoxColumn.HeaderText = "dsArea";
            this.dsAreaDataGridViewTextBoxColumn.Name = "dsAreaDataGridViewTextBoxColumn";
            this.dsAreaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // areaNoteDataGridViewTextBoxColumn
            // 
            this.areaNoteDataGridViewTextBoxColumn.DataPropertyName = "areaNote";
            this.areaNoteDataGridViewTextBoxColumn.HeaderText = "areaNote";
            this.areaNoteDataGridViewTextBoxColumn.Name = "areaNoteDataGridViewTextBoxColumn";
            this.areaNoteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nrMassimoLavoratoriDataGridViewTextBoxColumn
            // 
            this.nrMassimoLavoratoriDataGridViewTextBoxColumn.DataPropertyName = "nrMassimoLavoratori";
            this.nrMassimoLavoratoriDataGridViewTextBoxColumn.HeaderText = "nrMassimoLavoratori";
            this.nrMassimoLavoratoriDataGridViewTextBoxColumn.Name = "nrMassimoLavoratoriDataGridViewTextBoxColumn";
            this.nrMassimoLavoratoriDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cAPindirCantiereDataGridViewTextBoxColumn
            // 
            this.cAPindirCantiereDataGridViewTextBoxColumn.DataPropertyName = "CAPindirCantiere";
            this.cAPindirCantiereDataGridViewTextBoxColumn.HeaderText = "CAPindirCantiere";
            this.cAPindirCantiereDataGridViewTextBoxColumn.Name = "cAPindirCantiereDataGridViewTextBoxColumn";
            this.cAPindirCantiereDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // idagpDataGridViewTextBoxColumn1
            // 
            this.idagpDataGridViewTextBoxColumn1.DataPropertyName = "idagp";
            this.idagpDataGridViewTextBoxColumn1.HeaderText = "idagp";
            this.idagpDataGridViewTextBoxColumn1.Name = "idagpDataGridViewTextBoxColumn1";
            this.idagpDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // personaCantTR4BindingSource
            // 
            this.personaCantTR4BindingSource.DataSource = typeof(Cemi.NotifichePreliminari.WindowsApp.ServiceReferenceCantieri.PersonaCantTR4);
            // 
            // ruoloDataGridViewTextBoxColumn
            // 
            this.ruoloDataGridViewTextBoxColumn.DataPropertyName = "ruolo";
            this.ruoloDataGridViewTextBoxColumn.HeaderText = "ruolo";
            this.ruoloDataGridViewTextBoxColumn.Name = "ruoloDataGridViewTextBoxColumn";
            this.ruoloDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codFiscaleDocumentoDataGridViewTextBoxColumn
            // 
            this.codFiscaleDocumentoDataGridViewTextBoxColumn.DataPropertyName = "codFiscaleDocumento";
            this.codFiscaleDocumentoDataGridViewTextBoxColumn.HeaderText = "codFiscaleDocumento";
            this.codFiscaleDocumentoDataGridViewTextBoxColumn.Name = "codFiscaleDocumentoDataGridViewTextBoxColumn";
            this.codFiscaleDocumentoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cognomeDataGridViewTextBoxColumn
            // 
            this.cognomeDataGridViewTextBoxColumn.DataPropertyName = "cognome";
            this.cognomeDataGridViewTextBoxColumn.HeaderText = "cognome";
            this.cognomeDataGridViewTextBoxColumn.Name = "cognomeDataGridViewTextBoxColumn";
            this.cognomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomeDataGridViewTextBoxColumn
            // 
            this.nomeDataGridViewTextBoxColumn.DataPropertyName = "nome";
            this.nomeDataGridViewTextBoxColumn.HeaderText = "nome";
            this.nomeDataGridViewTextBoxColumn.Name = "nomeDataGridViewTextBoxColumn";
            this.nomeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtNascitaDataGridViewTextBoxColumn
            // 
            this.dtNascitaDataGridViewTextBoxColumn.DataPropertyName = "dtNascita";
            this.dtNascitaDataGridViewTextBoxColumn.HeaderText = "dtNascita";
            this.dtNascitaDataGridViewTextBoxColumn.Name = "dtNascitaDataGridViewTextBoxColumn";
            this.dtNascitaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nazioneDataGridViewTextBoxColumn1
            // 
            this.nazioneDataGridViewTextBoxColumn1.DataPropertyName = "nazione";
            this.nazioneDataGridViewTextBoxColumn1.HeaderText = "nazione";
            this.nazioneDataGridViewTextBoxColumn1.Name = "nazioneDataGridViewTextBoxColumn1";
            this.nazioneDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // regioneDataGridViewTextBoxColumn
            // 
            this.regioneDataGridViewTextBoxColumn.DataPropertyName = "regione";
            this.regioneDataGridViewTextBoxColumn.HeaderText = "regione";
            this.regioneDataGridViewTextBoxColumn.Name = "regioneDataGridViewTextBoxColumn";
            this.regioneDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsProvinciaDataGridViewTextBoxColumn1
            // 
            this.dsProvinciaDataGridViewTextBoxColumn1.DataPropertyName = "dsProvincia";
            this.dsProvinciaDataGridViewTextBoxColumn1.HeaderText = "dsProvincia";
            this.dsProvinciaDataGridViewTextBoxColumn1.Name = "dsProvinciaDataGridViewTextBoxColumn1";
            this.dsProvinciaDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dsComuneDataGridViewTextBoxColumn2
            // 
            this.dsComuneDataGridViewTextBoxColumn2.DataPropertyName = "dsComune";
            this.dsComuneDataGridViewTextBoxColumn2.HeaderText = "dsComune";
            this.dsComuneDataGridViewTextBoxColumn2.Name = "dsComuneDataGridViewTextBoxColumn2";
            this.dsComuneDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // indirizzoDataGridViewTextBoxColumn1
            // 
            this.indirizzoDataGridViewTextBoxColumn1.DataPropertyName = "indirizzo";
            this.indirizzoDataGridViewTextBoxColumn1.HeaderText = "indirizzo";
            this.indirizzoDataGridViewTextBoxColumn1.Name = "indirizzoDataGridViewTextBoxColumn1";
            this.indirizzoDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // zipCodeDataGridViewTextBoxColumn1
            // 
            this.zipCodeDataGridViewTextBoxColumn1.DataPropertyName = "zipCode";
            this.zipCodeDataGridViewTextBoxColumn1.HeaderText = "zipCode";
            this.zipCodeDataGridViewTextBoxColumn1.Name = "zipCodeDataGridViewTextBoxColumn1";
            this.zipCodeDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(947, 791);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRicerca);
            this.Controls.Add(this.txtNumeroNotifica);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewCantiere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cantiereTR4BindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewImprese)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.impresaCantTR4BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewIndirizzi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewPersone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.indirizzoCantTR4BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaCantTR4BindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumeroNotifica;
        private System.Windows.Forms.Button btnRicerca;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView GridViewCantiere;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdContrattoAppaltoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdContrattoCIGDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdContrattoCUIDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrNotificaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrProtocolloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdTipoOperaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsTipoOperaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtComunicazioneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrLavAutonomiDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrImpreseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsTipoCategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsTipoTipologiaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsAltraCategoriaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsAltraTipologiaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ammontareComplessivoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flArt90C11DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flArt99C1BDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn flArt99C1CDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtAggNotificaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idagpDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource cantiereTR4BindingSource;
        private System.Windows.Forms.DataGridView GridViewImprese;
        private System.Windows.Forms.DataGridViewTextBoxColumn codFiscaleIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ragioneSocialeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn indirizzoSedeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsComuneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsSiglaProvinciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazioneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zipCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdTipoIncaricoAppaltoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsTipoIncaricoAppaltoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pivaImpresaAssocDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource impresaCantTR4BindingSource;
        private System.Windows.Forms.DataGridView GridViewIndirizzi;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsComuneDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsProvinciaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtInizioLavoriDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn durataLavoriDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsDurataLavoriDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn indirizzoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdAreaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsAreaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn areaNoteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrMassimoLavoratoriDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAPindirCantiereDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idagpDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource indirizzoCantTR4BindingSource;
        private System.Windows.Forms.DataGridView GridViewPersone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ruoloDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codFiscaleDocumentoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cognomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtNascitaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazioneDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn regioneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsProvinciaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsComuneDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn indirizzoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn zipCodeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource personaCantTR4BindingSource;
    }
}

