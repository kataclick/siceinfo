﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Globalization;
using Cemi.NotifichePreliminari.Data;
using Cemi.NotifichePreliminari.Type.Domain;
using Cemi.NotifichePreliminari.Type.Entities;
using System.Data.Entity;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Cemi.NotifichePreliminari.ServiceReferenceCantieri.ServiceReferenceCantieri;

namespace Cemi.NotifichePreliminari.Business.Imp
{
    public class DataProvider : IDataProvider
    {

        //SecurityType st = CreateSecurityType();
        //Intestazione intest = CreateIntestazione();

        public SecurityType CreateSecurityType()
        {
            SecurityType st;
            st = new SecurityType();
            st.UsernameToken = new UsernameToken();

            st.UsernameToken.Username = "cassaedilemilano";
            st.UsernameToken.Password = "JeRXVcVf";
            st.Timestamp = new Timestamp();
            st.Timestamp.Created = DateTime.Now.AddHours(-12);
            st.Timestamp.Expires = DateTime.Now.AddHours(12);
            return st;
        }

        public Intestazione CreateIntestazione()
        {
            Intestazione intest = new Intestazione();
            intest.IntestazioneMessaggio = new IntestazioneMessaggio();
            intest.IntestazioneMessaggio.Mittente = new IdentificativoParte[1];

            IdentificativoParte ident = new IdentificativoParte();
            intest.IntestazioneMessaggio.Mittente[0] = ident;
            ident.tipo = "CodicePA";
            ident.Value = "Cassa Edile Milano, Lodi, Monza e Brianza";
            ident.indirizzoTelematico = "www.cassaedilemilano.it";

            IdentificativoParte idenDest = new IdentificativoParte();
            intest.IntestazioneMessaggio.Destinatario = new Destinatario();
            intest.IntestazioneMessaggio.Destinatario.IdentificativoParte = idenDest;
            idenDest.tipo = "CodicePA";
            idenDest.Value = "Regione Lombardia";


            intest.IntestazioneMessaggio.ProfiloCollaborazione = new ProfiloCollaborazione();
            intest.IntestazioneMessaggio.ProfiloCollaborazione.Value =
                ProfiloCollaborazioneBaseType.EGOV_IT_ServizioSincrono;

            intest.IntestazioneMessaggio.Servizio = new Servizio();
            intest.IntestazioneMessaggio.Servizio.tipo = "TEST";
            intest.IntestazioneMessaggio.Servizio.Value = "WS - CANTIERI";

            intest.ListaTrasmissioni = new Trasmissione[1];
            intest.ListaTrasmissioni[0] = new Trasmissione();
            intest.ListaTrasmissioni[0].Origine = new Origine();
            intest.ListaTrasmissioni[0].Origine.IdentificativoParte = ident;
            intest.ListaTrasmissioni[0].OraRegistrazione = new OraRegistrazione();
            intest.ListaTrasmissioni[0].OraRegistrazione.Value = DateTime.Now;
            intest.ListaTrasmissioni[0].Destinazione = new Destinazione();
            intest.ListaTrasmissioni[0].Destinazione.IdentificativoParte = idenDest;


            intest.IntestazioneMessaggio.Messaggio = new Messaggio();
            intest.IntestazioneMessaggio.Messaggio.Identificatore = DateTime.Now.ToString("ddMMyyyyHHmmss");
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione = new OraRegistrazione();
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione.tempo = OraRegistrazioneTempo.EGOV_IT_Locale;
            intest.IntestazioneMessaggio.Messaggio.OraRegistrazione.Value = DateTime.Now;
            intest.IntestazioneMessaggio.Messaggio.RiferimentoMessaggio = DateTime.Now.ToString("ddMMyyyyHHmmss");
            intest.IntestazioneMessaggio.Messaggio.ScadenzaSpecified = false;

            intest.IntestazioneMessaggio.Azione = "WS - CANTIERI";
            intest.IntestazioneMessaggio.ProfiloTrasmissione = new ProfiloTrasmissione();
            intest.IntestazioneMessaggio.ProfiloTrasmissione.confermaRicezione = false;
            intest.IntestazioneMessaggio.ProfiloTrasmissione.inoltro = ProfiloTrasmissioneInoltro.EGOV_IT_PIUDIUNAVOLTA;

            intest.IntestazioneMessaggio.ProfiloCollaborazione = new ProfiloCollaborazione();
            intest.IntestazioneMessaggio.ProfiloCollaborazione.Value =
                ProfiloCollaborazioneBaseType.EGOV_IT_ServizioSincrono;

            intest.IntestazioneMessaggio.Collaborazione = String.Format("CassaEdileMilanoRegioneLombardia{0}",
                                                                        DateTime.Now.ToString("ddMMyyyyHHmmss"));
            return intest;
        }

        public void UploadFromEdilconnect()
        {
            DateTime? dataQuery = new DateTime();
            //string dataUpdate = string.Empty;
            using (var context = new SiceContext())
            {
                dataQuery = (from Edilconnect in context.NotificaPreliminareEdilconnect
                             orderby Edilconnect.DataUltimoControllo descending
                             select Edilconnect.DataUltimoControllo).FirstOrDefault();
            }

            /*
            if (dataQuery.HasValue)
            {
                dataUpdate = String.Format("{0:yyyyMMdd}", dataQuery);
            }
            */

            //TEMPORANEO: PER CARICAMENTO TOTALE
            //dataQuery = null;
            //TEMPORANEO: PER CARICAMENTO TOTALE

            List<TbAslNotifica> NotificheEdilconnect = new List<TbAslNotifica>();

            //List<TbAslNotificaLight> NotificheEdilconnect = new List<TbAslNotificaLight>();

            Console.WriteLine("");
            Console.WriteLine("-- Recupero notifiche da Edilconnect in corso --");
            Console.WriteLine("");

            if (!dataQuery.HasValue)
            {
                try
                {
                    using (var context = new EdilConnectContext())
                    {
                        NotificheEdilconnect = (from ne in context.TbAslNotifica
                                                where DbFunctions.TruncateTime(ne.DbLastCheckedDate) >= DbFunctions.AddDays(DateTime.Now, -15)
                                                orderby DbFunctions.TruncateTime(ne.DbDataInserimento) descending
                                                select ne).ToList();
                    }
                }
                catch (Exception ex)
                {
                    TracciaErrore(ex.Message, "Select EdilConnect totale");
                }
            }
            else
            {
                try
                {
                    using (var context = new EdilConnectContext())
                    {
                        NotificheEdilconnect = (from ne in context.TbAslNotifica
                                                where DbFunctions.TruncateTime(ne.DbLastCheckedDate) >= DbFunctions.TruncateTime(dataQuery)
                                                orderby DbFunctions.TruncateTime(ne.DbDataInserimento) descending
                                                select ne).ToList();
                        /*
                        NotificheEdilconnect = (from ne in context.TbAslNotifica
                                                                    where DbFunctions.TruncateTime(ne.DbLastCheckedDate) >= DbFunctions.TruncateTime(dataQuery) &&
                                                                        DbFunctions.TruncateTime(ne.DbLastCheckedDate) == DbFunctions.TruncateTime(ne.DbDataInserimento)
                                                                    orderby ne.DbDataInserimento descending
                                                select ne).ToList();

                        NotificheEdilconnect.AddRange((from ne in context.TbAslNotifica
                                                        where DbFunctions.TruncateTime(ne.DbLastCheckedDate) >= DbFunctions.TruncateTime(dataQuery) &&
                                                            DbFunctions.TruncateTime(ne.DbDataUltimaModifica) >= DbFunctions.AddDays(DbFunctions.TruncateTime(ne.DbLastCheckedDate),-10)
                                                        orderby ne.DbDataInserimento descending
                                                       select ne).ToList());
                        
                        
                        List<TbAslNotificaLight> lghList = new List<TbAslNotificaLight>();
                        
                        foreach (TbAslNotificaLight x in NotificheEdilconnectLight)
                        {
                            TbAslNotificaLight lghPro = new TbAslNotificaLight();
                            lghPro.DbNumeroNotifica = x.DbNumeroNotifica;
                            lghPro.DbDataInserimento = x.DbDataInserimento;
                            lghPro.DbDataUltimaModifica = x.DbDataUltimaModifica;
                            lghPro.DbLastCheckedDate = x.DbLastCheckedDate;

                           /// lghList.Add(lghPro);
                        }
                        
                        */
                    }
                }
                catch (Exception ex)
                {
                    TracciaErrore(ex.Message, "Select EdilConnect con data aggiornamento");
                }
            }

            Console.WriteLine($"-- Notifiche caricate da Edilconnect: {NotificheEdilconnect.Count} --");
            Console.WriteLine("");

            #region Update globale

            List<NotificaPreliminareEdilconnect> notDB = new List<NotificaPreliminareEdilconnect>();

            using (var context = new SiceContext())
            {
                notDB = (from siceNot in context.NotificaPreliminareEdilconnect
                                                              select siceNot).ToList();
            }

            var notSiceUp = from connectNot in NotificheEdilconnect
                            join siceNot in notDB
                            on connectNot.DbNumeroNotifica equals siceNot.NumeroNotifica into st
                            from notTot in st.DefaultIfEmpty()
                            select new { connectNot.DbNumeroNotifica, connectNot.DbDataInserimento, connectNot.DbDataUltimaModifica, connectNot.DbLastCheckedDate, presenzaSice = (notTot == null ? String.Empty : notTot.NumeroNotifica), SiceDataComunicazione = (notTot == null ? null : notTot.DataComunicazioneNotifica), SiceDataAggiornamento = (notTot == null ? null : notTot.DataAggiornamentoRecord), SiceDataCheck = (notTot == null ? null : notTot.DataUltimoControllo) };

            var notSiceFiltro = from connectNot in notSiceUp
                                where connectNot.presenzaSice == null || (connectNot.SiceDataAggiornamento != connectNot.DbDataUltimaModifica || connectNot.SiceDataComunicazione != connectNot.DbDataInserimento || connectNot.SiceDataCheck != connectNot.DbLastCheckedDate)
                                select connectNot;

            int countTot = notSiceFiltro.Count();
            Console.WriteLine($"");
            Console.WriteLine($"");
            Console.WriteLine($"-- Notifiche da aggiornare: {countTot} --");
            Console.WriteLine($"");

            int count = 0;

            using (var context = new SiceContext())
            { 
                foreach (var not in notSiceFiltro)
                {
                    count += 1;
                    if (!String.IsNullOrEmpty(not.presenzaSice))
                    {
                        /*var query = from sn in context.NotificaPreliminareEdilconnect
                                    where sn.NumeroNotifica == not.DbNumeroNotifica
                                    select sn;

                        foreach (NotificaPreliminareEdilconnect np in query)
                        {
                            np.DataComunicazioneNotifica = not.DbDataInserimento;
                            np.DataAggiornamentoNotifica = not.DbDataUltimaModifica;
                            np.DataUltimoControllo = not.DbLastCheckedDate;
                            np.DataAggiornamentoRecord = DateTime.Now;
                        }
                        */

                        try
                        {
                            context.NotifichePreliminariEdilconnectUpdate(not.DbNumeroNotifica, not.DbDataUltimaModifica, not.DbLastCheckedDate);
                            Console.WriteLine($"-- {count}/{countTot}: Aggiornata notifica {not.DbNumeroNotifica} --");
                        }
                        catch (Exception ex)
                        {
                            TracciaErrore(ex.Message, "Update EdilConnect");
                        }
                    }
                    else
                    {
                        /*NotificaPreliminareEdilconnect np = new NotificaPreliminareEdilconnect();
                        np.NumeroNotifica = not.DbNumeroNotifica;
                        np.DataComunicazioneNotifica = not.DbDataInserimento;
                        np.DataAggiornamentoNotifica = not.DbDataUltimaModifica;
                        np.DataUltimoControllo = not.DbLastCheckedDate;
                        np.DataInserimentoRecord = DateTime.Now;

                        context.NotificaPreliminareEdilconnect.Add(np);
                        */

                        try
                        {
                            context.NotifichePreliminariEdilconnectInsert(not.DbNumeroNotifica, not.DbDataInserimento, not.DbDataUltimaModifica, not.DbLastCheckedDate);
                            Console.WriteLine($"-- {count}/{countTot}: Inserita notifica {not.DbNumeroNotifica} --");
                        }
                        catch (Exception ex)
                        {
                            TracciaErrore(ex.Message, "Insert EdilConnect");
                        }

                    }
                    //var notUpdate = (from connect in NotificheEdilconnect
                    //                 where connect.DbNumeroNotifica == not.NumeroNotifica
                    //                 select connect).SingleOrDefault();

                    //not.DataUltimoControllo = notUpdate.DbLastCheckedDate;
                    //not.DataAggiornamentoNotifica = notUpdate.DbDataUltimaModifica;
                    //not.DataAggiornamentoRecord = DateTime.Now;

                    //Console.WriteLine($"-- Aggiornata notifica {not.NumeroNotifica} --");
                }



                /*

                                List<TbAslNotifica> notSiceIn = (from connectNot in NotificheEdilconnect
                                                                                  where !(from siceNot in context.NotificaPreliminareEdilconnect
                                                                                          select siceNot.NumeroNotifica).Contains(connectNot.DbNumeroNotifica)
                                                                                  select connectNot).ToList();

                                Console.WriteLine($"");
                                Console.WriteLine($"");
                                Console.WriteLine($"-- Notifiche da inserire: {notSiceIn.Count} --");
                                Console.WriteLine($"");

                                                     //(from siceNot in context.NotificaPreliminareEdilconnect
                                                     //                                             where !(from connectNot in NotificheEdilconnect
                                                     //                                                     select connectNot.DbNumeroNotifica).Contains(siceNot.NumeroNotifica)
                                                     //                                                     select siceNot).ToList();


                                foreach (TbAslNotifica not in notSiceIn)
                                {
                                    NotificaPreliminareEdilconnect nped = new NotificaPreliminareEdilconnect();

                                    nped.NumeroNotifica = not.DbNumeroNotifica;
                                    nped.DataComunicazioneNotifica = not.DbDataInserimento;
                                    nped.DataAggiornamentoNotifica = not.DbDataUltimaModifica;
                                    nped.DataUltimoControllo = not.DbLastCheckedDate;
                                    nped.DataInserimentoRecord = DateTime.Now;

                                    context.NotificaPreliminareEdilconnect.Add(nped);

                                    Console.WriteLine($"-- Inserita notifica {nped.NumeroNotifica} --");


                                                        //TbAslNotifica notUpdate = (from connect in NotificheEdilconnect
                                                        //                           where connect.DbNumeroNotifica == not.NumeroNotifica
                                                        //                           select connect).SingleOrDefault();

                                                        //not.DataUltimoControllo = notUpdate.DbLastCheckedDate;
                                                        //not.DataAggiornamentoNotifica = notUpdate.DbDataUltimaModifica;
                                                        //not.DataAggiornamentoRecord = DateTime.Now;

                                }
                */

                /*
                try
                {
                    context.SaveChanges();
                }

                catch (Exception ex)
                {
                    TracciaErrore(ex.Message, "Update EdilConnect");
                }
                */


                
            }

            #endregion

            
            #region Update notifica singola
            /*
            int numNot = 0;
            int ciclo = 0;
            int chkCase = 0;
            foreach (TbAslNotifica not in NotificheEdilconnect)
            {
                ciclo = ciclo + 1;

                using (var context = new SiceContext())
                {
                    numNot = (from EdilConnectCount in context.NotificaPreliminareEdilconnect
                              where EdilConnectCount.NumeroNotifica == not.DbNumeroNotifica
                              select EdilConnectCount.NumeroNotifica).Count();
                }

                if (numNot == 0)
                {
                    try
                    {
                        using (var context = new SiceContext())
                        {
                            context.NotifichePreliminariEdilconnectInsert(not.DbNumeroNotifica, not.DbDataInserimento, not.DbDataUltimaModifica, not.DbLastCheckedDate);
                        }
                        
                        if (chkCase != 1)
                        {
                            Console.WriteLine("");
                        }
                        Console.CursorLeft = 0;
                        Console.Write($"-- {ciclo}/{NotificheEdilconnect.Count} - Caricata la notifica {not.DbNumeroNotifica} --");
                        //Console.WriteLine("");
                        chkCase = 1;
                    }
                    catch (Exception ex)
                    {
                        TracciaErrore(ex.Message, "Edilconnect Insert");
                    }
                }
                else
                {
                    try
                    {
                        using (var context = new SiceContext())
                        {
                            NotificaPreliminareEdilconnect n = null;
                            n = (from ne in context.NotificaPreliminareEdilconnect
                                                                where ne.NumeroNotifica == not.DbNumeroNotifica
                                                                select ne).FirstOrDefault();

                            DateTime dtAggSice = DateTime.MinValue;
                            DateTime dtLastChkSice = DateTime.MinValue;

                            if (!(n.DataAggiornamentoNotifica == null))
                            {
                                dtAggSice = (DateTime)n.DataAggiornamentoNotifica;
                            }

                            if (!(n.DataUltimoControllo == null))
                            {
                                dtLastChkSice = (DateTime)n.DataUltimoControllo;
                            }

                            if (dtAggSice.Date != not.DbDataUltimaModifica.Date)
                            {
                                n.DataAggiornamentoNotifica = not.DbDataUltimaModifica;
                                n.DataUltimoControllo = not.DbLastCheckedDate;
                                n.DataAggiornamentoRecord = DateTime.Now;

                                context.SaveChanges();
                                
                                if (chkCase != 2)
                                {
                                    Console.WriteLine("");
                                }
                                Console.CursorLeft = 0;
                                Console.Write($"-- {ciclo}/{NotificheEdilconnect.Count} - Notifica {not.DbNumeroNotifica} aggiornata --");
                                //Console.WriteLine("");
                                chkCase = 2;
                            }

                            if ((dtAggSice.Date == not.DbDataUltimaModifica.Date) && (dtLastChkSice.Date != not.DbLastCheckedDate.Date))
                            {
                                n.DataUltimoControllo = not.DbLastCheckedDate;

                                context.SaveChanges();
                                
                                if (chkCase != 3)
                                {
                                    Console.WriteLine("");
                                }
                                Console.CursorLeft = 0;
                                Console.Write($"-- {ciclo}/{NotificheEdilconnect.Count} - Notifica {not.DbNumeroNotifica} già aggiornata --");
                                //Console.WriteLine("");
                                chkCase = 3;
                            }

                            if ((dtAggSice.Date == not.DbDataUltimaModifica.Date) && (dtLastChkSice.Date == not.DbLastCheckedDate.Date))
                            {
                                if (chkCase != 4)
                                {
                                    Console.WriteLine("");
                                }
                                Console.CursorLeft = 0;
                                Console.Write($"-- {ciclo}/{NotificheEdilconnect.Count} - Notifica invariata --");
                                chkCase = 4;
                            }
                            //context.NotifichePreliminariEdilconnectUpdate(not.DbNumeroNotifica, not.DbDataUltimaModifica, not.DbLastCheckedDate);
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        TracciaErrore(ex.Message, "Edilconnect Update");
                    }
                }
            }
            */
            #endregion


            Console.WriteLine("");
        }

        public void InsertNewNotifiche()
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            List<NotificaPreliminareEdilconnect> newNotifiche = new List<NotificaPreliminareEdilconnect>();
            using (var context = new SiceContext())
            {
                newNotifiche = (from notEd in context.NotificaPreliminareEdilconnect
                                //join notSi in context.NotifichePreliminariCantieri on notEd.NumeroNotifica equals notSi.NumeroNotifica into notTot
                                //from item in notTot.DefaultIfEmpty()
                                where DbFunctions.Left(DbFunctions.Right(notEd.NumeroNotifica, 5), 1) == "/" &&
                                !(from notSi in context.NotificaPreliminareCantiere
                                  select notSi.NumeroNotifica).Contains(notEd.NumeroNotifica) &&
                                //string.IsNullOrEmpty(notSi.NumeroNotifica) &&
                                notEd.DataComunicazioneNotifica <= DbFunctions.AddDays(DbFunctions.TruncateTime(DateTime.Now), -1)
                                select notEd).ToList();
            }

            Console.WriteLine($"-- NOTIFICHE DA INSERIRE: {newNotifiche.Count} --");
            Console.WriteLine(" ");

            int totCantieri = 0;

            foreach (NotificaPreliminareEdilconnect npe in newNotifiche)
            {
                // CHIAMATA AL SERVIZIO
                List<CantiereTR4> canList = RecuperaCantieri(npe.NumeroNotifica);
                // CHIAMATA AL SERVIZIO
                
                foreach (CantiereTR4 can in canList)
                {
                    int totImprese = 0;
                    int totIndirizzi = 0;
                    int totPersone = 0;

                    #region Carico i cantieri
                    //Console.WriteLine("-- SALVO SU DB IL CANTIERE {0} --", i+1);
                    InsertCantiere(can);
                    totCantieri = totCantieri + 1;
                    #endregion

                    #region Recupero e carico le imprese per ogni cantere

                    // CHIAMATA AL SERVIZIO
                    List<ImpresaCantTR4> impList = RecuperaImprese(can.nrNotifica);
                    // CHIAMATA AL SERVIZIO

                    totImprese = totImprese + impList.Count;

                    if (impList.Count > 0)
                    {
                        foreach (ImpresaCantTR4 imp in impList)
                        {
                            //Console.WriteLine("-- SALVO SU DB L'IMPRESA {0} DEL CANTIERE {1} --", y+1, i+1);
                            InsertImpresa(imp, can.nrNotifica);
                        }
                    }
                    #endregion

                    #region Recupero e carico gli indirizzi per ogni cantiere

                    // CHIAMATA AL SERVIZIO
                    List<IndirizzoCantTR4> indList = RecuperaIndirizzi(can.nrNotifica);
                    // CHIAMATA AL SERVIZIO

                    totIndirizzi = totIndirizzi + indList.Count;

                    if (indList.Count > 0)
                    {
                        foreach (IndirizzoCantTR4 ind in indList)
                        {
                            //Console.WriteLine("-- SALVO SU DB L'INDIRIZZO {0} DEL CANTIERE {1} --", y + 1, i + 1);
                            InsertIndirizzo(ind, can.nrNotifica);
                        }
                    }
                    #endregion

                    #region Recupero e carico le persone per ogni cantiere

                    // CHIAMATA AL SERVIZIO
                    List<PersonaCantTR4> perList = RecuperaPersone(can.nrNotifica);
                    // CHIAMATA AL SERVIZIO

                    totPersone = totPersone + perList.Count;

                    if (perList.Count > 0)
                    {
                        foreach (PersonaCantTR4 per in perList)
                        {
                            //Console.WriteLine("-- SALVO SU DB LA PERSONA {0} DEL CANTIERE {1} --", y + 1, i + 1);
                            InsertPersona(per, can.nrNotifica);
                        }
                    }
                    #endregion

                    Console.WriteLine("-- INSERITA NOTIFICA {3} CON {0} IMPRESE, {1} INDIRIZZI, {2} PERSONE --", totImprese, totIndirizzi, totPersone, can.nrNotifica);
                    Console.WriteLine(" ");
                }
            }
            Console.WriteLine("-- INSERITI {0} CANTIERI --", totCantieri);
            Console.WriteLine(" ");
        }

        public void UpdateFromEdilconnect()
        {
            List<NotificaPreliminareEdilconnectSelectUpdate> notUpdate = new List<NotificaPreliminareEdilconnectSelectUpdate>();
            try
            {
                using (var context = new SiceContext())
                {
                    notUpdate = context.NotifichePreliminariEdilconnectSelectUpdate();
                    /*queryListUpdate = (from notEdil in context.NotificaPreliminareEdilconnect
                                        join
                                        notCant in context.NotificaPreliminareCantiere on notEdil.NumeroNotifica equals notCant.NumeroNotifica
                                        where DbFunctions.Left(DbFunctions.Right(notEdil.NumeroNotifica, 5), 1) == "/" &&
                                        notEdil.DataAggiornamentoNotifica != notCant.DataAggiornamentoNotifica &&
                                        notEdil.DataAggiornamentoNotifica < DbFunctions.AddDays(DbFunctions.TruncateTime(DateTime.Now), -1)
                                        //orderby DbFunctions.Right(notEdil.NumeroNotifica, 4).Cast<int>() descending, notEdil.DataAggiornamentoNotifica descending, DbFunctions.Left(notEdil.NumeroNotifica, (notEdil.NumeroNotifica.Length - 5)).Cast<int>() descending
                                        select notEdil.NumeroNotifica).Distinct().ToList();
                                        */
                }
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, "elenco notifiche da aggiornare");
            }
            if (notUpdate.Count > 0)
            {
                Console.WriteLine($"-- NOTIFICHE RECUPERATE PER L'UPDATE: {notUpdate.Count}");
                Console.WriteLine("");
                foreach (NotificaPreliminareEdilconnectSelectUpdate not in notUpdate)
                {
                    UpdateNotifica(not.numeroNotifica, (DateTime)not.dataComunicazioneNotifica, (DateTime)not.dataAggiornamentoNotifica, "edilconnect");
                }
            }
            
        }
        
        public void UpdateIncomplete()
        {
            List<NotificaPreliminareIncompleta> notIncomplete = new List<NotificaPreliminareIncompleta>();
            try
            {
                using (var context = new SiceContext())
                {
                    notIncomplete = context.NotificaPreliminareIncompleta();
                    /*queryListUpdate = (from notEdil in context.NotificaPreliminareEdilconnect
                                        join
                                        notCant in context.NotificaPreliminareCantiere on notEdil.NumeroNotifica equals notCant.NumeroNotifica
                                        where DbFunctions.Left(DbFunctions.Right(notEdil.NumeroNotifica, 5), 1) == "/" &&
                                        notEdil.DataAggiornamentoNotifica != notCant.DataAggiornamentoNotifica &&
                                        notEdil.DataAggiornamentoNotifica < DbFunctions.AddDays(DbFunctions.TruncateTime(DateTime.Now), -1)
                                        //orderby DbFunctions.Right(notEdil.NumeroNotifica, 4).Cast<int>() descending, notEdil.DataAggiornamentoNotifica descending, DbFunctions.Left(notEdil.NumeroNotifica, (notEdil.NumeroNotifica.Length - 5)).Cast<int>() descending
                                        select notEdil.NumeroNotifica).Distinct().ToList();
                                        */
                }
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, "elenco notifiche incomplete da aggiornare");
            }
            if (notIncomplete.Count > 0)
            {
                Console.WriteLine($"-- NOTIFICHE RECUPERATE PER L'UPDATE: {notIncomplete.Count}");
                Console.WriteLine("");
                foreach (NotificaPreliminareIncompleta not in notIncomplete)
                {
                    UpdateNotifica(not.numeroNotifica, (DateTime)not.dataComunicazioneNotifica, (DateTime)not.dataAggiornamentoNotifica, "incomplete");
                }
            }

        }
        


        #region Chiamate base al servizio

        public List<CantiereTR4> RecuperaCantieri(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            CantiereTR4[] cantiereTR4;
            List<CantiereTR4> canList = new List<CantiereTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();
            try
            {
                client.ricercaCantieriTR4(st, ref intest,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            numeroNotifica,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            string.Empty,
                                            string.Empty,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            string.Empty,
                                            null,
                                            null,
                                            new String[0],
                                            out cantiereTR4);
                canList = cantiereTR4.ToList();
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, "Collegamento WS, metodo Cantieri");
            }           

            return canList;
        }

        public List<ImpresaCantTR4> RecuperaImprese(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            ImpresaCantTR4[] imprese;
            List<ImpresaCantTR4> impList = new List<ImpresaCantTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();
            try
            {
                client.ricercaImpreseTR4(st, ref intest, numeroNotifica, string.Empty, out imprese);
                impList = imprese.ToList();
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, "Collegamento WS, metodo Imprese");
            }            

            return impList;
        }

        public List<IndirizzoCantTR4> RecuperaIndirizzi(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            IndirizzoCantTR4[] indirizzi;
            List<IndirizzoCantTR4> indList = new List<IndirizzoCantTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();

            //Console.WriteLine("-- RECUPERO GLI INDIRIZZI PER IL CANTIERE {0}--", i + 1);
            try
            { 
                client.ricercaIndirizziTR4(st, ref intest, numeroNotifica, string.Empty, out indirizzi);
                indList = indirizzi.ToList();
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, "Collegamento WS, metodo Indirizzi");
            }

            return indList;
        }

        public List<PersonaCantTR4> RecuperaPersone(string numeroNotifica)
        {
            SecurityType st = CreateSecurityType();
            Intestazione intest = CreateIntestazione();
            PersonaCantTR4[] persone;
            List<PersonaCantTR4> perList = new List<PersonaCantTR4>();

            CantieriServicePortClient client = new CantieriServicePortClient();
            try
            {
                client.ricercaPersoneTR4(st, ref intest, numeroNotifica, out persone);
                perList = persone.ToList();
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, "Collegamento WS, metodo Persone");
            }

            return perList;
        }

        #endregion
        

        #region Inserimenti Base

        public void InsertCantiere(CantiereTR4 cantiere)
        {
            DateTime dataNotifica = DateTime.ParseExact(cantiere.dtComunicazione, "dd/MM/yyyy", null);
            DateTime? dtComunicazione = new DateTime();
            //object dtComunicazione = null;
            if (dataNotifica.Year < 1900 || dataNotifica.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
            {
                dtComunicazione = null;
            }
            else
            {
                dtComunicazione = Convert.ToDateTime(cantiere.dtComunicazione);
            }

            DateTime? dtAggNotifica = new DateTime();
            if ((cantiere.dtAggNotifica != null) && (Convert.ToString(cantiere.dtAggNotifica) != ""))
            {
                DateTime dataAggiornamento = DateTime.ParseExact(cantiere.dtAggNotifica,"dd/MM/yyyy",null);
                
                if (dataAggiornamento.Year < 1900 || dataAggiornamento.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                {
                    dtAggNotifica = null;
                }
                else
                {
                    dtAggNotifica = dataAggiornamento;
                }
            }
            else
            {
                dtAggNotifica = null;
            }
            
            try
            {
                using (var context = new SiceContext())
                {
                    context.NotifichePreliminariCantiereInsert(cantiere.nrNotifica, cantiere.nrProtocollo, dtComunicazione, dtAggNotifica, cantiere.nrLavAutonomi, cantiere.nrImprese, cantiere.cdContrattoAppalto, cantiere.cdContrattoCIG, cantiere.cdContrattoCUI, cantiere.cdTipoOpera, cantiere.dsTipoOpera, cantiere.dsTipoCategoria, cantiere.dsTipoTipologia, cantiere.dsAltraCategoria, cantiere.dsAltraTipologia, cantiere.ammontareComplessivo, Convert.ToBoolean(cantiere.flArt90C11), Convert.ToBoolean(cantiere.flArt99C1B), Convert.ToBoolean(cantiere.flArt99C1C), cantiere.idagp);
                }
            }
            catch (SqlException exc)
            {
                if (exc.Number == 2601)
                {
                    TracciaErrore(exc.Message, string.Format("SQL per l'inserimento della notifica numero {0}", cantiere.nrNotifica));
                    //var.Log(string.Format("Eccezione sql per l'inserimento della notifica numero {0}:\n {1}", cantiere.nrNotifica, exc.Message));
                }

            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, string.Format("inserimento notifica {0}", cantiere.nrNotifica));
            }
        }

        public void InsertImpresa(ImpresaCantTR4 impresa, string numeroNotifica)
        {
            try
            {
                using (var context = new SiceContext())
                {
                    context.NotifichePreliminariImpresaInsert(numeroNotifica, impresa.ragioneSociale, impresa.codFiscaleId, impresa.PivaImpresaAssoc, impresa.indirizzoSede, impresa.dsComune, impresa.dsSiglaProvincia, impresa.nazione, impresa.zipCode, impresa.cdTipoIncaricoAppalto, impresa.dsTipoIncaricoAppalto);
                }
            }
            catch (SqlException exc)
            {
                if (exc.Number == 2601)
                {
                    TracciaErrore(exc.Message, string.Format("SQL per l'inserimento dell'impresa della notifica numero {0}", numeroNotifica));
                    //var.Log(string.Format("Eccezione sql per l'inserimento della notifica numero {0}:\n {1}", cantiere.nrNotifica, exc.Message));
                }

            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, string.Format("inserimento impresa della notifica {0}", numeroNotifica));
            }
            
        }

        public void InsertIndirizzo(IndirizzoCantTR4 indirizzo, string numeroNotifica)
        {
            try
            {
                DateTime dataInizio = DateTime.ParseExact(indirizzo.dtInizioLavori,"dd/MM/yyyy",null);
                DateTime? dtInizioLavori = new DateTime();
                if (dataInizio.Year < 1900 || dataInizio.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                {
                    dtInizioLavori = null;
                }
                else
                {
                    dtInizioLavori = dataInizio;
                }

                using (var context = new SiceContext())
                {
                    context.NotifichePreliminariIndirizzoInsert(numeroNotifica, indirizzo.indirizzo, indirizzo.dsComune, indirizzo.CAPindirCantiere, indirizzo.dsProvincia, dtInizioLavori, indirizzo.durataLavori, indirizzo.dsDurataLavori, indirizzo.cdArea, indirizzo.dsArea, indirizzo.areaNote, indirizzo.nrMassimoLavoratori, indirizzo.idagp.ToString());
                }
            }
            catch (SqlException exc)
            {
                if (exc.Number == 2601)
                {
                    TracciaErrore(exc.Message, string.Format("SQL per l'inserimento dell'indirizzo della notifica numero {0}", numeroNotifica));
                    //var.Log(string.Format("Eccezione sql per l'inserimento della notifica numero {0}:\n {1}", cantiere.nrNotifica, exc.Message));
                }
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, string.Format("inserimento indirizzo per la notifica {0}", numeroNotifica));
            }
        }

        public void InsertPersona(PersonaCantTR4 persona, string numeroNotifica)
        {
            try
            {
                DateTime? dtNascita = new DateTime();

                if (String.IsNullOrWhiteSpace(persona.dtNascita))
                {
                    dtNascita = null;
                }
                else
                { 
                    DateTime dataNascita = DateTime.ParseExact(persona.dtNascita,"dd/MM/yyyy",null);
                
                    if (dataNascita.Year < 1900 || dataNascita.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                    {
                        dtNascita = null;
                    }
                    else
                    {
                        dtNascita = dataNascita;
                    }
                }

                using (var context = new SiceContext())
                {
                    context.NotifichePreliminariPersonaInsert(numeroNotifica, persona.nome, persona.cognome, persona.codFiscaleDocumento, dtNascita, persona.regione, persona.nazione, persona.indirizzo, persona.dsComune, persona.dsProvincia, persona.zipCode, persona.ruolo);
                }
            }
            catch (SqlException exc)
            {
                if (exc.Number == 2601)
                {
                    TracciaErrore(exc.Message, string.Format("SQL per l'inserimento persona della notifica numero {0}", numeroNotifica));
                    //var.Log(string.Format("Eccezione sql per l'inserimento della notifica numero {0}:\n {1}", cantiere.nrNotifica, exc.Message));
                }
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, string.Format("inserimento persona per la notifica {0}", numeroNotifica));
            }
        }

        public void UpdateCantiere(CantiereTR4 cantiere)
        {
            DateTime dataNotifica = DateTime.ParseExact(cantiere.dtComunicazione, "dd/MM/yyyy", null);
            DateTime? dtComunicazione = new DateTime();
            //object dtComunicazione = null;
            if (dataNotifica.Year < 1900 || dataNotifica.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
            {
                dtComunicazione = null;
            }
            else
            {
                dtComunicazione = Convert.ToDateTime(cantiere.dtComunicazione);
            }

            DateTime? dtAggNotifica = new DateTime();
            if ((cantiere.dtAggNotifica != null) && (Convert.ToString(cantiere.dtAggNotifica) != ""))
            {
                DateTime dataAggiornamento = DateTime.ParseExact(cantiere.dtAggNotifica, "dd/MM/yyyy", null);

                if (dataAggiornamento.Year < 1900 || dataAggiornamento.Year > 2070) //1/1/1753   per smalldatetime 1/1/1900
                {
                    dtAggNotifica = null;
                }
                else
                {
                    dtAggNotifica = dataAggiornamento;
                }
            }
            else
            {
                dtAggNotifica = null;
            }

            try
            {
                using (var context = new SiceContext())
                {
                    context.NotifichePreliminariCantiereUpdate(cantiere.nrNotifica, cantiere.nrProtocollo, dtComunicazione, dtAggNotifica, cantiere.nrLavAutonomi, cantiere.nrImprese, cantiere.cdContrattoAppalto, cantiere.cdContrattoCIG, cantiere.cdContrattoCUI, cantiere.cdTipoOpera, cantiere.dsTipoOpera, cantiere.dsTipoCategoria, cantiere.dsTipoTipologia, cantiere.dsAltraCategoria, cantiere.dsAltraTipologia, cantiere.ammontareComplessivo, Convert.ToBoolean(cantiere.flArt90C11), Convert.ToBoolean(cantiere.flArt99C1B), Convert.ToBoolean(cantiere.flArt99C1C), cantiere.idagp);
                }
            }
            catch (SqlException exc)
            {
                if (exc.Number == 2601)
                {
                    TracciaErrore(exc.Message, string.Format($"SQL per l'update della notifica numero {cantiere.nrNotifica}"));
                }
            }
            catch (Exception ex)
            {
                TracciaErrore(ex.Message, string.Format($"update notifica {cantiere.nrNotifica}"));
            }            
        }

        public void UpdateNotifica(string nrNotifica, DateTime? dataComunicazioneNotifica, DateTime? dataAggiornamentoNotifica, string modalita)
        {
            List<CantiereTR4> listaCantieri = RecuperaCantieri(nrNotifica);
            List<ImpresaCantTR4> listaImprese = RecuperaImprese(nrNotifica);
            List<IndirizzoCantTR4> listaIndirizzi = RecuperaIndirizzi(nrNotifica);
            List<PersonaCantTR4> listaPersone = RecuperaPersone(nrNotifica);

            if (listaCantieri.Count > 0 && listaImprese.Count > 0 && listaIndirizzi.Count > 0 && listaPersone.Count > 0)
            {
                foreach (CantiereTR4 can in listaCantieri)
                {
                    int compareAgg = 0;

                    if (modalita == "edilconnect")
                    { 
                        DateTime dtAggChk = new DateTime();
                        using (var context = new SiceContext())
                        {
                            var queryCant = (from notCant in context.NotificaPreliminareCantiere
                                             where notCant.NumeroNotifica == can.nrNotifica
                                             select notCant.DataAggiornamentoNotifica).SingleOrDefault();

                            dtAggChk = (DateTime)queryCant;
                        }                    

                        if (!String.IsNullOrEmpty(can.dtAggNotifica))
                            compareAgg = DateTime.Compare(dtAggChk, DateTime.Parse(can.dtAggNotifica));
                    }
                    if (modalita == "incomplete")
                    {
                        compareAgg = 1;
                    }

                    if (compareAgg != 0)
                    {
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                        if (modalita == "edilconnect")
                            Console.WriteLine($"-- UPDATE NOTIFICA {can.nrNotifica} DEL {((DateTime)dataComunicazioneNotifica).ToString("dd/MM/yyyy")}, AGGIORNAMENTO DEL {((DateTime)dataAggiornamentoNotifica).ToString("dd/MM/yyyy")} --");
                        else
                            Console.WriteLine($"-- UPDATE NOTIFICA {can.nrNotifica} DEL {((DateTime)dataComunicazioneNotifica).ToString("dd/MM/yyyy")} --");

                        UpdateCantiere(can);

                        using (var context = new SiceContext())
                        {
                            context.NotifichePreliminariDeleteByNumeroNotifica(can.nrNotifica);
                        }

                        //InsertCantiere(can);

                        foreach (ImpresaCantTR4 imp in listaImprese)
                        {
                            InsertImpresa(imp, can.nrNotifica);
                        }

                        foreach (IndirizzoCantTR4 ind in listaIndirizzi)
                        {
                            InsertIndirizzo(ind, can.nrNotifica);
                        }

                        foreach (PersonaCantTR4 per in listaPersone)
                        {
                            InsertPersona(per, can.nrNotifica);
                        }

                        Console.WriteLine($"-- AGGIORNATI {listaImprese.Count} IMPRESE, {listaIndirizzi.Count} INDIRIZZI, {listaPersone.Count} PERSONE --");
                        Console.WriteLine("");
                    }
                    else
                    {
                        if (modalita == "edilconnect")
                            Console.WriteLine($"-- NESSUN UPDATE DEL {((DateTime)dataAggiornamentoNotifica).ToString("dd/MM/yyyy")} DALLA REGIONE PER NOTIFICA {can.nrNotifica} --");
                        else
                            Console.WriteLine($"-- NESSUN UPDATE PER NOTIFICA {can.nrNotifica} --");
                        Console.WriteLine("");
                    }
                }
            }
            else
            {
                Console.WriteLine($"-- NOTIFICA {nrNotifica} INCOMPLETA DALLA REGIONE --");
            }
            listaCantieri.Clear();
            listaImprese.Clear();
            listaIndirizzi.Clear();
            listaPersone.Clear();
        }

        #endregion


        public void TracciaErrore(string messaggioErrore, string sottoCategoria)
        {
            string logNotifiche = ConfigurationManager.AppSettings["LogNotifiche"];

            //Console.WriteLine(messaggioErrore);
            if (string.IsNullOrEmpty(sottoCategoria))
            {
                Console.WriteLine($"Eccezione: {messaggioErrore}");
            }
            else
            {
                Console.WriteLine($"Eccezione {sottoCategoria}: {messaggioErrore}");
            }
            Console.WriteLine("");

            if (logNotifiche == "true")
            {
                string source = "Notifiche Preliminari";
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, "Application");

                if (string.IsNullOrEmpty(sottoCategoria))
                {
                    EventLog.WriteEntry(source, $"Eccezione: {messaggioErrore}");
                }
                else
                {
                    EventLog.WriteEntry(source, $"Eccezione {sottoCategoria}: {messaggioErrore}");
                }                   
            }
        }
    }
}
