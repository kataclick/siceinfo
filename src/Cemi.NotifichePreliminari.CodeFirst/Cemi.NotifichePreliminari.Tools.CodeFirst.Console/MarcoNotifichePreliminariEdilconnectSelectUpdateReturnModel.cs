// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace Cemi.NotifichePreliminari.Tools.CodeFirst.Console
{

    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.22.1.0")]
    public partial class MarcoNotifichePreliminariEdilconnectSelectUpdateReturnModel
    {
        public System.String numeroNotifica { get; set; }
        public System.DateTime? dataComunicazioneNotifica { get; set; }
        public System.DateTime? dataAggiornamentoNotifica { get; set; }
    }

}
// </auto-generated>
