// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.61
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using Cemi.NotifichePreliminari.Type.Domain;

namespace Cemi.NotifichePreliminari.Data.Mapping
{

    // NotifichePreliminiariEdilconnect
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.22.1.0")]
    public partial class NotifichePreliminiariEdilconnectMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<NotificaPreliminareEdilconnect>
    {
        public NotifichePreliminiariEdilconnectMap()
            : this("dbo")
        {
        }

        public NotifichePreliminiariEdilconnectMap(string schema)
        {
            ToTable("NotifichePreliminariEdilconnect", schema);
            HasKey(x => x.IdNotificaPreliminareEdilconnect);

            Property(x => x.IdNotificaPreliminareEdilconnect).HasColumnName(@"idNotificaPreliminareEdilconnect").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.NumeroNotifica).HasColumnName(@"numeroNotifica").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.DataComunicazioneNotifica).HasColumnName(@"dataComunicazioneNotifica").IsOptional().HasColumnType("smalldatetime");
            Property(x => x.DataAggiornamentoNotifica).HasColumnName(@"dataAggiornamentoNotifica").IsOptional().HasColumnType("smalldatetime");
            Property(x => x.DataInserimentoRecord).HasColumnName(@"dataInserimentoRecord").IsOptional().HasColumnType("smalldatetime");
            Property(x => x.DataAggiornamentoRecord).HasColumnName(@"dataAggiornamentoRecord").IsOptional().HasColumnType("smalldatetime");
            Property(x => x.DataUltimoControllo).HasColumnName(@"dataUltimoControllo").IsOptional().HasColumnType("smalldatetime");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
