﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cemi.NotifichePreliminari.Type.Domain
{
    public partial class NotificaPreliminareEdilconnectGiorni
    {
        public System.DateTime? DataComunicazioneNotifica { get; set; }
    }
}
