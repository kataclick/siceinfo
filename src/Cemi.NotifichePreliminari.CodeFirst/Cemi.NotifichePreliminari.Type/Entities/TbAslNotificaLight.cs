﻿namespace Cemi.NotifichePreliminari.Type.Entities
{
    public partial class TbAslNotificaLight
    {

        public string DbNumeroNotifica { get; set; } // dbNUMERO_NOTIFICA (length: 50)
        public System.DateTime DbDataInserimento { get; set; } // dbDATA_INSERIMENTO
        public System.DateTime DbDataUltimaModifica { get; set; } // dbDATA_ULTIMA_MODIFICA
        public System.DateTime DbLastCheckedDate { get; set; } // dbLAST_CHECKED_DATE


        public TbAslNotificaLight()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }
}
