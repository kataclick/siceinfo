﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Cemi.AnagraficaUnicaRicerca
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonImpresa_Click(object sender, EventArgs e)
        {
            labelErrore.Text = string.Empty;

            try
            {
                AnagraficaUnicaService.AnagraficaUnicaClient client = new AnagraficaUnicaService.AnagraficaUnicaClient();
                DataSet dsImprese = client.RicercaImprese(textBoxImpresa.Text);
                dataGridViewImprese.DataSource = dsImprese.Tables["Imprese"];
            }
            catch (Exception ex)
            {
                labelErrore.Text = ex.Message;
            }
        }

        private void buttonLavoratore_Click(object sender, EventArgs e)
        {
            labelErrore.Text = string.Empty;

            try
            {
                AnagraficaUnicaService.AnagraficaUnicaClient client = new AnagraficaUnicaService.AnagraficaUnicaClient();
                DataSet dsLavoratori = client.RicercaLavoratori(textBoxLavoratore.Text);
                dataGridViewLavoratori.DataSource = dsLavoratori.Tables["Lavoratori"];
            }
            catch (Exception ex)
            {
                labelErrore.Text = ex.Message;
            }
        }
    }
}
