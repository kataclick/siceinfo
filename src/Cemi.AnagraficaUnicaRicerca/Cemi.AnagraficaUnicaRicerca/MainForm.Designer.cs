﻿namespace Cemi.AnagraficaUnicaRicerca
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxImpresa = new System.Windows.Forms.TextBox();
            this.textBoxLavoratore = new System.Windows.Forms.TextBox();
            this.buttonImpresa = new System.Windows.Forms.Button();
            this.buttonLavoratore = new System.Windows.Forms.Button();
            this.dataGridViewImprese = new System.Windows.Forms.DataGridView();
            this.dataGridViewLavoratori = new System.Windows.Forms.DataGridView();
            this.labelErrore = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewImprese)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLavoratori)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewImprese);
            this.groupBox1.Controls.Add(this.buttonImpresa);
            this.groupBox1.Controls.Add(this.textBoxImpresa);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(867, 247);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Imprese";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewLavoratori);
            this.groupBox2.Controls.Add(this.buttonLavoratore);
            this.groupBox2.Controls.Add(this.textBoxLavoratore);
            this.groupBox2.Location = new System.Drawing.Point(16, 270);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(867, 247);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lavoratori";
            // 
            // textBoxImpresa
            // 
            this.textBoxImpresa.Location = new System.Drawing.Point(16, 35);
            this.textBoxImpresa.MaxLength = 16;
            this.textBoxImpresa.Name = "textBoxImpresa";
            this.textBoxImpresa.Size = new System.Drawing.Size(179, 23);
            this.textBoxImpresa.TabIndex = 0;
            // 
            // textBoxLavoratore
            // 
            this.textBoxLavoratore.Location = new System.Drawing.Point(16, 35);
            this.textBoxLavoratore.MaxLength = 16;
            this.textBoxLavoratore.Name = "textBoxLavoratore";
            this.textBoxLavoratore.Size = new System.Drawing.Size(179, 23);
            this.textBoxLavoratore.TabIndex = 1;
            // 
            // buttonImpresa
            // 
            this.buttonImpresa.Location = new System.Drawing.Point(228, 35);
            this.buttonImpresa.Name = "buttonImpresa";
            this.buttonImpresa.Size = new System.Drawing.Size(75, 23);
            this.buttonImpresa.TabIndex = 1;
            this.buttonImpresa.Text = "Cerca";
            this.buttonImpresa.UseVisualStyleBackColor = true;
            this.buttonImpresa.Click += new System.EventHandler(this.buttonImpresa_Click);
            // 
            // buttonLavoratore
            // 
            this.buttonLavoratore.Location = new System.Drawing.Point(228, 35);
            this.buttonLavoratore.Name = "buttonLavoratore";
            this.buttonLavoratore.Size = new System.Drawing.Size(75, 23);
            this.buttonLavoratore.TabIndex = 2;
            this.buttonLavoratore.Text = "Cerca";
            this.buttonLavoratore.UseVisualStyleBackColor = true;
            this.buttonLavoratore.Click += new System.EventHandler(this.buttonLavoratore_Click);
            // 
            // dataGridViewImprese
            // 
            this.dataGridViewImprese.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewImprese.Location = new System.Drawing.Point(16, 73);
            this.dataGridViewImprese.Name = "dataGridViewImprese";
            this.dataGridViewImprese.Size = new System.Drawing.Size(832, 158);
            this.dataGridViewImprese.TabIndex = 2;
            // 
            // dataGridViewLavoratori
            // 
            this.dataGridViewLavoratori.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLavoratori.Location = new System.Drawing.Point(16, 73);
            this.dataGridViewLavoratori.Name = "dataGridViewLavoratori";
            this.dataGridViewLavoratori.Size = new System.Drawing.Size(832, 158);
            this.dataGridViewLavoratori.TabIndex = 3;
            // 
            // labelErrore
            // 
            this.labelErrore.AutoSize = true;
            this.labelErrore.ForeColor = System.Drawing.Color.Red;
            this.labelErrore.Location = new System.Drawing.Point(13, 529);
            this.labelErrore.Name = "labelErrore";
            this.labelErrore.Size = new System.Drawing.Size(0, 17);
            this.labelErrore.TabIndex = 2;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Items.AddRange(new object[] {
            "00868420159",
            "04590830156",
            "",
            "FRHGTJ84D29Z100Z",
            "0688895096",
            ""});
            this.listBox1.Location = new System.Drawing.Point(890, 25);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(141, 228);
            this.listBox1.TabIndex = 3;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 16;
            this.listBox2.Items.AddRange(new object[] {
            "RSSMRA51R31B711Y",
            "VRDFNC48T25C894Q",
            "",
            "VNRGRG68L22L319V",
            "LMNFVP58C20E801O"});
            this.listBox2.Location = new System.Drawing.Point(890, 280);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(141, 228);
            this.listBox2.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 555);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.labelErrore);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewImprese)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLavoratori)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridViewImprese;
        private System.Windows.Forms.Button buttonImpresa;
        private System.Windows.Forms.TextBox textBoxImpresa;
        private System.Windows.Forms.DataGridView dataGridViewLavoratori;
        private System.Windows.Forms.Button buttonLavoratore;
        private System.Windows.Forms.TextBox textBoxLavoratore;
        private System.Windows.Forms.Label labelErrore;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
    }
}