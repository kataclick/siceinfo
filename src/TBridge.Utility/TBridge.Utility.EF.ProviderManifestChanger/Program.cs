﻿using System.Xml;

namespace TBridge.Utility.EF.ProviderManifestChanger
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string modelName = args[0];
            string filePath = string.Format("./{0}.ssdl", modelName);
            string targetVersion = args[1];

            var edmx = new XmlDocument();
            edmx.Load(filePath);
            var nsm = new XmlNamespaceManager(edmx.NameTable);
            nsm.AddNamespace("a", "http://schemas.microsoft.com/ado/2009/02/edm/ssdl");
            var x = edmx.SelectSingleNode("/a:Schema", nsm);
            x.Attributes["ProviderManifestToken"].Value = targetVersion;
            edmx.Save(filePath);
        }
    }
}