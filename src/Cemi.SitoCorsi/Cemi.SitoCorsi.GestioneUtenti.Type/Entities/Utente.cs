﻿using System;
using System.Web.Security;

namespace Cemi.SitoCorsi.GestioneUtenti.Type.Entities
{
    public class Utente : MembershipUser
    {
        private int idUtente;

        public Utente(int idUtente, string username, string nome, string cognome)
            : base(
                "SQLServerMembershipProvider", username, null, null, null, null, true, false, DateTime.Now, DateTime.Now,
                DateTime.Now, DateTime.Now, DateTime.Now)
        {
            IdUtente = idUtente;
            Username = username;
            Nome = nome;
            Cognome = cognome;
        }

        public int IdUtente
        { 
            get { return idUtente; }
            set { idUtente = value; }
        }
        public string Username { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
    }
}
