﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Default_800.aspx.cs" Inherits="Cemi.SitoCorsi.Default_800" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
            <table border="0">
<tr>
<td>
<div style="margin-Left:5px; margin-top:15px; height:40px;" align="left">
                <asp:HyperLink ID="lnk_login" NavigateUrl="~/Account/Login.aspx" runat="server">Accesso Amministratore</asp:HyperLink>
                <asp:LoginName ID="LoginName" runat="server" FormatString="Benvenuto {0}" /><br />
                <asp:LoginStatus ID="LoginStatus" runat="server" LogoutPageUrl="~/Default_800.aspx" LogoutText="Disconnetti" onloggedout="LoginStatus_LoggedOut" />
            </div>
</td>
<td>
<div style="margin-right:5px; margin-top:15px; height:40px; padding-top:0em;" align="right">
                <asp:TextBox ID="txt_search" runat="server" Height="16px" AutoCompleteType="Search" BorderColor="#84C6AC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="X-Small" Width="200px" ValidationGroup="Search"></asp:TextBox>
                <asp:Button ID="btn_search" runat="server" Text="Cerca" Height="20px" BackColor="#84C6AC" Font-Names="Calibri" BorderColor="#84C6AC" BorderStyle="Solid" BorderWidth="1px" Font-Size="Small" Font-Bold="true" Font-Strikeout="False" ForeColor="White" onclick="btn_search_Click" ValidationGroup="Search" />
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Font-Names="Calibri" ErrorMessage="Inserire un valore per effettuare la ricerca" ControlToValidate="txt_search" ForeColor="#FF3300" ValidationGroup="Search"></asp:RequiredFieldValidator>
            </div>
</td>
</tr>
</table>
    <div style="margin:20px auto 20px auto; text-align:justify; font-family:Calibri,Verdana; font-size:12pt; line-height:1.2em;">
        <strong style="font-size:14pt; color:Navy;line-height:1.5em;">L'offerta formativa del sistema paritetico: il valore aggiunto per imprese e lavoratori edili</strong><br />
        <strong>ESEM - Ente Scuola Edile Milanese e CPT - Sicurezza in Edilizia</strong> sono da sempre impegnati in un lavoro sinergico per garantire, in maniera integrata, 
        alle imprese e ai lavoratori iscritti in <strong>Cassa Edile</strong> di Milano, Lodi, Monza e Brianza, maggiori e soprattutto più efficienti servizi in materia di 
        formazione ed addestramento.<br />
        Da questo sodalizio nasce il catalogo on line <strong>dell'offerta formativa del sistema paritetico</strong> che contiene i corsi di formazione a disposizione 
        delle imprese e delle maestranze, per il consolidamento e la crescita delle competenze professionali dei lavoratori in quanto elementi fondamentali per lo sviluppo di 
        valori imprescindibili quali sicurezza e regolarità.
        <br /><br />
        Le attività formative proposte, sono suddivise in <strong>FORMAZIONE OBBLIGATORIA</strong> per contratto o per legge e <strong>CRESCITA PROFESSIONALE</strong> 
        consistente in programmi didattici dedicati a profili operai e tecnici edili, progettati per implementare la preparazione di base ed apprendere nuove metodologie di lavoro.
        <br /><br />
        La <strong>regolare iscrizione in Cassa Edile</strong> di Milano, Lodi, Monza e Brianza è un<strong> fattore altamente competitivo</strong> per imprese e lavoratori che si traduce in vantaggi concreti: 
        <strong>i corsi di formazione obbligatoria sono gratuiti e quelli di crescita professionale sono fortemente scontati</strong>.
        <br /><br />
        Il catalogo on line è in costante aggiornamento, in sintonia con l’evoluzione delle normative e con il mercato dell’edilizia. 
        <br /><br />
        Selezionando l’offerta formativa di interesse, si aprirà un link di collegamento per accedere alle informazioni relative ai corsi: modalità di iscrizione, date di inizio, durata e costi.
    </div>
    <div style="width:1038px;">
        <table style="width:100%;">
            <tr>
                <td valign="top" style="width:800px; margin-top:0px;">
                    <asp:Table runat="server" ID="Sommario" CellPadding="0" CellSpacing="0">

                        
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderSmall">
                                <asp:ImageButton id="btn_Legislativo" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="Images/800-2.gif" onclick="btn_Legislativo_Click" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" id="RowLegislativo">
                        <asp:TableCell Cssclass="bordo">
                            <asp:GridView ID="SommarioLegislativo" runat="server" AutoGenerateColumns="False" OnRowDataBound="SommarioLegislativo_RowDataBound" ShowHeader="false">
                                <AlternatingRowStyle BackColor="#bcdecd"/>
                                <Columns>
                                    <asp:TemplateField ShowHeader="false">
                                        <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td style="width:20%;"><asp:Label ID="categoria" runat="server" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:GridView ID="CorsiLegislativo" runat="server" AutoGenerateColumns="false" AllowPaging="False" onrowdatabound="CorsiLegislativo_RowDataBound" DataKeyNames="idCorso" ShowHeader="false" Cssclass="bordo">
                                                            <AlternatingRowStyle BackColor="#bcdecd"/>
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="LinkLegislativo" HeaderText="Formazione obbligatoria - Legislativa" runat="server" ></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView></td>
                                                    </tr>
                                                </table>
                                          </ItemTemplate>
                        
                                    </asp:TemplateField>
                                </Columns>


                            </asp:GridView>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell CssClass="invisible">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderSmall">
                                <asp:ImageButton id="btn_Operai" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/800-3.gif" onclick="btn_Operai_Click" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" id="RowOperai">
                        <asp:TableCell Cssclass="bordo">
                            <asp:GridView ID="SommarioCrescitaOperai" runat="server" AutoGenerateColumns="False" AllowPaging="False" OnRowDataBound="SommarioCrescitaOperai_RowDataBound" ShowHeader="false">
                                <AlternatingRowStyle BackColor="#a6dbf2"/>
                                <Columns>
                                    <asp:TemplateField>
                                    <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td style="width:20%;"><asp:Label ID="categoria" runat="server" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:GridView ID="CorsiCrescitaOperai" runat="server" AutoGenerateColumns="false" AllowPaging="False" onrowdatabound="CorsiCrescitaOperai_RowDataBound" DataKeyNames="idCorso" ShowHeader="False" Cssclass="bordo">
                                                            <AlternatingRowStyle BackColor="#a6dbf2" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="LinkCrescitaOperai" HeaderText="Crescita professionale Operai" runat="server" ></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView></td>
                                                    </tr>
                                                </table>
                                          </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell CssClass="invisible" >&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderSmall">
                                <asp:ImageButton id="btn_Tecnici" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/800-4.gif" onclick="btn_Tecnici_Click" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="RowTecnici">
                            <asp:TableCell Cssclass="bordo">
                                <asp:GridView ID="SommarioCrescitaTecnici" runat="server" AutoGenerateColumns="False" AllowPaging="False" OnRowDataBound="SommarioCrescitaTecnici_RowDataBound" ShowHeader="false">
                                    <AlternatingRowStyle BackColor="#9fc7bf"/>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td style="width:20%;"><asp:Label ID="categoria" runat="server" Font-Bold="true"></asp:Label></td>
                                                        <td><asp:GridView ID="CorsiCrescitaTecnici" runat="server" AutoGenerateColumns="false" AllowPaging="False" onrowdatabound="CorsiCrescitaTecnici_RowDataBound" DataKeyNames="idCorso" ShowHeader="false" Cssclass="bordo">
                                                            <AlternatingRowStyle BackColor="#9fc7bf"/>
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:HyperLink ID="LinkCrescitaTecnici" runat="server" ></asp:HyperLink>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView></td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
                <td valign="top" style="margin-top:2px;">
                    <asp:GridView ID="Top10" runat="server" OnRowDataBound="Top10_RowDataBound" AutoGenerateColumns="false" CssClass="bordo">
                    <RowStyle HorizontalAlign="Center" />
                    <HeaderStyle Font-Bold="true" BackColor="#c7cf99" />
                    <AlternatingRowStyle BackColor="#daefc7" />
                    <Columns>
                    <asp:TemplateField HeaderText="10 Parole più ricercate">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnk_Top10" runat="server" ></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
                                <% /*
                                <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderSmall" VerticalAlign="Middle">
                                <asp:ImageButton id="btn_Contrattuale" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/800-1.gif" onclick="btn_Contrattuale_Click"  />
                                <a name="1"></a>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" id="RowContrattuale">
                            <asp:TableCell Cssclass="bordo">
                            <asp:GridView ID="SommarioContrattuale" runat="server" AutoGenerateColumns="False" OnRowDataBound="SommarioContrattuale_RowDataBound" DataKeyNames="idCorso" CellPadding="0" ShowHeader="false">
                                <AlternatingRowStyle VerticalAlign="Top" BackColor="#c7cf99"/>
                                <Columns>
                                    <asp:BoundField DataField="idCorso" HeaderText="idCorso" Visible="False"/>
                                    <asp:BoundField DataField="TitoloCorso" HeaderText="Formazione obbligatoria - Contrattuale" Visible="False" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="LinkContrattuale" HeaderText="Formazione obbligatoria - Contrattuale" runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell CssClass="invisible">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                                    * */
                        %>
    </div>

    </asp:Content>


