﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DettagliCorso.aspx.cs" Inherits="Cemi.SitoCorsi.DettagliCorso" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>


<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="ReadOnly" visible="true">
        <p>&nbsp;</p>
        <table class="bordo" style="background-color:White; width:800px; text-align:left;">
            <tr>
                <td valign="top" align="center" colspan="2"><asp:Image ID="ImgHeader" runat="server" /></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelTitoloCorso" runat="server">Titolo Corso:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtTitoloCorso" runat="server"></asp:Label></td>
            </tr>
            <tr id="RowCategoria" runat="server" visible="false">
                <td class="dettaglio" valign="top"><asp:Label ID="LabelCategoria" runat="server">Categoria:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtCategoria" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelDestinatari" runat="server">Destinatari:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtDestinatari" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelRequisitiAccesso" runat="server">Requisiti Di Accesso:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtRequisitiAccesso" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelDurata" runat="server">Durata:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtDurata" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top" nowrap="nowrap"><asp:Label ID="LabelContenutiDiMassima" runat="server">Contenuti Di Massima:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtContenutiDiMassima" runat="server"></asp:Label></td>
            </tr>
            <tr id="RowNormativa" runat="server" visible="false">
                <td class="dettaglio" valign="top" nowrap="nowrap"><asp:Label ID="LabelRiferimentiLeggeNormativa" runat="server">Riferimenti Legge Normativa:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtRiferimentiLeggeNormativa" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelFrequenzaMinima" runat="server">Frequenza Minima:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtFrequenzaMinima" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelVerificaFinale" runat="server">Verifica Finale:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtVerificaFinale" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelAttestazioneRilasciata" runat="server">Attestazione Rilasciata:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtAttestazioneRilasciata" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top"><asp:Label ID="LabelCadenzaPeriodica" runat="server">Cadenza Periodica:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtCadenzaPeriodica" runat="server"></asp:Label></td>
            </tr>
            <tr id="RowValidita" runat="server" visible="false">
                <td class="dettaglio" valign="top"><asp:Label ID="LabelValiditaAttestato" runat="server">Validita' Attestato:&nbsp;</asp:Label></td>
                <td class="dettaglio" valign="top"><asp:Label ID="TxtValiditaAttestato" runat="server"></asp:Label></td>
            </tr>
            <tr id="RowLink" runat="server" visible="false">
                <td class="dettaglio" valign="top" colspan="2"><asp:Label ID="lbl_Link" runat="server">Per maggiori informazioni sull'iscrizione al corso cliccare&nbsp;</asp:Label><asp:HyperLink ID="lnk_link" runat="server" Font-Bold="true">QUI</asp:HyperLink><asp:Label ID="lbl_reserved" runat="server" Visible="false"> (accesso ad area riservata)</asp:Label></td>
            </tr>
        </table>
        <p runat="server" id="EditDiv">
                    <asp:HyperLink ID="HyperLinkEdit" runat="server" Font-Names="verdana" Font-Size="10pt">Modifica Dati</asp:HyperLink>
        </p>
        <p>
        <asp:HyperLink NavigateUrl="~/Default_800.aspx" runat="server" Font-Names="verdana" Font-Size="10pt">Torna Indietro</asp:HyperLink>
        </p>
    </div>
    <div runat="server" id="Modify" visible="true">
        <p>&nbsp;</p>
        <table class="bordo" style="background-color:White; width:800px; text-align:left;">
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">
                    <asp:Panel ID="Mod_pnl_Titolo" runat="server" Font-Bold="true" GroupingText="Titolo Corso" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                        <asp:TextBox ID="Mod_txt_Titolo" runat="server" Width="600" /><br /><asp:RequiredFieldValidator SetFocusOnError=true ID="ValidatorTitoloCorso" runat="server" ErrorMessage="Campo obbligatorio" ControlToValidate="Mod_txt_Titolo" ForeColor="Red" Font-Size="x-Small" ValidationGroup="UpdateCorso"></asp:RequiredFieldValidator>
                    </asp:Panel>
                </td>
            </tr>
            <tr id="Mod_lbl_Row_Categoria" runat="server" visible="false">
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr id="Mod_txt_Row_Categoria" runat="server" visible="false">
                <td class="dettaglio" valign="top">
                    <asp:Panel ID="Mod_pnl_Categoria" runat="server" Font-Bold="true" GroupingText="Categoria" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                        <asp:DropDownList ID="Mod_txt_Categoria" runat="server" Width="600">
                        </asp:DropDownList>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">
                    <asp:Panel ID="Mod_pnl_Destinatari" runat="server" Font-Bold="true" GroupingText="Destinatari" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                        <asp:TextBox ID="Mod_txt_Destinatari" runat="server" Width="600" /><br />
                        <asp:RequiredFieldValidator ID="ValidatorDestinatari" runat="server" ErrorMessage="Campo obbligatorio" ControlToValidate="Mod_txt_Destinatari" ValidationGroup="UpdateCorso" ForeColor="Red" Font-Size="X-Small"></asp:RequiredFieldValidator>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">
                    <asp:Panel ID="Mod_pnl_Requisiti" runat="server" Font-Bold="true" GroupingText="Requisiti" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                        <asp:TextBox ID="Mod_txt_Requisiti" runat="server" Width="600"></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Durata" runat="server" Font-Bold="true" GroupingText="Durata" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                    <asp:TextBox ID="Mod_txt_Durata" runat="server" Width="600px"></asp:TextBox>
                </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top" align="left">
                    <asp:Panel ID="Mod_pnl_Contenuti" runat="server" Font-Bold="true" GroupingText="Contenuti Di Massima" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
                        <cc1:Editor ID="Mod_txt_Contenuti" runat="server" ActiveMode="Design" ClientIDMode="AutoID" Height="200" Width="800" IgnoreTab="true" NoUnicode="true" SuppressTabInDesignMode="true">
                        </cc1:Editor>
                        <br />
                        <asp:RequiredFieldValidator ID="ValidatorContenuti" ControlToValidate="Mod_txt_Contenuti" runat="server" ErrorMessage="Campo obbligatorio" ValidationGroup="UpdateCorsi" ForeColor="Red" Font-Size="X-Small"></asp:RequiredFieldValidator>
                    </asp:Panel>
                </td>
            </tr>
            <tr id="Mod_lbl_Row_Riferimenti" runat="server" visible="false">
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr id="Mod_txt_Row_Riferimenti" runat="server" visible="false">
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Riferimenti" runat="server" Font-Bold="true" GroupingText="Riferimenti Legge Normativa" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                <asp:TextBox ID="Mod_txt_Riferimenti" runat="server" Width="600"></asp:TextBox>
                </asp:Panel></td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Frequenza" runat="server" Font-Bold="true" GroupingText="Frequenza Minima" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                <asp:TextBox ID="Mod_txt_Frequenza" runat="server" Width="600"></asp:TextBox>
                </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Verifica" runat="server" Font-Bold="true" GroupingText="Verifica Finale" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                <asp:TextBox ID="Mod_txt_Verifica" runat="server" Width="600"></asp:TextBox>
                </asp:Panel>
                </td>
                </tr>
                <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
                </tr>
                <tr>
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Cadenza" runat="server" Font-Bold="true" GroupingText="Cadenza Periodica" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                    <asp:TextBox ID="Mod_txt_Cadenza" runat="server" Width="600"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="ValidatorCadenza" ControlToValidate="Mod_txt_Cadenza" runat="server" ErrorMessage="Campo obbligatorio" ValidationGroup="UpdateCorsi" ForeColor="Red" Font-Size="X-Small"></asp:RequiredFieldValidator>
                </asp:Panel>
                </td>
                </tr>
                <tr>
                <td class="dettaglio" valign="top">&nbsp;</td>
                </tr>
                <tr>
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Attestazione" runat="server" Font-Bold="true" GroupingText="Attestazione Rilasciata" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                    <asp:TextBox ID="Mod_txt_Attestazione" runat="server" Width="600"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="ValidatorAttestazione" ControlToValidate="Mod_txt_Attestazione" runat="server" ErrorMessage="Campo obbligatorio" ValidationGroup="UpdateCorsi" ForeColor="Red" Font-Size="X-Small"></asp:RequiredFieldValidator>
                </asp:Panel>
                </td>
                </tr>
                
                <tr id="Mod_lbl_Row_Validita" runat="server" visible="false">
                <td class="dettaglio" valign="top">&nbsp;</td>
                </tr>
                <tr id="Mod_txt_Row_Validita" runat="server" visible="false">
                <td class="dettaglio" valign="top">
                <asp:Panel ID="Mod_pnl_Validita" runat="server" Font-Bold="true" GroupingText="Validita' Attestato" Font-Names="Verdana" Wrap="false" BackColor="Transparent" Font-Size="Small">
                <asp:TextBox ID="Mod_txt_Validita" runat="server" Width="600"></asp:TextBox>
                </asp:Panel>
                </td>
                </tr>
                </table>
                <p>
                    <asp:Button runat="server" ID="UpdateSubmit" ValidationGroup="UpdateCorso" Text="Salva Modifiche" BackColor="#84C6AC" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Calibri" Font-Bold="true" Font-Size="0.9em" ForeColor="White" onclick="UpdateSubmit_Click" />
                    <br />
                    <asp:label id="UpdateResult" runat="server" visible="false" ForeColor="Red"></asp:label>
                </p>
                <p>
                    <asp:HyperLink ID="HyperLink2" NavigateUrl="javascript:history.back();" runat="server" Font-Names="verdana" Font-Size="10pt">Torna Indietro</asp:HyperLink>
                </p>
                
            </div>
        </asp:Content>