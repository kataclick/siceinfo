﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Cemi.SitoCorsi.Type;
using Cemi.SitoCorsi.Type.Entities;
using Cemi.SitoCorsi.Data;

namespace Cemi.SitoCorsi
{
    public partial class Default_800 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                
                //RowContrattuale.Style.Add("display", "none");
                RowLegislativo.Style.Add("display", "none");
                RowOperai.Style.Add("display", "none");
                RowTecnici.Style.Add("display", "none");
            }

            if (Membership.GetUser() != null)
            {
                lnk_login.Style.Add("display", "none");
                LoginName.Style.Add("display", "true");
                LoginStatus.Style.Add("display", "true");                
            }
            else
            {
                lnk_login.Style.Add("display", "true");
                LoginName.Style.Add("display", "none");
                LoginStatus.Style.Add("display", "none");
            }
            

            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("MasterPage");
            body.Style.Add(HtmlTextWriterStyle.BackgroundImage, "Images/onda-0.gif");

            CorsiDataAccess cda = new CorsiDataAccess();

            Top10.DataSource = cda.Top10();
            Top10.DataBind();

/*            SommarioContrattuale.Columns[0].Visible = false;
            SommarioContrattuale.Columns[1].Visible = false;
            SommarioContrattuale.DataSource = cda.GetSommario(1);
            SommarioContrattuale.DataBind();
*/
            List<Categoria> cateLegislativo = new List<Categoria>();
            cateLegislativo = cda.GetSommarioCategoria(2);
            SommarioLegislativo.DataSource = cateLegislativo;
            SommarioLegislativo.DataBind();

            List<Categoria> cateCrescitaOperai = new List<Categoria>();
            cateCrescitaOperai = cda.GetSommarioCategoria(3);
            SommarioCrescitaOperai.DataSource = cateCrescitaOperai;
            SommarioCrescitaOperai.DataBind();

            List<Categoria> cateCrescitaTecnici = new List<Categoria>();
            cateCrescitaTecnici = cda.GetSommarioCategoria(4);
            SommarioCrescitaTecnici.DataSource = cateCrescitaTecnici;
            SommarioCrescitaTecnici.DataBind();
        }

        public DataSet GetSommarioDS(string gruppo)
        {
            string conStr = ConfigurationManager.ConnectionStrings["Corsi"].ConnectionString.ToString();
            DataSet ds = new DataSet();

            List<Corso> corsi = new List<Corso>();

            string query = "SELECT [idCorso],[TitoloCorso] FROM [CatalogoCorsi].[dbo].[Corsi] WHERE idGruppo = " + gruppo + " ORDER BY [TitoloCorso]";
            try
            {
                SqlConnection conn = new SqlConnection(conStr);
                SqlDataAdapter sda = new SqlDataAdapter(query, conn);
                sda.Fill(ds);
            }
            catch 
            {
                return ds;
            }
            return ds;
        }

        protected void Top10_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //string sVal = drv.Row["idCorso"].ToString();

                HyperLink lnk_Top10 = (HyperLink)e.Row.FindControl("lnk_Top10");
                lnk_Top10.Text = drv.Row["StringaRicerca"].ToString();
                //Link1.Text = drv.Row["idCorso"].ToString();
                lnk_Top10.NavigateUrl = "SearchResult.aspx?srcstr=" + drv.Row["StringaRicerca"].ToString();
            }
        }

        protected void LoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Response.Redirect("~/Default_800.aspx");
        }

        #region Contrattuale
        protected void SommarioContrattuale_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //string sVal = drv.Row["idCorso"].ToString();

                HyperLink LinkContrattuale = (HyperLink)e.Row.FindControl("LinkContrattuale");
                LinkContrattuale.Text = drv.Row["TitoloCorso"].ToString();
                //Link1.Text = drv.Row["idCorso"].ToString();
                LinkContrattuale.NavigateUrl = "DettagliCorso.aspx?idCorso=" + drv.Row["idCorso"].ToString()+"&Gruppo=1";
            }
        }
        #endregion

        #region Legislativo
        protected void SommarioLegislativo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Categoria categoria = (Categoria)e.Row.DataItem;
                Label lblCorsi = (Label)e.Row.FindControl("categoria");
                GridView gvCorsi = (GridView)e.Row.FindControl("CorsiLegislativo");

                lblCorsi.Text = categoria.Nome;
                lblCorsi.DataBind();

                gvCorsi.DataSource = categoria.Corsi;
                gvCorsi.DataBind();
            }
        }

        protected void CorsiLegislativo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CorsoBase corso = (CorsoBase)e.Row.DataItem;
                HyperLink hlCorso = (HyperLink)e.Row.FindControl("LinkLegislativo");
                //DataRowView drv = (DataRowView)e.Row.DataItem;

                hlCorso.Text = corso.TitoloCorso;
                hlCorso.NavigateUrl = "DettagliCorso.aspx?idCorso=" + corso.idCorso.ToString() + "&Gruppo=2";
            }
        }
        #endregion

        #region Crescita Operai
        protected void SommarioCrescitaOperai_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Categoria categoria = (Categoria)e.Row.DataItem;
                Label lblCorsi = (Label)e.Row.FindControl("categoria");
                GridView gvCorsi = (GridView)e.Row.FindControl("CorsiCrescitaOperai");

                lblCorsi.Text = categoria.Nome;
                lblCorsi.DataBind();

                gvCorsi.DataSource = categoria.Corsi;
                gvCorsi.DataBind();
            }
        }

        protected void CorsiCrescitaOperai_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CorsoBase corso = (CorsoBase)e.Row.DataItem;
                HyperLink hlCorso = (HyperLink)e.Row.FindControl("LinkCrescitaOperai");
                //DataRowView drv = (DataRowView)e.Row.DataItem;

                hlCorso.Text = corso.TitoloCorso;
                hlCorso.NavigateUrl = "DettagliCorso.aspx?idCorso=" + corso.idCorso.ToString() + "&Gruppo=3";
            }
        }
        #endregion

        #region Crescita tecnici
        protected void SommarioCrescitaTecnici_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Categoria categoria = (Categoria)e.Row.DataItem;
                Label lblCorsi = (Label)e.Row.FindControl("categoria");
                GridView gvCorsi = (GridView)e.Row.FindControl("CorsiCrescitaTecnici");

                lblCorsi.Text = categoria.Nome;
                lblCorsi.DataBind();
                
                gvCorsi.DataSource = categoria.Corsi;
                gvCorsi.DataBind();
            }
        }

        protected void CorsiCrescitaTecnici_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CorsoBase corso = (CorsoBase)e.Row.DataItem;
                HyperLink hlCorso = (HyperLink)e.Row.FindControl("LinkCrescitaTecnici");
                //DataRowView drv = (DataRowView)e.Row.DataItem;

                hlCorso.Text = corso.TitoloCorso;
                hlCorso.NavigateUrl = "DettagliCorso.aspx?idCorso=" + corso.idCorso.ToString() + "&Gruppo=4";
            }
        }
        #endregion

        protected void btn_search_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txt_search.Text))
            {
                //Page.ResolveUrl("SearchResult.aspx?srcstr=" + txt_search.Text);
                Response.Redirect("SearchResult.aspx?srcstr=" + txt_search.Text.ToString());
            }
        }

        
        #region Codice ShowHide Gruppi

/*        protected void btn_Contrattuale_Click(object sender, ImageClickEventArgs e)
        {
            if (RowContrattuale.Style["display"] == "none")
            {
                RowContrattuale.Style.Add("display", "inline");
            }
            else
            {
                RowContrattuale.Style.Add("display", "none");
            }
            RowContrattuale.Page.Focus();
        }
        */
        protected void btn_Legislativo_Click(object sender, ImageClickEventArgs e)
        {
            if (RowLegislativo.Style["display"] == "none")
            {
                RowLegislativo.Style.Add("display", "inline");
            }
            else
            {
                RowLegislativo.Style.Add("display", "none");
            }
            RowLegislativo.Focus();
        }

        protected void btn_Operai_Click(object sender, ImageClickEventArgs e)
        {
            if (RowOperai.Style["display"] == "none")
            {
                RowOperai.Style.Add("display", "inline");
            }
            else
            {
                RowOperai.Style.Add("display", "none");
            }
            RowOperai.Focus();
        }

        protected void btn_Tecnici_Click(object sender, ImageClickEventArgs e)
        {
            if (RowTecnici.Style["display"] == "none")
            {
                RowTecnici.Style.Add("display", "inline");
            }
            else
            {
                RowTecnici.Style.Add("display", "none");
            }
            RowTecnici.Focus();
        }
        #endregion
    }
}
