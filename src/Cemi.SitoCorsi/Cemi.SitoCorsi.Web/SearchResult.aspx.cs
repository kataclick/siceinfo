﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Cemi.SitoCorsi.Type;
using Cemi.SitoCorsi.Data;

namespace Cemi.SitoCorsi
{
    public partial class SearchResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("MasterPage");
            body.Style.Add(HtmlTextWriterStyle.BackgroundImage, "Images/onda-1.gif");

            CorsiDataAccess cda = new CorsiDataAccess();
            List<CorsoRicerca> results = new List<CorsoRicerca>();
            //List<CorsoRicerca> results = new List<CorsoRicerca>();
            if (!String.IsNullOrEmpty(Request.QueryString["srcstr"]))
            {
                results = cda.Search(Request.QueryString["srcstr"]);
            }
            else
            {
                lbl_search.Text = "Inserire un valore per effettuare la ricerca";
            }
            if (results.Count > 0)
            {
                lbl_search.Text = "Risultati della ricerca";
                TableRow trHead = new TableRow();
                tbl_results.Rows.Add(trHead);

                TableCell tdHead = new TableCell();
                tdHead.Text = "Titolo del corso";
                tdHead.Font.Bold = true;
                tdHead.ColumnSpan = 2;
                trHead.Cells.Add(tdHead);


                for (int i = 0; i < results.Count; i++)
                {
                    TableRow trEmpty = new TableRow();
                    tbl_results.Rows.Add(trEmpty);

                    TableCell tdEmpty = new TableCell();
                    tdEmpty.Text = "&nbsp;";
                    tdEmpty.CssClass = "risultati";
                    tdEmpty.ColumnSpan = 2;
                    trEmpty.Cells.Add(tdEmpty);

                    TableRow trCorso = new TableRow();
                    tbl_results.Rows.Add(trCorso);

                    TableCell tdPosizione = new TableCell();
                    tdPosizione.CssClass = "risultati";
                    tdPosizione.VerticalAlign = VerticalAlign.Top;
                    tdPosizione.Text = (i+1).ToString() + ". ";
                    tdPosizione.Width = Unit.Pixel(20);
                    TableCell tdCorso = new TableCell();

                    HyperLink h = new HyperLink();
                    h.Text = results[i].TitoloCorso;
                    h.NavigateUrl = "DettagliCorso.aspx?idCorso=" + results[i].idCorso.ToString() + "&Gruppo=" + results[i].Gruppo;
                    tdCorso.CssClass = "risultati";
                    tdCorso.VerticalAlign = VerticalAlign.Top;
                    tdCorso.Controls.Add(h);
                    trCorso.Cells.Add(tdPosizione);
                    trCorso.Cells.Add(tdCorso);
                }
            }
            else
            {
                lbl_search.Text = "La ricerca non ha prodotto alcun risultato";
            }            
        }
    }
}