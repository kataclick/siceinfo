﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Cemi.SitoCorsi
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlContainerControl controlBody = FindControl("body") as HtmlContainerControl;

            if (controlBody != null)
                controlBody.Attributes.Add("style",
                                           "background-repeat: repeat-y; background-image:url(" +
                                           ResolveUrl("~/images/onda-0.gif") + ");background-position:center top;");
        }
    }
}
