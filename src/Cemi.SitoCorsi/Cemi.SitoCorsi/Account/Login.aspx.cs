﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Cemi.SitoCorsi.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //MasterPage master = Page.Master;
            //Image img = (Image) master.FindControl("BANNER");
            //img.ImageUrl = ResolveUrl("~/Images/BANNER.gif");
            //img.DataBind();
        }

        public void LoginCorsi_Authenticate(object sender, AuthenticateEventArgs e)
        {
            string username = LoginCorsi.UserName;
            string password = LoginCorsi.Password;            


            if (Membership.ValidateUser(username, password))
            {
                e.Authenticated = true;
            }
            else
            {
                e.Authenticated = false;
            }
        }
    }
}


