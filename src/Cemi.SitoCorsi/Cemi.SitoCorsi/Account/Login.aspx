﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="Cemi.SitoCorsi.Account.Login" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 style="color:Navy;">
        Accesso utenti registrati
    </h2>
    <div style="width:600px">
        <asp:Login ID="LoginCorsi" runat="server" BorderColor="#E6E2D8" BackColor="#F7F6F3" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" Font-Names="Calibri" Font-Size="0.9em" ForeColor="#333333" OnAuthenticate="LoginCorsi_Authenticate" DisplayRememberMe="false" DestinationPageUrl="~/Default_800.aspx">
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <LayoutTemplate>
                <table cellpadding="4" cellspacing="0" style=" border: none none none;">
                    <tr>
                        <td>
                            <table cellpadding="0">
                                <tr>
                                    <td align="center" colspan="2" style="color:White;background-color:#84C6AC;font-size:0.9em;font-weight:bold;">
                                        Log In</td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Utente:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="UserName" runat="server" Font-Size="1em" Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="LoginCorsi">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Password" runat="server" Font-Size="1em" Width="200px" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="LoginCorsi">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2" style="color:Red;">
                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="2">
                                        <asp:Button ID="LoginButton" runat="server" BackColor="#84C6AC" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Calibri" Font-Bold="true" Font-Size="0.9em" ForeColor="White" Text="Accedi" ValidationGroup="LoginCorsi" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            
        </asp:Login>
        <!-- <LoginButtonStyle BackColor="#84C6AC" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="Calibri" Font-Size="0.8em" ForeColor="#284775" />
            <TextBoxStyle Font-Size="1em" />
            <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="1em" ForeColor="White" /> -->
    </div>

</asp:Content>
