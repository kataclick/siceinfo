﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Cemi.SitoCorsi.Type;
using Cemi.SitoCorsi.Data;
using Cemi.SitoCorsi.GestioneUtenti.Type.Entities;
using Cemi.SitoCorsi.GestioneUtenti.Type;
using System.Web.Security;

using System.Web.UI.HtmlControls;

namespace Cemi.SitoCorsi
{
    public partial class DettagliCorso : Page
    {
        //CorsiDataAccess Cda = new CorsiDataAccess();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Modify.Visible = false;
                ReadOnly.Visible = true;

                if (Request.QueryString["Gruppo"] != null)
                {
                    HtmlGenericControl body = (HtmlGenericControl)Master.FindControl("MasterPage");
                    body.Style.Add(HtmlTextWriterStyle.BackgroundImage, "Images/onda-" + Request.QueryString["Gruppo"] + ".gif");
                    ImgHeader.ImageUrl = "Images/800-" + Request.QueryString["Gruppo"] + ".gif";
                }

             

                if (Request.QueryString["idCorso"] != null)
                {
                    if (Membership.GetUser() != null)
                    {
                        HyperLinkEdit.NavigateUrl = "~/DettagliCorso.aspx?idCorso=" + Request.QueryString["idCorso"].ToString() + "&Gruppo=" + Request.QueryString["Gruppo"].ToString() + "&Mod=true";
                        EditDiv.Style.Add("display", "true");
                    }
                    else
                    {
                        EditDiv.Style.Add("display", "none");
                    }

                    Corso Corso = new Corso();
                    CorsiDataAccess cda = new CorsiDataAccess();
                    int idCorso = Convert.ToInt32(Request.QueryString["idCorso"]);
                    //DettaglioCorso.DataSource = GetCorsoDS(idCorso);
                    //DettaglioCorso.DataBind();
                    Corso = cda.GetCorso(idCorso, null);

                    InitializeReadOnly(Corso);

                    if (Request.QueryString["Mod"] != null)
                    {
                        HyperLink2.NavigateUrl = "~/DettagliCorso.aspx?idCorso=" + Request.QueryString["idCorso"].ToString() + "&Gruppo=" + Request.QueryString["Gruppo"].ToString() + "&Mod=false";

                        if (Request.QueryString["Mod"].ToString() == "false")
                        {
                            Modify.Visible = false;
                            ReadOnly.Visible = true;
                            //InitializeReadOnly(Corso);

                        }
                        if (Request.QueryString["Mod"].ToString() == "true")
                        {
                            if (Membership.GetUser() != null)
                            {
                                Modify.Visible = true;
                                ReadOnly.Visible = false;
                                InitializeModify(Corso);
                            }
                        }
                    }
                    #region Vecchio codice
                    /*                    
                    TxtGruppo.Text = Corso.Gruppo;

                    if (!String.IsNullOrEmpty(Corso.Categoria))
                    {
                        RowCategoria.Visible = true;
                        TxtCategoria.Text = Corso.Categoria;
                        Mod_txt_Categoria.Text = Corso.Categoria;
                    }

                    TxtTitoloCorso.Text = Corso.TitoloCorso;
                    Mod_txt_Titolo.Text = Corso.TitoloCorso;
                    TxtDestinatari.Text = Corso.Destinatari;
                    Mod_txt_Destinatari.Text = Corso.Destinatari;

                    if (String.IsNullOrEmpty(Corso.RequisitiAccesso))
                        TxtRequisitiAccesso.Text = "Nessuno";
                    else
                        TxtRequisitiAccesso.Text = Corso.RequisitiAccesso;
                    Mod_txt_Requisiti.Text = Corso.RequisitiAccesso;

                    TxtDurata.Text = Corso.Durata;
                    Mod_txt_Durata.Text = Corso.Durata;
                    TxtContenutiDiMassima.Text = Corso.ContenutiDiMassima;
                    Mod_txt_Contenuti.Content = Corso.ContenutiDiMassima;

                    if (!String.IsNullOrEmpty(Corso.RiferimentiLeggeNormativi))
                    {
                        RowNormativa.Visible = true;
                        TxtRiferimentiLeggeNormativa.Text = Corso.RiferimentiLeggeNormativi;
                        Mod_txt_Riferimenti.Text = Corso.RiferimentiLeggeNormativi;
                    }

                    TxtFrequenzaMinima.Text = Corso.FrequenzaMinima;
                    Mod_txt_Frequenza.Text = Corso.FrequenzaMinima;

                    if (String.IsNullOrEmpty(Corso.VerificaFinale))
                        TxtVerificaFinale.Text = "No";
                    else
                        TxtVerificaFinale.Text = Corso.VerificaFinale;
                    Mod_txt_Verifica.Text = Corso.VerificaFinale;

                    TxtAttestazioneRilasciata.Text = Corso.AttestazioneRilasciata;
                    Mod_txt_Attestazione.Text = Corso.AttestazioneRilasciata;
                    TxtCadenzaPeriodica.Text = Corso.CadenzaPeriodica;
                    Mod_txt_Cadenza.Text = Corso.CadenzaPeriodica;

                    if (!String.IsNullOrEmpty(Corso.ValiditaAttestato))
                    {
                        RowValidita.Visible = true;
                        //LabelValiditaAttestato.Visible = true;
                        //TxtValiditaAttestato.Visible = true;
                        TxtValiditaAttestato.Text = Corso.ValiditaAttestato;
                        Mod_txt_Validita.Text = Corso.ValiditaAttestato;
                    }

                    if (!String.IsNullOrEmpty(Corso.Link))
                    {
                        RowLink.Visible = true;
                        lnk_link.NavigateUrl = Corso.Link;
                        if (Corso.Fonte == "2")
                        {
                            lbl_reserved.Visible = true;
                        }
                    }
                    

                    TxtGruppo.DataBind();
                    DataSet ds2 = GetCorsoDS(idCorso);
                    FormDettaglioCorso.DataSource = ds2;
                    FormDettaglioCorso.DataBind();
                    */
                    #endregion
                }
            }

        }

        private void InitializeReadOnly(Corso Corso)
        {
            if (!String.IsNullOrEmpty(Corso.Categoria))
            {
                RowCategoria.Visible = true;
                TxtCategoria.Text = Corso.Categoria;
            }

            TxtTitoloCorso.Text = Corso.TitoloCorso;
            TxtDestinatari.Text = Corso.Destinatari;

            if (String.IsNullOrEmpty(Corso.RequisitiAccesso))
                TxtRequisitiAccesso.Text = "Nessuno";
            else
                TxtRequisitiAccesso.Text = Corso.RequisitiAccesso;

            TxtDurata.Text = Corso.Durata;
            TxtContenutiDiMassima.Text = Corso.ContenutiDiMassima;

            if (!String.IsNullOrEmpty(Corso.RiferimentiLeggeNormativi))
            {
                RowNormativa.Visible = true;
                TxtRiferimentiLeggeNormativa.Text = Corso.RiferimentiLeggeNormativi;
            }

            TxtFrequenzaMinima.Text = Corso.FrequenzaMinima;

            if (String.IsNullOrEmpty(Corso.VerificaFinale))
                TxtVerificaFinale.Text = "No";
            else
                TxtVerificaFinale.Text = Corso.VerificaFinale;


            TxtAttestazioneRilasciata.Text = Corso.AttestazioneRilasciata;
            TxtCadenzaPeriodica.Text = Corso.CadenzaPeriodica;

            if (!String.IsNullOrEmpty(Corso.ValiditaAttestato))
            {
                RowValidita.Visible = true;
                TxtValiditaAttestato.Text = Corso.ValiditaAttestato;
            }

            if (!String.IsNullOrEmpty(Corso.Link))
            {
                RowLink.Visible = true;
                lnk_link.NavigateUrl = Corso.Link;
                if (Corso.Fonte == "2")
                {
                    lbl_reserved.Visible = true;
                }
            }
        }

        private void InitializeModify(Corso Corso)
        {
//            if (!String.IsNullOrEmpty(Corso.Categoria))
//            {
                Mod_txt_Categoria.Text = Corso.Categoria;
//            }

            Mod_txt_Titolo.Text = Corso.TitoloCorso;
            Mod_txt_Destinatari.Text = Corso.Destinatari;
            Mod_txt_Requisiti.Text = Corso.RequisitiAccesso;
            Mod_txt_Durata.Text = Corso.Durata;
            Mod_txt_Contenuti.Content = Corso.ContenutiDiMassima;

//            if (!String.IsNullOrEmpty(Corso.RiferimentiLeggeNormativi))
//            {
                Mod_txt_Riferimenti.Text = Corso.RiferimentiLeggeNormativi;
//            }

            Mod_txt_Frequenza.Text = Corso.FrequenzaMinima;
            Mod_txt_Verifica.Text = Corso.VerificaFinale;
            Mod_txt_Attestazione.Text = Corso.AttestazioneRilasciata;
            Mod_txt_Cadenza.Text = Corso.CadenzaPeriodica;

//            if (!String.IsNullOrEmpty(Corso.ValiditaAttestato))
//            {
                Mod_txt_Validita.Text = Corso.ValiditaAttestato;
//            }

        }

        protected void UpdateSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Corso corso = new Corso();
                CorsiDataAccess cda = new CorsiDataAccess();
                bool result;

                corso.idCorso = Convert.ToInt32(Request.QueryString["idCorso"]);
                corso.TitoloCorso = Mod_txt_Titolo.Text;
                corso.Destinatari = Mod_txt_Destinatari.Text;
                corso.RequisitiAccesso = Mod_txt_Requisiti.Text;
                corso.Durata = Mod_txt_Durata.Text;
                corso.ContenutiDiMassima = Mod_txt_Contenuti.Content;
                corso.RiferimentiLeggeNormativi = Mod_txt_Riferimenti.Text;
                corso.FrequenzaMinima = Mod_txt_Frequenza.Text;
                corso.VerificaFinale = Mod_txt_Verifica.Text;
                corso.CadenzaPeriodica = Mod_txt_Cadenza.Text;
                corso.AttestazioneRilasciata = Mod_txt_Attestazione.Text;
                corso.ValiditaAttestato = Mod_txt_Validita.Text;

                result = cda.UpdateCorso(corso);
                if (result == false)
                {
                    UpdateResult.Text = "Errore durante l'aggiornamento. Ritentare in seguito";
                    UpdateResult.Visible = true;
                }
                else
                {
                    Server.Transfer("~/DettagliCorso.aspx?idCorso=" + Request.QueryString["idCorso"].ToString() + "&Gruppo=" + Request.QueryString["Gruppo"].ToString());
                }
            }
        }

        /* NON PIU' USATO
        protected DataSet GetCorsoDS(int idCorso)
        {
            string query = "SELECT TG.descrizione AS gruppo,CG.Descrizione AS Categoria,[TitoloCorso],[Destinatari],[RequisitiAccesso],[Durata],[ContenutiDiMassima],[RiferimentiLeggeNormativi],[FrequenzaMinima],[VerificaFinale],[AttestazioneRilasciata],[CadenzaPeriodica],[ValiditaAttestato] FROM dbo.Corsi C INNER JOIN dbo.CorsiTipiGruppo TG ON C.idGruppo = TG.idCorsiGruppo LEFT OUTER JOIN dbo.CorsiTipiCategoria CG ON C.idCategoria = CG.IdCorsiCategoria WHERE idCorso = " + idCorso.ToString();
            string conStr = ConfigurationManager.ConnectionStrings["Corsi"].ConnectionString.ToString();
            SqlConnection conn = new SqlConnection(conStr);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(ds);
            }
            catch
            {
                return ds;
            }
            return ds;
        }
         * */

    }
}