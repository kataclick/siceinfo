﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Cemi.SitoCorsi._Default" %>
    

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<table border="0">
<tr>
<td>
<div style="margin-Left:5px; margin-top:15px; vertical-align:bottom; text-align:left; height:40px;" align="left">
                <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Account/Login.aspx" runat="server">Accesso Amministratore</asp:HyperLink>
            </div>
</td>
<td>
<div style="margin-right:5px; margin-top:15px; vertical-align:bottom; text-align:right; height:40px;" align="right">
                <asp:TextBox ID="txt_search" runat="server" Height="15px" AutoCompleteType="Search" BorderColor="#84C6AC" BorderStyle="Solid" Font-Names="Verdana" Font-Size="X-Small" Width="200px" ValidationGroup="Search"></asp:TextBox>
                <asp:Button ID="btn_search" runat="server" Text="Cerca" Height="20px" BackColor="#84C6AC" Font-Names="verdana" BorderColor="#84C6AC" BorderStyle="Solid" BorderWidth="0px" Font-Size="X-Small" Font-Strikeout="False" ForeColor="Black" onclick="btn_search_Click" ValidationGroup="Search" />
                <br />
                <asp:RequiredFieldValidator ID="SearchValidator" runat="server" ErrorMessage="Inserire un valore per effettuare la ricerca" ControlToValidate="txt_search" ForeColor="#FF3300" ValidationGroup="Search"></asp:RequiredFieldValidator>
            </div>
</td>
</tr>
</table>
            
            
    <h2>
       Benvenuti!
    </h2>
    <div>Testo Introduttivo</div>

    <div style="width:1038px;">
    <asp:Table runat="server" ID="Sommario">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderWide">
                <asp:ImageButton id="btn_Contrattuale" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Formazione-obbligatoria-contrattuale-10214x50.gif" onclick="btn_Contrattuale_Click" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" id="RowContrattuale">
            <asp:TableCell Cssclass="bordo">
            <asp:GridView ID="SommarioContrattuale" runat="server" AutoGenerateColumns="False" OnRowDataBound="SommarioContrattuale_RowDataBound" DataKeyNames="idCorso" CellPadding="0" ShowHeader="false">
                    <AlternatingRowStyle VerticalAlign="Top" BackColor="#c7cf99"/>
                    <HeaderStyle Height="56px" Width="1028px" />
                <Columns>
                    <asp:BoundField DataField="idCorso" HeaderText="idCorso" Visible="False"/>
                    <asp:BoundField DataField="TitoloCorso" HeaderText="Formazione obbligatoria - Contrattuale" Visible="False" />
                    <asp:TemplateField HeaderText="Formazione obbligatoria - Contrattuale" HeaderImageUrl="~/Images/Formazione-obbligatoria-contrattuale-10214x50.gif">
                        <ItemTemplate>
                            <asp:HyperLink ID="LinkContrattuale" HeaderText="Formazione obbligatoria - Contrattuale" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                        <HeaderStyle Width="1028px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="invisible">&nbsp;</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderWide">
                <asp:ImageButton id="btn_Legislativo" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="Images/Formazione-obbligatoria-legislativo10214x50.gif" onclick="btn_Legislativo_Click" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" id="RowLegislativo">
        <asp:TableCell Cssclass="bordo">
            <asp:GridView ID="SommarioLegislativo" runat="server" AutoGenerateColumns="False" OnRowDataBound="SommarioLegislativo_RowDataBound" ShowHeader="false">
                <AlternatingRowStyle BackColor="#bcdecd"/>
                <HeaderStyle Height="56px" Width="1028px" />
                <Columns>
                    <asp:TemplateField ShowHeader="false">
                        <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width:20%;"><asp:Label ID="categoria" runat="server" Font-Bold="true"></asp:Label></td>
                                        <td><asp:GridView ID="CorsiLegislativo" runat="server" AutoGenerateColumns="false" AllowPaging="False" onrowdatabound="CorsiLegislativo_RowDataBound" DataKeyNames="idCorso" ShowHeader="false" Cssclass="bordo">
                                            <AlternatingRowStyle BackColor="#bcdecd"/>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="LinkLegislativo" HeaderText="Formazione obbligatoria - Legislativa" runat="server" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView></td>
                                    </tr>
                                </table>
                          </ItemTemplate>
                        
                    </asp:TemplateField>
                </Columns>


            </asp:GridView>
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="invisible">&nbsp;</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderWide">
                <asp:ImageButton id="btn_Operai" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Crescita-Professionale-operi-10214x50.gif" onclick="btn_Operai_Click" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" id="RowOperai">
        <asp:TableCell Cssclass="bordo">
            <asp:GridView ID="SommarioCrescitaOperai" runat="server" AutoGenerateColumns="False" AllowPaging="False" OnRowDataBound="SommarioCrescitaOperai_RowDataBound" ShowHeader="false">
                <AlternatingRowStyle BackColor="#a6dbf2"/>
                <HeaderStyle Height="56px" Width="1028px" />
                <Columns>
                    <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width:20%;"><asp:Label ID="categoria" runat="server" Font-Bold="true"></asp:Label></td>
                                        <td><asp:GridView ID="CorsiCrescitaOperai" runat="server" AutoGenerateColumns="false" AllowPaging="False" onrowdatabound="CorsiCrescitaOperai_RowDataBound" DataKeyNames="idCorso" ShowHeader="False" Cssclass="bordo">
                                            <AlternatingRowStyle BackColor="#a6dbf2" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="LinkCrescitaOperai" HeaderText="Crescita professionale Operai" runat="server" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView></td>
                                    </tr>
                                </table>
                          </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell CssClass="invisible" >&nbsp;</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" Cssclass="bordoHeaderWide">
                <asp:ImageButton id="btn_Tecnici" runat="server" AlternateText="Clicca per espandere" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Crescita-professionale-tecnici-10214x50.gif" onclick="btn_Tecnici_Click" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="RowTecnici">
            <asp:TableCell Cssclass="bordo">
                <asp:GridView ID="SommarioCrescitaTecnici" runat="server" AutoGenerateColumns="False" AllowPaging="False" OnRowDataBound="SommarioCrescitaTecnici_RowDataBound" ShowHeader="false">
                    <AlternatingRowStyle BackColor="#9fc7bf"/>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width:20%;"><asp:Label ID="categoria" runat="server" Font-Bold="true"></asp:Label></td>
                                        <td><asp:GridView ID="CorsiCrescitaTecnici" runat="server" AutoGenerateColumns="false" AllowPaging="False" onrowdatabound="CorsiCrescitaTecnici_RowDataBound" DataKeyNames="idCorso" ShowHeader="false" Cssclass="bordo">
                                            <AlternatingRowStyle BackColor="#9fc7bf"/>
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="LinkCrescitaTecnici" runat="server" ></asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView></td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </div>

    </asp:Content>
