﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchResult.aspx.cs" Inherits="Cemi.SitoCorsi.SearchResult" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p><asp:Label ID="lbl_search" runat="server" Font-Bold="true" Font-Size="Medium"></asp:Label></p>
    <p><asp:Table ID="tbl_results" runat="server" class="bordo" style="background-color:#daefc7; width:800px; text-align:left;">
    </asp:Table></p>
    <p>
        <asp:HyperLink ID="HyperLink1" NavigateUrl="javascript:history.back();" runat="server" Font-Names="verdana" Font-Size="10pt">Torna Indietro</asp:HyperLink>
    </p>
    </asp:Content>
