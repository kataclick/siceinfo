﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Cemi.SitoCorsi.Type;
using System.Xml.Serialization;
using Cemi.SitoCorsi.Type.Entities;

namespace Cemi.SitoCorsi.Data
{
    public class CorsiDataAccess
    {
        public CorsiDataAccess()
        {
            DatabaseCorsi = DatabaseFactory.CreateDatabase("Corsi");
        }

        public Database DatabaseCorsi { get; set; }

        public string FirstUpper(string CapsLock)
        {
            string firstUpper = string.Empty;
            firstUpper = char.ToUpper(CapsLock[0]).ToString();
            for (int i = 1; i < CapsLock.Length; i++)
            {
                firstUpper = firstUpper + char.ToLower(CapsLock[i]);
            }
            return firstUpper;
        }

        public Corso GetCorso(int? idCorso, int? idCategoria)
        {
            string query = "SELECT TG.descrizione AS gruppo,CG.Descrizione AS categoria,[TitoloCorso],[Destinatari],[RequisitiAccesso],[Durata],";
            query = query + "[ContenutiDiMassima],[RiferimentiLeggeNormativi],[FrequenzaMinima],[VerificaFinale],";
            query = query + "[AttestazioneRilasciata],[CadenzaPeriodica],[ValiditaAttestato],[Link],[Organizzatore] ";
            query = query + "FROM dbo.Corsi C INNER JOIN dbo.CorsiTipiGruppo TG ON C.idGruppo = TG.idCorsiGruppo LEFT OUTER JOIN dbo.CorsiTipiCategoria CG ON C.idCategoria = CG.IdCorsiCategoria WHERE 1 = 1";
            if (idCorso != null)
                query = query + " AND idCorso = " + idCorso.ToString();
            if (idCategoria != null)
                query = query + " AND idCategoria = " + idCategoria.ToString();
            Corso corso = new Corso();

            using (DbCommand comm = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader reader = DatabaseCorsi.ExecuteReader(comm))
                {
                    while (reader.Read())
                    {
                        corso.Gruppo = reader["gruppo"].ToString();
                        corso.Categoria = reader["categoria"].ToString();
                        corso.TitoloCorso = FirstUpper(reader["TitoloCorso"].ToString());
                        corso.Destinatari = reader["Destinatari"].ToString();
                        corso.RequisitiAccesso = reader["RequisitiAccesso"].ToString();
                        corso.Durata = reader["Durata"].ToString();
                        corso.ContenutiDiMassima = reader["ContenutiDiMassima"].ToString();
                        corso.RiferimentiLeggeNormativi = reader["RiferimentiLeggeNormativi"].ToString();
                        corso.FrequenzaMinima = reader["FrequenzaMinima"].ToString();
                        corso.VerificaFinale = reader["VerificaFinale"].ToString();
                        corso.AttestazioneRilasciata = reader["AttestazioneRilasciata"].ToString();
                        corso.CadenzaPeriodica = reader["CadenzaPeriodica"].ToString();
                        corso.ValiditaAttestato = reader["ValiditaAttestato"].ToString();
                        corso.Link = reader["Link"].ToString();
                        corso.Fonte = reader["Organizzatore"].ToString();
                    }
                }
            }
            return corso;
        }

        public DataSet GetSommario(int gruppo)
        {
            string query = "SELECT [idCorso],[TitoloCorso] FROM [CatalogoCorsi].[dbo].[Corsi]";

            query = query + " Where 1 = 1";
            query = query + " AND idGruppo = " + gruppo.ToString() + " AND Link IS NOT NULL";
            query = query + " ORDER BY [TitoloCorso]";
            using (DbCommand comm = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader dr = DatabaseCorsi.ExecuteReader(comm))
                {
                    DataSet ds = new DataSet("Sommario");
                    ds.Load(dr, System.Data.LoadOption.Upsert, "Sommario");
                    return ds;
                }
            }
        }

        public List<Categoria> GetSommarioCategoria(int gruppo)
        {
            string query = "SELECT C.idCategoria,CG.Descrizione AS Categoria";
            query = query + " FROM dbo.Corsi C LEFT OUTER JOIN dbo.CorsiTipiCategoria CG ON C.idCategoria = CG.IdCorsiCategoria";
            query = query + " WHERE idGruppo = " + gruppo.ToString() + " AND C.Link IS NOT NULL";
            query = query + " GROUP BY C.idCategoria,CG.descrizione";
            List<Categoria> CateList = new List<Categoria>();
            
            
            using (DbCommand comm = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader dr = DatabaseCorsi.ExecuteReader(comm))
                {
                    while (dr.Read())
                    {
                        Categoria cat = new Categoria();
                        cat.Nome = dr["Categoria"].ToString();
                        cat.Corsi = GetListaCorsi((int)dr["idCategoria"]);
                        CateList.Add(cat);
                     }
                }
            }
            return CateList;
        }
        
        public List<CorsoBase> GetListaCorsi(int categoria)
        {
            string query = "SELECT [idCorso],[TitoloCorso] FROM [CatalogoCorsi].[dbo].[Corsi]";

            query = query + " Where 1 = 1";
            query = query + " AND idCategoria = " + categoria.ToString();
            query = query + " ORDER BY [TitoloCorso]";
            List<CorsoBase> listaCorsi = new List<CorsoBase>();
            
            using (DbCommand comm = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader dr = DatabaseCorsi.ExecuteReader(comm))
                {
                    while (dr.Read())
                    {
                        CorsoBase cb = new CorsoBase();
                        cb.idCorso = (int)dr["idCorso"];
                        cb.TitoloCorso = FirstUpper(dr["TitoloCorso"].ToString());
                        listaCorsi.Add(cb);
                    }
                }
            }
            return listaCorsi;
        }

        public List<CorsoRicerca> Search(string searchString)
        {
            List<CorsoRicerca> results = new List<CorsoRicerca>();

            if (!string.IsNullOrEmpty(searchString))
            {
                string query = "INSERT INTO [CatalogoCorsi].[dbo].[CorsiRicerche] ([StringaRicerca],[dataInserimentoRecord]) ";
                query = query + "VALUES ('"+searchString.ToString()+"',getDate())";
                using (DbCommand commWrite = DatabaseCorsi.GetSqlStringCommand(query))
                {
                    DatabaseCorsi.ExecuteNonQuery(commWrite);
                }

                using (DbCommand comm = DatabaseCorsi.GetStoredProcCommand("SearchAllTables", searchString))
                {
                    using (IDataReader dr = DatabaseCorsi.ExecuteReader(comm))
                    {
                        while (dr.Read())
                        {
                            CorsoRicerca cb = new CorsoRicerca();
                            cb.idCorso = (int)dr["idCorso"];
                            cb.TitoloCorso = FirstUpper(dr["TitoloCorso"].ToString());
                            cb.Gruppo = dr["idGruppo"].ToString();
                            results.Add(cb);
                        }
                    }
                }
            }
            return results;
        }

        public DataSet Top10()
        {
            //List<string> top10 = new List<string>();
            string query = "SELECT TOP 10 [StringaRicerca] FROM [dbo].[CorsiRicerche] GROUP BY [StringaRicerca] ORDER BY COUNT(*) DESC, [StringaRicerca] ASC";
            using (DbCommand comm = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader dr = DatabaseCorsi.ExecuteReader(comm))
                {
                        DataSet ds = new DataSet("Top10");
                        ds.Load(dr, System.Data.LoadOption.Upsert, "Top10");
                        return ds;
                        //top10.Add(dr["StringaRicerca"].ToString());
                }
            }
        }

        public bool UpdateCorso(Corso corso)
        {
            bool res;
            try
            {
                using (DbCommand comm = DatabaseCorsi.GetStoredProcCommand("dbo.CorsiUpdate"))
                {
                    DatabaseCorsi.AddInParameter(comm, "@idCorso", DbType.Int32, corso.idCorso);
                    DatabaseCorsi.AddInParameter(comm, "@TitoloCorso", DbType.String, corso.TitoloCorso);
                    DatabaseCorsi.AddInParameter(comm, "@Destinatari", DbType.String, corso.Destinatari);
                    DatabaseCorsi.AddInParameter(comm, "@RequisitiAccesso", DbType.String, corso.RequisitiAccesso);
                    DatabaseCorsi.AddInParameter(comm, "@Durata", DbType.String, corso.Durata);
                    DatabaseCorsi.AddInParameter(comm, "@ContenutiDiMassima", DbType.String, corso.ContenutiDiMassima);
                    DatabaseCorsi.AddInParameter(comm, "@RiferimentiLeggeNormativi", DbType.String, corso.RiferimentiLeggeNormativi);
                    DatabaseCorsi.AddInParameter(comm, "@FrequenzaMinima", DbType.String, corso.FrequenzaMinima);
                    DatabaseCorsi.AddInParameter(comm, "@VerificaFinale", DbType.String, corso.VerificaFinale);
                    DatabaseCorsi.AddInParameter(comm, "@CadenzaPeriodica", DbType.String, corso.CadenzaPeriodica);
                    DatabaseCorsi.AddInParameter(comm, "@AttestazioneRilasciata", DbType.String, corso.AttestazioneRilasciata);
                    DatabaseCorsi.AddInParameter(comm, "@ValiditaAttestato", DbType.String, corso.ValiditaAttestato);

                    DatabaseCorsi.ExecuteNonQuery(comm);
                }
                res = true;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        
    }

}
