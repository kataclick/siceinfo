﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.SitoCorsi.Type.Entities
{
    public class Categoria
    {
        public String Nome { get; set; }

        public List<CorsoBase> Corsi { get; set; }
    }
}
