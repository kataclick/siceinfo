﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.SitoCorsi.Type
{
    public class Corso : CorsoBase
    {
        //public int idCorso { get; set; }
        public string Gruppo { get; set; }
        public string Categoria { get; set; }
        //public string TitoloCorso { get; set; }
        public string Destinatari { get; set; }
        public string RequisitiAccesso { get; set; }
        public string Durata { get; set; }
        public string ContenutiDiMassima { get; set; }
        public string RiferimentiLeggeNormativi { get; set; }
        public string FrequenzaMinima { get; set; }
        public string VerificaFinale { get; set; }
        public string AttestazioneRilasciata { get; set; }
        public string CadenzaPeriodica { get; set; }
        public string ValiditaAttestato { get; set; }
        public string Link { get; set; }
        public string Fonte { get; set; }
    }

    public class CorsoBase
    {
        public int idCorso { get; set; }
        public string TitoloCorso { get; set; }
    }

    public class CorsoRicerca : CorsoBase
    {
        public string Gruppo { get; set; }
    }
}
