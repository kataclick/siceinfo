﻿using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using Cemi.SitoCorsi.GestioneUtenti.Type.Entities;

namespace Cemi.SitoCorsi.GestioneUtenti.Data.Providers
{
    internal class SQLServerMembershipProvider : MembershipProvider
    {
        private GestioneUtenti cda;
        private int _minRequiredPasswordLength;

        

        public override int MinRequiredPasswordLength
        {
            get { return _minRequiredPasswordLength; }
        }

        // MembershipProvider Methods
        public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
                throw new ArgumentNullException("config");

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
                name = "SQLServerMembershipProvider";

            //Inizializzo lo strato di accesso al db
            cda = new GestioneUtenti();

            _minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "4"));

            // Call the base class's Initialize method
            base.Initialize(name, config);
        }

        private static string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        public override bool ValidateUser(string username, string password)
        {
            // Validate input parameters
            if (String.IsNullOrEmpty(username) ||
                String.IsNullOrEmpty(password))
                return false;

            try
            {
                // Validate the user name and password

                if (cda.EsisteUtente(username, password) /*&&
                    !Roles.IsUserInRole(username, "UtenteDisabilitato")*/)
                {
                    // NOTE: A read/write membership provider
                    // would update the user's LastLoginDate here.
                    // A fully featured provider would also fire
                    // an AuditMembershipAuthenticationSuccess
                    // Web event

                    return true;
                }

                // NOTE: A fully featured membership provider would
                // fire an AuditMembershipAuthenticationFailure
                // Web event here
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            // Utilizzo il Context per salvare l'utente corrente in modo che per ogni singola pagina
            // richiesta venga fatta solamente una richiesta al DB. Da notare che quando viene effettuato
            // un postback o si cambia pagina il Context torna ad essere nullo e quindi l'utente viene
            // nuovamente recuperato da DB
            //
            // Questo trucco è particolarmente utile per le molte chiamate al metodo "Autorizzato" sulla
            // stessa pagina. Alla prima chiamata viene memorizzato l'utente nel Context e poi recuperato
            // da li fin quando la pagina completa non viene inviata al client
            //
            // Sarebbe da verificare se può essere utilizzato HttpContext.Current.User al posto di creare
            // una voce ad-hoc

            Utente utente = null;

            if (!String.IsNullOrEmpty(username))
            {
                if (HttpContext.Current.Items["__Utente"] == null)
                {
                    HttpContext.Current.Items["__Utente"] = cda.GetUtente(username);
                }

                utente = HttpContext.Current.Items["__Utente"] as Utente;
            }

            //Utente utente = _gestioneUtentiDataAccess.GetUtente(username);
            return utente;
        }


        #region Not Supported

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotSupportedException(); }
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotSupportedException(); }
        }

        public override string ApplicationName
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotSupportedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotSupportedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotSupportedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotSupportedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotSupportedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotSupportedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotSupportedException(); }
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotSupportedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotSupportedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
                                                             string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotSupportedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email,
                                                  string passwordQuestion, string passwordAnswer, bool isApproved,
                                                  object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotSupportedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotSupportedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
                                                                  out int totalRecords)
        {
            throw new NotSupportedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
                                                                 out int totalRecords)
        {
            throw new NotSupportedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotSupportedException();
        }

        public override bool ChangePassword(string username, string answer, string var1)
        {
            throw new NotSupportedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotSupportedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotSupportedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotSupportedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotSupportedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotSupportedException();
        }

        #endregion
        








   




    }
}
