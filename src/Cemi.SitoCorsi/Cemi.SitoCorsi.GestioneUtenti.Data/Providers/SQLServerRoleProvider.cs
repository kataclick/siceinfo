﻿using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using Cemi.SitoCorsi.GestioneUtenti.Type;
using Cemi.SitoCorsi.GestioneUtenti.Type.Entities;

namespace Cemi.SitoCorsi.GestioneUtenti.Data.Providers
{
    internal class SQLServerRoleProvider : RoleProvider
    {
        private GestioneUtenti cda;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
                throw new ArgumentNullException("config");

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
                name = "SQLServerRoleProvider";

            //Inizializzo lo strato di accesso al db
            cda = new GestioneUtenti();

            // Call the base class's Initialize method
            base.Initialize(name, config);
        }

        public override bool IsUserInRole()
        {
            return true;
        }
    }
}
