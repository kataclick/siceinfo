﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.Security;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Cemi.SitoCorsi.GestioneUtenti.Type.Entities;
using Cemi.SitoCorsi.Data;

namespace Cemi.SitoCorsi.GestioneUtenti.Data
{
    public class GestioneUtenti
    {
        public GestioneUtenti()
        {
            DatabaseCorsi = DatabaseFactory.CreateDatabase("Corsi");
        }

        public Database DatabaseCorsi { get; set; }

        public bool EsisteUtente(string username, string password)
        {
            Utente utente = GetUtente(username, password);
            return utente != null;
        }

        public Utente GetUtente(string username, string password)
        {
            Utente utente = null;
            string query = "SELECT idUtente, Username, Nome, Cognome";
            query = query + " FROM dbo.Utenti";
            query = query + " Where Username = '"+ username +"' AND Password = '" + password + "'";
            using (DbCommand dbCommand = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader reader = DatabaseCorsi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        utente = new Utente((int)reader["idUtente"], (string)reader["Username"], (string)reader["nome"], (string)reader["Cognome"]);
                    }
                }
            }

            return utente;
        }

        public Utente GetUtente(string username)
        {
            Utente utente = null;
            string query = "SELECT idUtente, Username, Nome, Cognome";
            query = query + " FROM dbo.Utenti";
            query = query + " Where Username = '" + username + "'";

            using (DbCommand dbCommand = DatabaseCorsi.GetSqlStringCommand(query))
            {
                using (IDataReader reader = DatabaseCorsi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        try
                        {
                            utente = new Utente((int)reader["idUtente"], (string)reader["Username"], (string)reader["nome"], (string)reader["Cognome"]);
                        }
                        catch (Exception exception)
                        {
                            throw new Exception("GetUtente: impossibile recuperare l'utente corrente - " + exception.Message);
                        }
                    }
                }
            }
            return utente;
        }

        


    }
}
