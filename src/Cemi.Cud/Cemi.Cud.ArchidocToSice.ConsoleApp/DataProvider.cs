﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Cemi.Cud.ArchidocToSice.ConsoleApp
{
    internal class DataProvider
    {
        public static void CudInsertUpdate(CUD cud, bool checkPosizioneLavoratoreValida)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ToString()))
            {
                conn.Open();

                using (SqlCommand command = new SqlCommand("dbo.[USP_DocumentiCudInsertUpdate]", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@idArchidoc", cud.Guid);
                    command.Parameters.AddWithValue("@protocolloScansione", cud.RiferimentoAchidoc);
                    command.Parameters.AddWithValue("@anno", cud.Anno);
                    command.Parameters.AddWithValue("@idLavoratore", cud.IdLavoratore);
                    command.Parameters.AddWithValue("@codiceFiscale", cud.CodiceFiscale);
                    command.Parameters.AddWithValue("@checkPosizioneValida", checkPosizioneLavoratoreValida);
                    if (command.ExecuteNonQuery() == 0)
                    {
                    }
                }
            }
        }


        //public static List<CUD> CudInsertUpdate(List<CUD> cudList)
        //{
        //    int count = 0;
        //    List<CUD> cudScartati = new List<CUD>();
        //    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CEMI"].ToString()))
        //    {
        //        conn.Open();

        //        foreach (var cud in cudList)
        //        {

        //            count++;
        //            if (count % 1000 == 0)
        //            {
        //                Console.WriteLine("{0}/{1}", count, cudList.Count);
        //            }

        //            if (cud.IsValid)
        //            {

        //                try
        //                {
        //                    using (SqlCommand command = new SqlCommand("dbo.[USP_DocumentiCudInsertUpdate]", conn))
        //                    {
        //                        command.CommandType = System.Data.CommandType.StoredProcedure;
        //                        command.Parameters.AddWithValue("@idArchidoc", cud.Guid);
        //                        command.Parameters.AddWithValue("@protocolloScansione", cud.RiferimentoAchidoc);
        //                        command.Parameters.AddWithValue("@anno", cud.AnnoInt);
        //                        command.Parameters.AddWithValue("@idLavoratore", cud.IdLavoratoreInt);

        //                        command.ExecuteNonQuery();
        //                    }
        //                }
        //                catch (Exception exc)
        //                {
        //                    cudScartati.Add(cud);
        //                }
        //            }
        //            else
        //            {
        //                cudScartati.Add(cud);
        //            }

        //        }

        //    }


        //    return cudScartati;
        //}
    }
}