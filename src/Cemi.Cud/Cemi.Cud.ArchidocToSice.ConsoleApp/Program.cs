﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Cemi.Cud.ArchidocToSice.ConsoleApp.ServiceArchidoc;
using TBridge.Cemi.Business.Archidoc.Enums;

namespace Cemi.Cud.ArchidocToSice.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string ArchidocServer = ConfigurationManager.AppSettings["ArchidocServer"];
            string ArchidocMainEm = ConfigurationManager.AppSettings["ArchidocMainEm"];
            string ArchidocGpEm = ConfigurationManager.AppSettings["ArchidocGpEm"];
            string ArchidocDatabase = ConfigurationManager.AppSettings["ArchidocDatabase"];
            string ArchidocUsername = ConfigurationManager.AppSettings["ArchidocUsername"];
            string ArchidocPassword = ConfigurationManager.AppSettings["ArchidocPassword"];
            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "CUD";

            string key21;
            List<CUD> cudList = new List<CUD>();
            bool continueSearch;
            string referenceFrom;
            string referenceTo;
            string riferimetoArchidocAggiornamento = args.Length == 1 ? args[0] : null;

            if (string.IsNullOrEmpty(riferimetoArchidocAggiornamento))
            {
                key21 = ConfigurationManager.AppSettings["AnnoCud"];
                referenceFrom = null;
                referenceTo = null;
                continueSearch = true;
                Console.WriteLine("Avvio ricerca {0} anno {1}", nomeTipoDocumento, key21);
            }
            else
            {
                key21 = null;
                referenceFrom = riferimetoArchidocAggiornamento;
                referenceTo = riferimetoArchidocAggiornamento;
                continueSearch = false;

                Console.WriteLine("Avvio ricerca {0} riferimento {1}", nomeTipoDocumento,
                    riferimetoArchidocAggiornamento);
            }

            // String reference = "28172/12";
            // DataSet cardsDataSet;

            do
            {
                try
                {
                    DataSet cardsDataSet;
                    using (ServiceSoapClient client = new ServiceSoapClient())
                    {
                        cardsDataSet = client.GetKeys(ArchidocServer, ArchidocDatabase, ArchidocUsername,
                            ArchidocPassword, nomeArchivio, nomeTipoDocumento, 0,
                            referenceFrom, referenceTo,
                            null,
                            null, null,
                            null, null,
                            null, null, null, null, null,
                            key21, null, null, null, null,
                            null, null, null, null, null,
                            null, null, null, null, null,
                            null);
                    }

                    if (cardsDataSet.Tables.Count > 0 && cardsDataSet.Tables[0].Rows.Count > 0)
                    {
                        DataTable cardsTable = cardsDataSet.Tables[0];
                        foreach (DataRow card in cardsTable.Rows)
                        {
                            CUD cud = new CUD();
                            cud.Guid = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                            cud.RiferimentoAchidoc = card[ChiaviArchidocWsSsd.svIfReference.ToString()].ToString();
                            cud.CodiceFiscale = card[ChiaviArchidocWsSsd.svIfKey22.ToString()].ToString();
                            cud.ParseAnno(card[ChiaviArchidocWsSsd.svIfKey21.ToString()].ToString());
                            cud.ParseCodiceLavoratore(card[ChiaviArchidocWsSsd.svIfProtocol.ToString()].ToString());
                            cudList.Add(cud);
                        }

                        //cudFound = false;
                        GetNextReferences(cudList, out referenceFrom, out referenceTo);
                        Console.WriteLine("Documenti trovati: {0}", cudList.Count);
                    }
                    else
                    {
                        continueSearch = false;
                        Console.WriteLine("Ricerca terminata", cudList.Count);
                        Console.WriteLine("Documenti trovati: {0}", cudList.Count);
                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine("ERRORE:");
                    Console.WriteLine(exc);
                    continueSearch = false;
                }
            } while (continueSearch);

            if (cudList.Count > 0)
            {
                // DateTime start = DateTime.Now;
                Console.WriteLine("Inizio salvataggio su DB");
                SaveCud(cudList);
                Console.WriteLine("Salvataggio su DB completato");

                // Console.WriteLine(DateTime.Now - start);
            }

            Console.ReadKey();
        }
        
        private static void SaveCud(List<CUD> cudList)
        {
            int count = 0;
            bool checkPosizioneLavValida = ConfigurationManager.AppSettings["checkPosizioneLavoratoreValida"] == "1";
            List<CUD> cudScartati = new List<CUD>();
            foreach (var item in cudList.Where(x => !x.IdFamiliare.HasValue)) //Solo lavoratori
            {
                count++;
                if (count%1000 == 0)
                {
                    Console.WriteLine("{0}/{1}", count, cudList.Count);
                }

                if (item.IsValid)
                {
                    try
                    {
                        DataProvider.CudInsertUpdate(item, checkPosizioneLavValida);
                    }
                    catch (Exception exc)
                    {
                        cudScartati.Add(item);
                        Console.WriteLine("ERRORE:");
                        Console.WriteLine(exc.Message);
                        //throw;
                    }
                }
                else
                {
                    cudScartati.Add(item);
                }
            }

            if (cudScartati.Count > 0)
            {
                Console.WriteLine("ATTENZIONE:");
                Console.WriteLine("Salvataggio su DB fallito per {0} documenti", cudScartati.Count);
                foreach (var item in cudScartati)
                {
                    Console.WriteLine(item.RiferimentoAchidoc);
                }
            }
        }
        
        private static void GetNextReferences(List<CUD> cudList, out string referenceFrom, out string referenceTo)
        {
            const char separator = '/';

            if (cudList.Count > 0)
            {
                string[] split = cudList[cudList.Count - 1].RiferimentoAchidoc.Split(separator);
                string anno = split[1];
                int progr = int.Parse(split[0]);

                referenceFrom = $"{progr + 1}{separator}{anno}";
                referenceTo = $"{progr + 10001}{separator}{anno}";
            }
            else
            {
                referenceFrom = null;
                referenceTo = null;
            }
        }
    }
}