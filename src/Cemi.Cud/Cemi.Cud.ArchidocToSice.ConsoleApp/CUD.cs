﻿namespace Cemi.Cud.ArchidocToSice.ConsoleApp
{
    internal class CUD
    {
        public string RiferimentoAchidoc { set; get; }
        public string Guid { set; get; }
        public int IdLavoratore { private set; get; }
        public int? IdFamiliare { private set; get; }
        public string CodiceFiscale { set; get; }
        public int Anno { get; private set; }

        public bool IsValid
        {
            get { return !string.IsNullOrEmpty(CodiceFiscale) && IdLavoratore != 0 && Anno != 0; }
        }


        public void ParseAnno(string anno)
        {
            int result;
            if (int.TryParse(anno, out result))
            {
                Anno = result;
            }
            else
            {
                Anno = 0;
            }
        }

        public void ParseCodiceLavoratore(string codiceLavoratore)
        {
            IdLavoratore = 0;
            IdFamiliare = null;
            if (!string.IsNullOrEmpty(codiceLavoratore))
            {
                int idLav;
                int idFam;
                if (codiceLavoratore.Length == 8)
                {
                    if (int.TryParse(codiceLavoratore, out idLav))
                    {
                        IdLavoratore = idLav;
                    }
                }
                else if (codiceLavoratore.Length > 8)
                {
                    if (int.TryParse(codiceLavoratore.Substring(0, 8), out idLav))
                    {
                        IdLavoratore = idLav;
                        if (int.TryParse(codiceLavoratore.Substring(8, codiceLavoratore.Length - 8), out idFam))
                        {
                            IdFamiliare = idFam;
                        }
                    }
                }
            }
        }
    }
}