//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.ExpoChiusura.Type.Domain
{
    public partial class ExpoChiusuraPresenza
    {
        #region Primitive Properties
    
        public virtual int IdExpoChiusuraPresenza
        {
            get;
            set;
        }
    
        public virtual string IdExpo
        {
            get;
            set;
        }
    
        public virtual string Nome
        {
            get;
            set;
        }
    
        public virtual string Cognome
        {
            get;
            set;
        }
    
        public virtual string CodiceFiscale
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataNascita
        {
            get;
            set;
        }
    
        public virtual string Nazionalita
        {
            get;
            set;
        }
    
        public virtual string ComuneNascita
        {
            get;
            set;
        }
    
        public virtual string Sesso
        {
            get;
            set;
        }
    
        public virtual string Qualifica
        {
            get;
            set;
        }
    
        public virtual string Mansione
        {
            get;
            set;
        }
    
        public virtual string Categoria
        {
            get;
            set;
        }
    
        public virtual string AffidatarioRagioneSociale
        {
            get;
            set;
        }
    
        public virtual string AffidatarioPartitaIva
        {
            get;
            set;
        }
    
        public virtual string EsecutoreRagioneSociale
        {
            get;
            set;
        }
    
        public virtual string EsecutorePartitaIva
        {
            get;
            set;
        }
    
        public virtual string DatoreLavoroRagioneSociale
        {
            get;
            set;
        }
    
        public virtual string DatoreLavoroPartitaIva
        {
            get;
            set;
        }
    
        public virtual string CCNL
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> GiornoTimbratura
        {
            get;
            set;
        }

        #endregion
    }
}
