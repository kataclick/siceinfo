﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.ExpoChiusura.Business;

namespace Cemi.ExpoChiusura.ConsoleApp
{
    public class ExpoChiusuraProgram
    {
        private static readonly ExpoManager _expoManager = new ExpoManager();

        private static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("-- Specificare un parametro nel percorso file --");
                Console.WriteLine("-- Premere un tasto per terminare --");
                Console.ReadLine();
            }

            switch (args[0])
            {
                case "singolo":
                    GetPresenze(Convert.ToDateTime(args[1]), null);
                    break;
                case "range":
                    GetPresenze(DateTime.ParseExact(args[1],"dd/MM/yyyy",null), DateTime.ParseExact(args[2],"dd/MM/yyyy",null));
                    break;
            }
            //Console.ReadLine();
        }

        private static void GetPresenze(DateTime dataInizio, DateTime? dataFine)
        {
            if (dataFine == null)
            {
                dataFine = dataInizio;
            }

            DateTime giorno = dataInizio;

            while (giorno <= dataFine)
            {
                _expoManager.InsertPresenze(giorno);
                giorno = giorno.AddDays(1);
            }
        }
    }
}
