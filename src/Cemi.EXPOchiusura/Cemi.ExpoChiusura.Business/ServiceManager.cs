﻿using System;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Security.Cryptography;
//using Cemi.ExpoChiusura.Business.ExpoChiusuraServiceReference;
using Cemi.ExpoChiusura.Business.ExpoChiusuraService;
using Cemi.ExpoChiusura.Type.Domain;

namespace Cemi.ExpoChiusura.Business
{
    class ServiceManager
    {
        private String Password { get; set; }
        //public CEServiceSoapClient ExpoService { get; set; }
        public CEService ExpoService { get; set; }

        public ServiceManager()
        {
            Password = ConfigurationManager.AppSettings["Password"];
            //ExpoService = new CEServiceSoapClient();
            ExpoService = new CEService();
        }

        public List<ExpoChiusuraPresenza> GetPresenze(DateTime giorno)
        {

            Persone[] per = new Persone[0];
            
            try
            {
                per = ExpoService.getPeoplePresence(giorno.ToString("dd/MM/yyyy"), CalculateMD5Hash(giorno.ToString("dd/MM/yyyy") + Password).ToString());
            }

            catch (WebException exc)
            {
                Console.WriteLine(String.Format("Eccezione WebException: {0}", exc.Message));
                Console.ReadLine();
            }

            List<ExpoChiusuraPresenza> presenze = new List<ExpoChiusuraPresenza>();

            if (per != null)
            {
                foreach (Persone persona in per)
                {
                    ExpoChiusuraPresenza presenza = new ExpoChiusuraPresenza();

                    presenza.IdExpo = persona.ID;
                    presenza.Nome = !String.IsNullOrEmpty(persona.Nome) ? persona.Nome : null;
                    presenza.Cognome = !String.IsNullOrEmpty(persona.Cognome) ? persona.Cognome : null;

                    if (persona.CF.Length > 16)
                    {
                        presenza.CodiceFiscale = persona.CF.Trim().Substring(0, 16);
                    }
                    else { presenza.CodiceFiscale = !String.IsNullOrEmpty(persona.CF) ? persona.CF : null; }


                    presenza.DataNascita = DateTime.ParseExact(persona.Data_Nascita, "dd/MM/yyyy", null);
                    presenza.Nazionalita = !String.IsNullOrEmpty(persona.Nazionalità) ? persona.Nazionalità : null;
                    presenza.ComuneNascita = !String.IsNullOrEmpty(persona.Comune_Nascita) ? persona.Comune_Nascita : null;

                    if (persona.Sesso.Length > 1)
                    {
                        presenza.Sesso = persona.Sesso.Substring(0, 1);
                    }
                    else { presenza.Sesso = !String.IsNullOrEmpty(persona.Sesso) ? persona.Sesso : null; }
                    
                    presenza.Qualifica = !String.IsNullOrEmpty(persona.Qualifica) ? persona.Qualifica : null;
                    presenza.Mansione = !String.IsNullOrEmpty(persona.Mansione) ? persona.Mansione : null;
                    presenza.Categoria = !String.IsNullOrEmpty(persona.Categoria) ? persona.Categoria : null;

                    presenza.AffidatarioRagioneSociale = !String.IsNullOrEmpty(persona.Affidatario) ? persona.Affidatario : null;

                    if (persona.Piva_Affidatario.Length > 16)
                    {
                        presenza.AffidatarioPartitaIva = persona.Piva_Affidatario.Substring(0, 16);
                    }
                    else { presenza.AffidatarioPartitaIva = !String.IsNullOrEmpty(persona.Piva_Affidatario) ? persona.Piva_Affidatario : null; }

                    presenza.EsecutoreRagioneSociale = !String.IsNullOrEmpty(persona.Esecutore) ? persona.Esecutore : null;

                    if (persona.Piva_Esecutore.Length > 16)
                    {
                        presenza.EsecutorePartitaIva = persona.Piva_Esecutore.Substring(0, 16);
                    }
                    else { presenza.EsecutorePartitaIva = !String.IsNullOrEmpty(persona.Piva_Esecutore) ? persona.Piva_Esecutore : null; }

                    presenza.DatoreLavoroRagioneSociale = !String.IsNullOrEmpty(persona.Datore_Lavoro) ? persona.Datore_Lavoro : null;


                    if (persona.Piva_Datore_Lavoro.Length > 16)
                    {
                        presenza.DatoreLavoroPartitaIva = persona.Piva_Datore_Lavoro.Substring(0, 16);
                    }
                    else { presenza.DatoreLavoroPartitaIva = !String.IsNullOrEmpty(persona.Piva_Datore_Lavoro) ? persona.Piva_Datore_Lavoro : null; }
                    
                    presenza.CCNL = !String.IsNullOrEmpty(persona.CCNL) ? persona.CCNL : null;
                    presenza.GiornoTimbratura = giorno;

                    presenze.Add(presenza);
                }
            }
            
            return presenze;
        
        }

        public static String CalculateMD5Hash(String input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}
