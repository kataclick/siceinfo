﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Cemi.ExpoChiusura.Type.Domain;
using Cemi.ExpoChiusura.Data;

namespace Cemi.ExpoChiusura.Business
{
    public class ExpoManager
    {
        ServiceManager _serviceManager = new ServiceManager();
        int numRec = 0;

        public bool InsertPresenze(DateTime giorno)
        {
            List<ExpoChiusuraPresenza> presenze = _serviceManager.GetPresenze(giorno);

            Console.WriteLine(String.Format("-- Gestione del giorno {0} --",giorno));
            

            if (presenze.Count > 0)
            {
                using (SICEEntities ExpoChiusura = new SICEEntities())
                {
                    foreach (ExpoChiusuraPresenza presenza in presenze)
                    {
                        Console.WriteLine(String.Format("-- Inserimento Lavoratore {0} {1} (ID: {2}) --", presenza.Nome, presenza.Cognome, presenza.IdExpo));
                        Console.WriteLine("");
                        ExpoChiusura.ExpoChiusuraPresenze.AddObject(presenza);

                        /*
                        Log(String.Format("IdExpo: {0}",presenza.IdExpo));
                        Log(String.Format("Nome: {0}", presenza.Nome));
                        Log(String.Format("Cognome: {0}", presenza.Cognome));
                        Log(String.Format("CodiceFiscale: {0}", presenza.CodiceFiscale));
                        Log(String.Format("DataNascita: {0}", presenza.DataNascita));
                        Log(String.Format("Nazionalita: {0}", presenza.Nazionalita));
                        Log(String.Format("ComuneNascita: {0}", presenza.ComuneNascita));
                        Log(String.Format("Sesso: {0}", presenza.Sesso));
                        Log(String.Format("Qualifica: {0}", presenza.Qualifica));
                        Log(String.Format("Mansione: {0}", presenza.Mansione));
                        Log(String.Format("Categoria: {0}", presenza.Categoria));
                        Log(String.Format("AffidatarioRagioneSociale: {0}", presenza.AffidatarioRagioneSociale));
                        Log(String.Format("AffidatarioPartitaIva: {0}", presenza.AffidatarioPartitaIva));
                        Log(String.Format("EsecutoreRagioneSociale: {0}", presenza.EsecutoreRagioneSociale));
                        Log(String.Format("EsecutorePartitaIva: {0}", presenza.EsecutorePartitaIva));
                        Log(String.Format("DatoreLavoroRagioneSociale: {0}", presenza.DatoreLavoroRagioneSociale));
                        Log(String.Format("DatoreLavoroPartitaIva: {0}", presenza.DatoreLavoroPartitaIva));
                        Log(String.Format("CCNL: {0}", presenza.CCNL));
                        Log(String.Format("GiornoTimbratura: {0}", presenza.GiornoTimbratura));
                        Log(" ");
                        */
                    }

                    numRec = ExpoChiusura.SaveChanges();
                }
            }
            else
            {
                Console.WriteLine(String.Format("Nessuna presenza trovata per il giorno {0}",giorno.ToString()));
                Console.WriteLine("");
            }

            return numRec > 0;            
        }

        public void Log(String message)
        {
            //String msg = string.Empty;
            if (string.IsNullOrEmpty(message))
            {
                using (StreamWriter writer = new StreamWriter("LogLight.txt", true))
                {
                    writer.WriteLine();
                }
            }
            else
            {
                String msg = String.Format("{0} ---- {1}", DateTime.Now, message);
                using (StreamWriter writer = new StreamWriter("LogLight.txt", true))
                {
                    writer.WriteLine(msg);
                }
            }
        }
    }
}
