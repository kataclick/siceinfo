﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Cemi.SiceInfo.Utility
{
    public static class StringHelper
    {
        public static string NormalizzaCampoTesto(string campo)
        {
            if (!string.IsNullOrEmpty(campo))
            {
                // Il Regex.Replace serve per sostituire i caratteri non ASCII 
                // con la stringa vuota
                return Regex.Replace(campo, @"[^\u0000-\u007F]", string.Empty).Trim().ToUpper();
            }

            return string.Empty;
        }

        public static string NormalizzaCampoTestoSoloLettere(string campo)
        {
            var campoTmp = campo.Normalize(NormalizationForm.FormKD).Replace('´', '\'').Replace('`', '\'')
                .Replace("\u0301", "\'").Replace("\u0300", "\'");
            Regex rgx = new Regex("[^a-zA-Z' ]");
            campoTmp = rgx.Replace(campoTmp, string.Empty);
            campoTmp = NormalizzaCampoTesto(campoTmp);

            return campoTmp;
        }

        public static string StampaValoreMonetario(decimal? valore)
        {
            string ret = string.Empty;

            if (valore.HasValue)
            {
                ret = valore.Value.ToString("C");
            }

            return ret;
        }

        public static string StampaDataFormattataClassica(DateTime? data)
        {
            string ret = string.Empty;

            if (data.HasValue)
            {
                ret = data.Value.ToShortDateString();
            }

            return ret;
        }

        public static string StampaStringaExcel(string valore)
        {
            string result = string.Empty;

            if (valore != null)
            {
                result = valore.Replace(';', ' ').Replace('\n', ' ').Replace('\r', ' ');
            }

            return result;
        }
    }
}