﻿using System.Collections.Generic;
using AutoMapper;

namespace Cemi.SiceInfo.Utility
{
    public static class MyMapper<TSource, TDestination>
    {
        public static TDestination Map(TSource obj)
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<TSource, TDestination>());
            IMapper mapper = config.CreateMapper();

            TDestination ret = mapper.Map<TDestination>(obj);

            return ret;
        }

        public static TDestination MapToExisting(TSource obj, TDestination existingObj)
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<TSource, TDestination>());
            IMapper mapper = config.CreateMapper();

            mapper.Map(obj, existingObj);

            return existingObj;
        }

        public static IEnumerable<TDestination> MapEnumerable(IEnumerable<TSource> objEnumerable)
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<TSource, TDestination>());
            IMapper mapper = config.CreateMapper();

            IEnumerable<TDestination> ret = mapper.Map<IEnumerable<TDestination>>(objEnumerable);

            return ret;
        }
    }
}
