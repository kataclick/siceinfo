﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Cemi.SiceInfo.Utility
{
    public static class GenericStaticMethods
    {
        /// <summary>
        ///     Compresses the string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string CompressString(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        ///     Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string DecompressString(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static string ToCamelCase(this string text)
        {
            return $"{text.Substring(0, 1).ToLower()}{text.Substring(1)}";
        }

        public static byte[] CompressString2Byte(string text)
        {
            byte[] data = Encoding.UTF8.GetBytes(text);
            var stream = new MemoryStream();
            using (Stream ds = new GZipStream(stream, CompressionMode.Compress))
            {
                ds.Write(data, 0, data.Length);
            }

            byte[] compressed = stream.ToArray();

            return compressed;
        }

        public static string DecompressString2Byte(byte[] compressedText)
        {
            try
            {
                if (compressedText.Length == 0)
                {
                    return string.Empty;
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    int msgLength = BitConverter.ToInt32(compressedText, 0);
                    ms.Write(compressedText, 0, compressedText.Length - 0);

                    byte[] buffer = new byte[msgLength];

                    ms.Position = 0;
                    using (GZipStream zip = new GZipStream(ms, CompressionMode.Decompress))
                    {
                        zip.Read(buffer, 0, buffer.Length);
                    }

                    return Encoding.UTF8.GetString(buffer);
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetIndirizzoCompleto(string indirizzo, string cap, string comune, string provincia,
            string stato = "Italia")
        {
            return stato == "Italia"
                ? $"{indirizzo} {cap} {comune} {provincia}"
                : $"{indirizzo} {cap} {comune} {provincia} {stato}";
        }

        /// <summary>
        /// Zip a file stream
        /// </summary>
        /// <param name="originalFileStream"> MemoryStream with original file </param>
        /// <param name="fileName"> Name of the file in the ZIP container </param>
        /// <returns> Return byte array of zipped file </returns>
        public static byte[] GetZippedFile(MemoryStream originalFileStream, string fileName)
        {
            using (MemoryStream zipStream = new MemoryStream())
            {
                using (ZipArchive zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
                {
                    var zipEntry = zip.CreateEntry(fileName);
                    using (var writer = new StreamWriter(zipEntry.Open()))
                    {
                        originalFileStream.WriteTo(writer.BaseStream);
                    }
                    return zipStream.ToArray();
                }
            }
        }

        /// <summary>
        /// Zip multiple file streams
        /// </summary>
        /// <param name="dictionaryFiles">Key: filename - Value: memoryStream of file</param>
        /// <returns>Return byte array of zipped file</returns>
        public static byte[] GetZippedFiles(Dictionary<string, MemoryStream> dictionaryFiles)
        {
            using (MemoryStream zipStream = new MemoryStream())
            {
                using (ZipArchive zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
                {
                    foreach (KeyValuePair<string, MemoryStream> file in dictionaryFiles)
                    {
                        var zipEntry = zip.CreateEntry(file.Key);
                        using (var writer = new StreamWriter(zipEntry.Open()))
                        {
                            file.Value.WriteTo(writer.BaseStream);
                        }
                    }

                    return zipStream.ToArray();
                }
            }
        }
    }

}
