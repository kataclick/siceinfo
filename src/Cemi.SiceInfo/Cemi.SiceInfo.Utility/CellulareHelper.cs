﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cemi.SiceInfo.Utility
{
    public static class CellulareHelper
    {
        public static string PulisciNumeroCellulare(string numero)
        {
            string numeroPulito = numero.Trim();
            if (numeroPulito.StartsWith("00393"))
                numeroPulito = $"+{numeroPulito.Substring(2)}";
            if (!numeroPulito.StartsWith("+393"))
                numeroPulito = $"+39{numeroPulito}";
            return numeroPulito;
        }

        public static bool NumeroCellulareValido(string numero)
        {
            //numero = PulisciNumeroCellulare(numero);
            return Regex.IsMatch(numero, @"^([+]39)?3\d{8,15}$");
        }
    }
}
