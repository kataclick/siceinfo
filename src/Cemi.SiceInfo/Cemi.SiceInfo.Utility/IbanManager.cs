﻿using System.Text;

namespace Cemi.SiceInfo.Utility
{
    public class IbanManager
    {
        public static bool VerificaCodiceIban(string iban)
        {
            string ibanTmp = iban.Substring(4, iban.Length - 4) + iban.Substring(0, 4);
            StringBuilder sb = new StringBuilder();
            foreach (char c in ibanTmp)
            {
                int v;
                if (char.IsLetter(c))
                {
                    v = c - 'A' + 10;
                }
                else
                {
                    v = c - '0';
                }
                sb.Append(v);
            }
            string checkSumString = sb.ToString();
            int checksum = int.Parse(checkSumString.Substring(0, 1));
            for (int i = 1; i < checkSumString.Length; i++)
            {
                int v = int.Parse(checkSumString.Substring(i, 1));
                checksum *= 10;
                checksum += v;
                checksum %= 97;
            }

            return checksum == 1;
        }
    }
}