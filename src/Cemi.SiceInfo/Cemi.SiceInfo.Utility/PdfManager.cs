﻿using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace Cemi.SiceInfo.Utility
{
    public class PdfManager
    {
        public Stream MergePdf(Stream mainPdf, Stream childPdf)
        {
            MemoryStream output = new MemoryStream();
            PdfDocument outputDocument = PdfReader.Open(mainPdf, PdfDocumentOpenMode.Modify);
            // Open the document to import pages from it.
            if (childPdf != null)
            {
                PdfDocument inputDocument = PdfReader.Open(childPdf, PdfDocumentOpenMode.Import);
                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            outputDocument.Save(output, false);
            byte[] buffer = new byte[output.Length];
            output.Seek(0, SeekOrigin.Begin);
            output.Flush();
            output.Read(buffer, 0, (int) output.Length);
            return output;
        }

        public void MergePdf(Stream mainPdf, Stream childPdf, string pathFileDest)
        {
            PdfDocument outputDocument = PdfReader.Open(mainPdf, PdfDocumentOpenMode.Modify);
            // Open the document to import pages from it.
            if (childPdf != null)
            {
                PdfDocument inputDocument = PdfReader.Open(childPdf, PdfDocumentOpenMode.Import);
                // Iterate pages
                int count = inputDocument.PageCount;
                for (int idx = 0; idx < count; idx++)
                {
                    // Get the page from the external document...
                    PdfPage page = inputDocument.Pages[idx];
                    // ...and add it to the output document.
                    outputDocument.AddPage(page);
                }
            }

            outputDocument.Save(pathFileDest);
        }

        public Stream MergePdf(string mainPdf, string childPdf)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            return MergePdf(main, child);
        }

        public void MergePdf(string mainPdf, string childPdf, string pathFileDest)
        {
            Stream main = GetFileStream(mainPdf);
            Stream child = GetFileStream(childPdf);
            MergePdf(main, child, pathFileDest);
        }

        public Stream GetPdfPage(Stream pdf, int pageIndex)
        {
            PdfDocument outputDocument = new PdfDocument();
            PdfDocument inputDocument = PdfReader.Open(pdf, PdfDocumentOpenMode.Import);
            if (inputDocument.PageCount < pageIndex || pageIndex < 0)
                return null;

            // Get the page from the external document...
            PdfPage page = inputDocument.Pages[pageIndex];
            // ...and add it to the output document.
            outputDocument.AddPage(page);

            MemoryStream output = new MemoryStream();
            outputDocument.Save(output, false);
            //byte[] buffer = new byte[output.Length];
            output.Seek(0, SeekOrigin.Begin);
            //output.Flush();
            //output.Read(buffer, 0, (int) output.Length);
            return output;
        }

        public Stream GetPdfPage(string pdf, int pageIndex)
        {
            Stream pdfStream = GetFileStream(pdf);
            return GetPdfPage(pdfStream, pageIndex);
        }

        public Stream GetFileStream(string filename)
        {
            MemoryStream ms = new MemoryStream();
            FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] bytes = new byte[file.Length];
            file.Read(bytes, 0, (int) file.Length);
            ms.Write(bytes, 0, (int) file.Length);
            file.Close();
            return ms;
        }

        public Stream GetByteStream(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            return ms;
        }
    }
}