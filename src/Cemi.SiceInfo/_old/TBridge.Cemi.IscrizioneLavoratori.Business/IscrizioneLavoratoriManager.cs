using System;
using System.Collections.Generic;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Cpt;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.IscrizioneLavoratori.Data;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;

namespace TBridge.Cemi.IscrizioneLavoratori.Business
{
    public class IscrizioneLavoratoriManager
    {
        private readonly IscrizioneLavoratoriDataAccess dataAccess = new IscrizioneLavoratoriDataAccess();

        public DichiarazioneCollection GetDichiarazioni(DichiarazioneFilter filtro)
        {
            return dataAccess.GetDichiarazioni(filtro);
        }

        public LavoratoreCollection GetLavoratori(LavoratoreFilter filtro, int idImpresa)
        {
            return dataAccess.GetLavoratori(filtro, idImpresa);
        }

        public Lavoratore GetLavoratore(int idLavoratore)
        {
            return dataAccess.GetLavoratore(idLavoratore);
        }

        public Impresa GetImpresa(int idImpresa)
        {
            return dataAccess.GetImpresa(idImpresa);
        }

        public Impresa GetImpresa(string codiceFiscalePartitaIva)
        {
            return dataAccess.GetImpresa(codiceFiscalePartitaIva);
        }

        public bool InsertDichiarazione(Dichiarazione dichiarazione)
        {
            return dataAccess.InsertDichiarazione(dichiarazione);
        }

        public RapportoDiLavoro GetRapportoDiLavoro(int idLavoratore, int idImpresa)
        {
            return dataAccess.GetRapportoDiLavoro(idLavoratore, idImpresa);
        }

        public RapportoDiLavoroCollection GetRapportiDiLavoroAperti(int idLavoratore,
            DateTime dataInizioRapportoDiLavoro)
        {
            return dataAccess.GetRapportiDiLavoroAperti(idLavoratore, dataInizioRapportoDiLavoro);
        }

        public RapportoDiLavoroCollection GetDichiarazioniAperte(int idDichiarazioneCorrente, string codiceFiscale,
            DateTime dataInizioRapportoDiLavoro)
        {
            return dataAccess.GetDichiarazioniAperte(idDichiarazioneCorrente, codiceFiscale,
                dataInizioRapportoDiLavoro);
        }

        public Dichiarazione GetDichiarazione(int idDichiarazione)
        {
            return dataAccess.GetDichiarazione(idDichiarazione);
        }

        public LavoratoreCollection GetLavoratoriConStessoCodiceFiscale(string codiceFiscale)
        {
            return dataAccess.GetLavoratoriConStessoCodiceFiscale(codiceFiscale);
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagrafici(
            string cognome,
            string nome,
            char sesso,
            DateTime dataNascita,
            string luogoNascita)
        {
            return dataAccess.GetLavoratoriConStessiDatiAnagrafici(
                cognome,
                nome,
                sesso,
                dataNascita,
                luogoNascita);
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
            string cognome,
            string nome,
            char sesso,
            DateTime dataNascita,
            string luogoNascita,
            string codiceFiscale)
        {
            return dataAccess.GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
                cognome,
                nome,
                sesso,
                dataNascita,
                luogoNascita,
                codiceFiscale);
        }

        public void CambiaStatoDichiarazione(int idDichiarazione, TipoStatoGestionePratica vecchioStato,
            TipoStatoGestionePratica nuovoStato, int idUtenteCambioStato)
        {
            dataAccess.CambiaStatoDichiarazione(idDichiarazione, vecchioStato, nuovoStato, idUtenteCambioStato);
        }

        public void CambiaLavoratoreSelezionato(int idDichiarazione, int? idLavoratore, bool nuovoLavoratore)
        {
            dataAccess.CambiaLavoratoreSelezionato(idDichiarazione, idLavoratore, nuovoLavoratore);
        }

        public void ControlloSdoppioneEffettuato(int idDichiarazione, bool controlloEffettuato)
        {
            dataAccess.ControlloSdoppioneEffettuato(idDichiarazione, controlloEffettuato);
        }

        public void CambiaIndirizzoDichiarato(int idDichiarazione, bool mantieni)
        {
            dataAccess.CambiaIndirizzoDichiarato(idDichiarazione, mantieni);
        }

        public void CambiaSelezioneRapportoCessazione(int idDichiarazione, bool mantieni, int? idLavoratore,
            int? idImpresa, DateTime? dataFineValiditaRapporto)
        {
            dataAccess.CambiaSelezioneRapportoCessazione(idDichiarazione, mantieni, idLavoratore, idImpresa,
                dataFineValiditaRapporto);
        }

        public void CambiaIBANDichiarato(int idDichiarazione, bool mantieni)
        {
            dataAccess.CambiaIBANDichiarato(idDichiarazione, mantieni);
        }

        public DichiarazioneCollection GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(int idImpresa,
            string codiceFiscale,
            DateTime?
                dataInizioValiditaRapporto,
            DateTime? dataCessazione)
        {
            return dataAccess.GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(idImpresa, codiceFiscale,
                dataInizioValiditaRapporto,
                dataCessazione);
        }


        public void CambiaPosizioneValida(int idDichirazione, bool? posizioneValida)
        {
            dataAccess.CambiaPosizioneValida(idDichirazione, posizioneValida);
        }

        #region Costanti

        public const string URLSEMAFOROGIALLO = "~/CeServizi/images/semaforoGiallo.png";
        public const string URLSEMAFOROROSSO = "~/CeServizi/images/semaforoRosso.png";
        public const string URLSEMAFOROVERDE = "~/CeServizi/images/semaforoVerde.png";

        #endregion

        #region Metodi per i controlli

        public string LavoratoreSelezionatoONuovo(Dichiarazione dichiarazione)
        {
            string res = null;

            if (!dichiarazione.NuovoLavoratore
                && !dichiarazione.IdLavoratoreSelezionato.HasValue)
            {
                res = URLSEMAFOROROSSO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public string AnagraficaUgualeSenzaOmocodia(Dichiarazione dichiarazione,
            LavoratoreCollection lavoratoriStessiDatiAnagrafici)
        {
            string res = null;

            if (lavoratoriStessiDatiAnagrafici.Count >= 1
                &&
                ConfrontaAnagraficaUgualeSenzaOmocodia(dichiarazione, lavoratoriStessiDatiAnagrafici))
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        private bool ConfrontaAnagraficaUgualeSenzaOmocodia(Dichiarazione dichiarazione,
            LavoratoreCollection lavoratori)
        {
            bool res = false;
            Lavoratore lavDichiarato = dichiarazione.Lavoratore;

            if (lavDichiarato.CodiceFiscale.Length == 16
            ) //Non eseguo il controllo per cf provvisori (11 caratteri es:23372119000)
            {
                foreach (Lavoratore lavTrovato in lavoratori)
                {
                    if (lavDichiarato.CodiceFiscale.ToUpper() != lavTrovato.CodiceFiscale)
                    {
                        string lavDichiaratoLuogoNascita = lavDichiarato.CodiceFiscale.Substring(12, 3);
                        string lavTrovatoLuogoNascita = lavTrovato.CodiceFiscale.Substring(12, 3);

                        int temp1, temp2;
                        if (int.TryParse(lavDichiaratoLuogoNascita, out temp1)
                            && int.TryParse(lavTrovatoLuogoNascita, out temp2))
                        {
                            res = true;
                        }
                    }
                }
            }

            return res;
        }

        public string AnagraficaUgualeConOmocodia(Dichiarazione dichiarazione,
            LavoratoreCollection lavoratoriStessiDatiAnagrafici)
        {
            string res = null;

            if (lavoratoriStessiDatiAnagrafici.Count >= 1
                &&
                ConfrontaAnagraficaUgualeConOmocodia(dichiarazione, lavoratoriStessiDatiAnagrafici))
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public string EmissioneTesseraSanitaria(Dichiarazione dichiarazione)
        {
            string res = null;
            if (CodiceFiscaleManager.GetDataSpedizioneTesseraSanitariaByCf(dichiarazione.Lavoratore.CodiceFiscale) !=
                null)
            {
                res = URLSEMAFOROVERDE;
            }
            else
            {
                res = URLSEMAFOROGIALLO;
            }

            return res;
        }

        private bool ConfrontaAnagraficaUgualeConOmocodia(Dichiarazione dichiarazione, LavoratoreCollection lavoratori)
        {
            bool res = false;

            Lavoratore lavDichiarato = dichiarazione.Lavoratore;
            if (lavDichiarato.CodiceFiscale.Length == 16
            ) //Non eseguo il controllo per cf provvisori (11 caratteri es:23372119000)
            {
                foreach (Lavoratore lavTrovato in lavoratori)
                {
                    if (lavDichiarato.CodiceFiscale.ToUpper() != lavTrovato.CodiceFiscale)
                    {
                        string lavDichiaratoLuogoNascita = lavDichiarato.CodiceFiscale.Substring(12, 3);
                        string lavTrovatoLuogoNascita = lavTrovato.CodiceFiscale.Substring(12, 3);

                        int temp1, temp2;
                        if (!int.TryParse(lavDichiaratoLuogoNascita, out temp1)
                            || !int.TryParse(lavTrovatoLuogoNascita, out temp2))
                        {
                            res = true;
                        }
                    }
                }
            }

            return res;
        }

        public string AnagraficaDoppione(Dichiarazione dichiarazione,
            LavoratoreCollection lavoratoriStessiDatiAnagraficiECodiceFiscale)
        {
            string res = null;

            if (lavoratoriStessiDatiAnagraficiECodiceFiscale.Count > 1)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public string ControlloRapportoCessazione(Dichiarazione dichiarazione)
        {
            string res = null;

            if (!dichiarazione.RapportoAssociatoCessazione)
            {
                res = URLSEMAFOROROSSO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            if (dichiarazione.NuovoLavoratore)
                res = URLSEMAFOROVERDE;

            return res;
        }

        public bool ControlloContrattoImpresaTipoContratto(Dichiarazione dichiarazione)
        {
            bool result = false;
            if (dichiarazione.RapportoDiLavoro != null && dichiarazione.RapportoDiLavoro.Contratto != null
                && dichiarazione.Impresa != null && dichiarazione.Impresa.Contratto != null
                && string.Equals(dichiarazione.RapportoDiLavoro.Contratto.IdContratto,
                    dichiarazione.Impresa.Contratto.IdContratto))
            {
                result = true;
            }

            return result;
        }

        public string AnagraficaSdoppione(Dichiarazione dichiarazione, LavoratoreCollection lavoratoriCodFisc,
            LavoratoreCollection lavoratoriAnag)
        {
            string res = null;

            if (!dichiarazione.ControlloSdoppione
                && ConfrontaAnagraficaSdoppione(dichiarazione, lavoratoriCodFisc, lavoratoriAnag))
            {
                res = URLSEMAFOROROSSO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        private bool ConfrontaAnagraficaSdoppione(Dichiarazione dichiarazione, LavoratoreCollection lavoratoriCodFisc,
            LavoratoreCollection lavoratoriAnag)
        {
            bool res = false;

            List<int> lavoratoriTrovati = new List<int>();
            foreach (Lavoratore lav in lavoratoriCodFisc)
            {
                bool inserisci = true;

                for (int i = 0; i < lavoratoriTrovati.Count; i++)
                {
                    if (lav.IdLavoratore.Value == lavoratoriTrovati[i])
                    {
                        inserisci = false;
                        break;
                    }
                }

                if (inserisci)
                {
                    lavoratoriTrovati.Add(lav.IdLavoratore.Value);
                }
            }
            foreach (Lavoratore lav in lavoratoriAnag)
            {
                bool inserisci = true;

                for (int i = 0; i < lavoratoriTrovati.Count; i++)
                {
                    if (lav.IdLavoratore.Value == lavoratoriTrovati[i])
                    {
                        inserisci = false;
                        break;
                    }
                }

                if (inserisci)
                {
                    lavoratoriTrovati.Add(lav.IdLavoratore.Value);
                }
            }

            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();
            foreach (int idLav in lavoratoriTrovati)
            {
                rapportiAperti.AddRange(
                    GetRapportiDiLavoroAperti(idLav, dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto.Value));
            }
            rapportiAperti.AddRange(GetDichiarazioniAperte(dichiarazione.IdDichiarazione.Value,
                dichiarazione.Lavoratore.CodiceFiscale,
                dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto.Value));

            if (rapportiAperti.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public string AnagraficaDubbia(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            string res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (!dichiarazione.NuovoLavoratore
                    && lavoratore != null
                    && ConfrontaAnagraficaDubbia(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROGIALLO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaAnagraficaDubbia(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            bool res = false;

            Lavoratore lavDichiarato = dichiarazione.Lavoratore;
            if (lavDichiarato.Cognome.ToUpper() != lavoratore.Cognome.ToUpper()
                || lavDichiarato.Nome.ToUpper() != lavoratore.Nome.ToUpper()
                || lavDichiarato.Sesso != lavoratore.Sesso
                || lavDichiarato.DataNascita != lavoratore.DataNascita
                || lavDichiarato.ComuneNascita != lavoratore.ComuneNascita)
            {
                res = true;
            }

            return res;
        }

        public string IbanDiverso(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            string res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (lavoratore != null
                    && !dichiarazione.MantieniIbanAnagrafica.HasValue
                    && ConfrontaIban(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROROSSO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaIban(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(dichiarazione.Lavoratore.IBAN)
                && !string.IsNullOrEmpty(lavoratore.IBAN)
                && dichiarazione.Lavoratore.IBAN != lavoratore.IBAN)
            {
                res = true;
            }

            return res;
        }

        public string InformazioniPrepagata(Dichiarazione dichiarazione)
        {
            string res = null;

            if (dichiarazione.Lavoratore.RichiestaInfoCartaPrepagata)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                res = URLSEMAFOROVERDE;
            }

            return res;
        }

        public string IndirizzoDiverso(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            string res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (lavoratore != null
                    && !dichiarazione.MantieniIndirizzoAnagrafica.HasValue
                    && ConfrontaIndirizzo(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROROSSO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaIndirizzo(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(dichiarazione.Lavoratore.Indirizzo.Indirizzo1)
                && !string.IsNullOrEmpty(lavoratore.Indirizzo.Indirizzo1)
                && (dichiarazione.Lavoratore.Indirizzo.Indirizzo1 != lavoratore.Indirizzo.Indirizzo1
                    || dichiarazione.Lavoratore.Indirizzo.Provincia != lavoratore.Indirizzo.Provincia
                    || dichiarazione.Lavoratore.Indirizzo.Comune != lavoratore.Indirizzo.Comune
                    || dichiarazione.Lavoratore.Indirizzo.Cap != lavoratore.Indirizzo.Cap))
            {
                res = true;
            }

            return res;
        }

        public string EmailDiversa(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            string res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (!dichiarazione.NuovoLavoratore
                    && lavoratore != null
                    && ConfrontaEmail(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROGIALLO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaEmail(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(lavoratore.Email)
                && lavoratore.Email != dichiarazione.Lavoratore.Email)
            {
                res = true;
            }

            return res;
        }

        public string CellulareDiverso(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            string res = null;

            if (!dichiarazione.NuovoLavoratore
                &&
                lavoratore == null)
            {
                res = URLSEMAFOROGIALLO;
            }
            else
            {
                if (!dichiarazione.NuovoLavoratore
                    && lavoratore != null
                    && ConfrontaCellulare(dichiarazione, lavoratore))
                {
                    res = URLSEMAFOROGIALLO;
                }
                else
                {
                    res = URLSEMAFOROVERDE;
                }
            }

            return res;
        }

        private bool ConfrontaCellulare(Dichiarazione dichiarazione, Lavoratore lavoratore)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(lavoratore.NumeroTelefonoCellulare)
                && lavoratore.NumeroTelefonoCellulare != dichiarazione.Lavoratore.NumeroTelefonoCellulare)
            {
                res = true;
            }

            return res;
        }

        public string CantiereInNotifica(Dichiarazione dichiarazione)
        {
            string res = null;

            if (ControlloPresenzaInNotifiche(dichiarazione.Cantiere))
            {
                res = URLSEMAFOROVERDE;
            }
            else
            {
                res = URLSEMAFOROGIALLO;
            }

            return res;
        }

        private bool ControlloPresenzaInNotifiche(Indirizzo indirizzo)
        {
            bool ret = true;

            CptBusiness cptBiz = new CptBusiness();
            NotificaFilter filtro = new NotificaFilter();
            filtro.Indirizzo = indirizzo.Indirizzo1;
            filtro.IndirizzoComune = indirizzo.ComuneDescrizione;
            NotificaCollection notifiche =
                cptBiz.RicercaNotifiche(filtro);
            if (notifiche == null || notifiche.Count == 0)
            {
                ret = false;
            }

            return ret;
        }

        #endregion
    }
}