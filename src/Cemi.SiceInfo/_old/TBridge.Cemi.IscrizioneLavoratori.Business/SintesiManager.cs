using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TBridge.Cemi.Business.SintesiServiceReference;
using TBridge.Cemi.IscrizioneLavoratori.Data;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi.Original;
using Impresa = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;

namespace TBridge.Cemi.IscrizioneLavoratori.Business
{
    public class SintesiManager
    {
        private readonly SintesiDataAccess _dataAccess = new SintesiDataAccess();

        /*
        public RapportoLavoro GetRapportoLavoro(string idComunicazione)
        {
            RapportoLavoro rapportoLavoro = null;

            try
            {
                string xmlRisposta = SintesiWsGetXmlRisposta(idComunicazione);

                //GetXmlMin proxy = new GetXmlMin
                //                      {
                //                          Credentials = new NetworkCredential(username, password)
                //                      };

                //string xmlRisposta = proxy.getXmlMinFromIdAuth(idComunicazione, username, password);

                StringReader source = new StringReader(xmlRisposta);
                XmlSerializer serializer = new XmlSerializer(typeof(RapportoLavoro));
                rapportoLavoro = (RapportoLavoro) serializer.Deserialize(source);
            }
            catch (Exception)
            {
                //
            }

            return rapportoLavoro;
        }
        */

        public SintesiServiceResult GetRapportoLavoro(string idComunicazione, out RapportoLavoro rapportoLavoro)
        {
            string xmlRisposta;

            rapportoLavoro = null;

            if (string.IsNullOrEmpty(idComunicazione))
            {
                return new SintesiServiceResult {Error = SintesiServiceErrors.InvalidId, Message = "idComunicazione vuoto"};
            }

            try
            {
                xmlRisposta = SintesiWsGetXmlRisposta(idComunicazione);
            }
            catch (Exception ex)
            {
                return new SintesiServiceResult {Error = SintesiServiceErrors.ServiceCallFailed, Message = ex.Message};
            }

            if (IsXml(xmlRisposta))
            {
                SintesiServiceResult result = new SintesiServiceResult();
                try
                {
                    StringReader source = new StringReader(xmlRisposta);
                    XmlSerializer serializer = new XmlSerializer(typeof(RapportoLavoro));
                    rapportoLavoro = (RapportoLavoro) serializer.Deserialize(source);

                    result.Error = SintesiServiceErrors.NoError;
                    result.Message = xmlRisposta;
                }
                catch (Exception ex)
                {
                    result.Error = SintesiServiceErrors.InvalidResponse;
                    result.Message = ex.Message;
                }

                return result;
            }

            return new SintesiServiceResult {Error = SintesiServiceErrors.ServiceErrorMessage, Message = xmlRisposta};
        }

        private static string SintesiWsGetXmlRisposta(string idComunicazione)
        {
            string xmlRisposta;
            string password;
            string username;
            string wsEndpoint;
            string identificativoProvincia = idComunicazione.Substring(0, 5);
            switch (identificativoProvincia)
            {
                case "10015":
                    //Milano
                    wsEndpoint = "getXmlMinSoapMi";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordMilano"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameMilano"];
                    break;
                case "10108":
                    //Monza-Brianza
                    wsEndpoint = "getXmlMinSoapMb";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordMonzaBrianza"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameMonzaBrianza"];
                    break;
                case "10098":
                    //Lodi
                    wsEndpoint = "getXmlMinSoapLo";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordLodi"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameLodi"];
                    break;
                default:
                    //Milano
                    wsEndpoint = "getXmlMinSoapMi";
                    password = ConfigurationManager.AppSettings["WsSintesiPasswordMilano"];
                    username = ConfigurationManager.AppSettings["WsSintesiUsernameMilano"];
                    break;
            }

            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            using (getXmlMinSoapClient client = new getXmlMinSoapClient(wsEndpoint))
            {
                xmlRisposta = client.getXmlMinFromCodAuth(idComunicazione, username, password);
            }


            return xmlRisposta;
        }

        public void ImpostaTipoContratto(RapportoDiLavoro rapportoLavoro, TipoContratto tipoContratto, int idImpresa)
        {
            IscrizioneLavoratoriDataAccess iscrizioneLavoratoriDataAccess = new IscrizioneLavoratoriDataAccess();
            Impresa impresa = iscrizioneLavoratoriDataAccess.GetImpresa(idImpresa);

            if (tipoContratto.IdContratto != impresa.Contratto.IdContratto)
            {
                rapportoLavoro.Contratto = impresa.Contratto;
                rapportoLavoro.ContrattoOriginaleSintesi = tipoContratto;
            }
            else
            {
                rapportoLavoro.Contratto = tipoContratto;
            }
        }

        public TipoFineRapporto GetTipoFineRapporto(string codiceCessazione)
        {
            return _dataAccess.GetTipoFineRapporto(codiceCessazione);
        }

        public Lavoratore GetLavoratore(string codiceFiscale, int idImpresa)
        {
            return _dataAccess.GetLavoratore(codiceFiscale, idImpresa);
        }

        public RapportoDiLavoro GetRapportoLavoro(int? idLavoratore, int idImpresa)
        {
            return _dataAccess.GetRapportoLavoro(idLavoratore, idImpresa);
        }

        public int SalvaDatiComunicazione(Guid? guid, string codiceComunicazione, string codiceComunicazionePrec,
            int idDichiarazione)
        {
            return _dataAccess.SalvaDatiComunicazione(guid, codiceComunicazione, codiceComunicazionePrec,
                idDichiarazione);
        }

        public bool DichiarazioneExistByIdComunicazioneSintesi(string idComunicazione)
        {
            return _dataAccess.GetIdDichiarazioneByIdComunicazoneSintesi(idComunicazione).HasValue;
        }

        public bool IsImpresaCompetenzaConsulente(int idConsulente, string impresaPartitaIvaCodiceFiscale)
        {
            return _dataAccess.IsImpresaCompetenzaConsulente(idConsulente, impresaPartitaIvaCodiceFiscale);
        }

        public bool IsDataAssunzioneValida(RapportoLavoro rapportoLavoro)
        {
            bool result = true;
            if (rapportoLavoro.Item is RapportoLavoroInizioRapporto assunzione)
            {
                DateTime dataLimiteTmp = assunzione.dataInizio.AddMonths(1);
                //giorno 5 del mese successivo 
                DateTime dataLimite = new DateTime(dataLimiteTmp.Year, dataLimiteTmp.Month, 5);
                if (DateTime.Now.Date > dataLimite)
                {
                    result = false;
                }
            }
            return result;
        }

        private bool IsXml(string message)
        {
            if (!string.IsNullOrEmpty(message) && message[0] == '<')
            {
                try
                {
                    XElement.Parse(message);
                    return true;
                }
                catch (XmlException)
                {
                    return false;
                }
            }

            return false;
        }
    }
}