using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Data.AnagraficaComune;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Exceptions;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;
using TBridge.Cemi.Type.Entities;
using Impresa = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using Indirizzo = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo;

namespace TBridge.Cemi.IscrizioneLavoratori.Data
{
    public class IscrizioneLavoratoriDataAccess
    {
        public IscrizioneLavoratoriDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public DichiarazioneCollection GetDichiarazioni(DichiarazioneFilter filtro)
        {
            DichiarazioneCollection dichiarazioni = new DichiarazioneCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniRicercaSelect"))
            {
                if (filtro.DataInvioDa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInvioDa", DbType.DateTime, filtro.DataInvioDa.Value);
                }
                if (filtro.DataInvioA.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInvioA", DbType.DateTime, filtro.DataInvioA.Value);
                }

                if (filtro.TipoAttivita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@attivita", DbType.Int32, filtro.TipoAttivita.Value);
                }

                if (!string.IsNullOrEmpty(filtro.CodiceImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceImpresa", DbType.Int32, filtro.CodiceImpresa);
                }
                if (!string.IsNullOrEmpty(filtro.PartitaIVA))
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, filtro.PartitaIVA);
                }
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (filtro.StatoGestionePratica.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, filtro.StatoGestionePratica.Value);
                }
                if (filtro.IdDichiarazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32,
                        filtro.IdDichiarazione.Value);
                }
                if (!string.IsNullOrEmpty(filtro.IdNazionalita))
                {
                    DatabaseCemi.AddInParameter(comando, "@idNazionalita", DbType.String, filtro.IdNazionalita);
                }

                DatabaseCemi.AddInParameter(comando, "@cfDiversoDatiUguali", DbType.Boolean,
                    filtro.DuplicazioneCfDiversoDatiUguali);
                DatabaseCemi.AddInParameter(comando, "@cfUgualeDatiDiversi", DbType.Boolean,
                    filtro.DuplicazioneCfUgualeDatiDiversi);
                DatabaseCemi.AddInParameter(comando, "@cfUgualeDatiUguali", DbType.Boolean,
                    filtro.DuplicazioneCfUgualeDatiUguali);
                DatabaseCemi.AddInParameter(comando, "@cfUgualeRapportoApeto", DbType.Boolean,
                    filtro.DuplicazioneCfUgualeRapportoAperto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdIscrizioneLavoratoriDichiarazione =
                        reader.GetOrdinal("idIscrizioneLavoratoriDichiarazione");
                    int indiceDataInserimentoRecord = reader.GetOrdinal("dataInserimentoRecord");
                    int indiceAttivita = reader.GetOrdinal("attivita");
                    int indiceIdStato = reader.GetOrdinal("idStato");
                    int indiceIdUtente = reader.GetOrdinal("idUtente");
                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("partitaIVA");
                    int indiceCodiceFiscaleImpresa = reader.GetOrdinal("codiceFiscaleImpresa");
                    int indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceCivico = reader.GetOrdinal("civico");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("Provincia");
                    int indiceCap = reader.GetOrdinal("CAP");
                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIdLavoratoreCe = reader.GetOrdinal("idLavoratoreCE");
                    int indiceCognomeCe = reader.GetOrdinal("cognomeCE");
                    int indiceNomeCe = reader.GetOrdinal("nomeCE");
                    int indiceDataNascitaCe = reader.GetOrdinal("dataNascitaCE");
                    int indiceCodiceFiscaleCe = reader.GetOrdinal("codiceFiscaleCE");
                    int indiceIdDichiarazionePrecedente = reader.GetOrdinal("idDichiarazionePrecedente");
                    int indiceIdDichiarazioneSuccessiva = reader.GetOrdinal("idDichiarazioneSuccessiva");
                    int indiceIdComunicazionePrecedente = reader.GetOrdinal("idComunicazionePrecedente");
                    int indiceIdComunicazioneSuccessiva = reader.GetOrdinal("idComunicazioneSuccessiva");

                    #endregion

                    while (reader.Read())
                    {
                        Dichiarazione dichiarazione = new Dichiarazione();
                        dichiarazioni.Add(dichiarazione);

                        dichiarazione.IdDichiarazione = reader.GetInt32(indiceIdIscrizioneLavoratoriDichiarazione);
                        //if (!reader.IsDBNull(indiceAttivita))
                        //{
                        //    dichiarazione.IdDichiarazione = reader.GetInt16(indiceIdAttivita);
                        //}
                        if (!reader.IsDBNull(indiceIdUtente))
                        {
                            dichiarazione.IdUtente = reader.GetInt32(indiceIdUtente);
                        }
                        if (!reader.IsDBNull(indiceDataInserimentoRecord))
                        {
                            dichiarazione.DataInvio = reader.GetDateTime(indiceDataInserimentoRecord);
                        }
                        if (!reader.IsDBNull(indiceAttivita))
                        {
                            dichiarazione.Attivita = (TipoAttivita) reader.GetInt16(indiceAttivita);
                        }
                        if (!reader.IsDBNull(indiceIdStato))
                        {
                            dichiarazione.Stato = (TipoStatoGestionePratica) reader.GetInt32(indiceIdStato);
                        }
                        dichiarazione.Impresa = new Impresa();
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            dichiarazione.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            dichiarazione.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        }
                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            dichiarazione.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            dichiarazione.Impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            dichiarazione.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceIdCantiere))
                        {
                            dichiarazione.IdCantiere = reader.GetInt32(indiceIdCantiere);
                        }
                        dichiarazione.Cantiere = new Indirizzo();
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            dichiarazione.Cantiere.Indirizzo1 = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceCivico))
                        {
                            dichiarazione.Cantiere.Civico = reader.GetString(indiceCivico);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            dichiarazione.Cantiere.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            dichiarazione.Cantiere.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            dichiarazione.Cantiere.Cap = reader.GetString(indiceCap);
                        }
                        dichiarazione.Lavoratore = new Lavoratore();
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIdLavoratoreCe))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratoreCe);
                        }
                        if (!reader.IsDBNull(indiceCognomeCe))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognomeCe);
                        }
                        if (!reader.IsDBNull(indiceNomeCe))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNomeCe);
                        }
                        if (!reader.IsDBNull(indiceDataNascitaCe))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascitaCe);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleCe))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscaleCe);
                        }
                        if (!reader.IsDBNull(indiceIdDichiarazionePrecedente))
                        {
                            dichiarazione.IdDichiarazionePrecedente = reader.GetInt32(indiceIdDichiarazionePrecedente);
                        }
                        if (!reader.IsDBNull(indiceIdDichiarazioneSuccessiva))
                        {
                            dichiarazione.IdDichiarazioneSuccessiva = reader.GetInt32(indiceIdDichiarazioneSuccessiva);
                        }
                        if (!reader.IsDBNull(indiceIdComunicazionePrecedente))
                        {
                            dichiarazione.IdComunicazionePrecedente = reader.GetString(indiceIdComunicazionePrecedente);
                        }
                        if (!reader.IsDBNull(indiceIdComunicazioneSuccessiva))
                        {
                            dichiarazione.IdComunicazioneSuccessiva = reader.GetString(indiceIdComunicazioneSuccessiva);
                        }
                    }
                }
            }


            return dichiarazioni;
        }

        public LavoratoreCollection GetLavoratori(LavoratoreFilter filtro, int idImpresa)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriRicercaSelect"))
            {
                if (filtro.Codice.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.Codice.Value);
                }
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!string.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public Lavoratore GetLavoratore(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriSelect")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIban = reader.GetOrdinal("iban");
                    int indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    int indiceIndirizzoDataInivioValidita = reader.GetOrdinal("dataInizioValiditaIndirizzo");

                    #endregion

                    if (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!Convert.IsDBNull(reader["sesso"]))
                        {
                            lavoratore.Sesso = ((string) reader["sesso"])[0];
                        }
                        else
                        {
                            lavoratore.Sesso = 'M';
                        }
                        if (!Convert.IsDBNull(reader["provinciaNascita"]))
                        {
                            lavoratore.ProvinciaNascita = (string) reader["provinciaNascita"];
                        }
                        if (!Convert.IsDBNull(reader["luogoNascita"]))
                        {
                            lavoratore.ComuneNascita = (string) reader["luogoNascita"];
                        }
                        if (!Convert.IsDBNull(reader["idStatoCivile"]))
                        {
                            lavoratore.IdStatoCivile = (string) reader["idStatoCivile"];
                        }
                        if (!Convert.IsDBNull(reader["idNazionalita"]))
                        {
                            lavoratore.NazioneNascita = (string) reader["idNazionalita"];
                        }

                        lavoratore.Indirizzo = new Indirizzo();
                        if (!Convert.IsDBNull(reader["indirizzoDenominazione"]))
                        {
                            lavoratore.Indirizzo.Indirizzo1 = (string) reader["indirizzoDenominazione"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoProvincia"]))
                        {
                            lavoratore.Indirizzo.Provincia = (string) reader["indirizzoProvincia"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoComuneCodiceCatastale"]))
                        {
                            lavoratore.Indirizzo.Comune = (string) reader["indirizzoComuneCodiceCatastale"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoCap"]))
                        {
                            lavoratore.Indirizzo.Cap = (string) reader["indirizzoCap"];
                        }
                        if (!Convert.IsDBNull(reader["civicoIndirizzo"]))
                        {
                            lavoratore.Indirizzo.Civico = (string) reader["civicoIndirizzo"];
                        }

                        lavoratore.Indirizzo.Comune = reader.IsDBNull(indiceIndirizzoComune)
                            ? null
                            : reader.GetString(indiceIndirizzoComune);
                        lavoratore.Indirizzo.ComuneDescrizione = lavoratore.Indirizzo.Comune;
                        lavoratore.Indirizzo.DataInizioValiditÓ = reader.IsDBNull(indiceIndirizzoDataInivioValidita)
                            ? (DateTime?) null
                            : reader.GetDateTime(indiceIndirizzoDataInivioValidita);

                        if (!Convert.IsDBNull(reader["telefono"]))
                        {
                            lavoratore.NumeroTelefono = (string) reader["telefono"];
                        }
                        if (!Convert.IsDBNull(reader["cellulare"]))
                        {
                            lavoratore.NumeroTelefonoCellulare = (string) reader["cellulare"];
                        }
                        if (!Convert.IsDBNull(reader["email"]))
                        {
                            lavoratore.Email = (string) reader["email"];
                        }
                        if (!reader.IsDBNull(indiceIban))
                        {
                            lavoratore.IBAN = reader.GetString(indiceIban);
                        }
                    }
                }
            }

            return lavoratore;
        }

        public RapportoDiLavoro GetRapportoDiLavoro(int idLavoratore, int idImpresa)
        {
            RapportoDiLavoro rapporto = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriRapportoImpresaPersonaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        rapporto = new RapportoDiLavoro();

                        rapporto.IdImpresa = (int) reader["idImpresa"];
                        rapporto.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["dataInizioValiditaRapporto"]))
                        {
                            rapporto.DataInizioValiditaRapporto = (DateTime) reader["dataInizioValiditaRapporto"];
                        }
                        rapporto.DataFineValiditaRapporto = (DateTime) reader["dataFineValiditaRapporto"];
                        if (!Convert.IsDBNull(reader["dataAssunzione"]))
                        {
                            rapporto.DataAssunzione = (DateTime) reader["dataAssunzione"];
                        }
                        if (!Convert.IsDBNull(reader["dataLicenziamento"]))
                        {
                            rapporto.DataLicenziamento = (DateTime) reader["dataLicenziamento"];
                        }
                        if (!Convert.IsDBNull(reader["idContratto"]))
                        {
                            rapporto.Contratto = new TipoContratto();
                            rapporto.Contratto.IdContratto = (string) reader["idContratto"];
                            rapporto.Contratto.Descrizione = (string) reader["descrizioneContratto"];
                        }
                        if (!Convert.IsDBNull(reader["idCategoria"]))
                        {
                            rapporto.Categoria = new TipoCategoria();
                            rapporto.Categoria.IdCategoria = (string) reader["idCategoria"];
                            rapporto.Categoria.Descrizione = (string) reader["descrizioneCategoria"];
                        }
                        if (!Convert.IsDBNull(reader["idQualifica"]))
                        {
                            rapporto.Qualifica = new TipoQualifica();
                            rapporto.Qualifica.IdQualifica = (string) reader["idQualifica"];
                            rapporto.Qualifica.Descrizione = (string) reader["descrizioneQualifica"];
                        }
                        if (!Convert.IsDBNull(reader["idMansione"]))
                        {
                            rapporto.Mansione = new TipoMansione();
                            rapporto.Mansione.IdMansione = (string) reader["idMansione"];
                            rapporto.Mansione.Descrizione = (string) reader["descrizioneMansione"];
                        }
                        if (!Convert.IsDBNull(reader["tipoInizioRapporto"]))
                        {
                            rapporto.TipoInizioRapporto = new TipoInizioRapporto();
                            rapporto.TipoInizioRapporto.IdTipoInizioRapporto = (string) reader["tipoInizioRapporto"];
                        }
                    }
                }
            }

            return rapporto;
        }

        public Impresa GetImpresa(string codiceFiscalePartitaIva)
        {
            Impresa impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_ImpreseSelectByPartitaIva]"))
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, codiceFiscalePartitaIva);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexStato = reader.GetOrdinal("stato");
                    int indexCodiceContratto = reader.GetOrdinal("codiceContratto");
                    int indexDescrizioneContratto = reader.GetOrdinal("descrizioneContratto");

                    #endregion

                    if (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.IdImpresa = (int) reader["idImpresa"];
                        impresa.RagioneSociale = (string) reader["ragioneSociale"];

                        if (!Convert.IsDBNull(reader["partitaIva"]))
                        {
                            impresa.PartitaIva = (string) reader["partitaIva"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        {
                            impresa.CodiceFiscale = (string) reader["codiceFiscale"];
                        }
                        //if (!Convert.IsDBNull(reader["codiceContratto"]))
                        //{
                        //    impresa.CodiceContratto = (String)reader["codiceContratto"];
                        //}
                        if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                        {
                            impresa.SedeLegaleIndirizzo = (string) reader["indirizzoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                        {
                            impresa.SedeLegaleComune = (string) reader["localitaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                        {
                            impresa.SedeLegaleProvincia = (string) reader["provinciaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeLegale"]))
                        {
                            impresa.SedeLegaleCap = (string) reader["capSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeLegale"]))
                        {
                            impresa.SedeLegaleTelefono = (string) reader["telefonoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeLegale"]))
                        {
                            impresa.SedeLegaleFax = (string) reader["faxSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeLegale"]))
                        {
                            impresa.SedeLegaleEmail = (string) reader["emailSedeLegale"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaIndirizzo = (string) reader["indirizzoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaComune = (string) reader["localitaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaProvincia = (string) reader["provinciaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaCap = (string) reader["capSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaTelefono = (string) reader["telefonoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaFax = (string) reader["faxSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaEmail = (string) reader["emailSedeAmministrazione"];
                        }

                        impresa.Contratto = new TipoContratto
                        {
                            IdContratto =
                                reader.IsDBNull(indexCodiceContratto) ? null : reader.GetString(indexCodiceContratto),
                            Descrizione = reader.IsDBNull(indexDescrizioneContratto)
                                ? null
                                : reader.GetString(indexDescrizioneContratto)
                        };

                        impresa.Stato = reader.IsDBNull(indexStato) ? null : reader.GetString(indexStato);
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresa(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexStato = reader.GetOrdinal("stato");
                    int indexCodiceContratto = reader.GetOrdinal("codiceContratto");
                    int indexDescrizioneContratto = reader.GetOrdinal("descrizioneContratto");

                    #endregion

                    if (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.IdImpresa = (int) reader["idImpresa"];
                        impresa.RagioneSociale = (string) reader["ragioneSociale"];

                        if (!Convert.IsDBNull(reader["partitaIva"]))
                        {
                            impresa.PartitaIva = (string) reader["partitaIva"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        {
                            impresa.CodiceFiscale = (string) reader["codiceFiscale"];
                        }
                        //if (!Convert.IsDBNull(reader["codiceContratto"]))
                        //{
                        //    impresa.CodiceContratto = (String) reader["codiceContratto"];
                        //}
                        if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                        {
                            impresa.SedeLegaleIndirizzo = (string) reader["indirizzoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                        {
                            impresa.SedeLegaleComune = (string) reader["localitaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                        {
                            impresa.SedeLegaleProvincia = (string) reader["provinciaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeLegale"]))
                        {
                            impresa.SedeLegaleCap = (string) reader["capSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeLegale"]))
                        {
                            impresa.SedeLegaleTelefono = (string) reader["telefonoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeLegale"]))
                        {
                            impresa.SedeLegaleFax = (string) reader["faxSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeLegale"]))
                        {
                            impresa.SedeLegaleEmail = (string) reader["emailSedeLegale"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaIndirizzo = (string) reader["indirizzoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaComune = (string) reader["localitaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaProvincia = (string) reader["provinciaSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaCap = (string) reader["capSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaTelefono = (string) reader["telefonoSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaFax = (string) reader["faxSedeAmministrazione"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeAmministrazione"]))
                        {
                            impresa.SedeAmministrativaEmail = (string) reader["emailSedeAmministrazione"];
                        }

                        impresa.Contratto = new TipoContratto
                        {
                            IdContratto =
                                reader.IsDBNull(indexCodiceContratto) ? null : reader.GetString(indexCodiceContratto),
                            Descrizione = reader.IsDBNull(indexDescrizioneContratto)
                                ? null
                                : reader.GetString(indexDescrizioneContratto)
                        };

                        impresa.Stato = reader.IsDBNull(indexStato) ? null : reader.GetString(indexStato);
                    }
                }
            }

            return impresa;
        }

        public RapportoDiLavoroCollection GetRapportiDiLavoroAperti(int idLavoratore,
            DateTime dataInizioRapportoDiLavoro)
        {
            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriRapportiDiLavoroApertiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoLavoro", DbType.DateTime,
                    dataInizioRapportoDiLavoro);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceTipoRapportoLavoro = reader.GetOrdinal("tipoRapporto");
                    int indiceStato = reader.GetOrdinal("stato");
                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIvaImpresa = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscaleImpresa = reader.GetOrdinal("impCodiceFiscale");
                    int indiceDataInizio = reader.GetOrdinal("dataInizioValiditaRapporto");
                    int indiceDataFine = reader.GetOrdinal("dataFineValiditaRapporto");
                    int indiceImpresaDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");

                    #endregion

                    while (reader.Read())
                    {
                        RapportoDiLavoro rapportoDiLavoro = new RapportoDiLavoro();
                        rapportiAperti.Add(rapportoDiLavoro);

                        rapportoDiLavoro.TipoRapportoLavoro =
                            (TipologiaRapportoLavoro) reader.GetInt32(indiceTipoRapportoLavoro);
                        if (!reader.IsDBNull(indiceStato))
                        {
                            rapportoDiLavoro.StatoPratica = (TipoStatoGestionePratica) reader.GetInt32(indiceStato);
                        }
                        rapportoDiLavoro.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.RagioneSocialeImpresa = reader.GetString(indiceRagioneSocialeImpresa);
                        rapportoDiLavoro.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                        rapportoDiLavoro.Lavoratore = new Lavoratore();
                        rapportoDiLavoro.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            rapportoDiLavoro.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            rapportoDiLavoro.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            rapportoDiLavoro.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            rapportoDiLavoro.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }

                        rapportoDiLavoro.Impresa = new Impresa();
                        rapportoDiLavoro.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.Impresa.RagioneSociale = reader.GetString(indiceRagioneSocialeImpresa);
                        if (!reader.IsDBNull(indicePartitaIvaImpresa))
                        {
                            rapportoDiLavoro.Impresa.PartitaIva = reader.GetString(indicePartitaIvaImpresa);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            rapportoDiLavoro.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceImpresaDataUltimaDenuncia))
                        {
                            rapportoDiLavoro.Impresa.DataUltimaDenuncia =
                                reader.GetDateTime(indiceImpresaDataUltimaDenuncia);
                        }

                        if (!reader.IsDBNull(indiceDataInizio))
                        {
                            rapportoDiLavoro.DataInizioValiditaRapporto = reader.GetDateTime(indiceDataInizio);
                        }
                        if (!reader.IsDBNull(indiceDataFine))
                        {
                            rapportoDiLavoro.DataFineValiditaRapporto = reader.GetDateTime(indiceDataFine);
                        }
                    }
                }
            }

            return rapportiAperti;
        }

        public RapportoDiLavoroCollection GetDichiarazioniAperte(int idDichiarazioneCorrente, string codiceFiscale,
            DateTime dataInizioRapportoDiLavoro)
        {
            RapportoDiLavoroCollection rapportiAperti = new RapportoDiLavoroCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniAperteSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazioneCorrente);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoLavoro", DbType.DateTime,
                    dataInizioRapportoDiLavoro);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceTipoRapportoLavoro = reader.GetOrdinal("tipoRapporto");
                    int indiceStato = reader.GetOrdinal("stato");
                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIvaImpresa = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscaleImpresa = reader.GetOrdinal("impCodiceFiscale");
                    int indiceDataInizio = reader.GetOrdinal("dataInizioValiditaRapporto");
                    int indiceDataFine = reader.GetOrdinal("dataFineValiditaRapporto");
                    int indiceImpresaDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");

                    #endregion

                    while (reader.Read())
                    {
                        RapportoDiLavoro rapportoDiLavoro = new RapportoDiLavoro();
                        rapportiAperti.Add(rapportoDiLavoro);

                        rapportoDiLavoro.TipoRapportoLavoro =
                            (TipologiaRapportoLavoro) reader.GetInt32(indiceTipoRapportoLavoro);
                        if (!reader.IsDBNull(indiceStato))
                        {
                            rapportoDiLavoro.StatoPratica = (TipoStatoGestionePratica) reader.GetInt32(indiceStato);
                        }
                        rapportoDiLavoro.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.RagioneSocialeImpresa = reader.GetString(indiceRagioneSocialeImpresa);
                        rapportoDiLavoro.IdLavoratore = reader.GetInt32(indiceIdLavoratore);

                        rapportoDiLavoro.Lavoratore = new Lavoratore();
                        rapportoDiLavoro.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            rapportoDiLavoro.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            rapportoDiLavoro.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            rapportoDiLavoro.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            rapportoDiLavoro.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }

                        rapportoDiLavoro.Impresa = new Impresa();
                        rapportoDiLavoro.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        rapportoDiLavoro.Impresa.RagioneSociale = reader.GetString(indiceRagioneSocialeImpresa);
                        if (!reader.IsDBNull(indicePartitaIvaImpresa))
                        {
                            rapportoDiLavoro.Impresa.PartitaIva = reader.GetString(indicePartitaIvaImpresa);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            rapportoDiLavoro.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceImpresaDataUltimaDenuncia))
                        {
                            rapportoDiLavoro.Impresa.DataUltimaDenuncia =
                                reader.GetDateTime(indiceImpresaDataUltimaDenuncia);
                        }

                        if (!reader.IsDBNull(indiceDataInizio))
                        {
                            rapportoDiLavoro.DataInizioValiditaRapporto = reader.GetDateTime(indiceDataInizio);
                        }
                        if (!reader.IsDBNull(indiceDataFine))
                        {
                            rapportoDiLavoro.DataFineValiditaRapporto = reader.GetDateTime(indiceDataFine);
                        }
                    }
                }
            }

            return rapportiAperti;
        }

        public Dichiarazione GetDichiarazione(int idDichiarazione)
        {
            Dichiarazione dichiarazione = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniSelectByID"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexDataUltimoCambioStato = reader.GetOrdinal("dataUltimoCambioStato");
                    int indexImpresaDataUltimaDenuncia = reader.GetOrdinal("dataUltimaDenuncia");
                    int indexImpresaStato = reader.GetOrdinal("stato");
                    int indexIdUtenteUltimoCambioStato = reader.GetOrdinal("idUtenteUltimoCambioStato");
                    int indexIdComunicazionePrecedente = reader.GetOrdinal("idComunicazionePrecedente");
                    int indexIdComunicazioneSuccessiva = reader.GetOrdinal("idComunicazioneSuccessiva");

                    #endregion

                    if (reader.Read())
                    {
                        #region dati generali dichiarazione

                        dichiarazione = new Dichiarazione();

                        dichiarazione.IdDichiarazione = (int) reader["idDichiarazione"];

                        if (!Convert.IsDBNull(reader["dataInvio"]))
                        {
                            dichiarazione.DataInvio = (DateTime) reader["dataInvio"];
                        }

                        if (!Convert.IsDBNull(reader["attivita"]))
                        {
                            dichiarazione.Attivita = (TipoAttivita) (short) reader["attivita"];
                        }

                        if (!Convert.IsDBNull(reader["idUtente"]))
                        {
                            dichiarazione.IdUtente = (int) reader["idUtente"];
                        }

                        if (!Convert.IsDBNull(reader["idStato"]))
                        {
                            dichiarazione.Stato = (TipoStatoGestionePratica) (int) reader["idStato"];
                        }

                        dichiarazione.NuovoLavoratore = (bool) reader["nuovoLavoratore"];

                        if (!Convert.IsDBNull(reader["idLavoratoreSelezionato"]))
                        {
                            dichiarazione.IdLavoratoreSelezionato = (int) reader["idLavoratoreSelezionato"];
                        }

                        if (!Convert.IsDBNull(reader["mantieniIbanAnagrafica"]))
                        {
                            dichiarazione.MantieniIbanAnagrafica = (bool) reader["mantieniIbanAnagrafica"];
                        }

                        if (!Convert.IsDBNull(reader["mantieniIndirizzoAnagrafica"]))
                        {
                            dichiarazione.MantieniIndirizzoAnagrafica = (bool) reader["mantieniIndirizzoAnagrafica"];
                        }
                        dichiarazione.ControlloSdoppione = (bool) reader["controlloSdoppione"];

                        dichiarazione.RapportoAssociatoCessazione = (bool) reader["rapportoAssociatoCessazione"];

                        if (!Convert.IsDBNull(reader["idImpresaRapportoSelezionatoCessazione"]))
                        {
                            dichiarazione.IdImpresaRapportoSelezionatoCessazione =
                                (int) reader["idImpresaRapportoSelezionatoCessazione"];
                        }

                        if (!Convert.IsDBNull(reader["idLavoratoreRapportoSelezionatoCessazione"]))
                        {
                            dichiarazione.IdLavoratoreRapportoSelezionatoCessazione =
                                (int) reader["idLavoratoreRapportoSelezionatoCessazione"];
                        }

                        if (!Convert.IsDBNull(reader["dataFineRapportoSelezionatoCessazione"]))
                        {
                            dichiarazione.DataFineRapportoSelezionatoCessazione =
                                (DateTime) reader["dataFineRapportoSelezionatoCessazione"];
                        }

                        if (!Convert.IsDBNull(reader["idDichiarazionePrecedente"]))
                        {
                            dichiarazione.IdDichiarazionePrecedente = (int) reader["idDichiarazionePrecedente"];
                        }

                        if (!Convert.IsDBNull(reader["idDichiarazioneSuccessiva"]))
                        {
                            dichiarazione.IdDichiarazioneSuccessiva = (int) reader["idDichiarazioneSuccessiva"];
                        }

                        if (!Convert.IsDBNull(reader["cellulareSMSSiceInfo"]))
                        {
                            dichiarazione.CellulareSMSSiceInfo = (string) reader["cellulareSMSSiceInfo"];
                        }

                        dichiarazione.IdComunicazioneSuccessiva = reader.IsDBNull(indexIdComunicazioneSuccessiva)
                            ? null
                            : reader.GetString(indexIdComunicazioneSuccessiva);
                        dichiarazione.IdComunicazionePrecedente = reader.IsDBNull(indexIdComunicazionePrecedente)
                            ? null
                            : reader.GetString(indexIdComunicazionePrecedente);
                        dichiarazione.DataUltimoCambioStato = reader.IsDBNull(indexDataUltimoCambioStato)
                            ? (DateTime?) null
                            : reader.GetDateTime(indexDataUltimoCambioStato);
                        dichiarazione.UltimoCambioStatoIdUtente = reader.IsDBNull(indexIdUtenteUltimoCambioStato)
                            ? (int?) null
                            : reader.GetInt32(indexIdUtenteUltimoCambioStato);

                        #endregion

                        #region impresa

                        dichiarazione.Impresa = new Impresa();

                        if (!Convert.IsDBNull(reader["idImpresa"]))
                        {
                            dichiarazione.Impresa.IdImpresa = (int) reader["idImpresa"];
                        }
                        if (!Convert.IsDBNull(reader["ragioneSocialeImpresa"]))
                        {
                            dichiarazione.Impresa.RagioneSociale = (string) reader["ragioneSocialeImpresa"];
                        }
                        if (!Convert.IsDBNull(reader["partitaIVA"]))
                        {
                            dichiarazione.Impresa.PartitaIva = (string) reader["partitaIVA"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscaleImpresa"]))
                        {
                            dichiarazione.Impresa.CodiceFiscale = (string) reader["codiceFiscaleImpresa"];
                        }
                        if (!Convert.IsDBNull(reader["codiceContratto"]))
                        {
                            dichiarazione.Impresa.Contratto = new TipoContratto();
                            dichiarazione.Impresa.Contratto.IdContratto = (string) reader["codiceContratto"];
                            dichiarazione.Impresa.Contratto.Descrizione = (string) reader["contrattoImpresa"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleIndirizzo = (string) reader["indirizzoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["capSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleCap = (string) reader["capSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["localitaSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleComune = (string) reader["localitaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleProvincia = (string) reader["provinciaSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleTelefono = (string) reader["telefonoSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["faxSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleFax = (string) reader["faxSedeLegale"];
                        }
                        if (!Convert.IsDBNull(reader["emailSedeLegale"]))
                        {
                            dichiarazione.Impresa.SedeLegaleEmail = (string) reader["emailSedeLegale"];
                        }

                        dichiarazione.Impresa.Stato = reader.IsDBNull(indexImpresaStato)
                            ? null
                            : reader.GetString(indexImpresaStato);
                        dichiarazione.Impresa.DataUltimaDenuncia = reader.IsDBNull(indexImpresaDataUltimaDenuncia)
                            ? (DateTime?) null
                            : reader.GetDateTime(indexImpresaDataUltimaDenuncia);

                        #endregion

                        #region cantiere

                        dichiarazione.Cantiere = new Indirizzo();
                        if (!Convert.IsDBNull(reader["idCantiere"]))
                        {
                            //dichiarazione.Cantiere.IdCantiere = (Int32)reader["idCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoCantiere"]))
                        {
                            dichiarazione.Cantiere.Indirizzo1 = (string) reader["indirizzoCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["comuneCantiere"]))
                        {
                            dichiarazione.Cantiere.Comune = (string) reader["comuneCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["comuneCantiereDescrizione"]))
                        {
                            dichiarazione.Cantiere.ComuneDescrizione = (string) reader["comuneCantiereDescrizione"];
                        }
                        if (!Convert.IsDBNull(reader["civicoCantiere"]))
                        {
                            dichiarazione.Cantiere.Civico = (string) reader["civicoCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaCantiere"]))
                        {
                            dichiarazione.Cantiere.Provincia = (string) reader["provinciaCantiere"];
                        }
                        if (!Convert.IsDBNull(reader["capCantiere"]))
                        {
                            dichiarazione.Cantiere.Cap = (string) reader["capCantiere"];
                        }

                        #endregion

                        #region consulente

                        if (!Convert.IsDBNull(reader["idConsulente"]))
                        {
                            dichiarazione.Consulente = new Consulente();
                            dichiarazione.Consulente.IdConsulente = (int) reader["idConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["ragioneSocialeConsulente"]))
                        {
                            dichiarazione.Consulente.RagioneSociale = (string) reader["ragioneSocialeConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscaleConsulente"]))
                        {
                            dichiarazione.Consulente.CodiceFiscale = (string) reader["codiceFiscaleConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["indirizzoConsulente"]))
                        {
                            //dichiarazione.Consulente.Indirizzo = (String)reader["indirizzoConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["capConsulente"]))
                        {
                            //dichiarazione.Consulente.Cap = (String)reader["capConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["localitaConsulente"]))
                        {
                            //dichiarazione.Consulente.Comune = (String)reader["localitaConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaConsulente"]))
                        {
                            //dichiarazione.Consulente.Provincia = (String)reader["provinciaConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoConsulente"]))
                        {
                            dichiarazione.Consulente.Telefono = (string) reader["telefonoConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["faxConsulente"]))
                        {
                            dichiarazione.Consulente.Fax = (string) reader["faxConsulente"];
                        }
                        if (!Convert.IsDBNull(reader["emailConsulente"]))
                        {
                            dichiarazione.Consulente.Email = (string) reader["emailConsulente"];
                        }

                        #endregion

                        #region lavoratore

                        dichiarazione.Lavoratore = new Lavoratore();

                        //Lavoratore nuovo
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                            dichiarazione.Lavoratore.IdLavoratore = (int) reader["IdLavoratore"];

                            if (!Convert.IsDBNull(reader["cognomeLavoratore"]))
                            {
                                dichiarazione.Lavoratore.Cognome = (string) reader["cognomeLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["nomeLavoratore"]))
                            {
                                dichiarazione.Lavoratore.Nome = (string) reader["nomeLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["sessoLavoratore"]))
                            {
                                dichiarazione.Lavoratore.Sesso = ((string) reader["sessoLavoratore"])[0];
                            }
                            if (!Convert.IsDBNull(reader["dataNascitaLavoratore"]))
                            {
                                dichiarazione.Lavoratore.DataNascita = (DateTime) reader["dataNascitaLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["codiceFiscaleLavoratore"]))
                            {
                                dichiarazione.Lavoratore.CodiceFiscale = (string) reader["codiceFiscaleLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["comuneNascitaLavoratore"]))
                            {
                                dichiarazione.Lavoratore.ComuneNascita = (string) reader["comuneNascitaLavoratore"];
                            }
                            if (!Convert.IsDBNull(reader["provinciaNascitaLavoratore"]))
                            {
                                dichiarazione.Lavoratore.ProvinciaNascita =
                                    (string) reader["provinciaNascitaLavoratore"];
                            }
                        }

                        //Lavoratore CE
                        if (!Convert.IsDBNull(reader["idLavoratoreCE"]))
                        {
                            dichiarazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            dichiarazione.Lavoratore.IdLavoratore = (int) reader["IdLavoratoreCE"];

                            if (!Convert.IsDBNull(reader["cognomeLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.Cognome = (string) reader["cognomeLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["nomeLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.Nome = (string) reader["nomeLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["sessoLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.Sesso = ((string) reader["sessoLavoratoreCE"])[0];
                            }
                            if (!Convert.IsDBNull(reader["dataNascitaLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.DataNascita = (DateTime) reader["dataNascitaLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["codiceFiscaleLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.CodiceFiscale = (string) reader["codiceFiscaleLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["comuneNascitaLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.ComuneNascita = (string) reader["comuneNascitaLavoratoreCE"];
                            }
                            if (!Convert.IsDBNull(reader["provinciaNascitaLavoratoreCE"]))
                            {
                                dichiarazione.Lavoratore.ProvinciaNascita =
                                    (string) reader["provinciaNascitaLavoratoreCE"];
                            }
                        }

                        //Dati aggiuntivi
                        if (!Convert.IsDBNull(reader["italianoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Italiano = (bool) reader["italianoLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["statoCivileLavoratore"]))
                        {
                            dichiarazione.Lavoratore.IdStatoCivile = (string) reader["statoCivileLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["cittadinanzaLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Cittadinanza = (string) reader["cittadinanzaLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["livelloIstruzioneLavoratore"]))
                        {
                            dichiarazione.Lavoratore.LivelloIstruzione = (string) reader["livelloIstruzioneLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["linguaComunicazioniLavoratore"]))
                        {
                            dichiarazione.Lavoratore.LinguaComunicazione =
                                (string) reader["linguaComunicazioniLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["IBANLavoratore"]))
                        {
                            dichiarazione.Lavoratore.IBAN = (string) reader["IBANLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["cognomeCointestatarioLavoratore"]))
                        {
                            dichiarazione.Lavoratore.CognomeCointestatario =
                                (string) reader["cognomeCointestatarioLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["richiestaInfoCartaPrepagata"]))
                        {
                            dichiarazione.Lavoratore.RichiestaInfoCartaPrepagata =
                                (bool) reader["richiestaInfoCartaPrepagata"];
                        }


                        if (!Convert.IsDBNull(reader["idTipoCartaPrepagata"]))
                        {
                            dichiarazione.Lavoratore.TipoPrepagata = new TipoCartaPrepagata();
                            dichiarazione.Lavoratore.TipoPrepagata.IdTipoCartaPrepagata =
                                (string) reader["idTipoCartaPrepagata"];
                            dichiarazione.Lavoratore.TipoPrepagata.Descrizione =
                                (string) reader["tipoCartaPrepagataLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["tagliaFelpaHuskyLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TagliaFelpaHusky = (string) reader["tagliaFelpaHuskyLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["tagliaPantaloniLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TagliaPantaloni = (string) reader["tagliaPantaloniLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["tagliaScarpeLavoratore"]))
                        {
                            dichiarazione.Lavoratore.TagliaScarpe = (string) reader["tagliaScarpeLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["richiestaIscrizione16OreLavoratore"]))
                        {
                            dichiarazione.Lavoratore.RichiestaIscrizioneCorsi16Ore =
                                (bool) reader["richiestaIscrizione16OreLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["descrizioneDocumento"]))
                        {
                            dichiarazione.Lavoratore.TipoDocumento = (string) reader["descrizioneDocumento"];
                        }

                        if (!Convert.IsDBNull(reader["numeroDocumento"]))
                        {
                            dichiarazione.Lavoratore.NumeroDocumento = (string) reader["numeroDocumento"];
                        }

                        if (!Convert.IsDBNull(reader["descrizioneMotivoPermesso"]))
                        {
                            dichiarazione.Lavoratore.MotivoPermesso = (string) reader["descrizioneMotivoPermesso"];
                        }

                        if (!Convert.IsDBNull(reader["dataRichiestaPermesso"]))
                        {
                            dichiarazione.Lavoratore.DataRichiestaPermesso = (DateTime) reader["dataRichiestaPermesso"];
                        }

                        if (!Convert.IsDBNull(reader["dataScadenzaPermesso"]))
                        {
                            dichiarazione.Lavoratore.DataScadenzaPermesso = (DateTime) reader["dataScadenzaPermesso"];
                        }

                        dichiarazione.Lavoratore.Indirizzo = new Indirizzo();

                        if (!Convert.IsDBNull(reader["indirizzoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Indirizzo1 = (string) reader["indirizzoLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["civicoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Civico = (string) reader["civicoLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["comuneLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Comune = (string) reader["comuneLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["comuneLavoratoreDescrizione"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.ComuneDescrizione =
                                (string) reader["comuneLavoratoreDescrizione"];
                        }
                        if (!Convert.IsDBNull(reader["provinciaLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Provincia = (string) reader["provinciaLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["capLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Indirizzo.Cap = (string) reader["capLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["telefonoLavoratore"]))
                        {
                            dichiarazione.Lavoratore.NumeroTelefono = (string) reader["telefonoLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["cellulareLavoratore"]))
                        {
                            dichiarazione.Lavoratore.NumeroTelefonoCellulare = (string) reader["cellulareLavoratore"];
                        }
                        if (!Convert.IsDBNull(reader["eMailLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Email = (string) reader["eMailLavoratore"];
                        }

                        if (!Convert.IsDBNull(reader["aderisceServizioSMS"]))
                        {
                            dichiarazione.Lavoratore.AderisceServizioSMS = (bool) reader["aderisceServizioSMS"];
                        }

                        if (!Convert.IsDBNull(reader["noteLavoratore"]))
                        {
                            dichiarazione.Lavoratore.Note = (string) reader["noteLavoratore"];
                        }

                        #endregion

                        #region rapporto di lavoro

                        if (!Convert.IsDBNull(reader["idRapportoDiLavoro"]))
                        {
                            dichiarazione.RapportoDiLavoro = new RapportoDiLavoro();

                            if (!Convert.IsDBNull(reader["dataAssunzione"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataAssunzione = (DateTime) reader["dataAssunzione"];
                            }

                            if (!Convert.IsDBNull(reader["dataInizioRapportoDiLavoro"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataInizioValiditaRapporto =
                                    (DateTime) reader["dataInizioRapportoDiLavoro"];
                            }

                            if (!Convert.IsDBNull(reader["dataFineRapportoDiLavoro"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataFineValiditaRapporto =
                                    (DateTime) reader["dataFineRapportoDiLavoro"];
                            }

                            dichiarazione.RapportoDiLavoro.Contratto = new TipoContratto();
                            if (!Convert.IsDBNull(reader["idContrattoApplicatoLavoratore"]))
                            {
                                dichiarazione.RapportoDiLavoro.Contratto.IdContratto =
                                    (string) reader["idContrattoApplicatoLavoratore"];
                            }

                            if (!Convert.IsDBNull(reader["descrizioneContrattoApplicato"]))
                            {
                                dichiarazione.RapportoDiLavoro.Contratto.Descrizione =
                                    (string) reader["descrizioneContrattoApplicato"];
                            }

                            dichiarazione.RapportoDiLavoro.TipoInizioRapporto = new TipoInizioRapporto();
                            if (!Convert.IsDBNull(reader["tipologiaContrattualeAssunzione"]))
                            {
                                dichiarazione.RapportoDiLavoro.TipoInizioRapporto.Descrizione =
                                    (string) reader["tipologiaContrattualeAssunzione"];
                            }

                            if (!Convert.IsDBNull(reader["partTime"]))
                            {
                                dichiarazione.RapportoDiLavoro.PartTime = (bool) reader["partTime"];
                            }

                            if (!Convert.IsDBNull(reader["percentualePartTime"]))
                            {
                                dichiarazione.RapportoDiLavoro.PercentualePartTime =
                                    (decimal) reader["percentualePartTime"];
                            }

                            if (!Convert.IsDBNull(reader["livelloInquadramento"]))
                            {
                                dichiarazione.RapportoDiLavoro.LivelloInquadramento =
                                    (string) reader["livelloInquadramento"];
                            }

                            if (!Convert.IsDBNull(reader["idCategoria"]))
                            {
                                dichiarazione.RapportoDiLavoro.Categoria = new TipoCategoria();
                                dichiarazione.RapportoDiLavoro.Categoria.IdCategoria = (string) reader["idCategoria"];
                                dichiarazione.RapportoDiLavoro.Categoria.Descrizione = (string) reader["categoria"];
                            }

                            if (!Convert.IsDBNull(reader["idQualifica"]))
                            {
                                dichiarazione.RapportoDiLavoro.Qualifica = new TipoQualifica();
                                dichiarazione.RapportoDiLavoro.Qualifica.IdQualifica = (string) reader["idQualifica"];
                                dichiarazione.RapportoDiLavoro.Qualifica.Descrizione = (string) reader["qualifica"];
                            }

                            if (!Convert.IsDBNull(reader["idMansione"]))
                            {
                                dichiarazione.RapportoDiLavoro.Mansione = new TipoMansione();
                                dichiarazione.RapportoDiLavoro.Mansione.IdMansione = (string) reader["idMansione"];
                                dichiarazione.RapportoDiLavoro.Mansione.Descrizione = (string) reader["mansione"];
                            }

                            if (!Convert.IsDBNull(reader["dataCessazione"]))
                            {
                                dichiarazione.RapportoDiLavoro.DataCessazione = (DateTime) reader["dataCessazione"];
                            }

                            dichiarazione.RapportoDiLavoro.TipoFineRapporto = new TipoFineRapporto();
                            if (!Convert.IsDBNull(reader["causaCessazione"]))
                            {
                                dichiarazione.RapportoDiLavoro.TipoFineRapporto.Descrizione =
                                    (string) reader["causaCessazione"];
                            }
                        }

                        #endregion

                        if (!Convert.IsDBNull(reader["posizioneValida"]))
                        {
                            dichiarazione.Lavoratore.PosizioneValida = (bool) reader["posizioneValida"];
                        }
                    }
                }

                return dichiarazione;
            }
        }

        public LavoratoreCollection GetLavoratoriConStessoCodiceFiscale(string codiceFiscale)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriLavoratoriRicercaPerCodiceFiscaleSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceSesso = reader.GetOrdinal("sesso");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLuogoNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagrafici(
            string cognome,
            string nome,
            char sesso,
            DateTime dataNascita,
            string luogoNascita)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriLavoratoriRicercaPerDatiAnagraficiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, sesso.ToString());
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
                DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String, luogoNascita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceSesso = reader.GetOrdinal("sesso");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLuogoNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public LavoratoreCollection GetLavoratoriConStessiDatiAnagraficiECodiceFiscale(
            string cognome,
            string nome,
            char sesso,
            DateTime dataNascita,
            string luogoNascita,
            string codiceFiscale)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriLavoratoriRicercaPerDatiAnagraficiECodiceFiscaleSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, sesso.ToString());
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, dataNascita);
                DatabaseCemi.AddInParameter(comando, "@luogoNascita", DbType.String, luogoNascita);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceSesso = reader.GetOrdinal("sesso");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceLuogoNascita = reader.GetOrdinal("luogoNascita");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIban = reader.GetOrdinal("iban");
                    int indiceCellulare = reader.GetOrdinal("cellulare");
                    int indiceEmail = reader.GetOrdinal("email");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzoDenominazione");
                    int indiceProvincia = reader.GetOrdinal("indirizzoProvincia");
                    int indiceComune = reader.GetOrdinal("indirizzoComune");
                    int indiceCap = reader.GetOrdinal("indirizzoCAP");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceLuogoNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIban))
                        {
                            lavoratore.IBAN = reader.GetString(indiceIban);
                        }
                        if (!reader.IsDBNull(indiceCellulare))
                        {
                            lavoratore.NumeroTelefonoCellulare = reader.GetString(indiceCellulare);
                        }
                        if (!reader.IsDBNull(indiceEmail))
                        {
                            lavoratore.Email = reader.GetString(indiceEmail);
                        }
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            lavoratore.Indirizzo = new Indirizzo();

                            lavoratore.Indirizzo.Indirizzo1 = reader.GetString(indiceIndirizzo);

                            if (!reader.IsDBNull(indiceProvincia))
                            {
                                lavoratore.Indirizzo.Provincia = reader.GetString(indiceProvincia);
                            }
                            if (!reader.IsDBNull(indiceComune))
                            {
                                lavoratore.Indirizzo.Comune = reader.GetString(indiceComune);
                            }
                            if (!reader.IsDBNull(indiceCap))
                            {
                                lavoratore.Indirizzo.Cap = reader.GetString(indiceCap);
                            }
                        }
                    }
                }
            }

            return lavoratori;
        }

        public void CambiaStatoDichiarazione(int idDichiarazione, TipoStatoGestionePratica vecchioStato,
            TipoStatoGestionePratica nuovoStato, int idUtenteCambioStato)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniCambioStateUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                DatabaseCemi.AddInParameter(comando, "@vecchioStato", DbType.Int32, vecchioStato);
                DatabaseCemi.AddInParameter(comando, "@nuovoStato", DbType.Int32, nuovoStato);
                DatabaseCemi.AddInParameter(comando, "@idUtenteCambioStato", DbType.Int32, idUtenteCambioStato);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new StatoGiaVariatoException();
                }
            }
        }

        public void CambiaLavoratoreSelezionato(int idDichiarazione, int? idLavoratore, bool nuovoLavoratore)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniSelezioneLavoratoreUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (idLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@nuovoLavoratore", DbType.Boolean, nuovoLavoratore);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("CambiaLavoratoreSelezionato: cambio selezione lavoratore non riuscito");
                }
            }
        }

        public void ControlloSdoppioneEffettuato(int idDichiarazione, bool controlloEffettuato)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniControlloSdoppioneUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                DatabaseCemi.AddInParameter(comando, "@controlloEffettuato", DbType.Boolean, controlloEffettuato);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("ControlloSdoppioneEffettuato: aggiornamento controllo sdoppione non riuscito");
                }
            }
        }

        public void CambiaIBANDichiarato(int idDichiarazione, bool? mantieni)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniCambioIBANUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (mantieni.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mantieni", DbType.Boolean, mantieni);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio selezione IBAN non effettuato");
                }
            }
        }

        public void CambiaIndirizzoDichiarato(int idDichiarazione, bool? mantieni)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniCambioIndirizzoUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (mantieni.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mantieni", DbType.Boolean, mantieni);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio indirizzo dichiarato non effettuato");
                }
            }
        }

        public void CambiaSelezioneRapportoCessazione(int idDichiarazione, bool? mantieni, int? idLavoratore,
            int? idImpresa, DateTime? dataFineValiditaRapporto)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniCambioSelezioneRapportoCessazione"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (mantieni.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mantieni", DbType.Boolean, mantieni);
                if (idLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                if (idImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);
                if (dataFineValiditaRapporto.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataFineValiditaRapporto", DbType.DateTime,
                        dataFineValiditaRapporto);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio selezione rapporto da cessare non effettuato");
                }
            }
        }

        public void CambiaPosizioneValida(int idDichiarazione, bool? posizioneValida)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniCambioPosizioneValidaUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDichiarazione", DbType.Int32, idDichiarazione);
                if (posizioneValida.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@posizioneValida", DbType.Boolean, posizioneValida);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Cambio posizione valida non effettuato");
                }
            }
        }

        #region inserimento

        private void InsertLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            AnagraficaComuneDataProvider anagraficaComuneDataProvider = new AnagraficaComuneDataProvider();
            if (anagraficaComuneDataProvider.InsertLavoratore(lavoratore, transaction))
            {
                InsertLavoratoreDatiAggiuntivi(lavoratore, transaction);
            }
            else
            {
                throw new Exception("InsertLavoratore: errore durante l'inserimento nell'anagrafica comune");
            }
        }

        private bool InsertLavoratoreDatiAggiuntivi(Lavoratore lavoratore, DbTransaction transaction)
        {
            bool res = false;

            if (lavoratore.IdLavoratore.HasValue)
            {
                using (DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriLavoratoriDatiAggiuntiviInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);

                    if (!string.IsNullOrEmpty(lavoratore.IdStatoCivile))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idStatoCivile", DbType.String, lavoratore.IdStatoCivile);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.Cittadinanza))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, lavoratore.Cittadinanza);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.LivelloIstruzione))
                    {
                        DatabaseCemi.AddInParameter(comando, "@livelloIstruzione", DbType.String,
                            lavoratore.LivelloIstruzione);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.IdLingua))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idLingua", DbType.String, lavoratore.IdLingua);
                    }

                    if (lavoratore.DataDecesso.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime, lavoratore.DataDecesso);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.IdTipoDecesso))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoDecesso", DbType.String, lavoratore.IdTipoDecesso);
                    }

                    //if (lavoratore.Italiano)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@italiano", DbType.Boolean, lavoratore.Italiano);
                    //}

                    if (lavoratore.Indirizzo != null)
                    {
                        if (!string.IsNullOrEmpty(lavoratore.Indirizzo.Indirizzo1))
                        {
                            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String,
                                lavoratore.Indirizzo.Indirizzo1);
                        }
                        if (!string.IsNullOrEmpty(lavoratore.Indirizzo.Civico))
                        {
                            DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, lavoratore.Indirizzo.Civico);
                        }
                        if (!string.IsNullOrEmpty(lavoratore.Indirizzo.Comune))
                        {
                            DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, lavoratore.Indirizzo.Comune);
                        }
                        if (!string.IsNullOrEmpty(lavoratore.Indirizzo.Provincia))
                        {
                            DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String,
                                lavoratore.Indirizzo.Provincia);
                        }
                        if (!string.IsNullOrEmpty(lavoratore.Indirizzo.Cap))
                        {
                            DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, lavoratore.Indirizzo.Cap);
                        }
                        //if (!String.IsNullOrEmpty(lavoratore.Indirizzo.InfoAggiuntiva))
                        //{
                        //    //DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String, lavoratoreInfoAggiuntiva);
                        //}
                    }

                    if (!string.IsNullOrEmpty(lavoratore.NumeroTelefono))
                    {
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, lavoratore.NumeroTelefono);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.NumeroTelefonoCellulare))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String,
                            lavoratore.NumeroTelefonoCellulare);
                    }

                    //if (lavoratore.AderisceServizioSMS)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@aderisceServizioSMS", DbType.Boolean,
                        lavoratore.AderisceServizioSMS);
                    //}

                    if (!string.IsNullOrEmpty(lavoratore.Email))
                    {
                        DatabaseCemi.AddInParameter(comando, "@email", DbType.String, lavoratore.Email);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.IBAN))
                    {
                        DatabaseCemi.AddInParameter(comando, "@IBAN", DbType.String, lavoratore.IBAN);
                    }

                    //if (lavoratore.CoIntestato)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@coIntestato", DbType.Boolean, lavoratore.CoIntestato);
                    //}

                    if (!string.IsNullOrEmpty(lavoratore.CognomeCointestatario))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cognomeCointestatario", DbType.String,
                            lavoratore.CognomeCointestatario);
                    }

                    if (lavoratore.IdTagliaFelpaHusky.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTagliaFelpaHusky", DbType.Int32,
                            lavoratore.IdTagliaFelpaHusky.Value);
                    }

                    if (lavoratore.IdTagliaPantaloni.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTagliaPantaloni", DbType.Int32,
                            lavoratore.IdTagliaPantaloni.Value);
                    }

                    if (lavoratore.IdTagliaScarpe.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTagliaScarpe", DbType.Int32,
                            lavoratore.IdTagliaScarpe.Value);
                    }

                    //if (lavoratore.RichiestaIscrizioneCorsi16Ore)
                    //{
                    DatabaseCemi.AddInParameter(comando, "@richiestaIscrizione16Ore", DbType.Boolean,
                        lavoratore.RichiestaIscrizioneCorsi16Ore);
                    //}

                    if (!string.IsNullOrEmpty(lavoratore.Note))
                    {
                        DatabaseCemi.AddInParameter(comando, "@note", DbType.String, lavoratore.Note);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.IdSindacato))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idSindacato", DbType.String, lavoratore.IdSindacato);
                    }

                    if (lavoratore.DataAdesione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataAdesione", DbType.DateTime, lavoratore.DataAdesione);
                    }

                    if (lavoratore.DataDisdetta.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataDisdetta", DbType.DateTime, lavoratore.DataDisdetta);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.NumeroDelega))
                    {
                        DatabaseCemi.AddInParameter(comando, "@numeroDelega", DbType.String, lavoratore.NumeroDelega);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.TipoDocumento))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.String,
                            lavoratore.TipoDocumento);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.NumeroDocumento))
                    {
                        DatabaseCemi.AddInParameter(comando, "@numeroDocumento", DbType.String,
                            lavoratore.NumeroDocumento);
                    }

                    if (!string.IsNullOrEmpty(lavoratore.MotivoPermesso))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idMotivoPermesso", DbType.String,
                            lavoratore.MotivoPermesso);
                    }

                    if (lavoratore.DataRichiestaPermesso.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataRichiestaPermesso", DbType.DateTime,
                            lavoratore.DataRichiestaPermesso);
                    }

                    if (lavoratore.DataScadenzaPermesso.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataScadenzaPermesso", DbType.DateTime,
                            lavoratore.DataRichiestaPermesso);
                    }


                    //if (lavoratore.RapportoDiLavoro != null)
                    //{
                    //    if (lavoratore.RapportoDiLavoro.DataAssunzione.HasValue)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoDiLavoro", DbType.DateTime,
                    //                                    lavoratore.RapportoDiLavoro.DataInizioValiditaRapporto.Value);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Contratto.IdContratto))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idTipologiaContrattuale", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Contratto.IdContratto);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Categoria.IdCategoria))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idCategoria", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Categoria.IdCategoria);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Qualifica.IdQualifica))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idQualifica", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Qualifica.IdQualifica);
                    //    }

                    //    if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.Mansione.IdMansione))
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idMansione", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.Mansione.IdMansione);
                    //    }

                    //    if (lavoratore.RapportoDiLavoro.DataCessazione.HasValue)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime,
                    //                                    lavoratore.RapportoDiLavoro.DataFineValiditaRapporto);
                    //    }

                    //    if (lavoratore.RapportoDiLavoro.TipoFineRapporto != null)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@idCausaCessazione", DbType.String,
                    //                                    lavoratore.RapportoDiLavoro.TipoFineRapporto.IdTipoFineRapporto);
                    //    }

                    //    if (lavoratore.RapportoDiLavoro.DataTrasformazione.HasValue)
                    //    {
                    //        DatabaseCemi.AddInParameter(comando, "@dataTrasformazione", DbType.DateTime,
                    //                                    lavoratore.RapportoDiLavoro.DataTrasformazione.Value);
                    //    }


                    //    //TODO VALLA: rivedere specifiche
                    //    //if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.))
                    //    //{
                    //    //    DatabaseCemi.AddInParameter(comando, "@idCodiceTrasformazione", DbType.String, lavoratore.IdCodiceTrasformazione);
                    //    //}

                    //}

                    if (lavoratore.RichiestaInfoCartaPrepagata != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@richiestaInfoCartaPrepagata", DbType.Boolean,
                            lavoratore.RichiestaInfoCartaPrepagata);
                    }

                    if (lavoratore.TipoPrepagata != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoCartaPrepagata", DbType.String,
                            lavoratore.TipoPrepagata.IdTipoCartaPrepagata);
                    }

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }


        private int? InsertCantiere(Indirizzo cantiere, DbTransaction transaction)
        {
            int? idCantiere = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriCantieriInsert"))
            {
                if (cantiere != null)
                {
                    if (!string.IsNullOrEmpty(cantiere.Indirizzo1))
                    {
                        DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String,
                            cantiere.Indirizzo1);
                    }
                    if (!string.IsNullOrEmpty(cantiere.Civico))
                    {
                        DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, cantiere.Civico);
                    }
                    if (!string.IsNullOrEmpty(cantiere.Comune))
                    {
                        DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, cantiere.Comune);
                    }
                    if (!string.IsNullOrEmpty(cantiere.Provincia))
                    {
                        DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, cantiere.Provincia);
                    }
                    if (!string.IsNullOrEmpty(cantiere.Cap))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, cantiere.Cap);
                    }
                    if (cantiere.Latitudine.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal, cantiere.Latitudine);
                    }
                    if (cantiere.Longitudine.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal, cantiere.Longitudine);
                    }
                    //if (!String.IsNullOrEmpty(cantiere.InfoAggiuntiva))
                    //{
                    //    //DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String, cantiere.InfoAggiuntiva);
                    //}

                    DatabaseCemi.AddOutParameter(comando, "@idCantiere", DbType.Int32, 4);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    idCantiere = (int) DatabaseCemi.GetParameterValue(comando, "@idCantiere");
                }
            }

            return idCantiere;
        }

        private int? InsertRapportoDiLavoro(RapportoDiLavoro rapportoDiLavoro, DbTransaction transaction)
        {
            int? idRapportoDiLavoro = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriRapportiDiLavoroInsert"))
            {
                if (rapportoDiLavoro != null)
                {
                    if (rapportoDiLavoro.DataAssunzione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                            rapportoDiLavoro.DataAssunzione.Value);
                    }

                    if (rapportoDiLavoro.DataInizioValiditaRapporto.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataInizioRapportoDiLavoro", DbType.DateTime,
                            rapportoDiLavoro.DataInizioValiditaRapporto.Value);
                    }

                    if (rapportoDiLavoro.DataFineValiditaRapporto.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataFineRapportoDiLavoro", DbType.DateTime,
                            rapportoDiLavoro.DataFineValiditaRapporto.Value);
                    }

                    if (rapportoDiLavoro.Contratto != null)
                    {
                        if (!string.IsNullOrEmpty(rapportoDiLavoro.Contratto.IdContratto))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idTipologiaContrattuale", DbType.String,
                                rapportoDiLavoro.Contratto.IdContratto);
                        }
                    }

                    if (rapportoDiLavoro.Categoria != null)
                    {
                        if (!string.IsNullOrEmpty(rapportoDiLavoro.Categoria.IdCategoria))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCategoria", DbType.String,
                                rapportoDiLavoro.Categoria.IdCategoria);
                        }
                    }

                    if (rapportoDiLavoro.Qualifica != null)
                    {
                        if (!string.IsNullOrEmpty(rapportoDiLavoro.Qualifica.IdQualifica))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idQualifica", DbType.String,
                                rapportoDiLavoro.Qualifica.IdQualifica);
                        }
                    }

                    if (rapportoDiLavoro.Mansione != null)
                    {
                        if (!string.IsNullOrEmpty(rapportoDiLavoro.Mansione.IdMansione))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idMansione", DbType.String,
                                rapportoDiLavoro.Mansione.IdMansione);
                        }
                    }

                    if (rapportoDiLavoro.DataCessazione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime,
                            rapportoDiLavoro.DataCessazione);
                    }

                    if (rapportoDiLavoro.TipoFineRapporto != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idCausaCessazione", DbType.String,
                            rapportoDiLavoro.TipoFineRapporto.IdTipoFineRapporto);
                    }

                    if (rapportoDiLavoro.DataTrasformazione.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataTrasformazione", DbType.DateTime,
                            rapportoDiLavoro.DataTrasformazione.Value);
                    }

                    if (rapportoDiLavoro.TipoInizioRapporto != null)
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipoInizioRapporto", DbType.String,
                            rapportoDiLavoro.TipoInizioRapporto.IdTipoInizioRapporto);
                    }

                    DatabaseCemi.AddInParameter(comando, "@partTime", DbType.Boolean,
                        rapportoDiLavoro.PartTime);

                    if (rapportoDiLavoro.PercentualePartTime.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@percentualePartTime", DbType.Decimal,
                            rapportoDiLavoro.PercentualePartTime);
                    }

                    if (!string.IsNullOrEmpty(rapportoDiLavoro.LivelloInquadramento))
                    {
                        DatabaseCemi.AddInParameter(comando, "@livelloInquadramento", DbType.String,
                            rapportoDiLavoro.LivelloInquadramento);
                    }

                    if (rapportoDiLavoro.ContrattoOriginaleSintesi != null &&
                        !string.IsNullOrEmpty(rapportoDiLavoro.ContrattoOriginaleSintesi.IdContratto))
                    {
                        DatabaseCemi.AddInParameter(comando, "@idTipologiaContrattualeOriginaleSintesi", DbType.String,
                            rapportoDiLavoro.ContrattoOriginaleSintesi.IdContratto);
                    }

                    //TODO VALLA: rivedere specifiche
                    //if (!String.IsNullOrEmpty(lavoratore.RapportoDiLavoro.))
                    //{
                    //    DatabaseCemi.AddInParameter(comando, "@idCodiceTrasformazione", DbType.String, lavoratore.IdCodiceTrasformazione);
                    //}


                    DatabaseCemi.AddOutParameter(comando, "@idRapportoDiLavoro", DbType.Int32, 4);
                }


                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    idRapportoDiLavoro = (int) DatabaseCemi.GetParameterValue(comando, "@idRapportoDiLavoro");
                }
            }
            return idRapportoDiLavoro;
        }


        public bool InsertDichiarazione(Dichiarazione dichiarazione)
        {
            bool res = false;
            int? idCantiere = null;
            int? idRapportoDiLavoro = null;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (!dichiarazione.Lavoratore.IdLavoratore.HasValue)
                        {
                            InsertLavoratore(dichiarazione.Lavoratore, transaction);
                        }

                        if (dichiarazione.Cantiere != null)
                        {
                            idCantiere = InsertCantiere(dichiarazione.Cantiere, transaction);
                        }

                        if (dichiarazione.RapportoDiLavoro != null)
                        {
                            idRapportoDiLavoro = InsertRapportoDiLavoro(dichiarazione.RapportoDiLavoro, transaction);
                        }

                        using (
                            DbCommand comando =
                                DatabaseCemi.GetStoredProcCommand("dbo.USP_IscrizioneLavoratoriDichiarazioniInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, dichiarazione.IdImpresa);
                            DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, dichiarazione.IdUtente);

                            if (dichiarazione.IdConsulente.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32,
                                    dichiarazione.IdConsulente);
                            }

                            if (dichiarazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                    dichiarazione.Lavoratore.IdLavoratore);
                            }
                            else
                            {
                                if (dichiarazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idLavoratoreIscrizioneLavoratori",
                                        DbType.Int32,
                                        dichiarazione.Lavoratore.IdLavoratore);
                                }
                            }

                            if (idCantiere.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idCantiere", DbType.Int32,
                                    idCantiere);
                            }

                            if (idRapportoDiLavoro.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idRapportoDiLavoro", DbType.Int32,
                                    idRapportoDiLavoro);
                            }

                            DatabaseCemi.AddInParameter(comando, "@attivita", DbType.Int32, dichiarazione.Attivita);
                            DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, (int) dichiarazione.Stato);

                            DatabaseCemi.AddOutParameter(comando, "@idDichiarazione", DbType.Int32, 32);

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                res = true;
                                dichiarazione.IdDichiarazione =
                                    (int?) DatabaseCemi.GetParameterValue(comando, "@idDichiarazione");
                            }
                        }

                        if (res)
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            throw new Exception(
                                "InsertDichiarazione: l'inserimento di una dichiarazione non Ŕ andato a buon fine");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        res = false;
                        throw;
                    }
                }
            }

            return res;
        }

        public DichiarazioneCollection GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(int idImpresa,
            string codiceFiscale,
            DateTime?
                dataInizioValiditaRapporto,
            DateTime? dataCessazione)
        {
            DichiarazioneCollection dichiarazioni = new DichiarazioneCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_IscrizioneLavoratoriDichiarazioniSelectByImpresaCodiceFiscaleDataInizioDataCessazione")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@codiceImpresa", DbType.Int32, idImpresa);

                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);


                if (dataInizioValiditaRapporto.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInizioRapporto", DbType.DateTime,
                        dataInizioValiditaRapporto.Value);
                }

                if (dataCessazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataCessazione", DbType.DateTime, dataCessazione.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdIscrizioneLavoratoriDichiarazione =
                        reader.GetOrdinal("idIscrizioneLavoratoriDichiarazione");
                    int indiceDataInserimentoRecord = reader.GetOrdinal("dataInserimentoRecord");
                    int indiceAttivita = reader.GetOrdinal("attivita");
                    int indiceIdStato = reader.GetOrdinal("idStato");
                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIVA = reader.GetOrdinal("partitaIVA");
                    int indiceCodiceFiscaleImpresa = reader.GetOrdinal("codiceFiscaleImpresa");
                    int indiceIdCantiere = reader.GetOrdinal("idCantiere");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceCivico = reader.GetOrdinal("civico");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("Provincia");
                    int indiceCAP = reader.GetOrdinal("CAP");
                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIdLavoratoreCE = reader.GetOrdinal("idLavoratoreCE");
                    int indiceCognomeCE = reader.GetOrdinal("cognomeCE");
                    int indiceNomeCE = reader.GetOrdinal("nomeCE");
                    int indiceDataNascitaCE = reader.GetOrdinal("dataNascitaCE");
                    int indiceCodiceFiscaleCE = reader.GetOrdinal("codiceFiscaleCE");
                    int indiceIdUtente = reader.GetOrdinal("idUtente");

                    #endregion

                    while (reader.Read())
                    {
                        Dichiarazione dichiarazione = new Dichiarazione();
                        dichiarazioni.Add(dichiarazione);

                        dichiarazione.IdDichiarazione = reader.GetInt32(indiceIdIscrizioneLavoratoriDichiarazione);
                        if (!reader.IsDBNull(indiceDataInserimentoRecord))
                        {
                            dichiarazione.DataInvio = reader.GetDateTime(indiceDataInserimentoRecord);
                        }
                        if (!reader.IsDBNull(indiceAttivita))
                        {
                            dichiarazione.Attivita = (TipoAttivita) reader.GetInt16(indiceAttivita);
                        }
                        if (!reader.IsDBNull(indiceIdStato))
                        {
                            dichiarazione.Stato = (TipoStatoGestionePratica) reader.GetInt32(indiceIdStato);
                        }
                        if (!reader.IsDBNull(indiceIdUtente))
                        {
                            dichiarazione.IdUtente = reader.GetInt32(indiceIdUtente);
                        }
                        dichiarazione.Impresa = new Impresa();
                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            dichiarazione.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            dichiarazione.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        }
                        if (!reader.IsDBNull(indiceRagioneSociale))
                        {
                            dichiarazione.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        if (!reader.IsDBNull(indicePartitaIVA))
                        {
                            dichiarazione.Impresa.PartitaIva = reader.GetString(indicePartitaIVA);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleImpresa))
                        {
                            dichiarazione.Impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscaleImpresa);
                        }
                        if (!reader.IsDBNull(indiceIdCantiere))
                        {
                            dichiarazione.IdCantiere = reader.GetInt32(indiceIdCantiere);
                        }
                        dichiarazione.Cantiere = new Indirizzo();
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            dichiarazione.Cantiere.Indirizzo1 = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceCivico))
                        {
                            dichiarazione.Cantiere.Civico = reader.GetString(indiceCivico);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            dichiarazione.Cantiere.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            dichiarazione.Cantiere.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCAP))
                        {
                            dichiarazione.Cantiere.Cap = reader.GetString(indiceCAP);
                        }
                        dichiarazione.Lavoratore = new Lavoratore();
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIdLavoratoreCE))
                        {
                            dichiarazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratoreCE);
                        }
                        if (!reader.IsDBNull(indiceCognomeCE))
                        {
                            dichiarazione.Lavoratore.Cognome = reader.GetString(indiceCognomeCE);
                        }
                        if (!reader.IsDBNull(indiceNomeCE))
                        {
                            dichiarazione.Lavoratore.Nome = reader.GetString(indiceNomeCE);
                        }
                        if (!reader.IsDBNull(indiceDataNascitaCE))
                        {
                            dichiarazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascitaCE);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscaleCE))
                        {
                            dichiarazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscaleCE);
                        }
                    }
                }
            }


            return dichiarazioni;
        }

        #endregion
    }
}