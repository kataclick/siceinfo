using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Corsi.Type.DataSets;
using TBridge.Cemi.Data.AnagraficaComune;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.Type.Enums.Corsi;
using TBridge.Cemi.Type.Exceptions.Corsi;
using TBridge.Cemi.Type.Filters.Corsi;

namespace TBridge.Cemi.Corsi.Data
{
    public class CorsiDataAccess
    {
        public CorsiDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public LocazioneCollection GetLocazioni()
        {
            LocazioneCollection locazioni = new LocazioneCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniSelectAll"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceCap = reader.GetOrdinal("cap");
                    int indiceLatitudine = reader.GetOrdinal("latitudine");
                    int indiceLongitudine = reader.GetOrdinal("longitudine");
                    int indiceNote = reader.GetOrdinal("note");
                    int indiceEsistePianificazione = reader.GetOrdinal("esistePianificazione");

                    #endregion

                    while (reader.Read())
                    {
                        Locazione locazione = new Locazione();
                        locazioni.Add(locazione);

                        locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                            locazione.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            locazione.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            locazione.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceLatitudine))
                            locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        if (!reader.IsDBNull(indiceNote))
                            locazione.Note = reader.GetString(indiceNote);
                        locazione.EsistePianificazione = reader.GetBoolean(indiceEsistePianificazione);
                    }
                }
            }

            return locazioni;
        }

        public bool InsertLocazione(Locazione locazione)
        {
            bool res = false;

            if (locazione == null)
            {
                throw new ArgumentNullException();
            }
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, locazione.Indirizzo);
                if (!string.IsNullOrEmpty(locazione.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, locazione.Comune);
                if (!string.IsNullOrEmpty(locazione.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, locazione.Provincia);
                if (!string.IsNullOrEmpty(locazione.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, locazione.Cap);
                if (locazione.Latitudine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Decimal, locazione.Latitudine);
                if (locazione.Longitudine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Decimal, locazione.Longitudine);
                if (!string.IsNullOrEmpty(locazione.Note))
                    DatabaseCemi.AddInParameter(comando, "@note", DbType.String, locazione.Note);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public CorsoCollection GetCorsiAll(bool conPianificazioni, bool pianificabili)
        {
            CorsoCollection corsi = new CorsoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiSelectAll"))
            {
                DatabaseCemi.AddInParameter(comando, "@conSchedulazioni", DbType.Boolean, conPianificazioni);
                DatabaseCemi.AddInParameter(comando, "@pianificabili", DbType.Boolean, pianificabili);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdCorso = reader.GetOrdinal("idCorso");
                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");
                    int indiceVersione = reader.GetOrdinal("versione");
                    int indiceDurataComplessiva = reader.GetOrdinal("durataComplessiva");
                    int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    int indiceDescrizioneModulo = reader.GetOrdinal("descrizioneModulo");
                    int indiceDurataModulo = reader.GetOrdinal("durataModulo");
                    int indiceProgressivoModulo = reader.GetOrdinal("progressivoModulo");
                    int indiceNumeroPianificazioni = reader.GetOrdinal("numeroPianificazioni");
                    int indiceIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");

                    #endregion

                    Corso corso = null;

                    while (reader.Read())
                    {
                        int idCorso = reader.GetInt32(indiceIdCorso);

                        if (corso == null || corso.IdCorso != idCorso)
                        {
                            corso = new Corso();
                            corsi.Add(corso);

                            corso.IdCorso = reader.GetInt32(indiceIdCorso);
                            corso.Codice = reader.GetString(indiceCodice);
                            corso.Descrizione = reader.GetString(indiceDescrizione);
                            if (!reader.IsDBNull(indiceVersione))
                            {
                                corso.Versione = reader.GetString(indiceVersione);
                            }
                            corso.DurataComplessiva = reader.GetInt16(indiceDurataComplessiva);
                            corso.NumeroPianificazioni = reader.GetInt32(indiceNumeroPianificazioni);
                            corso.IscrizioneLibera = reader.GetBoolean(indiceIscrizioneLibera);
                        }

                        Modulo modulo = new Modulo();
                        corso.Moduli.Add(modulo);

                        modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        modulo.Descrizione = reader.GetString(indiceDescrizioneModulo);
                        modulo.OreDurata = reader.GetInt16(indiceDurataModulo);
                        modulo.Progressivo = reader.GetInt16(indiceProgressivoModulo);
                    }
                }
            }

            return corsi;
        }

        public Corso GetCorsoByKey(int idCorso)
        {
            Corso corso = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader 1

                    int indiceIdCorso = reader.GetOrdinal("idCorso");
                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");
                    int indiceVersione = reader.GetOrdinal("versione");
                    int indiceDurataComplessiva = reader.GetOrdinal("durataComplessiva");
                    int indiceIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");
                    int indiceReportSingolo = reader.GetOrdinal("nomeReportSingolo");
                    int indiceReportMultiplo = reader.GetOrdinal("nomeReportMultipli");

                    #endregion

                    reader.Read();

                    // Corso
                    corso = new Corso();
                    corso.IdCorso = reader.GetInt32(indiceIdCorso);
                    corso.Codice = reader.GetString(indiceCodice);
                    corso.Descrizione = reader.GetString(indiceDescrizione);
                    if (!reader.IsDBNull(indiceVersione))
                    {
                        corso.Versione = reader.GetString(indiceVersione);
                    }
                    corso.DurataComplessiva = reader.GetInt16(indiceDurataComplessiva);
                    corso.IscrizioneLibera = reader.GetBoolean(indiceIscrizioneLibera);
                    corso.ReportAttestato = reader.GetString(indiceReportSingolo);
                    corso.ReportAttestati = reader.GetString(indiceReportMultiplo);

                    reader.NextResult();

                    #region Indici per reader 2

                    int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    int indiceDescrizioneModulo = reader.GetOrdinal("descrizioneModulo");
                    int indiceDurataModulo = reader.GetOrdinal("durataModulo");
                    int indiceProgressivoModulo = reader.GetOrdinal("progressivoModulo");

                    #endregion

                    // Moduli
                    while (reader.Read())
                    {
                        Modulo modulo = new Modulo();
                        corso.Moduli.Add(modulo);

                        modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        modulo.Descrizione = reader.GetString(indiceDescrizioneModulo);
                        modulo.OreDurata = reader.GetInt16(indiceDurataModulo);
                        modulo.Progressivo = reader.GetInt16(indiceProgressivoModulo);
                    }
                }
            }

            return corso;
        }

        public bool InsertCorso(Corso corso)
        {
            bool res = false;

            if (corso == null)
            {
                throw new ArgumentNullException();
            }

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@codice", DbType.String, corso.Codice);
                            DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, corso.Descrizione);
                            if (!string.IsNullOrEmpty(corso.Versione))
                            {
                                DatabaseCemi.AddInParameter(comando, "@versione", DbType.String, corso.Versione);
                            }
                            DatabaseCemi.AddInParameter(comando, "@durata", DbType.Int16, corso.DurataComplessiva);
                            DatabaseCemi.AddInParameter(comando, "@iscrizioneLibera", DbType.Int16,
                                corso.IscrizioneLibera);
                            DatabaseCemi.AddOutParameter(comando, "@idCorso", DbType.Int32, 4);
                            DatabaseCemi.ExecuteNonQuery(comando, transaction);

                            corso.IdCorso = (int) DatabaseCemi.GetParameterValue(comando, "@idCorso");
                            if (corso.IdCorso > 0)
                            {
                                res = true;

                                foreach (Modulo modulo in corso.Moduli)
                                {
                                    if (!InsertModulo(corso.IdCorso.Value, modulo, transaction))
                                    {
                                        res = false;
                                        throw new Exception(
                                            string.Format("InsertModulo: Errore nell'inserimento di un modulo: {0}",
                                                modulo.Descrizione));
                                    }
                                }

                                transaction.Commit();
                            }
                            else
                            {
                                throw new Exception(string.Format(
                                    "InsertCorso: Errore nell'inserimento del corso: {0}",
                                    corso.Descrizione));
                            }
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private bool InsertModulo(int idCorso, Modulo modulo, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, modulo.Descrizione);
                DatabaseCemi.AddInParameter(comando, "@durata", DbType.Int16, modulo.OreDurata);
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);
                DatabaseCemi.AddInParameter(comando, "@progressivo", DbType.Int16, modulo.Progressivo);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
                else
                {
                    throw new Exception(string.Format("InsertModulo: Errore nell'inserimento di un modulo: {0}",
                        modulo.Descrizione));
                }
            }

            return res;
        }

        public ModuloCollection GetModuliConProgrammazioniFuture(int idCorso, DateTime dataInizio, DateTime dataFine,
            bool? prenotabili, bool overBooking)
        {
            ModuloCollection moduli = new ModuloCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectByCorsoKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, dataInizio);
                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataFine);
                DatabaseCemi.AddInParameter(comando, "@registroConfermato", DbType.Boolean, false);
                DatabaseCemi.AddInParameter(comando, "@overBooking", DbType.Boolean, overBooking);
                if (prenotabili.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@prenotabili", DbType.Boolean, prenotabili.Value);
                }

                Modulo moduloCorr = null;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    int indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    int indiceInizio = reader.GetOrdinal("inizio");
                    int indiceFine = reader.GetOrdinal("fine");
                    int indicePartecipanti = reader.GetOrdinal("partecipanti");
                    int indiceMaxPartecipanti = reader.GetOrdinal("maxPartecipanti");
                    int indiceIdCorso = reader.GetOrdinal("idCorso");
                    int indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    int indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    int indiceIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");
                    int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    int indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    int indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    int indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceCap = reader.GetOrdinal("cap");
                    int indiceLatitudine = reader.GetOrdinal("latitudine");
                    int indiceLongitudine = reader.GetOrdinal("longitudine");
                    int indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        int idModulo = reader.GetInt32(indiceIdModulo);

                        if (moduloCorr == null || moduloCorr.IdModulo != idModulo)
                        {
                            moduloCorr = new Modulo();
                            moduloCorr.Programmazioni = new ProgrammazioneModuloCollection();
                            moduli.Add(moduloCorr);

                            // Modulo
                            moduloCorr.IdModulo = reader.GetInt32(indiceIdModulo);
                            moduloCorr.Descrizione = reader.GetString(indiceModuloDescrizione);
                            moduloCorr.OreDurata = reader.GetInt16(indiceModuloDurata);
                            moduloCorr.Progressivo = reader.GetInt16(indiceModuloProgressivo);
                        }

                        ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                        moduloCorr.Programmazioni.Add(programmazione);

                        programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                        programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);
                        programmazione.Partecipanti = reader.GetInt32(indicePartecipanti);
                        programmazione.MaxPartecipanti = reader.GetInt32(indiceMaxPartecipanti);

                        // Corso
                        programmazione.Corso = new Corso();
                        programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);
                        programmazione.Corso.IscrizioneLibera = reader.GetBoolean(indiceIscrizioneLibera);

                        // Modulo
                        programmazione.Modulo = new Modulo();
                        programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                        // Locazione
                        programmazione.Locazione = new Locazione();
                        programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }
                    }
                }
            }

            return moduli;
        }


        protected bool InsertPartecipazioneModulo(PartecipazioneModulo partecipazioneModulo, int idPartecipazione,
            DbTransaction transaction, Lavoratore lavoratore,
            bool overBooking)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPartecipazioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);
                DatabaseCemi.AddInParameter(comando, "@idPianificazioneModulo", DbType.Int32,
                    partecipazioneModulo.Programmazione.IdProgrammazioneModulo);
                DatabaseCemi.AddInParameter(comando, "@overBooking", DbType.Int32, overBooking);

                if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore);
                }
                else
                {
                    if (lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica)
                    {
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscaleLavoratore", DbType.String,
                            lavoratore.CodiceFiscale);
                    }
                }
                DatabaseCemi.AddOutParameter(comando, "@risultato", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando, transaction);
                int risultato = (int) DatabaseCemi.GetParameterValue(comando, "@risultato");

                switch (risultato)
                {
                    case -1:
                        throw new LavoratoreGiaIscrittoException();
                        break;
                    case -2:
                        throw new DisponibilitaEsauritaException();
                        break;
                    case 1:
                        res = true;
                        break;
                    default:
                        throw new Exception("Errore nell'inserimento di una partecipazione modulo");
                        break;
                }
            }

            return res;
        }

        public bool InsertPartecipazione(Partecipazione partecipazione, bool overBooking)
        {
            bool res = true;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        // Devo controllare che il lavoratore e l'impresa siano presenti nel db
                        if (!partecipazione.Lavoratore.IdLavoratore.HasValue)
                        {
                            // Inserisco nel database il lavoratore
                            InsertLavoratore(partecipazione.Lavoratore, transaction);
                        }

                        if (!partecipazione.Impresa.IdImpresa.HasValue)
                        {
                            // Inserisco nel database l'impresa
                            InsertImpresa(partecipazione.Impresa, transaction);
                        }

                        using (
                            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32,
                                partecipazione.Corso.IdCorso.Value);
                            DatabaseCemi.AddInParameter(comando, "@prenotazioneImpresa", DbType.Boolean,
                                partecipazione.PrenotazioneImpresa);
                            DatabaseCemi.AddInParameter(comando, "@prenotazioneConsulente", DbType.Boolean,
                                partecipazione.PrenotazioneConsulente);
                            if (partecipazione.IdConsulente.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32,
                                    partecipazione.IdConsulente);
                            }
                            if (partecipazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                    partecipazione.Lavoratore.IdLavoratore);
                            }
                            else
                            {
                                if (partecipazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.Anagrafica)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idCorsiLavoratore", DbType.Int32,
                                        partecipazione.Lavoratore.IdLavoratore);
                                }
                            }
                            if (partecipazione.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                            {
                                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32,
                                    partecipazione.Impresa.IdImpresa);
                            }
                            else
                            {
                                if (partecipazione.Impresa.TipoImpresa == TipologiaImpresa.Anagrafica)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idCorsiImpresa", DbType.Int32,
                                        partecipazione.Impresa.IdImpresa);
                                }
                            }
                            if (partecipazione.Lavoratore.PrimaEsperienza.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@primaEsperienza", DbType.Boolean,
                                    partecipazione.Lavoratore.PrimaEsperienza.Value);
                            }
                            if (partecipazione.Lavoratore.DataAssunzione.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime,
                                    partecipazione.Lavoratore.DataAssunzione.Value);
                            }

                            DatabaseCemi.AddOutParameter(comando, "@idPartecipazione", DbType.Int32, 4);

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            {
                                partecipazione.IdPartecipazione =
                                    (int) DatabaseCemi.GetParameterValue(comando, "@idPartecipazione");

                                foreach (
                                    PartecipazioneModulo partecipazioneModulo in partecipazione.PartecipazioneModuli)
                                {
                                    if (
                                        !InsertPartecipazioneModulo(partecipazioneModulo,
                                            partecipazione.IdPartecipazione.Value, transaction,
                                            partecipazione.Lavoratore, overBooking))
                                    {
                                        res = false;
                                        throw new Exception("Errore nell'inserimento di una partecipazione modulo");
                                    }
                                }
                            }
                            else
                            {
                                res = false;
                            }
                        }

                        if (res)
                        {
                            transaction.Commit();
                        }
                        else
                        {
                            throw new Exception(
                                "InsertPartecipazioni: l'inserimento di una partecipazione non � andato a buon fine");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        res = false;
                        throw;
                    }
                }
            }

            return true;
        }

        public ProgrammazioneModuloCollection GetPartecipazione(PartecipazioneFilter filtro)
        {
            ProgrammazioneModuloCollection programmazioni = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectPartecipazioni")
            )
            {
                if (filtro.IdLavoratore.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    programmazioni = TrasformaReaderInProgrammazioneModuloCollection(reader);
                }
            }

            return programmazioni;
        }

        public ProgrammazioneModuloCollection GetPartecipazione(int idPartecipazione)
        {
            ProgrammazioneModuloCollection programmazioni = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    programmazioni = TrasformaReaderInProgrammazioneModuloCollection(reader);
                }
            }

            return programmazioni;
        }

        private ProgrammazioneModuloCollection TrasformaReaderInProgrammazioneModuloCollection(IDataReader reader)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            #region Indici per il reader

            int indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
            int indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
            int indiceInizio = reader.GetOrdinal("inizio");
            int indiceFine = reader.GetOrdinal("fine");
            int indiceIdCorso = reader.GetOrdinal("idCorso");
            int indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
            int indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
            int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
            int indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
            int indiceModuloDurata = reader.GetOrdinal("moduloDurata");
            int indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
            int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
            int indiceIndirizzo = reader.GetOrdinal("indirizzo");
            int indiceComune = reader.GetOrdinal("comune");
            int indiceProvincia = reader.GetOrdinal("provincia");
            int indiceCap = reader.GetOrdinal("cap");
            int indiceLatitudine = reader.GetOrdinal("latitudine");
            int indiceLongitudine = reader.GetOrdinal("longitudine");
            int indiceNote = reader.GetOrdinal("note");

            #endregion

            while (reader.Read())
            {
                ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                programmazioni.Add(programmazione);

                programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);

                // Corso
                programmazione.Corso = new Corso();
                programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);

                // Modulo
                programmazione.Modulo = new Modulo();
                programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                // Locazione
                programmazione.Locazione = new Locazione();
                programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                if (!reader.IsDBNull(indiceComune))
                {
                    programmazione.Locazione.Comune = reader.GetString(indiceComune);
                }
                if (!reader.IsDBNull(indiceProvincia))
                {
                    programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                }
                if (!reader.IsDBNull(indiceCap))
                {
                    programmazione.Locazione.Cap = reader.GetString(indiceCap);
                }
                if (!reader.IsDBNull(indiceLatitudine))
                {
                    programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                }
                if (!reader.IsDBNull(indiceLongitudine))
                {
                    programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                }
                if (!reader.IsDBNull(indiceNote))
                {
                    programmazione.Locazione.Note = reader.GetString(indiceNote);
                }
            }

            return programmazioni;
        }

        public bool UpdatePartecipazioneModuloPresenza(int idPartecipazioneModulo, bool presente)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPartecipazioniUpdatePresenza")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazioneModulo", DbType.Int32, idPartecipazioneModulo);
                DatabaseCemi.AddInParameter(comando, "@presente", DbType.Boolean, presente);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool DeleteCorso(int idCorso)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, idCorso);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = true;
            }

            return res;
        }

        public PartecipazioneModuloCollection GetPartecipazioniByLavoratoreImpresa(
            PartecipazioneLavoratoreImpresaFilter filtro)
        {
            PartecipazioneModuloCollection partecipazioni = new PartecipazioneModuloCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectByLavoratoreImpresa"))
            {
                if (!string.IsNullOrEmpty(filtro.LavoratoreCognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@lavCognome", DbType.String, filtro.LavoratoreCognome);
                }
                if (!string.IsNullOrEmpty(filtro.LavoratoreNome))
                {
                    DatabaseCemi.AddInParameter(comando, "@lavNome", DbType.String, filtro.LavoratoreNome);
                }
                if (!string.IsNullOrEmpty(filtro.LavoratoreCodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@lavCodFisc", DbType.String, filtro.LavoratoreCodiceFiscale);
                }
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!string.IsNullOrEmpty(filtro.ImpresaRagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@impRagSoc", DbType.String, filtro.ImpresaRagioneSociale);
                }
                if (!string.IsNullOrEmpty(filtro.ImpresaIvaFisc))
                {
                    DatabaseCemi.AddInParameter(comando, "@impIvaFisc", DbType.String, filtro.ImpresaIvaFisc);
                }
                if (filtro.IdConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    int indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    int indiceInizio = reader.GetOrdinal("inizio");
                    int indiceFine = reader.GetOrdinal("fine");
                    int indiceConfermaPresenze = reader.GetOrdinal("registroConfermato");
                    int indiceIdCorso = reader.GetOrdinal("idCorso");
                    int indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    int indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    int indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    int indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    int indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceCap = reader.GetOrdinal("cap");
                    int indiceLatitudine = reader.GetOrdinal("latitudine");
                    int indiceLongitudine = reader.GetOrdinal("longitudine");
                    int indiceNote = reader.GetOrdinal("note");
                    int indicePresente = reader.GetOrdinal("presente");
                    int indiceAttestato = reader.GetOrdinal("attestatoRilasciabile");

                    int indiceIdLavoratore = reader.GetOrdinal("lavoratoreIdLavoratore");
                    int indiceCognome = reader.GetOrdinal("lavoratoreCognome");
                    int indiceNome = reader.GetOrdinal("lavoratoreNome");
                    int indiceDataNascita = reader.GetOrdinal("lavoratoreDataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("lavoratoreCodiceFiscale");
                    int indiceCorsiIdLavoratore = reader.GetOrdinal("corsiLavoratoreIdLavoratore");
                    int indiceCorsiCognome = reader.GetOrdinal("corsiLavoratoreCognome");
                    int indiceCorsiNome = reader.GetOrdinal("corsiLavoratoreNome");
                    int indiceCorsiDataNascita = reader.GetOrdinal("corsiLavoratoreDataNascita");
                    int indiceCorsiCodiceFiscale = reader.GetOrdinal("corsiLavoratoreCodiceFiscale");

                    int indiceIdImpresa = reader.GetOrdinal("impresaIdImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    int indiceCorsiIdImpresa = reader.GetOrdinal("corsiImpresaIdImpresa");
                    int indiceCorsiRagioneSociale = reader.GetOrdinal("corsiImpresaRagioneSociale");

                    #endregion

                    while (reader.Read())
                    {
                        PartecipazioneModulo partecipazione = new PartecipazioneModulo();
                        partecipazioni.Add(partecipazione);

                        partecipazione.Programmazione = new ProgrammazioneModulo();

                        partecipazione.Programmazione.IdProgrammazioneModulo =
                            reader.GetInt32(indiceIdModuliProgrammazione);
                        partecipazione.Programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        partecipazione.Programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        partecipazione.Programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);

                        // Corso
                        partecipazione.Programmazione.Corso = new Corso();
                        partecipazione.Programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        partecipazione.Programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        partecipazione.Programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);

                        // Modulo
                        partecipazione.Programmazione.Modulo = new Modulo();
                        partecipazione.Programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        partecipazione.Programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        partecipazione.Programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        partecipazione.Programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);
                        partecipazione.Programmazione.PresenzeConfermate = reader.GetBoolean(indiceConfermaPresenze);

                        // Locazione
                        partecipazione.Programmazione.Locazione = new Locazione();
                        partecipazione.Programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        partecipazione.Programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            partecipazione.Programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            partecipazione.Programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            partecipazione.Programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            partecipazione.Programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            partecipazione.Programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            partecipazione.Programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }

                        #region Lavoratore

                        partecipazione.Lavoratore = new Lavoratore();

                        partecipazione.Lavoratore.Presente = reader.GetBoolean(indicePresente);
                        partecipazione.Lavoratore.Attestato = reader.GetBoolean(indiceAttestato);

                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            partecipazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            partecipazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                            partecipazione.Lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                            {
                                partecipazione.Lavoratore.Nome = reader.GetString(indiceNome);
                            }
                            if (!reader.IsDBNull(indiceDataNascita))
                            {
                                partecipazione.Lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                partecipazione.Lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                        }
                        else
                        {
                            if (!reader.IsDBNull(indiceCorsiIdLavoratore))
                            {
                                partecipazione.Lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                                partecipazione.Lavoratore.IdLavoratore = reader.GetInt32(indiceCorsiIdLavoratore);
                                partecipazione.Lavoratore.Cognome = reader.GetString(indiceCorsiCognome);
                                if (!reader.IsDBNull(indiceCorsiNome))
                                {
                                    partecipazione.Lavoratore.Nome = reader.GetString(indiceCorsiNome);
                                }
                                if (!reader.IsDBNull(indiceCorsiDataNascita))
                                {
                                    partecipazione.Lavoratore.DataNascita = reader.GetDateTime(indiceCorsiDataNascita);
                                }
                                if (!reader.IsDBNull(indiceCorsiCodiceFiscale))
                                {
                                    partecipazione.Lavoratore.CodiceFiscale =
                                        reader.GetString(indiceCorsiCodiceFiscale);
                                }
                            }
                        }

                        #endregion

                        #region Impresa

                        partecipazione.Impresa = new Impresa();
                        partecipazione.Impresa = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            partecipazione.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            partecipazione.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            partecipazione.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        else
                        {
                            partecipazione.Impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                            partecipazione.Impresa.IdImpresa = reader.GetInt32(indiceCorsiIdImpresa);
                            partecipazione.Impresa.RagioneSociale = reader.GetString(indiceCorsiRagioneSociale);
                        }

                        #endregion
                    }
                }
            }

            return partecipazioni;
        }

        public bool DeletePartecipazione(int idPartecipazioneModulo)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazioneModulo", DbType.Int32, idPartecipazioneModulo);

                if (DatabaseCemi.ExecuteNonQuery(comando) > 0)
                {
                    res = true;
                }
            }

            return res;
        }

        public Locazione GetLocazioneByKey(int idLocazione)
        {
            Locazione locazione = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, idLocazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceCap = reader.GetOrdinal("cap");
                    int indiceLatitudine = reader.GetOrdinal("latitudine");
                    int indiceLongitudine = reader.GetOrdinal("longitudine");
                    int indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        locazione = new Locazione();

                        locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                            locazione.Comune = reader.GetString(indiceComune);
                        if (!reader.IsDBNull(indiceProvincia))
                            locazione.Provincia = reader.GetString(indiceProvincia);
                        if (!reader.IsDBNull(indiceCap))
                            locazione.Cap = reader.GetString(indiceCap);
                        if (!reader.IsDBNull(indiceLatitudine))
                            locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        if (!reader.IsDBNull(indiceLongitudine))
                            locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        if (!reader.IsDBNull(indiceNote))
                            locazione.Note = reader.GetString(indiceNote);
                    }
                }
            }

            return locazione;
        }

        public EstrazioneFormedilImpresaCollection EstrazioneFormedilImprese(EstrazioneFormedilFilter filtro)
        {
            EstrazioneFormedilImpresaCollection estrazioni = new EstrazioneFormedilImpresaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiEstrazioneFormedilImprese"))
            {
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceCorso = reader.GetOrdinal("corso");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceTipoContratto = reader.GetOrdinal("tipoContratto");
                    int indiceTipoIscrizione = reader.GetOrdinal("tipoIscrizione");
                    int indiceNumeroDipendenti = reader.GetOrdinal("numeroDipendenti");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceNumeroLavoratori = reader.GetOrdinal("numeroLavoratori");
                    int indiceDateCorso = reader.GetOrdinal("dateCorso");

                    #endregion

                    while (reader.Read())
                    {
                        EstrazioneFormedilImpresa estrazione = new EstrazioneFormedilImpresa();
                        estrazioni.Add(estrazione);

                        estrazione.Corso = reader.GetString(indiceCorso);
                        estrazione.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            estrazione.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceTipoContratto))
                        {
                            estrazione.TipoContratto = reader.GetInt32(indiceTipoContratto);
                        }
                        if (!reader.IsDBNull(indiceTipoIscrizione))
                        {
                            estrazione.TipoIscrizione = reader.GetInt32(indiceTipoIscrizione);
                        }
                        if (!reader.IsDBNull(indiceNumeroDipendenti))
                        {
                            estrazione.NumeroDipendenti = reader.GetInt32(indiceNumeroDipendenti);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            estrazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            estrazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        estrazione.NumeroLavoratori = reader.GetInt32(indiceNumeroLavoratori);
                        estrazione.DateCorso = reader.GetString(indiceDateCorso);
                    }
                }
            }

            return estrazioni;
        }

        public EstrazioneFormedilLavoratoreCollection EstrazioneFormedilLavoratori(EstrazioneFormedilFilter filtro)
        {
            EstrazioneFormedilLavoratoreCollection estrazioni = new EstrazioneFormedilLavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiEstrazioneFormedilLavoratori"))
            {
                DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal);
                DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceCorso = reader.GetOrdinal("corso");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceSesso = reader.GetOrdinal("sesso");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceCittadinanza = reader.GetOrdinal("cittadinanza");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceProvinciaResidenza = reader.GetOrdinal("provinciaResidenza");
                    int indiceTitoloStudio = reader.GetOrdinal("titoloDiStudio");
                    int indiceAzienda = reader.GetOrdinal("azienda");
                    int indiceDateCorso = reader.GetOrdinal("dateCorso");

                    #endregion

                    while (reader.Read())
                    {
                        EstrazioneFormedilLavoratore estrazione = new EstrazioneFormedilLavoratore();
                        estrazioni.Add(estrazione);

                        estrazione.Corso = reader.GetString(indiceCorso);
                        estrazione.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            estrazione.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            estrazione.Sesso = reader.GetInt32(indiceSesso);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            estrazione.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceCittadinanza))
                        {
                            estrazione.Cittadinanza = reader.GetString(indiceCittadinanza);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            estrazione.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaResidenza))
                        {
                            estrazione.ProvinciaResidenza = reader.GetString(indiceProvinciaResidenza);
                        }
                        if (!reader.IsDBNull(indiceTitoloStudio))
                        {
                            estrazione.TitoloDiStudio = reader.GetInt32(indiceTitoloStudio);
                        }
                        if (!reader.IsDBNull(indiceAzienda))
                        {
                            estrazione.Azienda = reader.GetString(indiceAzienda);
                        }
                        estrazione.DateCorso = reader.GetString(indiceDateCorso);
                    }
                }
            }

            return estrazioni;
        }

        public bool EsisteImpresaConStessaIvaFisc(string partitaIva, string codiceFiscale)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiEsisteImpresaConIvaFisc"))
            {
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, partitaIva);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@esiste", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = (bool) DatabaseCemi.GetParameterValue(comando, "@esiste");
            }

            return res;
        }

        public bool EsisteLavoratoreConStessoCodiceFiscale(string codiceFiscale)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiEsisteLavoratoreConCodiceFiscale"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@esiste", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = (bool) DatabaseCemi.GetParameterValue(comando, "@esiste");
            }

            return res;
        }

        public bool DeleteLocazione(int idLocazione)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLocazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, idLocazione);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool LavoratoreSeguito16Ore(string codiceFiscale)
        {
            bool res = false;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectByCodiceFiscaleFor8Ore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, -1);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscaleP", DbType.String, codiceFiscale);

                string date = DatabaseCemi.ExecuteScalar(comando).ToString();
                if (!string.IsNullOrEmpty(date))
                {
                    res = true;
                }
            }

            return res;
        }

        #region Gestione Prestazioni domande corsi

        /// <summary>
        ///     Ritorna l'elenco delle prestazioni domande corsi generate (filtrate)
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public PrestazioniDomandeCorsiCollection GetPrestazioniDomandeCorsi(PrestazioneDomandaCorsoFilter filtro)
        {
            PrestazioneDomandaCorso domanda = null;
            PrestazioniDomandeCorsiCollection domande = new PrestazioniDomandeCorsiCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandeSelect"))
            {
                if (filtro != null)
                {
                    if (filtro.IdCorsiPrestazioneDomanda.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idCorsiPrestazioneDomanda", DbType.Int32,
                            filtro.IdCorsiPrestazioneDomanda.Value);
                    if (filtro.IdLavoratore.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                    if (!string.IsNullOrEmpty(filtro.Cognome))
                        DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                    if (!string.IsNullOrEmpty(filtro.Nome))
                        DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                    if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                        DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);

                    if (!string.IsNullOrEmpty(filtro.IdTipoStatoPrestazione))
                        DatabaseCemi.AddInParameter(comando, "@idTipoStatoPrestazione", DbType.String,
                            filtro.IdTipoStatoPrestazione);

                    if (filtro.CarpaPrepagataPresente.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@CP", DbType.Boolean,
                            filtro.CarpaPrepagataPresente.Value);
                    if (filtro.CodiceFiscalePresente.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@CF", DbType.Boolean, filtro.CodiceFiscalePresente.Value);
                    if (filtro.DenunciaPresente.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@denuncia", DbType.Boolean,
                            filtro.DenunciaPresente.Value);
                    if (filtro.Iscritto.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@iscritto", DbType.Boolean, filtro.Iscritto.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int iIdCorsiPrestazioneDomanda = reader.GetOrdinal("idCorsiPrestazioneDomanda");
                    int iIdCorsiPartecipazione = reader.GetOrdinal("idCorsiPartecipazione");
                    int iIdLavoratoreBeneficiario = reader.GetOrdinal("idLavoratoreBeneficiario");
                    int iCognome = reader.GetOrdinal("cognome");
                    int iNome = reader.GetOrdinal("nome");
                    int iCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int iDataNascita = reader.GetOrdinal("dataNascita");
                    int iCodiceFiscaleUnico = reader.GetOrdinal("codiceFiscaleUnico");
                    int iDenunciaPresente = reader.GetOrdinal("denunciaPresente");
                    int iCartaPrepagataPresente = reader.GetOrdinal("cartaPrepagataPresente");
                    int iDataLiquidazione = reader.GetOrdinal("dataLiquidazione");
                    int iDataConferma = reader.GetOrdinal("dataConferma");
                    int iIdTipoStatoPrestazione = reader.GetOrdinal("idTipoStatoPrestazione");
                    int iDescrizioneStatoPrestazione = reader.GetOrdinal("descrizioneStatoPrestazione");
                    int iInizioCorso = reader.GetOrdinal("inizioCorso");
                    int iFineCorso = reader.GetOrdinal("fineCorso");

                    int iNumeroProtocolloPrestazione = reader.GetOrdinal("numeroProtocolloPrestazione");
                    int iProtocolloPrestazione = reader.GetOrdinal("protocolloPrestazione");
                    int iPosizioneValida = reader.GetOrdinal("posizioneValida");

                    #endregion

                    while (reader.Read())
                    {
                        domanda = new PrestazioneDomandaCorso();

                        domanda.IdCorsiPrestazioneDomanda = reader.GetInt32(iIdCorsiPrestazioneDomanda);

                        if (!reader.IsDBNull(iIdLavoratoreBeneficiario))
                            domanda.IdLavoratore = reader.GetInt32(iIdLavoratoreBeneficiario);

                        domanda.Cognome = reader.GetString(iCognome);
                        domanda.Nome = reader.GetString(iNome);
                        domanda.DataNascita = reader.GetDateTime(iDataNascita);
                        if (!reader.IsDBNull(iCodiceFiscale))
                            domanda.CodiceFiscale = reader.GetString(iCodiceFiscale);

                        domanda.CodiceFiscaleUnico = reader.GetBoolean(iCodiceFiscaleUnico);
                        domanda.CartaPrepagataPresente = reader.GetBoolean(iCartaPrepagataPresente);
                        domanda.DenunciaPresente = reader.GetBoolean(iDenunciaPresente);

                        domanda.PosizioneValida = reader.GetBoolean(iPosizioneValida);

                        if (!reader.IsDBNull(iDataLiquidazione))
                            domanda.DataLiquidazione = reader.GetDateTime(iDataLiquidazione);
                        if (!reader.IsDBNull(iDataConferma))
                            domanda.DataConferma = reader.GetDateTime(iDataConferma);

                        if (!reader.IsDBNull(iInizioCorso))
                            domanda.InizioCorso = reader.GetDateTime(iInizioCorso);
                        if (!reader.IsDBNull(iFineCorso))
                            domanda.FineCorso = reader.GetDateTime(iFineCorso);

                        if (!reader.IsDBNull(iIdTipoStatoPrestazione))
                        {
                            domanda.Stato = new StatoDomanda(
                                reader.GetString(iIdTipoStatoPrestazione),
                                reader.GetString(iDescrizioneStatoPrestazione)
                            );
                        }

                        if (!reader.IsDBNull(iNumeroProtocolloPrestazione))
                            domanda.NumeroProtocolloPrestazione = reader.GetInt32(iNumeroProtocolloPrestazione);
                        if (!reader.IsDBNull(iProtocolloPrestazione))
                            domanda.ProtocolloPrestazione = reader.GetInt32(iProtocolloPrestazione);

                        domande.Add(domanda);
                    }
                }
            }

            return domande;
        }

        /// <summary>
        ///     Aggiorna lo stato di una do
        /// </summary>
        /// <param name="idCorsiPrestazioneDomanda"></param>
        /// <param name="idTipoStatoPrestazione"></param>
        /// <param name="idUtente"></param>
        public bool AggiornaStatoPrestazioneDomandaCorso(int idCorsiPrestazioneDomanda, string idTipoStatoPrestazione,
            int idUtente)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandeAggiornaStato")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idCorsiPrestazioneDomanda", DbType.Int32,
                    idCorsiPrestazioneDomanda);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@idTipoStatoPrestazione", DbType.String, idTipoStatoPrestazione);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public StatoDomandaCollection GetStatiDomanda()
        {
            StatoDomandaCollection stati = new StatoDomandaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniStatiDomandaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdStato = reader.GetOrdinal("idPrestazioniStatoDomanda");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        StatoDomanda stato = new StatoDomanda();
                        stati.Add(stato);

                        stato.IdStato = reader.GetString(indiceIdStato);
                        stato.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return stati;
        }

        /// <summary>
        ///     Permette l'aggiornamento del beneficiario della prestazione
        /// </summary>
        /// <returns></returns>
        public bool AggiornaLavoratoreBeneficiarioDomandaPrestazione(int idCorsiPrestazioneDomanda, int idLavoratore,
            int idUtente)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandeAggiornaLavoratore")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idCorsiPrestazioneDomanda", DbType.Int32,
                    idCorsiPrestazioneDomanda);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Get Lookup Collection

        public TitoloDiStudioCollection GetTitoliDiStudioAll()
        {
            TitoloDiStudioCollection titoliStudio = new TitoloDiStudioCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiTitoliDiStudioSelectAll"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdTitoloStudio = reader.GetOrdinal("idCorsiTitoloDiStudio");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TitoloDiStudio titoloStudio = new TitoloDiStudio();
                        titoliStudio.Add(titoloStudio);

                        titoloStudio.IdTitoloStudio = reader.GetInt32(indiceIdTitoloStudio);
                        titoloStudio.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return titoliStudio;
        }

        public TipoIscrizioneCollection GetTipiIscrizioneAll()
        {
            TipoIscrizioneCollection tipiIscrizione = new TipoIscrizioneCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiTipiIscrizioneSelectAll"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdTipoIscrizione = reader.GetOrdinal("idCorsiTipoIscrizione");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoIscrizione tipoIscrizione = new TipoIscrizione();
                        tipiIscrizione.Add(tipoIscrizione);

                        tipoIscrizione.IdTipoIscrizione = reader.GetInt32(indiceIdTipoIscrizione);
                        tipoIscrizione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiIscrizione;
        }

        public TipoContrattoCollection GetTipiContrattoAll()
        {
            TipoContrattoCollection tipiContratto = new TipoContrattoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiTipiContrattoSelectAll"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdTipoContratto = reader.GetOrdinal("idCorsiTipoContratto");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoContratto tipoContratto = new TipoContratto();
                        tipiContratto.Add(tipoContratto);

                        tipoContratto.IdTipoContratto = reader.GetInt32(indiceIdTipoContratto);
                        tipoContratto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiContratto;
        }

        #endregion

        #region Gestione Lavoratori Impresa

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriSiceNewSelect"))
            {
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!string.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (filtro.DataNascita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                }
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceComuneNascita = reader.GetOrdinal("luogoNascita");
                    int indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    int indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    int indiceIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    int indiceIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    int indiceOreDenunciate = reader.GetOrdinal("oreDenunciate");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.i
                        lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.IndirizzoDenominazione = reader.GetString(indiceIndirizzoDenominazione);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoComune))
                        {
                            lavoratore.IndirizzoComune = reader.GetString(indiceIndirizzoComune);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoProvincia))
                        {
                            lavoratore.IndirizzoProvincia = reader.GetString(indiceIndirizzoProvincia);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoCap))
                        {
                            lavoratore.IndirizzoCap = reader.GetString(indiceIndirizzoCap);
                        }

                        lavoratore.OreDenunciate = reader.GetInt32(indiceOreDenunciate);
                    }
                }
            }

            return lavoratori;
        }

        public LavoratoreCollection GetLavoratoriAnagraficaCondivisa(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriAnagraficaCondivisaSelect"))
            {
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!string.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (filtro.DataNascita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                }
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                DatabaseCemi.AddInParameter(comando, "@inForza", DbType.Boolean, filtro.InForza);
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("codiceCassaEdile");
                    int indiceIdAnagraficaCondivisa = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceComuneNascita = reader.GetOrdinal("luogoNascita");
                    int indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    int indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    int indiceIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    int indiceIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    int indiceOreDenunciate = reader.GetOrdinal("oreDenunciate");
                    int indiceIdGestore = reader.GetOrdinal("idEnte");
                    int indiceGestore = reader.GetOrdinal("fonte");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.i
                        //lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            //lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        //else
                        //{
                        //    lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        //}
                        lavoratore.IdAnagraficaCondivisa = reader.GetInt32(indiceIdAnagraficaCondivisa);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.IndirizzoDenominazione = reader.GetString(indiceIndirizzoDenominazione);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoComune))
                        {
                            lavoratore.IndirizzoComune = reader.GetString(indiceIndirizzoComune);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoProvincia))
                        {
                            lavoratore.IndirizzoProvincia = reader.GetString(indiceIndirizzoProvincia);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoCap))
                        {
                            lavoratore.IndirizzoCap = reader.GetString(indiceIndirizzoCap);
                        }

                        lavoratore.OreDenunciate = reader.GetInt32(indiceOreDenunciate);

                        int idEnte = reader.GetInt32(indiceIdGestore);
                        if (idEnte == 1)
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        }
                        lavoratore.Gestore = reader.GetString(indiceGestore);
                    }
                }
            }

            return lavoratori;
        }

        public Lavoratore GetLavoratoreAnagraficaCondivisaByKey(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriAnagraficaCondivisaSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("codiceCassaEdile");
                    int indiceIdAnagraficaCondivisa = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceSesso = reader.GetOrdinal("sesso");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indicePaeseNascita = reader.GetOrdinal("idNazionalita");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceComuneNascita = reader.GetOrdinal("luogoNascita");
                    int indiceCodiceCatastaleNascita = reader.GetOrdinal("codiceCatastaleNascita");
                    int indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    int indiceIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                    int indiceIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    int indiceIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                    int indiceOreDenunciate = reader.GetOrdinal("oreDenunciate");
                    int indiceIdGestore = reader.GetOrdinal("idEnte");
                    int indiceGestore = reader.GetOrdinal("fonte");

                    #endregion

                    if (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        //lavoratore.i
                        //lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        lavoratore.IdAnagraficaCondivisa = reader.GetInt32(indiceIdAnagraficaCondivisa);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceSesso))
                        {
                            string sesso = reader.GetString(indiceSesso);
                            if (sesso.Length == 1)
                            {
                                lavoratore.Sesso = sesso[0];
                            }
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indicePaeseNascita))
                        {
                            lavoratore.PaeseNascita = reader.GetString(indicePaeseNascita);
                        }
                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceCatastaleNascita))
                        {
                            lavoratore.CodiceCatastaleComuneNascita = reader.GetString(indiceCodiceCatastaleNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.IndirizzoDenominazione = reader.GetString(indiceIndirizzoDenominazione);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoComune))
                        {
                            lavoratore.IndirizzoComune = reader.GetString(indiceIndirizzoComune);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoProvincia))
                        {
                            lavoratore.IndirizzoProvincia = reader.GetString(indiceIndirizzoProvincia);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoCap))
                        {
                            lavoratore.IndirizzoCap = reader.GetString(indiceIndirizzoCap);
                        }

                        lavoratore.OreDenunciate = reader.GetInt32(indiceOreDenunciate);

                        int idEnte = reader.GetInt32(indiceIdGestore);
                        if (idEnte == 1)
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        }
                        else
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        }
                        lavoratore.Gestore = reader.GetString(indiceGestore);
                    }
                }
            }

            return lavoratore;
        }

        public Lavoratore GetLavoratoreSiceNewByKey(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoreSiceNewSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratore;
        }

        private void InsertImpresa(Impresa impresa, DbTransaction transaction)
        {
            AnagraficaComuneDataProvider anagraficaComuneDataProvider = new AnagraficaComuneDataProvider();
            anagraficaComuneDataProvider.InsertImpresa(impresa, transaction);
            InsertImpresaDatiAggiuntivi(impresa, transaction);
        }

        private bool InsertImpresaDatiAggiuntivi(Impresa impresa, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseDatiAggiuntiviInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                if (impresa.TipoContratto != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipoContratto", DbType.Int32,
                        impresa.TipoContratto.IdTipoContratto);
                }
                if (impresa.TipoIscrizione != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipoIscrizione", DbType.Int32,
                        impresa.TipoIscrizione.IdTipoIscrizione);
                }
                if (impresa.NumeroDipendenti.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@numeroDipendenti", DbType.Int32, impresa.NumeroDipendenti);
                }
                if (!string.IsNullOrEmpty(impresa.ProvinciaSedeLegale))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaSedeLegale", DbType.String,
                        impresa.ProvinciaSedeLegale);
                }
                if (!string.IsNullOrEmpty(impresa.ComuneSedeLegale))
                {
                    DatabaseCemi.AddInParameter(comando, "@comuneSedeLegale", DbType.String, impresa.ComuneSedeLegale);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private void InsertLavoratore(Lavoratore lavoratore, DbTransaction transaction)
        {
            AnagraficaComuneDataProvider anagraficaComuneDataProvider = new AnagraficaComuneDataProvider();
            anagraficaComuneDataProvider.InsertLavoratore(lavoratore, transaction);
            InsertLavoratoreDatiAggiuntivi(lavoratore, transaction);
        }

        private bool InsertLavoratoreDatiAggiuntivi(Lavoratore lavoratore, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriDatiAggiuntiviInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                if (!string.IsNullOrEmpty(lavoratore.Cittadinanza))
                {
                    DatabaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, lavoratore.Cittadinanza);
                }
                if (!string.IsNullOrEmpty(lavoratore.ProvinciaResidenza))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaResidenza", DbType.String,
                        lavoratore.ProvinciaResidenza);
                }
                if (lavoratore.TitoloStudio != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTitoloDiStudio", DbType.Int32,
                        lavoratore.TitoloStudio.IdTitoloStudio);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public LavoratoreCollection GetPartecipanti(PartecipanteFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPartecipazioniSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idModuloProgrammazione", DbType.Int32,
                    filtro.IdProgrammazioneModulo);
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!string.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }
                DatabaseCemi.AddInParameter(comando, "@attestatoRilasciato", DbType.Boolean,
                    filtro.AttestatoRilasciato);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceLuogoNascita = reader.GetOrdinal("LuogoNascita");
                    int indiceCorsiIdLavoratore = reader.GetOrdinal("corsiIdLavoratore");
                    int indiceCorsiCognome = reader.GetOrdinal("corsiCognome");
                    int indiceCorsiNome = reader.GetOrdinal("corsiNome");
                    int indiceCorsiDataNascita = reader.GetOrdinal("corsiDataNascita");
                    int indiceCorsiCodiceFiscale = reader.GetOrdinal("corsiCodiceFiscale");
                    int indiceCorsiLuogoNascita = reader.GetOrdinal("corsiLuogoNascita");
                    int indicePresente = reader.GetOrdinal("presente");
                    int indiceIdPartecipazione = reader.GetOrdinal("idCorsiPartecipazione");
                    int indiceAttestato = reader.GetOrdinal("attestatoRilasciabile");
                    int indicePrenotazioneImpresa = reader.GetOrdinal("prenotazioneImpresa");
                    int indicePrenotazioneConsulente = reader.GetOrdinal("prenotazioneConsulente");
                    int indiceIdPartecipazioneModulo = reader.GetOrdinal("idCorsiModuliPartecipazione");
                    int indiceRegistroConfermato = reader.GetOrdinal("registroConfermato");

                    int indiceIdImpresa = reader.GetOrdinal("impresaIdImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("impresaPartitaIva");
                    int indiceImpCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    int indiceCorsiIdImpresa = reader.GetOrdinal("corsiImpresaIdImpresa");
                    int indiceCorsiRagioneSociale = reader.GetOrdinal("corsiImpresaRagioneSociale");
                    int indiceCorsiPartitaIva = reader.GetOrdinal("corsiImpresaPartitaIva");
                    int indiceCorsiImpCodiceFiscale = reader.GetOrdinal("corsiImpresaCodiceFiscale");
                    int indiceCodiceCorso = reader.GetOrdinal("codiceCorso");
                    int indiceIdCorso = reader.GetOrdinal("idCorso");

                    int indicePrimaEsperienza = reader.GetOrdinal("primaEsperienza");
                    int indiceDataAssunzione = reader.GetOrdinal("dataAssunzione");
                    int indiceDataInizio = reader.GetOrdinal("inizio");
                    int indiceDiritto = reader.GetOrdinal("diritto");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        lavoratore.Presente = reader.GetBoolean(indicePresente);
                        lavoratore.Attestato = reader.GetBoolean(indiceAttestato);
                        lavoratore.PrenotazioneImpresa = reader.GetBoolean(indicePrenotazioneImpresa);
                        lavoratore.PrenotazioneConsulente = reader.GetBoolean(indicePrenotazioneConsulente);
                        lavoratore.RegistroConfermato = reader.GetBoolean(indiceRegistroConfermato);
                        lavoratore.IdPartecipazione = reader.GetInt32(indiceIdPartecipazione);
                        lavoratore.IdPartecipazioneModulo = reader.GetInt32(indiceIdPartecipazioneModulo);
                        lavoratore.CodiceCorso = reader.GetString(indiceCodiceCorso);
                        lavoratore.IdCorso = reader.GetInt32(indiceIdCorso);

                        if (!reader.IsDBNull(indicePrimaEsperienza))
                        {
                            lavoratore.PrimaEsperienza = reader.GetBoolean(indicePrimaEsperienza);
                        }
                        if (!reader.IsDBNull(indiceDataAssunzione))
                        {
                            lavoratore.DataAssunzione = reader.GetDateTime(indiceDataAssunzione);
                        }
                        if (!reader.IsDBNull(indiceDataInizio))
                        {
                            lavoratore.DataInizioCorso = reader.GetDateTime(indiceDataInizio);
                        }
                        if (!reader.IsDBNull(indiceDiritto))
                        {
                            lavoratore.Diritto = reader.GetBoolean(indiceDiritto);
                        }

                        #region Lavoratore

                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            lavoratore.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                            lavoratore.Cognome = reader.GetString(indiceCognome);
                            if (!reader.IsDBNull(indiceNome))
                            {
                                lavoratore.Nome = reader.GetString(indiceNome);
                            }
                            if (!reader.IsDBNull(indiceDataNascita))
                            {
                                lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                            if (!reader.IsDBNull(indiceLuogoNascita))
                            {
                                lavoratore.ComuneNascita = reader.GetString(indiceLuogoNascita);
                            }
                        }
                        else
                        {
                            if (!reader.IsDBNull(indiceCorsiIdLavoratore))
                            {
                                lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                                lavoratore.IdLavoratore = reader.GetInt32(indiceCorsiIdLavoratore);
                                lavoratore.Cognome = reader.GetString(indiceCorsiCognome);
                                if (!reader.IsDBNull(indiceCorsiNome))
                                {
                                    lavoratore.Nome = reader.GetString(indiceCorsiNome);
                                }
                                if (!reader.IsDBNull(indiceCorsiDataNascita))
                                {
                                    lavoratore.DataNascita = reader.GetDateTime(indiceCorsiDataNascita);
                                }
                                if (!reader.IsDBNull(indiceCorsiCodiceFiscale))
                                {
                                    lavoratore.CodiceFiscale = reader.GetString(indiceCorsiCodiceFiscale);
                                }
                                if (!reader.IsDBNull(indiceCorsiLuogoNascita))
                                {
                                    lavoratore.ComuneNascita = reader.GetString(indiceCorsiLuogoNascita);
                                }
                            }
                        }

                        #endregion

                        #region Impresa

                        lavoratore.Impresa = new Impresa();

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            lavoratore.Impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            lavoratore.Impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            lavoratore.Impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        }
                        else
                        {
                            lavoratore.Impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                            lavoratore.Impresa.IdImpresa = reader.GetInt32(indiceCorsiIdImpresa);
                            lavoratore.Impresa.RagioneSociale = reader.GetString(indiceCorsiRagioneSociale);
                        }

                        #endregion
                    }
                }
            }

            return lavoratori;
        }

        public ImpresaCollection GetImpreseSiceNewEAnagrafica(ImpresaFilter filtro)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseSelect"))
            {
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                }
                if (!string.IsNullOrEmpty(filtro.IvaFisc))
                {
                    DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, filtro.IvaFisc);
                }
                if (filtro.IdConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceCorsiIdImpresa = reader.GetOrdinal("corsiIdImpresa");
                    int indiceCorsiRagioneSociale = reader.GetOrdinal("corsiRagioneSociale");
                    int indiceCorsiPartitaIva = reader.GetOrdinal("corsiPartitaIva");
                    int indiceCorsiCodiceFiscale = reader.GetOrdinal("corsiCodiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceIdImpresa))
                        {
                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                            if (!reader.IsDBNull(indicePartitaIva))
                            {
                                impresa.PartitaIva = reader.GetString(indicePartitaIva);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                        }
                        else
                        {
                            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                            impresa.IdImpresa = reader.GetInt32(indiceCorsiIdImpresa);
                            impresa.RagioneSociale = reader.GetString(indiceCorsiRagioneSociale);
                            if (!reader.IsDBNull(indiceCorsiPartitaIva))
                            {
                                impresa.PartitaIva = reader.GetString(indiceCorsiPartitaIva);
                            }
                            if (!reader.IsDBNull(indiceCorsiCodiceFiscale))
                            {
                                impresa.CodiceFiscale = reader.GetString(indiceCorsiCodiceFiscale);
                            }
                        }
                    }
                }
            }

            return imprese;
        }

        public ImpresaCollection GetImpreseAnagraficaCondivisa(ImpresaFilter filtro)
        {
            ImpresaCollection imprese = new ImpresaCollection();

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseAnagraficaCondivisaSelect"))
            {
                if (filtro.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa.Value);
                }
                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);
                }
                if (!string.IsNullOrEmpty(filtro.IvaFisc))
                {
                    DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, filtro.IvaFisc);
                }
                if (filtro.IdConsulente.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, filtro.IdConsulente);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceCodiceCassaEdile = reader.GetOrdinal("codiceCassaEdile");

                    #endregion

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();
                        imprese.Add(impresa);

                        if (!reader.IsDBNull(indiceCodiceCassaEdile))
                        {
                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceCodiceCassaEdile);
                        }
                        else
                        {
                            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        }

                        impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        impresa.IdAnagraficaCondivisa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return imprese;
        }

        public Impresa GetImpresaAnagraficaCondivisaByKey(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpreseAnagraficaCondivisaSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceCodiceCassaEdile = reader.GetOrdinal("codiceCassaEdile");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        impresa = new Impresa();
                        impresa.IdAnagraficaCondivisa = reader.GetInt32(indiceIdImpresa);

                        if (reader.IsDBNull(indiceCodiceCassaEdile))
                        {
                            impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        }
                        else
                        {
                            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                            impresa.IdImpresa = reader.GetInt32(indiceCodiceCassaEdile);
                        }
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresaSiceNewByKey(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpresaSiceNewSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.TipoImpresa = TipologiaImpresa.SiceNew;
                        impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return impresa;
        }

        public Impresa GetImpresaCorsiByKey(int idImpresa)
        {
            Impresa impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiImpresaCorsiSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSociale = reader.GetOrdinal("ragioneSociale");
                    int indicePartitaIva = reader.GetOrdinal("partitaIva");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        impresa = new Impresa();

                        impresa.TipoImpresa = TipologiaImpresa.Anagrafica;
                        impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSociale);
                        if (!reader.IsDBNull(indicePartitaIva))
                        {
                            impresa.PartitaIva = reader.GetString(indicePartitaIva);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            impresa.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return impresa;
        }

        public Lavoratore GetCorsiLavoratore(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indicePaeseNascita = reader.GetOrdinal("paeseNascita");
                    int indiceProvinciaNascita = reader.GetOrdinal("provinciaNascita");
                    int indiceComuneNascita = reader.GetOrdinal("codiceCatastaleComuneNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceSesso = reader.GetOrdinal("sesso");
                    int indiceFonte = reader.GetOrdinal("fonte");
                    int indiceCittadinanza = reader.GetOrdinal("cittadinanza");
                    int indiceProvinciaResidenza = reader.GetOrdinal("provinciaResidenza");
                    int indiceIdTitoloStudio = reader.GetOrdinal("idCorsiTitoloDiStudio");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        lavoratore = new Lavoratore();
                        lavoratore.TipoLavoratore = TipologiaLavoratore.Anagrafica;
                        lavoratore.Fonte = (FonteAnagraficheComuni) reader.GetInt32(indiceFonte);

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        lavoratore.Nome = reader.GetString(indiceNome);
                        lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        lavoratore.Sesso = reader.GetString(indiceSesso)[0];
                        lavoratore.PaeseNascita = reader.GetString(indicePaeseNascita);

                        if (!reader.IsDBNull(indiceProvinciaNascita))
                        {
                            lavoratore.ProvinciaNascita = reader.GetString(indiceProvinciaNascita);
                        }
                        if (!reader.IsDBNull(indiceComuneNascita))
                        {
                            lavoratore.ComuneNascita = reader.GetString(indiceComuneNascita);
                        }
                        if (!reader.IsDBNull(indiceCittadinanza))
                        {
                            lavoratore.Cittadinanza = reader.GetString(indiceCittadinanza);
                        }
                        if (!reader.IsDBNull(indiceProvinciaResidenza))
                        {
                            lavoratore.ProvinciaResidenza = reader.GetString(indiceProvinciaResidenza);
                        }
                        if (!reader.IsDBNull(indiceIdTitoloStudio))
                        {
                            lavoratore.TitoloStudio = new TitoloDiStudio();
                            lavoratore.TitoloStudio.IdTitoloStudio = reader.GetInt32(indiceIdTitoloStudio);
                            lavoratore.TitoloStudio.Descrizione = reader.GetString(indiceDescrizione);
                        }
                    }
                }
            }

            return lavoratore;
        }

        public void GetDatiAggiuntiviPartecipazione(int idPartecipazione, out bool? primaEsperienza,
            out DateTime? dataAssunzione)
        {
            primaEsperienza = null;
            dataAssunzione = null;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniSelectDatiAggiuntivi"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        primaEsperienza = reader["primaEsperienza"] as bool?;
                        dataAssunzione = reader["dataAssunzione"] as DateTime?;
                    }
                }
            }
        }

        public bool UpdateCorsiLavoratore(Lavoratore lavoratore, int idPartecipazione)
        {
            bool res = false;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        AnagraficaComuneDataProvider anagraficaComuneDataProvider = new AnagraficaComuneDataProvider();
                        if (anagraficaComuneDataProvider.UpdateLavoratore(lavoratore, transaction)
                            && UpdateLavoratoreDatiAggiuntivi(lavoratore, transaction)
                            && UpdatePartecipazione(idPartecipazione, lavoratore.PrimaEsperienza,
                                lavoratore.DataAssunzione, transaction))
                        {
                            res = true;
                            transaction.Commit();
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private bool UpdateLavoratoreDatiAggiuntivi(Lavoratore lavoratore, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiLavoratoriDatiAggiuntiviUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                if (!string.IsNullOrEmpty(lavoratore.Cittadinanza))
                {
                    DatabaseCemi.AddInParameter(comando, "@cittadinanza", DbType.String, lavoratore.Cittadinanza);
                }
                if (!string.IsNullOrEmpty(lavoratore.ProvinciaResidenza))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaResidenza", DbType.String,
                        lavoratore.ProvinciaResidenza);
                }
                if (lavoratore.TitoloStudio != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idTitoloDiStudio", DbType.Int32,
                        lavoratore.TitoloStudio.IdTitoloStudio);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdatePartecipazione(int idPartecipazione, bool? primaEsperienza, DateTime? dataAssunzione,
            DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPartecipazioniUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPartecipazione", DbType.Int32, idPartecipazione);
                if (primaEsperienza.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@primaEsperienza", DbType.Boolean, primaEsperienza.Value);
                }
                if (dataAssunzione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime, dataAssunzione.Value);
                }

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public int? GetUltimaImpresaLavoratore(int idLavoratore)
        {
            int? idImpresa = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiRapportiImpresaPersonaSelectUltimoRapporto"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);
                idImpresa = DatabaseCemi.GetParameterValue(comando, "@idImpresa") as int?;
            }

            return idImpresa;
        }

        #endregion

        #region Gestione Programmazione

        public ProgrammazioneModuloCollection GetProgrammazioni(ProgrammazioneModuloFilter filtro)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectByFilter"))
            {
                if (filtro.Dal.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal.Value);
                }
                if (filtro.Al.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al.Value);
                }
                if (filtro.IdCorso.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32, filtro.IdCorso.Value);
                }
                if (filtro.IdLocazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, filtro.IdLocazione.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    int indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    int indiceInizio = reader.GetOrdinal("inizio");
                    int indiceFine = reader.GetOrdinal("fine");
                    int indiceConfermaPresenze = reader.GetOrdinal("registroConfermato");
                    int indiceIdCorso = reader.GetOrdinal("idCorso");
                    int indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    int indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    int indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    int indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    int indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceCap = reader.GetOrdinal("cap");
                    int indiceLatitudine = reader.GetOrdinal("latitudine");
                    int indiceLongitudine = reader.GetOrdinal("longitudine");
                    int indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                        programmazioni.Add(programmazione);

                        programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                        programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);
                        programmazione.PresenzeConfermate = reader.GetBoolean(indiceConfermaPresenze);

                        // Corso
                        programmazione.Corso = new Corso();
                        programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);

                        // Modulo
                        programmazione.Modulo = new Modulo();
                        programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                        // Locazione
                        programmazione.Locazione = new Locazione();
                        programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }
                    }
                }
            }

            return programmazioni;
        }

        public bool InsertProgrammazione(Programmazione programmazione)
        {
            bool res = false;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        using (
                            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniInsert"))
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCorso", DbType.Int32,
                                programmazione.Corso.IdCorso.Value);
                            DatabaseCemi.AddOutParameter(comando, "@idPianificazione", DbType.Int32, 4);
                            DatabaseCemi.ExecuteNonQuery(comando, transaction);
                            programmazione.IdProgrammazione =
                                (int) DatabaseCemi.GetParameterValue(comando, "@idPianificazione");

                            if (programmazione.IdProgrammazione > 0)
                            {
                                res = true;

                                foreach (ProgrammazioneModulo prog in programmazione.ProgrammazioniModuli)
                                {
                                    if (!InsertProgrammazione(prog, programmazione.IdProgrammazione.Value, transaction))
                                    {
                                        res = false;
                                        throw new Exception(
                                            "Errore durante l'inserimento di una programmazione modulo");
                                    }
                                }

                                transaction.Commit();
                            }
                            else
                            {
                                throw new Exception("Errore durante l'inserimento della programmazione");
                            }
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        public bool UpdateProgrammazione(Programmazione programmazione)
        {
            bool res = true;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (ProgrammazioneModulo prog in programmazione.ProgrammazioniModuli)
                        {
                            if (!UpdateProgrammazione(prog, transaction))
                            {
                                res = false;
                                throw new Exception("Errore durante l'aggiornamento di una programmazione modulo");
                            }
                        }
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private bool InsertProgrammazione(ProgrammazioneModulo prog, int idProgrammazione,
            DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPianificazioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idModulo", DbType.Int32, prog.Modulo.IdModulo);
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, prog.Locazione.IdLocazione.Value);
                DatabaseCemi.AddInParameter(comando, "@inizio", DbType.DateTime, prog.Schedulazione);
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, prog.SchedulazioneFine);
                DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.Int32, idProgrammazione);
                DatabaseCemi.AddInParameter(comando, "@maxPartecipanti", DbType.Int32, prog.MaxPartecipanti);
                DatabaseCemi.AddInParameter(comando, "@prenotabile", DbType.Boolean, prog.Prenotabile);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
                else
                {
                    throw new Exception("InsertProgrammazione: Errore nell'inserimento della programmazione modulo");
                }
            }

            return res;
        }

        private bool UpdateProgrammazione(ProgrammazioneModulo prog, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPianificazioniUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idModuliPianificazione", DbType.Int32,
                    prog.IdProgrammazioneModulo.Value);
                DatabaseCemi.AddInParameter(comando, "@idLocazione", DbType.Int32, prog.Locazione.IdLocazione.Value);
                DatabaseCemi.AddInParameter(comando, "@inizio", DbType.DateTime, prog.Schedulazione);
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, prog.SchedulazioneFine);
                DatabaseCemi.AddInParameter(comando, "@maxPartecipanti", DbType.Int32, prog.MaxPartecipanti);
                DatabaseCemi.AddInParameter(comando, "@prenotabile", DbType.Boolean, prog.Prenotabile);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
                else
                {
                    throw new Exception("UpdateProgrammazione: Errore nell'aggiornamento della programmazione modulo");
                }
            }

            return res;
        }

        public ProgrammazioneModuloCollection GetProgrammazione(int idProgrammazione)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniSelectTutteDelCorsoByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.Int32, idProgrammazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdModuliProgrammazione = reader.GetOrdinal("idCorsiModuliPianificazione");
                    int indiceIdProgrammazione = reader.GetOrdinal("idCorsiPianificazione");
                    int indiceInizio = reader.GetOrdinal("inizio");
                    int indiceFine = reader.GetOrdinal("fine");
                    int indiceConfermaPresenze = reader.GetOrdinal("registroConfermato");
                    int indicePartecipanti = reader.GetOrdinal("partecipanti");
                    int indiceMaxPartecipanti = reader.GetOrdinal("maxPartecipanti");
                    int indicePrenotabile = reader.GetOrdinal("prenotabile");
                    int indiceIdCorso = reader.GetOrdinal("idCorso");
                    int indiceCorsoCodice = reader.GetOrdinal("corsoCodice");
                    int indiceCorsoDescrizione = reader.GetOrdinal("corsoDescrizione");
                    int indiceCorsoIscrizioneLibera = reader.GetOrdinal("iscrizioneLibera");
                    int indiceIdModulo = reader.GetOrdinal("idCorsiModulo");
                    int indiceModuloDescrizione = reader.GetOrdinal("moduloDescrizione");
                    int indiceModuloDurata = reader.GetOrdinal("moduloDurata");
                    int indiceModuloProgressivo = reader.GetOrdinal("moduloProgressivo");
                    int indiceIdLocazione = reader.GetOrdinal("idCorsiLocazione");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");
                    int indiceCap = reader.GetOrdinal("cap");
                    int indiceLatitudine = reader.GetOrdinal("latitudine");
                    int indiceLongitudine = reader.GetOrdinal("longitudine");
                    int indiceNote = reader.GetOrdinal("note");

                    #endregion

                    while (reader.Read())
                    {
                        ProgrammazioneModulo programmazione = new ProgrammazioneModulo();
                        programmazioni.Add(programmazione);

                        programmazione.IdProgrammazioneModulo = reader.GetInt32(indiceIdModuliProgrammazione);
                        programmazione.IdProgrammazione = reader.GetInt32(indiceIdProgrammazione);
                        programmazione.Schedulazione = reader.GetDateTime(indiceInizio);
                        programmazione.SchedulazioneFine = reader.GetDateTime(indiceFine);
                        programmazione.PresenzeConfermate = reader.GetBoolean(indiceConfermaPresenze);
                        programmazione.Partecipanti = reader.GetInt32(indicePartecipanti);
                        programmazione.MaxPartecipanti = reader.GetInt32(indiceMaxPartecipanti);
                        programmazione.Prenotabile = reader.GetBoolean(indicePrenotabile);

                        // Corso
                        programmazione.Corso = new Corso();
                        programmazione.Corso.IdCorso = reader.GetInt32(indiceIdCorso);
                        programmazione.Corso.Codice = reader.GetString(indiceCorsoCodice);
                        programmazione.Corso.Descrizione = reader.GetString(indiceCorsoDescrizione);
                        programmazione.Corso.IscrizioneLibera = reader.GetBoolean(indiceCorsoIscrizioneLibera);

                        // Modulo
                        programmazione.Modulo = new Modulo();
                        programmazione.Modulo.IdModulo = reader.GetInt32(indiceIdModulo);
                        programmazione.Modulo.Descrizione = reader.GetString(indiceModuloDescrizione);
                        programmazione.Modulo.OreDurata = reader.GetInt16(indiceModuloDurata);
                        programmazione.Modulo.Progressivo = reader.GetInt16(indiceModuloProgressivo);

                        // Locazione
                        programmazione.Locazione = new Locazione();
                        programmazione.Locazione.IdLocazione = reader.GetInt32(indiceIdLocazione);
                        programmazione.Locazione.Indirizzo = reader.GetString(indiceIndirizzo);
                        if (!reader.IsDBNull(indiceComune))
                        {
                            programmazione.Locazione.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            programmazione.Locazione.Provincia = reader.GetString(indiceProvincia);
                        }
                        if (!reader.IsDBNull(indiceCap))
                        {
                            programmazione.Locazione.Cap = reader.GetString(indiceCap);
                        }
                        if (!reader.IsDBNull(indiceLatitudine))
                        {
                            programmazione.Locazione.Latitudine = reader.GetDecimal(indiceLatitudine);
                        }
                        if (!reader.IsDBNull(indiceLongitudine))
                        {
                            programmazione.Locazione.Longitudine = reader.GetDecimal(indiceLongitudine);
                        }
                        if (!reader.IsDBNull(indiceNote))
                        {
                            programmazione.Locazione.Note = reader.GetString(indiceNote);
                        }
                    }
                }
            }

            return programmazioni;
        }

        public bool UpdateProgrammazioneModuloConfermaPresenze(int idProgrammazioneModulo)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiModuliPianificazioniUpdateConfermaPresenze"))
            {
                DatabaseCemi.AddInParameter(comando, "@idProgrammazioneModulo", DbType.Int32, idProgrammazioneModulo);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool DeleteProgrammazione(int idProgrammazione)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPianificazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.Int32, idProgrammazione);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = true;
            }

            return res;
        }

        #endregion

        #region Anagrafica Unica

        public Type.DataSets.Corsi AnagraficaUnicaRicercaCorsi(AnagraficaUnicaCorsiFilter filtro)
        {
            Type.DataSets.Corsi dsCorsi = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiCemiSelect"))
            {
                comando.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (filtro.IdPianificazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.String,
                        filtro.IdPianificazione.Value);
                }

                dsCorsi = new Type.DataSets.Corsi();
                DatabaseCemi.LoadDataSet(comando, dsCorsi, "Corsi");
            }

            return dsCorsi;
        }

        public Iscrizioni AnagraficaUnicaRicercaIscrizioni(AnagraficaUnicaIscrizioniFilter filtro)
        {
            Iscrizioni dsIscrizioni = null;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaCorsiLavoratoriImpreseCemiSelect"))
            {
                comando.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (filtro.IdPianificazione.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idPianificazione", DbType.String,
                        filtro.IdPianificazione.Value);
                }

                dsIscrizioni = new Iscrizioni();
                DatabaseCemi.LoadDataSet(comando, dsIscrizioni, new string[2] {"Lavoratori", "Imprese"});
            }

            return dsIscrizioni;
        }

        public Lavoratori AnagraficaUnicaRicercaLavoratori(AnagraficaUnicaLavoratoriFilter filtro)
        {
            Lavoratori dsLavoratori = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaLavoratoriSelect"))
            {
                comando.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (!string.IsNullOrEmpty(filtro.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, filtro.CodiceFiscale);
                }

                dsLavoratori = new Lavoratori();
                DatabaseCemi.LoadDataSet(comando, dsLavoratori, "Lavoratori");
            }

            return dsLavoratori;
        }

        public Imprese AnagraficaUnicaRicercaImprese(AnagraficaUnicaImpreseFilter filtro)
        {
            Imprese dsImprese = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_AnagraficaUnicaImpreseSelect"))
            {
                comando.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["Timeout"]);

                if (!string.IsNullOrEmpty(filtro.CodiceFiscalePartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscalePartitaIva", DbType.String,
                        filtro.CodiceFiscalePartitaIva);
                }

                dsImprese = new Imprese();
                DatabaseCemi.LoadDataSet(comando, dsImprese, "Imprese");
            }

            return dsImprese;
        }

        #endregion

        #region Prestazioni DTA

        public DateTime? DataErogazionePrestazioniDTA(int anno)
        {
            DateTime? dataErogazione = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTASelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddOutParameter(comando, "@erogato", DbType.DateTime, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                dataErogazione = DatabaseCemi.GetParameterValue(comando, "@erogato") as DateTime?;
            }

            return dataErogazione;
        }

        public List<PrestazioneDTA> GetPrestazioniDTALavoratori(int anno, DTAFilter filtro)
        {
            List<PrestazioneDTA> prestazioni = new List<PrestazioneDTA>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTALavoratoriSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                if (filtro != null)
                {
                    if (!string.IsNullOrEmpty(filtro.Cognome))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                    }
                    if (!string.IsNullOrEmpty(filtro.Corso))
                    {
                        DatabaseCemi.AddInParameter(comando, "@corso", DbType.String, filtro.Corso);
                    }
                    if (filtro.Diritto.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@diritto", DbType.Boolean, filtro.Diritto.Value);
                    }
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceDataInizioCorso = reader.GetOrdinal("dataInizioCorso");
                    int indiceDataFineCorso = reader.GetOrdinal("dataFineCorso");
                    int indiceCodiceCorso = reader.GetOrdinal("codiceCorso");
                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceDataErogazione = reader.GetOrdinal("dataErogazione");
                    int indiceSelezionato = reader.GetOrdinal("selezionato");
                    int indiceDenunciaPresente = reader.GetOrdinal("denunciaPresente");
                    int indiceIbanPresente = reader.GetOrdinal("ibanPresente");
                    int indiceProtocolloPrestazione = reader.GetOrdinal("protocolloPrestazione");
                    int indiceNumeroProtocolloPrestazione = reader.GetOrdinal("numeroProtocolloPrestazione");
                    int indiceIdStatoProtocolloPrestazione = reader.GetOrdinal("idTipoStatoPrestazione");
                    int indiceDescrizioneStatoProtocolloPrestazione = reader.GetOrdinal("descrizioneStatoPrestazione");

                    #endregion

                    while (reader.Read())
                    {
                        PrestazioneDTA prestazione = new PrestazioneDTA();
                        prestazioni.Add(prestazione);

                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            prestazione.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceCognome))
                        {
                            prestazione.Cognome = reader.GetString(indiceCognome);
                        }
                        if (!reader.IsDBNull(indiceNome))
                        {
                            prestazione.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            prestazione.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceDataInizioCorso))
                        {
                            prestazione.DataInizioCorso = reader.GetDateTime(indiceDataInizioCorso);
                        }
                        if (!reader.IsDBNull(indiceDataFineCorso))
                        {
                            prestazione.DataFineCorso = reader.GetDateTime(indiceDataFineCorso);
                        }
                        if (!reader.IsDBNull(indiceCodiceCorso))
                        {
                            prestazione.CodiceCorso = reader.GetString(indiceCodiceCorso);
                        }
                        if (!reader.IsDBNull(indiceIdLavoratore))
                        {
                            prestazione.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        }
                        if (!reader.IsDBNull(indiceDataErogazione))
                        {
                            prestazione.DataErogazione = reader.GetDateTime(indiceDataErogazione);
                        }
                        if (!reader.IsDBNull(indiceSelezionato))
                        {
                            prestazione.Selezionato = reader.GetBoolean(indiceSelezionato);
                        }
                        if (!reader.IsDBNull(indiceDenunciaPresente))
                        {
                            prestazione.DenunciaPresente = reader.GetBoolean(indiceDenunciaPresente);
                        }
                        if (!reader.IsDBNull(indiceIbanPresente))
                        {
                            prestazione.IbanPresente = reader.GetBoolean(indiceIbanPresente);
                        }
                        if (!reader.IsDBNull(indiceProtocolloPrestazione))
                        {
                            prestazione.ProtocolloPrestazione = reader.GetInt32(indiceProtocolloPrestazione);
                        }
                        if (!reader.IsDBNull(indiceNumeroProtocolloPrestazione))
                        {
                            prestazione.NumeroProtocolloPrestazione =
                                reader.GetInt32(indiceNumeroProtocolloPrestazione);
                        }
                        if (!reader.IsDBNull(indiceIdStatoProtocolloPrestazione))
                        {
                            prestazione.IdStato = reader.GetString(indiceIdStatoProtocolloPrestazione);
                        }
                        if (!reader.IsDBNull(indiceDescrizioneStatoProtocolloPrestazione))
                        {
                            prestazione.DescrizioneStato =
                                reader.GetString(indiceDescrizioneStatoProtocolloPrestazione);
                        }
                    }
                }
            }

            return prestazioni;
        }

        public void SalvaPrestazioniDTALavoratori(List<PrestazioneDTALavoratore> prestazioni)
        {
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (PrestazioneDTALavoratore prestazione in prestazioni)
                        {
                            SalvaPrestazioneDTALavoratore(prestazione, transaction);
                        }

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public void SalvaPrestazioneDTALavoratore(PrestazioneDTALavoratore prestazione, DbTransaction transaction)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTALavoratoriInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, prestazione.Anno);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, prestazione.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@selezionato", DbType.Boolean, prestazione.Selezionato);

                if (transaction != null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
            }
        }

        public void GeneraDomandePrestazioniDTALavoratori(int anno)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiPrestazioniDomandaGeneraDTA"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public Dictionary<int, string> GetPrestazioniDTAAnni()
        {
            Dictionary<int, string> anni = new Dictionary<int, string>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CorsiErogazioniDTAAnniSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceAnno = reader.GetOrdinal("anno");

                    #endregion

                    while (reader.Read())
                    {
                        int anno = reader.GetInt32(indiceAnno);
                        anni.Add(anno, string.Format("{0}/{1}", anno, anno + 1));
                    }
                }
            }

            return anni;
        }

        #endregion
    }
}