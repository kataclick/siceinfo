using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Xml.Serialization;
using TBridge.Cemi.Business.Reporting.ReportExecution2005;
using TBridge.Cemi.Business.Reporting.ReportingService2005;
using DataSourceCredentials = TBridge.Cemi.Business.Reporting.ReportingService2005.DataSourceCredentials;
using ReportParameter = TBridge.Cemi.Business.Reporting.ReportingService2005.ReportParameter;
using Warning = TBridge.Cemi.Business.Reporting.ReportExecution2005.Warning;

namespace TBridge.Cemi.Business.Reporting
{
    public class BusinessReporting
    {
        public byte[] CreaPdfReport(
            string wsReportingServices2005,
            string wsReportingExecution2005,
            string nomeReport,
            ParameterValue[] parameters
        )
        {
            // Create a new proxy to the web service
            ReportingService2005.ReportingService2005 rs = new ReportingService2005.ReportingService2005
                {Url = wsReportingServices2005};
            ReportExecutionService rsExec = new ReportExecutionService {Url = wsReportingExecution2005};

            // Authenticate to the Web service using Windows credentials
            rs.Credentials = CredentialCache.DefaultCredentials;
            rsExec.Credentials = CredentialCache.DefaultCredentials;

            // Prepare Render arguments
            string historyID = null;
            string deviceInfo = null;
            string format = "PDF";
            byte[] results = null;
            string encoding = string.Empty;
            string mimeType = string.Empty;
            string extension = string.Empty;
            Warning[] warnings = null;
            string[] streamIDs = null;

            // Define variables needed for GetParameters() method
            // Get the report name
            string _reportName = nomeReport;
            string _historyID = null;
            bool _forRendering = false;
            ReportingService2005.ParameterValue[] _values = null;
            DataSourceCredentials[] _credentials = null;
            ReportParameter[] _parameters = null;

            // Get if any parameters needed.
            _parameters = rs.GetReportParameters(_reportName, _historyID, _forRendering, _values, _credentials);

            // Load the selected report.
            ExecutionInfo ei = rsExec.LoadReport(_reportName, historyID);

            // Prepare report parameter.

            // Set the parameters for the report needed.

            ReportExecution2005.ParameterValue[] reportParameters =
                new ReportExecution2005.ParameterValue[parameters.Length];

            int i = 0;
            foreach (ParameterValue par in parameters)
            {
                reportParameters[i] = new ReportExecution2005.ParameterValue();
                reportParameters[i].Label = par.Label;
                reportParameters[i].Name = par.Name;
                reportParameters[i].Value = par.Value;

                i++;
            }

            rsExec.SetExecutionParameters(reportParameters, "it-IT");
            results = rsExec.Render(format, deviceInfo,
                out extension, out encoding,
                out mimeType, out warnings, out streamIDs);

            return results;
        }

        public CatalogItem[] GetReports(string wsReportingServices2005,
            string wsReportingExecution2005)
        {
            // Create a new proxy to the web service
            ReportingService2005.ReportingService2005 rs = new ReportingService2005.ReportingService2005
                {Url = wsReportingServices2005};
            ReportExecutionService rsExec = new ReportExecutionService {Url = wsReportingExecution2005};

            // Authenticate to the Web service using Windows credentials
            rs.Credentials = CredentialCache.DefaultCredentials;
            rsExec.Credentials = CredentialCache.DefaultCredentials;

            CatalogItem[] items = rs.ListChildren("/", true);

            return items;
        }

        #region Nested type: ParameterValue

        [GeneratedCode("System.Xml", "2.0.50727.1433")]
        [Serializable]
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(Namespace = "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
        public class ParameterValue : ParameterValueOrFieldReference
        {
            private string labelField;
            private string nameField;
            private string valueField;

            /// <remarks />
            public string Name
            {
                get => nameField;
                set => nameField = value;
            }

            /// <remarks />
            public string Value
            {
                get => valueField;
                set => valueField = value;
            }

            /// <remarks />
            public string Label
            {
                get => labelField;
                set => labelField = value;
            }
        }

        #endregion

        #region Nested type: ParameterValueOrFieldReference

        /// <remarks />
        [XmlInclude(typeof(ParameterValue))]
        [GeneratedCode("System.Xml", "2.0.50727.1433")]
        [Serializable]
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(Namespace = "http://schemas.microsoft.com/sqlserver/2005/06/30/reporting/reportingservices")]
        public class ParameterValueOrFieldReference
        {
        }

        #endregion
    }
}