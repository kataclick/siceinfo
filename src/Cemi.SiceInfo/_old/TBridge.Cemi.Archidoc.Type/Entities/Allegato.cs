﻿namespace TBridge.Cemi.Archidoc.Type.Entities
{
    public class Allegato
    {
        public string AllegatoGuId { set; get; }
        public string CardGuid { set; get; }
        public bool Interno { set; get; }
        public int Codice { set; get; }
        public string NomeFile { set; get; }
        public string Note { set; get; }
        public byte[] FileByteArray { set; get; }
    }
}