﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    internal class Durc : Documento
    {
        private string RagioneSociale { set; get; }
        private string CodiceImpresa { set; get; }
        private string NumeroProtocolloDocumento { set; get; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            string ret;
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    ret = RagioneSociale;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    ret = CodiceFiscale;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    ret = CodiceImpresa;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    ret = NumeroProtocolloDocumento;
                    break;
                case "svIfDateDoc":
                case "svwsIfDateDoc":
                    if (DataInserimentoRecord.HasValue)
                        ret = DataInserimentoRecord.Value.ToString("dd/MM/yyyy");
                    else
                        ret = valoreOriginale;
                    break;
                default:
                    ret = base.Get(archidocKey, valoreOriginale);
                    break;
            }

            return ret;
        }

        public override void Set(string archidocKey, string valore)
        {
            DateTime data;
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    RagioneSociale = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    CodiceFiscale = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    CodiceImpresa = valore;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    NumeroProtocolloDocumento = valore;
                    break;
                case "svIfDateDoc":
                case "svwsIfDateDoc":
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataInserimentoRecord = data;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}