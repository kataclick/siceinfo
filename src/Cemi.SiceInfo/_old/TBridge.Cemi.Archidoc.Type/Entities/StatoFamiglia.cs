﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public class StatoFamiglia : Documento
    {
        public int? IdPrestazioniDomanda { set; get; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    return Cognome;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    return Nome;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    if (DataNascita.HasValue)
                        return DataNascita.Value.ToString("dd/MM/yyyy");
                    else return valoreOriginale;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    return IdPrestazioniDomanda.HasValue ? IdPrestazioniDomanda.Value.ToString() : valoreOriginale;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    if (IdLavoratore.HasValue)
                        return IdLavoratore.Value.ToString();
                    else return valoreOriginale;
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    return CodiceFiscale;
                    break;
                default:
                    return base.Get(archidocKey, valoreOriginale);
                    break;
            }
        }

        public override void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    Nome = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    DateTime data;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    int idPrest;
                    if (int.TryParse(valore, out idPrest))
                    {
                        IdPrestazioniDomanda = idPrest;
                    }
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    int id;
                    if (int.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    CodiceFiscale = valore;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}