﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public class CodiceFiscale : Documento
    {
        public int? IdFamiliare { get; set; }
        public int? IdPrestazioniDomanda { set; get; }

        /// <summary>
        ///     Metodo Get: in funzione dell'archidocKey richiesta, il metodo ritorna il valore del campo corretto. Se per tale key
        ///     non esiste un campo nella classe viene restituito il valore originale
        /// </summary>
        /// <param name="archidocKey"></param>
        /// <param name="valoreOriginale"></param>
        /// <returns></returns>
        public override string Get(string archidocKey, string valoreOriginale)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    return Cognome;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    return Nome;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    if (DataNascita.HasValue)
                        return DataNascita.Value.ToString("dd/MM/yyyy");
                    else return valoreOriginale;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    if (IdLavoratore.HasValue)
                        return IdLavoratore.Value.ToString();
                    else return valoreOriginale;
                    break;

                case "svIfKey34":
                case "svwsIfKey34":
                    return IdPrestazioniDomanda.HasValue ? IdPrestazioniDomanda.Value.ToString() : valoreOriginale;
                    break;

                case "svIfKey41":
                case "svwsIfKey41":
                    if (IdFamiliare.HasValue)
                        return IdFamiliare.Value.ToString();
                    else return valoreOriginale;
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    return CodiceFiscale;
                    break;
                default:
                    return base.Get(archidocKey, valoreOriginale);
                    break;
            }
        }

        /// <summary>
        ///     Metodo Set: in funzione dell'archidocKey setta il valore al campo della classe
        /// </summary>
        /// <param name="archidocKey"></param>
        /// <param name="valore">Valore da settare</param>
        public override void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    Nome = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    DateTime data;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    int id;
                    if (int.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    int idPrest;
                    if (int.TryParse(valore, out idPrest))
                    {
                        IdPrestazioniDomanda = idPrest;
                    }
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    int idFam;
                    if (!string.IsNullOrEmpty(valore) && int.TryParse(valore, out idFam))
                        IdFamiliare = idFam;
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    CodiceFiscale = valore;
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}