﻿using System;
using System.Globalization;

namespace TBridge.Cemi.Archidoc.Type.Entities
{
    [Serializable]
    public class DichiarazioneIntervento : Documento
    {
        public string IdTipoPrestazione { get; set; }
        public int? ProtocolloPrestazione { get; set; }
        public int? NumeroProtocolloPrestazione { get; set; }
        public int? IdPrestazioniDomanda { get; set; }
        public int? IdFamiliare { get; set; }

        public override string Get(string archidocKey, string valoreOriginale)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    return Cognome;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    return Nome;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    if (DataNascita.HasValue)
                        return DataNascita.Value.ToString("dd/MM/yyyy");
                    else return valoreOriginale;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    if (IdLavoratore.HasValue)
                        return IdLavoratore.Value.ToString();
                    else return valoreOriginale;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    return ConvertIdTipoPrestazioneToArchidoc(IdTipoPrestazione);
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    return IdFamiliare.HasValue ? IdFamiliare.Value.ToString() : valoreOriginale;
                    break;
                case "svIfKey43":
                case "svwsIfKey43":
                    return CodiceFiscale;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    return NumeroProtocolloPrestazione.ToString();
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    return ProtocolloPrestazione.ToString();
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    return IdPrestazioniDomanda.HasValue ? IdPrestazioniDomanda.Value.ToString() : valoreOriginale;
                    break;
                default:
                    return base.Get(archidocKey, valoreOriginale);
                    break;
            }
        }

        public override void Set(string archidocKey, string valore)
        {
            switch (archidocKey)
            {
                case "svIfKey11":
                case "svwsIfKey11":
                    Cognome = valore;
                    break;
                case "svIfKey12":
                case "svwsIfKey12":
                    Nome = valore;
                    break;
                case "svIfKey21":
                case "svwsIfKey21":
                    DateTime data;
                    if (DateTime.TryParseExact(valore, "dd/MM/yyyy", null, DateTimeStyles.None, out data))
                        DataNascita = data;
                    break;
                case "svIfKey22":
                case "svwsIfKey22":
                    int id;
                    if (int.TryParse(valore, out id))
                        IdLavoratore = id;
                    break;
                case "svIfKey41":
                case "svwsIfKey41":
                    IdTipoPrestazione = ConvertIdTipoPrestazioneFromArchidoc(valore);
                    break;
                case "svIfKey42":
                case "svwsIfKey42":
                    int idFam;
                    if (!string.IsNullOrEmpty(valore) && int.TryParse(valore, out idFam))
                        IdFamiliare = idFam;
                    break;
                case "svIfKey43":
                case "svwsIfKey43":
                    CodiceFiscale = valore;
                    break;
                case "svIfKey31":
                case "svwsIfKey31":
                    int numerop;
                    if (int.TryParse(valore, out numerop))
                        NumeroProtocolloPrestazione = numerop;
                    break;
                case "svIfKey32":
                case "svwsIfKey32":
                    int prot;
                    if (int.TryParse(valore, out prot))
                        ProtocolloPrestazione = prot;
                    break;
                case "svIfKey34":
                case "svwsIfKey34":
                    int idPrest;
                    if (int.TryParse(valore, out idPrest))
                    {
                        IdPrestazioniDomanda = idPrest;
                    }
                    break;
                default:
                    base.Set(archidocKey, valore);
                    break;
            }
        }
    }
}