﻿using System;

namespace TBridge.Cemi.Archidoc.Type.Exceptions
{
    public class ArchidocWebServiceException : Exception
    {
        public ArchidocWebServiceException(string message)
            : base(message)
        {
        }
    }
}