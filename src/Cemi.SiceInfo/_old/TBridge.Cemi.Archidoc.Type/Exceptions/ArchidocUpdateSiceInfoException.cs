﻿using System;

namespace TBridge.Cemi.Archidoc.Type.Exceptions
{
    public class ArchidocUpdateSiceInfoException : Exception
    {
        public ArchidocUpdateSiceInfoException(string message)
            : base(message)
        {
        }
    }
}