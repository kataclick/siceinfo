﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Archidoc.Type.Entities;

namespace TBridge.Cemi.Archidoc.Type.Collection
{
    [Serializable]
    public class LibroUnicoCollection : List<LibroUnico>
    {
    }
}