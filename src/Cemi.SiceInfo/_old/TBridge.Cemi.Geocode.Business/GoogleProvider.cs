﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Text;
using System.Xml;
using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Geocode.Business
{
    internal class GoogleProvider
    {
        private static readonly string Key = ConfigurationManager.AppSettings["GoogleKey"];

        //// Versione 2
        //internal static List<Indirizzo> Geocode(String address)
        //{
        //    List<Indirizzo> indirizzi = new List<Indirizzo>();

        //    try
        //    {
        //        WebClient webClient = new WebClient { Encoding = Encoding.UTF8 };
        //        string urlRequest = string.Format("http://maps.google.com/maps/geo?q={0}&output=xml&hl=it&gl=it&key={1}",
        //                                          address, Key);

        //        string page = webClient.DownloadString(urlRequest);
        //        XmlDocument xmlDocument = new XmlDocument();

        //        xmlDocument.LoadXml(page);
        //        XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDocument.NameTable);
        //        nsManager.AddNamespace("a", "http://earth.google.com/kml/2.0");
        //        XmlNode respCodeNode =
        //            xmlDocument.SelectSingleNode("/a:kml/a:Response/a:Status/a:code/text()", nsManager);
        //        string respCode = respCodeNode.Value;
        //        if (respCode == "200")
        //        {
        //            XmlNodeList coordNodeList = xmlDocument.SelectNodes("/a:kml/a:Response/a:Placemark", nsManager);
        //            if (coordNodeList != null)
        //            {
        //                for (int i = 1; i <= coordNodeList.Count; i++)
        //                {
        //                    Indirizzo indirizzo = new Indirizzo();

        //                    nsManager.AddNamespace("b", "urn:oasis:names:tc:ciq:xsdschema:xAL:2.0");

        //                    XmlNode indirizzoNode = xmlDocument.SelectSingleNode(
        //                        string.Format(
        //                            "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:Thoroughfare/b:ThoroughfareName/text()",
        //                            i), nsManager);
        //                    if (indirizzoNode == null)
        //                    {
        //                        indirizzoNode =
        //                            xmlDocument.SelectSingleNode(
        //                                string.Format(
        //                                    "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:DependentLocality/b:Thoroughfare/b:ThoroughfareName/text()",
        //                                    i), nsManager);
        //                    }
        //                    if (indirizzoNode == null)
        //                    {
        //                        indirizzoNode =
        //                            xmlDocument.SelectSingleNode(
        //                                string.Format(
        //                                    "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:AddressLine/text()",
        //                                    i), nsManager);
        //                    }
        //                    string ind = indirizzoNode.Value;
        //                    string[] indArr = ind.Split(',');
        //                    if (indArr.Length == 2)
        //                        indirizzo.Civico = indArr[1].Trim();
        //                    indirizzo.NomeVia = indArr[0].Trim();

        //                    XmlNode capNode =
        //                        xmlDocument.SelectSingleNode(
        //                            string.Format(
        //                                "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:PostalCode/b:PostalCodeNumber/text()",
        //                                i),
        //                            nsManager);
        //                    if (capNode != null)
        //                    {
        //                        string cap = capNode.Value;
        //                        indirizzo.Cap = cap;
        //                    }
        //                    else
        //                    {
        //                        XmlNode capNodeInner =
        //                            xmlDocument.SelectSingleNode(
        //                                string.Format(
        //                                    "a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:DependentLocality/b:PostalCode/b:PostalCodeNumber/text()",
        //                                    i),
        //                                nsManager);
        //                        if (capNodeInner != null)
        //                        {
        //                            string cap = capNodeInner.Value;
        //                            indirizzo.Cap = cap;
        //                        }
        //                    }

        //                    XmlNode comuneNode =
        //                        xmlDocument.SelectSingleNode(
        //                            string.Format(
        //                                "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:LocalityName/text()",
        //                                i),
        //                            nsManager);
        //                    string comune = comuneNode.Value;
        //                    indirizzo.Comune = comune;

        //                    XmlNode provinciaNode =
        //                        xmlDocument.SelectSingleNode(
        //                            string.Format(
        //                                "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:SubAdministrativeAreaName/text()",
        //                                i),
        //                            nsManager);
        //                    string provincia = provinciaNode.Value;
        //                    indirizzo.Provincia = provincia;

        //                    XmlNode coordNode =
        //                        xmlDocument.SelectSingleNode(
        //                            string.Format("/a:kml/a:Response/a:Placemark[{0}]/a:Point/a:coordinates/text()", i),
        //                            nsManager);
        //                    string coord = coordNode.Value;

        //                    string[] coordSplit = coord.Split(',');
        //                    string lat = coordSplit[1];
        //                    string lng = coordSplit[0];

        //                    try
        //                    {
        //                        NumberFormatInfo nfi = new NumberFormatInfo { NumberDecimalSeparator = "." };

        //                        decimal latitude = decimal.Parse(lat, nfi);
        //                        decimal longitude = decimal.Parse(lng, nfi);

        //                        indirizzo.Latitudine = latitude;
        //                        indirizzo.Longitudine = longitude;
        //                    }
        //                    catch (Exception exc)
        //                    {
        //                    }

        //                    indirizzi.Add(indirizzo);
        //                }
        //            }
        //        }
        //    }
        //    catch { }

        //    return indirizzi;
        //}

        // Versione 3
        internal static List<Indirizzo> Geocode(string address)
        {
            List<Indirizzo> indirizzi = new List<Indirizzo>();

            try
            {
                WebClient webClient = new WebClient {Encoding = Encoding.UTF8};
                string urlRequest = string.Format(
                    "http://maps.googleapis.com/maps/api/geocode/xml?address={0}&language=it&sensor=false",
                    address, Key);

                string page = webClient.DownloadString(urlRequest);
                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.LoadXml(page);
                XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDocument.NameTable);
                //nsManager.AddNamespace("a", "http://earth.google.com/kml/2.0");
                XmlNode respCodeNode =
                    xmlDocument.SelectSingleNode("/GeocodeResponse/status/text()", nsManager);
                string respCode = respCodeNode.Value;
                if (!string.IsNullOrWhiteSpace(respCode) && respCode.ToUpper() == "OK")
                {
                    XmlNodeList coordNodeList = xmlDocument.SelectNodes("/GeocodeResponse/result", nsManager);
                    if (coordNodeList != null)
                    {
                        for (int i = 1; i <= coordNodeList.Count; i++)
                        {
                            Indirizzo indirizzo = new Indirizzo();

                            //nsManager.AddNamespace("b", "urn:oasis:names:tc:ciq:xsdschema:xAL:2.0");

                            XmlNode indirizzoNode = xmlDocument.SelectSingleNode(
                                string.Format(
                                    "/GeocodeResponse/result[{0}]/address_component[type='route']/long_name/text()",
                                    i), nsManager);
                            //if (indirizzoNode == null)
                            //{
                            //    indirizzoNode =
                            //        xmlDocument.SelectSingleNode(
                            //            string.Format(
                            //                "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:DependentLocality/b:Thoroughfare/b:ThoroughfareName/text()",
                            //                i), nsManager);
                            //}
                            //if (indirizzoNode == null)
                            //{
                            //    indirizzoNode =
                            //        xmlDocument.SelectSingleNode(
                            //            string.Format(
                            //                "/a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:AddressLine/text()",
                            //                i), nsManager);
                            //}
                            //string ind = indirizzoNode.Value;
                            //string[] indArr = ind.Split(',');
                            //if (indArr.Length == 2)
                            //    indirizzo.Civico = indArr[1].Trim();
                            indirizzo.NomeVia = indirizzoNode.Value;

                            XmlNode civicoNode = xmlDocument.SelectSingleNode(
                                string.Format(
                                    "/GeocodeResponse/result[{0}]/address_component[type='street_number']/long_name/text()",
                                    i), nsManager);
                            indirizzo.Civico = civicoNode.Value;

                            XmlNode capNode =
                                xmlDocument.SelectSingleNode(
                                    string.Format(
                                        "/GeocodeResponse/result[{0}]/address_component[type='postal_code']/long_name/text()",
                                        i),
                                    nsManager);
                            if (capNode != null)
                            {
                                string cap = capNode.Value;
                                indirizzo.Cap = cap;
                            }
                            //else
                            //{
                            //    XmlNode capNodeInner =
                            //        xmlDocument.SelectSingleNode(
                            //            string.Format(
                            //                "a:kml/a:Response/a:Placemark[{0}]/b:AddressDetails/b:Country/b:AdministrativeArea/b:SubAdministrativeArea/b:Locality/b:DependentLocality/b:PostalCode/b:PostalCodeNumber/text()",
                            //                i),
                            //            nsManager);
                            //    if (capNodeInner != null)
                            //    {
                            //        string cap = capNodeInner.Value;
                            //        indirizzo.Cap = cap;
                            //    }
                            //}

                            XmlNode comune1Node =
                                xmlDocument.SelectSingleNode(
                                    string.Format(
                                        "/GeocodeResponse/result[{0}]/address_component[type='administrative_area_level_3']/long_name/text()",
                                        i),
                                    nsManager);
                            if (comune1Node != null && !string.IsNullOrWhiteSpace(comune1Node.Value))
                            {
                                string comune = comune1Node.Value;
                                indirizzo.Comune = comune;
                            }
                            else
                            {
                                XmlNode comune2Node =
                                    xmlDocument.SelectSingleNode(
                                        string.Format(
                                            "/GeocodeResponse/result[{0}]/address_component[type='locality']/long_name/text()",
                                            i),
                                        nsManager);
                                if (comune2Node != null && !string.IsNullOrWhiteSpace(comune2Node.Value))
                                {
                                    string comune = comune2Node.Value;
                                    indirizzo.Comune = comune;
                                }
                            }

                            XmlNode provinciaNode =
                                xmlDocument.SelectSingleNode(
                                    string.Format(
                                        "/GeocodeResponse/result[{0}]/address_component[type='administrative_area_level_2']/short_name/text()",
                                        i),
                                    nsManager);
                            string provincia = provinciaNode.Value;
                            indirizzo.Provincia = provincia;

                            XmlNode coordLatNode =
                                xmlDocument.SelectSingleNode(
                                    string.Format("/GeocodeResponse/result[{0}]/geometry/location/lat/text()", i),
                                    nsManager);
                            string coordLat = coordLatNode.Value;

                            XmlNode coordLongNode =
                                xmlDocument.SelectSingleNode(
                                    string.Format("/GeocodeResponse/result[{0}]/geometry/location/lng/text()", i),
                                    nsManager);
                            string coordLong = coordLongNode.Value;

                            //string[] coordSplit = coord.Split(',');
                            //string lat = coordSplit[1];
                            //string lng = coordSplit[0];

                            try
                            {
                                NumberFormatInfo nfi = new NumberFormatInfo {NumberDecimalSeparator = "."};

                                decimal latitude = decimal.Parse(coordLat, nfi);
                                decimal longitude = decimal.Parse(coordLong, nfi);

                                indirizzo.Latitudine = latitude;
                                indirizzo.Longitudine = longitude;
                            }
                            catch (Exception exc)
                            {
                            }

                            indirizzi.Add(indirizzo);
                        }
                    }
                }
            }
            catch
            {
            }

            return indirizzi;
        }
    }
}