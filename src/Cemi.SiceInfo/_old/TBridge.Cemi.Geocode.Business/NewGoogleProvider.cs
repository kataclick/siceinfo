﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geocoding.Google;
using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Geocode.Business
{
    public class NewGoogleProvider
    {
        public static List<Indirizzo> Geocode(string indirizzo)
        {
            List<Indirizzo> ret = new List<Indirizzo>();

            try
            {
                GoogleGeocoder geocoder = new GoogleGeocoder(ConfigurationManager.AppSettings["GoogleMapApiKey"]);
                IEnumerable<GoogleAddress> addresses = geocoder.GeocodeAsync(indirizzo).Result;

                foreach (var address in addresses)
                {
                    ret.Add(new Indirizzo
                    {
                        Cap = address[GoogleAddressType.PostalCode]?.LongName,
                        Comune = address[GoogleAddressType.AdministrativeAreaLevel3]?.LongName,
                        Provincia = address[GoogleAddressType.AdministrativeAreaLevel2]?.ShortName,
                        Stato = address[GoogleAddressType.Country]?.LongName,
                        NomeVia = address[GoogleAddressType.Route]?.LongName,
                        Civico = address[GoogleAddressType.StreetNumber]?.LongName,

                        Latitudine = Convert.ToDecimal(address.Coordinates.Latitude),
                        Longitudine = Convert.ToDecimal(address.Coordinates.Longitude)
                    });
                }
            }
            catch (Exception ex)
            {
                //
            }

            return ret;
        }
    }
}
