﻿using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Geocode.Business
{
    public class GeocodeProvider
    {
        private static readonly string GeocodeProviderType = ConfigurationManager.AppSettings["GeocodeProviderType"];

        public static List<Indirizzo> Geocode(string address)
        {
            List<Indirizzo> ret;

            switch (GeocodeProviderType)
            {
                case "Google":
                    ret = GoogleProvider.Geocode(address);
                    break;
                case "GoogleV2":
                    ret = NewGoogleProvider.Geocode(address);
                    break;
                case "Bing":
                    ret = BingProvider.Geocode(address);
                    break;
                default:
                    ret = NewGoogleProvider.Geocode(address);
                    break;
            }

            return ret;
        }
    }
}