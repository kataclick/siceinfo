﻿using System;

namespace Cemi.MalattiaTelematica.Type.Filters
{
    [Serializable]
    public class AssenzeFilter
    {
        public int? IdImpresa { get; set; }
        public string RagioneSocialeImpresa { get; set; }
        public string CodiceFiscaleImpresa { get; set; }
        public int? IdLavoratore { get; set; }
        public string NomeLavoratore { get; set; }
        public string CognomeLavoratore { get; set; }
        public DateTime? DataNascitaLavoratore { get; set; }
        public string StatoAssenza { get; set; }
        public string TipoAssenza { get; set; }
        public DateTime? PeriodoDa { get; set; }
        public DateTime? PeriodoA { get; set; }
        public int? IdAssenza { get; set; }
        public int? IdUtente { get; set; }
        public DateTime? DataInvioDa { get; set; }
        public DateTime? DataInvioA { get; set; }
    }
}