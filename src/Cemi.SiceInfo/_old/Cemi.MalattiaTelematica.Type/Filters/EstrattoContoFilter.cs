﻿using System;

namespace Cemi.MalattiaTelematica.Type.Filters
{
    public class EstrattoContoFilter
    {
        public int? IdImpresa { get; set; }
        public string RagioneSocialeImpresa { get; set; }
        public string CodiceFiscaleImpresa { get; set; }
        public DateTime? PeriodoDa { get; set; }
        public DateTime? PeriodoA { get; set; }
    }
}