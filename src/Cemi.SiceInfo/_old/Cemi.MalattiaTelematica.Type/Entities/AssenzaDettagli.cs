﻿using System;
using TBridge.Cemi.Type.Collections;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class AssenzaDettagli
    {
        //public List<Cemi.MalattiaTelematica.Type.Entities.CasseEdili> CasseEdili;
        public CassaEdileCollection CasseEdili;
        //public AssenzaDettagli(Assenza ass, DateTime? dataAss, DateTime? dataInizioValiditaRapporto, DateTime dataFineValiditaRapporto, string descrizioneCategoria, int? partTimePercentuale)
        //{
        //    //throw new NotImplementedException();
        //    IdLavoratore = ass.IdLavoratore;
        //    IdImpresa = ass.IdImpresa;


        //    dataAssunzione = dataAss;
        //    dataInizio = dataInizioValiditaRapporto;
        //    dataFine = dataFineValiditaRapporto;
        //    categoria = descrizioneCategoria;
        //    percentualePT = partTimePercentuale;

        //}

        public int? IdLavoratore { get; set; }
        public int? IdImpresa { get; set; }
        public string RagioneSocialeImpresa { get; set; }
        public string EmailImpresa { get; set; }
        public int IdAssenza { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string CodiceFiscale { get; set; }
        public DateTime DataNascita { get; set; }
        public string Stato { get; set; }
        public string IdStato { get; set; }
        public string Tipo { get; set; }
        public string IdTipo { get; set; }
        public DateTime? DataInizioMalattia { get; set; }
        public DateTime? DataInizioAssenza { get; set; }
        public DateTime? DataFineAssenza { get; set; }
        public bool? Ricaduta { get; set; }

        public DateTime? DataAssunzione { get; set; }
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public string Categoria { get; set; }
        public int? PercentualePT { get; set; }
        public int? OreSettimanali { get; set; }
        public string Note { get; set; }
        public bool? Telefonata { get; set; }
        public bool? Email { get; set; }

        public DateTime? DataInvio { get; set; }
        public int? IdUtenteInCarico { get; set; }

        public int? NumeroProtocollo { get; set; }
        public int? AnnoProtocollo { get; set; }

        public int? OrePerseDich { get; set; }

        public DateTime? DataInAttesa { get; set; }
        public DateTime? DataInvioPrimoSollecito { get; set; }

        public string TipoAssenza
        {
            get
            {
                string retVal = "";
                if (DataInizioMalattia.Value == DataInizioAssenza.Value)
                {
                    retVal = "Inizio";
                }
                else
                {
                    if (Ricaduta.HasValue && Ricaduta.Value)
                    {
                        retVal = "Ricaduta";
                    }
                    else
                    {
                        retVal = "Continuazione";
                    }
                }

                return retVal;
            }
        }


        public string NomeCompleto => string.Format("{0} {1}", Cognome, Nome);

        public string RapportoLavoro
        {
            get
            {
                string ret = Categoria;

                if (DataInizio.HasValue)
                    ret += string.Format(" dal {0}", DataInizio.Value.ToShortDateString());

                if (DataFine.HasValue)
                    if (DataFine.Value != new DateTime(2079, 6, 6))
                        ret += string.Format(" al {0}", DataFine.Value.ToShortDateString());

                return ret;
            }
        }

        public string PartTime
        {
            get
            {
                string ret = "";

                if (PercentualePT.HasValue)
                    if (PercentualePT != 0)
                        ret += string.Format("Part-time al {0} %", PercentualePT);


                return ret;
            }
        }

        public string Prestazione => string.Format("{0} / {1}", NumeroProtocollo, AnnoProtocollo);


        public bool PresenteCassaEdileNellaLista(string idCassaEdile)
        {
            //foreach (Cemi.MalattiaTelematica.Type.Entities.CasseEdili casseEdili in CasseEdili)
            //{
            //    if (casseEdili.CE.IdCassaEdile == idCassaEdile)
            //    {
            //        return true;
            //    }
            //}
            return false;
        }
    }
}