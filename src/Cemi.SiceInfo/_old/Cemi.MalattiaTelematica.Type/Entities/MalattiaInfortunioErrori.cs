﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class MalattiaInfortunioErrori : AssenzaErrori
    {
        private DateTime dataFineEvento;
        private DateTime dataInizioEvento;


        public MalattiaInfortunioErrori()
        {
        }

        public MalattiaInfortunioErrori(
            int idImpresa,
            string ragioneSociale,
            int idLavoratore,
            string cognome,
            string nome,
            string idCassaEdile,
            string tipoProtocollo,
            int annoProtocollo,
            int numeroProtocollo,
            DateTime dataInizio,
            DateTime dataFine,
            DateTime dataInizioMalattia,
            string descrizione,
            DateTime dataInizioEvento,
            DateTime dataFineEvento
        )
        {
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            IdLavoratore = idLavoratore;
            Cognome = cognome;
            Nome = nome;
            IdCassaEdile = idCassaEdile;
            TipoProtocollo = tipoProtocollo;
            AnnoProtocollo = annoProtocollo;
            NumeroProtocollo = numeroProtocollo;
            DataInizio = dataInizio;
            DataFine = dataFine;
            DataInizioMalattia = dataInizioMalattia;
            Descrizione = descrizione;

            this.dataInizioEvento = dataInizioEvento;
            this.dataFineEvento = dataFineEvento;
        }


        public DateTime DataInizioEvento
        {
            get => dataInizioEvento;
            set => dataInizioEvento = value;
        }

        public DateTime DataFineEvento
        {
            get => dataFineEvento;
            set => dataFineEvento = value;
        }
    }
}