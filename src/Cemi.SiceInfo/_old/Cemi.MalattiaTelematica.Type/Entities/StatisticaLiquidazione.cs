﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class StatisticaLiquidazione
    {
        public StatisticaLiquidazione()
        {
        }

        public StatisticaLiquidazione(
            DateTime data,
            decimal importo,
            int numeroLavoratori,
            int numeroImprese)
        {
            Data = data;
            Importo = importo;
            NumeroLavoratori = numeroLavoratori;
            NumeroImprese = numeroImprese;
        }


        public DateTime Data { get; set; }

        public decimal Importo { get; set; }

        public int NumeroLavoratori { get; set; }

        public int NumeroImprese { get; set; }
    }
}