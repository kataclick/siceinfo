﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class PrestazioneDomandaAssenza
    {
        public PrestazioneDomandaAssenza()
        {
        }

        public PrestazioneDomandaAssenza(
            int idImpresa,
            string ragioneSociale,
            string codiceFiscale,
            string partitaIVA,
            string tipoProtocollo,
            int? annoProtocollo,
            int? numeroProtocollo,
            DateTime? data,
            decimal importo,
            int numero,
            int idAssenza)
        {
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            CodiceFiscale = codiceFiscale;
            PartitaIVA = partitaIVA;
            TipoProtocollo = tipoProtocollo;
            AnnoProtocollo = annoProtocollo;
            NumeroProtocollo = numeroProtocollo;
            Data = data;
            Importo = importo;
            Numero = numero;
            IdAssenza = idAssenza;
        }


        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIVA { get; set; }

        public string TipoProtocollo { get; set; }

        public int? AnnoProtocollo { get; set; }

        public int? NumeroProtocollo { get; set; }

        public DateTime? Data { get; set; }

        public decimal Importo { get; set; }

        public int Numero { get; set; }

        public int IdAssenza { get; set; }
    }
}