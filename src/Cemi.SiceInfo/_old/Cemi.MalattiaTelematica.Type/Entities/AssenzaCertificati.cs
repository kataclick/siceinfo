﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Domain;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class AssenzaCertificati
    {
        public int IdAssenza { get; set; }

        public int? IdLavoratore { get; set; }

        public int? IdImpresa { get; set; }

        public DateTime? InizioMalattia { get; set; }

        public DateTime? Inizio { get; set; }

        public DateTime? Fine { get; set; }

        public Utente Utente { get; set; }

        public Assenza Assenza { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public TipoStatoMalattiaTelematica TipoStatoMalattiaTelematica { get; set; }

        public TipoAssenza TipoAssenza { get; set; }

        public List<MalattiaTelematicaCertificatoMedico> Certificati { get; set; }

        public string Tipo
        {
            get
            {
                string retVal = "";
                if (InizioMalattia.Value == Inizio.Value)
                {
                    retVal = "I";
                }
                else
                {
                    if (Assenza != null)
                    {
                        if (Assenza.Ricaduta.HasValue && Assenza.Ricaduta.Value)
                        {
                            retVal = "R";
                        }
                        else
                        {
                            retVal = "C";
                        }
                    }
                    else
                    {
                        retVal = "C";
                    }
                }

                return retVal;
            }
        }
    }
}