﻿using TBridge.Cemi.Type.Domain;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class CasseEdili
    {
        public MalattiaTelematicaCassaEdile CE { get; set; }

        public string Descrizione { get; set; }

        public bool Cnce { get; set; }
    }
}