﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class AssenzaErrori
    {
        private int annoProtocollo;
        private string cognome;
        private DateTime dataFine;
        private DateTime dataInizio;
        private DateTime dataInizioMalattia;
        private string descrizione;
        private string idCassaEdile;
        private int idImpresa;
        private int idLavoratore;
        private string nome;
        private int numeroProtocollo;
        private string ragioneSociale;
        private string tipoProtocollo;

        public AssenzaErrori()
        {
        }

        public AssenzaErrori(
            int idImpresa,
            string ragioneSociale,
            int idLavoratore,
            string cognome,
            string nome,
            string idCassaEdile,
            string tipoProtocollo,
            int annoProtocollo,
            int numeroProtocollo,
            DateTime dataInizio,
            DateTime dataFine,
            DateTime dataInizioMalattia,
            string descrizione)
        {
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.idLavoratore = idLavoratore;
            this.cognome = cognome;
            this.nome = nome;
            this.idCassaEdile = idCassaEdile;
            this.tipoProtocollo = tipoProtocollo;
            this.annoProtocollo = annoProtocollo;
            this.numeroProtocollo = numeroProtocollo;
            this.dataInizio = dataInizio;
            this.dataFine = dataFine;
            this.dataInizioMalattia = dataInizioMalattia;
            this.descrizione = descrizione;
        }


        public int IdImpresa
        {
            get => idImpresa;
            set => idImpresa = value;
        }

        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        public int IdLavoratore
        {
            get => idLavoratore;
            set => idLavoratore = value;
        }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string IdCassaEdile
        {
            get => idCassaEdile;
            set => idCassaEdile = value;
        }

        public string TipoProtocollo
        {
            get => tipoProtocollo;
            set => tipoProtocollo = value;
        }

        public int AnnoProtocollo
        {
            get => annoProtocollo;
            set => annoProtocollo = value;
        }

        public int NumeroProtocollo
        {
            get => numeroProtocollo;
            set => numeroProtocollo = value;
        }

        public DateTime DataInizio
        {
            get => dataInizio;
            set => dataInizio = value;
        }

        public DateTime DataFine
        {
            get => dataFine;
            set => dataFine = value;
        }

        public DateTime DataInizioMalattia
        {
            get => dataInizioMalattia;
            set => dataInizioMalattia = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }
    }
}