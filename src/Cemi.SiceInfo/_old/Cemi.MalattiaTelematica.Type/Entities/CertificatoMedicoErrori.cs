﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class CertificatoMedicoErrori : AssenzaErrori
    {
        private DateTime dataFineCertificato;
        private DateTime dataInizioCertificato;
        private DateTime dataRilascioCertificato;
        private string numeroCertificato;


        public CertificatoMedicoErrori()
        {
        }

        public CertificatoMedicoErrori(
            int idImpresa,
            string ragioneSociale,
            int idLavoratore,
            string cognome,
            string nome,
            string idCassaEdile,
            string tipoProtocollo,
            int annoProtocollo,
            int numeroProtocollo,
            DateTime dataInizio,
            DateTime dataFine,
            DateTime dataInizioMalattia,
            string descrizione,
            string numeroCertificato,
            DateTime dataInizioCertificato,
            DateTime dataFineCertificato,
            DateTime dataRilascioCertificato
        )
        {
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            IdLavoratore = idLavoratore;
            Cognome = cognome;
            Nome = nome;
            IdCassaEdile = idCassaEdile;
            TipoProtocollo = tipoProtocollo;
            AnnoProtocollo = annoProtocollo;
            NumeroProtocollo = numeroProtocollo;
            DataInizio = dataInizio;
            DataFine = dataFine;
            DataInizioMalattia = dataInizioMalattia;
            Descrizione = descrizione;

            this.numeroCertificato = numeroCertificato;
            this.dataInizioCertificato = dataInizioCertificato;
            this.dataFineCertificato = dataFineCertificato;
            this.dataRilascioCertificato = dataRilascioCertificato;
        }

        public string NumeroCertificato
        {
            get => numeroCertificato;
            set => numeroCertificato = value;
        }

        public DateTime DataInizioCertificato
        {
            get => dataInizioCertificato;
            set => dataInizioCertificato = value;
        }

        public DateTime DataFineCertificato
        {
            get => dataFineCertificato;
            set => dataFineCertificato = value;
        }

        public DateTime DataRilascioCertificato
        {
            get => dataRilascioCertificato;
            set => dataRilascioCertificato = value;
        }
    }
}