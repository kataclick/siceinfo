﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class CertificatiMediciINPS
    {
        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }
        public DateTime DataRilascio { get; set; }

        public string Tipo { get; set; }
    }
}