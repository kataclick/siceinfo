﻿namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class StatisticaEvase
    {
        public StatisticaEvase()
        {
        }

        public StatisticaEvase(
            string stato,
            string utente,
            int numeroAssenze)
        {
            Stato = stato;
            Utente = utente;
            NumeroAssenze = numeroAssenze;
        }


        public string Stato { get; set; }

        public string Utente { get; set; }

        public int NumeroAssenze { get; set; }
    }
}