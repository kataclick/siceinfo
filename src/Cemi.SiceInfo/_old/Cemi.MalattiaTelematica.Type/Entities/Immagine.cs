﻿namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class Immagine
    {
        public string IdArchidoc { get; set; }
        public byte[] File { get; set; }
        public string NomeFile { get; set; }
    }
}