﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class GiustificativiPeriodoGriglia
    {
        public string Tipo { get; set; }
        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }
        public DateTime DataRilascio { get; set; }
        public bool? Ricovero { get; set; }
    }
}