﻿using System;
using System.Collections.Generic;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    [Serializable]
    public class AssenzaGriglia
    {
        public AssenzaGriglia()
        {
            GiustificativiPeriodo = new List<GiustificativiPeriodoGriglia>();

            GiorniNonIndennizzabili = new List<DateTime>();
        }

        public string Tipo { get; set; }
        public DateTime DataInizioAssenza { get; set; }
        public DateTime DataFineAssenza { get; set; }
        public int OrePerseDichiarate { get; set; }

        public List<GiustificativiPeriodoGriglia> GiustificativiPeriodo { get; set; }

        public List<DateTime> GiorniNonIndennizzabili { get; set; }
    }
}