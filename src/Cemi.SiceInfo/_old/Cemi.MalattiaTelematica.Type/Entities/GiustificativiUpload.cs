﻿using System;

namespace Cemi.MalattiaTelematica.Type.Entities
{
    public class GiustificativiUpload
    {
        public int? IdImpresa { get; set; }
        public string RagioneSociale { get; set; }
        public string PartitaIVA { get; set; }
        public string CodiceINPS { get; set; }

        public int? IdLavoratore { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string CodiceFiscale { get; set; }
        public DateTime DataNascita { get; set; }

        public string Tipo { get; set; }
        public string Numero { get; set; }
        public DateTime DataRilascio { get; set; }
        public DateTime DataInizio { get; set; }
        public DateTime DataFine { get; set; }

        public byte[] Documento { get; set; }
        public string NomeDocumento { get; set; }
    }
}