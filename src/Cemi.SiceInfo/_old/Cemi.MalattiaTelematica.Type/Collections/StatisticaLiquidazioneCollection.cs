﻿using System;
using System.Collections.Generic;
using Cemi.MalattiaTelematica.Type.Entities;

namespace Cemi.MalattiaTelematica.Type.Collections
{
    [Serializable]
    public class StatisticaLiquidazioneCollection : List<StatisticaLiquidazione>
    {
    }
}