﻿using System;
using System.Collections.Generic;
using Cemi.MalattiaTelematica.Type.Entities;

namespace Cemi.MalattiaTelematica.Type.Collections
{
    [Serializable]
    public class AssenzaErroriCollection : List<AssenzaErrori>
    {
    }
}