﻿using Cemi.MalattiaTelematica.Type.Filters;

namespace Cemi.MalattiaTelematica.Type.Delegates
{
    public delegate void AssenzeFilterSelectedEventHandler(AssenzeFilter filtro);


    public delegate void CertificatiSelectedEventHandler(int id);

    public delegate void AltriDocumentiSelectedEventHandler();


    public delegate void CertificatiReturnedEventHandler(bool carica);


    public delegate void AltriDocumentiReturnedEventHandler(bool carica);
}