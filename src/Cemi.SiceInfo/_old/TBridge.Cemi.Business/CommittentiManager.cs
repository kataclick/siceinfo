﻿using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Filters;

namespace TBridge.Cemi.Business
{
    public class CommittentiManager
    {
        public List<Committente> GetCommittenti(CommittenteFilter filtro)
        {
            List<Committente> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from committente in context.Committenti
                    where (string.IsNullOrEmpty(filtro.Cognome) || committente.Cognome.Contains(filtro.Cognome))
                          && (string.IsNullOrEmpty(filtro.Nome) || committente.Nome.Contains(filtro.Nome))
                          && (string.IsNullOrEmpty(filtro.RagioneSociale) ||
                              committente.RagioneSociale.Contains(filtro.RagioneSociale))
                          && (string.IsNullOrEmpty(filtro.CodiceFiscale) ||
                              committente.CodiceFiscale.Contains(filtro.CodiceFiscale))
                    select committente;
                ret = query.ToList();
            }

            return ret;
        }

        public Committente GetCommittente(int idCommittente)
        {
            Committente ret = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from committente in context.Committenti
                    where committente.IdCommittente == idCommittente
                    select committente;
                ret = query.Single();
            }

            return ret;
        }
    }
}