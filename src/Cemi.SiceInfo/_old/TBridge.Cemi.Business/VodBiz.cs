﻿using System.Collections.Generic;
using TBridge.Cemi.Data.VOD;
using TBridge.Cemi.Type.Entities.VCOD;
using TBridge.Cemi.Type.Filters.VCOD;

namespace TBridge.Cemi.Business
{
    public class VodBiz
    {
        public static List<ImpresaConFasciaIndirizzo> GetImprese(ImpreseFilter impreseFilter)
        {
            return Provider.GetImprese(impreseFilter);
        }
    }
}