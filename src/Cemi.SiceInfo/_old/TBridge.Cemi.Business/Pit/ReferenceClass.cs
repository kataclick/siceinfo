﻿using System;
using System.Net;

namespace TBridge.Cemi.Business.Pit
{
    public partial class PIT16orews
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            HttpWebRequest webRequest = (HttpWebRequest)
                base.GetWebRequest(uri);
            webRequest.KeepAlive = false;
            //webRequest.ProtocolVersion = HttpVersion.Version10;
            return webRequest;
        }
    }
}