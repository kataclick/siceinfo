﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TBridge.Cemi.Business.SintesiServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SintesiServiceReference.getXmlMinSoap")]
    public interface getXmlMinSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getXmlMinFromId", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string getXmlMinFromId(string idComunicazione);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getXmlMinFromIdAuth", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string getXmlMinFromIdAuth(string idComunicazione, string username, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/getXmlMinFromCodAuth", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string getXmlMinFromCodAuth(string CodiceComunicazione, string username, string password);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface getXmlMinSoapChannel : TBridge.Cemi.Business.SintesiServiceReference.getXmlMinSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class getXmlMinSoapClient : System.ServiceModel.ClientBase<TBridge.Cemi.Business.SintesiServiceReference.getXmlMinSoap>, TBridge.Cemi.Business.SintesiServiceReference.getXmlMinSoap {
        
        public getXmlMinSoapClient() {
        }
        
        public getXmlMinSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public getXmlMinSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public getXmlMinSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public getXmlMinSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string getXmlMinFromId(string idComunicazione) {
            return base.Channel.getXmlMinFromId(idComunicazione);
        }
        
        public string getXmlMinFromIdAuth(string idComunicazione, string username, string password) {
            return base.Channel.getXmlMinFromIdAuth(idComunicazione, username, password);
        }
        
        public string getXmlMinFromCodAuth(string CodiceComunicazione, string username, string password) {
            return base.Channel.getXmlMinFromCodAuth(CodiceComunicazione, username, password);
        }
    }
}
