﻿using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Business
{
    public class CudManager
    {
        private readonly Data.Common commonData = new Data.Common();

        public Cud GetCud(int idLavoratore)
        {
            return commonData.GetCud(idLavoratore);
        }

        public bool CudScaricato(int idUtente, int annoRedditi)
        {
            return commonData.CudScaricato(idUtente, annoRedditi);
        }
    }
}