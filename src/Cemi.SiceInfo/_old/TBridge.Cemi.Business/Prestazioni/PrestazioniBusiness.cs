using System;
using System.Collections.Generic;
using TBridge.Cemi.Business.Crm;
using TBridge.Cemi.Business.Reporting;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Filters.Prestazioni;

namespace TBridge.Cemi.Business.Prestazioni
{
    public class PrestazioniBusiness
    {
        public const string URLSEMAFOROGIALLO = "~/CeServizi/images/semaforoGiallo.png";
        public const string URLSEMAFOROROSSO = "~/CeServizi/images/semaforoRosso.png";
        public const string URLSEMAFOROVERDE = "~/CeServizi/images/semaforoVerde.png";
        private readonly PrestazioniDataAccess _dataAccess = new PrestazioniDataAccess();

        public bool IsLavoratoreInAnagrafica(Lavoratore lavoratore)
        {
            return _dataAccess.IsLavoratoreInAnagrafica(lavoratore);
        }

        public TipoPrestazioneCollection GetTipiPrestazionePerGradoParentela(string gradoParentela)
        {
            return _dataAccess.GetTipiPrestazionePerGradoParentela(gradoParentela);
        }

        public TipoPrestazione GetTipoPrestazione(string gradoParentela, string idTipoPrestazione)
        {
            return _dataAccess.GetTipoPrestazione(gradoParentela, idTipoPrestazione);
        }

        /// <summary>
        ///     Seleziona i tipi di prestazioni
        /// </summary>
        /// <param name="gradoParentela"></param>
        /// <param name="soloAttive">in funzione della data pesca solamente le attive, se false prende tutte le prestazioni</param>
        /// <returns></returns>
        public TipoPrestazioneCollection GetTipiPrestazionePerGradoParentela(string gradoParentela, bool soloAttive)
        {
            return _dataAccess.GetTipiPrestazionePerGradoParentela(gradoParentela, soloAttive);
        }

        /// <summary>
        ///     Recupera i familiari del lavoratore filtrando per grado parentela
        /// </summary>
        /// <param name="idLavoratore">id del lavoratore</param>
        /// <param name="gradoParentela">Se grado parentela � null o empty non effettua filtro sul grado</param>
        /// <returns>lista dei familiari</returns>
        public FamiliareCollection GetFamiliariPerGradoParentela(int idLavoratore, string gradoParentela,
            bool conDataNascita)
        {
            return _dataAccess.GetFamiliariPerGradoParentela(idLavoratore, gradoParentela, conDataNascita);
        }

        public TipoPrestazioneCollection GetTipiPrestazione()
        {
            return _dataAccess.GetTipiPrestazione();
        }

        public Familiare GetFamiliare(int idLavoratore, int idFamiliare)
        {
            return _dataAccess.GetFamiliare(idLavoratore, idFamiliare);
        }

        public Configurazione GetConfigurazionePrestazione(string idTipoPrestazione, string beneficiario,
            int idLavoratore)
        {
            return _dataAccess.GetConfigurazionePrestazione(idTipoPrestazione, beneficiario, idLavoratore);
        }

        public bool IsFamiliareInAnagrafica(Familiare familiare)
        {
            return _dataAccess.IsFamiliareInAnagrafica(familiare);
        }

        /// <summary>
        ///     Permette l'inserimento di una fattura dichiarata, fuori dal contesto di inserimento di una domanda
        /// </summary>
        /// <param name="fatturaDichiarata">Fattura Dichiarata</param>
        /// <param name="idDomanda">Id della domanda</param>
        /// <returns></returns>
        public bool AggiungiFatturaDichiarata(FatturaDichiarata fatturaDichiarata, int idDomanda)
        {
            return _dataAccess.InsertFatturaDichiarata(fatturaDichiarata, idDomanda);
        }

        public bool InsertDomanda(Domanda domanda, string loginCNCE, string passwordCNCE, string urlCNCE,
            int? idDomandaTemporanea, int idUtente)
        {
            bool res = _dataAccess.InsertDomanda(domanda, idDomandaTemporanea, idUtente, false);

            if (res)
            {
                ControllaDomandaLavoratore(domanda);
                ControllaDomandaFamiliare(domanda);
                ControllaDomandaUnivocita(domanda);
                ControllaDomandaDocumenti(domanda);
                ControllaDomandaFatture(domanda);
                ControllaDomandaScolastiche(domanda);

                bool tutteNelCNCE;
                try
                {
                    GestisciOreCNCE(domanda, loginCNCE, passwordCNCE, urlCNCE);
                    tutteNelCNCE = domanda.CasseEdili.SonoTutteNelCircuitoCNCE();
                }
                catch
                {
                    tutteNelCNCE = false;
                }

                if (!tutteNelCNCE)
                    ForzaControlloOreCNCE(domanda.IdDomanda.Value, false);
            }

            return res;
        }

        public bool InsertOrUpdateDomandaTemporanea(Domanda domanda)
        {
            if (!domanda.IdDomanda.HasValue)
                return _dataAccess.InsertDomandaTemporanea(domanda);
            return _dataAccess.UpdateDomandaTemporanea(domanda);
        }

        //public void GestisciOreCNCE(Domanda domanda, string loginCNCE, string passwordCNCE, string urlCNCE)
        //{
        //    OreMensiliCNCECollection ore = GetOreFromCNCE(domanda, loginCNCE, passwordCNCE, urlCNCE);

        //    foreach (OreMensiliCNCE ora in ore)
        //    {
        //        bool duplicate;

        //        InsertOreMensili(ora, out duplicate);
        //    }
        //}

        public void GestisciOreCNCE(Domanda domanda, string loginCNCE, string passwordCNCE, string urlCNCE)
        {
            OreCNCEManager.GestisciOreCNCE(domanda.CasseEdili,
                loginCNCE,
                passwordCNCE,
                urlCNCE,
                domanda.Lavoratore.IdLavoratore.Value,
                domanda.Lavoratore.CodiceFiscale,
                domanda.Lavoratore.Cognome,
                domanda.Lavoratore.Nome,
                domanda.DataRiferimento);
        }

        public DomandaCollection GetDomande(DomandaFilter filtro)
        {
            return _dataAccess.GetDomande(filtro);
        }

        public DomandaCollection GetDomandeQuorum(DomandaFilter filtro)
        {
            return _dataAccess.GetDomandeQuorum(filtro);
        }


        public List<Quorum> GetQuorum(DomandaFilter filtro)
        {
            return _dataAccess.GetQuorum(filtro);
        }

        public DomandaCollection GetDomandeTemporanee(DomandaFilter filtro)
        {
            return _dataAccess.GetDomandeTemporanee(filtro);
        }

        public DocumentoCollection GetDocumenti(DocumentoFilter filtro)
        {
            return _dataAccess.GetDocumenti(filtro);
        }

        public Domanda GetDomanda(int idDomanda)
        {
            return _dataAccess.GetDomanda(idDomanda);
        }

        public Domanda GetDomandaTemporanea(int idDomandaTemporanea)
        {
            return _dataAccess.GetDomandaTemporanea(idDomandaTemporanea);
        }

        public bool UpdateDomandaSetFamiliareDaAnagrafica(int idDomanda, int idFamiliare)
        {
            return _dataAccess.UpdateDomandaSetFamiliareDaAnagrafica(idDomanda, idFamiliare);
        }

        public string ConvertiBoolInSemaforo(bool? stato)
        {
            if (!stato.HasValue)
                return URLSEMAFOROGIALLO;
            if (stato.Value)
                return URLSEMAFOROVERDE;
            return URLSEMAFOROROSSO;
        }

        public bool UpdateDomandaSetPresaInCarico(int idDomanda, int idUtente, bool inCarico)
        {
            return _dataAccess.UpdateDomandaSetPresaInCarico(idDomanda, idUtente, inCarico);
        }

        //public OreMensiliCNCECollection GetOreMensiliCNCE(int idLavoratore)
        //{
        //    return dataAccess.GetOreMensiliCNCE(idLavoratore);
        //}

        //public OreMensiliCNCE GetOreMensiliCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        //{
        //    return dataAccess.GetOreMensiliCNCE(idLavoratore, idCassaEdile, anno, mese);
        //}

        //public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        //{
        //    return dataAccess.InsertOreMensili(ore, out oreDuplicate);
        //}

        public StatoDomandaCollection GetStatiDomanda()
        {
            return _dataAccess.GetStatiDomanda();
        }

        public byte[] CreaModuloPdfDomanda(Domanda domanda, TipoModulo tipoModulo, string wsReporting2005,
            string wsExecution2005)
        {
            BusinessReporting businessReporting = new BusinessReporting();

            BusinessReporting.ParameterValue[] parameters = new BusinessReporting.ParameterValue[1];

            parameters[0] = new BusinessReporting.ParameterValue();
            parameters[0].Label = "idDomanda";
            parameters[0].Name = "idDomanda";
            parameters[0].Value = domanda.IdDomanda.ToString();

            byte[] results = businessReporting.CreaPdfReport(wsReporting2005, wsExecution2005, tipoModulo.Modulo,
                parameters);

            return results;
        }


        //public byte[] CreaModuloPdfDomanda(
        //    string wsReportingServices2005,
        //    string wsReportingExecution2005,
        //    Domanda domanda)
        //{
        //    //return true;

        //    ReportingService2005.ReportingService2005 rs;
        //    ReportExecution2005.ReportExecutionService rsExec;

        //    // Create a new proxy to the web service

        //    rs = new ReportingService2005.ReportingService2005(wsReportingServices2005);
        //    rsExec = new ReportExecution2005.ReportExecutionService(wsReportingExecution2005);

        //    // Authenticate to the Web service using Windows credentials

        //    rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
        //    rsExec.Credentials = System.Net.CredentialCache.DefaultCredentials;

        //    // Prepare Render arguments
        //    string historyID = null;
        //    string deviceInfo = null;
        //    string format = "PDF";
        //    Byte[] results = null;
        //    string encoding = String.Empty;
        //    string mimeType = String.Empty;
        //    string extension = String.Empty;
        //    ReportExecution2005.Warning[] warnings = null;
        //    string[] streamIDs = null;

        //    // Define variables needed for GetParameters() method
        //    // Get the report name
        //    string _reportName = @"/ReportPrestazioni/ReportModuliPrestazioniSanitarie";
        //    string _historyID = null;
        //    bool _forRendering = false;
        //    ReportingService2005.ParameterValue[] _values = null;
        //    ReportingService2005.DataSourceCredentials[] _credentials = null;
        //    ReportingService2005.ReportParameter[] _parameters = null;

        //    // Get if any parameters needed.
        //    _parameters = rs.GetReportParameters(_reportName, _historyID,
        //                  _forRendering, _values, _credentials);

        //    // Load the selected report.

        //    ReportExecution2005.ExecutionInfo ei =
        //          rsExec.LoadReport(_reportName, historyID);

        //    // Prepare report parameter.

        //    // Set the parameters for the report needed.

        //    ReportExecution2005.ParameterValue[] parameters =
        //           new ReportExecution2005.ParameterValue[1];

        //    // Place to include the parameter.

        //    if (_parameters.Length > 0)
        //    {
        //        parameters[0] = new ReportExecution2005.ParameterValue();
        //        parameters[0].Label = "idDomanda";
        //        parameters[0].Name = "idDomanda";
        //        parameters[0].Value = domanda.IdDomanda.ToString();
        //    }
        //    rsExec.SetExecutionParameters(parameters, "it-IT");
        //    results = rsExec.Render(format, deviceInfo,
        //              out extension, out encoding,
        //              out mimeType, out warnings, out streamIDs);

        //    return results;
        //}

        public bool UpdateDomandaAccogli(int idDomanda, int idUtente, string idTipoPrestazione)
        {
            return _dataAccess.UpdateDomandaAccogli(idDomanda, idUtente, idTipoPrestazione);
        }

        public bool UpdateDomandaAnnulla(int idDomanda, int idUtente)
        {
            return _dataAccess.UpdateDomandaAnnulla(idDomanda, idUtente);
        }

        public bool UpdateDomandaAttesa(int idDomanda)
        {
            return _dataAccess.UpdateDomandaAttesa(idDomanda);
        }

        public bool UpdateDomandaAttesaDenuncia(int idDomanda)
        {
            return _dataAccess.UpdateDomandaAttesaDenuncia(idDomanda);
        }

        public bool UpdateDomandaEsame(int idDomanda)
        {
            return _dataAccess.UpdateDomandaEsame(idDomanda);
        }

        public bool UpdateDomandaRespingi(int idDomanda, string idTipoCausale)
        {
            return _dataAccess.UpdateDomandaRespingi(idDomanda, idTipoCausale);
        }

        public bool IsDomandaAccoglibile(Domanda domanda)
        {
            bool res = false;

            Configurazione configurazione = GetConfigurazionePrestazione(
                domanda.IdTipoPrestazione,
                domanda.Beneficiario,
                domanda.Lavoratore.IdLavoratore.Value);
            bool considerareFamiliare = domanda.Beneficiario != "L";
            bool considerareOreCNCE = domanda.CasseEdili.Count > 0;

            if (domanda.ControlloLavoratore.HasValue
                && (domanda.ControlloFamiliare.HasValue || !considerareFamiliare)
                && domanda.ControlloPresenzaDocumenti.HasValue
                && (domanda.ControlloFatture.HasValue || !configurazione.RichiestaFattura)
                &&
                (domanda.ControlloScolastiche.HasValue ||
                 configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione != 2)
                && domanda.ControlloUnivocitaPrestazione.HasValue
                && (domanda.ControlloOreCnce.HasValue || !considerareOreCNCE))

                res = domanda.ControlloLavoratore.Value
                      && (!considerareFamiliare || domanda.ControlloFamiliare.Value)
                      && domanda.ControlloPresenzaDocumenti.Value
                      && (!configurazione.RichiestaFattura || domanda.ControlloFatture.Value)
                      &&
                      (configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione != 2 ||
                       domanda.ControlloScolastiche.Value)
                      && domanda.ControlloUnivocitaPrestazione.Value
                      && (!considerareOreCNCE || domanda.ControlloOreCnce.Value);

            return res;
        }

        //private static OreMensiliCNCECollection GetOreFromCNCE(Domanda domanda, string loginCNCE, string passwordCNCE,
        //                                                       string urlCNCE)
        //{
        //    OreMensiliCNCECollection ore = new OreMensiliCNCECollection();

        //    NetworkCredential Cred = new NetworkCredential();
        //    Cred.UserName = loginCNCE;
        //    Cred.Password = passwordCNCE;

        //    // Add certificate support.
        //    ServicePointManager.CertificatePolicy = new AcceptAllCertificatesPolicy();

        //    string urlTemporanea = urlCNCE;

        //    foreach (CassaEdile cassaEdile in domanda.CasseEdili)
        //    {
        //        if (cassaEdile.Cnce)
        //        {
        //            for (int i = 0; i < 6; i++)
        //            {
        //                // Da ripetere per tutte le casse edili selezionate e per tutti gli anni            
        //                urlTemporanea = urlTemporanea.Replace("@codiceFiscale", domanda.Lavoratore.CodiceFiscale);
        //                urlTemporanea = urlTemporanea.Replace("@nome", domanda.Lavoratore.Nome);
        //                urlTemporanea = urlTemporanea.Replace("@cognome", domanda.Lavoratore.Cognome);
        //                urlTemporanea = urlTemporanea.Replace("@cassaEdile", cassaEdile.IdCassaEdile);
        //                urlTemporanea = urlTemporanea.Replace("@annoApe",
        //                                                      domanda.DataRiferimento.AddYears(-i).Year.ToString());

        //                WebClient wc1 = new WebClient();
        //                wc1.Credentials = Cred;
        //                try
        //                {
        //                    using (Stream s = wc1.OpenRead(urlTemporanea))
        //                    {
        //                        using (StreamReader sr = new StreamReader(s))
        //                        {
        //                            // Tutto questo serve solo per scatenare l'eccezione,
        //                            // perch� alla prima chiamata viene fatto dal sito un Redirect e generata l'eccezione
        //                        }
        //                    }
        //                }
        //                catch (WebException webExc)
        //                {
        //                    WebClient wc2 = new WebClient();

        //                    wc2.Credentials = Cred;

        //                    using (Stream s2 = wc2.OpenRead(webExc.Response.ResponseUri))
        //                    {
        //                        using (StreamReader sr2 = new StreamReader(s2))
        //                        {
        //                            sr2.ReadLine();
        //                            string recordLetto;

        //                            while ((recordLetto = sr2.ReadLine()) != null)
        //                            {
        //                                char[] separator = new char[1];
        //                                separator[0] = ',';
        //                                OreMensiliCNCE ora = new OreMensiliCNCE();
        //                                ore.Add(ora);

        //                                ora.IdLavoratore = domanda.Lavoratore.IdLavoratore.Value;

        //                                string[] valori = recordLetto.Split(separator, StringSplitOptions.None);
        //                                if (!string.IsNullOrEmpty(valori[0]))
        //                                    ora.Anno = Int32.Parse(valori[0]);
        //                                if (!string.IsNullOrEmpty(valori[1]))
        //                                    ora.Mese = Int32.Parse(valori[1]);
        //                                if (!string.IsNullOrEmpty(valori[2]))
        //                                    ora.IdCassaEdile = valori[2];
        //                                if (!string.IsNullOrEmpty(valori[4]))
        //                                    ora.OreLavorate = ParseOreCNCE(valori[4]);
        //                                if (!string.IsNullOrEmpty(valori[5]))
        //                                    ora.OreFerie = ParseOreCNCE(valori[5]);
        //                                if (!string.IsNullOrEmpty(valori[6]))
        //                                    ora.OreInfortunio = ParseOreCNCE(valori[6]);
        //                                if (!string.IsNullOrEmpty(valori[7]))
        //                                    ora.OreMalattia = ParseOreCNCE(valori[7]);
        //                                if (!string.IsNullOrEmpty(valori[8]))
        //                                    ora.OreCassaIntegrazione = ParseOreCNCE(valori[8]);
        //                                if (!string.IsNullOrEmpty(valori[9]))
        //                                    ora.OrePermessoRetribuito = ParseOreCNCE(valori[9]);
        //                                if (!string.IsNullOrEmpty(valori[10]))
        //                                    ora.OrePermessoNonRetribuito = ParseOreCNCE(valori[10]);
        //                                if (!string.IsNullOrEmpty(valori[11]))
        //                                    ora.OreAltro = ParseOreCNCE(valori[11]);
        //                            }
        //                        }
        //                    }
        //                }

        //                urlTemporanea = urlCNCE;
        //            }
        //        }
        //    }

        //    // Fien

        //    return ore;
        //}

        //private static int ParseOreCNCE(string oreCNCE)
        //{
        //    int ore = 0;

        //    //talvolta nel CNCE usano il punto come separatore e talvolta indicano .5 per dire 0.5. 
        //    //A noi interessa solo la parte intera, sicenew non � in grado di gestire i decimali per quei campi
        //    string oreIntere = oreCNCE.Split('.')[0];

        //    if (!string.IsNullOrEmpty(oreIntere))
        //    {
        //        if (!int.TryParse(oreIntere, out ore))
        //            ore = 0;
        //    }
        //    else
        //        ore = 0;

        //    return ore;
        //}

        public TipoDocumentoCollection GetTipiDocumento()
        {
            return _dataAccess.GetTipiDocumento();
        }

        public DocumentoCollection GetDocumentiNecessari(int idDomanda)
        {
            return _dataAccess.GetDocumentiNecessari(idDomanda);
        }

        public DocumentoCollection GetDocumentiNecessariPiuRecenti(int idDomanda)
        {
            return _dataAccess.GetDocumentiNecessariPiuRecenti(idDomanda);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare)
        {
            return _dataAccess.GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare, bool operatore, int? idPrestazioniDomanda)
        {
            return _dataAccess.GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare,
                operatore, idPrestazioniDomanda);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare, bool operatore)
        {
            return _dataAccess.GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare,
                operatore);
        }

        public DocumentoCopertinaCollection GetDocumentiStampaCopertine(int idDomanda)
        {
            return _dataAccess.GetDocumentiStampaCopertine(idDomanda);
        }

        public Documento GetDocumento(short idTipoDocumento, int idDocumento)
        {
            return _dataAccess.GetDocumento(idTipoDocumento, idDocumento);
        }

        public bool AssociaPersonaDocumento(short idTipoDocumento, int idDocumento, Lavoratore lavoratore,
            Familiare familiare, int? idDomanda)
        {
            return _dataAccess.AssociaPersonaDocumento(idTipoDocumento, idDocumento, lavoratore, familiare, idDomanda);
        }

        public bool DisassociaDocumento(short idTipoDocumento, int idDocumento)
        {
            return _dataAccess.DisassociaDocumento(idTipoDocumento, idDocumento);
        }

        public string TrasformaGradoParentelaInDescrizione(string gradoParentela)
        {
            switch (gradoParentela)
            {
                case "L":
                    return "Lavoratore";
                case "C":
                    return "Coniuge";
                case "F":
                    return "Figlio/a";
                default:
                    return string.Empty;
            }
        }

        public PrestazioneErogataCollection GetPrestazioniErogate(PrestazioneErogataFilter filtro)
        {
            return _dataAccess.GetPrestazioniErogate(filtro);
        }

        public bool FatturaGiaUtilizzata(FatturaDichiarata fattura, int idLavoratore)
        {
            return _dataAccess.FatturaGiaUtilizzata(fattura, idLavoratore);
        }

        //public bool DeleteOreCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        //{
        //    return dataAccess.DeleteOreCNCE(idLavoratore, idCassaEdile, anno, mese);
        //}

        public bool DeleteDomandaTemporanea(int idDomanda)
        {
            return _dataAccess.DeleteDomandaTemporanea(idDomanda, null);
        }

        public TipoScuolaCollection GetTipiScuola(string idTipoPrestazione)
        {
            return _dataAccess.GetTipiScuola(idTipoPrestazione);
        }

        public TipoDecessoCollection GetTipiDecesso()
        {
            return _dataAccess.GetTipiDecesso();
        }

        public TipoPromozioneCollection GetTipiPromozione(int idTipoScuola)
        {
            return _dataAccess.GetTipiPromozione(idTipoScuola);
        }

        public bool InsertUpdateDatiAggiuntiviScolastiche(int idDomanda, DatiScolastiche datiScolastiche)
        {
            if (!datiScolastiche.IdDatiScolastiche.HasValue)
            {
                return _dataAccess.InsertDatiAggiuntiviScolastiche(idDomanda, datiScolastiche);
            }
            return _dataAccess.UpdateDatiAggiuntiviScolastiche(idDomanda, datiScolastiche);
        }

        public bool SalvaImportoPrestazione(PrestazioneImporto importo)
        {
            bool res = false;

            if (importo.IdImporto.HasValue)
            {
                res = _dataAccess.UpdateImportoPrestazione(importo);
            }
            else
            {
                res = _dataAccess.InsertImportoPrestazione(importo);
            }

            return res;
        }

        public bool InsertCassaEdileInDomanda(int idDomanda, string idCassaEdile)
        {
            return _dataAccess.InsertCassaEdileInDomanda(idDomanda, idCassaEdile);
        }

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            return _dataAccess.GetLavoratoriSiceNew(filtro);
        }

        public Lavoratore GetLavoratoreSiceNewByKey(int idLavoratore)
        {
            return _dataAccess.GetLavoratoreSiceNewByKey(idLavoratore);
        }

        /// <summary>
        ///     Ritorna i tipi causali finalizzati alla gestione fast delle prestazioni
        /// </summary>
        /// <returns>Elenco delle causali</returns>
        public TipoCausaleCollection GetTipiCausaliFast()
        {
            return _dataAccess.GetTipiCausaliFast();
        }

        /// <summary>
        ///     Ritorna i tipi causali finalizzati alla gestione di una particolare prestazione
        /// </summary>
        /// <param name="fast"></param>
        /// <param name="idTipoPrestazione"></param>
        /// <returns></returns>
        public TipoCausaleCollection GetTipiCausali(bool? fast, string idTipoPrestazione)
        {
            return _dataAccess.GetTipiCausali(fast, idTipoPrestazione);
        }

        /// <summary>
        ///     Inserisce una lista di documenti consegnati. quella precedente se presente
        ///     viene pima cancellata (tutto transazionalmente)
        /// </summary>
        /// <param name="idDomanda"></param>
        /// <param name="documenti"></param>
        public void InserisciDocumentiConsegnati(int idDomanda, DocumentoCollection documenti)
        {
            _dataAccess.InserisciDocumentiConsegnati(idDomanda, documenti);
        }

        #region FATTUREIMPORTI

        public bool fattureImportiUpdatePerIdPrestazioneImportoFattura(int idPrestazioneImportoFattura, decimal valore)
        {
            return _dataAccess.fattureImportiUpdatePerIdPrestazioneImportoFattura(idPrestazioneImportoFattura, valore,
                null);
        }

        #endregion

        #region Controllo Fatture

        public void ControllaDomandaFatture(Domanda domanda)
        {
            _dataAccess.ControllaDomandaFatture(domanda);
        }

        #endregion

        #region Controllo Ore CNCE

        public bool ForzaControlloOreCNCE(int idDomanda, bool stato)
        {
            return _dataAccess.ForzaControlloOreCNCE(idDomanda, stato);
        }

        #endregion

        public string TraduciBeneficiariReali(string stato)
        {
            switch (stato)
            {
                case "O":
                    return "A";
                default:
                    return stato;
            }
        }

        public void UpdateDomandaDataRiferimento(int idDomanda, DateTime dataRiferimento)
        {
            _dataAccess.UpdateDomandaDataRiferimento(idDomanda, dataRiferimento);
        }

        public void UpdateDomandaNota(int idDomanda, int idUtente, string nota)
        {
            _dataAccess.UpdateDomandaNota(idDomanda, idUtente, nota);
        }

        public StatiCureProtesi GetStatiCureProtesi(int idDomanda)
        {
            return _dataAccess.GetStatiCureProtesi(idDomanda);
        }

        #region Metodi per la chiamata del WS prestazioni del CRM

        /// <summary>
        ///     Ritorna l'elenco di prestazioni da passare al CRM
        /// </summary>
        /// <param name="idPrestazioneDomanda"></param>
        /// <returns>Elenco di prestazioni in ottica CRM</returns>
        public PrestazioneCRMCollection GetPrestazioniCRMColletcion(int idPrestazioneDomanda)
        {
            return _dataAccess.GetPrestazioniCRMColletcion(idPrestazioneDomanda);
        }

        /// <summary>
        ///     Metodo che racchiude in una chiamata la creazione della prestazione ad uso e consumo del CRM e la chiamata al WS
        /// </summary>
        /// <param name="idDomanda"></param>
        public void CallCrmWsPrestazioni(int idDomanda)
        {
            PrestazioneCRMCollection listaPrestazioniCrm = GetPrestazioniCRMColletcion(idDomanda);
            PrestazioniManager.NotificaPrestazione(listaPrestazioniCrm);
        }

        #endregion

        #region FATTURE

        public Fattura FattureImportiFill(Fattura fattura)
        {
            FatturaImportoCollection importi = new FatturaImportoCollection();

            //fattura.importi = dataAccess.getImportiFatturePerIdPrestazioniFattura(fattura.IdPrestazioniFattura);

            //dataAccess.
            return fattura;
        }

        public bool fattureImportiInsertImportiDefault(string idTipoPrestazione, int idPrestazioniFattura)
        {
            if (idPrestazioniFattura > 0 && idTipoPrestazione.Length > 0)
            {
                return _dataAccess.InsertImportoFatturaGeneraRecord(idTipoPrestazione, idPrestazioniFattura, null);
            }
            return false;
        }

        public FatturaCollection getFattureFisiche(int idPrestazioniFatturaDichiarata)
        {
            return _dataAccess.getFatturePerIdFatturaDichiarata(idPrestazioniFatturaDichiarata);
        }

        public FatturaCollection getFattureFisicheAssociate(int idPrestazioniFatturaDichiarata)
        {
            //Solo Fatture associate alla fattura dichiarata
            return _dataAccess.getFatturePerIdFatturaDichiarata(idPrestazioniFatturaDichiarata);
        }


        public FatturaImportoCollection getFattureFisicheImporti(int idPrestazioneFattura)
        {
            return _dataAccess.getImportiFatturePerIdPrestazioniFattura(idPrestazioneFattura);
        }


        public bool PrestazioniFatturaAggiornaValidazione(int idPrestazioniFattura, bool stato)
        {
            return _dataAccess.PrestazioniFatturaAggiornaValidazione(idPrestazioniFattura, stato, null);
        }

        public FatturaCollection getFattureFisicheAssociabili(DateTime? data, string numero, decimal? importo,
            bool adaptSearch)
        {
            return _dataAccess.getFattureFisicheAssociabili(data, numero, importo, adaptSearch);
        }

        #endregion

        #region FATTUREDICHIARATE

        public bool AnnullaFatturaDichiarata(int idPrestazioniFatturaDichiarata)
        {
            if (idPrestazioniFatturaDichiarata > 0)
                return _dataAccess.AnnullaFatturaDichiarataPerIdFatturaDichiarata(idPrestazioniFatturaDichiarata, null);
            return false;
        }

        public bool RipristinaAnnullamentoFatturaDichiarata(int idPrestazioniFatturaDichiarata)
        {
            if (idPrestazioniFatturaDichiarata > 0)
                return
                    _dataAccess.RipristinaAnnullamentoFatturaDichiarataPerIdFatturaDichiarata(
                        idPrestazioniFatturaDichiarata, null);
            return false;
        }

        public bool fatturaDichiarataAssociaFatturaFisica(int idPrestazioniFattura, int idPrestazioniFatturaDichiarata)
        {
            if (idPrestazioniFattura > 0 && idPrestazioniFatturaDichiarata > 0)
            {
                return _dataAccess.fatturaDichiarataAssociaFatturaFisica(idPrestazioniFattura,
                    idPrestazioniFatturaDichiarata, null);
            }
            return false;
        }


        public bool fatturaDichiarataDisassociaFatturaFisica(int idPrestazioniFatturaDichiarata)
        {
            if (idPrestazioniFatturaDichiarata > 0)
            {
                //TODO DA rivedere erroe nella store.. probabile fail di EXEC
                _dataAccess.ImportoFatturaDeleteRecordPerIdPrestazioniFatturaDichiarata(idPrestazioniFatturaDichiarata,
                    null);
                _dataAccess.fatturaDichiarataDisassociaFatturaFisica(idPrestazioniFatturaDichiarata, null);
                return true;
            }
            return false;
        }

        #endregion

        #region Controllo Documenti

        public void ControllaDomandaDocumenti(Domanda domanda)
        {
            _dataAccess.ControllaDomandaDocumenti(domanda);
        }

        public bool ForzaPresenzaDocumenti(int idDomanda)
        {
            return _dataAccess.ForzaPresenzaDocumenti(idDomanda);
        }

        #endregion

        #region Controllo Familiare

        public void ControllaDomandaFamiliare(Domanda domanda)
        {
            _dataAccess.ControllaDomandaFamiliare(domanda);
        }

        public bool ForzaFamiliareDataNascita(int idDomanda)
        {
            return _dataAccess.ForzaFamiliareDataNascita(idDomanda);
        }

        public bool ForzaFamiliareDataDecesso(int idDomanda)
        {
            return _dataAccess.ForzaFamiliareDataDecesso(idDomanda);
        }

        public bool ForzaFamiliareCodiceFiscale(int idDomanda)
        {
            return _dataAccess.ForzaFamiliareCodiceFiscale(idDomanda);
        }

        public bool ForzaFamiliareACarico(int idDomanda)
        {
            return _dataAccess.ForzaFamiliareACarico(idDomanda);
        }

        #endregion

        #region Controllo Dati Anagrafici

        public void ControllaDomandaLavoratore(Domanda domanda)
        {
            _dataAccess.ControllaDomandaLavoratore(domanda);
        }

        public bool ForzaLavoratoreIndirizzo(int idDomanda)
        {
            return _dataAccess.ForzaLavoratoreIndirizzo(idDomanda);
        }

        public bool ForzaLavoratoreEmail(int idDomanda)
        {
            return _dataAccess.ForzaLavoratoreEmail(idDomanda);
        }

        public bool ForzaLavoratoreCellulare(int idDomanda)
        {
            return _dataAccess.ForzaLavoratoreCellulare(idDomanda);
        }

        #endregion

        #region Controllo Univocit�

        public void ControllaDomandaUnivocita(Domanda domanda)
        {
            _dataAccess.ControllaDomandaUnivocita(domanda);
        }

        public bool ForzaUnivocita(int idDomanda)
        {
            return _dataAccess.ForzaUnivocita(idDomanda);
        }

        #endregion

        #region ControlloScolastiche

        public void ControllaDomandaScolastiche(Domanda domanda)
        {
            _dataAccess.ControllaDomandaScolastiche(domanda);
        }

        public bool ForzaControlloPeriodoFrequenza180Giorni(int idDomanda)
        {
            return _dataAccess.ForzaControlloPeriodoFrequenza180Giorni(idDomanda);
        }

        public bool ForzaControlloPeriodoFrequenza3Anni(int idDomanda)
        {
            return _dataAccess.ForzaControlloPeriodoFrequenza3Anni(idDomanda);
        }

        public bool ForzaControlloAnnoFrequenza(int idDomanda)
        {
            return _dataAccess.ForzaControlloAnnoFrequenza(idDomanda);
        }

        #endregion

        #region Metodi per la chiamata del WS prestazioni del CRM

        ///// <summary>
        ///// Ritorna l'elenco di prestazioni da passare al CRM
        ///// </summary>
        ///// <param name="idPrestazioneDomanda"></param>
        ///// <returns>Elenco di prestazioni in ottica CRM</returns>
        //public PrestazioneCRMCollection GetPrestazioniCRMColletcion(int idPrestazioneDomanda)
        //{
        //    return dataAccess.GetPrestazioniCRMColletcion(idPrestazioneDomanda);
        //}

        ///// <summary>
        ///// Metodo che racchiude in una chiamata la creazione della prestazione ad uso e consumo del CRM e la chiamata al WS
        ///// </summary>
        ///// <param name="idDomanda"></param>
        //public void CallCrmWsPrestazioni(int idDomanda)
        //{
        //    try
        //    {
        //        PrestazioneCRMCollection listaPrestazioniCrm = GetPrestazioniCRMColletcion(idDomanda);
        //        PrestazioniManager.NotificaPrestazione(listaPrestazioniCrm);
        //    }
        //    catch (Exception ec)
        //    {
        //        //todo gestione errore chiamata CRM
        //        //throw ec;
        //    }
        //} 

        #endregion

        #region Metodi per filtri ricerca

        public void InsertFiltroRicerca(int userID, DomandaFilter filtro, int pagina)
        {
            _dataAccess.InsertFiltroRicerca(userID, filtro, pagina);
        }

        public DomandaFilter GetFiltroRicerca(int userID, out int pagina)
        {
            return _dataAccess.GetFiltroRicerca(userID, out pagina);
        }

        public void DeleteFiltroRicerca(int userID)
        {
            _dataAccess.DeleteFiltroRicerca(userID);
        }

        #endregion
    }
}