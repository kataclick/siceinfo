using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace TBridge.Cemi.Business.Subappalti
{
    /// <summary>
    ///     Classe business di base Subappalti
    /// </summary>
    public abstract class SubappaltiEntity
    {
        /// <summary>
        ///     Numero di telefono cellulare dell'entity - riferimento interno
        /// </summary>
        protected string cellulare;

        /// <summary>
        ///     Codice fiscale dell'entity - riferimento interno
        /// </summary>
        protected string codiceFiscale;

        /// <summary>
        ///     Indirizzo di posta elettronica dell'entity - riferimento interno
        /// </summary>
        protected string email;

        /// <summary>
        ///     Codice identificativo unico dell'entity - riferimento interno
        /// </summary>
        protected int id;

        /// <summary>
        ///     Numero telefonico dell'entity - riferimento interno
        /// </summary>
        protected string telefono;

        /// <summary>
        ///     Identifica se l'entity � anche un utente - Riferimento interno
        /// </summary>
        protected Utente utente;

        /// <summary>
        ///     Codice identificativo unico dell'entity
        /// </summary>
        public int Id
        {
            get => id;
            set => id = value;
        }

        /// <summary>
        ///     Identifica se l'entity � anche un utente - Riferimento interno
        /// </summary>
        public Utente Utente
        {
            get => utente;
            set => utente = value;
        }

        /// <summary>
        ///     Codice fiscale dell'entity
        /// </summary>
        public string CodiceFiscale
        {
            get => codiceFiscale;
            set => codiceFiscale = value;
        }

        /// <summary>
        ///     Numero telefonico dell'entity
        /// </summary>
        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        /// <summary>
        ///     Numero di telefono cellulare dell'entity
        /// </summary>
        public string Cellulare
        {
            get => cellulare;
            set => cellulare = value;
        }

        /// <summary>
        ///     Indirizzo di posta elettronica dell'entity
        /// </summary>
        public string Email
        {
            get => email;
            set => email = value;
        }
    } // Class
} // NameSpace