using System;
using System.Data;
using TBridge.Cemi.Data;

//using TBridge.Cemi.ActivityTracking;

namespace TBridge.Cemi.Business.Subappalti
{
    /// <summary>
    ///     Gestore business dei subappalti
    /// </summary>
    public class SubappaltiBusiness
    {
        /// <summary>
        ///     Oggetto di accesso ai dati
        /// </summary>
        private readonly SubappaltiDataAccess subappaltiDataAccess;

        /// <summary>
        ///     Costruttore che inizializza l'accesso ai dati
        /// </summary>
        public SubappaltiBusiness()
        {
            subappaltiDataAccess = new SubappaltiDataAccess();
        }

        /// <summary>
        ///     Trova i dati di un'impresa in base al suo codice
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <returns></returns>
        public DataTable RicercaImpresa(int idImpresa)
        {
            //ACTIVITY TRACKING
            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Ricerca impresa", "ID = " + idImpresa.ToString());
            //Log.Write("Effettuata ricerca impresa", logItemCollection, 
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.RicercaImpresa(idImpresa);
        } // RicercaImpresa

        /// <summary>
        ///     Trova i dati di un'impresa in base alla sua ragione sociale, anche parziale
        ///     Pu� restituire pi� di un risultato
        /// </summary>
        /// <param name="ragioneSociale"></param>
        /// <returns></returns>
        public DataTable RicercaImpresa(string ragioneSociale)
        {
            //ACTIVITY TRACKING
            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Ricerca impresa", "Ragione sociale = " + ragioneSociale);
            //Log.Write("Effettuata ricerca impresa", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.RicercaImpresa(ragioneSociale);
        } // RicercaImpresa

        /// <summary>
        ///     Restituisce i dati di un lavoratore in base al suo codice
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="idLavoratore"></param>
        /// <returns></returns>
        public DataTable RicercaLavoratori(int idImpresa, int idLavoratore)
        {
            //ACTIVITY TRACKING
            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Ricerca lavoratori", 
            //        "IdImpresa = " + idImpresa.ToString() + " IdLavoratore = " + idLavoratore.ToString());
            //Log.Write("Effettuata ricerca lavoratori", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.RicercaLavoratori(idImpresa, idLavoratore);
        }

        public DataTable RicercaStoricoLavoratori(int idUtente, int idImpresa)
        {
            //ACTIVITY TRACKING
            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Ricerca storico lavoratori",
            //        "IdImpresa = " + idImpresa.ToString() + " IdUtente = " + idUtente.ToString());
            //Log.Write("Effettuata ricerca lavoratori", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.VisualizzaStoricoRicercheLavoratori(idUtente, idImpresa);
        }

        /// <summary>
        ///     Restituisce un lavoratore in base al suo cognome, anche parziale, ed eventualmente il nome
        ///     Pu� restituire pi� di un risultato
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="cognome"></param>
        /// <param name="nome"></param>
        /// <returns></returns>
        public DataTable RicercaLavoratori(int idImpresa, string cognome, string nome)
        {
            //ACTIVITY TRACKING
            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Ricerca lavoratori",
            //        "IdImpresa = " + idImpresa.ToString() + " Cognome = " + cognome);
            //Log.Write("Effettuata ricerca lavoratori", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.RicercaLavoratori(idImpresa, cognome, nome);
        }

        /// <summary>
        ///     Indica se un utente ha gi� effettuato 10 ricerche nell'ambito della giornata
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public bool RicercaPossibile(int idUtente)
        {
            //ACTIVITY TRACKING
            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Ricerca possibile",
            //        "IdUtente = " + idUtente.ToString());
            //Log.Write("Verificata possibilit� ricerca", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);


            return subappaltiDataAccess.ConteggioRicerche(idUtente) < 10;
        }

        /// <summary>
        ///     Visualizza dallo storico una lista di ricerche
        ///     in base ai parametri selezionati, restituendo una DataTable
        /// </summary>
        /// <param name="idUtente">
        ///     utente che effettua la query dello storico
        ///     Da inserire se � un'impresa che sta facendo la richiesta per vedere i propri storici
        /// </param>
        /// <param name="idImpresa">Impresa ricercata</param>
        /// <param name="idImpresaRicercante">Impresa che ha effettuato la ricerca</param>
        /// <param name="dataDa">Data Da del range di ricerca</param>
        /// <param name="dataA">Data A del range di ricerca</param>
        /// <returns></returns>
        public DataTable VisualizzaStorico(int? idUtente, int? idImpresa,
            int? idImpresaRicercante,
            DateTime? dataDa, DateTime? dataA)
        {
            //ACTIVITY TRACKING
            //string descr = "";
            //if (idUtente != null)
            //    descr += " IdUtente = " + idUtente.ToString();
            //if (idImpresa != null)
            //    descr += " IdImpresa = " + idImpresa.ToString();
            //if (idImpresaRicercante != null)
            //    descr += " IdImpresaRicercante = " + idImpresaRicercante.ToString();
            //if (dataDa != null)
            //    descr += " DataDa = " + ((DateTime) dataDa).ToString("dd/MM/yyyy");
            //if (dataA != null)
            //    descr += " DataDa = " + ((DateTime) dataA).ToString("dd/MM/yyyy");

            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Visualizzazione storico", descr);
            //Log.Write("Effettuata visualizzazione storico", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.VisualizzaStorico(idUtente, idImpresa, idImpresaRicercante, dataDa, dataA);
        }

        /// <summary>
        ///     Inserisce nello storico la ricerca di un utente
        ///     Restituisce l'id del record appena inserito
        /// </summary>
        /// <param name="idUtente">Id dell'utente che ha effettuato la ricerca</param>
        /// <param name="idImpresa">Id impresa ricercata</param>
        /// <param name="criteri">Stringa descrittiva dei criteri di ricerca</param>
        public int InserisciRicerca(int idUtente, int idImpresa, string criteri)
        {
            //ACTIVITY TRACKING
            //string descr = "";
            //descr += " IdUtente = " + idUtente.ToString();
            //descr += " IdImpresa = " + idImpresa.ToString();
            //if (!string.IsNullOrEmpty(criteri))
            //    descr += " Criteri = " + criteri;

            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Inserimento Ricerca", descr);
            //Log.Write("Inserimento ricerca", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            return subappaltiDataAccess.InserisciRicerca(idUtente, idImpresa, criteri);
        }

        /// <summary>
        ///     Aggiunge un criterio nello storico di una ricerca specifica
        /// </summary>
        /// <param name="idRicerca">Identificativo della ricerca</param>
        /// <param name="criterio">Criterio da aggiungere</param>
        public void AggiungiCriterioRicerca(int idRicerca, string criterio)
        {
            //string descr = "";
            //descr += " IdRicerca = " + idRicerca.ToString();
            //descr += " Criteri = " + criterio;

            //LogItemCollection logItemCollection = new LogItemCollection();
            //logItemCollection.Add("Aggiornamento Ricerca", descr);
            //Log.Write("Inserimento nuovi criteri di ricerca", logItemCollection,
            //            Log.categorie.SUBAPPALTI, Log.sezione.LOGGING);

            subappaltiDataAccess.AggiungiCriterioRicerca(idRicerca, criterio);
        }
    } // Class
} // Namespace