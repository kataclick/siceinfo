using System;
using System.Collections.Generic;
using System.Data;
using TBridge.Cemi.Data;

//using TBridge.Cemi.ActivityTracking;

namespace TBridge.Cemi.Business.Subappalti
{
    /// <summary>
    ///     Business Entity che identifica un'impresa
    /// </summary>
    public class ImpresaEntity : SubappaltiEntity
    {
        #region Metodi pubblici

        /// <summary>
        ///     Restituisce la stringa che rappresenta l'impresa.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return id + " - " + ragioneSociale;
        }

        #endregion

        #region Propriet�

        /// <summary>
        ///     Numero di iscrizione CCIAA - riferimento interno
        /// </summary>
        private int numeroIscrizioneCCIAA = -1;

        /// <summary>
        ///     Ragione sociale dell'impresa - riferimento interno
        /// </summary>
        private string ragioneSociale;

        /// <summary>
        ///     Ragione sociale dell'impresa
        /// </summary>
        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        /// <summary>
        ///     Numero di partita IVA dell'impresa
        /// </summary>
        public string PartitaIVA { get; set; }

        /// <summary>
        ///     Tipologia di impresa
        /// </summary>
        public string TipoImpresa { get; set; }

        /// <summary>
        ///     Codice del contratto associato
        /// </summary>
        public string CodiceContratto { get; set; }

        /// <summary>
        ///     Codice INAIL dell'impresa
        /// </summary>
        public string CodiceINAIL { get; set; }

        /// <summary>
        ///     Codice INPS dell'impresa
        /// </summary>
        public string CodiceINPS { get; set; }

        /// <summary>
        ///     Numero di iscrizione CCIAA
        /// </summary>
        public int NumeroIscrizioneCCIAA
        {
            get => numeroIscrizioneCCIAA;
            set => numeroIscrizioneCCIAA = value;
        }

        /// <summary>
        ///     Attivit� ISTAT
        /// </summary>
        public string AttivitaISTAT { get; set; }

        /// <summary>
        ///     Natura giuridica
        /// </summary>
        public string NaturaGiuridica { get; set; }

        /// <summary>
        ///     Indirizzo della sede legale dell'impresa
        /// </summary>
        public string IndirizzoSedeLegale { get; set; }

        /// <summary>
        ///     CAP della sede legale dell'impresa
        /// </summary>
        public string CapSedeLegale { get; set; }

        /// <summary>
        ///     Localit� della sede legale dell'impresa
        /// </summary>
        public string LocalitaSedeLegale { get; set; }

        /// <summary>
        ///     Provincia della sede legale dell'impresa
        /// </summary>
        public string ProvinciaSedeLegale { get; set; }

        /// <summary>
        ///     Indirizzo della sede amministrativa dell'impresa
        /// </summary>
        public string IndirizzoSedeAmministrazione { get; set; }

        /// <summary>
        ///     CAP della sede amministrativa dell'impresa
        /// </summary>
        public string CapSedeAmministrazione { get; set; }

        /// <summary>
        ///     Localit� della sede amministrativa dell'impresa
        /// </summary>
        public string LocalitaSedeAmministrazione { get; set; }

        /// <summary>
        ///     Provincia della sede amministrativa dell'impresa
        /// </summary>
        public string ProvinciaSedeAmministrazione { get; set; }

        /// <summary>
        ///     Segnalazione del titolare della sede amministrativa
        /// </summary>
        public string PressoSedeAmministrazione { get; set; }

        /// <summary>
        ///     Numero di fax della sede amministrativa dell'impresa
        /// </summary>
        public string FaxSedeAmministrazione { get; set; }

        /// <summary>
        ///     Indirizzo del sito web dell'impresa
        /// </summary>
        public string SitoWeb { get; set; }

        /// <summary>
        ///     Lista dei lavoratori dell'impresa
        /// </summary>
        public List<LavoratoreEntity> Lavoratori
        {
            // TODO: Ragionarci
            get;
            set;
        }

        #endregion

        #region Costruttori

        /// <summary>
        ///     Crea una nuova impresa
        /// </summary>
        public ImpresaEntity()
        {
        }

        /// <summary>
        ///     Crea una nuova impresa dal database
        /// </summary>
        /// <param name="idImpresa"></param>
        public ImpresaEntity(int idImpresa)
        {
            SubappaltiDataAccess dataAccess = new SubappaltiDataAccess();
            DataRow rigaImpresa = dataAccess.CaricaImpresa(idImpresa);

            try
            {
                id = (int) rigaImpresa["idImpresa"]; //Obbligatorio
                // outVal.Utente = TODO: Caricamento utente
                ragioneSociale = rigaImpresa["ragioneSociale"].ToString(); //Obbligatorio

                if (!Convert.IsDBNull(rigaImpresa["codiceFiscale"]))
                    codiceFiscale = rigaImpresa["codiceFiscale"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["partitaIVA"]))
                    PartitaIVA = rigaImpresa["partitaIVA"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["descrizioneTipoImpresa"]))
                    TipoImpresa = rigaImpresa["descrizioneTipoImpresa"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["codiceContratto"]))
                    CodiceContratto = rigaImpresa["codiceContratto"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["codiceINAIL"]))
                    CodiceINAIL = rigaImpresa["codiceINAIL"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["codiceINPS"]))
                    CodiceINPS = rigaImpresa["codiceINPS"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["numeroIscrizioneCCIAA"]))
                    numeroIscrizioneCCIAA = (int) rigaImpresa["numeroIscrizioneCCIAA"];
                if (!Convert.IsDBNull(rigaImpresa["descrizioneAttivitaISTAT"]))
                    AttivitaISTAT = rigaImpresa["descrizioneAttivitaISTAT"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["descrizioneNaturaGiuridica"]))
                    NaturaGiuridica = rigaImpresa["descrizioneNaturaGiuridica"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["indirizzoSedeLegale"]))
                    IndirizzoSedeLegale = rigaImpresa["indirizzoSedeLegale"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["capSedeLegale"]))
                    CapSedeLegale = rigaImpresa["capSedeLegale"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["localitaSedeLegale"]))
                    LocalitaSedeLegale = rigaImpresa["localitaSedeLegale"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["provinciaSedeLegale"]))
                    ProvinciaSedeLegale = rigaImpresa["provinciaSedeLegale"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["indirizzoSedeAmministrazione"]))
                    IndirizzoSedeAmministrazione = rigaImpresa["indirizzoSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["capSedeAmministrazione"]))
                    CapSedeAmministrazione = rigaImpresa["capSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["localitaSedeAmministrazione"]))
                    LocalitaSedeAmministrazione = rigaImpresa["localitaSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["provinciaSedeAmministrazione"]))
                    ProvinciaSedeAmministrazione = rigaImpresa["provinciaSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["pressoSedeAmministrazione"]))
                    PressoSedeAmministrazione = rigaImpresa["pressoSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["telefonoSedeAmministrazione"]))
                    telefono = rigaImpresa["telefonoSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["faxSedeAmministrazione"]))
                    FaxSedeAmministrazione = rigaImpresa["faxSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["eMailSedeAmministrazione"]))
                    email = rigaImpresa["eMailSedeAmministrazione"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["cellulare"]))
                    cellulare = rigaImpresa["cellulare"].ToString();
                if (!Convert.IsDBNull(rigaImpresa["sitoWeb"]))
                    SitoWeb = rigaImpresa["sitoWeb"].ToString();
            }
            catch (Exception exc)
            {
                // TODO: il formato dei log com'�?
                //Log.Write("ImpresaEntity : " + exc.Message,
                //    Log.categorie.SUBAPPALTI, Log.sezione.SYSTEMEXCEPTION);
                throw;
            }
        } // ImpresaEntity

        #endregion
    } // class
} // namespace