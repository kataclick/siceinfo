using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.Business.CrmIscrizione
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "IscrizioneImpresaSoap", Namespace = "localhost")]
    public class IscrizioneImpresa : SoapHttpClientProtocol, IIscrizioneImpresa
    {
        private SendOrPostCallback ManageAccountOperationCompleted;

        /// <remarks />
        public IscrizioneImpresa()
        {
            string urlSetting = ConfigurationManager.AppSettings["IscrizioneImpresa.ServiceEndpointURL"];
            if (urlSetting != null
                && urlSetting != "")
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://localhost/CEMI_CustomWSs/WsIscrizioneImpresaNew/wsiscrizione.asmx";
            }
        }

        /// <remarks />
        [SoapDocumentMethod("localhost/ManageAccount", RequestNamespace = "localhost", ResponseNamespace = "localhost",
            Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        public string ManageAccount([XmlElement(ElementName = "name")] string name,
            [XmlElement(ElementName = "mail")] string mail, [XmlElement(ElementName = "code")] string code,
            [XmlElement(ElementName = "iva")] string iva, [XmlElement(ElementName = "idlead")] string idlead,
            [XmlElement(ElementName = "codelead")] string codelead,
            [XmlElement(ElementName = "namelead")] string namelead, [XmlElement(ElementName = "status")] string status)
        {
            object[] results = Invoke("ManageAccount", new object[]
            {
                name,
                mail,
                code,
                iva,
                idlead,
                codelead,
                namelead,
                status
            });
            return (string) results[0];
        }

        /// <remarks />
        public event ManageAccountCompletedEventHandler ManageAccountCompleted;

        /// <remarks />
        public void ManageAccountAsync(string name, string mail, string code, string iva, string idlead,
            string codelead, string namelead, string status)
        {
            ManageAccountAsync(name, mail, code, iva, idlead, codelead, namelead, status, null);
        }

        /// <remarks />
        public void ManageAccountAsync(string name, string mail, string code, string iva, string idlead,
            string codelead, string namelead, string status, object userState)
        {
            if (ManageAccountOperationCompleted == null)
            {
                ManageAccountOperationCompleted = OnManageAccountOperationCompleted;
            }
            InvokeAsync("ManageAccount", new object[]
            {
                name,
                mail,
                code,
                iva,
                idlead,
                codelead,
                namelead,
                status
            }, ManageAccountOperationCompleted, userState);
        }

        private void OnManageAccountOperationCompleted(object arg)
        {
            if (ManageAccountCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                ManageAccountCompleted(this,
                    new ManageAccountCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    public delegate void ManageAccountCompletedEventHandler(object sender, ManageAccountCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class ManageAccountCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal ManageAccountCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public string Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (string) results[0];
            }
        }
    }
}