namespace TBridge.Cemi.Business.CrmIscrizione
{
    public interface IIscrizioneImpresa
    {
        string ManageAccount(string name, string mail, string code, string iva, string idlead, string codelead,
            string namelead, string status);
    }
}