using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.Business.CrmPrestazioni
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "WsPrestazioniSoap", Namespace = "http://localhost/")]
    public class WsPrestazioni : SoapHttpClientProtocol, IWsPrestazioni
    {
        private SendOrPostCallback ManagePrestazioniOperationCompleted;

        /// <remarks />
        public WsPrestazioni()
        {
            string urlSetting = ConfigurationManager.AppSettings["WsPrestazioni.ServiceEndpointURL"];
            if (urlSetting != null
                && urlSetting != "")
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://172.16.10.13/CEMI_CustomWSs/WsPrestazioni/WsPrestazioni.asmx";
            }
        }

        /// <remarks />
        [SoapDocumentMethod("http://localhost/ManagePrestazioni", RequestNamespace = "http://localhost/",
            ResponseNamespace = "http://localhost/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public string ManagePrestazioni([XmlArray(ElementName = "lstPrestazione")] prestazioni[] lstPrestazione)
        {
            object[] results = Invoke("ManagePrestazioni", new object[]
            {
                lstPrestazione
            });
            return (string) results[0];
        }

        /// <remarks />
        public event ManagePrestazioniCompletedEventHandler ManagePrestazioniCompleted;

        /// <remarks />
        public void ManagePrestazioniAsync(prestazioni[] lstPrestazione)
        {
            ManagePrestazioniAsync(lstPrestazione, null);
        }

        /// <remarks />
        public void ManagePrestazioniAsync(prestazioni[] lstPrestazione, object userState)
        {
            if (ManagePrestazioniOperationCompleted == null)
            {
                ManagePrestazioniOperationCompleted = OnManagePrestazioniOperationCompleted;
            }
            InvokeAsync("ManagePrestazioni", new object[]
            {
                lstPrestazione
            }, ManagePrestazioniOperationCompleted, userState);
        }

        private void OnManagePrestazioniOperationCompleted(object arg)
        {
            if (ManagePrestazioniCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                ManagePrestazioniCompleted(this,
                    new ManagePrestazioniCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.3074")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://localhost/", TypeName = "prestazioni")]
    public class prestazioni
    {
        /// <remarks />
        [XmlArray(ElementName = "causali")] public List<string> causali;

        /// <remarks />
        [XmlElement(ElementName = "codiceTipoPrestazione")] public string codiceTipoPrestazione;

        /// <remarks />
        [XmlArray(ElementName = "dettaglio")] public List<dettaglioPrestazione> dettaglio;

        /// <remarks />
        [XmlArray(ElementName = "documenti")] public List<string> documenti;

        /// <remarks />
        [XmlElement(ElementName = "numeroprotocollo")] public string numeroprotocollo;

        /// <remarks />
        [XmlElement(ElementName = "protocollo")] public string protocollo;

        public prestazioni()
        {
        }

        public prestazioni(string codiceTipoPrestazione, string numeroprotocollo, string protocollo,
            List<dettaglioPrestazione> dettaglio, List<string> causali, List<string> documenti)
        {
            this.codiceTipoPrestazione = codiceTipoPrestazione;
            this.numeroprotocollo = numeroprotocollo;
            this.protocollo = protocollo;
            this.dettaglio = dettaglio;
            this.causali = causali;
            this.documenti = documenti;
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.3074")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://localhost/", TypeName = "dettaglioPrestazione")]
    public class dettaglioPrestazione
    {
        /// <remarks />
        [XmlElement(ElementName = "chiave")] public int chiave;

        /// <remarks />
        [XmlElement(ElementName = "valore")] public string valore;

        public dettaglioPrestazione()
        {
        }

        public dettaglioPrestazione(int chiave, string valore)
        {
            this.chiave = chiave;
            this.valore = valore;
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    public delegate void ManagePrestazioniCompletedEventHandler(object sender, ManagePrestazioniCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class ManagePrestazioniCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal ManagePrestazioniCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public string Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (string) results[0];
            }
        }
    }
}