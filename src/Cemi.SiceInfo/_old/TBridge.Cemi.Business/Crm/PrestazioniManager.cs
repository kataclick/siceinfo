﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using TBridge.Cemi.Business.CrmPrestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Business.Crm
{
    public static class PrestazioniManager
    {
        private static readonly string domain = ConfigurationManager.AppSettings["DomainWSCrm"];
        private static readonly string logPrestazioni = ConfigurationManager.AppSettings["LogPrestazioni"];
        private static readonly string password = ConfigurationManager.AppSettings["PasswordWSCrm"];
        private static readonly string username = ConfigurationManager.AppSettings["UsernameWSCrm"];
        private static readonly WsPrestazioni ws = new WsPrestazioni();

        public static string NotificaPrestazione(List<PrestazioneCrm> listaPrestazioni)
        {
            string result = string.Empty;

            try
            {
                NetworkCredential credential = new NetworkCredential(username, password) {Domain = domain};
                ws.Credentials = credential;

                if (listaPrestazioni.Count > 0)
                {
                    prestazioni[] arrayPrestazioni = new prestazioni[listaPrestazioni.Count];
                    for (int i = 0; i < listaPrestazioni.Count; i++)
                    {
                        PrestazioneCrm prestazioneCrm = listaPrestazioni[i];
                        prestazioni prestazione = new prestazioni();

                        //chiavi
                        prestazione.codiceTipoPrestazione = prestazioneCrm.IdTipoPrestazione;
                        prestazione.numeroprotocollo = prestazioneCrm.NumeroProtocolloPrestazione.ToString();
                        prestazione.protocollo = prestazioneCrm.ProtocolloPrestazione.ToString();

                        //dettagli
                        List<dettaglioPrestazione> dettagli = new List<dettaglioPrestazione>();

                        if (!string.IsNullOrEmpty(prestazioneCrm.Descrizione))
                            dettagli.Add(new dettaglioPrestazione(1, prestazioneCrm.Descrizione));
                        if (!string.IsNullOrEmpty(prestazioneCrm.Beneficiario))
                            dettagli.Add(new dettaglioPrestazione(2, prestazioneCrm.Beneficiario));
                        if (prestazioneCrm.DataDomanda.HasValue)
                            dettagli.Add(new dettaglioPrestazione(3,
                                prestazioneCrm.DataDomanda.Value.Date.ToString(
                                    "dd/MM/yyyy")));
                        if (prestazioneCrm.DataRiferimento.HasValue)
                            dettagli.Add(new dettaglioPrestazione(4,
                                prestazioneCrm.DataRiferimento.Value.Date.ToString(
                                    "dd/MM/yyyy")));
                        if (prestazioneCrm.IdLavoratore.HasValue)
                            dettagli.Add(new dettaglioPrestazione(5, prestazioneCrm.IdLavoratore.ToString()));
                        if (prestazioneCrm.IdFamiliare.HasValue)
                            dettagli.Add(new dettaglioPrestazione(6, prestazioneCrm.IdFamiliare.ToString()));
                        if (!string.IsNullOrEmpty(prestazioneCrm.Stato))
                            dettagli.Add(new dettaglioPrestazione(7, prestazioneCrm.Stato));
                        if (!string.IsNullOrEmpty(prestazioneCrm.LavoratoreIndirizzo))
                            dettagli.Add(new dettaglioPrestazione(8, prestazioneCrm.LavoratoreIndirizzo));
                        if (!string.IsNullOrEmpty(prestazioneCrm.LavoratoreProvincia))
                            dettagli.Add(new dettaglioPrestazione(9, prestazioneCrm.LavoratoreProvincia));
                        if (!string.IsNullOrEmpty(prestazioneCrm.LavoratoreComune))
                            dettagli.Add(new dettaglioPrestazione(10, prestazioneCrm.LavoratoreComune));
                        if (!string.IsNullOrEmpty(prestazioneCrm.LavoratoreFrazione))
                            dettagli.Add(new dettaglioPrestazione(11, prestazioneCrm.LavoratoreFrazione));
                        if (!string.IsNullOrEmpty(prestazioneCrm.LavoratoreCap))
                            dettagli.Add(new dettaglioPrestazione(12, prestazioneCrm.LavoratoreCap));
                        if (!string.IsNullOrEmpty(prestazioneCrm.LavoratoreEmail))
                            dettagli.Add(new dettaglioPrestazione(14, prestazioneCrm.LavoratoreEmail));
                        if (!string.IsNullOrEmpty(prestazioneCrm.FamiliareCognome))
                            dettagli.Add(new dettaglioPrestazione(15, prestazioneCrm.FamiliareCognome));
                        if (!string.IsNullOrEmpty(prestazioneCrm.FamiliareNome))
                            dettagli.Add(new dettaglioPrestazione(16, prestazioneCrm.FamiliareNome));
                        if (prestazioneCrm.FamiliareDataNascita.HasValue)
                            dettagli.Add(new dettaglioPrestazione(17,
                                prestazioneCrm.FamiliareDataNascita.Value.Date.ToString("dd/MM/yyyy")));
                        if (!string.IsNullOrEmpty(prestazioneCrm.FamiliareCodiceFiscale))
                            dettagli.Add(new dettaglioPrestazione(18, prestazioneCrm.FamiliareCodiceFiscale));
                        if (prestazioneCrm.ImportoErogatoLordo.HasValue)
                            dettagli.Add(new dettaglioPrestazione(19, prestazioneCrm.ImportoErogatoLordo.ToString()));
                        if (prestazioneCrm.ImportoErogato.HasValue)
                            dettagli.Add(new dettaglioPrestazione(20, prestazioneCrm.ImportoErogato.ToString()));
                        if (!string.IsNullOrEmpty(prestazioneCrm.ModalitaPagamento))
                            dettagli.Add(new dettaglioPrestazione(21, prestazioneCrm.ModalitaPagamento));
                        if (!string.IsNullOrEmpty(prestazioneCrm.NumeroMandato))
                            dettagli.Add(new dettaglioPrestazione(22, prestazioneCrm.NumeroMandato));
                        if (!string.IsNullOrEmpty(prestazioneCrm.StatoAssegno))
                            dettagli.Add(new dettaglioPrestazione(23, prestazioneCrm.StatoAssegno));
                        if (!string.IsNullOrEmpty(prestazioneCrm.DescrizioneStatoAssegno))
                            dettagli.Add(new dettaglioPrestazione(24, prestazioneCrm.DescrizioneStatoAssegno));
                        if (prestazioneCrm.DataLiquidazione.HasValue)
                            dettagli.Add(new dettaglioPrestazione(25,
                                prestazioneCrm.DataLiquidazione.Value.Date.ToString(
                                    "dd/MM/yyyy")));
                        if (!string.IsNullOrEmpty(prestazioneCrm.IdTipoPagamento))
                            dettagli.Add(new dettaglioPrestazione(26, prestazioneCrm.IdTipoPagamento));
                        if (!string.IsNullOrEmpty(prestazioneCrm.Iban))
                            dettagli.Add(new dettaglioPrestazione(27, prestazioneCrm.Iban));
                        if (!string.IsNullOrEmpty(prestazioneCrm.TestoGruppo))
                            dettagli.Add(new dettaglioPrestazione(28, prestazioneCrm.TestoGruppo));


                        prestazione.dettaglio = dettagli;
                        prestazione.causali = prestazioneCrm.CausaliRespinta;
                        prestazione.documenti = prestazioneCrm.DocumentiMancanti;

                        arrayPrestazioni[i] = prestazione;
                    }

                    result = ws.ManagePrestazioni(arrayPrestazioni);

                    // Per il Log
                    if (logPrestazioni == "true")
                    {
                        using (EventLog appLog = new EventLog())
                        {
                            appLog.Source = "SiceInfo";

                            appLog.WriteEntry(
                                string.Format(
                                    "{0} - CRM Prestazioni [{1}] - risposta: {2}",
                                    DateTime.Now, listaPrestazioni.Count, result));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Per il Log
                if (logPrestazioni == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(string.Format("Eccezione CRMPrestazioni: {0} - {1}", e.Message,
                            e.InnerException));
                    }
                }
            }

            return result;
        }
    }
}