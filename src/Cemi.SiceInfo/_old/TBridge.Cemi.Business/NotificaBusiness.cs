using System;
using System.Collections.Generic;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Entities.Notifiche;

//using TBridge.Cemi.ActivityTracking;

namespace TBridge.Cemi.Business
{
    public class NotificaBusiness
    {
        private readonly NotificaDataAccess _notificaDataAccess;

        public NotificaBusiness()
        {
            _notificaDataAccess = new NotificaDataAccess();
        }

        public void Assunzione(Notifica notifica)
        {
            _notificaDataAccess.AssunzioneNotifica(notifica);
        }

        public void Cessazione(Notifica notifica)
        {
            try
            {
                switch (notifica.TipoNotifica)
                {
                    case TipoNotifica.CessazioneDenuncia:
                        _notificaDataAccess.CessazioneDenuncia(notifica);
                        break;
                    case TipoNotifica.CessazioneNotifica:
                        _notificaDataAccess.CessazioneNotifica(notifica);
                        break;
                    default:
                        throw new Exception("Notifica non valida");
                }
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.Cessazione", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);
                throw;
            }
        }

        public List<Notifica> Ricerca(string codiceFiscale, string cognome, string nome, DateTime? dataNascita,
            int idImpresa)
        {
            try
            {
                return _notificaDataAccess.RicercaNotificheDenunce(codiceFiscale, cognome, nome, dataNascita,
                    idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.Ricerca", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new List<Notifica>();
            }
        }

        public List<Notifica> RapportiCessazioni(int idImpresa)
        {
            try
            {
                return _notificaDataAccess.RicercaRapportiCessazioni(idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.RapportiCessazioni", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new List<Notifica>();
            }
        }

        public List<Notifica> RapportiAssunzioni(int idImpresa)
        {
            try
            {
                return _notificaDataAccess.RicercaRapportiAssunzioni(idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.RapportiAssunzioni", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new List<Notifica>();
            }
        }

        public List<Notifica> NotificheNonDenunciate()
        {
            try
            {
                return _notificaDataAccess.RicercaNotificheNonDenunciate();
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.NotificheNonDenunciate", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new List<Notifica>();
            }
        }

        public List<Notifica> NotificheRapportiCessatiDenunciati()
        {
            try
            {
                return _notificaDataAccess.RicercaNotificheRapportiCessatiDenunciati();
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.NotificheRapportiCessatiDenunciati", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new List<Notifica>();
            }
        }

        public List<Notifica> RicercaPerSubappalti(string cognome, string nome, int idImpresa)
        {
            try
            {
                return _notificaDataAccess.RicercaLavoratoriImpresa(cognome, nome, idImpresa);
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.RicercaPerSubappalti", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);

                return new List<Notifica>();
            }
        }

        public int MesiRitardoDati()
        {
            int res = 0;

            try
            {
                res = _notificaDataAccess.MesiRitardoDati();
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.Notifiche.Business.NotificaBusiness.MesiRitardoDati", logItemCollection, Log.categorie.NOTIFICHE, Log.sezione.SYSTEMEXCEPTION);
            }

            return res;
        }

        public List<Impresa> ImpreseConsulente(int idConsulente)
        {
            return _notificaDataAccess.GetImpreseConsulente(idConsulente);
        }
    }
}