﻿using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Business.Geocode
{
    public class GeocodeProvider
    {
        public static List<Indirizzo> Geocode(string address)
        {
            return GoogleProvider.Geocode(address);
        }
    }
}