﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Geocoding.Google;
using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Business.Geocode
{
    internal class GoogleProvider
    {
        public static List<Indirizzo> Geocode(string indirizzo)
        {
            List<Indirizzo> ret = new List<Indirizzo>();

            try
            {
                GoogleGeocoder geocoder = new GoogleGeocoder(ConfigurationManager.AppSettings["GoogleMapApiKey"]) {Language = "it"};
                IEnumerable<GoogleAddress> addresses = geocoder.GeocodeAsync(indirizzo).Result;

                ret.AddRange(addresses.Select(address => new Indirizzo
                {
                    Cap = address[GoogleAddressType.PostalCode]?.LongName,
                    Comune = address[GoogleAddressType.AdministrativeAreaLevel3]?.LongName,
                    Provincia = address[GoogleAddressType.AdministrativeAreaLevel2]?.ShortName,
                    Stato = address[GoogleAddressType.Country]?.LongName,
                    NomeVia = address[GoogleAddressType.Route]?.LongName,
                    Civico = address[GoogleAddressType.StreetNumber]?.LongName,

                    Latitudine = Convert.ToDecimal(address.Coordinates.Latitude),
                    Longitudine = Convert.ToDecimal(address.Coordinates.Longitude)
                }));
            }
            catch (Exception ex)
            {
                //
            }

            return ret;
        }
    }
}