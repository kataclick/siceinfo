﻿using System.Collections.Generic;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using TipoContratto = TBridge.Cemi.Type.Entities.Sintesi.TipoContratto;

namespace TBridge.Cemi.Business.Sintesi
{
    public class SintesiBusiness
    {
        private readonly SintesiDataAccess _sintesiDataAccess = new SintesiDataAccess();

        public List<StatusStraniero> GetStatusStraniero()
        {
            return _sintesiDataAccess.GetStatusStraniero();
        }

        public List<MotivoPermesso> GetMotiviPermesso()
        {
            return _sintesiDataAccess.GetMotiviPermesso();
        }

        public List<TitoloDiStudio> GetTitoliDiStudio()
        {
            return _sintesiDataAccess.GetTitoliDiStudio();
        }

        public List<TipoComunicazione> GetTipiComunicazione()
        {
            return _sintesiDataAccess.GetTipiComunicazione();
        }

        //NON USATO
        public List<TipoContratto> GetTipiContratto()
        {
            return _sintesiDataAccess.GetTipiContratto();
        }

        public List<CessazioneRL> GetCessazioniRL()
        {
            return _sintesiDataAccess.GetCessazioniRL();
        }

        public List<TrasformazioneRL> GetTrasformazioniRL()
        {
            return _sintesiDataAccess.GetTrasformazioniRL();
        }

        public List<CCNL> GetCCNL()
        {
            return _sintesiDataAccess.GetCCNL();
        }

        public List<TipoOrario> GetTipiOrario()
        {
            return _sintesiDataAccess.GetTipiOrario();
        }

        public Type.Entities.TipoContratto GetTipoContratto(string idTipoContratto)
        {
            return _sintesiDataAccess.GetTipoContratto(idTipoContratto);
        }

        public TitoloDiStudio GetTitoloDiStudio(string codice)
        {
            return _sintesiDataAccess.GetTitoloDiStudio(codice);
        }

        public NazionalitaSiceNew GetNazionalita(string idNazione)
        {
            return _sintesiDataAccess.GetNazionalita(idNazione);
        }

        public TipoInizioRapporto GetTipoInizioRapporto(string idTipoInizioRapporto)
        {
            // TODO potrebbe essere necessaria modifica per Aggiormaneto Contr. Nazionale 10/1/2014
            return _sintesiDataAccess.GetTipoInizioRapporto(idTipoInizioRapporto);
        }

        public TipoQualifica GetTipoQualifica(string idQualifica)
        {
            return _sintesiDataAccess.GetTipoQualifica(idQualifica);
        }

        public List<TipoQualifica> GetTipiQualifica(string tipoCategoria)
        {
            return _sintesiDataAccess.GetTipiQualifica(tipoCategoria);
        }

        public List<TipoCategoria> GetTipiCategoria()
        {
            return _sintesiDataAccess.GetTipiCategoria();
        }

        public List<TipoCategoria> GetTipiCategoria(string tipoContratto)
        {
            return _sintesiDataAccess.GetTipiCategoria(tipoContratto);
        }
    }
}