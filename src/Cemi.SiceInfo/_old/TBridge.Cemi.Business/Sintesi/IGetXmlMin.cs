namespace TBridge.Cemi.Business.Sintesi
{
    public interface IGetXmlMin
    {
        string getXmlMinFromId(string idComunicazione);

        string getXmlMinFromIdAuth(string idComunicazione, string username, string password);
    }
}