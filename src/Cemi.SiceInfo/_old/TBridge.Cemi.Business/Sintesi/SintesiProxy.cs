using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.Business.Sintesi
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "getXmlMinSoap", Namespace = "http://tempuri.org/")]
    public class GetXmlMin : SoapHttpClientProtocol, IGetXmlMin
    {
        private SendOrPostCallback getXmlMinFromIdAuthOperationCompleted;
        private SendOrPostCallback getXmlMinFromIdOperationCompleted;

        /// <remarks />
        public GetXmlMin()
        {
            string urlSetting = ConfigurationManager.AppSettings["getXmlMin.ServiceEndpointURL"];
            if (urlSetting != null
                && urlSetting != "")
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://sintesi.provincia.milano.it/sintesi/interoperability/getxmlmin.asmx";
            }
        }

        /// <remarks />
        public event getXmlMinFromIdCompletedEventHandler getXmlMinFromIdCompleted;

        /// <remarks />
        public event getXmlMinFromIdAuthCompletedEventHandler getXmlMinFromIdAuthCompleted;

        /// <remarks />
        public void getXmlMinFromIdAsync(string idComunicazione)
        {
            getXmlMinFromIdAsync(idComunicazione, null);
        }

        /// <remarks />
        public void getXmlMinFromIdAsync(string idComunicazione, object userState)
        {
            if (getXmlMinFromIdOperationCompleted == null)
            {
                getXmlMinFromIdOperationCompleted = OngetXmlMinFromIdOperationCompleted;
            }
            InvokeAsync("getXmlMinFromId", new object[]
            {
                idComunicazione
            }, getXmlMinFromIdOperationCompleted, userState);
        }

        private void OngetXmlMinFromIdOperationCompleted(object arg)
        {
            if (getXmlMinFromIdCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                getXmlMinFromIdCompleted(this,
                    new getXmlMinFromIdCompletedEventArgs(invokeArgs.Results, invokeArgs.Error,
                        invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void getXmlMinFromIdAuthAsync(string idComunicazione, string username, string password)
        {
            getXmlMinFromIdAuthAsync(idComunicazione, username, password, null);
        }

        /// <remarks />
        public void getXmlMinFromIdAuthAsync(string idComunicazione, string username, string password, object userState)
        {
            if (getXmlMinFromIdAuthOperationCompleted == null)
            {
                getXmlMinFromIdAuthOperationCompleted = OngetXmlMinFromIdAuthOperationCompleted;
            }
            InvokeAsync("getXmlMinFromIdAuth", new object[]
            {
                idComunicazione,
                username,
                password
            }, getXmlMinFromIdAuthOperationCompleted, userState);
        }

        private void OngetXmlMinFromIdAuthOperationCompleted(object arg)
        {
            if (getXmlMinFromIdAuthCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                getXmlMinFromIdAuthCompleted(this,
                    new getXmlMinFromIdAuthCompletedEventArgs(invokeArgs.Results,
                        invokeArgs.Error,
                        invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        #region IGetXmlMin Members

        /// <remarks />
        [SoapDocumentMethod("http://tempuri.org/getXmlMinFromId", RequestNamespace = "http://tempuri.org/",
            ResponseNamespace = "http://tempuri.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public string getXmlMinFromId([XmlElement(ElementName = "idComunicazione")] string idComunicazione)
        {
            object[] results = Invoke("getXmlMinFromId", new object[]
            {
                idComunicazione
            });
            return (string) results[0];
        }

        /// <remarks />
        [SoapDocumentMethod("http://tempuri.org/getXmlMinFromIdAuth", RequestNamespace = "http://tempuri.org/",
            ResponseNamespace = "http://tempuri.org/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public string getXmlMinFromIdAuth([XmlElement(ElementName = "idComunicazione")] string idComunicazione,
            [XmlElement(ElementName = "username")] string username,
            [XmlElement(ElementName = "password")] string password)
        {
            object[] results = Invoke("getXmlMinFromIdAuth", new object[]
            {
                idComunicazione,
                username,
                password
            });
            return (string) results[0];
        }

        #endregion
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    public delegate void getXmlMinFromIdCompletedEventHandler(object sender, getXmlMinFromIdCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class getXmlMinFromIdCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal getXmlMinFromIdCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public string Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (string) results[0];
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    public delegate void getXmlMinFromIdAuthCompletedEventHandler(object sender, getXmlMinFromIdAuthCompletedEventArgs e
    );


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class getXmlMinFromIdAuthCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal getXmlMinFromIdAuthCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public string Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (string) results[0];
            }
        }
    }
}