using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.Business.Office
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "WordServiceSoap", Namespace = "http://www.cassaedilemilano.it/")]
    public class WordService : SoapHttpClientProtocol, IWordService
    {
        private SendOrPostCallback CreateWordOperationCompleted;

        /// <remarks />
        public WordService()
        {
            string urlSetting = ConfigurationManager.AppSettings["WordService.ServiceEndpointURL"];
            if (urlSetting != null
                && urlSetting != "")
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://localhost:9000/WordService.asmx";
            }
        }

        /// <remarks />
        [SoapDocumentMethod("http://www.cassaedilemilano.it/CreateWord",
            RequestNamespace = "http://www.cassaedilemilano.it/", ResponseNamespace = "http://www.cassaedilemilano.it/",
            Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        public string CreateWord([XmlElement(ElementName = "templatePath")] string templatePath,
            [XmlArray(ElementName = "properties")] WordField[] properties,
            [XmlElement(ElementName = "pathGenerazioneDocumenti")] string pathGenerazioneDocumenti)
        {
            object[] results = Invoke("CreateWord", new object[]
            {
                templatePath,
                properties,
                pathGenerazioneDocumenti
            });
            return (string) results[0];
        }

        /// <remarks />
        public event CreateWordCompletedEventHandler CreateWordCompleted;

        /// <remarks />
        public void CreateWordAsync(string templatePath, WordField[] properties, string pathGenerazioneDocumenti)
        {
            CreateWordAsync(templatePath, properties, pathGenerazioneDocumenti, null);
        }

        /// <remarks />
        public void CreateWordAsync(string templatePath, WordField[] properties, string pathGenerazioneDocumenti,
            object userState)
        {
            if (CreateWordOperationCompleted == null)
            {
                CreateWordOperationCompleted = OnCreateWordOperationCompleted;
            }
            InvokeAsync("CreateWord", new object[]
            {
                templatePath,
                properties,
                pathGenerazioneDocumenti
            }, CreateWordOperationCompleted, userState);
        }

        private void OnCreateWordOperationCompleted(object arg)
        {
            if (CreateWordCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                CreateWordCompleted(this,
                    new CreateWordCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.3053")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/", TypeName = "WordField")]
    public class WordField
    {
        /// <remarks />
        [XmlElement(ElementName = "Campo")] public string Campo;

        /// <remarks />
        [XmlElement(ElementName = "Valore")] public string Valore;

        public WordField()
        {
        }

        public WordField(string campo, string valore)
        {
            Campo = campo;
            Valore = valore;
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    public delegate void CreateWordCompletedEventHandler(object sender, CreateWordCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.3053")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class CreateWordCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal CreateWordCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public string Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (string) results[0];
            }
        }
    }
}