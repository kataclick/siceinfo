using System.Collections.Generic;

namespace TBridge.Cemi.Business.Office
{
    public static class WordManager
    {
        public static string CreateWord(string templatePath, IEnumerable<Type.WordField> ds,
            string pathGenerazioneDocumenti)
        {
            string ret;
            List<Type.WordField> dsList = (List<Type.WordField>) ds;
            WordService wordService = new WordService();
            WordField[] properties = new WordField[dsList.Count];
            for (int i = 0; i < dsList.Count; i++)
            {
                Type.WordField field = dsList[i];
                WordField property = new WordField();
                property.Campo = field.Campo;
                property.Valore = field.Valore;
                properties[i] = property;
            }
            ret = wordService.CreateWord(templatePath, properties, pathGenerazioneDocumenti);
            return ret;
        }
    }
}