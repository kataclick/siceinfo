namespace TBridge.Cemi.Business.Office
{
    public interface IWordService
    {
        string CreateWord(string templatePath, WordField[] properties, string pathGenerazioneDocumenti);
    }
}