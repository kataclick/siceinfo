using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Enums.Deleghe;
using TBridge.Cemi.Type.Filters.Deleghe;

namespace TBridge.Cemi.Business.Deleghe
{
    public class DelegheBusiness
    {
        private readonly Common _commonBiz;
        private readonly DelegheDataAccess _dataAccess;
        private readonly DelegheWordManager _wordManager;

        public DelegheBusiness()
        {
            _dataAccess = new DelegheDataAccess();
            _commonBiz = new Common();
            _wordManager = new DelegheWordManager();
        }

        public List<Lavoratore> GetLavoratori(LavoratoreFilter filtro)
        {
            return _dataAccess.GetLavoratori(filtro);
        }

        /// <summary>
        /// </summary>
        /// <param name="delega"></param>
        /// <returns></returns>
        public bool InsertUpdateDelega(Delega delega)
        {
            bool res;

            if (!delega.IdDelega.HasValue)
                res = _dataAccess.InsertDelega(delega);
            else
                res = _dataAccess.UpdateDelega(delega);

            return res;
        }

        /// <summary>
        ///     Genera un codice di sblocco delega
        /// </summary>
        public string GeneraCodiceSbloccoDelega()
        {
            return Guid.NewGuid().ToString().ToUpper().Substring(0, 8);
        }

        public bool SbloccoOmonimieAttivo()
        {
            bool sbloccoOmonimieAttivo;
            if (
                !bool.TryParse(ConfigurationManager.AppSettings["DelegheSbloccoOmonimieAttivo"],
                    out sbloccoOmonimieAttivo))
                sbloccoOmonimieAttivo = false;

            return sbloccoOmonimieAttivo;
        }

        public StringCollection ControllaDelega(Delega delega, out bool sbloccoDelega)
        {
            StringCollection problemi = new StringCollection();
            bool omonimia = false;

            // Effettua i controlli sulla delega

            if (EsisteCodiceDelega(delega.CodiceDelega) > 0)
            {
                problemi.Add("Codice Delega gi� presente a sistema.");
            }
            else
            { 
                if (DelegheConfermate(DateTime.Now.Month, DateTime.Now.Year, delega.Sindacato,
                    delega.ComprensorioSindacale))
                {
                    problemi.Add("Le deleghe per il comprensorio selezionato sono gi� state confermate.");
                }
                else
                {
                    // Verifica se il lavoratore ha deleghe sottoscritte attive (tabella LavoratoriDeleghe)
                    string sindacato =
                        DelegaAttiva(delega.Lavoratore.Cognome, delega.Lavoratore.Nome,
                            delega.Lavoratore.DataNascita.Value);
                    if (sindacato != null)
                    {
                        // Cambiare il messaggio in base al sindacato di cui si ha la delega e alla persona loggata
                        if (delega.OperatoreInserimento.Sindacato.Id == sindacato)
                            problemi.Add("E' gi� presente una delega attiva presso il tuo sindacato.");
                        else
                            problemi.Add("E' gi� presente una delega attiva presso un'altra organizzazione sindacale.");

                        omonimia = true;
                    }
                    else
                    {
                        // Verifica se il lavoratore ha deleghe sottoscritte non attive immesse nei mesi precedenti
                        sindacato =
                            DelegaNonAttivaPrecedente(delega.Lavoratore.Cognome, delega.Lavoratore.Nome,
                                delega.Lavoratore.DataNascita.Value);
                        if (sindacato != null)
                        {
                            // Cambiare il messaggio in base al sindacato di cui si ha la delega e alla persona loggata
                            if (delega.OperatoreInserimento.Sindacato.Id == sindacato)
                                problemi.Add(
                                    "E' gi� stata confermata precedentemente una comunicazione delega per il lavoratore per il tuo sindacato.");
                            else
                                problemi.Add(
                                    "E' gi� stata confermata precedentemente una comunicazione delega per il lavoratore da un altra organizzazione sindacale.");

                            omonimia = true;
                        }
                        else
                        {
                            // Verifica se il lavoratore ha deleghe sottoscritte non attive precedentemente immesse (quindi dello stesso sindacato o comprensorio)
                            if (
                                DelegaGiaImmessa(delega.Lavoratore.Cognome, delega.Lavoratore.Nome,
                                    delega.Lavoratore.DataNascita.Value, delega.Sindacato.Id))
                            {
                                problemi.Add(
                                    "E' gi� stata inserita una comunicazione delega con i dati forniti in questo mese dal tuo sindacato");
                            }

                            omonimia = true;
                        }
                    }
                }
            }

            // Permetto l'inserimento bloccato se � attivo e abbiamo un caso di omonimia
            sbloccoDelega = SbloccoOmonimieAttivo() && omonimia;
            return problemi;
        }

        private bool DelegaGiaImmessa(string cognome, string nome, DateTime dataNascita, string sindacato)
        {
            return _dataAccess.DelegaGiaImmessa(cognome, nome, dataNascita, sindacato);
        }

        private string DelegaNonAttivaPrecedente(string cognome, string nome, DateTime dataNascita)
        {
            return _dataAccess.DelegaNonAttivaPrecedente(cognome, nome, dataNascita);
        }

        public string DelegaAttiva(string cognome, string nome, DateTime dataNascita)
        {
            return _dataAccess.DelegaAttiva(cognome, nome, dataNascita);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<ComprensorioSindacale> GetComprensori()
        {
            return GetComprensori(false);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<ComprensorioSindacale> GetComprensori(bool selezionabili)
        {
            return _commonBiz.GetComprensori(selezionabili);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<Sindacato> GetSindacati()
        {
            return _commonBiz.GetSindacati();
        }

        /// <summary>
        ///     Recupera la lista dei sindaclisti di un certo comprensorio e sindacato
        /// </summary>
        /// <param name="sindacato"></param>
        /// <param name="comprensorio"></param>
        /// <returns></returns>
        public SindacalistiCollection GetSindacalisti(Sindacato sindacato, ComprensorioSindacale comprensorio)
        {
            return _dataAccess.GetSindacalisti(sindacato, comprensorio);
        }

        public Delega GetDelega(string codiceSblocco, string idSindacato)
        {
            return _dataAccess.GetDelega(codiceSblocco, idSindacato);
        }

        public Delega GetDelega(int idDelega)
        {
            return _dataAccess.GetDelega(idDelega);
        }

        public List<Delega> GetDeleghe(DelegheFilter filtro)
        {
            return _dataAccess.GetDeleghe(filtro);
        }

        public List<Delega> GetDelegheBloccate()
        {
            return _dataAccess.GetDelegheBloccate();
        }

        public bool DelegheConfermate(int meseConferma, int annoConferma, Sindacato sindacato,
            ComprensorioSindacale comprensorioSindacale)
        {
            DateTime mese = new DateTime(annoConferma, meseConferma, 1);
            return _dataAccess.DelegheConfermate(mese, sindacato, comprensorioSindacale);
        }

        public int EsisteCodiceDelega(string codiceDelega)
        {
            return _dataAccess.EsisteCodiceDelega(codiceDelega);
        }

        public int EsistonoDelegheNonConfermate(Sindacato sindacato, ComprensorioSindacale comprensorioSindacale)
        {
            return _dataAccess.EsistonoDelegheNonConfermate(sindacato, comprensorioSindacale);
        }

        public bool ConfermaDeleghe(Sindacato sindacato, ComprensorioSindacale comprensorioSindacale,
            StatoDelega statoDelega)
        {
            return _dataAccess.ConfermaDeleghe(sindacato, comprensorioSindacale, statoDelega);
        }

        public bool CambiaStatoDelega(int idDelega, StatoDelega stato)
        {
            return _dataAccess.CambiaStatoDelega(idDelega, stato);
        }

        public bool DeleteDelega(int idDelega)
        {
            return _dataAccess.DeleteDelega(idDelega);
        }

        public bool SbloccaDelega(int idDelega)
        {
            return _dataAccess.SbloccaDelega(idDelega);
        }

        public string GeneraLettera(TipologiaLettera tipoLettera, LetteraParam param)
        {
            string ret;

            List<WordField> ds = new List<WordField>();

            WordField wfDataLettera = new WordField
            {
                Campo = "DataLettera",
                Valore = DateTime.Today.ToShortDateString()
            };
            ds.Add(wfDataLettera);

            WordField wfProtocollo = new WordField
            {
                Campo = "NumeroProtocollo",
                Valore = param.Protocollo
            };
            ds.Add(wfProtocollo);

            if (param.DataAdesione.HasValue)
            {
                WordField wfDataDeleghe = new WordField
                {
                    Campo = "DataDeleghe",
                    Valore = param.DataAdesione.Value.ToShortDateString()
                };
                ds.Add(wfDataDeleghe);
            }

            List<WordField> dsTemp = _dataAccess.GetWordDataSource(tipoLettera, param);
            ds.AddRange(dsTemp);

            ret = _wordManager.CreateWord(tipoLettera, ds);

            return ret;
        }

        public bool ApertaFaseInserimento()
        {
            bool res = false;

            DateTime? aperturaInserimento = _dataAccess.GetAperturaFaseInserimento();
            if (aperturaInserimento.HasValue)
            {
                if (DateTime.Now > aperturaInserimento.Value && DateTime.Now.Month == aperturaInserimento.Value.Month &&
                    DateTime.Now.Year == aperturaInserimento.Value.Year)
                    res = true;
            }

            return res;
        }

        public List<Delega> GetDeleghePerArchiDoc()
        {
            return _dataAccess.GetDeleghePerArchiDoc();
        }

        public bool ForzaUpdateDelegaBloccata(Delega delega, int idUtente)
        {
            return _dataAccess.ForzaUpdateDelegaBloccata(delega, idUtente);
        }

        public List<StoricoDelegaBloccata> GetStoricoDelegaBloccata(int idDelega)
        {
            return _dataAccess.GetStoricoDelegaBloccata(idDelega);
        }

        public bool CheckModificaDelegaNonInseribile(Delega delega)
        {
            return _dataAccess.CheckModificaDelegaNonInseribile(delega);
        }

        public void ImpostaStampa(int idDelega)
        {
            _dataAccess.ImpostaStampa(idDelega);
        }
    }
}