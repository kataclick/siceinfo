using System;
using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Business.Office;
using TBridge.Cemi.Type.Enums.Deleghe;
using WordField = TBridge.Cemi.Type.WordField;

namespace TBridge.Cemi.Business.Deleghe
{
    public class DelegheWordManager
    {
        private readonly string _pathGenerazioneDocumenti =
            ConfigurationManager.AppSettings["PathGenerazioneDocumenti"];

        private readonly string _pathNonIscritti = ConfigurationManager.AppSettings["PathLetteraDelegheNonIscritti"];
        private readonly string _pathScadute = ConfigurationManager.AppSettings["PathLetteraDelegheScadute"];

        public string CreateWord(TipologiaLettera tipoLettera, List<WordField> ds)
        {
            string ret;

            switch (tipoLettera)
            {
                case TipologiaLettera.Scadute:
                    ret = WordManager.CreateWord(_pathScadute, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.NonIscritti:
                    ret = WordManager.CreateWord(_pathNonIscritti, ds, _pathGenerazioneDocumenti);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("tipoLettera");
            }

            return ret;
        }
    }
}