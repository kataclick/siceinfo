﻿using System;

namespace TBridge.Cemi.Business
{
    public class EdilconnectAudit
    {
        private readonly Data.Common _provider = new Data.Common();

        public void LogAccess(int idUtente, DateTime data, string url)
        {
            _provider.LogAccessEdilconnect(idUtente, data, url);
        }
    }
}