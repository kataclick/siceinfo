﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters.AnagraficaLavoratori;

namespace TBridge.Cemi.Business
{
    public class LavoratoreAnagraficaManager
    {
        public ComuniSiceNew GetComune(string comune)
        {
            ComuniSiceNew cm = new ComuniSiceNew();
            using (SICEEntities context = new SICEEntities())
            {
                var queryComuni = from com in context.ComuniSiceNew
                    where com.Comune == comune
                    select com;

                cm = queryComuni.SingleOrDefault();
            }

            return cm;
        }

        public ComuniSiceNew GetComune(string comune, string codiceFiscale)
        {
            ComuniSiceNew cm = new ComuniSiceNew();
            ComuneSiceNew cpro = new ComuneSiceNew();
            using (SICEEntities context = new SICEEntities())
            {
                var queryComuni = from com in context.ComuniSiceNew
                    where com.Comune == comune
                    select com;


                if (queryComuni.Count() > 1)
                {
                    cpro = CodiceFiscaleManager.ComuneSiceNewDaCodiceFiscale(codiceFiscale);
                    if (cpro != null)
                    {
                        cm.Comune = cpro.Comune;
                        cm.Provincia = cpro.Provincia;
                        cm.CAP = cpro.Cap;
                        cm.CodiceCatastale = cpro.CodiceCatastale;
                    }
                    else
                    {
                        cm = null;
                    }
                }
                else
                {
                    cm = queryComuni.SingleOrDefault();
                }
            }

            return cm;
        }

        public Lavoratore GetLavoratore(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (SICEEntities context = new SICEEntities())
            {
                var queryLav = from lav in context.Lavoratori
                    where lav.Id == idLavoratore
                    select new
                    {
                        lav,
                        lav.TipoVia,
                        rec = lav.Utente.SmsRecapitiUtenti.FirstOrDefault(r => !r.DataFineValidita.HasValue)
                    };

                //lavoratore = queryLav.SingleOrDefault(); 
                var lavRes = queryLav.SingleOrDefault();
                lavoratore = lavRes.lav;
                lavoratore.TipoVia = lavRes.TipoVia;
                lavoratore.cellulare = lavRes.rec != null ? lavRes.rec.Numero : lavoratore.cellulare;
                lavoratore.ServizioSMS = lavRes.rec != null;
            }

            return lavoratore;
        }

        public List<RapportoImpresaPersona> GetRapporti(int idLavoratore)
        {
            List<RapportoImpresaPersona> rapporti = null;

            using (SICEEntities context = new SICEEntities())
            {
                var queryRap = from rap in context.RapportiImpresaPersona.Include("Impresa")
                    where rap.IdLavoratore == idLavoratore
                    orderby rap.DataInizioValiditaRapporto descending
                    select rap;

                rapporti = queryRap.ToList();
            }

            return rapporti;
        }

        public List<DenunciaLavoratore> GetDenunce(int idLavoratore)
        {
            List<DenunciaLavoratore> oreDenunciate = new List<DenunciaLavoratore>();

            using (SICEEntities context = new SICEEntities())
            {
                var queryDen = (from ore in context.OreDenunciateCompleteImprese.Include("Impresa")
                    where ore.IdLavoratore == idLavoratore
                    //orderby ore.Data descending
                    select new DenunciaLavoratore
                    {
                        IdImpresa = ore.IdImpresa,
                        RagioneSocialeImpresa = ore.Impresa.RagioneSociale,
                        IdLavoratore = ore.IdLavoratore,
                        Data = ore.Data
                    }).Distinct().OrderByDescending(dl => dl.Data);
                oreDenunciate = queryDen.ToList();
            }

            return oreDenunciate;
        }

        public List<DenunciaLavoratore> GetUltimeDenunce(int idLavoratore)
        {
            List<DenunciaLavoratore> Denunce = GetDenunce(idLavoratore);

            List<DenunciaLavoratore> UltimeDenunce = new List<DenunciaLavoratore>();

            int i = 0;

            foreach (DenunciaLavoratore dl in Denunce)
            {
                UltimeDenunce.Add(dl);
                i++;
                if (i > 2)
                    break;
            }
            return UltimeDenunce;
        }

        public LavoratoreDelega GetDelegaAttiva(int idLavoratore)
        {
            LavoratoreDelega delega = null;

            using (SICEEntities context = new SICEEntities())
            {
                var queryDel = from dele in context.LavoratoriDeleghe.Include("SindacatoCorrente")
                    where dele.IdLavoratore == idLavoratore && !string.IsNullOrEmpty(dele.Sindacato)
                    select dele;

                delega = queryDel.SingleOrDefault();
            }

            return delega;
        }

        public List<Prestazione> GetPrestazioni(int idLavoratore, PrestazioneFilter filtro)
        {
            List<Prestazione> prestazioni = new List<Prestazione>();

            if (filtro == null)
                filtro = new PrestazioneFilter();

            using (SICEEntities context = new SICEEntities())
            {
                var queryPre = from pre in context.Prestazioni.Include("TipoPrestazione")
                    where pre.IdLavoratore == idLavoratore && pre.Stato == "L"
                          && (!filtro.Dal.HasValue || filtro.Dal.Value <= pre.DataDomanda)
                          && (!filtro.Al.HasValue || filtro.Al.Value >= pre.DataDomanda)
                          && (string.IsNullOrEmpty(filtro.IdTipoPrestazione) ||
                              filtro.IdTipoPrestazione == pre.IdPrestazione)
                    orderby pre.DataEmissioneAssegno descending, pre.DataDomanda descending
                    select pre;
                prestazioni = queryPre.ToList();
            }

            return prestazioni;
        }

        public List<Prestazione> GetUltimePrestazioni(int idLavoratore)
        {
            List<Prestazione> prestazioni = GetPrestazioni(idLavoratore, null);

            List<Prestazione> UltimePrestazioni = new List<Prestazione>();

            int i = 0;

            foreach (Prestazione p in prestazioni)
            {
                UltimePrestazioni.Add(p);
                i++;
                if (i > 2)
                    break;
            }
            return UltimePrestazioni;
        }

        public List<TipoPrestazione> GetTipiPrestazione(int idLavoratore)
        {
            List<TipoPrestazione> tipiPrestazione = new List<TipoPrestazione>();


            using (SICEEntities context = new SICEEntities())
            {
                var queryTipi = (from tipo in context.TipiPrestazione
                    join pre in context.Prestazioni on tipo.IdTipoPrestazione equals pre.IdPrestazione
                    where pre.IdLavoratore == idLavoratore
                    select tipo).Distinct().OrderBy(t => t.Descrizione);

                tipiPrestazione = queryTipi.ToList();
            }

            return tipiPrestazione;
        }


        public bool SaveRichiestaVariazioneRecapiti(
            LavoratoreRichiestaVariazioneRecapito lavoratoreRichiestaVariazioneRecapiti)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                context.LavoratoriRichiesteVariazioneRecapiti.AddObject(lavoratoreRichiestaVariazioneRecapiti);
                int numObj = context.SaveChanges();
                if (numObj > 0)
                    ret = true;
            }

            return ret;
        }

        public List<LavoratoreRichiestaVariazioneRecapito> GetRichiesteVariazioniRecapitiPendenti(int idLavoratore)
        {
            List<LavoratoreRichiestaVariazioneRecapito> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var queryRichiestePendenti =
                    from richiestaVariazioneRecapiti in context.LavoratoriRichiesteVariazioneRecapiti.Include(
                        "LavoratoreRecapito")
                    where
                        richiestaVariazioneRecapiti.IdLavoratore == idLavoratore &&
                        richiestaVariazioneRecapiti.Gestito == false
                    select richiestaVariazioneRecapiti;

                ret = queryRichiestePendenti.ToList();
            }

            return ret;
        }

        public void SaveStatoVariazioneIscrizioneSms(int idRichiesta, string messaggio)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var queryVariazioneSms =
                    from richiestaVariazioneRecapiti in context.LavoratoriRichiesteVariazioneRecapiti
                    where
                        richiestaVariazioneRecapiti.IdRichiesta == idRichiesta
                    select richiestaVariazioneRecapiti;

                LavoratoreRichiestaVariazioneRecapito rec = queryVariazioneSms.SingleOrDefault();
                if (rec != null)
                {
                    rec.DataGestioneSms = DateTime.Now;
                    rec.NotaSms = messaggio;

                    context.SaveChanges();
                }
            }
        }


        public bool SaveRichiestaVariazioneAnagrafica(
            LavoratoreRichiestaVariazioneAnagrafica lavoratoreRichiestaVariazioneAnagrafica)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                context.LavoratoriRichiesteVariazioniAnagrafica.AddObject(lavoratoreRichiestaVariazioneAnagrafica);
                int numObj = context.SaveChanges();
                if (numObj > 0)
                    ret = true;
            }

            return ret;
        }

        public List<LavoratoreRichiestaVariazioneAnagrafica> GetRichiesteVariazioniAnagrafichePendenti(int idLavoratore)
        {
            List<LavoratoreRichiestaVariazioneAnagrafica> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var queryRichiestePendenti =
                    from richiestaVariazioniAnagrafica in context.LavoratoriRichiesteVariazioniAnagrafica.Include(
                        "VariazioneAnagrafica")
                    where
                        richiestaVariazioniAnagrafica.IdLavoratore == idLavoratore &&
                        richiestaVariazioniAnagrafica.Gestito == false
                    select richiestaVariazioniAnagrafica;

                ret = queryRichiestePendenti.ToList();
            }

            return ret;
        }
    }
}