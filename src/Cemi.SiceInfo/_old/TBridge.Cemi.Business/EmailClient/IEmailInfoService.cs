namespace TBridge.Cemi.Business.EmailClient
{
    public interface IEmailInfoService
    {
        bool InviaEmail(EmailMessageSerializzabile email);

        EmailMessageSerializzabile[] InviaEmailCollection(EmailMessageSerializzabile[] listaMail);

        EmailMessageSerializzabile[] GetEMailDaInviare(EmailMessageFilter filtro);
    }
}