using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.Business.EmailClient
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "EmailInfoServiceSoap", Namespace = "http://www.cassaedilemilano.it/EmailInfoService/")]
    public class EmailInfoService : SoapHttpClientProtocol, IEmailInfoService
    {
        private SendOrPostCallback GetEMailDaInviareOperationCompleted;
        private SendOrPostCallback InviaEmailCollectionOperationCompleted;
        private SendOrPostCallback InviaEmailOperationCompleted;

        /// <remarks />
        public EmailInfoService()
        {
            string urlSetting = ConfigurationManager.AppSettings["EmailInfoService.ServiceEndpointURL"];
            if (urlSetting != null
                && urlSetting != "")
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://localhost:9100/emailinfoservice.asmx";
            }
        }

        /// <remarks />
        public event InviaEmailCompletedEventHandler InviaEmailCompleted;

        /// <remarks />
        public event InviaEmailCollectionCompletedEventHandler InviaEmailCollectionCompleted;

        /// <remarks />
        public event GetEMailDaInviareCompletedEventHandler GetEMailDaInviareCompleted;

        /// <remarks />
        public void InviaEmailAsync(EmailMessageSerializzabile email)
        {
            InviaEmailAsync(email, null);
        }

        /// <remarks />
        public void InviaEmailAsync(EmailMessageSerializzabile email, object userState)
        {
            if (InviaEmailOperationCompleted == null)
            {
                InviaEmailOperationCompleted = OnInviaEmailOperationCompleted;
            }
            InvokeAsync("InviaEmail", new object[]
            {
                email
            }, InviaEmailOperationCompleted, userState);
        }

        private void OnInviaEmailOperationCompleted(object arg)
        {
            if (InviaEmailCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                InviaEmailCompleted(this,
                    new InviaEmailCompletedEventArgs(invokeArgs.Results, invokeArgs.Error,
                        invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void InviaEmailCollectionAsync(EmailMessageSerializzabile[] listaMail)
        {
            InviaEmailCollectionAsync(listaMail, null);
        }

        /// <remarks />
        public void InviaEmailCollectionAsync(EmailMessageSerializzabile[] listaMail, object userState)
        {
            if (InviaEmailCollectionOperationCompleted == null)
            {
                InviaEmailCollectionOperationCompleted = OnInviaEmailCollectionOperationCompleted;
            }
            InvokeAsync("InviaEmailCollection", new object[]
            {
                listaMail
            }, InviaEmailCollectionOperationCompleted, userState);
        }

        private void OnInviaEmailCollectionOperationCompleted(object arg)
        {
            if (InviaEmailCollectionCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                InviaEmailCollectionCompleted(this,
                    new InviaEmailCollectionCompletedEventArgs(invokeArgs.Results,
                        invokeArgs.Error,
                        invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void GetEMailDaInviareAsync(EmailMessageFilter filtro)
        {
            GetEMailDaInviareAsync(filtro, null);
        }

        /// <remarks />
        public void GetEMailDaInviareAsync(EmailMessageFilter filtro, object userState)
        {
            if (GetEMailDaInviareOperationCompleted == null)
            {
                GetEMailDaInviareOperationCompleted = OnGetEMailDaInviareOperationCompleted;
            }
            InvokeAsync("GetEMailDaInviare", new object[]
            {
                filtro
            }, GetEMailDaInviareOperationCompleted, userState);
        }

        private void OnGetEMailDaInviareOperationCompleted(object arg)
        {
            if (GetEMailDaInviareCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                GetEMailDaInviareCompleted(this,
                    new GetEMailDaInviareCompletedEventArgs(invokeArgs.Results, invokeArgs.Error,
                        invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        #region IEmailInfoService Members

        /// <remarks />
        [SoapDocumentMethod("http://www.cassaedilemilano.it/EmailInfoService/InviaEmail",
            RequestNamespace = "http://www.cassaedilemilano.it/EmailInfoService/",
            ResponseNamespace = "http://www.cassaedilemilano.it/EmailInfoService/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public bool InviaEmail([XmlElement(ElementName = "email")] EmailMessageSerializzabile email)
        {
            object[] results = Invoke("InviaEmail", new object[]
            {
                email
            });
            return (bool) results[0];
        }

        /// <remarks />
        [SoapDocumentMethod("http://www.cassaedilemilano.it/EmailInfoService/InviaEmailCollection",
            RequestNamespace = "http://www.cassaedilemilano.it/EmailInfoService/",
            ResponseNamespace = "http://www.cassaedilemilano.it/EmailInfoService/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public EmailMessageSerializzabile[] InviaEmailCollection(
            [XmlArray(ElementName = "listaMail")] EmailMessageSerializzabile[] listaMail)
        {
            object[] results = Invoke("InviaEmailCollection", new object[]
            {
                listaMail
            });
            return (EmailMessageSerializzabile[]) results[0];
        }

        /// <remarks />
        [SoapDocumentMethod("http://www.cassaedilemilano.it/EmailInfoService/GetEMailDaInviare",
            RequestNamespace = "http://www.cassaedilemilano.it/EmailInfoService/",
            ResponseNamespace = "http://www.cassaedilemilano.it/EmailInfoService/", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public EmailMessageSerializzabile[] GetEMailDaInviare(
            [XmlElement(ElementName = "filtro")] EmailMessageFilter filtro)
        {
            object[] results = Invoke("GetEMailDaInviare", new object[]
            {
                filtro
            });
            return (EmailMessageSerializzabile[]) results[0];
        }

        #endregion
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.4927")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/", TypeName = "EmailMessageSerializzabile")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class EmailMessageSerializzabile
    {
        /// <remarks />
        private List<EmailAttachment> allegati;

        /// <remarks />
        private string bodyHTML;

        /// <remarks />
        private string bodyPlain;

        /// <remarks />
        private DateTime dataInvio;

        /// <remarks />
        private DateTime dataSchedulata;

        /// <remarks />
        private List<EmailAddress> destinatari;

        /// <remarks />
        private List<EmailAddress> destinatariBCC;

        /// <remarks />
        private List<EmailAddress> destinatariCC;

        /// <remarks />
        private bool esistonoAllegati;

        /// <remarks />
        private int? idEmail;

        /// <remarks />
        private EmailAddress mittente;

        /// <remarks />
        private string oggetto;

        /// <remarks />
        private DeliveryNotificationOptions opzioniNotifica;

        /// <remarks />
        private MailPriority priorita;

        /// <remarks />
        private EmailAddress replyTo;

        public EmailMessageSerializzabile()
        {
        }

        public EmailMessageSerializzabile(int? idEmail, string bodyPlain, string bodyHTML,
            DateTime dataSchedulata, DateTime dataInvio, bool esistonoAllegati,
            EmailAddress mittente, List<EmailAddress> destinatari,
            List<EmailAddress> destinatariCC, List<EmailAddress> destinatariBCC,
            string oggetto, MailPriority priorita, EmailAddress replyTo,
            DeliveryNotificationOptions opzioniNotifica, List<EmailAttachment> allegati)
        {
            this.idEmail = idEmail;
            this.bodyPlain = bodyPlain;
            this.bodyHTML = bodyHTML;
            this.dataSchedulata = dataSchedulata;
            this.dataInvio = dataInvio;
            this.esistonoAllegati = esistonoAllegati;
            this.mittente = mittente;
            this.destinatari = destinatari;
            this.destinatariCC = destinatariCC;
            this.destinatariBCC = destinatariBCC;
            this.oggetto = oggetto;
            this.priorita = priorita;
            this.replyTo = replyTo;
            this.opzioniNotifica = opzioniNotifica;
            this.allegati = allegati;
        }

        [XmlElement(IsNullable = true, ElementName = "IdEmail")]
        public int? IdEmail
        {
            get => idEmail;
            set
            {
                if (idEmail != value)
                {
                    idEmail = value;
                }
            }
        }

        [XmlElement(ElementName = "BodyPlain")]
        public string BodyPlain
        {
            get => bodyPlain;
            set
            {
                if (bodyPlain != value)
                {
                    bodyPlain = value;
                }
            }
        }

        [XmlElement(ElementName = "BodyHTML")]
        public string BodyHTML
        {
            get => bodyHTML;
            set
            {
                if (bodyHTML != value)
                {
                    bodyHTML = value;
                }
            }
        }

        [XmlElement(ElementName = "DataSchedulata")]
        public DateTime DataSchedulata
        {
            get => dataSchedulata;
            set
            {
                if (dataSchedulata != value)
                {
                    dataSchedulata = value;
                }
            }
        }

        [XmlElement(ElementName = "DataInvio")]
        public DateTime DataInvio
        {
            get => dataInvio;
            set
            {
                if (dataInvio != value)
                {
                    dataInvio = value;
                }
            }
        }

        [XmlElement(ElementName = "EsistonoAllegati")]
        public bool EsistonoAllegati
        {
            get => esistonoAllegati;
            set
            {
                if (esistonoAllegati != value)
                {
                    esistonoAllegati = value;
                }
            }
        }

        [XmlElement(ElementName = "Mittente")]
        public EmailAddress Mittente
        {
            get => mittente;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Mittente");
                }
                if (mittente != value)
                {
                    mittente = value;
                }
            }
        }

        [XmlArray(ElementName = "Destinatari")]
        public List<EmailAddress> Destinatari
        {
            get => destinatari;
            set
            {
                if (destinatari != value)
                {
                    destinatari = value;
                }
            }
        }

        [XmlArray(ElementName = "DestinatariCC")]
        public List<EmailAddress> DestinatariCC
        {
            get => destinatariCC;
            set
            {
                if (destinatariCC != value)
                {
                    destinatariCC = value;
                }
            }
        }

        [XmlArray(ElementName = "DestinatariBCC")]
        public List<EmailAddress> DestinatariBCC
        {
            get => destinatariBCC;
            set
            {
                if (destinatariBCC != value)
                {
                    destinatariBCC = value;
                }
            }
        }

        [XmlElement(ElementName = "Oggetto")]
        public string Oggetto
        {
            get => oggetto;
            set
            {
                if (oggetto != value)
                {
                    oggetto = value;
                }
            }
        }

        [XmlElement(ElementName = "Priorita")]
        public MailPriority Priorita
        {
            get => priorita;
            set
            {
                if (priorita != value)
                {
                    priorita = value;
                }
            }
        }

        [XmlElement(ElementName = "ReplyTo")]
        public EmailAddress ReplyTo
        {
            get => replyTo;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("ReplyTo");
                }
                if (replyTo != value)
                {
                    replyTo = value;
                }
            }
        }

        [XmlElement(ElementName = "OpzioniNotifica")]
        public DeliveryNotificationOptions OpzioniNotifica
        {
            get => opzioniNotifica;
            set
            {
                if (opzioniNotifica != value)
                {
                    opzioniNotifica = value;
                }
            }
        }

        [XmlArray(ElementName = "Allegati")]
        public List<EmailAttachment> Allegati
        {
            get => allegati;
            set
            {
                if (allegati != value)
                {
                    allegati = value;
                }
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.4927")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/", TypeName = "EmailAddress")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class EmailAddress
    {
        /// <remarks />
        private string indirizzo;

        /// <remarks />
        private string nome;

        public EmailAddress()
        {
        }

        public EmailAddress(string nome, string indirizzo)
        {
            this.nome = nome;
            this.indirizzo = indirizzo;
        }

        [XmlElement(ElementName = "Nome")]
        public string Nome
        {
            get => nome;
            set
            {
                if (nome != value)
                {
                    nome = value;
                }
            }
        }

        [XmlElement(ElementName = "Indirizzo")]
        public string Indirizzo
        {
            get => indirizzo;
            set
            {
                if (indirizzo != value)
                {
                    indirizzo = value;
                }
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.4927")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/", TypeName = "EmailMessageFilter")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class EmailMessageFilter
    {
        /// <remarks />
        private bool? daInviare;

        /// <remarks />
        private DateTime? giornoInvio;

        public EmailMessageFilter()
        {
        }

        public EmailMessageFilter(bool? daInviare, DateTime? giornoInvio)
        {
            this.daInviare = daInviare;
            this.giornoInvio = giornoInvio;
        }

        [XmlElement(IsNullable = true, ElementName = "DaInviare")]
        public bool? DaInviare
        {
            get => daInviare;
            set
            {
                if (daInviare != value)
                {
                    daInviare = value;
                }
            }
        }

        [XmlElement(IsNullable = true, ElementName = "GiornoInvio")]
        public DateTime? GiornoInvio
        {
            get => giornoInvio;
            set
            {
                if (giornoInvio != value)
                {
                    giornoInvio = value;
                }
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.4927")]
    [Serializable]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/", TypeName = "EmailAttachment")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class EmailAttachment
    {
        /// <remarks />
        private byte[] file;

        /// <remarks />
        private string nomeFile;

        public EmailAttachment()
        {
        }

        public EmailAttachment(string nomeFile, byte[] file)
        {
            this.nomeFile = nomeFile;
            this.file = file;
        }

        [XmlElement(ElementName = "NomeFile")]
        public string NomeFile
        {
            get => nomeFile;
            set
            {
                if (nomeFile != value)
                {
                    nomeFile = value;
                }
            }
        }

        [XmlElement(DataType = "base64Binary", ElementName = "File")]
        public byte[] File
        {
            get => file;
            set
            {
                if (file != value)
                {
                    file = value;
                }
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "2.0.50727.4927")]
    [Serializable]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/", TypeName = "MailPriority")]
    public enum MailPriority
    {
        /// <remarks />
        [XmlEnum(Name = "Normal")] Normal,

        /// <remarks />
        [XmlEnum(Name = "Low")] Low,

        /// <remarks />
        [XmlEnum(Name = "High")] High
    }

    /// <remarks />
    [Flags]
    [GeneratedCode("System.Xml", "2.0.50727.4927")]
    [Serializable]
    [XmlType(Namespace = "http://www.cassaedilemilano.it/EmailInfoService/", TypeName = "DeliveryNotificationOptions")]
    public enum DeliveryNotificationOptions
    {
        /// <remarks />
        [XmlEnum(Name = "None")] None = 1,

        /// <remarks />
        [XmlEnum(Name = "OnSuccess")] OnSuccess = 2,

        /// <remarks />
        [XmlEnum(Name = "OnFailure")] OnFailure = 4,

        /// <remarks />
        [XmlEnum(Name = "Delay")] Delay = 8,

        /// <remarks />
        [XmlEnum(Name = "Never")] Never = 16
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    public delegate void InviaEmailCompletedEventHandler(object sender, InviaEmailCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class InviaEmailCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal InviaEmailCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public bool Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (bool) results[0];
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    public delegate void InviaEmailCollectionCompletedEventHandler(
        object sender, InviaEmailCollectionCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class InviaEmailCollectionCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal InviaEmailCollectionCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public EmailMessageSerializzabile[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (EmailMessageSerializzabile[]) results[0];
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    public delegate void GetEMailDaInviareCompletedEventHandler(object sender, GetEMailDaInviareCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "2.0.50727.4927")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class GetEMailDaInviareCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal GetEMailDaInviareCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public EmailMessageSerializzabile[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (EmailMessageSerializzabile[]) results[0];
            }
        }
    }
}