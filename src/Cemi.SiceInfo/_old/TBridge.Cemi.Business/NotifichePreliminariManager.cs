﻿using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.NotifichePreliminari.Types.Enums;
using Cemi.NotifichePreliminari.Types.Filters;
using TBridge.Cemi.Data;

namespace TBridge.Cemi.Business
{
    public class NotifichePreliminariManager
    {
        private readonly NotifichePreliminariDataAccess _dataProvider = new NotifichePreliminariDataAccess();

        public TipoOperaNotificaCollection GetTipiOpera()
        {
            return _dataProvider.GetTipiOpera();
        }

        public NotificaPreliminareCollection RicercaNotificheByFilter(NotificaPreliminareFilter filter)
        {
            return _dataProvider.NotificheSelectByFilter(filter);
        }

        public NotificaPreliminare GetNotficaByNumero(string numeroNotifica)
        {
            NotificaPreliminare notifica = null;
            if (!string.IsNullOrEmpty(numeroNotifica))
            {
                notifica = _dataProvider.NotificaSelectByNumero(numeroNotifica);
                CaricaPersoneNotifica(notifica);
                CaricaImpreseNotifica(notifica);
                CaricaIndirizziCantieriNotifica(notifica);
            }
            return notifica;
        }

        public PersonaCollection GetPersoneByNumeroNotifica(string numeroNotifica)
        {
            return _dataProvider.PersoneSelectByNumeroNotificaRuolo(numeroNotifica, null);
        }

        public void CaricaPersoneNotifica(NotificaPreliminare notifica)
        {
            PersonaCollection persone = _dataProvider.PersoneSelectByNumeroNotificaRuolo(notifica.NumeroNotifica, null);

            foreach (Persona item in persone)
            {
                if (item.RuoloId == IdRuoliPersone.Committente)
                {
                    notifica.Committenti.Add(item);
                }
                else
                {
                    notifica.AltrePersone.Add(item);
                }
            }

            persone.Clear();
        }

        public ImpresaNotifichePreliminariCollection GetImpreseByNumeroNotifica(string numeroNotifica)
        {
            return _dataProvider.ImpreseSelectByNumeroNotificaIncarico(numeroNotifica, null);
        }

        public void CaricaImpreseNotifica(NotificaPreliminare notifica)
        {
            ImpresaNotifichePreliminariCollection imprese =
                _dataProvider.ImpreseSelectByNumeroNotificaIncarico(notifica.NumeroNotifica, null);
            notifica.Imprese = imprese;
        }

        public IndirizzoCollection GetIndirizziByNumeroNoifica(string numeroNotifica)
        {
            return _dataProvider.IndirizziSelectByNumeroNotifica(numeroNotifica);
        }

        public void CaricaIndirizziCantieriNotifica(NotificaPreliminare notifica)
        {
            notifica.Indirizzi = _dataProvider.IndirizziSelectByNumeroNotifica(notifica.NumeroNotifica);
        }

        public CantiereCollection RicercaCantieriPerCommittenteByFilter(NotificaPreliminareFilter filter)
        {
            return _dataProvider.CantieriPerCommittenteSelectByFilter(filter);
        }

        public CantiereCollection RicercaCantieriPerImpresaByFilter(NotificaPreliminareFilter filter)
        {
            return _dataProvider.CantieriPerImpresaSelectByFilter(filter);
        }

        public TBridge.Cemi.Type.Collections.Cantieri.CantiereCollection GetCantieriGenerati(string protocolloRegione)
        {
            return _dataProvider.GetCantieriGenerati(protocolloRegione);
        }
    }
}