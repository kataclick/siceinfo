﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Business
{
    #region Classe per non accettare di default tutti i certificati forniti dal server contattato

    public class AcceptAllCertificatesPolicy : ICertificatePolicy
    {
        #region ICertificatePolicy Members

        public bool CheckValidationResult(
            ServicePoint srvPoint,
            X509Certificate certificate,
            WebRequest request, int certificateProblem)
        {
            // TODO:  Add AcceptAllCertificatesPolicy.CheckValidationResult
            // implementation
            return true;
        }

        #endregion
    }

    #endregion

    public class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest w = base.GetWebRequest(uri);
            w.Timeout = 5 * 60 * 1000;
            return w;
        }
    }

    public class OreCNCEManager
    {
        public static List<OreMensiliCNCE> GetOreFromCNCE(
            CassaEdileCollection casseEdili,
            string loginCNCE,
            string passwordCNCE,
            string urlCNCE,
            int idLavoratore,
            string codiceFiscale,
            string cognome,
            string nome,
            DateTime dataRiferimento)
        {
            List<OreMensiliCNCE> ore = new List<OreMensiliCNCE>();

            NetworkCredential Cred = new NetworkCredential();
            Cred.UserName = loginCNCE;
            Cred.Password = passwordCNCE;

            // Add certificate support.
            //ServicePointManager.CertificatePolicy = new AcceptAllCertificatesPolicy();
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            string urlTemporanea = urlCNCE;

            foreach (CassaEdile cassaEdile in casseEdili)
            {
                if (cassaEdile.Cnce)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        // Da ripetere per tutte le casse edili selezionate e per tutti gli anni            
                        urlTemporanea = urlTemporanea.Replace("@codiceFiscale", codiceFiscale);
                        urlTemporanea = urlTemporanea.Replace("@nome", nome);
                        urlTemporanea = urlTemporanea.Replace("@cognome", cognome);
                        urlTemporanea = urlTemporanea.Replace("@cassaEdile", cassaEdile.IdCassaEdile);
                        urlTemporanea = urlTemporanea.Replace("@annoApe",
                            dataRiferimento.AddYears(-i).Year.ToString());

                        MyWebClient wc1 = new MyWebClient();
                        wc1.Credentials = Cred;
                        try
                        {
                            using (Stream s = wc1.OpenRead(urlTemporanea))
                            {
                                using (StreamReader sr = new StreamReader(s))
                                {
                                    // Tutto questo serve solo per scatenare l'eccezione,
                                    // perchè alla prima chiamata viene fatto dal sito un Redirect e generata l'eccezione
                                }
                            }
                        }
                        catch (WebException webExc)
                        {
                            MyWebClient wc2 = new MyWebClient();

                            wc2.Credentials = Cred;

                            using (Stream s2 = wc2.OpenRead(webExc.Response.ResponseUri))
                            {
                                using (StreamReader sr2 = new StreamReader(s2))
                                {
                                    sr2.ReadLine();
                                    string recordLetto;

                                    while ((recordLetto = sr2.ReadLine()) != null)
                                    {
                                        char[] separator = new char[1];
                                        separator[0] = ',';
                                        OreMensiliCNCE ora = new OreMensiliCNCE();
                                        ore.Add(ora);

                                        ora.IdLavoratore = idLavoratore;

                                        string[] valori = recordLetto.Split(separator, StringSplitOptions.None);
                                        if (!string.IsNullOrEmpty(valori[0]))
                                            ora.Anno = int.Parse(valori[0]);
                                        if (!string.IsNullOrEmpty(valori[1]))
                                            ora.Mese = int.Parse(valori[1]);
                                        if (!string.IsNullOrEmpty(valori[2]))
                                            ora.IdCassaEdile = valori[2];
                                        if (!string.IsNullOrEmpty(valori[4]))
                                            ora.OreLavorate = ParseOreCNCE(valori[4]);
                                        if (!string.IsNullOrEmpty(valori[5]))
                                            ora.OreFerie = ParseOreCNCE(valori[5]);
                                        if (!string.IsNullOrEmpty(valori[6]))
                                            ora.OreInfortunio = ParseOreCNCE(valori[6]);
                                        if (!string.IsNullOrEmpty(valori[7]))
                                            ora.OreMalattia = ParseOreCNCE(valori[7]);
                                        if (!string.IsNullOrEmpty(valori[8]))
                                            ora.OreCassaIntegrazione = ParseOreCNCE(valori[8]);
                                        if (!string.IsNullOrEmpty(valori[9]))
                                            ora.OrePermessoRetribuito = ParseOreCNCE(valori[9]);
                                        if (!string.IsNullOrEmpty(valori[10]))
                                            ora.OrePermessoNonRetribuito = ParseOreCNCE(valori[10]);
                                        if (!string.IsNullOrEmpty(valori[11]))
                                            ora.OreAltro = ParseOreCNCE(valori[11]);
                                    }
                                }
                            }
                        }

                        urlTemporanea = urlCNCE;
                    }
                }
            }

            // Fine

            return ore;
        }

        private static int ParseOreCNCE(string oreCNCE)
        {
            int ore = 0;

            //talvolta nel CNCE usano il punto come separatore e talvolta indicano .5 per dire 0.5. 
            //A noi interessa solo la parte intera, sicenew non è in grado di gestire i decimali per quei campi
            string oreIntere = oreCNCE.Split('.')[0];

            if (!string.IsNullOrEmpty(oreIntere))
            {
                if (!int.TryParse(oreIntere, out ore))
                    ore = 0;
            }
            else
                ore = 0;

            return ore;
        }

        public static void GestisciOreCNCE(CassaEdileCollection casseEdili, string loginCNCE, string passwordCNCE,
            string urlCNCE,
            int idLavoratore,
            string codiceFiscale,
            string cognome,
            string nome,
            DateTime dataRiferimento)
        {
            List<OreMensiliCNCE> ore = GetOreFromCNCE(casseEdili, loginCNCE, passwordCNCE, urlCNCE, idLavoratore,
                codiceFiscale, cognome, nome, dataRiferimento);

            foreach (OreMensiliCNCE ora in ore)
            {
                bool duplicate;

                InsertOreMensili(ora, out duplicate);
            }
        }

        public static bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        {
            Data.Common commonDA = new Data.Common();
            return commonDA.InsertOreMensili(ore, out oreDuplicate);
        }

        public static List<OreMensiliCNCE> GetOreMensiliCNCE(int idLavoratore)
        {
            Data.Common commonDA = new Data.Common();
            return commonDA.GetOreMensiliCNCE(idLavoratore);
        }

        public static OreMensiliCNCE GetOreMensiliCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        {
            Data.Common commonDA = new Data.Common();
            return commonDA.GetOreMensiliCNCE(idLavoratore, idCassaEdile, anno, mese);
        }

        public static bool DeleteOreCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        {
            Data.Common commonDA = new Data.Common();
            return commonDA.DeleteOreCNCE(idLavoratore, idCassaEdile, anno, mese);
        }
    }
}