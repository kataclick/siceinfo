using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Business.Cpt
{
    public class CptBusiness
    {
        private const string URLFILEEXCEL = "~/CeServizi/images/excel.gif";
        private const string URLFILEJPG = "~/CeServizi/images/jpg.gif";
        private const string URLFILEPDF = "~/CeServizi/images/pdf.gif";
        private const string URLFILESCONOSCIUTO = "~/CeServizi/images/sconosciuto.gif";
        private const string URLFILETXT = "~/CeServizi/images/txt.gif";
        private const string URLFILEWORD = "~/CeServizi/images/word.gif";

        private readonly CptDataAccess _dataAccess = new CptDataAccess();

        public decimal GetLimiteImporto()
        {
            try
            {
                return _dataAccess.GetLimiteImporto();
            }
            catch
            {
            }

            return 0;
        }

        public bool InserisciNotifica(Notifica notifica)
        {
            return _dataAccess.InserisciNotifica(notifica);
        }

        public bool InserisciNotifica(NotificaTelematica notifica)
        {
            return _dataAccess.InserisciNotifica(notifica);
        }

        public NotificaCollection RicercaNotifichePerAggiornamento(DateTime? dataParam, string committenteParam,
            string indirizzoParam, DateTime? dataInserimentoParam,
            string numeroAppalto,
            int? protocollo, short idArea,
            Guid? idUtenteTelematiche)
        {
            var notifiche = _dataAccess.RicercaNotifichePerAggiornamento(dataParam, committenteParam, indirizzoParam,
                dataInserimentoParam, numeroAppalto, protocollo, idArea,
                idUtenteTelematiche, null, null);

            return notifiche;
        }

        public NotificaCollection RicercaNotificheTelematichePerAggiornamento(
            NotificaTelematicaAggiornamentoFilter filtro)
        {
            return _dataAccess.RicercaNotifichePerAggiornamento(null,
                null,
                filtro.Indirizzo,
                filtro.DataInserimento,
                null,
                filtro.IdNotifica,
                1,
                filtro.IdUtenteTelematiche,
                filtro.NaturaOpera,
                filtro.Comune);
        }

        public Notifica GetNotifica(int idNotifica)
        {
            return _dataAccess.GetNotifica(idNotifica);
        }

        public NotificaTelematica GetNotificaTelematica(int idNotifica)
        {
            return _dataAccess.GetNotificaTelematica(idNotifica);
        }

        public Notifica GetNotificaUltimaVersione(int idNotificaPadre)
        {
            return _dataAccess.GetNotificaUltimaVersione(idNotificaPadre);
        }

        public NotificaTelematica GetNotificaTelematicaUltimaVersione(int idNotificaPadre)
        {
            return _dataAccess.GetNotificaTelematicaUltimaVersione(idNotificaPadre);
        }

        public NotificaCollection RicercaNotifiche(NotificaFilter filtro)
        {
            var notifiche = _dataAccess.RicercaNotifiche(filtro);

            return notifiche;
        }

        /// <summary>
        ///     Ricerca i cantieri legati ad una notifica CPT ordinando per committente
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public CantiereNotificaCollection RicercaCantieriPerCommittente(CantiereFilter filtro)
        {
            return _dataAccess.RicercaCantieriPerCommittente(filtro);
        }

        /// <summary>
        ///     Ricerca i cantieri legati a notifiche CPT ordinando per impresa
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public CantiereNotificaCollection RicercaCantieriPerImpresa(CantiereFilter filtro)
        {
            return _dataAccess.RicercaCantieriPerImpresa(filtro);
        }

        public bool AnnullaNotifica(int idNotifica, string utente)
        {
            bool res = false;

            try
            {
                res = _dataAccess.AnnullaNotifica(idNotifica, utente);
            }
            catch
            {
            }

            return res;
        }

        public List<int> GetIdNotificheCorrelate(int idNotifica)
        {
            List<int> notificheCorrelate = null;

            try
            {
                notificheCorrelate = _dataAccess.GetIdNotificheCorrelate(idNotifica);
            }
            catch
            {
            }

            return notificheCorrelate;
        }

        public List<string> GetTipologieAttivita()
        {
            List<string> tipologieAttivita = null;

            try
            {
                tipologieAttivita = _dataAccess.GetTipologieAttivita();
            }
            catch
            {
            }

            return tipologieAttivita;
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIVA, string codiceFiscale)
        {
            bool[] res = null;

            try
            {
                res = _dataAccess.EsisteIvaFiscImpresa(partitaIVA, codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

        public bool[] EsisteIvaFiscCommittente(string partitaIVA, string codiceFiscale)
        {
            bool[] res = null;

            try
            {
                res = _dataAccess.EsisteIvaFiscCommittente(partitaIVA, codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

        public IndirizzoCollection GetIndirizziNonGeocodificati()
        {
            return _dataAccess.GetIndirizziNonGeocodificati();
        }

        public bool UpdateIndirizzo(Indirizzo indirizzo, GeocodingResult geocodingResult)
        {
            return _dataAccess.UpdateIndirizzo(indirizzo, geocodingResult);
        }

        public TipologiaVisitaCollection GetTipologieVisita()
        {
            return _dataAccess.GetTipologieVisita();
        }

        public EsitoVisitaCollection GetEsitiVisita()
        {
            return _dataAccess.GetEsitiVisita();
        }

        public GradoIrregolaritaCollection GetGradiIrregolarita()
        {
            return _dataAccess.GetGradiIrregolarita();
        }

        public bool InsertVisita(Visita visita)
        {
            return _dataAccess.InsertVisita(visita);
        }

        public AllegatoCollection GetAllegati(int idVisita)
        {
            return _dataAccess.GetAllegati(idVisita);
        }

        public bool InsertAllegato(Allegato allegato)
        {
            return _dataAccess.InsertAllegato(allegato);
        }

        public string GetAllegatoImageUrl(string nomeFile)
        {
            string estensione = Path.GetExtension(nomeFile);
            estensione = estensione.ToUpper();

            switch (estensione)
            {
                case ".DOC":
                case ".DOCX":
                    return URLFILEWORD;
                case ".PDF":
                    return URLFILEPDF;
                case ".JPG":
                    return URLFILEJPG;
                case ".XLS":
                case ".XLSX":
                    return URLFILEEXCEL;
                case ".TXT":
                    return URLFILETXT;
                default:
                    return URLFILESCONOSCIUTO;
            }
        }

        public Allegato GetAllegato(int idAllegato)
        {
            return _dataAccess.GetAllegato(idAllegato);
        }

        public VisitaCollection GetVisite(int idNotificaRiferimento)
        {
            return _dataAccess.GetVisite(idNotificaRiferimento);
        }

        public VisitaCollection GetVisite(VisitaFilter filtro)
        {
            return _dataAccess.GetVisite(filtro);
        }

        public Visita GetVisita(int idVisita)
        {
            return _dataAccess.GetVisita(idVisita);
        }

        public bool UpdateVisita(Visita visita)
        {
            return _dataAccess.UpdateVisita(visita);
        }

        public bool DeleteVisita(int idVisita)
        {
            return _dataAccess.DeleteVisita(idVisita);
        }

        public bool DeleteVisitaAllegato(int idVisita, int idAllegato)
        {
            return _dataAccess.DeleteVisitaAllegato(idVisita, idAllegato);
        }

        public AreaCollection GetAree()
        {
            return _dataAccess.GetAree();
        }

        public bool InsertUtenteTelematiche(UtenteNotificheTelematiche utente)
        {
            return _dataAccess.InsertUtenteTelematiche(utente);
        }

        public UtenteNotificheTelematiche GetUtenteTelematiche(Guid userID)
        {
            return _dataAccess.GetUtenteTelematiche(userID);
        }

        public CommittenteNotificheTelematiche GetCommittentiTelematicheDaAnagrafica(string ivaFisc)
        {
            return _dataAccess.GetCommittentiTelematicheDaAnagrafica(ivaFisc);
        }

        public ImpresaNotificheTelematiche GetImpreseTelematicheDaSiceNewEAnagrafica(string ivaFisc)
        {
            return _dataAccess.GetImpreseTelematicheDaSiceNewEAnagrafica(ivaFisc);
        }

        public bool UpdateCommittenteTelematiche(CommittenteNotificheTelematiche committente)
        {
            return _dataAccess.UpdateCommittenteTelematiche(committente);
        }

        public bool InsertNotificaTemporanea(Guid userId, NotificaTelematica notifica)
        {
            return _dataAccess.InsertNotificaTemporanea(userId, notifica);
        }

        public bool UpdateNotificaTemporanea(NotificaTelematica notifica)
        {
            return _dataAccess.UpdateNotificaTemporanea(notifica);
        }

        public NotificaTelematicaCollection GetNotificheTemporanee(Guid userId)
        {
            return _dataAccess.GetNotificheTemporanee(userId);
        }

        public NotificaTelematica GetNotificaTemporanea(int idNotificaTemporanea)
        {
            return _dataAccess.GetNotificaTemporanea(idNotificaTemporanea);
        }

        public bool DeleteNotificaTemporanea(int idNotificaTemporanea)
        {
            return _dataAccess.DeleteNotificaTemporanea(idNotificaTemporanea, null);
        }

        public bool IsNotificaTelematica(int idNotifica)
        {
            return _dataAccess.IsNotificaTelematica(idNotifica);
        }

        public TipologiaCommittenteCollection GetTipologieCommittente()
        {
            return _dataAccess.GetTipologieCommittente();
        }

        public int? GetIdImpresaDaCodiceFiscale(string codiceFiscale)
        {
            return _dataAccess.GetIdImpresaDaCodiceFiscale(codiceFiscale);
        }

        public bool EsisteNotificaRegione(string protocolloRegione)
        {
            return _dataAccess.EsisteNotificaRegione(protocolloRegione);
        }

        public NotificaTelematica ConvertiNotificaDaWebService(underground notificaDaWebService)
        {
            NotificaTelematica notifica = null;

            if (notificaDaWebService != null && notificaDaWebService.NOTIFICA != null &&
                notificaDaWebService.NOTIFICA.Length == 1)
            {
                notifica = new NotificaTelematica();
                notifica.Utente = "WEBSERVICE";
                notifica.Area = new Area
                {
                    IdArea = 1
                };
                notifica.Guid = Guid.NewGuid();

                undergroundNOTIFICA cantiere = notificaDaWebService.NOTIFICA[0];

                // Protocollo regione
                if (string.IsNullOrEmpty(cantiere.NR_NOTIFICA))
                {
                    throw new Exception("Numero protocollo non presente");
                }
                notifica.ProtocolloRegione = cantiere.NR_NOTIFICA;

                char[] separatore1 = new char[1];
                separatore1[0] = ' ';
                char[] separatore2 = new char[1];
                separatore2[0] = '/';
                char[] separatore3 = new char[1];
                separatore3[0] = ':';

                // Data dd/MM/yyyy HH:mm:ss
                if (string.IsNullOrEmpty(cantiere.DT_MODIFICA))
                {
                    // Non � un 'aggiornamento
                    if (!string.IsNullOrEmpty(cantiere.DT_CREAZIONE))
                    {
                        // Non � un 'aggiornamento
                        // notifica.Data = DateTime.ParseExact(cantiere.DT_INSERIMENTO, "dd/MM/yyyy HH:mm:ss", null);

                        string[] primoSplit = cantiere.DT_CREAZIONE.Split(separatore1);
                        string[] splitData = primoSplit[0].Split(separatore2);
                        string[] splitOra = primoSplit[1].Split(separatore3);

                        notifica.Data = new DateTime(int.Parse(splitData[2]), int.Parse(splitData[1]),
                            int.Parse(splitData[0]),
                            int.Parse(splitOra[0]), int.Parse(splitOra[1]), int.Parse(splitOra[2]));
                    }
                }
                else
                {
                    // notifica.Data = DateTime.ParseExact(cantiere.DT_MODIFICA, "dd/MM/yyyy HH:mm:ss", null);

                    string[] primoSplit1 = cantiere.DT_MODIFICA.Split(separatore1);
                    string[] splitData1 = primoSplit1[0].Split(separatore2);
                    string[] splitOra1 = primoSplit1[1].Split(separatore3);

                    notifica.Data = new DateTime(int.Parse(splitData1[2]), int.Parse(splitData1[1]),
                        int.Parse(splitData1[0]),
                        int.Parse(splitOra1[0]), int.Parse(splitOra1[1]), int.Parse(splitOra1[2]));
                }

                // Data primo inserimento dd/MM/yyyy HH:mm:ss
                // notifica.DataPrimoInserimento = DateTime.ParseExact(cantiere.DT_INSERIMENTO, "dd/MM/yyyy HH:mm:ss", null);
                string[] primoSplit2 = cantiere.DT_CREAZIONE.Split(separatore1);
                string[] splitData2 = primoSplit2[0].Split(separatore2);
                string[] splitOra2 = primoSplit2[1].Split(separatore3);

                notifica.DataPrimoInserimento = new DateTime(int.Parse(splitData2[2]), int.Parse(splitData2[1]),
                    int.Parse(splitData2[0]),
                    int.Parse(splitOra2[0]), int.Parse(splitOra2[1]), int.Parse(splitOra2[2]));

                // Note
                notifica.Note = cantiere.DS_NOTE;

                // CIG, CUI
                if (!string.IsNullOrWhiteSpace(cantiere.CD_CIG))
                {
                    notifica.NumeroAppalto = cantiere.CD_CIG;
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(cantiere.CD_CUI))
                    {
                        notifica.NumeroAppalto = cantiere.CD_CUI;
                    }
                }

                // Tipo opera
                notifica.OperaPubblica = false;
                if (!string.IsNullOrWhiteSpace(cantiere.DS_TIPO_OPERA)
                    && cantiere.DS_TIPO_OPERA.ToUpper().Trim() == "PUBBLICA")
                {
                    notifica.OperaPubblica = true;
                }

                // Natura opera
                if (string.IsNullOrEmpty(cantiere.DS_CATEGORIA))
                {
                    throw new Exception("Natura opera non presente");
                }
                if (cantiere.DS_CATEGORIA == "ALTRO...")
                {
                    // Se la Categoria � Altro metto la descrizione presente in altra categoria
                    notifica.NaturaOpera = cantiere.DS_ALTRA_CATEGORIA;
                }
                else
                {
                    // Metto la categoria
                    notifica.NaturaOpera = cantiere.DS_CATEGORIA;
                }

                // Ammontare
                if (!string.IsNullOrEmpty(cantiere.NR_IMPORTO_TOTALE))
                {
                    notifica.AmmontareComplessivo = decimal.Parse(cantiere.NR_IMPORTO_TOTALE.Replace('.', ','));
                }

                // Indirizzi
                if (cantiere.LISTA_CANTIERE_DETTAGLIO != null && cantiere.LISTA_CANTIERE_DETTAGLIO.Length > 0)
                {
                    foreach (undergroundNOTIFICALISTA_CANTIERE_DETTAGLIOCANTIERE_DETTAGLIO indCant in cantiere
                        .LISTA_CANTIERE_DETTAGLIO)
                    {
                        Indirizzo indirizzo = new Indirizzo();
                        notifica.Indirizzi.Add(indirizzo);
                        indirizzo.Indirizzo1 = indCant.DS_INDIRIZZO;
                        indirizzo.Comune = indCant.DS_LOCALITA;
                        indirizzo.Provincia = indCant.DS_SIG_PROV;
                        indirizzo.Cap = indCant.CD_CAP;

                        //// Coordinate
                        //if (!String.IsNullOrEmpty(indCant.UTM_X) && indCant.UTM_X != "0.0" && !String.IsNullOrEmpty(indCant.UTM_Y) && indCant.UTM_Y != "0.0")
                        //{
                        //    indirizzo.Latitudine = Decimal.Parse(indCant.UTM_Y.Replace('.', ','));
                        //    indirizzo.Longitudine = Decimal.Parse(indCant.UTM_X.Replace('.', ','));
                        //}

                        // Data inizio lavori
                        if (!string.IsNullOrEmpty(indCant.DT_INIZIO_LAVORI))
                        {
                            DateTime dataInizioLavori =
                                DateTime.ParseExact(indCant.DT_INIZIO_LAVORI, "dd/MM/yyyy", null);

                            if (VerificaDataSmallDateTime(dataInizioLavori))
                            {
                                indirizzo.DataInizioLavori = dataInizioLavori;
                                if (cantiere.LISTA_CANTIERE_DETTAGLIO.Length == 1)
                                {
                                    notifica.DataInizioLavori = indirizzo.DataInizioLavori;
                                }
                            }
                        }

                        // Tipo Durata
                        if (!string.IsNullOrEmpty(indCant.DS_DURATA))
                        {
                            indirizzo.DescrizioneDurata = indCant.DS_DURATA;
                        }

                        // Numero Durata
                        if (!string.IsNullOrEmpty(indCant.NR_DURATA_LAVORI))
                        {
                            indirizzo.NumeroDurata = int.Parse(indCant.NR_DURATA_LAVORI);
                        }

                        // Numero lavoratori
                        if (!string.IsNullOrEmpty(indCant.NR_MAX_LAVORATORI))
                        {
                            indirizzo.NumeroMassimoLavoratori = int.Parse(indCant.NR_MAX_LAVORATORI);

                            if (cantiere.LISTA_CANTIERE_DETTAGLIO.Length == 1)
                            {
                                notifica.NumeroMassimoLavoratori = indirizzo.NumeroMassimoLavoratori;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Indirizzo non presente");
                }


                // Committente
                if (cantiere.LISTA_COMMITTENTE != null && cantiere.LISTA_COMMITTENTE.Length > 0)
                {
                    // Se sono presenti uno o pi� committenti prendiamo sempre il primo
                    PERSONA committente = cantiere.LISTA_COMMITTENTE[0];
                    notifica.Committente = new CommittenteNotificheTelematiche();
                    notifica.Committente.TipologiaCommittente = new TipologiaCommittente
                    {
                        IdTipologiaCommittente = 3
                    };

                    notifica.Committente.PersonaCognome = committente.DS_COGNOME;
                    notifica.Committente.PersonaNome = committente.DS_NOME;
                    notifica.Committente.PersonaCodiceFiscale = committente.CD_CODICE_FISCALE;
                    notifica.Committente.PersonaIndirizzo = committente.DS_P_INDIRIZZO;
                    notifica.Committente.PersonaComune = committente.DS_COMUNE;
                    notifica.Committente.PersonaProvincia = committente.CD_P_PROV;
                    notifica.Committente.PersonaEmail = committente.DS_EMAIL;

                    notifica.Committente.FonteNotifica = true;
                }
                else
                {
                    // Se non � presente il committente ne mettiamo uno fittizio
                    notifica.Committente = new CommittenteNotificheTelematiche();
                    notifica.Committente.TipologiaCommittente = new TipologiaCommittente
                    {
                        IdTipologiaCommittente = 3
                    };

                    notifica.Committente.PersonaCognome = "NON";
                    notifica.Committente.PersonaNome = "DEFINITO";

                    notifica.Committente.FonteNotifica = true;
                }

                // Coordinatore progettazione
                if (cantiere.LISTA_COORD_PROG != null && cantiere.LISTA_COORD_PROG.Length >= 1)
                {
                    PERSONA coordProg = cantiere.LISTA_COORD_PROG[0];
                    notifica.CoordinatoreSicurezzaProgettazione = new PersonaNotificheTelematiche();

                    notifica.CoordinatoreSicurezzaProgettazione.PersonaCognome = coordProg.DS_COGNOME;
                    notifica.CoordinatoreSicurezzaProgettazione.PersonaNome = coordProg.DS_NOME;
                    notifica.CoordinatoreSicurezzaProgettazione.PersonaCodiceFiscale = coordProg.CD_CODICE_FISCALE;
                    notifica.CoordinatoreSicurezzaProgettazione.Indirizzo = coordProg.DS_P_INDIRIZZO;
                    notifica.CoordinatoreSicurezzaProgettazione.PersonaComune = coordProg.DS_COMUNE;
                    notifica.CoordinatoreSicurezzaProgettazione.PersonaProvincia = coordProg.CD_P_PROV;
                    notifica.CoordinatoreSicurezzaProgettazione.PersonaEmail = coordProg.DS_EMAIL;
                }
                else
                {
                    notifica.CoordinatoreProgettazioneNonNominato = true;
                }

                // Coordinatore realizzazione
                if (cantiere.LISTA_COORD_REALI != null && cantiere.LISTA_COORD_REALI.Length >= 1)
                {
                    PERSONA coordReali = cantiere.LISTA_COORD_REALI[0];
                    notifica.CoordinatoreSicurezzaRealizzazione = new PersonaNotificheTelematiche();

                    notifica.CoordinatoreSicurezzaRealizzazione.PersonaCognome = coordReali.DS_COGNOME;
                    notifica.CoordinatoreSicurezzaRealizzazione.PersonaNome = coordReali.DS_NOME;
                    notifica.CoordinatoreSicurezzaRealizzazione.PersonaCodiceFiscale = coordReali.CD_CODICE_FISCALE;
                    notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo = coordReali.DS_P_INDIRIZZO;
                    notifica.CoordinatoreSicurezzaRealizzazione.PersonaComune = coordReali.DS_COMUNE;
                    notifica.CoordinatoreSicurezzaRealizzazione.PersonaProvincia = coordReali.CD_P_PROV;
                    notifica.CoordinatoreSicurezzaRealizzazione.PersonaEmail = coordReali.DS_EMAIL;
                }
                else
                {
                    notifica.CoordinatoreEsecuzioneNonNominato = true;
                }

                // Responsabile lavori
                if (cantiere.LISTA_RESP_LAVORI != null && cantiere.LISTA_RESP_LAVORI.Length >= 1)
                {
                    PERSONA respLavori = cantiere.LISTA_RESP_LAVORI[0];
                    notifica.DirettoreLavori = new PersonaNotificheTelematiche();

                    notifica.DirettoreLavori.PersonaCognome = respLavori.DS_COGNOME;
                    notifica.DirettoreLavori.PersonaNome = respLavori.DS_NOME;
                    notifica.DirettoreLavori.PersonaCodiceFiscale = respLavori.CD_CODICE_FISCALE;
                    notifica.DirettoreLavori.Indirizzo = respLavori.DS_P_INDIRIZZO;
                    notifica.DirettoreLavori.PersonaComune = respLavori.DS_COMUNE;
                    notifica.DirettoreLavori.PersonaProvincia = respLavori.CD_P_PROV;
                    notifica.DirettoreLavori.PersonaEmail = respLavori.DS_EMAIL;
                }
                else
                {
                    notifica.ResponsabileCommittente = true;
                }

                //// Data inizio lavori
                //if (!String.IsNullOrEmpty(cantiere.DT_INIZIO_LAVORI))
                //{
                //    notifica.DataInizioLavori = DateTime.ParseExact(cantiere.DT_INIZIO_LAVORI, "dd/MM/yyyy", null);
                //}

                //// Durata lavori
                //if (!String.IsNullOrEmpty(cantiere.NR_DURATA_LAVORI))
                //{
                //    notifica.Durata = Int32.Parse(cantiere.NR_DURATA_LAVORI) * 30;
                //}

                //// Numero lavoratori
                //if (!String.IsNullOrEmpty(cantiere.NR_MAX_LAV_PRESENTI))
                //{
                //    notifica.NumeroMassimoLavoratori = Int32.Parse(cantiere.NR_MAX_LAV_PRESENTI);
                //}

                // Numero imprese
                if (!string.IsNullOrEmpty(cantiere.NR_IMPRESE))
                {
                    notifica.NumeroImprese = int.Parse(cantiere.NR_IMPRESE);
                }

                // Numero lavoratori autonomi
                if (!string.IsNullOrEmpty(cantiere.NR_LAV_AUTONOMI))
                {
                    notifica.NumeroLavoratoriAutonomi = int.Parse(cantiere.NR_LAV_AUTONOMI);
                }

                // Imprese
                notifica.ImpreseAffidatarie = new SubappaltoNotificheTelematicheCollection();
                notifica.ImpreseEsecutrici = new SubappaltoNotificheTelematicheCollection();

                if (cantiere.LISTA_IMPRESA_GECA != null && cantiere.LISTA_IMPRESA_GECA.Length > 0)
                {
                    foreach (undergroundNOTIFICALISTA_IMPRESA_GECAIMPRESA_GECA impresaWS in cantiere.LISTA_IMPRESA_GECA)
                    {
                        SubappaltoNotificheTelematiche sub = new SubappaltoNotificheTelematiche();
                        ImpresaNotificheTelematiche impresa = new ImpresaNotificheTelematiche();
                        sub.ImpresaSelezionata = impresa;

                        if (impresaWS.DS_TIPO_INCARICO == "AFFIDATARIA")
                        {
                            sub.Affidatarie = true;
                            notifica.ImpreseAffidatarie.Add(sub);
                        }
                        else
                        {
                            notifica.ImpreseEsecutrici.Add(sub);
                        }

                        impresa.Provincia = impresaWS.CD_SIGPROV;
                        impresa.CodiceFiscale = impresaWS.DS_CODICE_FISCALE;
                        impresa.Indirizzo = impresaWS.DS_INDIRIZZO;
                        impresa.Comune = impresaWS.DS_LOCALITA;
                        impresa.Telefono = impresaWS.NR_TEL;
                        impresa.PartitaIva = impresaWS.PARTITA_IVA;
                        impresa.RagioneSociale = impresaWS.DS_DENOMINAZIONE;
                        impresa.MatricolaINAIL = impresaWS.NR_ISCRIZ_INAIL;
                        impresa.MatricolaINPS = impresaWS.NR_ISCRIZ_INPS;

                        if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                        {
                            impresa.IdImpresa = GetIdImpresaDaCodiceFiscale(impresa.CodiceFiscale);
                        }

                        //if (!String.IsNullOrEmpty(impresaWS.COD_FISC_AFFIDATARIA))
                        //{
                        //    ImpresaNotificheTelematiche impresaNelleSelezionate = notifica.ImpreseEsecutrici.RecuperaImpresaPerCodiceFiscale(impresaWS.COD_FISC_AFFIDATARIA);
                        //    if (impresaNelleSelezionate != null)
                        //    {
                        //        sub.AppaltataDa = impresaNelleSelezionate;
                        //    }
                        //}
                    }
                }

                // Cerco di ricostruire i rapporti tra le imprese
                if (cantiere.ALBERO_IMPRESE != null && cantiere.ALBERO_IMPRESE.Length > 0)
                {
                    foreach (undergroundNOTIFICAALBERO_IMPRESEIMPRESA nodo in cantiere.ALBERO_IMPRESE)
                    {
                        if (!string.IsNullOrWhiteSpace(nodo.PADRE) && !string.IsNullOrWhiteSpace(nodo.NODO))
                        {
                            string impresaCFFiglio = nodo.NODO;
                            string impresaCFPadre = nodo.PADRE;

                            SubappaltoNotificheTelematiche sub =
                                notifica.ImpreseEsecutrici.RecuperaSubappaltoPerCodiceFiscale(impresaCFFiglio);
                            if (sub != null && sub.ImpresaSelezionata != null)
                            {
                                ImpresaNotificheTelematiche impresaNelleAffidatarie =
                                    notifica.ImpreseAffidatarie.RecuperaImpresaPerCodiceFiscale(impresaCFPadre);
                                if (impresaNelleAffidatarie != null)
                                {
                                    sub.AppaltataDa = impresaNelleAffidatarie;
                                }
                                else
                                {
                                    ImpresaNotificheTelematiche impresaNelleEsecutrici =
                                        notifica.ImpreseEsecutrici.RecuperaImpresaPerCodiceFiscale(impresaCFPadre);
                                    if (impresaNelleEsecutrici != null)
                                    {
                                        sub.AppaltataDa = impresaNelleEsecutrici;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return notifica;
        }

        public bool VerificaDataSmallDateTime(DateTime data)
        {
            DateTime limiteInferiore = new DateTime(1900, 1, 1);
            DateTime limiteSuperiore = new DateTime(2079, 6, 6);

            if (limiteInferiore <= data && data <= limiteSuperiore)
            {
                return true;
            }

            return false;
        }

        public void CompletaNotificaTelematicaConRiferimenti(NotificaTelematica notifica)
        {
            _dataAccess.CompletaNotificaTelematicaConRiferimenti(notifica);
        }

        public bool UpdateNotifica(NotificaTelematica notificaPreliminare)
        {
            CompletaNotificaTelematicaConRiferimenti(notificaPreliminare);

            if (notificaPreliminare.IdNotifica.HasValue)
            {
                return _dataAccess.UpdateNotifica(notificaPreliminare);
            }
            throw new Exception("UpdateNotifica: notifica non trovata");
        }

        public bool NotificaRegioneDaAggiornare(string protocolloRegione, DateTime data)
        {
            return _dataAccess.NotificaRegioneDaAggiornare(protocolloRegione, data);
        }

        public List<string> GetProtocolliRegioneCaricati()
        {
            return _dataAccess.GetProtocolliRegioneCaricati();
        }

        public IndirizzoCollection GetCantieriGenerati(int idNotificaRiferimento)
        {
            return _dataAccess.GetCantieriGenerati(idNotificaRiferimento);
        }

        #region Log ricerche

        public string GetPropertiesDelFiltro(object filtro)
        {
            StringBuilder res = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(res);

            XmlSerializer x = new XmlSerializer(filtro.GetType());
            x.Serialize(xmlWriter, filtro);

            return res.ToString();
        }

        public void InsertLogRicerca(LogRicerca log)
        {
            _dataAccess.InsertLogRicerca(log);
        }

        #endregion

        #region ConvertiNotificaDaWebService Versione 1

        //public NotificaTelematica ConvertiNotificaDaWebService(underground notificaDaWebService)
        //{
        //    NotificaTelematica notifica = null;

        //    if (notificaDaWebService != null && notificaDaWebService.CANTIERE != null && notificaDaWebService.CANTIERE.Length == 1)
        //    {
        //        notifica = new NotificaTelematica();
        //        notifica.Utente = "WEBSERVICE";
        //        notifica.Area = new Area {IdArea = 1};
        //        notifica.Guid = Guid.NewGuid();

        //        undergroundCANTIERE cantiere = notificaDaWebService.CANTIERE[0];

        //        // Protocollo regione
        //        if (String.IsNullOrEmpty(cantiere.NR_CANTIERE))
        //        {
        //            throw new Exception("Numero protocollo non presente");
        //        }
        //        else
        //        {
        //            notifica.ProtocolloRegione = cantiere.NR_CANTIERE;
        //        }

        //        Char[] separatore1 = new char[1];
        //        separatore1[0] = ' ';
        //        Char[] separatore2 = new char[1];
        //        separatore2[0] = '/';
        //        Char[] separatore3 = new char[1];
        //        separatore3[0] = ':';

        //        // Data dd/MM/yyyy HH:mm:ss
        //        if (String.IsNullOrEmpty(cantiere.DT_MODIFICA))
        //        {
        //            // Non � un 'aggiornamento
        //            if (!String.IsNullOrEmpty(cantiere.DT_INSERIMENTO))
        //            {
        //                // Non � un 'aggiornamento
        //                // notifica.Data = DateTime.ParseExact(cantiere.DT_INSERIMENTO, "dd/MM/yyyy HH:mm:ss", null);

        //                String[] primoSplit = cantiere.DT_INSERIMENTO.Split(separatore1);
        //                String[] splitData = primoSplit[0].Split(separatore2);
        //                String[] splitOra = primoSplit[1].Split(separatore3);

        //                notifica.Data = new DateTime(Int32.Parse(splitData[2]), Int32.Parse(splitData[1]), Int32.Parse(splitData[0]),
        //                    Int32.Parse(splitOra[0]), Int32.Parse(splitOra[1]), Int32.Parse(splitOra[2]));
        //            }
        //        }
        //        else
        //        {
        //            // notifica.Data = DateTime.ParseExact(cantiere.DT_MODIFICA, "dd/MM/yyyy HH:mm:ss", null);

        //            String[] primoSplit1 = cantiere.DT_MODIFICA.Split(separatore1);
        //            String[] splitData1 = primoSplit1[0].Split(separatore2);
        //            String[] splitOra1 = primoSplit1[1].Split(separatore3);

        //            notifica.Data = new DateTime(Int32.Parse(splitData1[2]), Int32.Parse(splitData1[1]), Int32.Parse(splitData1[0]),
        //                Int32.Parse(splitOra1[0]), Int32.Parse(splitOra1[1]), Int32.Parse(splitOra1[2]));
        //        }

        //        // Data primo inserimento dd/MM/yyyy HH:mm:ss
        //        // notifica.DataPrimoInserimento = DateTime.ParseExact(cantiere.DT_INSERIMENTO, "dd/MM/yyyy HH:mm:ss", null);
        //        String[] primoSplit2 = cantiere.DT_INSERIMENTO.Split(separatore1);
        //        String[] splitData2 = primoSplit2[0].Split(separatore2);
        //        String[] splitOra2 = primoSplit2[1].Split(separatore3);

        //        notifica.DataPrimoInserimento = new DateTime(Int32.Parse(splitData2[2]), Int32.Parse(splitData2[1]), Int32.Parse(splitData2[0]),
        //            Int32.Parse(splitOra2[0]), Int32.Parse(splitOra2[1]), Int32.Parse(splitOra2[2]));

        //        // Note
        //        notifica.Note = cantiere.NOTE;

        //        // Natura opera
        //        if (String.IsNullOrEmpty(cantiere.DS_NATURA_OPERA))
        //        {
        //            throw new Exception("Natura opera non presente");
        //        }
        //        else
        //        {
        //            notifica.NaturaOpera = cantiere.DS_NATURA_OPERA;
        //        }

        //        // Ammontare
        //        if (!String.IsNullOrEmpty(cantiere.NR_IMPORTO_TOTALE))
        //        {
        //            notifica.AmmontareComplessivo = Decimal.Parse(cantiere.NR_IMPORTO_TOTALE.Replace('.', ','));
        //        }

        //        // Indirizzi (per ora singolo)
        //        if (String.IsNullOrEmpty(cantiere.DS_INDIRIZZO))
        //        {
        //            throw new Exception("Indirizzo non presente");
        //        }
        //        else
        //        {
        //            Indirizzo indirizzo = new Indirizzo();
        //            notifica.Indirizzi.Add(indirizzo);
        //            indirizzo.Indirizzo1 = cantiere.DS_INDIRIZZO;
        //            indirizzo.Comune = cantiere.DS_LOCALITA;
        //            indirizzo.Provincia = cantiere.CD_SIGLA_PROV;
        //            indirizzo.Cap = cantiere.CD_CAP;
        //        }

        //        // Committente
        //        if (cantiere.LISTA_COMMITTENTE != null && cantiere.LISTA_COMMITTENTE.Length > 0)
        //        {
        //            // Se sono presenti uno o pi� committenti prendiamo sempre il primo
        //            PERSONA committente = cantiere.LISTA_COMMITTENTE[0];
        //            notifica.Committente = new CommittenteNotificheTelematiche();
        //            notifica.Committente.TipologiaCommittente = new TipologiaCommittente {IdTipologiaCommittente = 3};

        //            notifica.Committente.PersonaCognome = committente.DS_COGNOME;
        //            notifica.Committente.PersonaNome = committente.DS_NOME;
        //            notifica.Committente.PersonaCodiceFiscale = committente.CD_CODICE_FISCALE;
        //            notifica.Committente.PersonaIndirizzo = committente.DS_P_INDIRIZZO;
        //            notifica.Committente.PersonaComune = committente.DS_COMUNE;
        //            notifica.Committente.PersonaProvincia = committente.CD_P_PROV;
        //            notifica.Committente.PersonaEmail = committente.DS_EMAIL;

        //            notifica.Committente.FonteNotifica = true;
        //        }
        //        else
        //        {
        //            // Se non � presente il committente ne mettiamo uno fittizio
        //            notifica.Committente = new CommittenteNotificheTelematiche();
        //            notifica.Committente.TipologiaCommittente = new TipologiaCommittente { IdTipologiaCommittente = 3 };

        //            notifica.Committente.PersonaCognome = "NON";
        //            notifica.Committente.PersonaNome = "DEFINITO";

        //            notifica.Committente.FonteNotifica = true;
        //        }

        //        // Coordinatore progettazione
        //        if (cantiere.LISTA_COORD_PROG != null && cantiere.LISTA_COORD_PROG.Length >= 1)
        //        {
        //            PERSONA coordProg = cantiere.LISTA_COORD_PROG[0];
        //            notifica.CoordinatoreSicurezzaProgettazione = new PersonaNotificheTelematiche();

        //            notifica.CoordinatoreSicurezzaProgettazione.PersonaCognome = coordProg.DS_COGNOME;
        //            notifica.CoordinatoreSicurezzaProgettazione.PersonaNome = coordProg.DS_NOME;
        //            notifica.CoordinatoreSicurezzaProgettazione.PersonaCodiceFiscale = coordProg.CD_CODICE_FISCALE;
        //            notifica.CoordinatoreSicurezzaProgettazione.Indirizzo = coordProg.DS_P_INDIRIZZO;
        //            notifica.CoordinatoreSicurezzaProgettazione.PersonaComune = coordProg.DS_COMUNE;
        //            notifica.CoordinatoreSicurezzaProgettazione.PersonaProvincia = coordProg.CD_P_PROV;
        //            notifica.CoordinatoreSicurezzaProgettazione.PersonaEmail = coordProg.DS_EMAIL;
        //        }
        //        else
        //        {
        //            notifica.CoordinatoreProgettazioneNonNominato = true;
        //        }

        //        // Coordinatore realizzazione
        //        if (cantiere.LISTA_COORD_REALI != null && cantiere.LISTA_COORD_REALI.Length >= 1)
        //        {
        //            PERSONA coordReali = cantiere.LISTA_COORD_REALI[0];
        //            notifica.CoordinatoreSicurezzaRealizzazione = new PersonaNotificheTelematiche();

        //            notifica.CoordinatoreSicurezzaRealizzazione.PersonaCognome = coordReali.DS_COGNOME;
        //            notifica.CoordinatoreSicurezzaRealizzazione.PersonaNome = coordReali.DS_NOME;
        //            notifica.CoordinatoreSicurezzaRealizzazione.PersonaCodiceFiscale = coordReali.CD_CODICE_FISCALE;
        //            notifica.CoordinatoreSicurezzaRealizzazione.Indirizzo = coordReali.DS_P_INDIRIZZO;
        //            notifica.CoordinatoreSicurezzaRealizzazione.PersonaComune = coordReali.DS_COMUNE;
        //            notifica.CoordinatoreSicurezzaRealizzazione.PersonaProvincia = coordReali.CD_P_PROV;
        //            notifica.CoordinatoreSicurezzaRealizzazione.PersonaEmail = coordReali.DS_EMAIL;
        //        }
        //        else
        //        {
        //            notifica.CoordinatoreEsecuzioneNonNominato = true;
        //        }

        //        // Responsabile lavori
        //        if (cantiere.LISTA_RESP_LAVORI != null && cantiere.LISTA_RESP_LAVORI.Length >= 1)
        //        {
        //            PERSONA respLavori = cantiere.LISTA_RESP_LAVORI[0];
        //            notifica.DirettoreLavori = new PersonaNotificheTelematiche();

        //            notifica.DirettoreLavori.PersonaCognome = respLavori.DS_COGNOME;
        //            notifica.DirettoreLavori.PersonaNome = respLavori.DS_NOME;
        //            notifica.DirettoreLavori.PersonaCodiceFiscale = respLavori.CD_CODICE_FISCALE;
        //            notifica.DirettoreLavori.Indirizzo = respLavori.DS_P_INDIRIZZO;
        //            notifica.DirettoreLavori.PersonaComune = respLavori.DS_COMUNE;
        //            notifica.DirettoreLavori.PersonaProvincia = respLavori.CD_P_PROV;
        //            notifica.DirettoreLavori.PersonaEmail = respLavori.DS_EMAIL;
        //        }
        //        else
        //        {
        //            notifica.ResponsabileCommittente = true;
        //        }

        //        // Data inizio lavori
        //        if (!String.IsNullOrEmpty(cantiere.DT_INIZIO_LAVORI))
        //        {
        //            notifica.DataInizioLavori = DateTime.ParseExact(cantiere.DT_INIZIO_LAVORI, "dd/MM/yyyy", null);
        //        }

        //        // Durata lavori
        //        if (!String.IsNullOrEmpty(cantiere.NR_DURATA_LAVORI))
        //        {
        //            notifica.Durata = Int32.Parse(cantiere.NR_DURATA_LAVORI) * 30;
        //        }

        //        // Numero lavoratori
        //        if (!String.IsNullOrEmpty(cantiere.NR_MAX_LAV_PRESENTI))
        //        {
        //            notifica.NumeroMassimoLavoratori = Int32.Parse(cantiere.NR_MAX_LAV_PRESENTI);
        //        }

        //        // Numero imprese
        //        if (!String.IsNullOrEmpty(cantiere.NR_IMP_LAV_AUTONOMI))
        //        {
        //            notifica.NumeroImprese = Int32.Parse(cantiere.NR_IMP_LAV_AUTONOMI);
        //        }

        //        // Numero lavoratori autonomi
        //        if (!String.IsNullOrEmpty(cantiere.NR_AUTONOMI))
        //        {
        //            notifica.NumeroLavoratoriAutonomi = Int32.Parse(cantiere.NR_AUTONOMI);
        //        }

        //        // Imprese
        //        notifica.ImpreseAffidatarie = new SubappaltoNotificheTelematicheCollection();
        //        notifica.ImpreseEsecutrici = new SubappaltoNotificheTelematicheCollection();

        //        if (cantiere.LISTA_IMPRESA != null && cantiere.LISTA_IMPRESA.Length > 0)
        //        {
        //            foreach (undergroundCANTIERELISTA_IMPRESAIMPRESA impresaWS in cantiere.LISTA_IMPRESA)
        //            {
        //                SubappaltoNotificheTelematiche sub = new SubappaltoNotificheTelematiche();
        //                ImpresaNotificheTelematiche impresa = new ImpresaNotificheTelematiche();
        //                sub.ImpresaSelezionata = impresa;
        //                notifica.ImpreseEsecutrici.Add(sub);

        //                impresa.Provincia = impresaWS.CD_SIGLA_PROV_SEDE;
        //                impresa.CodiceFiscale = impresaWS.DS_CODICE_FISCALE;
        //                impresa.Indirizzo = impresaWS.DS_INDIRIZZO;
        //                impresa.Comune = impresaWS.DS_LOCALITA;
        //                impresa.Telefono = impresaWS.NR_TEL;
        //                impresa.PartitaIva = impresaWS.PARTITA_IVA;
        //                impresa.RagioneSociale = impresaWS.RAG_SOC;

        //                if (!String.IsNullOrEmpty(impresa.CodiceFiscale))
        //                {
        //                    impresa.IdImpresa = GetIdImpresaDaCodiceFiscale(impresa.CodiceFiscale);
        //                }
        //            }
        //        }
        //    }

        //    return notifica;
        //}

        #endregion
    }
}