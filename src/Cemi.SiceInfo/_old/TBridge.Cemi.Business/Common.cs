using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;

namespace TBridge.Cemi.Business
{
    public class Common
    {
        private readonly Data.Common _commonData = new Data.Common();
        private readonly DateTime LIMITEINFERIORESMALLDATETIME = new DateTime(1900, 1, 1);
        private readonly DateTime LIMITESUPERIORESMALLDATETIME = new DateTime(2079, 6, 1);

        public string GetDataPremioFedelta()
        {
            return _commonData.GetDataPremioFedelta();
        }

        public string GetDataImpreseRegolari()
        {
            return _commonData.GetDataImpreseRegolari();
        }

        public int GetMesiDelegheScadute()
        {
            return _commonData.GetMesiDelegheScadute();
        }

        public ListDictionary GetNatureGiuridiche()
        {
            return _commonData.GetNatureGiuridiche();
        }

        public ListDictionary GetAttivitaIstat()
        {
            return _commonData.GetAttivitaIstat();
        }

        public ListDictionary GetTipiImpresa()
        {
            return _commonData.GetTipiImpresa();
        }

        /// <summary>
        ///     Ritorna i tipi invio denuncia
        /// </summary>
        /// <returns>(ListDictionary) tipi invio denuncia</returns>
        public ListDictionary GetTipiInvioDenuncia()
        {
            return _commonData.GetTipiInvioDenuncia();
        }

        public StringCollection GetProvinceSiceNew()
        {
            return _commonData.GetProvinceSiceNew();
        }

        public List<ComuneSiceNew> GetComuniSiceNew(string provincia)
        {
            return _commonData.GetComuniSiceNew(provincia);
        }

        public ComuneSiceNew GetComuneSiceNew(string idComune)
        {
            return _commonData.GetComuneSiceNew(idComune);
        }

        public ListDictionary GetNazioni()
        {
            return _commonData.GetNazioni();
        }

        public ListDictionary GetPreIndirizzi()
        {
            return _commonData.GetPreIndirizzi();
        }

        public ListDictionary GetAssociazioniImprenditoriali()
        {
            return _commonData.GetAssociazioniImprenditoriali();
        }

        public List<FrazioneSiceNew> GetFrazioniSiceNew(string codiceCatastale)
        {
            return _commonData.GetFrazioniSiceNew(codiceCatastale);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<ComprensorioSindacale> GetComprensori(bool selezionabili)
        {
            return _commonData.GetComprensori(selezionabili);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public List<Sindacato> GetSindacati()
        {
            return _commonData.GetSindacati();
        }

        public bool IsGrandeCitta(string codiceCatastale)
        {
            bool res = false;

            switch (codiceCatastale)
            {
                // Bari
                case "A662":
                    res = true;
                    break;
                // Bergamo
                case "A794":
                    res = true;
                    break;
                // Bologna
                case "A944":
                    res = true;
                    break;
                // Brescia
                case "B157":
                    res = true;
                    break;
                // Cagliari
                case "B354":
                    res = true;
                    break;
                // Catania
                case "C351":
                    res = true;
                    break;
                // Firenze
                case "D612":
                    res = true;
                    break;
                // Genova
                case "D969":
                    res = true;
                    break;
                // La Spezia
                case "E463":
                    res = true;
                    break;
                // Livorno
                case "E625":
                    res = true;
                    break;
                // Messina
                case "F158":
                    res = true;
                    break;
                // Milano
                case "F205":
                    res = true;
                    break;
                // Napoli
                case "F839":
                    res = true;
                    break;
                // Padova
                case "G224":
                    res = true;
                    break;
                // Palermo
                case "G273":
                    res = true;
                    break;
                // Perugia
                case "G478":
                    res = true;
                    break;
                // Pescara
                case "G482":
                    res = true;
                    break;
                // Pisa
                case "G702":
                    res = true;
                    break;
                // Reggio di Calabria
                case "H224":
                    res = true;
                    break;
                // Roma
                case "H501":
                    res = true;
                    break;
                // Salerno
                case "H703":
                    res = true;
                    break;
                // Torino
                case "L219":
                    res = true;
                    break;
                // Trieste
                case "L424":
                    res = true;
                    break;
                // Venezia
                case "L736":
                    res = true;
                    break;
                // Verbania
                case "L746":
                    res = true;
                    break;
                // Verona
                case "L781":
                    res = true;
                    break;
            }

            return res;
        }

        public CassaEdileCollection GetCasseEdili()
        {
            return _commonData.GetCasseEdili(true);
        }

        public CassaEdileCollection GetCasseEdiliSenzaMilano()
        {
            return _commonData.GetCasseEdili(false);
        }

        public List<StatoCivile> GetStatiCivili()
        {
            return _commonData.GetStatiCivili();
        }

        public DataTable GetProvince()
        {
            return _commonData.GetProvince();
        }

        public DataTable GetComuniDellaProvincia(int provincia)
        {
            return _commonData.GetComuniByProvincia(provincia);
        }

        public DataTable GetCAPDelComune(long idComune)
        {
            return _commonData.GetCapByComune(idComune);
        }

        public List<Impresa> ImpreseConsulente(int idConsulente)
        {
            return _commonData.GetImpreseConsulente(idConsulente);
        }

        public void ConsulenteSelezionaImpresa(int idConsulente, int? idImpresa)
        {
            _commonData.ConsulenteSelezionaImpresa(idConsulente, idImpresa);
        }

        public void ConsulenteImpresaSelezionata(int idConsulente, out int idImpresa, out string ragioneSociale)
        {
            _commonData.ConsulenteImpresaSelezionata(idConsulente, out idImpresa, out ragioneSociale);
        }

        public void ConsulenteImpresaSelezionata(int idConsulente, out int idImpresa, out string ragioneSociale,
            out string codiceFiscale)
        {
            _commonData.ConsulenteImpresaSelezionata(idConsulente, out idImpresa, out ragioneSociale,
                out codiceFiscale);
        }

        public List<TipoContratto> GetTipiContratto()
        {
            return _commonData.GetTipiContratto();
        }

        public List<TipoQualifica> GetTipiQualifica()
        {
            return _commonData.GetTipiQualifica();
        }

        public List<TipoQualifica> GetTipiQualifica(string tipoCategoria)
        {
            return _commonData.GetTipiQualifica(tipoCategoria);
        }

        public List<TipoMansione> GetTipiMansione()
        {
            return _commonData.GetTipiMansione();
        }

        public TipoMansione GetTipoMansione(string codiceQualificaSiceNew)
        {
            return _commonData.GetTipoMansione(codiceQualificaSiceNew);
        }

        public List<TipoCategoria> GetTipiCategoria()
        {
            return _commonData.GetTipiCategoria();
        }

        public List<TipoCategoria> GetTipiCategoria(string tipoContratto)
        {
            return _commonData.GetTipiCategoria(tipoContratto);
        }

        public List<TipoFineRapporto> GetTipiFineRapporto()
        {
            return _commonData.GetTipiFineRapporto();
        }

        public List<TipoInizioRapporto> GetTipiInizioRapporto()
        {
            return _commonData.GetTipiInizioRapporto();
        }

        public List<TipoCartaPrepagata> GetTipiCartaPrepagata()
        {
            return _commonData.GetTipiCartaPrepagata();
        }

        public List<TipoLingua> GetLingue()
        {
            return _commonData.GetLingue();
        }

        // SOSTITUITA DA GetBollettiniStatistichePagati
        public BollettiniFrecciaStatistichePagati GetBollettiniFrecciaStatistichePagati()
        {
            return _commonData.GetBollettiniFrecciaStatistichePagati();
        }

        public Impresa GetImpresa(string codiceFiscale)
        {
            return _commonData.GetImpresa(codiceFiscale);
        }

        public List<BollettinoFrecciaStampabile> GetBollettiniFrecciaStampabili(
            BollettinoFrecciaStampabileFilter filtro)
        {
            return _commonData.GetBollettiniFrecciaStampabili(filtro);
        }

        public List<Festivita> GetFestivita(int anno)
        {
            return _commonData.GetFestivita(anno);
        }

        public List<int> GetAnniTrattamentoCIGO()
        {
            return _commonData.GetAnniTrattamentoCIGO();
        }

        public bool IsImpresaIrregolareBNI(int idImpresa)
        {
            return _commonData.IsImpresaIrregolareBNI(idImpresa);
        }

        public int GetIdUtenteByIdImpresa(int idImpresa)
        {
            int idUtente = -1;

            using (SICEEntities context = new SICEEntities())
            {
                var impresa = (from imp in context.Imprese
                    where imp.Id == idImpresa
                    select imp).FirstOrDefault();

                if (impresa != null)
                {
                    idUtente = impresa.IdUtente.Value;
                }
            }

            return idUtente;
        }

        public int GetIdUtenteByIdConsulente(int idConsulente)
        {
            int idUtente = -1;

            using (SICEEntities context = new SICEEntities())
            {
                var consulente = (from con in context.Consulenti
                    where con.Id == idConsulente
                    select con).FirstOrDefault();

                if (consulente != null)
                {
                    idUtente = consulente.IdUtente.Value;
                }
            }

            return idUtente;
        }

        #region Bollettino (Freccia o Mav)

        // SOSTITUITA DA GetDatiBollettiniFrecciaMav
        public List<BollettinoFreccia> GetDatiBollettini(int idImpresa)
        {
            return _commonData.GetDatiBollettiniFreccia(idImpresa);
        }

        // SOSTITUITA DA GetBollettiniStampati
        public List<BollettinoFrecciaStampato> GetBollettiniFrecciaStampati(BollettinoFrecciaStampatoFilter filtro)
        {
            return _commonData.GetBollettiniFrecciaStampati(filtro);
        }

        // SOSTITUITA DA GetUtentiBollettiniStampati
        public void GetUtentiBollettiniFrecciaStampati(out int numeroImprese, out int numeroConsulenti)
        {
            _commonData.GetUtentiBollettiniFrecciaStampati(out numeroImprese, out numeroConsulenti);
        }

        public int GetBollettiniFrecciaStampabili(DateTime? dataInizio, DateTime? dataFine)
        {
            return _commonData.GetBollettiniFrecciaStampabili(dataInizio, dataFine);
        }

        // SOSTITUITA DA RegistraRichiestaBollettino
        public bool RegistraRichiesta(BollettinoFreccia bollettino, int idUtente)
        {
            return _commonData.RegistraRichiesta(bollettino, idUtente);
        }

        #region Nuovi - Getstione MAV

        public List<Bollettino> GetDatiBollettiniFrecciaMav(int idImpresa)
        {
            return _commonData.GetDatiBollettini(idImpresa);
        }

        public List<Bollettino> GetDatiDenunce(int idImpresa)
        {
            return _commonData.GetDatiDenunce(idImpresa);
        }

        public Bollettino GetDettaglioBollettino(int idImpresa, int anno, int mese, int sequenza)
        {
            return _commonData.GetDettaglioBollettino(idImpresa, anno, mese, sequenza);
        }

        public bool RegistraRichiestaBollettino(Bollettino bollettino, int idUtente)
        {
            return _commonData.RegistraRichiestaBollettino(bollettino, idUtente);
        }

        public bool RegistraRichiestaBollettinoDenuncia(Bollettino bollettino, int idUtente)
        {
            return _commonData.RegistraRichiestaBollettinoDenuncia(bollettino, idUtente);
        }

        public List<BollettinoStampato> GetBollettiniStampati(BollettinoStampatoFilter filtro)
        {
            return _commonData.GetBollettiniStampati(filtro);
        }

        public void GetUtentiBollettiniStampati(out int numeroImprese, out int numeroConsulenti)
        {
            _commonData.GetUtentiBollettiniStampati(out numeroImprese, out numeroConsulenti);
        }

        public BollettiniFrecciaStatistichePagati GetBollettiniStatistichePagati()
        {
            return _commonData.GetBollettiniStatistichePagati();
        }

        public List<TipoCanalePagamentoBolletino> GetTipiCanalePagamentoBollettino(bool conElementoVuoto)
        {
            return _commonData.GetTipiCanalePagamentoBollettino(conElementoVuoto);
        }

        #endregion

        #endregion

        #region Controlli per le date

        public bool IsDataSmallDateTime(DateTime data)
        {
            bool res = LIMITEINFERIORESMALLDATETIME <= data && data < LIMITESUPERIORESMALLDATETIME;

            return res;
        }

        public bool IsCampoDataSmallDateTime(string textData)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(textData))
            {
                DateTime dtParsato;

                if (DateTime.TryParse(textData, out dtParsato))
                {
                    if (IsDataSmallDateTime(dtParsato))
                        res = true;
                }
            }
            else
                res = true;

            return res;
        }

        public bool IsCampoDataSmallDateTimeMinoreDiOggi(string textData)
        {
            bool res = false;

            if (!string.IsNullOrEmpty(textData))
            {
                DateTime dtParsato;

                if (DateTime.TryParse(textData, out dtParsato))
                {
                    if (IsDataSmallDateTime(dtParsato) && dtParsato < DateTime.Now)
                        res = true;
                }
            }
            else
                res = true;

            return res;
        }

        #endregion
    }
}