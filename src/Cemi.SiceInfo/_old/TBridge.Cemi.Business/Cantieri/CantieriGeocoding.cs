using System;
using System.Collections.Generic;
using TBridge.Cemi.Business.Geocode;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Business.Cantieri
{
    [Obsolete("Utilizzare TBridge.Cemi.Business.Geocode")]
    public class CantieriGeocoding
    {
        public static IndirizzoCollection GeoCodeGoogleMultiplo(string address)
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            List<Indirizzo> indirizziGeocodificati = GeocodeProvider.Geocode(address);

            if (indirizziGeocodificati != null)
            {
                foreach (Indirizzo indirizzo in indirizziGeocodificati)
                {
                    indirizzi.Add(new Cemi.Cpt.Type.Entities.Indirizzo(indirizzo.Via, indirizzo.Civico,
                        indirizzo.Comune,
                        indirizzo.Provincia, indirizzo.Cap,
                        indirizzo.Latitudine, indirizzo.Longitudine));
                }
            }

            return indirizzi;
        }
    }
}