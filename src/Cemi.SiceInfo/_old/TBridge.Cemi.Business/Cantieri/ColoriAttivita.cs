﻿using System.Drawing;

namespace TBridge.Cemi.Business.Cantieri
{
    public static class ColoriAttivita
    {
        public const int SFONDOISPEZIONER = 255;
        public const int SFONDOISPEZIONEG = 200;
        public const int SFONDOISPEZIONEB = 170;

        public const int SFONDOISPEZIONECONSIDERATAR = 255;
        public const int SFONDOISPEZIONECONSIDERATAG = 240;
        public const int SFONDOISPEZIONECONSIDERATAB = 220;

        public const int SFONDOISPEZIONEFANTASMAR = 250;
        public const int SFONDOISPEZIONEFANTASMAG = 250;
        public const int SFONDOISPEZIONEFANTASMAB = 250;

        public const int SFONDOAPPUNTAMENTOR = 183;
        public const int SFONDOAPPUNTAMENTOG = 255;
        public const int SFONDOAPPUNTAMENTOB = 195;

        public const int SFONDOBACKOFFICER = 251;
        public const int SFONDOBACKOFFICEG = 255;
        public const int SFONDOBACKOFFICEB = 186;

        public const int SFONDOVARIER = 209;
        public const int SFONDOVARIEG = 204;
        public const int SFONDOVARIEB = 255;

        public static Color SfondoIspezione => Color.FromArgb(SFONDOISPEZIONER, SFONDOISPEZIONEG, SFONDOISPEZIONEB);

        public static Color SfondoIspezioneConsiderata => Color.FromArgb(SFONDOISPEZIONECONSIDERATAR,
            SFONDOISPEZIONECONSIDERATAG, SFONDOISPEZIONECONSIDERATAB);

        public static Color SfondoIspezioneFantasma => Color.FromArgb(SFONDOISPEZIONEFANTASMAR,
            SFONDOISPEZIONEFANTASMAG, SFONDOISPEZIONEFANTASMAB);

        public static Color SfondoAppuntamento =>
            Color.FromArgb(SFONDOAPPUNTAMENTOR, SFONDOAPPUNTAMENTOG, SFONDOAPPUNTAMENTOB);

        public static Color SfondoBackOffice => Color.FromArgb(SFONDOBACKOFFICER, SFONDOBACKOFFICEG, SFONDOBACKOFFICEB);

        public static Color SfondoVarie => Color.FromArgb(SFONDOVARIER, SFONDOVARIEG, SFONDOVARIEB);
    }
}