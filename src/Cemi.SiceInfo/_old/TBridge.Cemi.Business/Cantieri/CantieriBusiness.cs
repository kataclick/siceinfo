using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Filters.Cantieri;

namespace TBridge.Cemi.Business.Cantieri
{
    public class CantieriBusiness
    {
        private readonly Common _commonBiz = new Common();
        private readonly CantieriDataAccess _dataAccess = new CantieriDataAccess();
        private readonly CantieriWordManager _wordManager = new CantieriWordManager();

        public Cantiere GetCantiere(int idCantiere)
        {
            CantieriFilter filtro = new CantieriFilter {IdCantiere = idCantiere};

            return GetCantieri(filtro)[0];
        }

        /// <summary>
        ///     Restituisce la lista dei cantieri presenti nel database
        /// </summary>
        /// <returns></returns>
        public CantiereCollection GetCantieri(CantieriFilter filtro)
        {
            return _dataAccess.GetCantieriReader(filtro, null, null, false);
        }

        /// <summary>
        ///     Restituisce la lista dei cantieri presenti nel database ordinati per il criterio passato
        /// </summary>
        /// <returns></returns>
        public CantiereCollection GetCantieriOrdinati(CantieriFilter filtro, string sortExpression,
            string sortDirection)
        {
            return _dataAccess.GetCantieriReader(filtro, sortExpression, sortDirection, false);
        }

        public CantiereCollection GetCantieriSenzaCoordinate()
        {
            return _dataAccess.GetCantieriReader(new CantieriFilter(), null, null, true);
        }

        /// <summary>
        ///     Inserisce un cantiere nel database. In caso di inserimento corretto il campo IdCantiere risulter� valorizzato
        /// </summary>
        /// <param name="cantiere"></param>
        public bool InsertCantiere(Cantiere cantiere)
        {
            bool res = false;

            _dataAccess.InsertCantiere(cantiere, null);

            if (cantiere.IdCantiere.HasValue)
                res = true;

            return res;
        }

        /// <summary>
        ///     Aggiorna un cantiere gi� presente nel database
        /// </summary>
        /// <param name="cantiere"></param>
        /// <returns></returns>
        public bool UpdateCantiere(Cantiere cantiere)
        {
            return _dataAccess.UpdateCantiere(cantiere);
        }

        /// <summary>
        ///     Restituisce la lista delle zone presenti nel database
        /// </summary>
        /// <returns></returns>
        public ZonaCollection GetZone(int? idZona)
        {
            return _dataAccess.GetZone(idZona);
        }

        /// <summary>
        ///     Inserisce una zona nel database. In caso di inserimento corretto il campo IdZona risulter� valorizzato
        /// </summary>
        /// <param name="zona"></param>
        public bool InsertZona(Zona zona)
        {
            bool res = false;
            bool doppia = false;

            doppia = _dataAccess.InsertZona(zona);

            if (zona.IdZona.HasValue)
                res = true;

            if (doppia)
                throw new ArgumentException();

            return res;
        }

        /// <summary>
        ///     Aggiorna una zona gi� presente nel database
        /// </summary>
        /// <param name="zona"></param>
        /// <returns></returns>
        public bool UpdateZona(Zona zona)
        {
            return _dataAccess.UpdateZona(zona);
        }

        /// <summary>
        ///     Restituisce la lista degli ispettori presenti nel database
        /// </summary>
        /// <returns></returns>
        public IspettoreCollection GetIspettori(int? idIspettore, bool? operativi)
        {
            return _dataAccess.GetIspettori(idIspettore, operativi);
        }

        /// <summary>
        ///     Restituisce la programmazione settimanale di un ispettore
        /// </summary>
        /// <param name="dal"></param>
        /// <param name="al"></param>
        /// <param name="idIspettore"></param>
        /// <param name="programmato"></param>
        /// <param name="preventivato"></param>
        /// <param name="consuntivato"></param>
        /// <returns></returns>
        public ProgrammazioneGiornaliereIspettoreCollection GetProgrammazioneSettimanale(DateTime dal, DateTime al,
            int idIspettore,
            bool programmato,
            bool preventivato,
            bool consuntivato)
        {
            return _dataAccess.GetProgrammazioneSettimanale(dal, al, idIspettore, programmato, preventivato,
                consuntivato);
        }

        public AttivitaCollection GetProgrammazionePerCantiere(int idCantiere)
        {
            return _dataAccess.GetProgrammazionePerCantiere(idCantiere);
        }

        /// <summary>
        ///     Restituisce la lista delle attivita presenti nel database
        /// </summary>
        /// <returns></returns>
        public TipologiaAttivitaCollection GetTipologieAttivita()
        {
            return _dataAccess.GetTipologieAttivita();
        }

        /// <summary>
        ///     Inserisce una attivita nel database
        /// </summary>
        /// <param name="attivita"></param>
        public bool InsertAttivita(Attivita attivita)
        {
            bool res = false;

            _dataAccess.InsertAttivita(attivita);

            if (attivita.IdAttivita.HasValue)
                res = true;

            return res;
        }

        /// <summary>
        ///     Cancella una attivit�
        /// </summary>
        /// <param name="idAttivita"></param>
        /// <returns></returns>
        public bool DeleteAttivita(int idAttivita)
        {
            return _dataAccess.DeleteAttivita(idAttivita, null);
        }

        /// <summary>
        ///     Restituisce la lista dei committenti presenti nel database ordinati per il criterio passato
        /// </summary>
        /// <returns></returns>
        public CommittenteCollection GetCommittentiOrdinati(int? idCommittenteParam, string ragioneSocialeParam,
            string comuneParam, string indirizzoParam, string ivaFiscale,
            string stringExpression, string sortDirection,
            bool modalitaNotifiche)
        {
            return _dataAccess.GetCommittenti(idCommittenteParam, ragioneSocialeParam, comuneParam, indirizzoParam,
                ivaFiscale, stringExpression, sortDirection, modalitaNotifiche);
        }

        /// <summary>
        ///     Restituisce la lista delle imprese presenti nel database ordinati per il criterio passato
        /// </summary>
        /// <returns></returns>
        public ImpresaCollection GetimpreseOrdinate(TipologiaImpresa tipoImpresa, int? idImpresaParam,
            string ragioneSocialeParam, string comuneParam,
            string indirizzoParam, string ivaFiscale, string stringExpression,
            string sortDirection, bool modalitaNotifiche)
        {
            return _dataAccess.GetImprese(tipoImpresa, idImpresaParam, ragioneSocialeParam, comuneParam, indirizzoParam,
                ivaFiscale, stringExpression, sortDirection, modalitaNotifiche);
        }

        public bool UpdateCantiereCoordinate(int idCantiere, GeocodingResult geoResult)
        {
            return _dataAccess.UpdateCantiereCoordinate(idCantiere, geoResult);
        }

        public bool UpdateAttivitaPreventivata(int idAttivita, bool preventivata)
        {
            return _dataAccess.UpdateAttivitaPrevCons(idAttivita, preventivata, null);
        }

        public bool UpdateAttivitaConsuntivata(int idAttivita, bool consuntivata)
        {
            return _dataAccess.UpdateAttivitaPrevCons(idAttivita, null, consuntivata);
        }

        public RapportoIspezione GetIspezione(int idAttivita)
        {
            RapportoIspezione ispezione = null;

            IspezioniFilter filtro = new IspezioniFilter();
            filtro.IdAttivita = idAttivita;

            RapportoIspezioneCollection ispezioni = _dataAccess.GetIspezione(filtro, false);
            if (ispezioni != null && ispezioni.Count == 1)
            {
                ispezione = ispezioni[0];
            }

            return ispezione;
        }

        public RapportoIspezione GetIspezioneByKey(int idIspezione)
        {
            RapportoIspezione ispezione = null;

            IspezioniFilter filtro = new IspezioniFilter();
            filtro.IdIspezione = idIspezione;
            ispezione = _dataAccess.GetIspezione(filtro, false)[0];

            return ispezione;
        }

        public RapportoIspezioneCollection GetIspezioni(IspezioniFilter filtro)
        {
            return _dataAccess.GetIspezione(filtro, false);
        }

        public RapportoIspezioneCollection GetIspezioniConGruppo(IspezioniFilter filtro)
        {
            return _dataAccess.GetIspezione(filtro, true);
        }

        public RapportoIspezioneImpresaCollection GetIspezioniImprese(int idIspezione, int idCantiere)
        {
            return _dataAccess.GetIspezioniImprese(idIspezione, idCantiere);
        }

        public SubappaltoCollection GetSubappalti(int idCantiere)
        {
            return _dataAccess.GetSubappalti(idCantiere);
        }

        public bool InsertSubappalti(SubappaltoCollection subappalti)
        {
            return _dataAccess.InsertSubappalti(subappalti, null);
        }

        public bool InsertUpdateIspezione(RapportoIspezione ispezione)
        {
            return _dataAccess.InsertUpdateIspezione(ispezione);
        }

        public OSSCollection GetOSS()
        {
            return _dataAccess.GetOSS();
        }

        public RACCollection GetRAC()
        {
            return _dataAccess.GetRAC();
        }

        public LavoratoreCollection GetLavoratoriOrdinati(TipologiaLavoratore tipoLavoratore, int? idLavoratore,
            string cognome, string nome, DateTime? dataNascita,
            string sortExpression, string direct)
        {
            return _dataAccess.GetLavoratoriOrdinatiReader(tipoLavoratore, idLavoratore, cognome, nome, dataNascita,
                sortExpression, direct);
        }

        public LavoratoreCollection GetLavoratoriConUltimoImpiego(string cognome, string nome, DateTime dataNascita)
        {
            return _dataAccess.GetLavoratoriConUltimoImpiego(cognome, nome, dataNascita);
        }

        public RapportoIspezioneLavoratoreCollection GetLavoratoriIspezione(int? idIspezione)
        {
            return _dataAccess.GetLavoratoriIspezione(idIspezione);
        }

        public bool InsertIspezioneLavoratori(RapportoIspezione ispezione)
        {
            return _dataAccess.InsertIspezioneLavoratori(ispezione);
        }

        public bool DeleteIspezioneLavoratore(int idIspezioneLavoratore)
        {
            return _dataAccess.DeleteIspezioneLavoratore(idIspezioneLavoratore);
        }

        public bool DeleteSubappalto(int idSubappalto)
        {
            return _dataAccess.DeleteSubappalto(idSubappalto);
        }

        public bool UpdateIspezioneLavoratore(int idIspezioneLavoratore, int idLavoratoreSiceNew)
        {
            return _dataAccess.UpdateIspezioneLavoratore(idIspezioneLavoratore, idLavoratoreSiceNew);
        }

        public bool UpdateCommittente(Committente committente)
        {
            return _dataAccess.UpdateCommittente(committente);
        }

        public void InsertCommittente(Committente committente)
        {
            _dataAccess.InsertCommittente(committente);
        }

        public bool UpdateImpresa(Impresa impresa)
        {
            return _dataAccess.UpdateImpresa(impresa);
        }

        public void InsertImpresa(Impresa impresa)
        {
            _dataAccess.InsertImpresa(impresa);
        }

        public bool DeleteZona(int idZona)
        {
            return _dataAccess.DeleteZona(idZona);
        }

        public bool ZonaAssociata(int idZona)
        {
            return _dataAccess.ZonaAssociata(idZona);
        }

        public bool DeleteIspezione(int idAttivita)
        {
            return _dataAccess.DeleteIspezione(idAttivita, null, null);
        }

        public bool DeleteIspezioneFromKey(int idIspezione)
        {
            return _dataAccess.DeleteIspezione(null, idIspezione, null);
        }

        public bool UpdateLavoratore(Lavoratore lavoratore)
        {
            return _dataAccess.UpdateLavoratore(lavoratore);
        }

        public void InsertLavoratore(Lavoratore lavoratore)
        {
            _dataAccess.InsertLavoratore(lavoratore);
        }

        public bool DeleteCantiere(int idCantiere)
        {
            // Non � gestita come transazione ma pazienza..
            bool canc = false;
            Cantiere cantiere = GetCantiere(idCantiere);

            if (cantiere.PresaInCarico != null)
            {
                cantiere.Ispezioni = GetIspezioni(new IspezioniFilter {IdCantiere = cantiere.IdCantiere.Value});
                if (cantiere.Ispezioni != null && cantiere.Ispezioni.Count > 0)
                {
                    foreach (RapportoIspezione ispezione in cantiere.Ispezioni)
                    {
                        DeleteIspezioneFromKey(ispezione.IdIspezione.Value);
                    }
                }

                BusinessEF bizEF = new BusinessEF();
                bizEF.DeleteAttivita(cantiere.PresaInCarico.Id);
            }

            canc = _dataAccess.DeleteCantiere(idCantiere);

            return canc;
        }

        public bool FusioneCantieri(Cantiere cantiere, CantiereCollection cantieriAssimilabili)
        {
            return _dataAccess.FusioneCantieri(cantiere, cantieriAssimilabili);
        }

        public List<CantiereFonte> GetCantieriFonti(bool soloNonGeoreferenziati)
        {
            List<CantiereFonte> cantieriFonti = new List<CantiereFonte>();

            List<CantiereFonte> cantieriAem = _dataAccess.GetCantieriFontiAem(soloNonGeoreferenziati);
            List<CantiereFonte> cantieriAsle = _dataAccess.GetCantieriFontiAsle(soloNonGeoreferenziati);
            List<CantiereFonte> cantieriCpt = _dataAccess.getCantieriFontiCpt(soloNonGeoreferenziati);
            cantieriFonti.AddRange(cantieriAem);
            cantieriFonti.AddRange(cantieriAsle);
            cantieriFonti.AddRange(cantieriCpt);

            return cantieriFonti;
        }

        public void UpdateCantiereFonteCoordinate(CantiereFonte cantiereFonte, GeocodingResult geocodingResult)
        {
            switch (cantiereFonte.NomeFonte)
            {
                case "Aem":
                    _dataAccess.UpdateCantiereCoordinateFonteAem(cantiereFonte.Id, geocodingResult);
                    break;
                case "Asle":
                    _dataAccess.UpdateCantiereCoordinateFonteAsle(cantiereFonte.Id, geocodingResult);
                    break;
                case "Cpt":
                    _dataAccess.UpdateCantiereCoordinateFonteCpt(cantiereFonte.Id, geocodingResult);
                    break;
                default:
                    throw new Exception("Fonte non riconosciuta");
            }
        }

        public int NumeroDiProgrammazioniPerCantiere(int idCantiere)
        {
            return _dataAccess.NumeroDiProgrammazioniPerCantiere(idCantiere);
        }

        public bool DeleteIspezioneImpresa(int idRapportoIspezioneImpresa)
        {
            return _dataAccess.DeleteIspezioneImpresa(idRapportoIspezioneImpresa);
        }

        public bool UpdateCommittenteIspettore(Committente committente)
        {
            return _dataAccess.UpdateCommittenteIspettore(committente);
        }

        public bool UpdateImpresaIspettore(Impresa impresa)
        {
            return _dataAccess.UpdateImpresaIspettore(impresa, null);
        }

        public IspettoreCollection GetGruppoIspezione(int? idIspezione)
        {
            return _dataAccess.GetGruppoIspezione(idIspezione);
        }

        public bool InsertSegnalazione(Segnalazione segnalazione)
        {
            return _dataAccess.InsertSegnalazione(segnalazione);
        }

        public bool InsertAssegnazione(Assegnazione assegnazione)
        {
            return _dataAccess.InsertAssegnazione(assegnazione);
        }

        public void CambioPresaInCarico(int idAttivita, int idIspettorePromosso)
        {
            _dataAccess.CambioPresaInCarico(idAttivita, idIspettorePromosso);
        }

        public CantieriStatistiche GetStatisticheCantieri(DateTime dal, DateTime al)
        {
            return _dataAccess.GetStatisticheCantieri(dal, al);
        }

        public int? GetIdImpresaDaCodiceFiscale(string codiceFiscale)
        {
            return _dataAccess.GetIdImpresaDaCodiceFiscale(codiceFiscale);
        }

        public ContrattoCollection GetContratti()
        {
            return _dataAccess.GetContratti();
        }

        public CassaEdileCollection GetIspezioneImpresaCasseEdili(int idIspezioneImpresa)
        {
            return _dataAccess.GetIspezioneImpresaCasseEdili(idIspezioneImpresa);
        }

        public ContrattoCollection GetIspezioneImpresaContratti(int idIspezioneImpresa)
        {
            return _dataAccess.GetIspezioneImpresaContratti(idIspezioneImpresa);
        }

        public void DeleteCassaEdile(int idIspezione, string idCassaEdile)
        {
            _dataAccess.DeleteCassaEdile(idIspezione, idCassaEdile);
        }

        public void DeleteContratto(int idIspezione, string idContratto)
        {
            _dataAccess.DeleteContratto(idIspezione, idContratto);
        }

        #region Lettere

        public string GeneraLettera(TipologiaLettera tipoLettera, LetteraParam param)
        {
            string ret;

            List<WordField> ds = new List<WordField>();

            WordField wfDataLettera = new WordField();
            wfDataLettera.Campo = "DataLettera";
            wfDataLettera.Valore = DateTime.Today.ToShortDateString();

            WordField wfProtocollo = new WordField();
            wfProtocollo.Campo = "NumeroProtocollo";
            wfProtocollo.Valore = param.Protocollo;

            ds.Add(wfDataLettera);
            ds.Add(wfProtocollo);

            if (param.DataAppuntamento.HasValue)
            {
                WordField wfDataAppuntamento = new WordField();
                wfDataAppuntamento.Campo = "DataAppuntamento";
                wfDataAppuntamento.Valore = param.DataAppuntamento.Value.ToShortDateString();
                ds.Add(wfDataAppuntamento);
            }

            List<WordField> dsTemp = _dataAccess.GetWordDataSource(tipoLettera, param);
            ds.AddRange(dsTemp);

            ret = _wordManager.CreateWord(tipoLettera, ds);

            LogLettera(DateTime.Now, param.Protocollo, ret, param.IdIspezione, param.GruppoLettera, param.IdImpresa,
                param.IdCantieriImpresa);

            return ret;
        }

        private void LogLettera(DateTime data, string protocollo, string fileLettera, int idIspezione,
            GruppoLettera gruppoLettera, int? idImpresa, int? idCantieriImpresa)
        {
            _dataAccess.LogLettera(data, protocollo, fileLettera, idIspezione, gruppoLettera, idImpresa,
                idCantieriImpresa);
        }

        public TipologiaAppalto CaricaTipologiaAppalto(int idIspezione)
        {
            return _dataAccess.GetTipologiaAppalto(idIspezione);
        }

        public List<LogLettera> CaricaLogLettere(LettereFilter filtro)
        {
            return _dataAccess.GetLogLettere(filtro);
        }

        public List<LogLettera> CaricaLogLetterePerIspezione(int idIspezione)
        {
            return _dataAccess.GetLogLetterePerIspezione(idIspezione);
        }

        public byte[] CaricaLettera(string protocollo)
        {
            return _dataAccess.GetLettera(protocollo);
        }

        public List<SubappaltoLettera> CaricaSubappaltiLettera(int idIspezione)
        {
            return _dataAccess.GetSubappaltiLettera(idIspezione);
        }

        public bool DeleteLettera(string protocollo)
        {
            return _dataAccess.DeleteLettera(protocollo);
        }

        #endregion

        #region Funzioni per il caricamento delle DropDown per le province, comuni e cap

        public void CaricaProvinceInDropDown(DropDownList dropDownProvince)
        {
            DataTable dtProvince = _commonBiz.GetProvince();

            dropDownProvince.Items.Clear();
            dropDownProvince.Items.Add(new ListItem(string.Empty, null));

            dropDownProvince.DataSource = dtProvince;
            dropDownProvince.DataTextField = "sigla";
            dropDownProvince.DataValueField = "idProvincia";

            dropDownProvince.DataBind();
        }

        public void CaricaComuniInDropDown(DropDownList dropDownComuni, int idProvincia)
        {
            dropDownComuni.Items.Clear();
            dropDownComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idProvincia > 0)
            {
                DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

                dropDownComuni.DataSource = dtComuni;
                dropDownComuni.DataTextField = "denominazione";
                dropDownComuni.DataValueField = "idComune";
            }

            dropDownComuni.DataBind();
        }

        public void CaricaCapInDropDown(DropDownList dropDownCap, long idComune)
        {
            dropDownCap.Items.Clear();
            dropDownCap.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idComune > 0)
            {
                DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

                dropDownCap.DataSource = dtCAP;
                dropDownCap.DataTextField = "cap";
                dropDownCap.DataValueField = "cap";
            }

            dropDownCap.DataBind();
        }

        public void CambioSelezioneDropDownProvincia(DropDownList ddlProvincia, DropDownList ddlComune,
            DropDownList ddlCap)
        {
            int idProvincia = -1;

            if (ddlProvincia.SelectedValue != null)
                int.TryParse(ddlProvincia.SelectedValue, out idProvincia);

            CaricaComuniInDropDown(ddlComune, idProvincia);

            CaricaCapInDropDown(ddlCap, -1);
        }

        public void CambioSelezioneDropDownComune(DropDownList ddlComune, DropDownList ddlCap)
        {
            long idComune = -1;

            if (ddlComune.SelectedValue != null)
                long.TryParse(ddlComune.SelectedValue, out idComune);

            CaricaCapInDropDown(ddlCap, idComune);
        }

        #endregion

        #region Gestione scadenza

        /// <summary>
        ///     Aggiorna il numero di giorni oltre i quali un ispettore non pu� pi� modificare un rapporto d'ispezione
        /// </summary>
        /// <returns></returns>
        public void AggiornaScadenza(int scadenza)
        {
            _dataAccess.AggiornaScadenza(scadenza);
        }

        /// <summary>
        ///     Ritorna il numero di giorni oltre i quali un ispettore non pu� pi� modificare un rapporto d'ispezione
        /// </summary>
        /// <returns></returns>
        public int GetScadenza()
        {
            return _dataAccess.GetScadenza();
        }

        #endregion
    }
}