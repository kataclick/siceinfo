using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Business.Office;
using TBridge.Cemi.Type.Enums.Cantieri;
using WordField = TBridge.Cemi.Type.WordField;

namespace TBridge.Cemi.Business.Cantieri
{
    public class CantieriWordManager
    {
        private readonly string _pathGenerazioneDocumenti =
            ConfigurationManager.AppSettings["PathGenerazioneDocumenti"];

        private readonly string _pathM70501 = ConfigurationManager.AppSettings["M70501"];
        private readonly string _pathM70505 = ConfigurationManager.AppSettings["M70505"];
        private readonly string _pathM7050501 = ConfigurationManager.AppSettings["M7050501"];
        private readonly string _pathM7050502Comune = ConfigurationManager.AppSettings["M7050502Comune"];
        private readonly string _pathM70506 = ConfigurationManager.AppSettings["M70506"];
        private readonly string _pathM7050601 = ConfigurationManager.AppSettings["M7050601"];
        private readonly string _pathM7050602 = ConfigurationManager.AppSettings["M7050602"];
        private readonly string _pathM7050603 = ConfigurationManager.AppSettings["M7050603"];
        private readonly string _pathM70509 = ConfigurationManager.AppSettings["M70509"];
        private readonly string _pathM7050901 = ConfigurationManager.AppSettings["M7050901"];
        private readonly string _pathM70510 = ConfigurationManager.AppSettings["M70510"];
        private readonly string _pathM70511 = ConfigurationManager.AppSettings["M70511"];
        private readonly string _pathM7051101Comune = ConfigurationManager.AppSettings["M7051101Comune"];
        private readonly string _pathM70511Comune = ConfigurationManager.AppSettings["M70511Comune"];

        public string CreateWord(TipologiaLettera tipoLettera, List<WordField> ds)
        {
            string ret;

            switch (tipoLettera)
            {
                case TipologiaLettera.M70505:
                    ret = WordManager.CreateWord(_pathM70505, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050502Comune:
                    ret = WordManager.CreateWord(_pathM7050502Comune, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050601:
                    ret = WordManager.CreateWord(_pathM7050601, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050602:
                    ret = WordManager.CreateWord(_pathM7050602, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050603:
                    ret = WordManager.CreateWord(_pathM7050603, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70506:
                    ret = WordManager.CreateWord(_pathM70506, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050901:
                    ret = WordManager.CreateWord(_pathM7050901, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70509:
                    ret = WordManager.CreateWord(_pathM70509, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70510:
                    ret = WordManager.CreateWord(_pathM70510, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70511:
                    ret = WordManager.CreateWord(_pathM70511, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70511Comune:
                    ret = WordManager.CreateWord(_pathM70511Comune, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7051101Comune:
                    ret = WordManager.CreateWord(_pathM7051101Comune, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M70501:
                    ret = WordManager.CreateWord(_pathM70501, ds, _pathGenerazioneDocumenti);
                    break;
                case TipologiaLettera.M7050501:
                    ret = WordManager.CreateWord(_pathM7050501, ds, _pathGenerazioneDocumenti);
                    break;
                default:
                    //throw new ArgumentOutOfRangeException("tipoLettera");
                    ret = string.Empty;
                    break;
            }

            return ret;
        }
    }
}