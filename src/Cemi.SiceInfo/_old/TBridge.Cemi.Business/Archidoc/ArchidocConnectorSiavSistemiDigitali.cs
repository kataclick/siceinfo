﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using Cemi.SiceInfo.Utility;
using TBridge.Cemi.Archidoc.Type.Collection;
using TBridge.Cemi.Archidoc.Type.Collections;
using TBridge.Cemi.Archidoc.Type.Entities;
using TBridge.Cemi.Business.Archidoc.Enums;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Business.ArchidocWsSsd;

namespace TBridge.Cemi.Business.Archidoc
{
    internal class ArchidocConnectorSiavSistemiDigitali : IArchidocService
    {
        public ArchidocConnectorSiavSistemiDigitali()
        {
            ArchidocServer = ConfigurationManager.AppSettings["ArchidocServer"];
            ArchidocMainEm = ConfigurationManager.AppSettings["ArchidocMainEm"];
            ArchidocGpEm = ConfigurationManager.AppSettings["ArchidocGpEm"];
            ArchidocDatabase = ConfigurationManager.AppSettings["ArchidocDatabase"];
            ArchidocUsername = ConfigurationManager.AppSettings["ArchidocUsername"];
            ArchidocPassword = ConfigurationManager.AppSettings["ArchidocPassword"];
        }

        //private String ArchidocServer = (ConfigurationManager.AppSettings["ArchidocServer"]);
        //private String ArchidocMainEm = (ConfigurationManager.AppSettings["ArchidocMainEm"]);
        //private String ArchidocGpEm = (ConfigurationManager.AppSettings["ArchidocGpEm"]);
        //private String ArchidocDatabase = (ConfigurationManager.AppSettings["ArchidocDatabase"]);
        //private String ArchidocUsername = (ConfigurationManager.AppSettings["ArchidocUsername"]);
        //private String ArchidocPassword = (ConfigurationManager.AppSettings["ArchidocPassword"]);

        private string ArchidocServer { get; }
        private string ArchidocMainEm { get; }
        private string ArchidocGpEm { get; }
        private string ArchidocDatabase { get; }
        private string ArchidocUsername { get; }
        private string ArchidocPassword { get; }

        #region Private

        /// <summary>
        ///     Per i tipi ricerca DataScansione e IdDelega impostare i parametri da, a allo stesso valore
        /// </summary>
        /// <param name="nomeArchivio"></param>
        /// <param name="nomeTipoDocumento"></param>
        /// <param name="da"></param>
        /// <param name="a"></param>
        /// <param name="tipoRicerca"></param>
        /// <returns></returns>
        private DataSet GetCardsByTipoRicerca(string nomeArchivio, string nomeTipoDocumento, string da, string a,
            TipiRicercaArchidoc tipoRicerca)
        {
            DataSet cardsDataSet = null;
            string sReferenceFrom = null;
            string sReferenceTo = null;
            string sDateReg = null;
            // String sDateRegTo = null;
            string sKey22 = null;

            switch (tipoRicerca)
            {
                case TipiRicercaArchidoc.DataScansione:
                    sDateReg = da;
                    // sDateRegTo = a;
                    break;
                case TipiRicercaArchidoc.ProtocolloArchidoc:
                    sReferenceFrom = da;
                    sReferenceTo = a;
                    break;
                case TipiRicercaArchidoc.IdDelega:
                    sKey22 = da;
                    break;
                default:
                    break;
            }

            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                cardsDataSet = archidocServiceClient.GetList(ArchidocServer, ArchidocDatabase, ArchidocUsername,
                    ArchidocPassword, nomeArchivio, nomeTipoDocumento, 0, sReferenceFrom, sReferenceTo,
                    sDateReg, null, null, null, null, null, null, null, null, null, null, null, sKey22, null, null,
                    null, null, null, null, null, null, null, null, null, null, null);
            }

            return cardsDataSet;
        }

        #endregion

        #region IArchidocService Members

        public byte[] GetDocument(string guid, bool tiffToPdf)
        {
            string documentName;
            string documentExtention;
            byte[] document;
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                document = archidocServiceClient.GetDocumentByGUID(ArchidocServer, ArchidocDatabase, ArchidocGpEm,
                    ArchidocMainEm, ArchidocUsername, ArchidocPassword, guid, out documentName, out documentExtention);
            }

            if (tiffToPdf && document != null)
            {
                TiffToPdfConverter converter = new TiffToPdfConverter();
                if ((documentExtention.Equals("tif", StringComparison.CurrentCultureIgnoreCase)
                     || documentExtention.Equals("tiff", StringComparison.CurrentCultureIgnoreCase))
                    && converter.IsTiff(document))
                {
                    //TODO converti
                    document = converter.TiffToPdf(document);
                }
            }

            return document;
        }

        public byte[] GetDocument(string guid)
        {
            return GetDocument(guid, true);
        }

        public void UpdateCard(Documento documento, string nomeArchivio, string nomeTipoDocumento)
        {
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                // Recupero il documento dal protocollo scansione 
                // IMPORTANTE: Il protocollo del tipo Documento corrisponde al Progressivo Annuo di Archidoc (univoco)
                DataSet dataSet = archidocServiceClient.GetList(ArchidocServer, ArchidocDatabase, ArchidocUsername,
                    ArchidocPassword, nomeArchivio, nomeTipoDocumento, 0, documento.ProtocolloScansione,
                    documento.ProtocolloScansione,
                    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
                    null, null, null, null, null, null, null, null, null, null);

                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count == 1)
                {
                    DataTable cardTable = dataSet.Tables[0];

                    if (cardTable.Rows.Count == 1)
                    {
                        DataRow card = cardTable.Rows[0];

                        string cardGUID = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();

                        archidocServiceClient.UpdateByGUID(
                            cardGUID, ArchidocServer, ArchidocDatabase, ArchidocUsername, ArchidocPassword,
                            card[ChiaviArchidocWsSsd.svIfProtocol.ToString()].ToString(),
                            documento.Get(ChiaviArchidocWsSsd.svIfDateDoc.ToString(),
                                card[ChiaviArchidocWsSsd.svIfDateDoc.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey11.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey11.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey12.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey12.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey13.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey13.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey14.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey14.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey15.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey15.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey21.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey21.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey22.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey22.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey23.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey23.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey24.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey24.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey25.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey25.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey31.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey31.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey32.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey32.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey33.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey33.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey34.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey34.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey35.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey35.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey41.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey41.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey42.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey42.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey43.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey43.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey44.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey44.ToString()].ToString()),
                            documento.Get(ChiaviArchidocWsSsd.svIfKey45.ToString(),
                                card[ChiaviArchidocWsSsd.svIfKey45.ToString()].ToString()),
                            card[ChiaviArchidocWsSsd.svIfObj.ToString()].ToString(),
                            null,
                            null
                        );
                    }
                }
            }
        }

        public void InsertCardWithDocument(string nomeArchivio, string nomeTipoDocumento, Documento documento)
        {
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                string guid = archidocServiceClient.Insert(
                    ArchidocServer, ArchidocDatabase, ArchidocGpEm, ArchidocMainEm, ArchidocUsername, ArchidocPassword,
                    nomeArchivio, nomeTipoDocumento,
                    null,
                    documento.Get(ChiaviArchidocWsSsd.svIfDateDoc.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey11.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey12.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey13.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey14.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey15.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey21.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey22.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey23.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey24.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey25.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey31.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey32.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey33.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey34.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey35.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey41.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey42.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey43.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey44.ToString(), null),
                    documento.Get(ChiaviArchidocWsSsd.svIfKey45.ToString(), null),
                    null,
                    documento.FileByteArray,
                    documento.FileNome,
                    documento.FileByteArray == null
                );

                documento.IdArchidoc = guid;
            }
        }

        public void InsertCardsWithDocuments<T>(string nomeArchivio, string nomeTipoDocumento, List<T> documenti)
            where T : Documento
        {
            string guid;
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                foreach (Documento documento in documenti)
                {
                    guid = archidocServiceClient.Insert(
                        ArchidocServer, ArchidocDatabase, ArchidocGpEm, ArchidocMainEm, ArchidocUsername,
                        ArchidocPassword, nomeArchivio, nomeTipoDocumento,
                        null,
                        documento.Get(ChiaviArchidocWsSsd.svIfDateDoc.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey11.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey12.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey13.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey14.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey15.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey21.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey22.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey23.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey24.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey25.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey31.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey32.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey33.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey34.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey35.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey41.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey42.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey43.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey44.ToString(), null),
                        documento.Get(ChiaviArchidocWsSsd.svIfKey45.ToString(), null),
                        null,
                        documento.FileByteArray,
                        documento.FileNome,
                        documento.FileByteArray == null
                    );

                    documento.IdArchidoc = guid;
                }
            }
        }

        #region Allegati

        public void InsertAllegatoEsterno(string cardGuid, string note, string nomeFile, byte[] fileByteArray)
        {
            string retVal;
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                retVal = archidocServiceClient.InsertAttachment(ArchidocServer, ArchidocDatabase, ArchidocGpEm,
                    ArchidocMainEm, ArchidocUsername, ArchidocPassword, cardGuid, note, fileByteArray, nomeFile);
            }
        }

        public void InsertAllegatoInterno(string cardGuid, string progressivoAnnuo, string note, string nomeArchivio)
        {
            bool result;
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                result = archidocServiceClient.InsertInternalAttachment(ArchidocServer, ArchidocDatabase, ArchidocGpEm,
                    ArchidocMainEm, ArchidocUsername, ArchidocPassword, cardGuid, note, progressivoAnnuo, nomeArchivio);
            }
        }

        public AllegatoCollection GetAllegati(string cardGuid, bool getDocumenti)
        {
            AllegatoCollection allegati = new AllegatoCollection();
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                DataSet dataSet = archidocServiceClient.GetAttachments(ArchidocServer, ArchidocDatabase,
                    ArchidocUsername, ArchidocPassword, cardGuid);
                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count == 1)
                {
                    DataTable attachmentsTable = dataSet.Tables[0];
                    foreach (DataRow attachment in attachmentsTable.Rows)
                    {
                        allegati.Add(ArchidocHelpers.DataRowToAllegato(attachment));
                    }
                }

                if (getDocumenti)
                {
                    for (int i = 0; i < allegati.Count; i++)
                    {
                        byte[] buffer;
                        archidocServiceClient.SaveAttachment(ArchidocServer, ArchidocDatabase, ArchidocUsername,
                            ArchidocPassword, cardGuid, i, out buffer);
                        allegati[i].FileByteArray = buffer;
                    }
                }
            }

            return allegati;
        }

        public Allegato GetAllegatoEsternoByNomeFile(string cardGuid, string nomeFile, bool getDocumento)
        {
            Allegato allegato = null;
            int index = -1;

            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                DataSet dataSet = archidocServiceClient.GetAttachments(ArchidocServer, ArchidocDatabase,
                    ArchidocUsername, ArchidocPassword, cardGuid);
                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count == 1)
                {
                    DataTable attachmentsTable = dataSet.Tables[0];
                    foreach (DataRow attachment in attachmentsTable.Rows)
                    {
                        index++;
                        string file = ArchidocHelpers.GetNomeFileAllegatoFromDataRow(attachment);
                        if (file != null && file.Equals(nomeFile, StringComparison.CurrentCultureIgnoreCase))
                        {
                            allegato = ArchidocHelpers.DataRowToAllegato(attachment);
                            break;
                        }
                    }
                }

                if (allegato != null && getDocumento)
                {
                    byte[] buffer;
                    archidocServiceClient.SaveAttachment(ArchidocServer, ArchidocDatabase, ArchidocUsername,
                        ArchidocPassword, cardGuid, index, out buffer);
                    allegato.FileByteArray = buffer;
                }
            }

            return allegato;
        }

        public void DeleteAllegato(string cardGuid, string nomeAllegato)
        {
            using (ServiceSoapClient archidocServiceClient = new ServiceSoapClient())
            {
                archidocServiceClient.DeleteAttachment(ArchidocUsername, ArchidocPassword, cardGuid, nomeAllegato);
            }
        }

        #endregion

        #region Get Collections

        public CodiceFiscaleCollection GetCodiciFiscali(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            CodiceFiscaleCollection codiciFiscali = new CodiceFiscaleCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Codice Fiscale";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    TipiRicercaArchidoc.DataScansione);

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        CodiceFiscale codiceFiscale = new CodiceFiscale();
                        codiceFiscale.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            codiceFiscale.Set(column.ColumnName, card[column].ToString());
                        }

                        codiciFiscali.Add(codiceFiscale);
                    }
                }
            }

            return codiciFiscali;
        }

        public ModuloCollection GetModuliPrestazioni(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            ModuloCollection domandePrestazioni = new ModuloCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Prestazioni";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    TipiRicercaArchidoc.DataScansione);
                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        Modulo modulo = new Modulo();
                        modulo.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            modulo.Set(column.ColumnName, card[column].ToString());
                        }

                        domandePrestazioni.Add(modulo);
                    }
                }
            }

            return domandePrestazioni;
        }

        public StatoFamigliaCollection GetStatiDiFamiglia(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            StatoFamigliaCollection statiDiFamiglia = new StatoFamigliaCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Stato di Famiglia";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    TipiRicercaArchidoc.DataScansione);
                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        StatoFamiglia statoFamiglia = new StatoFamiglia();
                        statoFamiglia.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            statoFamiglia.Set(column.ColumnName, card[column].ToString());
                        }

                        statiDiFamiglia.Add(statoFamiglia);
                    }
                }
            }

            return statiDiFamiglia;
        }

        public CertificatoCollection GetCertificati(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            CertificatoCollection certificati = new CertificatoCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Certificati";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    TipiRicercaArchidoc.DataScansione);

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        Certificato certificato = new Certificato();
                        certificato.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            certificato.Set(column.ColumnName, card[column].ToString());
                        }

                        certificati.Add(certificato);
                    }
                }
            }

            return certificati;
        }

        public FatturaCollection GetFatture(DateTime dataScansioneDa, DateTime dataScansioneA)
        {
            FatturaCollection fatture = new FatturaCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Fatture";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    TipiRicercaArchidoc.DataScansione);

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        Fattura fattura = new Fattura();
                        fattura.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            fattura.Set(column.ColumnName, card[column].ToString());
                        }

                        fatture.Add(fattura);
                    }
                }
            }

            return fatture;
        }

        public DichiarazioneInterventoCollection GetDichiarazioniIntervento(DateTime dataScansioneDa,
            DateTime dataScansioneA)
        {
            DichiarazioneInterventoCollection dichiarazioni = new DichiarazioneInterventoCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Dettaglio Fatture";

            TimeSpan giorniTimeSpan = dataScansioneA.Subtract(dataScansioneDa);
            int giorni = giorniTimeSpan.Days;

            for (int giornoScansione = 0; giornoScansione <= giorni; giornoScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    dataScansioneDa.AddDays(giornoScansione).ToString("dd/MM/yyyy"),
                    TipiRicercaArchidoc.DataScansione);

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        DichiarazioneIntervento dichiarazione = new DichiarazioneIntervento();
                        dichiarazione.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            dichiarazione.Set(column.ColumnName, card[column].ToString());
                        }

                        dichiarazioni.Add(dichiarazione);
                    }
                }
            }

            return dichiarazioni;
        }

        public DelegaCollection GetDeleghe(int delegaDa, int delegaA)
        {
            DelegaCollection deleghe = new DelegaCollection();

            string nomeArchivio = "Generale";
            string nomeTipoDocumento = "Deleghe";

            for (int delegaScansione = delegaDa; delegaScansione <= delegaA; delegaScansione++)
            {
                DataSet cardsDataSet = GetCardsByTipoRicerca(nomeArchivio, nomeTipoDocumento,
                    delegaScansione.ToString(),
                    delegaScansione.ToString(),
                    TipiRicercaArchidoc.IdDelega);

                if (cardsDataSet.Tables.Count == 1)
                {
                    DataTable cardsTable = cardsDataSet.Tables[0];

                    foreach (DataRow card in cardsTable.Rows)
                    {
                        Delega delega = new Delega();
                        delega.IdArchidoc = card[ChiaviArchidocWsSsd.GUID.ToString()].ToString();
                        foreach (DataColumn column in cardsTable.Columns)
                        {
                            delega.Set(column.ColumnName, card[column].ToString());
                        }

                        deleghe.Add(delega);
                    }
                }
            }

            return deleghe;
        }

        #endregion

        #endregion
    }
}