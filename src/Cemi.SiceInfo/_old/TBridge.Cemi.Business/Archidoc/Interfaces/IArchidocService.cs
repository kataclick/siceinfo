using System;
using System.Collections.Generic;
using TBridge.Cemi.Archidoc.Type.Collection;
using TBridge.Cemi.Archidoc.Type.Collections;
using TBridge.Cemi.Archidoc.Type.Entities;

namespace TBridge.Cemi.Business.Archidoc.Interfaces
{
    public interface IArchidocService
    {
        /// <summary>
        ///     Restituisce i byte del documento ricercato tramite il suo guid
        /// </summary>
        /// <param name="guid">archidoc guid</param>
        /// <returns></returns>
        byte[] GetDocument(string guid);

        /// <summary>
        ///     Restituisce i byte del documento ricercato tramite il suo guid
        /// </summary>
        /// <param name="guid">archidoc guid</param>
        /// <param name="tifToPdf">converte tif in pdf (funziona solo con file tif)</param>
        /// <returns></returns>
        byte[] GetDocument(string guid, bool tifToPdf);

        ///// <summary>
        ///// Restituisce i byte del documento ricercato tramite il suo guid
        ///// </summary>
        ///// <param name="guid">archidoc guid</param>
        ///// <param name="tiffToPdf">converte tiff in pdf (funziona solo per i tiff)</param>
        ///// <returns></returns>
        //byte[] GetDocument(string guid, int pageFrom, int pageTo);

        void UpdateCard(Documento documento, string nomeArchivio, string nomeTipoDocumento);

        void InsertCardWithDocument(string nomeArchivio, string nomeTipoDocumento, Documento documento);

        void InsertCardsWithDocuments<T>(string nomeArchivio, string nomeTipoDocumento, List<T> documenti)
            where T : Documento;

        CodiceFiscaleCollection GetCodiciFiscali(DateTime dataScansioneDa, DateTime dataScansioneA);

        ModuloCollection GetModuliPrestazioni(DateTime dataScansioneDa, DateTime dataScansioneA);

        StatoFamigliaCollection GetStatiDiFamiglia(DateTime dataScansioneDa, DateTime dataScansioneA);

        CertificatoCollection GetCertificati(DateTime dataScansioneDa, DateTime dataScansioneA);

        FatturaCollection GetFatture(DateTime dataScansioneDa, DateTime dataScansioneA);

        DichiarazioneInterventoCollection GetDichiarazioniIntervento(DateTime dataScansioneDa, DateTime dataScansioneA);

        DelegaCollection GetDeleghe(int delegaDa, int delegaA);

        #region Allegati

        void InsertAllegatoEsterno(string cardGuid, string note, string nomeFile, byte[] fileByteArray);

        void InsertAllegatoInterno(string cardGuid, string progressivoAnnuo, string note, string nomeArchivio);

        AllegatoCollection GetAllegati(string cardGuid, bool getDocumenti);

        Allegato GetAllegatoEsternoByNomeFile(string cardGuid, string nomeFile, bool getDocumento);

        void DeleteAllegato(string cardGuid, string nomeAllegato);

        #endregion
    }
}