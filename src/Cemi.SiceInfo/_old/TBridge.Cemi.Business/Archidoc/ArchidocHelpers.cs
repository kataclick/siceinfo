﻿using System.Data;
using TBridge.Cemi.Archidoc.Type.Entities;

namespace TBridge.Cemi.Business.Archidoc
{
    internal class ArchidocHelpers
    {
        internal static Allegato SvwsstAttachmentToAllegato(svwsstAttachment attachment)
        {
            return new Allegato
            {
                CardGuid = attachment.GuidCard,
                AllegatoGuId = attachment.GuidAttachment,
                Interno = attachment.IsInternal,
                NomeFile = attachment.Name,
                Codice = attachment.Code,
                Note = attachment.Note,
                FileByteArray = attachment.AttachmentFile
            };
        }

        internal static Allegato DataRowToAllegato(DataRow attachment)
        {
            Allegato allegato = new Allegato();
            for (int i = 0; i < attachment.ItemArray.Length; i++)
            {
                string value = attachment[i] != null ? attachment[i].ToString() : null;
                switch (i)
                {
                    case 0:
                        allegato.NomeFile = value;
                        break;
                    case 1:
                        allegato.Note = value;
                        break;
                    case 2:
                        allegato.AllegatoGuId = value;
                        break;
                    case 3:
                        allegato.CardGuid = value;
                        break;
                    case 4:
                        allegato.Interno = value == "1" ? true : false;
                        break;
                    default:
                        break;
                }
            }
            return allegato;
        }


        internal static string GetNomeFileAllegatoFromDataRow(DataRow attachment)
        {
            return attachment[0] as string;
        }
    }
}