﻿namespace TBridge.Cemi.Business.Archidoc
{
    public class ArchidocLogin
    {
        public string ArchidocServer { get; set; }
        public string ArchidocMainEm { get; set; }
        public string ArchidocGpEm { get; set; }
        public string ArchidocDataBase { get; set; }
        public string ArchidocUsername { get; set; }
        public string ArchidocPassword { get; set; }
    }
}