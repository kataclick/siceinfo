namespace TBridge.Cemi.Business.Archidoc.Enums
{
    public enum TipiRicercaArchidoc
    {
        DataScansione,
        ProtocolloArchidoc,
        IdDelega
    }
}