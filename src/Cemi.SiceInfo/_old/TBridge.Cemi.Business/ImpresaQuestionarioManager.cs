﻿using System;
using System.Collections.Generic;
using System.Linq;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;

namespace TBridge.Cemi.Business
{
    public class ImpresaQuestionarioManager
    {
        private readonly Data.Common dataAccess = new Data.Common();

        public ImpresaQuestionarioImpiegati GetQuestionario(int idImpresa)
        {
            ImpresaQuestionarioImpiegati ret;

            using (SICEEntities context = new SICEEntities())
            {
                var queryQuestionario =
                    from questionario in context.ImpresaQuestionarioImpiegati.Include("Scadenze")
                    where questionario.IdImpresa == idImpresa
                    select questionario;

                ret = queryQuestionario.SingleOrDefault();
            }

            return ret;
        }

        public List<ImpreseConsulente> GetImpreseConsulente(int idConsulente, ConsulenteImpreseFilter filtro)
        {
            List<ImpreseConsulente> ret = null;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from impreseConsulente in context.ImpreseConsulenti
                    join impresa in context.Imprese
                        on impreseConsulente.IdImpresa equals impresa.Id
                    join questionario in context.ImpresaQuestionarioImpiegati
                        on impresa.Id equals questionario.IdImpresa
                        into quest
                    from questionario in quest.DefaultIfEmpty()
                    where impreseConsulente.IdConsulente == idConsulente
                          && !impreseConsulente.DataDisdetta.HasValue
                          // impresa    
                          && (!filtro.IdImpresa.HasValue || impresa.Id == filtro.IdImpresa)
                          && (string.IsNullOrEmpty(filtro.RagioneSocialeImpresa) ||
                              impresa.RagioneSociale.Contains(filtro.RagioneSocialeImpresa))
                          && (string.IsNullOrEmpty(filtro.CodiceFiscaleImpresa) ||
                              impresa.CodiceFiscale.Contains(filtro.CodiceFiscaleImpresa))
                          && (!filtro.Questionario.HasValue || filtro.Questionario.Value && quest.Count() == 1 ||
                              !filtro.Questionario.Value && quest.Count() == 0)
                    select
                        new ImpreseConsulente
                        {
                            IdImpresa = impresa.Id,
                            RagioneSociale = impresa.RagioneSociale,
                            CodiceFiscale = impresa.CodiceFiscale,
                            PartitaIva = impresa.PartitaIVA,
                            QuestionarioPresente = questionario == null ? false : true
                        };


                ret = query.ToList();
            }

            return ret;
        }

        public void SaveQuestionario(ImpresaQuestionarioImpiegati questionario)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var queryQuestionario =
                    from quest in context.ImpresaQuestionarioImpiegati.Include("Scadenze")
                    where quest.IdImpresa == questionario.IdImpresa
                    select quest;

                ImpresaQuestionarioImpiegati questUpd = queryQuestionario.SingleOrDefault();

                if (questUpd != null)
                {
                    questUpd.ImpiegatiForzaAmministrativi = questionario.ImpiegatiForzaAmministrativi;
                    questUpd.ImpiegatiForzaTecnici = questionario.ImpiegatiForzaTecnici;
                    questUpd.ImpiegatiIndeterminatiParziali = questionario.ImpiegatiIndeterminatiParziali;
                    questUpd.ImpiegatiIndeterminatiPieni = questionario.ImpiegatiIndeterminatiPieni;
                    questUpd.ImpiegatiDeterminatiParziali = questionario.ImpiegatiDeterminatiParziali;
                    questUpd.ImpiegatiDeterminatiPieni = questionario.ImpiegatiDeterminatiPieni;
                    questUpd.OreAssenzaMalattia = questionario.OreAssenzaMalattia;
                    questUpd.OreAssenzaCassa = questionario.OreAssenzaCassa;

                    foreach (var scad in questUpd.Scadenze.ToList())
                    {
                        context.ImpresaQuestionarioImpiegatiDeterminatiScadenze.DeleteObject(scad);
                    }
                    foreach (ImpresaQuestionarioImpiegatiDeterminatiScadenza scad in questionario.Scadenze)
                    {
                        questUpd.Scadenze.Add(scad);
                    }

                    questUpd.DataModificaRecord = DateTime.Now;
                }
                else
                {
                    questionario.DataInserimentoRecord = DateTime.Now;
                    questionario.DataModificaRecord = DateTime.Now;
                    context.ImpresaQuestionarioImpiegati.AddObject(questionario);
                }

                context.SaveChanges();
            }
        }

        public void InsertFiltroRicerca(int userID, ConsulenteImpreseFilter filtro, int pagina)
        {
            dataAccess.InsertFiltroRicerca(userID, filtro, pagina);
        }

        public ConsulenteImpreseFilter GetFiltroRicerca(int userID, out int pagina)
        {
            return dataAccess.GetFiltroRicerca(userID, out pagina);
        }

        public void DeleteFiltroRicerca(int userID)
        {
            dataAccess.DeleteFiltroRicerca(userID);
        }
    }
}