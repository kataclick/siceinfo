﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace TBridge.Cemi.Business.MavProximaWebReference {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="MAVOnlineBinding", Namespace="http://webservice.pagamenti.infogroup.it/")]
    public partial class MAVOnlineBeanService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback CreaMAVOnlineOperationCompleted;
        
        private System.Threading.SendOrPostCallback CreaMAVOnlineNoDocOperationCompleted;
        
        private System.Threading.SendOrPostCallback FindMAVOnlineByIdOperazioneOperationCompleted;
        
        private System.Threading.SendOrPostCallback FindMAVOnlineByNumeroMAVOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public MAVOnlineBeanService() {
            this.Url = global::TBridge.Cemi.Business.Properties.Settings.Default.TBridge_Cemi_Business_MavProximaWebReference_MAVOnlineBeanService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event CreaMAVOnlineCompletedEventHandler CreaMAVOnlineCompleted;
        
        /// <remarks/>
        public event CreaMAVOnlineNoDocCompletedEventHandler CreaMAVOnlineNoDocCompleted;
        
        /// <remarks/>
        public event FindMAVOnlineByIdOperazioneCompletedEventHandler FindMAVOnlineByIdOperazioneCompleted;
        
        /// <remarks/>
        public event FindMAVOnlineByNumeroMAVCompletedEventHandler FindMAVOnlineByNumeroMAVCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.pagamenti.infogroup.it/", ResponseNamespace="http://webservice.pagamenti.infogroup.it/", Use=System.Web.Services.Description.SoapBindingUse.Literal)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public rispostaMAVOnlineWS CreaMAVOnline(richiestaMAVOnlineWS RichiestaEmissioneMAV) {
            object[] results = this.Invoke("CreaMAVOnline", new object[] {
                        RichiestaEmissioneMAV});
            return ((rispostaMAVOnlineWS)(results[0]));
        }
        
        /// <remarks/>
        public void CreaMAVOnlineAsync(richiestaMAVOnlineWS RichiestaEmissioneMAV) {
            this.CreaMAVOnlineAsync(RichiestaEmissioneMAV, null);
        }
        
        /// <remarks/>
        public void CreaMAVOnlineAsync(richiestaMAVOnlineWS RichiestaEmissioneMAV, object userState) {
            if ((this.CreaMAVOnlineOperationCompleted == null)) {
                this.CreaMAVOnlineOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCreaMAVOnlineOperationCompleted);
            }
            this.InvokeAsync("CreaMAVOnline", new object[] {
                        RichiestaEmissioneMAV}, this.CreaMAVOnlineOperationCompleted, userState);
        }
        
        private void OnCreaMAVOnlineOperationCompleted(object arg) {
            if ((this.CreaMAVOnlineCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CreaMAVOnlineCompleted(this, new CreaMAVOnlineCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.pagamenti.infogroup.it/", ResponseNamespace="http://webservice.pagamenti.infogroup.it/", Use=System.Web.Services.Description.SoapBindingUse.Literal)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public rispostaMAVOnlineWS CreaMAVOnlineNoDoc(richiestaMAVOnlineWS RichiestaEmissioneMAV) {
            object[] results = this.Invoke("CreaMAVOnlineNoDoc", new object[] {
                        RichiestaEmissioneMAV});
            return ((rispostaMAVOnlineWS)(results[0]));
        }
        
        /// <remarks/>
        public void CreaMAVOnlineNoDocAsync(richiestaMAVOnlineWS RichiestaEmissioneMAV) {
            this.CreaMAVOnlineNoDocAsync(RichiestaEmissioneMAV, null);
        }
        
        /// <remarks/>
        public void CreaMAVOnlineNoDocAsync(richiestaMAVOnlineWS RichiestaEmissioneMAV, object userState) {
            if ((this.CreaMAVOnlineNoDocOperationCompleted == null)) {
                this.CreaMAVOnlineNoDocOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCreaMAVOnlineNoDocOperationCompleted);
            }
            this.InvokeAsync("CreaMAVOnlineNoDoc", new object[] {
                        RichiestaEmissioneMAV}, this.CreaMAVOnlineNoDocOperationCompleted, userState);
        }
        
        private void OnCreaMAVOnlineNoDocOperationCompleted(object arg) {
            if ((this.CreaMAVOnlineNoDocCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CreaMAVOnlineNoDocCompleted(this, new CreaMAVOnlineNoDocCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.pagamenti.infogroup.it/", ResponseNamespace="http://webservice.pagamenti.infogroup.it/", Use=System.Web.Services.Description.SoapBindingUse.Literal)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public rispostaMAVOnlineWS FindMAVOnlineByIdOperazione(ricercaMAVOnlineByOpWS RicercaMAVOnlineByOp) {
            object[] results = this.Invoke("FindMAVOnlineByIdOperazione", new object[] {
                        RicercaMAVOnlineByOp});
            return ((rispostaMAVOnlineWS)(results[0]));
        }
        
        /// <remarks/>
        public void FindMAVOnlineByIdOperazioneAsync(ricercaMAVOnlineByOpWS RicercaMAVOnlineByOp) {
            this.FindMAVOnlineByIdOperazioneAsync(RicercaMAVOnlineByOp, null);
        }
        
        /// <remarks/>
        public void FindMAVOnlineByIdOperazioneAsync(ricercaMAVOnlineByOpWS RicercaMAVOnlineByOp, object userState) {
            if ((this.FindMAVOnlineByIdOperazioneOperationCompleted == null)) {
                this.FindMAVOnlineByIdOperazioneOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFindMAVOnlineByIdOperazioneOperationCompleted);
            }
            this.InvokeAsync("FindMAVOnlineByIdOperazione", new object[] {
                        RicercaMAVOnlineByOp}, this.FindMAVOnlineByIdOperazioneOperationCompleted, userState);
        }
        
        private void OnFindMAVOnlineByIdOperazioneOperationCompleted(object arg) {
            if ((this.FindMAVOnlineByIdOperazioneCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FindMAVOnlineByIdOperazioneCompleted(this, new FindMAVOnlineByIdOperazioneCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapRpcMethodAttribute("", RequestNamespace="http://webservice.pagamenti.infogroup.it/", ResponseNamespace="http://webservice.pagamenti.infogroup.it/", Use=System.Web.Services.Description.SoapBindingUse.Literal)]
        [return: System.Xml.Serialization.XmlElementAttribute("return")]
        public rispostaMAVOnlineWS FindMAVOnlineByNumeroMAV(ricercaMAVOnlineByMAVWS RicercaMAVOnlineByMAV) {
            object[] results = this.Invoke("FindMAVOnlineByNumeroMAV", new object[] {
                        RicercaMAVOnlineByMAV});
            return ((rispostaMAVOnlineWS)(results[0]));
        }
        
        /// <remarks/>
        public void FindMAVOnlineByNumeroMAVAsync(ricercaMAVOnlineByMAVWS RicercaMAVOnlineByMAV) {
            this.FindMAVOnlineByNumeroMAVAsync(RicercaMAVOnlineByMAV, null);
        }
        
        /// <remarks/>
        public void FindMAVOnlineByNumeroMAVAsync(ricercaMAVOnlineByMAVWS RicercaMAVOnlineByMAV, object userState) {
            if ((this.FindMAVOnlineByNumeroMAVOperationCompleted == null)) {
                this.FindMAVOnlineByNumeroMAVOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFindMAVOnlineByNumeroMAVOperationCompleted);
            }
            this.InvokeAsync("FindMAVOnlineByNumeroMAV", new object[] {
                        RicercaMAVOnlineByMAV}, this.FindMAVOnlineByNumeroMAVOperationCompleted, userState);
        }
        
        private void OnFindMAVOnlineByNumeroMAVOperationCompleted(object arg) {
            if ((this.FindMAVOnlineByNumeroMAVCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FindMAVOnlineByNumeroMAVCompleted(this, new FindMAVOnlineByNumeroMAVCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservice.pagamenti.infogroup.it/")]
    public partial class richiestaMAVOnlineWS {
        
        private string capDebitoreField;
        
        private string causalePagamentoField;
        
        private string cellulareDebitoreField;
        
        private string codiceDebitoreField;
        
        private string codiceEnteField;
        
        private string codiceFiscaleDebitoreField;
        
        private string cognomeORagioneSocialeDebitoreField;
        
        private string emailDebitoreField;
        
        private string frazioneDebitoreField;
        
        private string idOperazioneField;
        
        private string importoPagamentoInCentesimiField;
        
        private string indirizzoDebitoreField;
        
        private string informazioniAggiuntiveRichiestaField;
        
        private string localitaDebitoreField;
        
        private string nazioneDebitoreField;
        
        private string nomeDebitoreField;
        
        private string provinciaDebitoreField;
        
        private string scadenzaPagamentoField;
        
        private string tipoPagamentoField;
        
        private string userNameField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string capDebitore {
            get {
                return this.capDebitoreField;
            }
            set {
                this.capDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string causalePagamento {
            get {
                return this.causalePagamentoField;
            }
            set {
                this.causalePagamentoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string cellulareDebitore {
            get {
                return this.cellulareDebitoreField;
            }
            set {
                this.cellulareDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codiceDebitore {
            get {
                return this.codiceDebitoreField;
            }
            set {
                this.codiceDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codiceEnte {
            get {
                return this.codiceEnteField;
            }
            set {
                this.codiceEnteField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codiceFiscaleDebitore {
            get {
                return this.codiceFiscaleDebitoreField;
            }
            set {
                this.codiceFiscaleDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string cognomeORagioneSocialeDebitore {
            get {
                return this.cognomeORagioneSocialeDebitoreField;
            }
            set {
                this.cognomeORagioneSocialeDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string emailDebitore {
            get {
                return this.emailDebitoreField;
            }
            set {
                this.emailDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string frazioneDebitore {
            get {
                return this.frazioneDebitoreField;
            }
            set {
                this.frazioneDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string idOperazione {
            get {
                return this.idOperazioneField;
            }
            set {
                this.idOperazioneField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string importoPagamentoInCentesimi {
            get {
                return this.importoPagamentoInCentesimiField;
            }
            set {
                this.importoPagamentoInCentesimiField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string indirizzoDebitore {
            get {
                return this.indirizzoDebitoreField;
            }
            set {
                this.indirizzoDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string informazioniAggiuntiveRichiesta {
            get {
                return this.informazioniAggiuntiveRichiestaField;
            }
            set {
                this.informazioniAggiuntiveRichiestaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string localitaDebitore {
            get {
                return this.localitaDebitoreField;
            }
            set {
                this.localitaDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string nazioneDebitore {
            get {
                return this.nazioneDebitoreField;
            }
            set {
                this.nazioneDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string nomeDebitore {
            get {
                return this.nomeDebitoreField;
            }
            set {
                this.nomeDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string provinciaDebitore {
            get {
                return this.provinciaDebitoreField;
            }
            set {
                this.provinciaDebitoreField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string scadenzaPagamento {
            get {
                return this.scadenzaPagamentoField;
            }
            set {
                this.scadenzaPagamentoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string tipoPagamento {
            get {
                return this.tipoPagamentoField;
            }
            set {
                this.tipoPagamentoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string userName {
            get {
                return this.userNameField;
            }
            set {
                this.userNameField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservice.pagamenti.infogroup.it/")]
    public partial class ricercaMAVOnlineByMAVWS {
        
        private string codiceEnteField;
        
        private string informazioniAggiuntiveRichiestaField;
        
        private string numeroMAVField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codiceEnte {
            get {
                return this.codiceEnteField;
            }
            set {
                this.codiceEnteField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string informazioniAggiuntiveRichiesta {
            get {
                return this.informazioniAggiuntiveRichiestaField;
            }
            set {
                this.informazioniAggiuntiveRichiestaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string numeroMAV {
            get {
                return this.numeroMAVField;
            }
            set {
                this.numeroMAVField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservice.pagamenti.infogroup.it/")]
    public partial class ricercaMAVOnlineByOpWS {
        
        private string codiceEnteField;
        
        private string idOperazioneField;
        
        private string informazioniAggiuntiveRichiestaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codiceEnte {
            get {
                return this.codiceEnteField;
            }
            set {
                this.codiceEnteField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string idOperazione {
            get {
                return this.idOperazioneField;
            }
            set {
                this.idOperazioneField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string informazioniAggiuntiveRichiesta {
            get {
                return this.informazioniAggiuntiveRichiestaField;
            }
            set {
                this.informazioniAggiuntiveRichiestaField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.7.3190.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://webservice.pagamenti.infogroup.it/")]
    public partial class rispostaMAVOnlineWS {
        
        private string codiceRisultatoField;
        
        private string descrizioneRisultatoField;
        
        private string descrizioneTecnicaRisultatoField;
        
        private string informazioniAggiuntiveRispostaField;
        
        private string numeroMAVField;
        
        private string pdfDocumentoField;
        
        private richiestaMAVOnlineWS richiestaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string codiceRisultato {
            get {
                return this.codiceRisultatoField;
            }
            set {
                this.codiceRisultatoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string descrizioneRisultato {
            get {
                return this.descrizioneRisultatoField;
            }
            set {
                this.descrizioneRisultatoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string descrizioneTecnicaRisultato {
            get {
                return this.descrizioneTecnicaRisultatoField;
            }
            set {
                this.descrizioneTecnicaRisultatoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string informazioniAggiuntiveRisposta {
            get {
                return this.informazioniAggiuntiveRispostaField;
            }
            set {
                this.informazioniAggiuntiveRispostaField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string numeroMAV {
            get {
                return this.numeroMAVField;
            }
            set {
                this.numeroMAVField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string pdfDocumento {
            get {
                return this.pdfDocumentoField;
            }
            set {
                this.pdfDocumentoField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public richiestaMAVOnlineWS richiesta {
            get {
                return this.richiestaField;
            }
            set {
                this.richiestaField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    public delegate void CreaMAVOnlineCompletedEventHandler(object sender, CreaMAVOnlineCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CreaMAVOnlineCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal CreaMAVOnlineCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public rispostaMAVOnlineWS Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((rispostaMAVOnlineWS)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    public delegate void CreaMAVOnlineNoDocCompletedEventHandler(object sender, CreaMAVOnlineNoDocCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CreaMAVOnlineNoDocCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal CreaMAVOnlineNoDocCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public rispostaMAVOnlineWS Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((rispostaMAVOnlineWS)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    public delegate void FindMAVOnlineByIdOperazioneCompletedEventHandler(object sender, FindMAVOnlineByIdOperazioneCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FindMAVOnlineByIdOperazioneCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FindMAVOnlineByIdOperazioneCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public rispostaMAVOnlineWS Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((rispostaMAVOnlineWS)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    public delegate void FindMAVOnlineByNumeroMAVCompletedEventHandler(object sender, FindMAVOnlineByNumeroMAVCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.7.3190.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FindMAVOnlineByNumeroMAVCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FindMAVOnlineByNumeroMAVCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public rispostaMAVOnlineWS Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((rispostaMAVOnlineWS)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591