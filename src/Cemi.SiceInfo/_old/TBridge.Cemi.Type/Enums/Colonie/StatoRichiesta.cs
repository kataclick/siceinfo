﻿namespace TBridge.Cemi.Type.Enums.Colonie
{
    public enum StatoRichiesta
    {
        DAGESTIRE = 0,
        FISSATOCOLLOQUIO,
        VALUTATO,
        EFFETTUATAPROPOSTA,
        PROPOSTAACCETTATA,
        PROPOSTANONACCETTATA,
        RESPINTA
    }
}