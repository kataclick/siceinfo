namespace TBridge.Cemi.Type.Enums.Colonie
{
    public enum StatoDomandaACE
    {
        DaValutare = 0,
        Confermata,
        Rifiutata
    }
}