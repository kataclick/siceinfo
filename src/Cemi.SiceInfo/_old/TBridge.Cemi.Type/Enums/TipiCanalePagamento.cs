﻿namespace TBridge.Cemi.Type.Enums
{
    public enum TipiCanalePagamento
    {
        BollettinoFreccia = 1,
        MavPopolareSondrio = 2,
        MavBancaProxima = 3
    }
}