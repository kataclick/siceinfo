﻿namespace TBridge.Cemi.Type.Enums
{
    public enum TipoRecapito
    {
        Legale = 1,
        Amministrativo = 2,
        Corrispondenza = 3
    }
}