namespace TBridge.Cemi.Type.Enums.IscrizioneCe
{
    public enum StatoDomanda
    {
        Accettata = 0,
        Rifiutata,
        DaValutare,
        SospesaAttesaIntegrazione,
        SospesaDebiti
    }
}