﻿namespace TBridge.Cemi.Type.Enums.IscrizioneCe
{
    public enum TipoOrdinamento
    {
        DataRichiestaIscrizioneCrescente = 0,
        DataRichiestaIscrizioneDecrescente,
        DataInserimentoCrescente,
        DataInserimentoDecrescente,
        RagioneSocialeCrescente,
        RagioneSocialeDecrescente
    }
}