namespace TBridge.Cemi.Type.Enums.GestioneUtenti
{
    public enum TipiUtenti
    {
        Lavoratore,
        Impresa,
        Dipendente,
        Ospite,
        Fornitore,
        Sindacalista,
        ASL,
        Committente,
        Ispettore,
        Esattore,
        Consulente,
        CassaEdile
    }

    //TODO spostare in un progetto a parte
    public enum RuoliPredefiniti
    {
        LavoratoreSMS,
        Lavoratore,
        Impresa,
        Consulente,
        Dipendente,
        Fornitore,
        Ospite,
        UtenteDisabilitato,
        UtentePubblico,
        InApprovazione,
        Sindacalista,
        ASL,
        Committente
    }

    public enum FunzionalitaPredefinite
    {
        //Funzionalit� modulo SMSInfo
        SMSInfoGestisciAccount,
        SMSInfoGestisciSMSScadenza,
        SMSInfoGestisciSMSImmediati,
        SMSInfoGestisciErrori,
        SMSInfoGestisciSMSInviati,

        //Funzionalit� modulo Imprese Regolari
        ImpreseRegolariRicercaLista,
        ImpreseRegolariModificaLista,
        ImpreseRegolariApprovaLista,
        ImpreseRegolariImpostaParametri,
        ImpreseRegolariSegnalaAnomalie,
        ImpreseRegolariGestisciAnomalie,

        //Funzionalit� modulo subappalti
        SubappaltiRicerca,
        SubappaltiStorico,

        //Funionalit� activity tracking
        ActivityTrackingRicerca,

        //Funzionalit� gestione utenti
        GestioneUtentiGestisciUtenti,
        GestioneUtentiRegistrazione,
        GestioneUtentiGestionePIN,
        GestioneUtentiGestionePINLavoratori,
        GestioneUtentiGestionePINConsulenti,

        //Tute scarpe
        TuteScarpeGestioneFabbisogno,
        TuteScarpeGestioneFabbisogniConfermati,
        TuteScarpeGestioneOrdini,
        TuteScarpeGestioneOrdiniConfermati,
        TuteScarpeImmissioneFatture,
        TuteScarpeGestioneFatture,
        TuteScarpeStatistiche,
        TuteScarpeGestioneConsulenti,
        TuteScarpeGestioneSms,
        TuteScarpeSelezioneTaglie,
        TuteScarpeGestioneScadenze,

        //Notifiche [E' stata raddoppiata la N perch� Not � una parola chiave e messa all'inizio crea un errore sul parser dell'enterpriselibrary]
        NNotificheGestione,
        NNotificheAnomalie,

        InfoLavoratore,
        InfoImpresa,

        VodElencoImpreseConsulenti,
        VodElencoImpreseMedie,

        IISLogStatistiche,

        //Cantieri,
        CantieriGestione,
        CantieriProgrammazioneVisualizzazione,
        CantieriProgrammazioneGestione,
        CantieriConsuPrev,
        CantieriConsuPrevRUI,
        CantieriModifica,
        CantieriModificaIspettori,
        CantieriGestioneZone,
        CantieriStatistichePerIspettore,
        CantieriStatistichePerIspettoreRUI,
        CantieriStatisticheGenerali,
        CantieriLettere,
        CantieriSegnalazione,
        CantieriAssegnazione,
        CantieriGestioneGruppi,
        CantieriGestioneAttivita,
        CantieriGenerazioneDaNotifiche,
        CantieriEstrazioneLodi,

        //Colonie
        ColonieGestioneDomande,
        ColonieGestioneMatrice,
        ColonieGestioneVacanze,
        ColonieGestioneExport,
        ColonieSchedaPartecipante,
        ColonieCalcoloCosti,
        ColonieInserimentoDomandeACE,
        ColonieGestionePrenotazioni,
        ColonieGestioneDomandeACE,
        ColonieComunicazioniACE,
        ColonieRichiestePersonale,
        ColonieGestioneRichiestePersonale,
        ColonieEstrazioneINAZ,

        //Cpt
        CptInserimentoAggiornamentoNotifica,
        CptAnnullamentoNotifica,
        CptRicerca,
        CptStatistiche,
        CptAttivitaASLInserimento,
        CptAttivitaASLVisualizzazione,
        CptAttivitaCPTInserimento,
        CptAttivitaCPTVisualizzazione,
        CptAttivitaDPLInserimento,
        CptAttivitaDPLVisualizzazione,
        CptAttivitaASLERSLTInserimento,
        CptAttivitaASLERSLTVisualizzazione,
        CptAttivitaCassaEdileInserimento,
        CptAttivitaCassaEdileVisualizzazione,
        CptInserimentoAggiornamentoNotificaLodi,
        CptRicercaLodi,
        CptEstrazioneExcelCantieri,
        CptEstrazioneBrescia,
        CptEstrazioneAssimpredil,

        //Iscrizione CE
        IscrizioneCEGestioneDomande,
        IscrizioneCEIscrizione,

        // Deleghe
        DelegheGestione,
        DelegheConferma,
        DelegheGestioneCE,
        DelegheSblocco,
        DelegheEstrazioneMetadati,
        DelegheVisioneCE,

        // DURC
        DURCVisualizzaLista,
        DurcSollecito,
        DurcSollecitiRicerca,

        // Prestazioni
        PrestazioniCompilazioneDomanda,
        PrestazioniCompilazioneDomandaFast,
        PrestazioniGestioneDomande,
        PrestazioniGestioneDomandeLavoratore,
        PrestazioniGestioneDomandeAdmin,

        // InfoSindacati
        DownloadInfoSindacati,
        DownloadLavoratoriAttivi,
        DownloadDelegaCartacea,

        // Attestato di regolarit�
        AttestatoRegolaritaRichiesta,
        AttestatoRegolaritaGestioneCE,
        AttestatoRegolaritaRiepilogoAttestati,

        // Corsi
        CorsiGestione,
        CorsiVisualizzazione,
        CorsiIscrizioneImpresa,
        CorsiIscrizioneConsulente,
        CorsiIscrizioneLavoratore,
        CorsiEstrazioneFormedil,
        CorsiStatisticheAttestati,
        CorsiGestioneDomandePrestazione,
        CorsiPrestazioniDTA,

        // BollettinoFreccia
        BollettinoFrecciaRichiesta,
        BollettinoFrecciaStatistiche,

        // Accesso ai Cantieri
        AccessoCantieriGestioneCantiere,

        //AccessoCantieriRiepilogoCantieri,
        AccessoCantieriPerImpresa,
        AccessoCantieriAmministrazione,
        AccessoCantieriControlli,
        AccessoCantieriPerCommittente,

        // Iscrizione Lavoratori
        IscrizioneLavoratoriIscrizione,
        IscrizioneLavoratoriGestioneCE,
        IscrizioneLavoratoriIscrizioneSintesiInterfaccia,

        //Cruscotto
        CruscottoPrestazioni,
        CruscottoCrc,
        CruscottoImprese,
        CruscottoMassaSalariale,

        Impersonate,
        UtenteDisabilitato,

        ImpreseRichiestaVariazioneRecapiti,
        ImpreseGestioneStato,
        ImpreseGestioneRichiesteCambioStato,
        ImpreseQuestionario,
        ImpreseQuestionarioConsulenti,

        //MalattiaTelematica
        MalattiaTelematicaRicercaAssenzeImpresa,
        MalattiaTelematicaRicercaAssenzeBackOffice,
        MalattiaTelematicaUploadCertificati,
        MalattiaTelematicaEstrattoConto,
        MalattiaTelematicaRicercaMessaggi,
        MalattiaTelematicaRicercaErrori,
        MalattiaTelematicaStatisticaLiquidazioni,
        MalattiaTelematicaStatisticaEvase,
        MalattiaTelematicaRicercaAssenzeInAttesa,

        NotifichePreliminariRicercaNotifiche,
        NotifichePreliminariRicercaCantieri,
        NotifichePreliminariRicercaMappa,
        NotifichePreliminariEstrazioneExcelCantieri,
        NotifichePreliminariEstrazioneBrescia,
        NotifichePreliminariEstrazioneAssimpredil,
        NotifichePreliminariDatiPersone,

        // CigoTelematica
        CigoTelematicaEstrattoConto,
        CigoTelematicaGestioneImpresa,
        CigoTelematicaGestioneBackOffice,

        // SOLDO
        SOLDOOperatore,
        SOLDOAmministratoreApplicativo,
        SOLDOAmministratoreTecnico,
        SOLDOImpresa,
        SOLDOConsulente
    }
}