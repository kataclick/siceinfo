namespace TBridge.Cemi.Type.Enums.GestioneUtenti
{
    public enum ErroriRegistrazione
    {
        RegistrazioneEffettuata,
        ChallengeNonPassato,
        LoginPresente,
        RegistrazioneGiaPresente,
        EntitaNonPresente, //inteso come lavoratore,impresa,consulente
        Errore, //Errore generico, accesso db
        LoginNonCorretta,
        PasswordNonCorretta,
        RuoloNonAssociato
    }
}