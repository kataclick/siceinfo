﻿namespace TBridge.Cemi.Type.Enums.TuteScarpe
{
    public enum StatoConsegna
    {
        Consegnato,
        Avvisato,
        ConcordatoConsegna,
        Giacenza
    }
}