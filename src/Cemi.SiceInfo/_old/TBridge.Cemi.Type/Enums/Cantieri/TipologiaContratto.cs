namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum TipologiaContratto
    {
        ContrattoEdile = 0,
        AltroContratto
    }
}