namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum StatoAssegnazioneFiltro
    {
        Tutti = 0,
        Assegnati,
        Rifiutati,
        NonAssegnati
    }
}