namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum StatoRapportoIspezioneFiltro
    {
        Tutti = 0,
        ConRapportoIspezione,
        SenzaRapportoIspezione
    }
}