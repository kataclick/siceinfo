namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum EsitoIspezione
    {
        Indefinita = 0,
        Positiva,
        Negativa
    }
}