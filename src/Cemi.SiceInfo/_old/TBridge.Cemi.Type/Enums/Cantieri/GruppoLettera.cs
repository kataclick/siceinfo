namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum GruppoLettera
    {
        VerificaInCorso = 1,
        IrregolaritaRiscontrate,
        IrregolaritaRiscontrateConAppuntamento,
        EsitoPositivo,
        EsitoPositivoConSubappaltatrici,
        IrregolaritaRiscontrateAlCommittente,
        Sindacati,
        BollinoBlu,
        IrregolaritaRiscontrateAlComune,
        IrregolaritaRiscontrateAlComune01,
        VerificaInCorsoComune
    }
}