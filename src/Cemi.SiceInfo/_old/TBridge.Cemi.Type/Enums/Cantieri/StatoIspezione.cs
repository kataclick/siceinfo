namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum StatoIspezione
    {
        InVerifica = 1,
        Conclusa
    }
}