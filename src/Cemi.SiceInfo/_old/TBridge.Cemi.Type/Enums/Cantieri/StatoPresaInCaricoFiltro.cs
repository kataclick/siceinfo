namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum StatoPresaInCaricoFiltro
    {
        Tutti = 0,
        PresiInCarico,
        NonPresiInCarico,
        Rifiutati
    }
}