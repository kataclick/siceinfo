namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum TipologiaAppalto
    {
        NonDefinito = 0,
        Pubblico,
        Privato
    }
}