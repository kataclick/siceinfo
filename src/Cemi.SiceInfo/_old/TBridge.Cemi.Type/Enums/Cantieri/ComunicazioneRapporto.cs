namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum ComunicazioneRapporto
    {
        Archivio = 0,
        UfficioDatoriDiLavoro,
        UfficioCRC
    }
}