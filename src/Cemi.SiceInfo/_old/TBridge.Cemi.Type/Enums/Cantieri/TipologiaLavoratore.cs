namespace TBridge.Cemi.Type.Enums.Cantieri
{
    public enum TipologiaLavoratore
    {
        SiceNew = 0,
        Cantieri,
        Notifiche,
        TutteLeFonti
    }
}