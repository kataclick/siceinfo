using System;

namespace TBridge.Cemi.Type.Exceptions.Notifiche
{
    public class NotificaPresenteException : Exception
    {
    }

    public class DenunciaPresenteException : Exception
    {
    }
}