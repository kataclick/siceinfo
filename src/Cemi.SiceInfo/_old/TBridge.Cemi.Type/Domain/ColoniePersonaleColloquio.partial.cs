﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class ColoniePersonaleColloquio
    {
        public DateTime DataFine => Data.AddMinutes(30);
    }
}