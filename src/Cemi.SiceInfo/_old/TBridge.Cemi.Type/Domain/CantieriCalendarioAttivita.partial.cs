﻿using System;
using System.Linq;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantieriCalendarioAttivita
    {
        private const int LUNGHEZZASTRINGADESCRIZIONE = 50;
        public int? IdCantiere { get; set; }

        public string CognomeIspettore
        {
            get
            {
                if (Ispettore != null)
                {
                    return Ispettore.Cognome;
                }
                return null;
            }
        }

        public string NomeCompletoIspettore
        {
            get
            {
                if (Ispettore != null)
                {
                    return Ispettore.NomeCompleto;
                }
                return null;
            }
        }

        public string DescrizioneAttivita
        {
            get
            {
                if (CantieriCantieri != null && CantieriCantieri.Count > 0)
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine("<b>");
                    sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                    sbDescrizione.AppendLine("</b>");
                    if (!string.IsNullOrEmpty(Descrizione))
                    {
                        sbDescrizione.AppendLine("<br />");
                        sbDescrizione.AppendLine("<br />");
                        if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                        {
                            sbDescrizione.AppendLine(string.Format("{0}...",
                                Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                        }
                        else
                        {
                            sbDescrizione.AppendLine(Descrizione);
                        }
                    }
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine(string.Format("Titolare: <b>{0}</b>", Ispettore.NomeCompleto));
                    sbDescrizione.AppendLine("<br />");

                    foreach (Ispettore ispettore in IspettoriCorrelati)
                    {
                        sbDescrizione.AppendLine(ispettore.NomeCompleto);
                        sbDescrizione.AppendLine("<br />");
                    }

                    return sbDescrizione.ToString();
                }
                if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    return string.Format("{0}...", Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3));
                }
                return Descrizione;
            }
        }

        public string DescrizioneAttivitaPerElencoCompleto
        {
            get
            {
                if (CantieriCantieri != null && CantieriCantieri.Count > 0)
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    foreach (Ispettore ispettore in IspettoriCorrelati)
                    {
                        sbDescrizione.AppendLine(ispettore.Cognome);
                        sbDescrizione.AppendLine("<br />");
                    }

                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine(string.Format("<b>{0}</b>", TipologiaAttivita));
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                    if (!string.IsNullOrEmpty(Descrizione))
                    {
                        sbDescrizione.AppendLine("<br />");
                        sbDescrizione.AppendLine("<br />");
                        if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                        {
                            sbDescrizione.AppendLine(string.Format("{0}...",
                                Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                        }
                        else
                        {
                            sbDescrizione.AppendLine(Descrizione);
                        }
                    }

                    return sbDescrizione.ToString();
                }
                else
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine(string.Format("<b>{0}</b>", TipologiaAttivita));
                    sbDescrizione.AppendLine("<br />");
                    if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(string.Format("{0}...",
                            Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(Descrizione);
                    }

                    return sbDescrizione.ToString();
                }
            }
        }

        public string DescrizioneAttivitaPerElencoCompletoNonHtml
        {
            get
            {
                if (CantieriCantieri != null && CantieriCantieri.Count > 0)
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine(CognomeIspettore);
                    foreach (Ispettore ispettore in IspettoriCorrelati)
                    {
                        sbDescrizione.AppendLine(ispettore.Cognome);
                    }

                    sbDescrizione.AppendLine();
                    sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                    if (!string.IsNullOrEmpty(Descrizione))
                    {
                        sbDescrizione.AppendLine();
                        sbDescrizione.AppendLine();
                        if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                        {
                            sbDescrizione.AppendLine(string.Format("{0}...",
                                Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                        }
                        else
                        {
                            sbDescrizione.AppendLine(Descrizione);
                        }
                    }

                    return sbDescrizione.ToString();
                }
                else
                {
                    StringBuilder sbDescrizione = new StringBuilder();
                    sbDescrizione.AppendLine(CognomeIspettore);
                    sbDescrizione.AppendLine();
                    sbDescrizione.AppendLine(TipologiaAttivita.ToString());
                    if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(string.Format("{0}...",
                            Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(Descrizione);
                    }

                    return sbDescrizione.ToString();
                }
            }
        }

        public DateTime DataFineAttivita => Data.AddHours(Durata);

        public string DescrizioneAttivitaConRapportoIspezione(string statoIspezione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (CantieriCantieri != null && CantieriCantieri.Count > 0)
            {
                sbDescrizione.AppendLine(string.Format("Stato: <b>{0}</b>", statoIspezione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<b>");
                sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                sbDescrizione.AppendLine("</b>");
                if (!string.IsNullOrEmpty(Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(string.Format("{0}...",
                            Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(Descrizione);
                    }
                }
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(string.Format("Titolare: <b>{0}</b>", Ispettore.NomeCompleto));
                sbDescrizione.AppendLine("<br />");

                foreach (Ispettore ispettore in IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.NomeCompleto);
                    sbDescrizione.AppendLine("<br />");
                }

                return sbDescrizione.ToString();
            }
            if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
            {
                sbDescrizione.AppendLine(string.Format("{0}...",
                    Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
            }
            else
            {
                sbDescrizione.AppendLine(Descrizione);
            }

            return sbDescrizione.ToString();
        }

        public string DescrizioneAttivitaConRifiuto(string motivazione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (CantieriCantieri != null && CantieriCantieri.Count > 0)
            {
                sbDescrizione.AppendLine(string.Format("Non isp: <b>{0}</b>", motivazione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<b>");
                sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                sbDescrizione.AppendLine("</b>");
                if (!string.IsNullOrEmpty(Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(string.Format("{0}...",
                            Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(Descrizione);
                    }
                }
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(string.Format("Titolare: <b>{0}</b>", Ispettore.NomeCompleto));
                sbDescrizione.AppendLine("<br />");

                foreach (Ispettore ispettore in IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.NomeCompleto);
                    sbDescrizione.AppendLine("<br />");
                }

                return sbDescrizione.ToString();
            }
            if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
            {
                sbDescrizione.AppendLine(string.Format("{0}...",
                    Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
            }
            else
            {
                sbDescrizione.AppendLine(Descrizione);
            }

            return sbDescrizione.ToString();
        }

        public string DescrizioneAttivitaPerElencoCompletoConRapportoIspezione(string statoIspezione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (CantieriCantieri != null && CantieriCantieri.Count > 0)
            {
                foreach (Ispettore ispettore in IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.Cognome);
                    sbDescrizione.AppendLine("<br />");
                }

                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(string.Format("<b>{0}</b>", TipologiaAttivita));
                sbDescrizione.AppendLine(string.Format("<br />Stato: <b>{0}</b>", statoIspezione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                if (!string.IsNullOrEmpty(Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(string.Format("{0}...",
                            Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(Descrizione);
                    }
                }
            }
            else
            {
                sbDescrizione.AppendLine(string.Format("<b>{0}</b>", TipologiaAttivita));
                sbDescrizione.AppendLine("<br />");
                if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    sbDescrizione.AppendLine(string.Format("{0}...",
                        Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                }
                else
                {
                    sbDescrizione.AppendLine(Descrizione);
                }
            }

            return sbDescrizione.ToString();
        }

        public string DescrizioneAttivitaPerElencoCompletoConRifiuto(string motivazione)
        {
            StringBuilder sbDescrizione = new StringBuilder();

            if (CantieriCantieri != null && CantieriCantieri.Count > 0)
            {
                foreach (Ispettore ispettore in IspettoriCorrelati)
                {
                    sbDescrizione.AppendLine(ispettore.Cognome);
                    sbDescrizione.AppendLine("<br />");
                }

                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(string.Format("<b>{0}</b>", TipologiaAttivita));
                sbDescrizione.AppendLine(string.Format("<br />Non isp: <b>{0}</b>", motivazione));
                sbDescrizione.AppendLine("<br />");
                sbDescrizione.AppendLine(CantieriCantieri.First().IndirizzoCompletoLimitato);
                if (!string.IsNullOrEmpty(Descrizione))
                {
                    sbDescrizione.AppendLine("<br />");
                    sbDescrizione.AppendLine("<br />");
                    if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                    {
                        sbDescrizione.AppendLine(string.Format("{0}...",
                            Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                    }
                    else
                    {
                        sbDescrizione.AppendLine(Descrizione);
                    }
                }
            }
            else
            {
                sbDescrizione.AppendLine(string.Format("<b>{0}</b>", TipologiaAttivita));
                sbDescrizione.AppendLine("<br />");
                if (Descrizione.Length > LUNGHEZZASTRINGADESCRIZIONE)
                {
                    sbDescrizione.AppendLine(string.Format("{0}...",
                        Descrizione.Substring(0, LUNGHEZZASTRINGADESCRIZIONE - 3)));
                }
                else
                {
                    sbDescrizione.AppendLine(Descrizione);
                }
            }

            return sbDescrizione.ToString();
        }

        public bool InCarico(int idIspettore)
        {
            if (IdIspettore == idIspettore)
            {
                return true;
            }
            return false;
        }
    }
}