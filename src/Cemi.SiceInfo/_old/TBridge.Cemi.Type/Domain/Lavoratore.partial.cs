﻿namespace TBridge.Cemi.Type.Domain
{
    public partial class Lavoratore
    {
        public bool ServizioSMS { get; set; }

        public string IBAN
        {
            get
            {
                string iban = string.Empty;
                if (!string.IsNullOrEmpty(codicePaese))
                {
                    iban = string.Format("{0}{1}{2}{3}{4}{5}", codicePaese, codiceCINPaese, codiceCIN, codiceABI,
                        codiceCAB, numeroCC);
                }
                return iban;
            }
        }
    }
}