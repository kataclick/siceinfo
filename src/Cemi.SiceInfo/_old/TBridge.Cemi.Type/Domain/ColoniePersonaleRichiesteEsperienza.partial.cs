﻿namespace TBridge.Cemi.Type.Domain
{
    public partial class ColoniePersonaleRichiesteEsperienza
    {
        public string Descrizione => string.Format("Dal {2} al {3} {0} presso {1}", Mansione, Impresa,
            Dal.ToString("dd/MM/yyyy"), Al.HasValue ? Al.Value.ToString("dd/MM/yyyy") : string.Empty);
    }
}