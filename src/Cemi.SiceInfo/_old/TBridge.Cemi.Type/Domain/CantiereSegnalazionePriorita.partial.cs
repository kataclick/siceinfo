﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantiereSegnalazionePriorita
    {
        public override string ToString()
        {
            return Descrizione;
        }
    }
}