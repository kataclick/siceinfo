﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class Ispettore
    {
        public string NomeCompleto => string.Format("{0} {1}", Cognome, Nome);
    }
}