﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class Cantiere
    {
        private const int LUNGHEZZAINDIRIZZOLIMITATO = 50;

        public string IndirizzoCompleto => string.Format("{0} {1}, {2} {3} {4}",
            Indirizzo,
            Numero,
            Cap,
            Comune,
            Provincia);

        public string IndirizzoCompletoLimitato
        {
            get
            {
                string indirizzoCompleto = string.Format("{0} {1}, {2} {3} {4}",
                    Indirizzo,
                    Numero,
                    Cap,
                    Comune,
                    Provincia);

                if (indirizzoCompleto.Length > LUNGHEZZAINDIRIZZOLIMITATO)
                {
                    indirizzoCompleto = indirizzoCompleto.Substring(0, LUNGHEZZAINDIRIZZOLIMITATO - 3);
                    return string.Format("{0}...", indirizzoCompleto);
                }
                return indirizzoCompleto;
            }
        }
    }
}