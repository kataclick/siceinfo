﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class ColonieTurno
    {
        private string descrizioneAppoggio;

        public string Descrizione =>
            string.Format("{0} {1}", ColonieDestinazione.ColonieTipoDestinazione.Descrizione, Progressivo);

        public string DescrizioneAppoggio
        {
            get
            {
                if (ColonieDestinazione != null && ColonieDestinazione.ColonieTipoDestinazione != null)
                {
                    return string.Format("{0} {1}", ColonieDestinazione.ColonieTipoDestinazione.Descrizione,
                        Progressivo);
                }

                return descrizioneAppoggio;
            }
            set => descrizioneAppoggio = value;
        }
    }
}