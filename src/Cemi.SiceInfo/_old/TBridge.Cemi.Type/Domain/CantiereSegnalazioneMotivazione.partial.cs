﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantiereSegnalazioneMotivazione
    {
        public override string ToString()
        {
            return Descrizione;
        }
    }
}