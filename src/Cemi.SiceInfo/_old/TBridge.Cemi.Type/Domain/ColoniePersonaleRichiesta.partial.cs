﻿using System;
using System.Text;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class ColoniePersonaleRichiesta
    {
        public string NomeCompleto => string.Format("{0} {1}", Cognome, Nome);

        public DateTime? DataColloquioInizio
        {
            get
            {
                foreach (ColoniePersonaleColloquio coll in ColoniePersonaleColloquio)
                {
                    return coll.Data;
                }

                return null;
            }
        }

        public DateTime? DataColloquioFine
        {
            get
            {
                foreach (ColoniePersonaleColloquio coll in ColoniePersonaleColloquio)
                {
                    return coll.DataFine;
                }

                return null;
            }
        }

        public string ElencoMansioni
        {
            get
            {
                StringBuilder mansioni = new StringBuilder();

                foreach (ColoniePersonaleColloquio coll in ColoniePersonaleColloquio)
                {
                    foreach (ColoniePersonaleMansione mans in coll.ColoniePersonaleMansioni)
                    {
                        mansioni.AppendLine(mans.Descrizione);
                    }
                }

                return mansioni.ToString();
            }
        }
    }
}