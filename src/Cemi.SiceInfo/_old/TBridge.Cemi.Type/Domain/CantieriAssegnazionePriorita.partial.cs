﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantieriAssegnazionePriorita
    {
        public override string ToString()
        {
            return Descrizione;
        }
    }
}