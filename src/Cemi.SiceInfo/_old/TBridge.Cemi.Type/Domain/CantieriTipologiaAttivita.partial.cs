﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantieriTipologiaAttivita
    {
        public override string ToString()
        {
            return Descrizione;
        }
    }
}