﻿using System;

namespace TBridge.Cemi.Type.Domain
{
    [Serializable]
    public partial class CantieriAssegnazioneMotivazione
    {
        public override string ToString()
        {
            return Descrizione;
        }
    }
}