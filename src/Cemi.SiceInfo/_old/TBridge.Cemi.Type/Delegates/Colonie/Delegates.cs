using TBridge.Cemi.Type.Entities.Colonie;

namespace TBridge.Cemi.Type.Delegates.Colonie
{
    public delegate void DomandaACESelectedEventHandler(DomandaACE domanda);

    public delegate void LavoratoreACESelectedEventHandler(LavoratoreACE lavoratore);

    public delegate void LavoratoreACEConfirmedEventHandler(LavoratoreACE lavoratore);

    public delegate void LavoratoreACEModifiedEventHandler(LavoratoreACE lavoratore);

    public delegate void FamiliareACEModifiedEventHandler(FamiliareACE familiare);

    public delegate void FamiliareACEConfirmedEventHandler(FamiliareACE lavoratore);

    public delegate void CancelledEventHandler();

    public delegate void CassaEdileSelectEventHandler(CassaEdile cassaEdile);
}