using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Delegates.Cantieri
{
    public delegate void CantieriSelectedEventHandler(Cantiere cantiere);

    public delegate void CommittentiSelectedEventHandler(Committente committente);

    public delegate void ImpreseSelectedEventHandler(Impresa impresa);

    public delegate void LavoratoriSelectedEventHandler(Lavoratore lavoratore);

    public delegate void IspezioneSelectedEventHandler(RapportoIspezione ispezione);

    public delegate void CantiereGeneratoEventHandler();
}