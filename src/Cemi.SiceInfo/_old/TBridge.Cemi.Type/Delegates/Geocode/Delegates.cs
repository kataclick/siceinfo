using TBridge.Cemi.Type.Entities.Geocode;

namespace TBridge.Cemi.Type.Delegates.Geocode
{
    public delegate void IndirizzoSelectedEventHandler(Indirizzo indirizzo);
}