using TBridge.Cemi.Type.Entities.Deleghe;

namespace TBridge.Cemi.Type.Delegates.Deleghe
{
    public delegate void LavoratoriSelectedEventHandler(Lavoratore lavoratore);

    public delegate void DelegaSelectedEventHandler(Delega delega);

    public delegate void DelegaSearchedEventHandler();
}