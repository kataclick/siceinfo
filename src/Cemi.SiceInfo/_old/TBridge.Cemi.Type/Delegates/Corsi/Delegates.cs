using TBridge.Cemi.Type.Entities.Corsi;

namespace TBridge.Cemi.Type.Delegates.Corsi
{
    public delegate void ProgrammazioneSelectedEventHandler(int idProgrammazione, int idProgrammazioneModulo);

    public delegate void LavoratoreSelectedEventHandler(Lavoratore lavoratore);

    public delegate void LavoratoreNuovoEventHandler();

    public delegate void LavoratoreRicercaEventHandler();

    public delegate void ImpresaSelectedEventHandler(Impresa impresa);

    public delegate void ImpresaNuovaEventHandler();

    public delegate void RicercaLavoratoreSelectedEventHandler(int idCorsiPrestazioneDomanda);
}