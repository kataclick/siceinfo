using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Delegates.Prestazioni
{
    public delegate void FamiliareSelectedEventHandler(Familiare familiare);

    public delegate void DocumentoSelectedEventHandler(Documento documento);

    public delegate void AssociazioneEffettuataEventHandler();

    public delegate void FatturaSelectedEventHandler(int idFattura);

    public delegate void DomandaTemporaneaSelectedEventHandler(int idDomanda);

    public delegate void LavoratoreSelectedEventHandler(Lavoratore lavoratore);

    public delegate void LavoratoreNuovoEventHandler();
}