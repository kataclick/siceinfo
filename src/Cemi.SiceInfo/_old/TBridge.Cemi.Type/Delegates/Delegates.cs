using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Type.Delegates
{
    public delegate void ImpresaSelectedEventHandler(int idImpresa, string codiceRagioneSociale);

    public delegate void RichiestaVariazioneStatoImpresaSelectedEventHandler(int idRichiestaVariazioneStatoImpresa);

    public delegate void RicercaRichiesteVariazioneStatoImpresaSelectedEventHandler();

    public delegate void CommittenteSelectedEventHandler(Committente committente);
}