using System;

namespace TBridge.Cemi.Type.Filters.Colonie
{
    public class LavoratoreACEFilter
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string IdCassaEdile { get; set; }
    }
}