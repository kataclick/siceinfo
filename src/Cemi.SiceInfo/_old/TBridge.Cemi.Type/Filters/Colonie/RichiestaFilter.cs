﻿using TBridge.Cemi.Type.Enums.Colonie;

namespace TBridge.Cemi.Type.Filters.Colonie
{
    public class RichiestaFilter
    {
        public int? IdRichiesta { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public int? IdMansione { get; set; }

        public int? IdMansioneAssegnata { get; set; }

        public int? IdMansioneColloquio { get; set; }

        public int? IdTurno { get; set; }

        public int? IdTurnoAssegnato { get; set; }

        public StatoRichiesta? Stato { get; set; }

        public int? IdValutazione { get; set; }

        public bool? EsperienzaCE { get; set; }

        public string Note { get; set; }

        public int? IdVacanza { get; set; }
    }
}