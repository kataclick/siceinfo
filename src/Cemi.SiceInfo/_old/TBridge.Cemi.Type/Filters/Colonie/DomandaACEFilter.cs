using TBridge.Cemi.Type.Enums.Colonie;

namespace TBridge.Cemi.Type.Filters.Colonie
{
    public class DomandaACEFilter
    {
        public int? IdDomanda { get; set; }

        public int? IdVacanza { get; set; }

        public string IdCassaEdile { get; set; }

        public int? IdTurno { get; set; }

        public string CognomeLavoratore { get; set; }

        public string CognomeBambino { get; set; }

        public StatoDomandaACE? Stato { get; set; }
    }
}