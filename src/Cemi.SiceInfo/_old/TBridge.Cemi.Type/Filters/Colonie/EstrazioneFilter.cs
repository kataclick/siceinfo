﻿using System;

namespace TBridge.Cemi.Type.Filters.Colonie
{
    public class EstrazioneFilter
    {
        public int IdVacanza { get; set; }

        public DateTime? DataAssunzioneDal { get; set; }

        public DateTime? DataAssunzioneAl { get; set; }

        public int? IdMansione { get; set; }
    }
}