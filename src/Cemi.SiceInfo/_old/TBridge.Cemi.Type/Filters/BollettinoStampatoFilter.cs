﻿using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Filters
{
    public class BollettinoStampatoFilter
    {
        public int? Anno { get; set; }
        public int? Mese { get; set; }
        public string RagioneSociale { get; set; }
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public TipiCanalePagamento? CanalePagamento { set; get; }
        public int? IdImpresa { get; set; }
    }
}