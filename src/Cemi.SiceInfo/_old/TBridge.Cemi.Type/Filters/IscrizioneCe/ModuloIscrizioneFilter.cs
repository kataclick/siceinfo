using System;
using TBridge.Cemi.Type.Enums.IscrizioneCe;

namespace TBridge.Cemi.IscrizioneCE.Type.Filters
{
    public class ModuloIscrizioneFilter
    {
        public int? IdConsulente { get; set; }

        public StatoDomanda? StatoDomanda { get; set; }

        public bool? Confermate { get; set; }

        public string RagioneSociale { get; set; }

        public bool? CompilateConsulente { get; set; }

        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public DateTime? DataRichiestaDal { get; set; }

        public DateTime? DataRichiestaAl { get; set; }
    }
}