﻿namespace TBridge.Cemi.Type.Filters.AnagraficaCondivisa
{
    public class ImpresaFilter
    {
        public int? CodiceCassaEdile { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }
    }
}