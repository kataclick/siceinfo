﻿using System;

namespace TBridge.Cemi.Type.Filters.AnagraficaCondivisa
{
    public class LavoratoreFilter
    {
        public int? CodiceCassaEdile { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }
    }
}