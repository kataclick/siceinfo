﻿using System;

namespace TBridge.Cemi.Type.Filters.VCOD
{
    [Serializable]
    public class ImpreseFilter
    {
        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
        public string Fascia { get; set; }
        public int? IdConsulente { get; set; }
        public int? IdImpresa { get; set; }
        public int? MediaPartTime { get; set; }
        public int? NumeroOperai { get; set; }
        public string Stato { get; set; }
        public string IdEsattore { get; set; }
        public string EsitoControlloDenuncia { get; set; }
        public double? OreFerie { set; get; }
        public double? OrePermessiRetribuiti { set; get; }
        public double? OreCigStraordinaria { set; get; }
        public double? OreCigDeroga { set; get; }
        public double? OreCigo { set; get; }
        public double? OreCigoMaltempo { set; get; }
    }
}