﻿namespace TBridge.Cemi.Type.Filters
{
    public class CommittenteFilter
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }
    }
}