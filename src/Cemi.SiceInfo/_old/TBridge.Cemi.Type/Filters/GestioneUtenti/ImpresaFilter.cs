using System;

namespace TBridge.Cemi.Type.Filters.GestioneUtenti
{
    public class ImpresaFilter
    {
        public int? Codice { get; set; }

        public string RagioneSociale { get; set; }

        public string PartitaIVA { get; set; }

        public string CodiceFiscale { get; set; }

        public string Cap { get; set; }

        public DateTime? PinDal { get; set; }

        public DateTime? PinAl { get; set; }

        public bool? NuoveIscritte { get; set; }

        public bool? ConPin { get; set; }

        public bool? ConFabbisogno { get; set; }

        public bool? ConDenuncia { get; set; }

        public DateTime? IscrittaDal { get; set; }

        public DateTime? IscrittaAl { get; set; }

        public bool? IscrittaSitoWeb { get; set; }
    }
}