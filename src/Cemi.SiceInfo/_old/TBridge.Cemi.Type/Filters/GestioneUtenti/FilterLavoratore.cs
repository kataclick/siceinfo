﻿using System;

namespace TBridge.Cemi.Type.Filters.GestioneUtenti
{
    public class FilterLavoratore
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public int? IdLavoratore { get; set; }

        public DateTime? DataNascita { get; set; }

        public bool? WithPIN { get; set; }

        public DateTime? DataGenerazionePinDa { get; set; }

        public DateTime? DataGenerazionePinA { get; set; }

        public int? IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public bool? IscrittoSitoWeb { get; set; }

        public string NumeroCelluare { get; set; }
    }
}