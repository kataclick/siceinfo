﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class ImpresaFilter
    {
        public int? IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string IvaFisc { get; set; }

        public int? IdConsulente { get; set; }
    }
}