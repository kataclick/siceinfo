﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class PartecipazioneLavoratoreImpresaFilter
    {
        public string LavoratoreCognome { get; set; }

        public string LavoratoreNome { get; set; }

        public string LavoratoreCodiceFiscale { get; set; }

        public int? IdImpresa { get; set; }

        public string ImpresaRagioneSociale { get; set; }

        public string ImpresaIvaFisc { get; set; }

        public int? IdConsulente { get; set; }
    }
}