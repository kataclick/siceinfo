﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class PartecipazioneFilter
    {
        public int? IdLavoratore { get; set; }
    }
}