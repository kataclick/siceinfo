﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi.Esem
{
    public class CorsoFilter
    {
        public DateTime Dal { get; set; }

        public DateTime Al { get; set; }

        public string CodiceTipoCorso { get; set; }

        public string CodiceSede { get; set; }
    }
}