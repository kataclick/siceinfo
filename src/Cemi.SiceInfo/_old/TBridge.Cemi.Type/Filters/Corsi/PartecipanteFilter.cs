﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class PartecipanteFilter
    {
        public int IdProgrammazioneModulo { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public bool AttestatoRilasciato { get; set; }
    }
}