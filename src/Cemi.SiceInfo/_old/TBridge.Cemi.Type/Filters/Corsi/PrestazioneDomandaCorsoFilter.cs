using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class PrestazioneDomandaCorsoFilter
    {
        public int? IdCorsiPrestazioneDomanda { get; set; }

        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string CodiceFiscale { get; set; }

        public int? IdLavoratore { get; set; }

        public string IdTipoStatoPrestazione { get; set; }

        public bool? DenunciaPresente { get; set; }
        public bool? CodiceFiscalePresente { get; set; }
        public bool? CarpaPrepagataPresente { get; set; }
        public bool? Iscritto { get; set; }
    }
}