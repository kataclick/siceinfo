﻿namespace TBridge.Cemi.Type.Filters.Corsi
{
    public class AnagraficaUnicaImpreseFilter
    {
        public string CodiceFiscalePartitaIva { get; set; }
    }
}