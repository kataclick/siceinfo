﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class LavoratoreFilter
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public bool InForza { get; set; }

        public int? IdImpresa { get; set; }
    }
}