﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class EstrazioneFormedilFilter
    {
        public DateTime Dal { get; set; }

        public DateTime Al { get; set; }
    }
}