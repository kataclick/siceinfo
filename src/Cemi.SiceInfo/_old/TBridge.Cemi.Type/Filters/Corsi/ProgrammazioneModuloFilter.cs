﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class ProgrammazioneModuloFilter
    {
        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public int? IdCorso { get; set; }

        public int? IdLocazione { get; set; }
    }
}