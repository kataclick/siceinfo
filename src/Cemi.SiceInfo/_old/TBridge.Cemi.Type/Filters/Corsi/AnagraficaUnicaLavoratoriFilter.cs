﻿namespace TBridge.Cemi.Type.Filters.Corsi
{
    public class AnagraficaUnicaLavoratoriFilter
    {
        public string CodiceFiscale { get; set; }
    }
}