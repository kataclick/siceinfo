﻿using System;

namespace TBridge.Cemi.Type.Filters.Corsi
{
    [Serializable]
    public class DTAFilter
    {
        public string Cognome { get; set; }

        public string Corso { get; set; }

        public bool? Diritto { get; set; }
    }
}