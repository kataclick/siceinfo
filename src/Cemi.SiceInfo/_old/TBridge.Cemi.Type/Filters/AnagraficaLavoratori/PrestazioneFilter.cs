﻿using System;

namespace TBridge.Cemi.Type.Filters.AnagraficaLavoratori
{
    public class PrestazioneFilter
    {
        public DateTime? Dal { get; set; }
        public DateTime? Al { get; set; }
        public string IdTipoPrestazione { get; set; }
    }
}