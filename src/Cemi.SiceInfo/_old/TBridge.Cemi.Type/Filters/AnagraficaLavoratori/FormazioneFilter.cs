﻿using System;

namespace TBridge.Cemi.Type.Filters.AnagraficaLavoratori
{
    public class FormazioneFilter
    {
        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }
    }
}