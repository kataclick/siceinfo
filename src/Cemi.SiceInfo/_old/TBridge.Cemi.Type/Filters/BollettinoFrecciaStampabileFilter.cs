﻿namespace TBridge.Cemi.Type.Filters
{
    public class BollettinoFrecciaStampabileFilter
    {
        public int? Anno { get; set; }
        public int? Mese { get; set; }
        public int? IdImpresa { get; set; }
        public string RagioneSociale { get; set; }
    }
}