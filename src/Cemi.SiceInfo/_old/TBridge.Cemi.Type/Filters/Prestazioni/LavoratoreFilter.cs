using System;

namespace TBridge.Cemi.Type.Filters.Prestazioni
{
    [Serializable]
    public class LavoratoreFilter
    {
        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }
    }
}