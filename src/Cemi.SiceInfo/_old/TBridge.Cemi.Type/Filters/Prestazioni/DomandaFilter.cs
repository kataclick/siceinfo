using System;
using TBridge.Cemi.Type.Enums.Prestazioni;

namespace TBridge.Cemi.Type.Filters.Prestazioni
{
    public class DomandaFilter
    {
        public DateTime? DataNascitaLavoratore { get; set; }

        public string NomeLavoratore { get; set; }

        public string CognomeLavoratore { get; set; }

        public int? IdLavoratore { get; set; }

        public char? Stato { get; set; }

        public string IdTipoPrestazione { get; set; }

        public int? Anno { get; set; }

        public int? IdUtente { get; set; }

        public int? Protocollo { get; set; }

        public TipoInserimento? TipologiaInserimento { get; set; }

        public DateTime? DataDomandaDal { get; set; }

        public DateTime? DataDomandaAl { get; set; }

        public int? IdGruppo { get; set; }
    }
}