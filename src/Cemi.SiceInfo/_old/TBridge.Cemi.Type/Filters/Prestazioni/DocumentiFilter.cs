using System;

namespace TBridge.Cemi.Type.Filters.Prestazioni
{
    public class DocumentoFilter
    {
        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public short? IdTipoDocumento { get; set; }

        public DateTime? DataScansioneDa { get; set; }

        public DateTime? DataScansioneA { get; set; }

        public string IdTipoPrestazione { get; set; }

        public int? ProtocolloPrestazione { get; set; }

        public int? NumeroProtocolloPrestazione { get; set; }
    }
}