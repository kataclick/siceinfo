using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Filters.Cantieri
{
    [Serializable]
    public class IspezioniFilter
    {
        private DateTime? al;
        private string appaltatrice;
        private DateTime? dal;
        private int? idAttivita;
        private int? idCantiere;
        private int? idIspettore;
        private int? idIspezione;
        private StatoIspezione? stato;
        private string subAppaltatrice;

        public int? IdIspezione
        {
            get => idIspezione;
            set => idIspezione = value;
        }

        public DateTime? Dal
        {
            get => dal;
            set => dal = value;
        }

        public DateTime? Al
        {
            get => al;
            set => al = value;
        }

        public int? IdIspettore
        {
            get => idIspettore;
            set => idIspettore = value;
        }

        public StatoIspezione? Stato
        {
            get => stato;
            set => stato = value;
        }

        public int? IdCantiere
        {
            get => idCantiere;
            set => idCantiere = value;
        }

        public string Appaltatrice
        {
            get => appaltatrice;
            set => appaltatrice = value;
        }

        public string SubAppaltatrice
        {
            get => subAppaltatrice;
            set => subAppaltatrice = value;
        }

        public int? IdAttivita
        {
            get => idAttivita;
            set => idAttivita = value;
        }

        public string Provincia { get; set; }

        public int? IdImpresa { get; set; }
    }
}