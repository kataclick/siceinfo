﻿using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Filters.Cantieri
{
    public class CantieriFilter
    {
        public int? IdCantiere { get; set; }

        public string Provincia { get; set; }

        public string Comune { get; set; }

        public string Indirizzo { get; set; }

        public double? Importo { get; set; }

        public int? IdZona { get; set; }

        public int? IdImpresa { get; set; }

        public string Impresa { get; set; }

        public string CodiceFiscaleImpresa { get; set; }

        public string Committente { get; set; }

        public bool? Segnalati { get; set; }

        public StatoAssegnazioneFiltro? Assegnati { get; set; }

        public StatoPresaInCaricoFiltro? PresaInCarico { get; set; }

        public StatoRapportoIspezioneFiltro RapportoIspezione { get; set; }

        public int? IdIspettoreAssegnato { get; set; }

        public int? IdIspettorePresoInCarico { get; set; }

        public bool NonCollegatiAttivita { get; set; }

        public string Cap { get; set; }

        public DateTime? DataInserimentoDal { get; set; }

        public DateTime? DataInserimentoAl { get; set; }

        public DateTime? DataSegnalazioneDal { get; set; }

        public DateTime? DataSegnalazioneAl { get; set; }

        public DateTime? DataAssegnazioneDal { get; set; }

        public DateTime? DataAssegnazioneAl { get; set; }

        public DateTime? DataPresaCaricoDal { get; set; }

        public DateTime? DataPresaCaricoAl { get; set; }
    }
}