﻿using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Filters.Cantieri
{
    public class LettereFilter
    {
        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public GruppoLettera? TipoLettera { get; set; }

        public string Protocollo { get; set; }

        public string IndirizzoCantiere { get; set; }

        public string ComuneCantiere { get; set; }

        public int? IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }

        public string IvaFiscImpresa { get; set; }
    }
}