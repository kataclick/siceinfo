﻿using System;

namespace TBridge.Cemi.Type.Filters
{
    [Serializable]
    public class ConsulenteImpreseFilter
    {
        public int? IdImpresa { get; set; }
        public string RagioneSocialeImpresa { get; set; }
        public string CodiceFiscaleImpresa { get; set; }
        public bool? Questionario { get; set; }
    }
}