﻿using System;

namespace TBridge.Cemi.Type.Filters
{
    [Obsolete("Usare versione in Cemi.SiceInfo.Type.Dto.Filters")]
    public class ImpresaRichiestaVariazioneStatoFilter
    {
        public int? IdImpresa { get; set; }
        public string RagioneSociale { get; set; }
        public DateTime? DataRichiestaDa { get; set; }
        public DateTime? DataRichiestaA { get; set; }
        public int? IdTipoStatoImpresa { get; set; }
        public int? IdTipoStatoGestionePratica { get; set; }
        public DateTime? DataInizioCambioStato { get; set; }
    }
}