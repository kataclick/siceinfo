﻿using System;

namespace TBridge.Cemi.Type.Entities.VCOD
{
    public class ImpresaConFasciaIndirizzo
    {
        public int IdImpresa { get; set; }
        public string RagioneSociale { get; set; }
        public string Stato { get; set; }
        public decimal MediaFinale { get; set; }
        public DateTime DataDenuncia { get; set; }
        public int IdConsulente { get; set; }
        public int NumeroOperai { get; set; }
        public string IdEsattore { get; set; }
        public decimal MediaAssenze { get; set; }
        public int PartTime { get; set; }
        public Indirizzo IndirizzoConsulente { get; set; }
        public Indirizzo IndirizzoContatto { get; set; }
    }
}