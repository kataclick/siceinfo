using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoCategoria
    {
        public string IdCategoria { get; set; }

        public string Descrizione { get; set; }
    }
}