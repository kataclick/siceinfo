﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class Festivita : IEquatable<Festivita>
    {
        public DateTime Giorno { get; set; }

        public bool Equals(Festivita other)
        {
            if (Giorno == other.Giorno)
            {
                return true;
            }

            return false;
            //throw new NotImplementedException();
        }
    }
}