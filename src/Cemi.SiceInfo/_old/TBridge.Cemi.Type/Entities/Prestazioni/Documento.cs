using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    public class Documento
    {
        public int? IdDocumento { get; set; }

        public TipoDocumento TipoDocumento { get; set; }

        public string IdArchidoc { get; set; }

        public int? IdLavoratore { get; set; }

        public int? IdFamiliare { get; set; }

        public string LavoratoreCognome { get; set; }

        public string LavoratoreNome { get; set; }

        public string FamiliareCognome { get; set; }

        public string FamiliareNome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public DateTime? DataScansione { get; set; }

        public int? IdPrestazioniDomanda { get; set; }

        public string RiferitoA { get; set; }

        public bool PerPrestazione { get; set; }

        public bool? Originale { get; set; }

        public string IdTipoPrestazione { get; set; }

        public int? NumeroProtocolloPrestazione { get; set; }

        public int? ProtocolloPrestazione { get; set; }

        /// <summary>
        ///     Indica se il documento � in archidoc
        /// </summary>
        public bool PresenteInArchidoc => !string.IsNullOrEmpty(IdArchidoc);

        #region Propriet� per la memorizzazione dei documenti consegnati (FAST)

        public string BeneficiarioPrestazione { get; set; }

        public bool? Consegnato { get; set; }

        #endregion
    }
}