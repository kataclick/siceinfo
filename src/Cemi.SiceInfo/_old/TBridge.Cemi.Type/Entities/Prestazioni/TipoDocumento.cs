using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoDocumento
    {
        private string descrizione;
        private short idTipoDocumento;

        public short IdTipoDocumento
        {
            get => idTipoDocumento;
            set => idTipoDocumento = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public override string ToString()
        {
            return descrizione;
        }
    }
}