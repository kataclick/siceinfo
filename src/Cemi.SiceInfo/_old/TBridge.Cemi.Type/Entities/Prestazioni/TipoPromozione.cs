using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoPromozione
    {
        private string descrizione;
        private int idTipoPromozione;

        public int IdTipoPromozione
        {
            get => idTipoPromozione;
            set => idTipoPromozione = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public override string ToString()
        {
            return Descrizione;
        }

        #region Costruttori

        public TipoPromozione(int idTipoPromozione)
        {
            this.idTipoPromozione = idTipoPromozione;
        }

        public TipoPromozione()
        {
        }

        #endregion
    }
}