using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoDecesso
    {
        private string descrizione;
        private string idTipoDecesso;

        public string IdTipoDecesso
        {
            get => idTipoDecesso;
            set => idTipoDecesso = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public override string ToString()
        {
            return Descrizione;
        }

        #region Costruttori

        public TipoDecesso(string idTipoDecesso)
        {
            this.idTipoDecesso = idTipoDecesso;
        }

        public TipoDecesso()
        {
        }

        #endregion
    }
}