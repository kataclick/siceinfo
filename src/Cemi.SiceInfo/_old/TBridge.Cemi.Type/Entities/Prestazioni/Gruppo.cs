﻿using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Gruppo
    {
        public int Codice { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}