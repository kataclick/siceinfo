using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class PrestazioneImporto
    {
        private string descrizione;
        private int idDomanda;

        private int? idImporto;

        private int idTipoImporto;

        private string idTipoPrestazione;
        private decimal valore;

        public int IdDomanda
        {
            get => idDomanda;
            set => idDomanda = value;
        }

        public int? IdImporto
        {
            get => idImporto;
            set => idImporto = value;
        }

        public int IdTipoImporto
        {
            get => idTipoImporto;
            set => idTipoImporto = value;
        }

        public string IdTipoPrestazione
        {
            get => idTipoPrestazione;
            set => idTipoPrestazione = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public decimal Valore
        {
            get => valore;
            set => valore = value;
        }
    }
}