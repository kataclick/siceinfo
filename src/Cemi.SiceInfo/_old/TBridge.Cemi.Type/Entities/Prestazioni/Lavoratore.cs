using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Lavoratore
    {
        private string cellulare;
        private string codiceFiscale;
        private string cognome;
        private Comunicazioni comunicazioni;
        private Comunicazioni comunicazioniFornite;
        private bool? controlloCellulare;
        private bool? controlloEmail;
        private bool? controlloIndirizzo;
        private DateTime dataNascita;
        private string email;
        private int? idLavoratore;
        private string idTipoPagamento;
        private Indirizzo indirizzo;
        private Indirizzo indirizzoFornito;
        private string nome;

        public int? IdLavoratore
        {
            get => idLavoratore;
            set => idLavoratore = value;
        }

        public string CodiceFiscale
        {
            get => codiceFiscale;
            set => codiceFiscale = value;
        }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public DateTime DataNascita
        {
            get => dataNascita;
            set => dataNascita = value;
        }

        public Indirizzo Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public Comunicazioni Comunicazioni
        {
            get => comunicazioni;
            set => comunicazioni = value;
        }

        public Indirizzo IndirizzoFornito
        {
            get => indirizzoFornito;
            set => indirizzoFornito = value;
        }

        public Comunicazioni ComunicazioniFornite
        {
            get => comunicazioniFornite;
            set => comunicazioniFornite = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }

        public string Cellulare
        {
            get => cellulare;
            set => cellulare = value;
        }

        public bool? ControlloIndirizzo
        {
            get => controlloIndirizzo;
            set => controlloIndirizzo = value;
        }

        public bool? ControlloEmail
        {
            get => controlloEmail;
            set => controlloEmail = value;
        }

        public bool? ControlloCellulare
        {
            get => controlloCellulare;
            set => controlloCellulare = value;
        }

        public string IdTipoPagamento
        {
            get => idTipoPagamento;
            set => idTipoPagamento = value;
        }
    }
}