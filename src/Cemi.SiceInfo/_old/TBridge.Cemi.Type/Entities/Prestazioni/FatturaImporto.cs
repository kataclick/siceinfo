using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class FatturaImporto
    {
        private string _descrizione = string.Empty;
        private string _descrizioneTipoPrestazione = string.Empty;
        private int _idPrestazioneImportoFattura;
        private int _idPrestazioniFattura;
        private string _idTipoPrestazione = string.Empty;
        private int _idTipoPrestazioneImportoFattura;
        private decimal _valore;

        public int IdPrestazioneImportoFattura
        {
            get => _idPrestazioneImportoFattura;
            set => _idPrestazioneImportoFattura = value;
        }

        public int IdTipoPrestazioneImportoFattura
        {
            get => _idTipoPrestazioneImportoFattura;
            set => _idTipoPrestazioneImportoFattura = value;
        }

        public int IdPrestazioniFattura
        {
            get => _idPrestazioniFattura;
            set => _idPrestazioniFattura = value;
        }

        public string IdTipoPrestazione
        {
            get => _idTipoPrestazione;
            set => _idTipoPrestazione = value;
        }

        public string Descrizione
        {
            get => _descrizione;
            set => _descrizione = value;
        }

        public string DescrizioneTipoPrestazione
        {
            get => _descrizioneTipoPrestazione;
            set => _descrizioneTipoPrestazione = value;
        }

        public decimal Valore
        {
            get => _valore;
            set => _valore = value;
        }
    }
}