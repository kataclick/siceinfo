using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Collections.Prestazioni;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    public class PrestazioneCrm
    {
        private string causaleRespinta;

        public PrestazioneCrm()
        {
            DataDomanda = null;
            DataRiferimento = null;
            DataLiquidazione = null;
            IdLavoratore = null;
            IdFamiliare = null;
            FamiliareDataNascita = null;
            ImportoErogato = null;
            ImportoErogatoLordo = null;
            CausaliRespinta = new List<string>();
            //DocumentiMancanti = new List<string>();
        }

        public string IdTipoPrestazione { get; set; }

        public int ProtocolloPrestazione { get; set; }

        public int NumeroProtocolloPrestazione { get; set; }

        public string Descrizione { get; set; }

        public string Beneficiario { get; set; }

        public DateTime? DataDomanda { get; set; }

        public DateTime? DataRiferimento { get; set; }

        public int? IdLavoratore { get; set; }

        public int? IdFamiliare { get; set; }

        public string Stato { get; set; }

        public string CausaleRespinta
        {
            get => causaleRespinta;
            set
            {
                causaleRespinta = value;

                //Poich� in siceinfo ed in sicenew non s� in grado di gestire pi� causali, 
                //facciamo in modo che l'unico punto per impostarla sia la propriet�.
                //Per estensioni future per� usiamo una lista di causali
                CausaliRespinta.Clear();
                CausaliRespinta.Add(causaleRespinta);
            }
        }

        public string LavoratoreIndirizzo { get; set; }

        public string LavoratoreProvincia { get; set; }

        public string LavoratoreComune { get; set; }

        public string LavoratoreFrazione { get; set; }

        public string LavoratoreCap { get; set; }

        public string LavoratoreCellulare { get; set; }

        public string LavoratoreEmail { get; set; }

        public string LavoratoreIdTipoPagamento { get; set; }

        public string FamiliareCognome { get; set; }

        public string FamiliareNome { get; set; }

        public DateTime? FamiliareDataNascita { get; set; }

        public string FamiliareCodiceFiscale { get; set; }

        public string GradoParentela { get; set; }

        public decimal? ImportoErogatoLordo { get; set; }

        public decimal? ImportoErogato { get; set; }

        public string ModalitaPagamento { get; set; }

        public string NumeroMandato { get; set; }

        public string StatoAssegno { get; set; }

        public string DescrizioneStatoAssegno { get; set; }

        public DateTime? DataLiquidazione { get; set; }

        public string IdTipoPagamento { get; set; }

        public string Iban { get; set; }

        public List<string> CausaliRespinta { get; }

        public string TestoGruppo { get; set; }

        /// <summary>
        ///     Elabora la collection dei documenti mancanti ed estrae solamente quelli non presenti in CE
        /// </summary>
        public List<string> DocumentiMancanti
        {
            get
            {
                List<string> documentiMancanti = new List<string>();
                //Solamente se la domanda � in attesa documenti popoliamo la lista dei doc mancanti
                if (Stato == "O")
                {
                    foreach (Documento doc in Documenti)
                    {
                        //if (!doc.PresenteInArchidoc)  //rimosso per richiesta CEMI di inizio ottobre 2011 - sul CRM vogliono TUTTI i documenti
                        if (!documentiMancanti.Contains(doc.TipoDocumento.Descrizione))
                            documentiMancanti.Add(doc.TipoDocumento.Descrizione);
                    }
                }
                return documentiMancanti;
            }
        }

        /// <summary>
        ///     Elenco dei documenti legati alla domanda
        /// </summary>
        public DocumentoCollection Documenti { get; set; }
    }
}