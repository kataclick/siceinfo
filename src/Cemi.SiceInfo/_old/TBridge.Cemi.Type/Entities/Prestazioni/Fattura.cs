using System;
using TBridge.Cemi.Type.Collections.Prestazioni;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Fattura
    {
        private DateTime? data;
        private DateTime? dataAssociazione;
        private DateTime? dataInserimentoRecord;
        private DateTime? dataValidazione;
        private string idArchidoc;
        private int? idLavoratore;
        private int? idPrestazioniFattura;
        private FatturaImportoCollection importi;
        private decimal importoTotale;
        private string numero;
        private bool originale;
        private bool? valida;

        public int? IdPrestazioniFattura
        {
            get => idPrestazioniFattura;
            set => idPrestazioniFattura = value;
        }

        public int? IdLavoratore
        {
            get => idLavoratore;
            set => idLavoratore = value;
        }

        public string IdArchidoc
        {
            get => idArchidoc;
            set => idArchidoc = value;
        }

        public DateTime? Data
        {
            get => data;
            set => data = value;
        }


        public DateTime? DataValidazione
        {
            get => dataValidazione;
            set => dataValidazione = value;
        }

        public DateTime? DataAssociazione
        {
            get => dataAssociazione;
            set => dataAssociazione = value;
        }

        public string Numero
        {
            get => numero;
            set => numero = value;
        }

        public decimal ImportoTotale
        {
            get => importoTotale;
            set => importoTotale = value;
        }

        public bool? Valida
        {
            get => valida;
            set => valida = value;
        }

        public bool Originale
        {
            get => originale;
            set => originale = value;
        }

        public DateTime? DataInserimentoRecord
        {
            get => dataInserimentoRecord;
            set => dataInserimentoRecord = value;
        }

        public FatturaImportoCollection Importi
        {
            get => importi;
            set => importi = value;
        }

        public DateTime? DataScansione { get; set; }
    }
}