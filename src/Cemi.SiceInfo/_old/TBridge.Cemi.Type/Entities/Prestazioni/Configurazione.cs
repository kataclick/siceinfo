using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Configurazione
    {
        private int differenzaMesiFatturaDomanda;
        private string gradoParentela;
        private string idTipoPrestazione;
        private DateTime periodoRiferimentoFine;
        private DateTime periodoRiferimentoInizio;
        private bool reciprocita123;
        private bool reciprocita45;
        private bool richiestaFattura;
        private bool richiestaTipoScuola;
        private TipoMacroPrestazione tipoMacroPrestazione;
        private TipoModulo tipoModulo;
        private bool univocitaAnnuale;
        private bool univocitaFamiliare;

        public string IdTipoPrestazione
        {
            get => idTipoPrestazione;
            set => idTipoPrestazione = value;
        }

        public string GradoParentela
        {
            get => gradoParentela;
            set => gradoParentela = value;
        }

        public bool Reciprocita123
        {
            get => reciprocita123;
            set => reciprocita123 = value;
        }

        public bool Reciprocita45
        {
            get => reciprocita45;
            set => reciprocita45 = value;
        }

        public bool UnivocitaAnnuale
        {
            get => univocitaAnnuale;
            set => univocitaAnnuale = value;
        }

        public bool UnivocitaFamiliare
        {
            get => univocitaFamiliare;
            set => univocitaFamiliare = value;
        }

        public DateTime PeriodoRiferimentoInizio
        {
            get => periodoRiferimentoInizio;
            set => periodoRiferimentoInizio = value;
        }

        public DateTime PeriodoRiferimentoFine
        {
            get => periodoRiferimentoFine;
            set => periodoRiferimentoFine = value;
        }

        public int DifferenzaMesiFatturaDomanda
        {
            get => differenzaMesiFatturaDomanda;
            set => differenzaMesiFatturaDomanda = value;
        }

        public bool RichiestaFattura
        {
            get => richiestaFattura;
            set => richiestaFattura = value;
        }

        public bool RichiestaTipoScuola
        {
            get => richiestaTipoScuola;
            set => richiestaTipoScuola = value;
        }

        public TipoMacroPrestazione TipoMacroPrestazione
        {
            get => tipoMacroPrestazione;
            set => tipoMacroPrestazione = value;
        }

        public TipoModulo TipoModulo
        {
            get => tipoModulo;
            set => tipoModulo = value;
        }
    }
}