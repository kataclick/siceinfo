﻿using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    public class DocumentoCopertina
    {
        public int IdDomanda { get; set; }

        public short? IdTipoDocumento { get; set; }

        public int IdTipoDocumentoArchidoc { get; set; }

        public string DescrizioneDocumento { get; set; }

        public string DescrizioneBeneficiario { get; set; }

        public int? IdFatturaDichiarata { get; set; }

        public bool Consegnato { get; set; }

        public string GradoParentela { get; set; }

        public string GradoParentelaDocumento { get; set; }

        public DateTime? FatturaData { get; set; }

        public string FatturaNumero { get; set; }
    }
}