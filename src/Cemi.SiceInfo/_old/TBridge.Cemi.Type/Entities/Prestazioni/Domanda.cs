using System;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Enums.Prestazioni;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Domanda
    {
        private string beneficiario;
        private CassaEdileCollection casseEdili;
        private bool certificatoFamiglia;
        private bool confermata;
        private bool? controlloFamiliare;
        private bool? controlloFatture;
        private bool? controlloLavoratore;
        private bool? controlloOreCnce;
        private bool? controlloPresenzaDocumenti;
        private bool? controlloScolastiche;
        private bool? controlloUnivocitaPrestazione;

        //private DateTime? dataConferma;

        private DateTime dataRiferimento;
        private DatiScolastiche datiAggiuntiviScolastiche;
        private Familiare familiare;
        private Familiare familiareFornito;
        private FatturaDichiarataCollection fattureDichiarate;
        private FatturaCollection fattureRicevute;
        private Guid guid;
        private int? idDomanda;
        private int? idUtenteInCarico;
        private PrestazioneImportoCollection importi;
        private Lavoratore lavoratore;
        private string loginInCarico;
        private bool moduloDentista;

        /// <summary>
        ///     Rappresenta il numero di fatture dichiarate all'atto di compilazione della domanda;
        ///     per il pregresso viene usato il count delle fatture dichiarate
        /// </summary>
        private int? numeroFatture;

        private int numeroProtocolloPrestazione;
        private short protocolloPrestazione;
        private StatoDomanda stato;
        private TipoPrestazione tipoPrestazione;

        public int? IdDomanda
        {
            get => idDomanda;
            set => idDomanda = value;
        }

        /// <summary>
        ///     [DEPRECATO] utilizzare data domanda. La propriet� usa comunque dataDomanda
        /// </summary>
        public DateTime? DataConferma
        {
            get => DataDomanda;
            set => DataDomanda = value;
        }

        public Lavoratore Lavoratore
        {
            get => lavoratore;
            set => lavoratore = value;
        }

        public Familiare Familiare
        {
            get => familiare;
            set => familiare = value;
        }

        public Familiare FamiliareFornito
        {
            get => familiareFornito;
            set => familiareFornito = value;
        }

        public string Beneficiario
        {
            get => beneficiario;
            set => beneficiario = value;
        }

        public string IdTipoPrestazione
        {
            get
            {
                if (tipoPrestazione != null)
                    return tipoPrestazione.IdTipoPrestazione;
                return string.Empty;
            }
        }

        public string DescrizioneTipoPrestazione
        {
            get
            {
                if (tipoPrestazione != null)
                    return tipoPrestazione.Descrizione;
                return string.Empty;
            }
        }

        public TipoPrestazione TipoPrestazione
        {
            get => tipoPrestazione;
            set => tipoPrestazione = value;
        }

        public FatturaDichiarataCollection FattureDichiarate
        {
            get => fattureDichiarate;
            set => fattureDichiarate = value;
        }

        public FatturaCollection FattureRicevute
        {
            get => fattureRicevute;
            set => fattureRicevute = value;
        }

        public StatoDomanda Stato
        {
            get => stato;
            set => stato = value;
        }

        public bool? ControlloFamiliare
        {
            get => controlloFamiliare;
            set => controlloFamiliare = value;
        }

        public bool? ControlloPresenzaDocumenti
        {
            get => controlloPresenzaDocumenti;
            set => controlloPresenzaDocumenti = value;
        }

        public bool? ControlloFatture
        {
            get => controlloFatture;
            set => controlloFatture = value;
        }

        public bool? ControlloUnivocitaPrestazione
        {
            get => controlloUnivocitaPrestazione;
            set => controlloUnivocitaPrestazione = value;
        }

        public bool? ControlloLavoratore
        {
            get => controlloLavoratore;
            set => controlloLavoratore = value;
        }

        public bool? ControlloOreCnce
        {
            get => controlloOreCnce;
            set => controlloOreCnce = value;
        }

        public int? IdUtenteInCarico
        {
            get => idUtenteInCarico;
            set => idUtenteInCarico = value;
        }

        public string LoginInCarico
        {
            get => loginInCarico;
            set => loginInCarico = value;
        }

        public CassaEdileCollection CasseEdili
        {
            get => casseEdili;
            set => casseEdili = value;
        }

        public DateTime DataRiferimento
        {
            get => dataRiferimento;
            set => dataRiferimento = value;
        }

        public Guid Guid
        {
            get => guid;
            set => guid = value;
        }

        public short ProtocolloPrestazione
        {
            get => protocolloPrestazione;
            set => protocolloPrestazione = value;
        }

        public int NumeroProtocolloPrestazione
        {
            get => numeroProtocolloPrestazione;
            set => numeroProtocolloPrestazione = value;
        }

        public bool Confermata
        {
            get => confermata;
            set => confermata = value;
        }

        public string ProtocolloPrestazioneCompleto =>
            string.Format("{0}/{1}", ProtocolloPrestazione, NumeroProtocolloPrestazione);

        public bool? ControlloScolastiche
        {
            get => controlloScolastiche;
            set => controlloScolastiche = value;
        }

        public DatiScolastiche DatiAggiuntiviScolastiche
        {
            get => datiAggiuntiviScolastiche;
            set => datiAggiuntiviScolastiche = value;
        }

        public PrestazioneImportoCollection Importi
        {
            get => importi;
            set => importi = value;
        }

        public bool CertificatoFamiglia
        {
            get => certificatoFamiglia;
            set => certificatoFamiglia = value;
        }

        public bool ModuloDentista
        {
            get => moduloDentista;
            set => moduloDentista = value;
        }

        /// <summary>
        ///     Data della domanda, se nulla viene usata quella di inserimento
        /// </summary>
        public DateTime? DataDomanda { get; set; }

        public DocumentoCollection Documenti { get; set; }

        /// <summary>
        ///     Tipo di insermento, tipicamente fast o normale
        /// </summary>
        public TipoInserimento TipoInserimento { get; set; }

        public int? NumeroFatture
        {
            get
            {
                if (numeroFatture.HasValue)
                    return numeroFatture;
                if (FattureDichiarate != null)
                {
                    return FattureDichiarate.Count;
                }
                return -1;
            }
            set => numeroFatture = value;
        }

        /// <summary>
        ///     Causale di respinta
        /// </summary>
        public TipoCausale TipoCausale { get; set; }

        /// <summary>
        ///     indica il tipo modulo della prestazione
        /// </summary>
        public TipoModulo TipoModulo { get; set; }

        /// <summary>
        ///     Data di gestione della domanda
        /// </summary>
        public DateTime? DataGestione { get; set; }

        /// <summary>
        ///     Serve per differenziare la domanda nel funerario, � solo di facciata
        /// </summary>
        public bool GenitoriConviventi { get; set; }

        public string Nota { get; set; }

        public Domanda CopiaBase()
        {
            Domanda domanda = new Domanda();

            domanda.TipoPrestazione = TipoPrestazione;
            domanda.NumeroProtocolloPrestazione = NumeroProtocolloPrestazione;
            domanda.ProtocolloPrestazione = ProtocolloPrestazione;
            domanda.beneficiario = Beneficiario;
            domanda.DataDomanda = DataDomanda;
            domanda.DataRiferimento = DataRiferimento;
            domanda.Lavoratore = Lavoratore;
            domanda.Familiare = Familiare;
            domanda.Stato = Stato;

            domanda.FattureDichiarate = new FatturaDichiarataCollection();
            domanda.FattureRicevute = new FatturaCollection();
            domanda.CasseEdili = new CassaEdileCollection();

            domanda.Guid = Guid.NewGuid();

            return domanda;
        }
    }
}