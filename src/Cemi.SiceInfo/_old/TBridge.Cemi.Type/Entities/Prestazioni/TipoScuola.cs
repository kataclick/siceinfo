using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoScuola
    {
        private string descrizione;
        private int idTipoScuola;

        public int IdTipoScuola
        {
            get => idTipoScuola;
            set => idTipoScuola = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public override string ToString()
        {
            return Descrizione;
        }

        #region Costruttori

        public TipoScuola(int idTipoScuola)
        {
            this.idTipoScuola = idTipoScuola;
        }

        public TipoScuola()
        {
        }

        #endregion
    }
}