﻿namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    public class StatiCureProtesi
    {
        public string StatoCL { get; set; }

        public string StatoPL { get; set; }
    }
}