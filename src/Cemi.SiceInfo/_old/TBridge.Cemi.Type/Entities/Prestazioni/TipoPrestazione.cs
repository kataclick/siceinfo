using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoPrestazione
    {
        private string descrizione;
        private string gradoParentela;
        private string idTipoPrestazione;

        public string IdTipoPrestazione
        {
            get => idTipoPrestazione;
            set => idTipoPrestazione = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public string GradoParentela
        {
            get => gradoParentela;
            set => gradoParentela = value;
        }

        public DateTime? ValidaDa { get; set; }
        public DateTime? ValidaA { get; set; }

        public Gruppo Gruppo { get; set; }

        public override string ToString()
        {
            return descrizione;
        }
    }
}