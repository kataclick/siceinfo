﻿namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    public class DocumentoStampaCopertina
    {
        public DocumentoStampaCopertina(string id, bool? originale)
        {
            Id = id;
            Originale = originale;
        }

        public string Id { set; get; }
        public bool? Originale { set; get; }
    }
}