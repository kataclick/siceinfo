using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class DatiScolastiche
    {
        private int? annoDaErogare;
        private short? annoFrequenza;
        private short? annoImmatricolazione;
        private string applicaDeduzione;
        private short? cfuConseguiti;
        private short? cfuPrevisti;
        private short? classeConclusa;
        private DateTime? dataDecesso;
        private int? idDatiScolastiche;
        private int? idFamiliareErede;
        private string idTipoPrestazione;
        private string idTipoPrestazioneDecesso;
        private decimal? mediaVoti;
        private short? numeroAnniFrequentati;
        private short? numeroAnniFuoriCorso;
        private short? numeroEsamiSostenuti;
        private short? numeroMesiFrequentati;
        private int? tipoDaErogare;
        private TipoPromozione tipoPromozione;
        private TipoScuola tipoScuola;

        public int? IdDatiScolastiche
        {
            get => idDatiScolastiche;
            set => idDatiScolastiche = value;
        }

        public short? AnnoFrequenza
        {
            get => annoFrequenza;
            set => annoFrequenza = value;
        }

        public TipoScuola TipoScuola
        {
            get => tipoScuola;
            set => tipoScuola = value;
        }

        public TipoPromozione TipoPromozione
        {
            get => tipoPromozione;
            set => tipoPromozione = value;
        }

        public short? AnnoImmatricolazione
        {
            get => annoImmatricolazione;
            set => annoImmatricolazione = value;
        }

        public short? NumeroAnniFrequentati
        {
            get => numeroAnniFrequentati;
            set => numeroAnniFrequentati = value;
        }

        public short? NumeroAnniFuoriCorso
        {
            get => numeroAnniFuoriCorso;
            set => numeroAnniFuoriCorso = value;
        }

        public decimal? MediaVoti
        {
            get => mediaVoti;
            set => mediaVoti = value;
        }

        public short? NumeroEsamiSostenuti
        {
            get => numeroEsamiSostenuti;
            set => numeroEsamiSostenuti = value;
        }

        public short? CfuConseguiti
        {
            get => cfuConseguiti;
            set => cfuConseguiti = value;
        }

        public short? CfuPrevisti
        {
            get => cfuPrevisti;
            set => cfuPrevisti = value;
        }

        public short? NumeroMesiFrequentati
        {
            get => numeroMesiFrequentati;
            set => numeroMesiFrequentati = value;
        }

        public short? ClasseConclusa
        {
            get => classeConclusa;
            set => classeConclusa = value;
        }

        public string IdTipoPrestazione
        {
            get => idTipoPrestazione;
            set => idTipoPrestazione = value;
        }

        public int? AnnoDaErogare
        {
            get => annoDaErogare;
            set => annoDaErogare = value;
        }

        public int? TipoDaErogare
        {
            get => tipoDaErogare;
            set => tipoDaErogare = value;
        }

        public DateTime? DataDecesso
        {
            get => dataDecesso;
            set => dataDecesso = value;
        }

        //Non deve essere pi� usato
        //private int? numeroFigliCarico;
        //public int? NumeroFigliCarico
        //{
        //    get { return numeroFigliCarico; }
        //    set { numeroFigliCarico = value; }
        //}

        public string IdTipoPrestazioneDecesso
        {
            get => idTipoPrestazioneDecesso;
            set => idTipoPrestazioneDecesso = value;
        }

        public string ApplicaDeduzione
        {
            get => applicaDeduzione;
            set => applicaDeduzione = value;
        }

        public int? IdFamiliareErede
        {
            get => idFamiliareErede;
            set => idFamiliareErede = value;
        }

        public DateTime? PeriodoDal { get; set; }

        public DateTime? PeriodoAl { get; set; }

        public bool ForzaturaPeriodoFrequenza180Giorni { get; set; }

        public bool ForzaturaPeriodoFrequenza3Anni { get; set; }

        public bool ForzaturaAnnoFrequenza { get; set; }

        public bool? RedditoSi { get; set; }

        public string AnnoScolastico { get; set; }

        public string IstitutoDenominazione { get; set; }

        public string IstitutoIndirizzo { get; set; }

        public string IstitutoProvincia { get; set; }

        public string IstitutoLocalita { get; set; }
    }
}