using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Familiare
    {
        private string aCarico;
        private string codiceFiscale;
        private string cognome;
        private bool? controlloACarico;
        private bool? controlloCodiceFiscale;
        private bool? controlloDataDecesso;
        private bool? controlloDataNascita;
        private bool? controlloFamiliareSelezionato;
        private bool? controlloGradoParentela;
        private DateTime? dataDecesso;
        private DateTime? dataNascita;
        private string gradoParentela;
        private int? idFamiliare;
        private int idLavoratore;
        private string nome;
        private char sesso = 'M';

        public int IdLavoratore
        {
            get => idLavoratore;
            set => idLavoratore = value;
        }

        public int? IdFamiliare
        {
            get => idFamiliare;
            set => idFamiliare = value;
        }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string NomeCompleto => string.Format("{0} {1}", cognome, nome);

        public DateTime? DataNascita
        {
            get => dataNascita;
            set => dataNascita = value;
        }

        public DateTime? DataDecesso
        {
            get => dataDecesso;
            set => dataDecesso = value;
        }

        public string CodiceFiscale
        {
            get => codiceFiscale;
            set => codiceFiscale = value;
        }

        public string GradoParentela
        {
            get => gradoParentela;
            set => gradoParentela = value;
        }

        public bool? ControlloFamiliareSelezionato
        {
            get => controlloFamiliareSelezionato;
            set => controlloFamiliareSelezionato = value;
        }

        public bool? ControlloDataDecesso
        {
            get => controlloDataDecesso;
            set => controlloDataDecesso = value;
        }

        public bool? ControlloGradoParentela
        {
            get => controlloGradoParentela;
            set => controlloGradoParentela = value;
        }

        public bool? ControlloDataNascita
        {
            get => controlloDataNascita;
            set => controlloDataNascita = value;
        }

        public bool? ControlloCodiceFiscale
        {
            get => controlloCodiceFiscale;
            set => controlloCodiceFiscale = value;
        }

        public bool? ControlloACarico
        {
            get => controlloACarico;
            set => controlloACarico = value;
        }

        public char Sesso
        {
            get => sesso;
            set => sesso = value;
        }

        public string ACarico
        {
            get => aCarico;
            set => aCarico = value;
        }
    }
}