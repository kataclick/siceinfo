using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Comunicazioni
    {
        private DateTime? appuntamento;
        private string cellulare;

        private string email;
        private string iban;

        private string idTipoPagamento;

        public string Cellulare
        {
            get => cellulare;
            set => cellulare = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }

        public string IdTipoPagamento
        {
            get => idTipoPagamento;
            set => idTipoPagamento = value;
        }

        public string Iban
        {
            get => iban;
            set => iban = value;
        }

        public DateTime? Appuntamento
        {
            get => appuntamento;
            set => appuntamento = value;
        }
    }
}