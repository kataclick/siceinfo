using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    public class PrestazioneErogata
    {
        public TipoPrestazione TipoPrestazione { get; set; }

        public int IdLavoratore { get; set; }

        public Familiare Familiare { get; set; }

        public DateTime DataDomanda { get; set; }

        public decimal ImportoLiquidabile { get; set; }

        public string IdCausaleRespinta { get; set; }

        public int ProtocolloPrestazione { get; set; }

        public int NumeroProtocolloPrestazione { get; set; }

        public string IdStatoPrestazione { get; set; }
    }
}