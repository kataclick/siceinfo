using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoModulo
    {
        private string descrizione;
        private short idTipoModulo;
        private string modulo;

        public short IdTipoModulo
        {
            get => idTipoModulo;
            set => idTipoModulo = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public string Modulo
        {
            get => modulo;
            set => modulo = value;
        }
    }
}