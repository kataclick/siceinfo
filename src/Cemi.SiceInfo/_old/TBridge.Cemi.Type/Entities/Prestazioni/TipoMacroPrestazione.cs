using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class TipoMacroPrestazione
    {
        private string descrizione;
        private short idTipoMacroPrestazione;

        public short IdTipoMacroPrestazione
        {
            get => idTipoMacroPrestazione;
            set => idTipoMacroPrestazione = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }
    }
}