using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class FatturaDichiarata
    {
        private bool aggiuntaOperatore;
        private DateTime data;
        private DateTime? dataAnnullamento;
        private DateTime dataInserimentoRecord;
        private string idFatturaArchidoc;
        private int? idPrestazioniDomanda;
        private int? idPrestazioniFattura;
        private int? idPrestazioniFatturaDichiarata;
        private decimal importoTotale;
        private string numero;
        private bool saldo;

        public int? IdPrestazioniFatturaDichiarata
        {
            get => idPrestazioniFatturaDichiarata;
            set => idPrestazioniFatturaDichiarata = value;
        }

        public int? IdPrestazioniDomanda
        {
            get => idPrestazioniDomanda;
            set => idPrestazioniDomanda = value;
        }

        public int? IdPrestazioniFattura
        {
            get => idPrestazioniFattura;
            set => idPrestazioniFattura = value;
        }

        public string IdFatturaArchidoc
        {
            get => idFatturaArchidoc;
            set => idFatturaArchidoc = value;
        }

        public DateTime Data
        {
            get => data;
            set => data = value;
        }

        public string Numero
        {
            get => numero;
            set => numero = value;
        }

        public decimal ImportoTotale
        {
            get => importoTotale;
            set => importoTotale = value;
        }

        public bool Saldo
        {
            get => saldo;
            set => saldo = value;
        }

        public DateTime? DataAnnullamento
        {
            get => dataAnnullamento;
            set => dataAnnullamento = value;
        }

        public DateTime DataInserimentoRecord
        {
            get => dataInserimentoRecord;
            set => dataInserimentoRecord = value;
        }

        //Indica se la fattura dichiarata � stata aggiunta dall'operatore successivamente all'inserimento della domanda da parte del lavoratore
        public bool AggiuntaOperatore
        {
            get => aggiuntaOperatore;
            set => aggiuntaOperatore = value;
        }

        public int? IdUtenteInserimento { get; set; }

        public string UtenteInserimento { get; set; }
    }
}