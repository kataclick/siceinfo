using System;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Indirizzo
    {
        private string cap;
        private string comune;
        private string frazione;
        private string indirizzoVia;

        private string provincia;

        public string IndirizzoVia
        {
            get => indirizzoVia;
            set => indirizzoVia = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Frazione
        {
            get => frazione;
            set => frazione = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public override bool Equals(object obj)
        {
            bool res = false;
            Indirizzo ind = obj as Indirizzo;

            if (ind != null)
            {
                if (IndirizzoVia == ind.IndirizzoVia
                    && Provincia == ind.Provincia
                    && Comune == ind.Comune
                    && Cap == ind.Cap)
                    res = true;
            }

            return res;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",
                IndirizzoVia,
                Comune,
                Provincia,
                Cap
            );
        }
    }
}