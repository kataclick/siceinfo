﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBridge.Cemi.Type.Entities.Prestazioni
{
    [Serializable]
    public class Quorum
    {
        public int IdLavoratore { get; set; }

        public int IdFamiliare { get; set; }

        public string Beneficiario { get; set; }

        public string IdTipoPrestazione { get; set; }

        public decimal Importo { get; set; }

        public bool Semaforo { get; set; }
    }
}
