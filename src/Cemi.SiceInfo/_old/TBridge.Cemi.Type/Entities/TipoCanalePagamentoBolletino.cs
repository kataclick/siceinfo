﻿namespace TBridge.Cemi.Type.Entities
{
    public class TipoCanalePagamentoBolletino
    {
        public int Id { get; set; }
        public string Descrizione { get; set; }
    }
}