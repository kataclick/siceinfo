﻿using System;

namespace TBridge.Cemi.Type.Entities.Geocode
{
    [Serializable]
    public class Indirizzo : Cemi.Type.Entities.Indirizzo
    {
        public decimal? Latitudine { get; set; }
        public decimal? Longitudine { get; set; }

        public string IndirizzoCompletoGeocode => $"{IndirizzoBase} {Localita} {Stato}";

        public bool Georeferenziato => Latitudine.HasValue && Longitudine.HasValue;
    }
}