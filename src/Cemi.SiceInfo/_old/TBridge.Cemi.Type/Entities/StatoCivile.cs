using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class StatoCivile
    {
        public string IdStatoCivile { get; set; }

        public string Descrizione { get; set; }
    }
}