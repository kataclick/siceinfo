﻿using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    public class BollettinoStampato
    {
        public BollettinoStampato(Bollettino bollettino)
        {
            if (bollettino == null)
            {
                throw new ArgumentNullException("bollettino");
            }

            Bollettino = bollettino;
        }

        public Bollettino Bollettino { get; }

        public string RagioneSociale { get; set; }

        public DateTime? DataVersamento { get; set; }

        public DateTime DataRichiesta { get; set; }

        public string DescrizioneTipoCanalePagamento { set; get; }

        public TipiCanalePagamento TipoCanalePagamento => Bollettino.TipoCanalePagamento;

        public int IdImpresa => Bollettino.IdImpresa;

        public int Anno => Bollettino.Anno;

        public int Mese => Bollettino.Mese;

        public int Sequenza => Bollettino.Sequenza;

        public decimal Importo => Bollettino.Importo;
    }
}