using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class ImpreseConsulente
    {
        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }

        public bool QuestionarioPresente { get; set; }
    }
}