﻿using System;

namespace TBridge.Cemi.Type.Entities.TuteScarpe
{
    public class Consegna
    {
        public int NumeroOrdine { get; set; }
        public int CodiceImpresa { get; set; }
        public string NumeroSpedizione { get; set; }
        public DateTime DataEvento { get; set; }
        public string Stato { get; set; }
        public int? IdentificativoFile { get; set; }
    }
}