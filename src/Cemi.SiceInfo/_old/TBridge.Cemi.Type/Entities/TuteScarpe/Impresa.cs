﻿namespace TBridge.Cemi.Type.Entities.TuteScarpe
{
    public class Impresa : Entities.Impresa
    {
        public string EmailConsulente { get; set; }
        public string EmailImpresa { get; set; }
        public Indirizzo Recapito { get; set; }
        public string Telefono { get; set; }
    }
}