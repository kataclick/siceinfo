using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoQualifica
    {
        public string IdQualifica { get; set; }

        public string Descrizione { get; set; }
    }
}