using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class ImpresaAnagraficaComune
    {
        public FonteAnagraficheComuni Fonte { get; set; }

        public int? IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }
    }
}