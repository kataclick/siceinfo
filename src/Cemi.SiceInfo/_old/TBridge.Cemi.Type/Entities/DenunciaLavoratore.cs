﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class DenunciaLavoratore
    {
        public int IdLavoratore { get; set; }

        public int IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }

        public DateTime Data { get; set; }
    }
}