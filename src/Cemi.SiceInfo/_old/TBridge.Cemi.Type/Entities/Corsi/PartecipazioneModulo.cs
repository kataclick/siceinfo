﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class PartecipazioneModulo
    {
        public int? IdPartecipazioneModulo { get; set; }

        public ProgrammazioneModulo Programmazione { get; set; }

        public DateTime? DataPianificazione
        {
            get
            {
                if (Programmazione == null)
                {
                    return null;
                }
                return Programmazione.Schedulazione;
            }
        }

        public int? IdCorso
        {
            get
            {
                if (Programmazione == null)
                {
                    return null;
                }
                return Programmazione.Corso.IdCorso;
            }
        }

        public int? IdLocazione
        {
            get
            {
                if (Programmazione == null)
                {
                    return null;
                }
                return Programmazione.Locazione.IdLocazione;
            }
        }

        public bool Presenza { get; set; }

        public int? IdPartecipazione { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }
    }
}