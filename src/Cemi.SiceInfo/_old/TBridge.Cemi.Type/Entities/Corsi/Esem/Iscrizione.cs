﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi.Esem
{
    [Serializable]
    public class Iscrizione
    {
        public Cemi.Type.Entities.Corsi.Esem.Corso Corso { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public string Contatto { get; set; }

        public bool Gratuito { get; set; }

        public bool Ticket { get; set; }
    }
}