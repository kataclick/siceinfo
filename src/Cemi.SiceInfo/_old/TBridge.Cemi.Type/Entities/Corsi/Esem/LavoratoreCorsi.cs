﻿using System;
using System.Collections.Generic;

namespace TBridge.Cemi.Type.Entities.Corsi.Esem
{
    public class LavoratoreCorsi : IComparable
    {
        public LavoratoreCorsi()
        {
            Corsi = new List<Cemi.Type.Entities.Corsi.Esem.Corso>();
        }

        public Lavoratore Lavoratore { get; set; }

        public List<Cemi.Type.Entities.Corsi.Esem.Corso> Corsi { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(LavoratoreCorsi))
            {
                if (Lavoratore != null && obj != null && ((LavoratoreCorsi) obj).Lavoratore != null)
                {
                    return Lavoratore.Cognome.CompareTo(((LavoratoreCorsi) obj).Lavoratore.Cognome);
                }
                return 0;
            }

            return 0;
        }
    }
}