﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi.Esem
{
    public class TipoCorso : IComparable
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }


        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(TipoCorso))
            {
                return Descrizione.CompareTo(((TipoCorso) obj).Descrizione);
            }

            return 0;
        }
    }
}