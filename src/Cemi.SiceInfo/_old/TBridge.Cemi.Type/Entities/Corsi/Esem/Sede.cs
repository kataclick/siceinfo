﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi.Esem
{
    public class Sede : IComparable
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }

        public string Indirizzo { get; set; }

        public string Calendarizzato { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(Sede))
            {
                return Descrizione.CompareTo(((Sede) obj).Descrizione);
            }

            return 0;
        }
    }
}