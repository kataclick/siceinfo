﻿using System;
using System.Collections.Generic;

namespace TBridge.Cemi.Type.Entities.Corsi.Esem
{
    [Serializable]
    public class Corso : IComparable
    {
        public string Codice { get; set; }

        public DateTime? DataInizio { get; set; }

        public string CodiceSede { get; set; }

        public string DescrizioneSede { get; set; }

        public string IndirizzoSede { get; set; }

        public int Progressivo { get; set; }

        public string Descrizione { get; set; }

        public DateTime? DataFine { get; set; }

        public DateTime? DataPresuntaPerPrenotazione { get; set; }

        public decimal Durata { get; set; }

        public decimal Costo { get; set; }

        public int PostiDisponibili { get; set; }

        public string ObbligoVisitaMedica { get; set; }

        public DateTime? DataScadenza { get; set; }

        public List<DateCorso> DateCorso { get; set; }

        public decimal CostoCauzione { get; set; }

        public DateTime? DataPagamentoCauzione { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(Corso))
            {
                if (DataInizio < ((Corso) obj).DataInizio)
                    return -1;
                if (DataInizio > ((Corso) obj).DataInizio)
                    return 1;
                if (DataInizio == ((Corso) obj).DataInizio)
                    return 0;
            }

            return 0;
        }
    }
}