﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi.Esem
{
    [Serializable]
    public class DateCorso : IComparable
    {
        public DateTime GiornoCorso { get; set; }

        public string OraInizio { get; set; }

        public string OraFine { get; set; }

        public int CompareTo(object obj)
        {
            if (obj != null && obj.GetType() == typeof(DateCorso))
            {
                if (GiornoCorso < ((DateCorso) obj).GiornoCorso)
                    return -1;
                if (GiornoCorso > ((DateCorso) obj).GiornoCorso)
                    return 1;
                if (GiornoCorso == ((DateCorso) obj).GiornoCorso)
                    return 0;
            }

            return 0;
        }
    }
}