﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Locazione
    {
        private string comune;
        private string indirizzo;
        private string provincia;
        public int? IdLocazione { get; set; }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Cap { get; set; }

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public string Note { get; set; }

        public bool EsistePianificazione { get; set; }

        public string IndirizzoPerGeocoder =>
            string.Format("{0} {1} {2}, Lombardia, Italy", indirizzo, comune, provincia);

        public string IndirizzoCompleto => string.Format("{0} {1} {2}", indirizzo, comune, provincia);
    }
}