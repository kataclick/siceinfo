﻿namespace TBridge.Cemi.Type.Entities.Corsi
{
    public class PrestazioneDTALavoratore
    {
        public int Anno { get; set; }

        public int IdLavoratore { get; set; }

        public bool Selezionato { get; set; }
    }
}