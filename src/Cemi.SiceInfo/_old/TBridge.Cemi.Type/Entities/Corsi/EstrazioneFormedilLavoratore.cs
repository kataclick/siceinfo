﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class EstrazioneFormedilLavoratore
    {
        public string Corso { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public int? Sesso { get; set; }

        public string CodiceFiscale { get; set; }

        public string Cittadinanza { get; set; }

        public DateTime? DataNascita { get; set; }

        public string ProvinciaResidenza { get; set; }

        public int? TitoloDiStudio { get; set; }

        public string Azienda { get; set; }

        public string DateCorso { get; set; }
    }
}