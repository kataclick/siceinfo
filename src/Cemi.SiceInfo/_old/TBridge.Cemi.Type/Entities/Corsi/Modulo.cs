﻿using System;
using TBridge.Cemi.Type.Collections.Corsi;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Modulo
    {
        public int IdModulo { get; set; }

        public string Descrizione { get; set; }

        public short OreDurata { get; set; }

        public short Progressivo { get; set; }

        public ProgrammazioneModuloCollection Programmazioni { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}