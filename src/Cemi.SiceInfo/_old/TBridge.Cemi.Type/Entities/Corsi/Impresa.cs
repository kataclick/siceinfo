﻿using System;
using TBridge.Cemi.Type.Enums.Corsi;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Impresa : ImpresaAnagraficaComune
    {
        public TipologiaImpresa TipoImpresa { get; set; }

        public string CodiceERagioneSociale
        {
            get
            {
                if (IdImpresa.HasValue && TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    return string.Format("{0} {1}", IdImpresa, RagioneSociale);
                }
                return RagioneSociale;
            }
        }

        public TipoContratto TipoContratto { get; set; }

        public TipoIscrizione TipoIscrizione { get; set; }

        public int? NumeroDipendenti { get; set; }

        public string ProvinciaSedeLegale { get; set; }

        public string ComuneSedeLegale { get; set; }

        public int IdAnagraficaCondivisa { get; set; }
    }
}