﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class ProgrammazioneModulo
    {
        private Modulo modulo;
        public int? IdProgrammazioneModulo { get; set; }

        public Corso Corso { get; set; }

        public Modulo Modulo
        {
            get => modulo;
            set => modulo = value;
        }

        public int IdModulo
        {
            get
            {
                if (modulo != null) return modulo.IdModulo;
                return -1;
            }
        }

        public DateTime? Schedulazione { get; set; }

        public DateTime? SchedulazioneFine { get; set; }

        public string SchedulazioneData
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    return Schedulazione.Value.ToShortDateString();
                }
                return string.Empty;
            }
        }

        public string SchedulazioneOra
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    return Schedulazione.Value.ToShortTimeString();
                }
                return string.Empty;
            }
        }

        public string SchedulazioneStringa
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    if (Locazione.Latitudine.HasValue && Locazione.Longitudine.HasValue)
                    {
                        return
                            string.Format(
                                "<B>{0} - {1}</B> ({2})<BR />Posti occupati: <B>{3} / {4}</B><BR /><A href=\"{5}\"> Visualizza Mappa</A>",
                                Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                SchedulazioneFine.Value.ToString("HH:mm"),
                                Locazione.IndirizzoCompleto,
                                Partecipanti,
                                MaxPartecipanti,
                                string.Format("MappaLocazione.aspx?idLocazione={0}", Locazione.IdLocazione));
                    }
                    return string.Format("<B>{0} - {1}</B> ({2})<BR />Posti occupati: <B>{3} / {4}</B><BR />",
                        Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                        Schedulazione.Value.ToString("HH:mm"),
                        Locazione.IndirizzoCompleto,
                        Partecipanti,
                        MaxPartecipanti);
                }
                return string.Empty;
            }
        }

        public string SchedulazioneStringaImpresa
        {
            get
            {
                if (Schedulazione.HasValue)
                {
                    if (Locazione.Latitudine.HasValue && Locazione.Longitudine.HasValue)
                    {
                        return
                            string.Format(
                                "<B>{0} - {1}</B> ({2})<BR />Posti residui: <B>{3}</B><BR /><A href=\"{4}\"> Visualizza Mappa</A>",
                                Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                                SchedulazioneFine.Value.ToString("HH:mm"),
                                Locazione.IndirizzoCompleto,
                                MaxPartecipanti - Partecipanti,
                                string.Format("MappaLocazione.aspx?idLocazione={0}", Locazione.IdLocazione));
                    }
                    return string.Format("<B>{0} - {1}</B> ({2})<BR />",
                        Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"),
                        SchedulazioneFine.Value.ToString("HH:mm"),
                        Locazione.IndirizzoCompleto);
                }
                return string.Empty;
            }
        }

        public Locazione Locazione { get; set; }

        public int? IdProgrammazione { get; set; }

        public int Partecipanti { get; set; }

        public int MaxPartecipanti { get; set; }

        public bool PresenzeConfermate { get; set; }

        public bool Prenotabile { get; set; }

        public string IdComposto => string.Format("{0}|{1}", IdProgrammazioneModulo, IdProgrammazione);

        public static int IdProgrammazioneModuloDaIdComposto(string idComposto)
        {
            string[] idCompostoSplitted = idComposto.Split('|');
            return int.Parse(idCompostoSplitted[0]);
        }

        public static int IdProgrammazioneDaIdComposto(string idComposto)
        {
            string[] idCompostoSplitted = idComposto.Split('|');
            return int.Parse(idCompostoSplitted[1]);
        }
    }
}