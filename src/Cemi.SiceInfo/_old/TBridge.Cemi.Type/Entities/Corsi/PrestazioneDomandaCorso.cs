using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class PrestazioneDomandaCorso
    {
        public int IdCorsiPrestazioneDomanda { get; set; }

        /// <summary>
        ///     E' il codice lavoratore di SICENEW, se NULL indica che il lavoratore non � in anagrafica
        /// </summary>
        public int? IdLavoratore { get; set; }

        /// <summary>
        ///     Nome del lavoratore
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        ///     Cognome del lavoratore
        /// </summary>
        public string Cognome { get; set; }

        public string CodiceFiscale { get; set; }

        /// <summary>
        ///     Data nascita del lavoratore
        /// </summary>
        public DateTime? DataNascita { get; set; }

        public bool CodiceFiscaleUnico { get; set; }

        public bool DenunciaPresente { get; set; }

        public bool CartaPrepagataPresente { get; set; }

        public DateTime? InizioCorso { get; set; }

        public DateTime? FineCorso { get; set; }

        /// <summary>
        ///     Data in cui � stata confermata la domanda
        /// </summary>
        public DateTime? DataConferma { get; set; }

        /// <summary>
        ///     Data in cui la domanda � passata in stato L
        /// </summary>
        public DateTime? DataLiquidazione { get; set; }

        public StatoDomanda Stato { get; set; }

        public string IdTIpoPrestazione { get; set; }

        public int? NumeroProtocolloPrestazione { get; set; }
        public int? ProtocolloPrestazione { get; set; }

        public bool PosizioneValida { get; set; }
    }

    //E' replicato rispetto a Prestazioni.Entities, bisognerebbe trovare il modo di creare dei type comuni o meglio ancora delle entit� condivise
    [Serializable]
    public class StatoDomanda
    {
        public StatoDomanda()
        {
        }

        public StatoDomanda(string idStato, string descrizione)
        {
            IdStato = idStato;
            Descrizione = descrizione;
        }

        public string Descrizione { get; set; }
        public string IdStato { get; set; }

        #region Metodi statici

        /// <summary>
        ///     Ritorna true se la domanda � in uno stato tale per cui pu� essere gestita dall'utente
        /// </summary>
        /// <param name="idStato">Stato da controllare</param>
        /// <returns>True, false</returns>
        public static bool StatoDomandaModificabile(string idStato)
        {
            if (idStato == "I" || idStato == "T" || idStato == "O")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Ritorna true se la domanda � in uno stato definitivo e quindi la domanda non � pi� modificabile dall'utente
        /// </summary>
        /// <param name="idStato">Stato da controllare</param>
        /// <returns>True, false</returns>
        public static bool StatoDomandaDefinitivo(string idStato)
        {
            return !StatoDomandaModificabile(idStato);
        }

        #endregion
    }
}