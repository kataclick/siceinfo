﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class TipoIscrizione
    {
        public int IdTipoIscrizione { get; set; }

        public string Descrizione { get; set; }
    }
}