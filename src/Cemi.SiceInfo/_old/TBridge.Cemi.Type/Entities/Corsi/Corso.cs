﻿using System;
using TBridge.Cemi.Type.Collections.Corsi;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Corso
    {
        public Corso()
        {
            Moduli = new ModuloCollection();
        }

        public int? IdCorso { get; set; }

        public string Codice { get; set; }

        public string Descrizione { get; set; }

        public string Versione { get; set; }

        public short DurataComplessiva { get; set; }

        public string NomeCompleto => string.Format("{0} - {1}", Codice, Descrizione);

        public ModuloCollection Moduli { get; set; }

        public int NumeroPianificazioni { get; set; }

        public bool IscrizioneLibera { get; set; }

        public string ReportAttestato { get; set; }

        public string ReportAttestati { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Codice, Descrizione);
        }
    }
}