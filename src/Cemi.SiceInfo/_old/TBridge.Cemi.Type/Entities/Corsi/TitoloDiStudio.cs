﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class TitoloDiStudio
    {
        public int IdTitoloStudio { get; set; }

        public string Descrizione { get; set; }
    }
}