﻿using System;
using TBridge.Cemi.Type.Collections.Corsi;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Programmazione
    {
        public int? IdProgrammazione { get; set; }

        public Corso Corso { get; set; }

        public ProgrammazioneModuloCollection ProgrammazioniModuli { get; set; }
    }
}