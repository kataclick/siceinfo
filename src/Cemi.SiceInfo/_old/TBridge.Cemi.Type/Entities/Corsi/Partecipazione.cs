﻿using System;
using TBridge.Cemi.Type.Collections.Corsi;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Partecipazione
    {
        public int? IdPartecipazione { get; set; }

        public Corso Corso { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public bool AttestatoRilasciabile { get; set; }

        public PartecipazioneModuloCollection PartecipazioneModuli { get; set; }

        public bool PrenotazioneImpresa { get; set; }

        public bool PrenotazioneConsulente { get; set; }

        public int? IdConsulente { get; set; }
    }
}