﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class EstrazioneFormedilImpresa
    {
        public string Corso { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public int? TipoContratto { get; set; }

        public int TipoIscrizione { get; set; }

        public int? NumeroDipendenti { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public int NumeroLavoratori { get; set; }

        public string DateCorso { get; set; }
    }
}