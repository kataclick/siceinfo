using System;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.Type.Enums.Corsi;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class Lavoratore : LavoratoreAnagraficaComune
    {
        public Lavoratore()
        {
            Fonte = FonteAnagraficheComuni.IscrizioneLavoratori;
        }

        public TipologiaLavoratore TipoLavoratore { get; set; }

        public bool Presente { get; set; }

        public bool Attestato { get; set; }

        public bool PrenotazioneImpresa { get; set; }

        public bool PrenotazioneConsulente { get; set; }

        public int IdPartecipazione { get; set; }

        public int IdPartecipazioneModulo { get; set; }

        public Impresa Impresa { get; set; }

        public bool RegistroConfermato { get; set; }

        public string Cittadinanza { get; set; }

        public string ProvinciaResidenza { get; set; }

        public TitoloDiStudio TitoloStudio { get; set; }

        public int IdCorso { get; set; }

        public string CodiceCorso { get; set; }

        public string IndirizzoDenominazione { get; set; }

        public string IndirizzoComune { get; set; }

        public string IndirizzoProvincia { get; set; }

        public string IndirizzoCap { get; set; }

        public string IndirizzoCompleto => string.Format("{0} {1} {2}",
            IndirizzoDenominazione,
            IndirizzoComune,
            IndirizzoProvincia);

        public bool? PrimaEsperienza { get; set; }

        public DateTime? DataAssunzione { get; set; }

        public int OreDenunciate { get; set; }

        public DateTime? DataInizioCorso { get; set; }

        public bool? Diritto { get; set; }

        public string Gestore { get; set; }

        public int IdAnagraficaCondivisa { get; set; }

        public string CodiceCatastaleComuneNascita { get; set; }

        public string Telefono { get; set; }

        public string Cellulare { get; set; }

        public string Email { get; set; }
    }
}