﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    public class PrestazioneDTA
    {
        public string CodiceFiscale { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public DateTime DataInizioCorso { get; set; }

        public DateTime? DataFineCorso { get; set; }

        public string CodiceCorso { get; set; }

        public int? IdLavoratore { get; set; }

        public DateTime? DataErogazione { get; set; }

        public bool DenunciaPresente { get; set; }

        public bool IbanPresente { get; set; }

        public bool Diritto
        {
            get
            {
                bool diritto = DenunciaPresente;
                return diritto;
            }
        }

        public bool Selezionato { get; set; }

        public int? ProtocolloPrestazione { get; set; }

        public int? NumeroProtocolloPrestazione { get; set; }

        public string ProtocolloPrestazioneCompleto
        {
            get
            {
                string prot = null;

                if (ProtocolloPrestazione.HasValue
                    && NumeroProtocolloPrestazione.HasValue)
                {
                    prot = string.Format("{0}/{1}", ProtocolloPrestazione, NumeroProtocolloPrestazione);
                }

                return prot;
            }
        }

        public string IdStato { get; set; }

        public string DescrizioneStato { get; set; }
    }
}