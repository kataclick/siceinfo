﻿using System;

namespace TBridge.Cemi.Type.Entities.Corsi
{
    [Serializable]
    public class TipoContratto
    {
        public int IdTipoContratto { get; set; }

        public string Descrizione { get; set; }
    }
}