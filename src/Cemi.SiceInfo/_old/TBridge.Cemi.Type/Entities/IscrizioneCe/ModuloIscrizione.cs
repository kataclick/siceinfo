using System;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.IscrizioneCe;
using Lavoratore = TBridge.Cemi.Type.Entities.GestioneUtenti.Lavoratore;

namespace TBridge.Cemi.Type.Entities.IscrizioneCe
{
    public class ModuloIscrizione
    {
        public ModuloIscrizione()
        {
            LegaleRappresentante = new Lavoratore();
            Cantiere = new Cantiere();
            SedeLegale = new Sede();
            SedeAmministrativa = new Sede();
            Impresa = new GestioneUtenti.Impresa();
        }

        public string IndirizzoConsulente { get; set; }

        public string ProvinciaConsulente { get; set; }

        public string CodiceCatastaleConsulente { get; set; }

        public string CapConsulente { get; set; }

        public string TelefonoConsulente { get; set; }

        public string FaxConsulente { get; set; }

        public string EmailConsulente { get; set; }

        public string PecConsulente { get; set; }

        public string CodiceFiscaleConsulente { get; set; }

        public string ComuneConsulente { get; set; }

        public int? CodiceConsulente { get; set; }

        public string RagSocConsulente { get; set; }

        public string IBAN { get; set; }

        public string IntestatarioConto { get; set; }

        public TipoModulo TipoModulo { get; set; }

        public string IdTipoInvioDenuncia { get; set; }

        public string IdTipoIscrizione { get; set; }

        public string DescrizioneTipoInvioDenuncia { get; set; }

        public string DescrizioneTipoIscrizione { get; set; }

        public string UtenteCompilazione
        {
            get
            {
                if (CompilazioneConsulente && IdConsulente.HasValue)
                    return RagioneSocialeConsulente;
                if (IdUtente.HasValue)
                    return Utente;
                return "Impresa";
            }
        }

        public string RagioneSocialeConsulente { get; set; }

        public bool CompilazioneConsulente { get; set; }

        public int? IdUtente { get; set; }

        public string Utente { get; set; }

        public string PosizioneOrganizzazioneImprenditoriale { get; set; }

        public int? IdConsulente { get; set; }

        public Consulente Consulente { get; set; }

        public string CodiceOrganizzazioneImprenditoriale { get; set; }

        public string DescrizioneOrganizzazioneImprenditoriale { get; set; }

        public string NumeroIscrizioneCameraCommercio { get; set; }

        public string NumeroIscrizioneImpreseArtigiane { get; set; }

        public DateTime? DataIscrizioneCommercioArtigiane { get; set; }

        public string CodiceTipoImpresa { get; set; }

        public string DescrizioneTipoImpresa { get; set; }

        public string CodiceNaturaGiuridica { get; set; }

        public string DescrizioneNaturaGiuridica { get; set; }

        public string CodiceAttivitaISTAT { get; set; }

        public string DescrizioneAttivitaISTAT { get; set; }

        public int? IdDomanda { get; set; }

        public string RagioneSociale
        {
            get
            {
                if (Impresa != null)
                    return Impresa.RagioneSociale;
                return string.Empty;
            }
        }

        public string PartitaIva
        {
            get
            {
                if (Impresa != null)
                    return Impresa.PartitaIVA;
                return string.Empty;
            }
        }

        public string CodiceFiscale
        {
            get
            {
                if (Impresa != null)
                    return Impresa.CodiceFiscale;
                return string.Empty;
            }
        }

        public string IdImpresa
        {
            get
            {
                if (Impresa != null && Impresa.IdImpresa > 0)
                    return Impresa.IdImpresa.ToString();
                return string.Empty;
            }
        }

        public bool Confermata { get; set; }

        public StatoDomanda? Stato { get; set; }

        public Sede SedeLegale { get; set; }

        public Sede SedeAmministrativa { get; set; }

        public GestioneUtenti.Impresa Impresa { get; set; }

        public Lavoratore LegaleRappresentante { get; set; }

        //public Banca Banca
        //{
        //    get { return banca; }
        //    set { banca = value; }
        //}

        public Cantiere Cantiere { get; set; }

        public int? NumeroOpeImp { get; set; }

        public int? NumeroOpe { get; set; }

        public DateTime? DataRichiestaIscrizione { get; set; }

        public string CorrispondenzaPresso { get; set; }

        public bool GeocodificaOkSedeLegale { get; set; }

        public bool GeocodificaOkSedeAmministrativa { get; set; }

        public bool GeocodificaOkCantiere { get; set; }

        public bool CantiereInNotifiche { get; set; }

        public DateTime DataPresentazione { get; set; }

        public string IdCassaEdile { get; set; }

        public Guid guid { get; set; }

        public string PEC { get; set; }
    }
}