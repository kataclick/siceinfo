namespace TBridge.Cemi.Type.Entities.IscrizioneCe
{
    public class TipoIscrizione
    {
        private const string carattereSeparatore = "|";

        public string IdTipoImpresa { get; set; }

        public string IdTipoIscrizione { get; set; }

        public string IdComposto => string.Format("{0}{1}{2}", IdTipoImpresa, carattereSeparatore, IdTipoIscrizione);

        public string Descrizione { get; set; }
    }
}