using System;

namespace TBridge.Cemi.Type.Entities.IscrizioneCe
{
    [Serializable]
    public class Banca
    {
        private string abi;
        private string agenzia;
        private string banca;
        private string cab;
        private string cap;
        private string cc;
        private string cin;
        private string cinPaese;
        private string civico;
        private string comune;
        private string comuneDescrizione;
        private string fax;
        private string frazione;
        private int? idBanca;
        private string indirizzo;
        private string intestatario;
        private string paese;
        private string preIndirizzo;
        private string preIndirizzoDescrizione;
        private string provincia;
        private string telefono;

        public int? IdBanca
        {
            get => idBanca;
            set => idBanca = value;
        }

        public string Cin
        {
            get => cin;
            set => cin = value;
        }

        public string Abi
        {
            get => abi;
            set => abi = value;
        }

        public string Cab
        {
            get => cab;
            set => cab = value;
        }

        public string Cc
        {
            get => cc;
            set => cc = value;
        }

        public string Intestatario
        {
            get => intestatario;
            set => intestatario = value;
        }

        public string Banca1
        {
            get => banca;
            set => banca = value;
        }

        public string Agenzia
        {
            get => agenzia;
            set => agenzia = value;
        }

        public string PreIndirizzo
        {
            get => preIndirizzo;
            set => preIndirizzo = value;
        }

        public string PreIndirizzoDescrizione
        {
            get => preIndirizzoDescrizione;
            set => preIndirizzoDescrizione = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Civico
        {
            get => civico;
            set => civico = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string ComuneDescrizione
        {
            get => comuneDescrizione;
            set => comuneDescrizione = value;
        }

        public string Frazione
        {
            get => frazione;
            set => frazione = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string IndirizzoCompleto => string.Format("{0} {1} {2}", PreIndirizzoDescrizione, Indirizzo, Civico);

        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        public string Fax
        {
            get => fax;
            set => fax = value;
        }

        public string CinPaese
        {
            get => cinPaese;
            set => cinPaese = value;
        }

        public string Paese
        {
            get => paese;
            set => paese = value;
        }
    }
}