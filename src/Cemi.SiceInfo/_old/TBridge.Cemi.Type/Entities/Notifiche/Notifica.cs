using System;

namespace TBridge.Cemi.Type.Entities.Notifiche
{
    public enum TipoNotifica
    {
        Assunzione,
        CessazioneNotifica,
        CessazioneDenuncia,
        Visualizzazione
    }

    public enum TipoErrore
    {
        DenunciaPresente,
        NotificaPresente,
        NonPresente
    }

    public class Notifica
    {
        #region Propriet�

        private DateTime? dataCessazione;

        private int? idNotifica;

        public TipoNotifica TipoNotifica { get; }

        public int? IdNotifica
        {
            get => idNotifica;
            set
            {
                if (idNotifica == null) idNotifica = value;
            }
        }

        public int IdLavoratore { get; }

        public string Cognome { get; }

        public string Nome { get; }

        public DateTime DataNascita { get; }

        public string LuogoNascita { get; }

        public string Sesso { get; }

        public string CodiceFiscale { get; }

        public int IdImpresa { get; }

        public string RagioneSociale { get; }


        public DateTime? DataAssunzione { get; }

        public DateTime? DataCessazione
        {
            get => dataCessazione;
            set
            {
                if (dataCessazione == null) dataCessazione = value;
                else throw new Exception("Data cessazione gi� impostata");
            }
        }

        #endregion

        #region Costruttori

        public Notifica(int idLavoratore, int idImpresa, DateTime dataCessazione)
        {
            IdLavoratore = idLavoratore;
            IdImpresa = idImpresa;
            this.dataCessazione = dataCessazione;
            TipoNotifica = TipoNotifica.CessazioneDenuncia;
        }

        public Notifica(string cognome, string nome, DateTime dataNascita, string luogoNascita, string sesso,
            string codiceFiscale, DateTime dataAssunzione, int idImpresa)
        {
            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            LuogoNascita = luogoNascita;
            Sesso = sesso;
            CodiceFiscale = codiceFiscale;
            DataAssunzione = dataAssunzione;
            IdImpresa = idImpresa;
            dataCessazione = null;
            TipoNotifica = TipoNotifica.Assunzione;
        }

        public Notifica(TipoNotifica tipo, int idNotLav, string cognome, string nome, DateTime dataNascita,
            string luogoNascita, string sesso, string codiceFiscale, DateTime? dataAssunzione,
            DateTime? dataCessazione, int idImpresa, string ragioneSociale)
        {
            switch (tipo)
            {
                case TipoNotifica.CessazioneDenuncia:
                    IdLavoratore = idNotLav;
                    break;
                case TipoNotifica.CessazioneNotifica:
                    idNotifica = idNotLav;
                    break;
            }

            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            LuogoNascita = luogoNascita;
            Sesso = sesso;
            CodiceFiscale = codiceFiscale;
            DataAssunzione = dataAssunzione;
            this.dataCessazione = dataCessazione;
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            TipoNotifica = tipo;
        }

        public Notifica(string cognome, string nome, string codiceFiscale, DateTime? dataAssunzione)
        {
            TipoNotifica = TipoNotifica.Visualizzazione;
            Cognome = cognome;
            Nome = nome;
            CodiceFiscale = codiceFiscale;
            DataAssunzione = dataAssunzione;
        }

        #endregion
    }
}