namespace TBridge.Cemi.Type.Entities.Notifiche
{
    public class Impresa
    {
        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string NomeComposto => $"{IdImpresa} - {RagioneSociale}";
    }
}