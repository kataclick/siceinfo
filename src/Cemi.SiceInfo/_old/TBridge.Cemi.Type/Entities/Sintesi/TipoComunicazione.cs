namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class TipoComunicazione
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}