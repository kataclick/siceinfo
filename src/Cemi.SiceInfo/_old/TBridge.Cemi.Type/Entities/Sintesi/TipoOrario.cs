namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class TipoOrario
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}