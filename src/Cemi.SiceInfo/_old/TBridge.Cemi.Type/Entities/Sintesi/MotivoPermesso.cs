namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class MotivoPermesso
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}