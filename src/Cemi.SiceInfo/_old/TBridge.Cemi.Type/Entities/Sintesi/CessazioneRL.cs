namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class CessazioneRL
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}