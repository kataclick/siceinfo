namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class CCNL
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}