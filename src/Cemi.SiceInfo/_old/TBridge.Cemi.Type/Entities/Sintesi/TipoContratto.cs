namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class TipoContratto
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}