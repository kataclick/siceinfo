namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class TitoloDiStudio
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}