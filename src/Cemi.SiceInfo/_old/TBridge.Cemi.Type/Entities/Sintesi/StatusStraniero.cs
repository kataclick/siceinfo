namespace TBridge.Cemi.Type.Entities.Sintesi
{
    public class StatusStraniero
    {
        public string Codice { get; set; }

        public string Descrizione { get; set; }
    }
}