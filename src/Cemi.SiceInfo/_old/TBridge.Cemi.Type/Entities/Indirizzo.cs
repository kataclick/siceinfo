﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class Indirizzo
    {
        /// <summary>
        ///     Identificativo: codice numerico
        /// </summary>
        /// <example>
        ///     3243
        /// </example>
        public int Identificativo { get; set; }

        /// <summary>
        ///     Tipologia: Via, Piazza, etc.
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: Piazza
        /// </example>
        public string Qualificatore { get; set; }

        /// <summary>
        ///     Nome della via
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: della Vittoria
        /// </example>
        public string NomeVia { get; set; }

        /// <summary>
        ///     Civico
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: 11A
        /// </example>
        public string Civico { get; set; }

        /// <summary>
        ///     Comune
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: Genova
        /// </example>
        public string Comune { get; set; }

        /// <summary>
        ///     Codice Catastale del Comune
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: D969
        /// </example>
        public string ComuneCodiceCatastale { get; set; }

        /// <summary>
        ///     CAP
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: 16121
        /// </example>
        public string Cap { get; set; }

        /// <summary>
        ///     Provincia
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: GE
        /// </example>
        public string Provincia { get; set; }

        /// <summary>
        ///     Stato
        /// </summary>
        /// <example>
        ///     Per la sede di T Bridge sarebbe: Italia/Italy
        /// </example>
        public string Stato { get; set; }

        public string InformazioniAggiuntive { get; set; }

        /// <summary>
        ///     Qualificatore + NomeVia: ex. Piazza della Vittoria
        /// </summary>
        /// <example>Per la sede di T Bridge sarebbe: Piazza della Vittoria</example>
        public string Via => !string.IsNullOrEmpty(Qualificatore)
            ? $"{Qualificatore} {NomeVia}"
            : NomeVia;

        /// <summary>
        ///     Qualificatore + NomeVia + Civico: ex. Piazza della Vittoria 11A
        /// </summary>
        /// <example>Per la sede di T Bridge sarebbe: Piazza della Vittoria 11A</example>
        public string IndirizzoBase => !string.IsNullOrEmpty(Civico) ? $"{Via} {Civico}" : Via;

        /// <summary>
        ///     CAP + Comune + Provincia: ex. 16121 Genova GE
        /// </summary>
        /// <example>Per la sede di T Bridge sarebbe: 16121 Genova GE</example>
        public string Localita => $"{Cap} {Comune} {Provincia}";

        public string IndirizzoCompleto => !string.IsNullOrEmpty(InformazioniAggiuntive)
            ? $"{IndirizzoBase}{Environment.NewLine}{Localita} ({InformazioniAggiuntive})"
            : $"{IndirizzoBase}{Environment.NewLine}{Localita}";

        public override string ToString()
        {
            return IndirizzoCompleto;
        }
    }
}