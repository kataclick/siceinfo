namespace TBridge.Cemi.Type.Entities
{
    public class ComprensorioSindacale : TipiDeleghe<string, string>
    {
        public ComprensorioSindacale()
        {
        }

        public ComprensorioSindacale(string _id, string _descrizione)
        {
            id = _id;
            descrizione = _descrizione;
        }

        public override string ToString()
        {
            return descrizione;
        }
    }
}