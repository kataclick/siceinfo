using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoCartaPrepagata
    {
        public string IdTipoCartaPrepagata { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}