using System;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.Deleghe;

namespace TBridge.Cemi.Type.Entities.Deleghe
{
    [Serializable]
    public class Delega
    {
        public Delega()
        {
            Lavoratore = new Lavoratore();
            Lavoratore.Residenza = new Indirizzo();
            Cantiere = new Indirizzo();
        }

        public Delega(Lavoratore lavoratore, Indirizzo cantiere, Sindacalista responsabileInserimento,
            string operatoreTerritorio, Sindacato sindacato, ComprensorioSindacale comprensorio)
        {
            Lavoratore = lavoratore;
            Cantiere = cantiere;
            OperatoreInserimento = responsabileInserimento;
            OperatoreTerritorio = operatoreTerritorio;
            Sindacato = sindacato;
            ComprensorioSindacale = comprensorio;
        }

        public int? IdDelega { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public string NomeLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return string.Format("{0} {1}", Lavoratore.Cognome, Lavoratore.Nome);
                return string.Empty;
            }
        }

        public string LavoratoreCognome
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Cognome;
                return string.Empty;
            }
        }

        public string LavoratoreNome
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Nome;
                return string.Empty;
            }
        }

        public string LavoratoreCellulare
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Cellulare;
                return string.Empty;
            }
        }

        public string LavoratoreId
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.IdLavoratore.ToString();
                return string.Empty;
            }
        }

        public string DataNascitaLavoratore
        {
            get
            {
                if (Lavoratore != null && Lavoratore.DataNascita.HasValue)
                    return Lavoratore.DataNascita.Value.ToShortDateString();
                return string.Empty;
            }
        }

        public string LavoratoreCodiceFiscale
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.CodiceFiscale;
                return string.Empty;
            }
        }

        public string IdImpresaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.IdImpresa.ToString();
                return string.Empty;
            }
        }

        public string ImpresaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.Impresa;
                return string.Empty;
            }
        }

        public string ResidenzaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                    return Lavoratore.ResidenzaCompleto;
                return string.Empty;
            }
        }

        public string IndirizzoDenominazioneLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.IndirizzoDenominazione;
                }
                return string.Empty;
            }
        }

        public string ComuneLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Comune;
                }
                return string.Empty;
            }
        }

        public string CapLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Cap;
                }
                return string.Empty;
            }
        }

        public string ProvinciaLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Provincia;
                }
                return string.Empty;
            }
        }

        public string CivicoLavoratore
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.Residenza.Civico;
                }
                return string.Empty;
            }
        }

        public Indirizzo Cantiere { get; set; }

        public string IndirizzoCantiere
        {
            get
            {
                if (Cantiere != null)
                    return Cantiere.IndirizzoCompleto;
                return string.Empty;
            }
        }

        public Sindacalista OperatoreInserimento { get; set; }

        public string OperatoreTerritorio { get; set; }

        public ComprensorioSindacale ComprensorioSindacale { get; set; }

        public string ComprensorioStringa
        {
            get
            {
                if (ComprensorioSindacale != null)
                    return ComprensorioSindacale.Descrizione;
                return string.Empty;
            }
        }

        public Sindacato Sindacato { get; set; }

        public StatoDelega? Stato { get; set; }

        public DateTime? DataConferma { get; set; }

        public string CodiceSblocco { get; set; }

        public DateTime? DataAdesione { get; set; }

        public string IdArchidoc { get; set; }

        public DateTime DataInserimento { get; set; }

        public string NomeAllegatoLettera { get; set; }

        public string NomeAllegatoBusta { get; set; }

        public bool Stampata { get; set; }

        public string AnagraficaResidenzaIndirizzo { get; set; }

        public string AnagraficaResidenzaComune { get; set; }

        public string AnagraficaResidenzaProvincia { get; set; }

        public string AnagraficaResidenzaCap { get; set; }

        public string AnagraficaResidenzaCivico { get; set; }

        public string CodiceDelega { get; set; }
    }
}