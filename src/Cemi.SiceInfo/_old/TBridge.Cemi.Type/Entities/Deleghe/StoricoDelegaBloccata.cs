﻿using System;

namespace TBridge.Cemi.Type.Entities.Deleghe
{
    public class StoricoDelegaBloccata
    {
        public int IdDelega { get; set; }
        public DateTime DataVariazione { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascita { get; set; }
        public string Utente { get; set; }
    }
}