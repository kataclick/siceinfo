namespace TBridge.Cemi.Type.Entities.Deleghe
{
    public class Indirizzo : Entities.Indirizzo
    {
        public string IndirizzoDenominazione
        {
            get => Via;
            set => NomeVia = value;
        }

        public new string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(Provincia))
                    return string.Format("{0} {1} {2} ({3}) {4}", IndirizzoDenominazione, Civico, Comune, Provincia,
                        Cap);
                return string.Format("{0} {1} {2} {3}", IndirizzoDenominazione, Civico, Comune, Cap);
            }
        }
    }
}