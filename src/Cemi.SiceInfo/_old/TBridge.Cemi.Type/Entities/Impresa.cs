namespace TBridge.Cemi.Type.Entities
{
    public class Impresa
    {
        public int IdImpresa { get; set; }

        public int IdUtente { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }


        /// <summary>
        ///     Indirizzo della sede legale dell'impresa
        /// </summary>
        public string IndirizzoSedeLegale { get; set; }

        /// <summary>
        ///     CAP della sede legale dell'impresa
        /// </summary>
        public string CapSedeLegale { get; set; }

        /// <summary>
        ///     Localit� della sede legale dell'impresa
        /// </summary>
        public string LocalitaSedeLegale { get; set; }

        /// <summary>
        ///     Provincia della sede legale dell'impresa
        /// </summary>
        public string ProvinciaSedeLegale { get; set; }

        /// <summary>
        ///     Email della sede legale dell'impresa
        /// </summary>
        public string EmailSedeLegale { get; set; }


        public string NomeComposto => string.Format("{0} - {1}", IdImpresa, RagioneSociale);

        public override string ToString()
        {
            return NomeComposto;
        }
    }
}