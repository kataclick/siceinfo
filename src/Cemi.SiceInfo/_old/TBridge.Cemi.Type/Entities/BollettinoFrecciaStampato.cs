﻿using System;

namespace TBridge.Cemi.Type.Entities
{
    public class BollettinoFrecciaStampato : BollettinoFreccia
    {
        public string RagioneSociale { get; set; }

        public DateTime? DataVersamento { get; set; }

        public DateTime DataRichiesta { get; set; }
    }
}