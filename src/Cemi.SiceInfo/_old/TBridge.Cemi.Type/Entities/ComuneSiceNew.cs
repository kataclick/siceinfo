using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class ComuneSiceNew
    {
        private string cap;
        private string codiceCatastale;
        private string comune;
        private string provincia;

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string CodiceCatastale
        {
            get => codiceCatastale;
            set => codiceCatastale = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string CodicePerCombo => string.Format("{0}|{1}", codiceCatastale, cap);
    }
}