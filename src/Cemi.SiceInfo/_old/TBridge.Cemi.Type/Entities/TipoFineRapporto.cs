using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoFineRapporto
    {
        public string IdTipoFineRapporto { get; set; }

        public string Descrizione { get; set; }
    }
}