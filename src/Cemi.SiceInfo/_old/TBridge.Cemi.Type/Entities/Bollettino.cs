﻿using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    public class Bollettino
    {
        public int IdImpresa { get; set; }
        public int Anno { get; set; }
        public int Mese { get; set; }
        public int Sequenza { get; set; }
        public decimal Importo { get; set; }
        public virtual TipiCanalePagamento TipoCanalePagamento { set; get; }

        public decimal ImportoDovuto0 { get; set; }
        public decimal ImportoDovuto1 { get; set; }
        public decimal ImportoDovuto2 { get; set; }
        public decimal ImportoDovuto3 { get; set; }
        public decimal ImportoDovuto4 { get; set; }
        public decimal ImportoDovuto5 { get; set; }
    }
}