using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoLingua
    {
        public string IdLingua { get; set; }

        public string Descrizione { get; set; }
    }
}