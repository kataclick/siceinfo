namespace TBridge.Cemi.Type.Entities
{
    public class TipiDeleghe<TKey, TDescription>
    {
        protected TDescription descrizione;
        protected TKey id;

        public TKey Id
        {
            get => id;
            set => id = value;
        }

        public TDescription Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }
    }
}