﻿namespace TBridge.Cemi.Type.Entities
{
    public class OreMensiliCNCE
    {
        public int IdLavoratore { get; set; }

        public string IdCassaEdile { get; set; }

        public int Anno { get; set; }

        public int Mese { get; set; }

        public int OreLavorate { get; set; }

        public int OreFerie { get; set; }

        public int OreInfortunio { get; set; }

        public int OreMalattia { get; set; }

        public int OreCassaIntegrazione { get; set; }

        public int OrePermessoRetribuito { get; set; }

        public int OrePermessoNonRetribuito { get; set; }

        public int OreAltro { get; set; }

        public int LivelloErogazione { get; set; }

        public bool InseriteManualmente { get; set; }
    }
}