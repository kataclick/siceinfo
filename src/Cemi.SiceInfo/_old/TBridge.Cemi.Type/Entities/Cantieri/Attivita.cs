using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class Attivita
    {
        public Attivita(int? idAttivita, DateTime giorno, DateTime ora, string nomeAttivita, string descrizione,
            Ispettore ispettore, Cantiere cantiere, bool programmato, bool preventivato, bool consuntivato)
        {
            IdAttivita = idAttivita;
            Giorno = giorno;
            Ora = ora;
            NomeAttivita = nomeAttivita;
            Descrizione = descrizione;
            Ispettore = ispettore;
            Cantiere = cantiere;
            Programmato = programmato;
            Preventivato = preventivato;
            Consuntivato = consuntivato;
        }

        public int? IdAttivita { set; get; }

        public DateTime Giorno { get; }

        public DateTime Ora { get; }

        public string NomeAttivita { get; }

        public string Descrizione { get; }

        public Ispettore Ispettore { get; }

        public string NomeIspettore
        {
            get
            {
                if (Ispettore != null) return Ispettore.NomeCompleto;
                return string.Empty;
            }
        }

        public Cantiere Cantiere { get; }

        public bool Programmato { get; }

        public bool Preventivato { get; }

        public bool Consuntivato { get; }

        public string NomeCompletoCantiere
        {
            get
            {
                if (Cantiere != null)
                    return
                        Cantiere.Indirizzo + " " + Cantiere.Civico + " - " + Cantiere.Comune + " " + Cantiere.Provincia;
                return string.Empty;
            }
        }
    }
}