using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Zona
    {
        private int? idZona;

        public Zona(int? idZona, string nome, string descrizione, List<CAP> listaCap)
        {
            this.idZona = idZona;
            Nome = nome;
            Descrizione = descrizione;

            if (listaCap != null)
                ListaCap = listaCap;
            else
                ListaCap = new List<CAP>();
        }

        public int? IdZona
        {
            set => idZona = value;
            get => idZona;
        }

        public string Nome { get; }

        public string Descrizione { get; }

        public List<CAP> ListaCap { get; }
    }
}