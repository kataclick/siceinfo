using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class SubappaltoLettera
    {
        public int? IdCantieriImpresa { get; set; }

        public TipologiaImpresa TipoImpresa
        {
            get
            {
                if (IdCantieriImpresa.HasValue) return TipologiaImpresa.Cantieri;
                return TipologiaImpresa.SiceNew;
            }
        }

        public int IdImpresa
        {
            get
            {
                if (IdCantieriImpresa.HasValue) return IdCantieriImpresa.Value;
                return IdImpresaSubappaltata.Value;
            }
        }

        public string RagioneSocialeCantieriImp { get; set; }

        public int? IdImpresaSubappaltata { get; set; }

        public string RagioneSocialeImp { get; set; }
    }
}