using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class LetteraParam
    {
        public string Protocollo { get; set; }

        public int IdIspezione { get; set; }

        public GruppoLettera GruppoLettera { get; set; }

        public int? IdImpresa { get; set; }

        public int? IdCantieriImpresa { get; set; }

        public DateTime? DataAppuntamento { get; set; }
    }
}