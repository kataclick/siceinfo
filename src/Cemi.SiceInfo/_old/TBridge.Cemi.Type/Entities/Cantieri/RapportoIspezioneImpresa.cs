using System;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class RapportoIspezioneImpresa
    {
        private bool? caschiCE;
        private decimal? contributiRecuperati;
        private DateTime? daAttuare;
        private DateTime? dataChiusura;
        private int? dimensioneAziendale;
        private int? idRapportoIspezioneImpresa;
        private Impresa impresa;
        private bool nuovaIscritta;
        private int? operaiIntegrati;
        private int? operaiRegolari;
        private int? operaiRegolarizzati;
        private int? operaiTrasformati;
        private OSSCollection oss;
        private RACCollection rac;
        private bool? scarpeCE;
        private bool statistiche = true;
        private bool? tuteCE;

        public RapportoIspezioneImpresa()
        {
        }

        public RapportoIspezioneImpresa(int? idRapportoIspezioneImpresa, Impresa impresa, DateTime? daAttuare,
            DateTime? dataChiusura,
            int? operaiRegolari, int? operaiIntegrati, int? operaiTrasformati,
            int? operaiRegolarizzati, int? dimensioneAziendale,
            bool? caschiCE, bool? tuteCE, bool? scarpeCE)
        {
            this.idRapportoIspezioneImpresa = idRapportoIspezioneImpresa;
            this.impresa = impresa;
            this.daAttuare = daAttuare;
            this.dataChiusura = dataChiusura;
            this.operaiRegolari = operaiRegolari;
            this.operaiIntegrati = operaiIntegrati;
            this.operaiTrasformati = operaiTrasformati;
            this.operaiRegolarizzati = operaiRegolarizzati;
            this.dimensioneAziendale = dimensioneAziendale;
            this.caschiCE = caschiCE;
            this.tuteCE = tuteCE;
            this.scarpeCE = scarpeCE;
        }

        public int? IdRapportoIspezioneImpresa
        {
            get => idRapportoIspezioneImpresa;
            set => idRapportoIspezioneImpresa = value;
        }

        public Impresa Impresa
        {
            get => impresa;
            set => impresa = value;
        }

        public string NomeImpresa
        {
            get
            {
                if (impresa != null)
                {
                    if (impresa.TipoImpresa == TipologiaImpresa.Cantieri)
                        return impresa.RagioneSociale;
                    return string.Format("{0} - {1}", impresa.IdImpresa, impresa.RagioneSociale);
                }
                return string.Empty;
            }
        }

        public int? IdImpresa
        {
            get
            {
                if (impresa != null)
                    return impresa.IdImpresa;
                return null;
            }
        }

        public TipologiaImpresa TipoImpresa
        {
            get
            {
                if (impresa != null)
                    return impresa.TipoImpresa;
                throw new Exception("Impresa inesistente");
            }
        }

        public DateTime? DaAttuare
        {
            get => daAttuare;
            set => daAttuare = value;
        }

        public DateTime? DataChiusura
        {
            get => dataChiusura;
            set => dataChiusura = value;
        }

        public int? OperaiRegolari
        {
            get => operaiRegolari;
            set => operaiRegolari = value;
        }

        public int? OperaiIntegrati
        {
            get => operaiIntegrati;
            set => operaiIntegrati = value;
        }

        public int? OperaiTrasformati
        {
            get => operaiTrasformati;
            set => operaiTrasformati = value;
        }

        public int? OperaiRegolarizzati
        {
            get => operaiRegolarizzati;
            set => operaiRegolarizzati = value;
        }

        public int? DimensioneAziendale
        {
            get => dimensioneAziendale;
            set => dimensioneAziendale = value;
        }

        public bool? CaschiCE
        {
            get => caschiCE;
            set => caschiCE = value;
        }

        public bool? TuteCE
        {
            get => tuteCE;
            set => tuteCE = value;
        }

        public bool? ScarpeCE
        {
            get => scarpeCE;
            set => scarpeCE = value;
        }

        public OSSCollection Oss
        {
            get => oss;
            set => oss = value;
        }

        public RACCollection Rac
        {
            get => rac;
            set => rac = value;
        }

        public bool NuovaIscritta
        {
            get => nuovaIscritta;
            set => nuovaIscritta = value;
        }

        public decimal? ContributiRecuperati
        {
            get => contributiRecuperati;
            set => contributiRecuperati = value;
        }

        public bool Statistiche
        {
            get => statistiche;
            set => statistiche = value;
        }

        public decimal? ElenchiIntegrativi { get; set; }

        public int? OperaiTrasferta { get; set; }

        public int? OperaiNoAltreCE { get; set; }

        public int? OperaiDistacco { get; set; }

        public int? OperaiAltroCCNL { get; set; }

        public CassaEdileCollection CasseEdili { get; set; }

        public ContrattoCollection Contratti { get; set; }
    }
}