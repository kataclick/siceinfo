using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class LogLettera
    {
        public string Protocollo { get; set; }

        public DateTime Giorno { get; set; }

        public string CantiereIndirizzo { get; set; }

        public string CantiereComune { get; set; }

        public string CantiereProvincia { get; set; }

        public DateTime IspezioneGiorno { get; set; }

        public GruppoLettera TipoLettera { get; set; }

        public TipologiaImpresa TipoImpresa { get; set; }

        public int? IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }
    }
}