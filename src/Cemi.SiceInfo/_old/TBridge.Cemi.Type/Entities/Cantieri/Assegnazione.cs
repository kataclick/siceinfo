﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Assegnazione
    {
        public int IdAssegnazione { get; set; }

        public int IdCantiere { get; set; }

        public DateTime Data { get; set; }

        public CantieriAssegnazioneMotivazione MotivazioneRifiuto { get; set; }

        public CantieriAssegnazionePriorita Priorita { get; set; }

        public List<Domain.Ispettore> Ispettori { get; set; }
    }
}