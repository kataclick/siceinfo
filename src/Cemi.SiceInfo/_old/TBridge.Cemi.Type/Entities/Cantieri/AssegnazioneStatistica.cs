﻿namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class AssegnazioneStatistica
    {
        public bool Assegnati { get; set; }

        public int NumeroCantieri { get; set; }

        public int NumeroPresiInCarico { get; set; }
    }
}