using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class RapportoIspezioneLavoratore
    {
        private int? idIspezioneLavoratore;
        private Impresa impresa;
        private Lavoratore lavoratore;
        private int? numeroLavoratori;

        public RapportoIspezioneLavoratore()
        {
        }

        public RapportoIspezioneLavoratore(int? idIspezioneLavoratore, Lavoratore lavoratore, Impresa impresa)
        {
            this.idIspezioneLavoratore = idIspezioneLavoratore;
            this.lavoratore = lavoratore;
            this.impresa = impresa;
        }

        public RapportoIspezioneLavoratore(int? idIspezioneLavoratore, int numeroLavoratori, Impresa impresa)
        {
            this.idIspezioneLavoratore = idIspezioneLavoratore;
            this.numeroLavoratori = numeroLavoratori;
            this.impresa = impresa;
        }

        public int? IdIspezioneLavoratore
        {
            get => idIspezioneLavoratore;
            set => idIspezioneLavoratore = value;
        }

        public Lavoratore Lavoratore
        {
            get => lavoratore;
            set => lavoratore = value;
        }

        public int? NumeroLavoratori
        {
            get => numeroLavoratori;
            set => numeroLavoratori = value;
        }

        public string NomeLavoratore
        {
            get
            {
                if (lavoratore != null) return lavoratore.Cognome + " " + lavoratore.Nome;
                return string.Empty;
            }
        }

        public string DataNascitaLavoratore
        {
            get
            {
                if (lavoratore != null && lavoratore.DataNascita.HasValue)
                    return lavoratore.DataNascita.Value.ToShortDateString();
                return string.Empty;
            }
        }

        public Impresa Impresa
        {
            get => impresa;
            set => impresa = value;
        }

        public string NomeImpresa
        {
            get
            {
                if (impresa != null) return impresa.RagioneSociale;
                return string.Empty;
            }
        }
    }
}