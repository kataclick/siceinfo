﻿using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class SegnalazioneStatistica
    {
        public CantiereSegnalazionePriorita CantieriSegnalazionePriorita { get; set; }

        public int NumeroCantieri { get; set; }

        public int NumeroAssegnatiRifiutati { get; set; }
    }
}