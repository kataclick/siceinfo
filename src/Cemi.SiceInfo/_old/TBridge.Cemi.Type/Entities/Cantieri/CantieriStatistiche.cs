﻿using TBridge.Cemi.Type.Collections.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class CantieriStatistiche
    {
        public CantieriStatistiche()
        {
            Segnalazioni = new SegnalazioneStatisticaCollection();
            Assegnazioni = new AssegnazioneStatisticaCollection();
            AssegnazioniPerIspettore = new AssegnazionePerIspettoreStatisticaCollection();
            PreseInCarico = new PresaInCaricoStatisticaCollection();
            PreseInCaricoPerIspettore = new PresaInCaricoPerIspettoreStatisticaCollection();
        }

        public SegnalazioneStatisticaCollection Segnalazioni { get; set; }

        public AssegnazioneStatisticaCollection Assegnazioni { get; set; }

        public AssegnazionePerIspettoreStatisticaCollection AssegnazioniPerIspettore { get; set; }

        public PresaInCaricoStatisticaCollection PreseInCarico { get; set; }

        public PresaInCaricoPerIspettoreStatisticaCollection PreseInCaricoPerIspettore { get; set; }
    }
}