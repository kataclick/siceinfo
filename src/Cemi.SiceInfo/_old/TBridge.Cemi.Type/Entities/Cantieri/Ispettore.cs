using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Ispettore
    {
        private string cognome;
        private int? idIspettore;
        private string nome;

        public Ispettore()
        {
        }

        public Ispettore(int? idIspettore, string cognome, string nome, Zona zona, bool operativo)
        {
            this.idIspettore = idIspettore;
            this.cognome = cognome;
            this.nome = nome;
            Zona = zona;
            Operativo = operativo;
        }

        public Ispettore(int? idIspettore, string cognome, string nome, int idZona, string nomeZona,
            string descrizioneZona, bool operativo)
        {
            this.idIspettore = idIspettore;
            this.cognome = cognome;
            this.nome = nome;
            Operativo = operativo;

            Zona = new Zona(idZona, nomeZona, descrizioneZona, null);
        }

        public int? IdIspettore
        {
            get => idIspettore;
            set => idIspettore = value;
        }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string NomeCompleto => cognome + " " + nome;

        public Zona Zona { get; }

        public bool Operativo { get; }
    }
}