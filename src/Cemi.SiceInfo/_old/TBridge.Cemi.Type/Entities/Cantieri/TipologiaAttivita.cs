namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class TipologiaAttivita
    {
        public TipologiaAttivita(string nome, string descrizione)
        {
            Nome = nome;
            Descrizione = descrizione;
        }

        public string Nome { get; }

        public string Descrizione { get; }
    }
}