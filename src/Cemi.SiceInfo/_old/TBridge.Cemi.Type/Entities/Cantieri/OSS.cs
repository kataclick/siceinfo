using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class OSS
    {
        private string descrizione;
        private int idOSS;

        public OSS(int idOSS, string descrizione)
        {
            this.idOSS = idOSS;
            this.descrizione = descrizione;
        }

        public int IdOSS
        {
            get => idOSS;
            set => idOSS = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }
    }
}