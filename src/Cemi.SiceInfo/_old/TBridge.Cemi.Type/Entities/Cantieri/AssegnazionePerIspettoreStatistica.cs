﻿namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class AssegnazionePerIspettoreStatistica
    {
        public int IdIspettore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CognomeNome => string.Format("{0} {1}", Cognome, Nome);

        public int NumeroAssegnazioni { get; set; }
    }
}