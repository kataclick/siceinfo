namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class CantiereFonte
    {
        public int Id { get; set; }

        public string Indirizzo { get; set; }

        public string Civico { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string IndirizzoCompleto => Indirizzo + " " + Civico + " " + Comune + " " + Provincia;

        public string NomeFonte { get; set; }
    }
}