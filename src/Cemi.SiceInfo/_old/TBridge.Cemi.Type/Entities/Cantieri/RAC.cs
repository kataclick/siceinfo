using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class RAC
    {
        private string descrizione;
        private string idRAC;

        public RAC(string idRAC, string descrizione)
        {
            this.idRAC = idRAC;
            this.descrizione = descrizione;
        }

        public string IdRAC
        {
            get => idRAC;
            set => idRAC = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }
    }
}