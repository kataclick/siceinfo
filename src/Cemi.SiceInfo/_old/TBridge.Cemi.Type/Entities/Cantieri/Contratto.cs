﻿using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Contratto
    {
        public int Id { get; set; }

        public string Descrizione { get; set; }
    }
}