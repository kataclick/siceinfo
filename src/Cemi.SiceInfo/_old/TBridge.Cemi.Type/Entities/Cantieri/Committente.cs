using System;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Committente
    {
        private string cap;
        private string codiceFiscale;
        private string comune;
        private string fax;
        private bool fonteNotifica;
        private int? idCommittente;
        private string indirizzo;
        private bool modificato;
        private string partitaIva;
        private string personaRiferimento;
        private string provincia;
        private string ragioneSociale;
        private string telefono;

        public Committente()
        {
        }

        public Committente(int? idCommittente, string ragioneSociale, string partitaIva, string codiceFiscale,
            string indirizzo, string comune, string provincia, string cap)
        {
            this.idCommittente = idCommittente;
            this.ragioneSociale = ragioneSociale;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;
            this.indirizzo = indirizzo;
            this.comune = comune;
            this.provincia = provincia;
            this.cap = cap;
        }

        public Committente(int? idCommittente, string ragioneSociale, string partitaIva, string codiceFiscale,
            string indirizzo, string comune, string provincia, string cap, string telefono, string fax,
            string personaRiferimento)
        {
            this.idCommittente = idCommittente;
            this.ragioneSociale = ragioneSociale;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;
            this.indirizzo = indirizzo;
            this.comune = comune;
            this.provincia = provincia;
            this.cap = cap;

            // Per parte Cpt
            this.telefono = telefono;
            this.fax = fax;
            this.personaRiferimento = personaRiferimento;
        }

        public int? IdCommittente
        {
            get => idCommittente;
            set => idCommittente = value;
        }

        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        public string PartitaIva
        {
            get => partitaIva;
            set => partitaIva = value;
        }

        public string CodiceFiscale
        {
            get => codiceFiscale;
            set => codiceFiscale = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string NomeCompleto => RagioneSociale + Environment.NewLine + Indirizzo + Environment.NewLine + Comune +
                                      " - " + Provincia +
                                      " " + Cap;

        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        public string Fax
        {
            get => fax;
            set => fax = value;
        }

        public string PersonaRiferimento
        {
            get => personaRiferimento;
            set => personaRiferimento = value;
        }

        public bool Modificato
        {
            get => modificato;
            set => modificato = value;
        }

        public bool FonteNotifica
        {
            get => fonteNotifica;
            set => fonteNotifica = value;
        }
    }
}