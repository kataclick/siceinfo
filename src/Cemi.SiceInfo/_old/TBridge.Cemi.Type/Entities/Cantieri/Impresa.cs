using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Impresa
    {
        private const string SEPARATORE_CAMPO_COMPOSTO = "|";
        private string ammiCap;
        private string ammiComune;
        private string ammiIndirizzo;
        private string ammiProvincia;
        private string cap;
        private string codiceFiscale;
        private string comune;
        private DateTime? dataFineValiditaSedeAmministrativa;
        private DateTime? dataFineValiditaSedeLegale;
        private DateTime? dataInizioValiditaSedeAmministrativa;
        private DateTime? dataInizioValiditaSedeLegale;
        private string fax;
        private bool fonteNotifica;
        private int? idImpresa;
        private int? idImpresaTemporaneo;
        private string indirizzo;
        private bool lavoratoreAutonomo;
        private bool modificato;
        private string partitaIva;
        private string personaRiferimento;
        private string provincia;
        private string ragioneSociale;
        private string telefono;
        private string tipoAttivita;

        private TipologiaImpresa tipoImpresa;

        public Impresa()
        {
        }

        public Impresa(TipologiaImpresa tipoImpresa, int? idImpresa, string ragioneSociale, string indirizzo,
            string provincia, string comune, string cap, string partitaIva, string codiceFiscale)
        {
            this.tipoImpresa = tipoImpresa;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;

            lavoratoreAutonomo = false;
            idImpresaTemporaneo = null;
        }

        public Impresa(TipologiaImpresa tipoImpresa, int? idImpresa, string ragioneSociale, string indirizzo,
            string provincia, string comune, string cap, string partitaIva, string codiceFiscale,
            string tipoAttivita, string telefono, string fax, string personaRiferimento,
            bool lavoratoreAutonomo)
        {
            this.tipoImpresa = tipoImpresa;
            this.idImpresa = idImpresa;
            this.ragioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.partitaIva = partitaIva;
            this.codiceFiscale = codiceFiscale;

            // Per parte Cpt
            this.tipoAttivita = tipoAttivita;
            this.telefono = telefono;
            this.fax = fax;
            this.personaRiferimento = personaRiferimento;
            this.lavoratoreAutonomo = lavoratoreAutonomo;
            idImpresaTemporaneo = null;
        }

        public TipologiaImpresa TipoImpresa
        {
            get => tipoImpresa;
            set => tipoImpresa = value;
        }

        public int? IdImpresa
        {
            get => idImpresa;
            set => idImpresa = value;
        }

        // Campo per Cpt, gestione subappalti

        public int? IdImpresaTemporaneo
        {
            get => idImpresaTemporaneo;
            set => idImpresaTemporaneo = value;
        }

        public bool LavoratoreAutonomo
        {
            get => lavoratoreAutonomo;
            set => lavoratoreAutonomo = value;
        }

        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string AmmiIndirizzo
        {
            get => ammiIndirizzo;
            set => ammiIndirizzo = value;
        }

        public string AmmiProvincia
        {
            get => ammiProvincia;
            set => ammiProvincia = value;
        }

        public string AmmiComune
        {
            get => ammiComune;
            set => ammiComune = value;
        }

        public string AmmiCap
        {
            get => ammiCap;
            set => ammiCap = value;
        }

        public string PartitaIva
        {
            get => partitaIva;
            set => partitaIva = value;
        }

        public string CodiceFiscale
        {
            get => codiceFiscale;
            set => codiceFiscale = value;
        }

        //private LavoratoreCollection lavoratori;
        //public LavoratoreCollection Lavoratori
        //{
        //    get { return lavoratori; }
        //    set { lavoratori = value; }
        //}

        public string TipoAttivita
        {
            get => tipoAttivita;
            set => tipoAttivita = value;
        }

        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        public string Fax
        {
            get => fax;
            set => fax = value;
        }

        public string PersonaRiferimento
        {
            get => personaRiferimento;
            set => personaRiferimento = value;
        }

        public DateTime? DataInizioValiditaSedeLegale
        {
            get => dataInizioValiditaSedeLegale;
            set => dataInizioValiditaSedeLegale = value;
        }

        public DateTime? DataFineValiditaSedeLegale
        {
            get => dataFineValiditaSedeLegale;
            set => dataFineValiditaSedeLegale = value;
        }

        public DateTime? DataInizioValiditaSedeAmministrativa
        {
            get => dataInizioValiditaSedeAmministrativa;
            set => dataInizioValiditaSedeAmministrativa = value;
        }

        public DateTime? DataFineValiditaSedeAmministrativa
        {
            get => dataFineValiditaSedeAmministrativa;
            set => dataFineValiditaSedeAmministrativa = value;
        }

        public bool FonteNotifica
        {
            get => fonteNotifica;
            set => fonteNotifica = value;
        }

        public string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(provincia))
                    return string.Format("{0} {1} ({2}) {3}", indirizzo, comune, provincia, cap);
                return string.Format("{0} {1} {2}", indirizzo, comune, cap);
            }
        }

        public string AmmiIndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(ammiProvincia))
                    return string.Format("{0} {1} ({2}) {3}", ammiIndirizzo, ammiComune, ammiProvincia, ammiCap);
                return string.Format("{0} {1} {2}", ammiIndirizzo, ammiComune, ammiCap);
            }
        }

        /// <summary>
        ///     Ritorna TipoImpresa + SEPARATORE + IdImpresa; per conoscere il separatore accedere alla proprietÓ
        ///     SeparatoreCampoComposto
        /// </summary>
        public string IdImpresaComposto => (int) TipoImpresa + SEPARATORE_CAMPO_COMPOSTO + IdImpresa;

        /// <summary>
        ///     Ritorna il valore del separatore usato nei campi composti (es IdImpresaComposto)
        /// </summary>
        public static string SeparatoreCampoComposto => SEPARATORE_CAMPO_COMPOSTO;

        public string NomeCompleto => string.Format("{0}\nLeg: {1}\nAmmi:{2}", RagioneSociale, IndirizzoCompleto,
            AmmiIndirizzoCompleto);

        public bool Modificato
        {
            get => modificato;
            set => modificato = value;
        }

        /// <summary>
        ///     Separa l'idcomposto
        /// </summary>
        /// <param name="idImpresaComposto">Id Impresa composto</param>
        /// <param name="idImpresa">valorizza idimpresa</param>
        /// <param name="tipoImpresa">valorizza tipoimpresa</param>
        public static void SplitIdImpresaComposto(string idImpresaComposto, out int idImpresa, out int tipoImpresa)
        {
            string[] impresaIds = idImpresaComposto.Split(SeparatoreCampoComposto.ToCharArray());
            idImpresa = int.Parse(impresaIds[1]);
            tipoImpresa = int.Parse(impresaIds[0]);
        }
    }
}