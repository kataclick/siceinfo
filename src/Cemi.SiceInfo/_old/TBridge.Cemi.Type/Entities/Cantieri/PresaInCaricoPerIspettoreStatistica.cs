﻿namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class PresaInCaricoPerIspettoreStatistica
    {
        public int IdIspettore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CognomeNome => string.Format("{0} {1}", Cognome, Nome);

        public int NumeroPreseInCarico { get; set; }
    }
}