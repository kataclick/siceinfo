﻿using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class PresaInCaricoStatistica
    {
        public StatoIspezione? StatoIspezione { get; set; }

        public int NumeroPreseInCarico { get; set; }

        public int NumeroRapportiIspezione { get; set; }
    }
}