using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Lavoratore
    {
        private string cognome;
        private DateTime? dataNascita;
        private int? idLavoratore;
        private string impresa;
        private string nome;
        private TipologiaLavoratore tipoLavoratore;

        public Lavoratore()
        {
        }

        public Lavoratore(TipologiaLavoratore tipoLavoratore, int? idLavoratore, string cognome, string nome,
            DateTime? dataNascita)
        {
            this.tipoLavoratore = tipoLavoratore;
            this.idLavoratore = idLavoratore;
            this.cognome = cognome;
            this.nome = nome;
            this.dataNascita = dataNascita;
        }

        public TipologiaLavoratore TipoLavoratore
        {
            get => tipoLavoratore;
            set => tipoLavoratore = value;
        }

        public int? IdLavoratore
        {
            get => idLavoratore;
            set => idLavoratore = value;
        }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public DateTime? DataNascita
        {
            get => dataNascita;
            set => dataNascita = value;
        }

        public string Impresa
        {
            get => impresa;
            set => impresa = value;
        }
    }
}