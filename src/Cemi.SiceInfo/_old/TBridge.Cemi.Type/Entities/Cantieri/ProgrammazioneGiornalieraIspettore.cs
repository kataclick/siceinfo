using System;
using TBridge.Cemi.Type.Collections.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    public class ProgrammazioneGiornalieraIspettore
    {
        public ProgrammazioneGiornalieraIspettore(Ispettore ispettore, DateTime giorno,
            AttivitaCollection listaAttivita)
        {
            Ispettore = ispettore;
            Giorno = giorno;
            ListaAttivita = listaAttivita;
        }

        public Ispettore Ispettore { get; }

        public DateTime Giorno { get; }

        public AttivitaCollection ListaAttivita { get; }
    }
}