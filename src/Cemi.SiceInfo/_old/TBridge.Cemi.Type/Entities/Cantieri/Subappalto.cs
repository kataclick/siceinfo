using System;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Subappalto
    {
        private int idCantiere;
        private int? idSubappalto;
        private Impresa subAppaltata;
        private Impresa subAppaltatrice;
        private TipologiaContratto tipoContratto;

        public Subappalto()
        {
        }

        public Subappalto(int? idSubappalto, int idCantiere, Impresa subAppaltatrice, Impresa subAppaltata,
            TipologiaContratto tipoContratto)
        {
            this.idSubappalto = idSubappalto;
            this.idCantiere = idCantiere;
            this.subAppaltatrice = subAppaltatrice;
            this.subAppaltata = subAppaltata;
            this.tipoContratto = tipoContratto;
        }

        public int? IdSubappalto
        {
            get => idSubappalto;
            set => idSubappalto = value;
        }

        public int IdCantiere
        {
            get => idCantiere;
            set => idCantiere = value;
        }

        public Impresa SubAppaltatrice
        {
            get => subAppaltatrice;
            set => subAppaltatrice = value;
        }

        public string NomeAppaltatrice
        {
            get
            {
                if (subAppaltatrice != null)
                {
                    if (subAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri) return subAppaltatrice.RagioneSociale;
                    return
                        string.Format("{0} - {1}", subAppaltatrice.IdImpresa,
                            subAppaltatrice.RagioneSociale);
                }
                return string.Empty;
            }
        }

        public Impresa SubAppaltata
        {
            get => subAppaltata;
            set => subAppaltata = value;
        }

        public string NomeAppaltata
        {
            get
            {
                if (subAppaltata != null)
                {
                    if (subAppaltata.TipoImpresa == TipologiaImpresa.Cantieri) return subAppaltata.RagioneSociale;
                    return
                        string.Format("{0} - {1}", subAppaltata.IdImpresa, subAppaltata.RagioneSociale);
                }
                return string.Empty;
            }
        }

        public TipologiaContratto TipoContratto
        {
            get => tipoContratto;
            set => tipoContratto = value;
        }
    }
}