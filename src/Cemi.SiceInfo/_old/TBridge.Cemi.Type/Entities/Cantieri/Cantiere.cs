using System;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Cantiere
    {
        #region ProprietÓ

        private bool attivo;
        private string cap;
        private string civico;
        private Committente committente;
        private string committenteTrovato;
        private string comune;
        private DateTime? dataFineLavori;
        private DateTime? dataInizioLavori;
        private string descrizioneLavori;
        private string direzioneLavori;
        private string fonti;
        private int? idCantiere;
        private double? importo;
        private Impresa impresaAppaltatrice;
        private string impresaAppaltatriceTrovata;
        private string indirizzo;
        private RapportoIspezioneCollection ispezioni;
        private double? latitudine;
        private double? longitudine;
        private string permessoCostruire;
        private string preIndirizzo;
        private string provincia;
        private string responsabileCantiere;
        private string responsabileProcedimento;
        private string tipoImpresaAppaltatrice;
        private TipologiaAppalto tipologiaAppalto;

        public int? IdCantiere
        {
            set => idCantiere = value;
            get => idCantiere;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Civico
        {
            get => civico;
            set => civico = value;
        }

        public string IndirizzoMappa => indirizzo + " " + civico + ", " + comune + " " + provincia;

        public string ImpresaAppaltatriceTrovata
        {
            get => impresaAppaltatriceTrovata;
            set => impresaAppaltatriceTrovata = value;
        }

        public Impresa ImpresaAppaltatrice
        {
            get => impresaAppaltatrice;
            set => impresaAppaltatrice = value;
        }

        public string ImpresaAppaltatriceStringa
        {
            get
            {
                if (ImpresaAppaltatrice != null)
                {
                    return ImpresaAppaltatrice.RagioneSociale;
                }
                return ImpresaAppaltatriceTrovata;
            }
        }

        public string NomeImpresa
        {
            get
            {
                if (impresaAppaltatrice != null)
                    return impresaAppaltatrice.RagioneSociale;
                return string.Empty;
            }
        }

        public string PermessoCostruire
        {
            get => permessoCostruire;
            set => permessoCostruire = value;
        }

        public double? Importo
        {
            get => importo;
            set => importo = value;
        }

        public DateTime? DataInizioLavori
        {
            get => dataInizioLavori;
            set => dataInizioLavori = value;
        }

        public DateTime? DataFineLavori
        {
            get => dataFineLavori;
            set => dataFineLavori = value;
        }

        public TipologiaAppalto TipologiaAppalto
        {
            get => tipologiaAppalto;
            set => tipologiaAppalto = value;
        }

        public bool Attivo
        {
            get => attivo;
            set => attivo = value;
        }

        public Committente Committente
        {
            get => committente;
            set => committente = value;
        }

        public string CommittenteStringa
        {
            get
            {
                if (Committente != null)
                {
                    return Committente.RagioneSociale;
                }
                return CommittenteTrovato;
            }
        }

        public string CommittenteTrovato
        {
            get => committenteTrovato;
            set => committenteTrovato = value;
        }

        public double? Latitudine
        {
            get => latitudine;
            set => latitudine = value;
        }

        public double? Longitudine
        {
            get => longitudine;
            set => longitudine = value;
        }

        public string DirezioneLavori
        {
            get => direzioneLavori;
            set => direzioneLavori = value;
        }

        public string ResponsabileProcedimento
        {
            get => responsabileProcedimento;
            set => responsabileProcedimento = value;
        }

        public string ResponsabileCantiere
        {
            get => responsabileCantiere;
            set => responsabileCantiere = value;
        }

        public string DescrizioneLavori
        {
            get => descrizioneLavori;
            set => descrizioneLavori = value;
        }

        public RapportoIspezioneCollection Ispezioni
        {
            get => ispezioni;
            set => ispezioni = value;
        }

        public string IspezioneStringa
        {
            get
            {
                if (Ispezioni != null && Ispezioni.Count == 1)
                {
                    return Ispezioni[0].Giorno.ToShortDateString();
                }
                return string.Empty;
            }
        }

        public string TipoImpresaAppaltatrice
        {
            get => tipoImpresaAppaltatrice;
            set => tipoImpresaAppaltatrice = value;
        }

        public string Fonti
        {
            get => fonti;
            set => fonti = value;
        }

        // Utilizzato per Notifiche preliminari

        public string PreIndirizzo
        {
            get => preIndirizzo;
            set => preIndirizzo = value;
        }

        #region ProprietÓ per Iscrizione CE

        private string frazione;

        public string Frazione
        {
            get => frazione;
            set => frazione = value;
        }

        #endregion

        public Segnalazione Segnalazione { get; set; }

        public string SegnalazioneStringa
        {
            get
            {
                if (Segnalazione != null)
                {
                    return Segnalazione.Data.ToShortDateString();
                }
                return string.Empty;
            }
        }

        public string SegnalazioneMotivazione
        {
            get
            {
                if (Segnalazione != null)
                {
                    return Segnalazione.Motivazione.ToString();
                }
                return string.Empty;
            }
        }

        public string SegnalazionePriorita
        {
            get
            {
                if (Segnalazione != null)
                {
                    return Segnalazione.Priorita.ToString();
                }
                return string.Empty;
            }
        }

        public string SegnalazioneNote
        {
            get
            {
                if (Segnalazione != null)
                {
                    return Segnalazione.Note;
                }
                return string.Empty;
            }
        }

        public Assegnazione Assegnazione { get; set; }

        public string AssegnazioneStringa
        {
            get
            {
                if (Assegnazione != null)
                {
                    return Assegnazione.Data.ToShortDateString();
                }
                return string.Empty;
            }
        }

        public CantieriCalendarioAttivita PresaInCarico { get; set; }

        public string PresaInCaricoStringa
        {
            get
            {
                if (PresaInCarico != null)
                {
                    return PresaInCarico.Data.ToShortDateString();
                }
                return string.Empty;
            }
        }

        public int? NotificaRiferimento { get; set; }

        public StatoIspezione? StatoIspezione { get; set; }

        public string ProtocolloRegionaleNotifica { get; set; }

        #endregion

        #region Costruttori

        /// <summary>
        ///     Costruttore per la serializzazione
        /// </summary>
        public Cantiere()
        {
        }

        public Cantiere(int? idCantiere, string provincia, string comune, string cap,
            string indirizzo, string civico, string impresaAppaltatriceTrovata, string permessoCostruire,
            double? importo, DateTime? dataInizioLavori, DateTime? dataFineLavori,
            TipologiaAppalto tipologiaAppalto,
            bool attivo, Committente committente, string committenteTrovato, Impresa impresa,
            double? latitudine,
            double? longitudine, string direzioneLavori, string responsabileProcedimento,
            string responsabileCantiere,
            string descrizioneLavori, string tipoImpresaAppaltatrice, string fonti)
        {
            this.idCantiere = idCantiere;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.indirizzo = indirizzo;
            this.civico = civico;
            this.impresaAppaltatriceTrovata = impresaAppaltatriceTrovata;
            this.permessoCostruire = permessoCostruire;
            this.importo = importo;
            this.dataInizioLavori = dataInizioLavori;
            this.dataFineLavori = dataFineLavori;
            this.tipologiaAppalto = tipologiaAppalto;
            this.attivo = attivo;
            this.committente = committente;
            this.committenteTrovato = committenteTrovato;
            impresaAppaltatrice = impresa;
            this.impresaAppaltatriceTrovata = impresaAppaltatriceTrovata;
            this.latitudine = latitudine;
            this.longitudine = longitudine;
            this.direzioneLavori = direzioneLavori;
            this.responsabileProcedimento = responsabileProcedimento;
            this.responsabileCantiere = responsabileCantiere;
            this.descrizioneLavori = descrizioneLavori;
            this.tipoImpresaAppaltatrice = tipoImpresaAppaltatrice;
            this.fonti = fonti;
        }

        #endregion
    }
}