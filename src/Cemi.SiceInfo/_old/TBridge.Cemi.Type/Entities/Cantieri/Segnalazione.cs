﻿using System;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.Type.Entities.Cantieri
{
    [Serializable]
    public class Segnalazione
    {
        public int IdSegnalazione { get; set; }

        public int IdCantiere { get; set; }

        public DateTime Data { get; set; }

        public bool Ricorrente { get; set; }

        public CantiereSegnalazioneMotivazione Motivazione { get; set; }

        public CantiereSegnalazionePriorita Priorita { get; set; }

        public int IdUtente { get; set; }

        public string NomeUtente { get; set; }

        public int? IdSegnalazioneSuccessiva { get; set; }

        public string Note { get; set; }
    }
}