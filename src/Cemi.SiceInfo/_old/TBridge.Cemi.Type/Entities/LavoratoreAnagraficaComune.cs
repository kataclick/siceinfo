using System;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class LavoratoreAnagraficaComune
    {
        public FonteAnagraficheComuni Fonte { get; set; }

        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CognomeNome => string.Format("{0} {1}", Cognome, Nome);

        public char Sesso { get; set; }

        public DateTime DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public string PaeseNascita { get; set; }

        public string ProvinciaNascita { get; set; }

        public string ComuneNascita { get; set; }
    }
}