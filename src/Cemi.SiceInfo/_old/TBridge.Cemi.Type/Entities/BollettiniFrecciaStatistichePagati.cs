﻿namespace TBridge.Cemi.Type.Entities
{
    public class BollettiniFrecciaStatistichePagati
    {
        public int NumeroBollettiniUniciStampati { get; set; }
        public int NumeroBollettiniUniciStampatiPagati { get; set; }
        public decimal ImportoPagato { get; set; }
    }
}