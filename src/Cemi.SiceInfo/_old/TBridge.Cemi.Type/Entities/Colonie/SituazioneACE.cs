namespace TBridge.Cemi.Type.Entities.Colonie
{
    public class SituazioneACE
    {
        public int IdTurno { get; set; }

        public string Turno { get; set; }

        public int PostiDisponibili { get; set; }

        public int PostiPrenotati { get; set; }

        public int DomandeAccettate { get; set; }

        public int AccompagnatoriAccettati { get; set; }

        public int DomandeInCarico { get; set; }

        public int AccompagnatoriInCarico { get; set; }

        public int DomandeRifiutate { get; set; }

        public int PostiAccettatiRichiesti =>
            DomandeAccettate + AccompagnatoriAccettati + DomandeInCarico + AccompagnatoriInCarico;
    }
}