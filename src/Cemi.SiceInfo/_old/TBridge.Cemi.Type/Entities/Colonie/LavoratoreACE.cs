using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class LavoratoreACE
    {
        private string cap;
        private string cognome;
        private string comune;
        private string indirizzo;
        private string nome;
        private string provincia;

        public int? IdLavoratore { get; set; }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string NomeCompleto => string.Format("{0} {1}", cognome, nome);

        public DateTime DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public char Sesso { get; set; }

        public string Telefono { get; set; }

        public string Cellulare { get; set; }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string IndirizzoCompleto => string.Format("{0} {1} ({2}) {3}", indirizzo, comune, provincia, cap);

        public string IdCassaEdile { get; set; }

        public bool Modificabile { get; set; }
    }
}