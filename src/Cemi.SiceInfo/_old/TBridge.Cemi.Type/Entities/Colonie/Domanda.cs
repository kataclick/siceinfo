using System;
using TBridge.Cemi.Type.Collections.Colonie;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Domanda
    {
        private DateTime? dataNascita;
        private TurnoDomandaCollection turni;

        public Domanda()
        {
        }

        public Domanda(int idDomanda, string cognome, string nome, DateTime? dataNascita, string stato,
            string cassaEdile)
        {
            IdDomanda = idDomanda;
            Cognome = cognome;
            Nome = nome;
            this.dataNascita = dataNascita;
            Stato = stato;
            CassaEdile = cassaEdile;
            turni = new TurnoDomandaCollection();
        }

        public int IdDomanda { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita
        {
            get => dataNascita;
            set => dataNascita = value;
        }

        public string Accompagnatore { get; set; }

        public string StringaDataNascita
        {
            get
            {
                if (dataNascita.HasValue) return dataNascita.Value.ToShortDateString();
                return string.Empty;
            }
        }

        public TurnoDomandaCollection Turni
        {
            get => turni;
            set => turni = value;
        }

        public string Stato { get; set; }

        public string CassaEdile { get; set; }

        public int IdVacanza { get; set; }

        public string TargaAutobus
        {
            get
            {
                if (turni != null && turni.Count == 1) return turni[0].TargaAutobus;
                return string.Empty;
            }
        }

        public string RientroAnticipatoStringa
        {
            get
            {
                if (turni != null && turni.Count == 1) return turni[0].RientroAnticipatoStringa;
                return string.Empty;
            }
        }

        public string AnnullamentoStringa
        {
            get
            {
                if (turni != null && turni.Count == 1) return turni[0].RichiestaAnnullamentoStringa;
                return string.Empty;
            }
        }

        public int IdTurno
        {
            get
            {
                if (turni != null && turni.Count == 1) return turni[0].IdTurno;
                return -1;
            }
        }

        public bool PortatoreHandicap
        {
            get
            {
                if (turni != null && turni.Count == 1) return turni[0].PortatoreHandicap;
                return false;
            }
        }
    }
}