using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class TipoVacanza
    {
        public TipoVacanza()
        {
        }

        public TipoVacanza(int idTipoVacanza, string descrizione)
        {
            IdTipoVacanza = idTipoVacanza;
            Descrizione = descrizione;
        }

        public int IdTipoVacanza { get; set; }

        public string Descrizione { get; set; }
    }
}