using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class CassaEdile
    {
        public CassaEdile()
        {
        }

        public CassaEdile(string idCassaEdile, string descrizione)
        {
            IdCassaEdile = idCassaEdile;
            Descrizione = descrizione;
        }

        public string IdCassaEdile { get; set; }

        public string Descrizione { get; set; }

        public string Email { get; set; }
    }
}