using TBridge.Cemi.Type.Enums.Colonie;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    public class DomandaACE
    {
        public DomandaACE()
        {
            Bambino = new FamiliareACE();
        }

        public int? IdDomanda { get; set; }

        public int IdTurno { get; set; }

        public int? IdTaglia { get; set; }

        public string StringaLavoratore
        {
            get
            {
                if (Bambino != null && Bambino.Parente != null) return Bambino.Parente.NomeCompleto;
                return string.Empty;
            }
        }

        public string CodiceFiscaleLavoratore
        {
            get
            {
                if (Bambino != null && Bambino.Parente != null) return Bambino.Parente.CodiceFiscale;
                return string.Empty;
            }
        }

        public string IndirizzoLavoratore
        {
            get
            {
                if (Bambino != null && Bambino.Parente != null) return Bambino.Parente.IndirizzoCompleto;
                return string.Empty;
            }
        }

        public FamiliareACE Bambino { get; set; }

        public string StringaPartecipante
        {
            get
            {
                if (Bambino != null) return Bambino.NomeCompleto;
                return string.Empty;
            }
        }

        public string CodiceFiscalePartecipante
        {
            get
            {
                if (Bambino != null) return Bambino.CodiceFiscale;
                return string.Empty;
            }
        }

        public string StringaCassaEdile
        {
            get
            {
                if (Bambino != null && Bambino.Parente != null) return Bambino.Parente.IdCassaEdile;
                return string.Empty;
            }
        }

        public Accompagnatore Accompagnatore { get; set; }

        public StatoDomandaACE StatoDomanda { get; set; }

        public string MotivazioneRifiuto { get; set; }

        public int? NumeroCorredo { get; set; }
    }
}