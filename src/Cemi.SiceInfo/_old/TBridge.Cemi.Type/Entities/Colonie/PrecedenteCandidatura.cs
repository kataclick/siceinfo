﻿namespace TBridge.Cemi.Type.Entities.Colonie
{
    public class PrecedenteCandidatura
    {
        public int Anno { get; set; }

        public string Stato { get; set; }

        public string Note { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1} {2}", Anno, Stato,
                string.IsNullOrWhiteSpace(Note) ? "" : string.Format("({0})", Note));
        }
    }
}