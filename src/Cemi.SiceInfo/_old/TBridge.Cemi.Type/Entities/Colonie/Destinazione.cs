using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Destinazione
    {
        private int? idDestinazione;
        private string luogo;
        private TipoDestinazione tipoDestinazione;

        public Destinazione()
        {
        }

        public Destinazione(int? idDestinazione, TipoDestinazione tipoDestinazione, string luogo, bool attiva)
        {
            this.idDestinazione = idDestinazione;
            this.tipoDestinazione = tipoDestinazione;
            this.luogo = luogo;
            Attiva = attiva;
        }

        public int? IdDestinazione
        {
            get => idDestinazione;
            set => idDestinazione = value;
        }

        public int IdCombo
        {
            get
            {
                if (idDestinazione.HasValue) return idDestinazione.Value;
                return -1;
            }
        }

        public TipoDestinazione TipoDestinazione
        {
            get => tipoDestinazione;
            set => tipoDestinazione = value;
        }

        public string DescrizioneTD
        {
            get
            {
                if (tipoDestinazione != null) return tipoDestinazione.Descrizione;
                return string.Empty;
            }
        }

        public string DescrizioneTV
        {
            get
            {
                if (tipoDestinazione != null && tipoDestinazione.TipoVacanza != null)
                    return tipoDestinazione.TipoVacanza.Descrizione;
                return string.Empty;
            }
        }

        public string Luogo
        {
            get => luogo;
            set => luogo = value;
        }

        public string DestinazioneLuogo
        {
            get
            {
                if (tipoDestinazione != null) return tipoDestinazione.Descrizione + " - " + luogo;
                return luogo;
            }
        }

        public bool Attiva { get; set; }
    }
}