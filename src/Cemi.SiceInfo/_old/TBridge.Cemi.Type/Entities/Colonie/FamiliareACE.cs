using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class FamiliareACE
    {
        private string cognome;
        private string nome;

        public int? IdFamiliare { get; set; }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string NomeCompleto => string.Format("{0} {1}", cognome, nome);

        public DateTime DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public char Sesso { get; set; }

        public bool PortatoreHandicap { get; set; }

        public string NotaDisabilita { get; set; }

        public string IntolleranzeAlimentari { get; set; }

        public LavoratoreACE Parente { get; set; }

        public bool Modificabile { get; set; }
    }
}