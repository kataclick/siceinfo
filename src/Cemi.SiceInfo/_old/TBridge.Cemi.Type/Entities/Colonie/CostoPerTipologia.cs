using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class CostoPerTipologia
    {
        public CostoPerTipologia(string tipoCosto)
        {
            TipoCosto = tipoCosto;
            NumeroPersone = 0;
            PeriodoPermanenza = 0;
            TotaleGiorni = 0;
            QuotaIndividuale = 0;
            Totale = 0;
        }

        public CostoPerTipologia(string tipoCosto, int durata, decimal quotaIndividuale)
        {
            TipoCosto = tipoCosto;
            NumeroPersone = 0;
            PeriodoPermanenza = durata;
            TotaleGiorni = 0;
            QuotaIndividuale = quotaIndividuale;
            Totale = 0;
        }

        public string TipoCosto { get; set; }

        public int NumeroPersone { get; set; }

        public int PeriodoPermanenza { get; set; }

        public int TotaleGiorni { get; set; }

        public decimal QuotaIndividuale { get; set; }

        public decimal Totale { get; set; }
    }
}