using System;
using TBridge.Cemi.Type.Enums.Colonie;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class TurnoDomanda
    {
        private Autobus autobus;
        private DateTime? richiestaAnnullamento;
        private DateTime? rientroAnticipato;

        public TurnoDomanda()
        {
        }

        public TurnoDomanda(int idTurno, int idDomanda, Autobus autobus, bool? imbarcato, DateTime? rientroAnticipato,
            DateTime? richiestaAnnullamento, string mancatoImbarco, bool? presenteColonia,
            bool portatoreHandicap,
            bool intolleranzeAlimentari, DateTime? partenzaPosticipata)
        {
            IdTurno = idTurno;
            IdDomanda = idDomanda;
            Imbarcato = imbarcato;
            this.autobus = autobus;
            this.rientroAnticipato = rientroAnticipato;
            this.richiestaAnnullamento = richiestaAnnullamento;
            MancatoImbarco = mancatoImbarco;
            PresenteColonia = presenteColonia;
            PortatoreHandicap = portatoreHandicap;
            IntolleranzeAlimentari = intolleranzeAlimentari;
            PartenzaPosticipata = partenzaPosticipata;
        }

        public TurnoDomanda(int idTurno, int idDomanda, Autobus autobus, bool? imbarcato, DateTime? rientroAnticipato,
            DateTime? richiestaAnnullamento, string mancatoImbarco, bool? presenteColonia,
            bool portatoreHandicap,
            bool intolleranzeAlimentari, DateTime? partenzaPosticipata, int progressivo,
            string descrizione)
        {
            IdTurno = idTurno;
            IdDomanda = idDomanda;
            Imbarcato = imbarcato;
            this.autobus = autobus;
            this.rientroAnticipato = rientroAnticipato;
            this.richiestaAnnullamento = richiestaAnnullamento;
            MancatoImbarco = mancatoImbarco;
            PresenteColonia = presenteColonia;
            PortatoreHandicap = portatoreHandicap;
            IntolleranzeAlimentari = intolleranzeAlimentari;
            PartenzaPosticipata = partenzaPosticipata;
            Progressivo = progressivo;
            Descrizione = descrizione;
        }

        public int IdTurno { get; set; }

        public int IdDomanda { get; set; }

        public bool? Imbarcato { get; set; }

        public Autobus Autobus
        {
            get => autobus;
            set => autobus = value;
        }

        public string MancatoImbarco { get; set; }

        public bool? PresenteColonia { get; set; }

        public DateTime? RientroAnticipato
        {
            get => rientroAnticipato;
            set => rientroAnticipato = value;
        }

        public string RientroAnticipatoStringa
        {
            get
            {
                if (rientroAnticipato.HasValue)
                    return rientroAnticipato.Value.ToShortDateString();
                return string.Empty;
            }
        }

        public DateTime? RichiestaAnnullamento
        {
            get => richiestaAnnullamento;
            set => richiestaAnnullamento = value;
        }

        public bool RichiestaAnnullamentoEsplicita { get; set; }

        public string RichiestaAnnullamentoStringa
        {
            get
            {
                if (richiestaAnnullamento.HasValue)
                    return richiestaAnnullamento.Value.ToShortDateString();
                return string.Empty;
            }
        }

        public string TargaAutobus
        {
            get
            {
                if (autobus != null)
                    return autobus.Targa;
                return string.Empty;
            }
        }

        public bool PortatoreHandicap { get; set; }

        public bool IntolleranzeAlimentari { get; set; }

        public DateTime? PartenzaPosticipata { get; set; }

        public TipoDomanda TipoDomanda { get; set; }

        #region Triffo: aggiunte due proprietÓ per descrivere il turno

        public string Descrizione { get; set; }
        public int Progressivo { get; set; }

        public string DescrizioneCompleta => Descrizione + " " + Progressivo;

        #endregion
    }
}