using System;
using TBridge.Cemi.Type.Collections.Colonie;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class PrenotazioniCassa
    {
        private PrenotazioneCollection prenotazioni;

        public PrenotazioniCassa()
        {
        }

        public PrenotazioniCassa(string idCassaEdile, PrenotazioneCollection prenotazioni)
        {
            IdCassaEdile = idCassaEdile;
            this.prenotazioni = prenotazioni;
        }

        public string IdCassaEdile { get; set; }

        public PrenotazioneCollection Prenotazioni
        {
            get => prenotazioni;
            set => prenotazioni = value;
        }

        public int IdPrenotazione
        {
            get
            {
                if (prenotazioni != null && prenotazioni.Count == 1 && prenotazioni[0].IdPrenotazione.HasValue)
                    return prenotazioni[0].IdPrenotazione.Value;
                return -1;
            }
        }
    }
}