using System;
using TBridge.Cemi.Type.Collections.Colonie;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class DomandaACEEffettiva
    {
        public DomandaACEEffettiva()
        {
        }

        public DomandaACEEffettiva(int idDomanda, string cognome, string nome, DateTime? dataNascita, string stato
        )
        {
            IdDomanda = idDomanda;
            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            Stato = stato;

            Turni = new TurnoDomandaCollection();
        }

        public int IdDomanda { get; set; }
        public int NumeroCorredo { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }
        public string Sesso { get; set; }
        public bool? PortatoreHandicap { get; set; }

        //public DateTime? RichiestaAnnullamento { get; set; }
        //public DateTime? RientroAnticipato{ get; set; }
        //public DateTime? PartenzaPosticipata{ get; set; }
        //public Boolean? PresenteColonia { get; set; }

        public string Descrizione { get; set; }
        public int Progressivo { get; set; }
        public bool? IntolleranzeAlimentari { get; set; }

        public string Accompagnatore { get; set; }
        public string Stato { get; set; }

        public TurnoDomandaCollection Turni { get; set; }
    }
}