using System;
using TBridge.Cemi.Type.Collections.Colonie;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class RigaMatriceTurni
    {
        private Destinazione destinazione;

        public RigaMatriceTurni()
        {
        }

        public RigaMatriceTurni(Destinazione destinazione, TurnoCollection turni)
        {
            this.destinazione = destinazione;
            Turni = turni;
        }

        public Destinazione Destinazione
        {
            get => destinazione;
            set => destinazione = value;
        }

        public string StringaDestinazione
        {
            get
            {
                if (destinazione != null) return destinazione.Luogo;
                return string.Empty;
            }
        }

        public TurnoCollection Turni { get; set; }
    }
}