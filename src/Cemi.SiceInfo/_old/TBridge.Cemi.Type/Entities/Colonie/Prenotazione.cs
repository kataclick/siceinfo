using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Prenotazione
    {
        private Turno turno;

        public Prenotazione()
        {
        }

        public Prenotazione(int? idPrenotazione, Turno turno, string idCassaEdile, int postiRichiesti,
            int postiConcessi,
            bool accettata)
        {
            IdPrenotazione = idPrenotazione;
            this.turno = turno;
            IdCassaEdile = idCassaEdile;
            PostiRichiesti = postiRichiesti;
            PostiConcessi = postiConcessi;
            Accettata = accettata;
        }

        public int? IdPrenotazione { get; set; }

        public Turno Turno
        {
            get => turno;
            set => turno = value;
        }

        public string IdCassaEdile { get; set; }

        public int? PostiRichiesti { get; set; }

        public int? PostiConcessi { get; set; }

        public bool? Accettata { get; set; }

        #region Wrapper per proprietÓ turno

        public int IdTurno
        {
            get
            {
                if (turno != null && turno.IdTurno.HasValue) return turno.IdTurno.Value;
                return -1;
            }
        }

        public string DescrizioneTurno
        {
            get
            {
                if (turno != null) return turno.DescrizioneTurno;
                return string.Empty;
            }
        }

        public string DalTurno
        {
            get
            {
                if (turno != null) return turno.Dal.ToShortDateString();
                return string.Empty;
            }
        }

        public string AlTurno
        {
            get
            {
                if (turno != null) return turno.Al.ToShortDateString();
                return string.Empty;
            }
        }

        public string StringaDestinazione
        {
            get
            {
                if (turno != null) return turno.StringaDestinazione;
                return string.Empty;
            }
        }

        #endregion
    }
}