using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Autobus
    {
        private string codiceAutobus;
        private int? idAutobus;
        private string targa;

        public Autobus()
        {
        }

        public Autobus(int? idAutobus, string codiceAutobus, string targa, int idTurno)
        {
            this.idAutobus = idAutobus;
            this.codiceAutobus = codiceAutobus;
            this.targa = targa;
            IdTurno = idTurno;
        }

        public int? IdAutobus
        {
            get => idAutobus;
            set => idAutobus = value;
        }

        public int IdCombo
        {
            get
            {
                if (idAutobus.HasValue) return idAutobus.Value;
                return -1;
            }
        }

        public string CodiceAutobus
        {
            get => codiceAutobus;
            set => codiceAutobus = value;
        }

        public string Targa
        {
            get => targa;
            set => targa = value;
        }

        public string TestoCombo => codiceAutobus + " - " + targa;

        public int IdTurno { get; set; }
    }
}