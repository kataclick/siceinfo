using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Accompagnatore
    {
        public int? IdAccompagnatore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime DataNascita { get; set; }

        public char Sesso { get; set; }
    }
}