using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class TurnoPresenza
    {
        private Destinazione destinazione;
        private int progressivoTurno;

        public TurnoPresenza()
        {
        }

        public TurnoPresenza(int idDomanda, Destinazione destinazione, int progressivoTurno, int idTurno, bool presenza)
        {
            IdDomanda = idDomanda;
            this.destinazione = destinazione;
            this.progressivoTurno = progressivoTurno;
            IdTurno = idTurno;
            Presenza = presenza;
        }

        public int IdDomanda { get; set; }

        public Destinazione Destinazione
        {
            get => destinazione;
            set => destinazione = value;
        }

        public string DescrizioneTurno => destinazione.DescrizioneTD + " " + progressivoTurno;

        public int ProgressivoTurno
        {
            get => progressivoTurno;
            set => progressivoTurno = value;
        }

        public int IdTurno { get; set; }

        public bool Presenza { get; set; }
    }
}