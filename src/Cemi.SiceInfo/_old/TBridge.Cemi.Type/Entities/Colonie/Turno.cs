using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Turno
    {
        private DateTime al;
        private decimal costoGiornaliero;
        private decimal costoGiornalieroAccompagnatore;
        private DateTime dal;
        private Destinazione destinazione;
        private int? idTurno;
        private int progressivoTurno;

        public Turno()
        {
        }

        public Turno(int? idTurno, int progressivoTurno, DateTime dal, DateTime al, Destinazione destinazione,
            int idDestinazione, int idVacanza, int postiDisponibili, decimal costoGiornaliero)
        {
            IdTurno = idTurno;
            this.progressivoTurno = progressivoTurno;
            this.dal = dal;
            this.al = al;
            this.destinazione = destinazione;
            IdDestinazione = idDestinazione;
            IdVacanza = idVacanza;
            PostiDisponibili = postiDisponibili;
            CostoGiornaliero = costoGiornaliero;
        }

        public Turno(int? idTurno, int progressivoTurno, DateTime dal, DateTime al, Destinazione destinazione,
            int idDestinazione, int idVacanza, int postiDisponibili, int postiOccupati,
            decimal costoGiornaliero
        )
        {
            IdTurno = idTurno;
            this.progressivoTurno = progressivoTurno;
            this.dal = dal;
            this.al = al;
            this.destinazione = destinazione;
            IdDestinazione = idDestinazione;
            IdVacanza = idVacanza;
            PostiDisponibili = postiDisponibili;
            PostiOccupati = postiOccupati;
            CostoGiornaliero = costoGiornaliero;
        }

        public int? IdTurno
        {
            get => idTurno;
            set => idTurno = value;
        }

        public int IdCombo
        {
            get
            {
                if (idTurno.HasValue)
                    return idTurno.Value;
                return -1;
            }
        }

        public int ProgressivoTurno
        {
            get => progressivoTurno;
            set => progressivoTurno = value;
        }

        public string DescrizioneTurno
        {
            get
            {
                if (destinazione != null)
                {
                    return string.Format("{0} {1}", destinazione.DescrizioneTD, progressivoTurno);
                }
                return progressivoTurno.ToString();
            }
        }

        public string DescrizioneTurnoEstesa
        {
            get
            {
                if (destinazione != null)
                {
                    return string.Format("{0} {1} (dal {2:dd/MM/yyyy} al {3:dd/MM/yyyy})", destinazione.DescrizioneTD,
                        progressivoTurno, dal, al);
                }
                return progressivoTurno.ToString();
            }
        }

        public DateTime Dal
        {
            get => dal;
            set => dal = value;
        }

        public DateTime Al
        {
            get => al;
            set => al = value;
        }

        public int PostiDisponibili { get; set; }

        public int IdVacanza { get; set; }

        public int IdDestinazione { get; set; }


        public Destinazione Destinazione
        {
            get => destinazione;
            set => destinazione = value;
        }

        public decimal CostoGiornaliero
        {
            get => costoGiornaliero;
            set => costoGiornaliero = value;
        }

        public decimal CostoGiornalieroAccompagnatore
        {
            get => costoGiornalieroAccompagnatore;
            set => costoGiornalieroAccompagnatore = value;
        }

        public string StringaDestinazione
        {
            get
            {
                if (destinazione != null)
                    return destinazione.Luogo;
                return string.Empty;
            }
        }

        public int PostiOccupati { get; set; }

        public string DettaglioTurno => string.Format("{0} {1}-{2}", progressivoTurno, dal.ToShortDateString(),
            al.ToShortDateString());
    }
}