using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class TipoDestinazione
    {
        public TipoDestinazione()
        {
        }

        public TipoDestinazione(int idTipoDestinazione, string descrizione, TipoVacanza tipoVacanza)
        {
            IdTipoDestinazione = idTipoDestinazione;
            Descrizione = descrizione;
            TipoVacanza = tipoVacanza;
        }

        public int IdTipoDestinazione { get; set; }

        public string Descrizione { get; set; }

        public TipoVacanza TipoVacanza { get; set; }
    }
}