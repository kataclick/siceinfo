using System;

namespace TBridge.Cemi.Type.Entities.Colonie
{
    [Serializable]
    public class Partecipante
    {
        public Partecipante()
        {
        }

        public Partecipante(int idFamiliare, string cognome, string nome, string sesso, DateTime? dataNascita,
            string numTesseraSan,
            bool portatoreHandicap, bool intolleranzeAlimentari, string allergie, string terapie,
            string dieta, string protesi)
        {
            IdFamiliare = idFamiliare;
            Cognome = cognome;
            Nome = nome;
            Sesso = sesso;
            DataNascita = dataNascita;
            NumeroTesseraSanitaria = numTesseraSan;
            PortatoreHandicap = portatoreHandicap;
            IntolleranzeAlimentari = intolleranzeAlimentari;
            Allergie = allergie;
            TerapieInCorso = terapie;
            Dieta = dieta;
            ProtesiAusili = protesi;
        }

        public int IdFamiliare { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        // Indirizzo

        public string Sesso { get; set; }

        public DateTime? DataNascita { get; set; }

        // Tessera sanitaria

        public string NumeroTesseraSanitaria { get; set; }

        public bool PortatoreHandicap { get; set; }

        public bool IntolleranzeAlimentari { get; set; }

        public string Allergie { get; set; }

        public string TerapieInCorso { get; set; }

        public string Dieta { get; set; }

        public string ProtesiAusili { get; set; }
    }
}