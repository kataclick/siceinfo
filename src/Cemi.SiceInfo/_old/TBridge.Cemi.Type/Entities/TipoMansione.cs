using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoMansione
    {
        public string IdMansione { get; set; }

        public string Descrizione { get; set; }
    }
}