﻿namespace TBridge.Cemi.Type.Entities
{
    public class Cud
    {
        public int Anno { get; set; }

        public int IdLavoratore { get; set; }

        public string IdArchidoc { get; set; }
    }
}