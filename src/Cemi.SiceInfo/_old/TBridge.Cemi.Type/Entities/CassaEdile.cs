using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class CassaEdile
    {
        public static char IDCOMPOSTOSEPARATOR = '|';
        private bool cnce;
        private string descrizione;

        private string idCassaEdile;

        public CassaEdile()
        {
        }

        public CassaEdile(string idCassaEdile, string descrizione)
        {
            this.idCassaEdile = idCassaEdile;
            this.descrizione = descrizione;
        }

        public string IdCassaEdile
        {
            get => idCassaEdile;
            set => idCassaEdile = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public bool Cnce
        {
            get => cnce;
            set => cnce = value;
        }

        public string IdComposito => string.Format("{0}{1}{2}", idCassaEdile, IDCOMPOSTOSEPARATOR, cnce);

        #region Attributo necessario per prestazioni

        private bool inseritaManualmente;

        public bool InseritaManualmente
        {
            get => inseritaManualmente;
            set => inseritaManualmente = value;
        }

        #endregion
    }
}