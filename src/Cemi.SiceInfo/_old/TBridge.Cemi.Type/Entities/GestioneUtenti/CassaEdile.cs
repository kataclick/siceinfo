using System;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class CassaEdile : Utente
    {
        public CassaEdile()
        {
        }

        public CassaEdile(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        public CassaEdile(string idCassaEdile, string descrizione)
        {
            IdCassaEdile = idCassaEdile;
            Descrizione = descrizione;
        }

        public string IdCassaEdile { get; set; }

        public string Descrizione { get; set; }
    }
}