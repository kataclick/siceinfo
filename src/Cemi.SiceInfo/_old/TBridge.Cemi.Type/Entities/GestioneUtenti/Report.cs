﻿namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class Report
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Path { get; set; }
        public string Theme { get; set; }
    }
}