using System;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class Committente : Utente
    {
        private string cap;
        private string codiceFiscale;
        private string cognome;
        private long? comune;
        private string fax;
        private int idCommittente;
        private string indirizzo;
        private string nome;
        private string partitaIva;
        private int? provincia;
        private string ragioneSociale;
        private string telefono;
        private TipologiaCommittente tipologia;

        public Committente()
        {
        }

        public Committente(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        public int IdCommittente
        {
            get => idCommittente;
            set => idCommittente = value;
        }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string Cognome
        {
            get => cognome;
            set => cognome = value;
        }

        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        public string PartitaIva
        {
            get => partitaIva;
            set => partitaIva = value;
        }

        public string CodiceFiscale
        {
            get => codiceFiscale;
            set => codiceFiscale = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public long? Comune
        {
            get => comune;
            set => comune = value;
        }

        public int? Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        public string Fax
        {
            get => fax;
            set => fax = value;
        }

        public TipologiaCommittente Tipologia
        {
            get => tipologia;
            set => tipologia = value;
        }
    }
}