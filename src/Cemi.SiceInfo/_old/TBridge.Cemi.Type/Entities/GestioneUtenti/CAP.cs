using System;
using System.Collections.Generic;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class CAP
    {
        public CAP()
        {
        }

        public CAP(string cap)
        {
            Cap = cap;
            ListaComuni = new List<CAP>();
        }

        public CAP(string cap, string comune, string provincia)
        {
            Cap = cap;
            Comune = comune;
            Provincia = provincia;
        }

        public string Cap { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Testo => Comune + " " + Provincia + " - " + Cap;

        public List<CAP> ListaComuni { get; set; }
    }
}