﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class EsitoRegistrazioneUtente
    {
        public ErroriRegistrazione Esito { get; set; }
        public Exception Exc { get; set; }
    }
}
