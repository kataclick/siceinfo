using System;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class Ispettore : Utente
    {
        /// <summary>
        ///     Costruttore pubblico
        /// </summary>
        public Ispettore()
        {
        }

        public Ispettore(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        /// <summary>
        ///     Costruttore
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="cognome"></param>
        public Ispettore(string nome, string cognome)
        {
            Nome = nome;
            Cognome = cognome;
        }

        /// <summary>
        ///     Identificativo univoco dell'ispettore
        /// </summary>
        public int IdIspettore { get; set; }

        /// <summary>
        ///     Nome ispettore
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        ///     Cognome ispettore
        /// </summary>
        public string Cognome { get; set; }

        /// <summary>
        ///     Identificativo zona cantiere
        /// </summary>
        public int IdCantieriZona { get; set; }

        /// <summary>
        ///     Descrizione della zona
        /// </summary>
        public string NomeZona { get; set; }

        /// <summary>
        ///     Descrizione della zona
        /// </summary>
        public string DescrizioneZona { get; set; }


        /// <summary>
        ///     Operativo si/no
        /// </summary>
        public bool Operativo { get; set; }

        public string Cellulare { get; set; }
    }
}