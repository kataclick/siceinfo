namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class ConsulenteConCredenziali : Consulente
    {
        public string Username { get; set; }

        public new string Password { get; set; }
    }
}