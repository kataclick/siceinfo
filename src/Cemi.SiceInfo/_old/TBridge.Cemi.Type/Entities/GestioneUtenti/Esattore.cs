namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class Esattore : Utente
    {
        public Esattore()
        {
        }

        public Esattore(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        public string IdEsattore { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }
    }
}