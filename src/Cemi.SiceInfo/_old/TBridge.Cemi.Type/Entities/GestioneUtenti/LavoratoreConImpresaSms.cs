namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class LavoratoreConImpresaSms : LavoratoreConImpresa
    {
        public string NumeroTelefono { get; set; }
    }
}