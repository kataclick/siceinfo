using System.Collections.Generic;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class ASL : Utente
    {
        public ASL()
        {
            Comuni = new List<string>();
        }

        public ASL(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
            Comuni = new List<string>();
        }

        public int IdASLUtente { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public int IdASL { get; set; }

        public string Codice { get; set; }

        public string Descrizione { get; set; }

        public List<string> Comuni { get; set; }
    }
}