namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class Dipendente : Utente
    {
        public Dipendente()
        {
        }

        public Dipendente(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        public Dipendente(string nome, string cognome)
        {
            Nome = nome;
            Cognome = cognome;
        }

        public int IdDipendente { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }

        public string EMail { get; set; }
    }
}