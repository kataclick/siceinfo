namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class LavoratoreConImpresa : Lavoratore
    {
        public int IdImpresa { get; set; }

        public string RagioneSocialeImpresa { get; set; }
    }
}