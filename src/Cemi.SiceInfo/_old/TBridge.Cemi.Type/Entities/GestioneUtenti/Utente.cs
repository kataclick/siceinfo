using System;
using System.Web.Security;
using TBridge.Cemi.Type.Collections.GestioneUtenti;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class Utente : MembershipUser
    {
        private int idUtente = -1;

        //public Utente(int idUtente, string username, string password)
        //{
        //    this.idUtente = idUtente;
        //    this.Username = username;
        //    this.password = password;
        //}
        public Utente(int idUtente, string username, string password)
            : base(
                "SQLServerMembershipProvider", username, null, null, null, null, true, false, DateTime.Now,
                DateTime.Now,
                DateTime.Now, DateTime.Now, DateTime.Now)
        {
            IdUtente = idUtente;
            //this.Username = username;
            Password = password;
        }

        public Utente()
        {
        }

        public int IdUtente
        {
            get => idUtente;
            set => idUtente = value;
        }

        //public string Username { get; set; }

        public string Password { get; set; }

        [Obsolete("Da non utilizzare in quanto la IsInRole interroga direttamente il DB", true)]
        public RuoliCollection Ruoli { get; set; }

        [Obsolete("Da non utilizzare in quanto la IsInRole interroga direttamente il DB", true)]
        public FunzionalitaCollection Funzionalita { get; set; }

        //public Utente(string providerName, string name, object providerUserKey, string email, 
        //    string passwordQuestion, string comment, bool isApproved, bool isLockedOut, DateTime creationDate, 
        //    DateTime lastLoginDate, DateTime lastActivityDate, DateTime lastPasswordChangedDate, 
        //    DateTime lastLockoutDate)
        //    : base(providerName, name, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate)
        //{ }	
    }
}