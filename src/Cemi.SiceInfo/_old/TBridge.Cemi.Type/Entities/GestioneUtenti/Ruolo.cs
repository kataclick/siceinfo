using System;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class Ruolo
    {
        private bool modificabile = true;
        private string nome;

        public Ruolo()
        {
        }

        public Ruolo(int idRuolo)
        {
            IdRuolo = idRuolo;
        }

        public Ruolo(int idRuolo, string nome, string descrizione)
        {
            IdRuolo = idRuolo;
            this.nome = nome;
            Descrizione = descrizione;
        }

        public Ruolo(string nome, string descrizione)
        {
            IdRuolo = -1;
            this.nome = nome;
            Descrizione = descrizione;
        }

        public int IdRuolo { get; set; }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string Descrizione { get; set; }

        public bool Predefinito { get; set; }

        public bool Anagrafica { get; set; }

        public bool Modificabile
        {
            get => modificabile;
            set => modificabile = value;
        }

        public override string ToString()
        {
            return nome;
        }
    }
}