namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class ImpresaConCredenziali : Impresa
    {
        public string Username { get; set; }

        public new string Password { get; set; }
    }
}