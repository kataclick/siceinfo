using System;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class Impresa : Utente
    {
        public Impresa()
        {
        }

        public Impresa(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        public int IdImpresa { get; set; } = -1;

        /// <summary>
        ///     Ragione sociale dell'impresa
        /// </summary>
        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        /// <summary>
        ///     Numero di partita IVA dell'impresa
        /// </summary>
        public string PartitaIVA { get; set; }

        /// <summary>
        ///     Tipologia di impresa
        /// </summary>
        public string IdTipoImpresa { get; set; }

        /// <summary>
        ///     Codice del contratto associato
        /// </summary>
        public string CodiceContratto { get; set; }

        /// <summary>
        ///     Codice INAIL dell'impresa
        /// </summary>
        public string CodiceINAIL { get; set; }

        // Campo non mappato su DB ma serve per Iscrizione CE

        public string ControCodiceINAIL { get; set; }

        /// <summary>
        ///     Codice INPS dell'impresa
        /// </summary>
        public string CodiceINPS { get; set; }

        /// <summary>
        ///     Numero di iscrizione CCIAA
        /// </summary>
        public int NumeroIscrizioneCCIAA { get; set; } = -1;

        /// <summary>
        ///     Attivit� ISTAT
        /// </summary>
        public string IdAttivitaISTAT { get; set; }

        /// <summary>
        ///     Natura giuridica
        /// </summary>
        public string IdNaturaGiuridica { get; set; }

        /// <summary>
        ///     Indirizzo della sede legale dell'impresa
        /// </summary>
        public string IndirizzoSedeLegale { get; set; }

        /// <summary>
        ///     CAP della sede legale dell'impresa
        /// </summary>
        public string CapSedeLegale { get; set; }

        /// <summary>
        ///     Localit� della sede legale dell'impresa
        /// </summary>
        public string LocalitaSedeLegale { get; set; }

        /// <summary>
        ///     Provincia della sede legale dell'impresa
        /// </summary>
        public string ProvinciaSedeLegale { get; set; }

        /// <summary>
        ///     Indirizzo della sede amministrativa dell'impresa
        /// </summary>
        public string IndirizzoSedeAmministrazione { get; set; }

        /// <summary>
        ///     CAP della sede amministrativa dell'impresa
        /// </summary>
        public string CapSedeAmministrazione { get; set; }

        /// <summary>
        ///     Localit� della sede amministrativa dell'impresa
        /// </summary>
        public string LocalitaSedeAmministrazione { get; set; }

        /// <summary>
        ///     Provincia della sede amministrativa dell'impresa
        /// </summary>
        public string ProvinciaSedeAmministrazione { get; set; }

        /// <summary>
        ///     Segnalazione del titolare della sede amministrativa
        /// </summary>
        public string PressoSedeAmministrazione { get; set; }


        /// <summary>
        ///     Numero di fax della sede amministrativa dell'impresa
        /// </summary>
        public string TelefonoSedeAmministrazione { get; set; }

        /// <summary>
        ///     Numero di fax della sede amministrativa dell'impresa
        /// </summary>
        public string FaxSedeAmministrazione { get; set; }

        public DateTime? DataInizioValiditaSedeLegale { get; set; }

        public DateTime? DataFineValiditaSedeLegale { get; set; }

        public DateTime? DataInizioValiditaSedeAmministrativa { get; set; }

        public DateTime? DataFineValiditaSedeAmministrativa { get; set; }

        /// <summary>
        ///     Indirizzo del sito web dell'impresa
        /// </summary>
        public string SitoWeb { get; set; }

        /// <summary>
        ///     Numero di cellulare
        /// </summary>
        public string Cellulare { get; set; }

        /// <summary>
        ///     PIN
        /// </summary>
        public string PIN { get; set; }

        /// <summary>
        ///     Codice cassa edile consulente
        /// </summary>
        public int IdConsulente { get; set; }

        /// <summary>
        ///     Data di iscrizione dell'impresa all'interno delle anagrafiche cassa edile
        /// </summary>
        public DateTime DataIscrizione { get; set; } = DateTime.MinValue;

        /// <summary>
        ///     Data di cancellazione da CE
        /// </summary>
        public DateTime DataDisdetta { get; set; }

        /// <summary>
        ///     Data di generazione del PIN
        /// </summary>
        public DateTime? DataGenerazionePIN { get; set; }

        public string GuidEdilconnect { get; set; }
    }
}