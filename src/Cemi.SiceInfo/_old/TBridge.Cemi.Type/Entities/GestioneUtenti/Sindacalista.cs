namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class Sindacalista : Utente
    {
        public Sindacalista()
        {
        }

        public Sindacalista(int _id)
        {
            Id = _id;
        }

        public Sindacalista(int idUtente, string userName)
            : base(idUtente, userName, null)
        {
        }

        public Sindacato Sindacato { get; set; }

        public ComprensorioSindacale ComprensorioSindacale { get; set; }

        public int Id { get; set; }

        public string Nome { get; set; }

        public string Cognome { get; set; }

        /// <summary>
        ///     Solo GET: ritorna "Nome Cognome"
        /// </summary>
        public string NomeCompleto => string.Format("{0} {1}", Cognome, Nome);
    }
}