namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    public class LavoratoreConImpresaSmsCredenziali : LavoratoreConImpresaSms
    {
        public string Username { get; set; }

        public new string Password { get; set; }
    }
}