using System;

namespace TBridge.Cemi.Type.Entities.GestioneUtenti
{
    [Serializable]
    public class Funzionalita
    {
        private bool assegnabile = true;
        private string nome;

        public Funzionalita()
        {
        }

        public Funzionalita(int idFunzionalita, string nome, string descrizione)
        {
            IdFunzionalita = idFunzionalita;
            this.nome = nome;
            Descrizione = descrizione;
        }

        public int IdFunzionalita { get; set; }

        public string Nome
        {
            get => nome;
            set => nome = value;
        }

        public string Descrizione { get; set; }

        public bool Assegnabile
        {
            get => assegnabile;
            set => assegnabile = value;
        }


        public override string ToString()
        {
            return nome;
        }
    }
}