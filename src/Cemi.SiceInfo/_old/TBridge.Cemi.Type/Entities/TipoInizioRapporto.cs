using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoInizioRapporto
    {
        public string IdTipoInizioRapporto { get; set; }

        public string Descrizione { get; set; }
    }
}