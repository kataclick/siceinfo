using System;

namespace TBridge.Cemi.Type.Entities
{
    [Serializable]
    public class TipoContratto
    {
        public string IdContratto { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}