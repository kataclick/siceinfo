﻿namespace TBridge.Cemi.Type.Entities
{
    public class ImpresaRecapiti
    {
        public int IdImpresa { get; set; }

        public string CodiceFiscale { get; set; }

        public string RagioneSociale { get; set; }

        public string StatoImpresa { get; set; }

        public Indirizzo IndirizzoSedeLegale { get; set; }

        public string PressoSedeLegale { get; set; }

        public string TelefonoSedeLegale { get; set; }

        public string FaxSedeLegale { get; set; }

        public string EmailSedeLegale { get; set; }

        public string PECSedeLegale { get; set; }

        public Indirizzo IndirizzoSedeAmministrativa { get; set; }

        public string PressoSedeAmministrativa { get; set; }

        public string TelefonoSedeAmministrativa { get; set; }

        public string FaxSedeAmministrativa { get; set; }

        public string EmailSedeAmministrativa { get; set; }

        public Indirizzo IndirizzoCorrispondenza { get; set; }

        public string PressoCorrispondenza { get; set; }

        public string TelefonoCorrispondenza { get; set; }

        public string FaxCorrispondenza { get; set; }

        public string EmailCorrispondenza { get; set; }
    }
}