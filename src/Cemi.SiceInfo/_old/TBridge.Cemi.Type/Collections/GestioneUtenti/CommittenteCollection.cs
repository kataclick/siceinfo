using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace TBridge.Cemi.Type.Collections.GestioneUtenti
{
    [Serializable]
    public class CommittenteCollection : List<Committente>
    {
    }
}