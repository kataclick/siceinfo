using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Colonie;

namespace TBridge.Cemi.Type.Collections.Colonie
{
    [Serializable]
    public class AutobusCollection : List<Autobus>
    {
    }
}