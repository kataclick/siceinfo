using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Colonie;

namespace TBridge.Cemi.Type.Collections.Colonie
{
    [Serializable]
    public class CostoPerCassaCollection : List<CostoPerCassa>
    {
        public CostoPerCassa GetByCassaEdile(string cassaEdile)
        {
            foreach (CostoPerCassa costoTemp in this)
            {
                if (costoTemp.CassaEdile == cassaEdile)
                    return costoTemp;
            }

            return null;
        }
    }
}