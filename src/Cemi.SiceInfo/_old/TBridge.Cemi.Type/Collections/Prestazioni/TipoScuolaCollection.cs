using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Collections.Prestazioni
{
    [Serializable]
    public class TipoScuolaCollection : List<TipoScuola>
    {
    }
}