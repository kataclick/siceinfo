using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Collections.Prestazioni
{
    [Serializable]
    public class FatturaDichiarataCollection : List<FatturaDichiarata>
    {
        /// <summary>
        ///     Ritorna la denuncia con data maggiore, NULL se non vi sono fatture
        /// </summary>
        public DateTime? MaxDataFattura
        {
            get
            {
                DateTime? dataMinima = null;

                foreach (FatturaDichiarata fattura in this)
                {
                    if (!fattura.DataAnnullamento.HasValue)
                    {
                        if (dataMinima.HasValue && fattura.Data > dataMinima.Value)
                        {
                            dataMinima = fattura.Data;
                        }
                        else if (!dataMinima.HasValue)
                            dataMinima = fattura.Data;
                    }
                }

                return dataMinima;
            }
        }

        /// <summary>
        ///     ritorna la denuncia con data minore, NULL se non vi sono fatture
        /// </summary>
        public DateTime? MinDataFattura
        {
            get
            {
                DateTime? dataMinima = null;

                foreach (FatturaDichiarata fattura in this)
                {
                    if (!fattura.DataAnnullamento.HasValue)
                    {
                        if (dataMinima.HasValue && fattura.Data < dataMinima.Value)
                        {
                            dataMinima = fattura.Data;
                        }
                        else if (!dataMinima.HasValue)
                            dataMinima = fattura.Data;
                    }
                }

                return dataMinima;
            }
        }

        public bool IsPresenteFatturaSaldo()
        {
            foreach (FatturaDichiarata fattura in this)
            {
                if (fattura.Saldo)
                    return true;
            }

            return false;
        }

        public bool IsGiaPresente(FatturaDichiarata fattura)
        {
            if (fattura == null)
                throw new ArgumentNullException("fattura");
            foreach (FatturaDichiarata fatturaDellaLista in this)
            {
                if (fattura.Data == fatturaDellaLista.Data
                    && fattura.Numero == fatturaDellaLista.Numero)
                    return true;
            }

            return false;
        }
    }
}