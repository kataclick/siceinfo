using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Collections.Prestazioni
{
    [Serializable]
    public class FatturaImportoCollection : List<FatturaImporto>
    {
        public decimal TotaleFattureConImportoTotale()
        {
            decimal importo = 0;

            foreach (FatturaImporto fImporto in this)
            {
                if (fImporto.Descrizione == "Importo Totale")
                {
                    importo += fImporto.Valore;
                }
            }

            return importo;
        }
    }
}