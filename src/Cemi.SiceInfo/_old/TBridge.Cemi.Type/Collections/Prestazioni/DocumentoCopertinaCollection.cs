﻿using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Collections.Prestazioni
{
    public class DocumentoCopertinaCollection : List<DocumentoCopertina>
    {
        public int NumeroDocumentiConsegnati
        {
            get
            {
                int counter = 0;

                foreach (DocumentoCopertina doc in this)
                {
                    if (doc.Consegnato)
                    {
                        counter++;
                    }
                }

                return counter;
            }
        }
    }
}