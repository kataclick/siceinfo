using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Collections.Prestazioni
{
    [Serializable]
    public class PrestazioneImportoCollection : List<PrestazioneImporto>
    {
        public decimal Totale()
        {
            decimal totale = 0;

            foreach (PrestazioneImporto importo in this)
            {
                totale += importo.Valore;
            }

            return totale;
        }
    }
}