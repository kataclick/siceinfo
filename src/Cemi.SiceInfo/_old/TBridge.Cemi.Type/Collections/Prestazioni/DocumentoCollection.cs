using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace TBridge.Cemi.Type.Collections.Prestazioni
{
    [Serializable]
    public class DocumentoCollection : List<Documento>
    {
        public bool DocumentoGiaPresente(Documento documento)
        {
            foreach (Documento doc in this)
            {
                if (doc.TipoDocumento.IdTipoDocumento == documento.TipoDocumento.IdTipoDocumento
                    && doc.RiferitoA == documento.RiferitoA)
                {
                    return true;
                }
            }

            return false;
        }
    }
}