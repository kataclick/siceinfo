﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Corsi;

namespace TBridge.Cemi.Type.Collections.Corsi
{
    [Serializable]
    public class ModuloCollection : List<Modulo>
    {
        public int DurataComplessiva
        {
            get
            {
                int durataComplessiva = 0;

                foreach (Modulo modulo in this)
                {
                    durataComplessiva += modulo.OreDurata;
                }

                return durataComplessiva;
            }
        }
    }
}