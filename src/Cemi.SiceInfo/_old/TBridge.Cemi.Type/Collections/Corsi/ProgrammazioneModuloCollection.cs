﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Corsi;

namespace TBridge.Cemi.Type.Collections.Corsi
{
    [Serializable]
    public class ProgrammazioneModuloCollection : List<ProgrammazioneModulo>
    {
        public ProgrammazioneModulo GetProgrammazioneModulo(int idProgrammazioneModulo)
        {
            foreach (ProgrammazioneModulo prog in this)
            {
                if (prog.IdProgrammazioneModulo.HasValue
                    && prog.IdProgrammazioneModulo.Value == idProgrammazioneModulo)
                {
                    return prog;
                }
            }

            return null;
        }

        public bool EsistonoIscritti()
        {
            foreach (ProgrammazioneModulo prog in this)
            {
                if (prog.Partecipanti > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool EsisteRegistroConfermato()
        {
            foreach (ProgrammazioneModulo prog in this)
            {
                if (prog.PresenzeConfermate)
                {
                    return true;
                }
            }

            return false;
        }
    }
}