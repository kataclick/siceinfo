using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Corsi;

namespace TBridge.Cemi.Type.Collections.Corsi
{
    [Serializable]
    public class PrestazioniDomandeCorsiCollection : List<PrestazioneDomandaCorso>
    {
    }
}