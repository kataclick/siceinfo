﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class ContrattoCollection : List<Contratto>
    {
        public bool PresenteNellaLista(int idContratto)
        {
            foreach (Contratto contratto in this)
            {
                if (contratto.Id == idContratto)
                {
                    return true;
                }
            }

            return false;
        }
    }
}