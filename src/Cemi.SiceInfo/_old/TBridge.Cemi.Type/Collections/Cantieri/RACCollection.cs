using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class RACCollection : List<RAC>
    {
        public new bool Contains(RAC rac)
        {
            bool res = false;

            foreach (RAC r in this)
            {
                if (r.IdRAC == rac.IdRAC)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }
    }
}