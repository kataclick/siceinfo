using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class SubappaltoCollection : List<Subappalto>
    {
        public Impresa FindImpresa(string codiceFiscale)
        {
            foreach (Subappalto subappalto in this)
            {
                if (subappalto.SubAppaltata != null
                    && subappalto.SubAppaltata.CodiceFiscale == codiceFiscale)
                {
                    return subappalto.SubAppaltata;
                }

                if (subappalto.SubAppaltatrice != null
                    && subappalto.SubAppaltatrice.CodiceFiscale == codiceFiscale)
                {
                    return subappalto.SubAppaltatrice;
                }
            }

            return null;
        }
    }
}