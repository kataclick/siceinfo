using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class CantiereCollection : List<Cantiere>
    {
        public new bool Contains(Cantiere cantiere)
        {
            bool res = false;

            foreach (Cantiere cantiereLista in this)
            {
                if (cantiereLista.IdCantiere.HasValue && cantiere.IdCantiere.HasValue &&
                    cantiere.IdCantiere.Value == cantiereLista.IdCantiere.Value)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }
    }
}