using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class ImpresaCollection : List<Impresa>
    {
        /// <summary>
        ///     Controlla la presenza dell'impresa nella lista; l'uguaglianza si basa su idImpresa
        ///     e tipoImpresa (distingue le imprese nell'anagrafica reale da quella di Cantieri
        /// </summary>
        /// <param name="impresa"></param>
        /// <returns></returns>
        public new bool Contains(Impresa impresa)
        {
            foreach (Impresa imp in this)
            {
                //controlliamo se l'impresa � presente
                if (imp.IdImpresa.HasValue && impresa.IdImpresa.HasValue && imp.IdImpresa == impresa.IdImpresa &&
                    imp.TipoImpresa == impresa.TipoImpresa)
                    return true;
                if (imp.TipoImpresa == TipologiaImpresa.Cantieri && imp.IdImpresaTemporaneo.HasValue &&
                    impresa.IdImpresaTemporaneo.HasValue && imp.IdImpresaTemporaneo == impresa.IdImpresaTemporaneo)
                    return true;
            }
            return false;
        }
    }
}