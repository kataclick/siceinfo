using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class RapportoIspezioneLavoratoreCollection : List<RapportoIspezioneLavoratore>
    {
    }
}