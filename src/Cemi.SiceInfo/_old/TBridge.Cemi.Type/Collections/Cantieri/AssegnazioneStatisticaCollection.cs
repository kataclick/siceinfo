﻿using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    public class AssegnazioneStatisticaCollection : List<AssegnazioneStatistica>
    {
        public int Totale
        {
            get
            {
                int totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    totale += assStat.NumeroCantieri;
                }

                return totale;
            }
        }

        public int TotaleAssegnati
        {
            get
            {
                int totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    if (assStat.Assegnati)
                    {
                        totale += assStat.NumeroCantieri;
                    }
                }

                return totale;
            }
        }

        public int TotaleRifiutati
        {
            get
            {
                int totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    if (!assStat.Assegnati)
                    {
                        totale += assStat.NumeroCantieri;
                    }
                }

                return totale;
            }
        }

        public int TotalePresiInCarico
        {
            get
            {
                int totale = 0;

                foreach (AssegnazioneStatistica assStat in this)
                {
                    totale += assStat.NumeroPresiInCarico;
                }

                return totale;
            }
        }
    }
}