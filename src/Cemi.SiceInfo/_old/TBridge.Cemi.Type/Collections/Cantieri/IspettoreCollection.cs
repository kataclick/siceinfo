using System;
using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    [Serializable]
    public class IspettoreCollection : List<Ispettore>
    {
        /// <summary>
        ///     Controlla la presenza dell'ispettore nella lista; l'uguaglianza si basa su idIspettore
        /// </summary>
        /// <param name="ispettore"></param>
        /// <returns></returns>
        public new bool Contains(Ispettore ispettore)
        {
            foreach (Ispettore isp in this)
            {
                if (ispettore.IdIspettore.HasValue && isp.IdIspettore.HasValue &&
                    isp.IdIspettore.Value == ispettore.IdIspettore.Value)
                    return true;
            }

            return false;
        }
    }
}