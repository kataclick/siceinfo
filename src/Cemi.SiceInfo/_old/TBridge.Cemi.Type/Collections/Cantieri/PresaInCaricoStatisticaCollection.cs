﻿using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    public class PresaInCaricoStatisticaCollection : List<PresaInCaricoStatistica>
    {
        public int TotalePresiInCarico
        {
            get
            {
                int totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    totale += presInCarico.NumeroPreseInCarico;
                }

                return totale;
            }
        }

        public int TotaleIspezioni
        {
            get
            {
                int totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    if (presInCarico.StatoIspezione.HasValue)
                    {
                        totale += presInCarico.NumeroPreseInCarico;
                    }
                }

                return totale;
            }
        }

        public int TotaleIspezioniInVerifica
        {
            get
            {
                int totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    if (presInCarico.StatoIspezione.HasValue &&
                        presInCarico.StatoIspezione.Value == StatoIspezione.InVerifica)
                    {
                        totale += presInCarico.NumeroPreseInCarico;
                    }
                }

                return totale;
            }
        }

        public int TotaleIspezioniConcluse
        {
            get
            {
                int totale = 0;

                foreach (PresaInCaricoStatistica presInCarico in this)
                {
                    if (presInCarico.StatoIspezione.HasValue &&
                        presInCarico.StatoIspezione.Value == StatoIspezione.Conclusa)
                    {
                        totale += presInCarico.NumeroPreseInCarico;
                    }
                }

                return totale;
            }
        }
    }
}