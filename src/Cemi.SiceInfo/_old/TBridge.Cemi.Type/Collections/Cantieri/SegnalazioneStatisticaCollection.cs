﻿using System.Collections.Generic;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Type.Collections.Cantieri
{
    public class SegnalazioneStatisticaCollection : List<SegnalazioneStatistica>
    {
        public int Totale
        {
            get
            {
                int totale = 0;

                foreach (SegnalazioneStatistica segStat in this)
                {
                    totale += segStat.NumeroCantieri;
                }

                return totale;
            }
        }

        public int TotaleAssegnazioniRifiuti
        {
            get
            {
                int totale = 0;

                foreach (SegnalazioneStatistica segStat in this)
                {
                    totale += segStat.NumeroAssegnatiRifiutati;
                }

                return totale;
            }
        }
    }
}