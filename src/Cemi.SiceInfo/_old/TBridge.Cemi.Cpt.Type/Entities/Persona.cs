using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class Persona
    {
        private string fax;
        private int? idPersona;
        private string indirizzo;
        private string nominativo;
        private string ragioneSociale;
        private string telefono;

        public Persona()
        {
        }

        public Persona(int? idPersona, string nominativo, string ragioneSociale, string indirizzo,
            string telefono, string fax)
        {
            this.idPersona = idPersona;
            this.nominativo = nominativo;
            this.ragioneSociale = ragioneSociale;
            this.indirizzo = indirizzo;
            this.telefono = telefono;
            this.fax = fax;
        }

        public int? IdPersona
        {
            get => idPersona;
            set => idPersona = value;
        }

        public string Nominativo
        {
            get => nominativo;
            set => nominativo = value;
        }

        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        public string Fax
        {
            get => fax;
            set => fax = value;
        }
    }
}