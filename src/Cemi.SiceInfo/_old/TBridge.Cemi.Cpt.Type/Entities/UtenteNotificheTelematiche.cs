using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class UtenteNotificheTelematiche
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CartaIdentita { get; set; }

        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string Telefono { get; set; }

        public string Fax { get; set; }

        public Guid IdUtente { get; set; }

        public CommittenteNotificheTelematiche CommittenteTelematiche { get; set; }
    }
}