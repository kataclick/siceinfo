namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class GradoIrregolarita
    {
        public short IdGradoIrregolarita { get; set; }


        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}