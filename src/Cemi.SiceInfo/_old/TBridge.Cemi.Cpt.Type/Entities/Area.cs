using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class Area
    {
        private string descrizione;
        private short idArea;

        public short IdArea
        {
            get => idArea;
            set => idArea = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }
    }
}