using System;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class Subappalto
    {
        private Impresa appaltante;
        private Impresa appaltata;
        private int idNotifica;
        private int? idSubappalto;

        public Subappalto()
        {
        }

        public Subappalto(int? idSubappalto, int idNotifica, Impresa appaltante, Impresa appaltata)
        {
            this.idSubappalto = idSubappalto;
            this.idNotifica = idNotifica;
            this.appaltante = appaltante;
            this.appaltata = appaltata;
        }

        public int? IdSubappalto
        {
            get => idSubappalto;
            set => idSubappalto = value;
        }

        public int IdNotifica
        {
            get => idNotifica;
            set => idNotifica = value;
        }

        public Impresa Appaltante
        {
            get => appaltante;
            set => appaltante = value;
        }

        public string AppaltanteStringa
        {
            get
            {
                if (appaltante != null) return appaltante.RagioneSociale;
                return string.Empty;
            }
        }

        public string AppaltanteCodiceFiscale
        {
            get
            {
                if (appaltante != null) return appaltante.CodiceFiscale;
                return string.Empty;
            }
        }

        public string AppaltantePartitaIva
        {
            get
            {
                if (appaltante != null) return appaltante.PartitaIva;
                return string.Empty;
            }
        }

        public string AppaltanteIndirizzo
        {
            get
            {
                if (appaltante != null)
                    return string.Format("{0} - {1} {2}", appaltante.Indirizzo, appaltante.Comune,
                        appaltante.Provincia);
                return string.Empty;
            }
        }

        public string AppaltataCodiceFiscale
        {
            get
            {
                if (appaltata != null) return appaltata.CodiceFiscale;
                return string.Empty;
            }
        }

        public string AppaltataPartitaIva
        {
            get
            {
                if (appaltata != null) return appaltata.PartitaIva;
                return string.Empty;
            }
        }

        public string AppaltataIndirizzo
        {
            get
            {
                if (appaltata != null)
                    return string.Format("{0} - {1} {2}", appaltata.Indirizzo, appaltata.Comune, appaltata.Provincia);
                return string.Empty;
            }
        }

        public Impresa Appaltata
        {
            get => appaltata;
            set => appaltata = value;
        }

        public string AppaltataStringa
        {
            get
            {
                if (appaltata != null) return appaltata.RagioneSociale;
                return string.Empty;
            }
        }

        public bool Affidatarie { get; set; }
    }
}