using System;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Enums;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class Visita
    {
        public int? IdVisita { get; set; }

        public EnteVisita Ente { get; set; }

        public int IdUtente { get; set; }

        public DateTime Data { get; set; }

        public TipologiaVisita Tipologia { get; set; }

        public EsitoVisita Esito { get; set; }

        public AllegatoCollection Allegati { get; set; }

        public int IdNotifica { get; set; }

        public IndirizzoCollection Indirizzi { get; set; }

        public GradoIrregolarita GradoIrregolarita { get; set; }
    }
}