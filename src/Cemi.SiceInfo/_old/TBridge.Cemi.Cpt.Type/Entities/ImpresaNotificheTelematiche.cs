using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class ImpresaNotificheTelematiche
    {
        public Guid IdTemporaneo { get; set; }

        public int? IdImpresaTelematica { get; set; }

        public int? IdImpresaAnagrafica { get; set; }

        public string RagioneSociale { get; set; }

        public bool LavoratoreAutonomo { get; set; }

        public string PartitaIva { get; set; }

        public string CodiceFiscale { get; set; }

        public string AttivitaPrevalente { get; set; }

        public int? IdImpresa { get; set; }

        public string IdCassaEdile { get; set; }

        public string MatricolaINAIL { get; set; }

        public string MatricolaINPS { get; set; }

        public string MatricolaCCIAA { get; set; }

        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string Telefono { get; set; }

        public string Fax { get; set; }
    }
}