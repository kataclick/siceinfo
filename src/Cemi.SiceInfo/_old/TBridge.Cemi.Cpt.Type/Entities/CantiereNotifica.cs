using System;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class CantiereNotifica
    {
        private Committente committente;
        private DateTime data;
        private DateTime? dataFineLavori;
        private DateTime? dataInizioLavori;
        private DateTime dataInserimento;
        private int idIndirizzo;
        private int idNotifica;
        private Impresa impresaAppaltata;
        private Impresa impresaRicercata;
        private Impresa impresaSubappaltata;
        private Indirizzo indirizzo;
        private string naturaOpera;
        private int numeroVisiteASL;
        private int numeroVisiteASLERSLT;
        private int numeroVisiteCassaEdile;
        private int numeroVisiteCPT;
        private int numeroVisiteDPL;

        public int IdNotifica
        {
            get => idNotifica;
            set => idNotifica = value;
        }

        public int IdIndirizzo
        {
            get => idIndirizzo;
            set => idIndirizzo = value;
        }

        public DateTime Data
        {
            get => data;
            set => data = value;
        }

        public string NaturaOpera
        {
            get => naturaOpera;
            set => naturaOpera = value;
        }

        public Indirizzo Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string IndirizzoCompleto => indirizzo.IndirizzoCompleto;

        public string IndirizzoDenominazione
        {
            get
            {
                if (Indirizzo != null)
                {
                    return string.Format("{0} {1}", Indirizzo.Indirizzo1, Indirizzo.Civico);
                }
                return string.Empty;
            }
        }

        public string IndirizzoComune
        {
            get
            {
                if (Indirizzo != null)
                {
                    return Indirizzo.Comune;
                }
                return string.Empty;
            }
        }

        public string IndirizzoProvincia
        {
            get
            {
                if (Indirizzo != null)
                {
                    return Indirizzo.Provincia;
                }
                return string.Empty;
            }
        }

        public string IndirizzoCap
        {
            get
            {
                if (Indirizzo != null)
                {
                    return Indirizzo.Cap;
                }
                return string.Empty;
            }
        }

        public string Latitudine => indirizzo.Latitudine.ToString();

        public string Longitudine => indirizzo.Longitudine.ToString();

        public DateTime? DataInizioLavori
        {
            get => dataInizioLavori;
            set => dataInizioLavori = value;
        }

        public DateTime? DataFineLavori
        {
            get => dataFineLavori;
            set => dataFineLavori = value;
        }

        public DateTime DataInserimento
        {
            get => dataInserimento;
            set => dataInserimento = value;
        }

        /// <summary>
        ///     Ritorna il committente; null se non presente
        /// </summary>
        public Committente Committente
        {
            get => committente;
            set => committente = value;
        }

        /// <summary>
        ///     Ritorna la ragione sociale del committente
        /// </summary>
        public string CommittenteRagioneSociale
        {
            get
            {
                if (committente != null)
                    return committente.RagioneSociale;
                return string.Empty;
            }
        }

        /// <summary>
        ///     Impresa Appaltata; null se non presente
        /// </summary>
        public Impresa ImpresaAppaltata
        {
            get => impresaAppaltata;
            set => impresaAppaltata = value;
        }

        /// <summary>
        ///     Impresa Subappaltata; null se non presente
        /// </summary>
        public Impresa ImpresaSubappaltata
        {
            get => impresaSubappaltata;
            set => impresaSubappaltata = value;
        }

        /// <summary>
        ///     Ragione sociale dell' impresa appaltata
        /// </summary>
        public string ImpresaAppaltataRagioneSociale
        {
            get
            {
                if (impresaAppaltata != null)
                    return impresaAppaltata.RagioneSociale;
                return string.Empty;
            }
        }

        /// <summary>
        ///     Ragione sociale dell' impresa subappaltata
        /// </summary>
        public string ImpresaSubppaltataRagioneSociale
        {
            get
            {
                if (impresaSubappaltata != null)
                    return impresaSubappaltata.RagioneSociale;
                return string.Empty;
            }
        }

        /// <summary>
        ///     L'oggetto � in realt� ImpresaAppaltata o SubAppaltata. Qui eien
        /// </summary>
        public Impresa ImpresaRicercata
        {
            get => impresaRicercata;
            set => impresaRicercata = value;
        }

        /// <summary>
        ///     Ragione sociale dell' impresa ricercata
        /// </summary>
        public string ImpresaRicercataRagioneSociale
        {
            get
            {
                if (impresaRicercata != null)
                    return impresaRicercata.RagioneSociale;
                return string.Empty;
            }
        }

        public string ImpresaRicercataPartitaIva
        {
            get
            {
                if (impresaRicercata != null)
                    return impresaRicercata.PartitaIva;
                return string.Empty;
            }
        }

        public string ImpresaRicercataCodiceFiscale
        {
            get
            {
                if (impresaRicercata != null)
                    return impresaRicercata.CodiceFiscale;
                return string.Empty;
            }
        }

        public int NumeroVisiteASL
        {
            get => numeroVisiteASL;
            set => numeroVisiteASL = value;
        }

        public int NumeroVisiteCPT
        {
            get => numeroVisiteCPT;
            set => numeroVisiteCPT = value;
        }

        public int NumeroVisiteDPL
        {
            get => numeroVisiteDPL;
            set => numeroVisiteDPL = value;
        }

        public int NumeroVisiteASLERSLT
        {
            get => numeroVisiteASLERSLT;
            set => numeroVisiteASLERSLT = value;
        }

        public int NumeroVisiteCassaEdile
        {
            get => numeroVisiteCassaEdile;
            set => numeroVisiteCassaEdile = value;
        }

        public int? NumeroImprese { get; set; }

        public int? NumeroLavoratori { get; set; }

        public decimal Ammontare { get; set; }

        public string ProtocolloRegioneNotifica { set; get; }
    }
}