namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class PersonaNotificheTelematiche : Persona
    {
        public string Citta { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string PersonaCognome { get; set; }

        public string PersonaNome { get; set; }

        public string PersonaComune { get; set; }

        public string PersonaProvincia { get; set; }

        public string PersonaCap { get; set; }

        public string PersonaCellulare { get; set; }

        public string PersonaEmail { get; set; }

        public string PersonaCodiceFiscale { get; set; }

        public string EntePartitaIva { get; set; }

        public string EnteCodiceFiscale { get; set; }

        public string EnteIndirizzo { get; set; }

        public string EnteComune { get; set; }

        public string EnteProvincia { get; set; }

        public string EnteCap { get; set; }

        public string EnteTelefono { get; set; }

        public string EnteFax { get; set; }
    }
}