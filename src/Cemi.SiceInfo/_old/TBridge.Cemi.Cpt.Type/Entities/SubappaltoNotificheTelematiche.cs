using System;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class SubappaltoNotificheTelematiche
    {
        public int? IdSubappalto { get; set; }

        public ImpresaNotificheTelematiche ImpresaSelezionata { get; set; }

        public ImpresaNotificheTelematiche AppaltataDa { get; set; }

        public bool Affidatarie { get; set; }

        public string ImpresaSelezionataRagioneSociale =>
            ImpresaSelezionata != null ? ImpresaSelezionata.RagioneSociale : string.Empty;

        public string ImpresaSelezionataPartitaIva =>
            ImpresaSelezionata != null ? ImpresaSelezionata.PartitaIva : string.Empty;

        public string ImpresaSelezionataCodiceFiscale =>
            ImpresaSelezionata != null ? ImpresaSelezionata.CodiceFiscale : string.Empty;

        public string ImpresaAppaltataDaRagioneSociale =>
            AppaltataDa != null ? AppaltataDa.RagioneSociale : string.Empty;

        public string ImpresaAppaltataDaPartitaIva => AppaltataDa != null ? AppaltataDa.PartitaIva : string.Empty;

        public Guid IdTemporaneoImpresaSelezionata
        {
            get
            {
                if (ImpresaSelezionata != null) return ImpresaSelezionata.IdTemporaneo;
                return Guid.Empty;
            }
        }
    }
}