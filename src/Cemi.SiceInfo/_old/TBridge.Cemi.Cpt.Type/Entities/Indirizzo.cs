using System;
using System.Text;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    /// <summary>
    ///     Rappresenta un singolo indirizzo di una notifica
    /// </summary>
    [Serializable]
    public class Indirizzo : Cemi.Type.Entities.Geocode.Indirizzo
    {
        public Indirizzo()
        {
        }

        public Indirizzo(string indirizzo, string civico, string comune, string provincia, string cap,
            decimal? latitudine, decimal? longitudine)
        {
            Indirizzo1 = indirizzo;
            Civico = civico;
            Comune = comune;
            Provincia = provincia;
            Cap = cap;
            Latitudine = latitudine;
            Longitudine = longitudine;
        }

        public Indirizzo(string indirizzo, string civico, string comune, string provincia, string cap,
            decimal? latitudine, decimal? longitudine, string infoAggiuntiva)
        {
            Indirizzo1 = indirizzo;
            Civico = civico;
            Comune = comune;
            Provincia = provincia;
            Cap = cap;
            Latitudine = latitudine;
            Longitudine = longitudine;
            InfoAggiuntiva = infoAggiuntiva;
        }

        public int? IdIndirizzo { get; set; }

        public string Indirizzo1
        {
            get => Via;
            set => NomeVia = value;
        }

        public string InfoAggiuntiva
        {
            get => InformazioniAggiuntive;
            set => InformazioniAggiuntive = value;
        }

        public string IndirizzoDenominazione => IndirizzoBase;

        public string IndirizzoPerGeocoder => string.Format("{0}, {1}", IndirizzoCompleto, Stato);

        public string IndirizzoPerGeocoderGenerico => IndirizzoCompletoGeocode;

        public new string IndirizzoCompleto
        {
            get
            {
                if (!string.IsNullOrEmpty(InfoAggiuntiva))
                    return string.Format("{0} {1}{2} {3} {4} ({5})", Indirizzo1, Civico, Environment.NewLine, Comune,
                        Provincia, InfoAggiuntiva);
                return string.Format("{0} {1}{2} {3} {4}", Indirizzo1, Civico, Environment.NewLine, Comune, Provincia);
            }
        }

        public string IndirizzoDatiAggiuntivi
        {
            get
            {
                StringBuilder res = new StringBuilder();

                if (DataInizioLavori.HasValue)
                {
                    res.Append(string.Format("Data Inizio Lav. <b>{0}</b><br />",
                        DataInizioLavori.Value.ToShortDateString()));
                }
                if (NumeroMassimoLavoratori.HasValue)
                {
                    res.Append(string.Format("Num. max lav. <b>{0}</b><br />", NumeroMassimoLavoratori));
                }
                if (!string.IsNullOrEmpty(DescrizioneDurata) && NumeroDurata.HasValue)
                {
                    res.Append(string.Format("Durata <b>{0} {1}</b><br />", NumeroDurata, DescrizioneDurata));
                }

                return res.ToString();
            }
        }

        public DateTime? DataInizioLavori { get; set; }

        public string DescrizioneDurata { get; set; }

        public int? NumeroDurata { get; set; }

        public int? NumeroMassimoLavoratori { get; set; }

        public bool HaCoordinate()
        {
            return Georeferenziato;
        }
    }
}