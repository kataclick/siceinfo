using System;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class CommittenteNotificheTelematiche : Committente
    {
        public int? IdCommittenteTelematiche { get; set; }

        public int? IdCommittenteAnagrafica { get; set; }

        public TipologiaCommittente TipologiaCommittente { get; set; }

        public string PersonaCognome { get; set; }

        public string PersonaNome { get; set; }

        public string PersonaIndirizzo { get; set; }

        public string PersonaComune { get; set; }

        public string PersonaProvincia { get; set; }

        public string PersonaCap { get; set; }

        public string PersonaTelefono { get; set; }

        public string PersonaFax { get; set; }

        public string PersonaCellulare { get; set; }

        public string PersonaEmail { get; set; }

        public string PersonaCodiceFiscale { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(RagioneSociale))
            {
                return RagioneSociale;
            }
            return string.Format("{0} {1}", PersonaCognome, PersonaNome);
        }
    }
}