namespace TBridge.Cemi.Cpt.Type.Entities
{
    public class TipologiaCommittente
    {
        public int IdTipologiaCommittente { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}