using System;
using System.Xml.Serialization;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace TBridge.Cemi.Cpt.Type.Entities
{
    [Serializable]
    public class Notifica
    {
        private decimal? ammontareComplessivo;
        private bool annullata;
        private Area area;
        private Persona coordinatoreSicurezzaProgettazione;
        private Persona coordinatoreSicurezzaRealizzazione;
        private DateTime data;
        private DateTime? dataAnnullamento;
        private DateTime? dataFineLavori;
        private DateTime? dataInizioLavori;
        private DateTime dataInserimento;
        private DateTime? dataNotificaPadre;
        private Persona direttoreLavori;
        private int? durata;
        private int? idNotifica;
        private int? idNotificaPadre;
        private int idNotificaRiferimento;
        private bool impresaPresente;
        private IndirizzoCollection indirizzi;
        private string naturaOpera;
        private string numeroAppalto;
        private int? numeroGiorniUomo;
        private int? numeroImprese;
        private int? numeroLavoratoriAutonomi;
        private int? numeroMassimoLavoratori;
        private int numeroVisiteASL;
        private int numeroVisiteASLERSLT;
        private int numeroVisiteCassaEdile;
        private int numeroVisiteCPT;
        private int numeroVisiteDPL;
        private bool responsabileCommittente;
        private NotificaCollection storia;
        private SubappaltoCollection subappalti;
        private string utente;
        private string utenteAnnullamento;

        public Notifica()
        {
            indirizzi = new IndirizzoCollection();
            subappalti = new SubappaltoCollection();
            storia = new NotificaCollection();
        }

        public Notifica(int? idNotifica, int? idNotificaPadre, DateTime data, IndirizzoCollection indirizzi,
            Committente committente,
            string naturaOpera, Persona coordinatoreSicurezzaProgettazione,
            Persona coordinatoreSicurezzaRealizzazione,
            Persona direttoreLavori, DateTime? dataInizioLavori, DateTime? dataFineLavori, int? durata,
            int? numeroGiorniUomo,
            int? numeroMassimoLavoratori, int? numeroImprese, int? numeroLavoratoriAutonomi,
            SubappaltoCollection subappalti,
            decimal? ammontareComplessivo, string utente)
        {
            this.idNotifica = idNotifica;
            this.idNotificaPadre = idNotificaPadre;
            this.data = data;
            this.indirizzi = indirizzi;
            Committente = committente;
            this.naturaOpera = naturaOpera;
            this.coordinatoreSicurezzaProgettazione = coordinatoreSicurezzaProgettazione;
            this.coordinatoreSicurezzaRealizzazione = coordinatoreSicurezzaRealizzazione;
            this.direttoreLavori = direttoreLavori;
            this.dataInizioLavori = dataInizioLavori;
            this.dataFineLavori = dataFineLavori;
            this.durata = durata;
            this.numeroGiorniUomo = numeroGiorniUomo;
            this.numeroMassimoLavoratori = numeroMassimoLavoratori;
            this.numeroImprese = numeroImprese;
            this.numeroLavoratoriAutonomi = numeroLavoratoriAutonomi;
            this.subappalti = subappalti;
            this.ammontareComplessivo = ammontareComplessivo;
            this.utente = utente;

            storia = new NotificaCollection();
        }

        public bool? OperaPubblica { get; set; }

        public int? IdNotifica
        {
            get => idNotifica;
            set => idNotifica = value;
        }

        public int? IdNotificaPadre
        {
            get => idNotificaPadre;
            set => idNotificaPadre = value;
        }

        public DateTime? DataNotificaPadre
        {
            get => dataNotificaPadre;
            set => dataNotificaPadre = value;
        }

        public DateTime Data
        {
            get => data;
            set => data = value;
        }

        public IndirizzoCollection Indirizzi
        {
            get => indirizzi;
            set => indirizzi = value;
        }

        [XmlIgnore]
        public Committente Committente { get; set; }

        public string CommittenteRagioneSociale
        {
            get
            {
                if (Committente != null)
                    return Committente.RagioneSociale;
                return string.Empty;
            }
        }

        public string NaturaOpera
        {
            get => naturaOpera;
            set => naturaOpera = value;
        }

        public string NumeroAppalto
        {
            get => numeroAppalto;
            set => numeroAppalto = value;
        }

        [XmlIgnore]
        public Persona CoordinatoreSicurezzaProgettazione
        {
            get => coordinatoreSicurezzaProgettazione;
            set => coordinatoreSicurezzaProgettazione = value;
        }

        [XmlIgnore]
        public Persona CoordinatoreSicurezzaRealizzazione
        {
            get => coordinatoreSicurezzaRealizzazione;
            set => coordinatoreSicurezzaRealizzazione = value;
        }

        [XmlIgnore]
        public Persona DirettoreLavori
        {
            get => direttoreLavori;
            set => direttoreLavori = value;
        }

        public bool ResponsabileCommittente
        {
            get => responsabileCommittente;
            set => responsabileCommittente = value;
        }

        public DateTime? DataInizioLavori
        {
            get => dataInizioLavori;
            set => dataInizioLavori = value;
        }

        public DateTime? DataFineLavori
        {
            get => dataFineLavori;
            set => dataFineLavori = value;
        }

        public int? Durata
        {
            get => durata;
            set => durata = value;
        }

        public int? NumeroGiorniUomo
        {
            get => numeroGiorniUomo;
            set => numeroGiorniUomo = value;
        }

        public int? NumeroMassimoLavoratori
        {
            get => numeroMassimoLavoratori;
            set => numeroMassimoLavoratori = value;
        }

        public int? NumeroImprese
        {
            get => numeroImprese;
            set => numeroImprese = value;
        }

        public int? NumeroLavoratoriAutonomi
        {
            get => numeroLavoratoriAutonomi;
            set => numeroLavoratoriAutonomi = value;
        }

        public SubappaltoCollection Subappalti
        {
            get => subappalti;
            set => subappalti = value;
        }

        public decimal? AmmontareComplessivo
        {
            get => ammontareComplessivo;
            set => ammontareComplessivo = value;
        }

        public string Utente
        {
            get => utente;
            set => utente = value;
        }

        public DateTime DataInserimento
        {
            get => dataInserimento;
            set => dataInserimento = value;
        }

        public bool Annullata
        {
            get => annullata;
            set => annullata = value;
        }

        public DateTime? DataAnnullamento
        {
            get => dataAnnullamento;
            set => dataAnnullamento = value;
        }

        public string UtenteAnnullamento
        {
            get => utenteAnnullamento;
            set => utenteAnnullamento = value;
        }

        public NotificaCollection Storia
        {
            get => storia;
            set => storia = value;
        }

        public bool ImpresaPresente
        {
            get => impresaPresente;
            set => impresaPresente = value;
        }

        public int NumeroVisiteASL
        {
            get => numeroVisiteASL;
            set => numeroVisiteASL = value;
        }

        public int NumeroVisiteCPT
        {
            get => numeroVisiteCPT;
            set => numeroVisiteCPT = value;
        }

        public int NumeroVisiteDPL
        {
            get => numeroVisiteDPL;
            set => numeroVisiteDPL = value;
        }

        public int NumeroVisiteASLERSLT
        {
            get => numeroVisiteASLERSLT;
            set => numeroVisiteASLERSLT = value;
        }

        public int NumeroVisiteCassaEdile
        {
            get => numeroVisiteCassaEdile;
            set => numeroVisiteCassaEdile = value;
        }

        public Area Area
        {
            get => area;
            set => area = value;
        }

        public int IdNotificaRiferimento
        {
            get => idNotificaRiferimento;
            set => idNotificaRiferimento = value;
        }

        public string ProtocolloRegione { get; set; }

        public DateTime? DataPrimoInserimento { get; set; }
    }
}