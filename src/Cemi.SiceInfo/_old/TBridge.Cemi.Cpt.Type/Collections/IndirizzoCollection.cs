using System;
using System.Collections.Generic;
using TBridge.Cemi.Cpt.Type.Entities;

namespace TBridge.Cemi.Cpt.Type.Collections
{
    /// <summary>
    ///     Collezione di indirizzi
    /// </summary>
    [Serializable]
    public class IndirizzoCollection : List<Indirizzo>
    {
        public void AddUnico(Indirizzo indirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == indirizzo.IdIndirizzo)
                    return;
            }

            Add(indirizzo);
        }

        public Indirizzo GetById(int idIndirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == idIndirizzo)
                    return ind;
            }

            return null;
        }
    }
}