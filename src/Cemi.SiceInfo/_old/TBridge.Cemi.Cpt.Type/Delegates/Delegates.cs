using TBridge.Cemi.Cpt.Type.Collections;

namespace TBridge.Cemi.Cpt.Type.Delegates
{
    public delegate void ResponsabileDeiLavoriEventHandler(bool nonNominato);

    public delegate void CoordinatoreSicurezzaProgettazioneEventHandler(bool nonNominato);

    public delegate void CoordinatoreSicurezzaEsecuzioneEventHandler(bool nonNominato);

    public delegate void CoordinatoreSicurezzaProgettazioneComeResponsabileLavoriEventHandler(bool stato);

    public delegate void CoordinatoreSicurezzaEsecuzioneComeResponsabileLavoriEventHandler(bool stato);

    public delegate void CoordinatoreSicurezzaEsecuzioneComeCoordinatoreSicurezzaProgettazioneEventHandler(bool stato);

    public delegate void ImpresaSelectedEventHandler();

    public delegate void ImpresaNuovaEventHandler();

    public delegate void NotificaSelectedEventHandler(int idNotifica, int idNotificaRiferimento);

    public delegate void SubappaltoCreatedOrDeletedEventHandler(SubappaltoNotificheTelematicheCollection subappalti);
}