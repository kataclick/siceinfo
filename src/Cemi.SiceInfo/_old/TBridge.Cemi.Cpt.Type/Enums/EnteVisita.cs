namespace TBridge.Cemi.Cpt.Type.Enums
{
    public enum EnteVisita
    {
        ASL = 0,
        CPT,
        DPL,
        ASLERSLT,
        CassaEdile
    }
}