namespace TBridge.Cemi.Cpt.Type.Enums
{
    public enum SezioneLogRicerca
    {
        RicercaNotifiche = 0,
        RicercaCantieri,
        LocalizzazioneCantieri
    }
}