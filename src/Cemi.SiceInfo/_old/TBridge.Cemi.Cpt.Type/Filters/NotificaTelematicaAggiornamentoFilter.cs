using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    public class NotificaTelematicaAggiornamentoFilter
    {
        public int? IdNotifica { get; set; }

        public DateTime? DataInserimento { get; set; }

        public string NaturaOpera { get; set; }

        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        public Guid? IdUtenteTelematiche { get; set; }
    }
}