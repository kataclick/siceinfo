namespace TBridge.Cemi.Cpt.Type.Filters
{
    public class UtenteNotificheTelematicheFilter
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string Login { get; set; }

        public string TipoUtenti { get; set; }
    }
}