using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    [Serializable]
    public class NotificaFilter
    {
        private decimal? ammontare;
        private string cap;
        private string committente;
        private DateTime? dataFine;
        private DateTime? dataInizio;
        private string fiscIva;
        private short idArea;
        private int? idASL;
        private string impresa;
        private string indirizzo;
        private string indirizzoComune;
        private string naturaOpera;
        private string numeroAppalto;

        public short IdArea
        {
            get => idArea;
            set => idArea = value;
        }

        public DateTime? Dal { get; set; }

        public DateTime? Al { get; set; }

        public string Committente
        {
            get => committente;
            set => committente = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Impresa
        {
            get => impresa;
            set => impresa = value;
        }

        public string NaturaOpera
        {
            get => naturaOpera;
            set => naturaOpera = value;
        }

        public decimal? Ammontare
        {
            get => ammontare;
            set => ammontare = value;
        }

        public DateTime? DataInizio
        {
            get => dataInizio;
            set => dataInizio = value;
        }

        public DateTime? DataFine
        {
            get => dataFine;
            set => dataFine = value;
        }

        public string FiscIva
        {
            get => fiscIva;
            set => fiscIva = value;
        }

        public string IndirizzoComune
        {
            get => indirizzoComune;
            set => indirizzoComune = value;
        }

        public string NumeroAppalto
        {
            get => numeroAppalto;
            set => numeroAppalto = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public int? IdASL
        {
            get => idASL;
            set => idASL = value;
        }

        public Guid? IdUtenteTelematiche { get; set; }

        public int? IdNotifica { get; set; }

        public string ProvinciaImpresa { get; set; }

        public string CapImpresa { get; set; }

        public string IndirizzoProvincia { get; set; }

        public string ProtocolloRegione { get; set; }
    }
}