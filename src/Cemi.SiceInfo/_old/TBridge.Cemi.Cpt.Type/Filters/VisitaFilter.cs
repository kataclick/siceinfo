using System;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    public class VisitaFilter
    {
        public short IdArea { get; set; }

        public int? IdASL { get; set; }

        public string Comune { get; set; }

        public string Indirizzo { get; set; }

        public DateTime? Data { get; set; }

        public TipologiaVisita Tipologia { get; set; }

        public EnteVisita? Ente { get; set; }

        public EsitoVisita Esito { get; set; }
    }
}