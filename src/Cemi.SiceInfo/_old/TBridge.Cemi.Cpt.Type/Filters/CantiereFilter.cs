using System;

namespace TBridge.Cemi.Cpt.Type.Filters
{
    [Serializable]
    public class CantiereFilter
    {
        private decimal? ammontare;
        private string committente;
        private string comune;
        private DateTime? dataFine;
        private DateTime? dataInizio;
        private short idArea;
        private int? idASL;
        private string impresa;
        private string indirizzo;
        private bool? localizzati;
        private string naturaOpera;

        public string Committente
        {
            get => committente;
            set => committente = value;
        }

        public string Indirizzo
        {
            get => indirizzo;
            set => indirizzo = value;
        }

        public string Impresa
        {
            get => impresa;
            set => impresa = value;
        }

        public decimal? Ammontare
        {
            get => ammontare;
            set => ammontare = value;
        }

        public string NaturaOpera
        {
            get => naturaOpera;
            set => naturaOpera = value;
        }

        public DateTime? DataInizio
        {
            get => dataInizio;
            set => dataInizio = value;
        }

        public DateTime? DataFine
        {
            get => dataFine;
            set => dataFine = value;
        }

        public bool? Localizzati
        {
            get => localizzati;
            set => localizzati = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public int? IdASL
        {
            get => idASL;
            set => idASL = value;
        }

        public short IdArea
        {
            get => idArea;
            set => idArea = value;
        }

        public int? IdIndirizzo { get; set; }

        public Guid? IdUtenteTelematiche { get; set; }

        public int? NumeroLavoratori { get; set; }
    }
}