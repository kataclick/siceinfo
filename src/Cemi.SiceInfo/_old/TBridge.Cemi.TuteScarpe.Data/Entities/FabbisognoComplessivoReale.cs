using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoComplessivoReale
    {
        public FabbisognoComplessivoReale(int codiceFabbisognoComplessivo, DateTime dataGenerazione,
            DateTime? dataScadenza)
        {
            CodiceFabbisognoComplessivo = codiceFabbisognoComplessivo;
            DataGenerazione = dataGenerazione;
            DataScadenza = dataScadenza;
        }

        public int CodiceFabbisognoComplessivo { get; }

        public DateTime DataGenerazione { get; }

        public DateTime? DataScadenza { get; }

        public DateTime? DataConfermaAutomatica { get; set; }

        public DateTime? DataConfermaAutomaticaEffettuata { get; set; }

        public DateTime? DataInvioMailEffettuato { get; set; }

        public DateTime? DataInvioSmsEffettuato { get; set; }
    }
}