using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class Fattura
    {
        private BollaList bolle;
        private int codiceFattura;
        private string descrizione;
        private int idFattura;

        public Fattura(string descrizione, DateTime dataUpload, string codiceFornitore, byte[] fileFattura)
        {
            this.descrizione = descrizione;
            DataUpload = dataUpload;
            CodiceFornitore = codiceFornitore;
            FileFattura = fileFattura;
            StatoInserimento = false;
            ForzataCorrettezza = false;
            bolle = new BollaList();
        }

        public Fattura(int idFattura, int codiceFattura, string descrizione, DateTime dataUpload,
            string codiceFornitore,
            bool statoInserimento, bool forzataCorrettezza)
        {
            this.idFattura = idFattura;
            this.codiceFattura = codiceFattura;
            this.descrizione = descrizione;
            DataUpload = dataUpload;
            CodiceFornitore = codiceFornitore;
            StatoInserimento = statoInserimento;
            ForzataCorrettezza = forzataCorrettezza;
            bolle = new BollaList();
        }

        public int IdFattura
        {
            get => idFattura;
            set => idFattura = value;
        }

        public int CodiceFattura
        {
            get => codiceFattura;
            set => codiceFattura = value;
        }

        public string Descrizione
        {
            get => descrizione;
            set => descrizione = value;
        }

        public byte[] FileFattura { get; }

        public DateTime DataUpload { get; }

        public string CodiceFornitore { get; }

        public bool StatoInserimento { get; }

        public bool ForzataCorrettezza { get; }

        public BollaList Bolle
        {
            get => bolle;
            set => bolle = value;
        }

        public bool Stato()
        {
            bool ret = true;

            if (Bolle != null)
            {
                foreach (Bolla b in Bolle)
                {
                    if (!(b.Stato.Count == 0))
                    {
                        ret = false;
                        break;
                    }
                }
            }

            return ret;
        }
    }
}