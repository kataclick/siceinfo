using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class IndumentoRiassunto
    {
        public IndumentoRiassunto()
        {
        }

        public IndumentoRiassunto(string indumento, int totale)
        {
            Indumento = indumento;
            Totale = totale;
        }

        public string Indumento { get; set; }

        public int Totale { get; set; }
    }

    public class OrdineRiassuntoIndumenti : List<IndumentoRiassunto>
    {
    }

    public class OrdineRiassunto
    {
        public OrdineRiassunto()
        {
            ListaIndumenti = new OrdineRiassuntoIndumenti();
        }

        public OrdineRiassuntoIndumenti ListaIndumenti { get; }

        public void AggiungiIndumentoRiassunto(string indumento, int totale)
        {
            ListaIndumenti.Add(new IndumentoRiassunto(indumento, totale));
        }
    }

    public class OrdineRiassuntoImpLav
    {
        public OrdineRiassuntoImpLav()
        {
            ListaElementi = new ListaElementiRiassunti();
        }

        public ListaElementiRiassunti ListaElementi { get; set; }

        public void AggiungiElemento(string nome, int totale)
        {
            ListaElementi.Add(new ElementoRiassunto(nome, totale));
        }
    }

    public class ElementoRiassunto
    {
        public ElementoRiassunto()
        {
        }

        public ElementoRiassunto(string nome, int totale)
        {
            NomeElemento = nome;
            Totale = totale;
        }

        public string NomeElemento { get; set; }

        public int Totale { get; set; }
    }

    public class ListaElementiRiassunti : List<ElementoRiassunto>
    {
    }
}