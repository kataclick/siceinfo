using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class VoceBolla
    {
        private int idVoceBolla;
        private int quantita;
        private int quantitaFabbisogno;

        public VoceBolla(string tipoVestiario, int quantita)
        {
            idVoceBolla = -1;
            TipoVestiario = tipoVestiario;
            this.quantita = quantita;
            quantitaFabbisogno = 0;
        }

        public VoceBolla(string tipoVestiario, int quantita, int quantitaFabbisogno)
        {
            idVoceBolla = -1;
            TipoVestiario = tipoVestiario;
            this.quantita = quantita;
            this.quantitaFabbisogno = quantitaFabbisogno;
        }

        public int IdVoceBolla
        {
            get => idVoceBolla;
            set => idVoceBolla = value;
        }

        public string TipoVestiario { get; }

        public int Quantita
        {
            get => quantita;
            set => quantita = value;
        }

        public int QuantitaFabbisogno
        {
            get => quantitaFabbisogno;
            set => quantitaFabbisogno = value;
        }

        public bool Stato()
        {
            if (Quantita == QuantitaFabbisogno)
                return true;
            return false;
        }
    }
}