﻿namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Indumento
    {
        public string IdIndumento { get; set; }

        public string Descrizione { get; set; }

        public override string ToString()
        {
            return Descrizione;
        }
    }
}