using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoDettaglio
    {
        public FabbisognoDettaglio(int idFabbisognoConfermatoDettaglio, int idLavoratore, string nomeCompleto,
            string indumento, string taglia, int quantita, TagliaList listaTaglie)
        {
            IdFabbisognoConfermatoDettaglio = idFabbisognoConfermatoDettaglio;
            IdLavoratore = idLavoratore;
            NomeCompleto = nomeCompleto;
            Indumento = indumento;
            Taglia = taglia;
            Quantita = quantita;
            ListaTaglie = listaTaglie;
        }

        public int IdFabbisognoConfermatoDettaglio { get; }

        public int IdLavoratore { get; }

        public string NomeCompleto { get; }

        public string Indumento { get; }

        public string Taglia { get; set; }

        public int Quantita { get; }

        public TagliaList ListaTaglie { get; }
    }
}