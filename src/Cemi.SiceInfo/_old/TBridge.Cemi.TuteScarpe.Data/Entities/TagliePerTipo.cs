﻿using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class TagliePerTipo
    {
        public TagliePerTipo()
        {
            Indumento = new IndumentoCollection();
            Taglie = new List<string>();
        }

        public string TipoTaglia { get; set; }

        public IndumentoCollection Indumento { get; set; }

        public List<string> Taglie { get; set; }

        public int IdLavoratore { get; set; }

        public string TagliaScelta { get; set; }
    }
}