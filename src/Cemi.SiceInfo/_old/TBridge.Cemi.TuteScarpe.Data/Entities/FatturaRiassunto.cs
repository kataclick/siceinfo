using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FatturaRiassuntoVoce
    {
        public FatturaRiassuntoVoce(string categoriaVestiario, int quantita)
        {
            CategoriaVestiario = categoriaVestiario;
            Quantita = quantita;
        }

        public string CategoriaVestiario { get; }

        public int Quantita { get; }
    }

    public class FatturaRiassunto : List<FatturaRiassuntoVoce>
    {
    }
}