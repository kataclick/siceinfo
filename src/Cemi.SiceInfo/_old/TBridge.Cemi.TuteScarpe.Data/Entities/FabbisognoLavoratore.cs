﻿using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoLavoratore
    {
        public FabbisognoLavoratore()
        {
            Gruppi = new GruppoFabbisognoLavoratoreCollection();
        }

        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public bool ImpresaAsfaltista { get; set; }

        public int IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public GruppoFabbisognoLavoratoreCollection Gruppi { get; set; }

        public bool ForzatoAsfaltista
        {
            get
            {
                if (Gruppi.Count > 0)
                {
                    if (Gruppi[0].Dettagli.Count > 0)
                    {
                        return Gruppi[0].Dettagli[0].ForzatoAsfaltista;
                    }
                }

                return false;
            }
        }
    }
}