namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Taglia
    {
        public Taglia(int idTaglia, string descrizioneTaglia)
        {
            IdTaglia = idTaglia;
            DescrizioneTaglia = descrizioneTaglia;
        }

        public int IdTaglia { get; }

        public string DescrizioneTaglia { get; }
    }
}