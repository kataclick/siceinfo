using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Fabbisogno
    {
        public Fabbisogno(int idFabbisogno, string codiceFornitore, DateTime dataConfermaImpresa,
            int progressivoFornitura)
        {
            IdFabbisogno = idFabbisogno;
            CodiceFornitore = codiceFornitore;
            DataConfermaImpresa = dataConfermaImpresa;
            ProgressivoFornitura = progressivoFornitura;
        }

        public Fabbisogno(int idFabbisogno, string codiceFornitore, DateTime dataConfermaImpresa, int idImpresa,
            string ragioneSociale)
        {
            IdFabbisogno = idFabbisogno;
            CodiceFornitore = codiceFornitore;
            DataConfermaImpresa = dataConfermaImpresa;
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
        }

        public Fabbisogno(int idFabbisogno, string codiceFornitore, DateTime dataConfermaImpresa, int idImpresa,
            string ragioneSociale,
            int progressivoFornitura)
        {
            IdFabbisogno = idFabbisogno;
            CodiceFornitore = codiceFornitore;
            DataConfermaImpresa = dataConfermaImpresa;
            IdImpresa = idImpresa;
            RagioneSociale = ragioneSociale;
            ProgressivoFornitura = progressivoFornitura;
        }

        public int IdFabbisogno { get; }

        public string CodiceFornitore { get; }

        public DateTime DataConfermaImpresa { get; }

        public int IdImpresa { get; }

        public string RagioneSociale { get; }

        public int ProgressivoFornitura { get; }
    }
}