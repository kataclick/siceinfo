using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class Bolla
    {
        private int idBolla;
        private string ragioneSociale;
        private VoceBollaList vociBolla;

        public Bolla(int idImpresa, int idOrdine, DateTime data)
        {
            idBolla = -1;
            IdImpresa = idImpresa;
            IdOrdine = idOrdine;
            Data = data;
            vociBolla = new VoceBollaList();
            TipiVestiario = new List<string>();
            Stato = new StatoBollaList();
        }

        public int IdBolla
        {
            get => idBolla;
            set => idBolla = value;
        }

        public int IdImpresa { get; }

        public string RagioneSociale
        {
            get => ragioneSociale;
            set => ragioneSociale = value;
        }

        public int IdOrdine { get; }

        public DateTime Data { get; }

        public List<string> TipiVestiario { get; }

        public VoceBollaList VociBolla
        {
            get => vociBolla;
            set => vociBolla = value;
        }

        public StatoBollaList Stato { get; }

        public void AggiornaStato()
        {
            if (VociBolla != null)
            {
                foreach (VoceBolla b in VociBolla)
                {
                    if (!b.Stato())
                    {
                        Stato.Add(new StatoBollaClass(StatoBolla.DifferenzaQuantita));
                        break;
                    }
                }
            }
        }

        private void AggiungiTipoVestiario(string tipoVestiario)
        {
            if (!TipiVestiario.Contains(tipoVestiario))
                TipiVestiario.Add(tipoVestiario);
        }

        public void AggiungiVoceBolla(VoceBolla voce)
        {
            vociBolla.Add(voce);
            AggiungiTipoVestiario(voce.TipoVestiario);
        }
    }
}