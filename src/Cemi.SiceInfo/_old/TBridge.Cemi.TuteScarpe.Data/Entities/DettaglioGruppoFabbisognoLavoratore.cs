﻿using System.Collections.Generic;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class DettaglioGruppoFabbisognoLavoratore
    {
        public DettaglioGruppoFabbisognoLavoratore()
        {
            Taglie = new List<string>();
        }

        public string IdIndumento { get; set; }

        public string Descrizione { get; set; }

        public int Quantita { get; set; }

        public List<string> Taglie { get; set; }

        public string TagliaSelezionata { get; set; }

        public bool ForzatoAsfaltista { get; set; }

        public bool Selezionato { get; set; }
    }
}