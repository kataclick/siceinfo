﻿namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class OrdineAutomatico
    {
        public int IdImpresa { get; set; }
        public string RagioneSociale { get; set; }
        public int IdLavoratore { get; set; }
        public string Cognome { get; set; }
        public string Nome { get; set; }
        public string IdIndumento { get; set; }
        public string Descrizione { get; set; }
        public string Taglia { get; set; }
        public string TipologiaFornitura { get; set; }
    }
}