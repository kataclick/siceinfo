using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Ordine
    {
        public Ordine(int idOrdine, DateTime dataCreazione, DateTime? dataConferma, string codiceFornitore,
            string descrizione, DateTime? scaricatoFornitore)
        {
            IdOrdine = idOrdine;
            DataCreazione = dataCreazione;
            DataConferma = dataConferma;
            CodiceFornitore = codiceFornitore;
            Descrizione = descrizione;
            ScaricatoFornitore = scaricatoFornitore;
            ListaFabbisogni = new FabbisognoList();
        }

        public Ordine(int idOrdine, DateTime dataCreazione, DateTime? dataConferma, string codiceFornitore,
            string descrizione, FabbisognoList listaFabbisogni)
        {
            IdOrdine = idOrdine;
            DataCreazione = dataCreazione;
            DataConferma = dataConferma;
            CodiceFornitore = codiceFornitore;
            Descrizione = descrizione;
            ListaFabbisogni = listaFabbisogni;
        }

        public int IdOrdine { get; }

        public DateTime DataCreazione { get; }

        public DateTime? DataConferma { get; }

        public string CodiceFornitore { get; }

        public string Descrizione { get; }

        public DateTime? ScaricatoFornitore { get; }


        public FabbisognoList ListaFabbisogni { get; }
    }
}