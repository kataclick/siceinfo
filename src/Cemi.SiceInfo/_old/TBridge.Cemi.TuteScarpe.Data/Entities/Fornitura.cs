namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Fornitura
    {
        public Fornitura(int idImpresa, int progressivoFornitura)
        {
            IdImpresa = idImpresa;
            ProgressivoFornitura = progressivoFornitura;
        }

        public int IdImpresa { get; }

        public int ProgressivoFornitura { get; }
    }
}