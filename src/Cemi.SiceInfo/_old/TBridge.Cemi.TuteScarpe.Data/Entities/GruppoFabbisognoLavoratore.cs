﻿using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class GruppoFabbisognoLavoratore : ICloneable
    {
        public GruppoFabbisognoLavoratore()
        {
            Dettagli = new DettaglioGruppoFabbisognoLavoratoreCollection();
        }

        public int IdGruppo { get; set; }

        public string Descrizione { get; set; }

        public int CodiceAlternative { get; set; }

        public bool GruppoPerAsfaltisti { get; set; }

        public bool Selezionato { get; set; }

        public DettaglioGruppoFabbisognoLavoratoreCollection Dettagli { get; set; }

        public object Clone()
        {
            GruppoFabbisognoLavoratore cloned = new GruppoFabbisognoLavoratore();

            cloned.IdGruppo = IdGruppo;
            cloned.Descrizione = Descrizione;
            cloned.CodiceAlternative = CodiceAlternative;
            cloned.GruppoPerAsfaltisti = GruppoPerAsfaltisti;
            cloned.Selezionato = Selezionato;

            cloned.Dettagli = new DettaglioGruppoFabbisognoLavoratoreCollection();
            foreach (DettaglioGruppoFabbisognoLavoratore dett in Dettagli)
            {
                DettaglioGruppoFabbisognoLavoratore clonedDett = new DettaglioGruppoFabbisognoLavoratore();
                cloned.Dettagli.Add(clonedDett);

                clonedDett.Descrizione = dett.Descrizione;
                clonedDett.ForzatoAsfaltista = dett.ForzatoAsfaltista;
                clonedDett.IdIndumento = dett.IdIndumento;
                clonedDett.Quantita = dett.Quantita;
                clonedDett.TagliaSelezionata = dett.TagliaSelezionata;
                clonedDett.Taglie = dett.Taglie;
                clonedDett.Selezionato = dett.Selezionato;
            }

            return cloned;
        }
    }
}