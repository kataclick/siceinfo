using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FileOrdine
    {
        public FileOrdineImpresaCollection ListaOrdiniImpresa { get; } = new FileOrdineImpresaCollection();

        public void AggiungiOrdineImpresa(FileOrdineImpresa ordineImpresa)
        {
            ListaOrdiniImpresa.Add(ordineImpresa);
        }

        public string ScriviFile()
        {
            string lista = string.Empty;

            foreach (FileOrdineImpresa fileImpresa in ListaOrdiniImpresa)
                lista += fileImpresa.ScriviRecord();

            return lista;
        }
    }
}