using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class Indirizzo
    {
        private string cap;
        private string comune;
        private bool corretto;
        private string email;
        private string fax;
        private string indirizzoVia;
        private string presso;
        private string provincia;
        private string telefono;

        public Indirizzo(string indirizzo, string provincia, string comune, string cap, bool corretto, string presso)
        {
            indirizzoVia = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
            this.corretto = corretto;
            this.presso = presso;
        }

        public Indirizzo(string indirizzo, string provincia, string comune, string cap)
        {
            indirizzoVia = indirizzo;
            this.provincia = provincia;
            this.comune = comune;
            this.cap = cap;
        }

        public Indirizzo()
        {
        }

        public string IndirizzoVia
        {
            get => indirizzoVia;
            set => indirizzoVia = value;
        }

        public string Provincia
        {
            get => provincia;
            set => provincia = value;
        }

        public string Comune
        {
            get => comune;
            set => comune = value;
        }

        public string Cap
        {
            get => cap;
            set => cap = value;
        }

        public string Presso
        {
            get => presso;
            set => presso = value;
        }

        public bool Corretto
        {
            get => corretto;
            set => corretto = value;
        }

        public string IndirizzoCompleto => IndirizzoVia + " - " + Comune + " " + Provincia + ", " + Cap;

        public string Telefono
        {
            get => telefono;
            set => telefono = value;
        }

        public string Fax
        {
            get => fax;
            set => fax = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }

        public string Cellulare { get; set; }
    }
}