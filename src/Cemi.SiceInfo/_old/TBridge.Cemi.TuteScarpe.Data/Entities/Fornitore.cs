namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class Fornitore
    {
        public Fornitore(string codiceFornitore)
        {
            CodiceFornitore = codiceFornitore;
        }

        public string CodiceFornitore { get; }
    }
}