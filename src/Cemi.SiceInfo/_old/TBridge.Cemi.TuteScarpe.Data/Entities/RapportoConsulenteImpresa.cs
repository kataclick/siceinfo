using System;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    [Serializable]
    public class RapportoConsulenteImpresa
    {
        public DateTime? DataFine { get; set; }
        public DateTime DataInizio { get; set; }
        public int IdConsulente { get; set; }
        public int IdImpresa { get; set; }
        public int? IdRapportoConsulenteImpresa { get; set; }
        public string Nota { get; set; }
        public string RagioneSocialeImpresa { get; set; }
        public int NumeroLavoratori { get; set; }
    }
}