namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class TipoVestiario
    {
        public TipoVestiario(string nomeVestiario, int quantita)
        {
            NomeVestiario = nomeVestiario;
            Quantita = quantita;
        }

        public string NomeVestiario { get; }

        public int Quantita { get; }
    }
}