using System;
using TBridge.Cemi.TuteScarpe.Data.Collections;

namespace TBridge.Cemi.TuteScarpe.Data.Entities
{
    public class FabbisognoComplessivo
    {
        public FabbisognoComplessivo(int idFabbisognoComplessivo, DateTime dataFabbisognoComplessivo,
            FabbisognoList listaFabbisogni, int idImpresa, string ragioneSocialeImpresa,
            DateTime dataConfermaImpresa)
        {
            IdFabbisognoComplessivo = idFabbisognoComplessivo;
            DataFabbisognoComplessivo = dataFabbisognoComplessivo;
            ListaFabbisogni = listaFabbisogni;
            IdImpresa = idImpresa;
            RagioneSocialeImpresa = ragioneSocialeImpresa;
            DataConfermaImpresa = dataConfermaImpresa;
        }

        public FabbisognoComplessivo(int idFabbisognoComplessivo, DateTime dataFabbisognoComplessivo)
        {
            IdFabbisognoComplessivo = idFabbisognoComplessivo;
            DataFabbisognoComplessivo = dataFabbisognoComplessivo;
        }

        public int IdFabbisognoComplessivo { get; }

        public DateTime DataFabbisognoComplessivo { get; }

        public FabbisognoList ListaFabbisogni { get; }

        public int IdImpresa { get; }

        public string RagioneSocialeImpresa { get; }

        public DateTime DataConfermaImpresa { get; }

        public string Data => DataFabbisognoComplessivo.ToShortDateString();
    }
}