using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace TBridge.Cemi.TuteScarpe.Data.Delegates
{
    public delegate void ConsulenteSelectedEventHandler(Consulente consulente);
}