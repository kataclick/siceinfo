﻿using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    public class TagliePerTipoCollection : List<TagliePerTipo>
    {
        public TagliePerTipo FindByTipoTaglia(string tipoTaglia)
        {
            foreach (TagliePerTipo tpt in this)
            {
                if (tpt.TipoTaglia == tipoTaglia)
                {
                    return tpt;
                }
            }

            return null;
        }
    }
}