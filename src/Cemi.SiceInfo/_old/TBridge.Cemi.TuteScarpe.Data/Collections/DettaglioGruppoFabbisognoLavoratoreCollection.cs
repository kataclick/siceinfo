﻿using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    public class DettaglioGruppoFabbisognoLavoratoreCollection : List<DettaglioGruppoFabbisognoLavoratore>
    {
        public bool EliminaIndumento(string idIndumento)
        {
            bool res = false;

            for (int i = 0; i < Count; i++)
            {
                if (this[i].IdIndumento == idIndumento)
                {
                    RemoveAt(i);
                    res = true;
                    break;
                }
            }

            return res;
        }

        public DettaglioGruppoFabbisognoLavoratore GetIndumento(string idIndumento)
        {
            DettaglioGruppoFabbisognoLavoratore res = null;

            foreach (DettaglioGruppoFabbisognoLavoratore dett in this)
            {
                if (dett.IdIndumento == idIndumento)
                {
                    res = dett;
                    break;
                }
            }

            return res;
        }
    }
}