﻿using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    public class IndumentoCollection : List<Indumento>
    {
        public bool PresenteIndumentoPerDescrizione(string descrizione)
        {
            foreach (Indumento indumento in this)
            {
                if (indumento.Descrizione == descrizione)
                {
                    return true;
                }
            }

            return false;
        }
    }
}