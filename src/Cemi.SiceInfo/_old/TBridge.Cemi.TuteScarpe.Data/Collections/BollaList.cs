using System;
using System.Collections.Generic;
using TBridge.Cemi.TuteScarpe.Data.Entities;

namespace TBridge.Cemi.TuteScarpe.Data.Collections
{
    [Serializable]
    public class BollaList : List<Bolla>
    {
    }
}