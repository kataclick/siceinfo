﻿using System;
using System.Collections.Generic;
using System.IO;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Type.Entities.TuteScarpe;

namespace TBridge.Cemi.TuteScarpe.Service
{
    public class UploadService : IUploadService
    {
        private readonly TSBusiness _biz = new TSBusiness();

        #region IUploadService Members

        public void UploadFile(Stream data)
        {
            byte[] imageData = new byte[data.Length];

            data.Read(imageData, 0, Convert.ToInt32(data.Length));

            data.Position = 0;

            if (_biz.InsertFileConsegneOrdini(imageData, out int idTuteScarpeConsegnaOrdineFile))
            {
                StreamReader reader = new StreamReader(data);
                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    Consegna ordine = new Consegna();

                    ordine.NumeroOrdine = Int32.Parse(line.Substring(0, 10));
                    ordine.CodiceImpresa = Int32.Parse(line.Substring(10, 10));
                    ordine.NumeroSpedizione = line.Substring(20, 15);
                    ordine.DataEvento = DateTime.Parse(line.Substring(35, 10));
                    ordine.Stato = line.Substring(45);
                    ordine.IdentificativoFile = idTuteScarpeConsegnaOrdineFile;

                    _biz.InsertConsegneOrdini(ordine);
                }
            }
            data.Close();
        }

        public void UploadConsegne(List<InformazioniConsegna> informazioniConsegne)
        {
            foreach (InformazioniConsegna informazioniConsegna in informazioniConsegne)
            {
                Consegna consegna = new Consegna
                                        {
                                            CodiceImpresa = informazioniConsegna.CodiceImpresa,
                                            DataEvento = informazioniConsegna.DataEvento,
                                            NumeroOrdine = informazioniConsegna.NumeroOrdine,
                                            NumeroSpedizione = informazioniConsegna.NumeroSpedizione,
                                            Stato = informazioniConsegna.Stato,
                                            IdentificativoFile = null
                                        };

                _biz.InsertConsegneOrdini(consegna);
            }
        }

        #endregion
    }
}