﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace TBridge.Cemi.TuteScarpe.Service
{
    [ServiceContract]
    public interface IUploadService
    {
        [OperationContract]
        void UploadFile(Stream data);

        [OperationContract]
        void UploadConsegne(List<InformazioniConsegna> informazioniConsegne);
    }

    [DataContract]
    public class InformazioniConsegna
    {
        [DataMember]
        public Int32 NumeroOrdine { get; set; }
        [DataMember]
        public Int32 CodiceImpresa { get; set; }
        [DataMember]
        public String NumeroSpedizione { get; set; }
        [DataMember]
        public DateTime DataEvento { get; set; }
        [DataMember]
        public String Stato { get; set; }
    }
}