﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.Data.AnagraficaComune
{
    public class AnagraficaComuneDataProvider
    {
        public AnagraficaComuneDataProvider()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        #region Imprese anagrafica comune

        public bool InsertImpresa(ImpresaAnagraficaComune impresa, DbTransaction transaction)
        {
            bool res = false;

            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseAnagraficaComuneInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);

                DatabaseCemi.AddInParameter(comando, "@fonte", DbType.Int32, impresa.Fonte);
                DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                if (transaction == null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }

                int idTemp = (int) DatabaseCemi.GetParameterValue(comando, "@idImpresa");
                if (idTemp > 0)
                {
                    impresa.IdImpresa = idTemp;
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Lavoratore anagrafica comune

        public bool InsertLavoratore(LavoratoreAnagraficaComune lavoratore, DbTransaction transaction)
        {
            bool res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriAnagraficaComuneInsert")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, lavoratore.Sesso);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);

                DatabaseCemi.AddInParameter(comando, "@fonte", DbType.Int32, lavoratore.Fonte);
                DatabaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);

                DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String, lavoratore.PaeseNascita);

                if (!string.IsNullOrEmpty(lavoratore.ProvinciaNascita))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                        lavoratore.ProvinciaNascita);
                }
                if (!string.IsNullOrEmpty(lavoratore.ComuneNascita))
                {
                    DatabaseCemi.AddInParameter(comando, "@comuneNascita", DbType.String, lavoratore.ComuneNascita);
                }

                if (transaction == null)
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                }
                else
                {
                    DatabaseCemi.ExecuteNonQuery(comando, transaction);
                }

                int idTemp = (int) DatabaseCemi.GetParameterValue(comando, "@idLavoratore");
                if (idTemp > 0)
                {
                    lavoratore.IdLavoratore = idTemp;
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateLavoratore(LavoratoreAnagraficaComune lavoratore, DbTransaction transaction)
        {
            bool res = false;

            if (lavoratore == null)
            {
                throw new ArgumentNullException("lavoratore");
            }
            if (!lavoratore.IdLavoratore.HasValue)
            {
                throw new ArgumentException("Non è presente l'ID", "lavoratore");
            }
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriAnagraficaComuneUpdate")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                    lavoratore.IdLavoratore.Value);
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, lavoratore.Sesso);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);

                DatabaseCemi.AddInParameter(comando, "@paeseNascita", DbType.String, lavoratore.PaeseNascita);

                if (!string.IsNullOrEmpty(lavoratore.ProvinciaNascita))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaNascita", DbType.String,
                        lavoratore.ProvinciaNascita);
                }
                if (!string.IsNullOrEmpty(lavoratore.ComuneNascita))
                {
                    DatabaseCemi.AddInParameter(comando, "@comuneNascita", DbType.String,
                        lavoratore.ComuneNascita);
                }

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion
    }
}