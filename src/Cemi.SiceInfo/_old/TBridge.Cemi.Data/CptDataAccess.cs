using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.Cpt.Type.Enums;
using TBridge.Cemi.Cpt.Type.Exceptions;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using Subappalto = TBridge.Cemi.Cpt.Type.Entities.Subappalto;

namespace TBridge.Cemi.Data
{
    public class CptDataAccess
    {
        public CptDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public decimal GetLimiteImporto()
        {
            decimal limiteImporto;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectNotificheImportoMinimo"))
            {
                limiteImporto = (decimal) DatabaseCemi.ExecuteScalar(comando);
            }

            return limiteImporto;
        }

        public bool AnnullaNotifica(int idNotifica, string utente)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheAnnulla"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);
                DatabaseCemi.AddInParameter(comando, "@utente", DbType.String, utente);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public List<int> GetIdNotificheCorrelate(int idNotifica)
        {
            List<int> notificheCorrelate = new List<int>();
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheSelectIdNotificheCorrelate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        notificheCorrelate.Add(reader.GetInt32(reader.GetOrdinal("idCptNotifica")));
                    }
                }
            }

            return notificheCorrelate;
        }

        public List<string> GetTipologieAttivita()
        {
            List<string> tipologieAttivita = new List<string>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptTipologieAttivitaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        tipologieAttivita.Add(reader.GetString(reader.GetOrdinal("descrizione")));
                    }
                }
            }

            return tipologieAttivita;
        }

        public bool[] EsisteIvaFiscImpresa(string partitaIVA, string codiceFiscale)
        {
            bool[] res = new bool[4];

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptEsisteImpresaConIvaFisc"))
            {
                if (!string.IsNullOrEmpty(partitaIVA))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, partitaIVA);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@impresaIva", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@cantieriImpresaIva", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@impresaFisc", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@cantieriImpresaFisc", DbType.Boolean, 1);
                DatabaseCemi.ExecuteNonQuery(comando);

                res[0] = (bool) comando.Parameters["@impresaIva"].Value;
                res[1] = (bool) comando.Parameters["@cantieriImpresaIva"].Value;
                res[2] = (bool) comando.Parameters["@impresaFisc"].Value;
                res[3] = (bool) comando.Parameters["@cantieriImpresaFisc"].Value;
            }

            return res;
        }

        public bool[] EsisteIvaFiscCommittente(string partitaIVA, string codiceFiscale)
        {
            bool[] res = new bool[2];

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptEsisteCommittenteConIvaFisc"))
            {
                if (!string.IsNullOrEmpty(partitaIVA))
                    DatabaseCemi.AddInParameter(comando, "@partitaIVA", DbType.String, partitaIVA);
                if (!string.IsNullOrEmpty(codiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@committenteIva", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@committenteFiscale", DbType.Boolean, 1);
                DatabaseCemi.ExecuteNonQuery(comando);

                res[0] = (bool) comando.Parameters["@committenteIva"].Value;
                res[1] = (bool) comando.Parameters["@committenteFiscale"].Value;
            }

            return res;
        }

        public bool UpdateIndirizzo(Indirizzo indirizzo, GeocodingResult geocodingResult)
        {
            bool res = false;

            if (indirizzo.IdIndirizzo.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptIndirizziUpdateCoordinate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idIndirizzo", DbType.Int32, indirizzo.IdIndirizzo.Value);
                    DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Double, geocodingResult.Latitude);
                    DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Double, geocodingResult.Longitude);
                    DatabaseCemi.AddInParameter(comando, "@indirizzoGeocoder", DbType.String,
                        geocodingResult.Indirizzo);
                    DatabaseCemi.AddInParameter(comando, "@civicoGeocoder", DbType.String, geocodingResult.Civico);
                    DatabaseCemi.AddInParameter(comando, "@provinciaGeocoder", DbType.String,
                        geocodingResult.Provincia);
                    DatabaseCemi.AddInParameter(comando, "@comuneGeocoder", DbType.String, geocodingResult.Comune);
                    DatabaseCemi.AddInParameter(comando, "@capGeocoder", DbType.String, geocodingResult.Cap);

                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        public IndirizzoCollection GetIndirizziNonGeocodificati()
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptIndirizziSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Indirizzo indirizzo = new Indirizzo();
                        indirizzi.Add(indirizzo);

                        indirizzo.IdIndirizzo = (int) reader["idCptIndirizzo"];
                        indirizzo.Indirizzo1 = (string) reader["indirizzo"];
                        if (!Convert.IsDBNull(reader["civico"]))
                            indirizzo.Civico = (string) reader["civico"];
                        if (!Convert.IsDBNull(reader["comune"]))
                            indirizzo.Comune = (string) reader["comune"];
                        if (!Convert.IsDBNull(reader["provincia"]))
                            indirizzo.Provincia = (string) reader["provincia"];
                        if (!Convert.IsDBNull(reader["cap"]))
                            indirizzo.Cap = (string) reader["cap"];
                        if (!Convert.IsDBNull(reader["latitudine"]))
                            indirizzo.Latitudine = (decimal) reader["latitudine"];
                        if (!Convert.IsDBNull(reader["longitudine"]))
                            indirizzo.Longitudine = (decimal) reader["longitudine"];
                        if (!Convert.IsDBNull(reader["infoAggiuntiva"]))
                            indirizzo.InfoAggiuntiva = (string) reader["infoAggiuntiva"];
                    }
                }
            }

            return indirizzi;
        }

        public void InsertLogRicerca(LogRicerca log)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptLogRicercheInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, log.IdUtente);
                DatabaseCemi.AddInParameter(comando, "@filtri", DbType.Xml, log.XmlFiltro);
                DatabaseCemi.AddInParameter(comando, "@sezione", DbType.Int32, log.Sezione);

                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        public bool InsertUtenteTelematiche(UtenteNotificheTelematiche utente)
        {
            bool res;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    try
                    {
                        if (utente.CommittenteTelematiche != null &&
                            InsertCommittenteTelematicheAnagrafica(utente.CommittenteTelematiche, transaction) &&
                            InserisciCommittenteNotifica(utente.CommittenteTelematiche, transaction)
                            || utente.CommittenteTelematiche == null)
                        {
                            using (
                                DbCommand comando =
                                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaUtentiInsert"))
                            {
                                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, utente.Cognome);
                                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, utente.Nome);
                                DatabaseCemi.AddInParameter(comando, "@cartaIdentita", DbType.String,
                                    utente.CartaIdentita);
                                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, utente.Indirizzo);
                                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, utente.Comune);
                                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, utente.Provincia);
                                DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, utente.Cap);
                                DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, utente.Telefono);
                                if (!string.IsNullOrEmpty(utente.Fax))
                                {
                                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, utente.Fax);
                                }
                                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Guid, utente.IdUtente);
                                if (utente.CommittenteTelematiche != null &&
                                    utente.CommittenteTelematiche.IdCommittenteTelematiche.HasValue)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@idCommittenteTelematica", DbType.String,
                                        utente.CommittenteTelematiche.IdCommittenteTelematiche.Value);
                                }

                                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                {
                                    transaction.Commit();
                                    res = true;
                                }
                                else
                                {
                                    throw new Exception(
                                        "InsertUtenteTelematiche: errore durante l'inserimento dell'utente");
                                }
                            }
                        }
                        else
                        {
                            throw new Exception(
                                "InsertUtenteTelematiche: errore durante l'inserimento del committente");
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        public UtenteNotificheTelematiche GetUtenteTelematiche(Guid userID)
        {
            UtenteNotificheTelematiche utente = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaUtentiSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Guid, userID);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        utente = new UtenteNotificheTelematiche
                        {
                            IdUtente = (Guid) reader["userID"],
                            Cognome = (string) reader["cognome"],
                            Nome = (string) reader["nome"],
                            CartaIdentita = (string) reader["cartaIdentita"],
                            Indirizzo = (string) reader["indirizzo"],
                            Comune = (string) reader["comune"],
                            Provincia = (string) reader["provincia"],
                            Cap = (string) reader["cap"],
                            Telefono = (string) reader["telefono"]
                        };

                        if (!Convert.IsDBNull(reader["fax"]))
                        {
                            utente.Fax = (string) reader["fax"];
                        }

                        if (!Convert.IsDBNull(reader["idCptNotificaTelematicaCommittente"]))
                        {
                            utente.CommittenteTelematiche = new CommittenteNotificheTelematiche();

                            utente.CommittenteTelematiche.IdCommittenteTelematiche =
                                (int) reader["idCptNotificaTelematicaCommittente"];
                            utente.CommittenteTelematiche.TipologiaCommittente = new TipologiaCommittente();
                            utente.CommittenteTelematiche.TipologiaCommittente.IdTipologiaCommittente =
                                (int) reader["idCptTipologiaCommittente"];
                            if (!Convert.IsDBNull(reader["personaCognome"]))
                            {
                                utente.CommittenteTelematiche.PersonaCognome = (string) reader["personaCognome"];
                            }
                            if (!Convert.IsDBNull(reader["personaNome"]))
                            {
                                utente.CommittenteTelematiche.PersonaNome = (string) reader["personaNome"];
                            }
                            if (!Convert.IsDBNull(reader["personaCodiceFiscale"]))
                            {
                                utente.CommittenteTelematiche.PersonaCodiceFiscale =
                                    (string) reader["personaCodiceFiscale"];
                            }
                            if (!Convert.IsDBNull(reader["personaIndirizzo"]))
                            {
                                utente.CommittenteTelematiche.PersonaIndirizzo = (string) reader["personaIndirizzo"];
                            }
                            if (!Convert.IsDBNull(reader["personaComune"]))
                            {
                                utente.CommittenteTelematiche.PersonaComune = (string) reader["personaComune"];
                            }
                            if (!Convert.IsDBNull(reader["personaProvincia"]))
                            {
                                utente.CommittenteTelematiche.PersonaProvincia = (string) reader["personaProvincia"];
                            }
                            if (!Convert.IsDBNull(reader["personaCap"]))
                            {
                                utente.CommittenteTelematiche.PersonaCap = (string) reader["personaCap"];
                            }
                            if (!Convert.IsDBNull(reader["personaTelefono"]))
                            {
                                utente.CommittenteTelematiche.PersonaTelefono = (string) reader["personaTelefono"];
                            }
                            if (!Convert.IsDBNull(reader["personaFax"]))
                            {
                                utente.CommittenteTelematiche.PersonaFax = (string) reader["personaFax"];
                            }
                            if (!Convert.IsDBNull(reader["personaCellulare"]))
                            {
                                utente.CommittenteTelematiche.PersonaCellulare = (string) reader["personaCellulare"];
                            }
                            if (!Convert.IsDBNull(reader["personaEmail"]))
                            {
                                utente.CommittenteTelematiche.PersonaEmail = (string) reader["personaEmail"];
                            }
                            if (!Convert.IsDBNull(reader["enteRagioneSociale"]))
                            {
                                utente.CommittenteTelematiche.RagioneSociale = (string) reader["enteRagioneSociale"];
                            }
                            if (!Convert.IsDBNull(reader["entePartitaIva"]))
                            {
                                utente.CommittenteTelematiche.PartitaIva = (string) reader["entePartitaIva"];
                            }
                            if (!Convert.IsDBNull(reader["enteCodiceFiscale"]))
                            {
                                utente.CommittenteTelematiche.CodiceFiscale = (string) reader["enteCodiceFiscale"];
                            }
                            if (!Convert.IsDBNull(reader["enteIndirizzo"]))
                            {
                                utente.CommittenteTelematiche.Indirizzo = (string) reader["enteIndirizzo"];
                            }
                            if (!Convert.IsDBNull(reader["enteComune"]))
                            {
                                utente.CommittenteTelematiche.Comune = (string) reader["enteComune"];
                            }
                            if (!Convert.IsDBNull(reader["enteProvincia"]))
                            {
                                utente.CommittenteTelematiche.Provincia = (string) reader["enteProvincia"];
                            }
                            if (!Convert.IsDBNull(reader["enteCap"]))
                            {
                                utente.CommittenteTelematiche.Cap = (string) reader["enteCap"];
                            }
                            if (!Convert.IsDBNull(reader["enteTelefono"]))
                            {
                                utente.CommittenteTelematiche.Telefono = (string) reader["enteTelefono"];
                            }
                            if (!Convert.IsDBNull(reader["enteFax"]))
                            {
                                utente.CommittenteTelematiche.Fax = (string) reader["enteFax"];
                            }
                        }
                    }
                }
            }

            return utente;
        }

        public ImpresaNotificheTelematiche GetImpreseTelematicheDaSiceNewEAnagrafica(string ivaFisc)
        {
            ImpresaNotificheTelematiche impresa = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaImpreseSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, ivaFisc);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        impresa = new ImpresaNotificheTelematiche();

                        int tipoImpresa = (int) reader["tipoImpresa"];
                        switch (tipoImpresa)
                        {
                            case 1:
                                impresa.IdImpresa = (int) reader["idImpresa"];
                                break;
                            case 2:
                                impresa.IdImpresaAnagrafica = (int) reader["idImpresa"];
                                break;
                        }

                        impresa.RagioneSociale = (string) reader["ragioneSociale"];
                        if (!Convert.IsDBNull(reader["partitaIva"]))
                        {
                            impresa.PartitaIva = (string) reader["partitaIva"];
                        }
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        {
                            impresa.CodiceFiscale = (string) reader["codiceFiscale"];
                        }
                        if (!Convert.IsDBNull(reader["codiceINAIL"]))
                        {
                            impresa.MatricolaINAIL = (string) reader["codiceINAIL"];
                        }
                        if (!Convert.IsDBNull(reader["codiceINPS"]))
                        {
                            impresa.MatricolaINPS = (string) reader["codiceINPS"];
                        }
                        if (!Convert.IsDBNull(reader["codiceCCIAA"]))
                        {
                            int numeroCCIAA = (int) reader["codiceCCIAA"];

                            if (numeroCCIAA > 0)
                            {
                                impresa.MatricolaCCIAA = reader["codiceCCIAA"].ToString();
                            }
                        }
                        if (!Convert.IsDBNull(reader["indirizzo"]))
                        {
                            impresa.Indirizzo = (string) reader["indirizzo"];
                        }
                        if (!Convert.IsDBNull(reader["provincia"]))
                        {
                            impresa.Provincia = (string) reader["provincia"];
                        }
                        if (!Convert.IsDBNull(reader["comune"]))
                        {
                            impresa.Comune = (string) reader["comune"];
                        }
                        if (!Convert.IsDBNull(reader["cap"]))
                        {
                            impresa.Cap = (string) reader["cap"];
                        }
                        if (!Convert.IsDBNull(reader["telefono"]))
                        {
                            impresa.Telefono = (string) reader["telefono"];
                        }
                        if (!Convert.IsDBNull(reader["fax"]))
                        {
                            impresa.Fax = (string) reader["fax"];
                        }
                    }
                }
            }

            return impresa;
        }

        public CommittenteNotificheTelematiche GetCommittentiTelematicheDaAnagrafica(string ivaFisc)
        {
            CommittenteNotificheTelematiche committente = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaCommittentiAnagraficaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, ivaFisc);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        committente = new CommittenteNotificheTelematiche();

                        committente.IdCommittenteAnagrafica =
                            (int) reader["idCptNotificaTelematicaCommittenteAnagrafica"];

                        committente.TipologiaCommittente = new TipologiaCommittente();
                        committente.TipologiaCommittente.IdTipologiaCommittente =
                            (int) reader["idCptTipologiaCommittente"];
                        committente.TipologiaCommittente.Descrizione = (string) reader["descrizione"];

                        if (!Convert.IsDBNull(reader["personaCognome"]))
                        {
                            committente.PersonaCognome = (string) reader["personaCognome"];
                        }
                        if (!Convert.IsDBNull(reader["personaNome"]))
                        {
                            committente.PersonaNome = (string) reader["personaNome"];
                        }
                        if (!Convert.IsDBNull(reader["personaCodiceFiscale"]))
                        {
                            committente.PersonaCodiceFiscale = (string) reader["personaCodiceFiscale"];
                        }
                        if (!Convert.IsDBNull(reader["enteRagioneSociale"]))
                        {
                            committente.RagioneSociale = (string) reader["enteRagioneSociale"];
                        }
                        if (!Convert.IsDBNull(reader["entePartitaIva"]))
                        {
                            committente.PartitaIva = (string) reader["entePartitaIva"];
                        }
                        if (!Convert.IsDBNull(reader["enteCodiceFiscale"]))
                        {
                            committente.CodiceFiscale = (string) reader["enteCodiceFiscale"];
                        }
                    }
                }
            }

            return committente;
        }

        public bool InsertCommittenteTelematicheAnagrafica(CommittenteNotificheTelematiche committente,
            DbTransaction transaction)
        {
            bool res = false;

            if (!committente.IdCommittenteAnagrafica.HasValue)
            {
                using (
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaCommittentiAnagraficaInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipologiaCommittente", DbType.Int32,
                        committente.TipologiaCommittente.IdTipologiaCommittente);

                    if (!string.IsNullOrEmpty(committente.PersonaCognome))
                    {
                        DatabaseCemi.AddInParameter(comando, "@personaCognome", DbType.String,
                            committente.PersonaCognome);
                    }
                    if (!string.IsNullOrEmpty(committente.PersonaNome))
                    {
                        DatabaseCemi.AddInParameter(comando, "@personaNome", DbType.String, committente.PersonaNome);
                    }
                    if (!string.IsNullOrEmpty(committente.PersonaCodiceFiscale))
                    {
                        DatabaseCemi.AddInParameter(comando, "@personaCodiceFiscale", DbType.String,
                            committente.PersonaCodiceFiscale);
                    }
                    if (!string.IsNullOrEmpty(committente.RagioneSociale))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteRagioneSociale", DbType.String,
                            committente.RagioneSociale);
                    }
                    if (!string.IsNullOrEmpty(committente.PartitaIva))
                    {
                        DatabaseCemi.AddInParameter(comando, "@entePartitaIva", DbType.String, committente.PartitaIva);
                    }
                    if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteCodiceFiscale", DbType.String,
                            committente.CodiceFiscale);
                    }
                    DatabaseCemi.AddOutParameter(comando, "@idCommittenteAnagrafica", DbType.Int32, 4);

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        committente.IdCommittenteAnagrafica =
                            (int) DatabaseCemi.GetParameterValue(comando, "idCommittenteAnagrafica");
                        res = true;
                    }
                }
            }

            return res;
        }

        private void InsertImpresaAnagrafica(ImpresaNotificheTelematiche impresa, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaImpreseAnagraficaInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                if (!string.IsNullOrEmpty(impresa.PartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                }
                if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                }
                DatabaseCemi.AddOutParameter(comando, "@idImpresaAnagrafica", DbType.Int32, 4);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    impresa.IdImpresaAnagrafica =
                        (int) DatabaseCemi.GetParameterValue(comando, "@idImpresaAnagrafica");
                }
            }
        }

        public bool UpdateCommittenteTelematiche(CommittenteNotificheTelematiche committente)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaCommittentiUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCommittenteTelematiche", DbType.Int32,
                    committente.IdCommittenteTelematiche.Value);
                DatabaseCemi.AddInParameter(comando, "@idTipologiaCommittente", DbType.Int32,
                    committente.TipologiaCommittente.IdTipologiaCommittente);

                if (!string.IsNullOrEmpty(committente.PersonaIndirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaIndirizzo", DbType.String,
                        committente.PersonaIndirizzo);
                }
                if (!string.IsNullOrEmpty(committente.PersonaComune))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaComune", DbType.String, committente.PersonaComune);
                }
                if (!string.IsNullOrEmpty(committente.PersonaProvincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaProvincia", DbType.String,
                        committente.PersonaProvincia);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCap))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCap", DbType.String, committente.PersonaCap);
                }
                if (!string.IsNullOrEmpty(committente.PersonaTelefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaTelefono", DbType.String,
                        committente.PersonaTelefono);
                }
                if (!string.IsNullOrEmpty(committente.PersonaFax))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaFax", DbType.String, committente.PersonaFax);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCellulare))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCellulare", DbType.String,
                        committente.PersonaCellulare);
                }
                if (!string.IsNullOrEmpty(committente.PersonaEmail))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaEmail", DbType.String, committente.PersonaEmail);
                }
                if (!string.IsNullOrEmpty(committente.Indirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteIndirizzo", DbType.String, committente.Indirizzo);
                }
                if (!string.IsNullOrEmpty(committente.Comune))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteComune", DbType.String, committente.Comune);
                }
                if (!string.IsNullOrEmpty(committente.Provincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteProvincia", DbType.String, committente.Provincia);
                }
                if (!string.IsNullOrEmpty(committente.Cap))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCap", DbType.String, committente.Cap);
                }
                if (!string.IsNullOrEmpty(committente.Telefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteTelefono", DbType.String, committente.Telefono);
                }
                if (!string.IsNullOrEmpty(committente.Fax))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteFax", DbType.String, committente.Fax);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool InsertNotificaTemporanea(Guid userId, NotificaTelematica notifica)
        {
            bool res = false;

            StringBuilder notificaSerializzata = new StringBuilder();
            XmlSerializer serializer = new XmlSerializer(typeof(NotificaTelematica));

            using (TextWriter writer = new StringWriter(notificaSerializzata))
            {
                serializer.Serialize(writer, notifica);
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTemporaneeInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtenteTelematiche", DbType.Guid, userId);
                DatabaseCemi.AddInParameter(comando, "@notifica", DbType.Xml, notificaSerializzata.ToString());

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateNotificaTemporanea(NotificaTelematica notifica)
        {
            bool res = false;

            StringBuilder notificaSerializzata = new StringBuilder();
            XmlSerializer serializer = new XmlSerializer(typeof(NotificaTelematica));

            using (TextWriter writer = new StringWriter(notificaSerializzata))
            {
                serializer.Serialize(writer, notifica);
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTemporaneeUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotificaTemporanea", DbType.Int32,
                    notifica.IdNotificaTemporanea.Value);
                DatabaseCemi.AddInParameter(comando, "@notifica", DbType.Xml, notificaSerializzata.ToString());

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public NotificaTelematicaCollection GetNotificheTemporanee(Guid userId)
        {
            NotificaTelematicaCollection notifiche = new NotificaTelematicaCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTemporaneeSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@userId", DbType.Guid, userId);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdTemporanea = reader.GetOrdinal("idCptNotificaTemporanea");
                    int indiceNotifica = reader.GetOrdinal("notifica");

                    #endregion

                    while (reader.Read())
                    {
                        NotificaTelematica notifica = new NotificaTelematica();

                        TextReader textReader = new StringReader(reader.GetString(indiceNotifica));
                        XmlSerializer serializer = new XmlSerializer(typeof(NotificaTelematica));
                        notifica = (NotificaTelematica) serializer.Deserialize(textReader);

                        notifica.IdNotificaTemporanea = reader.GetInt32(indiceIdTemporanea);

                        notifiche.Add(notifica);
                    }
                }
            }

            return notifiche;
        }

        public NotificaTelematica GetNotificaTemporanea(int idNotificaTemporanea)
        {
            NotificaTelematica notifica = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTemporaneeSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotificaTemporanea", DbType.Int32, idNotificaTemporanea);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceNotifica = reader.GetOrdinal("notifica");

                    #endregion

                    if (reader.Read())
                    {
                        notifica = new NotificaTelematica();

                        TextReader textReader = new StringReader(reader.GetString(indiceNotifica));
                        XmlSerializer serializer = new XmlSerializer(typeof(NotificaTelematica));
                        notifica = (NotificaTelematica) serializer.Deserialize(textReader);

                        notifica.IdNotificaTemporanea = idNotificaTemporanea;
                        notifica.IdNotificaPadre = null;
                    }
                }
            }

            return notifica;
        }

        public bool DeleteNotificaTemporanea(int idNotificaTemporanea, DbTransaction transaction)
        {
            bool res = true;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTemporaneeDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotificaTemporanea", DbType.Int32, idNotificaTemporanea);

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool IsNotificaTelematica(int idNotifica)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaSelectControllo"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCptNotifica", DbType.Int32, idNotifica);
                DatabaseCemi.AddOutParameter(comando, "@telematica", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);
                res = (bool) DatabaseCemi.GetParameterValue(comando, "@telematica");
            }

            return res;
        }

        public TipologiaCommittenteCollection GetTipologieCommittente()
        {
            TipologiaCommittenteCollection tipologie = new TipologiaCommittenteCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptTipologieCommittentiSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdTipologiaCommittente = reader.GetOrdinal("idCptTipologiaCommittente");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipologiaCommittente tipologia = new TipologiaCommittente();
                        tipologie.Add(tipologia);

                        tipologia.IdTipologiaCommittente = reader.GetInt32(indiceIdTipologiaCommittente);
                        tipologia.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipologie;
        }

        #region Aree

        public AreaCollection GetAree()
        {
            AreaCollection aree = new AreaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptAreeSelectAll"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdArea = reader.GetOrdinal("idCptArea");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        Area area = new Area();
                        aree.Add(area);

                        area.IdArea = reader.GetInt16(indiceIdArea);
                        area.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return aree;
        }

        #endregion

        public bool NotificaRegioneDaAggiornare(string protocolloRegione, DateTime data)
        {
            bool res = true;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheSelectDaAggiornare"))
            {
                DatabaseCemi.AddInParameter(comando, "@protocolloRegione", DbType.String, protocolloRegione);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                DatabaseCemi.AddOutParameter(comando, "@daAggiornare", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);

                res = (bool) DatabaseCemi.GetParameterValue(comando, "@daAggiornare");
            }

            return res;
        }

        public List<string> GetProtocolliRegioneCaricati()
        {
            List<string> protocolli = new List<string>();

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheSelectRegione"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceProtocollo = reader.GetOrdinal("protocolloRegione");

                    #endregion

                    while (reader.Read())
                    {
                        protocolli.Add(reader.GetString(indiceProtocollo));
                    }
                }
            }

            return protocolli;
        }

        public IndirizzoCollection GetCantieriGenerati(int idNotificaRiferimento)
        {
            IndirizzoCollection cantieri = new IndirizzoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheSelectCantieriGenerati"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotificaRiferimento", DbType.Int32, idNotificaRiferimento);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceId = reader.GetOrdinal("idCantiere");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");

                    #endregion

                    while (reader.Read())
                    {
                        Indirizzo indirizzo = new Indirizzo();
                        cantieri.Add(indirizzo);

                        indirizzo.Identificativo = reader.GetInt32(indiceId);
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            indirizzo.Indirizzo1 = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            indirizzo.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            indirizzo.Provincia = reader.GetString(indiceProvincia);
                        }
                    }
                }
            }

            return cantieri;
        }

        #region Notifiche da Web Service

        public int? GetIdImpresaDaCodiceFiscale(string codiceFiscale)
        {
            int? idImpresa = null;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_CptNotificheTelematicheRecuperaCodiceImpresaDaCodiceFiscale"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codiceFiscale);
                DatabaseCemi.AddOutParameter(comando, "@idImpresa", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                idImpresa = DatabaseCemi.GetParameterValue(comando, "@idImpresa") as int?;
            }

            return idImpresa;
        }

        public bool EsisteNotificaRegione(string protocolloRegione)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheEsisteNotificaRegione"))
            {
                DatabaseCemi.AddInParameter(comando, "@protocolloRegione", DbType.String, protocolloRegione);
                DatabaseCemi.AddOutParameter(comando, "@esiste", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);

                res = (bool) DatabaseCemi.GetParameterValue(comando, "@esiste");
            }

            return res;
        }

        public void CompletaNotificaTelematicaConRiferimenti(NotificaTelematica notifica)
        {
            if (string.IsNullOrEmpty(notifica.ProtocolloRegione))
            {
                throw new ArgumentException(
                    "CompletaNotificaTelematicaConRiferimenti: Protocollo regione non valorizzato");
            }

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheRiferimentiSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@protocolloRegione", DbType.String, notifica.ProtocolloRegione);
                DatabaseCemi.AddOutParameter(comando, "@idNotifica", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@idCommittente", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@idRespLavori", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@idCoordProg", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@idCoordReal", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                notifica.IdNotifica = DatabaseCemi.GetParameterValue(comando, "@idNotifica") as int?;
                if (notifica.Committente != null)
                {
                    notifica.Committente.IdCommittenteTelematiche =
                        DatabaseCemi.GetParameterValue(comando, "@idCommittente") as int?;
                }
                if (notifica.DirettoreLavori != null)
                {
                    notifica.DirettoreLavori.IdPersona =
                        DatabaseCemi.GetParameterValue(comando, "@idRespLavori") as int?;
                }
                if (notifica.CoordinatoreSicurezzaProgettazione != null)
                {
                    notifica.CoordinatoreSicurezzaProgettazione.IdPersona =
                        DatabaseCemi.GetParameterValue(comando, "@idCoordProg") as int?;
                }
                if (notifica.CoordinatoreSicurezzaRealizzazione != null)
                {
                    notifica.CoordinatoreSicurezzaRealizzazione.IdPersona =
                        DatabaseCemi.GetParameterValue(comando, "@idCoordReal") as int?;
                }
            }
        }

        private bool UpdateCommittenteTotale(CommittenteNotificheTelematiche committente, DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaCommittentiUpdateTotale"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32,
                    committente.IdCommittenteTelematiche.Value);

                DatabaseCemi.AddInParameter(comando, "@idTipologiaCommittente", DbType.Int32,
                    committente.TipologiaCommittente.IdTipologiaCommittente);
                if (!string.IsNullOrEmpty(committente.PersonaCognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCognome", DbType.String, committente.PersonaCognome);
                }
                if (!string.IsNullOrEmpty(committente.PersonaNome))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaNome", DbType.String, committente.PersonaNome);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCodiceFiscale", DbType.String,
                        committente.PersonaCodiceFiscale);
                }
                if (!string.IsNullOrEmpty(committente.PersonaIndirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaIndirizzo", DbType.String,
                        committente.PersonaIndirizzo);
                }
                if (!string.IsNullOrEmpty(committente.PersonaComune))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaComune", DbType.String, committente.PersonaComune);
                }
                if (!string.IsNullOrEmpty(committente.PersonaProvincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaProvincia", DbType.String,
                        committente.PersonaProvincia);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCap))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCap", DbType.String, committente.PersonaCap);
                }
                if (!string.IsNullOrEmpty(committente.PersonaTelefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaTelefono", DbType.String,
                        committente.PersonaTelefono);
                }
                if (!string.IsNullOrEmpty(committente.PersonaFax))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaFax", DbType.String, committente.PersonaFax);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCellulare))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCellulare", DbType.String,
                        committente.PersonaCellulare);
                }
                if (!string.IsNullOrEmpty(committente.PersonaEmail))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaEmail", DbType.String, committente.PersonaEmail);
                }
                if (!string.IsNullOrEmpty(committente.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteRagioneSociale", DbType.String,
                        committente.RagioneSociale);
                }
                if (!string.IsNullOrEmpty(committente.PartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@entePartitaIva", DbType.String, committente.PartitaIva);
                }
                if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCodiceFiscale", DbType.String,
                        committente.CodiceFiscale);
                }
                if (!string.IsNullOrEmpty(committente.Indirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteIndirizzo", DbType.String, committente.Indirizzo);
                }
                if (!string.IsNullOrEmpty(committente.Comune))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteComune", DbType.String, committente.Comune);
                }
                if (!string.IsNullOrEmpty(committente.Provincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteProvincia", DbType.String, committente.Provincia);
                }
                if (!string.IsNullOrEmpty(committente.Cap))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCap", DbType.String, committente.Cap);
                }
                if (!string.IsNullOrEmpty(committente.Telefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteTelefono", DbType.String, committente.Telefono);
                }
                if (!string.IsNullOrEmpty(committente.Fax))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteFax", DbType.String, committente.Fax);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private bool UpdatePersona(PersonaNotificheTelematiche persona, DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptPersoneUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPersona", DbType.String, persona.IdPersona.Value);
                DatabaseCemi.AddInParameter(comando, "@nominativo", DbType.String,
                    string.Format("{0} {1}", persona.PersonaCognome, persona.PersonaNome));
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, persona.PersonaCognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, persona.PersonaNome);
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, persona.PersonaCodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, persona.Indirizzo);
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, persona.PersonaComune);
                DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, persona.PersonaProvincia);
                if (!string.IsNullOrEmpty(persona.PersonaCap))
                {
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, persona.PersonaCap);
                }
                if (!string.IsNullOrEmpty(persona.Telefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, persona.Telefono);
                }
                if (!string.IsNullOrEmpty(persona.Fax))
                {
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, persona.Fax);
                }
                if (!string.IsNullOrEmpty(persona.PersonaCellulare))
                {
                    DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String, persona.PersonaCellulare);
                }
                if (!string.IsNullOrEmpty(persona.PersonaEmail))
                {
                    DatabaseCemi.AddInParameter(comando, "@email", DbType.String, persona.PersonaEmail);
                }

                // Ente
                if (!string.IsNullOrEmpty(persona.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, persona.RagioneSociale);
                }
                if (!string.IsNullOrEmpty(persona.EntePartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@entePartitaIva", DbType.String, persona.EntePartitaIva);
                }
                if (!string.IsNullOrEmpty(persona.EnteCodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCodiceFiscale", DbType.String,
                        persona.EnteCodiceFiscale);
                }
                if (!string.IsNullOrEmpty(persona.EnteIndirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteIndirizzo", DbType.String, persona.EnteIndirizzo);
                }
                if (!string.IsNullOrEmpty(persona.EnteComune))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteComune", DbType.String, persona.EnteComune);
                }
                if (!string.IsNullOrEmpty(persona.EnteProvincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteProvincia", DbType.String, persona.EnteProvincia);
                }
                if (!string.IsNullOrEmpty(persona.EnteCap))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCap", DbType.String, persona.EnteCap);
                }
                if (!string.IsNullOrEmpty(persona.EnteTelefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteTelefono", DbType.String, persona.EnteTelefono);
                }
                if (!string.IsNullOrEmpty(persona.EnteFax))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteFax", DbType.String, persona.EnteFax);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private void DeleteIndirizziNotifica(int idNotifica, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificaIndirizziDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) <= 0)
                {
                    throw new Exception("DeleteIndirizziNotifica: cancellazione indirizzi non riuscita");
                }
            }
        }

        private void DeleteSubappaltiNotificaTelematica(int idNotifica, DbTransaction transaction)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaSubappaltiDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);

                DatabaseCemi.ExecuteNonQuery(comando, transaction);
            }
        }

        public bool UpdateNotifica(NotificaTelematica notifica)
        {
            bool res = false;

            if (notifica.Indirizzi != null && notifica.Indirizzi.Count > 0 && notifica.Committente != null
                && !string.IsNullOrEmpty(notifica.NaturaOpera))
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();
                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            if (notifica.DirettoreLavori != null && notifica.DirettoreLavori.IdPersona.HasValue)
                            {
                                UpdatePersona(notifica.DirettoreLavori, transaction);
                            }
                            if (notifica.CoordinatoreSicurezzaProgettazione != null &&
                                notifica.CoordinatoreSicurezzaProgettazione.IdPersona.HasValue)
                            {
                                UpdatePersona(notifica.CoordinatoreSicurezzaProgettazione, transaction);
                            }
                            if (notifica.CoordinatoreSicurezzaRealizzazione != null &&
                                notifica.CoordinatoreSicurezzaRealizzazione.IdPersona.HasValue)
                            {
                                UpdatePersona(notifica.CoordinatoreSicurezzaRealizzazione, transaction);
                            }

                            if (InserisciPersoneNotifica(notifica, transaction))
                            {
                                bool resParziale = false;

                                if (notifica.Committente.IdCommittenteTelematiche.HasValue)
                                {
                                    resParziale = UpdateCommittenteTotale(notifica.Committente, transaction);
                                }
                                else
                                {
                                    resParziale = InserisciCommittenteNotifica(notifica.Committente, transaction);
                                }

                                if (resParziale)
                                {
                                    // Inserimento dati notifica
                                    using (DbCommand comando =
                                        DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheUpdate"))
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32,
                                            notifica.IdNotifica.Value);
                                        DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, notifica.Data);
                                        DatabaseCemi.AddInParameter(comando, "@idNotificaTelematicaCommittente",
                                            DbType.Int32,
                                            notifica.Committente.IdCommittenteTelematiche.Value);
                                        DatabaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String,
                                            notifica.NaturaOpera);
                                        if (!string.IsNullOrEmpty(notifica.Note))
                                        {
                                            DatabaseCemi.AddInParameter(comando, "@note", DbType.String,
                                                notifica.Note);
                                        }
                                        if (notifica.OperaPubblica.HasValue)
                                        {
                                            DatabaseCemi.AddInParameter(comando, "@operaPubblica", DbType.Boolean,
                                                notifica.OperaPubblica.Value);
                                        }

                                        if (!string.IsNullOrEmpty(notifica.NumeroAppalto))
                                            DatabaseCemi.AddInParameter(comando, "@numeroAppalto", DbType.String,
                                                notifica.NumeroAppalto);

                                        // Persone
                                        if (notifica.CoordinatoreSicurezzaProgettazione != null)
                                            DatabaseCemi.AddInParameter(comando,
                                                "@idCptPersonaCoordinatoreProgettazione",
                                                DbType.Int32,
                                                notifica.CoordinatoreSicurezzaProgettazione.IdPersona.Value);
                                        if (notifica.CoordinatoreSicurezzaRealizzazione != null)
                                            DatabaseCemi.AddInParameter(comando,
                                                "@idCptPersonaCoordinatoreRealizzazione",
                                                DbType.Int32,
                                                notifica.CoordinatoreSicurezzaRealizzazione.IdPersona.Value);
                                        if (notifica.DirettoreLavori != null)
                                            DatabaseCemi.AddInParameter(comando, "@idCptPersonaDirettoreLavori",
                                                DbType.Int32,
                                                notifica.DirettoreLavori.IdPersona.Value);

                                        if (notifica.DataInizioLavori.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@dataInizioLavori", DbType.DateTime,
                                                notifica.DataInizioLavori.Value);
                                        if (notifica.DataFineLavori.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@dataFineLavori", DbType.DateTime,
                                                notifica.DataFineLavori.Value);
                                        if (notifica.Durata.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@durataLavori", DbType.Int32,
                                                notifica.Durata.Value);
                                        if (notifica.NumeroGiorniUomo.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@giorniUomoLavori", DbType.Int32,
                                                notifica.NumeroGiorniUomo.Value);
                                        if (notifica.NumeroMassimoLavoratori.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@numeroMassimoLavoratori",
                                                DbType.Int32,
                                                notifica.NumeroMassimoLavoratori.Value);
                                        if (notifica.NumeroImprese.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@numeroImprese", DbType.Int32,
                                                notifica.NumeroImprese.Value);
                                        if (notifica.NumeroLavoratoriAutonomi.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@numeroLavoratoriAutonomi",
                                                DbType.Int32,
                                                notifica.NumeroLavoratoriAutonomi.Value);
                                        if (notifica.AmmontareComplessivo.HasValue)
                                            DatabaseCemi.AddInParameter(comando, "@ammontareComplessivo",
                                                DbType.Decimal,
                                                notifica.AmmontareComplessivo.Value);

                                        if (notifica.ResponsabileCommittente)
                                            DatabaseCemi.AddInParameter(comando, "@responsabileLavoriCommittente",
                                                DbType.Boolean,
                                                notifica.ResponsabileCommittente);

                                        DatabaseCemi.AddInParameter(comando, "@coordinatoreProgettazioneNonNominato",
                                            DbType.Boolean,
                                            notifica.CoordinatoreProgettazioneNonNominato);
                                        DatabaseCemi.AddInParameter(comando, "@coordinatoreEsecuzioneNonNominato",
                                            DbType.Boolean,
                                            notifica.CoordinatoreEsecuzioneNonNominato);

                                        if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                        {
                                            DeleteIndirizziNotifica(notifica.IdNotifica.Value, transaction);

                                            // Inserimento dati indirizzi
                                            if (
                                                InserisciIndirizziNotifica(notifica.Indirizzi,
                                                    notifica.IdNotifica.Value,
                                                    transaction))
                                            {
                                                // SISTEMO LE IMPRESE IN MODO DA NON DOVERLE INSERIRE PIU' VOLTE
                                                if (notifica.ImpreseAffidatarie != null)
                                                {
                                                    // Sistemo le imprese affidatarie
                                                    for (int i = 0; i < notifica.ImpreseAffidatarie.Count; i++)
                                                    {
                                                        for (int k = 0; k < notifica.ImpreseEsecutrici.Count; k++)
                                                        {
                                                            if (notifica.ImpreseAffidatarie[i].ImpresaSelezionata !=
                                                                null
                                                                &&
                                                                notifica.ImpreseEsecutrici[k].AppaltataDa != null
                                                                &&
                                                                (notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                     .IdTemporaneo != Guid.Empty
                                                                 &&
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                     .IdTemporaneo !=
                                                                 Guid.Empty
                                                                 &&
                                                                 notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                     .IdTemporaneo ==
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa.IdTemporaneo
                                                                 ||
                                                                 notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                     .IdImpresaTelematica.HasValue
                                                                 &&
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                     .IdImpresaTelematica.HasValue
                                                                 &&
                                                                 notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                     .IdImpresaTelematica ==
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                     .IdImpresaTelematica
                                                                )
                                                            )
                                                            {
                                                                notifica.ImpreseEsecutrici[k].AppaltataDa =
                                                                    notifica.ImpreseAffidatarie[i].ImpresaSelezionata;
                                                            }
                                                        }
                                                    }

                                                    // Sistemo le esecutrici
                                                    for (int i = 0; i < notifica.ImpreseEsecutrici.Count; i++)
                                                    {
                                                        for (int k = i + 1; k < notifica.ImpreseEsecutrici.Count; k++)
                                                        {
                                                            if (notifica.ImpreseEsecutrici[i].ImpresaSelezionata != null
                                                                &&
                                                                notifica.ImpreseEsecutrici[k].AppaltataDa != null
                                                                &&
                                                                (notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                     .IdTemporaneo != Guid.Empty
                                                                 &&
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                     .IdTemporaneo !=
                                                                 Guid.Empty
                                                                 &&
                                                                 notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                     .IdTemporaneo ==
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa.IdTemporaneo
                                                                 ||
                                                                 notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                     .IdImpresaTelematica.HasValue
                                                                 &&
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                     .IdImpresaTelematica.HasValue
                                                                 &&
                                                                 notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                     .IdImpresaTelematica ==
                                                                 notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                     .IdImpresaTelematica
                                                                )
                                                            )
                                                            {
                                                                notifica.ImpreseEsecutrici[k].AppaltataDa =
                                                                    notifica.ImpreseEsecutrici[i].ImpresaSelezionata;
                                                            }
                                                        }
                                                    }
                                                }
                                                foreach (
                                                    SubappaltoNotificheTelematiche sub in notifica.ImpreseAffidatarie)
                                                {
                                                    sub.IdSubappalto = null;

                                                    if (sub.ImpresaSelezionata != null)
                                                    {
                                                        sub.ImpresaSelezionata.IdImpresaTelematica = null;
                                                    }
                                                    if (sub.AppaltataDa != null)
                                                    {
                                                        sub.AppaltataDa.IdImpresaTelematica = null;
                                                    }
                                                }
                                                foreach (
                                                    SubappaltoNotificheTelematiche sub in notifica.ImpreseEsecutrici)
                                                {
                                                    sub.IdSubappalto = null;

                                                    if (sub.ImpresaSelezionata != null)
                                                    {
                                                        sub.ImpresaSelezionata.IdImpresaTelematica = null;
                                                    }
                                                    if (sub.AppaltataDa != null)
                                                    {
                                                        sub.AppaltataDa.IdImpresaTelematica = null;
                                                    }
                                                }
                                                // FINE

                                                DeleteSubappaltiNotificaTelematica(notifica.IdNotifica.Value,
                                                    transaction);

                                                if (
                                                    InserisciSubappaltiNotifica(notifica.ImpreseAffidatarie,
                                                        notifica.IdNotifica.Value, transaction)
                                                    &&
                                                    InserisciSubappaltiNotifica(notifica.ImpreseEsecutrici,
                                                        notifica.IdNotifica.Value, transaction))
                                                {
                                                    //if (notifica.IdNotificaTemporanea.HasValue)
                                                    //{
                                                    //    DeleteNotificaTemporanea(notifica.IdNotificaTemporanea.Value, transaction);
                                                    //}

                                                    res = true;
                                                }
                                                else
                                                {
                                                    throw new Exception(
                                                        "Errore durante l'inserimento di un subappalto");
                                                }
                                            }
                                            else
                                            {
                                                throw new Exception("Errore durante l'inserimento di un indirizzo");
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("Aggiornamento della notifica non andato a buon fine");
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception(
                                        "Non � andata a buon fine l'inserimento/aggiornamento del committente");
                                }
                            }
                            else
                            {
                                throw new Exception("Non � andata a buon fine l'inserimento delle persone");
                            }
                        }
                        finally
                        {
                            if (res)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                            }
                        }
                    }
                }
            }

            return res;
        }

        #endregion

        #region Visite ai cantieri

        public TipologiaVisitaCollection GetTipologieVisita()
        {
            TipologiaVisitaCollection tipologie = new TipologiaVisitaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptTipologieVisitaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipologia = reader.GetOrdinal("idCptTipologiaVisita");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipologiaVisita tipologia = new TipologiaVisita();
                        tipologie.Add(tipologia);

                        tipologia.IdTipologiaVisita = reader.GetInt32(indiceIdTipologia);
                        tipologia.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipologie;
        }

        public EsitoVisitaCollection GetEsitiVisita()
        {
            EsitoVisitaCollection esiti = new EsitoVisitaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptEsitiVisitaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdEsito = reader.GetOrdinal("idCptEsitoVisita");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        EsitoVisita esito = new EsitoVisita();
                        esiti.Add(esito);

                        esito.IdEsitoVisita = reader.GetInt32(indiceIdEsito);
                        esito.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return esiti;
        }

        public GradoIrregolaritaCollection GetGradiIrregolarita()
        {
            GradoIrregolaritaCollection gradi = new GradoIrregolaritaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptGradiIrregolaritaSelectAll"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdGrado = reader.GetOrdinal("idCptGradoIrregolarita");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        GradoIrregolarita grado = new GradoIrregolarita();
                        gradi.Add(grado);

                        grado.IdGradoIrregolarita = reader.GetInt16(indiceIdGrado);
                        grado.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return gradi;
        }

        public bool InsertVisita(Visita visita)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@ente", DbType.Int32, visita.Ente);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, visita.IdUtente);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, visita.Data);
                DatabaseCemi.AddInParameter(comando, "@idTipologiaVisita", DbType.Int32,
                    visita.Tipologia.IdTipologiaVisita);
                DatabaseCemi.AddInParameter(comando, "@idEsitoVisita", DbType.Int32, visita.Esito.IdEsitoVisita);
                if (visita.GradoIrregolarita != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idGradoIrregolarita", DbType.Int16,
                        visita.GradoIrregolarita.IdGradoIrregolarita);
                }
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, visita.IdNotifica);
                DatabaseCemi.AddOutParameter(comando, "@idVisita", DbType.Int32, 4);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    int idVisita = (int) DatabaseCemi.GetParameterValue(comando, "@idVisita");

                    if (idVisita > 0)
                    {
                        visita.IdVisita = idVisita;
                        res = true;
                    }
                }
            }

            return res;
        }

        public AllegatoCollection GetAllegati(int idVisita)
        {
            AllegatoCollection allegati = new AllegatoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteAllegatiSelectByVisita"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVisita", DbType.Int32, idVisita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdAllegato = reader.GetOrdinal("idCptVisitaAllegato");
                    int indiceIdVisita = reader.GetOrdinal("idCptVisita");
                    int indiceNomeFile = reader.GetOrdinal("nomeFile");

                    while (reader.Read())
                    {
                        Allegato allegato = new Allegato();
                        allegati.Add(allegato);

                        allegato.IdAllegato = reader.GetInt32(indiceIdAllegato);
                        allegato.NomeFile = reader.GetString(indiceNomeFile);
                        allegato.IdVisita = reader.GetInt32(indiceIdVisita);
                    }
                }
            }

            return allegati;
        }

        public bool InsertAllegato(Allegato allegato)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteAllegatiInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVisita", DbType.Int32, allegato.IdVisita);
                DatabaseCemi.AddInParameter(comando, "@nomeFile", DbType.String, allegato.NomeFile);
                DatabaseCemi.AddInParameter(comando, "@file", DbType.Binary, allegato.File);
                DatabaseCemi.AddOutParameter(comando, "@idAllegato", DbType.Int32, 4);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    int idAllegato = (int) DatabaseCemi.GetParameterValue(comando, "@idAllegato");

                    if (idAllegato > 0)
                    {
                        allegato.IdAllegato = idAllegato;
                        res = true;
                    }
                }
            }

            return res;
        }

        public Allegato GetAllegato(int idAllegato)
        {
            Allegato allegato = new Allegato();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteAllegatiSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idAllegato", DbType.Int32, idAllegato);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdAllegato = reader.GetOrdinal("idCptVisitaAllegato");
                    int indiceIdVisita = reader.GetOrdinal("idCptVisita");
                    int indiceNomeFile = reader.GetOrdinal("nomeFile");

                    reader.Read();

                    allegato.IdAllegato = reader.GetInt32(indiceIdAllegato);
                    allegato.NomeFile = reader.GetString(indiceNomeFile);
                    allegato.IdVisita = reader.GetInt32(indiceIdVisita);
                    allegato.File = (byte[]) reader["file"];
                }
            }

            return allegato;
        }

        public VisitaCollection GetVisite(int idNotificaRiferimento)
        {
            VisitaCollection visite = new VisitaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteSelectByIdNotifica"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotificaRiferimento);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Visita visita = null;

                    while (reader.Read())
                    {
                        int idVisita = (int) reader["idCptVisita"];

                        if (visita == null || visita.IdVisita.Value != idVisita)
                        {
                            visita = new Visita();
                            visite.Add(visita);

                            visita.IdVisita = idVisita;
                            visita.Ente = (EnteVisita) reader["ente"];
                            visita.IdUtente = (int) reader["idUtente"];
                            visita.Data = (DateTime) reader["data"];
                            visita.Tipologia = new TipologiaVisita();
                            visita.Tipologia.IdTipologiaVisita = (int) reader["idCptTipologiaVisita"];
                            visita.Tipologia.Descrizione = (string) reader["tipologiaDescrizione"];
                            visita.Esito = new EsitoVisita();
                            visita.Esito.IdEsitoVisita = (int) reader["idCptEsitoVisita"];
                            visita.Esito.Descrizione = (string) reader["esitoDescrizione"];

                            if (!Convert.IsDBNull(reader["idCptGradoIrregolarita"]))
                            {
                                visita.GradoIrregolarita = new GradoIrregolarita();
                                visita.GradoIrregolarita.IdGradoIrregolarita = (short) reader["idCptGradoIrregolarita"];
                                visita.GradoIrregolarita.Descrizione = (string) reader["gradoIrregolaritaDescrizione"];
                            }

                            visita.Allegati = new AllegatoCollection();
                        }

                        if (!Convert.IsDBNull(reader["idCptVisitaAllegato"]))
                        {
                            Allegato allegato = new Allegato();
                            allegato.IdAllegato = (int) reader["idCptVisitaAllegato"];
                            allegato.NomeFile = (string) reader["nomeFile"];

                            visita.Allegati.Add(allegato);
                        }
                    }
                }
            }

            return visite;
        }

        public VisitaCollection GetVisite(VisitaFilter filtro)
        {
            VisitaCollection visite = new VisitaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteSelectByFilter"))
            {
                if (filtro.Data.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, filtro.Data);
                if (filtro.Ente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@ente", DbType.Int32, filtro.Ente);
                if (filtro.Tipologia != null)
                    DatabaseCemi.AddInParameter(comando, "@tipologia", DbType.Int32,
                        filtro.Tipologia.IdTipologiaVisita);
                if (filtro.Esito != null)
                    DatabaseCemi.AddInParameter(comando, "@esito", DbType.Int32, filtro.Esito.IdEsitoVisita);
                if (!string.IsNullOrEmpty(filtro.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filtro.Indirizzo);
                if (!string.IsNullOrEmpty(filtro.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.Comune);
                if (filtro.IdASL.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idASL", DbType.Int32, filtro.IdASL.Value);
                DatabaseCemi.AddInParameter(comando, "@idArea", DbType.Int16, filtro.IdArea);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    Visita visita = null;

                    while (reader.Read())
                    {
                        int idVisita = (int) reader["idCptVisita"];

                        if (visita == null || visita.IdVisita.Value != idVisita)
                        {
                            visita = new Visita();
                            visite.Add(visita);

                            visita.IdVisita = idVisita;
                            visita.Ente = (EnteVisita) reader["ente"];
                            visita.IdUtente = (int) reader["idUtente"];
                            visita.Data = (DateTime) reader["data"];
                            visita.Tipologia = new TipologiaVisita();
                            visita.Tipologia.IdTipologiaVisita = (int) reader["idCptTipologiaVisita"];
                            visita.Tipologia.Descrizione = (string) reader["tipologiaDescrizione"];
                            visita.Esito = new EsitoVisita();
                            visita.Esito.IdEsitoVisita = (int) reader["idCptEsitoVisita"];
                            visita.Esito.Descrizione = (string) reader["esitoDescrizione"];
                            visita.IdNotifica = (int) reader["idCptNotificaRiferimento"];

                            if (!Convert.IsDBNull(reader["idCptGradoIrregolarita"]))
                            {
                                visita.GradoIrregolarita = new GradoIrregolarita();
                                visita.GradoIrregolarita.IdGradoIrregolarita = (short) reader["idCptGradoIrregolarita"];
                                visita.GradoIrregolarita.Descrizione = (string) reader["gradoIrregolaritaDescrizione"];
                            }

                            visita.Allegati = new AllegatoCollection();
                            visita.Indirizzi = new IndirizzoCollection();
                        }

                        if (!Convert.IsDBNull(reader["idCptVisitaAllegato"]))
                        {
                            Allegato allegato = new Allegato();
                            allegato.IdAllegato = (int) reader["idCptVisitaAllegato"];
                            allegato.NomeFile = (string) reader["nomeFile"];

                            visita.Allegati.AddUnico(allegato);
                        }

                        if (!Convert.IsDBNull(reader["idCptIndirizzo"]))
                        {
                            Indirizzo indirizzo = new Indirizzo();
                            indirizzo.Indirizzo1 = (string) reader["indirizzo"];
                            if (!Convert.IsDBNull(reader["civico"]))
                                indirizzo.Civico = (string) reader["civico"];
                            if (!Convert.IsDBNull(reader["comune"]))
                                indirizzo.Comune = (string) reader["comune"];
                            if (!Convert.IsDBNull(reader["provincia"]))
                                indirizzo.Provincia = (string) reader["provincia"];
                            if (!Convert.IsDBNull(reader["cap"]))
                                indirizzo.Cap = (string) reader["cap"];
                            if (!Convert.IsDBNull(reader["infoAggiuntiva"]))
                                indirizzo.InfoAggiuntiva = (string) reader["infoAggiuntiva"];

                            visita.Indirizzi.AddUnico(indirizzo);
                        }
                    }
                }
            }

            return visite;
        }

        public Visita GetVisita(int idVisita)
        {
            Visita visita = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVisita", DbType.Int32, idVisita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        if (visita == null)
                        {
                            visita = new Visita();

                            visita.IdVisita = (int) reader["idCptVisita"];
                            visita.Ente = (EnteVisita) reader["ente"];
                            visita.IdUtente = (int) reader["idUtente"];
                            visita.Data = (DateTime) reader["data"];
                            visita.Tipologia = new TipologiaVisita();
                            visita.Tipologia.IdTipologiaVisita = (int) reader["idCptTipologiaVisita"];
                            visita.Tipologia.Descrizione = (string) reader["tipologiaDescrizione"];
                            visita.Esito = new EsitoVisita();
                            visita.Esito.IdEsitoVisita = (int) reader["idCptEsitoVisita"];
                            visita.Esito.Descrizione = (string) reader["esitoDescrizione"];
                            visita.IdNotifica = (int) reader["idCptNotificaRiferimento"];

                            if (!Convert.IsDBNull(reader["idCptGradoIrregolarita"]))
                            {
                                visita.GradoIrregolarita = new GradoIrregolarita();
                                visita.GradoIrregolarita.IdGradoIrregolarita = (short) reader["idCptGradoIrregolarita"];
                                visita.GradoIrregolarita.Descrizione = (string) reader["gradoIrregolaritaDescrizione"];
                            }

                            visita.Allegati = new AllegatoCollection();
                        }

                        if (!Convert.IsDBNull(reader["idCptVisitaAllegato"]))
                        {
                            Allegato allegato = new Allegato();
                            allegato.IdAllegato = (int) reader["idCptVisitaAllegato"];
                            allegato.NomeFile = (string) reader["nomeFile"];

                            visita.Allegati.Add(allegato);
                        }
                    }
                }
            }

            return visita;
        }

        public bool UpdateVisita(Visita visita)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVisita", DbType.Int32, visita.IdVisita.Value);
                DatabaseCemi.AddInParameter(comando, "@ente", DbType.Int32, visita.Ente);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, visita.IdUtente);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, visita.Data);
                DatabaseCemi.AddInParameter(comando, "@idTipologiaVisita", DbType.Int32,
                    visita.Tipologia.IdTipologiaVisita);
                DatabaseCemi.AddInParameter(comando, "@idEsitoVisita", DbType.Int32, visita.Esito.IdEsitoVisita);
                if (visita.GradoIrregolarita != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@idGradoIrregolarita", DbType.Int16,
                        visita.GradoIrregolarita.IdGradoIrregolarita);
                }
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, visita.IdNotifica);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool DeleteVisita(int idVisita)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVisita", DbType.Int32, idVisita);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool DeleteVisitaAllegato(int idVisita, int idAllegato)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptVisiteAllegatiDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVisita", DbType.Int32, idVisita);
                DatabaseCemi.AddInParameter(comando, "@idAllegato", DbType.Int32, idAllegato);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                {
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Inserimento notifica

        public bool InserisciNotifica(Notifica notifica)
        {
            bool res = false;

            if (notifica.Indirizzi != null && notifica.Indirizzi.Count > 0 && notifica.Committente != null
                && !string.IsNullOrEmpty(notifica.NaturaOpera))
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();
                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            if (InserisciPersoneNotifica(notifica, transaction))
                            {
                                if (notifica.Committente.IdCommittente.HasValue && !notifica.Committente.Modificato
                                    ||
                                    notifica.Committente.IdCommittente.HasValue &&
                                    ModificaCommittenteNotifica(notifica, transaction)
                                    || InserisciCommittenteNotifica(notifica, transaction))
                                {
                                    // Inserimento dati notifica
                                    DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheInsert");

                                    // Obbligatori
                                    DatabaseCemi.AddInParameter(comando, "@idArea", DbType.Int16, notifica.Area.IdArea);
                                    DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, notifica.Data);
                                    DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32,
                                        notifica.Committente.IdCommittente.Value);
                                    DatabaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String,
                                        notifica.NaturaOpera);

                                    // Facoltativi
                                    if (notifica.IdNotificaPadre.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@idNotificaPadre", DbType.Int32,
                                            notifica.IdNotificaPadre.Value);
                                    if (!string.IsNullOrEmpty(notifica.NumeroAppalto))
                                        DatabaseCemi.AddInParameter(comando, "@numeroAppalto", DbType.String,
                                            notifica.NumeroAppalto);

                                    // Persone
                                    if (notifica.CoordinatoreSicurezzaProgettazione != null)
                                        DatabaseCemi.AddInParameter(comando, "@idCptPersonaCoordinatoreProgettazione",
                                            DbType.Int32,
                                            notifica.CoordinatoreSicurezzaProgettazione.IdPersona.Value);
                                    if (notifica.CoordinatoreSicurezzaRealizzazione != null)
                                        DatabaseCemi.AddInParameter(comando, "@idCptPersonaCoordinatoreRealizzazione",
                                            DbType.Int32,
                                            notifica.CoordinatoreSicurezzaRealizzazione.IdPersona.Value);
                                    if (notifica.DirettoreLavori != null)
                                        DatabaseCemi.AddInParameter(comando, "@idCptPersonaDirettoreLavori",
                                            DbType.Int32,
                                            notifica.DirettoreLavori.IdPersona.Value);

                                    if (notifica.DataInizioLavori.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@dataInizioLavori", DbType.DateTime,
                                            notifica.DataInizioLavori.Value);
                                    if (notifica.DataFineLavori.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@dataFineLavori", DbType.DateTime,
                                            notifica.DataFineLavori.Value);
                                    if (notifica.Durata.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@durataLavori", DbType.Int32,
                                            notifica.Durata.Value);
                                    if (notifica.NumeroGiorniUomo.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@giorniUomoLavori", DbType.Int32,
                                            notifica.NumeroGiorniUomo.Value);
                                    if (notifica.NumeroMassimoLavoratori.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@numeroMassimoLavoratori", DbType.Int32,
                                            notifica.NumeroMassimoLavoratori.Value);
                                    if (notifica.NumeroImprese.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@numeroImprese", DbType.Int32,
                                            notifica.NumeroImprese.Value);
                                    if (notifica.NumeroLavoratoriAutonomi.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@numeroLavoratoriAutonomi", DbType.Int32,
                                            notifica.NumeroLavoratoriAutonomi.Value);
                                    if (notifica.AmmontareComplessivo.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@ammontareComplessivo", DbType.Decimal,
                                            notifica.AmmontareComplessivo.Value);

                                    DatabaseCemi.AddInParameter(comando, "@utente", DbType.String, notifica.Utente);

                                    if (notifica.ResponsabileCommittente)
                                        DatabaseCemi.AddInParameter(comando, "@responsabileCommittente", DbType.Boolean,
                                            notifica.ResponsabileCommittente);

                                    decimal idNotificaTemp = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);
                                    if (idNotificaTemp > 0)
                                    {
                                        notifica.IdNotifica = decimal.ToInt32(idNotificaTemp);

                                        // Inserimento dati indirizzi
                                        if (
                                            InserisciIndirizziNotifica(notifica.Indirizzi, notifica.IdNotifica.Value,
                                                transaction))
                                        {
                                            // Inserimento dati subappalti
                                            if (
                                                InserisciSubappaltiNotifica(notifica.Subappalti,
                                                    notifica.IdNotifica.Value, transaction))
                                                res = true;
                                        }
                                    }
                                }
                            }
                        }
                        finally
                        {
                            if (res)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                                notifica.IdNotifica = null;
                            }
                        }
                    }
                    connection.Close();
                }
            }

            return res;
        }

        #region Funzioni per InserisciNotifica

        #region Subappalti

        private bool InserisciSubappaltiNotifica(IEnumerable<Subappalto> subappalti, int idNotifica,
            DbTransaction transaction)
        {
            bool res = true;

            if (subappalti != null)
            {
                foreach (Subappalto subappalto in subappalti)
                {
                    if (!InserisciSubappaltoNotifica(subappalto, idNotifica, transaction))
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        private bool InserisciSubappaltoNotifica(Subappalto subappalto, int idNotifica, DbTransaction transaction)
        {
            bool res = false;

            if (!subappalto.IdSubappalto.HasValue && subappalto.Appaltata != null)
            {
                // Inserimento, se il caso delle imprese
                if (subappalto.Appaltata.IdImpresa.HasValue && !subappalto.Appaltata.Modificato ||
                    subappalto.Appaltata.IdImpresa.HasValue &&
                    ModificaImpresaNotifica(subappalto.Appaltata, transaction) ||
                    InserisciImpresaSubappaltoNotifica(subappalto.Appaltata, transaction))
                {
                    if (subappalto.Appaltante == null ||
                        subappalto.Appaltante.IdImpresa.HasValue && !subappalto.Appaltante.Modificato ||
                        subappalto.Appaltante.IdImpresa.HasValue &&
                        ModificaImpresaNotifica(subappalto.Appaltante, transaction) ||
                        InserisciImpresaSubappaltoNotifica(subappalto.Appaltante, transaction))
                    {
                        // Inserimento del subappalto
                        DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaSubappaltiInsert");

                        // Obbligatori
                        DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);
                        if (subappalto.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                            DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltata", DbType.Int32,
                                subappalto.Appaltata.IdImpresa.Value);
                        else
                            DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaAppaltata", DbType.Int32,
                                subappalto.Appaltata.IdImpresa.Value);

                        // Facoltativi
                        if (subappalto.Appaltante != null)
                        {
                            if (subappalto.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew)
                                DatabaseCemi.AddInParameter(comando, "@idImpresaAppaltante", DbType.Int32,
                                    subappalto.Appaltante.IdImpresa.Value);
                            else
                                DatabaseCemi.AddInParameter(comando, "@idCantieriImpresaAppaltante", DbType.Int32,
                                    subappalto.Appaltante.IdImpresa.Value);
                        }

                        if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            res = true;
                    }
                }
            }

            return res;
        }

        private bool InserisciImpresaSubappaltoNotifica(Impresa impresa, DbTransaction transaction)
        {
            bool res = false;

            if (!impresa.IdImpresa.HasValue && !string.IsNullOrEmpty(impresa.RagioneSociale))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseInsertPerCpt");

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);

                // Facoltativi
                if (!string.IsNullOrEmpty(impresa.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                if (!string.IsNullOrEmpty(impresa.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                if (!string.IsNullOrEmpty(impresa.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                if (!string.IsNullOrEmpty(impresa.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                if (!string.IsNullOrEmpty(impresa.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                if (!string.IsNullOrEmpty(impresa.TipoAttivita))
                    DatabaseCemi.AddInParameter(comando, "@tipoAttivita", DbType.String, impresa.TipoAttivita);
                if (!string.IsNullOrEmpty(impresa.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, impresa.Telefono);
                if (!string.IsNullOrEmpty(impresa.Fax))
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, impresa.Fax);
                if (!string.IsNullOrEmpty(impresa.PersonaRiferimento))
                    DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                        impresa.PersonaRiferimento);
                DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean, impresa.LavoratoreAutonomo);

                decimal idImpresa = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);

                if (idImpresa > 0)
                {
                    impresa.IdImpresa = decimal.ToInt32(idImpresa);
                    res = true;
                }
            }

            return res;
        }

        private bool ModificaImpresaNotifica(Impresa impresa, DbTransaction transaction)
        {
            bool res = false;

            if (impresa.IdImpresa.HasValue && !string.IsNullOrEmpty(impresa.RagioneSociale))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriImpreseUpdatePerCpt");

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);

                // Facoltativi
                if (!string.IsNullOrEmpty(impresa.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                if (!string.IsNullOrEmpty(impresa.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                if (!string.IsNullOrEmpty(impresa.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                if (!string.IsNullOrEmpty(impresa.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                if (!string.IsNullOrEmpty(impresa.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                if (!string.IsNullOrEmpty(impresa.TipoAttivita))
                    DatabaseCemi.AddInParameter(comando, "@tipoAttivita", DbType.String, impresa.TipoAttivita);
                if (!string.IsNullOrEmpty(impresa.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, impresa.Telefono);
                if (!string.IsNullOrEmpty(impresa.Fax))
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, impresa.Fax);
                if (!string.IsNullOrEmpty(impresa.PersonaRiferimento))
                    DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                        impresa.PersonaRiferimento);
                DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean, impresa.LavoratoreAutonomo);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Indirizzi

        private bool InserisciIndirizziNotifica(IEnumerable<Indirizzo> indirizzi, int idNotifica,
            DbTransaction transaction)
        {
            bool res = true;

            if (indirizzi != null)
            {
                foreach (Indirizzo indirizzo in indirizzi)
                {
                    if (!indirizzo.IdIndirizzo.HasValue && !InserisciIndirizzo(indirizzo, transaction))
                    {
                        res = false;
                        break;
                    }
                    if (!InserisciIndirizzoNotifica(indirizzo, idNotifica, transaction))
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        private bool InserisciIndirizzo(Indirizzo indirizzo, DbTransaction transaction)
        {
            bool res = false;

            if (!indirizzo.IdIndirizzo.HasValue && !string.IsNullOrEmpty(indirizzo.Indirizzo1))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptIndirizziInsert");

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzo.Indirizzo1);

                // Facoltativi
                if (!string.IsNullOrEmpty(indirizzo.Civico))
                    DatabaseCemi.AddInParameter(comando, "@civico", DbType.String, indirizzo.Civico);
                if (!string.IsNullOrEmpty(indirizzo.InfoAggiuntiva))
                    DatabaseCemi.AddInParameter(comando, "@infoAggiuntiva", DbType.String, indirizzo.InfoAggiuntiva);
                if (!string.IsNullOrEmpty(indirizzo.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, indirizzo.Comune);
                if (!string.IsNullOrEmpty(indirizzo.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, indirizzo.Provincia);
                if (!string.IsNullOrEmpty(indirizzo.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, indirizzo.Cap);
                if (indirizzo.Latitudine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@latitudine", DbType.Double, indirizzo.Latitudine.Value);
                if (indirizzo.Longitudine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@longitudine", DbType.Double, indirizzo.Longitudine.Value);
                if (indirizzo.DataInizioLavori.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataInizioLavori", DbType.DateTime,
                        indirizzo.DataInizioLavori.Value);
                }
                if (!string.IsNullOrEmpty(indirizzo.DescrizioneDurata))
                {
                    DatabaseCemi.AddInParameter(comando, "@descrizioneDurata", DbType.String,
                        indirizzo.DescrizioneDurata);
                }
                if (indirizzo.NumeroDurata.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@numeroDurata", DbType.Int32, indirizzo.NumeroDurata.Value);
                }
                if (indirizzo.NumeroMassimoLavoratori.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@numeroMassimoLavoratori", DbType.Int32,
                        indirizzo.NumeroMassimoLavoratori.Value);
                }

                decimal idIndirizzo = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);

                if (idIndirizzo > 0)
                {
                    indirizzo.IdIndirizzo = decimal.ToInt32(idIndirizzo);
                    res = true;
                }
            }

            return res;
        }

        private bool InserisciIndirizzoNotifica(Indirizzo indirizzo, int idNotifica, DbTransaction transaction)
        {
            bool res = false;

            if (indirizzo.IdIndirizzo.HasValue && indirizzo.IdIndirizzo.Value > 0 && idNotifica > 0)
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaIndirizziInsert");

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@idIndirizzo", DbType.Int32, indirizzo.IdIndirizzo.Value);
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Persone

        private bool InserisciPersoneNotifica(Notifica notifica, DbTransaction transaction)
        {
            bool res = true;

            if (notifica.CoordinatoreSicurezzaProgettazione != null &&
                !notifica.CoordinatoreSicurezzaProgettazione.IdPersona.HasValue)
                res = InserisciPersona(notifica.CoordinatoreSicurezzaProgettazione, transaction);

            if (res && notifica.CoordinatoreSicurezzaRealizzazione != null &&
                !notifica.CoordinatoreSicurezzaRealizzazione.IdPersona.HasValue)
                res = InserisciPersona(notifica.CoordinatoreSicurezzaRealizzazione, transaction);

            if (res && notifica.DirettoreLavori != null && !notifica.DirettoreLavori.IdPersona.HasValue)
                res = InserisciPersona(notifica.DirettoreLavori, transaction);

            return res;
        }

        private bool InserisciPersona(Persona persona, DbTransaction transaction)
        {
            bool res = false;

            if (!persona.IdPersona.HasValue && !string.IsNullOrEmpty(persona.Nominativo))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptPersoneInsert");

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@nominativo", DbType.String, persona.Nominativo);

                // Facoltativi
                if (!string.IsNullOrEmpty(persona.RagioneSociale))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, persona.RagioneSociale);
                if (!string.IsNullOrEmpty(persona.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, persona.Indirizzo);
                if (!string.IsNullOrEmpty(persona.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, persona.Telefono);
                if (!string.IsNullOrEmpty(persona.Fax))
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, persona.Fax);

                decimal idPersona = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);

                if (idPersona > 0)
                {
                    persona.IdPersona = decimal.ToInt32(idPersona);
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Committente

        private bool InserisciCommittenteNotifica(Notifica notifica, DbTransaction transaction)
        {
            bool res = false;

            if (!notifica.Committente.IdCommittente.HasValue &&
                !string.IsNullOrEmpty(notifica.Committente.RagioneSociale))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriCommittentiInsertPerCpt");
                Committente committente = notifica.Committente;

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, committente.RagioneSociale);

                // Facoltativi
                if (!string.IsNullOrEmpty(committente.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                if (!string.IsNullOrEmpty(committente.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                if (!string.IsNullOrEmpty(committente.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                if (!string.IsNullOrEmpty(committente.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                if (!string.IsNullOrEmpty(committente.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, committente.PartitaIva);
                if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, committente.CodiceFiscale);
                if (!string.IsNullOrEmpty(committente.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, committente.Telefono);
                if (!string.IsNullOrEmpty(committente.Fax))
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, committente.Fax);
                if (!string.IsNullOrEmpty(committente.PersonaRiferimento))
                    DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                        committente.PersonaRiferimento);

                decimal idCommittente = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);

                if (idCommittente > 0)
                {
                    committente.IdCommittente = decimal.ToInt32(idCommittente);
                    res = true;
                }
            }

            return res;
        }

        private bool ModificaCommittenteNotifica(Notifica notifica, DbTransaction transaction)
        {
            bool res = false;

            if (notifica.Committente.IdCommittente.HasValue &&
                !string.IsNullOrEmpty(notifica.Committente.RagioneSociale))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CantieriCommittentiUpdatePerCpt");
                Committente committente = notifica.Committente;

                // Obbligatori
                DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32, committente.IdCommittente.Value);
                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, committente.RagioneSociale);

                // Facoltativi
                if (!string.IsNullOrEmpty(committente.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, committente.Indirizzo);
                if (!string.IsNullOrEmpty(committente.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, committente.Comune);
                if (!string.IsNullOrEmpty(committente.Provincia))
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, committente.Provincia);
                if (!string.IsNullOrEmpty(committente.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, committente.Cap);
                if (!string.IsNullOrEmpty(committente.PartitaIva))
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, committente.PartitaIva);
                if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, committente.CodiceFiscale);
                if (!string.IsNullOrEmpty(committente.Telefono))
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, committente.Telefono);
                if (!string.IsNullOrEmpty(committente.Fax))
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, committente.Fax);
                if (!string.IsNullOrEmpty(committente.PersonaRiferimento))
                    DatabaseCemi.AddInParameter(comando, "@personaRiferimento", DbType.String,
                        committente.PersonaRiferimento);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #endregion

        #endregion

        #region Notifiche telematiche

        public bool InserisciNotifica(NotificaTelematica notifica)
        {
            bool res = false;

            if (notifica.Indirizzi != null && notifica.Indirizzi.Count > 0 && notifica.Committente != null
                && !string.IsNullOrEmpty(notifica.NaturaOpera))
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();
                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            if (InserisciPersoneNotifica(notifica, transaction))
                            {
                                if (!notifica.Committente.IdCommittenteAnagrafica.HasValue)
                                {
                                    InsertCommittenteTelematicheAnagrafica(notifica.Committente, transaction);
                                }

                                if (InserisciCommittenteNotifica(notifica.Committente, transaction))
                                {
                                    // Inserimento dati notifica
                                    DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheInsert");

                                    // Obbligatori
                                    DatabaseCemi.AddInParameter(comando, "@guid", DbType.Guid, notifica.Guid);
                                    DatabaseCemi.AddInParameter(comando, "@idArea", DbType.Int16, notifica.Area.IdArea);
                                    DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, notifica.Data);
                                    //if (notifica.Committente.IdCommittente.HasValue)
                                    //{
                                    //    DatabaseCemi.AddInParameter(comando, "@idCommittente", DbType.Int32,
                                    //                                notifica.Committente.IdCommittente.Value);
                                    //}
                                    DatabaseCemi.AddInParameter(comando, "@idCommittenteTelematiche", DbType.Int32,
                                        notifica.Committente.IdCommittenteTelematiche.Value);
                                    DatabaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String,
                                        notifica.NaturaOpera);
                                    if (!string.IsNullOrEmpty(notifica.Note))
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@note", DbType.String,
                                            notifica.Note);
                                    }
                                    if (notifica.OperaPubblica.HasValue)
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@operaPubblica", DbType.Boolean,
                                            notifica.OperaPubblica.Value);
                                    }

                                    // Facoltativi
                                    if (notifica.IdNotificaPadre.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@idNotificaPadre", DbType.Int32,
                                            notifica.IdNotificaPadre.Value);
                                    if (!string.IsNullOrEmpty(notifica.NumeroAppalto))
                                        DatabaseCemi.AddInParameter(comando, "@numeroAppalto", DbType.String,
                                            notifica.NumeroAppalto);

                                    // Persone
                                    if (notifica.CoordinatoreSicurezzaProgettazione != null)
                                        DatabaseCemi.AddInParameter(comando, "@idCptPersonaCoordinatoreProgettazione",
                                            DbType.Int32,
                                            notifica.CoordinatoreSicurezzaProgettazione.IdPersona.Value);
                                    if (notifica.CoordinatoreSicurezzaRealizzazione != null)
                                        DatabaseCemi.AddInParameter(comando, "@idCptPersonaCoordinatoreRealizzazione",
                                            DbType.Int32,
                                            notifica.CoordinatoreSicurezzaRealizzazione.IdPersona.Value);
                                    if (notifica.DirettoreLavori != null)
                                        DatabaseCemi.AddInParameter(comando, "@idCptPersonaDirettoreLavori",
                                            DbType.Int32,
                                            notifica.DirettoreLavori.IdPersona.Value);

                                    if (notifica.DataInizioLavori.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@dataInizioLavori", DbType.DateTime,
                                            notifica.DataInizioLavori.Value);
                                    if (notifica.DataFineLavori.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@dataFineLavori", DbType.DateTime,
                                            notifica.DataFineLavori.Value);
                                    if (notifica.Durata.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@durataLavori", DbType.Int32,
                                            notifica.Durata.Value);
                                    if (notifica.NumeroGiorniUomo.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@giorniUomoLavori", DbType.Int32,
                                            notifica.NumeroGiorniUomo.Value);
                                    if (notifica.NumeroMassimoLavoratori.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@numeroMassimoLavoratori", DbType.Int32,
                                            notifica.NumeroMassimoLavoratori.Value);
                                    if (notifica.NumeroImprese.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@numeroImprese", DbType.Int32,
                                            notifica.NumeroImprese.Value);
                                    if (notifica.NumeroLavoratoriAutonomi.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@numeroLavoratoriAutonomi", DbType.Int32,
                                            notifica.NumeroLavoratoriAutonomi.Value);
                                    if (notifica.AmmontareComplessivo.HasValue)
                                        DatabaseCemi.AddInParameter(comando, "@ammontareComplessivo", DbType.Decimal,
                                            notifica.AmmontareComplessivo.Value);

                                    DatabaseCemi.AddInParameter(comando, "@utente", DbType.String, notifica.Utente);

                                    if (notifica.ResponsabileCommittente)
                                        DatabaseCemi.AddInParameter(comando, "@responsabileCommittente", DbType.Boolean,
                                            notifica.ResponsabileCommittente);

                                    DatabaseCemi.AddInParameter(comando, "@idUtenteTelematiche", DbType.Guid,
                                        notifica.IdUtenteTelematiche);
                                    DatabaseCemi.AddInParameter(comando, "@coordinatoreProgettazioneNonNominato",
                                        DbType.Boolean,
                                        notifica.CoordinatoreProgettazioneNonNominato);
                                    DatabaseCemi.AddInParameter(comando, "@coordinatoreEsecuzioneNonNominato",
                                        DbType.Boolean,
                                        notifica.CoordinatoreEsecuzioneNonNominato);

                                    if (!string.IsNullOrEmpty(notifica.ProtocolloRegione))
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@protocolloRegione",
                                            DbType.String,
                                            notifica.ProtocolloRegione);
                                    }

                                    if (notifica.DataPrimoInserimento.HasValue)
                                    {
                                        DatabaseCemi.AddInParameter(comando, "@dataPrimoInserimento",
                                            DbType.DateTime,
                                            notifica.DataPrimoInserimento.Value);
                                    }

                                    decimal idNotificaTemp = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);
                                    if (idNotificaTemp > 0)
                                    {
                                        notifica.IdNotifica = decimal.ToInt32(idNotificaTemp);

                                        // Inserimento dati indirizzi
                                        if (
                                            InserisciIndirizziNotifica(notifica.Indirizzi, notifica.IdNotifica.Value,
                                                transaction))
                                        {
                                            // SISTEMO LE IMPRESE IN MODO DA NON DOVERLE INSERIRE PIU' VOLTE
                                            if (notifica.ImpreseAffidatarie != null)
                                            {
                                                // Sistemo le imprese affidatarie
                                                for (int i = 0; i < notifica.ImpreseAffidatarie.Count; i++)
                                                {
                                                    for (int k = 0; k < notifica.ImpreseEsecutrici.Count; k++)
                                                    {
                                                        if (notifica.ImpreseAffidatarie[i].ImpresaSelezionata != null
                                                            &&
                                                            notifica.ImpreseEsecutrici[k].AppaltataDa != null
                                                            &&
                                                            (notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                 .IdTemporaneo != Guid.Empty
                                                             &&
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa.IdTemporaneo !=
                                                             Guid.Empty
                                                             &&
                                                             notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                 .IdTemporaneo ==
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa.IdTemporaneo
                                                             ||
                                                             notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                 .IdImpresaTelematica.HasValue
                                                             &&
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                 .IdImpresaTelematica.HasValue
                                                             &&
                                                             notifica.ImpreseAffidatarie[i].ImpresaSelezionata
                                                                 .IdImpresaTelematica ==
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                 .IdImpresaTelematica
                                                            )
                                                        )
                                                        {
                                                            notifica.ImpreseEsecutrici[k].AppaltataDa =
                                                                notifica.ImpreseAffidatarie[i].ImpresaSelezionata;
                                                        }
                                                    }
                                                }

                                                // Sistemo le esecutrici
                                                for (int i = 0; i < notifica.ImpreseEsecutrici.Count; i++)
                                                {
                                                    for (int k = i + 1; k < notifica.ImpreseEsecutrici.Count; k++)
                                                    {
                                                        if (notifica.ImpreseEsecutrici[i].ImpresaSelezionata != null
                                                            &&
                                                            notifica.ImpreseEsecutrici[k].AppaltataDa != null
                                                            &&
                                                            (notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                 .IdTemporaneo != Guid.Empty
                                                             &&
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa.IdTemporaneo !=
                                                             Guid.Empty
                                                             &&
                                                             notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                 .IdTemporaneo ==
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa.IdTemporaneo
                                                             ||
                                                             notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                 .IdImpresaTelematica.HasValue
                                                             &&
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                 .IdImpresaTelematica.HasValue
                                                             &&
                                                             notifica.ImpreseEsecutrici[i].ImpresaSelezionata
                                                                 .IdImpresaTelematica ==
                                                             notifica.ImpreseEsecutrici[k].AppaltataDa
                                                                 .IdImpresaTelematica
                                                            )
                                                        )
                                                        {
                                                            notifica.ImpreseEsecutrici[k].AppaltataDa =
                                                                notifica.ImpreseEsecutrici[i].ImpresaSelezionata;
                                                        }
                                                    }
                                                }
                                            }
                                            foreach (SubappaltoNotificheTelematiche sub in notifica.ImpreseAffidatarie)
                                            {
                                                sub.IdSubappalto = null;

                                                if (sub.ImpresaSelezionata != null)
                                                {
                                                    sub.ImpresaSelezionata.IdImpresaTelematica = null;
                                                }
                                                if (sub.AppaltataDa != null)
                                                {
                                                    sub.AppaltataDa.IdImpresaTelematica = null;
                                                }
                                            }
                                            foreach (SubappaltoNotificheTelematiche sub in notifica.ImpreseEsecutrici)
                                            {
                                                sub.IdSubappalto = null;

                                                if (sub.ImpresaSelezionata != null)
                                                {
                                                    sub.ImpresaSelezionata.IdImpresaTelematica = null;
                                                }
                                                if (sub.AppaltataDa != null)
                                                {
                                                    sub.AppaltataDa.IdImpresaTelematica = null;
                                                }
                                            }
                                            // FINE

                                            if (
                                                InserisciSubappaltiNotifica(notifica.ImpreseAffidatarie,
                                                    notifica.IdNotifica.Value, transaction)
                                                &&
                                                InserisciSubappaltiNotifica(notifica.ImpreseEsecutrici,
                                                    notifica.IdNotifica.Value, transaction))
                                            {
                                                //if (notifica.IdNotificaTemporanea.HasValue)
                                                //{
                                                //    DeleteNotificaTemporanea(notifica.IdNotificaTemporanea.Value, transaction);
                                                //}

                                                res = true;
                                            }
                                            else
                                            {
                                                throw new Exception("Errore durante l'inserimento di un subappalto");
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception("Errore durante l'inserimento di un indirizzo");
                                        }
                                    }
                                    else
                                    {
                                        if (idNotificaTemp == -2)
                                        {
                                            throw new NotificaGiaInseritaException();
                                        }
                                        else
                                        {
                                            throw new Exception("Inserimento della notifica non andato a buon fine");
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("Non � andata a buon fine l'inserimento del committente");
                                }
                            }
                            else
                            {
                                throw new Exception("Non � andata a buon fine l'inserimento delle persone");
                            }
                        }
                        finally
                        {
                            if (res)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                                notifica.IdNotifica = null;
                            }
                        }
                    }
                    connection.Close();
                }
            }

            return res;
        }

        private bool InserisciCommittenteNotifica(CommittenteNotificheTelematiche committente,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaCommittentiInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipologiaCommittente", DbType.Int32,
                    committente.TipologiaCommittente.IdTipologiaCommittente);
                if (!string.IsNullOrEmpty(committente.PersonaCognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCognome", DbType.String, committente.PersonaCognome);
                }
                if (!string.IsNullOrEmpty(committente.PersonaNome))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaNome", DbType.String, committente.PersonaNome);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCodiceFiscale", DbType.String,
                        committente.PersonaCodiceFiscale);
                }
                if (!string.IsNullOrEmpty(committente.PersonaIndirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaIndirizzo", DbType.String,
                        committente.PersonaIndirizzo);
                }
                if (!string.IsNullOrEmpty(committente.PersonaComune))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaComune", DbType.String, committente.PersonaComune);
                }
                if (!string.IsNullOrEmpty(committente.PersonaProvincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaProvincia", DbType.String,
                        committente.PersonaProvincia);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCap))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCap", DbType.String, committente.PersonaCap);
                }
                if (!string.IsNullOrEmpty(committente.PersonaTelefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaTelefono", DbType.String,
                        committente.PersonaTelefono);
                }
                if (!string.IsNullOrEmpty(committente.PersonaFax))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaFax", DbType.String, committente.PersonaFax);
                }
                if (!string.IsNullOrEmpty(committente.PersonaCellulare))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaCellulare", DbType.String,
                        committente.PersonaCellulare);
                }
                if (!string.IsNullOrEmpty(committente.PersonaEmail))
                {
                    DatabaseCemi.AddInParameter(comando, "@personaEmail", DbType.String, committente.PersonaEmail);
                }
                if (!string.IsNullOrEmpty(committente.RagioneSociale))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteRagioneSociale", DbType.String,
                        committente.RagioneSociale);
                }
                if (!string.IsNullOrEmpty(committente.PartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@entePartitaIva", DbType.String, committente.PartitaIva);
                }
                if (!string.IsNullOrEmpty(committente.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCodiceFiscale", DbType.String,
                        committente.CodiceFiscale);
                }
                if (!string.IsNullOrEmpty(committente.Indirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteIndirizzo", DbType.String, committente.Indirizzo);
                }
                if (!string.IsNullOrEmpty(committente.Comune))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteComune", DbType.String, committente.Comune);
                }
                if (!string.IsNullOrEmpty(committente.Provincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteProvincia", DbType.String, committente.Provincia);
                }
                if (!string.IsNullOrEmpty(committente.Cap))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteCap", DbType.String, committente.Cap);
                }
                if (!string.IsNullOrEmpty(committente.Telefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteTelefono", DbType.String, committente.Telefono);
                }
                if (!string.IsNullOrEmpty(committente.Fax))
                {
                    DatabaseCemi.AddInParameter(comando, "@enteFax", DbType.String, committente.Fax);
                }
                DatabaseCemi.AddInParameter(comando, "@idCommittenteAnagrafica", DbType.Int32,
                    committente.IdCommittenteAnagrafica);

                DatabaseCemi.AddOutParameter(comando, "@idCommittente", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando, transaction);
                int idCommittente = (int) DatabaseCemi.GetParameterValue(comando, "@idCommittente");
                if (idCommittente > 0)
                {
                    committente.IdCommittenteTelematiche = idCommittente;
                    res = true;
                }
            }

            return res;
        }

        private bool InserisciSubappaltiNotifica(SubappaltoNotificheTelematicheCollection subappalti, int idNotifica,
            DbTransaction transaction)
        {
            bool res = true;

            if (subappalti != null)
            {
                foreach (SubappaltoNotificheTelematiche subappalto in subappalti)
                {
                    if (!InserisciSubappaltoNotifica(subappalto, idNotifica, transaction))
                    {
                        res = false;
                        break;
                    }
                }
            }

            return res;
        }

        private bool InserisciSubappaltoNotifica(SubappaltoNotificheTelematiche subappalto, int idNotifica,
            DbTransaction transaction)
        {
            bool res = false;

            if (!subappalto.IdSubappalto.HasValue)
            {
                // Inserimento, se il caso delle imprese
                if (!subappalto.ImpresaSelezionata.IdImpresaAnagrafica.HasValue
                    && !subappalto.ImpresaSelezionata.IdImpresa.HasValue)
                {
                    InsertImpresaAnagrafica(subappalto.ImpresaSelezionata, transaction);
                }
                if (subappalto.AppaltataDa != null
                    && !subappalto.AppaltataDa.IdImpresaTelematica.HasValue
                    && !subappalto.AppaltataDa.IdImpresa.HasValue)
                {
                    InsertImpresaAnagrafica(subappalto.AppaltataDa, transaction);
                }

                if (subappalto.ImpresaSelezionata.IdImpresaTelematica.HasValue ||
                    InserisciImpresaSubappaltoNotifica(subappalto.ImpresaSelezionata, transaction))
                {
                    if (subappalto.AppaltataDa == null
                        ||
                        subappalto.AppaltataDa.IdImpresaTelematica.HasValue
                        ||
                        InserisciImpresaSubappaltoNotifica(subappalto.AppaltataDa, transaction))
                    {
                        // Inserimento del subappalto
                        DbCommand comando =
                            DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaSubappaltiInsert");

                        DatabaseCemi.AddInParameter(comando, "@idCptNotifica", DbType.Int32, idNotifica);
                        DatabaseCemi.AddInParameter(comando, "@idCptNotificaTelematicaImpresaSelezionata", DbType.Int32,
                            subappalto.ImpresaSelezionata.IdImpresaTelematica);
                        DatabaseCemi.AddInParameter(comando, "@affidatarie", DbType.Boolean, subappalto.Affidatarie);

                        // Facoltativi
                        if (subappalto.AppaltataDa != null)
                        {
                            DatabaseCemi.AddInParameter(comando, "@idCptNotificaTelematicaImpresaAppaltataDa",
                                DbType.Int32, subappalto.AppaltataDa.IdImpresaTelematica.Value);
                        }

                        if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                            res = true;
                    }
                }
            }

            return res;
        }

        private bool InserisciImpresaSubappaltoNotifica(ImpresaNotificheTelematiche impresa, DbTransaction transaction)
        {
            bool res = false;

            if (!impresa.IdImpresaTelematica.HasValue && !string.IsNullOrEmpty(impresa.RagioneSociale))
            {
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificaTelematicaImpreseInsert");

                DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, impresa.RagioneSociale);
                DatabaseCemi.AddInParameter(comando, "@lavoratoreAutonomo", DbType.Boolean, impresa.LavoratoreAutonomo);
                if (!string.IsNullOrEmpty(impresa.PartitaIva))
                {
                    DatabaseCemi.AddInParameter(comando, "@partitaIva", DbType.String, impresa.PartitaIva);
                }
                if (!string.IsNullOrEmpty(impresa.CodiceFiscale))
                {
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, impresa.CodiceFiscale);
                }
                if (!string.IsNullOrEmpty(impresa.AttivitaPrevalente))
                {
                    DatabaseCemi.AddInParameter(comando, "@attivitaPrevalente", DbType.String,
                        impresa.AttivitaPrevalente);
                }
                if (impresa.IdImpresa.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, impresa.IdImpresa.Value);
                }
                if (impresa.IdImpresaAnagrafica.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idCptNotificaTelematicaImpresaAnagrafica", DbType.Int32,
                        impresa.IdImpresaAnagrafica.Value);
                }
                if (!string.IsNullOrEmpty(impresa.IdCassaEdile))
                {
                    DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, impresa.IdCassaEdile);
                }
                if (!string.IsNullOrEmpty(impresa.MatricolaINAIL))
                {
                    DatabaseCemi.AddInParameter(comando, "@matricolaINAIL", DbType.String, impresa.MatricolaINAIL);
                }
                if (!string.IsNullOrEmpty(impresa.MatricolaINPS))
                {
                    DatabaseCemi.AddInParameter(comando, "@matricolaINPS", DbType.String, impresa.MatricolaINPS);
                }
                if (!string.IsNullOrEmpty(impresa.MatricolaCCIAA))
                {
                    DatabaseCemi.AddInParameter(comando, "@matricolaCCIAA", DbType.String, impresa.MatricolaCCIAA);
                }
                if (!string.IsNullOrEmpty(impresa.Indirizzo))
                {
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, impresa.Indirizzo);
                }
                if (!string.IsNullOrEmpty(impresa.Comune))
                {
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, impresa.Comune);
                }
                if (!string.IsNullOrEmpty(impresa.Provincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, impresa.Provincia);
                }
                if (!string.IsNullOrEmpty(impresa.Cap))
                {
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, impresa.Cap);
                }
                if (!string.IsNullOrEmpty(impresa.Telefono))
                {
                    DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, impresa.Telefono);
                }
                if (!string.IsNullOrEmpty(impresa.Fax))
                {
                    DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, impresa.Fax);
                }
                DatabaseCemi.AddOutParameter(comando, "@idImpresaTelematica", DbType.Int32, 4);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    impresa.IdImpresaTelematica =
                        (int) DatabaseCemi.GetParameterValue(comando, "@idImpresaTelematica");
                    res = true;
                }
            }

            return res;
        }

        public NotificaTelematica GetNotificaTelematica(int idNotifica)
        {
            NotificaTelematica notifica;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheSelectSingola")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotifica);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    notifica = TrasformaReaderInNotificaTelematica(reader);
                }
            }

            return notifica;
        }

        public NotificaTelematica GetNotificaTelematicaUltimaVersione(int idNotificaPadre)
        {
            NotificaTelematica notifica;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheTelematicheSelectSingolaUltimaVersione")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idNotificaPadre", DbType.Int32, idNotificaPadre);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    notifica = TrasformaReaderInNotificaTelematica(reader);
                }
            }

            return notifica;
        }

        private static NotificaTelematica TrasformaReaderInNotificaTelematica(IDataReader reader)
        {
            NotificaTelematica notifica = new NotificaTelematica();
            // Va memorizzato da qualche parte
            //DateTime? dataNotificaPadre = null;
            int tempOrdinal;

            // Notifica
            reader.Read();

            // Id
            notifica.IdNotifica = reader.GetInt32(reader.GetOrdinal("idCptNotifica"));

            // Utente
            notifica.IdUtenteTelematiche = (Guid) reader["idUtenteTelematiche"];

            // Notifica padre
            tempOrdinal = reader.GetOrdinal("idCptNotificaPadre");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.IdNotificaPadre = reader.GetInt32(tempOrdinal);

            tempOrdinal = reader.GetOrdinal("notificaRiferimento");
            if (!reader.IsDBNull(tempOrdinal))
            {
                notifica.IdNotificaRiferimento = reader.GetInt32(tempOrdinal);
            }

            // Area
            notifica.Area = new Area();
            notifica.Area.IdArea = (short) reader["idCptArea"];
            notifica.Area.Descrizione = (string) reader["areaDescrizione"];

            // Data
            notifica.Data = reader.GetDateTime(reader.GetOrdinal("Data"));

            // Natura dell'opera
            notifica.NaturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));

            // Note
            tempOrdinal = reader.GetOrdinal("note");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.Note = reader.GetString(tempOrdinal);

            // Tipo Opera
            tempOrdinal = reader.GetOrdinal("operaPubblica");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.OperaPubblica = reader.GetBoolean(tempOrdinal);

            // Numero appalto
            tempOrdinal = reader.GetOrdinal("numeroAppalto");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroAppalto = reader.GetString(tempOrdinal);

            // Committente
            CommittenteNotificheTelematiche committente = new CommittenteNotificheTelematiche();
            committente.IdCommittenteTelematiche = reader.GetOrdinal("idCptNotificaTelematicaCommittente");
            committente.TipologiaCommittente = new TipologiaCommittente();
            committente.TipologiaCommittente.IdTipologiaCommittente = (int) reader["idCptTipologiaCommittente"];
            committente.TipologiaCommittente.Descrizione = (string) reader["committenteTipologiaDescrizione"];
            if (!Convert.IsDBNull(reader["committentePersonaCognome"]))
            {
                committente.PersonaCognome = (string) reader["committentePersonaCognome"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaNome"]))
            {
                committente.PersonaNome = (string) reader["committentePersonaNome"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaCodiceFiscale"]))
            {
                committente.PersonaCodiceFiscale = (string) reader["committentePersonaCodiceFiscale"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaIndirizzo"]))
            {
                committente.PersonaIndirizzo = (string) reader["committentePersonaIndirizzo"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaComune"]))
            {
                committente.PersonaComune = (string) reader["committentePersonaComune"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaProvincia"]))
            {
                committente.PersonaProvincia = (string) reader["committentePersonaProvincia"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaCap"]))
            {
                committente.PersonaCap = (string) reader["committentePersonaCap"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaTelefono"]))
            {
                committente.PersonaTelefono = (string) reader["committentePersonaTelefono"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaFax"]))
            {
                committente.PersonaFax = (string) reader["committentePersonaFax"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaCellulare"]))
            {
                committente.PersonaCellulare = (string) reader["committentePersonaCellulare"];
            }
            if (!Convert.IsDBNull(reader["committentePersonaEmail"]))
            {
                committente.PersonaEmail = (string) reader["committentePersonaEmail"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteRagioneSociale"]))
            {
                committente.RagioneSociale = (string) reader["committenteEnteRagioneSociale"];
            }
            if (!Convert.IsDBNull(reader["committenteEntePartitaIva"]))
            {
                committente.PartitaIva = (string) reader["committenteEntePartitaIva"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteCodiceFiscale"]))
            {
                committente.CodiceFiscale = (string) reader["committenteEnteCodiceFiscale"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteIndirizzo"]))
            {
                committente.Indirizzo = (string) reader["committenteEnteIndirizzo"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteComune"]))
            {
                committente.Comune = (string) reader["committenteEnteComune"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteProvincia"]))
            {
                committente.Provincia = (string) reader["committenteEnteProvincia"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteCap"]))
            {
                committente.Cap = (string) reader["committenteEnteCap"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteTelefono"]))
            {
                committente.Telefono = (string) reader["committenteEnteTelefono"];
            }
            if (!Convert.IsDBNull(reader["committenteEnteFax"]))
            {
                committente.Fax = (string) reader["committenteEnteFax"];
            }
            if (!Convert.IsDBNull(reader["committenteIdAnagrafica"]))
            {
                committente.IdCommittenteAnagrafica = (int) reader["committenteIdAnagrafica"];
            }
            notifica.Committente = committente;

            // Coordinatore progettazione
            tempOrdinal = reader.GetOrdinal("idCptPersonaCoordinatoreProgettazione");
            if (!reader.IsDBNull(tempOrdinal))
            {
                PersonaNotificheTelematiche coordProgettazione = new PersonaNotificheTelematiche();
                coordProgettazione.IdPersona = reader.GetInt32(tempOrdinal);

                // Persona
                coordProgettazione.Nominativo = reader.GetString(reader.GetOrdinal("coordinatoreProgettazione"));
                if (!Convert.IsDBNull(reader["cognomeCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaCognome = (string) reader["cognomeCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["nomeCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaNome = (string) reader["nomeCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["codiceFiscaleCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaCodiceFiscale = (string) reader["codiceFiscaleCoordinatoreProgettazione"];
                }
                tempOrdinal = reader.GetOrdinal("indirizzoCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.Indirizzo = reader.GetString(tempOrdinal);
                if (!Convert.IsDBNull(reader["comuneCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaComune = (string) reader["comuneCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["provinciaCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaProvincia = (string) reader["provinciaCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["capCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaCap = (string) reader["capCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["cellulareCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaCellulare = (string) reader["cellulareCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["emailCoordinatoreProgettazione"]))
                {
                    coordProgettazione.PersonaEmail = (string) reader["emailCoordinatoreProgettazione"];
                }
                tempOrdinal = reader.GetOrdinal("telefonoCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.Telefono = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("faxCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.Fax = reader.GetString(tempOrdinal);

                // Ente
                tempOrdinal = reader.GetOrdinal("ragSocCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.RagioneSociale = reader.GetString(tempOrdinal);
                if (!Convert.IsDBNull(reader["entePartitaIvaCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EntePartitaIva = (string) reader["entePartitaIvaCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteCodiceFiscaleCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteCodiceFiscale =
                        (string) reader["enteCodiceFiscaleCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteIndirizzoCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteIndirizzo = (string) reader["enteIndirizzoCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteComuneCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteComune = (string) reader["enteComuneCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteProvinciaCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteProvincia = (string) reader["enteProvinciaCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteCapCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteCap = (string) reader["enteCapCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteTelefonoCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteTelefono = (string) reader["enteTelefonoCoordinatoreProgettazione"];
                }
                if (!Convert.IsDBNull(reader["enteFaxCoordinatoreProgettazione"]))
                {
                    coordProgettazione.EnteFax = (string) reader["enteFaxCoordinatoreProgettazione"];
                }

                notifica.CoordinatoreSicurezzaProgettazione = coordProgettazione;
            }

            // Coordinatore realizzazione
            tempOrdinal = reader.GetOrdinal("idCptPersonaCoordinatoreRealizzazione");
            if (!reader.IsDBNull(tempOrdinal))
            {
                PersonaNotificheTelematiche coordRealizzazione = new PersonaNotificheTelematiche();
                coordRealizzazione.IdPersona = reader.GetInt32(tempOrdinal);

                // Persona
                coordRealizzazione.Nominativo = reader.GetString(reader.GetOrdinal("coordinatoreRealizzazione"));
                if (!Convert.IsDBNull(reader["cognomeCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaCognome = (string) reader["cognomeCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["nomeCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaNome = (string) reader["nomeCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["codiceFiscaleCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaCodiceFiscale = (string) reader["codiceFiscaleCoordinatoreRealizzazione"];
                }
                tempOrdinal = reader.GetOrdinal("indirizzoCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.Indirizzo = reader.GetString(tempOrdinal);
                if (!Convert.IsDBNull(reader["comuneCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaComune = (string) reader["comuneCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["provinciaCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaProvincia = (string) reader["provinciaCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["capCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaCap = (string) reader["capCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["cellulareCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaCellulare = (string) reader["cellulareCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["emailCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.PersonaEmail = (string) reader["emailCoordinatoreRealizzazione"];
                }
                tempOrdinal = reader.GetOrdinal("telefonoCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.Telefono = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("faxCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.Fax = reader.GetString(tempOrdinal);

                // Ente
                tempOrdinal = reader.GetOrdinal("ragSocCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.RagioneSociale = reader.GetString(tempOrdinal);
                if (!Convert.IsDBNull(reader["entePartitaIvaCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EntePartitaIva = (string) reader["entePartitaIvaCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteCodiceFiscaleCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteCodiceFiscale =
                        (string) reader["enteCodiceFiscaleCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteIndirizzoCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteIndirizzo = (string) reader["enteIndirizzoCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteComuneCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteComune = (string) reader["enteComuneCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteProvinciaCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteProvincia = (string) reader["enteProvinciaCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteCapCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteCap = (string) reader["enteCapCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteTelefonoCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteTelefono = (string) reader["enteTelefonoCoordinatoreRealizzazione"];
                }
                if (!Convert.IsDBNull(reader["enteFaxCoordinatoreRealizzazione"]))
                {
                    coordRealizzazione.EnteFax = (string) reader["enteFaxCoordinatoreRealizzazione"];
                }

                notifica.CoordinatoreSicurezzaRealizzazione = coordRealizzazione;
            }

            // Direttore lavori
            tempOrdinal = reader.GetOrdinal("idCptPersonaDirettoreLavori");
            if (!reader.IsDBNull(tempOrdinal))
            {
                PersonaNotificheTelematiche direttoreLavori = new PersonaNotificheTelematiche();
                direttoreLavori.IdPersona = reader.GetInt32(tempOrdinal);

                // Persona
                direttoreLavori.Nominativo = reader.GetString(reader.GetOrdinal("direttoreLavori"));
                if (!Convert.IsDBNull(reader["cognomeDirettoreLavori"]))
                {
                    direttoreLavori.PersonaCognome = (string) reader["cognomeDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["nomeDirettoreLavori"]))
                {
                    direttoreLavori.PersonaNome = (string) reader["nomeDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["codiceFiscaleDirettoreLavori"]))
                {
                    direttoreLavori.PersonaCodiceFiscale = (string) reader["codiceFiscaleDirettoreLavori"];
                }
                tempOrdinal = reader.GetOrdinal("indirizzoDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.Indirizzo = reader.GetString(tempOrdinal);
                if (!Convert.IsDBNull(reader["comuneDirettoreLavori"]))
                {
                    direttoreLavori.PersonaComune = (string) reader["comuneDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["provinciaDirettoreLavori"]))
                {
                    direttoreLavori.PersonaProvincia = (string) reader["provinciaDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["capDirettoreLavori"]))
                {
                    direttoreLavori.PersonaCap = (string) reader["capDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["cellulareDirettoreLavori"]))
                {
                    direttoreLavori.PersonaCellulare = (string) reader["cellulareDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["emailDirettoreLavori"]))
                {
                    direttoreLavori.PersonaEmail = (string) reader["emailDirettoreLavori"];
                }
                tempOrdinal = reader.GetOrdinal("telefonoDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.Telefono = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("faxDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.Fax = reader.GetString(tempOrdinal);

                // Ente
                tempOrdinal = reader.GetOrdinal("ragSocDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.RagioneSociale = reader.GetString(tempOrdinal);
                if (!Convert.IsDBNull(reader["entePartitaIvaDirettoreLavori"]))
                {
                    direttoreLavori.EntePartitaIva = (string) reader["entePartitaIvaDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteCodiceFiscaleDirettoreLavori"]))
                {
                    direttoreLavori.EnteCodiceFiscale = (string) reader["enteCodiceFiscaleDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteIndirizzoDirettoreLavori"]))
                {
                    direttoreLavori.EnteIndirizzo = (string) reader["enteIndirizzoDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteComuneDirettoreLavori"]))
                {
                    direttoreLavori.EnteComune = (string) reader["enteComuneDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteProvinciaDirettoreLavori"]))
                {
                    direttoreLavori.EnteProvincia = (string) reader["enteProvinciaDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteCapDirettoreLavori"]))
                {
                    direttoreLavori.EnteCap = (string) reader["enteCapDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteTelefonoDirettoreLavori"]))
                {
                    direttoreLavori.EnteTelefono = (string) reader["enteTelefonoDirettoreLavori"];
                }
                if (!Convert.IsDBNull(reader["enteFaxDirettoreLavori"]))
                {
                    direttoreLavori.EnteFax = (string) reader["enteFaxDirettoreLavori"];
                }

                notifica.DirettoreLavori = direttoreLavori;
            }

            notifica.ResponsabileCommittente = reader.GetBoolean(reader.GetOrdinal("responsabileLavoriCommittente"));
            notifica.CoordinatoreProgettazioneNonNominato =
                reader.GetBoolean(reader.GetOrdinal("coordinatoreProgettazioneNonNominato"));
            notifica.CoordinatoreEsecuzioneNonNominato =
                reader.GetBoolean(reader.GetOrdinal("coordinatoreEsecuzioneNonNominato"));

            // Data inizio lavori
            tempOrdinal = reader.GetOrdinal("dataInizioLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataInizioLavori = reader.GetDateTime(tempOrdinal);

            // Data fine lavori
            tempOrdinal = reader.GetOrdinal("dataFineLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataFineLavori = reader.GetDateTime(tempOrdinal);

            // Durata
            tempOrdinal = reader.GetOrdinal("durataLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.Durata = reader.GetInt32(tempOrdinal);

            // Giorni uomo
            tempOrdinal = reader.GetOrdinal("giorniUomoLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroGiorniUomo = reader.GetInt32(tempOrdinal);

            // Numero lavoratori
            tempOrdinal = reader.GetOrdinal("numeroMassimoLavoratori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroMassimoLavoratori = reader.GetInt32(tempOrdinal);

            // Numero imprese
            tempOrdinal = reader.GetOrdinal("numeroPrevistoImprese");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroImprese = reader.GetInt32(tempOrdinal);

            // Numero autonomi
            tempOrdinal = reader.GetOrdinal("numeroLavoratoriAutonomi");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroLavoratoriAutonomi = reader.GetInt32(tempOrdinal);

            // Ammontare complessivo
            tempOrdinal = reader.GetOrdinal("ammontareComplessivo");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.AmmontareComplessivo = reader.GetDecimal(tempOrdinal);

            // Data inserimento
            notifica.DataInserimento = reader.GetDateTime(reader.GetOrdinal("dataInserimento"));

            // Utente
            notifica.Utente = reader.GetString(reader.GetOrdinal("utente"));

            // Annullata
            notifica.Annullata = reader.GetBoolean(reader.GetOrdinal("annullata"));

            // Data annullamento
            tempOrdinal = reader.GetOrdinal("dataAnnullamento");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataAnnullamento = reader.GetDateTime(tempOrdinal);

            // Utente annullamento
            tempOrdinal = reader.GetOrdinal("utenteAnnullamento");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.UtenteAnnullamento = reader.GetString(tempOrdinal);

            // Protocollo regione
            tempOrdinal = reader.GetOrdinal("protocolloRegione");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.ProtocolloRegione = reader.GetString(tempOrdinal);

            // Data primo inserimento
            tempOrdinal = reader.GetOrdinal("dataPrimoInserimento");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataPrimoInserimento = reader.GetDateTime(tempOrdinal);

            #region Indirizzi

            reader.NextResult();

            #region Indici per reader

            int indiceIndirizzoDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
            int indiceIndirizzoDescrizioneDurata = reader.GetOrdinal("descrizioneDurata");
            int indiceIndirizzoNumeroDurata = reader.GetOrdinal("numeroDurata");
            int indiceIndirizzoNumeroMassimoLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");

            #endregion

            while (reader.Read())
            {
                Indirizzo indirizzo = new Indirizzo();

                indirizzo.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("idCptIndirizzo"));

                indirizzo.Indirizzo1 = reader.GetString(reader.GetOrdinal("indirizzo"));

                tempOrdinal = reader.GetOrdinal("civico");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Civico = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("infoAggiuntiva");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.InfoAggiuntiva = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("comune");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Comune = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("provincia");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Provincia = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("cap");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Cap = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("latitudine");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Latitudine = reader.GetDecimal(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("longitudine");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Longitudine = reader.GetDecimal(tempOrdinal);

                if (!reader.IsDBNull(indiceIndirizzoDataInizioLavori))
                {
                    indirizzo.DataInizioLavori = reader.GetDateTime(indiceIndirizzoDataInizioLavori);
                }

                if (!reader.IsDBNull(indiceIndirizzoDescrizioneDurata))
                {
                    indirizzo.DescrizioneDurata = reader.GetString(indiceIndirizzoDescrizioneDurata);
                }

                if (!reader.IsDBNull(indiceIndirizzoNumeroDurata))
                {
                    indirizzo.NumeroDurata = reader.GetInt32(indiceIndirizzoNumeroDurata);
                }

                if (!reader.IsDBNull(indiceIndirizzoNumeroMassimoLavoratori))
                {
                    indirizzo.NumeroMassimoLavoratori = reader.GetInt32(indiceIndirizzoNumeroMassimoLavoratori);
                }

                notifica.Indirizzi.Add(indirizzo);
            }

            #endregion

            #region Subappalti

            reader.NextResult();

            notifica.ImpreseAffidatarie = new SubappaltoNotificheTelematicheCollection();
            notifica.ImpreseEsecutrici = new SubappaltoNotificheTelematicheCollection();
            while (reader.Read())
            {
                SubappaltoNotificheTelematiche subappalto = new SubappaltoNotificheTelematiche();

                subappalto.IdSubappalto = reader.GetInt32(reader.GetOrdinal("idCptNotificaTelematicaSubappalto"));

                // Impresa selezionata

                ImpresaNotificheTelematiche impresaSelezionata = new ImpresaNotificheTelematiche();
                impresaSelezionata.IdImpresaTelematica = (int) reader["idCptNotificaTelematicaImpresaSelezionata"];
                impresaSelezionata.RagioneSociale = (string) reader["selezionataRagioneSociale"];
                impresaSelezionata.LavoratoreAutonomo = (bool) reader["selezionataLavoratoreAutonomo"];
                if (!Convert.IsDBNull(reader["selezionataPartitaIva"]))
                {
                    impresaSelezionata.PartitaIva = (string) reader["selezionataPartitaIva"];
                }
                if (!Convert.IsDBNull(reader["selezionataCodiceFiscale"]))
                {
                    impresaSelezionata.CodiceFiscale = (string) reader["selezionataCodiceFiscale"];
                }
                if (!Convert.IsDBNull(reader["selezionataAttivitaPrevalente"]))
                {
                    impresaSelezionata.AttivitaPrevalente = (string) reader["selezionataAttivitaPrevalente"];
                }
                if (!Convert.IsDBNull(reader["selezionataIdImpresa"]))
                {
                    impresaSelezionata.IdImpresa = (int) reader["selezionataIdImpresa"];
                }
                if (!Convert.IsDBNull(reader["selezionataIdCptNotificaTelematicaImpresaAnagrafica"]))
                {
                    impresaSelezionata.IdImpresaAnagrafica =
                        (int) reader["selezionataIdCptNotificaTelematicaImpresaAnagrafica"];
                }
                if (!Convert.IsDBNull(reader["selezionataIdCassaEdile"]))
                {
                    impresaSelezionata.IdCassaEdile = (string) reader["selezionataIdCassaEdile"];
                }
                if (!Convert.IsDBNull(reader["selezionataMatricolaINAIL"]))
                {
                    impresaSelezionata.MatricolaINAIL = (string) reader["selezionataMatricolaINAIL"];
                }
                if (!Convert.IsDBNull(reader["selezionataMatricolaINPS"]))
                {
                    impresaSelezionata.MatricolaINPS = (string) reader["selezionataMatricolaINPS"];
                }
                if (!Convert.IsDBNull(reader["selezionataMatricolaCCIAA"]))
                {
                    impresaSelezionata.MatricolaCCIAA = (string) reader["selezionataMatricolaCCIAA"];
                }
                if (!Convert.IsDBNull(reader["selezionataIndirizzo"]))
                {
                    impresaSelezionata.Indirizzo = (string) reader["selezionataIndirizzo"];
                }
                if (!Convert.IsDBNull(reader["selezionataComune"]))
                {
                    impresaSelezionata.Comune = (string) reader["selezionataComune"];
                }
                if (!Convert.IsDBNull(reader["selezionataProvincia"]))
                {
                    impresaSelezionata.Provincia = (string) reader["selezionataProvincia"];
                }
                if (!Convert.IsDBNull(reader["selezionataCap"]))
                {
                    impresaSelezionata.Cap = (string) reader["selezionataCap"];
                }
                if (!Convert.IsDBNull(reader["selezionataTelefono"]))
                {
                    impresaSelezionata.Telefono = (string) reader["selezionataTelefono"];
                }
                if (!Convert.IsDBNull(reader["selezionataFax"]))
                {
                    impresaSelezionata.Fax = (string) reader["selezionataFax"];
                }

                subappalto.ImpresaSelezionata = impresaSelezionata;

                // Impresa Appaltata da
                // Pu� non essere presente

                int tempOrdinal2;
                if (!Convert.IsDBNull(reader["idCptNotificaTelematicaImpresaAppaltataDa"]))
                {
                    ImpresaNotificheTelematiche impresaAppaltataDa = new ImpresaNotificheTelematiche();
                    impresaAppaltataDa.IdImpresaTelematica = (int) reader["idCptNotificaTelematicaImpresaAppaltataDa"];
                    impresaAppaltataDa.RagioneSociale = (string) reader["appaltataDaRagioneSociale"];
                    impresaAppaltataDa.LavoratoreAutonomo = (bool) reader["appaltataDaLavoratoreAutonomo"];
                    if (!Convert.IsDBNull(reader["appaltataDaPartitaIva"]))
                    {
                        impresaAppaltataDa.PartitaIva = (string) reader["appaltataDaPartitaIva"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaCodiceFiscale"]))
                    {
                        impresaAppaltataDa.CodiceFiscale = (string) reader["appaltataDaCodiceFiscale"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaAttivitaPrevalente"]))
                    {
                        impresaAppaltataDa.AttivitaPrevalente = (string) reader["appaltataDaAttivitaPrevalente"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaIdImpresa"]))
                    {
                        impresaAppaltataDa.IdImpresa = (int) reader["appaltataDaIdImpresa"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaIdCptNotificaTelematicaImpresaAnagrafica"]))
                    {
                        impresaAppaltataDa.IdImpresaAnagrafica =
                            (int) reader["appaltataDaIdCptNotificaTelematicaImpresaAnagrafica"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaIdCassaEdile"]))
                    {
                        impresaAppaltataDa.IdCassaEdile = (string) reader["appaltataDaIdCassaEdile"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaMatricolaINAIL"]))
                    {
                        impresaAppaltataDa.MatricolaINAIL = (string) reader["appaltataDaMatricolaINAIL"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaMatricolaINPS"]))
                    {
                        impresaAppaltataDa.MatricolaINPS = (string) reader["appaltataDaMatricolaINPS"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaMatricolaCCIAA"]))
                    {
                        impresaAppaltataDa.MatricolaCCIAA = (string) reader["appaltataDaMatricolaCCIAA"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaIndirizzo"]))
                    {
                        impresaAppaltataDa.Indirizzo = (string) reader["appaltataDaIndirizzo"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaComune"]))
                    {
                        impresaAppaltataDa.Comune = (string) reader["appaltataDaComune"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaProvincia"]))
                    {
                        impresaAppaltataDa.Provincia = (string) reader["appaltataDaProvincia"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaCap"]))
                    {
                        impresaAppaltataDa.Cap = (string) reader["appaltataDaCap"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaTelefono"]))
                    {
                        impresaAppaltataDa.Telefono = (string) reader["appaltataDaTelefono"];
                    }
                    if (!Convert.IsDBNull(reader["appaltataDaFax"]))
                    {
                        impresaAppaltataDa.Fax = (string) reader["appaltataDaFax"];
                    }

                    subappalto.AppaltataDa = impresaAppaltataDa;
                }

                bool affidatarie = (bool) reader["affidatarie"];
                if (affidatarie)
                {
                    subappalto.Affidatarie = true;
                    notifica.ImpreseAffidatarie.Add(subappalto);
                }
                else
                {
                    subappalto.Affidatarie = false;
                    notifica.ImpreseEsecutrici.Add(subappalto);
                }
            }

            #endregion

            return notifica;
        }

        #region Persone Notifica Telematica

        private bool InserisciPersoneNotifica(NotificaTelematica notifica, DbTransaction transaction)
        {
            bool res = true;

            if (notifica.CoordinatoreSicurezzaProgettazione != null &&
                !notifica.CoordinatoreSicurezzaProgettazione.IdPersona.HasValue)
                res = InserisciPersona(notifica.CoordinatoreSicurezzaProgettazione, transaction);

            if (res && notifica.CoordinatoreSicurezzaRealizzazione != null &&
                !notifica.CoordinatoreSicurezzaRealizzazione.IdPersona.HasValue)
                res = InserisciPersona(notifica.CoordinatoreSicurezzaRealizzazione, transaction);

            if (res && notifica.DirettoreLavori != null && !notifica.DirettoreLavori.IdPersona.HasValue)
                res = InserisciPersona(notifica.DirettoreLavori, transaction);

            return res;
        }

        private bool InserisciPersona(PersonaNotificheTelematiche persona, DbTransaction transaction)
        {
            bool res = false;

            if (!persona.IdPersona.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptPersoneInsert"))
                {
                    // Persona
                    DatabaseCemi.AddInParameter(comando, "@nominativo", DbType.String,
                        string.Format("{0} {1}", persona.PersonaCognome, persona.PersonaNome));
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, persona.PersonaCognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, persona.PersonaNome);
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, persona.PersonaCodiceFiscale);
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, persona.Indirizzo);
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, persona.PersonaComune);
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, persona.PersonaProvincia);
                    if (!string.IsNullOrEmpty(persona.PersonaCap))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, persona.PersonaCap);
                    }
                    if (!string.IsNullOrEmpty(persona.Telefono))
                    {
                        DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, persona.Telefono);
                    }
                    if (!string.IsNullOrEmpty(persona.Fax))
                    {
                        DatabaseCemi.AddInParameter(comando, "@fax", DbType.String, persona.Fax);
                    }
                    if (!string.IsNullOrEmpty(persona.PersonaCellulare))
                    {
                        DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String, persona.PersonaCellulare);
                    }
                    if (!string.IsNullOrEmpty(persona.PersonaEmail))
                    {
                        DatabaseCemi.AddInParameter(comando, "@email", DbType.String, persona.PersonaEmail);
                    }

                    // Ente
                    if (!string.IsNullOrEmpty(persona.RagioneSociale))
                    {
                        DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, persona.RagioneSociale);
                    }
                    if (!string.IsNullOrEmpty(persona.EntePartitaIva))
                    {
                        DatabaseCemi.AddInParameter(comando, "@entePartitaIva", DbType.String, persona.EntePartitaIva);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteCodiceFiscale))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteCodiceFiscale", DbType.String,
                            persona.EnteCodiceFiscale);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteIndirizzo))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteIndirizzo", DbType.String, persona.EnteIndirizzo);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteComune))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteComune", DbType.String, persona.EnteComune);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteProvincia))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteProvincia", DbType.String, persona.EnteProvincia);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteCap))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteCap", DbType.String, persona.EnteCap);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteTelefono))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteTelefono", DbType.String, persona.EnteTelefono);
                    }
                    if (!string.IsNullOrEmpty(persona.EnteFax))
                    {
                        DatabaseCemi.AddInParameter(comando, "@enteFax", DbType.String, persona.EnteFax);
                    }

                    decimal idPersona = (decimal) DatabaseCemi.ExecuteScalar(comando, transaction);

                    if (idPersona > 0)
                    {
                        persona.IdPersona = decimal.ToInt32(idPersona);
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #endregion

        #region Ricerche notifiche

        public NotificaCollection RicercaNotifichePerAggiornamento(DateTime? dataParam, string committenteParam,
            string indirizzoParam, DateTime? dataInserimentoParam,
            string numeroAppaltoParam, int? protocollo,
            short idArea, Guid? idUtenteTelematiche,
            string naturaOperaParam,
            string comuneParam)
        {
            NotificaCollection notifiche = new NotificaCollection();

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheSelect");

            if (dataParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, dataParam.Value);
            if (!string.IsNullOrEmpty(committenteParam))
                DatabaseCemi.AddInParameter(comando, "@committente", DbType.String, committenteParam);
            if (!string.IsNullOrEmpty(indirizzoParam))
                DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, indirizzoParam);
            if (dataInserimentoParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@dataInserimento", DbType.DateTime, dataInserimentoParam.Value);
            if (!string.IsNullOrEmpty(numeroAppaltoParam))
                DatabaseCemi.AddInParameter(comando, "@numeroAppalto", DbType.String, numeroAppaltoParam);
            if (protocollo.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, protocollo.Value);
            DatabaseCemi.AddInParameter(comando, "@idArea", DbType.Int16, idArea);
            if (idUtenteTelematiche.HasValue)
            {
                DatabaseCemi.AddInParameter(comando, "@idUtenteTelematiche", DbType.Guid, idUtenteTelematiche.Value);
            }
            if (!string.IsNullOrEmpty(naturaOperaParam))
            {
                DatabaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, naturaOperaParam);
            }
            if (!string.IsNullOrEmpty(comuneParam))
            {
                DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, comuneParam);
            }

            Notifica notificaCorrente = null;
            using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
            {
                while (reader.Read())
                {
                    int tempOrdinal;

                    int idNotifica;
                    int? idNotificaPadre = null;

                    idNotifica = reader.GetInt32(reader.GetOrdinal("IdCptNotifica"));

                    if (notificaCorrente == null || notificaCorrente.IdNotifica.Value != idNotifica)
                    {
                        notificaCorrente = new Notifica();
                        notificaCorrente.IdNotifica = idNotifica;

                        // Notifica padre
                        tempOrdinal = reader.GetOrdinal("IdCptNotificaPadre");
                        if (!reader.IsDBNull(tempOrdinal))
                            idNotificaPadre = reader.GetInt32(tempOrdinal);
                        notificaCorrente.IdNotificaPadre = idNotificaPadre;

                        tempOrdinal = reader.GetOrdinal("notificaRiferimento");
                        notificaCorrente.IdNotificaRiferimento = reader.GetInt32(tempOrdinal);

                        // Data
                        DateTime data = reader.GetDateTime(reader.GetOrdinal("Data"));
                        notificaCorrente.Data = data;

                        // Data inserimento
                        notificaCorrente.DataInserimento = (DateTime) reader["dataInserimento"];

                        // Natura dell'opera
                        string naturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));
                        notificaCorrente.NaturaOpera = naturaOpera;

                        // Numero appalto
                        tempOrdinal = reader.GetOrdinal("numeroAppalto");
                        if (!reader.IsDBNull(tempOrdinal))
                            notificaCorrente.NumeroAppalto = reader.GetString(tempOrdinal);

                        // Utente
                        notificaCorrente.Utente = reader.GetString(reader.GetOrdinal("utente"));

                        // Committente
                        Committente committente = new Committente();
                        if (!Convert.IsDBNull(reader["idCantieriCommittente"]))
                        {
                            committente.IdCommittente = reader.GetInt32(reader.GetOrdinal("idCantieriCommittente"));
                        }
                        committente.RagioneSociale = reader.GetString(reader.GetOrdinal("ragioneSociale"));
                        notificaCorrente.Committente = committente;

                        notifiche.Add(notificaCorrente);
                    }

                    Indirizzo indirizzo = new Indirizzo();
                    notificaCorrente.Indirizzi.Add(indirizzo);

                    // Id indirizzo
                    indirizzo.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("idCptIndirizzo"));

                    // Indirizzo
                    indirizzo.Indirizzo1 = reader.GetString(reader.GetOrdinal("indirizzo"));

                    // Civico
                    tempOrdinal = reader.GetOrdinal("civico");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.Civico = reader.GetString(tempOrdinal);

                    // Info Aggiuntiva
                    tempOrdinal = reader.GetOrdinal("infoAggiuntiva");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.InfoAggiuntiva = reader.GetString(tempOrdinal);

                    // Comune
                    tempOrdinal = reader.GetOrdinal("comune");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.Comune = reader.GetString(tempOrdinal);

                    // Provincia
                    tempOrdinal = reader.GetOrdinal("provincia");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.Provincia = reader.GetString(tempOrdinal);

                    // Cap
                    tempOrdinal = reader.GetOrdinal("cap");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.Cap = reader.GetString(tempOrdinal);

                    // Latitudine
                    tempOrdinal = reader.GetOrdinal("latitudine");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.Latitudine = reader.GetDecimal(tempOrdinal);

                    // Longitudine
                    tempOrdinal = reader.GetOrdinal("longitudine");
                    if (!reader.IsDBNull(tempOrdinal))
                        indirizzo.Longitudine = reader.GetDecimal(tempOrdinal);
                }
            }

            return notifiche;
        }

        public NotificaCollection RicercaNotifiche(NotificaFilter filtro)
        {
            NotificaCollection notifiche = new NotificaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheSelectRicerca"))
            {
                if (filtro.Dal.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filtro.Dal.Value);
                if (filtro.Al.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filtro.Al.Value);
                if (!string.IsNullOrEmpty(filtro.Committente))
                    DatabaseCemi.AddInParameter(comando, "@committente", DbType.String, filtro.Committente);
                if (!string.IsNullOrEmpty(filtro.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filtro.Indirizzo);
                if (!string.IsNullOrEmpty(filtro.Impresa))
                    DatabaseCemi.AddInParameter(comando, "@impresa", DbType.String, filtro.Impresa);
                if (!string.IsNullOrEmpty(filtro.NaturaOpera))
                    DatabaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filtro.NaturaOpera);
                if (filtro.Ammontare.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filtro.Ammontare);
                if (filtro.DataInizio.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filtro.DataInizio.Value);
                if (filtro.DataFine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, filtro.DataFine.Value);
                if (!string.IsNullOrEmpty(filtro.FiscIva))
                    DatabaseCemi.AddInParameter(comando, "@ivaFisc", DbType.String, filtro.FiscIva);
                if (!string.IsNullOrEmpty(filtro.IndirizzoComune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.IndirizzoComune);
                if (!string.IsNullOrEmpty(filtro.NumeroAppalto))
                    DatabaseCemi.AddInParameter(comando, "@numeroAppalto", DbType.String, filtro.NumeroAppalto);
                if (!string.IsNullOrEmpty(filtro.Cap))
                    DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, filtro.Cap);
                if (filtro.IdASL.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idASL", DbType.Int32, filtro.IdASL.Value);
                DatabaseCemi.AddInParameter(comando, "@idArea", DbType.Int16, filtro.IdArea);
                if (filtro.IdUtenteTelematiche.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idUtenteTelematiche", DbType.Guid,
                        filtro.IdUtenteTelematiche);
                }
                if (filtro.IdNotifica.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, filtro.IdNotifica.Value);
                }
                if (!string.IsNullOrEmpty(filtro.ProvinciaImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@provinciaImpresa", DbType.String, filtro.ProvinciaImpresa);
                }
                if (!string.IsNullOrEmpty(filtro.CapImpresa))
                {
                    DatabaseCemi.AddInParameter(comando, "@capImpresa", DbType.String, filtro.CapImpresa);
                }
                if (!string.IsNullOrEmpty(filtro.IndirizzoProvincia))
                {
                    DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, filtro.IndirizzoProvincia);
                }
                if (!string.IsNullOrEmpty(filtro.ProtocolloRegione))
                {
                    DatabaseCemi.AddInParameter(comando, "@protocolloRegione", DbType.String, filtro.ProtocolloRegione);
                }

                comando.CommandTimeout = 20000;

                Notifica notificaCorrenteRif = null;
                int idNotificaRifCorr = -1;
                Notifica notificaCorr = null;
                int ultimoAgg = 1;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        int tempOrdinal;
                        int idNotificaRif;
                        int idNotifica;

                        idNotificaRif = reader.GetInt32(reader.GetOrdinal("notificaRiferimento"));

                        // Notifica riferimento
                        if (notificaCorrenteRif == null || idNotificaRifCorr != idNotificaRif)
                        {
                            idNotificaRifCorr = idNotificaRif;
                            notificaCorrenteRif = new Notifica();
                            notifiche.Add(notificaCorrenteRif);
                            ultimoAgg = 1;

                            // Recupero dati notifica rif
                            notificaCorrenteRif.IdNotificaRiferimento = idNotificaRif;
                            notificaCorrenteRif.IdNotificaPadre = idNotificaRifCorr;
                            //tempOrdinal = reader.GetOrdinal("dataPadre");
                            notificaCorrenteRif.IdNotifica = idNotificaRifCorr;
                            notificaCorrenteRif.Data = reader.GetDateTime(reader.GetOrdinal("data"));
                            notificaCorrenteRif.DataNotificaPadre = notificaCorrenteRif.Data;
                            notificaCorrenteRif.NaturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));
                            tempOrdinal = reader.GetOrdinal("numeroAppalto");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.NumeroAppalto = reader.GetString(tempOrdinal);

                            notificaCorrenteRif.Committente = new Committente();
                            if (!Convert.IsDBNull(reader["idCantieriCommittente"]))
                            {
                                notificaCorrenteRif.Committente.IdCommittente =
                                    reader.GetInt32(reader.GetOrdinal("idCantieriCommittente"));
                            }
                            notificaCorrenteRif.Committente.RagioneSociale =
                                reader.GetString(reader.GetOrdinal("ragioneSociale"));

                            tempOrdinal = reader.GetOrdinal("indirizzoComm");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.Committente.Indirizzo = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("comuneComm");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.Committente.Comune = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("provinciaComm");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.Committente.Provincia = reader.GetString(tempOrdinal);

                            notificaCorrenteRif.NumeroVisiteASL = (int) reader["numeroVisiteASL"];
                            notificaCorrenteRif.NumeroVisiteCPT = (int) reader["numeroVisiteCPT"];
                            notificaCorrenteRif.NumeroVisiteDPL = (int) reader["numeroVisiteDPL"];
                            notificaCorrenteRif.NumeroVisiteASLERSLT = (int) reader["numeroVisiteASLERSLT"];
                            notificaCorrenteRif.NumeroVisiteCassaEdile = (int) reader["numeroVisiteCassaEdile"];

                            tempOrdinal = reader.GetOrdinal("protocolloRegione");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.ProtocolloRegione = reader.GetString(tempOrdinal);

                            tempOrdinal = reader.GetOrdinal("dataPrimoInserimento");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.DataPrimoInserimento = reader.GetDateTime(tempOrdinal);

                            tempOrdinal = reader.GetOrdinal("operaPubblica");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.OperaPubblica = reader.GetBoolean(tempOrdinal);
                        }

                        idNotifica = reader.GetInt32(reader.GetOrdinal("idCptNotifica"));

                        // Storia notifiche
                        if (notificaCorr == null || notificaCorr.IdNotifica.Value != idNotifica)
                        {
                            int idNotificaCorr = idNotifica;
                            notificaCorr = new Notifica();
                            notificaCorrenteRif.Storia.Add(notificaCorr);

                            // Recupero i dati della notifica
                            notificaCorr.IdNotificaRiferimento = idNotificaRif;
                            notificaCorr.IdNotifica = reader.GetInt32(reader.GetOrdinal("idCptNotifica"));
                            tempOrdinal = reader.GetOrdinal("idCptNotificaPadre");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorr.IdNotificaPadre = reader.GetInt32(tempOrdinal);
                            notificaCorr.Data = reader.GetDateTime(reader.GetOrdinal("data"));
                            notificaCorr.NaturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));

                            tempOrdinal = reader.GetOrdinal("protocolloRegione");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorr.ProtocolloRegione = reader.GetString(tempOrdinal);

                            tempOrdinal = reader.GetOrdinal("dataPrimoInserimento");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorr.DataPrimoInserimento = reader.GetDateTime(tempOrdinal);

                            notificaCorr.Committente = new Committente();
                            if (!Convert.IsDBNull(reader["idCantieriCommittente"]))
                            {
                                notificaCorr.Committente.IdCommittente =
                                    reader.GetInt32(reader.GetOrdinal("idCantieriCommittente"));
                            }
                            notificaCorr.Committente.RagioneSociale =
                                reader.GetString(reader.GetOrdinal("ragioneSociale"));

                            tempOrdinal = reader.GetOrdinal("impresaPresente");
                            if (!reader.IsDBNull(tempOrdinal))
                            {
                                int impresaPresente = reader.GetInt32(tempOrdinal);
                                notificaCorr.ImpresaPresente = impresaPresente != 0;
                            }
                            else
                                notificaCorr.ImpresaPresente = false;

                            // Controllo se la notifica che leggo � l'ultimo aggiornamento
                            //if (ultimoAgg == 0)
                            //{
                            notificaCorrenteRif.IdNotifica = idNotificaCorr;
                            notificaCorrenteRif.Data = reader.GetDateTime(reader.GetOrdinal("data"));
                            notificaCorrenteRif.NaturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));
                            notificaCorrenteRif.Committente = new Committente();
                            if (!Convert.IsDBNull(reader["idCantieriCommittente"]))
                            {
                                notificaCorrenteRif.Committente.IdCommittente =
                                    reader.GetInt32(reader.GetOrdinal("idCantieriCommittente"));
                            }
                            notificaCorrenteRif.Committente.RagioneSociale =
                                reader.GetString(reader.GetOrdinal("ragioneSociale"));

                            tempOrdinal = reader.GetOrdinal("indirizzoComm");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.Committente.Indirizzo = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("comuneComm");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.Committente.Comune = reader.GetString(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("provinciaComm");
                            if (!reader.IsDBNull(tempOrdinal))
                                notificaCorrenteRif.Committente.Provincia = reader.GetString(tempOrdinal);
                            //}

                            ultimoAgg--;
                        }

                        // Indirizzo
                        Indirizzo indirizzo = new Indirizzo();
                        notificaCorr.Indirizzi.Add(indirizzo);

                        indirizzo.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("idCptIndirizzo"));
                        indirizzo.Indirizzo1 = reader.GetString(reader.GetOrdinal("indirizzo"));
                        tempOrdinal = reader.GetOrdinal("civico");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.Civico = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("infoAggiuntiva");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.InfoAggiuntiva = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.Provincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("comune");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.Comune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cap");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("latitudine");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.Latitudine = reader.GetDecimal(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("longitudine");
                        if (!reader.IsDBNull(tempOrdinal))
                            indirizzo.Longitudine = reader.GetDecimal(tempOrdinal);

                        // Controllo se la notifica che leggo � l'ultimo aggiornamento
                        //if (ultimoAgg == 0 || ultimoAgg == 1)
                        //{
                        notificaCorrenteRif.Indirizzi = notificaCorr.Indirizzi;
                        //}
                    }
                }
            }

            return notifiche;
        }

        public Notifica GetNotifica(int idNotificaParam)
        {
            Notifica notifica;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheSelectSingola"))
            {
                DatabaseCemi.AddInParameter(comando, "@idNotifica", DbType.Int32, idNotificaParam);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    notifica = TrasformaReaderInNotifica(reader);
                }
            }

            return notifica;
        }

        public Notifica GetNotificaUltimaVersione(int idNotificaPadre)
        {
            Notifica notifica;
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CptNotificheSelectSingolaUltimaVersione")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idNotificaPadre", DbType.Int32, idNotificaPadre);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    notifica = TrasformaReaderInNotifica(reader);
                }
            }

            return notifica;
        }

        private static Notifica TrasformaReaderInNotifica(IDataReader reader)
        {
            Notifica notifica = new Notifica();
            // Va memorizzato da qualche parte
            //DateTime? dataNotificaPadre = null;
            int tempOrdinal;

            // Notifica
            reader.Read();

            // Id
            notifica.IdNotifica = reader.GetInt32(reader.GetOrdinal("idCptNotifica"));

            // Notifica padre
            tempOrdinal = reader.GetOrdinal("idCptNotificaPadre");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.IdNotificaPadre = reader.GetInt32(tempOrdinal);
            //tempOrdinal = reader.GetOrdinal("dataNotificaPadre");
            //if (!reader.IsDBNull(tempOrdinal))
            //    dataNotificaPadre = reader.GetDateTime(tempOrdinal);

            // Area
            notifica.Area = new Area();
            notifica.Area.IdArea = (short) reader["idCptArea"];
            notifica.Area.Descrizione = (string) reader["areaDescrizione"];

            // Data
            notifica.Data = reader.GetDateTime(reader.GetOrdinal("Data"));

            // Natura dell'opera
            notifica.NaturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));

            // Numero appalto
            tempOrdinal = reader.GetOrdinal("numeroAppalto");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroAppalto = reader.GetString(tempOrdinal);

            // Committente
            Committente committente = new Committente();
            committente.IdCommittente = reader.GetInt32(reader.GetOrdinal("idCantieriCommittente"));
            committente.RagioneSociale = reader.GetString(reader.GetOrdinal("ragioneSociale"));
            tempOrdinal = reader.GetOrdinal("commIndirizzo");
            if (!reader.IsDBNull(tempOrdinal))
                committente.Indirizzo = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commComune");
            if (!reader.IsDBNull(tempOrdinal))
                committente.Comune = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commProvincia");
            if (!reader.IsDBNull(tempOrdinal))
                committente.Provincia = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commCap");
            if (!reader.IsDBNull(tempOrdinal))
                committente.Cap = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commPartitaIva");
            if (!reader.IsDBNull(tempOrdinal))
                committente.PartitaIva = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commCodiceFiscale");
            if (!reader.IsDBNull(tempOrdinal))
                committente.CodiceFiscale = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commTelefono");
            if (!reader.IsDBNull(tempOrdinal))
                committente.Telefono = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commFax");
            if (!reader.IsDBNull(tempOrdinal))
                committente.Fax = reader.GetString(tempOrdinal);
            tempOrdinal = reader.GetOrdinal("commPersonaRiferimento");
            if (!reader.IsDBNull(tempOrdinal))
                committente.PersonaRiferimento = reader.GetString(tempOrdinal);
            notifica.Committente = committente;

            // Coordinatore progettazione
            tempOrdinal = reader.GetOrdinal("idCptPersonaCoordinatoreProgettazione");
            if (!reader.IsDBNull(tempOrdinal))
            {
                Persona coordProgettazione = new Persona();
                coordProgettazione.IdPersona = reader.GetInt32(tempOrdinal);
                coordProgettazione.Nominativo = reader.GetString(reader.GetOrdinal("coordinatoreProgettazione"));
                tempOrdinal = reader.GetOrdinal("ragSocCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.RagioneSociale = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("indirizzoCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.Indirizzo = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("telefonoCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.Telefono = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("faxCoordinatoreProgettazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordProgettazione.Fax = reader.GetString(tempOrdinal);

                notifica.CoordinatoreSicurezzaProgettazione = coordProgettazione;
            }

            // Coordinatore realizzazione
            tempOrdinal = reader.GetOrdinal("idCptPersonaCoordinatoreRealizzazione");
            if (!reader.IsDBNull(tempOrdinal))
            {
                Persona coordRealizzazione = new Persona();
                coordRealizzazione.IdPersona = reader.GetInt32(tempOrdinal);
                coordRealizzazione.Nominativo = reader.GetString(reader.GetOrdinal("coordinatoreRealizzazione"));
                tempOrdinal = reader.GetOrdinal("ragSocCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.RagioneSociale = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("indirizzoCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.Indirizzo = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("telefonoCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.Telefono = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("faxCoordinatoreRealizzazione");
                if (!reader.IsDBNull(tempOrdinal))
                    coordRealizzazione.Fax = reader.GetString(tempOrdinal);

                notifica.CoordinatoreSicurezzaRealizzazione = coordRealizzazione;
            }

            // Direttore lavori
            tempOrdinal = reader.GetOrdinal("idCptPersonaDirettoreLavori");
            if (!reader.IsDBNull(tempOrdinal))
            {
                Persona direttoreLavori = new Persona();
                direttoreLavori.IdPersona = reader.GetInt32(tempOrdinal);
                direttoreLavori.Nominativo = reader.GetString(reader.GetOrdinal("direttoreLavori"));
                tempOrdinal = reader.GetOrdinal("ragSocDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.RagioneSociale = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("indirizzoDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.Indirizzo = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("telefonoDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.Telefono = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("faxDirettoreLavori");
                if (!reader.IsDBNull(tempOrdinal))
                    direttoreLavori.Fax = reader.GetString(tempOrdinal);

                notifica.DirettoreLavori = direttoreLavori;
            }

            notifica.ResponsabileCommittente = reader.GetBoolean(reader.GetOrdinal("responsabileLavoriCommittente"));

            // Data inizio lavori
            tempOrdinal = reader.GetOrdinal("dataInizioLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataInizioLavori = reader.GetDateTime(tempOrdinal);

            // Data fine lavori
            tempOrdinal = reader.GetOrdinal("dataFineLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataFineLavori = reader.GetDateTime(tempOrdinal);

            // Durata
            tempOrdinal = reader.GetOrdinal("durataLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.Durata = reader.GetInt32(tempOrdinal);

            // Giorni uomo
            tempOrdinal = reader.GetOrdinal("giorniUomoLavori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroGiorniUomo = reader.GetInt32(tempOrdinal);

            // Numero lavoratori
            tempOrdinal = reader.GetOrdinal("numeroMassimoLavoratori");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroMassimoLavoratori = reader.GetInt32(tempOrdinal);

            // Numero imprese
            tempOrdinal = reader.GetOrdinal("numeroPrevistoImprese");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroImprese = reader.GetInt32(tempOrdinal);

            // Numero autonomi
            tempOrdinal = reader.GetOrdinal("numeroLavoratoriAutonomi");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.NumeroLavoratoriAutonomi = reader.GetInt32(tempOrdinal);

            // Ammontare complessivo
            tempOrdinal = reader.GetOrdinal("ammontareComplessivo");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.AmmontareComplessivo = reader.GetDecimal(tempOrdinal);

            // Data inserimento
            notifica.DataInserimento = reader.GetDateTime(reader.GetOrdinal("dataInserimento"));

            // Utente
            notifica.Utente = reader.GetString(reader.GetOrdinal("utente"));

            // Annullata
            notifica.Annullata = reader.GetBoolean(reader.GetOrdinal("annullata"));

            // Data annullamento
            tempOrdinal = reader.GetOrdinal("dataAnnullamento");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.DataAnnullamento = reader.GetDateTime(tempOrdinal);

            // Utente annullamento
            tempOrdinal = reader.GetOrdinal("utenteAnnullamento");
            if (!reader.IsDBNull(tempOrdinal))
                notifica.UtenteAnnullamento = reader.GetString(tempOrdinal);

            #region Indirizzi

            reader.NextResult();
            while (reader.Read())
            {
                Indirizzo indirizzo = new Indirizzo();

                indirizzo.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("idCptIndirizzo"));

                indirizzo.Indirizzo1 = reader.GetString(reader.GetOrdinal("indirizzo"));

                tempOrdinal = reader.GetOrdinal("civico");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Civico = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("infoAggiuntiva");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.InfoAggiuntiva = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("comune");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Comune = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("provincia");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Provincia = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("cap");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Cap = reader.GetString(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("latitudine");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Latitudine = reader.GetDecimal(tempOrdinal);

                tempOrdinal = reader.GetOrdinal("longitudine");
                if (!reader.IsDBNull(tempOrdinal))
                    indirizzo.Longitudine = reader.GetDecimal(tempOrdinal);

                notifica.Indirizzi.Add(indirizzo);
            }

            #endregion

            #region Subappalti

            reader.NextResult();
            while (reader.Read())
            {
                Subappalto subappalto = new Subappalto();

                subappalto.IdSubappalto = reader.GetInt32(reader.GetOrdinal("idCptSubappalto"));

                // Impresa appaltata, devo stare attento all'anagrafica di provenienza
                Impresa impresaAppaltata = new Impresa();
                tempOrdinal = reader.GetOrdinal("idImpresaAppaltata");
                if (!reader.IsDBNull(tempOrdinal))
                {
                    // Impresa SiceNew
                    impresaAppaltata.TipoImpresa = TipologiaImpresa.SiceNew;
                    impresaAppaltata.IdImpresa = reader.GetInt32(tempOrdinal);
                    impresaAppaltata.RagioneSociale = reader.GetString(reader.GetOrdinal("impresaAppaltata"));
                }
                else
                {
                    // Impresa Cantieri
                    impresaAppaltata.TipoImpresa = TipologiaImpresa.Cantieri;
                    impresaAppaltata.IdImpresa = reader.GetInt32(reader.GetOrdinal("idCantieriImpresaAppaltata"));
                    impresaAppaltata.RagioneSociale = reader.GetString(reader.GetOrdinal("cantieriImpresaAppaltata"));
                }

                tempOrdinal = reader.GetOrdinal("codiceFiscaleAppaltata");
                if (!reader.IsDBNull(tempOrdinal))
                    impresaAppaltata.CodiceFiscale = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("partitaIVAAppaltata");
                if (!reader.IsDBNull(tempOrdinal))
                    impresaAppaltata.PartitaIva = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("indirizzoAppaltata");
                if (!reader.IsDBNull(tempOrdinal))
                    impresaAppaltata.Indirizzo = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("comuneAppaltata");
                if (!reader.IsDBNull(tempOrdinal))
                    impresaAppaltata.Comune = reader.GetString(tempOrdinal);
                tempOrdinal = reader.GetOrdinal("provinciaAppaltata");
                if (!reader.IsDBNull(tempOrdinal))
                    impresaAppaltata.Provincia = reader.GetString(tempOrdinal);

                subappalto.Appaltata = impresaAppaltata;

                // Impresa appaltante, devo stare attento all'anagrafica di provenienza
                // Pu� non essere presente
                int tempOrdinal2;
                tempOrdinal = reader.GetOrdinal("idImpresaAppaltante");
                tempOrdinal2 = reader.GetOrdinal("idCantieriImpresaAppaltante");
                if (!reader.IsDBNull(tempOrdinal) || !reader.IsDBNull(tempOrdinal2))
                {
                    Impresa impresaAppaltante = new Impresa();

                    if (!reader.IsDBNull(tempOrdinal))
                    {
                        // Impresa SiceNew
                        impresaAppaltante.TipoImpresa = TipologiaImpresa.SiceNew;
                        impresaAppaltante.IdImpresa = reader.GetInt32(tempOrdinal);
                        impresaAppaltante.RagioneSociale = reader.GetString(reader.GetOrdinal("impresaAppaltante"));
                    }
                    else
                    {
                        // Impresa Cantieri
                        impresaAppaltante.TipoImpresa = TipologiaImpresa.Cantieri;
                        impresaAppaltante.IdImpresa = reader.GetInt32(tempOrdinal2);
                        impresaAppaltante.RagioneSociale =
                            reader.GetString(reader.GetOrdinal("cantieriImpresaAppaltante"));
                    }

                    tempOrdinal = reader.GetOrdinal("codiceFiscaleAppaltante");
                    if (!reader.IsDBNull(tempOrdinal))
                        impresaAppaltante.CodiceFiscale = reader.GetString(tempOrdinal);
                    tempOrdinal = reader.GetOrdinal("partitaIVAAppaltante");
                    if (!reader.IsDBNull(tempOrdinal))
                        impresaAppaltante.PartitaIva = reader.GetString(tempOrdinal);
                    tempOrdinal = reader.GetOrdinal("indirizzoAppaltante");
                    if (!reader.IsDBNull(tempOrdinal))
                        impresaAppaltante.Indirizzo = reader.GetString(tempOrdinal);
                    tempOrdinal = reader.GetOrdinal("comuneAppaltante");
                    if (!reader.IsDBNull(tempOrdinal))
                        impresaAppaltante.Comune = reader.GetString(tempOrdinal);
                    tempOrdinal = reader.GetOrdinal("provinciaAppaltante");
                    if (!reader.IsDBNull(tempOrdinal))
                        impresaAppaltante.Provincia = reader.GetString(tempOrdinal);

                    subappalto.Appaltante = impresaAppaltante;
                }

                notifica.Subappalti.Add(subappalto);
            }

            #endregion

            return notifica;
        }

        #endregion

        #region Ricerche cantieri su notifiche

        /// <summary>
        ///     Ritorna i cantieri ordinati per committenti
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public CantiereNotificaCollection RicercaCantieriPerCommittente(CantiereFilter filtro)
        {
            return RicercaCantieri("dbo.USP_CptNotificheSelectRicercaCantieriOrdPerCommittente", filtro);
        }

        /// <summary>
        ///     Ritorna i cantieri ordinati per impresa
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public CantiereNotificaCollection RicercaCantieriPerImpresa(CantiereFilter filtro)
        {
            return RicercaCantieri("dbo.USP_CptNotificheSelectRicercaCantieriOrdPerImpresa", filtro);
        }

        /// <summary>
        ///     il metodo "RicercaCantieriPerCommittente" e "RicercaCantieriPerImpresa" utilizzano lo stesso codice, quindi per il
        ///     codice � stato unificato
        ///     sotto un unico metodo eda stata parametrizzata la SP da chiamare. Le due SP variano per l'ordinamento.
        /// </summary>
        /// <param name="USP"></param>
        /// <param name="filtro"></param>
        /// <returns></returns>
        private CantiereNotificaCollection RicercaCantieri(string USP, CantiereFilter filtro)
        {
            CantiereNotificaCollection cantieri = new CantiereNotificaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand(USP))
            {
                if (!string.IsNullOrEmpty(filtro.Committente))
                    DatabaseCemi.AddInParameter(comando, "@committente", DbType.String, filtro.Committente);
                if (!string.IsNullOrEmpty(filtro.Indirizzo))
                    DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filtro.Indirizzo);
                if (!string.IsNullOrEmpty(filtro.Impresa))
                    DatabaseCemi.AddInParameter(comando, "@impresa", DbType.String, filtro.Impresa);
                if (filtro.Ammontare.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filtro.Ammontare.Value);
                if (!string.IsNullOrEmpty(filtro.NaturaOpera))
                    DatabaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filtro.NaturaOpera);
                if (filtro.DataInizio.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filtro.DataInizio.Value);
                if (filtro.DataFine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, filtro.DataFine.Value);
                if (filtro.Localizzati.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@localizzati", DbType.Boolean, filtro.Localizzati.Value);
                if (!string.IsNullOrEmpty(filtro.Comune))
                    DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, filtro.Comune);
                if (filtro.IdASL.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idASL", DbType.Int32, filtro.IdASL.Value);
                DatabaseCemi.AddInParameter(comando, "@idArea", DbType.Int16, filtro.IdArea);
                if (filtro.IdIndirizzo.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idIndirizzo", DbType.Int32, filtro.IdIndirizzo.Value);
                if (filtro.IdUtenteTelematiche.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idUtenteTelematiche", DbType.Guid,
                        filtro.IdUtenteTelematiche.Value);
                if (filtro.NumeroLavoratori.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@numeroLavoratori", DbType.Int32,
                        filtro.NumeroLavoratori.Value);
                }

                comando.CommandTimeout = 20000;

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int tempOrdinal;

                    #region Indici reader

                    int indicePartitaIva = reader.GetOrdinal("partitaIVA");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceProtocolloRegione = reader.GetOrdinal("protocolloRegione");

                    #endregion

                    while (reader.Read())
                    {
                        CantiereNotifica cantiere = new CantiereNotifica();
                        cantieri.Add(cantiere);

                        cantiere.IdNotifica = reader.GetInt32(reader.GetOrdinal("idCptNotifica"));
                        cantiere.NaturaOpera = reader.GetString(reader.GetOrdinal("naturaOpera"));
                        cantiere.Data = reader.GetDateTime(reader.GetOrdinal("data"));
                        cantiere.DataInserimento = reader.GetDateTime(reader.GetOrdinal("dataInserimento"));

                        cantiere.Committente = new Committente();
                        if (!Convert.IsDBNull(reader["idCommittente"]))
                        {
                            cantiere.Committente.IdCommittente = reader.GetInt32(reader.GetOrdinal("idCommittente"));
                        }
                        cantiere.Committente.RagioneSociale =
                            reader.GetString(reader.GetOrdinal("ragioneSocialeCommittente"));

                        tempOrdinal = reader.GetOrdinal("idImpresa");
                        //come per committente; se � presente l'id istanziamo la classe altrimenti no
                        if (!reader.IsDBNull(tempOrdinal))
                        {
                            cantiere.ImpresaRicercata = new Impresa();
                            cantiere.ImpresaRicercata.IdImpresa = reader.GetInt32(reader.GetOrdinal("idImpresa"));
                            cantiere.ImpresaRicercata.RagioneSociale =
                                reader.GetString(reader.GetOrdinal("ragioneSociale"));

                            if (!reader.IsDBNull(indicePartitaIva))
                            {
                                cantiere.ImpresaRicercata.PartitaIva = reader.GetString(indicePartitaIva);
                            }
                            if (!reader.IsDBNull(indiceCodiceFiscale))
                            {
                                cantiere.ImpresaRicercata.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                            }
                        }

                        tempOrdinal = reader.GetOrdinal("dataInizioLavori");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.DataInizioLavori = reader.GetDateTime(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("dataFineLavori");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.DataFineLavori = reader.GetDateTime(tempOrdinal);

                        cantiere.Indirizzo = new Indirizzo();
                        cantiere.IdIndirizzo = reader.GetInt32(reader.GetOrdinal("idCptIndirizzo"));
                        cantiere.Indirizzo.Indirizzo1 = reader.GetString(reader.GetOrdinal("indirizzo"));
                        tempOrdinal = reader.GetOrdinal("civico");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.Civico = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("infoAggiuntiva");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.InfoAggiuntiva = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("comune");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.Comune = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("provincia");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.Provincia = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("cap");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.Cap = reader.GetString(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("latitudine");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.Latitudine = reader.GetDecimal(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("longitudine");
                        if (!reader.IsDBNull(tempOrdinal))
                            cantiere.Indirizzo.Longitudine = reader.GetDecimal(tempOrdinal);

                        tempOrdinal = reader.GetOrdinal("numeroPrevistoImprese");
                        if (!reader.IsDBNull(tempOrdinal))
                        {
                            cantiere.NumeroImprese = reader.GetInt32(tempOrdinal);
                        }

                        tempOrdinal = reader.GetOrdinal("numeroMassimoLavoratori");
                        if (!reader.IsDBNull(tempOrdinal))
                        {
                            cantiere.NumeroLavoratori = reader.GetInt32(tempOrdinal);
                        }

                        tempOrdinal = reader.GetOrdinal("ammontareComplessivo");
                        if (!reader.IsDBNull(tempOrdinal))
                        {
                            cantiere.Ammontare = reader.GetDecimal(tempOrdinal);
                        }

                        cantiere.ProtocolloRegioneNotifica = reader.IsDBNull(indiceProtocolloRegione)
                            ? null
                            : reader.GetString(indiceProtocolloRegione);

                        if (USP == "dbo.USP_CptNotificheSelectRicercaCantieriOrdPerCommittente")
                        {
                            cantiere.NumeroVisiteASL = (int) reader["numeroVisiteASL"];
                            cantiere.NumeroVisiteCPT = (int) reader["numeroVisiteCPT"];
                            cantiere.NumeroVisiteDPL = (int) reader["numeroVisiteDPL"];
                            cantiere.NumeroVisiteASLERSLT = (int) reader["numeroVisiteASLERSLT"];
                            cantiere.NumeroVisiteCassaEdile = (int) reader["numeroVisiteCassaEdile"];
                        }
                    }
                }
            }

            return cantieri;
        }

        #endregion
    }
}