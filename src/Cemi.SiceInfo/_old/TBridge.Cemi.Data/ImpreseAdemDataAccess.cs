using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Cemi.SiceInfo.Type.Dto.ImpreseAdem;
using Cemi.SiceInfo.Type.Dto.ImpreseAdem.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.Data
{
    public class ImpreseAdemDataAccess
    {
        public ImpreseAdemDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public List<EccezioneLista> GetEccezioniListaImpreseAdempienti(EccezioneListaFilter filtro)
        {
            List<EccezioneLista> listaEccezioni = new List<EccezioneLista>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ListaImpreseRegolariEccezioniSelect"))
            {
                if (filtro.IdImpresa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, filtro.IdImpresa);
                if (!string.IsNullOrEmpty(filtro.RagioneSociale))
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, filtro.RagioneSociale);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        EccezioneLista eccezione = new EccezioneLista();
                        listaEccezioni.Add(eccezione);

                        eccezione.Impresa.IdImpresa = (int) reader["idImpresa"];
                        eccezione.Impresa.RagioneSociale = (string) reader["ragioneSociale"];
                        eccezione.Stato = (bool) reader["stato"];
                    }
                }
            }

            return listaEccezioni;
        }

        public bool InsertEccezioneListaImpreseAdempienti(EccezioneLista eccezione, out bool giaPresente)
        {
            bool res = false;
            giaPresente = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ListaImpreseRegolariEccezioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, eccezione.Impresa.IdImpresa);
                DatabaseCemi.AddInParameter(comando, "@stato", DbType.String, eccezione.Stato);

                try
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
                catch (DbException dbExcp)
                {
                    if (dbExcp.ErrorCode == -2146232060)
                        giaPresente = true;
                    else
                        throw;
                }
            }

            return res;
        }

        public bool DeleteEccezioneListaImpreseAdempienti(int idImpresa)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ListaImpreseRegolariEccezioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpresa);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public void UpdateParametriImpreseRegolari(string singolo, string totale)
        {
            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_UpdateParametriImpreseRegolari"))
            {
                command.CommandText = "dbo.USP_UpdateParametriImpreseRegolari";
                DatabaseCemi.AddInParameter(command, "@singolo", DbType.String, singolo);
                DatabaseCemi.AddInParameter(command, "@totale", DbType.String, totale);
                DatabaseCemi.ExecuteNonQuery(command);
            }
        }

        public void GetParametriImpreseRegolari(out string singolo, out string totale)
        {
            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_GetParametriImpreseRegolari"))
            {
                using (IDataReader rs = DatabaseCemi.ExecuteReader(command))
                {
                    rs.Read();
                    singolo = rs.GetValue(0).ToString();
                    totale = rs.GetValue(1).ToString();
                    rs.Close();
                }
            }
        }

        public List<AttivitaIstat> GetAttivitaIstatImpreseAdempienti()
        {
            List<AttivitaIstat> listAttivitaIstat = new List<AttivitaIstat>();

            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRegolariAttivitaISTATSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        AttivitaIstat attivitaIstat =
                            new AttivitaIstat
                            {
                                CodiceAttivita = reader["idattivitaistat"].ToString(),
                                Descrizione = reader["descrizione"].ToString()
                            };

                        listAttivitaIstat.Add(attivitaIstat);
                    }
                }
            }

            return listAttivitaIstat;
        }

        public List<Comune> GetComuniSedeAmministrativaImpreseRegolari()
        {
            List<Comune> listAttivitaIstat = new List<Comune>();

            using (DbCommand command =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRegolariComuniSedeAmministrativaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Comune attivitaIstat = new Comune
                        {
                            Nome = reader["comune"].ToString(),
                            CodiceCatastale = reader["codiceCatastale"].ToString()
                        };

                        listAttivitaIstat.Add(attivitaIstat);
                    }
                }
            }

            return listAttivitaIstat;
        }

        public List<Comune> GetComuniSedeLegaleImpreseRegolari()
        {
            List<Comune> listAttivitaIstat = new List<Comune>();

            using (DbCommand command =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRegolariComuniSedeLegaleSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        Comune attivitaIstat = new Comune
                        {
                            Nome = reader["comune"].ToString(),
                            CodiceCatastale = reader["codiceCatastale"].ToString()
                        };

                        listAttivitaIstat.Add(attivitaIstat);
                    }
                }
            }

            return listAttivitaIstat;
        }
    }
}