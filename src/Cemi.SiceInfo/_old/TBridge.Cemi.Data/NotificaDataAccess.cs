using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Entities.Notifiche;
using TBridge.Cemi.Type.Exceptions.Notifiche;

namespace TBridge.Cemi.Data
{
    public class NotificaDataAccess
    {
        public NotificaDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; }

        public void AssunzioneNotifica(Notifica notifica)
        {
            if (notifica.TipoNotifica == TipoNotifica.Assunzione)
            {
                // Controllo che non esista gi� una denuncia o una notifica
                TipoErrore err = EsisteDenunciaNotifica(notifica);

                switch (err)
                {
                    case TipoErrore.NonPresente:
                        // Inserisco la notifica
                        DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheInsertAssunzione");
                        DatabaseCemi.AddInParameter(dbCommand, "@cognome", DbType.String, notifica.Cognome);
                        DatabaseCemi.AddInParameter(dbCommand, "@nome", DbType.String, notifica.Nome);
                        DatabaseCemi.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, notifica.DataNascita);
                        DatabaseCemi.AddInParameter(dbCommand, "@luogoNascita", DbType.String, notifica.LuogoNascita);
                        DatabaseCemi.AddInParameter(dbCommand, "@sesso", DbType.String, notifica.Sesso);
                        DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, notifica.CodiceFiscale);
                        DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, notifica.IdImpresa);
                        DatabaseCemi.AddInParameter(dbCommand, "@dataAssunzione", DbType.DateTime,
                            notifica.DataAssunzione);

                        notifica.IdNotifica = decimal.ToInt32((decimal) DatabaseCemi.ExecuteScalar(dbCommand));
                        break;
                    case TipoErrore.DenunciaPresente:
                        throw new DenunciaPresenteException();
                    case TipoErrore.NotificaPresente:
                        throw new NotificaPresenteException();
                }
            }
            else
                throw new Exception("Errata chiamata ad assunzioneNotifica");
        }

        public void CessazioneNotifica(Notifica notifica)
        {
            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheInsertCessazioneDaNotifica"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idNotifica", DbType.Int32, notifica.IdNotifica);
                DatabaseCemi.AddInParameter(dbCommand, "@dataCessazione", DbType.DateTime,
                    notifica.DataCessazione.Value);

                DatabaseCemi.ExecuteNonQuery(dbCommand);
            }
        }

        public void CessazioneDenuncia(Notifica notifica)
        {
            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheInsertCessazioneDaDenuncia"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idLavoratore", DbType.Int32, notifica.IdLavoratore);
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, notifica.IdImpresa);
                DatabaseCemi.AddInParameter(dbCommand, "@dataCessazione", DbType.DateTime,
                    notifica.DataCessazione.Value);

                notifica.IdNotifica = decimal.ToInt32((decimal) DatabaseCemi.ExecuteScalar(dbCommand));
            }
        }

        private TipoErrore EsisteDenunciaNotifica(Notifica notifica)
        {
            try
            {
                using (
                    DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelectEsisteLavoratore"))
                {
                    DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, notifica.CodiceFiscale);
                    DatabaseCemi.AddInParameter(dbCommand, "@cognome", DbType.String, notifica.Cognome);
                    DatabaseCemi.AddInParameter(dbCommand, "@nome", DbType.String, notifica.Nome);
                    DatabaseCemi.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, notifica.DataNascita);
                    DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, notifica.IdImpresa);
                    DatabaseCemi.AddInParameter(dbCommand, "@dataAssunzione", DbType.DateTime, notifica.DataAssunzione);

                    using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                    {
                        if (ds.Tables.Count == 2)
                        {
                            if ((int) ds.Tables[0].Rows[0].ItemArray[0] != 0)
                                return TipoErrore.NotificaPresente;
                            if ((int) ds.Tables[1].Rows[0].ItemArray[0] != 0)
                                return TipoErrore.DenunciaPresente;
                        }
                    }
                }
            }
            catch
            {
            }

            return TipoErrore.NonPresente;
        }

        public List<Notifica> RicercaNotificheDenunce(string codiceFiscale, string cognome, string nome,
            DateTime? dataNascita, int idImpresa)
        {
            List<Notifica> listaNotifiche;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelect"))
            {
                if (codiceFiscale != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@codiceFiscale", DbType.String, codiceFiscale);
                if (cognome != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@cognome", DbType.String, cognome);
                if (nome != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@nome", DbType.String, nome);
                if (dataNascita != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@dataNascita", DbType.DateTime, dataNascita);

                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    listaNotifiche = new List<Notifica>();

                    TrasformaDataSetInListaNotifiche(ds, listaNotifiche);
                }
            }

            return listaNotifiche;
        }

        public List<Notifica> RicercaRapportiCessazioni(int idImpresa)
        {
            List<Notifica> listaNotifiche;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelectRapportiCessati"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    listaNotifiche = new List<Notifica>();

                    TrasformaDataSetInListaNotifiche(ds, listaNotifiche);
                }
            }

            return listaNotifiche;
        }

        public List<Notifica> RicercaRapportiAssunzioni(int idImpresa)
        {
            List<Notifica> listaNotifiche;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelectRapportiNotificati"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    listaNotifiche = new List<Notifica>();

                    TrasformaDataSetInListaNotifiche(ds, listaNotifiche);
                }
            }

            return listaNotifiche;
        }

        public List<Notifica> RicercaNotificheNonDenunciate()
        {
            List<Notifica> listaNotifiche;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelectNoDenunce"))
            {
                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    listaNotifiche = new List<Notifica>();

                    TrasformaDataSetInListaNotifiche(ds, listaNotifiche);
                }
            }

            return listaNotifiche;
        }

        public List<Notifica> RicercaNotificheRapportiCessatiDenunciati()
        {
            List<Notifica> listaNotifiche;
            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelectDenunceRapportiCessati")
            )
            {
                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    listaNotifiche = new List<Notifica>();

                    TrasformaDataSetInListaNotifiche(ds, listaNotifiche);
                }
            }

            return listaNotifiche;
        }

        public List<Notifica> RicercaLavoratoriImpresa(string cognome, string nome, int idImpresa)
        {
            List<Notifica> listaNotifiche;
            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_NotificheSelectSubappalti"))
            {
                if (cognome != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@cognome", DbType.String, cognome);
                if (nome != null)
                    DatabaseCemi.AddInParameter(dbCommand, "@nome", DbType.String, nome);
                DatabaseCemi.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, idImpresa);

                using (DataSet ds = DatabaseCemi.ExecuteDataSet(dbCommand))
                {
                    listaNotifiche = new List<Notifica>();

                    if (ds != null && ds.Tables.Count == 1)
                    {
                        using (DataTable dt = ds.Tables[0])
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string cognomeDB = string.Empty;
                                if (dt.Rows[i]["cognome"] != DBNull.Value)
                                    cognomeDB = (string) dt.Rows[i]["cognome"];

                                string nomeDB = string.Empty;
                                if (dt.Rows[i]["nome"] != DBNull.Value)
                                    nomeDB = (string) dt.Rows[i]["nome"];

                                string codiceFiscaleDB = string.Empty;
                                if (dt.Rows[i]["codiceFiscale"] != DBNull.Value)
                                    codiceFiscaleDB = (string) dt.Rows[i]["codiceFiscale"];

                                DateTime? dataAssunzioneDB = null;
                                if (dt.Rows[i]["dataAssunzione"] != DBNull.Value)
                                    dataAssunzioneDB = (DateTime) dt.Rows[i]["dataAssunzione"];

                                Notifica notifica = new Notifica(cognomeDB, nomeDB, codiceFiscaleDB, dataAssunzioneDB);

                                listaNotifiche.Add(notifica);
                            }
                        }
                    }
                }
            }

            return listaNotifiche;
        }

        public List<Impresa> GetImpreseConsulente(int idConsulente)
        {
            List<Impresa> imprese = new List<Impresa>();

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_Imprese_ConsulentiSelectPerNotifiche"))
            {
                DatabaseCemi.AddInParameter(comando, "@idConsulente", DbType.Int32, idConsulente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdImpresa = reader.GetOrdinal("idImpresa");
                    int indiceRagioneSocialeImpresa = reader.GetOrdinal("ragioneSociale");

                    while (reader.Read())
                    {
                        Impresa impresa = new Impresa();

                        impresa.IdImpresa = reader.GetInt32(indiceIdImpresa);
                        impresa.RagioneSociale = reader.GetString(indiceRagioneSocialeImpresa);

                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        public int MesiRitardoDati()
        {
            int rit = (int) DatabaseCemi.ExecuteScalar(CommandType.Text, "select dbo.UF_MesiRitardoDati()");

            return rit;
        }

        private static void TrasformaDataSetInListaNotifiche(DataSet ds, ICollection<Notifica> listaNotifiche)
        {
            if (ds != null && ds.Tables.Count == 1)
            {
                DataTable dt = ds.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int id;
                    TipoNotifica tipo;

                    if ((string) dt.Rows[i]["tipo"] == "Notifica")
                    {
                        id = (int) dt.Rows[i]["idNotifica"];
                        tipo = TipoNotifica.CessazioneNotifica;
                    }
                    else if ((string) dt.Rows[i]["tipo"] == "Denuncia")
                    {
                        id = (int) dt.Rows[i]["idLavoratore"];
                        tipo = TipoNotifica.CessazioneDenuncia;
                    }
                    else
                    {
                        // Non dovrebbe mai passare di qui
                        id = -1;
                        tipo = TipoNotifica.CessazioneDenuncia;
                    }

                    string cognomeDB = string.Empty;
                    if (dt.Rows[i]["cognome"] != DBNull.Value)
                        cognomeDB = (string) dt.Rows[i]["cognome"];

                    string nomeDB = string.Empty;
                    if (dt.Rows[i]["nome"] != DBNull.Value)
                        nomeDB = (string) dt.Rows[i]["nome"];

                    DateTime dataNascitaDB = new DateTime();
                    if (dt.Rows[i]["dataNascita"] != DBNull.Value)
                        dataNascitaDB = (DateTime) dt.Rows[i]["dataNascita"];

                    string luogoNascitaDB = string.Empty;
                    if (dt.Rows[i]["luogoNascita"] != DBNull.Value)
                        luogoNascitaDB = (string) dt.Rows[i]["luogoNascita"];

                    string sessoDB = string.Empty;
                    if (dt.Rows[i]["sesso"] != DBNull.Value)
                        sessoDB = (string) dt.Rows[i]["sesso"];

                    string codiceFiscaleDB = string.Empty;
                    if (dt.Rows[i]["codiceFiscale"] != DBNull.Value)
                        codiceFiscaleDB = (string) dt.Rows[i]["codiceFiscale"];

                    DateTime? dataAssunzioneDB = null;
                    if (dt.Rows[i]["dataAssunzione"] != DBNull.Value)
                        dataAssunzioneDB = (DateTime) dt.Rows[i]["dataAssunzione"];

                    DateTime? dataCessazioneDB = null;
                    if (dt.Rows[i]["dataCessazione"] != DBNull.Value)
                        dataCessazioneDB = (DateTime) dt.Rows[i]["dataCessazione"];

                    int idImpresaDB = (int) dt.Rows[i]["idImpresa"];

                    string ragioneSociale = null;
                    if (dt.Rows[i]["ragioneSociale"] != DBNull.Value)
                        ragioneSociale = (string) dt.Rows[i]["ragioneSociale"];

                    Notifica notifica = new Notifica(tipo, id, cognomeDB, nomeDB, dataNascitaDB, luogoNascitaDB,
                        sessoDB,
                        codiceFiscaleDB, dataAssunzioneDB, dataCessazioneDB, idImpresaDB,
                        ragioneSociale);

                    listaNotifiche.Add(notifica);
                }
            }
        }
    }
}