using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.Colonie;
using TBridge.Cemi.Type.Filters.Colonie;
using CassaEdile = TBridge.Cemi.Type.Entities.Colonie.CassaEdile;

namespace TBridge.Cemi.Data
{
    public class ColonieDataAccess
    {
        //private Database DatabaseCemi;

        public ColonieDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        private Database DatabaseCemi { get; }

        public DestinazioneCollection GetDestinazioni(int? idTipoVacanza, int? idTipoDestinazione)
        {
            DestinazioneCollection destinazioni = new DestinazioneCollection();

            DataSet dsDestinazioni;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDestinazioniSelect"))
            {
                if (idTipoVacanza.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanza.Value);
                if (idTipoDestinazione.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTipoDestinazione", DbType.Int32, idTipoDestinazione.Value);

                dsDestinazioni = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsDestinazioni != null && dsDestinazioni.Tables.Count == 1)
            {
                for (int i = 0; i < dsDestinazioni.Tables[0].Rows.Count; i++)
                {
                    int idDestinazione;
                    string luogo;
                    bool attiva;
                    int idColonieTipoDestinazione;
                    string descrizioneTD;
                    int idColonieTipoVacanza;
                    string descrizioneTV;

                    idDestinazione = (int) dsDestinazioni.Tables[0].Rows[i]["idColonieDestinazione"];
                    luogo = (string) dsDestinazioni.Tables[0].Rows[i]["luogo"];
                    attiva = (bool) dsDestinazioni.Tables[0].Rows[i]["attiva"];
                    idColonieTipoDestinazione = (int) dsDestinazioni.Tables[0].Rows[i]["idColonieTipoDestinazione"];
                    descrizioneTD = (string) dsDestinazioni.Tables[0].Rows[i]["descrizioneTD"];
                    idColonieTipoVacanza = (int) dsDestinazioni.Tables[0].Rows[i]["idColonieTipoVacanza"];
                    descrizioneTV = (string) dsDestinazioni.Tables[0].Rows[i]["descrizioneTV"];

                    TipoVacanza tipoVac = new TipoVacanza(idColonieTipoVacanza, descrizioneTV);
                    TipoDestinazione tipoDest = new TipoDestinazione(idColonieTipoDestinazione, descrizioneTD, tipoVac);

                    destinazioni.Add(new Destinazione(idDestinazione, tipoDest, luogo, attiva));
                }
            }

            return destinazioni;
        }

        public TipoVacanzaCollection GetTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = new TipoVacanzaCollection();

            DataSet dsTipiVacanza;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTipiVacanzaSelect"))
            {
                dsTipiVacanza = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsTipiVacanza != null && dsTipiVacanza.Tables.Count == 1)
            {
                for (int i = 0; i < dsTipiVacanza.Tables[0].Rows.Count; i++)
                {
                    int idTipoVacanza = (int) dsTipiVacanza.Tables[0].Rows[i]["idColonieTipoVacanza"];
                    string descrizione = (string) dsTipiVacanza.Tables[0].Rows[i]["descrizione"];

                    tipiVacanza.Add(new TipoVacanza(idTipoVacanza, descrizione));
                }
            }

            return tipiVacanza;
        }

        public TipoDestinazioneCollection GetTipiDestinazione(int idTipoVacanza)
        {
            TipoDestinazioneCollection tipiDestinazione = new TipoDestinazioneCollection();

            DataSet dsTipiDestinazione;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTipiDestinazioneSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanza);

                dsTipiDestinazione = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsTipiDestinazione != null && dsTipiDestinazione.Tables.Count == 1)
            {
                for (int i = 0; i < dsTipiDestinazione.Tables[0].Rows.Count; i++)
                {
                    int idTipoDestinazione = (int) dsTipiDestinazione.Tables[0].Rows[i]["idColonieTipoDestinazione"];
                    string descrizione = (string) dsTipiDestinazione.Tables[0].Rows[i]["descrizioneTD"];

                    tipiDestinazione.Add(new TipoDestinazione(idTipoDestinazione, descrizione, null));
                }
            }

            return tipiDestinazione;
        }

        public bool UpdateDestinazione(Destinazione destinazione)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool InsertDestinazione(Destinazione destinazione)
        {
            bool res = false;

            if (!destinazione.IdDestinazione.HasValue && !string.IsNullOrEmpty(destinazione.Luogo) &&
                destinazione.TipoDestinazione != null && destinazione.TipoDestinazione.TipoVacanza != null)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDestinazioniInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@luogo", DbType.String, destinazione.Luogo);
                    DatabaseCemi.AddInParameter(comando, "@idTipoDestinazione", DbType.Int32,
                        destinazione.TipoDestinazione.IdTipoDestinazione);
                    DatabaseCemi.AddInParameter(comando, "@attiva", DbType.Boolean, destinazione.Attiva);

                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        public TurnoCollection GetTurni(int? idTurnoParam, int? idVacanzaParam, int? idDestinazioneParam,
            int? annoParam,
            int? idTipoVacanzaParam)
        {
            TurnoCollection turni = new TurnoCollection();

            DataSet dsTurni;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTurniSelect"))
            {
                if (idTurnoParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurnoParam.Value);
                if (idVacanzaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanzaParam.Value);
                if (idDestinazioneParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDestinazione", DbType.Int32, idDestinazioneParam.Value);
                if (annoParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, annoParam.Value);
                if (idTipoVacanzaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanzaParam.Value);

                dsTurni = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsTurni != null && dsTurni.Tables.Count == 1)
            {
                for (int i = 0; i < dsTurni.Tables[0].Rows.Count; i++)
                {
                    int idTurno;
                    int progressivo;
                    DateTime dal;
                    DateTime al;
                    int idDestinazione;
                    int idVacanza;
                    int postiDisponibili;
                    string luogo;
                    bool attiva;
                    int idTipoDestinazione;
                    string descrizione;
                    decimal costoGiornaliero;

                    idTurno = (int) dsTurni.Tables[0].Rows[i]["idColonieTurno"];
                    progressivo = (int) dsTurni.Tables[0].Rows[i]["progressivo"];
                    dal = (DateTime) dsTurni.Tables[0].Rows[i]["dal"];
                    al = (DateTime) dsTurni.Tables[0].Rows[i]["al"];

                    idDestinazione = (int) dsTurni.Tables[0].Rows[i]["idColonieDestinazione"];
                    luogo = (string) dsTurni.Tables[0].Rows[i]["luogo"];
                    attiva = (bool) dsTurni.Tables[0].Rows[i]["attiva"];
                    idTipoDestinazione = (int) dsTurni.Tables[0].Rows[i]["idColonieTipoDestinazione"];
                    descrizione = (string) dsTurni.Tables[0].Rows[i]["descrizione"];

                    idVacanza = (int) dsTurni.Tables[0].Rows[i]["idColonieVacanza"];
                    postiDisponibili = (int) dsTurni.Tables[0].Rows[i]["postiDisponibili"];

                    costoGiornaliero = (decimal) dsTurni.Tables[0].Rows[i]["costoGiornaliero"];

                    Destinazione destinazione =
                        new Destinazione(idDestinazione, new TipoDestinazione(idTipoDestinazione, descrizione, null),
                            luogo, attiva);

                    Turno turno = new Turno(idTurno, progressivo, dal, al, destinazione, -1, idVacanza,
                        postiDisponibili,
                        costoGiornaliero);
                    turno.CostoGiornalieroAccompagnatore =
                        (decimal) dsTurni.Tables[0].Rows[i]["costoGiornalieroAccompagnatore"];
                    turni.Add(turno);
                }
            }

            return turni;
        }

        public bool? InsertTurno(Turno turno)
        {
            bool? res = false;

            if (!turno.IdTurno.HasValue && turno.ProgressivoTurno > 0)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTurniInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idDestinazione", DbType.Int32, turno.IdDestinazione);
                    DatabaseCemi.AddInParameter(comando, "@progressivo", DbType.Int32, turno.ProgressivoTurno);
                    DatabaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, turno.Dal);
                    DatabaseCemi.AddInParameter(comando, "@al", DbType.DateTime, turno.Al);
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, turno.IdVacanza);
                    DatabaseCemi.AddInParameter(comando, "@postiDisponibili", DbType.Int32, turno.PostiDisponibili);
                    DatabaseCemi.AddInParameter(comando, "@costoGiornaliero", DbType.Decimal, turno.CostoGiornaliero);
                    DatabaseCemi.AddInParameter(comando, "@costoGiornalieroAccompagnatore", DbType.Decimal,
                        turno.CostoGiornalieroAccompagnatore);

                    try
                    {
                        if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                            res = true;
                    }
                    catch (SqlException exc)
                    {
                        // Controllo se � stato violato il vincolo di univocit�
                        if (exc.Errors.Count > 1 && exc.Errors[0].Number == 2627)
                            res = null;
                    }
                }
            }

            return res;
        }

        public bool DeleteTurno(int idTurno)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTurniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurno);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public Vacanza GetVacanza(int annoParam, int idTipoVacanzaParam)
        {
            Vacanza vacanza = null;

            DataSet dsVacanze;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzeSelectSingola"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, annoParam);
                DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanzaParam);

                dsVacanze = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsVacanze != null && dsVacanze.Tables.Count == 1 && dsVacanze.Tables[0].Rows.Count == 1)
            {
                int idVacanza;
                int anno;
                int idTipoVacanza;
                string tipoVacanza;
                decimal costoGiornaliero;
                decimal costoDisabile;
                decimal penalita;
                int limitePenalita;

                idVacanza = (int) dsVacanze.Tables[0].Rows[0]["idColonieVacanza"];
                anno = (int) dsVacanze.Tables[0].Rows[0]["anno"];
                idTipoVacanza = (int) dsVacanze.Tables[0].Rows[0]["idColonieTipoVacanza"];
                tipoVacanza = (string) dsVacanze.Tables[0].Rows[0]["descrizione"];
                costoGiornaliero = (decimal) dsVacanze.Tables[0].Rows[0]["costoGiornaliero"];
                costoDisabile = (decimal) dsVacanze.Tables[0].Rows[0]["costoDisabile"];
                penalita = (decimal) dsVacanze.Tables[0].Rows[0]["costoPenalita"];
                limitePenalita = (int) dsVacanze.Tables[0].Rows[0]["limitePenalita"];

                vacanza = new Vacanza(idVacanza, anno, new TipoVacanza(idTipoVacanza, tipoVacanza),
                    costoGiornaliero, penalita, costoDisabile, limitePenalita);

                if (dsVacanze.Tables[0].Rows[0]["dataInizioPrenotazioni"] != DBNull.Value)
                    vacanza.DataInizioPrenotazioni = (DateTime) dsVacanze.Tables[0].Rows[0]["dataInizioPrenotazioni"];
                if (dsVacanze.Tables[0].Rows[0]["dataFinePrenotazioni"] != DBNull.Value)
                    vacanza.DataFinePrenotazioni = (DateTime) dsVacanze.Tables[0].Rows[0]["dataFinePrenotazioni"];
                if (dsVacanze.Tables[0].Rows[0]["dataInizioDomandeACE"] != DBNull.Value)
                    vacanza.DataInizioDomandeACE = (DateTime) dsVacanze.Tables[0].Rows[0]["dataInizioDomandeACE"];
                if (dsVacanze.Tables[0].Rows[0]["dataFineDomandeACE"] != DBNull.Value)
                    vacanza.DataFineDomandeACE = (DateTime) dsVacanze.Tables[0].Rows[0]["dataFineDomandeACE"];
                vacanza.Attiva = (bool) dsVacanze.Tables[0].Rows[0]["attiva"];
                vacanza.LimiteInferioreBimbi = (DateTime) dsVacanze.Tables[0].Rows[0]["limiteInferioreBimbi"];
                vacanza.LimiteSuperioreBimbi = (DateTime) dsVacanze.Tables[0].Rows[0]["limiteSuperioreBimbi"];
                vacanza.CostoGiornalieroAccompagnatore =
                    (decimal) dsVacanze.Tables[0].Rows[0]["costoGiornalieroAccompagnatore"];
            }

            return vacanza;
        }

        public VacanzaCollection GetVacanze(int? annoParam, int? idVacanzaParam)
        {
            VacanzaCollection vacanze = new VacanzaCollection();
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzeSelect");
            if (annoParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, annoParam.Value);
            if (idVacanzaParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanzaParam.Value);

            DataSet dsVacanze = DatabaseCemi.ExecuteDataSet(comando);

            if (dsVacanze != null && dsVacanze.Tables.Count == 1)
            {
                for (int i = 0; i < dsVacanze.Tables[0].Rows.Count; i++)
                {
                    int idVacanza;
                    int anno;
                    int idTipoVacanza;
                    string tipoVacanza;
                    decimal costoGiornaliero;
                    decimal costoDisabile;
                    decimal penalita;
                    int limitePenalita;

                    idVacanza = (int) dsVacanze.Tables[0].Rows[i]["idColonieVacanza"];
                    anno = (int) dsVacanze.Tables[0].Rows[i]["anno"];
                    idTipoVacanza = (int) dsVacanze.Tables[0].Rows[i]["idColonieTipoVacanza"];
                    tipoVacanza = (string) dsVacanze.Tables[0].Rows[i]["descrizione"];
                    costoGiornaliero = (decimal) dsVacanze.Tables[0].Rows[i]["costoGiornaliero"];
                    costoDisabile = (decimal) dsVacanze.Tables[0].Rows[i]["costoDisabile"];
                    penalita = (decimal) dsVacanze.Tables[0].Rows[i]["costoPenalita"];
                    limitePenalita = (int) dsVacanze.Tables[0].Rows[i]["limitePenalita"];

                    Vacanza vacanza = new Vacanza(idVacanza, anno, new TipoVacanza(idTipoVacanza, tipoVacanza),
                        costoGiornaliero, penalita, costoDisabile, limitePenalita);
                    vacanze.Add(vacanza);

                    if (dsVacanze.Tables[0].Rows[0]["dataInizioPrenotazioni"] != DBNull.Value)
                        vacanza.DataInizioPrenotazioni =
                            (DateTime) dsVacanze.Tables[0].Rows[0]["dataInizioPrenotazioni"];
                    if (dsVacanze.Tables[0].Rows[0]["dataFinePrenotazioni"] != DBNull.Value)
                        vacanza.DataFinePrenotazioni = (DateTime) dsVacanze.Tables[0].Rows[0]["dataFinePrenotazioni"];
                    if (dsVacanze.Tables[0].Rows[0]["dataInizioDomandeACE"] != DBNull.Value)
                        vacanza.DataInizioDomandeACE = (DateTime) dsVacanze.Tables[0].Rows[0]["dataInizioDomandeACE"];
                    if (dsVacanze.Tables[0].Rows[0]["dataFineDomandeACE"] != DBNull.Value)
                        vacanza.DataFineDomandeACE = (DateTime) dsVacanze.Tables[0].Rows[0]["dataFineDomandeACE"];
                    vacanza.Attiva = (bool) dsVacanze.Tables[0].Rows[0]["attiva"];
                    vacanza.LimiteInferioreBimbi = (DateTime) dsVacanze.Tables[0].Rows[0]["limiteInferioreBimbi"];
                    vacanza.LimiteSuperioreBimbi = (DateTime) dsVacanze.Tables[0].Rows[0]["limiteSuperioreBimbi"];
                    vacanza.CostoGiornalieroAccompagnatore =
                        (decimal) dsVacanze.Tables[0].Rows[0]["costoGiornalieroAccompagnatore"];
                    if (dsVacanze.Tables[0].Rows[0]["dataInizioRichiestePersonale"] != DBNull.Value)
                        vacanza.DataInizioRichieste =
                            (DateTime) dsVacanze.Tables[0].Rows[0]["dataInizioRichiestePersonale"];
                    if (dsVacanze.Tables[0].Rows[0]["dataFineRichiestePersonale"] != DBNull.Value)
                        vacanza.DataFineRichieste =
                            (DateTime) dsVacanze.Tables[0].Rows[0]["dataFineRichiestePersonale"];
                }
            }

            return vacanze;
        }

        public Vacanza GetVacanzaByTurno(int idTurno)
        {
            Vacanza vacanza = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzeSelectByIdTurno"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurno);

                using (DataSet dsVacanza = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsVacanza != null && dsVacanza.Tables.Count == 1 && dsVacanza.Tables[0].Rows.Count == 1)
                    {
                        int idVacanza;
                        int anno;
                        int idTipoVacanza;
                        string tipoVacanza;
                        decimal costoGiornaliero;
                        decimal costoDisabile;
                        decimal penalita;
                        int limitePenalita;

                        idVacanza = (int) dsVacanza.Tables[0].Rows[0]["idColonieVacanza"];
                        anno = (int) dsVacanza.Tables[0].Rows[0]["anno"];
                        idTipoVacanza = (int) dsVacanza.Tables[0].Rows[0]["idColonieTipoVacanza"];
                        tipoVacanza = (string) dsVacanza.Tables[0].Rows[0]["descrizione"];
                        costoGiornaliero = (decimal) dsVacanza.Tables[0].Rows[0]["costoGiornaliero"];
                        costoDisabile = (decimal) dsVacanza.Tables[0].Rows[0]["costoDisabile"];
                        penalita = (decimal) dsVacanza.Tables[0].Rows[0]["costoPenalita"];
                        limitePenalita = (int) dsVacanza.Tables[0].Rows[0]["limitePenalita"];

                        vacanza = new Vacanza(idVacanza, anno, new TipoVacanza(idTipoVacanza, tipoVacanza),
                            costoGiornaliero, penalita, costoDisabile, limitePenalita);

                        if (dsVacanza.Tables[0].Rows[0]["dataInizioPrenotazioni"] != DBNull.Value)
                            vacanza.DataInizioPrenotazioni =
                                (DateTime) dsVacanza.Tables[0].Rows[0]["dataInizioPrenotazioni"];
                        if (dsVacanza.Tables[0].Rows[0]["dataFinePrenotazioni"] != DBNull.Value)
                            vacanza.DataFinePrenotazioni =
                                (DateTime) dsVacanza.Tables[0].Rows[0]["dataFinePrenotazioni"];
                        if (dsVacanza.Tables[0].Rows[0]["dataInizioDomandeACE"] != DBNull.Value)
                            vacanza.DataInizioDomandeACE =
                                (DateTime) dsVacanza.Tables[0].Rows[0]["dataInizioDomandeACE"];
                        if (dsVacanza.Tables[0].Rows[0]["dataFineDomandeACE"] != DBNull.Value)
                            vacanza.DataFineDomandeACE = (DateTime) dsVacanza.Tables[0].Rows[0]["dataFineDomandeACE"];
                        vacanza.Attiva = (bool) dsVacanza.Tables[0].Rows[0]["attiva"];
                        vacanza.LimiteInferioreBimbi = (DateTime) dsVacanza.Tables[0].Rows[0]["limiteInferioreBimbi"];
                        vacanza.LimiteSuperioreBimbi = (DateTime) dsVacanza.Tables[0].Rows[0]["limiteSuperioreBimbi"];
                        vacanza.CostoGiornalieroAccompagnatore =
                            (decimal) dsVacanza.Tables[0].Rows[0]["costoGiornalieroAccompagnatore"];
                    }
                }
            }

            return vacanza;
        }

        public bool UpdateVacanza(Vacanza vacanza)
        {
            bool res = false;

            if (vacanza.IdVacanza.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzeUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, vacanza.IdVacanza.Value);
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, vacanza.Anno);
                    DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32,
                        vacanza.TipoVacanza.IdTipoVacanza);
                    DatabaseCemi.AddInParameter(comando, "@costoGiornaliero", DbType.Decimal, vacanza.CostoGiornaliero);
                    DatabaseCemi.AddInParameter(comando, "@costoGiornalieroAccompagnatore", DbType.Decimal,
                        vacanza.CostoGiornalieroAccompagnatore);
                    DatabaseCemi.AddInParameter(comando, "@costoPenalita", DbType.Decimal, vacanza.Penalita);
                    DatabaseCemi.AddInParameter(comando, "@costoDisablie", DbType.Decimal, vacanza.CostoDisabile);
                    DatabaseCemi.AddInParameter(comando, "@limitePenalita", DbType.Int32, vacanza.LimitePenalita);
                    DatabaseCemi.AddInParameter(comando, "@attiva", DbType.Boolean, vacanza.Attiva);
                    if (vacanza.DataInizioPrenotazioni.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioPrenotazioni", DbType.DateTime,
                            vacanza.DataInizioPrenotazioni);
                    if (vacanza.DataInizioPrenotazioni.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFinePrenotazioni", DbType.DateTime,
                            vacanza.DataFinePrenotazioni);
                    if (vacanza.DataInizioDomandeACE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioDomandeACE", DbType.DateTime,
                            vacanza.DataInizioDomandeACE);
                    if (vacanza.DataInizioDomandeACE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFineDomandeACE", DbType.DateTime,
                            vacanza.DataFineDomandeACE);
                    DatabaseCemi.AddInParameter(comando, "@limiteInferioreBimbi", DbType.DateTime,
                        vacanza.LimiteInferioreBimbi);
                    DatabaseCemi.AddInParameter(comando, "@limiteSuperioreBimbi", DbType.DateTime,
                        vacanza.LimiteSuperioreBimbi);
                    if (vacanza.DataInizioRichieste.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataInizioRichieste", DbType.DateTime,
                            vacanza.DataInizioRichieste.Value);
                    }
                    if (vacanza.DataFineRichieste.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataFineRichieste", DbType.DateTime,
                            vacanza.DataFineRichieste.Value);
                    }

                    if (DatabaseCemi.ExecuteNonQuery(comando) > 0)
                        res = true;
                }
            }

            return res;
        }

        public bool InsertVacanza(Vacanza vacanza)
        {
            bool res = false;

            if (!vacanza.IdVacanza.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzeInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, vacanza.Anno);
                    DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32,
                        vacanza.TipoVacanza.IdTipoVacanza);
                    DatabaseCemi.AddInParameter(comando, "@costoGiornaliero", DbType.Decimal, vacanza.CostoGiornaliero);
                    DatabaseCemi.AddInParameter(comando, "@costoGiornalieroAccompagnatore", DbType.Decimal,
                        vacanza.CostoGiornalieroAccompagnatore);
                    DatabaseCemi.AddInParameter(comando, "@costoPenalita", DbType.Decimal, vacanza.Penalita);
                    DatabaseCemi.AddInParameter(comando, "@costoDisablie", DbType.Decimal, vacanza.CostoDisabile);
                    DatabaseCemi.AddInParameter(comando, "@limitePenalita", DbType.Int32, vacanza.LimitePenalita);
                    DatabaseCemi.AddInParameter(comando, "@attiva", DbType.Boolean, vacanza.Attiva);
                    if (vacanza.DataInizioPrenotazioni.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioPrenotazioni", DbType.DateTime,
                            vacanza.DataInizioPrenotazioni);
                    if (vacanza.DataInizioPrenotazioni.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFinePrenotazioni", DbType.DateTime,
                            vacanza.DataFinePrenotazioni);
                    if (vacanza.DataInizioDomandeACE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataInizioDomandeACE", DbType.DateTime,
                            vacanza.DataInizioDomandeACE);
                    if (vacanza.DataInizioDomandeACE.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataFineDomandeACE", DbType.DateTime,
                            vacanza.DataFineDomandeACE);
                    DatabaseCemi.AddInParameter(comando, "@limiteInferioreBimbi", DbType.DateTime,
                        vacanza.LimiteInferioreBimbi);
                    DatabaseCemi.AddInParameter(comando, "@limiteSuperioreBimbi", DbType.DateTime,
                        vacanza.LimiteSuperioreBimbi);
                    if (vacanza.DataInizioRichieste.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataInizioRichieste", DbType.DateTime,
                            vacanza.DataInizioRichieste.Value);
                    }
                    if (vacanza.DataFineRichieste.HasValue)
                    {
                        DatabaseCemi.AddInParameter(comando, "@dataFineRichieste", DbType.DateTime,
                            vacanza.DataFineRichieste.Value);
                    }

                    if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                        res = true;
                }
            }

            return res;
        }

        public int? GetIdVacanzaAttiva()
        {
            int? idVacanza = null;

            using (DbCommand comando = DatabaseCemi.GetSqlStringCommand("SELECT [dbo].[UF_ColonieIdVacanzaAttiva]()"))
            {
                object res = DatabaseCemi.ExecuteScalar(comando);
                if (res != null)
                    idVacanza = (int) res;
            }

            return idVacanza;
        }

        public MatriceTurni GetMatriceRichiesteDisponibilita(int annoParam, int idTipoVacanzaParam)
        {
            MatriceTurni matrice = new MatriceTurni();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTurniSelectPerMatrice"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, annoParam);
                DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanzaParam);

                using (DataSet dsMatrice = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsMatrice != null && dsMatrice.Tables.Count == 1)
                    {
                        RigaMatriceTurni riga = null;

                        for (int i = 0; i < dsMatrice.Tables[0].Rows.Count; i++)
                        {
                            int idTurno;
                            int progressivo;
                            DateTime dal;
                            DateTime al;
                            int idDestinazione;
                            int idVacanza;
                            int postiDisponibili;
                            string destinazione;
                            int idTipoDestinazione;
                            string tipoDestinazione;
                            bool attiva;
                            int postiOccupati;

                            idTurno = (int) dsMatrice.Tables[0].Rows[i]["idColonieTurno"];
                            progressivo = (int) dsMatrice.Tables[0].Rows[i]["progressivo"];
                            dal = (DateTime) dsMatrice.Tables[0].Rows[i]["dal"];
                            al = (DateTime) dsMatrice.Tables[0].Rows[i]["al"];
                            idDestinazione = (int) dsMatrice.Tables[0].Rows[i]["idColonieDestinazione"];
                            idVacanza = (int) dsMatrice.Tables[0].Rows[i]["idColonieVacanza"];
                            postiDisponibili = (int) dsMatrice.Tables[0].Rows[i]["postiDisponibili"];
                            idTipoDestinazione = (int) dsMatrice.Tables[0].Rows[i]["idColonieTipoDestinazione"];
                            destinazione = (string) dsMatrice.Tables[0].Rows[i]["luogo"];
                            attiva = (bool) dsMatrice.Tables[0].Rows[i]["attiva"];
                            postiOccupati = (int) dsMatrice.Tables[0].Rows[i]["postiOccupati"] +
                                            (int) dsMatrice.Tables[0].Rows[i]["accompagnatoriOccupati"];
                            tipoDestinazione = (string) dsMatrice.Tables[0].Rows[i]["tipoDestinazione"];

                            if (idTurno != -1 || idTurno == -1 && postiOccupati != 0)
                            {
                                if (riga == null || riga.Destinazione.IdDestinazione.Value != idDestinazione)
                                {
                                    riga = new RigaMatriceTurni(new Destinazione(idDestinazione,
                                            new TipoDestinazione(
                                                idTipoDestinazione,
                                                tipoDestinazione, null),
                                            destinazione, attiva),
                                        new TurnoCollection());

                                    matrice.Add(riga);
                                }

                                riga.Turni.Add(new Turno(idTurno, progressivo, dal, al, new Destinazione(null,
                                        new TipoDestinazione
                                        (
                                            idTipoDestinazione,
                                            tipoDestinazione,
                                            null),
                                        destinazione,
                                        true),
                                    idDestinazione, idVacanza, postiDisponibili, postiOccupati, 0));
                            }
                        }
                    }
                }
            }

            return matrice;
        }

        public DomandaCollection GetDomandeLiquidate(int? idVacanzaParam, int? idDestinazioneParam, string cognomeParam,
            int? idTurnoParam, bool? annullate, string cassaEdileParam, bool? nonAssegnate)
        {
            DomandaCollection domande = new DomandaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeSelectLiquidate"))
            {
                if (idVacanzaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanzaParam.Value);
                if (idDestinazioneParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDestinazione", DbType.Int32, idDestinazioneParam.Value);
                if (!string.IsNullOrEmpty(cognomeParam))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognomeParam);
                if (idTurnoParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurnoParam.Value);
                if (annullate.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@annullate", DbType.Boolean, annullate.Value);
                if (!string.IsNullOrEmpty(cassaEdileParam))
                    DatabaseCemi.AddInParameter(comando, "@cassaEdile", DbType.String, cassaEdileParam);
                DatabaseCemi.AddInParameter(comando, "@nonAssegnate", DbType.Boolean, nonAssegnate);

                using (DataSet dsDomande = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsDomande != null && dsDomande.Tables.Count == 1)
                    {
                        Domanda domandaPrec = null;

                        for (int i = 0; i < dsDomande.Tables[0].Rows.Count; i++)
                        {
                            int idDomanda;
                            string cognome;
                            string nome;
                            DateTime? dataNascita = null;
                            //int idVacanza = -1;
                            int idTurno = -1;
                            int? idAutobus;
                            Autobus autobus = null;
                            DateTime? rientroAnticipato = null;
                            DateTime? richiestaAnnullamento = null;
                            string stato;
                            string cassaEdile = null;
                            bool? imbarcato = null;
                            bool? presenteColonia = null;
                            bool handicap;
                            DateTime? partenzaPosticipata = null;

                            idDomanda = (int) dsDomande.Tables[0].Rows[i]["idColonieDomanda"];
                            cognome = (string) dsDomande.Tables[0].Rows[i]["cognome"];
                            nome = (string) dsDomande.Tables[0].Rows[i]["nome"];
                            stato = (string) dsDomande.Tables[0].Rows[i]["stato"];
                            if (dsDomande.Tables[0].Rows[i]["dataNascita"] != DBNull.Value)
                                dataNascita = (DateTime) dsDomande.Tables[0].Rows[i]["dataNascita"];
                            //idVacanza = (int) dsDomande.Tables[0].Rows[i]["idColonieVacanza"];
                            if (dsDomande.Tables[0].Rows[i]["idColonieTurno"] != DBNull.Value)
                                idTurno = (int) dsDomande.Tables[0].Rows[i]["idColonieTurno"];
                            if (dsDomande.Tables[0].Rows[i]["idColonieAutobus"] != DBNull.Value)
                            {
                                idAutobus = (int) dsDomande.Tables[0].Rows[i]["idColonieAutobus"];
                                string codice = (string) dsDomande.Tables[0].Rows[i]["codice"];
                                string targa = (string) dsDomande.Tables[0].Rows[i]["targa"];

                                autobus = new Autobus(idAutobus.Value, codice, targa, idTurno);
                            }

                            if (dsDomande.Tables[0].Rows[i]["idCassaEdile"] != DBNull.Value)
                                cassaEdile = (string) dsDomande.Tables[0].Rows[i]["idCassaEdile"];

                            if (domandaPrec == null || domandaPrec.IdDomanda != idDomanda)
                            {
                                domandaPrec = new Domanda(idDomanda, cognome, nome, dataNascita, stato, cassaEdile);
                                domande.Add(domandaPrec);

                                if (dsDomande.Tables[0].Rows[i]["accompagnatore"] != DBNull.Value)
                                    domandaPrec.Accompagnatore = (string) dsDomande.Tables[0].Rows[i]["accompagnatore"];
                            }

                            if (dsDomande.Tables[0].Rows[i]["rientroAnticipato"] != DBNull.Value)
                                rientroAnticipato = (DateTime) dsDomande.Tables[0].Rows[i]["rientroAnticipato"];
                            if (dsDomande.Tables[0].Rows[i]["richiestaAnnullamento"] != DBNull.Value)
                                richiestaAnnullamento = (DateTime) dsDomande.Tables[0].Rows[i]["richiestaAnnullamento"];

                            if (dsDomande.Tables[0].Rows[i]["imbarcato"] != DBNull.Value)
                                imbarcato = (bool) dsDomande.Tables[0].Rows[i]["imbarcato"];
                            if (dsDomande.Tables[0].Rows[i]["presenteColonia"] != DBNull.Value)
                                presenteColonia = (bool) dsDomande.Tables[0].Rows[i]["presenteColonia"];

                            handicap = (bool) dsDomande.Tables[0].Rows[i]["portatoreHandicap"];

                            if (dsDomande.Tables[0].Rows[i]["partenzaPosticipata"] != DBNull.Value)
                                partenzaPosticipata = (DateTime) dsDomande.Tables[0].Rows[i]["partenzaPosticipata"];

                            if (idTurno != -1)
                                domandaPrec.Turni.Add(
                                    new TurnoDomanda(idTurno, idDomanda, autobus, imbarcato, rientroAnticipato,
                                        richiestaAnnullamento, null, presenteColonia, handicap, false,
                                        partenzaPosticipata));
                        }
                    }
                }
            }

            return domande;
        }

        public DomandaACEEffettivaCollection GetDomandeACEEffettive(int? idVacanzaParam, int? idDestinazioneParam,
            string cognomeParam,
            int? idTurnoParam, bool? annullate,
            string cassaEdileParam)
        {
            DomandaACEEffettivaCollection domande = new DomandaACEEffettivaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACEEffettiveSelect"))
            {
                if (idVacanzaParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanzaParam.Value);
                if (idDestinazioneParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDestinazione", DbType.Int32, idDestinazioneParam.Value);
                if (!string.IsNullOrEmpty(cognomeParam))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognomeParam);
                if (idTurnoParam.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurnoParam.Value);
                if (annullate.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@annullate", DbType.Boolean, annullate.Value);
                if (!string.IsNullOrEmpty(cassaEdileParam))
                    DatabaseCemi.AddInParameter(comando, "@cassaEdile", DbType.String, cassaEdileParam);

                using (DataSet dsDomande = DatabaseCemi.ExecuteDataSet(comando))
                {
                    if (dsDomande != null && dsDomande.Tables.Count == 1)
                    {
                        DomandaACEEffettiva domandaPrec = null;

                        for (int i = 0; i < dsDomande.Tables[0].Rows.Count; i++)
                        {
                            int idDomanda;
                            int idTurno = -1;
                            int numeroCorredo;
                            string stato;

                            string cognome;
                            string nome;
                            string sesso;
                            DateTime? dataNascita = null;

                            DateTime? rientroAnticipato = null;
                            DateTime? richiestaAnnullamento = null;
                            DateTime? partenzaPosticipata = null;
                            bool? presenteColonia = null;

                            bool? handicap = null;
                            bool? intolleranzeAlimentari = null;

                            string descrizione = null;
                            int progressivo = -1;

                            string Accompagnatore = null;


                            idDomanda = (int) dsDomande.Tables[0].Rows[i]["idColonieDomanda"];
                            cognome = (string) dsDomande.Tables[0].Rows[i]["cognome"];
                            nome = (string) dsDomande.Tables[0].Rows[i]["nome"];
                            stato = (string) dsDomande.Tables[0].Rows[i]["stato"];

                            if (dsDomande.Tables[0].Rows[i]["dataNascita"] != DBNull.Value)
                                dataNascita = (DateTime) dsDomande.Tables[0].Rows[i]["dataNascita"];
                            //idVacanza = (int) dsDomande.Tables[0].Rows[i]["idColonieVacanza"];
                            if (dsDomande.Tables[0].Rows[i]["idColonieTurno"] != DBNull.Value)
                                idTurno = (int) dsDomande.Tables[0].Rows[i]["idColonieTurno"];

                            //if (dsDomande.Tables[0].Rows[i]["idCassaEdile"] != DBNull.Value)
                            //    cassaEdile = (string)dsDomande.Tables[0].Rows[i]["idCassaEdile"];

                            if (domandaPrec == null || domandaPrec.IdDomanda != idDomanda)
                            {
                                domandaPrec = new DomandaACEEffettiva(idDomanda, cognome, nome, dataNascita, stato);
                                domande.Add(domandaPrec);

                                if (dsDomande.Tables[0].Rows[i]["accompagnatore"] != DBNull.Value)
                                    domandaPrec.Accompagnatore = (string) dsDomande.Tables[0].Rows[i]["accompagnatore"];
                            }

                            if (dsDomande.Tables[0].Rows[i]["rientroAnticipato"] != DBNull.Value)
                                rientroAnticipato = (DateTime) dsDomande.Tables[0].Rows[i]["rientroAnticipato"];

                            if (dsDomande.Tables[0].Rows[i]["richiestaAnnullamento"] != DBNull.Value)
                                richiestaAnnullamento = (DateTime) dsDomande.Tables[0].Rows[i]["richiestaAnnullamento"];

                            if (dsDomande.Tables[0].Rows[i]["presenteColonia"] != DBNull.Value)
                                presenteColonia = (bool) dsDomande.Tables[0].Rows[i]["presenteColonia"];

                            if (dsDomande.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                                descrizione = (string) dsDomande.Tables[0].Rows[i]["descrizione"];

                            if (dsDomande.Tables[0].Rows[i]["progressivo"] != DBNull.Value)
                                progressivo = (int) dsDomande.Tables[0].Rows[i]["progressivo"];

                            handicap = (bool) dsDomande.Tables[0].Rows[i]["portatoreHandicap"];

                            if (dsDomande.Tables[0].Rows[i]["partenzaPosticipata"] != DBNull.Value)
                                partenzaPosticipata = (DateTime) dsDomande.Tables[0].Rows[i]["partenzaPosticipata"];

                            if (idTurno != -1)
                            {
                                TurnoDomanda td = new TurnoDomanda(idTurno, idDomanda, null, null, rientroAnticipato,
                                    richiestaAnnullamento, null, presenteColonia, handicap.Value, false,
                                    partenzaPosticipata, progressivo, descrizione);

                                td.RichiestaAnnullamentoEsplicita =
                                    dsDomande.Tables[0].Rows[i]["richiestaAnnullamentoEsplicita"] != DBNull.Value;

                                domandaPrec.Turni.Add(td);
                            }
                        }
                    }
                }
            }

            return domande;
        }

        public DomandaCollection GetDomandeLiquidateNonAssegnate(int idTipoVacanzaParam, int annoParam,
            string cognomeParam, string cassaEdileParam)
        {
            DomandaCollection domande = new DomandaCollection();

            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeSelectNonAssegnate");
            DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanzaParam);
            DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, annoParam);
            if (!string.IsNullOrEmpty(cognomeParam))
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognomeParam);
            if (!string.IsNullOrEmpty(cassaEdileParam))
                DatabaseCemi.AddInParameter(comando, "@cassaEdile", DbType.String, cassaEdileParam);

            DataSet dsDomande = DatabaseCemi.ExecuteDataSet(comando);

            if (dsDomande != null && dsDomande.Tables.Count == 1)
            {
                for (int i = 0; i < dsDomande.Tables[0].Rows.Count; i++)
                {
                    int idDomanda;
                    string cognome;
                    string nome;
                    DateTime? dataNascita = null;
                    int idVacanza;
                    string stato;
                    string cassaEdile = null;
                    //bool handicap = false;

                    idDomanda = (int) dsDomande.Tables[0].Rows[i]["idColonieDomanda"];
                    cognome = (string) dsDomande.Tables[0].Rows[i]["cognome"];
                    nome = (string) dsDomande.Tables[0].Rows[i]["nome"];
                    stato = (string) dsDomande.Tables[0].Rows[i]["stato"];
                    if (dsDomande.Tables[0].Rows[i]["dataNascita"] != DBNull.Value)
                        dataNascita = (DateTime) dsDomande.Tables[0].Rows[i]["dataNascita"];
                    idVacanza = (int) dsDomande.Tables[0].Rows[i]["idColonieVacanza"];

                    if (dsDomande.Tables[0].Rows[i]["idCassaEdile"] != DBNull.Value)
                        cassaEdile = (string) dsDomande.Tables[0].Rows[i]["idCassaEdile"];

                    //handicap = (bool) dsDomande.Tables[0].Rows[i]["portatoreHandicap"];

                    Domanda domanda = new Domanda(idDomanda, cognome, nome, dataNascita, stato, cassaEdile);
                    domanda.IdVacanza = idVacanza;
                    domande.Add(domanda);
                }
            }

            return domande;
        }

        public DomandaCollection GetDomande(int? idVacanzaParam, int? idDestinazioneParam, string cognomeParam,
            int? idTurnoParam, bool? annullate)
        {
            DomandaCollection domande = new DomandaCollection();

            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeSelect");
            if (idVacanzaParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanzaParam.Value);
            if (idDestinazioneParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idDestinazione", DbType.Int32, idDestinazioneParam.Value);
            if (!string.IsNullOrEmpty(cognomeParam))
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, cognomeParam);
            if (idTurnoParam.HasValue)
                DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurnoParam.Value);
            if (annullate.HasValue)
                DatabaseCemi.AddInParameter(comando, "@annullate", DbType.Boolean, annullate.Value);

            DataSet dsDomande = DatabaseCemi.ExecuteDataSet(comando);

            if (dsDomande != null && dsDomande.Tables.Count == 1)
            {
                Domanda domandaPrec = null;

                for (int i = 0; i < dsDomande.Tables[0].Rows.Count; i++)
                {
                    int idDomanda;
                    string cognome;
                    string nome;
                    DateTime? dataNascita = null;
                    //int idVacanza;
                    int idTurno = -1;
                    int? idAutobus;
                    Autobus autobus = null;
                    DateTime? rientroAnticipato = null;
                    DateTime? richiestaAnnullamento = null;
                    string stato;
                    string cassaEdile = null;
                    bool? imbarcato = null;
                    bool? presenteColonia = null;
                    bool handicap;
                    DateTime? partenzaPosticipata = null;

                    idDomanda = (int) dsDomande.Tables[0].Rows[i]["idColonieDomanda"];
                    cognome = (string) dsDomande.Tables[0].Rows[i]["cognome"];
                    nome = (string) dsDomande.Tables[0].Rows[i]["nome"];
                    stato = (string) dsDomande.Tables[0].Rows[i]["stato"];
                    if (dsDomande.Tables[0].Rows[i]["dataNascita"] != DBNull.Value)
                        dataNascita = (DateTime) dsDomande.Tables[0].Rows[i]["dataNascita"];
                    //idVacanza = (int) dsDomande.Tables[0].Rows[i]["idColonieVacanza"];
                    if (dsDomande.Tables[0].Rows[i]["idColonieTurno"] != DBNull.Value)
                        idTurno = (int) dsDomande.Tables[0].Rows[i]["idColonieTurno"];
                    if (dsDomande.Tables[0].Rows[i]["idColonieAutobus"] != DBNull.Value)
                    {
                        idAutobus = (int) dsDomande.Tables[0].Rows[i]["idColonieAutobus"];
                        string codice = (string) dsDomande.Tables[0].Rows[i]["codice"];
                        string targa = (string) dsDomande.Tables[0].Rows[i]["targa"];

                        autobus = new Autobus(idAutobus.Value, codice, targa, idTurno);
                    }

                    if (dsDomande.Tables[0].Rows[i]["idCassaEdile"] != DBNull.Value)
                        cassaEdile = (string) dsDomande.Tables[0].Rows[i]["idCassaEdile"];

                    if (domandaPrec == null || domandaPrec.IdDomanda != idDomanda)
                    {
                        domandaPrec = new Domanda(idDomanda, cognome, nome, dataNascita, stato, cassaEdile);
                        domande.Add(domandaPrec);
                    }

                    if (dsDomande.Tables[0].Rows[i]["rientroAnticipato"] != DBNull.Value)
                        rientroAnticipato = (DateTime) dsDomande.Tables[0].Rows[i]["rientroAnticipato"];
                    if (dsDomande.Tables[0].Rows[i]["richiestaAnnullamento"] != DBNull.Value)
                        richiestaAnnullamento = (DateTime) dsDomande.Tables[0].Rows[i]["richiestaAnnullamento"];

                    if (dsDomande.Tables[0].Rows[i]["imbarcato"] != DBNull.Value)
                        imbarcato = (bool) dsDomande.Tables[0].Rows[i]["imbarcato"];
                    if (dsDomande.Tables[0].Rows[i]["presenteColonia"] != DBNull.Value)
                        presenteColonia = (bool) dsDomande.Tables[0].Rows[i]["presenteColonia"];

                    handicap = (bool) dsDomande.Tables[0].Rows[i]["portatoreHandicap"];

                    if (dsDomande.Tables[0].Rows[i]["partenzaPosticipata"] != DBNull.Value)
                        partenzaPosticipata = (DateTime) dsDomande.Tables[0].Rows[i]["partenzaPosticipata"];

                    if (idTurno != -1)
                        domandaPrec.Turni.Add(
                            new TurnoDomanda(idTurno, idDomanda, autobus, imbarcato, rientroAnticipato,
                                richiestaAnnullamento, null, presenteColonia, handicap, false,
                                partenzaPosticipata));
                }
            }

            return domande;
        }

        public TurnoPresenzaCollection GetPresenzeTurno(int idDomanda)
        {
            TurnoPresenzaCollection turnoPres = new TurnoPresenzaCollection();

            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTurniSelectPresenza");
            DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

            DataSet dsPresenzaTurno = DatabaseCemi.ExecuteDataSet(comando);

            if (dsPresenzaTurno != null && dsPresenzaTurno.Tables.Count == 1)
            {
                for (int i = 0; i < dsPresenzaTurno.Tables[0].Rows.Count; i++)
                {
                    int idTurno;
                    int progressivo;
                    string destinazione;
                    int idTipoDestinazione;
                    string tipoDestinazione;
                    int idDestinazione;
                    bool attiva;
                    bool presente = true;
                    //int idVacanza;

                    idTurno = (int) dsPresenzaTurno.Tables[0].Rows[i]["idColonieTurno"];
                    progressivo = (int) dsPresenzaTurno.Tables[0].Rows[i]["progressivo"];
                    idDestinazione = (int) dsPresenzaTurno.Tables[0].Rows[i]["idColonieDestinazione"];
                    //idVacanza = (int) dsPresenzaTurno.Tables[0].Rows[i]["idColonieVacanza"];
                    idTipoDestinazione = (int) dsPresenzaTurno.Tables[0].Rows[i]["idColonieTipoDestinazione"];
                    destinazione = (string) dsPresenzaTurno.Tables[0].Rows[i]["luogo"];
                    attiva = (bool) dsPresenzaTurno.Tables[0].Rows[i]["attiva"];
                    tipoDestinazione = (string) dsPresenzaTurno.Tables[0].Rows[i]["descrizione"];

                    if (dsPresenzaTurno.Tables[0].Rows[i]["idColonieDomanda"] == DBNull.Value)
                        presente = false;

                    turnoPres.Add(new TurnoPresenza(idDomanda, new Destinazione(idDestinazione,
                            new TipoDestinazione(
                                idTipoDestinazione, tipoDestinazione,
                                null), destinazione, attiva),
                        progressivo, idTurno, presente));
                }
            }

            return turnoPres;
        }

        public bool SalvaPresenza(int idDomanda, List<int> turni)
        {
            bool res = true;

            using (DbConnection conn = DatabaseCemi.CreateConnection())
            {
                conn.Open();

                using (DbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        DbCommand comando;
                        comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandaTurniDelete");
                        DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                        DatabaseCemi.ExecuteNonQuery(comando, tran);

                        foreach (int idTurno in turni)
                        {
                            DbCommand comandoIns;
                            comandoIns = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandaTurniInsert");
                            DatabaseCemi.AddInParameter(comandoIns, "@idDomanda", DbType.Int32, idDomanda);
                            DatabaseCemi.AddInParameter(comandoIns, "@idTurno", DbType.Int32, idTurno);

                            if (DatabaseCemi.ExecuteNonQuery(comandoIns, tran) != 1)
                                throw new Exception();
                        }

                        tran.Commit();
                    }
                    catch
                    {
                        tran.Rollback();
                    }
                }
            }

            return res;
        }

        public TurnoDomanda GetTurnoDomanda(int idDomandaParam, int idTurnoParam)
        {
            DbCommand comando;
            TurnoDomanda turnoDomanda = null;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandaTurniSelect");
            DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomandaParam);
            DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurnoParam);

            DataSet dsTurnoDomanda = DatabaseCemi.ExecuteDataSet(comando);

            if (dsTurnoDomanda != null && dsTurnoDomanda.Tables.Count == 1 &&
                dsTurnoDomanda.Tables[0].Rows.Count == 1)
            {
                int idTurno;
                int idDomanda;
                DateTime? rientroAnticipato = null;
                DateTime? richiestaAnnullamento = null;
                bool? imbarcato = null;
                string mancatoImbarco = null;
                bool? presenteColonia = null;
                bool handicap;
                bool intolleranze;
                DateTime? partenzaPosticipata = null;

                Autobus autobus = null;

                idTurno = (int) dsTurnoDomanda.Tables[0].Rows[0]["idColonieTurno"];
                idDomanda = (int) dsTurnoDomanda.Tables[0].Rows[0]["idColonieDomanda"];
                if (dsTurnoDomanda.Tables[0].Rows[0]["idColonieAutobus"] != DBNull.Value)
                {
                    int idAutobus = (int) dsTurnoDomanda.Tables[0].Rows[0]["idColonieAutobus"];
                    string codiceAutobus = (string) dsTurnoDomanda.Tables[0].Rows[0]["codice"];
                    string targaAutobus = (string) dsTurnoDomanda.Tables[0].Rows[0]["targa"];
                    autobus = new Autobus(idAutobus, codiceAutobus, targaAutobus, idTurno);
                }

                if (dsTurnoDomanda.Tables[0].Rows[0]["rientroAnticipato"] != DBNull.Value)
                    rientroAnticipato = (DateTime) dsTurnoDomanda.Tables[0].Rows[0]["rientroAnticipato"];
                if (dsTurnoDomanda.Tables[0].Rows[0]["richiestaAnnullamento"] != DBNull.Value)
                    richiestaAnnullamento = (DateTime) dsTurnoDomanda.Tables[0].Rows[0]["richiestaAnnullamento"];
                if (dsTurnoDomanda.Tables[0].Rows[0]["imbarcato"] != DBNull.Value)
                    imbarcato = (bool) dsTurnoDomanda.Tables[0].Rows[0]["imbarcato"];
                if (dsTurnoDomanda.Tables[0].Rows[0]["motivoMancatoImbarco"] != DBNull.Value)
                    mancatoImbarco = (string) dsTurnoDomanda.Tables[0].Rows[0]["motivoMancatoImbarco"];
                if (dsTurnoDomanda.Tables[0].Rows[0]["presenteColonia"] != DBNull.Value)
                    presenteColonia = (bool) dsTurnoDomanda.Tables[0].Rows[0]["presenteColonia"];

                handicap = (bool) dsTurnoDomanda.Tables[0].Rows[0]["portatoreHandicap"];
                intolleranze = (bool) dsTurnoDomanda.Tables[0].Rows[0]["intolleranzeAlimentari"];

                if (dsTurnoDomanda.Tables[0].Rows[0]["partenzaPosticipata"] != DBNull.Value)
                    partenzaPosticipata = (DateTime) dsTurnoDomanda.Tables[0].Rows[0]["partenzaPosticipata"];

                turnoDomanda =
                    new TurnoDomanda(idTurno, idDomanda, autobus, imbarcato, rientroAnticipato, richiestaAnnullamento,
                        mancatoImbarco, presenteColonia, handicap, intolleranze, partenzaPosticipata);

                turnoDomanda.TipoDomanda = (TipoDomanda) dsTurnoDomanda.Tables[0].Rows[0]["tipoDomanda"];
            }

            return turnoDomanda;
        }

        public AutobusCollection GetAutobus(int idTurnoParam)
        {
            AutobusCollection autobus = new AutobusCollection();
            DbCommand comando;

            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieAutobusSelect");
            DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurnoParam);

            DataSet dsAutobus = DatabaseCemi.ExecuteDataSet(comando);

            if (dsAutobus != null && dsAutobus.Tables.Count == 1)
            {
                for (int i = 0; i < dsAutobus.Tables[0].Rows.Count; i++)
                {
                    int idAutobus;
                    string codiceAutobus;
                    string targaAutobus;
                    int idTurno;

                    idAutobus = (int) dsAutobus.Tables[0].Rows[i]["idColonieAutobus"];
                    codiceAutobus = (string) dsAutobus.Tables[0].Rows[i]["codice"];
                    targaAutobus = (string) dsAutobus.Tables[0].Rows[i]["targa"];
                    idTurno = (int) dsAutobus.Tables[0].Rows[i]["idColonieTurno"];

                    autobus.Add(new Autobus(idAutobus, codiceAutobus, targaAutobus, idTurno));
                }
            }

            return autobus;
        }

        public bool InsertAutobus(Autobus autobus)
        {
            bool res = false;

            if (!autobus.IdAutobus.HasValue && !string.IsNullOrEmpty(autobus.CodiceAutobus) &&
                !string.IsNullOrEmpty(autobus.Targa))
            {
                DbCommand comando;
                comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieAutobusInsert");
                DatabaseCemi.AddInParameter(comando, "@codice", DbType.String, autobus.CodiceAutobus);
                DatabaseCemi.AddInParameter(comando, "@targa", DbType.String, autobus.Targa);
                DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, autobus.IdTurno);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool UpdateTurnoDomanda(TurnoDomanda turnoDomanda)
        {
            bool res = false;

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandaTurniUpdate");
            DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, turnoDomanda.IdTurno);
            DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, turnoDomanda.IdDomanda);

            if (turnoDomanda.Autobus != null)
                DatabaseCemi.AddInParameter(comando, "@idAutobus", DbType.Int32, turnoDomanda.Autobus.IdAutobus.Value);
            if (turnoDomanda.Imbarcato.HasValue)
                DatabaseCemi.AddInParameter(comando, "@imbarcato", DbType.Boolean, turnoDomanda.Imbarcato.Value);
            if (turnoDomanda.RichiestaAnnullamento.HasValue)
                DatabaseCemi.AddInParameter(comando, "@richiestaAnnullamento", DbType.DateTime,
                    turnoDomanda.RichiestaAnnullamento.Value);
            if (turnoDomanda.RientroAnticipato.HasValue)
                DatabaseCemi.AddInParameter(comando, "@rientroAnticipato", DbType.DateTime,
                    turnoDomanda.RientroAnticipato.Value);
            if (!string.IsNullOrEmpty(turnoDomanda.MancatoImbarco))
                DatabaseCemi.AddInParameter(comando, "@motivoMancatoImbarco", DbType.String,
                    turnoDomanda.MancatoImbarco);
            if (turnoDomanda.PresenteColonia.HasValue)
                DatabaseCemi.AddInParameter(comando, "@presenteColonia", DbType.Boolean,
                    turnoDomanda.PresenteColonia.Value);
            if (turnoDomanda.PartenzaPosticipata.HasValue)
                DatabaseCemi.AddInParameter(comando, "@partenzaPosticipata", DbType.DateTime,
                    turnoDomanda.PartenzaPosticipata.Value);

            DatabaseCemi.AddInParameter(comando, "@handicap", DbType.Boolean, turnoDomanda.PortatoreHandicap);
            DatabaseCemi.AddInParameter(comando, "@intolleranze", DbType.Boolean, turnoDomanda.IntolleranzeAlimentari);

            DatabaseCemi.AddInParameter(comando, "@tipoDomanda", DbType.Int32, turnoDomanda.TipoDomanda);

            int tempRes = DatabaseCemi.ExecuteNonQuery(comando);
            if (turnoDomanda.TipoDomanda == TipoDomanda.CE && tempRes == 2 ||
                turnoDomanda.TipoDomanda == TipoDomanda.ACE && tempRes == 1)
                res = true;

            return res;
        }

        public DomandeStatoCollection GetDomandeAccettate(int idColonieTurno)
        {
            DomandeStatoCollection domandeAccettate = new DomandeStatoCollection();

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeAccettateSelect");
            DatabaseCemi.AddInParameter(comando, "@idColonieTurno", DbType.Int32, idColonieTurno);

            DataSet dsDomandeAccettate = DatabaseCemi.ExecuteDataSet(comando);

            if (dsDomandeAccettate != null && dsDomandeAccettate.Tables.Count == 1)
            {
                for (int i = 0; i < dsDomandeAccettate.Tables[0].Rows.Count; i++)
                {
                    string stato;
                    string indirizzoSesso;
                    string indirizzoCognome;
                    string indirizzoNome;
                    string indirizzoDenominazione = null;
                    string indirizzoCAP = null;
                    string indirizzoComune = null;
                    string indirizzoProvincia = null;
                    string cognomePartecipante;
                    string nomePartecipante;
                    string tipoVacanza;
                    string luogoVacanza;
                    DateTime? dataInizio;
                    DateTime? dataFine;
                    int? numeroCorredo;

                    stato = "L"; //(string)dsDomandeAccettate.Tables[0].Rows[i]["stato"];
                    indirizzoSesso = (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoSesso"];
                    indirizzoCognome = (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoCognome"];
                    indirizzoNome = (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoNome"];
                    if (dsDomandeAccettate.Tables[0].Rows[i]["indirizzoDenominazione"] != DBNull.Value)
                        indirizzoDenominazione =
                            (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoDenominazione"];
                    if (dsDomandeAccettate.Tables[0].Rows[i]["indirizzoCAP"] != DBNull.Value)
                        indirizzoCAP = (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoCAP"];
                    if (dsDomandeAccettate.Tables[0].Rows[i]["indirizzoComune"] != DBNull.Value)
                        indirizzoComune = (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoComune"];
                    if (dsDomandeAccettate.Tables[0].Rows[i]["indirizzoProvincia"] != DBNull.Value)
                        indirizzoProvincia = (string) dsDomandeAccettate.Tables[0].Rows[i]["indirizzoProvincia"];
                    cognomePartecipante = (string) dsDomandeAccettate.Tables[0].Rows[i]["cognomePartecipante"];
                    nomePartecipante = (string) dsDomandeAccettate.Tables[0].Rows[i]["nomePartecipante"];
                    tipoVacanza = (string) dsDomandeAccettate.Tables[0].Rows[i]["tipoVacanza"];
                    luogoVacanza = (string) dsDomandeAccettate.Tables[0].Rows[i]["luogoVacanza"];
                    dataInizio = (DateTime) dsDomandeAccettate.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime) dsDomandeAccettate.Tables[0].Rows[i]["dataFine"];
                    numeroCorredo = (int) dsDomandeAccettate.Tables[0].Rows[i]["numeroCorredo"];

                    domandeAccettate.Add(
                        new DomandeStato(stato, indirizzoSesso, indirizzoCognome, indirizzoNome, indirizzoDenominazione,
                            indirizzoCAP,
                            indirizzoComune, indirizzoProvincia, cognomePartecipante, nomePartecipante,
                            tipoVacanza, luogoVacanza, dataInizio, dataFine, numeroCorredo));
                }
            }

            return domandeAccettate;
        }

        public DomandeStatoCollection GetDomandeNonAccettate(int idColonieTurno)
        {
            DomandeStatoCollection domandeNonAccettate = new DomandeStatoCollection();

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeNonAccettateSelect");
            DatabaseCemi.AddInParameter(comando, "@idColonieTurno", DbType.Int32, idColonieTurno);

            DataSet dsDomandeNonAccettate = DatabaseCemi.ExecuteDataSet(comando);

            if (dsDomandeNonAccettate != null && dsDomandeNonAccettate.Tables.Count == 1)
            {
                for (int i = 0; i < dsDomandeNonAccettate.Tables[0].Rows.Count; i++)
                {
                    string stato;
                    string indirizzoSesso;
                    string indirizzoCognome;
                    string indirizzoNome;
                    string indirizzoDenominazione = null;
                    string indirizzoCAP = null;
                    string indirizzoComune = null;
                    string indirizzoProvincia = null;
                    string causale = null;
                    //string cognomePartecipante = null;
                    //string nomePartecipante = null;
                    //string tipoVacanza = null;
                    //string luogoVacanza = null;
                    //DateTime dataInizio = null;
                    //DateTime dataFine = null;
                    //int numeroCorredo = 0;

                    stato = "R"; // (string)dsDomandeNonAccettate.Tables[0].Rows[i]["stato"];
                    indirizzoSesso = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoSesso"];
                    indirizzoCognome = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoCognome"];
                    indirizzoNome = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoNome"];
                    if (dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoDenominazione"] != DBNull.Value)
                        indirizzoDenominazione =
                            (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoDenominazione"];
                    if (dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoCAP"] != DBNull.Value)
                        indirizzoCAP = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoCAP"];
                    if (dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoComune"] != DBNull.Value)
                        indirizzoComune = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoComune"];
                    if (dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoProvincia"] != DBNull.Value)
                        indirizzoProvincia = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["indirizzoProvincia"];
                    if (dsDomandeNonAccettate.Tables[0].Rows[i]["causale"] != DBNull.Value)
                        causale = (string) dsDomandeNonAccettate.Tables[0].Rows[i]["causale"];
                    //cognomePartecipante = (string)dsDomandeAccettate.Tables[0].Rows[i]["cognomePartecipante"];
                    //nomePartecipante = (string)dsDomandeAccettate.Tables[0].Rows[i]["nomePartecipante"];
                    //tipoVacanza = (string)dsDomandeAccettate.Tables[0].Rows[i]["tipoVacanza"];
                    //luogoVacanza = (string)dsDomandeAccettate.Tables[0].Rows[i]["luogoVacanza"];
                    //dataInizio = (DateTime)dsDomandeAccettate.Tables[0].Rows[i]["dataInizio"];
                    //dataFine = (DateTime)dsDomandeAccettate.Tables[0].Rows[i]["dataFine"];
                    //numeroCorredo = (int)dsDomandeAccettate.Tables[0].Rows[i]["numeroCorredo"];

                    domandeNonAccettate.Add(
                        new DomandeStato(stato, indirizzoSesso, indirizzoCognome, indirizzoNome, indirizzoDenominazione,
                            indirizzoCAP,
                            indirizzoComune, indirizzoProvincia, causale));
                }
            }

            return domandeNonAccettate;
        }

        public SchedaBambinoCollection GetSchedaBambino(int? idDomandaParam, int idTurnoParam)
        {
            Lavoratore lavoratore;

            //SchedaBambino schedaBambino = null;
            SchedaBambinoCollection schedaBambino = new SchedaBambinoCollection();

            DbCommand comando1;
            comando1 = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeSelectSchedaBambino");
            if (idDomandaParam.HasValue)
                DatabaseCemi.AddInParameter(comando1, "@idDomanda", DbType.Int32, idDomandaParam.Value);
            //DatabaseCemi.AddInParameter(comando1, "@idDomanda", DbType.Int32, idDomandaParam);
            DatabaseCemi.AddInParameter(comando1, "@idTurno", DbType.Int32, idTurnoParam);

            DataSet dsSchedaBambino = DatabaseCemi.ExecuteDataSet(comando1);

            if (dsSchedaBambino != null && dsSchedaBambino.Tables.Count == 1)
                // && (dsSchedaBambino.Tables[0].Rows.Count == 1))
            {
                for (int i = 0; i < dsSchedaBambino.Tables[0].Rows.Count; i++)
                {
                    int idFamiliare;
                    int idLavoratore;
                    string cognomeP;
                    string nomeP;
                    string sessoP = null;
                    DateTime? dataNascitaP = null;
                    bool handicap;
                    bool intolleranze;
                    string dieta = string.Empty;
                    string protesi = string.Empty;
                    string allergie = string.Empty;
                    string terapie = string.Empty;
                    string nrTesseraSanitaria = string.Empty;

                    string nomeParente = string.Empty;
                    string cognomeParente = string.Empty;
                    int? numCorredoParente = null;


                    idFamiliare = (int) dsSchedaBambino.Tables[0].Rows[i]["idFamiliare"];
                    idLavoratore = (int) dsSchedaBambino.Tables[0].Rows[i]["idLavoratore"];
                    cognomeP = (string) dsSchedaBambino.Tables[0].Rows[i]["pCognome"];
                    nomeP = (string) dsSchedaBambino.Tables[0].Rows[i]["pNome"];

                    if (dsSchedaBambino.Tables[0].Rows[i]["pSesso"] != DBNull.Value)
                        sessoP = (string) dsSchedaBambino.Tables[0].Rows[i]["pSesso"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["pDataNascita"] != DBNull.Value)
                        dataNascitaP = (DateTime) dsSchedaBambino.Tables[0].Rows[i]["pDataNascita"];

                    handicap = (bool) dsSchedaBambino.Tables[0].Rows[i]["portatoreHandicap"];
                    intolleranze = (bool) dsSchedaBambino.Tables[0].Rows[i]["intolleranzeAlimentari"];

                    if (dsSchedaBambino.Tables[0].Rows[i]["nrTesseraSanitaria"] != DBNull.Value)
                        nrTesseraSanitaria = (string) dsSchedaBambino.Tables[0].Rows[i]["nrTesseraSanitaria"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["allergie"] != DBNull.Value)
                        allergie = (string) dsSchedaBambino.Tables[0].Rows[i]["allergie"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["terapieInCorso"] != DBNull.Value)
                        terapie = (string) dsSchedaBambino.Tables[0].Rows[i]["terapieInCorso"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["dieta"] != DBNull.Value)
                        dieta = (string) dsSchedaBambino.Tables[0].Rows[i]["dieta"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["protesiAusili"] != DBNull.Value)
                        protesi = (string) dsSchedaBambino.Tables[0].Rows[i]["protesiAusili"];

                    Partecipante partecipante =
                        new Partecipante(idFamiliare, cognomeP, nomeP, sessoP, dataNascitaP, nrTesseraSanitaria,
                            handicap, intolleranze, allergie, terapie, dieta, protesi);

                    string cognomeL;
                    string nomeL;
                    string indirizzoL = null;
                    string comuneL = null;
                    string provinciaL = null;
                    string capL = null;
                    string telefonoL = null;
                    string cellulareL = null;
                    string idCassaEdile = null;

                    cognomeL = (string) dsSchedaBambino.Tables[0].Rows[i]["lCognome"];
                    nomeL = (string) dsSchedaBambino.Tables[0].Rows[i]["lNome"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["lIndirizzo"] != DBNull.Value)
                        indirizzoL = (string) dsSchedaBambino.Tables[0].Rows[i]["lIndirizzo"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["lComune"] != DBNull.Value)
                        comuneL = (string) dsSchedaBambino.Tables[0].Rows[i]["lComune"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["lProvincia"] != DBNull.Value)
                        provinciaL = (string) dsSchedaBambino.Tables[0].Rows[i]["lProvincia"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["lCAP"] != DBNull.Value)
                        capL = (string) dsSchedaBambino.Tables[0].Rows[i]["lCAP"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["lTelefono"] != DBNull.Value)
                        telefonoL = (string) dsSchedaBambino.Tables[0].Rows[i]["lTelefono"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["lCellulare"] != DBNull.Value)
                        cellulareL = (string) dsSchedaBambino.Tables[0].Rows[i]["lCellulare"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["idCassaEdile"] != DBNull.Value)
                        idCassaEdile = (string) dsSchedaBambino.Tables[0].Rows[i]["idCassaEdile"];

                    lavoratore = new Lavoratore();
                    lavoratore.IdLavoratore = idLavoratore;
                    lavoratore.Cognome = cognomeL;
                    lavoratore.Nome = nomeL;
                    lavoratore.IndirizzoDenominazione = indirizzoL;
                    lavoratore.IndirizzoComune = comuneL;
                    lavoratore.IndirizzoProvincia = provinciaL;
                    lavoratore.IndirizzoCAP = capL;
                    lavoratore.Telefono = telefonoL;
                    lavoratore.Cellulare = cellulareL;
                    lavoratore.IdCassaEdile = idCassaEdile;

                    int numeroCorredo = (int) dsSchedaBambino.Tables[0].Rows[i]["numeroCorredo"];
                    string tipoDestinazione = (string) dsSchedaBambino.Tables[0].Rows[i]["tipoDestinazione"];
                    string tipoVacanza = (string) dsSchedaBambino.Tables[0].Rows[i]["tipoVacanza"];
                    int anno = (int) dsSchedaBambino.Tables[0].Rows[i]["anno"];
                    int progressivoTurno = (int) dsSchedaBambino.Tables[0].Rows[i]["progressivo"];

                    if (dsSchedaBambino.Tables[0].Rows[i]["pCognomeP"] != DBNull.Value)
                        cognomeParente = (string) dsSchedaBambino.Tables[0].Rows[i]["pCognomeP"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["pNomeP"] != DBNull.Value)
                        nomeParente = (string) dsSchedaBambino.Tables[0].Rows[i]["pNomeP"];
                    if (dsSchedaBambino.Tables[0].Rows[i]["numeroCorredoP"] != DBNull.Value)
                        numCorredoParente = (int) dsSchedaBambino.Tables[0].Rows[i]["numeroCorredoP"];

                    SchedaBambino scheda =
                        new SchedaBambino(partecipante, lavoratore, numeroCorredo, tipoDestinazione, progressivoTurno,
                            anno, tipoVacanza, cognomeParente, nomeParente, numCorredoParente);
                    schedaBambino.Add(scheda);

                    if (dsSchedaBambino.Tables[0].Rows[i]["accompagnatoreCognome"] != DBNull.Value)
                    {
                        scheda.Accompagnatore = new Accompagnatore();
                        scheda.Accompagnatore.Cognome =
                            (string) dsSchedaBambino.Tables[0].Rows[i]["accompagnatoreCognome"];
                        scheda.Accompagnatore.Nome = (string) dsSchedaBambino.Tables[0].Rows[i]["accompagnatoreNome"];
                        scheda.Accompagnatore.DataNascita =
                            (DateTime) dsSchedaBambino.Tables[0].Rows[i]["accompagnatoreDataNascita"];
                        scheda.Accompagnatore.Sesso =
                            ((string) dsSchedaBambino.Tables[0].Rows[i]["accompagnatoreSesso"])[0];
                    }

                    if (dsSchedaBambino.Tables[0].Rows[i]["taglia"] != DBNull.Value)
                    {
                        scheda.Taglia = dsSchedaBambino.Tables[0].Rows[i]["taglia"].ToString();
                    }
                }
            }

            //schedaBambino = new SchedaBambino(partecipante, lavoratore, numeroCorredo, tipoDestinazione, progressivoTurno, anno, tipoVacanza);

            return schedaBambino;
        }

        public StringCollection GetMotiviMancatoImbarco()
        {
            StringCollection motivi = new StringCollection();

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniMotiviMancatoImbarcoSelect");

            DataSet dsMotivi = DatabaseCemi.ExecuteDataSet(comando);

            if (dsMotivi != null && dsMotivi.Tables.Count == 1)
            {
                for (int i = 0; i < dsMotivi.Tables[0].Rows.Count; i++)
                {
                    motivi.Add(dsMotivi.Tables[0].Rows[i]["motivo"].ToString());
                }
            }

            return motivi;
        }

        public bool DeleteAutobus(int idAutobus)
        {
            bool res = false;

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieAutobusDelete");
            DatabaseCemi.AddInParameter(comando, "@idAutobus", DbType.Int32, idAutobus);

            if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                res = true;

            return res;
        }

        public StringCollection GetSovrapposizioniTurni(int idDomanda)
        {
            StringCollection sovrapposizioni = new StringCollection();

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeTurniVerificaSovrapposizioni");
            DatabaseCemi.AddInParameter(comando, "@idColonieDomanda", DbType.Int32, idDomanda);

            DataSet dsSovrap = DatabaseCemi.ExecuteDataSet(comando);

            if (dsSovrap != null && dsSovrap.Tables.Count == 1)
            {
                for (int i = 0; i < dsSovrap.Tables[0].Rows.Count; i++)
                {
                    sovrapposizioni.Add(dsSovrap.Tables[0].Rows[i]["sovrapposizione"].ToString());
                }
            }

            return sovrapposizioni;
        }

        public CassaEdileCollection GetCasseEdili()
        {
            CassaEdileCollection casse = new CassaEdileCollection();

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CasseEdiliSelect");

            DataSet dsCasse = DatabaseCemi.ExecuteDataSet(comando);

            if (dsCasse != null && dsCasse.Tables.Count == 1)
            {
                for (int i = 0; i < dsCasse.Tables[0].Rows.Count; i++)
                {
                    string idCassaEdile;
                    string descrizione = null;

                    idCassaEdile = (string) dsCasse.Tables[0].Rows[i]["idCassaEdile"];
                    if (dsCasse.Tables[0].Rows[i]["descrizione"] != DBNull.Value)
                        descrizione = (string) dsCasse.Tables[0].Rows[i]["descrizione"];

                    casse.Add(new CassaEdile(idCassaEdile, descrizione));
                }
            }

            return casse;
        }

        public CassaEdileCollection GetCasseEdiliConDomandeACE(int idVacanza)
        {
            CassaEdileCollection casse = new CassaEdileCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CasseEdiliSelectConDomandeACE"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile();
                        casse.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        if (reader["descrizione"] != DBNull.Value)
                            cassaEdile.Descrizione = (string) reader["descrizione"];
                    }
                }
            }

            return casse;
        }

        public Dictionary<string, int> GetPartecipantiTurnoPerCassa(int idTurno)
        {
            Dictionary<string, int> casse = new Dictionary<string, int>();

            DbCommand comando;
            comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTurniSelectDettaglioPerCassa");
            DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, idTurno);

            DataSet dsPart = DatabaseCemi.ExecuteDataSet(comando);

            if (dsPart != null && dsPart.Tables.Count == 1)
            {
                for (int i = 0; i < dsPart.Tables[0].Rows.Count; i++)
                {
                    string idCassaEdile;
                    int numPart;

                    idCassaEdile = (string) dsPart.Tables[0].Rows[i]["idCassaEdile"];
                    numPart = (int) dsPart.Tables[0].Rows[i]["num"];

                    casse.Add(idCassaEdile, numPart);
                }
            }

            return casse;
        }

        public PrenotazioniCassa GetPrenotazioniDellaCassa(string cassaEdile, int anno, int idTipoVacanza)
        {
            PrenotazioniCassa prenotazioni = new PrenotazioniCassa();
            prenotazioni.Prenotazioni = new PrenotazioneCollection();
            prenotazioni.IdCassaEdile = cassaEdile;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePrenotazioniSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, cassaEdile);
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        int tempOrdinal;
                        Prenotazione prenotazione = new Prenotazione();
                        Turno turno = new Turno();
                        Destinazione destinazione = new Destinazione();

                        // Destinazione
                        destinazione.IdDestinazione = reader.GetInt32(reader.GetOrdinal("idColonieDestinazione"));
                        destinazione.Luogo = reader.GetString(reader.GetOrdinal("luogo"));
                        destinazione.TipoDestinazione = new TipoDestinazione();
                        destinazione.TipoDestinazione.IdTipoDestinazione =
                            reader.GetInt32(reader.GetOrdinal("idColonieTipoDestinazione"));
                        destinazione.TipoDestinazione.Descrizione = reader.GetString(reader.GetOrdinal("descrizione"));

                        // Turno
                        turno.IdTurno = reader.GetInt32(reader.GetOrdinal("idColonieTurno"));
                        turno.ProgressivoTurno = reader.GetInt32(reader.GetOrdinal("progressivo"));
                        turno.Dal = reader.GetDateTime(reader.GetOrdinal("dal"));
                        turno.Al = reader.GetDateTime(reader.GetOrdinal("al"));
                        turno.PostiDisponibili = reader.GetInt32(reader.GetOrdinal("postiDisponibili"));
                        turno.Destinazione = destinazione;

                        // Prenotazione
                        tempOrdinal = reader.GetOrdinal("idColoniePrenotazione");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.IdPrenotazione = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("postiRichiesti");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.PostiRichiesti = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("postiConcessi");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.PostiConcessi = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("accettata");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.Accettata = reader.GetBoolean(tempOrdinal);
                        prenotazione.Turno = turno;

                        prenotazioni.Prenotazioni.Add(prenotazione);
                    }
                }
            }

            return prenotazioni;
        }

        public bool InsertPrenotazione(Prenotazione prenotazione, DbTransaction transaction)
        {
            bool res = false;

            if (prenotazione.IdTurno != -1 && !string.IsNullOrEmpty(prenotazione.IdCassaEdile) &&
                prenotazione.PostiRichiesti.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePrenotazioniInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, prenotazione.IdTurno);
                    DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, prenotazione.IdCassaEdile);
                    DatabaseCemi.AddInParameter(comando, "@postiRichiesti", DbType.Int32,
                        prenotazione.PostiRichiesti.Value);

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        public bool InsertPrenotazioni(PrenotazioneCollection prenotazioni)
        {
            bool res = true;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();
                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (Prenotazione prenotazione in prenotazioni)
                        {
                            if (!InsertPrenotazione(prenotazione, transaction))
                                throw new Exception("Errore nel salvataggio di una prenotazione");
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        res = false;
                        transaction.Rollback();
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return res;
        }

        public PrenotazioniTurnoCollection GetPrenotazioniPerTurno(int anno, int idTipoVacanza)
        {
            PrenotazioniTurnoCollection prenotazioni = new PrenotazioniTurnoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePrenotazioniSelectPerTurno"))
            {
                DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
                DatabaseCemi.AddInParameter(comando, "@idTipoVacanza", DbType.Int32, idTipoVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    PrenotazioniTurno prenotazioneTurno = null;
                    while (reader.Read())
                    {
                        Turno turno = new Turno();
                        turno.IdTurno = reader.GetInt32(reader.GetOrdinal("idColonieTurno"));

                        if (prenotazioneTurno == null || prenotazioneTurno.Turno.IdTurno.Value != turno.IdTurno)
                        {
                            // Altro turno
                            Destinazione destinazione = new Destinazione();

                            // Destinazione
                            destinazione.IdDestinazione = reader.GetInt32(reader.GetOrdinal("idColonieDestinazione"));
                            destinazione.Luogo = reader.GetString(reader.GetOrdinal("luogo"));
                            destinazione.TipoDestinazione = new TipoDestinazione();
                            destinazione.TipoDestinazione.IdTipoDestinazione =
                                reader.GetInt32(reader.GetOrdinal("idColonieTipoDestinazione"));
                            destinazione.TipoDestinazione.Descrizione =
                                reader.GetString(reader.GetOrdinal("descrizione"));

                            // Turno
                            turno.ProgressivoTurno = reader.GetInt32(reader.GetOrdinal("progressivo"));
                            turno.Dal = reader.GetDateTime(reader.GetOrdinal("dal"));
                            turno.Al = reader.GetDateTime(reader.GetOrdinal("al"));
                            turno.PostiDisponibili = reader.GetInt32(reader.GetOrdinal("postiDisponibili"));
                            turno.Destinazione = destinazione;

                            prenotazioneTurno = new PrenotazioniTurno();
                            prenotazioneTurno.Turno = turno;
                            prenotazioneTurno.Prenotazioni = new PrenotazioniCassaCollection();
                            prenotazioni.Add(prenotazioneTurno);
                        }

                        int tempOrdinal;
                        string idCassaEdile = null;
                        tempOrdinal = reader.GetOrdinal("idCassaEdile");
                        if (!reader.IsDBNull(tempOrdinal))
                            idCassaEdile = reader.GetString(tempOrdinal);

                        // Controllo se � presente una prenotazione
                        if (!string.IsNullOrEmpty(idCassaEdile))
                        {
                            PrenotazioniCassa prenotazioneCassa = new PrenotazioniCassa();
                            prenotazioneCassa.Prenotazioni = new PrenotazioneCollection();
                            Prenotazione prenotazione = new Prenotazione();

                            prenotazioneCassa.IdCassaEdile = idCassaEdile;
                            prenotazioneCassa.Prenotazioni.Add(prenotazione);
                            prenotazione.IdCassaEdile = idCassaEdile;

                            // Prenotazione
                            tempOrdinal = reader.GetOrdinal("idColoniePrenotazione");
                            if (!reader.IsDBNull(tempOrdinal))
                                prenotazione.IdPrenotazione = reader.GetInt32(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("postiRichiesti");
                            if (!reader.IsDBNull(tempOrdinal))
                                prenotazione.PostiRichiesti = reader.GetInt32(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("postiConcessi");
                            if (!reader.IsDBNull(tempOrdinal))
                                prenotazione.PostiConcessi = reader.GetInt32(tempOrdinal);
                            tempOrdinal = reader.GetOrdinal("accettata");
                            if (!reader.IsDBNull(tempOrdinal))
                                prenotazione.Accettata = reader.GetBoolean(tempOrdinal);

                            prenotazioneTurno.Prenotazioni.Add(prenotazioneCassa);
                        }
                    }
                }
            }

            return prenotazioni;
        }

        public Prenotazione GetPrenotazione(int idPrenotazioneParam)
        {
            Prenotazione prenotazione = new Prenotazione();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePrenotazioniSelectSingola"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrenotazione", DbType.Int32, idPrenotazioneParam);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        Turno turno = new Turno();
                        turno.IdTurno = reader.GetInt32(reader.GetOrdinal("idColonieTurno"));
                        Destinazione destinazione = new Destinazione();

                        // Destinazione
                        destinazione.IdDestinazione = reader.GetInt32(reader.GetOrdinal("idColonieDestinazione"));
                        destinazione.Luogo = reader.GetString(reader.GetOrdinal("luogo"));
                        destinazione.TipoDestinazione = new TipoDestinazione();
                        destinazione.TipoDestinazione.IdTipoDestinazione =
                            reader.GetInt32(reader.GetOrdinal("idColonieTipoDestinazione"));
                        destinazione.TipoDestinazione.Descrizione = reader.GetString(reader.GetOrdinal("descrizione"));

                        // Turno
                        turno.ProgressivoTurno = reader.GetInt32(reader.GetOrdinal("progressivo"));
                        turno.Dal = reader.GetDateTime(reader.GetOrdinal("dal"));
                        turno.Al = reader.GetDateTime(reader.GetOrdinal("al"));
                        turno.PostiDisponibili = reader.GetInt32(reader.GetOrdinal("postiDisponibili"));
                        turno.Destinazione = destinazione;
                        prenotazione.Turno = turno;

                        int tempOrdinal;
                        string idCassaEdile = null;
                        tempOrdinal = reader.GetOrdinal("idCassaEdile");
                        if (!reader.IsDBNull(tempOrdinal))
                            idCassaEdile = reader.GetString(tempOrdinal);

                        prenotazione.IdCassaEdile = idCassaEdile;

                        // Prenotazione
                        tempOrdinal = reader.GetOrdinal("idColoniePrenotazione");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.IdPrenotazione = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("postiRichiesti");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.PostiRichiesti = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("postiConcessi");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.PostiConcessi = reader.GetInt32(tempOrdinal);
                        tempOrdinal = reader.GetOrdinal("accettata");
                        if (!reader.IsDBNull(tempOrdinal))
                            prenotazione.Accettata = reader.GetBoolean(tempOrdinal);
                    }
                }
            }

            return prenotazione;
        }

        public bool UpdatePrenotazione(Prenotazione prenotazione)
        {
            bool res = false;

            if (prenotazione.IdPrenotazione.HasValue && prenotazione.Accettata.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePrenotazioniUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idPrenotazione", DbType.Int32,
                        prenotazione.IdPrenotazione.Value);
                    DatabaseCemi.AddInParameter(comando, "@accettata", DbType.Boolean, prenotazione.Accettata.Value);
                    if (prenotazione.PostiConcessi.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@postiConcessi", DbType.Int32,
                            prenotazione.PostiConcessi.Value);

                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        public SituazioneACECollection GetSituazioneDomande(string idCassaEdile, int idVacanza)
        {
            SituazioneACECollection situazioni = new SituazioneACECollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACESelectSituazionePerCassaEdile"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        SituazioneACE situazione = new SituazioneACE();
                        situazioni.Add(situazione);

                        situazione.IdTurno = (int) reader["idColonieTurno"];
                        situazione.Turno = (string) reader["descrizioneTurno"];
                        situazione.PostiDisponibili = (int) reader["postiDisponibili"];
                        situazione.PostiPrenotati = (int) reader["postiPrenotati"];
                        situazione.DomandeAccettate = (int) reader["domandeAccettate"];
                        situazione.AccompagnatoriAccettati = (int) reader["accompagnatoriAccettati"];
                        situazione.DomandeInCarico = (int) reader["domandeInCarico"];
                        situazione.AccompagnatoriInCarico = (int) reader["accompagnatoriInCarico"];
                        situazione.DomandeRifiutate = (int) reader["domandeRifiutate"];
                    }
                }
            }

            return situazioni;
        }

        public CassaEdileCollection GetAbilitazioniVacanza(int idVacanza)
        {
            CassaEdileCollection casseEdili = new CassaEdileCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzaAbilitazioniSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile();
                        casseEdili.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        if (reader["descrizione"] != DBNull.Value)
                            cassaEdile.Descrizione = (string) reader["descrizione"];
                        if (!Convert.IsDBNull(reader["email"]))
                            cassaEdile.Email = (string) reader["email"];
                    }
                }
            }

            return casseEdili;
        }

        public bool DeleteAbilitazioneVacanza(int idVacanza, string idCassaEdile)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzaAbilitazioniDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public CassaEdileCollection GetCasseEdiliNonAbilitate(CassaEdileFilter filtro)
        {
            CassaEdileCollection casseEdili = new CassaEdileCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CasseEdiliSelectRicercaNonAutorizzate"))
            {
                if (!string.IsNullOrEmpty(filtro.IdCassaEdile))
                    DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, filtro.IdCassaEdile);
                if (!string.IsNullOrEmpty(filtro.Descrizione))
                    DatabaseCemi.AddInParameter(comando, "@descrizione", DbType.String, filtro.Descrizione);
                if (filtro.IdVacanza.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, filtro.IdVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile();
                        casseEdili.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        if (reader["descrizione"] != DBNull.Value)
                            cassaEdile.Descrizione = (string) reader["descrizione"];
                    }
                }
            }

            return casseEdili;
        }

        public bool InsertAbilitazioneVacanza(int idVacanza, string idCassaEdile)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzaAbilitazioniInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool CassaEdileAbilitata(int idVacanza, string idCassaEdile)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieVacanzaAbilitazioniSelectCassaEdileAbilitata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddOutParameter(comando, "@abilitata", DbType.Boolean, 1);

                DatabaseCemi.ExecuteNonQuery(comando);

                res = (bool) DatabaseCemi.GetParameterValue(comando, "@abilitata");
            }

            return res;
        }

        public DataTable GetTaglie()
        {
            DataTable dt = null;

            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTaglieSelect");
            DataSet ds = DatabaseCemi.ExecuteDataSet(comando);

            if (ds != null && ds.Tables.Count > 0)
                dt = ds.Tables[0];

            return dt;
        }

        public int? GetUltimaTaglia(int idFamiliare)
        {
            int? idTaglia = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieTagliaSelectUltima"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        if (reader["idTaglia"] != DBNull.Value)
                        {
                            idTaglia = (int) reader["idTaglia"];
                        }
                    }
                }
            }

            return idTaglia;
        }

        public void InsertFiltroRicercaRichieste(int idUtente, RichiestaFilter filtro, int pagina)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException();
            }

            XmlSerializer ser = new XmlSerializer(typeof(RichiestaFilter));
            string filtroRichiesteSerializzato = string.Empty;
            if (filtro != null)
            {
                using (StringWriter sw = new StringWriter())
                {
                    ser.Serialize(sw, filtro);
                    filtroRichiesteSerializzato = sw.ToString();
                }
            }

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePersonaleFiltriRicercaInsertUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@filtroRicercaRichieste", DbType.Xml,
                    filtroRichiesteSerializzato);
                DatabaseCemi.AddInParameter(comando, "@paginaRicercaRichieste", DbType.Int32, pagina);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Errore durante l'inserimento del filtro di ricerca");
                }
            }
        }

        public RichiestaFilter GetFiltroRicercaRichieste(int idUtente, out int pagina)
        {
            RichiestaFilter filtro = null;
            pagina = 0;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePersonaleFiltriRicercaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        #region Indici reader

                        int indiceIdUtente = reader.GetOrdinal("idUtente");
                        int indiceFiltro = reader.GetOrdinal("filtroRicercaRichieste");
                        int indicePagina = reader.GetOrdinal("paginaRicercaRichieste");

                        #endregion

                        if (!reader.IsDBNull(indiceIdUtente))
                        {
                            string filtroSerializzato = null;

                            filtroSerializzato = reader.GetString(indiceFiltro);
                            pagina = reader.GetInt32(indicePagina);

                            if (!string.IsNullOrEmpty(filtroSerializzato))
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(RichiestaFilter));
                                using (StringReader sr = new StringReader(filtroSerializzato))
                                {
                                    filtro = (RichiestaFilter) ser.Deserialize(sr);
                                }
                            }
                        }
                    }
                }
            }

            return filtro;
        }

        public void DeleteFiltroRicercaRichieste(int idUtente)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColoniePersonaleFiltriRicercaDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Guid, idUtente);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        #region Accompagnatore

        #region Insert

        public bool InsertAccompagnatore(Accompagnatore accompagnatore, DbTransaction transaction)
        {
            bool res = false;

            if (!accompagnatore.IdAccompagnatore.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieAccompagnatoriInsert"))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, accompagnatore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, accompagnatore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, accompagnatore.DataNascita);
                    DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, accompagnatore.Sesso.ToString());

                    int idAccompagnatore = decimal.ToInt32((decimal) DatabaseCemi.ExecuteScalar(comando, transaction));
                    if (idAccompagnatore > 0)
                    {
                        accompagnatore.IdAccompagnatore = idAccompagnatore;
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region Update

        public bool UpdateAccompagnatore(Accompagnatore accompagnatore, DbTransaction transaction)
        {
            bool res = false;

            if (accompagnatore.IdAccompagnatore.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieAccompagnatoriUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idAccompagnatore", DbType.Int32,
                        accompagnatore.IdAccompagnatore.Value);
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, accompagnatore.Cognome);
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, accompagnatore.Nome);
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, accompagnatore.DataNascita);
                    DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, accompagnatore.Sesso.ToString());

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        #endregion

        #region Select

        public Accompagnatore GetUltimoAccompagnatore(int idFamiliareACE)
        {
            Accompagnatore accompagnatore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieAccompagnatoreSelectUltimo"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFamiliareACE", DbType.Int32, idFamiliareACE);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        accompagnatore = new Accompagnatore();

                        accompagnatore.IdAccompagnatore = (int) reader["idColonieAccompagnatore"];
                        accompagnatore.Cognome = (string) reader["cognome"];
                        accompagnatore.Nome = (string) reader["nome"];
                        accompagnatore.DataNascita = (DateTime) reader["dataNascita"];
                        accompagnatore.Sesso = ((string) reader["sesso"])[0];
                    }
                }
            }

            return accompagnatore;
        }

        #endregion

        #endregion

        #region DomandeACE

        #region Common

        private void AggiungiParametriComandoInsertDomandaACE(DbCommand comando, DomandaACE domanda)
        {
            DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, domanda.IdTurno);
            DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, domanda.Bambino.IdFamiliare);
            if (domanda.Accompagnatore != null)
                DatabaseCemi.AddInParameter(comando, "@idAccompagnatore", DbType.Int32,
                    domanda.Accompagnatore.IdAccompagnatore.Value);
            DatabaseCemi.AddInParameter(comando, "@idTaglia", DbType.Int32, domanda.IdTaglia);
        }

        #endregion

        #region Insert

        public bool InsertDomandaACE(DomandaACE domanda, out bool giaPresente)
        {
            bool res = false;
            bool resAcc = true;
            giaPresente = false;

            if (!domanda.IdDomanda.HasValue)
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();

                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            if (domanda.Accompagnatore != null)
                            {
                                if (domanda.Accompagnatore.IdAccompagnatore.HasValue)
                                    resAcc = UpdateAccompagnatore(domanda.Accompagnatore, transaction);
                                else
                                    resAcc = InsertAccompagnatore(domanda.Accompagnatore, transaction);
                            }

                            if (resAcc)
                            {
                                if (InsertUpdateLavoratoreACE(domanda.Bambino.Parente, transaction))
                                {
                                    if (InsertUpdateFamiliareACE(domanda.Bambino, transaction))
                                    {
                                        using (
                                            DbCommand comando =
                                                DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACEInsert"))
                                        {
                                            AggiungiParametriComandoInsertDomandaACE(comando, domanda);

                                            int resI = (int) DatabaseCemi.ExecuteScalar(comando, transaction);
                                            switch (resI)
                                            {
                                                case -1:
                                                    res = false;
                                                    giaPresente = true;
                                                    break;
                                                case -2:
                                                    res = false;
                                                    giaPresente = false;
                                                    break;
                                                default:
                                                    res = true;
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        finally
                        {
                            if (res)
                                transaction.Commit();
                            else
                                transaction.Rollback();
                        }
                    }
                }
            }

            return res;
        }

        #endregion

        #region Update

        public bool UpdateDomandaACE(DomandaACE domanda)
        {
            bool res = false;
            bool resAcc = true;

            if (domanda.IdDomanda.HasValue)
            {
                using (DbConnection connection = DatabaseCemi.CreateConnection())
                {
                    connection.Open();

                    using (DbTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        try
                        {
                            if (domanda.Accompagnatore != null)
                            {
                                if (domanda.Accompagnatore.IdAccompagnatore.HasValue)
                                    resAcc = UpdateAccompagnatore(domanda.Accompagnatore, transaction);
                                else
                                    resAcc = InsertAccompagnatore(domanda.Accompagnatore, transaction);
                            }

                            if (resAcc)
                            {
                                if (InsertUpdateLavoratoreACE(domanda.Bambino.Parente, transaction))
                                {
                                    if (InsertUpdateFamiliareACE(domanda.Bambino, transaction))
                                    {
                                        using (
                                            DbCommand comando =
                                                DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACEUpdate"))
                                        {
                                            DatabaseCemi.AddInParameter(comando, "@idDomandaACE", DbType.Int32,
                                                domanda.IdDomanda.Value);
                                            AggiungiParametriComandoInsertDomandaACE(comando, domanda);

                                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                                                res = true;
                                        }
                                    }
                                }
                            }
                        }
                        finally
                        {
                            if (res)
                                transaction.Commit();
                            else
                                transaction.Rollback();
                        }
                    }
                }
            }

            return res;
        }

        public bool UpdateDomandaACEAccetta(int idDomandaACE)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACEUpdateAccetta"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomandaACE", DbType.Int32, idDomandaACE);

                try
                {
                    DatabaseCemi.ExecuteNonQuery(comando);
                    res = true;
                }
                catch (DataException)
                {
                }
            }

            return res;
        }

        public bool UpdateDomandaACERifiuta(int idDomandaACE, string motivazione)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACEUpdateRifiuta"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomandaACE", DbType.Int32, idDomandaACE);
                if (!string.IsNullOrEmpty(motivazione))
                    DatabaseCemi.AddInParameter(comando, "@motivazione", DbType.String, motivazione);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Select

        public DomandaACECollection GetDomandeACE(DomandaACEFilter filtro)
        {
            DomandaACECollection domande = new DomandaACECollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACESelect"))
            {
                if (filtro.IdDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDomandaACE", DbType.Int32, filtro.IdDomanda.Value);
                if (!string.IsNullOrEmpty(filtro.IdCassaEdile))
                    DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, filtro.IdCassaEdile);
                if (filtro.IdTurno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTurno", DbType.Int32, filtro.IdTurno);
                if (!string.IsNullOrEmpty(filtro.CognomeLavoratore))
                    DatabaseCemi.AddInParameter(comando, "@cognomeLavoratore", DbType.String, filtro.CognomeLavoratore);
                if (!string.IsNullOrEmpty(filtro.CognomeBambino))
                    DatabaseCemi.AddInParameter(comando, "@cognomeBambino", DbType.String, filtro.CognomeBambino);
                if (filtro.Stato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.Int32, filtro.Stato);
                if (filtro.IdVacanza.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, filtro.IdVacanza.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        DomandaACE domanda = new DomandaACE();
                        domande.Add(domanda);

                        domanda.IdDomanda = (int) reader["idDomandaACE"];
                        domanda.IdTurno = (int) reader["idTurno"];
                        domanda.StatoDomanda = (StatoDomandaACE) (short) reader["stato"];
                        if (reader["numeroCorredo"] != DBNull.Value)
                            domanda.NumeroCorredo = (int) reader["numeroCorredo"];
                        if (reader["motivazioneRifiuto"] != DBNull.Value)
                            domanda.MotivazioneRifiuto = (string) reader["motivazioneRifiuto"];

                        // Familiare
                        domanda.Bambino.IdFamiliare = (int) reader["idColonieFamiliareACE"];
                        domanda.Bambino.Cognome = (string) reader["bambinoCognome"];
                        domanda.Bambino.Nome = (string) reader["bambinoNome"];
                        domanda.Bambino.DataNascita = (DateTime) reader["bambinoDataNascita"];
                        domanda.Bambino.CodiceFiscale = (string) reader["bambinoCodiceFiscale"];
                        domanda.Bambino.Sesso = ((string) reader["bambinoSesso"])[0];
                        domanda.Bambino.PortatoreHandicap = (bool) reader["bambinoHandicap"];
                        if (reader["noteHandicap"] != DBNull.Value)
                            domanda.Bambino.NotaDisabilita = (string) reader["noteHandicap"];
                        if (reader["bambinoIntolleranzeAlimentari"] != DBNull.Value)
                            domanda.Bambino.IntolleranzeAlimentari = (string) reader["bambinoIntolleranzeAlimentari"];
                        if (reader["bambinoModificabile"] != DBNull.Value)
                            domanda.Bambino.Modificabile = false;
                        else
                            domanda.Bambino.Modificabile = true;
                        if (reader["idTaglia"] != DBNull.Value)
                            domanda.IdTaglia = (int) reader["idTaglia"];

                        // Lavoratore
                        domanda.Bambino.Parente = new LavoratoreACE();
                        domanda.Bambino.Parente.IdLavoratore = (int) reader["idColonieLavoratoreACE"];
                        domanda.Bambino.Parente.Cognome = (string) reader["lavoratoreCognome"];
                        domanda.Bambino.Parente.Nome = (string) reader["lavoratoreNome"];
                        domanda.Bambino.Parente.DataNascita = (DateTime) reader["lavoratoreDataNascita"];
                        domanda.Bambino.Parente.CodiceFiscale = (string) reader["lavoratoreCodiceFiscale"];
                        domanda.Bambino.Parente.Sesso = ((string) reader["lavoratoreSesso"])[0];
                        if (reader["lavoratoreTelefono"] != DBNull.Value)
                            domanda.Bambino.Parente.Telefono = (string) reader["lavoratoreTelefono"];
                        if (reader["lavoratoreCellulare"] != DBNull.Value)
                            domanda.Bambino.Parente.Cellulare = (string) reader["lavoratoreCellulare"];
                        domanda.Bambino.Parente.Indirizzo = (string) reader["lavoratoreIndirizzo"];
                        domanda.Bambino.Parente.Provincia = (string) reader["lavoratoreProvincia"];
                        domanda.Bambino.Parente.Comune = (string) reader["lavoratoreComune"];
                        domanda.Bambino.Parente.Cap = (string) reader["lavoratoreCap"];
                        if (reader["lavoratoreModificabile"] != DBNull.Value)
                            domanda.Bambino.Parente.Modificabile = false;
                        else
                            domanda.Bambino.Parente.Modificabile = true;

                        // Accompagnatore
                        if (reader["idColonieAccompagnatore"] != DBNull.Value)
                        {
                            domanda.Accompagnatore = new Accompagnatore();
                            domanda.Accompagnatore.IdAccompagnatore = (int) reader["idColonieAccompagnatore"];
                            domanda.Accompagnatore.Cognome = (string) reader["accompagnatoreCognome"];
                            domanda.Accompagnatore.Nome = (string) reader["accompagnatoreNome"];
                            domanda.Accompagnatore.DataNascita = (DateTime) reader["accompagnatoreDataNascita"];
                            domanda.Accompagnatore.Sesso = ((string) reader["accompagnatoreSesso"])[0];
                        }
                    }
                }
            }

            return domande;
        }

        #endregion

        #region Delete

        public bool DeleteDomandaACE(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieDomandeACEDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomandaACE", DbType.String, idDomanda);

                if ((int) DatabaseCemi.ExecuteScalar(comando) > 0)
                    res = true;
            }

            return res;
        }

        #endregion

        #endregion

        #region LavoratoreACE

        #region Common

        private bool InsertUpdateLavoratoreACE(LavoratoreACE lavoratore, DbTransaction transaction)
        {
            bool res;

            if (lavoratore.IdLavoratore.HasValue)
                res = UpdateLavoratoreAce(lavoratore, transaction);
            else
                res = InsertLavoratoreAce(lavoratore, transaction);

            return res;
        }

        private void AggiungiParametriStandardComandoLavoratoreAce(DbCommand comando, LavoratoreACE lavoratore)
        {
            DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
            DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
            DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);
            DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, lavoratore.Sesso);
            if (!string.IsNullOrEmpty(lavoratore.Telefono))
                DatabaseCemi.AddInParameter(comando, "@telefono", DbType.String, lavoratore.Telefono);
            if (!string.IsNullOrEmpty(lavoratore.Cellulare))
                DatabaseCemi.AddInParameter(comando, "@cellulare", DbType.String, lavoratore.Cellulare);
            DatabaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, lavoratore.Indirizzo);
            DatabaseCemi.AddInParameter(comando, "@provincia", DbType.String, lavoratore.Provincia);
            DatabaseCemi.AddInParameter(comando, "@comune", DbType.String, lavoratore.Comune);
            DatabaseCemi.AddInParameter(comando, "@cap", DbType.String, lavoratore.Cap);
            DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, lavoratore.IdCassaEdile);
        }

        #endregion

        #region Insert

        private bool InsertLavoratoreAce(LavoratoreACE lavoratore, DbTransaction transaction)
        {
            bool res = false;

            if (!lavoratore.IdLavoratore.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieLavoratoriACEInsert"))
                {
                    AggiungiParametriStandardComandoLavoratoreAce(comando, lavoratore);

                    int idLavoratore = (int) DatabaseCemi.ExecuteScalar(comando, transaction);
                    if (idLavoratore > 0)
                    {
                        lavoratore.IdLavoratore = idLavoratore;
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region Update

        private bool UpdateLavoratoreAce(LavoratoreACE lavoratore, DbTransaction transaction)
        {
            bool res = false;

            if (lavoratore.IdLavoratore.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieLavoratoriACEUpdate"))
                {
                    AggiungiParametriStandardComandoLavoratoreAce(comando, lavoratore);
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        #endregion

        #region Select

        public LavoratoreACE GetLavoratoreACE(int idLavoratoreACE, int? idVacanza)
        {
            LavoratoreACE lavoratore = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieLavoratoriACESelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratoreACE);
                if (idVacanza.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        lavoratore = new LavoratoreACE();

                        CaricaValoriLavoratoreACE(reader, lavoratore);
                        if (reader["modificabile"] != DBNull.Value)
                            lavoratore.Modificabile = false;
                    }
                }
            }

            return lavoratore;
        }

        public LavoratoreACECollection GetLavoratoriACE(LavoratoreACEFilter filtro)
        {
            LavoratoreACECollection lavoratori = new LavoratoreACECollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieLavoratoriACESelect"))
            {
                if (!string.IsNullOrEmpty(filtro.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                if (!string.IsNullOrEmpty(filtro.Nome))
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                if (filtro.DataNascita.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.String, filtro.DataNascita.Value);
                if (!string.IsNullOrEmpty(filtro.IdCassaEdile))
                    DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, filtro.IdCassaEdile);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        LavoratoreACE lavoratore = new LavoratoreACE();
                        lavoratori.Add(lavoratore);

                        CaricaValoriLavoratoreACE(reader, lavoratore);
                    }
                }
            }

            return lavoratori;
        }

        private static void CaricaValoriLavoratoreACE(IDataRecord reader, LavoratoreACE lavoratore)
        {
            lavoratore.IdLavoratore = (int) reader["idColonieLavoratoreACE"];
            lavoratore.Cognome = (string) reader["cognome"];
            lavoratore.Nome = (string) reader["nome"];
            lavoratore.DataNascita = (DateTime) reader["dataNascita"];
            lavoratore.CodiceFiscale = (string) reader["codiceFiscale"];
            lavoratore.Sesso = ((string) reader["sesso"])[0];
            if (reader["telefono"] != DBNull.Value)
                lavoratore.Telefono = (string) reader["telefono"];
            if (reader["cellulare"] != DBNull.Value)
                lavoratore.Cellulare = (string) reader["cellulare"];
            lavoratore.Indirizzo = (string) reader["indirizzo"];
            lavoratore.Provincia = (string) reader["provincia"];
            lavoratore.Comune = (string) reader["comune"];
            lavoratore.Cap = (string) reader["cap"];
            lavoratore.IdCassaEdile = (string) reader["idCassaEdile"];
            lavoratore.Modificabile = true;
        }

        #endregion

        #endregion

        #region FamiliareACE

        #region Common

        private bool InsertUpdateFamiliareACE(FamiliareACE familiare, DbTransaction transaction)
        {
            bool res;

            if (familiare.IdFamiliare.HasValue)
                res = UpdateFamiliareAce(familiare, transaction);
            else
                res = InsertFamiliareAce(familiare, transaction);

            return res;
        }

        private void AggiungiParametriStandardComandoFamiliareAce(DbCommand comando, FamiliareACE familiare)
        {
            DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, familiare.Cognome);
            DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, familiare.Nome);
            DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, familiare.DataNascita);
            DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, familiare.CodiceFiscale);
            DatabaseCemi.AddInParameter(comando, "@sesso", DbType.String, familiare.Sesso);
            DatabaseCemi.AddInParameter(comando, "@portatoreHandicap", DbType.Boolean, familiare.PortatoreHandicap);
            if (!string.IsNullOrEmpty(familiare.NotaDisabilita))
                DatabaseCemi.AddInParameter(comando, "@noteHandicap", DbType.String, familiare.NotaDisabilita);
            if (!string.IsNullOrEmpty(familiare.IntolleranzeAlimentari))
                DatabaseCemi.AddInParameter(comando, "@intolleranzeAlimentari", DbType.String,
                    familiare.IntolleranzeAlimentari);
            DatabaseCemi.AddInParameter(comando, "@idColonieLavoratoreACE", DbType.Int32,
                familiare.Parente.IdLavoratore.Value);
        }

        #endregion

        #region Insert

        private bool InsertFamiliareAce(FamiliareACE familiare, DbTransaction transaction)
        {
            bool res = false;

            if (!familiare.IdFamiliare.HasValue && familiare.Parente != null && familiare.Parente.IdLavoratore.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieFamiliariACEInsert"))
                {
                    AggiungiParametriStandardComandoFamiliareAce(comando, familiare);

                    int idFamiliare = (int) DatabaseCemi.ExecuteScalar(comando, transaction);
                    if (idFamiliare > 0)
                    {
                        familiare.IdFamiliare = idFamiliare;
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region Update

        private bool UpdateFamiliareAce(FamiliareACE familiare, DbTransaction transaction)
        {
            bool res = false;

            if (familiare.IdFamiliare.HasValue && familiare.Parente != null && familiare.Parente.IdLavoratore.HasValue)
            {
                using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieFamiliariACEUpdate"))
                {
                    AggiungiParametriStandardComandoFamiliareAce(comando, familiare);
                    DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, familiare.IdFamiliare.Value);

                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        #endregion

        #region Select

        public FamiliareACECollection GetFamiliariACE(FamiliareACEFilter filtro)
        {
            FamiliareACECollection familiari = new FamiliareACECollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieFamiliariACESelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratoreACE", DbType.Int32, filtro.IdLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        FamiliareACE familiare = new FamiliareACE();
                        familiari.Add(familiare);

                        CaricaValoriFamiliareACE(reader, familiare);
                    }
                }
            }

            return familiari;
        }

        public FamiliareACE GetFamiliareACE(int idFamiliareACE, int? idVacanza)
        {
            FamiliareACE familiare = new FamiliareACE();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ColonieFamiliariACESelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliareACE);
                if (idVacanza.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idVacanza", DbType.Int32, idVacanza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        familiare = new FamiliareACE();

                        CaricaValoriFamiliareACE(reader, familiare);
                        if (reader["modificabile"] != DBNull.Value)
                            familiare.Modificabile = false;
                    }
                }
            }

            return familiare;
        }

        private static void CaricaValoriFamiliareACE(IDataRecord reader, FamiliareACE familiare)
        {
            familiare.IdFamiliare = (int) reader["idColonieFamiliareACE"];
            familiare.Cognome = (string) reader["cognome"];
            familiare.Nome = (string) reader["nome"];
            familiare.DataNascita = (DateTime) reader["dataNascita"];
            familiare.CodiceFiscale = (string) reader["codiceFiscale"];
            familiare.Sesso = ((string) reader["sesso"])[0];
            familiare.PortatoreHandicap = (bool) reader["portatoreHandicap"];
            if (reader["noteHandicap"] != DBNull.Value)
                familiare.NotaDisabilita = (string) reader["noteHandicap"];
            if (reader["intolleranzeAlimentari"] != DBNull.Value)
                familiare.IntolleranzeAlimentari = (string) reader["intolleranzeAlimentari"];
            familiare.Modificabile = true;
        }

        #endregion

        #endregion
    }
}