﻿using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Xml.Serialization;
using Cemi.MalattiaTelematica.Type.Collections;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.MalattiaTelematica.Type.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.MalattiaTelematica.Data
{
    public class MalattiaTelematicaDataAccess
    {
        public MalattiaTelematicaDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }


        public int GetOreDaLavorare()
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_ParametriSelectMalattiaTelematicaOreLavorate"))
            {
                DatabaseCemi.AddOutParameter(comando, "@oreLavorate", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                return (int) DatabaseCemi.GetParameterValue(comando, "@oreLavorate");
            }

            return -1;
        }

        public decimal GetOreLavorate(int idLavoratore, DateTime dataInizio, DateTime dataAssunzione, int idAssenza)
        {
            decimal oreDich = 0.0M;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaOreLavorate")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, dataInizio);
                DatabaseCemi.AddInParameter(comando, "@dataAssunzione", DbType.DateTime, dataAssunzione);
                DatabaseCemi.AddInParameter(comando, "@idAssenza", DbType.Int32, idAssenza);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceOreDichiarate = reader.GetOrdinal("OreDichiarate");

                    while (reader.Read())
                    {
                        oreDich = reader.GetDecimal(indiceOreDichiarate);
                    }
                }

                return oreDich;
            }
        }

        public AssenzaErroriCollection GetAssenzaErrori(DateTime data)
        {
            AssenzaErroriCollection assenzaErrori = new AssenzaErroriCollection();

            DataSet dsErrori;
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaErroriAssenze")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                dsErrori = DatabaseCemi.ExecuteDataSet(comando);
            }
            if (dsErrori != null && dsErrori.Tables.Count == 1)
            {
                for (int i = 0; i < dsErrori.Tables[0].Rows.Count; i++)
                {
                    int idImpresa;
                    string ragioneSociale;
                    int idLavoratore;
                    string cognome;
                    string nome;
                    string idCassaEdile;
                    string tipoProtocollo;
                    int annoProtocollo;
                    int numeroProtocollo;
                    DateTime dataInizio;
                    DateTime dataFine;
                    DateTime dataInizioMalattia;
                    string descrizione;

                    idImpresa = (int) dsErrori.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (string) dsErrori.Tables[0].Rows[i]["ragioneSociale"];
                    idLavoratore = (int) dsErrori.Tables[0].Rows[i]["idLavoratore"];
                    cognome = (string) dsErrori.Tables[0].Rows[i]["cognome"];
                    nome = (string) dsErrori.Tables[0].Rows[i]["nome"];
                    idCassaEdile = (string) dsErrori.Tables[0].Rows[i]["idCassaEdile"];
                    tipoProtocollo = (string) dsErrori.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (int) dsErrori.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (int) dsErrori.Tables[0].Rows[i]["numeroProtocollo"];
                    dataInizio = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime) dsErrori.Tables[0].Rows[i]["dataFine"];
                    dataInizioMalattia = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizioMalattia"];
                    descrizione = (string) dsErrori.Tables[0].Rows[i]["descrizione"];

                    AssenzaErrori ass = new AssenzaErrori(idImpresa, ragioneSociale, idLavoratore, cognome, nome,
                        idCassaEdile, tipoProtocollo, annoProtocollo, numeroProtocollo, dataInizio, dataFine,
                        dataInizioMalattia, descrizione);

                    assenzaErrori.Add(ass);
                }
            }

            return assenzaErrori;
        }

        public CertificatoMedicoErroriCollection GetCertificatoMedicoErrori(DateTime data)
        {
            CertificatoMedicoErroriCollection cmErrori = new CertificatoMedicoErroriCollection();

            DataSet dsErrori;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaErroriCertificatiMedici")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                dsErrori = DatabaseCemi.ExecuteDataSet(comando);
            }
            if (dsErrori != null && dsErrori.Tables.Count == 1)
            {
                for (int i = 0; i < dsErrori.Tables[0].Rows.Count; i++)
                {
                    int idImpresa;
                    string ragioneSociale;
                    int idLavoratore;
                    string cognome;
                    string nome;
                    string idCassaEdile;
                    string tipoProtocollo;
                    int annoProtocollo;
                    int numeroProtocollo;
                    DateTime dataInizio;
                    DateTime dataFine;
                    DateTime dataInizioMalattia;
                    string descrizione;
                    string numeroCertificato;
                    DateTime dataInizioCertificato;
                    DateTime dataFineCertificato;
                    DateTime dataRilascioCertificato;


                    idImpresa = (int) dsErrori.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (string) dsErrori.Tables[0].Rows[i]["ragioneSociale"];
                    idLavoratore = (int) dsErrori.Tables[0].Rows[i]["idLavoratore"];
                    cognome = (string) dsErrori.Tables[0].Rows[i]["cognome"];
                    nome = (string) dsErrori.Tables[0].Rows[i]["nome"];
                    idCassaEdile = (string) dsErrori.Tables[0].Rows[i]["idCassaEdile"];
                    tipoProtocollo = (string) dsErrori.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (int) dsErrori.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (int) dsErrori.Tables[0].Rows[i]["numeroProtocollo"];
                    dataInizio = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime) dsErrori.Tables[0].Rows[i]["dataFine"];
                    dataInizioMalattia = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizioMalattia"];
                    descrizione = (string) dsErrori.Tables[0].Rows[i]["descrizione"];


                    numeroCertificato = (string) dsErrori.Tables[0].Rows[i]["numeroEnte"];
                    dataInizioCertificato = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizioCertificato"];
                    dataFineCertificato = (DateTime) dsErrori.Tables[0].Rows[i]["dataFineCertificato"];
                    dataRilascioCertificato = (DateTime) dsErrori.Tables[0].Rows[i]["dataRilascioCertificato"];


                    CertificatoMedicoErrori cm = new CertificatoMedicoErrori(idImpresa, ragioneSociale, idLavoratore,
                        cognome, nome, idCassaEdile, tipoProtocollo, annoProtocollo, numeroProtocollo, dataInizio,
                        dataFine, dataInizioMalattia, descrizione, numeroCertificato, dataInizioCertificato,
                        dataFineCertificato, dataRilascioCertificato);

                    cmErrori.Add(cm);
                }
            }

            return cmErrori;
        }

        public MalattiaInfortunioErroriCollection GetMalattiaInfortunioErrori(DateTime data)
        {
            MalattiaInfortunioErroriCollection miErrori = new MalattiaInfortunioErroriCollection();

            DataSet dsErrori;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaErroriMalattieInfortuni")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data);
                dsErrori = DatabaseCemi.ExecuteDataSet(comando);
            }
            if (dsErrori != null && dsErrori.Tables.Count == 1)
            {
                for (int i = 0; i < dsErrori.Tables[0].Rows.Count; i++)
                {
                    int idImpresa;
                    string ragioneSociale;
                    int idLavoratore;
                    string cognome;
                    string nome;
                    string idCassaEdile;
                    string tipoProtocollo;
                    int annoProtocollo;
                    int numeroProtocollo;
                    DateTime dataInizio;
                    DateTime dataFine;
                    DateTime dataInizioMalattia;
                    string descrizione;
                    DateTime dataInizioEvento;
                    DateTime dataFineEvento;


                    idImpresa = (int) dsErrori.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (string) dsErrori.Tables[0].Rows[i]["ragioneSociale"];
                    idLavoratore = (int) dsErrori.Tables[0].Rows[i]["idLavoratore"];
                    cognome = (string) dsErrori.Tables[0].Rows[i]["cognome"];
                    nome = (string) dsErrori.Tables[0].Rows[i]["nome"];
                    idCassaEdile = (string) dsErrori.Tables[0].Rows[i]["idCassaEdile"];
                    tipoProtocollo = (string) dsErrori.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (int) dsErrori.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (int) dsErrori.Tables[0].Rows[i]["numeroProtocollo"];
                    dataInizio = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizio"];
                    dataFine = (DateTime) dsErrori.Tables[0].Rows[i]["dataFine"];
                    dataInizioMalattia = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizioMalattia"];
                    descrizione = (string) dsErrori.Tables[0].Rows[i]["descrizione"];

                    dataInizioEvento = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizioEvento"];
                    dataFineEvento = (DateTime) dsErrori.Tables[0].Rows[i]["dataInizioEvento"];

                    MalattiaInfortunioErrori mi = new MalattiaInfortunioErrori(idImpresa, ragioneSociale, idLavoratore,
                        cognome, nome, idCassaEdile, tipoProtocollo, annoProtocollo, numeroProtocollo, dataInizio,
                        dataFine, dataInizioMalattia, descrizione, dataInizioEvento, dataFineEvento);

                    miErrori.Add(mi);
                }
            }

            return miErrori;
        }

        public PrestazioneDomandaAssenzaCollection GetPrestazioneDomandaAssenza(DateTime? inizio, DateTime? fine,
            string ragSoc, string codFisc, int? idImpr)
        {
            PrestazioneDomandaAssenzaCollection prestazioniDomandeAssenza = new PrestazioneDomandaAssenzaCollection();

            DataSet dsAssenze;
            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaPrestazioniDomandeAssenzaSelect"))
            {
                if (inizio.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@Inizio", DbType.DateTime, inizio.Value);
                if (fine.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, fine.Value);
                if (ragSoc != string.Empty)
                    DatabaseCemi.AddInParameter(comando, "@ragioneSociale", DbType.String, ragSoc);
                if (codFisc != string.Empty)
                    DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, codFisc);
                if (idImpr.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idImpresa", DbType.Int32, idImpr);

                dsAssenze = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsAssenze != null && dsAssenze.Tables.Count == 1)
            {
                for (int i = 0; i < dsAssenze.Tables[0].Rows.Count; i++)
                {
                    int idImpresa;
                    string ragioneSociale;
                    string codiceFiscale;
                    string partitaIVA = "";
                    string tipoProtocollo = "";
                    int annoProtocollo;
                    int numeroProtocollo;
                    DateTime data;
                    decimal importo;
                    int numero;
                    int idAssenza;

                    idImpresa = (int) dsAssenze.Tables[0].Rows[i]["idImpresa"];
                    ragioneSociale = (string) dsAssenze.Tables[0].Rows[i]["ragioneSociale"];
                    codiceFiscale = (string) dsAssenze.Tables[0].Rows[i]["codiceFiscale"];
                    if (dsAssenze.Tables[0].Rows[i]["partitaIVA"] != DBNull.Value)
                        partitaIVA = (string) dsAssenze.Tables[0].Rows[i]["partitaIVA"];
                    if (dsAssenze.Tables[0].Rows[i]["tipoProtocollo"] != DBNull.Value)
                        tipoProtocollo = (string) dsAssenze.Tables[0].Rows[i]["tipoProtocollo"];
                    annoProtocollo = (int) dsAssenze.Tables[0].Rows[i]["annoProtocollo"];
                    numeroProtocollo = (int) dsAssenze.Tables[0].Rows[i]["numeroProtocollo"];
                    data = (DateTime) dsAssenze.Tables[0].Rows[i]["data"];
                    importo = (decimal) dsAssenze.Tables[0].Rows[i]["importo"];
                    numero = (int) dsAssenze.Tables[0].Rows[i]["numero"];

                    idAssenza = (int) dsAssenze.Tables[0].Rows[i]["idAssenza"];

                    PrestazioneDomandaAssenza ass = new PrestazioneDomandaAssenza(idImpresa, ragioneSociale,
                        codiceFiscale, partitaIVA, tipoProtocollo, annoProtocollo, numeroProtocollo, data, importo,
                        numero, idAssenza);

                    prestazioniDomandeAssenza.Add(ass);
                }
            }

            return prestazioniDomandeAssenza;
        }

        //public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        //{
        //    bool res = false;
        //    oreDuplicate = false;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertUpdate"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, ore.IdLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, ore.IdCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, ore.Anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, ore.Mese);

        //        databaseCemi.AddInParameter(comando, "@oreLavorate", DbType.Int32, ore.OreLavorate);
        //        databaseCemi.AddInParameter(comando, "@oreFerie", DbType.Int32, ore.OreFerie);
        //        databaseCemi.AddInParameter(comando, "@oreInfortunio", DbType.Int32, ore.OreInfortunio);
        //        databaseCemi.AddInParameter(comando, "@oreMalattia", DbType.Int32, ore.OreMalattia);
        //        databaseCemi.AddInParameter(comando, "@oreCassaIntegrazione", DbType.Int32, ore.OreCassaIntegrazione);
        //        databaseCemi.AddInParameter(comando, "@orePermessoRetribuito", DbType.Int32, ore.OrePermessoRetribuito);
        //        databaseCemi.AddInParameter(comando, "@orePermessoNonRetribuito", DbType.Int32,
        //                                    ore.OrePermessoNonRetribuito);
        //        databaseCemi.AddInParameter(comando, "@oreAltro", DbType.Int32, ore.OreAltro);
        //        databaseCemi.AddInParameter(comando, "@livelloErogazione", DbType.Int32, ore.LivelloErogazione);
        //        databaseCemi.AddInParameter(comando, "@inseriteManualmente", DbType.Boolean, ore.InseriteManualmente);

        //        try
        //        {
        //            if (databaseCemi.ExecuteNonQuery(comando) == 1)
        //                res = true;
        //        }
        //        catch (SqlException sqlExc)
        //        {
        //            // Eccezione che viene generata se provo ad inserire due volte le ore per lo stesso mese
        //            if (sqlExc.Number == 2627)
        //                oreDuplicate = true;
        //            else
        //                throw;
        //        }
        //    }

        //    return res;
        //}


        public StatisticaLiquidazioneCollection GetStatisticaLiquidazioni(DateTime inizio, DateTime fine, string tipo)
        {
            StatisticaLiquidazioneCollection collection = new StatisticaLiquidazioneCollection();

            DataSet dsRighe;
            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaStatisticaLiquidazioneSelect"))
            {
                //if (inizio.HasValue)
                DatabaseCemi.AddInParameter(comando, "@Inizio", DbType.DateTime, inizio);
                //if (fine.HasValue)
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, fine);
                if (tipo != string.Empty)
                    DatabaseCemi.AddInParameter(comando, "@tipoAssenza", DbType.String, tipo);

                dsRighe = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsRighe != null && dsRighe.Tables.Count == 1)
            {
                for (int i = 0; i < dsRighe.Tables[0].Rows.Count; i++)
                {
                    DateTime data;
                    decimal importo;
                    int numeroLavoratori;
                    int numeroImprese;

                    data = (DateTime) dsRighe.Tables[0].Rows[i]["data"];
                    importo = (decimal) dsRighe.Tables[0].Rows[i]["importo"];
                    numeroLavoratori = (int) dsRighe.Tables[0].Rows[i]["numeroLavoratori"];
                    numeroImprese = (int) dsRighe.Tables[0].Rows[i]["numeroImprese"];


                    StatisticaLiquidazione stat =
                        new StatisticaLiquidazione(data, importo, numeroLavoratori, numeroImprese);

                    collection.Add(stat);
                }
            }

            return collection;
        }

        public StatisticaEvaseCollection GetStatisticaEvase(DateTime inizio, DateTime fine, string tipo)
        {
            StatisticaEvaseCollection collection = new StatisticaEvaseCollection();

            DataSet dsRighe;
            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaStatisticaEvaseSelect"))
            {
                //if (inizio.HasValue)
                DatabaseCemi.AddInParameter(comando, "@Inizio", DbType.DateTime, inizio);
                //if (fine.HasValue)
                DatabaseCemi.AddInParameter(comando, "@fine", DbType.DateTime, fine);
                if (tipo != string.Empty)
                    DatabaseCemi.AddInParameter(comando, "@tipoAssenza", DbType.String, tipo);

                dsRighe = DatabaseCemi.ExecuteDataSet(comando);
            }

            if (dsRighe != null && dsRighe.Tables.Count == 1)
            {
                for (int i = 0; i < dsRighe.Tables[0].Rows.Count; i++)
                {
                    string stato;
                    string utente;
                    int numeroAssenze;

                    stato = (string) dsRighe.Tables[0].Rows[i]["stato"];
                    utente = (string) dsRighe.Tables[0].Rows[i]["utente"];
                    numeroAssenze = (int) dsRighe.Tables[0].Rows[i]["numeroAssenze"];


                    StatisticaEvase stat = new StatisticaEvase(stato, utente, numeroAssenze);

                    collection.Add(stat);
                }
            }

            return collection;
        }

        public bool InsertCassaEdile(int idAssenza, string idCassaEdile)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCasseEdiliInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idAssenza", DbType.Int32, idAssenza);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@inseritaManualmente", DbType.Boolean, true);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public bool DeleteCassaEdile(int idAssenza, string idCassaEdile, int idLavoratore)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCasseEdiliDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idAssenza", DbType.Int32, idAssenza);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }


        public bool UpdateMalattiaTelematicaCertificatoMedico(int idCertificatoMedico, DateTime dataInizio,
            DateTime dataFine, DateTime dataRilascio, byte[] immagine, string nomeFile, string numero, string tipo,
            string tipoAssenza, bool? ricovero)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaCertificatiMediciUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idCertificatoMedico", DbType.Int32, idCertificatoMedico);
                DatabaseCemi.AddInParameter(comando, "@dataRilascio", DbType.DateTime, dataRilascio);
                DatabaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, dataInizio);
                DatabaseCemi.AddInParameter(comando, "@dataFine", DbType.DateTime, dataFine);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, numero);
                DatabaseCemi.AddInParameter(comando, "@tipo", DbType.String, tipo);
                DatabaseCemi.AddInParameter(comando, "@tipoAssenza", DbType.String, tipoAssenza);
                DatabaseCemi.AddInParameter(comando, "@immagine", DbType.Binary, immagine);
                DatabaseCemi.AddInParameter(comando, "@nomeFile", DbType.String, nomeFile);
                DatabaseCemi.AddInParameter(comando, "@ricovero", DbType.Boolean,
                    ricovero.HasValue ? ricovero.Value : false);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        #region filtri ricerca

        public void InsertFiltroRicerca(int idUtente, AssenzeFilter filtro, int? pagina)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException();
            }

            XmlSerializer ser = new XmlSerializer(typeof(AssenzeFilter));
            string filtroSerializzato = string.Empty;
            if (filtro != null)
            {
                using (StringWriter sw = new StringWriter())
                {
                    ser.Serialize(sw, filtro);
                    filtroSerializzato = sw.ToString();
                }
            }


            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaFiltriRicercaInsertUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                if (filtro != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@filtro", DbType.Xml, filtroSerializzato);
                    DatabaseCemi.AddInParameter(comando, "@pagina", DbType.Int32, pagina.Value);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Errore durante l'inserimento del filtro di ricerca");
                }
            }
        }

        public AssenzeFilter GetFiltroRicerca(int idUtente, out int pagina)
        {
            AssenzeFilter filtro = null;
            pagina = 0;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaFiltriRicercaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        #region Indici reader

                        int indiceUserID = reader.GetOrdinal("idUtente");
                        int indiceFiltro = reader.GetOrdinal("filtro");
                        int indicePagina = reader.GetOrdinal("pagina");

                        #endregion

                        if (!reader.IsDBNull(indiceUserID))
                        {
                            string filtroSerializzato = null;

                            if (!reader.IsDBNull(indiceFiltro))
                            {
                                filtroSerializzato = reader.GetString(indiceFiltro);
                                pagina = reader.GetInt32(indicePagina);
                            }
                            if (!string.IsNullOrEmpty(filtroSerializzato))
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(AssenzeFilter));
                                using (StringReader sr = new StringReader(filtroSerializzato))
                                {
                                    filtro = (AssenzeFilter) ser.Deserialize(sr);
                                }
                            }
                        }
                    }
                }
            }

            return filtro;
        }

        public void DeleteFiltroRicerca(int idUtente)
        {
            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_MalattiaTelematicaFiltriRicercaDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        #endregion
    }
}