﻿using System;
using System.Data;
using System.Data.Common;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.NotifichePreliminari.Types.Enums;
using Cemi.NotifichePreliminari.Types.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.Data
{
    public class NotifichePreliminariDataAccess
    {
        private readonly Database _databaseCemi;

        public NotifichePreliminariDataAccess()
        {
            _databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public TipoOperaNotificaCollection GetTipiOpera()
        {
            TipoOperaNotificaCollection l = new TipoOperaNotificaCollection();

            l.Add(new TipoOperaNotifica {Id = 1, Descrizione = "Pubblica"});

            return l;
        }

        public NotificaPreliminare NotificaSelectByNumero(string numeroNotifica)
        {
            NotificaPreliminare notifica = null;
            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariSelectByNumero"))
            {
                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, numeroNotifica);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexIdNotifica = reader.GetOrdinal("idNotificaPreliminareCantieri");
                    int indexNumeroNotfica = reader.GetOrdinal("numeroNotifica");
                    int indexProtocolloNotifica = reader.GetOrdinal("protocolloNotifica");
                    int indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    int indexDataAggiornamento = reader.GetOrdinal("dataAggiornamentoNotifica");
                    int indexNumeroLavoratoriAutonomi = reader.GetOrdinal("numeroLavoratoriAutonomi");
                    int indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    int indexIdTipoContratto = reader.GetOrdinal("idTipoContratto");
                    int indexDescrizioneTipoContratto = reader.GetOrdinal("descrizioneTipoContratto");
                    int indexIdContratto = reader.GetOrdinal("idContratto");
                    int indexIdTipoOpera = reader.GetOrdinal("idTipoOpera");
                    int indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    int indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    int indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    int indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    int indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    int indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    int indexArt9011 = reader.GetOrdinal("art9011");
                    int indexArt9011B = reader.GetOrdinal("art9011B");
                    int indexArt9011C = reader.GetOrdinal("art9011C");
                    int indexIdCantiereRegione = reader.GetOrdinal("idCantiereRegione");

                    #endregion


                    if (reader.Read())
                    {
                        /* notifica = new NotificaPreliminare
                        {
                            IdNotificaCantiere = reader.GetInt32(indexIdNotifica),
                            AltraCategoriaDescrizione = reader.GetString(indexDescrizioneAltraCategoria),
                            AltraTipologiaDescrizione = reader.GetString(indexDescrizioneAltraTipologia),
                            AmmontareComplessivo = reader.GetDecimal(indexAmmontare),
                            Articolo9011 = reader.GetBoolean(indexArt9011),
                            Articolo9011B = reader.GetBoolean(indexArt9011B),
                            Articolo9011C = reader.GetBoolean(indexArt9011C),
                            ContrattoId = reader.GetString(indexIdContratto),
                            DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione),
                            DataAggiornamentoNotifica = reader.IsDBNull(indexDataAggiornamento)
                                ? null as DateTime?
                                : reader.GetDateTime(indexDataAggiornamento),
                            IdCantiereRegione = reader.IsDBNull(indexIdCantiereRegione)
                                ? 0
                                : reader.GetInt32(indexIdCantiereRegione),
                            NumeroImprese =
                                reader.IsDBNull(indexNumeroImprese) ? 0 : reader.GetInt32(indexNumeroImprese),
                            NumeroLavoratoriAutonomi = reader.IsDBNull(indexNumeroLavoratoriAutonomi)
                                ? 0
                                : reader.GetInt32(indexNumeroLavoratoriAutonomi),
                            NumeroNotifica = reader.GetString(indexNumeroNotfica),
                            ProtocolloNotifica = reader.GetString(indexProtocolloNotifica),
                            TipoCategoriaDescrizione = reader.GetString(indexDescrizioneTipoCategoria),
                            TipoContrattoDescrizione = reader.GetString(indexDescrizioneTipoContratto),
                            TipoContrattoId = reader.GetString(indexIdTipoContratto),
                            TipoTipologiaDescrizione = reader.GetString(indexDescrizioneTipoTipologia),
                            TipoOperaDescrizione = reader.GetString(indexDescrizioneTipoOpera),
                            TipoOperaId =
                                (IdTipoOperaEnum) (reader.IsDBNull(indexIdTipoOpera)
                                    ? 0
                                    : reader.GetInt32(indexIdTipoOpera))
                        };*/
                        notifica = new NotificaPreliminare();

                        notifica.IdNotificaCantiere = reader.GetInt32(indexIdNotifica);
                        notifica.AltraCategoriaDescrizione = reader.IsDBNull(indexDescrizioneAltraCategoria)
                            ? ""
                            : reader.GetString(indexDescrizioneAltraCategoria);
                        notifica.AltraTipologiaDescrizione = reader.IsDBNull(indexDescrizioneAltraTipologia)
                            ? ""
                            : reader.GetString(indexDescrizioneAltraTipologia);
                        notifica.AmmontareComplessivo = reader.GetDecimal(indexAmmontare);
                        notifica.Articolo9011 = reader.GetBoolean(indexArt9011);
                        notifica.Articolo9011B = reader.GetBoolean(indexArt9011B);
                        notifica.Articolo9011C = reader.GetBoolean(indexArt9011C);
                        notifica.ContrattoId = reader.IsDBNull(indexIdContratto)
                            ? ""
                            : reader.GetString(indexIdContratto);
                        notifica.DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione);
                        notifica.DataAggiornamentoNotifica = reader.IsDBNull(indexDataAggiornamento)
                            ? null as DateTime?
                            : reader.GetDateTime(indexDataAggiornamento);
                        notifica.IdCantiereRegione = reader.IsDBNull(indexIdCantiereRegione)
                            ? 0
                            : reader.GetInt32(indexIdCantiereRegione);
                        notifica.NumeroImprese =
                            reader.IsDBNull(indexNumeroImprese)
                            ? 0
                            : reader.GetInt32(indexNumeroImprese);
                        notifica.NumeroLavoratoriAutonomi = reader.IsDBNull(indexNumeroLavoratoriAutonomi)
                            ? 0
                            : reader.GetInt32(indexNumeroLavoratoriAutonomi);
                        notifica.NumeroNotifica = reader.GetString(indexNumeroNotfica);
                        notifica.ProtocolloNotifica = reader.GetString(indexProtocolloNotifica);
                        notifica.TipoCategoriaDescrizione = reader.GetString(indexDescrizioneTipoCategoria);
                        notifica.TipoContrattoDescrizione = reader.IsDBNull(indexDescrizioneTipoContratto)
                            ? ""
                            : reader.GetString(indexDescrizioneTipoContratto);
                        notifica.TipoContrattoId = reader.IsDBNull(indexIdTipoContratto)
                            ? ""
                            : reader.GetString(indexIdTipoContratto);
                        notifica.TipoTipologiaDescrizione = reader.GetString(indexDescrizioneTipoTipologia);
                        notifica.TipoOperaDescrizione = reader.IsDBNull(indexDescrizioneTipoOpera)
                            ? ""
                            : reader.GetString(indexDescrizioneTipoOpera);
                        notifica.TipoOperaId = (IdTipoOperaEnum)(reader.IsDBNull(indexIdTipoOpera)
                            ? 0
                            : reader.GetInt32(indexIdTipoOpera));
                    }
                }
            }

            return notifica;
        }

        public NotificaPreliminareCollection NotificheSelectByFilter(NotificaPreliminareFilter filter)
        {
            NotificaPreliminareCollection notifiche = new NotificaPreliminareCollection();

            using (DbCommand comando = _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariSelectByFilter"))
            {
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriDal", DbType.DateTime,
                    filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriAl", DbType.DateTime,
                    filter.DataInizioLavoriAl);
                _databaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filter.NaturaOpera);
                _databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                _databaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String, filter.CantiereIndirizzo);
                _databaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, filter.CantiereCap);
                _databaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String, filter.CantiereProvncia);
                _databaseCemi.AddInParameter(comando, "@committenteCodiceFiscale", DbType.String,
                    filter.CommittenteCodiceFiscale);
                _databaseCemi.AddInParameter(comando, "@committenteCognome", DbType.String, filter.CommittenteCognome);
                _databaseCemi.AddInParameter(comando, "@committenteNome", DbType.String, filter.CommittenteNome);
                _databaseCemi.AddInParameter(comando, "@impresaRagioneSociale", DbType.String,
                    filter.ImpresaRagioneSociale);
                _databaseCemi.AddInParameter(comando, "@impresaCF", DbType.String, filter.ImpresaCfPIva);
                _databaseCemi.AddInParameter(comando, "@impresaProvincia", DbType.String, filter.ImpresaProvincia);
                _databaseCemi.AddInParameter(comando, "@impresaCAP", DbType.String, filter.ImpresaCap);
                _databaseCemi.AddInParameter(comando, "@dataComunicazoneDa", DbType.DateTime,
                    filter.DataComunicazioneDal);
                _databaseCemi.AddInParameter(comando, "@dataComunicazoneA", DbType.DateTime,
                    filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, filter.NumeroNotifica);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexIdNotifica = reader.GetOrdinal("idNotificaPreliminareCantieri");
                    int indexNumeroNotfica = reader.GetOrdinal("numeroNotifica");
                    int indexProtocolloNotifica = reader.GetOrdinal("protocolloNotifica");
                    int indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    int indexDataAggiornamento = reader.GetOrdinal("dataAggiornamentoNotifica");
                    int indexNumeroLavoratoriAutonomi = reader.GetOrdinal("numeroLavoratoriAutonomi");
                    int indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    int indexIdTipoContratto = reader.GetOrdinal("idTipoContratto");
                    int indexDescrizioneTipoContratto = reader.GetOrdinal("descrizioneTipoContratto");
                    int indexIdContratto = reader.GetOrdinal("idContratto");
                    int indexIdTipoOpera = reader.GetOrdinal("idTipoOpera");
                    int indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    int indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    int indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    int indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    int indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    int indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    int indexArt9011 = reader.GetOrdinal("art9011");
                    int indexArt9011B = reader.GetOrdinal("art9011B");
                    int indexArt9011C = reader.GetOrdinal("art9011C");
                    int indexIdCantiereRegione = reader.GetOrdinal("idCantiereRegione");
                    int indexIDPersona = reader.GetOrdinal("idNotificaPreliminarePersone");
                    int indexPersNome = reader.GetOrdinal("personaNome");
                    int indexPersCognome = reader.GetOrdinal("personaCognome");
                    int indexPersCf = reader.GetOrdinal("personaCf");
                    int indexIdIndirizzo = reader.GetOrdinal("idNotificaPreliminareIndirizzi");
                    int indexIndirizoComune = reader.GetOrdinal("indirizzoComune");
                    int indexIndirizzoIndirizzo = reader.GetOrdinal("indirizzoIndirizzo");
                    int indexIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                    int indexIndirizzoCap = reader.GetOrdinal("indirizzoCap");

                    #endregion

                    //NotificaPreliminare notifica;
                    NotificaPreliminare notificaPrec = null;
                    // Indirizzo indirizzo;
                    // Persona comittente;
                    int idNotifica;
                    int idPersona;
                    int idIndirizzo;
                    while (reader.Read())
                    {
                        idNotifica = reader.IsDBNull(indexIdNotifica) ? -1 : reader.GetInt32(indexIdNotifica);
                        idPersona = reader.IsDBNull(indexIDPersona) ? -1 : reader.GetInt32(indexIDPersona);
                        idIndirizzo = reader.IsDBNull(indexIdIndirizzo) ? -1 : reader.GetInt32(indexIdIndirizzo);

                        if (idNotifica != -1 && (notificaPrec == null || notificaPrec.IdNotificaCantiere != idNotifica))
                        {
                            notificaPrec = new NotificaPreliminare();

                            notificaPrec.IdNotificaCantiere = idNotifica;
                            notificaPrec.AltraCategoriaDescrizione = reader.IsDBNull(indexDescrizioneAltraCategoria)
                                ? ""
                                : reader.GetString(indexDescrizioneAltraCategoria);
                            notificaPrec.AltraTipologiaDescrizione = reader.IsDBNull(indexDescrizioneAltraTipologia)
                                ? ""
                                : reader.GetString(indexDescrizioneAltraTipologia);
                            notificaPrec.AmmontareComplessivo = reader.GetDecimal(indexAmmontare);
                            notificaPrec.Articolo9011 = reader.GetBoolean(indexArt9011);
                            notificaPrec.Articolo9011B = reader.GetBoolean(indexArt9011B);
                            notificaPrec.Articolo9011C = reader.GetBoolean(indexArt9011C);
                            notificaPrec.ContrattoId = reader.IsDBNull(indexIdContratto)
                                ? ""
                                : reader.GetString(indexIdContratto);
                            notificaPrec.DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione);
                            notificaPrec.DataAggiornamentoNotifica = reader.IsDBNull(indexDataAggiornamento)
                                ? null as DateTime?
                                : reader.GetDateTime(indexDataAggiornamento);
                            notificaPrec.IdCantiereRegione = reader.IsDBNull(indexIdCantiereRegione)
                                ? 0
                                : reader.GetInt32(indexIdCantiereRegione);
                            notificaPrec.NumeroImprese =
                                reader.IsDBNull(indexNumeroImprese) 
                                ? 0 
                                : reader.GetInt32(indexNumeroImprese);
                            notificaPrec.NumeroLavoratoriAutonomi = reader.IsDBNull(indexNumeroLavoratoriAutonomi)
                                ? 0
                                : reader.GetInt32(indexNumeroLavoratoriAutonomi);
                            notificaPrec.NumeroNotifica = reader.GetString(indexNumeroNotfica);
                            notificaPrec.ProtocolloNotifica = reader.GetString(indexProtocolloNotifica);
                            notificaPrec.TipoCategoriaDescrizione = reader.GetString(indexDescrizioneTipoCategoria);
                            notificaPrec.TipoContrattoDescrizione = reader.IsDBNull(indexDescrizioneTipoContratto)
                                ? ""
                                : reader.GetString(indexDescrizioneTipoContratto);
                            notificaPrec.TipoContrattoId = reader.IsDBNull(indexIdTipoContratto)
                                ? ""
                                : reader.GetString(indexIdTipoContratto);
                            notificaPrec.TipoTipologiaDescrizione = reader.GetString(indexDescrizioneTipoTipologia);
                            notificaPrec.TipoOperaDescrizione = reader.IsDBNull(indexDescrizioneTipoOpera)
                                ? ""
                                : reader.GetString(indexDescrizioneTipoOpera);
                            notificaPrec.TipoOperaId = (IdTipoOperaEnum)(reader.IsDBNull(indexIdTipoOpera)
                                ? 0
                                : reader.GetInt32(indexIdTipoOpera));

                            /*{
                                IdNotificaCantiere = idNotifica,
                                AltraCategoriaDescrizione = reader.GetString(indexDescrizioneAltraCategoria),
                                AltraTipologiaDescrizione = reader.GetString(indexDescrizioneAltraTipologia),
                                AmmontareComplessivo = reader.GetDecimal(indexAmmontare),
                                Articolo9011 = reader.GetBoolean(indexArt9011),
                                Articolo9011B = reader.GetBoolean(indexArt9011B),
                                Articolo9011C = reader.GetBoolean(indexArt9011C),
                                ContrattoId = reader.GetString(indexIdContratto),
                                DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione),
                                DataAggiornamentoNotifica = reader.IsDBNull(indexDataAggiornamento)
                                    ? null as DateTime?
                                    : reader.GetDateTime(indexDataAggiornamento),
                                IdCantiereRegione = reader.IsDBNull(indexIdCantiereRegione)
                                    ? 0
                                    : reader.GetInt32(indexIdCantiereRegione),
                                NumeroImprese =
                                    reader.IsDBNull(indexNumeroImprese) ? 0 : reader.GetInt32(indexNumeroImprese),
                                NumeroLavoratoriAutonomi = reader.IsDBNull(indexNumeroLavoratoriAutonomi)
                                    ? 0
                                    : reader.GetInt32(indexNumeroLavoratoriAutonomi),
                                NumeroNotifica = reader.GetString(indexNumeroNotfica),
                                ProtocolloNotifica = reader.GetString(indexProtocolloNotifica),
                                TipoCategoriaDescrizione = reader.GetString(indexDescrizioneTipoCategoria),
                                TipoContrattoDescrizione = reader.GetString(indexDescrizioneTipoContratto),
                                TipoContrattoId = reader.GetString(indexIdTipoContratto),
                                TipoTipologiaDescrizione = reader.GetString(indexDescrizioneTipoTipologia),
                                TipoOperaDescrizione = reader.GetString(indexDescrizioneTipoOpera),
                                TipoOperaId = (IdTipoOperaEnum) (reader.IsDBNull(indexIdTipoOpera)
                                    ? 0
                                    : reader.GetInt32(indexIdTipoOpera))
                            };*/
                            notifiche.Add(notificaPrec);
                        }

                        if (idIndirizzo != -1 && !notificaPrec.Indirizzi.ContainsId(idIndirizzo))
                        {
                            Indirizzo indirizzo = new Indirizzo();
                            indirizzo.IdIndirizzo = idIndirizzo;
                            indirizzo.Comune = reader.GetString(indexIndirizoComune);
                            indirizzo.NomeVia = reader.GetString(indexIndirizzoIndirizzo);
                            indirizzo.Provincia = reader.IsDBNull(indexIndirizzoProvincia)
                                ? null
                                : reader.GetString(indexIndirizzoProvincia);
                            indirizzo.Cap = reader.IsDBNull(indexIndirizzoCap)
                                ? null
                                : reader.GetString(indexIndirizzoCap);
                            notificaPrec.Indirizzi.Add(indirizzo);
                        }

                        if (idPersona != -1 && !notificaPrec.Committenti.ContainsId(idPersona))
                        {
                            Persona committente = new Persona
                            {
                                IdPersona = idPersona,
                                Nome = reader.GetString(indexPersNome),
                                CodiceFiscale = reader.GetString(indexPersCf),
                                Cognome = reader.GetString(indexPersCognome)
                            };
                            notificaPrec.Committenti.Add(committente);
                        }
                    }
                }
            }

            return notifiche;
        }


        public PersonaCollection PersoneSelectByNumeroNotificaRuolo(string numeroNotifica, IdRuoliPersone? ruolo)
        {
            PersonaCollection persone = new PersonaCollection();
            using (DbCommand command =
                _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariPersoneSelectByNumeroNotifica"))
            {
                _databaseCemi.AddInParameter(command, "@numeroNotifica", DbType.String, numeroNotifica);

                //TODO RUOLO

                using (IDataReader reader = _databaseCemi.ExecuteReader(command))
                {
                    int indexIDPersona = reader.GetOrdinal("idNotificaPreliminarePersone");
                    int indexNome = reader.GetOrdinal("nome");
                    int indexCognome = reader.GetOrdinal("cognome");
                    int indexCf = reader.GetOrdinal("codiceFiscale");
                    int indexDataNascita = reader.GetOrdinal("dataNascita");
                    int indexRegione = reader.GetOrdinal("regione");
                    int indexNazione = reader.GetOrdinal("nazione");
                    int indexIndirizzo = reader.GetOrdinal("indirizzo");
                    int indexComune = reader.GetOrdinal("comune");
                    int indexCap = reader.GetOrdinal("Cap");
                    int indexRuolo = reader.GetOrdinal("ruolo");
                    int indexProvincia = reader.GetOrdinal("provincia");

                    Persona persona;
                    string ruoloDescrizione;
                    while (reader.Read())
                    {
                        ruoloDescrizione = reader.GetString(indexRuolo);
                        persona = new Persona
                        {
                            CodiceFiscale = reader.GetString(indexCf),
                            DataNascita = reader.IsDBNull(indexDataNascita)
                            ? null as DateTime?
                            : reader.GetDateTime(indexDataNascita),
                            Cognome = reader.GetString(indexCognome),
                            Nome = reader.GetString(indexNome),
                            IdPersona = reader.GetInt32(indexIDPersona)
                        };

                        persona.Ruolo = new RuoloPersona(ruoloDescrizione);
                        persona.Indirizzo = new TBridge.Cemi.Type.Entities.Indirizzo
                        {
                            Cap = reader.IsDBNull(indexCap)
                                ? ""
                                : reader.GetString(indexCap),
                            NomeVia = reader.IsDBNull(indexIndirizzo)
                                ? ""
                                : reader.GetString(indexIndirizzo),
                            Comune = reader.IsDBNull(indexComune)
                                ? ""
                                : reader.GetString(indexComune),
                            Provincia = reader.IsDBNull(indexProvincia)
                                ? ""
                                : reader.GetString(indexProvincia),
                            Stato = reader.IsDBNull(indexNazione)
                                ? ""
                                : reader.GetString(indexNazione)
                        };


                        persone.Add(persona);
                    }
                }
            }
            return persone;
        }

        public ImpresaNotifichePreliminariCollection ImpreseSelectByNumeroNotificaIncarico(string numeroNotifica,
            IdTipoIncaricoImpresa? idIncarico)
        {
            ImpresaNotifichePreliminariCollection imprese = new ImpresaNotifichePreliminariCollection();

            using (DbCommand command =
                _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariImpreseSelectByNumeroNotifica"))
            {
                _databaseCemi.AddInParameter(command, "@numeroNotifica", DbType.String, numeroNotifica);

                //TODO INCARICO

                using (IDataReader reader = _databaseCemi.ExecuteReader(command))
                {
                    int indexIDImpresa = reader.GetOrdinal("idNotificaPreliminareImprese");
                    int indexRagSoc = reader.GetOrdinal("ragioneSociale");
                    int indexCf = reader.GetOrdinal("codiceFiscale");
                    int indexPIva = reader.GetOrdinal("partitaIva");
                    int indexNazione = reader.GetOrdinal("nazione");
                    int indexIndirizzo = reader.GetOrdinal("indirizzoSede");
                    int indexComune = reader.GetOrdinal("comune");
                    int indexCap = reader.GetOrdinal("CAP");
                    int indexProvincia = reader.GetOrdinal("provincia");
                    int indexIncaricoId = reader.GetOrdinal("idTipoIncaricoAppalto");
                    int indexIncaricoDescrione = reader.GetOrdinal("descrizioneTipoIncaricoAppalto");
                    ImpresaNotifichePreliminari impresa;
                    string incaricoId;
                    string incaricoDesc;
                    while (reader.Read())
                    {
                        impresa = new ImpresaNotifichePreliminari
                        {
                            CodiceFiscale = reader.GetString(indexCf),
                            PartitaIva = reader.IsDBNull(indexPIva) ? null : reader.GetString(indexPIva),
                            RagioneSociale = reader.GetString(indexRagSoc),
                            IdImpresa = reader.GetInt32(indexIDImpresa)
                        };

                        incaricoId = reader.IsDBNull(indexIncaricoId)
                            ? ""
                            : reader.GetString(indexIncaricoId);
                        incaricoDesc = reader.IsDBNull(indexIncaricoDescrione)
                            ? ""
                            : reader.GetString(indexIncaricoDescrione);
                        impresa.IncaricoImpresa = new IncaricoImpresa(incaricoId, incaricoDesc);

                        impresa.Indirizzo = new TBridge.Cemi.Type.Entities.Indirizzo
                        {
                            Cap = reader.IsDBNull(indexCap)
                                ? ""
                                : reader.GetString(indexCap),
                            NomeVia = reader.IsDBNull(indexIndirizzo)
                                ? ""
                                : reader.GetString(indexIndirizzo),
                            Comune = reader.IsDBNull(indexComune)
                                ? ""
                                : reader.GetString(indexComune),
                            Provincia = reader.IsDBNull(indexProvincia)
                                ? ""
                                : reader.GetString(indexProvincia),
                            Stato = reader.IsDBNull(indexNazione)
                                ? ""
                                : reader.GetString(indexNazione)
                        };


                        imprese.Add(impresa);
                    }
                }
            }

            return imprese;
        }

        public IndirizzoCollection IndirizziSelectByNumeroNotifica(string numeroNotifica)
        {
            IndirizzoCollection indirizzi = new IndirizzoCollection();

            using (DbCommand command =
                _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariIndirizziSelectByNumeroNotifica"))
            {
                _databaseCemi.AddInParameter(command, "@numeroNotifica", DbType.String, numeroNotifica);


                using (IDataReader reader = _databaseCemi.ExecuteReader(command))
                {
                    int indexIdIndirizzo = reader.GetOrdinal("idNotificaPreliminareIndirizzi");

                    int indexIndirizzo = reader.GetOrdinal("indirizzo");
                    int indexComune = reader.GetOrdinal("comune");
                    int indexDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
                    int indexDurata = reader.GetOrdinal("durataLavori");
                    int indexDescrizioneDurata = reader.GetOrdinal("descrizioneDurataLavori");
                    int indexAreaId = reader.GetOrdinal("idArea");
                    int indexAreaDescrizione = reader.GetOrdinal("descrizioneArea");
                    int indexAreaNote = reader.GetOrdinal("noteArea");
                    int indexNumLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");
                    int indexProvincia = reader.GetOrdinal("provincia");
                    int indexCap = reader.GetOrdinal("CAP");

                    Indirizzo indirizzo;
                    while (reader.Read())
                    {
                        indirizzo = new Indirizzo();
                        indirizzo.IdIndirizzo = reader.GetInt32(indexIdIndirizzo);
                        indirizzo.Comune = reader.GetString(indexComune);
                        indirizzo.NomeVia = reader.GetString(indexIndirizzo);
                        indirizzo.DataInizioLavori = reader.IsDBNull(indexDataInizioLavori)
                            ? (DateTime?) null
                            : reader.GetDateTime(indexDataInizioLavori);
                        indirizzo.NumeroDurata =
                            reader.IsDBNull(indexDurata) ? (int?) null : reader.GetInt32(indexDurata);
                        indirizzo.DescrizioneDurata = reader.IsDBNull(indexDescrizioneDurata)
                            ? null
                            : reader.GetString(indexDescrizioneDurata);
                        indirizzo.NumeroMassimoLavoratori = reader.IsDBNull(indexNumLavoratori)
                            ? (int?) null
                            : reader.GetInt32(indexNumLavoratori);
                        indirizzo.Provincia = reader.IsDBNull(indexProvincia) ? null : reader.GetString(indexProvincia);
                        indirizzo.Cap = reader.IsDBNull(indexCap) ? null : reader.GetString(indexCap);

                        indirizzi.Add(indirizzo);
                    }
                }
            }

            return indirizzi;
        }

        public CantiereCollection CantieriPerCommittenteSelectByFilter(NotificaPreliminareFilter filter)
        {
            CantiereCollection cantieri = new CantiereCollection();

            using (DbCommand comando =
                _databaseCemi.GetStoredProcCommand(
                    "dbo.USP_NotifichePreliminariCantieriSelectRicercaCantieriOrdPerCommittente"))
            {
                _databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                //   _databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filter.CantiereIndirizzo);
                _databaseCemi.AddInParameter(comando, "@comune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@provincia", DbType.String, filter.CantiereProvncia);
                _databaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filter.DataComunicazioneDal);
                _databaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@impresa", DbType.String, filter.ImpresaRagioneSociale);
                _databaseCemi.AddInParameter(comando, "@ivaFiscImpresa", DbType.String, filter.ImpresaCfPIva);
                _databaseCemi.AddInParameter(comando, "@provinciaImpresa", DbType.String, filter.ImpresaProvincia);
                _databaseCemi.AddInParameter(comando, "@capImpresa", DbType.String, filter.ImpresaCap);

                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, filter.NumeroNotifica);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriDal", DbType.DateTime,
                    filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriAl", DbType.DateTime,
                    filter.DataInizioLavoriAl);
                _databaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filter.NaturaOpera);
                //_databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                //_databaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String, filter.CantiereIndirizzo);
                //_databaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String, filter.CantiereProvncia);
                //_databaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, filter.CantiereCap);
                //_databaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@committenteCodiceFiscale", DbType.String,
                    filter.CommittenteCodiceFiscale);
                _databaseCemi.AddInParameter(comando, "@committenteCognome", DbType.String, filter.CommittenteCognome);
                _databaseCemi.AddInParameter(comando, "@committenteNome", DbType.String, filter.CommittenteNome);
                //_databaseCemi.AddInParameter(comando, "@impresaRagioneSociale", DbType.String, filter.ImpresaRagioneSociale);
                //_databaseCemi.AddInParameter(comando, "@impresaCF", DbType.String, filter.ImpresaCfPIva);
                //_databaseCemi.AddInParameter(comando, "@impresaProvincia", DbType.String, filter.ImpresaProvincia);
                //_databaseCemi.AddInParameter(comando, "@impresaCAP", DbType.String, filter.ImpresaCap);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneDa", DbType.DateTime, filter.DataComunicazioneDal);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneA", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@indirizzoGeolocalizzato", DbType.Boolean,
                    filter.CantiereIndirizzoGeolocalizzato);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexNumeroNotifica = reader.GetOrdinal("numeroNotifica");
                    int indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    int indexIndirizzo = reader.GetOrdinal("indirizzo");
                    int indexComune = reader.GetOrdinal("comune");
                    int indexDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
                    int indexDurata = reader.GetOrdinal("durataLavori");
                    int indexDescrizioneDurata = reader.GetOrdinal("descrizioneDurataLavori");
                    int indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    int indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    int indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    int indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    int indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    int indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    int indexCommittenteCognome = reader.GetOrdinal("committenteCognome");
                    int indexCommittenteNome = reader.GetOrdinal("committenteNome");
                    int indexCommittenteCodiceFiscale = reader.GetOrdinal("committenteCodiceFiscale");
                    int indexImpresaRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    int indexImpresaCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    int indexImpresaProvincia = reader.GetOrdinal("impresaProvincia");
                    int indexImpresaCap = reader.GetOrdinal("impresaCap");
                    int indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    int indexNumeroLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");
                    int indexLogitudine = reader.GetOrdinal("geolocalizzazioneLongitudine");
                    int indexLatitudine = reader.GetOrdinal("geolocalizzazioneLatitudine");
                    int indexIndirizzoProvincia = reader.GetOrdinal("provincia");

                    #endregion

                    while (reader.Read())
                    {
                        Cantiere cantiere = new Cantiere();
                        cantieri.Add(cantiere);

                        cantiere.NumeroNotifica = reader.GetString(indexNumeroNotifica);
                        cantiere.DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione);
                        //cantiere.Indirizzo = reader.GetString(indexIndirizzo);
                        //cantiere.Comune = reader.GetString(indexComune);
                        cantiere.DataInizio = !reader.IsDBNull(indexDataInizioLavori)
                            ? (DateTime?) reader.GetDateTime(indexDataInizioLavori)
                            : null;
                        cantiere.Durata = !reader.IsDBNull(indexDurata) ? reader.GetInt32(indexDurata) : 0;
                        cantiere.DescrizioneDurata = reader.IsDBNull(indexDescrizioneDurata)
                            ? null
                            : reader.GetString(indexDescrizioneDurata);

                        cantiere.TipoCategoriaDescrizione = reader.IsDBNull(indexDescrizioneDurata)
                            ? null
                            : reader.GetString(indexDescrizioneTipoCategoria);
                        cantiere.TipoOperaDescrizione = reader.IsDBNull(indexDescrizioneTipoOpera)
                            ? null
                            : reader.GetString(indexDescrizioneTipoOpera);
                        cantiere.TipoTipologiaDescrizione = reader.IsDBNull(indexDescrizioneTipoTipologia)
                            ? null
                            : reader.GetString(indexDescrizioneTipoTipologia);
                        cantiere.AltraCategoriaDescrizione = reader.IsDBNull(indexDescrizioneAltraCategoria)
                            ? null
                            : reader.GetString(indexDescrizioneAltraCategoria);
                        cantiere.AltraTipologiaDescrizione = reader.IsDBNull(indexDescrizioneAltraTipologia)
                            ? null
                            : reader.GetString(indexDescrizioneAltraTipologia);
                        cantiere.CommittenteCognome = reader.IsDBNull(indexCommittenteCognome)
                            ? null
                            : reader.GetString(indexCommittenteCognome);
                        cantiere.CommittenteNome = reader.IsDBNull(indexCommittenteNome)
                            ? null
                            : reader.GetString(indexCommittenteNome);
                        cantiere.CommittenteCodiceFiscale = reader.IsDBNull(indexCommittenteCodiceFiscale)
                            ? null
                            : reader.GetString(indexCommittenteCodiceFiscale);

                        cantiere.ImpresaRagioneSociale = !reader.IsDBNull(indexImpresaRagioneSociale)
                            ? reader.GetString(indexImpresaRagioneSociale)
                            : null;
                        cantiere.ImpresaCodiceFiscale = !reader.IsDBNull(indexImpresaCodiceFiscale)
                            ? reader.GetString(indexImpresaCodiceFiscale)
                            : null;
                        cantiere.ImpresaProvincia = !reader.IsDBNull(indexImpresaProvincia)
                            ? reader.GetString(indexImpresaProvincia)
                            : null;
                        cantiere.ImpresaCap =
                            !reader.IsDBNull(indexImpresaCap) ? reader.GetString(indexImpresaCap) : null;
                        cantiere.NumeroImprese = !reader.IsDBNull(indexNumeroImprese)
                            ? (int?) reader.GetInt32(indexNumeroImprese)
                            : null;
                        cantiere.NumeroLavoratori = !reader.IsDBNull(indexNumeroLavoratori)
                            ? (int?) reader.GetInt32(indexNumeroLavoratori)
                            : null;
                        cantiere.AmmontareComplessivo =
                            reader.IsDBNull(indexAmmontare) ? 0 : reader.GetDecimal(indexAmmontare);

                        cantiere.Indirizzo = new Type.Entities.Geocode.Indirizzo
                        {
                            NomeVia = reader.IsDBNull(indexIndirizzo) ? null : reader.GetString(indexIndirizzo),
                            Comune = reader.IsDBNull(indexComune) ? null : reader.GetString(indexComune),
                            Provincia = reader.IsDBNull(indexIndirizzoProvincia)
                                ? null
                                : reader.GetString(indexIndirizzoProvincia),
                            Latitudine =
                                reader.IsDBNull(indexLatitudine) ? (decimal?) null : reader.GetDecimal(indexLatitudine),
                            Longitudine = reader.IsDBNull(indexLogitudine)
                                ? (decimal?) null
                                : reader.GetDecimal(indexLogitudine)
                        };
                    }
                }
            }

            return cantieri;
        }

        public CantiereCollection CantieriPerImpresaSelectByFilter(NotificaPreliminareFilter filter)
        {
            CantiereCollection cantieri = new CantiereCollection();

            using (DbCommand comando =
                _databaseCemi.GetStoredProcCommand(
                    "dbo.USP_NotifichePreliminariCantieriSelectRicercaCantieriOrdPerImpresa"))
            {
                _databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                // _databaseCemi.AddInParameter(comando, "@dataInizio", DbType.DateTime, filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@indirizzo", DbType.String, filter.CantiereIndirizzo);
                _databaseCemi.AddInParameter(comando, "@comune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@provincia", DbType.String, filter.CantiereProvncia);
                _databaseCemi.AddInParameter(comando, "@dal", DbType.DateTime, filter.DataComunicazioneDal);
                _databaseCemi.AddInParameter(comando, "@al", DbType.DateTime, filter.DataComunicazioneAl);
                _databaseCemi.AddInParameter(comando, "@impresa", DbType.String, filter.ImpresaRagioneSociale);
                _databaseCemi.AddInParameter(comando, "@ivaFiscImpresa", DbType.String, filter.ImpresaCfPIva);
                _databaseCemi.AddInParameter(comando, "@provinciaImpresa", DbType.String, filter.ImpresaProvincia);
                _databaseCemi.AddInParameter(comando, "@capImpresa", DbType.String, filter.ImpresaCap);

                _databaseCemi.AddInParameter(comando, "@numeroNotifica", DbType.String, filter.NumeroNotifica);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriDal", DbType.DateTime,
                    filter.DataInizioLavoriDal);
                _databaseCemi.AddInParameter(comando, "@dataInizioLavoriAl", DbType.DateTime,
                    filter.DataInizioLavoriAl);
                _databaseCemi.AddInParameter(comando, "@naturaOpera", DbType.String, filter.NaturaOpera);
                //_databaseCemi.AddInParameter(comando, "@ammontare", DbType.Decimal, filter.Ammontare);
                //_databaseCemi.AddInParameter(comando, "@cantiereIndirizzo", DbType.String, filter.CantiereIndirizzo);
                //_databaseCemi.AddInParameter(comando, "@cantiereProvincia", DbType.String, filter.CantiereProvncia);
                //_databaseCemi.AddInParameter(comando, "@cantiereCap", DbType.String, filter.CantiereCap);
                //_databaseCemi.AddInParameter(comando, "@cantiereComune", DbType.String, filter.CantiereComune);
                _databaseCemi.AddInParameter(comando, "@committenteCodiceFiscale", DbType.String,
                    filter.CommittenteCodiceFiscale);
                _databaseCemi.AddInParameter(comando, "@committenteCognome", DbType.String, filter.CommittenteCognome);
                _databaseCemi.AddInParameter(comando, "@committenteNome", DbType.String, filter.CommittenteNome);
                //_databaseCemi.AddInParameter(comando, "@impresaRagioneSociale", DbType.String, filter.ImpresaRagioneSociale);
                //_databaseCemi.AddInParameter(comando, "@impresaCF", DbType.String, filter.ImpresaCfPIva);
                //_databaseCemi.AddInParameter(comando, "@impresaProvincia", DbType.String, filter.ImpresaProvincia);
                //_databaseCemi.AddInParameter(comando, "@impresaCAP", DbType.String, filter.ImpresaCap);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneDa", DbType.DateTime, filter.DataComunicazioneDal);
                //_databaseCemi.AddInParameter(comando, "@dataComunicazoneA", DbType.DateTime, filter.DataComunicazioneAl);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region index

                    int indexNumeroNotifica = reader.GetOrdinal("numeroNotifica");
                    int indexDataComunicazione = reader.GetOrdinal("dataComunicazioneNotifica");
                    int indexIndirizzo = reader.GetOrdinal("indirizzo");
                    int indexComune = reader.GetOrdinal("comune");
                    int indexDataInizioLavori = reader.GetOrdinal("dataInizioLavori");
                    int indexDurata = reader.GetOrdinal("durataLavori");
                    int indexDescrizioneDurata = reader.GetOrdinal("descrizioneDurataLavori");
                    int indexDescrizioneTipoOpera = reader.GetOrdinal("descrizioneTipoOpera");
                    int indexDescrizioneTipoTipologia = reader.GetOrdinal("descrizioneTipoTipologia");
                    int indexDescrizioneTipoCategoria = reader.GetOrdinal("descrizioneTipoCategoria");
                    int indexDescrizioneAltraCategoria = reader.GetOrdinal("descrizioneAltraCategoria");
                    int indexDescrizioneAltraTipologia = reader.GetOrdinal("descrizioneAltraTipologia");
                    int indexAmmontare = reader.GetOrdinal("ammontareComplessivo");
                    int indexCommittenteCognome = reader.GetOrdinal("committenteCognome");
                    int indexCommittenteNome = reader.GetOrdinal("committenteNome");
                    int indexCommittenteCodiceFiscale = reader.GetOrdinal("committenteCodiceFiscale");
                    int indexImpresaRagioneSociale = reader.GetOrdinal("impresaRagioneSociale");
                    int indexImpresaCodiceFiscale = reader.GetOrdinal("impresaCodiceFiscale");
                    int indexImpresaProvincia = reader.GetOrdinal("impresaProvincia");
                    int indexImpresaCap = reader.GetOrdinal("impresaCap");
                    int indexNumeroImprese = reader.GetOrdinal("numeroImprese");
                    int indexNumeroLavoratori = reader.GetOrdinal("numeroMassimoLavoratori");
                    int indexLogitudine = reader.GetOrdinal("geolocalizzazioneLongitudine");
                    int indexLatitudine = reader.GetOrdinal("geolocalizzazioneLatitudine");
                    int indexIndirizzoProvincia = reader.GetOrdinal("provincia");

                    #endregion

                    while (reader.Read())
                    {
                        Cantiere cantiere = new Cantiere();
                        cantieri.Add(cantiere);

                        cantiere.NumeroNotifica = reader.GetString(indexNumeroNotifica);
                        cantiere.DataComunicazioneNotifica = reader.GetDateTime(indexDataComunicazione);
                        //cantiere.Indirizzo = reader.GetString(indexIndirizzo);
                        //cantiere.Comune = reader.GetString(indexComune);
                        cantiere.DataInizio = !reader.IsDBNull(indexDataInizioLavori)
                            ? (DateTime?) reader.GetDateTime(indexDataInizioLavori)
                            : null;
                        cantiere.Durata = !reader.IsDBNull(indexDurata) ? reader.GetInt32(indexDurata) : 0;
                        cantiere.DescrizioneDurata = reader.IsDBNull(indexDescrizioneDurata)
                            ? null
                            : reader.GetString(indexDescrizioneDurata);

                        cantiere.TipoCategoriaDescrizione = reader.IsDBNull(indexDescrizioneDurata)
                            ? null
                            : reader.GetString(indexDescrizioneTipoCategoria);
                        cantiere.TipoOperaDescrizione = reader.IsDBNull(indexDescrizioneTipoOpera)
                            ? null
                            : reader.GetString(indexDescrizioneTipoOpera);
                        cantiere.TipoTipologiaDescrizione = reader.IsDBNull(indexDescrizioneTipoTipologia)
                            ? null
                            : reader.GetString(indexDescrizioneTipoTipologia);
                        cantiere.AltraCategoriaDescrizione = reader.IsDBNull(indexDescrizioneAltraCategoria)
                            ? null
                            : reader.GetString(indexDescrizioneAltraCategoria);
                        cantiere.AltraTipologiaDescrizione = reader.IsDBNull(indexDescrizioneAltraTipologia)
                            ? null
                            : reader.GetString(indexDescrizioneAltraTipologia);
                        cantiere.CommittenteCognome = reader.IsDBNull(indexCommittenteCognome)
                            ? null
                            : reader.GetString(indexCommittenteCognome);
                        cantiere.CommittenteNome = reader.IsDBNull(indexCommittenteNome)
                            ? null
                            : reader.GetString(indexCommittenteNome);
                        cantiere.CommittenteCodiceFiscale = reader.IsDBNull(indexCommittenteCodiceFiscale)
                            ? null
                            : reader.GetString(indexCommittenteCodiceFiscale);

                        cantiere.ImpresaRagioneSociale = !reader.IsDBNull(indexImpresaRagioneSociale)
                            ? reader.GetString(indexImpresaRagioneSociale)
                            : null;
                        cantiere.ImpresaCodiceFiscale = !reader.IsDBNull(indexImpresaCodiceFiscale)
                            ? reader.GetString(indexImpresaCodiceFiscale)
                            : null;
                        cantiere.ImpresaProvincia = !reader.IsDBNull(indexImpresaProvincia)
                            ? reader.GetString(indexImpresaProvincia)
                            : null;
                        cantiere.ImpresaCap =
                            !reader.IsDBNull(indexImpresaCap) ? reader.GetString(indexImpresaCap) : null;
                        cantiere.NumeroImprese = !reader.IsDBNull(indexNumeroImprese)
                            ? (int?) reader.GetInt32(indexNumeroImprese)
                            : null;
                        cantiere.NumeroLavoratori = !reader.IsDBNull(indexNumeroLavoratori)
                            ? (int?) reader.GetInt32(indexNumeroLavoratori)
                            : null;
                        cantiere.AmmontareComplessivo =
                            reader.IsDBNull(indexAmmontare) ? 0 : reader.GetDecimal(indexAmmontare);

                        cantiere.Indirizzo = new Type.Entities.Geocode.Indirizzo
                        {
                            NomeVia = reader.IsDBNull(indexIndirizzo) ? null : reader.GetString(indexIndirizzo),
                            Comune = reader.IsDBNull(indexComune) ? null : reader.GetString(indexComune),
                            Provincia = reader.IsDBNull(indexIndirizzoProvincia)
                                ? null
                                : reader.GetString(indexIndirizzoProvincia),
                            Latitudine =
                                reader.IsDBNull(indexLatitudine) ? (decimal?) null : reader.GetDecimal(indexLatitudine),
                            Longitudine = reader.IsDBNull(indexLogitudine)
                                ? (decimal?) null
                                : reader.GetDecimal(indexLogitudine)
                        };
                    }
                }
            }

            return cantieri;
        }

        public TBridge.Cemi.Type.Collections.Cantieri.CantiereCollection GetCantieriGenerati(string protocolloRegione)
        {
            TBridge.Cemi.Type.Collections.Cantieri.CantiereCollection cantieri =
                new TBridge.Cemi.Type.Collections.Cantieri.CantiereCollection();

            using (DbCommand comando =
                _databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariSelectCantieriGenerati"))
            {
                _databaseCemi.AddInParameter(comando, "@protocolloRegione", DbType.String, protocolloRegione);

                using (IDataReader reader = _databaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceId = reader.GetOrdinal("idCantiere");
                    int indiceIndirizzo = reader.GetOrdinal("indirizzo");
                    int indiceComune = reader.GetOrdinal("comune");
                    int indiceProvincia = reader.GetOrdinal("provincia");

                    #endregion

                    while (reader.Read())
                    {
                        TBridge.Cemi.Type.Entities.Cantieri.Cantiere indirizzo =
                            new TBridge.Cemi.Type.Entities.Cantieri.Cantiere();
                        cantieri.Add(indirizzo);

                        indirizzo.IdCantiere = reader.GetInt32(indiceId);
                        if (!reader.IsDBNull(indiceIndirizzo))
                        {
                            indirizzo.Indirizzo = reader.GetString(indiceIndirizzo);
                        }
                        if (!reader.IsDBNull(indiceComune))
                        {
                            indirizzo.Comune = reader.GetString(indiceComune);
                        }
                        if (!reader.IsDBNull(indiceProvincia))
                        {
                            indirizzo.Provincia = reader.GetString(indiceProvincia);
                        }
                    }
                }
            }

            return cantieri;
        }
    }
}