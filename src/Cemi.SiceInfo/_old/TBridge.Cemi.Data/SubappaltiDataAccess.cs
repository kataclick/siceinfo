using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.Data
{
    /// <summary>
    ///     Classe di accesso ai dati per i subappalti
    /// </summary>
    public class SubappaltiDataAccess
    {
        /// <summary>
        ///     Costruttore. Inizializza il database
        /// </summary>
        public SubappaltiDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        /// <summary>
        ///     Database di riferimento
        /// </summary>
        public Database DatabaseCemi { get; set; }

        /// <summary>
        ///     Restituisce il conteggio delle ricerche di un utente nella giornata attuale
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public int ConteggioRicerche(int idUtente)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_RicercheStoricoConteggio");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdUtente", DbType.Int32, idUtente);
            // Ricavo il risultato da un dataset
            DataTable dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0]; // Se non c'� � giusto che esploda
            // Ricavo l'unica riga e l'unica colonna di risultato
            var iRes = (int) dtRes.Rows[0]["Conteggio"];
            return iRes;
        } // ConteggioRicerche

        /// <summary>
        ///     Seleziona una lista di imprese in base ai parametri, restituendo una DataTable
        /// </summary>
        /// <param name="idImpresa"></param>
        public DataTable RicercaImpresa(int idImpresa)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRicerca");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
            // Ricavo il risultato da un dataset
            var dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0];
            return dtRes;
        } // RicercaImpresa

        /// <summary>
        ///     Seleziona una lista di imprese in base ai parametri, restituendo una DataTable
        /// </summary>
        /// <param name="ragioneSociale"></param>
        public DataTable RicercaImpresa(string ragioneSociale)
        {
            // TODO: Vedere se si pu� fondere in qualche modo con quella sopra
            // Si pu� fare con i nullabletypes
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseRicerca");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@RagioneSociale", DbType.String, ragioneSociale);
            // Ricavo il risultato da un dataset
            var dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0];
            return dtRes;
        } // RicercaImpresa


        /// <summary>
        ///     Carica una singola impresa dal database restituendola come DataRow
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <returns></returns>
        public DataRow CaricaImpresa(int idImpresa)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_ImpreseSelectById");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
            // Ricavo il risultato da un dataset
            var drRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0].Rows[0];
            return drRes;
        } // CaricaImpresa

        /// <summary>
        ///     Seleziona un lavoratore in base ai parametri, inserito in una DataTable
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="idLavoratore"></param>
        /// <returns></returns>
        public DataTable RicercaLavoratori(int idImpresa, int idLavoratore)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriSelectSingolo");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@IdLavoratore", DbType.Int32, idLavoratore);
            // Ricavo il risultato da un dataset
            var dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0];
            return dtRes;
        } // RicercaLavoratori

        /// <summary>
        ///     Seleziona uno o pi� lavoratori in base ai parametri, restituendo una DataTable
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="cognome"></param>
        /// <param name="nome"></param>
        /// <returns></returns>
        public DataTable RicercaLavoratori(int idImpresa, string cognome, string nome)
        {
            // TODO: Vedere se si pu� fondere in qualche modo con quella sopra
            // Si pu� fare con i nullabletypes
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_LavoratoriSelectSingolo");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@Cognome", DbType.String, cognome);
            if (!string.IsNullOrEmpty(nome))
                DatabaseCemi.AddInParameter(comando, "@Nome", DbType.String, nome);
            // Ricavo il risultato da un dataset
            var dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0];
            return dtRes;
        } //RicercaLavoratori

        /// <summary>
        ///     Inserisce nello storico la ricerca di un utente
        ///     Restituisce l'id del record appena inserito
        /// </summary>
        /// <param name="idUtente">Id dell'utente che ha effettuato la ricerca</param>
        /// <param name="idImpresa">Id impresa ricercata</param>
        /// <param name="criteri">Stringa descrittiva dei criteri di ricerca</param>
        public int InserisciRicerca(int idUtente, int idImpresa, string criteri)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_RicercheStoricoInsert");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdUtente", DbType.Int32, idUtente);
            DatabaseCemi.AddInParameter(comando, "@IDImpresa", DbType.Int32, idImpresa);
            DatabaseCemi.AddInParameter(comando, "@Criteri", DbType.String,
                !string.IsNullOrEmpty(criteri) ? criteri : "");
            // Ricavo il risultato da un dataset
            DataTable dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0]; // Se non c'� � giusto che esploda
            var iRes = int.Parse(dtRes.Rows[0]["idRicerca"].ToString());
            return iRes;
        } // InserisciRicerca

        /// <summary>
        ///     Aggiunge un criterio nello storico di una ricerca specifica
        /// </summary>
        /// <param name="idRicerca">Identificativo della ricerca</param>
        /// <param name="criterio">Criterio da aggiungere</param>
        public void AggiungiCriterioRicerca(int idRicerca, string criterio)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_RicercheStoricoUpdate");
            // Imposto i parametri
            DatabaseCemi.AddInParameter(comando, "@IdRicerca", DbType.Int32, idRicerca);
            DatabaseCemi.AddInParameter(comando, "@Criteri", DbType.String, criterio);

            // Eseguo la query
            DatabaseCemi.ExecuteNonQuery(comando);
        } // AggiungiCriterioRicerca

        /// <summary>
        ///     Visualizza dallo storico una lista di ricerche
        ///     in base ai parametri selezionati, restituendo una DataTable
        /// </summary>
        /// <param name="idUtente">
        ///     utente che effettua la query dello storico
        ///     Da inserire se � un'impresa che sta facendo la richiesta per vedere i propri storici
        /// </param>
        /// <param name="idImpresa">Impresa ricercata</param>
        /// <param name="idImpresaRicercante">Impresa che ha effettuato la ricerca</param>
        /// <param name="dataDa">Data Da del range di ricerca</param>
        /// <param name="dataA">Data A del range di ricerca</param>
        /// <returns></returns>
        public DataTable VisualizzaStorico(int? idUtente, int? idImpresa,
            int? idImpresaRicercante,
            DateTime? dataDa, DateTime? dataA)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_RicercheStoricoSelect");
            // Imposto i parametri

            if (idUtente != null)
                DatabaseCemi.AddInParameter(comando, "@IdUtente", DbType.Int32, idUtente);
            if (idImpresa != null)
                DatabaseCemi.AddInParameter(comando, "@IdImpresa", DbType.Int32, idImpresa);
            if (idImpresaRicercante != null)
                DatabaseCemi.AddInParameter(comando, "@IdImpresaRicercante", DbType.Int32, idImpresaRicercante);
            if (dataDa != null)
                DatabaseCemi.AddInParameter(comando, "@DataDa", DbType.DateTime, dataDa);
            if (dataA != null)
                DatabaseCemi.AddInParameter(comando, "@DataA", DbType.DateTime, dataA);

            // Ricavo il risultato da un dataset
            var dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0];
            return dtRes;
        } // VisualizzaStorico

        public DataTable VisualizzaStoricoRicercheLavoratori(int? idUtente, int? idImpresa)
        {
            // Seleziono la stored procedure
            DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_RicercheStoricoSelectLavoratori");
            // Imposto i parametri

            if (idUtente != null)
                DatabaseCemi.AddInParameter(comando, "@IdUtenteRicercante", DbType.Int32, idUtente);
            if (idImpresa != null)
                DatabaseCemi.AddInParameter(comando, "@IdImpresaRicercata", DbType.Int32, idImpresa);

            // Ricavo il risultato da un dataset
            var dtRes = DatabaseCemi.ExecuteDataSet(comando).Tables[0];
            return dtRes;
        }
    } // Class
} // Namespace