﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using TipoContratto = TBridge.Cemi.Type.Entities.Sintesi.TipoContratto;

namespace TBridge.Cemi.Data
{
    public class SintesiDataAccess
    {
        public SintesiDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public List<StatusStraniero> GetStatusStraniero()
        {
            List<StatusStraniero> status = new List<StatusStraniero>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiStatusStranieroSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        StatusStraniero statusS = new StatusStraniero();
                        status.Add(statusS);

                        statusS.Codice = reader.GetString(indiceCodice);
                        statusS.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return status;
        }

        public List<MotivoPermesso> GetMotiviPermesso()
        {
            List<MotivoPermesso> motivi = new List<MotivoPermesso>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiMotiviPermessoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        MotivoPermesso motivo = new MotivoPermesso();
                        motivi.Add(motivo);

                        motivo.Codice = reader.GetString(indiceCodice);
                        motivo.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return motivi;
        }

        public List<TitoloDiStudio> GetTitoliDiStudio()
        {
            List<TitoloDiStudio> titoli = new List<TitoloDiStudio>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTitoliDiStudioSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TitoloDiStudio titolo = new TitoloDiStudio();
                        titoli.Add(titolo);

                        titolo.Codice = reader.GetString(indiceCodice);
                        titolo.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return titoli;
        }

        public List<TipoComunicazione> GetTipiComunicazione()
        {
            List<TipoComunicazione> tipiComunicazione = new List<TipoComunicazione>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTipiComunicazioneSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoComunicazione tipoComunicazione = new TipoComunicazione();
                        tipiComunicazione.Add(tipoComunicazione);

                        tipoComunicazione.Codice = reader.GetString(indiceCodice);
                        tipoComunicazione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiComunicazione;
        }

        //NON USATO
        public List<TipoContratto> GetTipiContratto()
        {
            List<TipoContratto> tipiContratto = new List<TipoContratto>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTipiContrattoSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoContratto tipoContratto = new TipoContratto();
                        tipiContratto.Add(tipoContratto);

                        tipoContratto.Codice = reader.GetString(indiceCodice);
                        tipoContratto.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiContratto;
        }

        public List<CessazioneRL> GetCessazioniRL()
        {
            List<CessazioneRL> cessazioni = new List<CessazioneRL>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCessazioniRLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        CessazioneRL cessazione = new CessazioneRL();
                        cessazioni.Add(cessazione);

                        cessazione.Codice = reader.GetString(indiceCodice);
                        cessazione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return cessazioni;
        }

        public List<TrasformazioneRL> GetTrasformazioniRL()
        {
            List<TrasformazioneRL> trasformazioni = new List<TrasformazioneRL>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTrasformazioniRLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TrasformazioneRL trasformazione = new TrasformazioneRL();
                        trasformazioni.Add(trasformazione);

                        trasformazione.Codice = reader.GetString(indiceCodice);
                        trasformazione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return trasformazioni;
        }

        public List<CCNL> GetCCNL()
        {
            List<CCNL> ccnls = new List<CCNL>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCCNLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        CCNL ccnl = new CCNL();
                        ccnls.Add(ccnl);

                        ccnl.Codice = reader.GetString(indiceCodice);
                        ccnl.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return ccnls;
        }

        public List<TipoOrario> GetTipiOrario()
        {
            List<TipoOrario> tipiOrario = new List<TipoOrario>();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiCCNLSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    #region Indici per Reader

                    int indiceCodice = reader.GetOrdinal("codice");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoOrario tipoOrario = new TipoOrario();
                        tipiOrario.Add(tipoOrario);

                        tipoOrario.Codice = reader.GetString(indiceCodice);
                        tipoOrario.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiOrario;
        }

        public Type.Entities.TipoContratto GetTipoContratto(string idTipocontratto)
        {
            Type.Entities.TipoContratto contratto = new Type.Entities.TipoContratto();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiContrattoSintesiSelectByTipoContratto"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idTipocontratto", DbType.String, idTipocontratto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        contratto.IdContratto = reader["idContratto"].ToString();
                        contratto.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }
            return contratto;
        }

        public TitoloDiStudio GetTitoloDiStudio(string codice)
        {
            TitoloDiStudio titoloDiStudio = new TitoloDiStudio();

            using (
                DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTitoliDiStudioSelectByCodice"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@codice", DbType.String, codice);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        titoloDiStudio.Codice = reader["codice"].ToString();
                        titoloDiStudio.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }

            return titoloDiStudio;
        }

        public NazionalitaSiceNew GetNazionalita(string idNazione)
        {
            NazionalitaSiceNew nazionalita = new NazionalitaSiceNew();

            using (DbCommand dbCommand = DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiNazioniSelectByIdNazione"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idNazione", DbType.String, idNazione);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        nazionalita.IdNazione = reader["idNazioneSiceNew"].ToString();
                        nazionalita.Nazionalita = reader["nomeNazione"].ToString();
                    }
                }
            }

            return nazionalita;
        }

        public TipoInizioRapporto GetTipoInizioRapporto(string idTipoInizioRapporto)
        {
            TipoInizioRapporto tipoInizioRapporto = new TipoInizioRapporto();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiInizioRapportoSintesiSelectByTipoInizioRapporto"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idTipoInizioRapporto", DbType.String, idTipoInizioRapporto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        tipoInizioRapporto.IdTipoInizioRapporto = reader["idTipoInizioRapporto"].ToString();
                        tipoInizioRapporto.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }
            return tipoInizioRapporto;
        }

        public TipoQualifica GetTipoQualifica(string idQualifica)
        {
            TipoQualifica tipoQualifica = new TipoQualifica();

            using (
                DbCommand dbCommand =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_SintesiTipiQualificaSelectByTipoQualifica"))
            {
                DatabaseCemi.AddInParameter(dbCommand, "@idQualifica", DbType.String, idQualifica);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        tipoQualifica.IdQualifica = reader["idQualifica"].ToString();
                        tipoQualifica.Descrizione = reader["descrizione"].ToString();
                    }
                }
            }
            return tipoQualifica;
        }

        public List<TipoCategoria> GetTipiCategoria()
        {
            List<TipoCategoria> tipiCategoria = new List<TipoCategoria>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCategoriaSelectSintesi"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdCategoria = reader.GetOrdinal("idCategoria");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCategoria tipoCategoria = new TipoCategoria();
                        tipiCategoria.Add(tipoCategoria);

                        tipoCategoria.IdCategoria = reader.GetString(indiceIdCategoria);
                        tipoCategoria.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCategoria;
        }

        public List<TipoCategoria> GetTipiCategoria(string tipoContratto)
        {
            List<TipoCategoria> tipiCategoria = new List<TipoCategoria>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCategoriaSelectSintesi"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoContratto", DbType.String, tipoContratto);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdCategoria = reader.GetOrdinal("idCategoria");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoCategoria tipoCategoria = new TipoCategoria();
                        tipiCategoria.Add(tipoCategoria);

                        tipoCategoria.IdCategoria = reader.GetString(indiceIdCategoria);
                        tipoCategoria.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiCategoria;
        }


        public List<TipoQualifica> GetTipiQualifica(string tipoCategoria)
        {
            List<TipoQualifica> tipiQualifica = new List<TipoQualifica>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.[USP_SintesiTipiQualificaSelect]"))
            {
                DatabaseCemi.AddInParameter(comando, "@tipoCategoria", DbType.String, tipoCategoria);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per reader

                    int indiceIdQualifica = reader.GetOrdinal("idQualifica");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoQualifica tipoQualifica = new TipoQualifica();
                        tipiQualifica.Add(tipoQualifica);

                        tipoQualifica.IdQualifica = reader.GetString(indiceIdQualifica);
                        tipoQualifica.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiQualifica;
        }
    }
}