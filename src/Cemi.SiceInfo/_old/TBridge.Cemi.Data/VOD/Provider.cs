﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.VCOD;
using TBridge.Cemi.Type.Filters.VCOD;

namespace TBridge.Cemi.Data.VOD
{
    public class Provider
    {
        private static readonly Database DatabaseSice = DatabaseFactory.CreateDatabase("CEMI");

        public static List<ImpresaConFasciaIndirizzo> GetImprese(ImpreseFilter impreseFilter)
        {
            List<ImpresaConFasciaIndirizzo> ret = new List<ImpresaConFasciaIndirizzo>();

            using (
                DbCommand dbCommand =
                    DatabaseSice.GetStoredProcCommand("dbo.USP_OreDenunciateAggregateSelectByDateWithMediaIndirizzo"))
            {
                dbCommand.CommandTimeout = 20000;
                DatabaseSice.AddInParameter(dbCommand, "@dataInizio", DbType.DateTime, impreseFilter.DataInizio);
                DatabaseSice.AddInParameter(dbCommand, "@dataFine", DbType.DateTime, impreseFilter.DataFine);
                DatabaseSice.AddInParameter(dbCommand, "@fascia", DbType.String, impreseFilter.Fascia);
                DatabaseSice.AddInParameter(dbCommand, "@idConsulente", DbType.Int32, impreseFilter.IdConsulente);
                DatabaseSice.AddInParameter(dbCommand, "@idImpresa", DbType.Int32, impreseFilter.IdImpresa);
                DatabaseSice.AddInParameter(dbCommand, "@mediaPartTime", DbType.Int32, impreseFilter.MediaPartTime);
                DatabaseSice.AddInParameter(dbCommand, "@operai", DbType.Int32, impreseFilter.NumeroOperai);
                DatabaseSice.AddInParameter(dbCommand, "@stato", DbType.String, impreseFilter.Stato);
                DatabaseSice.AddInParameter(dbCommand, "@idEsattore", DbType.String, impreseFilter.IdEsattore);
                DatabaseSice.AddInParameter(dbCommand, "@esitoControlloDenuncia", DbType.String,
                    impreseFilter.EsitoControlloDenuncia);
                DatabaseSice.AddInParameter(dbCommand, "@oreFerie", DbType.Decimal, impreseFilter.OreFerie);
                DatabaseSice.AddInParameter(dbCommand, "@orePermessiRetribuiti", DbType.Decimal,
                    impreseFilter.OrePermessiRetribuiti);
                DatabaseSice.AddInParameter(dbCommand, "@oreCigStraordinaria", DbType.Decimal,
                    impreseFilter.OreCigStraordinaria);
                DatabaseSice.AddInParameter(dbCommand, "@oreCigDeroga", DbType.Decimal, impreseFilter.OreCigDeroga);
                DatabaseSice.AddInParameter(dbCommand, "@oreCigoMaltempo", DbType.Decimal,
                    impreseFilter.OreCigoMaltempo);
                DatabaseSice.AddInParameter(dbCommand, "@oreCigo", DbType.Decimal, impreseFilter.OreCigo);

                using (IDataReader reader = DatabaseSice.ExecuteReader(dbCommand))
                {
                    int idImpresaIndex = reader.GetOrdinal("idImpresa");
                    int ragioneSocialeIndex = reader.GetOrdinal("ragioneSociale");
                    //int statoIndex = reader.GetOrdinal("stato");
                    //int mediaFinaleIndex = reader.GetOrdinal("mediaFinale");
                    int dataDenunciaIndex = reader.GetOrdinal("dataDenuncia");
                    int idConsulenteIndex = reader.GetOrdinal("idConsulente");
                    //int numeroOperaiIndex = reader.GetOrdinal("numeroOperai");
                    int idEsattoreIndex = reader.GetOrdinal("idEsattore");
                    //int mediaAssenzeIndex = reader.GetOrdinal("mediaAssenze");
                    //int parttimeIndex = reader.GetOrdinal("parttime");
                    int indirizzoConsulenteIndex = reader.GetOrdinal("indirizzoConsulente");
                    int capConsulenteIndex = reader.GetOrdinal("capConsulente");
                    int localitaConsulenteIndex = reader.GetOrdinal("localitaConsulente");
                    int provinciaConsulenteIndex = reader.GetOrdinal("provinciaConsulente");
                    int denominazioneImpresaIndex = reader.GetOrdinal("denominazioneImpresa");
                    int tipoViaImpresaIndex = reader.GetOrdinal("tipoViaImpresa");
                    int civicoImpresaIndex = reader.GetOrdinal("civicoImpresa");
                    int capImpresaIndex = reader.GetOrdinal("capImpresa");
                    int provinciaImpresaIndex = reader.GetOrdinal("provinciaImpresa");
                    int localitaImpresaIndex = reader.GetOrdinal("localitaImpresa");


                    while (reader.Read())
                    {
                        ImpresaConFasciaIndirizzo impresaConFasciaIndirizzo = new ImpresaConFasciaIndirizzo();
                        Indirizzo indirizzoConsulente = new Indirizzo();
                        Indirizzo indirizzoContatto = new Indirizzo();

                        impresaConFasciaIndirizzo.DataDenuncia = reader.GetDateTime(dataDenunciaIndex);
                        impresaConFasciaIndirizzo.IdImpresa = reader.GetInt32(idImpresaIndex);
                        impresaConFasciaIndirizzo.RagioneSociale = reader.GetString(ragioneSocialeIndex);

                        if (!reader.IsDBNull(idEsattoreIndex))
                            impresaConFasciaIndirizzo.IdEsattore = reader.GetString(idEsattoreIndex);

                        if (!reader.IsDBNull(capImpresaIndex))
                            indirizzoContatto.Cap = reader.GetString(capImpresaIndex);
                        if (!reader.IsDBNull(civicoImpresaIndex))
                            indirizzoContatto.Civico = reader.GetString(civicoImpresaIndex);
                        if (!reader.IsDBNull(localitaImpresaIndex))
                            indirizzoContatto.Comune = reader.GetString(localitaImpresaIndex);
                        if (!reader.IsDBNull(denominazioneImpresaIndex))
                            indirizzoContatto.NomeVia = reader.GetString(denominazioneImpresaIndex);
                        if (!reader.IsDBNull(provinciaImpresaIndex))
                            indirizzoContatto.Provincia = reader.GetString(provinciaImpresaIndex);
                        if (!reader.IsDBNull(tipoViaImpresaIndex))
                            indirizzoContatto.Qualificatore = reader.GetString(tipoViaImpresaIndex);

                        impresaConFasciaIndirizzo.IndirizzoContatto = indirizzoContatto;

                        if (!reader.IsDBNull(idConsulenteIndex))
                        {
                            impresaConFasciaIndirizzo.IdConsulente = reader.GetInt32(idConsulenteIndex);
                            if (!reader.IsDBNull(indirizzoConsulenteIndex))
                                indirizzoConsulente.NomeVia = reader.GetString(indirizzoConsulenteIndex);
                            if (!reader.IsDBNull(capConsulenteIndex))
                                indirizzoConsulente.Cap = reader.GetString(capConsulenteIndex);
                            if (!reader.IsDBNull(localitaConsulenteIndex))
                                indirizzoConsulente.Comune = reader.GetString(localitaConsulenteIndex);
                            if (!reader.IsDBNull(provinciaConsulenteIndex))
                                indirizzoConsulente.Provincia = reader.GetString(provinciaConsulenteIndex);
                        }

                        impresaConFasciaIndirizzo.IndirizzoConsulente = indirizzoConsulente;

                        ret.Add(impresaConFasciaIndirizzo);
                    }
                }
            }

            return ret;
        }
    }
}