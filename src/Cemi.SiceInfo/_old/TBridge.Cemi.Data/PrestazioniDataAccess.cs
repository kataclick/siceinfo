using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Cemi.SiceInfo.Utility;
using Microsoft.Practices.EnterpriseLibrary.Data;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.Prestazioni;
using TBridge.Cemi.Type.Exceptions.Prestazioni;
using TBridge.Cemi.Type.Filters.Prestazioni;
using Indirizzo = TBridge.Cemi.Type.Entities.Prestazioni.Indirizzo;

namespace TBridge.Cemi.Data
{
    public class PrestazioniDataAccess
    {
        public PrestazioniDataAccess()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }

        public bool IsLavoratoreInAnagrafica(Lavoratore lavoratore)
        {
            bool res;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniControlloLavoratoreInAnagrafica"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, lavoratore.CodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, lavoratore.Cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, lavoratore.Nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, lavoratore.DataNascita);
                DatabaseCemi.AddOutParameter(comando, "@idLavoratore", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@numeroLavoratori", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@idTipoPagamento", DbType.String, 1);

                DatabaseCemi.ExecuteNonQuery(comando);

                object idLavoratoreObject = DatabaseCemi.GetParameterValue(comando, "@idLavoratore");
                int righe = (int) DatabaseCemi.GetParameterValue(comando, "@numeroLavoratori");
                if (righe == 1 && !Convert.IsDBNull(idLavoratoreObject))
                {
                    lavoratore.IdLavoratore = (int) idLavoratoreObject;

                    object idTipoPagamentoObj = DatabaseCemi.GetParameterValue(comando, "@idTipoPagamento");
                    if (!Convert.IsDBNull(idTipoPagamentoObj))
                    {
                        lavoratore.IdTipoPagamento = (string) idTipoPagamentoObj;
                    }

                    res = true;
                }
                else
                {
                    res = false;
                }
            }

            return res;
        }

        public TipoPrestazioneCollection GetTipiPrestazionePerGradoParentela(string gradoParentela, bool soloAttive)
        {
            TipoPrestazioneCollection tipiPrestazione = new TipoPrestazioneCollection();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniConfigurazioneSelectPerGrado")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, gradoParentela);
                DatabaseCemi.AddInParameter(comando, "@soloAttive", DbType.Boolean, soloAttive);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        TipoPrestazione tipoPrestazione = new TipoPrestazione();
                        tipiPrestazione.Add(tipoPrestazione);

                        tipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        tipoPrestazione.Descrizione = (string) reader["descrizione"];

                        //leggo anche il periodo di validit�
                        if (!Convert.IsDBNull(reader["periodoInserimentoInizio"]))
                            tipoPrestazione.ValidaDa = (DateTime) reader["periodoInserimentoInizio"];
                        if (!Convert.IsDBNull(reader["periodoInserimentoFine"]))
                            tipoPrestazione.ValidaA = (DateTime) reader["periodoInserimentoFine"];

                        tipoPrestazione.GradoParentela = gradoParentela;
                    }
                }
            }

            return tipiPrestazione;
        }

        public TipoPrestazioneCollection GetTipiPrestazionePerGradoParentela(string gradoParentela)
        {
            return GetTipiPrestazionePerGradoParentela(gradoParentela, true);
        }

        public TipoPrestazioneCollection GetTipiPrestazione()
        {
            TipoPrestazioneCollection tipiPrestazione = new TipoPrestazioneCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiPrestazioneSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici

                    int indiceIdTipoPrestazione = reader.GetOrdinal("idTipoPrestazione");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");
                    int indiceIdGruppo = reader.GetOrdinal("prestazioniGruppoId");
                    int indiceDescrizioneGruppo = reader.GetOrdinal("prestazioniGruppoDescrizione");

                    #endregion

                    while (reader.Read())
                    {
                        TipoPrestazione tipoPrestazione = new TipoPrestazione();
                        tipiPrestazione.Add(tipoPrestazione);

                        tipoPrestazione.IdTipoPrestazione = reader.GetString(indiceIdTipoPrestazione);
                        tipoPrestazione.Descrizione = reader.GetString(indiceDescrizione);

                        tipoPrestazione.Gruppo = new Gruppo();
                        tipoPrestazione.Gruppo.Codice = reader.GetInt32(indiceIdGruppo);
                        tipoPrestazione.Gruppo.Descrizione = reader.GetString(indiceDescrizioneGruppo);
                    }
                }
            }

            return tipiPrestazione;
        }

        /// <summary>
        ///     Recupera i familiari del lavoratore filtrando per grado parentela
        /// </summary>
        /// <param name="idLavoratore">id del lavoratore</param>
        /// <param name="gradoParentela">Se grado parentela � null o empty non effettua filtro sul grado</param>
        /// <returns>lista dei familiari</returns>
        public FamiliareCollection GetFamiliariPerGradoParentela(int idLavoratore, string gradoParentela,
            bool conDataNascita)
        {
            FamiliareCollection familiari = new FamiliareCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFamiliariSelectPerGradoParentela"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                if (!string.IsNullOrEmpty(gradoParentela))
                    DatabaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, gradoParentela);
                DatabaseCemi.AddInParameter(comando, "@conDataNascita", DbType.Boolean, conDataNascita);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Familiare familiare = RecuperaFamiliareDaReader(reader);
                        familiari.Add(familiare);
                    }
                }
            }

            return familiari;
        }

        public Familiare GetFamiliare(int idLavoratore, int idFamiliare)
        {
            Familiare familiare;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFamiliariSelectPerChiave"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    familiare = RecuperaFamiliareDaReader(reader);
                }
            }

            return familiare;
        }

        private static Familiare RecuperaFamiliareDaReader(IDataReader reader)
        {
            Familiare familiare = new Familiare();

            familiare.IdLavoratore = (int) reader["idLavoratore"];
            familiare.IdFamiliare = (int) reader["idFamiliare"];
            familiare.Cognome = (string) reader["cognome"];
            familiare.Nome = (string) reader["nome"];
            if (!Convert.IsDBNull(reader["dataNascita"]))
                familiare.DataNascita = (DateTime) reader["dataNascita"];
            if (!Convert.IsDBNull(reader["codiceFiscale"]))
                familiare.CodiceFiscale = (string) reader["codiceFiscale"];
            if (!Convert.IsDBNull(reader["gradoParentela"]))
                familiare.GradoParentela = (string) reader["gradoParentela"];
            if (!Convert.IsDBNull(reader["sesso"]))
                familiare.Sesso = ((string) reader["sesso"])[0];
            if (!Convert.IsDBNull(reader["segnaleSoggettoACarico"]))
                familiare.ACarico = (string) reader["segnaleSoggettoACarico"];

            return familiare;
        }

        public Configurazione GetConfigurazionePrestazione(string idTipoPrestazione, string beneficiario,
            int idLavoratore)
        {
            Configurazione configurazione = null;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniConfigurazioneSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);
                DatabaseCemi.AddInParameter(comando, "@beneficiario", DbType.String, beneficiario);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        configurazione = new Configurazione();

                        configurazione.IdTipoPrestazione = idTipoPrestazione;
                        configurazione.GradoParentela = beneficiario;
                        configurazione.Reciprocita123 = (bool) reader["reciprocita123"];
                        configurazione.Reciprocita45 = (bool) reader["reciprocita45"];
                        configurazione.UnivocitaAnnuale = (bool) reader["univocitaAnnuale"];
                        configurazione.UnivocitaFamiliare = (bool) reader["univocitaFamiliare"];
                        configurazione.PeriodoRiferimentoInizio = (DateTime) reader["periodoRiferimentoInizio"];
                        configurazione.PeriodoRiferimentoFine = (DateTime) reader["periodoRiferimentoFine"];
                        configurazione.DifferenzaMesiFatturaDomanda = (byte) reader["differenzaMesiFatturaDomanda"];
                        configurazione.RichiestaFattura = (bool) reader["richiestaFattura"];
                        configurazione.RichiestaTipoScuola = (bool) reader["richiestaTipoScuola"];

                        configurazione.TipoMacroPrestazione = new TipoMacroPrestazione();
                        configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione =
                            (short) reader["idTipoMacroPrestazione"];
                        configurazione.TipoMacroPrestazione.Descrizione =
                            (string) reader["tipoMacroPrestazioneDescrizione"];

                        configurazione.TipoModulo = new TipoModulo();
                        configurazione.TipoModulo.IdTipoModulo = (short) reader["idTipoModulo"];
                        configurazione.TipoModulo.Descrizione = (string) reader["tipoModuloDescrizione"];
                        configurazione.TipoModulo.Modulo = (string) reader["modulo"];
                    }
                }
            }

            return configurazione;
        }

        public bool IsFamiliareInAnagrafica(Familiare familiare)
        {
            bool res;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniControlloFamiliareInAnagrafica"))
            {
                DatabaseCemi.AddInParameter(comando, "@codiceFiscale", DbType.String, familiare.CodiceFiscale);
                DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, familiare.Cognome);
                DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, familiare.Nome);
                DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, familiare.DataNascita);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, familiare.IdLavoratore);
                DatabaseCemi.AddOutParameter(comando, "@idFamiliare", DbType.Int32, 4);
                DatabaseCemi.AddOutParameter(comando, "@numeroFamiliari", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                object idFamiliareObject = DatabaseCemi.GetParameterValue(comando, "@idFamiliare");
                int righe = (int) DatabaseCemi.GetParameterValue(comando, "@numeroFamiliari");
                if (righe == 1 && !Convert.IsDBNull(idFamiliareObject))
                {
                    familiare.IdFamiliare = (int) idFamiliareObject;
                    res = true;
                }
                else
                {
                    res = false;
                }
            }

            return res;
        }

        public bool InsertDomanda(Domanda domanda, int? idDomandaTemporanea, int idUtente, bool forzaNumeroProtocollo)
        {
            bool res;

            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (idDomandaTemporanea.HasValue
                            && !DeleteDomandaTemporanea(idDomandaTemporanea.Value, transaction))
                            throw new Exception(
                                "Inserimento domanda, fallita la cancellazione della domanda temporanea");

                        using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeInsert")
                        )
                        {
                            // Mettiamo come anno protocollo e numero quelli memorizzati
                            if (forzaNumeroProtocollo)
                            {
                                if (domanda.ProtocolloPrestazione <= 0)
                                    throw new ArgumentException("Valorizzare il protocollo prestazione",
                                        "domanda.ProtocolloPrestazione");
                                if (domanda.NumeroProtocolloPrestazione <= 0)
                                    throw new ArgumentException("Valorizzare il numero protocollo prestazione",
                                        "domanda.NumeroProtocolloPrestazione");

                                DatabaseCemi.AddInParameter(comando, "@protocolloPrestazione", DbType.Int16,
                                    domanda.ProtocolloPrestazione);
                                DatabaseCemi.AddInParameter(comando, "@numeroProtocolloPrestazione", DbType.Int32,
                                    domanda.NumeroProtocolloPrestazione);
                            }

                            DatabaseCemi.AddInParameter(comando, "@guid", DbType.Guid,
                                domanda.Guid);

                            DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String,
                                domanda.TipoPrestazione.IdTipoPrestazione);
                            DatabaseCemi.AddInParameter(comando, "@beneficiario", DbType.String, domanda.Beneficiario);
                            DatabaseCemi.AddInParameter(comando, "@stato", DbType.String, domanda.Stato.IdStato);
                            DatabaseCemi.AddInParameter(comando, "@dataRiferimento", DbType.Date,
                                domanda.DataRiferimento);
                            DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32,
                                domanda.Lavoratore.IdLavoratore);

                            if (domanda.Lavoratore.Indirizzo != null)
                            {
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.IndirizzoVia))
                                    DatabaseCemi.AddInParameter(comando, "@lavoratoreIndirizzo", DbType.String,
                                        domanda.Lavoratore.Indirizzo.IndirizzoVia);
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Provincia))
                                    DatabaseCemi.AddInParameter(comando, "@lavoratoreProvincia", DbType.String,
                                        domanda.Lavoratore.Indirizzo.Provincia);
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Comune))
                                    DatabaseCemi.AddInParameter(comando, "@lavoratoreComuneCodiceCatastale",
                                        DbType.String,
                                        domanda.Lavoratore.Indirizzo.Comune);
                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Cap))
                                    DatabaseCemi.AddInParameter(comando, "@lavoratoreCap", DbType.String,
                                        domanda.Lavoratore.Indirizzo.Cap);

                                if (!string.IsNullOrEmpty(domanda.Lavoratore.Indirizzo.Frazione))
                                    DatabaseCemi.AddInParameter(comando, "@lavoratoreFrazione", DbType.String,
                                        domanda.Lavoratore.Indirizzo.Frazione);
                            }
                            //il metodo getidutente della gestione utenti tipicametne ritorna -1 se l'utente nno � loggato; quindi se maggiore di 0 memorizziamo
                            //l'idutente che ha inserito la domanda, altrimenti non passiamo il parametro che verr� inizializzato dalla SP
                            if (idUtente > 0)
                                DatabaseCemi.AddInParameter(comando, "@idUtenteInserimento", DbType.String, idUtente);

                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Cellulare) &&
                                CellulareHelper.NumeroCellulareValido(domanda.Lavoratore.Comunicazioni.Cellulare))
                            {
                                DatabaseCemi.AddInParameter(comando, "@lavoratoreCellulare", DbType.String,
                                    CellulareHelper.PulisciNumeroCellulare(
                                        domanda.Lavoratore.Comunicazioni.Cellulare));
                            }
                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Email))
                            {
                                DatabaseCemi.AddInParameter(comando, "@lavoratoreEmail", DbType.String,
                                    domanda.Lavoratore.Comunicazioni.Email);
                            }
                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.IdTipoPagamento))
                            {
                                DatabaseCemi.AddInParameter(comando, "@lavoratoreIdTipoPagamento", DbType.String,
                                    domanda.Lavoratore.Comunicazioni.IdTipoPagamento);
                            }
                            if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Iban))
                            {
                                DatabaseCemi.AddInParameter(comando, "@lavoratoreIBAN", DbType.String,
                                    domanda.Lavoratore.Comunicazioni.Iban);
                            }
                            if (domanda.Lavoratore.Comunicazioni.Appuntamento.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@lavoratoreAppuntamento", DbType.DateTime,
                                    domanda.Lavoratore.Comunicazioni.Appuntamento.Value);
                            }

                            if (domanda.Beneficiario != "L")
                            {
                                if (domanda.Familiare.IdFamiliare.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32,
                                        domanda.Familiare.IdFamiliare.Value);
                                DatabaseCemi.AddInParameter(comando, "@familiareCognome", DbType.String,
                                    domanda.Familiare.Cognome);
                                DatabaseCemi.AddInParameter(comando, "@familiareNome", DbType.String,
                                    domanda.Familiare.Nome);

                                DatabaseCemi.AddInParameter(comando, "@familiareDataNascita", DbType.DateTime,
                                    domanda.Familiare.DataNascita);
                                DatabaseCemi.AddInParameter(comando, "@familiareCodiceFiscale", DbType.String,
                                    domanda.Familiare.CodiceFiscale);
                            }

                            if (domanda.DatiAggiuntiviScolastiche != null)
                            {
                                if (domanda.DatiAggiuntiviScolastiche.TipoScuola != null)
                                    DatabaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32,
                                        domanda.DatiAggiuntiviScolastiche.TipoScuola.IdTipoScuola);

                                if (domanda.DatiAggiuntiviScolastiche.TipoPromozione != null)
                                    DatabaseCemi.AddInParameter(comando, "@idTipoPromozione", DbType.Int32,
                                        domanda.DatiAggiuntiviScolastiche.TipoPromozione.IdTipoPromozione);

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso))
                                    DatabaseCemi.AddInParameter(comando, "@idTipoPrestazioneDecesso", DbType.String,
                                        domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso);

                                if (domanda.DatiAggiuntiviScolastiche.DataDecesso.HasValue)
                                    DatabaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime,
                                        domanda.DatiAggiuntiviScolastiche.DataDecesso);

                                if (domanda.DatiAggiuntiviScolastiche.RedditoSi.HasValue)
                                {
                                    DatabaseCemi.AddInParameter(comando, "@redditoSi", DbType.Boolean,
                                        domanda.DatiAggiuntiviScolastiche.RedditoSi);
                                }

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.AnnoScolastico))
                                {
                                    DatabaseCemi.AddInParameter(comando, "@annoScolastico", DbType.String,
                                        domanda.DatiAggiuntiviScolastiche.AnnoScolastico);
                                }

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione))
                                {
                                    DatabaseCemi.AddInParameter(comando, "@istitutoScolDenominazione", DbType.String,
                                        domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione);
                                }

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo))
                                {
                                    DatabaseCemi.AddInParameter(comando, "@istitutoScolIndirizzo", DbType.String,
                                        domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo);
                                }

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoProvincia))
                                {
                                    DatabaseCemi.AddInParameter(comando, "@istitutoScolProvincia", DbType.String,
                                        domanda.DatiAggiuntiviScolastiche.IstitutoProvincia);
                                }

                                if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoLocalita))
                                {
                                    DatabaseCemi.AddInParameter(comando, "@istitutoScolLocalita", DbType.String,
                                        domanda.DatiAggiuntiviScolastiche.IstitutoLocalita);
                                }
                            }

                            DatabaseCemi.AddInParameter(comando, "@certificatoFamiglia", DbType.Boolean,
                                domanda.CertificatoFamiglia);
                            DatabaseCemi.AddInParameter(comando, "@moduloDentista", DbType.Boolean,
                                domanda.ModuloDentista);

                            DatabaseCemi.AddInParameter(comando, "@dataDomanda", DbType.DateTime,
                                domanda.DataDomanda);

                            DatabaseCemi.AddInParameter(comando, "@tipoInserimento", DbType.Int16,
                                (short) domanda.TipoInserimento);

                            DatabaseCemi.AddInParameter(comando, "@genitoriConviventi", DbType.Boolean,
                                domanda.GenitoriConviventi);

                            if (domanda.NumeroFatture.HasValue)
                            {
                                DatabaseCemi.AddInParameter(comando, "@numeroFatture", DbType.Int32,
                                    domanda.NumeroFatture);
                            }
                            else if (domanda.FattureDichiarate != null)
                            {
                                DatabaseCemi.AddInParameter(comando, "@numeroFatture", DbType.Int32,
                                    domanda.FattureDichiarate.Count);
                            }

                            if (domanda.TipoCausale != null &&
                                !string.IsNullOrEmpty(domanda.TipoCausale.IdTipoCausale))
                            {
                                DatabaseCemi.AddInParameter(comando, "@idTipoCausale", DbType.String,
                                    domanda.TipoCausale.IdTipoCausale);
                            }
                            DatabaseCemi.AddOutParameter(comando, "@idDomanda", DbType.Int32, 4);

                            if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 0)
                            {
                                object idDomandaObj = DatabaseCemi.GetParameterValue(comando, "@idDomanda");
                                if (idDomandaObj.GetType() == typeof(int) && (int) idDomandaObj == -1)
                                    throw new UnivocitaViolataException();
                                if (idDomandaObj.GetType() == typeof(int) && (int) idDomandaObj == -2)
                                    throw new DomandaGiaInseritaException();

                                throw new Exception("Inserimento della domanda fallito");
                            }
                            domanda.IdDomanda = (int) DatabaseCemi.GetParameterValue(comando, "@idDomanda");

                            if (domanda.IdDomanda > 0)
                            {
                                InserisciDocumentiConsegnati(transaction, domanda.IdDomanda.Value, domanda.Documenti);

                                foreach (FatturaDichiarata fattura in domanda.FattureDichiarate)
                                {
                                    if (!InsertFatturaDichiarata(fattura, domanda.IdDomanda.Value, transaction))
                                    {
                                        throw new Exception("Errore nell'inserimento di una Fattura Dichiarata");
                                    }
                                }

                                foreach (CassaEdile cassaEdile in domanda.CasseEdili)
                                {
                                    if (!InsertDomandaCassaEdile(cassaEdile, domanda.IdDomanda.Value, transaction))
                                    {
                                        throw new Exception(
                                            "Errore nell'inserimento di una Cassa Edile legata alla domanda");
                                    }
                                }
                            }
                            else
                            {
                                if (domanda.IdDomanda == -2)
                                {
                                    throw new DomandaGiaInseritaException();
                                }
                            }
                        }

                        res = true;
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return res;
        }

        private void InserisciDocumentiConsegnati(DbTransaction transaction, int idDomanda,
            DocumentoCollection documenti)
        {
            if (documenti != null)
            {
                //inseriamo la lista
                foreach (Documento documento in documenti)
                {
                    if (!InsertDocumentoRichiestoConsegnato(documento, idDomanda, transaction))
                    {
                        throw new Exception("Errore nell'inserimento di un documento consegnato");
                    }
                }
            }
        }

        /// <summary>
        ///     Inserisce una nuova lista di documenti conegnati (quella precedente se presente viene cancellata)
        /// </summary>
        /// <param name="idDomanda"></param>
        /// <param name="documenti"></param>
        public void InserisciDocumentiConsegnati(int idDomanda, DocumentoCollection documenti)
        {
            using (DbConnection connection = DatabaseCemi.CreateConnection())
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (documenti != null)
                        {
                            //cancelliamo la lista
                            DeleteDocumentoRichiestoConsegnato(idDomanda, transaction);

                            //inseriamo la nuova lista
                            InserisciDocumentiConsegnati(transaction, idDomanda, documenti);
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                    }
                }
            }
        }

        public bool InsertDomandaTemporanea(Domanda domanda)
        {
            bool res = false;

            StringBuilder domandaSerializzata = new StringBuilder();

            XmlSerializer serializer = new XmlSerializer(typeof(Domanda));

            using (TextWriter writer = new StringWriter(domandaSerializzata))
            {
                serializer.Serialize(writer, domanda);
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, domanda.Lavoratore.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@domanda", DbType.Xml, domandaSerializzata.ToString());

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool UpdateDomandaTemporanea(Domanda domanda)
        {
            bool res = false;

            StringBuilder domandaSerializzata = new StringBuilder();

            XmlSerializer serializer = new XmlSerializer(typeof(Domanda));

            using (TextWriter writer = new StringWriter(domandaSerializzata))
            {
                serializer.Serialize(writer, domanda);
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                DatabaseCemi.AddInParameter(comando, "@domanda", DbType.Xml, domandaSerializzata.ToString());

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertDomandaCassaEdileTemporanea(CassaEdile cassaEdile, int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeCasseEdiliInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, cassaEdile.IdCassaEdile);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertFatturaDichiarataTemporanea(FatturaDichiarata fattura, int idDomanda,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureTemporaneeDichiarateInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                DatabaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertDomandaCassaEdile(CassaEdile cassaEdile, int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeCasseEdiliInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, cassaEdile.IdCassaEdile);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    res = true;
            }

            return res;
        }

        private bool InsertFatturaDichiarata(FatturaDichiarata fattura, int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                DatabaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);
                DatabaseCemi.AddInParameter(comando, "@aggiuntaOperatore", DbType.Boolean, fattura.AggiuntaOperatore);
                if (fattura.IdUtenteInserimento.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idUtenteInserimento", DbType.Int32,
                        fattura.IdUtenteInserimento);
                }

                if (transaction != null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
            }

            return res;
        }

        /// <summary>
        ///     inserimento di una fattura dichiarata
        /// </summary>
        /// <param name="fattura"></param>
        /// <param name="idDomanda"></param>
        /// <returns></returns>
        public bool InsertFatturaDichiarata(FatturaDichiarata fattura, int idDomanda)
        {
            //passiamo null come dbtransaction perch� questa chiamata prevede l'inserimento ad hoc di una fattura dic. in contesto non transazionale
            return InsertFatturaDichiarata(fattura, idDomanda, null);
        }

        public DomandaCollection GetDomande(DomandaFilter filtro)
        {
            DomandaCollection domande = new DomandaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeSelectRicerca"))
            {
                if (filtro.IdLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                if (filtro.Stato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.String, filtro.Stato.Value);
                if (!string.IsNullOrEmpty(filtro.IdTipoPrestazione))
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, filtro.IdTipoPrestazione);
                }
                if (filtro.IdGruppo.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idGruppo", DbType.Int32, filtro.IdGruppo.Value);
                }
                if (filtro.Anno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, filtro.Anno.Value);
                if (filtro.Protocollo.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@protocollo", DbType.Int32, filtro.Protocollo.Value);
                if (filtro.IdUtente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, filtro.IdUtente.Value);
                if (!string.IsNullOrEmpty(filtro.CognomeLavoratore))
                    DatabaseCemi.AddInParameter(comando, "@cognomeLavoratore", DbType.String, filtro.CognomeLavoratore);
                if (!string.IsNullOrEmpty(filtro.NomeLavoratore))
                    DatabaseCemi.AddInParameter(comando, "@nomeLavoratore", DbType.String, filtro.NomeLavoratore);
                if (filtro.DataNascitaLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascitaLavoratore", DbType.DateTime,
                        filtro.DataNascitaLavoratore);
                if (filtro.TipologiaInserimento.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoInserimento", DbType.Int16, filtro.TipologiaInserimento);
                if (filtro.DataDomandaDal.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataDomandaDal", DbType.DateTime,
                        filtro.DataDomandaDal.Value);
                }
                if (filtro.DataDomandaAl.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataDomandaAl", DbType.DateTime, filtro.DataDomandaAl.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Domanda domanda = new Domanda();
                        domande.Add(domanda);

                        domanda.IdDomanda = (int) reader["idPrestazioniDomanda"];
                        domanda.TipoPrestazione = new TipoPrestazione();
                        domanda.TipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        domanda.ProtocolloPrestazione = (short) reader["protocolloPrestazione"];
                        domanda.NumeroProtocolloPrestazione = (int) reader["numeroProtocolloPrestazione"];
                        domanda.TipoPrestazione.Descrizione = (string) reader["tipoPrestazioneDescrizione"];
                        domanda.Stato = new StatoDomanda();
                        domanda.Stato.IdStato = (string) reader["stato"];
                        domanda.Stato.Descrizione = (string) reader["statoDescrizione"];
                        domanda.Beneficiario = (string) reader["beneficiario"];

                        //Gestione data domanda
                        if (!Convert.IsDBNull(reader["dataDomanda"]))
                        {
                            domanda.DataDomanda = (DateTime) reader["dataDomanda"];
                        }
                        else if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                        {
                            domanda.DataDomanda = (DateTime) reader["dataInserimentoRecord"];
                        }

                        if (!Convert.IsDBNull(reader["login"]))
                            domanda.LoginInCarico = (string) reader["login"];
                        if (!Convert.IsDBNull(reader["idUtente"]))
                            domanda.IdUtenteInCarico = (int) reader["idUtente"];
                        domanda.Lavoratore = new Lavoratore();
                        domanda.Lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                        domanda.Lavoratore.Cognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                        {
                            domanda.Lavoratore.Nome = (string) reader["nome"];
                        }
                        if (!Convert.IsDBNull(reader["dataNascita"]))
                            domanda.Lavoratore.DataNascita = (DateTime) reader["dataNascita"];
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            domanda.Lavoratore.CodiceFiscale = (string) reader["codiceFiscale"];

                        if (!Convert.IsDBNull(reader["idFamiliare"]))
                        {
                            domanda.Familiare = new Familiare();
                            domanda.Familiare.IdFamiliare = (int) reader["idFamiliare"];
                            domanda.Familiare.Cognome = (string) reader["familiareAnagraficaCognome"];
                            domanda.Familiare.Nome = (string) reader["familiareAnagraficaNome"];
                        }

                        if (!Convert.IsDBNull(reader["familiareDomandaCognome"]))
                        {
                            domanda.FamiliareFornito = new Familiare();
                            domanda.FamiliareFornito.Cognome = (string) reader["familiareDomandaCognome"];
                            domanda.FamiliareFornito.Nome = (string) reader["familiareDomandaNome"];
                        }
                        if (!Convert.IsDBNull(reader["tipoInserimento"]))
                            domanda.TipoInserimento = (TipoInserimento) (short) reader["tipoInserimento"];
                    }
                }
            }

            return domande;
        }

        public DomandaCollection GetDomandeQuorum(DomandaFilter filtro)
        {
            DomandaCollection domande = new DomandaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeSoggiornoEstivoSelect"))
            {
                if (filtro.IdLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                if (filtro.Stato.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@stato", DbType.String, filtro.Stato.Value);
                if (!string.IsNullOrEmpty(filtro.IdTipoPrestazione))
                {
                    DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, filtro.IdTipoPrestazione);
                }
                if (filtro.IdGruppo.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idGruppo", DbType.Int32, filtro.IdGruppo.Value);
                }
                if (filtro.Anno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@anno", DbType.Int32, filtro.Anno.Value);
                if (filtro.Protocollo.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@protocollo", DbType.Int32, filtro.Protocollo.Value);
                if (filtro.IdUtente.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, filtro.IdUtente.Value);
                if (!string.IsNullOrEmpty(filtro.CognomeLavoratore))
                    DatabaseCemi.AddInParameter(comando, "@cognomeLavoratore", DbType.String, filtro.CognomeLavoratore);
                if (!string.IsNullOrEmpty(filtro.NomeLavoratore))
                    DatabaseCemi.AddInParameter(comando, "@nomeLavoratore", DbType.String, filtro.NomeLavoratore);
                if (filtro.DataNascitaLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataNascitaLavoratore", DbType.DateTime,
                        filtro.DataNascitaLavoratore);
                if (filtro.TipologiaInserimento.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoInserimento", DbType.Int16, filtro.TipologiaInserimento);
                if (filtro.DataDomandaDal.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataDomandaDal", DbType.DateTime,
                        filtro.DataDomandaDal.Value);
                }
                if (filtro.DataDomandaAl.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataDomandaAl", DbType.DateTime, filtro.DataDomandaAl.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Domanda domanda = new Domanda();
                        domande.Add(domanda);

                        domanda.IdDomanda = (int)reader["idPrestazioniDomanda"];
                        domanda.TipoPrestazione = new TipoPrestazione();
                        domanda.TipoPrestazione.IdTipoPrestazione = (string)reader["idTipoPrestazione"];
                        domanda.ProtocolloPrestazione = (short)reader["protocolloPrestazione"];
                        domanda.NumeroProtocolloPrestazione = (int)reader["numeroProtocolloPrestazione"];
                        domanda.TipoPrestazione.Descrizione = (string)reader["tipoPrestazioneDescrizione"];
                        domanda.Stato = new StatoDomanda();
                        domanda.Stato.IdStato = (string)reader["stato"];
                        domanda.Stato.Descrizione = (string)reader["statoDescrizione"];
                        domanda.Beneficiario = (string)reader["beneficiario"];

                        //Gestione data domanda
                        if (!Convert.IsDBNull(reader["dataDomanda"]))
                        {
                            domanda.DataDomanda = (DateTime)reader["dataDomanda"];
                        }
                        else if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                        {
                            domanda.DataDomanda = (DateTime)reader["dataInserimentoRecord"];
                        }

                        if (!Convert.IsDBNull(reader["login"]))
                            domanda.LoginInCarico = (string)reader["login"];
                        if (!Convert.IsDBNull(reader["idUtente"]))
                            domanda.IdUtenteInCarico = (int)reader["idUtente"];
                        domanda.Lavoratore = new Lavoratore();
                        domanda.Lavoratore.IdLavoratore = (int)reader["idLavoratore"];
                        domanda.Lavoratore.Cognome = (string)reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                        {
                            domanda.Lavoratore.Nome = (string)reader["nome"];
                        }
                        if (!Convert.IsDBNull(reader["dataNascita"]))
                            domanda.Lavoratore.DataNascita = (DateTime)reader["dataNascita"];
                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            domanda.Lavoratore.CodiceFiscale = (string)reader["codiceFiscale"];

                        if (!Convert.IsDBNull(reader["idFamiliare"]))
                        {
                            domanda.Familiare = new Familiare();
                            domanda.Familiare.IdFamiliare = (int)reader["idFamiliare"];
                            domanda.Familiare.Cognome = (string)reader["familiareAnagraficaCognome"];
                            domanda.Familiare.Nome = (string)reader["familiareAnagraficaNome"];
                        }

                        if (!Convert.IsDBNull(reader["familiareDomandaCognome"]))
                        {
                            domanda.FamiliareFornito = new Familiare();
                            domanda.FamiliareFornito.Cognome = (string)reader["familiareDomandaCognome"];
                            domanda.FamiliareFornito.Nome = (string)reader["familiareDomandaNome"];
                        }
                        if (!Convert.IsDBNull(reader["tipoInserimento"]))
                            domanda.TipoInserimento = (TipoInserimento)(short)reader["tipoInserimento"];
                    }
                }
            }

            return domande;
        }

        public List<Quorum> GetQuorum(DomandaFilter filtro)
        {
            List<Quorum> quorum = new List<Quorum>();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeQuorumSelectRicerca"))
            {
                if (filtro.IdLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);

                if (filtro.Anno.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@annoDomanda", DbType.Int32, filtro.Anno.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Quorum q = new Quorum();
                        quorum.Add(q);

                        q.IdLavoratore = (int)reader["idLavoratore"];
                        q.IdFamiliare = (int)reader["idFamiliare"];
                        q.Beneficiario = (string)reader["beneficiario"];
                        q.IdTipoPrestazione = (string)reader["idTipoPrestazione"];
                        q.Importo = (decimal)reader["importo"];
                        q.Semaforo = (bool)reader["semaforo"];
                    }
                }

            }

            return quorum;
        }

        public DomandaCollection GetDomandeTemporanee(DomandaFilter filtro)
        {
            DomandaCollection domande = new DomandaCollection();            

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeSelectRicerca"))
            {
                if (filtro.IdLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Domanda domanda = null;

                        TextReader textReader = new StringReader((string) reader["domanda"]);
                        XmlSerializer serializer = new XmlSerializer(typeof(Domanda));
                        domanda = (Domanda) serializer.Deserialize(textReader);

                        domanda.IdDomanda = (int) reader["idPrestazioniDomanda"];

                        domande.Add(domanda);
                    }
                }
            }

            return domande;
        }

        public Domanda GetDomanda(int idDomanda)
        {
            Domanda domanda;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    domanda = new Domanda();
                    domanda.IdDomanda = (int) reader["idPrestazioniDomanda"];
                    domanda.TipoPrestazione = new TipoPrestazione();
                    domanda.TipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                    domanda.TipoPrestazione.Descrizione = (string) reader["tipoPrestazioneDescrizione"];
                    domanda.Beneficiario = (string) reader["beneficiario"];

                    //domanda.DataConferma = (DateTime) reader["dataInserimentoRecord"];

                    domanda.DataRiferimento = (DateTime) reader["dataRiferimento"];
                    domanda.Stato = new StatoDomanda();
                    domanda.Stato.IdStato = (string) reader["stato"];
                    domanda.Stato.Descrizione = (string) reader["statoDescrizione"];
                    if (!Convert.IsDBNull(reader["idUtente"]))
                        domanda.IdUtenteInCarico = (int) reader["idUtente"];
                    if (!Convert.IsDBNull(reader["login"]))
                        domanda.LoginInCarico = (string) reader["login"];
                    domanda.ProtocolloPrestazione = (short) reader["protocolloPrestazione"];
                    domanda.NumeroProtocolloPrestazione = (int) reader["numeroProtocolloPrestazione"];
                    domanda.GenitoriConviventi = (bool) reader["genitoriConviventi"];

                    if (!Convert.IsDBNull(reader["nota"]))
                        domanda.Nota = (string) reader["nota"];

                    if (!Convert.IsDBNull(reader["tipoInserimento"]))
                        domanda.TipoInserimento = (TipoInserimento) (short) reader["tipoInserimento"];
                    else
                    {
                        //Compatibilit� con il passato
                        domanda.TipoInserimento = TipoInserimento.Normale;
                    }

                    if (!Convert.IsDBNull(reader["numeroFatture"]))
                        domanda.NumeroFatture = (int) reader["numeroFatture"];

                    if (!Convert.IsDBNull(reader["idTipoModulo"]))
                    {
                        domanda.TipoModulo = new TipoModulo();
                        domanda.TipoModulo.IdTipoModulo = (short) reader["idTipoModulo"];
                        domanda.TipoModulo.Descrizione = (string) reader["tipoModuloDescrizione"];
                        domanda.TipoModulo.Modulo = (string) reader["tipoModuloModulo"];
                    }

                    if (!Convert.IsDBNull(reader["idTipoCausale"]))
                    {
                        TipoCausale tc = new TipoCausale((string) reader["idTipoCausale"]);
                        if (!Convert.IsDBNull(reader["descrizioneTipoCausale"]))
                            tc.Descrizione = (string) reader["descrizioneTipoCausale"];
                        domanda.TipoCausale = tc;
                    }

                    //Gestione data domanda
                    if (!Convert.IsDBNull(reader["dataDomanda"]))
                    {
                        domanda.DataDomanda = (DateTime) reader["dataDomanda"];
                    }
                    else if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                    {
                        domanda.DataDomanda = (DateTime) reader["dataInserimentoRecord"];
                    }

                    #region Dati aggiuntivi della domanda

                    if (!Convert.IsDBNull(reader["idPrestazioniDomandeDatiAggiuntivi"]))
                    {
                        domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();
                        domanda.DatiAggiuntiviScolastiche.IdDatiScolastiche =
                            (int) reader["idPrestazioniDomandeDatiAggiuntivi"];

                        if (!Convert.IsDBNull(reader["idTipoScuola"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.TipoScuola = new TipoScuola();
                            domanda.DatiAggiuntiviScolastiche.TipoScuola.IdTipoScuola = (int) reader["idTipoScuola"];
                            domanda.DatiAggiuntiviScolastiche.TipoScuola.Descrizione =
                                (string) reader["tipoScuolaDescrizione"];
                        }

                        if (!Convert.IsDBNull(reader["login"]))
                            domanda.LoginInCarico = (string) reader["login"];

                        if (!Convert.IsDBNull(reader["idTipoPromozione"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.TipoPromozione = new TipoPromozione();
                            domanda.DatiAggiuntiviScolastiche.TipoPromozione.IdTipoPromozione =
                                (int) reader["idTipoPromozione"];
                            domanda.DatiAggiuntiviScolastiche.TipoPromozione.Descrizione =
                                (string) reader["tipoPromozioneDescrizione"];
                        }

                        if (!Convert.IsDBNull(reader["annoFrequenza"]))
                            domanda.DatiAggiuntiviScolastiche.AnnoFrequenza = (short) reader["annoFrequenza"];

                        if (!Convert.IsDBNull(reader["classeConclusa"]))
                            domanda.DatiAggiuntiviScolastiche.ClasseConclusa = (short) reader["classeConclusa"];

                        if (!Convert.IsDBNull(reader["numeroMesiFrequentati"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati =
                                (short) reader["numeroMesiFrequentati"];

                        if (!Convert.IsDBNull(reader["numeroMesiFrequentati"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati =
                                (short) reader["numeroMesiFrequentati"];

                        if (!Convert.IsDBNull(reader["annoImmatricolazione"]))
                            domanda.DatiAggiuntiviScolastiche.AnnoImmatricolazione =
                                (short) reader["annoImmatricolazione"];

                        if (!Convert.IsDBNull(reader["numeroAnniFrequentati"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroAnniFrequentati =
                                (short) reader["numeroAnniFrequentati"];

                        if (!Convert.IsDBNull(reader["numeroAnniFuoriCorso"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroAnniFuoriCorso =
                                (short) reader["numeroAnniFuoriCorso"];

                        if (!Convert.IsDBNull(reader["mediaVoti"]))
                            domanda.DatiAggiuntiviScolastiche.MediaVoti = (decimal) reader["mediaVoti"];

                        if (!Convert.IsDBNull(reader["numeroEsamiSostenuti"]))
                            domanda.DatiAggiuntiviScolastiche.NumeroEsamiSostenuti =
                                (short) reader["numeroEsamiSostenuti"];

                        if (!Convert.IsDBNull(reader["cfuConseguiti"]))
                            domanda.DatiAggiuntiviScolastiche.CfuConseguiti = (short) reader["cfuConseguiti"];

                        if (!Convert.IsDBNull(reader["cfuPrevisti"]))
                            domanda.DatiAggiuntiviScolastiche.CfuPrevisti = (short) reader["cfuPrevisti"];

                        if (!Convert.IsDBNull(reader["applicaDeduzione"]))
                            domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione = (string) reader["applicaDeduzione"];

                        //funerario
                        //dataDecesso � stata rinominata perch� non era corretto
                        if (!Convert.IsDBNull(reader["dataDecessoFunerario"]))
                            domanda.DatiAggiuntiviScolastiche.DataDecesso = (DateTime) reader["dataDecessoFunerario"];
                        if (!Convert.IsDBNull(reader["idFamiliareErede"]))
                            domanda.DatiAggiuntiviScolastiche.IdFamiliareErede = (int) reader["idFamiliareErede"];
                        if (!Convert.IsDBNull(reader["idTipoPrestazioneDecesso"]))
                            domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso =
                                (string) reader["idTipoPrestazioneDecesso"];

                        //handicap
                        if (!Convert.IsDBNull(reader["annoDaErogare"]))
                            domanda.DatiAggiuntiviScolastiche.AnnoDaErogare = (int) reader["annoDaErogare"];

                        if (!Convert.IsDBNull(reader["tipoDaErogare"]))
                            domanda.DatiAggiuntiviScolastiche.TipoDaErogare = (int) reader["tipoDaErogare"];

                        // asilo nido
                        if (!Convert.IsDBNull(reader["periodoFrequenzaDal"]))
                            domanda.DatiAggiuntiviScolastiche.PeriodoDal = (DateTime) reader["periodoFrequenzaDal"];

                        if (!Convert.IsDBNull(reader["periodoFrequenzaAl"]))
                            domanda.DatiAggiuntiviScolastiche.PeriodoAl = (DateTime) reader["periodoFrequenzaAl"];

                        if (!Convert.IsDBNull(reader["forzaturaScolastichePeriodoFrequenza180Giorni"]))
                            domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza180Giorni =
                                (bool) reader["forzaturaScolastichePeriodoFrequenza180Giorni"];

                        if (!Convert.IsDBNull(reader["forzaturaScolastichePeriodoFrequenza3Anni"]))
                            domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza3Anni =
                                (bool) reader["forzaturaScolastichePeriodoFrequenza3Anni"];

                        if (!Convert.IsDBNull(reader["forzaturaScolasticheAnnoFrequenza"]))
                            domanda.DatiAggiuntiviScolastiche.ForzaturaAnnoFrequenza =
                                (bool) reader["forzaturaScolasticheAnnoFrequenza"];

                        if (!Convert.IsDBNull(reader["redditoSi"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.RedditoSi = (bool) reader["redditoSi"];
                        }

                        if (!Convert.IsDBNull(reader["annoScolastico"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.AnnoScolastico = (string) reader["annoScolastico"];
                        }

                        if (!Convert.IsDBNull(reader["denominazioneIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione =
                                (string) reader["denominazioneIstituto"];
                        }

                        if (!Convert.IsDBNull(reader["indirizzoIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo = (string) reader["indirizzoIstituto"];
                        }

                        if (!Convert.IsDBNull(reader["localitaIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoLocalita = (string) reader["localitaIstituto"];
                        }

                        if (!Convert.IsDBNull(reader["provinciaIstituto"]))
                        {
                            domanda.DatiAggiuntiviScolastiche.IstitutoProvincia = (string) reader["provinciaIstituto"];
                        }
                    }

                    #endregion

                    #region Dati del lavoratore

                    // Lavoratore
                    domanda.Lavoratore = new Lavoratore();
                    domanda.Lavoratore.IdLavoratore = (int) reader["idLavoratore"];
                    domanda.Lavoratore.Cognome = (string) reader["lavoratoreCognome"];
                    domanda.Lavoratore.Nome = (string) reader["lavoratoreNome"];
                    if (!Convert.IsDBNull(reader["lavoratoreDataNascita"]))
                        domanda.Lavoratore.DataNascita = (DateTime) reader["lavoratoreDataNascita"];
                    if (!Convert.IsDBNull(reader["lavoratoreCodiceFiscale"]))
                        domanda.Lavoratore.CodiceFiscale = (string) reader["lavoratoreCodiceFiscale"];
                    domanda.Lavoratore.Indirizzo = new Indirizzo();
                    if (!Convert.IsDBNull(reader["lavoratoreIndirizzo"]))
                        domanda.Lavoratore.Indirizzo.IndirizzoVia = (string) reader["lavoratoreIndirizzo"];
                    if (!Convert.IsDBNull(reader["lavoratoreProvincia"]))
                        domanda.Lavoratore.Indirizzo.Provincia = (string) reader["lavoratoreProvincia"];
                    if (!Convert.IsDBNull(reader["lavoratoreComune"]))
                        domanda.Lavoratore.Indirizzo.Comune = (string) reader["lavoratoreComune"];
                    if (!Convert.IsDBNull(reader["lavoratoreCap"]))
                        domanda.Lavoratore.Indirizzo.Cap = (string) reader["lavoratoreCap"];
                    domanda.Lavoratore.Comunicazioni = new Comunicazioni();
                    if (!Convert.IsDBNull(reader["lavoratoreCellulare"]))
                        domanda.Lavoratore.Comunicazioni.Cellulare = (string) reader["lavoratoreCellulare"];
                    if (!Convert.IsDBNull(reader["lavoratoreEmail"]))
                        domanda.Lavoratore.Comunicazioni.Email = (string) reader["lavoratoreEmail"];
                    if (!Convert.IsDBNull(reader["lavoratoreIdTipoPagamento"]))
                        domanda.Lavoratore.Comunicazioni.IdTipoPagamento = (string) reader["lavoratoreIdTipoPagamento"];

                    #endregion

                    // Indirizzo fornito
                    domanda.Lavoratore.IndirizzoFornito = new Indirizzo();
                    if (!Convert.IsDBNull(reader["domandaLavoratoreIndirizzo"]))
                        domanda.Lavoratore.IndirizzoFornito.IndirizzoVia =
                            (string) reader["domandaLavoratoreIndirizzo"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreProvincia"]))
                        domanda.Lavoratore.IndirizzoFornito.Provincia = (string) reader["domandaLavoratoreProvincia"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreComune"]))
                        domanda.Lavoratore.IndirizzoFornito.Comune = (string) reader["domandaLavoratoreComune"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreFrazione"]))
                        domanda.Lavoratore.IndirizzoFornito.Frazione = (string) reader["domandaLavoratoreFrazione"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreCap"]))
                        domanda.Lavoratore.IndirizzoFornito.Cap = (string) reader["domandaLavoratoreCap"];

                    // Comunicazioni fornite
                    domanda.Lavoratore.ComunicazioniFornite = new Comunicazioni();
                    if (!Convert.IsDBNull(reader["domandaLavoratoreCellulare"]))
                        domanda.Lavoratore.ComunicazioniFornite.Cellulare =
                            (string) reader["domandaLavoratoreCellulare"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreEmail"]))
                        domanda.Lavoratore.ComunicazioniFornite.Email = (string) reader["domandaLavoratoreEmail"];
                    if (!Convert.IsDBNull(reader["domandaLavoratoreIdTipoPagamento"]))
                        domanda.Lavoratore.ComunicazioniFornite.IdTipoPagamento =
                            (string) reader["domandaLavoratoreIdTipoPagamento"];

                    #region Dati familiare

                    // Familiare
                    if (!Convert.IsDBNull(reader["idFamiliare"]))
                    {
                        domanda.Familiare = new Familiare();
                        domanda.Familiare.IdLavoratore = (int) domanda.Lavoratore.IdLavoratore;
                        domanda.Familiare.IdFamiliare = (int) reader["idFamiliare"];
                        domanda.Familiare.Cognome = (string) reader["familiareCognome"];
                        domanda.Familiare.Nome = (string) reader["familiareNome"];
                        if (!Convert.IsDBNull(reader["familiareDataNascita"]))
                            domanda.Familiare.DataNascita = (DateTime) reader["familiareDataNascita"];
                        if (!Convert.IsDBNull(reader["familiareDataDecesso"]))
                            domanda.Familiare.DataDecesso = (DateTime) reader["familiareDataDecesso"];
                        if (!Convert.IsDBNull(reader["familiareCodiceFiscale"]))
                            domanda.Familiare.CodiceFiscale = (string) reader["familiareCodiceFiscale"];
                        if (!Convert.IsDBNull(reader["familiareGradoParentela"]))
                            domanda.Familiare.GradoParentela = (string) reader["familiareGradoParentela"];
                        if (!Convert.IsDBNull(reader["domandaFamiliareACarico"]))
                            domanda.Familiare.ACarico = (string) reader["domandaFamiliareACarico"];
                    }

                    // Familiare fornito
                    if (!Convert.IsDBNull(reader["domandaFamiliareCognome"]))
                    {
                        domanda.FamiliareFornito = new Familiare();
                        domanda.FamiliareFornito.Cognome = (string) reader["domandaFamiliareCognome"];
                        domanda.FamiliareFornito.Nome = (string) reader["domandaFamiliareNome"];
                        if (!Convert.IsDBNull(reader["domandaFamiliareDataNascita"]))
                            domanda.FamiliareFornito.DataNascita = (DateTime) reader["domandaFamiliareDataNascita"];
                        if (!Convert.IsDBNull(reader["domandaFamiliareCodiceFiscale"]))
                            domanda.FamiliareFornito.CodiceFiscale = (string) reader["domandaFamiliareCodiceFiscale"];
                    }

                    #endregion

                    if (!Convert.IsDBNull(reader["controlloDatiAnagraficiFamiliare"]))
                        domanda.ControlloFamiliare = (bool) reader["controlloDatiAnagraficiFamiliare"];
                    if (!Convert.IsDBNull(reader["controlloPresenzaDocumenti"]))
                        domanda.ControlloPresenzaDocumenti = (bool) reader["controlloPresenzaDocumenti"];
                    if (!Convert.IsDBNull(reader["controlloFatture"]))
                        domanda.ControlloFatture = (bool) reader["controlloFatture"];
                    if (!Convert.IsDBNull(reader["controlloUnivocitaPrestazione"]))
                        domanda.ControlloUnivocitaPrestazione = (bool) reader["controlloUnivocitaPrestazione"];
                    if (!Convert.IsDBNull(reader["controlloDatiAnagraficiLavoratore"]))
                        domanda.ControlloLavoratore = (bool) reader["controlloDatiAnagraficiLavoratore"];
                    if (!Convert.IsDBNull(reader["controlloOreCNCE"]))
                        domanda.ControlloOreCnce = (bool) reader["controlloOreCNCE"];
                    if (!Convert.IsDBNull(reader["controlloDatiScolastiche"]))
                        domanda.ControlloScolastiche = (bool) reader["controlloDatiScolastiche"];

                    #region Fatture

                    // Fatture fornite
                    reader.NextResult();
                    domanda.FattureDichiarate = new FatturaDichiarataCollection();
                    while (reader.Read())
                    {
                        FatturaDichiarata fattura = new FatturaDichiarata();

                        //fattura.IdPrestazioniFatturaDichiarata = (int)reader["idPrestazioniFatturaDichiarata"];
                        //fattura.Data = (DateTime)reader["data"];
                        //fattura.Numero = (string)reader["numero"];
                        //fattura.ImportoTotale = (decimal)reader["importoTotale"];
                        //fattura.Saldo = (bool)reader["saldo"];


                        fattura.IdPrestazioniFatturaDichiarata = (int) reader["idPrestazioniFatturaDichiarata"];
                        //fattura.IdPrestazioniDomanda = (int)reader["idPrestazioniDomanda"];
                        if (!Convert.IsDBNull(reader["idPrestazioniFattura"]))
                            fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            fattura.IdFatturaArchidoc = (string) reader["idArchidoc"];

                        fattura.Data = (DateTime) reader["data"];
                        fattura.Numero = (string) reader["numero"];
                        fattura.ImportoTotale = (decimal) reader["importoTotale"];
                        fattura.Saldo = (bool) reader["saldo"];
                        fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                        if (!Convert.IsDBNull(reader["idUtenteInserimento"]))
                            fattura.IdUtenteInserimento = (int) reader["idUtenteInserimento"];
                        if (!Convert.IsDBNull(reader["utenteInserimento"]))
                            fattura.UtenteInserimento = (string) reader["utenteInserimento"];

                        if (!Convert.IsDBNull(reader["dataAnnullamento"]))
                            fattura.DataAnnullamento = (DateTime) reader["dataAnnullamento"];
                        //fattura.DataInserimentoRecord = (DateTime)reader["dataInserimentoRecord"];
                        domanda.FattureDichiarate.Add(fattura);
                    }

                    #endregion

                    // Casse Edili
                    reader.NextResult();
                    domanda.CasseEdili = new CassaEdileCollection();
                    while (reader.Read())
                    {
                        CassaEdile cassaEdile = new CassaEdile();
                        domanda.CasseEdili.Add(cassaEdile);

                        cassaEdile.IdCassaEdile = (string) reader["idCassaEdile"];
                        cassaEdile.Descrizione = (string) reader["descrizione"];
                        cassaEdile.InseritaManualmente = (bool) reader["inserimentoManuale"];
                        cassaEdile.Cnce = !Convert.IsDBNull(reader["cnceIdCassaEdile"]);
                    }

                    // Importi Prestazione
                    reader.NextResult();
                    domanda.Importi = new PrestazioneImportoCollection();
                    while (reader.Read())
                    {
                        PrestazioneImporto importo = new PrestazioneImporto();
                        domanda.Importi.Add(importo);

                        importo.IdDomanda = idDomanda;
                        if (!Convert.IsDBNull(reader["idPrestazioniImportoPrestazione"]))
                            importo.IdImporto = (int) reader["idPrestazioniImportoPrestazione"];
                        importo.IdTipoImporto = (int) reader["idPrestazioniTipoImportoPrestazione"];
                        importo.Descrizione = (string) reader["descrizione"];
                        if (!Convert.IsDBNull(reader["valore"]))
                            importo.Valore = (decimal) reader["valore"];
                    }
                }
            }

            return domanda;
        }

        public Domanda GetDomandaTemporanea(int idDomandaTemporanea)
        {
            Domanda domanda = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomandaTemporanea);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        TextReader textReader = new StringReader((string) reader["domanda"]);
                        XmlSerializer serializer = new XmlSerializer(typeof(Domanda));
                        domanda = (Domanda) serializer.Deserialize(textReader);
                    }
                }
            }

            return domanda;
        }

        /// <summary>
        ///     Ritorna una collection di documenti a partire da un oggetto filtro
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public DocumentoCollection GetDocumenti(DocumentoFilter filtro)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DocumentiLavoratoreSelect"))
            {
                if (filtro.IdLavoratore.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore.Value);
                if (filtro.IdTipoDocumento.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.String, filtro.IdTipoDocumento);
                if (!string.IsNullOrEmpty(filtro.Cognome))
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);

                if (filtro.DataScansioneDa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataScansioneDa", DbType.DateTime, filtro.DataScansioneDa);

                if (filtro.DataScansioneA.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataScansioneA", DbType.DateTime, filtro.DataScansioneA);
                if (!string.IsNullOrEmpty(filtro.IdTipoPrestazione))
                    DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, filtro.IdTipoPrestazione);
                if (filtro.ProtocolloPrestazione.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@protocolloPrestazione", DbType.Int32,
                        filtro.ProtocolloPrestazione);
                if (filtro.NumeroProtocolloPrestazione.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroProtocolloPrestazione", DbType.Int32,
                        filtro.NumeroProtocolloPrestazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();
                        documenti.Add(documento);

                        if (!Convert.IsDBNull(reader["codiceFiscale"]))
                            documento.CodiceFiscale = (string) reader["codiceFiscale"];
                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["dataNascita"]))
                            documento.DataNascita = (DateTime) reader["dataNascita"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            documento.DataNascita = (DateTime) reader["dataScansione"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idFamiliare"]))
                            documento.IdFamiliare = (int) reader["idFamiliare"];
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                            documento.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["idPrestazioniDomanda"]))
                            documento.IdPrestazioniDomanda = (int) reader["idPrestazioniDomanda"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];

                        if (!Convert.IsDBNull(reader["idTipoPrestazione"]))
                            documento.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        if (!Convert.IsDBNull(reader["numeroProtocolloPrestazione"]))
                            documento.NumeroProtocolloPrestazione = (int) reader["numeroProtocolloPrestazione"];
                        if (!Convert.IsDBNull(reader["protocolloPrestazione"]))
                            documento.ProtocolloPrestazione = (int) reader["protocolloPrestazione"];

                        documento.IdDocumento = (int) reader["idDocumento"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];
                    }
                }
            }

            return documenti;
        }

        public Documento GetDocumento(short idTipoDocumento, int idDocumento)
        {
            Documento documento;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_DocumentiLavoratoreSelectByKey"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16, idTipoDocumento);
                DatabaseCemi.AddInParameter(comando, "@idDocumento", DbType.Int32, idDocumento);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();
                    documento = new Documento();

                    if (!Convert.IsDBNull(reader["codiceFiscale"]))
                        documento.CodiceFiscale = (string) reader["codiceFiscale"];
                    if (!Convert.IsDBNull(reader["cognome"]))
                        documento.LavoratoreCognome = (string) reader["cognome"];
                    if (!Convert.IsDBNull(reader["nome"]))
                        documento.LavoratoreNome = (string) reader["nome"];
                    if (!Convert.IsDBNull(reader["dataNascita"]))
                        documento.DataNascita = (DateTime) reader["dataNascita"];
                    if (!Convert.IsDBNull(reader["dataScansione"]))
                        documento.DataNascita = (DateTime) reader["dataScansione"];
                    if (!Convert.IsDBNull(reader["idArchidoc"]))
                        documento.IdArchidoc = (string) reader["idArchidoc"];
                    if (!Convert.IsDBNull(reader["idFamiliare"]))
                        documento.IdFamiliare = (int) reader["idFamiliare"];
                    if (!Convert.IsDBNull(reader["idLavoratore"]))
                        documento.IdLavoratore = (int) reader["idLavoratore"];
                    if (!Convert.IsDBNull(reader["idPrestazioniDomanda"]))
                        documento.IdPrestazioniDomanda = (int) reader["idPrestazioniDomanda"];
                    if (!Convert.IsDBNull(reader["originale"]))
                        documento.Originale = (bool) reader["originale"];

                    documento.IdDocumento = (int) reader["idDocumento"];
                    documento.TipoDocumento = new TipoDocumento();
                    documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                    documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];
                }
            }

            return documento;
        }

        public DocumentoCollection GetDocumentiNecessari(int idDomanda)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumenti"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();
                        documenti.Add(documento);

                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idDocumento"]))
                            documento.IdDocumento = (int) reader["idDocumento"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];

                        documento.PerPrestazione = (bool) reader["univocoPrestazione"];
                        documento.RiferitoA = (string) reader["documentoRiferitoA"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];
                    }
                }
            }

            return documenti;
        }

        public DocumentoCollection GetDocumentiNecessariPiuRecenti(int idDomanda)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumenti"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();

                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idDocumento"]))
                            documento.IdDocumento = (int) reader["idDocumento"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            documento.DataScansione = (DateTime) reader["dataScansione"];

                        documento.PerPrestazione = (bool) reader["univocoPrestazione"];
                        documento.RiferitoA = (string) reader["documentoRiferitoA"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];

                        if (!documenti.DocumentoGiaPresente(documento))
                        {
                            documenti.Add(documento);
                        }
                    }
                }
            }

            return documenti;
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare)
        {
            return GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, false, null);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare, int? idPrestazioniDomanda)
        {
            return GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, false,
                idPrestazioniDomanda);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare, bool operatore)
        {
            return GetDocumentiNecessari(idTipoPrestazione, beneficiario, idLavoratore, idFamiliare, operatore);
        }

        public DocumentoCollection GetDocumentiNecessari(string idTipoPrestazione, string beneficiario,
            int idLavoratore,
            int? idFamiliare, bool operatore, int? idPrestazioniDomanda)
        {
            DocumentoCollection documenti = new DocumentoCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumentiGenerico"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);
                DatabaseCemi.AddInParameter(comando, "@beneficiario", DbType.String, beneficiario);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                if (idFamiliare.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare.Value);
                DatabaseCemi.AddInParameter(comando, "@operatore", DbType.Boolean, operatore);
                if (idPrestazioniDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32,
                        idPrestazioniDomanda.Value);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        Documento documento = new Documento();
                        documenti.Add(documento);

                        if (!Convert.IsDBNull(reader["cognome"]))
                            documento.LavoratoreCognome = (string) reader["cognome"];
                        if (!Convert.IsDBNull(reader["nome"]))
                            documento.LavoratoreNome = (string) reader["nome"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            documento.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idDocumento"]))
                            documento.IdDocumento = (int) reader["idDocumento"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            documento.DataScansione = (DateTime) reader["dataScansione"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            documento.Originale = (bool) reader["originale"];

                        documento.PerPrestazione = (bool) reader["univocoPrestazione"];
                        documento.RiferitoA = (string) reader["documentoRiferitoA"];
                        documento.TipoDocumento = new TipoDocumento();
                        documento.TipoDocumento.IdTipoDocumento = (short) reader["idTipoDocumento"];
                        documento.TipoDocumento.Descrizione = (string) reader["descrizioneDocumentoRichiesto"];

                        if (!Convert.IsDBNull(reader["consegnato"]))
                            documento.Consegnato = (bool) reader["consegnato"];
                    }
                }
            }

            return documenti;
        }

        public DocumentoCopertinaCollection GetDocumentiStampaCopertine(int idDomanda)
        {
            DocumentoCopertinaCollection documenti = new DocumentoCopertinaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeElencoDocumentiCopertine"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici

                    int indiceIdDomanda = reader.GetOrdinal("idPrestazioniDomanda");
                    int indiceIdTipoDocumento = reader.GetOrdinal("idTipoDocumento");
                    int indiceIdTipoDocumentoArchidoc = reader.GetOrdinal("idTipoDocumentoArchidoc");
                    int indiceDescrizioneDocumento = reader.GetOrdinal("documento");
                    int indiceDescrizioneBeneficiario = reader.GetOrdinal("descrizioneBeneficiario");
                    int indiceIdFatturaDichiarata = reader.GetOrdinal("idPrestazioniFatturaDichiarata");
                    int indiceConsegnato = reader.GetOrdinal("consegnato");
                    int indiceGradoParentela = reader.GetOrdinal("gradoParentela");
                    int indiceGradoParentelaDocumento = reader.GetOrdinal("gradoParentelaDocumento");
                    int indiceFatturaData = reader.GetOrdinal("fatturaData");
                    int indiceFatturaNumero = reader.GetOrdinal("fatturaNumero");

                    #endregion

                    while (reader.Read())
                    {
                        DocumentoCopertina documento = new DocumentoCopertina();
                        documenti.Add(documento);

                        documento.IdDomanda = reader.GetInt32(indiceIdDomanda);
                        if (!reader.IsDBNull(indiceIdTipoDocumento))
                        {
                            documento.IdTipoDocumento = reader.GetInt16(indiceIdTipoDocumento);
                        }
                        documento.IdTipoDocumentoArchidoc = reader.GetInt32(indiceIdTipoDocumentoArchidoc);
                        documento.DescrizioneDocumento = reader.GetString(indiceDescrizioneDocumento);
                        if (!reader.IsDBNull(indiceDescrizioneBeneficiario))
                        {
                            documento.DescrizioneBeneficiario = reader.GetString(indiceDescrizioneBeneficiario);
                        }
                        if (!reader.IsDBNull(indiceIdFatturaDichiarata))
                        {
                            documento.IdFatturaDichiarata = reader.GetInt32(indiceIdFatturaDichiarata);
                        }
                        if (!reader.IsDBNull(indiceConsegnato))
                        {
                            documento.Consegnato = reader.GetBoolean(indiceConsegnato);
                        }
                        if (!reader.IsDBNull(indiceGradoParentela))
                        {
                            documento.GradoParentela = reader.GetString(indiceGradoParentela);
                        }
                        if (!reader.IsDBNull(indiceGradoParentelaDocumento))
                        {
                            documento.GradoParentelaDocumento = reader.GetString(indiceGradoParentelaDocumento);
                        }
                        if (!reader.IsDBNull(indiceFatturaData))
                        {
                            documento.FatturaData = reader.GetDateTime(indiceFatturaData);
                        }
                        if (!reader.IsDBNull(indiceFatturaNumero))
                        {
                            documento.FatturaNumero = reader.GetString(indiceFatturaNumero);
                        }
                    }
                }
            }

            return documenti;
        }

        public bool UpdateDomandaSetFamiliareDaAnagrafica(int idDomanda, int idFamiliare)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateSetFamiliare")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, idFamiliare);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool UpdateDomandaSetPresaInCarico(int idDomanda, int idUtente, bool inCarico)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdatePresaInCarico"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);
                DatabaseCemi.AddInParameter(comando, "@inCarico", DbType.Boolean, inCarico);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        //public OreMensiliCNCECollection GetOreMensiliCNCE(int idLavoratore)
        //{
        //    OreMensiliCNCECollection ore = new OreMensiliCNCECollection();

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCEOreSelect"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

        //        using (IDataReader reader = databaseCemi.ExecuteReader(comando))
        //        {
        //            while (reader.Read())
        //            {
        //                OreMensiliCNCE oreMese = TrasformaReaderInOreMensili(reader);
        //                ore.Add(oreMese);
        //            }
        //        }
        //    }

        //    return ore;
        //}

        //public OreMensiliCNCE GetOreMensiliCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        //{
        //    OreMensiliCNCE ore = null;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCEOreSelectSingolo"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

        //        using (IDataReader reader = databaseCemi.ExecuteReader(comando))
        //        {
        //            if (reader.Read())
        //            {
        //                ore = TrasformaReaderInOreMensili(reader);
        //            }
        //        }
        //    }

        //    return ore;
        //}

        //private OreMensiliCNCE TrasformaReaderInOreMensili(IDataReader reader)
        //{
        //    OreMensiliCNCE oreMese = new OreMensiliCNCE();

        //    oreMese.IdCassaEdile = (string) reader["idCassaEdile"];
        //    oreMese.IdLavoratore = (int) reader["idLavoratore"];
        //    oreMese.Anno = (int) reader["anno"];
        //    oreMese.Mese = (int) reader["mese"];
        //    if (!Convert.IsDBNull(reader["oreLavorate"]))
        //        oreMese.OreLavorate = (int) reader["oreLavorate"];
        //    if (!Convert.IsDBNull(reader["oreFerie"]))
        //        oreMese.OreFerie = (int) reader["oreFerie"];
        //    if (!Convert.IsDBNull(reader["oreInfortunio"]))
        //        oreMese.OreInfortunio = (int) reader["oreInfortunio"];
        //    if (!Convert.IsDBNull(reader["oreMalattia"]))
        //        oreMese.OreMalattia = (int) reader["oreMalattia"];
        //    if (!Convert.IsDBNull(reader["oreCassaIntegrazione"]))
        //        oreMese.OreCassaIntegrazione = (int) reader["oreCassaIntegrazione"];
        //    if (!Convert.IsDBNull(reader["orePermessoRetribuito"]))
        //        oreMese.OrePermessoRetribuito = (int) reader["orePermessoRetribuito"];
        //    if (!Convert.IsDBNull(reader["orePermessoNonRetribuito"]))
        //        oreMese.OrePermessoNonRetribuito = (int) reader["orePermessoNonRetribuito"];
        //    if (!Convert.IsDBNull(reader["oreAltro"]))
        //        oreMese.OreAltro = (int) reader["oreAltro"];
        //    if (!Convert.IsDBNull(reader["livelloErogazione"]))
        //        oreMese.LivelloErogazione = (int) reader["livelloErogazione"];
        //    oreMese.InseriteManualmente = (bool) reader["inseriteManualmente"];

        //    return oreMese;
        //}

        //public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
        //{
        //    bool res = false;
        //    oreDuplicate = false;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertUpdate"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, ore.IdLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, ore.IdCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, ore.Anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, ore.Mese);

        //        databaseCemi.AddInParameter(comando, "@oreLavorate", DbType.Int32, ore.OreLavorate);
        //        databaseCemi.AddInParameter(comando, "@oreFerie", DbType.Int32, ore.OreFerie);
        //        databaseCemi.AddInParameter(comando, "@oreInfortunio", DbType.Int32, ore.OreInfortunio);
        //        databaseCemi.AddInParameter(comando, "@oreMalattia", DbType.Int32, ore.OreMalattia);
        //        databaseCemi.AddInParameter(comando, "@oreCassaIntegrazione", DbType.Int32, ore.OreCassaIntegrazione);
        //        databaseCemi.AddInParameter(comando, "@orePermessoRetribuito", DbType.Int32, ore.OrePermessoRetribuito);
        //        databaseCemi.AddInParameter(comando, "@orePermessoNonRetribuito", DbType.Int32,
        //                                    ore.OrePermessoNonRetribuito);
        //        databaseCemi.AddInParameter(comando, "@oreAltro", DbType.Int32, ore.OreAltro);
        //        databaseCemi.AddInParameter(comando, "@livelloErogazione", DbType.Int32, ore.LivelloErogazione);
        //        databaseCemi.AddInParameter(comando, "@inseriteManualmente", DbType.Boolean, ore.InseriteManualmente);

        //        try
        //        {
        //            if (databaseCemi.ExecuteNonQuery(comando) == 1)
        //                res = true;
        //        }
        //        catch (SqlException sqlExc)
        //        {
        //            // Eccezione che viene generata se provo ad inserire due volte le ore per lo stesso mese
        //            if (sqlExc.Number == 2627)
        //                oreDuplicate = true;
        //            else
        //                throw;
        //        }
        //    }

        //    return res;
        //}

        public void ControllaDomandaLavoratore(Domanda domanda)
        {
            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloLavoratore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                DatabaseCemi.AddOutParameter(comando, "@indirizzo", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@email", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@cellulare", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@controlloLavoratore", DbType.Boolean, 1);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.Lavoratore.ControlloIndirizzo =
                        DatabaseCemi.GetParameterValue(comando, "@indirizzo") as bool?;
                    domanda.Lavoratore.ControlloEmail = DatabaseCemi.GetParameterValue(comando, "@email") as bool?;
                    domanda.Lavoratore.ControlloCellulare =
                        DatabaseCemi.GetParameterValue(comando, "@cellulare") as bool?;
                    domanda.ControlloLavoratore =
                        DatabaseCemi.GetParameterValue(comando, "@controlloLavoratore") as bool?;
                }
            }
        }

        public void ControllaDomandaFamiliare(Domanda domanda)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloFamiliare")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                DatabaseCemi.AddOutParameter(comando, "@familiareSelezionato", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@dataDecesso", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@gradoParentela", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@dataNascita", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@codiceFiscale", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@controlloFamiliare", DbType.Boolean, 1);
                DatabaseCemi.AddOutParameter(comando, "@aCarico", DbType.Boolean, 1);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    if (domanda.Familiare != null)
                    {
                        domanda.Familiare.ControlloFamiliareSelezionato =
                            DatabaseCemi.GetParameterValue(comando, "@familiareSelezionato") as bool?;
                        domanda.Familiare.ControlloDataDecesso =
                            DatabaseCemi.GetParameterValue(comando, "@dataDecesso") as bool?;
                        domanda.Familiare.ControlloGradoParentela =
                            DatabaseCemi.GetParameterValue(comando, "@gradoParentela") as bool?;
                        domanda.Familiare.ControlloDataNascita =
                            DatabaseCemi.GetParameterValue(comando, "@dataNascita") as bool?;
                        domanda.Familiare.ControlloCodiceFiscale =
                            DatabaseCemi.GetParameterValue(comando, "@codiceFiscale") as bool?;
                        domanda.Familiare.ControlloACarico =
                            DatabaseCemi.GetParameterValue(comando, "@aCarico") as bool?;
                        domanda.ControlloFamiliare =
                            DatabaseCemi.GetParameterValue(comando, "@controlloFamiliare") as bool?;
                    }
                    else if (domanda.FamiliareFornito != null)
                    {
                        domanda.FamiliareFornito.ControlloFamiliareSelezionato =
                            DatabaseCemi.GetParameterValue(comando, "@familiareSelezionato") as bool?;
                        domanda.FamiliareFornito.ControlloDataDecesso =
                            DatabaseCemi.GetParameterValue(comando, "@dataDecesso") as bool?;
                        domanda.FamiliareFornito.ControlloGradoParentela =
                            DatabaseCemi.GetParameterValue(comando, "@gradoParentela") as bool?;
                        domanda.FamiliareFornito.ControlloDataNascita =
                            DatabaseCemi.GetParameterValue(comando, "@dataNascita") as bool?;
                        domanda.FamiliareFornito.ControlloCodiceFiscale =
                            DatabaseCemi.GetParameterValue(comando, "@codiceFiscale") as bool?;
                        domanda.ControlloFamiliare =
                            DatabaseCemi.GetParameterValue(comando, "@controlloFamiliare") as bool?;
                    }
                }
            }
        }

        public void ControllaDomandaUnivocita(Domanda domanda)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloUnivocita")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                DatabaseCemi.AddOutParameter(comando, "@controlloUnivocita", DbType.Boolean, 1);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.ControlloUnivocitaPrestazione =
                        DatabaseCemi.GetParameterValue(comando, "@controlloUnivocita") as bool?;
                }
            }
        }

        public void ControllaDomandaDocumenti(Domanda domanda)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloDocumenti")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                DatabaseCemi.AddOutParameter(comando, "@controlloDocumenti", DbType.Boolean, 1);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.ControlloPresenzaDocumenti =
                        DatabaseCemi.GetParameterValue(comando, "@controlloDocumenti") as bool?;
                }
            }
        }

        public void ControllaDomandaFatture(Domanda domanda)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloFatture")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                DatabaseCemi.AddOutParameter(comando, "@controlloFatture", DbType.Boolean, 1);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    domanda.ControlloFatture =
                        DatabaseCemi.GetParameterValue(comando, "@controlloFatture") as bool?;
                }
            }
        }

        public void ControllaDomandaScolastiche(Domanda domanda)
        {
            if (domanda.IdTipoPrestazione != "C-ANID")
            {
                domanda.ControlloScolastiche = true;
            }
            else
            {
                using (DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeControlloScolastiche")
                )
                {
                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, domanda.IdDomanda.Value);
                    DatabaseCemi.AddOutParameter(comando, "@numeroMesiFrequenza", DbType.Boolean, 1);
                    DatabaseCemi.AddOutParameter(comando, "@periodo180Giorni", DbType.Boolean, 1);
                    DatabaseCemi.AddOutParameter(comando, "@periodo3Anni", DbType.Boolean, 1);
                    DatabaseCemi.AddOutParameter(comando, "@annoFrequenza", DbType.Boolean, 1);
                    DatabaseCemi.AddOutParameter(comando, "@controlloScolastiche", DbType.Boolean, 1);

                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        //domanda.ControlloFatture =
                        //    databaseCemi.GetParameterValue(comando, "@controlloScolastiche") as bool?;
                        domanda.ControlloScolastiche =
                            DatabaseCemi.GetParameterValue(comando, "@controlloScolastiche") as bool?;
                    }
                }
            }
        }

        public bool UpdateDomandaAccogli(int idDomanda, int idUtente, string idTipoPrestazione)
        {
            bool res = false;

            // Se viene accolta una prestazione per cure e protesi dentarie
            // bisogna generare le due prestazioni per il calcolo:
            // - COO4CL per le cure
            // - C004PL per le protesi
            if (idTipoPrestazione == "C004CP")
            {
                Domanda domanda = GetDomanda(idDomanda);

                if (domanda.Importi != null && domanda.Importi.Count > 0)
                {
                    foreach (PrestazioneImporto importo in domanda.Importi)
                    {
                        // Cure
                        if (importo.IdTipoImporto == 19 && importo.Valore > 0)
                        {
                            Domanda domandaCure = domanda.CopiaBase();
                            domandaCure.TipoPrestazione.IdTipoPrestazione = "C004CL";
                            domandaCure.Stato.IdStato = "C";
                            domandaCure.Stato.Descrizione = "Accolta";

                            InsertDomanda(domandaCure, null, idUtente, true);
                        }

                        // Protesi
                        if (importo.IdTipoImporto == 18 && importo.Valore > 0)
                        {
                            Domanda domandaProtesi = domanda.CopiaBase();
                            domandaProtesi.TipoPrestazione.IdTipoPrestazione = "C004PL";
                            domandaProtesi.Stato.IdStato = "C";
                            domandaProtesi.Stato.Descrizione = "Accolta";

                            InsertDomanda(domandaProtesi, null, idUtente, true);
                        }
                    }
                }
            }

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAccogli"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (idUtente > 0)
                    DatabaseCemi.AddInParameter(comando, "@idUtenteGestione", DbType.Int32, idUtente);

                DatabaseCemi.AddOutParameter(comando, "@righeAggiornate", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);
                int righeAggiornate = (int) DatabaseCemi.GetParameterValue(comando, "@righeAggiornate");
                if (righeAggiornate == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public StatoDomandaCollection GetStatiDomanda()
        {
            StatoDomandaCollection stati = new StatoDomandaCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniStatiDomandaSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdStato = reader.GetOrdinal("idPrestazioniStatoDomanda");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        StatoDomanda stato = new StatoDomanda();
                        stati.Add(stato);

                        stato.IdStato = reader.GetString(indiceIdStato);
                        stato.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return stati;
        }

        public bool UpdateDomandaAnnulla(int idDomanda, int idUtente)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAnnulla"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                if (idUtente > 0)
                    DatabaseCemi.AddInParameter(comando, "@idUtenteGestione", DbType.Int32, idUtente);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaAttesa(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAttesa"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaAttesaDenuncia(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateAttesaDenuncia"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaEsame(int idDomanda)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateEsame"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public bool UpdateDomandaRespingi(int idDomanda, string idTipoCausale)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateRespingi"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idTipoCausale", DbType.String, idTipoCausale);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public TipoDocumentoCollection GetTipiDocumento()
        {
            TipoDocumentoCollection tipiDocumento = new TipoDocumentoCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiDocumentiSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoDocumento = reader.GetOrdinal("idTipoDocumento");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoDocumento tipoDocumento = new TipoDocumento();
                        tipiDocumento.Add(tipoDocumento);

                        tipoDocumento.IdTipoDocumento = reader.GetInt16(indiceIdTipoDocumento);
                        tipoDocumento.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiDocumento;
        }

        /// <summary>
        ///     Ritorna i tipi causali finalizzati alla gestione fast delle prestazioni
        /// </summary>
        /// <returns>Elenco delle causali</returns>
        public TipoCausaleCollection GetTipiCausaliFast()
        {
            return GetTipiCausali(true, string.Empty);
        }

        /// <summary>
        /// </summary>
        /// <param name="fast"></param>
        /// <param name="idTipoPrestazione"></param>
        /// <returns></returns>
        public TipoCausaleCollection GetTipiCausali(bool? fast, string idTipoPrestazione)
        {
            TipoCausaleCollection tipiCausale = new TipoCausaleCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiCausaliSelect"))
            {
                if (fast.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@visualizzabiliFast", DbType.Int32, fast.Value);
                if (!string.IsNullOrEmpty(idTipoPrestazione))
                    DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoCausale = reader.GetOrdinal("idTipoCausale");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");
                    int indiceDescrizioneEstesa = reader.GetOrdinal("descrizioneEstesa");

                    while (reader.Read())
                    {
                        TipoCausale tipoCausale = new TipoCausale();
                        tipiCausale.Add(tipoCausale);

                        tipoCausale.IdTipoCausale = reader.GetString(indiceIdTipoCausale);
                        tipoCausale.Descrizione = reader.GetString(indiceDescrizione);
                        if (!Convert.IsDBNull(reader["descrizioneEstesa"]))
                            tipoCausale.DescrizioneEstesa = reader.GetString(indiceDescrizioneEstesa);
                    }
                }
            }

            return tipiCausale;
        }

        public bool AssociaPersonaDocumento(short idTipoDocumento, int idDocumento,
            Lavoratore lavoratore, Familiare familiare, int? idDomanda)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniAssociaDocumento"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16, idTipoDocumento);
                DatabaseCemi.AddInParameter(comando, "@idDocumento", DbType.Int32, idDocumento);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, lavoratore.IdLavoratore.Value);
                if (familiare != null && familiare.IdFamiliare.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idFamiliare", DbType.Int32, familiare.IdFamiliare.Value);
                if (idDomanda.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) > 0)
                    res = true;
            }

            return res;
        }

        public bool DisassociaDocumento(short idTipoDocumento, int idDocumento)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDisassociaDocumento"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16, idTipoDocumento);
                DatabaseCemi.AddInParameter(comando, "@idDocumento", DbType.Int32, idDocumento);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }
            return res;
        }

        public PrestazioneErogataCollection GetPrestazioniErogate(PrestazioneErogataFilter filtro)
        {
            PrestazioneErogataCollection prestazioni = new PrestazioneErogataCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniCompleteSelectByIdLavoratore"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, filtro.IdLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceIdTipoPrestazione = reader.GetOrdinal("idTipoPrestazione");
                    int indiceDescrizioneTipoPrestazione = reader.GetOrdinal("descrizioneTipoPrestazione");
                    int indiceIdFamiliare = reader.GetOrdinal("idFamiliare");
                    int indiceCognomeFamiliare = reader.GetOrdinal("familiareCognome");
                    int indiceNomeFamiliare = reader.GetOrdinal("familiareNome");
                    int indiceProtocolloPrestazione = reader.GetOrdinal("protocolloPrestazione");
                    int indiceNumeroProtocolloPrestazione = reader.GetOrdinal("numeroProtocolloPrestazione");
                    int indiceImportoLiquidabile = reader.GetOrdinal("importoLiquidabile");
                    int indiceIdStatoPrestazione = reader.GetOrdinal("idTipoStatoPrestazione");
                    int indiceDataDomanda = reader.GetOrdinal("dataDomanda");

                    #endregion

                    while (reader.Read())
                    {
                        PrestazioneErogata prestazione = new PrestazioneErogata();
                        prestazioni.Add(prestazione);

                        prestazione.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        prestazione.TipoPrestazione = new TipoPrestazione();
                        prestazione.TipoPrestazione.IdTipoPrestazione = reader.GetString(indiceIdTipoPrestazione);
                        prestazione.TipoPrestazione.Descrizione = reader.GetString(indiceDescrizioneTipoPrestazione);
                        prestazione.ProtocolloPrestazione = reader.GetInt32(indiceProtocolloPrestazione);
                        prestazione.NumeroProtocolloPrestazione = reader.GetInt32(indiceNumeroProtocolloPrestazione);
                        if (!reader.IsDBNull(indiceImportoLiquidabile))
                            prestazione.ImportoLiquidabile = reader.GetDecimal(indiceImportoLiquidabile);
                        if (!reader.IsDBNull(indiceIdStatoPrestazione))
                            prestazione.IdStatoPrestazione = reader.GetString(indiceIdStatoPrestazione);
                        if (!reader.IsDBNull(indiceDataDomanda))
                            prestazione.DataDomanda = reader.GetDateTime(indiceDataDomanda);

                        if (!reader.IsDBNull(indiceIdFamiliare))
                        {
                            prestazione.Familiare = new Familiare();
                            prestazione.Familiare.IdLavoratore = prestazione.IdLavoratore;
                            prestazione.Familiare.IdFamiliare = reader.GetInt32(indiceIdFamiliare);
                            prestazione.Familiare.Cognome = reader.GetString(indiceCognomeFamiliare);
                            prestazione.Familiare.Nome = reader.GetString(indiceNomeFamiliare);
                        }
                    }
                }
            }

            return prestazioni;
        }

        public bool FatturaGiaUtilizzata(FatturaDichiarata fattura, int idLavoratore)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_FattureDichiarateSelectFatturaGiaPresente"))
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddOutParameter(comando, "@numeroFatture", DbType.Int32, 4);

                DatabaseCemi.ExecuteNonQuery(comando);

                if ((int) DatabaseCemi.GetParameterValue(comando, "@numeroFatture") > 0)
                    res = true;
            }

            return res;
        }

        //public bool DeleteOreCNCE(int idLavoratore, string idCassaEdile, int anno, int mese)
        //{
        //    bool res = false;

        //    using (DbCommand comando = databaseCemi.GetStoredProcCommand("dbo.USP_CNCELOreInsertDelete"))
        //    {
        //        databaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);
        //        databaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
        //        databaseCemi.AddInParameter(comando, "@anno", DbType.Int32, anno);
        //        databaseCemi.AddInParameter(comando, "@mese", DbType.Int32, mese);

        //        if (databaseCemi.ExecuteNonQuery(comando) == 1)
        //            res = true;
        //    }

        //    return res;
        //}

        public bool DeleteDomandaTemporanea(int idDomanda, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeTemporaneeDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                        res = true;
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                        res = true;
                }
            }

            return res;
        }

        public bool UpdateImportoPrestazione(PrestazioneImporto importo)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiPrestazioneUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idImporto", DbType.Int32, importo.IdImporto);
                DatabaseCemi.AddInParameter(comando, "@valore", DbType.Currency, importo.Valore);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public bool InsertImportoPrestazione(PrestazioneImporto importo)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiPrestazioneInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoImporto", DbType.Int32, importo.IdTipoImporto);
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, importo.IdDomanda);
                DatabaseCemi.AddInParameter(comando, "@valore", DbType.Currency, importo.Valore);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public bool InsertCassaEdileInDomanda(int idDomanda, string idCassaEdile)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeCasseEdiliInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idCassaEdile", DbType.String, idCassaEdile);
                DatabaseCemi.AddInParameter(comando, "@inseritaManualmente", DbType.Boolean, true);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniLavoratoriSiceNewSelect"))
            {
                if (filtro.IdLavoratore.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.String, filtro.IdLavoratore.Value);
                }
                if (!string.IsNullOrEmpty(filtro.Cognome))
                {
                    DatabaseCemi.AddInParameter(comando, "@cognome", DbType.String, filtro.Cognome);
                }
                if (!string.IsNullOrEmpty(filtro.Nome))
                {
                    DatabaseCemi.AddInParameter(comando, "@nome", DbType.String, filtro.Nome);
                }
                if (filtro.DataNascita.HasValue)
                {
                    DatabaseCemi.AddInParameter(comando, "@dataNascita", DbType.DateTime, filtro.DataNascita.Value);
                }

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");

                    #endregion

                    while (reader.Read())
                    {
                        Lavoratore lavoratore = new Lavoratore();
                        lavoratori.Add(lavoratore);

                        //lavoratore.i
                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                    }
                }
            }

            return lavoratori;
        }

        public Lavoratore GetLavoratoreSiceNewByKey(int idLavoratore)
        {
            Lavoratore lavoratore = null;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniLavoratoreSiceNewSelectByKey")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, idLavoratore);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    #region Indici per il reader

                    int indiceIdLavoratore = reader.GetOrdinal("idLavoratore");
                    int indiceCognome = reader.GetOrdinal("cognome");
                    int indiceNome = reader.GetOrdinal("nome");
                    int indiceDataNascita = reader.GetOrdinal("dataNascita");
                    int indiceCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                    int indiceIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                    int indiceComune = reader.GetOrdinal("indirizzoComune");
                    int indiceProvincia = reader.GetOrdinal("indirizzoProvincia");
                    int indiceCap = reader.GetOrdinal("indirizzoCap");
                    int indiceCellulare = reader.GetOrdinal("cellulare");
                    int indiceEmail = reader.GetOrdinal("eMail");
                    int indiceIdtipoPagamento = reader.GetOrdinal("idTipoPagamento");

                    #endregion

                    while (reader.Read())
                    {
                        lavoratore = new Lavoratore();

                        lavoratore.IdLavoratore = reader.GetInt32(indiceIdLavoratore);
                        lavoratore.Cognome = reader.GetString(indiceCognome);
                        if (!reader.IsDBNull(indiceNome))
                        {
                            lavoratore.Nome = reader.GetString(indiceNome);
                        }
                        if (!reader.IsDBNull(indiceDataNascita))
                        {
                            lavoratore.DataNascita = reader.GetDateTime(indiceDataNascita);
                        }
                        if (!reader.IsDBNull(indiceCodiceFiscale))
                        {
                            lavoratore.CodiceFiscale = reader.GetString(indiceCodiceFiscale);
                        }
                        if (!reader.IsDBNull(indiceIndirizzoDenominazione))
                        {
                            lavoratore.Indirizzo = new Indirizzo();

                            lavoratore.Indirizzo.IndirizzoVia = reader.GetString(indiceIndirizzoDenominazione);

                            if (!reader.IsDBNull(indiceComune))
                            {
                                lavoratore.Indirizzo.Comune = reader.GetString(indiceComune);
                            }
                            if (!reader.IsDBNull(indiceProvincia))
                            {
                                lavoratore.Indirizzo.Provincia = reader.GetString(indiceProvincia);
                            }
                            if (!reader.IsDBNull(indiceCap))
                            {
                                lavoratore.Indirizzo.Cap = reader.GetString(indiceCap);
                            }
                        }
                        if (!reader.IsDBNull(indiceCellulare))
                        {
                            lavoratore.Cellulare = reader.GetString(indiceCellulare);
                        }
                        if (!reader.IsDBNull(indiceEmail))
                        {
                            lavoratore.Email = reader.GetString(indiceEmail);
                        }
                        if (!reader.IsDBNull(indiceIdtipoPagamento))
                        {
                            lavoratore.IdTipoPagamento = reader.GetString(indiceIdtipoPagamento);
                        }
                    }
                }
            }

            return lavoratore;
        }

        public TipoPrestazione GetTipoPrestazione(string gradoParentela, string idTipoPrestazione)
        {
            TipoPrestazione tipoPrestazione = new TipoPrestazione();

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniConfigurazioneSelectSingle")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String, gradoParentela);
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    tipoPrestazione.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                    tipoPrestazione.Descrizione = (string) reader["descrizione"];

                    //leggo anche il periodo di validit�
                    if (!Convert.IsDBNull(reader["periodoInserimentoInizio"]))
                        tipoPrestazione.ValidaDa = (DateTime) reader["periodoInserimentoInizio"];
                    if (!Convert.IsDBNull(reader["periodoInserimentoFine"]))
                        tipoPrestazione.ValidaA = (DateTime) reader["periodoInserimentoFine"];

                    tipoPrestazione.GradoParentela = gradoParentela;
                }
            }

            return tipoPrestazione;
        }

        #region Metodi relativi a PrestazioniCRM

        /// <summary>
        ///     Permette di selezionare una prestazione a partire da una domanda di prestazione SICEINFO
        /// </summary>
        /// <param name="idPrestazioneDomanda"></param>
        /// <returns></returns>
        public PrestazioneCRMCollection GetPrestazioniCRMColletcion(int idPrestazioneDomanda)
        {
            DateTime dataLiquidazione = DateTime.Today;
            PrestazioneCRMCollection listaPrestazioni = new PrestazioneCRMCollection();

            #region Query

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_CRM_PrestazioniDomandeSelectById"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32, idPrestazioneDomanda);

                using (IDataReader dataReader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (dataReader != null)
                    {
                        #region Get ordinal index

                        int idTipoPrestazioneIndex = dataReader.GetOrdinal("idTipoPrestazione");
                        int protocolloPrestazioneIndex = dataReader.GetOrdinal("protocolloPrestazione");
                        int numeroProtocolloPrestazioneIndex = dataReader.GetOrdinal("numeroProtocolloPrestazione");
                        int descrizioneIndex = dataReader.GetOrdinal("descrizione");
                        int beneficiarioIndex = dataReader.GetOrdinal("beneficiario");
                        int dataDomandaIndex = dataReader.GetOrdinal("dataDomanda");
                        int dataRiferimentoIndex = dataReader.GetOrdinal("dataRiferimento");
                        int idLavoratoreIndex = dataReader.GetOrdinal("idLavoratore");
                        int idFamiliareIndex = dataReader.GetOrdinal("idFamiliare");
                        int statoIndex = dataReader.GetOrdinal("stato");
                        int causaleRespintaIndex = dataReader.GetOrdinal("causaleRespinta");
                        int lavoratoreIndirizzoIndex = dataReader.GetOrdinal("lavoratoreIndirizzo");
                        int lavoratoreProvinciaIndex = dataReader.GetOrdinal("lavoratoreProvincia");
                        int lavoratoreComuneIndex = dataReader.GetOrdinal("lavoratoreComune");
                        int lavoratoreFrazioneIndex = dataReader.GetOrdinal("lavoratoreFrazione");
                        int lavoratoreCapIndex = dataReader.GetOrdinal("lavoratoreCap");
                        int lavoratoreCellulareIndex = dataReader.GetOrdinal("lavoratoreCellulare");
                        int lavoratoreEmailIndex = dataReader.GetOrdinal("lavoratoreEmail");
                        int lavoratoreIdTipoPagamentoIndex = dataReader.GetOrdinal("lavoratoreIdTipoPagamento");
                        int familiareCognomeIndex = dataReader.GetOrdinal("familiareCognome");
                        int familiareNomeIndex = dataReader.GetOrdinal("familiareNome");
                        int familiareDataNascitaIndex = dataReader.GetOrdinal("familiareDataNascita");
                        int familiareCodiceFiscaleIndex = dataReader.GetOrdinal("familiareCodiceFiscale");
                        int gradoParentelaIndex = dataReader.GetOrdinal("gradoParentela");
                        int importoErogatoLordoIndex = dataReader.GetOrdinal("importoErogatoLordo");
                        int importoErogatoIndex = dataReader.GetOrdinal("importoErogato");
                        int modalitaPagamentoIndex = dataReader.GetOrdinal("modalitaPagamento");
                        int numeroMandatoIndex = dataReader.GetOrdinal("numeroMandato");
                        int statoAssegnoIndex = dataReader.GetOrdinal("statoAssegno");
                        int descrizioneStatoAssegnoIndex = dataReader.GetOrdinal("descrizioneStatoAssegno");
                        int dataLiquidazioneIndex = dataReader.GetOrdinal("dataLiquidazione");
                        int idTipoPagamentoIndex = dataReader.GetOrdinal("idTipoPagamento");
                        int ibanIndex = dataReader.GetOrdinal("IBAN");

                        #endregion

                        dataReader.Read();

                        PrestazioneCrm prestazioneCrm = new PrestazioneCrm();

                        // chiavi
                        prestazioneCrm.IdTipoPrestazione = dataReader.GetString(idTipoPrestazioneIndex);
                        prestazioneCrm.ProtocolloPrestazione = dataReader.GetInt32(protocolloPrestazioneIndex);
                        prestazioneCrm.NumeroProtocolloPrestazione =
                            dataReader.GetInt32(numeroProtocolloPrestazioneIndex);

                        if (dataReader[beneficiarioIndex] != DBNull.Value)
                            prestazioneCrm.Beneficiario = dataReader.GetString(beneficiarioIndex);

                        //Con questa property viene impostata anche la lista CausaliRespinta che pu� al massimo contenere un valore!
                        if (dataReader[causaleRespintaIndex] != DBNull.Value)
                            prestazioneCrm.CausaleRespinta = dataReader.GetString(causaleRespintaIndex);

                        if (dataReader[dataDomandaIndex] != DBNull.Value)
                            prestazioneCrm.DataDomanda = dataReader.GetDateTime(dataDomandaIndex);
                        if (dataReader[dataLiquidazioneIndex] != DBNull.Value)
                            prestazioneCrm.DataLiquidazione = dataReader.GetDateTime(dataLiquidazioneIndex);
                        if (dataReader[dataRiferimentoIndex] != DBNull.Value)
                            prestazioneCrm.DataRiferimento = dataReader.GetDateTime(dataRiferimentoIndex);
                        if (dataReader[descrizioneIndex] != DBNull.Value)
                            prestazioneCrm.Descrizione = dataReader.GetString(descrizioneIndex);
                        if (dataReader[descrizioneStatoAssegnoIndex] != DBNull.Value)
                            prestazioneCrm.DescrizioneStatoAssegno =
                                dataReader.GetString(descrizioneStatoAssegnoIndex);
                        if (dataReader[familiareCodiceFiscaleIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareCodiceFiscale =
                                dataReader.GetString(familiareCodiceFiscaleIndex);
                        if (dataReader[familiareCognomeIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareCognome = dataReader.GetString(familiareCognomeIndex);
                        if (dataReader[familiareDataNascitaIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareDataNascita =
                                dataReader.GetDateTime(familiareDataNascitaIndex);
                        if (dataReader[familiareNomeIndex] != DBNull.Value)
                            prestazioneCrm.FamiliareNome = dataReader.GetString(familiareNomeIndex);
                        if (dataReader[gradoParentelaIndex] != DBNull.Value)
                            prestazioneCrm.GradoParentela = dataReader.GetString(gradoParentelaIndex);
                        if (dataReader[ibanIndex] != DBNull.Value)
                            prestazioneCrm.Iban = dataReader.GetString(ibanIndex);
                        if (dataReader[idFamiliareIndex] != DBNull.Value)
                            prestazioneCrm.IdFamiliare = dataReader.GetInt32(idFamiliareIndex);
                        if (dataReader[idLavoratoreIndex] != DBNull.Value)
                            prestazioneCrm.IdLavoratore = dataReader.GetInt32(idLavoratoreIndex);
                        if (dataReader[idTipoPagamentoIndex] != DBNull.Value)
                            prestazioneCrm.IdTipoPagamento = dataReader.GetString(idTipoPagamentoIndex);
                        if (dataReader[importoErogatoIndex] != DBNull.Value)
                            prestazioneCrm.ImportoErogato = dataReader.GetDecimal(importoErogatoIndex);
                        if (dataReader[importoErogatoLordoIndex] != DBNull.Value)
                            prestazioneCrm.ImportoErogatoLordo = dataReader.GetDecimal(importoErogatoLordoIndex);
                        if (dataReader[lavoratoreCapIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreCap = dataReader.GetString(lavoratoreCapIndex);
                        if (dataReader[lavoratoreCellulareIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreCellulare = dataReader.GetString(lavoratoreCellulareIndex);
                        if (dataReader[lavoratoreComuneIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreComune = dataReader.GetString(lavoratoreComuneIndex);
                        if (dataReader[lavoratoreEmailIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreEmail = dataReader.GetString(lavoratoreEmailIndex);
                        if (dataReader[lavoratoreFrazioneIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreFrazione = dataReader.GetString(lavoratoreFrazioneIndex);
                        if (dataReader[lavoratoreIdTipoPagamentoIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreIdTipoPagamento =
                                dataReader.GetString(lavoratoreIdTipoPagamentoIndex);
                        if (dataReader[lavoratoreIndirizzoIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreIndirizzo = dataReader.GetString(lavoratoreIndirizzoIndex);
                        if (dataReader[lavoratoreProvinciaIndex] != DBNull.Value)
                            prestazioneCrm.LavoratoreProvincia = dataReader.GetString(lavoratoreProvinciaIndex);
                        if (dataReader[modalitaPagamentoIndex] != DBNull.Value)
                            prestazioneCrm.ModalitaPagamento = dataReader.GetString(modalitaPagamentoIndex);
                        if (dataReader[numeroMandatoIndex] != DBNull.Value)
                            prestazioneCrm.NumeroMandato = dataReader.GetString(numeroMandatoIndex);
                        if (dataReader[statoIndex] != DBNull.Value)
                            prestazioneCrm.Stato = dataReader.GetString(statoIndex);
                        if (dataReader[statoAssegnoIndex] != DBNull.Value)
                            prestazioneCrm.StatoAssegno = dataReader.GetString(statoAssegnoIndex);

                        //TODO leggere il DS dei documenti mancanti
                        // Documenti in attesa
                        dataReader.NextResult();
                        DocumentoCollection documenti = new DocumentoCollection();
                        while (dataReader.Read())
                        {
                            Documento documento = new Documento();

                            if (!Convert.IsDBNull(dataReader["cognome"]))
                                documento.LavoratoreCognome = (string) dataReader["cognome"];
                            if (!Convert.IsDBNull(dataReader["nome"]))
                                documento.LavoratoreNome = (string) dataReader["nome"];
                            if (!Convert.IsDBNull(dataReader["idArchidoc"]))
                                documento.IdArchidoc = (string) dataReader["idArchidoc"];
                            if (!Convert.IsDBNull(dataReader["idDocumento"]))
                                documento.IdDocumento = (int) dataReader["idDocumento"];
                            if (!Convert.IsDBNull(dataReader["originale"]))
                                documento.Originale = (bool) dataReader["originale"];

                            if (!Convert.IsDBNull(dataReader["univocoPrestazione"]))
                                documento.PerPrestazione = Convert.ToBoolean((int) dataReader["univocoPrestazione"]);
                            documento.RiferitoA = (string) dataReader["documentoRiferitoA"];
                            documento.TipoDocumento = new TipoDocumento();
                            documento.TipoDocumento.IdTipoDocumento =
                                Convert.ToInt16((int) dataReader["idTipoDocumento"]);
                            documento.TipoDocumento.Descrizione = (string) dataReader["descrizioneDocumentoRichiesto"];

                            documenti.Add(documento);
                        }

                        prestazioneCrm.Documenti = documenti;

                        listaPrestazioni.Add(prestazioneCrm);
                    }
                }
            }

            #endregion

            return listaPrestazioni;
        }

        #endregion

        public void UpdateDomandaDataRiferimento(int idDomanda, DateTime dataRiferimento)
        {
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateDataRiferimento"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@dataRiferimento", DbType.DateTime, dataRiferimento);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception(
                        "UpdateDomandaDataRiferimento: non � stato possibile aggiornare la data di riferimento");
                }
            }
        }

        public void UpdateDomandaNota(int idDomanda, int idUtente, string nota)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateNota"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@nota", DbType.String, nota);
                DatabaseCemi.AddInParameter(comando, "@idUtente", DbType.Int32, idUtente);

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("UpdateDomandaNota: non � stato possibile aggiornare la nota");
                }
            }
        }

        public StatiCureProtesi GetStatiCureProtesi(int idDomanda)
        {
            StatiCureProtesi stati = new StatiCureProtesi();

            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniSelectStatiPerCureProtesiDentarie"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        string idTipoPrestazione = reader["idTipoPrestazione"] as string;
                        string stato = reader["stato"] as string;

                        switch (idTipoPrestazione)
                        {
                            case "C004CL":
                                stati.StatoCL = stato;
                                break;
                            case "C004PL":
                                stati.StatoPL = stato;
                                break;
                        }
                    }
                }
            }

            return stati;
        }

        #region Get tipi

        /// <summary>
        ///     ritorna i tipi decesso (necessari alla prestazione funerario)
        /// </summary>
        /// <returns></returns>
        public TipoDecessoCollection GetTipiDecesso()
        {
            TipoDecessoCollection tipiDecesso = new TipoDecessoCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_TipiPrestazioniDecessiSelect"))
            {
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoDecesso = reader.GetOrdinal("idTipoPrestazioniDecesso");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoDecesso tipoDecesso = new TipoDecesso();
                        tipiDecesso.Add(tipoDecesso);

                        tipoDecesso.IdTipoDecesso = reader.GetString(indiceIdTipoDecesso);
                        tipoDecesso.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiDecesso;
        }

        public TipoScuolaCollection GetTipiScuola(string idTipoPrestazione)
        {
            TipoScuolaCollection tipiScuola = new TipoScuolaCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniTipiScuolaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoScuola = reader.GetOrdinal("idTipoScuola");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoScuola tipoScuola = new TipoScuola();
                        tipiScuola.Add(tipoScuola);

                        tipoScuola.IdTipoScuola = reader.GetInt32(indiceIdTipoScuola);
                        tipoScuola.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiScuola;
        }

        public TipoPromozioneCollection GetTipiPromozione(int idTipoScuola)
        {
            TipoPromozioneCollection tipiPromozione = new TipoPromozioneCollection();

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniTipiPromozioneSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32, idTipoScuola);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    int indiceIdTipoPromozione = reader.GetOrdinal("idTipoPromozione");
                    int indiceDescrizione = reader.GetOrdinal("descrizione");

                    while (reader.Read())
                    {
                        TipoPromozione tipoPromozione = new TipoPromozione();
                        tipiPromozione.Add(tipoPromozione);

                        tipoPromozione.IdTipoPromozione = reader.GetInt32(indiceIdTipoPromozione);
                        tipoPromozione.Descrizione = reader.GetString(indiceDescrizione);
                    }
                }
            }

            return tipiPromozione;
        }

        #endregion

        #region Dati aggiuntivi insert update

        public bool UpdateDatiAggiuntiviScolastiche(int idDomanda, DatiScolastiche datiScolastiche)
        {
            bool res = false;

            if (datiScolastiche.IdDatiScolastiche.HasValue)
            {
                using (
                    DbCommand comando =
                        DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDatiAggiuntiviUpdate"))
                {
                    DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                    DatabaseCemi.AddInParameter(comando, "@idDatiAggiuntivi", DbType.Int32,
                        datiScolastiche.IdDatiScolastiche.Value);
                    DatabaseCemi.AddInParameter(comando, "@annoFrequenza", DbType.Int16, datiScolastiche.AnnoFrequenza);
                    if (datiScolastiche.ClasseConclusa.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@classeConclusa", DbType.Int16,
                            datiScolastiche.ClasseConclusa.Value);
                    if (datiScolastiche.TipoScuola != null)
                        DatabaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32,
                            datiScolastiche.TipoScuola.IdTipoScuola);
                    if (datiScolastiche.TipoPromozione != null)
                        DatabaseCemi.AddInParameter(comando, "@idTipoPromozione", DbType.Int32,
                            datiScolastiche.TipoPromozione.IdTipoPromozione);
                    if (datiScolastiche.NumeroMesiFrequentati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numeroMesiFrequentati", DbType.Int16,
                            datiScolastiche.NumeroMesiFrequentati.Value);
                    if (datiScolastiche.AnnoImmatricolazione.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@annoImmatricolazione", DbType.Int16,
                            datiScolastiche.AnnoImmatricolazione.Value);
                    if (datiScolastiche.NumeroAnniFrequentati.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numeroAnniFrequentati", DbType.Int16,
                            datiScolastiche.NumeroAnniFrequentati.Value);
                    if (datiScolastiche.NumeroAnniFuoriCorso.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numeroAnniFuoriCorso", DbType.Int16,
                            datiScolastiche.NumeroAnniFuoriCorso.Value);
                    if (datiScolastiche.MediaVoti.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@mediaVoti", DbType.Decimal,
                            datiScolastiche.MediaVoti.Value);
                    if (datiScolastiche.NumeroEsamiSostenuti.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@numeroEsamiSostenuti", DbType.Int16,
                            datiScolastiche.NumeroEsamiSostenuti.Value);
                    if (datiScolastiche.CfuConseguiti.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@cfuConseguiti", DbType.Int16,
                            datiScolastiche.CfuConseguiti.Value);
                    if (datiScolastiche.CfuPrevisti.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@cfuPrevisti", DbType.Int16,
                            datiScolastiche.CfuPrevisti.Value);

                    //applica deduzione, vale per c003-123, C002-L
                    if (!string.IsNullOrEmpty(datiScolastiche.ApplicaDeduzione))
                        DatabaseCemi.AddInParameter(comando, "@applicaDeduzione", DbType.String,
                            datiScolastiche.ApplicaDeduzione);


                    //Funerario
                    if (datiScolastiche.DataDecesso.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime,
                            datiScolastiche.DataDecesso.Value);
                    if (!string.IsNullOrEmpty(datiScolastiche.IdTipoPrestazioneDecesso))
                        DatabaseCemi.AddInParameter(comando, "@idTipoPrestazioneDecesso", DbType.String,
                            datiScolastiche.IdTipoPrestazioneDecesso);
                    if (datiScolastiche.IdFamiliareErede.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@idFamiliareErede", DbType.Int16,
                            datiScolastiche.IdFamiliareErede.Value);

                    //handicap
                    if (datiScolastiche.AnnoDaErogare.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@annoDaErogare", DbType.Int16,
                            datiScolastiche.AnnoDaErogare.Value);
                    if (datiScolastiche.TipoDaErogare.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@tipoDaErogare", DbType.Int16,
                            datiScolastiche.TipoDaErogare.Value);

                    // Asilo Nido
                    if (datiScolastiche.PeriodoDal.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@periodoFrequenzaDal", DbType.DateTime,
                            datiScolastiche.PeriodoDal.Value);
                    if (datiScolastiche.PeriodoAl.HasValue)
                        DatabaseCemi.AddInParameter(comando, "@periodoFrequenzaAl", DbType.DateTime,
                            datiScolastiche.PeriodoAl.Value);

                    if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                        res = true;
                }
            }

            return res;
        }

        public bool InsertDatiAggiuntiviScolastiche(int idDomanda, DatiScolastiche datiScolastiche)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDatiAggiuntiviInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@annoFrequenza", DbType.Int16, datiScolastiche.AnnoFrequenza);
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String,
                    datiScolastiche.IdTipoPrestazione);

                if (datiScolastiche.ClasseConclusa.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@classeConclusa", DbType.Int16,
                        datiScolastiche.ClasseConclusa.Value);
                if (datiScolastiche.TipoScuola != null)
                    DatabaseCemi.AddInParameter(comando, "@idTipoScuola", DbType.Int32,
                        datiScolastiche.TipoScuola.IdTipoScuola);
                if (datiScolastiche.TipoPromozione != null)
                    DatabaseCemi.AddInParameter(comando, "@idTipoPromozione", DbType.Int32,
                        datiScolastiche.TipoPromozione.IdTipoPromozione);
                if (datiScolastiche.NumeroMesiFrequentati.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroMesiFrequentati", DbType.Int16,
                        datiScolastiche.NumeroMesiFrequentati.Value);
                if (datiScolastiche.AnnoImmatricolazione.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@annoImmatricolazione", DbType.Int16,
                        datiScolastiche.AnnoImmatricolazione.Value);
                if (datiScolastiche.NumeroAnniFrequentati.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroAnniFrequentati", DbType.Int16,
                        datiScolastiche.NumeroAnniFrequentati.Value);
                if (datiScolastiche.NumeroAnniFuoriCorso.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroAnniFuoriCorso", DbType.Int16,
                        datiScolastiche.NumeroAnniFuoriCorso.Value);
                if (datiScolastiche.MediaVoti.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@mediaVoti", DbType.Decimal, datiScolastiche.MediaVoti.Value);
                if (datiScolastiche.NumeroEsamiSostenuti.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@numeroEsamiSostenuti", DbType.Int16,
                        datiScolastiche.NumeroEsamiSostenuti.Value);
                if (datiScolastiche.CfuConseguiti.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@cfuConseguiti", DbType.Int16,
                        datiScolastiche.CfuConseguiti.Value);
                if (datiScolastiche.CfuPrevisti.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@cfuPrevisti", DbType.Int16,
                        datiScolastiche.CfuPrevisti.Value);

                //applica deduzione, vale per c003-123, C002-L
                if (!string.IsNullOrEmpty(datiScolastiche.ApplicaDeduzione))
                    DatabaseCemi.AddInParameter(comando, "@applicaDeduzione", DbType.String,
                        datiScolastiche.ApplicaDeduzione);

                //Funerario
                if (datiScolastiche.DataDecesso.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@dataDecesso", DbType.DateTime,
                        datiScolastiche.DataDecesso.Value);
                if (!string.IsNullOrEmpty(datiScolastiche.IdTipoPrestazioneDecesso))
                    DatabaseCemi.AddInParameter(comando, "@idTipoPrestazioneDecesso", DbType.String,
                        datiScolastiche.IdTipoPrestazioneDecesso);
                if (datiScolastiche.IdFamiliareErede.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@idFamiliareErede", DbType.Int32,
                        datiScolastiche.IdFamiliareErede.Value);

                //handicap
                if (datiScolastiche.AnnoDaErogare.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@annoDaErogare", DbType.Int16,
                        datiScolastiche.AnnoDaErogare.Value);
                if (datiScolastiche.TipoDaErogare.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@tipoDaErogare", DbType.Int16,
                        datiScolastiche.TipoDaErogare.Value);

                // Asilo Nido
                if (datiScolastiche.PeriodoDal.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@periodoFrequenzaDal", DbType.DateTime,
                        datiScolastiche.PeriodoDal.Value);
                if (datiScolastiche.PeriodoAl.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@periodoFrequenzaAl", DbType.DateTime,
                        datiScolastiche.PeriodoAl.Value);

                if (DatabaseCemi.ExecuteNonQuery(comando) >= 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region Forzatura Controlli

        public bool ForzaLavoratoreIndirizzo(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloLavoratoreIndirizzo"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaPresenzaDocumenti(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloDocumenti"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaUnivocita(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaUnivocita"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaLavoratoreEmail(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloLavoratoreEmail"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaLavoratoreCellulare(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloLavoratoreCellulare"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareDataNascita(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareDataNascita"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareDataDecesso(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareDataDecesso"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareCodiceFiscale(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareCodiceFiscale"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaFamiliareACarico(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloFamiliareACarico"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloOreCNCE(int idDomanda, bool stato)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeUpdateForzaControlloOreCNCE"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@stato", DbType.Boolean, stato);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloPeriodoFrequenza180Giorni(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloScolastichePeriodoFrequenza180Giorni"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloPeriodoFrequenza3Anni(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloScolastichePeriodoFrequenza3Anni"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        public bool ForzaControlloAnnoFrequenza(int idDomanda)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniDomandeUpdateForzaControlloScolasticheAnnoFrequenza"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    res = true;
            }

            return res;
        }

        #endregion

        #region FATTURA

        /*
        USP_PrestazioniFatturaSelectPerIdFattura
        USP_PrestazioniFatturaSelectPerIdArchidoc
        USP_PrestazioniFattureInsert
        USP_PrestazioniFattureUpdatePerIdPrestazioniFattura
        USP_PrestazioniFattureUpdateSetValidaPerIdPrestazioniFattura
         
         */

        public Fattura getFatturaPerIdPrestazioniFattura(int idPrestazioniFattura)
        {
            Fattura fattura;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaSelectPerIdFattura")
            )
            {
                DatabaseCemi.AddInParameter(comando, "idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    fattura = new Fattura();
                    fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                    if (!Convert.IsDBNull(reader["idArchidoc"]))
                        fattura.IdArchidoc = (string) reader["idArchidoc"];
                    if (!Convert.IsDBNull(reader["idLavoratore"]))
                        fattura.IdLavoratore = (int) reader["idLavoratore"];
                    fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                    fattura.ImportoTotale = (decimal) reader["totale"];
                    fattura.Numero = (string) reader["numero"];
                    fattura.Data = (DateTime) reader["data"];
                    if (!Convert.IsDBNull(reader["dataAssociazione"]))
                        fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                    if (!Convert.IsDBNull(reader["dataValidazione"]))
                        fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                    if (!Convert.IsDBNull(reader["valida"]))
                        fattura.Valida = (bool) reader["valida"];
                }
            }
            return fattura;
        }

        public Fattura getFatturaPerIdArchidoc(int idArchidoc)
        {
            Fattura fattura;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaGetByIdArchidoc"))
            {
                DatabaseCemi.AddInParameter(comando, "idArchidoc", DbType.Int32, idArchidoc);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    fattura = new Fattura();
                    fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                    if (!Convert.IsDBNull(reader["idArchidoc"]))
                        fattura.IdArchidoc = (string) reader["idArchidoc"];
                    if (!Convert.IsDBNull(reader["idLavoratore"]))
                        fattura.IdLavoratore = (int) reader["idLavoratore"];
                    fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                    fattura.ImportoTotale = (decimal) reader["totale"];
                    fattura.Numero = (string) reader["numero"];
                    fattura.Data = (DateTime) reader["data"];
                    if (!Convert.IsDBNull(reader["dataAssociazione"]))
                        fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                    if (!Convert.IsDBNull(reader["dataValidazione"]))
                        fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                    if (!Convert.IsDBNull(reader["valida"]))
                        fattura.Valida = (bool) reader["valida"];
                }
            }
            return fattura;
        }

        public bool updateFatturaPerIdPrestazioniFattura(Fattura fattura, DbTransaction transaction)
        {
            bool res = false;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32,
                    fattura.IdPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.Date, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@totale", DbType.Currency, fattura.ImportoTotale);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddInParameter(comando, "@valida", DbType.Boolean, fattura.Valida);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, fattura.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idArchidoc", DbType.Int32, fattura.IdArchidoc);
                DatabaseCemi.AddInParameter(comando, "@dataAssociazione", DbType.DateTime, fattura.DataAssociazione);
                DatabaseCemi.AddInParameter(comando, "@dataValidazione", DbType.DateTime, fattura.DataValidazione);
                //databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool fatturaDichiarataAssociaFatturaFisica(int idPrestazioniFattura, int idPrestazioniFatturaDichiarata,
            DbTransaction transaction)
        {
            bool res = false;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateAssociaIdPrestazioniFattura")
            )
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);


                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool fatturaDichiarataDisassociaFatturaFisica(int idPrestazioniFatturaDichiarata,
            DbTransaction transaction)
        {
            bool res = false;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateDisassociaIdPrestazioniFattura"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool insertFattura(Fattura fattura, DbTransaction transaction)
        {
            bool res = false;
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureInsert"))
            {
                //databaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, fattura.IdPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.Date, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@totale", DbType.Currency, fattura.ImportoTotale);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddInParameter(comando, "@valida", DbType.Boolean, fattura.Valida);
                DatabaseCemi.AddInParameter(comando, "@idLavoratore", DbType.Int32, fattura.IdLavoratore);
                DatabaseCemi.AddInParameter(comando, "@idArchidoc", DbType.Int32, fattura.IdArchidoc);
                DatabaseCemi.AddInParameter(comando, "@dataAssociazione", DbType.DateTime, fattura.DataAssociazione);
                DatabaseCemi.AddInParameter(comando, "@dataValidazione", DbType.DateTime, fattura.DataValidazione);
                //databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public FatturaCollection getFatturePerIdFatturaDichiarata(int idPrestazioniFatturaDichiarata)
        {
            FatturaCollection fatture = new FatturaCollection();
            Fattura fattura;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFatturaSelectPerIdFatturaDichiarata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        fattura = new Fattura();
                        fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];
                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            fattura.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                            fattura.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["totale"]))
                            fattura.ImportoTotale = (decimal) reader["totale"];
                        if (!Convert.IsDBNull(reader["numero"]))
                            fattura.Numero = (string) reader["numero"];
                        if (!Convert.IsDBNull(reader["data"]))
                            fattura.Data = (DateTime) reader["data"];
                        if (!Convert.IsDBNull(reader["dataAssociazione"]))
                            fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                        if (!Convert.IsDBNull(reader["dataValidazione"]))
                            fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                        if (!Convert.IsDBNull(reader["valida"]))
                            fattura.Valida = (bool) reader["valida"];
                        if (!Convert.IsDBNull(reader["originale"]))
                            fattura.Originale = (bool) reader["originale"];
                        if (!Convert.IsDBNull(reader["dataScansione"]))
                            fattura.DataScansione = (DateTime) reader["dataScansione"];

                        fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];

                        fatture.Add(fattura);
                    }
                }
            }
            return fatture;
        }

        public FatturaCollection getFattureFisicheAssociabili(DateTime? data, string numero, decimal? importo,
            bool adaptSearch)
        {
            FatturaCollection fatture = new FatturaCollection();
            Fattura fattura;

            using
            (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFatturaSelectNonAssociatePerDataNumeroImporto")
            )
            {
                if (data.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, data.Value);
                if (!string.IsNullOrEmpty(numero))
                    DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, numero);
                if (importo.HasValue)
                    DatabaseCemi.AddInParameter(comando, "@importo", DbType.Decimal, importo.Value);
                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    while (reader.Read())
                    {
                        fattura = new Fattura();

                        fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];

                        if (!Convert.IsDBNull(reader["idArchidoc"]))
                            fattura.IdArchidoc = (string) reader["idArchidoc"];
                        if (!Convert.IsDBNull(reader["idLavoratore"]))
                            fattura.IdLavoratore = (int) reader["idLavoratore"];
                        if (!Convert.IsDBNull(reader["dataInserimentoRecord"]))
                            fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];
                        if (!Convert.IsDBNull(reader["totale"]))
                            fattura.ImportoTotale = (decimal) reader["totale"];
                        if (!Convert.IsDBNull(reader["numero"]))
                            fattura.Numero = (string) reader["numero"];
                        if (!Convert.IsDBNull(reader["data"]))
                            fattura.Data = (DateTime) reader["data"];
                        if (!Convert.IsDBNull(reader["dataAssociazione"]))
                            fattura.DataAssociazione = (DateTime) reader["dataAssociazione"];
                        if (!Convert.IsDBNull(reader["dataValidazione"]))
                            fattura.DataValidazione = (DateTime) reader["dataValidazione"];
                        if (!Convert.IsDBNull(reader["valida"]))
                            fattura.Valida = (bool) reader["valida"];

                        fatture.Add(fattura);
                    }
                }
            }
            return fatture;
        }

        #endregion

        #region FATTURADICHIARATA

        /*
            USP_PrestazioniFattureDichiarateInsert
            USP_PrestazioniFattureDichiarateUpdate
            USP_PrestazioniFattureDichiarateSelectPerIdPrestazioniFatturaDichiarata
        
         */

        public FatturaDichiarata getFatturaDichiarataPerIdPrestazioniFatturaDichiarata(
            int idPrestazioniFatturaDichiarata, DbTransaction transaction)
        {
            FatturaDichiarata fattura;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateSelectPerIdPrestazioniFatturaDichiarata"))
            {
                DatabaseCemi.AddInParameter(comando, "idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    reader.Read();

                    fattura = new FatturaDichiarata();

                    fattura.IdPrestazioniFatturaDichiarata = (int) reader["idPrestazioniFatturaDichiarata"];
                    fattura.IdPrestazioniDomanda = (int) reader["idPrestazioniDomanda"];
                    fattura.IdPrestazioniFattura = (int) reader["idPrestazioniFattura"];

                    fattura.Data = (DateTime) reader["data"];
                    fattura.Numero = (string) reader["numero"];
                    fattura.ImportoTotale = (decimal) reader["importoTotale"];
                    fattura.Saldo = (bool) reader["saldo"];

                    fattura.DataAnnullamento = (DateTime) reader["dataAnnullamento"];
                    fattura.DataInserimentoRecord = (DateTime) reader["dataInserimentoRecord"];
                }
            }
            return fattura;
        }

        public bool UpdateFatturaDichiarataPerIdPrestazioniFatturaDichiarata(FatturaDichiarata fattura,
            DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    fattura.IdPrestazioniFatturaDichiarata);
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32,
                    fattura.IdPrestazioniDomanda);
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32,
                    fattura.IdPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                DatabaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);
                DatabaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);
                //                databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool InsertFatturaDichiarataFULL(FatturaDichiarata fattura, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFattureDichiarateInsert"))
            {
                //databaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32, fattura.IdPrestazioneFatturaDichiarata);
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniDomanda", DbType.Int32,
                    fattura.IdPrestazioniDomanda);
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32,
                    fattura.IdPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@data", DbType.DateTime, fattura.Data);
                DatabaseCemi.AddInParameter(comando, "@numero", DbType.String, fattura.Numero);
                DatabaseCemi.AddInParameter(comando, "@importoTotale", DbType.Decimal, fattura.ImportoTotale);
                DatabaseCemi.AddInParameter(comando, "@saldo", DbType.Boolean, fattura.Saldo);
                DatabaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);
                //                databaseCemi.AddInParameter(comando, "@dataInserimentoRecord", DbType.DateTime, fattura.dataInserimentoRecord);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool AnnullaFatturaDichiarataPerIdFatturaDichiarata(int idPrestazioniFatturaDichiarata,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateAnnullaPerIdPrestazioniFatturaDichiarata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);
                //databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool RipristinaAnnullamentoFatturaDichiarataPerIdFatturaDichiarata(int idPrestazioniFatturaDichiarata,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureDichiarateRipristinaAnnullamentoPerIdPrestazioniFatturaDichiarata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);
                //databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool PrestazioniFatturaAggiornaValidazione(int idPrestazioniFattura, bool valida,
            DbTransaction transaction)
        {
            bool res = false;
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniFattureUpdateValidazionePerIdPrestazioniFattura"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@valida", DbType.Boolean, valida);

                //databaseCemi.AddInParameter(comando, "@dataValidazione", DbType.DateTime, );
                //databaseCemi.AddInParameter(comando, "@dataAnnullamento", DbType.DateTime, fattura.DataAnnullamento);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }
            return res;
        }

        #endregion

        #region ImportiFattura

        /*
        USP_GetFattureImportiPerIdPrestazioniFattura
        USP_PrestazioniImportiFattureUpdate
        USP_PrestazioniImportiFattureGeneraPerIdTipoPrestazione
        USP_PrestazioniImportiFattureInsert
        USP_PrestazioniImportiFattureInsertChecked
         */

        public FatturaImportoCollection getImportiFatturePerIdPrestazioniFattura(int idPrestazioniFattura)
        {
            FatturaImportoCollection importi = new FatturaImportoCollection();
            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_FattureImportiSelectPerIdPrestazioniFattura"))
            {
                DatabaseCemi.AddInParameter(comando, "idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    FatturaImporto importo;
                    while (reader.Read())
                    {
                        importo = new FatturaImporto();

                        importo.IdPrestazioneImportoFattura = (int) reader["idPrestazioneImportoFattura"];
                        importo.IdTipoPrestazione = (string) reader["idTipoPrestazione"];
                        importo.IdTipoPrestazioneImportoFattura = (int) reader["idTipoPrestazioneImportoFattura"];
                        importo.Descrizione = (string) reader["descrizione"];
                        //importo.DescrizioneTipoPrestazione = (string)reader["descrizioneTipoPrestazione"]; ;
                        importo.Valore = (decimal) reader["valore"];

                        importi.Add(importo);
                    }
                }
            }
            return importi;
        }

        public bool UpdateImportoFattura(FatturaImporto importo, DbTransaction transaction)
        {
            bool res = false;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiFattureUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioneImportoFattura", DbType.Int32,
                    importo.IdTipoPrestazioneImportoFattura);
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32,
                    importo.IdPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazioneImportoFattura", DbType.Int32,
                    importo.IdTipoPrestazioneImportoFattura);
                DatabaseCemi.AddInParameter(comando, "@valore", DbType.Decimal, importo.Valore);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool fattureImportiUpdatePerIdPrestazioneImportoFattura(int idPrestazioneImportoFattura, decimal valore,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniImportiFattureUpdateValorePeridPrestazioneImportoFattura"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioneImportoFattura", DbType.Int32,
                    idPrestazioneImportoFattura);
                DatabaseCemi.AddInParameter(comando, "@valore", DbType.Decimal, valore);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        public bool InsertImportoFatturaGeneraRecord(string idTipoPrestazione, int idPrestazioniFattura,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniImportiFattureGeneraPerIdTipoPrestazione"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFattura", DbType.Int32, idPrestazioniFattura);
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, idTipoPrestazione);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }


        public bool ImportoFatturaDeleteRecordPerIdPrestazioniFatturaDichiarata(int idPrestazioniFatturaDichiarata,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand(
                        "dbo.USP_PrestazioniImportiFattureDeletePerIdPrestazioniFatturaDichiarata"))
            {
                DatabaseCemi.AddInParameter(comando, "@idPrestazioniFatturaDichiarata", DbType.Int32,
                    idPrestazioniFatturaDichiarata);

                if (transaction == null)
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando) == 1)
                    {
                        res = true;
                    }
                }
                else
                {
                    if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                    {
                        res = true;
                    }
                }
            }

            return res;
        }

        #endregion

        #region Gestione documenti richiesti

        private bool InsertDocumentoRichiestoConsegnato(Documento documento, int idDomanda,
            DbTransaction transaction)
        {
            bool res = false;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDocumentiConsegnatiInsert"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);
                DatabaseCemi.AddInParameter(comando, "@idTipoPrestazione", DbType.String, documento.IdTipoPrestazione);
                DatabaseCemi.AddInParameter(comando, "@gradoParentela", DbType.String,
                    documento.BeneficiarioPrestazione);
                DatabaseCemi.AddInParameter(comando, "@idTipoDocumento", DbType.Int16,
                    documento.TipoDocumento.IdTipoDocumento);
                DatabaseCemi.AddInParameter(comando, "@consegnato", DbType.Boolean, documento.Consegnato);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        private bool DeleteDocumentoRichiestoConsegnato(int idDomanda, DbTransaction transaction)
        {
            //todo fare un controllo su come si comporta (true messo per test)
            bool res = true;

            using (
                DbCommand comando =
                    DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniDomandeDocumentiConsegnatiDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@idDomanda", DbType.Int32, idDomanda);

                if (DatabaseCemi.ExecuteNonQuery(comando, transaction) == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        #endregion

        #region Metodi per filtri ricerca

        public void InsertFiltroRicerca(int idUtente, DomandaFilter filtro, int? pagina)
        {
            if (filtro == null)
            {
                throw new ArgumentNullException();
            }

            XmlSerializer ser = new XmlSerializer(typeof(DomandaFilter));
            string filtroSerializzato = string.Empty;
            if (filtro != null)
            {
                using (StringWriter sw = new StringWriter())
                {
                    ser.Serialize(sw, filtro);
                    filtroSerializzato = sw.ToString();
                }
            }


            using (DbCommand comando =
                DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFiltriRicercaInsertUpdate"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                if (filtro != null)
                {
                    DatabaseCemi.AddInParameter(comando, "@filtro", DbType.Xml, filtroSerializzato);
                    DatabaseCemi.AddInParameter(comando, "@pagina", DbType.Int32, pagina.Value);
                }

                if (DatabaseCemi.ExecuteNonQuery(comando) != 1)
                {
                    throw new Exception("Errore durante l'inserimento del filtro di ricerca");
                }
            }
        }

        public DomandaFilter GetFiltroRicerca(int idUtente, out int pagina)
        {
            DomandaFilter filtro = null;
            pagina = 0;

            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFiltriRicercaSelect"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);

                using (IDataReader reader = DatabaseCemi.ExecuteReader(comando))
                {
                    if (reader.Read())
                    {
                        #region Indici reader

                        int indiceUserID = reader.GetOrdinal("idUtente");
                        int indiceFiltro = reader.GetOrdinal("filtro");
                        int indicePagina = reader.GetOrdinal("pagina");

                        #endregion

                        if (!reader.IsDBNull(indiceUserID))
                        {
                            string filtroSerializzato = null;

                            if (!reader.IsDBNull(indiceFiltro))
                            {
                                filtroSerializzato = reader.GetString(indiceFiltro);
                                pagina = reader.GetInt32(indicePagina);
                            }
                            if (!string.IsNullOrEmpty(filtroSerializzato))
                            {
                                XmlSerializer ser = new XmlSerializer(typeof(DomandaFilter));
                                using (StringReader sr = new StringReader(filtroSerializzato))
                                {
                                    filtro = (DomandaFilter) ser.Deserialize(sr);
                                }
                            }
                        }
                    }
                }
            }

            return filtro;
        }

        public void DeleteFiltroRicerca(int idUtente)
        {
            using (DbCommand comando = DatabaseCemi.GetStoredProcCommand("dbo.USP_PrestazioniFiltriRicercaDelete"))
            {
                DatabaseCemi.AddInParameter(comando, "@userID", DbType.Int32, idUtente);
                DatabaseCemi.ExecuteNonQuery(comando);
            }
        }

        #endregion
    }
}