using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Filters
{
    public class DichiarazioneFilter
    {
        public DateTime? DataInvioDa { get; set; }

        public DateTime? DataInvioA { get; set; }

        public TipoAttivita? TipoAttivita { get; set; }

        public string CodiceImpresa { get; set; }

        public string PartitaIVA { get; set; }

        public string Cognome { get; set; }

        public string CodiceFiscale { get; set; }

        public TipoStatoGestionePratica? StatoGestionePratica { get; set; }

        public int? IdDichiarazione { get; set; }

        public string IdNazionalita { set; get; }

        public bool DuplicazioneCfUgualeDatiDiversi { set; get; }

        public bool DuplicazioneCfUgualeDatiUguali { set; get; }

        public bool DuplicazioneCfDiversoDatiUguali { set; get; }

        public bool DuplicazioneCfUgualeRapportoAperto { set; get; }
    }
}