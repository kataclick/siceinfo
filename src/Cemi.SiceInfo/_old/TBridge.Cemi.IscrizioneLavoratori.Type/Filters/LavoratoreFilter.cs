namespace TBridge.Cemi.IscrizioneLavoratori.Type.Filters
{
    public class LavoratoreFilter
    {
        public int? Codice { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }
    }
}