using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class Dichiarazione
    {
        public int? IdDichiarazione { get; set; }

        public int? IdDichiarazionePrecedente { get; set; }

        public string IdComunicazionePrecedente { get; set; }

        public int? IdDichiarazioneSuccessiva { get; set; }

        public string IdComunicazioneSuccessiva { get; set; }

        public int? IdConsulente { get; set; }

        public int IdImpresa { get; set; }

        public Impresa Impresa { get; set; }

        public Consulente Consulente { get; set; }

        public TipoAttivita Attivita { get; set; }

        public Indirizzo Cantiere { get; set; }

        public int IdCantiere { get; set; }

        public Lavoratore Lavoratore { get; set; }

        public RapportoDiLavoro RapportoDiLavoro { get; set; }

        public DateTime DataInvio { get; set; }

        public TipoStatoGestionePratica Stato { get; set; }

        public bool NuovoLavoratore { get; set; }

        public int? IdLavoratoreSelezionato { get; set; }

        public bool? MantieniIbanAnagrafica { get; set; }

        public bool? MantieniIndirizzoAnagrafica { get; set; }

        public bool ControlloSdoppione { get; set; }

        public bool RapportoAssociatoCessazione { get; set; }

        public int? IdLavoratoreRapportoSelezionatoCessazione { get; set; }

        public int? IdImpresaRapportoSelezionatoCessazione { get; set; }

        public DateTime? DataFineRapportoSelezionatoCessazione { get; set; }

        public int IdUtente { get; set; }

        public string CellulareSMSSiceInfo { get; set; }

        public DateTime? DataUltimoCambioStato { get; set; }

        public int? UltimoCambioStatoIdUtente { get; set; }

        public bool IsRettifica =>
            !string.IsNullOrEmpty(IdComunicazionePrecedente) || IdDichiarazionePrecedente.HasValue;

        public bool IsRettificata =>
            !string.IsNullOrEmpty(IdComunicazioneSuccessiva) || IdDichiarazioneSuccessiva.HasValue;
    }
}