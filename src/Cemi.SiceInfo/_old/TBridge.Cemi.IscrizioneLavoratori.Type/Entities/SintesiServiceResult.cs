﻿using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class SintesiServiceResult
    {
        public SintesiServiceErrors Error { set; get; }

        public string Message { set; get; }
    }
}