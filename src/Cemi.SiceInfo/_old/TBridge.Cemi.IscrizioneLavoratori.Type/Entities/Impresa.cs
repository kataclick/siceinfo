using System;
using TBridge.Cemi.Type.Entities;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    [Serializable]
    public class Impresa
    {
        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string PartitaIva { get; set; }

        public string CodiceFiscale { get; set; }

        public string CodiceContratto => Contratto != null ? Contratto.IdContratto : null;

        public string SedeLegaleIndirizzo { get; set; }

        public string SedeLegaleComune { get; set; }

        public string SedeLegaleProvincia { get; set; }

        public string SedeLegaleCap { get; set; }

        public string SedeLegaleTelefono { get; set; }

        public string SedeLegaleFax { get; set; }

        public string SedeLegaleEmail { get; set; }

        public string SedeLegale => string.Format("{0} {1} ({2}) {3}",
            SedeLegaleIndirizzo,
            SedeLegaleComune,
            SedeLegaleProvincia,
            SedeLegaleCap);

        public string SedeAmministrativaIndirizzo { get; set; }

        public string SedeAmministrativaComune { get; set; }

        public string SedeAmministrativaProvincia { get; set; }

        public string SedeAmministrativaCap { get; set; }

        public string SedeAmministrativaTelefono { get; set; }

        public string SedeAmministrativaFax { get; set; }

        public string SedeAmministrativaEmail { get; set; }

        public string SedeAmministrativa => string.Format("{0} {1} ({2}) {3}",
            SedeAmministrativaIndirizzo,
            SedeAmministrativaComune,
            SedeAmministrativaProvincia,
            SedeAmministrativaCap);

        public TipoContratto Contratto { get; set; }

        public DateTime? DataUltimaDenuncia { set; get; }

        public string Stato { set; get; }
    }
}