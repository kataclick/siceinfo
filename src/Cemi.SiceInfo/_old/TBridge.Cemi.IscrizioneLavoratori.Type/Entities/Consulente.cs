namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class Consulente
    {
        public int IdConsulente { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscale { get; set; }

        public string Telefono { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }
    }
}