using System;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Entities
{
    public class Lavoratore : LavoratoreAnagraficaComune
    {
        public Lavoratore()
        {
            Fonte = FonteAnagraficheComuni.IscrizioneLavoratori;
        }

        #region anagrafica

        public string IdStatoCivile { get; set; }

        public string Cittadinanza { get; set; }

        public string LivelloIstruzione { get; set; }

        public string IdLingua { get; set; }

        public DateTime? DataDecesso { get; set; }

        public string IdTipoDecesso { get; set; }

        public bool Italiano { get; set; }

        public string NazioneNascita { get; set; }

        public TipologiaLavoratore TipoLavoratore { get; set; }

        //foto

        #endregion anagrafica

        #region Documenti

        public string TipoDocumento { get; set; }

        public string NumeroDocumento { get; set; }

        public string MotivoPermesso { get; set; }

        public DateTime? DataRichiestaPermesso { get; set; }

        public DateTime? DataScadenzaPermesso { get; set; }

        #endregion documenti

        #region indirizzo

        public Indirizzo Indirizzo { get; set; }

        public string NumeroTelefono { get; set; }

        public string NumeroTelefonoCellulare { get; set; }

        public bool AderisceServizioSMS { get; set; }

        public string Email { get; set; }

        public string IndirizzoCompleto
        {
            get
            {
                if (Indirizzo == null)
                {
                    return string.Empty;
                }
                return Indirizzo.IndirizzoCompleto;
            }
        }

        #endregion indirizzo

        #region info aggiuntive

        public string IBAN { get; set; }

        public bool CoIntestato { get; set; }

        public string CognomeCointestatario { get; set; }

        public bool RichiestaInfoCartaPrepagata { get; set; }

        public TipoCartaPrepagata TipoPrepagata { get; set; }

        public int? IdTagliaFelpaHusky { get; set; }

        public int? IdTagliaPantaloni { get; set; }

        public int? IdTagliaScarpe { get; set; }

        public string TagliaFelpaHusky { get; set; }

        public string TagliaPantaloni { get; set; }

        public string TagliaScarpe { get; set; }

        public bool RichiestaIscrizioneCorsi16Ore { get; set; }

        public string Note { get; set; }

        public string IdSindacato { get; set; }

        public DateTime? DataAdesione { get; set; }

        public DateTime? DataDisdetta { get; set; }

        public string NumeroDelega { get; set; }

        public string LinguaComunicazione { get; set; }

        public bool? PosizioneValida { get; set; }

        #endregion info aggiuntive
    }
}