namespace TBridge.Cemi.IscrizioneLavoratori.Type.Enums
{
    public enum TipoAttivita
    {
        Assunzione = 1,
        Cessazione,
        Proroga,
        Trasformazione
    }
}