﻿namespace TBridge.Cemi.IscrizioneLavoratori.Type.Enums
{
    public enum SintesiServiceErrors
    {
        NoError,
        InvalidId,
        ServiceCallFailed,
        InvalidResponse,
        ServiceErrorMessage
    }
}