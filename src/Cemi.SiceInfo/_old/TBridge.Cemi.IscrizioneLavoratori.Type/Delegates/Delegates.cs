using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace TBridge.Cemi.IscrizioneLavoratori.Type.Delegates
{
    public delegate void TipoAttivitaSelectedEventHandler(TipoAttivita tipoAttivita);

    public delegate void LavoratoreSelectedEventHandler(int idLavoratore);

    public delegate void LavoratoreCompletoSelectedEventHandler(Lavoratore lavoratore);

    public delegate void DichiarazioneSelectedEventHandler(int idLavoratore);
}