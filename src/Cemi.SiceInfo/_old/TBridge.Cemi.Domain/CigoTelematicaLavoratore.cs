//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class CigoTelematicaLavoratore
    {
        #region Primitive Properties
    
        public virtual int IdLavoratore
        {
            get { return _idLavoratore; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idLavoratore != value)
                    {
                        if (Lavoratore != null && Lavoratore.Id != value)
                        {
                            Lavoratore = null;
                        }
                        _idLavoratore = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idLavoratore;
    
        public virtual int IdDomandaCigo
        {
            get { return _idDomandaCigo; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idDomandaCigo != value)
                    {
                        if (CigoTelematicaDomanda != null && CigoTelematicaDomanda.Id != value)
                        {
                            CigoTelematicaDomanda = null;
                        }
                        _idDomandaCigo = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idDomandaCigo;
    
        public virtual string IdStato
        {
            get { return _idStato; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idStato != value)
                    {
                        if (CigoTelematicaStatoLavoratore != null && CigoTelematicaStatoLavoratore.Id != value)
                        {
                            CigoTelematicaStatoLavoratore = null;
                        }
                        _idStato = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private string _idStato;
    
        public virtual Nullable<int> OreRichieste
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> PagaOrariaDichiarata
        {
            get;
            set;
        }
    
        public virtual byte[] Immagine
        {
            get;
            set;
        }
    
        public virtual string NomeFile
        {
            get;
            set;
        }
    
        public virtual string IdArchidoc
        {
            get;
            set;
        }
    
        public virtual Nullable<int> IdUtenteInserimento
        {
            get { return _idUtenteInserimento; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idUtenteInserimento != value)
                    {
                        if (Utente != null && Utente.Id != value)
                        {
                            Utente = null;
                        }
                        _idUtenteInserimento = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _idUtenteInserimento;
    
        public virtual Nullable<System.DateTime> DataInserimentoRecord
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataUltimoAggiornamento
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataGestione
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataInAttesa
        {
            get;
            set;
        }
    
        public virtual string IdTipoOreDenuncia
        {
            get { return _idTipoOreDenuncia; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idTipoOreDenuncia != value)
                    {
                        if (TipiOra != null && TipiOra.IdTipoOra != value)
                        {
                            TipiOra = null;
                        }
                        _idTipoOreDenuncia = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private string _idTipoOreDenuncia;
    
        public virtual Nullable<int> OreRiconosciute
        {
            get;
            set;
        }
    
        public virtual string idCassaEdile
        {
            get;
            set;
        }
    
        public virtual string tipoProtocollo
        {
            get;
            set;
        }
    
        public virtual Nullable<int> numeroProtocollo
        {
            get;
            set;
        }
    
        public virtual Nullable<int> annoProtocollo
        {
            get;
            set;
        }
    
        public virtual string idSegnaleErrore
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual CigoTelematicaDomanda CigoTelematicaDomanda
        {
            get { return _cigoTelematicaDomanda; }
            set
            {
                if (!ReferenceEquals(_cigoTelematicaDomanda, value))
                {
                    var previousValue = _cigoTelematicaDomanda;
                    _cigoTelematicaDomanda = value;
                    FixupCigoTelematicaDomanda(previousValue);
                }
            }
        }
        private CigoTelematicaDomanda _cigoTelematicaDomanda;
    
        public virtual CigoTelematicaStatoLavoratore CigoTelematicaStatoLavoratore
        {
            get { return _cigoTelematicaStatoLavoratore; }
            set
            {
                if (!ReferenceEquals(_cigoTelematicaStatoLavoratore, value))
                {
                    var previousValue = _cigoTelematicaStatoLavoratore;
                    _cigoTelematicaStatoLavoratore = value;
                    FixupCigoTelematicaStatoLavoratore(previousValue);
                }
            }
        }
        private CigoTelematicaStatoLavoratore _cigoTelematicaStatoLavoratore;
    
        public virtual Lavoratore Lavoratore
        {
            get { return _lavoratore; }
            set
            {
                if (!ReferenceEquals(_lavoratore, value))
                {
                    var previousValue = _lavoratore;
                    _lavoratore = value;
                    FixupLavoratore(previousValue);
                }
            }
        }
        private Lavoratore _lavoratore;
    
        public virtual Utente Utente
        {
            get { return _utente; }
            set
            {
                if (!ReferenceEquals(_utente, value))
                {
                    var previousValue = _utente;
                    _utente = value;
                    FixupUtente(previousValue);
                }
            }
        }
        private Utente _utente;
    
        public virtual ICollection<CigoTelematicaLavoratoreGiorno> CigoTelematicaLavoratoriGiorni
        {
            get
            {
                if (_cigoTelematicaLavoratoriGiorni == null)
                {
                    var newCollection = new FixupCollection<CigoTelematicaLavoratoreGiorno>();
                    newCollection.CollectionChanged += FixupCigoTelematicaLavoratoriGiorni;
                    _cigoTelematicaLavoratoriGiorni = newCollection;
                }
                return _cigoTelematicaLavoratoriGiorni;
            }
            set
            {
                if (!ReferenceEquals(_cigoTelematicaLavoratoriGiorni, value))
                {
                    var previousValue = _cigoTelematicaLavoratoriGiorni as FixupCollection<CigoTelematicaLavoratoreGiorno>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCigoTelematicaLavoratoriGiorni;
                    }
                    _cigoTelematicaLavoratoriGiorni = value;
                    var newValue = value as FixupCollection<CigoTelematicaLavoratoreGiorno>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCigoTelematicaLavoratoriGiorni;
                    }
                }
            }
        }
        private ICollection<CigoTelematicaLavoratoreGiorno> _cigoTelematicaLavoratoriGiorni;
    
        public virtual ICollection<CigoTelematicaBustaPaga> CigoTelematicaBustePaga
        {
            get
            {
                if (_cigoTelematicaBustePaga == null)
                {
                    var newCollection = new FixupCollection<CigoTelematicaBustaPaga>();
                    newCollection.CollectionChanged += FixupCigoTelematicaBustePaga;
                    _cigoTelematicaBustePaga = newCollection;
                }
                return _cigoTelematicaBustePaga;
            }
            set
            {
                if (!ReferenceEquals(_cigoTelematicaBustePaga, value))
                {
                    var previousValue = _cigoTelematicaBustePaga as FixupCollection<CigoTelematicaBustaPaga>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCigoTelematicaBustePaga;
                    }
                    _cigoTelematicaBustePaga = value;
                    var newValue = value as FixupCollection<CigoTelematicaBustaPaga>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCigoTelematicaBustePaga;
                    }
                }
            }
        }
        private ICollection<CigoTelematicaBustaPaga> _cigoTelematicaBustePaga;
    
        public virtual TipiOra TipiOra
        {
            get { return _tipiOra; }
            set
            {
                if (!ReferenceEquals(_tipiOra, value))
                {
                    var previousValue = _tipiOra;
                    _tipiOra = value;
                    FixupTipiOra(previousValue);
                }
            }
        }
        private TipiOra _tipiOra;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupCigoTelematicaDomanda(CigoTelematicaDomanda previousValue)
        {
            if (previousValue != null && previousValue.CigoTelematicaLavoratori.Contains(this))
            {
                previousValue.CigoTelematicaLavoratori.Remove(this);
            }
    
            if (CigoTelematicaDomanda != null)
            {
                if (!CigoTelematicaDomanda.CigoTelematicaLavoratori.Contains(this))
                {
                    CigoTelematicaDomanda.CigoTelematicaLavoratori.Add(this);
                }
                if (IdDomandaCigo != CigoTelematicaDomanda.Id)
                {
                    IdDomandaCigo = CigoTelematicaDomanda.Id;
                }
            }
        }
    
        private void FixupCigoTelematicaStatoLavoratore(CigoTelematicaStatoLavoratore previousValue)
        {
            if (previousValue != null && previousValue.CigoTelematicaLavoratori.Contains(this))
            {
                previousValue.CigoTelematicaLavoratori.Remove(this);
            }
    
            if (CigoTelematicaStatoLavoratore != null)
            {
                if (!CigoTelematicaStatoLavoratore.CigoTelematicaLavoratori.Contains(this))
                {
                    CigoTelematicaStatoLavoratore.CigoTelematicaLavoratori.Add(this);
                }
                if (IdStato != CigoTelematicaStatoLavoratore.Id)
                {
                    IdStato = CigoTelematicaStatoLavoratore.Id;
                }
            }
        }
    
        private void FixupLavoratore(Lavoratore previousValue)
        {
            if (previousValue != null && previousValue.CigoTelematicaLavoratori.Contains(this))
            {
                previousValue.CigoTelematicaLavoratori.Remove(this);
            }
    
            if (Lavoratore != null)
            {
                if (!Lavoratore.CigoTelematicaLavoratori.Contains(this))
                {
                    Lavoratore.CigoTelematicaLavoratori.Add(this);
                }
                if (IdLavoratore != Lavoratore.Id)
                {
                    IdLavoratore = Lavoratore.Id;
                }
            }
        }
    
        private void FixupUtente(Utente previousValue)
        {
            if (Utente != null)
            {
                if (IdUtenteInserimento != Utente.Id)
                {
                    IdUtenteInserimento = Utente.Id;
                }
            }
            else if (!_settingFK)
            {
                IdUtenteInserimento = null;
            }
        }
    
        private void FixupTipiOra(TipiOra previousValue)
        {
            if (previousValue != null && previousValue.CigoTelematicaLavoratori.Contains(this))
            {
                previousValue.CigoTelematicaLavoratori.Remove(this);
            }
    
            if (TipiOra != null)
            {
                if (!TipiOra.CigoTelematicaLavoratori.Contains(this))
                {
                    TipiOra.CigoTelematicaLavoratori.Add(this);
                }
                if (IdTipoOreDenuncia != TipiOra.IdTipoOra)
                {
                    IdTipoOreDenuncia = TipiOra.IdTipoOra;
                }
            }
            else if (!_settingFK)
            {
                IdTipoOreDenuncia = null;
            }
        }
    
        private void FixupCigoTelematicaLavoratoriGiorni(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CigoTelematicaLavoratoreGiorno item in e.NewItems)
                {
                    item.CigoTelematicaLavoratore = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CigoTelematicaLavoratoreGiorno item in e.OldItems)
                {
                    if (ReferenceEquals(item.CigoTelematicaLavoratore, this))
                    {
                        item.CigoTelematicaLavoratore = null;
                    }
                }
            }
        }
    
        private void FixupCigoTelematicaBustePaga(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CigoTelematicaBustaPaga item in e.NewItems)
                {
                    item.CigoTelematicaLavoratori = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CigoTelematicaBustaPaga item in e.OldItems)
                {
                    if (ReferenceEquals(item.CigoTelematicaLavoratori, this))
                    {
                        item.CigoTelematicaLavoratori = null;
                    }
                }
            }
        }

        #endregion

    }
}
