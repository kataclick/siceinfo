//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class CantieriSegnalazione
    {
        #region Primitive Properties
    
        public virtual int IdCantieriSegnalazione
        {
            get;
            set;
        }
    
        public virtual System.DateTime Data
        {
            get;
            set;
        }
    
        public virtual bool Ricorrente
        {
            get;
            set;
        }
    
        public virtual int IdCantieriSegnalazioneMotivazione
        {
            get { return _idCantieriSegnalazioneMotivazione; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idCantieriSegnalazioneMotivazione != value)
                    {
                        if (CantieriSegnalazioneMotivazione != null && CantieriSegnalazioneMotivazione.Id != value)
                        {
                            CantieriSegnalazioneMotivazione = null;
                        }
                        _idCantieriSegnalazioneMotivazione = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idCantieriSegnalazioneMotivazione;
    
        public virtual int IdCantieriSegnalazionePriorita
        {
            get { return _idCantieriSegnalazionePriorita; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idCantieriSegnalazionePriorita != value)
                    {
                        if (CantieriSegnalazionePriorita != null && CantieriSegnalazionePriorita.Id != value)
                        {
                            CantieriSegnalazionePriorita = null;
                        }
                        _idCantieriSegnalazionePriorita = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idCantieriSegnalazionePriorita;
    
        public virtual int IdUtente
        {
            get { return _idUtente; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idUtente != value)
                    {
                        if (Utente != null && Utente.Id != value)
                        {
                            Utente = null;
                        }
                        _idUtente = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private int _idUtente;
    
        public virtual Nullable<int> IdSegnalazioneSuccessiva
        {
            get { return _idSegnalazioneSuccessiva; }
            set
            {
                try
                {
                    _settingFK = true;
                    if (_idSegnalazioneSuccessiva != value)
                    {
                        if (SegnalazioneSuccessiva != null && SegnalazioneSuccessiva.IdCantieriSegnalazione != value)
                        {
                            SegnalazioneSuccessiva = null;
                        }
                        _idSegnalazioneSuccessiva = value;
                    }
                }
                finally
                {
                    _settingFK = false;
                }
            }
        }
        private Nullable<int> _idSegnalazioneSuccessiva;
    
        public virtual string Note
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual CantiereSegnalazioneMotivazione CantieriSegnalazioneMotivazione
        {
            get { return _cantieriSegnalazioneMotivazione; }
            set
            {
                if (!ReferenceEquals(_cantieriSegnalazioneMotivazione, value))
                {
                    var previousValue = _cantieriSegnalazioneMotivazione;
                    _cantieriSegnalazioneMotivazione = value;
                    FixupCantieriSegnalazioneMotivazione(previousValue);
                }
            }
        }
        private CantiereSegnalazioneMotivazione _cantieriSegnalazioneMotivazione;
    
        public virtual CantiereSegnalazionePriorita CantieriSegnalazionePriorita
        {
            get { return _cantieriSegnalazionePriorita; }
            set
            {
                if (!ReferenceEquals(_cantieriSegnalazionePriorita, value))
                {
                    var previousValue = _cantieriSegnalazionePriorita;
                    _cantieriSegnalazionePriorita = value;
                    FixupCantieriSegnalazionePriorita(previousValue);
                }
            }
        }
        private CantiereSegnalazionePriorita _cantieriSegnalazionePriorita;
    
        public virtual Utente Utente
        {
            get { return _utente; }
            set
            {
                if (!ReferenceEquals(_utente, value))
                {
                    var previousValue = _utente;
                    _utente = value;
                    FixupUtente(previousValue);
                }
            }
        }
        private Utente _utente;
    
        public virtual ICollection<Cantiere> Cantieri
        {
            get
            {
                if (_cantieri == null)
                {
                    var newCollection = new FixupCollection<Cantiere>();
                    newCollection.CollectionChanged += FixupCantieri;
                    _cantieri = newCollection;
                }
                return _cantieri;
            }
            set
            {
                if (!ReferenceEquals(_cantieri, value))
                {
                    var previousValue = _cantieri as FixupCollection<Cantiere>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCantieri;
                    }
                    _cantieri = value;
                    var newValue = value as FixupCollection<Cantiere>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCantieri;
                    }
                }
            }
        }
        private ICollection<Cantiere> _cantieri;
    
        public virtual ICollection<CantieriSegnalazione> SegnalazioniPrecedenti
        {
            get
            {
                if (_segnalazioniPrecedenti == null)
                {
                    var newCollection = new FixupCollection<CantieriSegnalazione>();
                    newCollection.CollectionChanged += FixupSegnalazioniPrecedenti;
                    _segnalazioniPrecedenti = newCollection;
                }
                return _segnalazioniPrecedenti;
            }
            set
            {
                if (!ReferenceEquals(_segnalazioniPrecedenti, value))
                {
                    var previousValue = _segnalazioniPrecedenti as FixupCollection<CantieriSegnalazione>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupSegnalazioniPrecedenti;
                    }
                    _segnalazioniPrecedenti = value;
                    var newValue = value as FixupCollection<CantieriSegnalazione>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupSegnalazioniPrecedenti;
                    }
                }
            }
        }
        private ICollection<CantieriSegnalazione> _segnalazioniPrecedenti;
    
        public virtual CantieriSegnalazione SegnalazioneSuccessiva
        {
            get { return _segnalazioneSuccessiva; }
            set
            {
                if (!ReferenceEquals(_segnalazioneSuccessiva, value))
                {
                    var previousValue = _segnalazioneSuccessiva;
                    _segnalazioneSuccessiva = value;
                    FixupSegnalazioneSuccessiva(previousValue);
                }
            }
        }
        private CantieriSegnalazione _segnalazioneSuccessiva;

        #endregion

        #region Association Fixup
    
        private bool _settingFK = false;
    
        private void FixupCantieriSegnalazioneMotivazione(CantiereSegnalazioneMotivazione previousValue)
        {
            if (previousValue != null && previousValue.CantieriSegnalazioni.Contains(this))
            {
                previousValue.CantieriSegnalazioni.Remove(this);
            }
    
            if (CantieriSegnalazioneMotivazione != null)
            {
                if (!CantieriSegnalazioneMotivazione.CantieriSegnalazioni.Contains(this))
                {
                    CantieriSegnalazioneMotivazione.CantieriSegnalazioni.Add(this);
                }
                if (IdCantieriSegnalazioneMotivazione != CantieriSegnalazioneMotivazione.Id)
                {
                    IdCantieriSegnalazioneMotivazione = CantieriSegnalazioneMotivazione.Id;
                }
            }
        }
    
        private void FixupCantieriSegnalazionePriorita(CantiereSegnalazionePriorita previousValue)
        {
            if (previousValue != null && previousValue.CantieriSegnalazioni.Contains(this))
            {
                previousValue.CantieriSegnalazioni.Remove(this);
            }
    
            if (CantieriSegnalazionePriorita != null)
            {
                if (!CantieriSegnalazionePriorita.CantieriSegnalazioni.Contains(this))
                {
                    CantieriSegnalazionePriorita.CantieriSegnalazioni.Add(this);
                }
                if (IdCantieriSegnalazionePriorita != CantieriSegnalazionePriorita.Id)
                {
                    IdCantieriSegnalazionePriorita = CantieriSegnalazionePriorita.Id;
                }
            }
        }
    
        private void FixupUtente(Utente previousValue)
        {
            if (previousValue != null && previousValue.CantieriSegnalazioni.Contains(this))
            {
                previousValue.CantieriSegnalazioni.Remove(this);
            }
    
            if (Utente != null)
            {
                if (!Utente.CantieriSegnalazioni.Contains(this))
                {
                    Utente.CantieriSegnalazioni.Add(this);
                }
                if (IdUtente != Utente.Id)
                {
                    IdUtente = Utente.Id;
                }
            }
        }
    
        private void FixupSegnalazioneSuccessiva(CantieriSegnalazione previousValue)
        {
            if (previousValue != null && previousValue.SegnalazioniPrecedenti.Contains(this))
            {
                previousValue.SegnalazioniPrecedenti.Remove(this);
            }
    
            if (SegnalazioneSuccessiva != null)
            {
                if (!SegnalazioneSuccessiva.SegnalazioniPrecedenti.Contains(this))
                {
                    SegnalazioneSuccessiva.SegnalazioniPrecedenti.Add(this);
                }
                if (IdSegnalazioneSuccessiva != SegnalazioneSuccessiva.IdCantieriSegnalazione)
                {
                    IdSegnalazioneSuccessiva = SegnalazioneSuccessiva.IdCantieriSegnalazione;
                }
            }
            else if (!_settingFK)
            {
                IdSegnalazioneSuccessiva = null;
            }
        }
    
        private void FixupCantieri(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Cantiere item in e.NewItems)
                {
                    item.CantieriSegnalazione = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Cantiere item in e.OldItems)
                {
                    if (ReferenceEquals(item.CantieriSegnalazione, this))
                    {
                        item.CantieriSegnalazione = null;
                    }
                }
            }
        }
    
        private void FixupSegnalazioniPrecedenti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CantieriSegnalazione item in e.NewItems)
                {
                    item.SegnalazioneSuccessiva = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CantieriSegnalazione item in e.OldItems)
                {
                    if (ReferenceEquals(item.SegnalazioneSuccessiva, this))
                    {
                        item.SegnalazioneSuccessiva = null;
                    }
                }
            }
        }

        #endregion

    }
}
