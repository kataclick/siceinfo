//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class TipoCategoria
    {
        #region Primitive Properties
    
        public virtual string Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    if (TipiCategoria2 != null && TipiCategoria2.Id != value)
                    {
                        TipiCategoria2 = null;
                    }
                    _id = value;
                }
            }
        }
        private string _id;
    
        public virtual string Descrizione
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> dataInserimentoRecord
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> dataModificaRecord
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<RapportoImpresaPersona> RapportiImpresaPersona
        {
            get
            {
                if (_rapportiImpresaPersona == null)
                {
                    var newCollection = new FixupCollection<RapportoImpresaPersona>();
                    newCollection.CollectionChanged += FixupRapportiImpresaPersona;
                    _rapportiImpresaPersona = newCollection;
                }
                return _rapportiImpresaPersona;
            }
            set
            {
                if (!ReferenceEquals(_rapportiImpresaPersona, value))
                {
                    var previousValue = _rapportiImpresaPersona as FixupCollection<RapportoImpresaPersona>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupRapportiImpresaPersona;
                    }
                    _rapportiImpresaPersona = value;
                    var newValue = value as FixupCollection<RapportoImpresaPersona>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupRapportiImpresaPersona;
                    }
                }
            }
        }
        private ICollection<RapportoImpresaPersona> _rapportiImpresaPersona;
    
        public virtual ICollection<RapportoLavoratoreImpresaCompleto> RapportiLavoratoreImpresaCompleti
        {
            get
            {
                if (_rapportiLavoratoreImpresaCompleti == null)
                {
                    var newCollection = new FixupCollection<RapportoLavoratoreImpresaCompleto>();
                    newCollection.CollectionChanged += FixupRapportiLavoratoreImpresaCompleti;
                    _rapportiLavoratoreImpresaCompleti = newCollection;
                }
                return _rapportiLavoratoreImpresaCompleti;
            }
            set
            {
                if (!ReferenceEquals(_rapportiLavoratoreImpresaCompleti, value))
                {
                    var previousValue = _rapportiLavoratoreImpresaCompleti as FixupCollection<RapportoLavoratoreImpresaCompleto>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupRapportiLavoratoreImpresaCompleti;
                    }
                    _rapportiLavoratoreImpresaCompleti = value;
                    var newValue = value as FixupCollection<RapportoLavoratoreImpresaCompleto>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupRapportiLavoratoreImpresaCompleti;
                    }
                }
            }
        }
        private ICollection<RapportoLavoratoreImpresaCompleto> _rapportiLavoratoreImpresaCompleti;
    
        public virtual TipoCategoria TipiCategoria1
        {
            get { return _tipiCategoria1; }
            set
            {
                if (!ReferenceEquals(_tipiCategoria1, value))
                {
                    var previousValue = _tipiCategoria1;
                    _tipiCategoria1 = value;
                    FixupTipiCategoria1(previousValue);
                }
            }
        }
        private TipoCategoria _tipiCategoria1;
    
        public virtual TipoCategoria TipiCategoria2
        {
            get { return _tipiCategoria2; }
            set
            {
                if (!ReferenceEquals(_tipiCategoria2, value))
                {
                    var previousValue = _tipiCategoria2;
                    _tipiCategoria2 = value;
                    FixupTipiCategoria2(previousValue);
                }
            }
        }
        private TipoCategoria _tipiCategoria2;

        #endregion

        #region Association Fixup
    
        private void FixupTipiCategoria1(TipoCategoria previousValue)
        {
            if (previousValue != null && ReferenceEquals(previousValue.TipiCategoria2, this))
            {
                previousValue.TipiCategoria2 = null;
            }
    
            if (TipiCategoria1 != null)
            {
                TipiCategoria1.TipiCategoria2 = this;
            }
        }
    
        private void FixupTipiCategoria2(TipoCategoria previousValue)
        {
            if (previousValue != null && ReferenceEquals(previousValue.TipiCategoria1, this))
            {
                previousValue.TipiCategoria1 = null;
            }
    
            if (TipiCategoria2 != null)
            {
                TipiCategoria2.TipiCategoria1 = this;
                if (Id != TipiCategoria2.Id)
                {
                    Id = TipiCategoria2.Id;
                }
            }
        }
    
        private void FixupRapportiImpresaPersona(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (RapportoImpresaPersona item in e.NewItems)
                {
                    item.TipoCategoria = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (RapportoImpresaPersona item in e.OldItems)
                {
                    if (ReferenceEquals(item.TipoCategoria, this))
                    {
                        item.TipoCategoria = null;
                    }
                }
            }
        }
    
        private void FixupRapportiLavoratoreImpresaCompleti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (RapportoLavoratoreImpresaCompleto item in e.NewItems)
                {
                    item.TipiCategoria = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (RapportoLavoratoreImpresaCompleto item in e.OldItems)
                {
                    if (ReferenceEquals(item.TipiCategoria, this))
                    {
                        item.TipiCategoria = null;
                    }
                }
            }
        }

        #endregion

    }
}
