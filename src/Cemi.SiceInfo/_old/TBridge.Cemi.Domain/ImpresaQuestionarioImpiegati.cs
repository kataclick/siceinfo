//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class ImpresaQuestionarioImpiegati
    {
        #region Primitive Properties
    
        public virtual int IdImpresa
        {
            get { return _idImpresa; }
            set
            {
                if (_idImpresa != value)
                {
                    if (Impresa != null && Impresa.Id != value)
                    {
                        Impresa = null;
                    }
                    _idImpresa = value;
                }
            }
        }
        private int _idImpresa;
    
        public virtual short ImpiegatiForzaAmministrativi
        {
            get;
            set;
        }
    
        public virtual short ImpiegatiForzaTecnici
        {
            get;
            set;
        }
    
        public virtual short ImpiegatiIndeterminatiParziali
        {
            get;
            set;
        }
    
        public virtual short ImpiegatiIndeterminatiPieni
        {
            get;
            set;
        }
    
        public virtual short ImpiegatiDeterminatiParziali
        {
            get;
            set;
        }
    
        public virtual short ImpiegatiDeterminatiPieni
        {
            get;
            set;
        }
    
        public virtual int OreAssenzaMalattia
        {
            get;
            set;
        }
    
        public virtual int OreAssenzaCassa
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataInserimentoRecord
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataModificaRecord
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual Impresa Impresa
        {
            get { return _impresa; }
            set
            {
                if (!ReferenceEquals(_impresa, value))
                {
                    var previousValue = _impresa;
                    _impresa = value;
                    FixupImpresa(previousValue);
                }
            }
        }
        private Impresa _impresa;
    
        public virtual ICollection<ImpresaQuestionarioImpiegatiDeterminatiScadenza> Scadenze
        {
            get
            {
                if (_scadenze == null)
                {
                    var newCollection = new FixupCollection<ImpresaQuestionarioImpiegatiDeterminatiScadenza>();
                    newCollection.CollectionChanged += FixupScadenze;
                    _scadenze = newCollection;
                }
                return _scadenze;
            }
            set
            {
                if (!ReferenceEquals(_scadenze, value))
                {
                    var previousValue = _scadenze as FixupCollection<ImpresaQuestionarioImpiegatiDeterminatiScadenza>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupScadenze;
                    }
                    _scadenze = value;
                    var newValue = value as FixupCollection<ImpresaQuestionarioImpiegatiDeterminatiScadenza>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupScadenze;
                    }
                }
            }
        }
        private ICollection<ImpresaQuestionarioImpiegatiDeterminatiScadenza> _scadenze;

        #endregion

        #region Association Fixup
    
        private void FixupImpresa(Impresa previousValue)
        {
            if (previousValue != null && ReferenceEquals(previousValue.ImpreseQuestionarioImpiegati, this))
            {
                previousValue.ImpreseQuestionarioImpiegati = null;
            }
    
            if (Impresa != null)
            {
                Impresa.ImpreseQuestionarioImpiegati = this;
                if (IdImpresa != Impresa.Id)
                {
                    IdImpresa = Impresa.Id;
                }
            }
        }
    
        private void FixupScadenze(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ImpresaQuestionarioImpiegatiDeterminatiScadenza item in e.NewItems)
                {
                    item.ImpresaQuestionarioImpiegati = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ImpresaQuestionarioImpiegatiDeterminatiScadenza item in e.OldItems)
                {
                    if (ReferenceEquals(item.ImpresaQuestionarioImpiegati, this))
                    {
                        item.ImpresaQuestionarioImpiegati = null;
                    }
                }
            }
        }

        #endregion

    }
}
