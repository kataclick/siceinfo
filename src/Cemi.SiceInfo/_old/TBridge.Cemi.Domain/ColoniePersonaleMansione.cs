//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class ColoniePersonaleMansione
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual string Descrizione
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<ColoniePersonaleRichiesta> ColoniePersonaleRichieste
        {
            get
            {
                if (_coloniePersonaleRichieste == null)
                {
                    var newCollection = new FixupCollection<ColoniePersonaleRichiesta>();
                    newCollection.CollectionChanged += FixupColoniePersonaleRichieste;
                    _coloniePersonaleRichieste = newCollection;
                }
                return _coloniePersonaleRichieste;
            }
            set
            {
                if (!ReferenceEquals(_coloniePersonaleRichieste, value))
                {
                    var previousValue = _coloniePersonaleRichieste as FixupCollection<ColoniePersonaleRichiesta>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupColoniePersonaleRichieste;
                    }
                    _coloniePersonaleRichieste = value;
                    var newValue = value as FixupCollection<ColoniePersonaleRichiesta>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupColoniePersonaleRichieste;
                    }
                }
            }
        }
        private ICollection<ColoniePersonaleRichiesta> _coloniePersonaleRichieste;
    
        public virtual ICollection<ColoniePersonaleColloquio> ColoniePersonaleColloqui
        {
            get
            {
                if (_coloniePersonaleColloqui == null)
                {
                    var newCollection = new FixupCollection<ColoniePersonaleColloquio>();
                    newCollection.CollectionChanged += FixupColoniePersonaleColloqui;
                    _coloniePersonaleColloqui = newCollection;
                }
                return _coloniePersonaleColloqui;
            }
            set
            {
                if (!ReferenceEquals(_coloniePersonaleColloqui, value))
                {
                    var previousValue = _coloniePersonaleColloqui as FixupCollection<ColoniePersonaleColloquio>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupColoniePersonaleColloqui;
                    }
                    _coloniePersonaleColloqui = value;
                    var newValue = value as FixupCollection<ColoniePersonaleColloquio>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupColoniePersonaleColloqui;
                    }
                }
            }
        }
        private ICollection<ColoniePersonaleColloquio> _coloniePersonaleColloqui;
    
        public virtual ICollection<ColoniePersonaleMansioniRetribuzione> ColoniePersonaleMansioniRetribuzioni
        {
            get
            {
                if (_coloniePersonaleMansioniRetribuzioni == null)
                {
                    var newCollection = new FixupCollection<ColoniePersonaleMansioniRetribuzione>();
                    newCollection.CollectionChanged += FixupColoniePersonaleMansioniRetribuzioni;
                    _coloniePersonaleMansioniRetribuzioni = newCollection;
                }
                return _coloniePersonaleMansioniRetribuzioni;
            }
            set
            {
                if (!ReferenceEquals(_coloniePersonaleMansioniRetribuzioni, value))
                {
                    var previousValue = _coloniePersonaleMansioniRetribuzioni as FixupCollection<ColoniePersonaleMansioniRetribuzione>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupColoniePersonaleMansioniRetribuzioni;
                    }
                    _coloniePersonaleMansioniRetribuzioni = value;
                    var newValue = value as FixupCollection<ColoniePersonaleMansioniRetribuzione>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupColoniePersonaleMansioniRetribuzioni;
                    }
                }
            }
        }
        private ICollection<ColoniePersonaleMansioniRetribuzione> _coloniePersonaleMansioniRetribuzioni;
    
        public virtual ICollection<ColoniePersonaleProposta> ColoniePersonaleProposte
        {
            get
            {
                if (_coloniePersonaleProposte == null)
                {
                    var newCollection = new FixupCollection<ColoniePersonaleProposta>();
                    newCollection.CollectionChanged += FixupColoniePersonaleProposte;
                    _coloniePersonaleProposte = newCollection;
                }
                return _coloniePersonaleProposte;
            }
            set
            {
                if (!ReferenceEquals(_coloniePersonaleProposte, value))
                {
                    var previousValue = _coloniePersonaleProposte as FixupCollection<ColoniePersonaleProposta>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupColoniePersonaleProposte;
                    }
                    _coloniePersonaleProposte = value;
                    var newValue = value as FixupCollection<ColoniePersonaleProposta>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupColoniePersonaleProposte;
                    }
                }
            }
        }
        private ICollection<ColoniePersonaleProposta> _coloniePersonaleProposte;

        #endregion

        #region Association Fixup
    
        private void FixupColoniePersonaleRichieste(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ColoniePersonaleRichiesta item in e.NewItems)
                {
                    if (!item.ColoniePersonaleMansioni.Contains(this))
                    {
                        item.ColoniePersonaleMansioni.Add(this);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ColoniePersonaleRichiesta item in e.OldItems)
                {
                    if (item.ColoniePersonaleMansioni.Contains(this))
                    {
                        item.ColoniePersonaleMansioni.Remove(this);
                    }
                }
            }
        }
    
        private void FixupColoniePersonaleColloqui(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ColoniePersonaleColloquio item in e.NewItems)
                {
                    if (!item.ColoniePersonaleMansioni.Contains(this))
                    {
                        item.ColoniePersonaleMansioni.Add(this);
                    }
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ColoniePersonaleColloquio item in e.OldItems)
                {
                    if (item.ColoniePersonaleMansioni.Contains(this))
                    {
                        item.ColoniePersonaleMansioni.Remove(this);
                    }
                }
            }
        }
    
        private void FixupColoniePersonaleMansioniRetribuzioni(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ColoniePersonaleMansioniRetribuzione item in e.NewItems)
                {
                    item.Mansione = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ColoniePersonaleMansioniRetribuzione item in e.OldItems)
                {
                    if (ReferenceEquals(item.Mansione, this))
                    {
                        item.Mansione = null;
                    }
                }
            }
        }
    
        private void FixupColoniePersonaleProposte(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (ColoniePersonaleProposta item in e.NewItems)
                {
                    item.Mansione = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ColoniePersonaleProposta item in e.OldItems)
                {
                    if (ReferenceEquals(item.Mansione, this))
                    {
                        item.Mansione = null;
                    }
                }
            }
        }

        #endregion

    }
}
