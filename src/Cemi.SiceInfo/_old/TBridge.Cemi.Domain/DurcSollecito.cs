//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class DurcSollecito
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual int IdUtente
        {
            get { return _idUtente; }
            set
            {
                if (_idUtente != value)
                {
                    if (Utente != null && Utente.Id != value)
                    {
                        Utente = null;
                    }
                    _idUtente = value;
                }
            }
        }
        private int _idUtente;
    
        public virtual string EMail
        {
            get;
            set;
        }
    
        public virtual string Cip
        {
            get;
            set;
        }
    
        public virtual string NumeroProtocollo
        {
            get;
            set;
        }
    
        public virtual string MotivoRichiesta
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataInserimentoRecord
        {
            get;
            set;
        }
    
        public virtual int IdImpresa
        {
            get { return _idImpresa; }
            set
            {
                if (_idImpresa != value)
                {
                    if (Impresa != null && Impresa.Id != value)
                    {
                        Impresa = null;
                    }
                    _idImpresa = value;
                }
            }
        }
        private int _idImpresa;
    
        public virtual bool RitiroSede
        {
            get;
            set;
        }
    
        public virtual string NumeroTelefono
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual Impresa Impresa
        {
            get { return _impresa; }
            set
            {
                if (!ReferenceEquals(_impresa, value))
                {
                    var previousValue = _impresa;
                    _impresa = value;
                    FixupImpresa(previousValue);
                }
            }
        }
        private Impresa _impresa;
    
        public virtual Utente Utente
        {
            get { return _utente; }
            set
            {
                if (!ReferenceEquals(_utente, value))
                {
                    var previousValue = _utente;
                    _utente = value;
                    FixupUtente(previousValue);
                }
            }
        }
        private Utente _utente;

        #endregion

        #region Association Fixup
    
        private void FixupImpresa(Impresa previousValue)
        {
            if (previousValue != null && previousValue.DurcSolleciti.Contains(this))
            {
                previousValue.DurcSolleciti.Remove(this);
            }
    
            if (Impresa != null)
            {
                if (!Impresa.DurcSolleciti.Contains(this))
                {
                    Impresa.DurcSolleciti.Add(this);
                }
                if (IdImpresa != Impresa.Id)
                {
                    IdImpresa = Impresa.Id;
                }
            }
        }
    
        private void FixupUtente(Utente previousValue)
        {
            if (previousValue != null && previousValue.DurcSolleciti.Contains(this))
            {
                previousValue.DurcSolleciti.Remove(this);
            }
    
            if (Utente != null)
            {
                if (!Utente.DurcSolleciti.Contains(this))
                {
                    Utente.DurcSolleciti.Add(this);
                }
                if (IdUtente != Utente.Id)
                {
                    IdUtente = Utente.Id;
                }
            }
        }

        #endregion

    }
}
