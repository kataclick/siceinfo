//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class AccessoCantieriFornitore
    {
        #region Primitive Properties
    
        public virtual int Id
        {
            get;
            set;
        }
    
        public virtual string RagioneSociale
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> dataInserimentoRecord
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<AccessoCantieriRilevatore> AccessoCantieriRilevatori
        {
            get
            {
                if (_accessoCantieriRilevatori == null)
                {
                    var newCollection = new FixupCollection<AccessoCantieriRilevatore>();
                    newCollection.CollectionChanged += FixupAccessoCantieriRilevatori;
                    _accessoCantieriRilevatori = newCollection;
                }
                return _accessoCantieriRilevatori;
            }
            set
            {
                if (!ReferenceEquals(_accessoCantieriRilevatori, value))
                {
                    var previousValue = _accessoCantieriRilevatori as FixupCollection<AccessoCantieriRilevatore>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupAccessoCantieriRilevatori;
                    }
                    _accessoCantieriRilevatori = value;
                    var newValue = value as FixupCollection<AccessoCantieriRilevatore>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupAccessoCantieriRilevatori;
                    }
                }
            }
        }
        private ICollection<AccessoCantieriRilevatore> _accessoCantieriRilevatori;

        #endregion

        #region Association Fixup
    
        private void FixupAccessoCantieriRilevatori(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (AccessoCantieriRilevatore item in e.NewItems)
                {
                    item.AccessoCantieriFornitore = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (AccessoCantieriRilevatore item in e.OldItems)
                {
                    if (ReferenceEquals(item.AccessoCantieriFornitore, this))
                    {
                        item.AccessoCantieriFornitore = null;
                    }
                }
            }
        }

        #endregion

    }
}
