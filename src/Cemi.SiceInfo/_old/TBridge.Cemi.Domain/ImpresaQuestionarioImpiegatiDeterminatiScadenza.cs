//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain
{
    public partial class ImpresaQuestionarioImpiegatiDeterminatiScadenza
    {
        #region Primitive Properties
    
        public virtual int IdScadenza
        {
            get;
            set;
        }
    
        public virtual int IdImpresa
        {
            get { return _idImpresa; }
            set
            {
                if (_idImpresa != value)
                {
                    if (ImpresaQuestionarioImpiegati != null && ImpresaQuestionarioImpiegati.IdImpresa != value)
                    {
                        ImpresaQuestionarioImpiegati = null;
                    }
                    _idImpresa = value;
                }
            }
        }
        private int _idImpresa;
    
        public virtual short TipoOrario
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataScadenza
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataInizio
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ImpresaQuestionarioImpiegati ImpresaQuestionarioImpiegati
        {
            get { return _impresaQuestionarioImpiegati; }
            set
            {
                if (!ReferenceEquals(_impresaQuestionarioImpiegati, value))
                {
                    var previousValue = _impresaQuestionarioImpiegati;
                    _impresaQuestionarioImpiegati = value;
                    FixupImpresaQuestionarioImpiegati(previousValue);
                }
            }
        }
        private ImpresaQuestionarioImpiegati _impresaQuestionarioImpiegati;

        #endregion

        #region Association Fixup
    
        private void FixupImpresaQuestionarioImpiegati(ImpresaQuestionarioImpiegati previousValue)
        {
            if (previousValue != null && previousValue.Scadenze.Contains(this))
            {
                previousValue.Scadenze.Remove(this);
            }
    
            if (ImpresaQuestionarioImpiegati != null)
            {
                if (!ImpresaQuestionarioImpiegati.Scadenze.Contains(this))
                {
                    ImpresaQuestionarioImpiegati.Scadenze.Add(this);
                }
                if (IdImpresa != ImpresaQuestionarioImpiegati.IdImpresa)
                {
                    IdImpresa = ImpresaQuestionarioImpiegati.IdImpresa;
                }
            }
        }

        #endregion

    }
}
