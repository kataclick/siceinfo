//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace TBridge.Cemi.Type.Domain.AnagraficaCondivisa
{
    public partial class Impresa
    {
        #region Primitive Properties
    
        public virtual int IdImpresa
        {
            get;
            set;
        }
    
        public virtual Nullable<int> CodiceCassaEdile
        {
            get;
            set;
        }
    
        public virtual string RagioneSociale
        {
            get;
            set;
        }
    
        public virtual string CodiceFiscale
        {
            get;
            set;
        }
    
        public virtual string PartitaIVA
        {
            get;
            set;
        }
    
        public virtual string CodiceINPS
        {
            get;
            set;
        }
    
        public virtual string CodiceINAIL
        {
            get;
            set;
        }
    
        public virtual string IscrizioneCCIAA
        {
            get;
            set;
        }
    
        public virtual int IdContratto
        {
            get;
            set;
        }
    
        public virtual string IdTipoImpresa
        {
            get;
            set;
        }
    
        public virtual string IdAttivitaISTAT
        {
            get;
            set;
        }
    
        public virtual Nullable<int> IdNaturaGiuridica
        {
            get;
            set;
        }
    
        public virtual string SitoWeb
        {
            get;
            set;
        }
    
        public virtual bool LavoratoreAutonomo
        {
            get;
            set;
        }
    
        public virtual Nullable<int> IdStatoImpresa
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataUltimaVariazioneStato
        {
            get;
            set;
        }
    
        public virtual Nullable<bool> Regolarita
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataUltimaDenuncia
        {
            get;
            set;
        }
    
        public virtual int IdEnteGestore
        {
            get;
            set;
        }
    
        public virtual int IdEnteInserimento
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataInserimento
        {
            get;
            set;
        }
    
        public virtual string UtenteInserimento
        {
            get;
            set;
        }
    
        public virtual int IdEnteUltimaModifica
        {
            get;
            set;
        }
    
        public virtual System.DateTime DataUltimaModifica
        {
            get;
            set;
        }
    
        public virtual string UtenteUltimaModifica
        {
            get;
            set;
        }
    
        public virtual bool Cancellata
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> DataCancellazione
        {
            get;
            set;
        }
    
        public virtual string UtenteCancellazione
        {
            get;
            set;
        }

        #endregion

        #region Navigation Properties
    
        public virtual ICollection<LavoratoreCorso> LavoratoriCorsi
        {
            get
            {
                if (_lavoratoriCorsi == null)
                {
                    var newCollection = new FixupCollection<LavoratoreCorso>();
                    newCollection.CollectionChanged += FixupLavoratoriCorsi;
                    _lavoratoriCorsi = newCollection;
                }
                return _lavoratoriCorsi;
            }
            set
            {
                if (!ReferenceEquals(_lavoratoriCorsi, value))
                {
                    var previousValue = _lavoratoriCorsi as FixupCollection<LavoratoreCorso>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupLavoratoriCorsi;
                    }
                    _lavoratoriCorsi = value;
                    var newValue = value as FixupCollection<LavoratoreCorso>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupLavoratoriCorsi;
                    }
                }
            }
        }
        private ICollection<LavoratoreCorso> _lavoratoriCorsi;
    
        public virtual ICollection<Sede> Sedi
        {
            get
            {
                if (_sedi == null)
                {
                    var newCollection = new FixupCollection<Sede>();
                    newCollection.CollectionChanged += FixupSedi;
                    _sedi = newCollection;
                }
                return _sedi;
            }
            set
            {
                if (!ReferenceEquals(_sedi, value))
                {
                    var previousValue = _sedi as FixupCollection<Sede>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupSedi;
                    }
                    _sedi = value;
                    var newValue = value as FixupCollection<Sede>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupSedi;
                    }
                }
            }
        }
        private ICollection<Sede> _sedi;

        #endregion

        #region Association Fixup
    
        private void FixupLavoratoriCorsi(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (LavoratoreCorso item in e.NewItems)
                {
                    item.Impresa = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (LavoratoreCorso item in e.OldItems)
                {
                    if (ReferenceEquals(item.Impresa, this))
                    {
                        item.Impresa = null;
                    }
                }
            }
        }
    
        private void FixupSedi(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Sede item in e.NewItems)
                {
                    item.Impresa = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Sede item in e.OldItems)
                {
                    if (ReferenceEquals(item.Impresa, this))
                    {
                        item.Impresa = null;
                    }
                }
            }
        }

        #endregion

    }
}
