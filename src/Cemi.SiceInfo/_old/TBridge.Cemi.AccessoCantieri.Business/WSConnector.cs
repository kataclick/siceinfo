﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.WSTypes;
using Impresa = TBridge.Cemi.AccessoCantieri.Type.WSTypes.Impresa;
using Lavoratore = TBridge.Cemi.AccessoCantieri.Type.WSTypes.Lavoratore;
using Referente = TBridge.Cemi.AccessoCantieri.Type.WSTypes.Referente;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class WSConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        public string CaricaAccessi(string codiceRilevatore, int idCantiere, List<Accesso> accessi)
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();
            string res = "OK";

            foreach (Accesso accesso in accessi)
            {
                Timbratura timbratura = new Timbratura
                {
                    CodiceFiscale = accesso.CodiceFiscaleLavoratore,
                    CodiceFiscaleImpresa = accesso.CodiceFiscaleImpresa,
                    PartitaIvaImpresa = accesso.CodiceFiscaleImpresa,
                    CodiceRilevatore = codiceRilevatore,
                    RagioneSociale = "WebService",
                    DataOra = accesso.DataOra,
                    IngressoUscita = accesso.Ingresso,
                    IdCantiere = idCantiere
                };

                // timbratura.RagioneSociale = ConfigurationManager.AppSettings["FornitoreRilevatore"];

                if (timbratura.IdCantiere != -1)
                {
                    timbratura.PartitaIvaImpresa =
                        _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere.Value,
                            timbratura.CodiceFiscale, timbratura.DataOra);
                }

                if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.WebService) == -1)
                {
                    timbrature.Add(timbratura);
                }
            }

            if (timbrature.Count > 0)
            {
                int nErr = _biz.InsertTimbrature(timbrature, TipologiaFornitore.WebService);

                if (nErr > 0)
                {
                    res = "Caricamento completato con errori";
                }
            }

            return res;
        }

        public string CaricaAccesso(string codiceRilevatore, int idCantiere, Accesso accesso)
        {
            TimbraturaCollection timbrature = new TimbraturaCollection();
            string res = "OK";

            Timbratura timbratura = new Timbratura
            {
                CodiceFiscale = accesso.CodiceFiscaleLavoratore,
                CodiceFiscaleImpresa = accesso.CodiceFiscaleImpresa,
                //PartitaIvaImpresa = accesso.CodiceFiscaleImpresa,
                CodiceRilevatore = codiceRilevatore,
                RagioneSociale = "WebService",
                DataOra = accesso.DataOra,
                IngressoUscita = accesso.Ingresso,
                IdCantiere = idCantiere
            };

            if (timbratura.CodiceFiscaleImpresa.Length == 11)
            {
                timbratura.PartitaIvaImpresa = timbratura.CodiceFiscaleImpresa;
            }

            // timbratura.RagioneSociale = ConfigurationManager.AppSettings["FornitoreRilevatore"];

            if (timbratura.IdCantiere != -1 && string.IsNullOrWhiteSpace(timbratura.PartitaIvaImpresa))
            {
                timbratura.PartitaIvaImpresa =
                    _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere.Value,
                        timbratura.CodiceFiscale, timbratura.DataOra);
            }

            if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.WebService) == -1)
            {
                timbrature.Add(timbratura);
            }

            if (timbrature.Count > 0)
            {
                int nErr = _biz.InsertTimbrature(timbrature, TipologiaFornitore.WebService);

                if (nErr > 0)
                {
                    res = "Caricamento completato con errori";
                }
            }

            return res;
        }

        public string CaricaImpreseLavoratori(string codiceRilevatore, int idCantiere, List<Impresa> imprese)
        {
            string res = "OK";

            WhiteList cantiere = new WhiteList();
            cantiere.IdWhiteList = idCantiere;
            ImpresaCollection impreseAnag = _biz.GetImpreseSelezionateInSubappalto(idCantiere);
            SubappaltoCollection subappalti = new SubappaltoCollection();
            cantiere.Subappalti = subappalti;
            WhiteListImpresaCollection wlImprese = new WhiteListImpresaCollection();
            cantiere.Lavoratori = wlImprese;
            WhiteListImpresaCollection lavAnag = _biz.GetLavoratoriInDomandaNoFoto(idCantiere);
            ReferenteCollection referenti = new ReferenteCollection();
            cantiere.ListaReferenti = referenti;

            // Questo primo giro mi serve per beccare la lista imprese
            foreach (Impresa impresa in imprese)
            {
                if (string.IsNullOrWhiteSpace(impresa.CodiceFiscale)
                    || string.IsNullOrWhiteSpace(impresa.PartitaIVA)
                    || string.IsNullOrWhiteSpace(impresa.RagioneSociale))
                {
                    _biz.InsertLogWSImprese(codiceRilevatore, impresa.RagioneSociale, impresa.CodiceFiscale,
                        impresa.PartitaIVA, "Manca un dato di base");
                    continue;
                }

                Subappalto sub = new Subappalto();
                Type.Entities.Impresa iAccCant = new Type.Entities.Impresa();
                sub.Appaltata = iAccCant;
                subappalti.Add(sub);

                // Verifico se l'impresa passata è presente in anagrafica Cemi,
                // in caso contrario la creo
                ImpresaCollection impSiceNew =
                    _biz.GetimpreseOrdinate(null, null, null, null, impresa.CodiceFiscale, null, null);
                if (impSiceNew != null && impSiceNew.Count > 0)
                {
                    iAccCant.TipoImpresa = TipologiaImpresa.SiceNew;
                    iAccCant.IdImpresa = impSiceNew[0].IdImpresa;
                }
                else
                {
                    iAccCant.TipoImpresa = TipologiaImpresa.Nuova;

                    // Guardo se l'impresa era già presente e nel caso ne recupero
                    // il codice
                    foreach (Type.Entities.Impresa impAnag in impreseAnag)
                    {
                        if (impresa.CodiceFiscale == impAnag.CodiceFiscale)
                        {
                            iAccCant.IdImpresa = impAnag.IdImpresa;
                            break;
                        }
                    }

                    if (!iAccCant.IdImpresa.HasValue)
                    {
                        iAccCant.IdTemporaneo = Guid.NewGuid();
                    }

                    iAccCant.RagioneSociale = impresa.RagioneSociale;
                    iAccCant.CodiceFiscale = impresa.CodiceFiscale;
                    iAccCant.PartitaIva = impresa.PartitaIVA;
                    iAccCant.Indirizzo = impresa.Indirizzo;
                    iAccCant.Comune = impresa.Comune;
                    iAccCant.Provincia = impresa.Provincia;
                    iAccCant.Cap = impresa.CAP;
                    iAccCant.ContrattoApplicato = impresa.ContrattoApplicato;
                }

                // Referenti
                if (impresa.Referenti != null && impresa.Referenti.Count > 0)
                {
                    foreach (Referente referente in impresa.Referenti)
                    {
                        if (string.IsNullOrWhiteSpace(referente.Cognome)
                            || string.IsNullOrWhiteSpace(referente.Nome)
                            || string.IsNullOrWhiteSpace(referente.CodiceFiscale)
                            || referente.DataNascita < new DateTime(1920, 1, 1))
                        {
                            continue;
                        }

                        Type.Entities.Referente refAccCant = new Type.Entities.Referente();
                        referenti.Add(refAccCant);

                        refAccCant.Cognome = referente.Cognome;
                        refAccCant.Nome = referente.Nome;
                        refAccCant.DataNascita = referente.DataNascita;
                        refAccCant.CodiceFiscale = referente.CodiceFiscale;
                        refAccCant.Telefono = string.Format("{0} {1}", referente.Telefono, referente.Cellulare).Trim();
                        refAccCant.Email = string.Format("{0} {1}", referente.EMail, referente.PEC);
                        refAccCant.TipoRuolo = TipologiaRuoloReferente.Impresa;
                    }
                }

                WhiteListImpresa wlImpresa = new WhiteListImpresa();
                wlImprese.Add(wlImpresa);

                wlImpresa.Impresa = iAccCant;
                wlImpresa.IdDomanda = idCantiere;
                wlImpresa.Lavoratori = new LavoratoreCollection();

                WhiteListImpresa wlImpAnag =
                    lavAnag.SelezionaDomandaImpresa(iAccCant.TipoImpresa, iAccCant.IdImpresa, null, null);

                // Lavoratori
                if (impresa.Lavoratori != null && impresa.Lavoratori.Count > 0)
                {
                    foreach (Lavoratore lavoratore in impresa.Lavoratori)
                    {
                        if (string.IsNullOrWhiteSpace(lavoratore.Cognome)
                            || string.IsNullOrWhiteSpace(lavoratore.Nome)
                            || !lavoratore.DataNascita.HasValue)
                        {
                            _biz.InsertLogWSLavoratore(codiceRilevatore, lavoratore.Cognome, lavoratore.Nome,
                                lavoratore.CodiceFiscale, "Manca un dato di base");
                            continue;
                        }

                        Type.Entities.Lavoratore lavAccCant = new Type.Entities.Lavoratore();
                        lavAccCant.EffettuaControlli = true;
                        wlImpresa.Lavoratori.Add(lavAccCant);

                        // Verifico se il lavoratore è presente in anagrafica Cemi,
                        // in caso contrario lo creo
                        LavoratoreCollection lavSiceNew = _biz.GetLavoratoriOrdinati(null, null, null, null,
                            lavoratore.CodiceFiscale, null, null, null, null, null);
                        if (lavSiceNew != null && lavSiceNew.Count > 0)
                        {
                            lavAccCant.TipoLavoratore = TipologiaLavoratore.SiceNew;
                            lavAccCant.IdLavoratore = lavSiceNew[0].IdLavoratore;
                        }
                        else
                        {
                            lavAccCant.TipoLavoratore = TipologiaLavoratore.Nuovo;

                            // Guardo se il lavoratore era già presente e nel caso ne recupero
                            // il codice
                            if (wlImpAnag != null)
                            {
                                foreach (Type.Entities.Lavoratore lavoAnag in wlImpAnag.Lavoratori)
                                {
                                    if (lavoratore.CodiceFiscale == lavoAnag.CodiceFiscale)
                                    {
                                        lavAccCant.IdLavoratore = lavoAnag.IdLavoratore;
                                        break;
                                    }
                                }
                            }

                            lavAccCant.Cognome = lavoratore.Cognome;
                            lavAccCant.Nome = lavoratore.Nome;
                            lavAccCant.CodiceFiscale = lavoratore.CodiceFiscale;
                            lavAccCant.DataNascita = lavoratore.DataNascita;
                        }

                        lavAccCant.ContrattoApplicato = lavoratore.ContrattoApplicato;
                        lavAccCant.DataInizioAttivita = lavoratore.Dal;
                        lavAccCant.DataFineAttivita = lavoratore.Al;
                    }
                }
            }

            // Questo secondo giro mi serve per beccare i subappaltatori
            for (int i = 0; i < imprese.Count; i++)
            {
                // Se è presente ho l'impresa appaltatrice
                if (!string.IsNullOrWhiteSpace(imprese[i].CodiceFiscaleAppaltatrice))
                {
                    subappalti[i].Appaltante =
                        subappalti.GetImpresaByCodiceFiscale(imprese[i].CodiceFiscaleAppaltatrice);
                }
            }

            if (subappalti.Count > 0)
            {
                _biz.DeleteSubappaltiDellaDomanda(idCantiere);
                _biz.DeleteLavoratoriDellaDomanda(idCantiere);
                _biz.DeleteReferentiDellaDomanda(idCantiere);

                bool ok = _biz.InsertUpdateSubappaltieLavoratori(cantiere);
                _biz.InsertReferenti(cantiere.ListaReferenti, idCantiere);

                if (!ok)
                {
                    res = "Caricamento completato con errori";
                }
            }

            return res;
        }

        public string CaricaImpresa(string codiceRilevatore, int idCantiere, Impresa impresa)
        {
            string res = "OK";

            WhiteList cantiere = _biz.GetDomandaByKey(idCantiere);
            cantiere.IdWhiteList = idCantiere;
            ImpresaCollection impreseAnag = _biz.GetImpreseSelezionateInSubappalto(idCantiere);

            Subappalto sub = new Subappalto();
            Type.Entities.Impresa iAccCant = new Type.Entities.Impresa();
            //sub.Appaltata = iAccCant;

            // Verifico se l'impresa passata è presente in anagrafica Cemi,
            // in caso contrario la creo
            ImpresaCollection impSiceNew =
                _biz.GetimpreseOrdinate(null, null, null, null, impresa.CodiceFiscale, null, null);
            if (impSiceNew != null && impSiceNew.Count > 0)
            {
                iAccCant.TipoImpresa = TipologiaImpresa.SiceNew;
                iAccCant.IdImpresa = impSiceNew[0].IdImpresa;
                iAccCant.CodiceFiscale = impSiceNew[0].CodiceFiscale;
                iAccCant.PartitaIva = impSiceNew[0].PartitaIva;
            }
            else
            {
                iAccCant.TipoImpresa = TipologiaImpresa.Nuova;

                // Guardo se l'impresa era già presente e nel caso ne recupero
                // il codice
                foreach (Type.Entities.Impresa impAnag in impreseAnag)
                {
                    if (impresa.CodiceFiscale == impAnag.CodiceFiscale)
                    {
                        iAccCant.IdImpresa = impAnag.IdImpresa;
                        break;
                    }
                }

                if (!iAccCant.IdImpresa.HasValue)
                {
                    iAccCant.IdTemporaneo = Guid.NewGuid();
                }

                iAccCant.RagioneSociale = impresa.RagioneSociale;
                iAccCant.CodiceFiscale = impresa.CodiceFiscale;
                iAccCant.PartitaIva = impresa.PartitaIVA;
                iAccCant.Indirizzo = impresa.Indirizzo;
                iAccCant.Comune = impresa.Comune;
                iAccCant.Provincia = impresa.Provincia;
                iAccCant.Cap = impresa.CAP;
                iAccCant.ContrattoApplicato = impresa.ContrattoApplicato;
            }

            Type.Entities.Impresa iAccCantApp = null;

            if (!string.IsNullOrWhiteSpace(impresa.CodiceFiscaleAppaltatrice))
            {
                iAccCantApp = new Type.Entities.Impresa();

                // Verifico se l'impresa passata è presente in anagrafica Cemi,
                // in caso contrario la creo
                ImpresaCollection impSiceNewApp = _biz.GetimpreseOrdinate(null, null, null, null,
                    impresa.CodiceFiscaleAppaltatrice, null, null);
                if (impSiceNewApp != null && impSiceNewApp.Count > 0)
                {
                    iAccCantApp.TipoImpresa = TipologiaImpresa.SiceNew;
                    iAccCantApp.IdImpresa = impSiceNewApp[0].IdImpresa;
                }
                else
                {
                    iAccCantApp.TipoImpresa = TipologiaImpresa.Nuova;

                    // Guardo se l'impresa era già presente e nel caso ne recupero
                    // il codice
                    foreach (Type.Entities.Impresa impAnag in impreseAnag)
                    {
                        if (impresa.CodiceFiscaleAppaltatrice == impAnag.CodiceFiscale)
                        {
                            iAccCantApp.IdImpresa = impAnag.IdImpresa;
                            break;
                        }
                    }

                    if (!iAccCantApp.IdImpresa.HasValue)
                    {
                        //iAccCantApp.IdTemporaneo = Guid.NewGuid();

                        return "Impresa appaltatrice non riconosciuta";
                    }

                    //iAccCantApp.RagioneSociale = impresa.RagioneSociale;
                    //iAccCantApp.CodiceFiscale = impresa.CodiceFiscale;
                    //iAccCantApp.PartitaIva = impresa.PartitaIVA;
                    //iAccCantApp.Indirizzo = impresa.Indirizzo;
                    //iAccCantApp.Comune = impresa.Comune;
                    //iAccCantApp.Provincia = impresa.Provincia;
                    //iAccCantApp.Cap = impresa.CAP;
                    //iAccCantApp.ContrattoApplicato = impresa.ContrattoApplicato;
                }
            }

            // Guardo se c'era già un subappalto con gli stessi codici fiscali
            Subappalto subEs =
                cantiere.Subappalti.GetSubappaltoByCodiceFiscale(iAccCant.CodiceFiscale,
                    impresa.CodiceFiscaleAppaltatrice);

            if (subEs == null)
            {
                // Devo creare il nuovo subappalto
                sub.Appaltata = iAccCant;
                sub.Appaltante = iAccCantApp;
            }
            else
            {
                if (subEs.Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                    && iAccCant.TipoImpresa == TipologiaImpresa.SiceNew
                    ||
                    subEs.Appaltante != null && iAccCantApp != null
                    && subEs.Appaltante.TipoImpresa == TipologiaImpresa.Nuova
                    && iAccCantApp.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    // Cancello il vecchio subappalto e lego all'impresa SiceNew
                    _biz.DeleteSubappalto(subEs.IdSubappalto.Value);
                    sub.Appaltata = iAccCant;
                    sub.Appaltante = iAccCantApp;
                }
                else
                {
                    if (subEs.Appaltata.TipoImpresa == TipologiaImpresa.Nuova
                        && iAccCant.TipoImpresa == TipologiaImpresa.Nuova)
                    {
                        // Aggiorno i dati dell'impresa
                        _biz.UpdateImpresa(iAccCant);

                        //if (subEs.Appaltante != null && iAccCantApp != null)
                        //{
                        //    _biz.UpdateImpresa(iAccCantApp);
                        //}

                        return res;
                    }

                    // In caso di impresa SiceNew (sia vecchia che nuova)
                    // non faccio niente
                }
            }

            //WhiteListImpresa wlImpresa = new WhiteListImpresa();
            //wlImprese.Add(wlImpresa);

            //wlImpresa.Impresa = iAccCant;
            //wlImpresa.IdDomanda = idCantiere;
            //wlImpresa.Lavoratori = new LavoratoreCollection();

            //WhiteListImpresa wlImpAnag = lavAnag.SelezionaDomandaImpresa(iAccCant.TipoImpresa, iAccCant.IdImpresa, null, null);
            //}

            // Questo secondo giro mi serve per beccare i subappaltatori
            //for (Int32 i = 0; i < imprese.Count; i++)
            //{
            //    // Se è presente ho l'impresa appaltatrice
            //    if (!String.IsNullOrWhiteSpace(imprese[i].CodiceFiscaleAppaltatrice))
            //    {
            //        subappalti[i].Appaltante = subappalti.GetImpresaByCodiceFiscale(imprese[i].CodiceFiscaleAppaltatrice);
            //    }
            //}

            //if (subappalti.Count > 0)
            //{
            //    _biz.DeleteSubappaltiDellaDomanda(idCantiere);
            //    _biz.DeleteLavoratoriDellaDomanda(idCantiere);
            //    _biz.DeleteReferentiDellaDomanda(idCantiere);

            //    Boolean ok = _biz.InsertUpdateSubappaltieLavoratori(cantiere);
            //    _biz.InsertReferenti(cantiere.ListaReferenti, idCantiere);

            //    if (!ok)
            //    {
            //        res = "Caricamento completato con errori";
            //    }
            //}
            if (sub != null && sub.Appaltata != null)
            {
                if (!_biz.InsertSubappalto(sub, idCantiere))
                {
                    res = "Caricamento completato con errori";
                }
            }

            return res;
        }

        public string CaricaLavoratore(string codiceRilevatore, int idCantiere, Lavoratore lavoratore)
        {
            string res = "OK";
            WhiteList cantiere = _biz.GetDomandaByKey(idCantiere);

            // Recupero l'impresa a cui associare il lavoratore
            Type.Entities.Impresa imp = cantiere.Subappalti.GetImpresaByCodiceFiscale(lavoratore.CodiceFiscaleImpresa);

            WhiteListImpresaCollection lavAnag = _biz.GetLavoratoriInDomandaNoFoto(idCantiere);
            WhiteListImpresa wlImpAnag = lavAnag.SelezionaDomandaImpresa(imp.TipoImpresa, imp.IdImpresa, null, null);
            Type.Entities.Lavoratore lavPres = null;

            // Verifico se il lavoratore passato è presente in anagrafica Cemi,
            // in caso contrario lo creo
            Type.Entities.Lavoratore iAccCantLav = new Type.Entities.Lavoratore();
            iAccCantLav.EffettuaControlli = true;
            LavoratoreCollection lavSiceNewApp = _biz.GetLavoratoriOrdinati(null, null, null, null,
                lavoratore.CodiceFiscale, null, null, null, null, null);
            if (wlImpAnag != null && wlImpAnag.Lavoratori != null)
            {
                lavPres = wlImpAnag.Lavoratori.GetByCodFisc(lavoratore.CodiceFiscale);
            }

            if (lavSiceNewApp != null && lavSiceNewApp.Count > 0)
            {
                iAccCantLav.TipoLavoratore = TipologiaLavoratore.SiceNew;
                iAccCantLav.IdLavoratore = lavSiceNewApp[0].IdLavoratore;
            }
            else
            {
                iAccCantLav.TipoLavoratore = TipologiaLavoratore.Nuovo;

                // Guardo se il lavoratore era già presente
                if (lavPres != null && lavPres.TipoLavoratore == TipologiaLavoratore.Nuovo)
                {
                    iAccCantLav.IdLavoratore = lavPres.IdLavoratore;
                    iAccCantLav.IdDomandaLavoratore = lavPres.IdDomandaLavoratore;
                }

                iAccCantLav.Cognome = lavoratore.Cognome;
                iAccCantLav.Nome = lavoratore.Nome;
                iAccCantLav.CodiceFiscale = lavoratore.CodiceFiscale;
                iAccCantLav.DataNascita = lavoratore.DataNascita;
                iAccCantLav.ContrattoApplicato = lavoratore.ContrattoApplicato;
                iAccCantLav.DataInizioAttivita = lavoratore.Dal;
                iAccCantLav.DataFineAttivita = lavoratore.Al;
            }

            if (lavPres == null)
            {
                // Inserisco il lavoratore
                _biz.InsertLavoratoreImpresa(iAccCantLav, idCantiere, imp);
            }
            else
            {
                if (lavPres.TipoLavoratore == TipologiaLavoratore.Nuovo
                    && iAccCantLav.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    // Cancello il lavoratore e lo inserisco nuovamente
                    _biz.DeleteLavoratoreInWhiteList(lavPres.IdDomandaLavoratore.Value);
                    _biz.InsertLavoratoreImpresa(iAccCantLav, idCantiere, imp);
                }
                else
                {
                    if (lavPres.TipoLavoratore == TipologiaLavoratore.Nuovo
                        && iAccCantLav.TipoLavoratore == TipologiaLavoratore.Nuovo)
                    {
                        // Aggiorno il lavoratore
                        _biz.UpdateLavoratore(iAccCantLav);
                        _biz.UpdateWhiteListLavoratore(iAccCantLav);
                    }
                }
            }

            return res;
        }
    }
}