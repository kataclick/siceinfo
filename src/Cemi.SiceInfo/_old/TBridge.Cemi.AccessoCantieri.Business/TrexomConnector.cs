﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Cemi.SiceInfo.Utility;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class TrexomConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        public int CaricaTimbrature()
        {
            int ret;

            TimbraturaCollection timbratureDB = new TimbraturaCollection();

            try
            {
                timbratureDB = _biz.GetTimbratureTrexom();
            }
            catch
            {
                return -2;
            }

            TimbraturaCollection timbrature = new TimbraturaCollection();

            foreach (Timbratura timbratura in timbratureDB)
            {
                try
                {
                    timbratura.RagioneSociale = "Trexom";
                    timbratura.Latitudine = 0; //Decimal.Parse(row["gps_lati"].ToString());
                    timbratura.Longitudine = 0; //Decimal.Parse(row["gps_longi"].ToString());

                    //return - 2;

                    timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.Trexom, timbratura.CodiceRilevatore,
                        timbratura.DataOra);

                    // Eliminiamo i caratteri non ASCII eventualmente presenti sul CF
                    // e ripuliamo da spazi vuoti
                    timbratura.CodiceFiscale = StringHelper.NormalizzaCampoTesto(timbratura.CodiceFiscale);

                    // Togliamo il carattere 0000 a volte presente nel dato proveniente dai lettori
                    timbratura.CodiceFiscale = Regex.Replace(timbratura.CodiceFiscale, "\u0000", string.Empty);

                    if (timbratura.IdCantiere.HasValue)
                    {
                        if (timbratura.IdCantiere != -1)
                            timbratura.PartitaIvaImpresa = _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere,
                                timbratura.CodiceFiscale,
                                timbratura.DataOra);
                    }

                    if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.Trexom) == -1)
                        timbrature.Add(timbratura);
                }
                catch
                {
                    //throw;
                }
            }

            if (timbrature.Count == 0)
                ret = -1;
            else
                ret = _biz.InsertTimbrature(timbrature, TipologiaFornitore.Trexom);

            return ret;
        }

        #region whitelist

        public void DeleteWhitelist(string codiceRilevatore)
        {
            _biz.TrexomInsertDeleteWhiteList(codiceRilevatore);
        }

        public void CaricaWhitelist(Dictionary<string, bool> codiciFiscali, string codiceRilevatore)
        {
            foreach (KeyValuePair<string, bool> lavoratore in codiciFiscali)
            {
                // Per ogni lavoratore (abilitato o meno) presente in white list
                // recupero l'ultimo comando lanciato per la white list sul lettore

                bool? ultimoComando = _biz.TrexomGetUltimoComandoWhiteList(codiceRilevatore, lavoratore.Key);

                // Inserisco un nuovo record solo se:
                // - non era presente uno stato precedente e lo stato nuovo è presente
                // - lo stato del lavoratore è cambiato
                if (!ultimoComando.HasValue && lavoratore.Value
                    || ultimoComando.HasValue && ultimoComando.Value != lavoratore.Value)
                {
                    _biz.TrexomInsertComandoWhiteList(codiceRilevatore, lavoratore.Key, lavoratore.Value);
                }
            }
        }

        #endregion
    }
}