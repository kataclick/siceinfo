﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class ProgettiESoluzioniConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        private static byte[] DownloadFile(string ftpAddress, string filename, string username, string password)
        {
            byte[] downloadedData = { };

            try
            {
                FtpWebRequest request = WebRequest.Create(ftpAddress + "/" + filename) as FtpWebRequest;

                if (request != null)
                {
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true; //don't close the connection

                    int dataLength = (int) request.GetResponse().ContentLength;

                    request = WebRequest.Create(ftpAddress + "/" + filename) as FtpWebRequest;
                    if (request != null)
                    {
                        request.Method = WebRequestMethods.Ftp.DownloadFile;
                        request.Credentials = new NetworkCredential(username, password);
                        request.UsePassive = true;
                        request.UseBinary = true;
                        request.KeepAlive = false; //close the connection when done
                    }
                }
                if (request != null)
                {
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    if (response != null)
                    {
                        Stream reader = response.GetResponseStream();

                        MemoryStream memStream = new MemoryStream();
                        byte[] buffer = new byte[1024]; //downloads in chuncks

                        while (true)
                        {
                            if (reader != null)
                            {
                                int bytesRead = reader.Read(buffer, 0, buffer.Length);

                                if (bytesRead == 0)
                                {
                                    break;
                                }
                                memStream.Write(buffer, 0, bytesRead);
                            }
                        }

                        downloadedData = memStream.ToArray();

                        reader.Close();
                        memStream.Close();
                    }
                    if (response != null)
                        response.Close();
                }
            }
            catch (Exception)
            {
                throw new Exception("Download non riuscito dal server ftp");
            }

            return downloadedData;
        }

        private static List<string> ListFtpFiles()
        {
            FtpWebRequest ftpWebRequest =
                (FtpWebRequest) WebRequest.Create(ConfigurationManager.AppSettings["ProgettiESoluzioniFTPAddress"]);

            ftpWebRequest.Method = WebRequestMethods.Ftp.ListDirectory;

            ftpWebRequest.Credentials =
                new NetworkCredential(ConfigurationManager.AppSettings["ProgettiESoluzioniUsername"],
                    ConfigurationManager.AppSettings["ProgettiESoluzioniPassword"]);

            ftpWebRequest.KeepAlive = false;
            ftpWebRequest.UseBinary = true;

            FtpWebResponse ftpWebResponse =
                (FtpWebResponse) ftpWebRequest.GetResponse();

            if (ftpWebResponse != null)
            {
                Stream ftpStream = ftpWebResponse.GetResponseStream();
                if (ftpStream != null)
                {
                    StreamReader reader = new StreamReader(ftpStream);

                    string[] directoryFiles2 = Regex.Split(reader.ReadToEnd(), Environment.NewLine);

                    List<string> directoryFiles = new List<string>();

                    foreach (string fileName in directoryFiles2)
                    {
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            if (
                                !File.Exists(string.Format("{0}{1}",
                                    ConfigurationManager.AppSettings["ProgettiESoluzioniPath"],
                                    fileName)))
                            {
                                directoryFiles.Add(fileName);
                            }
                        }
                    }

                    reader.Close();
                    ftpWebResponse.Close();

                    return directoryFiles;
                }
            }
            return null;
        }

        private static void Salva(byte[] downloadedData, string path)
        {
            if (downloadedData != null && downloadedData.Length != 0)
            {
                FileStream newFile = new FileStream(path, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
            }
        }

        private static DataTable ImportToMem(string csvFile)
        {
            StreamReader sr = new StreamReader(csvFile);
            string inputLine;

            bool firstString = true;

            using (DataTable dt = new DataTable())
            {
                while ((inputLine = sr.ReadLine()) != null)
                {
                    string[] values = inputLine.Split(',');
                    if (firstString)
                    {
                        foreach (string s in values)
                        {
                            dt.Columns.Add(s);
                            firstString = false;
                        }
                    }
                    else
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            row[i] = values[i];
                        }
                        dt.Rows.Add(row);
                    }
                }

                sr.Close();
                return dt;
            }
        }

        public int CaricaDati()
        {
            try
            {
                List<string> fileList = ListFtpFiles();
                int ret = 0;

                foreach (string fileName in fileList)
                {
                    byte[] file = DownloadFile(ConfigurationManager.AppSettings["ProgettiESoluzioniFTPAddress"],
                        fileName,
                        ConfigurationManager.AppSettings["ProgettiESoluzioniUsername"],
                        ConfigurationManager.AppSettings["ProgettiESoluzioniPassword"]);

                    string path = string.Format("{0}{1}", ConfigurationManager.AppSettings["ProgettiESoluzioniPath"],
                        fileName);
                    Salva(file, path);

                    DataTable dt = ImportToMem(path);

                    TimbraturaCollection timbrature = new TimbraturaCollection();

                    foreach (DataRow row in dt.Rows)
                    {
                        Timbratura timbratura = new Timbratura
                        {
                            CodiceFiscale = row["Codice fiscale"].ToString(),
                            CodiceRilevatore = row["Codice terminale POS"].ToString()
                        };

                        DateTime dataAccesso = DateTime.Parse(row["Data accesso"].ToString());
                        DateTime oraAccesso = DateTime.Parse(row["Ora accesso"].ToString());

                        DateTime data = new DateTime(dataAccesso.Year, dataAccesso.Month, dataAccesso.Day,
                            oraAccesso.Hour,
                            oraAccesso.Minute, oraAccesso.Second);

                        timbratura.DataOra = data;

                        timbratura.IngressoUscita = int.Parse(row["Tipo accesso"].ToString()) == 1;

                        timbratura.RagioneSociale = "Progetti e soluzioni";

                        timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.ProgettiSoluzioni,
                            timbratura.CodiceRilevatore, timbratura.DataOra);

                        if (timbratura.IdCantiere != -1)
                            timbratura.PartitaIvaImpresa =
                                _biz.GetImpresaByIdCantiereCodFiscLav(timbratura.IdCantiere.Value,
                                    timbratura.CodiceFiscale, timbratura.DataOra);

                        if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.ProgettiSoluzioni) == -1)
                            timbrature.Add(timbratura);
                    }

                    ret = ret + _biz.InsertTimbrature(timbrature, TipologiaFornitore.ProgettiSoluzioni);
                }

                if (fileList.Count == 0)
                    ret = -1;

                return ret;
            }
            catch
            {
                return -2;
            }
        }
    }
}