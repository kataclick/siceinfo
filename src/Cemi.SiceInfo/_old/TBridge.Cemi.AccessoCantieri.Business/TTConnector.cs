using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TimbraturaTT = TBridge.Cemi.AccessoCantieri.TTProvider.Timbratura;

namespace TBridge.Cemi.AccessoCantieri.Business
{
    public class TTConnector
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        public int CaricaTimbrature(TimbraturaTT[] timbratureTT)
        {
            int ret;

            TimbraturaCollection timbrature = new TimbraturaCollection();

            foreach (TimbraturaTT t in timbratureTT)
            {
                try
                {
                    Timbratura timbratura = new Timbratura
                    {
                        CodiceFiscale = t.CodiceFiscaleLavoratore.ToUpper().Trim(),
                        CodiceRilevatore = t.IdentificativoLettore.Trim(),
                        DataOra = DateTime.Parse(t.DataOra)
                    };

                    if (t.Ingresso.ToString().Trim() == "0")
                        timbratura.IngressoUscita = true;
                    else
                        timbratura.IngressoUscita = false;

                    timbratura.RagioneSociale = "Tre Torri";
                    //timbratura.Latitudine = Decimal.Parse(timbratureSelesta[i].latitudine);
                    //timbratura.Longitudine = Decimal.Parse(timbratureSelesta[i].longitudine);

                    //return - 2;

                    if (!string.IsNullOrEmpty(t.CodiceFiscaleImpresa))
                    {
                        timbratura.CodiceFiscaleImpresa = t.CodiceFiscaleImpresa.Trim();
                        timbratura.PartitaIvaImpresa = t.CodiceFiscaleImpresa.Trim();
                    }
                    timbratura.IdCantiere = _biz.GetIdCantiere(TipologiaFornitore.TreTorri, timbratura.CodiceRilevatore,
                        timbratura.DataOra);

                    if (_biz.GetIdTimbratura(timbratura, TipologiaFornitore.TreTorri) == -1)
                        timbrature.Add(timbratura);
                }
                catch (Exception)
                {
                }
            }

            if (timbrature.Count == 0)
                ret = -1;
            else
                ret = _biz.InsertTimbrature(timbrature, TipologiaFornitore.TreTorri);

            return ret;
        }
    }
}