using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Reporting;
using TBridge.Cemi.Data;
using TBridge.Cemi.IscrizioneCE.Type.Filters;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Entities.IscrizioneCe;
using TBridge.Cemi.Type.Enums.IscrizioneCe;
using Common = TBridge.Cemi.Business.Common;

namespace TBridge.Cemi.IscrizioneCE.Business
{
    public class IscrizioniManager
    {
        private readonly TSBusiness _bizTuteScarpe = new TSBusiness();
        private readonly IscrizioneCEDataAccess _dataAccess = new IscrizioneCEDataAccess();

        public bool PartitaIvaEsistente(string partitaIva)
        {
            bool res = true;

            try
            {
                res = _dataAccess.PartitaIvaEsistente(partitaIva);
            }
            catch
            {
            }

            return res;
        }

        public bool CodiceFiscaleEsistente(string codiceFiscale)
        {
            bool res = true;

            try
            {
                res = _dataAccess.CodiceFiscaleEsistente(codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

        public bool PartitaIvaEsistenteDomande(string partitaIva, int? idDomanda)
        {
            bool res = true;

            try
            {
                res = _dataAccess.PartitaIvaEsistenteDomande(partitaIva, idDomanda);
            }
            catch
            {
            }

            return res;
        }

        public bool CodiceFiscaleEsistenteDomande(string codiceFiscale, int? idDomanda)
        {
            bool res = true;

            try
            {
                res = _dataAccess.CodiceFiscaleEsistenteDomande(codiceFiscale, idDomanda);
            }
            catch
            {
            }

            return res;
        }

        public int? LegaleRappresentanteInAnagrafica(string cognome, string nome, DateTime dataNascita,
            string codiceFiscale)
        {
            int? res = null;

            try
            {
                res = _dataAccess.LegaleRappresentanteInAnagrafica(cognome, nome, dataNascita, codiceFiscale);
            }
            catch
            {
            }

            return res;
        }

/*
        private bool BancaEsistente(Banca banca)
        {
            bool res = true;

            try
            {
                res = dataAccess.BancaEsistente(banca);
            }
            catch
            {
            }

            return res;
        }
*/

/*
        private bool CapInAnagrafica(string codiceCatastale, string frazione, string cap)
        {
            bool res = false;

            try
            {
                res = dataAccess.CapInAnagrafica(codiceCatastale, frazione, cap);
            }
            catch
            {
            }

            return res;
        }
*/

        public bool InsertIscrizione(ModuloIscrizione iscrizione, string wsReporting2005, string wsExecution2005,
            bool modificaAbilitata)
        {
            return _dataAccess.InsertUpdateIscrizione(iscrizione, modificaAbilitata);
        }

        public List<ModuloIscrizione> GetDomande(ModuloIscrizioneFilter filtro, TipoOrdinamento? ordinamento)
        {
            var domande = _dataAccess.GetDomande(filtro, ordinamento);

            return domande;
        }

        public ModuloIscrizione GetDomanda(int idDomanda)
        {
            var domanda = _dataAccess.GetDomanda(idDomanda);

            return domanda;
        }

        public bool ControllaDomanda(int idDomanda, StringCollection listaErroriBloc,
            StringCollection listaErroriNonBloc)
        {
            bool blocco = false;
            ModuloIscrizione domanda = GetDomanda(idDomanda);

            if (listaErroriBloc == null)
                listaErroriBloc = new StringCollection();

            #region DATI GENERALI

            // Controllo codice fiscale e partita iva nelle imprese e nelle domande valide (BLOC)
            if (CodiceFiscaleEsistente(domanda.CodiceFiscale))
            {
                AggiungiAllaListaErrori("Codice fiscale gi� presente nelle imprese registrate", listaErroriBloc);
                blocco = true;
            }
            if (PartitaIvaEsistente(domanda.PartitaIva))
            {
                AggiungiAllaListaErrori("Partita IVA gi� presente nelle imprese registrate", listaErroriBloc);
                blocco = true;
            }
            if (CodiceFiscaleEsistenteDomande(domanda.CodiceFiscale, idDomanda))
            {
                AggiungiAllaListaErrori("Codice fiscale gi� presente nelle domande accettate", listaErroriBloc);
                blocco = true;
            }
            if (PartitaIvaEsistenteDomande(domanda.PartitaIva, idDomanda))
            {
                AggiungiAllaListaErrori("Partita IVA gi� presente nelle domande accettate", listaErroriBloc);
                blocco = true;
            }

            //// Organizzazione imprenditoriale selezionata (BLOC)
            //if (string.IsNullOrEmpty(domanda.CodiceOrganizzazioneImprenditoriale))
            //{
            //    AggiungiAllaListaErrori("Organizzazione imprenditoriale non presente nelle anagrafiche", listaErroriBloc);
            //    blocco = true;
            //}

            // Tipo Impresa selezionato (BLOC)
            if (string.IsNullOrEmpty(domanda.CodiceTipoImpresa))
            {
                AggiungiAllaListaErrori("Tipo impresa non presente nelle anagrafiche", listaErroriBloc);
                blocco = true;
            }

            // Forma giuridica selezionata (BLOC)
            if (string.IsNullOrEmpty(domanda.CodiceNaturaGiuridica))
            {
                AggiungiAllaListaErrori("Natura giuridica non presente nelle anagrafiche", listaErroriBloc);
                blocco = true;
            }

            // Codice attivit� selezionata (BLOC)
            if (string.IsNullOrEmpty(domanda.CodiceAttivitaISTAT))
            {
                AggiungiAllaListaErrori("Attivit� ISTAT non presente nelle anagrafiche", listaErroriBloc);
                blocco = true;
            }

            // Invio denunce consulente e consulente non selezionato (BLOC)
            if (domanda.IdTipoInvioDenuncia == "C" && domanda.Consulente == null)
            {
                AggiungiAllaListaErrori("Consulente non selezionato", listaErroriBloc);
                blocco = true;
            }

            #endregion

            #region SEDE LEGALE

            // Controllo correttezza indirizzo
            if (!domanda.GeocodificaOkSedeLegale)
                AggiungiAllaListaErrori("L'indirizzo fornito per la sede legale non � stato riconosciuto dal sistema",
                    listaErroriNonBloc);

            // CAP in anagrafica (comuni e frazioni) (BLOC)
            //if (!CapInAnagrafica(domanda.SedeLegale.Comune, domanda.SedeLegale.Frazione, domanda.SedeLegale.Cap))
            //    AggiungiAllaListaErrori("Il CAP fornito per la sede legale non � presente nell'anagrafica",
            //                            listaErroriBloc);

            #endregion

            #region SEDE AMMINISTRATIVA

            if (!string.IsNullOrEmpty(domanda.SedeAmministrativa.Indirizzo))
            {
                // Controllo correttezza indirizzo
                if (!domanda.GeocodificaOkSedeAmministrativa)
                    AggiungiAllaListaErrori(
                        "L'indirizzo fornito per la sede amministrativa non � stato riconosciuto dal sistema",
                        listaErroriNonBloc);
            }

            // CAP in anagrafica (comuni e frazioni) (BLOC)
            //if (!string.IsNullOrEmpty(domanda.SedeAmministrativa.Comune) &&
            //    !string.IsNullOrEmpty(domanda.SedeAmministrativa.Cap))
            //    if (
            //        !CapInAnagrafica(domanda.SedeAmministrativa.Comune, domanda.SedeAmministrativa.Frazione,
            //                         domanda.SedeAmministrativa.Cap))
            //        AggiungiAllaListaErrori("Il CAP fornito per la sede amministrativa non � presente nell'anagrafica",
            //                                listaErroriBloc);

            #endregion

            #region BANCA

            //// Controllo presenza per ABI CAB (BLOC)
            //if (!BancaEsistente(domanda.Banca))
            //{
            //    AggiungiAllaListaErrori(
            //        "La banca non � presente nell'anagrafica, la domanda non pu� essere accettata finch� non sar� inserita",
            //        listaErroriBloc);
            //    blocco = true;
            //}

            //if (banche != null && banche.Length == 2)
            //{
            //    banche[0] = domanda.Banca;
            //    banche[1] = GetBancaSiceNew(domanda.Banca.Abi, domanda.Banca.Cab);
            //}

            // Paese selezionato (non obbligatorio)

            #endregion

            #region LEGALE RAPPRESENTANTE

            // Controllo presenza in anagrafica (cognome, nome, datanascita, codicefiscale)
            int? idLavoratore =
                LegaleRappresentanteInAnagrafica(domanda.LegaleRappresentante.Cognome,
                    domanda.LegaleRappresentante.Nome,
                    domanda.LegaleRappresentante.DataNascita,
                    domanda.LegaleRappresentante.CodiceFiscale);
            if (idLavoratore.HasValue)
            {
                AggiungiAllaListaErrori(
                    "Il legale rappresentante risulta essere gi� presente nelle anagrafiche. Controllarne la posizione",
                    listaErroriNonBloc);
                domanda.LegaleRappresentante.IdLavoratore = idLavoratore.Value;
            }

            // CAP in anagrafica (comuni e frazioni) (BLOC)
            //if (
            //    !CapInAnagrafica(domanda.LegaleRappresentante.IndirizzoComune,
            //                     domanda.LegaleRappresentante.IndirizzoFrazione,
            //                     domanda.LegaleRappresentante.IndirizzoCAP))
            //    AggiungiAllaListaErrori(
            //        "Il CAP fornito per la residenza del legale rappresentante non � presente nell'anagrafica",
            //        listaErroriBloc);

            // Codice fiscale del legale rappresentante (NON BLOC)
            try
            {
                if (!CodiceFiscaleManager.VerificaCodiceFiscale(domanda.LegaleRappresentante.Nome,
                    domanda.LegaleRappresentante.Cognome,
                    domanda.LegaleRappresentante.Sesso,
                    domanda.LegaleRappresentante.DataNascita,
                    domanda.LegaleRappresentante.LuogoNascita,
                    domanda.LegaleRappresentante.CodiceFiscale))
                    AggiungiAllaListaErrori(
                        "Il codice fiscale fornito per il legale rappresentante potrebbe non essere corretto",
                        listaErroriNonBloc);
            }
            catch
            {
                AggiungiAllaListaErrori(
                    "Non � stato possibile controllare la validit� del codice fiscale",
                    listaErroriNonBloc);
            }

            #endregion

            #region CANTIERE

            if (domanda.TipoModulo == TipoModulo.Operai)
            {
                // Controllo correttezza indirizzo
                if (!domanda.GeocodificaOkCantiere)
                    AggiungiAllaListaErrori("L'indirizzo fornito per il cantiere non � stato riconosciuto dal sistema",
                        listaErroriNonBloc);

                // Controllo presenza cantiere nelle notifiche
                if (!domanda.CantiereInNotifiche)
                    AggiungiAllaListaErrori("Il cantiere non � stato trovato tra le notifiche preliminari presenti",
                        listaErroriNonBloc);

                // CAP in anagrafica (comuni e frazioni) (BLOC)
                //if (!CapInAnagrafica(domanda.Cantiere.Comune, domanda.Cantiere.Frazione, domanda.Cantiere.Cap))
                //    AggiungiAllaListaErrori("Il CAP fornito per il cantiere non � presente nell'anagrafica", listaErroriBloc);
            }

            #endregion

            if (listaErroriBloc.Count > 0)
                blocco = true;

            return blocco;
        }

        public byte[] CreaModuloPdfIscrizioneCE(int idModulo, TipoModulo tipoModulo, string wsReporting2005,
            string wsExecution2005, string reportNormale, string reportPrevedi)
        {
            BusinessReporting businessReporting = new BusinessReporting();

            BusinessReporting.ParameterValue[] parameters = new BusinessReporting.ParameterValue[1];

            parameters[0] = new BusinessReporting.ParameterValue();
            parameters[0].Label = "idDomanda";
            parameters[0].Name = "idDomanda";
            parameters[0].Value = idModulo.ToString();

            byte[] results;
            if (tipoModulo == TipoModulo.Operai)
                results = businessReporting.CreaPdfReport(wsReporting2005, wsExecution2005,
                    reportNormale, parameters);
            else
                results = businessReporting.CreaPdfReport(wsReporting2005, wsExecution2005,
                    reportPrevedi,
                    parameters);

            return results;
        }

        public Banca GetBancaSiceNew(string abi, string cab)
        {
            Banca banca = null;

            try
            {
                int iAbi;
                int iCab;

                if (int.TryParse(abi, out iAbi) && int.TryParse(cab, out iCab))
                {
                    banca = _dataAccess.GetBancaSiceNew(iAbi, iCab);
                }
            }
            catch
            {
            }

            return banca;
        }

        private static void AggiungiAllaListaErrori(string errore, StringCollection errori)
        {
            if (errori != null)
                errori.Add(errore);
        }

        public bool RifiutaDomanda(int idDomanda, int idUtente)
        {
            return _dataAccess.CambiaStatoDomanda(idDomanda, StatoDomanda.Rifiutata, null, idUtente);
        }

        public bool AccettaDomanda(int idDomanda, string idTipoIscrizione, int idUtente)
        {
            return _dataAccess.CambiaStatoDomanda(idDomanda, StatoDomanda.Accettata, idTipoIscrizione, idUtente);
        }

        public bool AttendiDocumentazioneDomanda(int idDomanda, int idUtente)
        {
            return _dataAccess.CambiaStatoDomanda(idDomanda, StatoDomanda.SospesaAttesaIntegrazione, null, idUtente);
        }

        public bool SituazioneDebitoriaDomanda(int idDomanda, int idUtente)
        {
            return _dataAccess.CambiaStatoDomanda(idDomanda, StatoDomanda.SospesaDebiti, null, idUtente);
        }

        public bool ConfrontaBanche(Banca banca1, Banca banca2)
        {
            bool res = false;

            if (banca1 != null && banca2 != null)
            {
                if (banca1.Abi == banca2.Abi
                    && banca1.Cab == banca2.Cab
                    && banca1.Banca1 == banca2.Banca1
                    && banca1.Agenzia == banca2.Agenzia)
                    res = true;
            }

            return res;
        }

        public bool DeleteDomanda(int idDomanda)
        {
            bool res = false;

            try
            {
                res = _dataAccess.DeleteDomanda(idDomanda);
            }
            catch
            {
            }

            return res;
        }

        public string ConvertiNaturaGiuridicaInTipoImpresa(string naturaGiuridica)
        {
            string tipoImpresa = null;

            switch (naturaGiuridica)
            {
                case "01":
                    tipoImpresa = "A";
                    break;
                case "02":
                    tipoImpresa = "I";
                    break;
                case "03":
                    tipoImpresa = "I";
                    break;
                case "04":
                    tipoImpresa = "I";
                    break;
                case "05":
                    tipoImpresa = "I";
                    break;
                case "06":
                    tipoImpresa = "I";
                    break;
                case "07":
                    tipoImpresa = "I";
                    break;
                case "08":
                    tipoImpresa = "C";
                    break;
            }

            return tipoImpresa;
        }

        public List<TipoIscrizione> GetTipiIscrizione(string idTipoImpresa)
        {
            return _dataAccess.GetTipiIscrizione(idTipoImpresa);
        }

        public bool IsGrandeCitta(string codiceCatastale)
        {
            bool res = false;

            switch (codiceCatastale)
            {
                // Bari
                case "A662":
                    res = true;
                    break;
                // Bergamo
                case "A794":
                    res = true;
                    break;
                // Bologna
                case "A944":
                    res = true;
                    break;
                // Brescia
                case "B157":
                    res = true;
                    break;
                // Cagliari
                case "B354":
                    res = true;
                    break;
                // Catania
                case "C351":
                    res = true;
                    break;
                // Firenze
                case "D612":
                    res = true;
                    break;
                // Genova
                case "D969":
                    res = true;
                    break;
                // La Spezia
                case "E463":
                    res = true;
                    break;
                // Livorno
                case "E625":
                    res = true;
                    break;
                // Messina
                case "F158":
                    res = true;
                    break;
                // Milano
                case "F205":
                    res = true;
                    break;
                // Napoli
                case "F839":
                    res = true;
                    break;
                // Padova
                case "G224":
                    res = true;
                    break;
                // Palermo
                case "G273":
                    res = true;
                    break;
                // Perugia
                case "G478":
                    res = true;
                    break;
                // Pescara
                case "G482":
                    res = true;
                    break;
                // Pisa
                case "G702":
                    res = true;
                    break;
                // Reggio di Calabria
                case "H224":
                    res = true;
                    break;
                // Roma
                case "H501":
                    res = true;
                    break;
                // Salerno
                case "H703":
                    res = true;
                    break;
                // Torino
                case "L219":
                    res = true;
                    break;
                // Trieste
                case "L424":
                    res = true;
                    break;
                // Venezia
                case "L736":
                    res = true;
                    break;
                // Verbania
                case "L746":
                    res = true;
                    break;
                // Verona
                case "L781":
                    res = true;
                    break;
            }

            return res;
        }

        public string GetPivaByIdDomanda(int idDomanda)
        {
            return _dataAccess.GetPivaByIdDomanda(idDomanda);
        }

        public string GetMailValidaPerInvio(ModuloIscrizione iscrizione)
        {
            string destinatario = string.Empty;

            switch (iscrizione.CorrispondenzaPresso)
            {
                case "L":
                    if (iscrizione.SedeLegale != null)
                    {
                        destinatario = iscrizione.SedeLegale.EMail;
                    }
                    break;
                case "A":
                    if (iscrizione.SedeAmministrativa != null)
                    {
                        destinatario = iscrizione.SedeAmministrativa.EMail;
                    }
                    break;
                case "C":
                    if (iscrizione.IdConsulente.HasValue)
                    {
                        Consulente consulente =
                            _bizTuteScarpe.GetConsulenti(iscrizione.IdConsulente, null, null, null, null)[0];
                        destinatario = consulente.EMail;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(iscrizione.EmailConsulente))
                        {
                            destinatario = iscrizione.EmailConsulente;
                        }
                    }
                    break;
            }

            // Se non ho trovato un indirizzo provo alla sede amministrativa
            if (string.IsNullOrEmpty(destinatario))
            {
                if (iscrizione.SedeAmministrativa != null)
                {
                    destinatario = iscrizione.SedeAmministrativa.EMail;
                }
            }

            return destinatario;
        }

        public string GetFrazioneDaCombo(string selectedValue)
        {
            return SplittaValoreCombo(selectedValue);
        }

        public string GetCodiceCatastaleDaCombo(string selectedValue)
        {
            return SplittaValoreCombo(selectedValue);
        }

        public string SplittaValoreCombo(string selectedValue)
        {
            string codice = null;

            string[] splitted = selectedValue.Split('|');
            if (splitted.Length > 1)
                codice = splitted[0];

            return codice;
        }

        public void UpdateIscrizioneSalvaDocumento(int idDomanda, byte[] moduli)
        {
            _dataAccess.UpdateIscrizioneSalvaDocumento(idDomanda, moduli);
        }

        public byte[] GetModuloDomanda(int idDomanda)
        {
            return _dataAccess.GetModuloDomanda(idDomanda);
        }

        #region Funzioni per il caricamento dei comuni e delle frazioni

        public void CaricaCapOFrazioni(DropDownList DropDownListLocalita, DropDownList DropDownListFrazione,
            TextBox textBoxCap)
        {
            string codiceComboComune = DropDownListLocalita.SelectedValue;
            string[] codiceSplittato = codiceComboComune.Split('|');

            if (codiceSplittato.Length > 1 && codiceSplittato[1] != null && codiceSplittato[1] != "0")
            {
                textBoxCap.Text = VerificaCapValido(codiceSplittato[0], codiceSplittato[1]);
            }
            else
            {
                textBoxCap.Text = string.Empty;
            }

            CaricaFrazioni(DropDownListFrazione, codiceSplittato[0]);
        }

        public void CaricaCap(string selectedValue, TextBox textBoxCap)
        {
            string[] codiceSplittato = selectedValue.Split('|');

            if (codiceSplittato.Length > 1 && codiceSplittato[1] != null && codiceSplittato[1] != "0")
                textBoxCap.Text = VerificaCapValido(codiceSplittato[0], codiceSplittato[1]);
        }

        public string VerificaCapValido(string codiceCatastale, string cap)
        {
            string retCap = cap;

            if (cap.Length == 5)
            {
                // Caso grande citt�
                if (IsGrandeCitta(codiceCatastale) && cap[2] == '1')
                {
                    if (cap[3] == '0' && cap[4] == '0')
                        retCap = retCap.Substring(0, 3);
                }
            }

            return retCap;
        }

        public void CaricaComuni(DropDownList dropDownListComuni, string provincia, DropDownList dropDownListFrazioni)
        {
            Common bizCommon = new Common();

            //svuotiamo e aggiumgiamo l'item vuoto
            dropDownListComuni.Items.Clear();
            dropDownListComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (dropDownListFrazioni != null)
                dropDownListFrazioni.Items.Clear();

            if (!string.IsNullOrEmpty(provincia))
            {
                List<ComuneSiceNew> comuni = bizCommon.GetComuniSiceNew(provincia);
                dropDownListComuni.DataSource = comuni;
                dropDownListComuni.DataTextField = "Comune";
                dropDownListComuni.DataValueField = "CodicePerCombo";
            }

            dropDownListComuni.DataBind();
        }

        public void CaricaFrazioni(DropDownList dropDownListFrazioni, string codiceCatastale)
        {
            Common bizCommon = new Common();

            dropDownListFrazioni.Items.Clear();
            dropDownListFrazioni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (!string.IsNullOrEmpty(codiceCatastale))
            {
                List<FrazioneSiceNew> frazioni = bizCommon.GetFrazioniSiceNew(codiceCatastale);
                dropDownListFrazioni.DataSource = frazioni;
                dropDownListFrazioni.DataTextField = "Frazione";
                dropDownListFrazioni.DataValueField = "CodicePerCombo";
            }

            dropDownListFrazioni.DataBind();
        }

        public static int IndiceComuneInDropDown(DropDownList dropDownListComuni, string codiceCatastale)
        {
            int res = 0;

            for (int i = 0; i < dropDownListComuni.Items.Count; i++)
            {
                string valoreCombo = dropDownListComuni.Items[i].Value;
                string[] splitted = valoreCombo.Split('|');
                if (splitted.Length > 1)
                {
                    if (splitted[0] == codiceCatastale)
                    {
                        res = i;
                        break;
                    }
                }
            }

            return res;
        }

        public static int IndiceFrazioneInDropDown(DropDownList dropDownListFrazioni, string frazione)
        {
            int res = 0;

            for (int i = 0; i < dropDownListFrazioni.Items.Count; i++)
            {
                string valoreCombo = dropDownListFrazioni.Items[i].Value;
                string[] splitted = valoreCombo.Split('|');
                if (splitted.Length > 1)
                {
                    if (splitted[0] == frazione)
                    {
                        res = i;
                        break;
                    }
                }
            }

            return res;
        }

        #endregion
    }
}