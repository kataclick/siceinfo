using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using TBridge.Cemi.Business.SmsServiceReference;
using TBridge.Cemi.TuteScarpe.Data;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.TuteScarpe;
using Impresa = TBridge.Cemi.Type.Entities.Impresa;
using Indirizzo = TBridge.Cemi.TuteScarpe.Data.Entities.Indirizzo;

namespace TBridge.Cemi.TuteScarpe.Business
{
    public class TSBusiness
    {
        private readonly TSDataAccess _tsData = new TSDataAccess();

        public DataTable ElencoLavoratori(int idUtente, bool? completati)
        {
            DataTable dt = _tsData.ElencoLavoratori(idUtente, completati);

            return dt;
        }

        public DataTable ElencoLavoratoriImpresa(int idImpresa, bool? completati)
        {
            DataTable dt = _tsData.ElencoLavoratoriImpresa(idImpresa, completati);

            return dt;
        }

        public bool EsisteFabbisogno(int idImpresa)
        {
            return _tsData.EsisteFabbisogno(idImpresa);
        }

        public DataTable getLegendaTaglie()
        {
            return _tsData.GetLegendaTaglie();
        }

        public DataTable getGruppiForPrint(int idImpresa)
        {
            return _tsData.GetSoluzioniVestiarioForPrint(idImpresa);
        }

        public DataTable getGruppiSelezionati(int idImpresa)
        {
            return _tsData.GetSoluzioniVestiarioSelezionato(idImpresa);
        }

        public void PulisciOrdineTemporaneo(int idUtente)
        {
            _tsData.PulisciOrdineTemporaneo(idUtente);
        }

        public int MemorizzaForzaAsfaltista(int idUtente)
        {
            return _tsData.MemorizzaForzaAsfaltista(idUtente);
        }

        public DataTable GetLavoratoriIncompleti(int idImpresa)
        {
            return _tsData.GetLavoratoriIncompleti(idImpresa);
        }

        public DataTable GetStorico(int idImpresa)
        {
            return _tsData.GetStorico(idImpresa);
        }

        public DataTable GetFabbisogniNonGestiti(int idImpresa)
        {
            return _tsData.GetFabbisogniNonGestiti(idImpresa);
        }

        public DataTable GetFabbisogniConfermatiDettagliComeXML(int idFabbisognoConfermato, int idImpresa)
        {
            return _tsData.GetFabbisogniConfermatiDettagliComeXML(idFabbisognoConfermato, idImpresa);
        }

        public DataTable GetOrdine(int idStoricoTS, string pathSchema)
        {
            // Vecchio storico

            DataTable dtRet = null;
            DataTable dtAux;

            try
            {
                dtAux = _tsData.GetOrdine(idStoricoTS, null);
                if (dtAux != null && dtAux.Rows.Count > 0)
                {
                    dtRet = new DataTable();
                    dtRet.ReadXmlSchema(pathSchema);

                    string xml = dtAux.Rows[0]["ordine"].ToString();
                    TextReader sr = new StringReader(xml);
                    dtRet.ReadXml(sr);
                }
            }
            catch
            {
            }

            return dtRet;
        }

        public Ordine GetOrdine(int idOrdine)
        {
            return _tsData.GetOrdine(idOrdine);
        }

        public int SaveStorico(int impresa, Indirizzo indirizzo, int? idConsulente, int? idFornitore)
        {
            _tsData.ConfermaFabbisogno(impresa, indirizzo, idConsulente, idFornitore);
            return 1;
        }

        /// <summary>
        ///     Elimina un lavoratore dalla lista dei lavoratori con diritto ad un fabbisogno
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="idLavoratore"></param>
        /// <returns></returns>
        public int EliminaLavoratore(int idImpresa, int idLavoratore)
        {
            return _tsData.EliminaLavoratore(idImpresa, idLavoratore);
        }

        /// <summary>
        ///     Ripristina un lavoratore eliminato dalla lista dei lavoratori con diritto ad un fabbisogno
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="idLavoratore"></param>
        /// <returns></returns>
        public int RipristinaLavoratore(int idImpresa, int idLavoratore)
        {
            return _tsData.RipristinaLavoratore(idImpresa, idLavoratore);
        }

        public Indirizzo GetIndirizzoAmministrativo(int idImpresa)
        {
            return _tsData.GetIndirizzoAmministrativo(idImpresa);
        }

        public DataTable GetPreIndirizzi()
        {
            return _tsData.GetPreIndirizzi();
        }

        public DataTable GetProvince()
        {
            return _tsData.GetProvince();
        }

        public DataTable GetComuniDellaProvincia(int provincia)
        {
            return _tsData.GetComuniDellaProvincia(provincia);
        }

        public int InsertIndirizzoOrdine(string preIndirizzo, string indirizzo, string provincia, string comune,
            string cap)
        {
            return _tsData.InserisciIndirizzoOrdine(preIndirizzo, indirizzo, provincia, comune, cap);
        }

        public DataTable GetCAPDelComune(long idComune)
        {
            return _tsData.GetCAPDelComune(idComune);
        }

        public FabbisognoDettaglioList GetFabbisognoDettaglio(int idImpresa, int idFabbisognoComplessivo)
        {
            return _tsData.GetFabbisogniConfermatiDettagli(idImpresa, idFabbisognoComplessivo);
        }

        public FabbisognoDettaglioList GetFabbisognoDettaglio(int idFabbisognoConfermato)
        {
            return _tsData.GetFabbisogniConfermatiDettagli(idFabbisognoConfermato);
        }

        public void AggiornaDettagliFabbisognoDettagli(int idFabbisognoConfermatoDettaglio, string taglia)
        {
            _tsData.AggiornaDettaglioFabbisogno(idFabbisognoConfermatoDettaglio, taglia);
        }

        public FabbisognoComplessivoList GetFabbisogniComplessivi()
        {
            return _tsData.GetFabbisogniComplessivi();
        }

        public FabbisognoList GetDettagliOrdine(Ordine ordine)
        {
            return GetDettagliOrdine(ordine.IdOrdine);
        }

        public FabbisognoList GetDettagliOrdine(int idOrdine)
        {
            try
            {
                return _tsData.GetDettagliOrdine(idOrdine);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetDettagliOrdine", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return null;
            }
        }

        public void OrdineScaricatoDaFornitore(int idOrdine)
        {
            _tsData.OrdineScaricatoDaFornitore(idOrdine);
        }

        public void RimuoviFabbisognoDaOrdine(int idFabbisogno)
        {
            _tsData.RimuoviFabbisognoDaOrdine(idFabbisogno);
        }

        public void AggiungiFabbisognoAOrdine(int idFabbisogno, int idOrdine)
        {
            _tsData.AggiungiFabbisognoAOrdine(idFabbisogno, idOrdine);
        }

        public int GetFabbisognoComplessivoOrdine(int idOrdine)
        {
            try
            {
                return _tsData.GetFabbisognoComplessivoOrdine(idOrdine);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetFabbisognoComplessivoOrdine", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return -1;
            }
        }

        public Fattura CreaFattura(Stream fileFattura, int numeroFattura, string descrizione, string codiceFornitore)
        {
            Fattura fattura;

            try
            {
                StreamReader sr = new StreamReader(fileFattura);

                try
                {
                    Bolla bolla = null;
                    int numeroLinea = 1;

                    // TO DO, METTERE IL NUMERO NEL TITOLO DEL FILE
                    byte[] fileFat = new byte[fileFattura.Length];
                    fileFattura.Read(fileFat, 0, (int) fileFattura.Length);
                    fileFattura.Seek(0, SeekOrigin.Begin);

                    fattura = new Fattura(descrizione, DateTime.Now, codiceFornitore, fileFat);

                    // Parse del file
                    while (!sr.EndOfStream)
                    {
                        string linea = sr.ReadLine();

                        string codiceImpresaS;
                        int codiceImpresa;
                        string idOrdineS;
                        int idOrdine;
                        string tipoVestiario;
                        string dataS;
                        DateTime data;
                        string quantitaS;
                        int quantita;
                        string numeroFatturaS;
                        int numeroFatturaFile;

                        bool resCodImp, resCodOrd, resQuant, resNumFat, resControlloFat, resData;

                        codiceImpresaS = linea.Substring(0, 6);
                        //idOrdineS = linea.Substring(5, 2);
                        idOrdineS = linea.Substring(6, 5);
                        //tipoVestiario = linea.Substring(7, 3);
                        tipoVestiario = linea.Substring(11, 6);
                        //dataS = linea.Substring(13, 8);
                        dataS = linea.Substring(17, 8);
                        //quantitaS = linea.Substring(48, 5);
                        quantitaS = linea.Substring(52, 5);
                        //numeroFatturaS = linea.Substring(53, 5);
                        numeroFatturaS = linea.Substring(57, 5);

                        try
                        {
                            data = ConvertiData(dataS);
                            resData = true;
                        }
                        catch
                        {
                            data = DateTime.Now;
                            resData = false;
                        }

                        resCodImp = int.TryParse(codiceImpresaS, out codiceImpresa);
                        resCodOrd = int.TryParse(idOrdineS, out idOrdine);
                        resQuant = int.TryParse(quantitaS, out quantita);
                        resNumFat = int.TryParse(numeroFatturaS, out numeroFatturaFile);
                        resControlloFat = numeroFattura == numeroFatturaFile;
                        tipoVestiario = tipoVestiario.Trim();

                        if (resCodImp && resCodOrd && resQuant && resNumFat && resControlloFat && resData)
                        {
                            if (bolla == null || bolla.IdImpresa != codiceImpresa || idOrdine != bolla.IdOrdine)
                            {
                                // Nuova bolla
                                bolla = new Bolla(codiceImpresa, idOrdine, data);
                                fattura.Bolle.Add(bolla);
                            }

                            bolla.AggiungiVoceBolla(new VoceBolla(tipoVestiario, quantita));
                        }
                        else
                        {
                            // IL FILE NON HA IL FORMATO CORRETTO
                            // NON E' POSSIBILE ACCETTARLO
                            if (!resCodImp)
                                throw new ArgumentException("Linea " + numeroLinea +
                                                            ": Campo codice impresa errato");
                            if (!resCodOrd)
                                throw new ArgumentException("Linea " + numeroLinea +
                                                            ": Campo codice ordine errato");
                            if (!resQuant)
                                throw new ArgumentException("Linea " + numeroLinea +
                                                            ": Campo quantit� errato");
                            if (!resNumFat)
                                throw new ArgumentException("Linea " + numeroLinea +
                                                            ": Campo numero fattura errato");
                            if (!resControlloFat)
                                throw new ArgumentException("Linea " + numeroLinea +
                                                            ": Campo numero fattura non coerente con nome file");
                            if (!resData) // E' SEMPRE TRUE!!!
                                throw new ArgumentException("Linea " + numeroLinea + ": Campo data errato");
                        }

                        numeroLinea++;
                    }

                    //tsData.InserisciFattura(fattura);
                }
                catch (ArgumentException aexc)
                {
                    throw;
                }
                catch (Exception exc)
                {
                    throw new ArgumentException("Errore generico nel formato del file", exc);
                }
                finally
                {
                    sr.Close();
                }
            }
            catch (ArgumentException aexc)
            {
                throw;
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.CreaFattura", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                throw;
            }

            return fattura;
        }


        public Fattura CreaFatturaDalDB(int idFattura)
        {
            Fattura fattura = null;

            try
            {
                fattura = _tsData.GetFattura(idFattura);
                BollaList listaBolle = _tsData.GetFatturaBolle(idFattura);
                fattura.Bolle = listaBolle;

                foreach (Bolla bolla in listaBolle)
                {
                    VoceBollaList listaVoci = _tsData.GetVociBolla(bolla.IdBolla, bolla.IdOrdine, bolla.IdImpresa);
                    if (listaVoci != null)
                        foreach (VoceBolla voce in listaVoci)
                            bolla.AggiungiVoceBolla(voce);
                }
            }
            catch
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.CreaFatturaDalDB", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);
            }

            return fattura;
        }


        public bool InserisciFattura(Fattura fattura)
        {
            return _tsData.InserisciFattura(fattura);
        }

        public bool DeleteFattura(int idFattura)
        {
            bool res = false;

            try
            {
                _tsData.DeleteFattura(idFattura);
                res = true;
            }
            catch
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.DeleteFattura", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);
            }

            return res;
        }

        public bool ForzaFattura(int idFattura)
        {
            bool res = false;

            try
            {
                _tsData.ForzaFattura(idFattura);
                res = true;
            }
            catch
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.ForzaFattura", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);
            }

            return res;
        }

        public void VerificaFattura(Fattura fattura)
        {
            if (fattura != null && fattura.Bolle != null)
            {
                foreach (Bolla bolla in fattura.Bolle)
                {
                    if (bolla.VociBolla != null)
                    {
                        // Recupre le richieste del fabbisogno
                        foreach (VoceBolla voce in bolla.VociBolla)
                        {
                            int richiestaReale =
                                GetRichiestaFabbisogno(bolla.IdOrdine, bolla.IdImpresa, voce.TipoVestiario);
                            if (richiestaReale != -1)
                                voce.QuantitaFabbisogno = richiestaReale;
                        }

                        // Aggiungo voci presenti nel fabbisogno ma non nella fattura
                        TipoVestiarioList listaVestiario =
                            GetTipiVestiarioInFabbisogno(bolla.IdOrdine, bolla.IdImpresa);
                        foreach (TipoVestiario tipoVestiario in listaVestiario)
                        {
                            if (!bolla.TipiVestiario.Contains(tipoVestiario.NomeVestiario))
                                bolla.AggiungiVoceBolla(
                                    new VoceBolla(tipoVestiario.NomeVestiario, 0, tipoVestiario.Quantita));
                        }

                        // Controllo esistenza impresa
                        string ragioneSociale = GetImpresaRagioneSociale(bolla.IdImpresa);
                        if (!string.IsNullOrEmpty(ragioneSociale))
                        {
                            bolla.RagioneSociale = ragioneSociale;
                        }
                        else
                        {
                            bolla.Stato.Add(new StatoBollaClass(StatoBolla.ImpresaNonPresente));
                        }

                        // Controllo esistenza ordine
                        BitArray bits = EsisteOrdine(bolla.IdOrdine, fattura.CodiceFornitore);
                        if (bits != null)
                        {
                            if (!bits[0])
                                // L'ordine non esiste
                                bolla.Stato.Add(new StatoBollaClass(StatoBolla.OrdineNonPresente));
                            else if (!bits[1])
                                // L'ordine esiste ma � di un altro fornitore
                                bolla.Stato.Add(new StatoBollaClass(StatoBolla.OrdineAltroFornitore));
                            else
                            {
                                // Controllo che la bolla non sia gi� stata gestita
                                if (
                                    _tsData.BollaGiaGestita(bolla.IdOrdine, bolla.IdImpresa, fattura.CodiceFornitore,
                                        fattura.DataUpload))
                                    bolla.Stato.Add(new StatoBollaClass(StatoBolla.GiaGestita));
                            }
                        }
                    }

                    // Controllo quantit�
                    bolla.AggiornaStato();
                }
            }
        }

        public string GetImpresaRagioneSociale(int idImpresa)
        {
            return _tsData.GetImpresaRagioneSociale(idImpresa);
        }

        public int GetRichiestaFabbisogno(int idOrdine, int idImpresa, string tipoVestiario)
        {
            int res = -1;

            try
            {
                return _tsData.GetRichiestaFabbisogno(idOrdine, idImpresa, tipoVestiario);
            }
            catch
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetRichiestaFabbisogno", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);
            }

            return res;
        }

        public TipoVestiarioList GetTipiVestiarioInFabbisogno(int idOrdine, int idImpresa)
        {
            return _tsData.GetTipiVestiarioInFabbisogno(idOrdine, idImpresa);
        }

        /// <summary>
        ///     Converte in DateTime una data in formato yyyymmdd
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public DateTime ConvertiData(string data)
        {
            try
            {
                string annos = data.Substring(0, 4);
                int anno;
                string meses = data.Substring(4, 2);
                int mese;
                string giornos = data.Substring(6, 2);
                int giorno;

                if (int.TryParse(annos, out anno) && int.TryParse(meses, out mese) &&
                    int.TryParse(giornos, out giorno))
                {
                    DateTime res = new DateTime(anno, mese, giorno);

                    return res;
                }
                throw new Exception();
            }
            catch (Exception)
            {
                throw new ArgumentException();
            }
        }

        public FatturaList GetFatture(DateTime dal, DateTime al, bool? corrette, bool? forzate)
        {
            return _tsData.GetFatture(dal, al, corrette, forzate);
        }

        public byte[] GetFileDellaFattura(int idFattura)
        {
            return _tsData.GetFileDellaFattura(idFattura);
        }

        public BitArray EsisteOrdine(int idOrdine, string codiceFornitore)
        {
            return _tsData.EsisteOrdine(idOrdine, codiceFornitore);
        }

        public bool EsisteFattura(int codiceFattura, string codiceFornitore)
        {
            return _tsData.FatturaGiaPresente(codiceFattura, codiceFornitore);
        }

        public FatturaRiassunto GetRiassuntoFattura(int idFattura)
        {
            return _tsData.GetRiassuntoFattura(idFattura);
        }

        public Indirizzo GetIndirizzoSpedizione(int idImpresa, int idFabbisognoComplessivo)
        {
            return _tsData.GetIndirizzoSpedizione(idImpresa, idFabbisognoComplessivo);
        }

        public bool AggiornaIndirizzoSpedizione(int idImpresa, int idFabbisognoComplessivo, Indirizzo indirizzoNuovo)
        {
            return _tsData.AggiornaIndirizzoSpedizione(idImpresa, idFabbisognoComplessivo, indirizzoNuovo);
        }

        public FabbisognoComplessivoReale GetUltimoFabbisognoComplessivo()
        {
            return _tsData.GetUltimoFabbisognoComplessivo();
        }

        public bool AggiornaScadenzaFabbisogni(int idFabbisognoComplessivo, DateTime? dataScadenza,
            DateTime? dataConfermaAutomatica)
        {
            return _tsData.AggiornaScadenzaFabbisogni(idFabbisognoComplessivo, dataScadenza, dataConfermaAutomatica);
        }

        /// <summary>
        ///     Ritorna la data di scadenza del fabbisogno complessivo attivo
        /// </summary>
        /// <returns>Data scadenza ultimo fabbisogno</returns>
        public DateTime? UltimoFabbisognoValido()
        {
            DateTime? dataScadenza = null;

            try
            {
                dataScadenza = _tsData.UltimoFabbisognoValido();
            }
            catch
            {
            }

            return dataScadenza;
        }

        public ConsulentiCollection GetConsulenti(int? idConsulente, string ragioneSociale, string comune,
            string indirizzo, string codiceFiscale)
        {
            ConsulentiCollection consulenti = null;

            try
            {
                consulenti = _tsData.GetConsulenti(idConsulente, ragioneSociale, comune, indirizzo, codiceFiscale);
            }
            catch
            {
            }

            return consulenti;
        }

        public RapportoConsulenteImpresaCollection GetConsulenteRapportiImpresa(int idConsulente, bool? attivi)
        {
            return _tsData.GetConsulenteRapportiImpresa(idConsulente, attivi);
        }

        public RapportoFornitoreImpresaCollection GetFornitoreRapportiImpresa(int idFornitore, bool? attivi,
            int? idImpresa, string ragioneSociale)
        {
            return _tsData.GetFornitoreRapportiImpresa(idFornitore, attivi, idImpresa, ragioneSociale);
        }


        public bool InsertConsulenteRapportoImpresa(RapportoConsulenteImpresa rapporto)
        {
            bool res = false;

            try
            {
                res = _tsData.InsertConsulenteRapportoImpresa(rapporto);
            }
            catch
            {
            }

            return res;
        }

        public RapportoConsulenteImpresaCollection GetConsulenteRapportiImpresaDenunce(int idConsulente)
        {
            RapportoConsulenteImpresaCollection rapporti = null;

            try
            {
                rapporti = _tsData.GetConsulenteRapportiImpresaDenunce(idConsulente);
            }
            catch
            {
            }

            return rapporti;
        }

        public bool UpdateRapportoConsulenteImpresa(int idRapporto, DateTime? dataFine, string nota)
        {
            bool res = false;

            try
            {
                res = _tsData.UpdateRapportoConsulenteImpresa(idRapporto, dataFine, nota);
            }
            catch
            {
            }

            return res;
        }

        public int GetIdUtenteByIdImpresa(int idImpresa)
        {
            int idUtente = -1;

            try
            {
                idUtente = _tsData.GetIdUtenteByIdImpresa(idImpresa);
            }
            catch
            {
            }

            return idUtente;
        }

        public List<SmsRichiestaTs> GetElencoSmsRichiesteTs()
        {
            return _tsData.GetElencoSmsRichiesteTs();
        }

        public List<SmsRichiestaTs> GetElencoSmsFeedbackTs()
        {
            return _tsData.GetElencoSmsFeedbackTs();
        }

        public Impresa GetImpresaSelezionataConsulente(int idConsulente)
        {
            return _tsData.GetImpresaSelezionataConsulente(idConsulente);
        }

        public Impresa GetImpresaSelezionataFornitore(int idConsulente)
        {
            return _tsData.GetImpresaSelezionataFornitore(idConsulente);
        }

        public void SetImpresaSelezionataConsulente(int idConsulente, int? idImpresa)
        {
            _tsData.SetImpresaSelezionataConsulente(idConsulente, idImpresa);
        }

        public void SetImpresaSelezionataFornitore(int idFornitore, int? idImpresa)
        {
            _tsData.SetImpresaSelezionataFornitore(idFornitore, idImpresa);
        }

        public OrdineAutomaticoCollection GetOrdiniAutomatici(int? idImpresa, int? idLavoratore)
        {
            return _tsData.GetOrdiniAutomatici(idImpresa, idLavoratore);
        }

        public List<Type.Entities.TuteScarpe.Impresa> GetImpreseConOrdiniAutomatici()
        {
            return _tsData.GetImpreseConOrdiniAutomatici();
        }

        public List<Consegna> GetConsegneOrdini(int idImpresa, int idOrdine)
        {
            return _tsData.GetConsegneOrdini(idImpresa, idOrdine);
        }

        public bool InsertConsegneOrdini(Consegna ordine)
        {
            return _tsData.InsertConsegneOrdini(ordine);
        }

        public bool InsertFileConsegneOrdini(byte[] data, out int idTuteScarpeConsegnaOrdineFile)
        {
            return _tsData.InsertFileConsegneOrdini(data, out idTuteScarpeConsegnaOrdineFile);
        }

        public void MemorizzaDataInvioMailCompletamentoAutomatico()
        {
            _tsData.MemorizzaDataInvioMailCompletamentoAutomatico();
        }

        public void MemorizzaDataInvioSmsLavoratori()
        {
            _tsData.MemorizzaDataInvioSmsLavoratori();
        }

        public void InviaSmsLavoratoriNonCompletabili(string testo)
        {
            List<SmsDaInviare.Destinatario> destinatari = GetRecapitiLavoratoriNonCompletabili();

            SmsDaInviare smsDaInviare = new SmsDaInviare
            {
                Data = DateTime.Now,
                Testo = testo,
                Tipologia = TipologiaSms.TuteScarpe,
                Destinatari = destinatari.ToArray()
            };

            SmsServiceClient client = new SmsServiceClient();
            client.InviaSms(smsDaInviare);
        }

        private List<SmsDaInviare.Destinatario> GetRecapitiLavoratoriNonCompletabili()
        {
            return _tsData.GetRecapitiLavoratoriNonCompletabili();
        }

        public void MemorizzaInvioMail(int idImpresa)
        {
            _tsData.MemorizzaInvioMail(idImpresa);
        }

        #region Gestione Ordini

        public OrdineList GetOrdini(bool confermati, DateTime? dal, DateTime? al)
        {
            try
            {
                return _tsData.GetOrdini(confermati, dal, al);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetOrdini", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return null;
            }
        }

        public OrdineList GetOrdini(string codiceFornitore, bool confermati, DateTime? dal, DateTime? al,
            string ragioneSociale, int? idImpresa)
        {
            return _tsData.GetOrdini(codiceFornitore, confermati, dal, al, ragioneSociale, idImpresa);
        }

        public bool InsertVoceOrdineTemporaneo(int idUtente, int idFabbisognoCompletato)
        {
            return _tsData.InsertVoceOrdineTemporaneo(idUtente, idFabbisognoCompletato);
        }

        public void DeleteVoceOrdineTemporaneo(int idUtente, int idFabbisognoCompletato)
        {
            _tsData.DeleteVoceOrdineTemporaneo(idUtente, idFabbisognoCompletato);
        }

        public FabbisognoList SelectOrdineTemporaneo(int idUtente)
        {
            return _tsData.SelectOrdineTemporaneo(idUtente);
        }

        public void CreaOrdine(string descrizione, FabbisognoList listaFabbisogni, bool confermato)
        {
            _tsData.CreaOrdine(descrizione, listaFabbisogni, confermato);
        }

        public FornitoreList GetFornitori()
        {
            return _tsData.GetFornitori();
        }

        public bool ConfermaOrdine(int idOrdine, string codiceFornitore)
        {
            try
            {
                int risultato = _tsData.ConfermaOrdine(idOrdine, codiceFornitore);
                if (risultato == 0)
                    return true;
                return false;
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.ConfermaOrdine", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                throw new Exception();
            }
        }

        public bool DeleteOrdine(int idOrdine)
        {
            try
            {
                int risultato = _tsData.DeleteOrdine(idOrdine);
                if (risultato == 1)
                    return true;
                return false;
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.DeleteOrdine", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                throw new Exception();
            }
        }

        #endregion

        #region Creazione file ordini

        /// <summary>
        /// </summary>
        /// <param name="idOrdine"></param>
        /// <param name="idimpresa"></param>
        /// <returns></returns>
        public FileOrdineImpresa GetOrdineFile(int idOrdine, int idimpresa)
        {
            //

            return _tsData.GetOrdineFile(idOrdine, idimpresa);
        }

        /// <summary>
        /// </summary>
        /// <param name="idOrdine"></param>
        /// <returns></returns>
        public FileOrdine GetOrdineFile(int idOrdine)
        {
            //
            return _tsData.GetOrdineFile(idOrdine);
        }

        public OrdineRiassunto GetOrdineRiassunto(int idOrdine)
        {
            //
            return _tsData.GetOrdineRiassunto(idOrdine);
        }

        public OrdineRiassuntoImpLav GetOrdineRiassuntoImpLav(int idOrdine)
        {
            //
            return _tsData.GetOrdineRiassuntoImpLav(idOrdine);
        }

        #endregion

        #region GetFabbisogniConfermati

        public FabbisognoComplessivoList GetFabbisogniConfermati(int codiceImpresa)
        {
            try
            {
                return _tsData.GetFabbisogniConfermati(codiceImpresa, null);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetFabbisogniConfermati", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return null;
            }
        }

        public FabbisognoComplessivoList GetFabbisogniConfermati(string ragioneSocialeImpresa)
        {
            try
            {
                return _tsData.GetFabbisogniConfermati(null, ragioneSocialeImpresa);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetFabbisogniConfermati", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return null;
            }
        }

        public FabbisognoComplessivoList GetFabbisogniConfermati()
        {
            try
            {
                return _tsData.GetFabbisogniConfermati(null, null);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetFabbisogniConfermati", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return null;
            }
        }

        public FabbisognoList GetFabbisogniConfermatiByComplessivo(int idFabbisognoComplessivo)
        {
            try
            {
                return _tsData.GetFabbisogniConfermati(idFabbisognoComplessivo);
            }
            catch (Exception exc)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", exc.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.TuteScarpe.Business.TSBusiness.GetFabbisogniConfermati", logItemCollection, Log.categorie.TUTESCARPE, Log.sezione.SYSTEMEXCEPTION);

                return null;
            }
        }

        #endregion

        #region Selezione taglie per il lavoratore

        public TagliePerTipoCollection GetTagliePerTipoTaglia()
        {
            return _tsData.GetTagliePerTipoTaglia();
        }

        public TagliePerTipoCollection GetTaglieSelezionateLavoratore(int idLavoratore)
        {
            return _tsData.GetTaglieSelezionateLavoratore(idLavoratore);
        }

        public void InserisciTaglieSelezionate(TagliePerTipoCollection taglieSelezionate)
        {
            _tsData.InserisciTaglieSelezionate(taglieSelezionate);
        }

        #endregion

        #region Nuovi metodi Tute & Scarpe

        public FabbisognoLavoratore GetFabbisognoLavoratore(int idImpresa, int idLavoratore)
        {
            return _tsData.GetFabbisognoLavoratore(idImpresa, idLavoratore);
        }

        public void SeparaGruppiVestiario(FabbisognoLavoratore fabbisogno,
            GruppoFabbisognoLavoratoreCollection primoGruppo,
            GruppoFabbisognoLavoratoreCollection secondoGruppo,
            GruppoFabbisognoLavoratoreCollection terzoGruppo, bool asfaltisti)
        {
            foreach (GruppoFabbisognoLavoratore gruppo in fabbisogno.Gruppi)
            {
                switch (gruppo.CodiceAlternative)
                {
                    case 0:
                        if (!gruppo.GruppoPerAsfaltisti || asfaltisti)
                        {
                            primoGruppo.Add(gruppo);
                        }
                        break;
                    case 1:
                        if (!gruppo.GruppoPerAsfaltisti || asfaltisti)
                        {
                            // Se il gruppo � "Abbigliamento Edile" devo sdoppiare 
                            // (a causa dei doppi pantaloni)
                            //if (gruppo.IdGruppo == 1)
                            //{
                            //    GruppoFabbisognoLavoratore gruppoAlternEdile = (GruppoFabbisognoLavoratore) gruppo.Clone();

                            //    gruppo.Dettagli.EliminaIndumento("PJ3");
                            //    gruppoAlternEdile.Dettagli.EliminaIndumento("PJ2");
                            //    gruppo.Selezionato = gruppo.Selezionato && gruppo.Dettagli.GetIndumento("PJ2").Selezionato;
                            //    gruppoAlternEdile.Selezionato = gruppoAlternEdile.Selezionato && gruppoAlternEdile.Dettagli.GetIndumento("PJ3").Selezionato;

                            //    gruppo.Descrizione = "Abbigliamento Edile (due jeans leggeri)";
                            //    gruppoAlternEdile.Descrizione = "Abbigliamento Edile (un jeans leggero e uno pesante)";

                            //    secondoGruppo.Add(gruppo);
                            //    secondoGruppo.Add(gruppoAlternEdile);
                            //}
                            //else
                            //{
                            secondoGruppo.Add(gruppo);
                            //}
                        }
                        break;
                    case 2:
                        if (!gruppo.GruppoPerAsfaltisti || asfaltisti)
                        {
                            terzoGruppo.Add(gruppo);
                        }
                        break;
                }
            }
        }

        public int GetLavoratoreSuccessivo(int idLavoratore, int idImpresa)
        {
            return _tsData.GetLavoratoreSuccessivo(idLavoratore, idImpresa);
        }

        public int GetLavoratorePrecedente(int idLavoratore, int idImpresa)
        {
            return _tsData.GetLavoratorePrecedente(idLavoratore, idImpresa);
        }

        public void SalvaFabbisognoLavoratore(int idLavoratore, int idImpresa,
            DettaglioGruppoFabbisognoLavoratoreCollection dettagli)
        {
            _tsData.SalvaFabbisognoLavoratore(idLavoratore, idImpresa, dettagli);
        }

        #endregion
    }
}