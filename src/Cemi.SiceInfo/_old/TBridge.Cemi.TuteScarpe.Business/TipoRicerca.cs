namespace TBridge.Cemi.TuteScarpe.Business
{
    public class TipoRicerca
    {
        public TipoRicerca(TipoRicercaEnum tipo, int? codice, string ragioneSociale)
        {
            Tipo = tipo;
            Codice = codice;
            RagioneSociale = ragioneSociale;
        }

        public TipoRicercaEnum Tipo { get; }

        public int? Codice { get; }

        public string RagioneSociale { get; }
    }

    public enum TipoRicercaEnum
    {
        Tutti,
        Codice,
        RagioneSociale
    }
}