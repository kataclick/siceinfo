using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace TBridge.Cemi.AccessoCantieri.SelestaProvider
{
    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "WSTimbratureSoap11Binding", Namespace = "http://wstimbrature")]
    public class WSTimbrature : SoapHttpClientProtocol, IWSTimbrature
    {
        private SendOrPostCallback getTimbratureOperationCompleted;
        private SendOrPostCallback getTimbraturePeriodoOperationCompleted;
        private SendOrPostCallback insertAccessListOperationCompleted;
        private SendOrPostCallback ProvaOperationCompleted;

        /// <remarks />
        public WSTimbrature()
        {
            string urlSetting = ConfigurationManager.AppSettings["WSTimbrature.ServiceEndpointURL"];
            if (urlSetting != null
                && urlSetting != "")
            {
                Url = urlSetting;
            }
            else
            {
                Url = "http://www.clickandfind.it:8180/axis2/services/WSTimbrature.WSTimbratureHttpSoap1" +
                      "1Endpoint/";
            }
        }

        /// <remarks />
        public event insertAccessListCompletedEventHandler insertAccessListCompleted;

        /// <remarks />
        public event ProvaCompletedEventHandler ProvaCompleted;

        /// <remarks />
        public event getTimbraturePeriodoCompletedEventHandler getTimbraturePeriodoCompleted;

        /// <remarks />
        public event getTimbratureCompletedEventHandler getTimbratureCompleted;

        /// <remarks />
        public void insertAccessListAsync(string idterminale, string[] accessList)
        {
            insertAccessListAsync(idterminale, accessList, null);
        }

        /// <remarks />
        public void insertAccessListAsync(string idterminale, string[] accessList, object userState)
        {
            if (insertAccessListOperationCompleted == null)
            {
                insertAccessListOperationCompleted = OninsertAccessListOperationCompleted;
            }
            InvokeAsync("insertAccessList", new object[]
            {
                idterminale,
                accessList
            }, insertAccessListOperationCompleted, userState);
        }

        private void OninsertAccessListOperationCompleted(object arg)
        {
            if (insertAccessListCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                insertAccessListCompleted(this,
                    new insertAccessListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error,
                        invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void ProvaAsync()
        {
            ProvaAsync(null);
        }

        /// <remarks />
        public void ProvaAsync(object userState)
        {
            if (ProvaOperationCompleted == null)
            {
                ProvaOperationCompleted = OnProvaOperationCompleted;
            }
            InvokeAsync("Prova", new object[0], ProvaOperationCompleted, userState);
        }

        private void OnProvaOperationCompleted(object arg)
        {
            if (ProvaCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                ProvaCompleted(this,
                    new ProvaCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void getTimbraturePeriodoAsync(string startdate, string stopdate)
        {
            getTimbraturePeriodoAsync(startdate, stopdate, null);
        }

        /// <remarks />
        public void getTimbraturePeriodoAsync(string startdate, string stopdate, object userState)
        {
            if (getTimbraturePeriodoOperationCompleted == null)
            {
                getTimbraturePeriodoOperationCompleted = OngetTimbraturePeriodoOperationCompleted;
            }
            InvokeAsync("getTimbraturePeriodo", new object[]
            {
                startdate,
                stopdate
            }, getTimbraturePeriodoOperationCompleted, userState);
        }

        private void OngetTimbraturePeriodoOperationCompleted(object arg)
        {
            if (getTimbraturePeriodoCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                getTimbraturePeriodoCompleted(this,
                    new getTimbraturePeriodoCompletedEventArgs(invokeArgs.Results,
                        invokeArgs.Error,
                        invokeArgs.Cancelled,
                        invokeArgs.UserState));
            }
        }

        /// <remarks />
        public void getTimbratureAsync()
        {
            getTimbratureAsync(null);
        }

        /// <remarks />
        public void getTimbratureAsync(object userState)
        {
            if (getTimbratureOperationCompleted == null)
            {
                getTimbratureOperationCompleted = OngetTimbratureOperationCompleted;
            }
            InvokeAsync("getTimbrature", new object[0], getTimbratureOperationCompleted, userState);
        }

        private void OngetTimbratureOperationCompleted(object arg)
        {
            if (getTimbratureCompleted != null)
            {
                InvokeCompletedEventArgs invokeArgs = (InvokeCompletedEventArgs) arg;
                getTimbratureCompleted(this,
                    new getTimbratureCompletedEventArgs(invokeArgs.Results, invokeArgs.Error,
                        invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks />
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        #region IWSTimbrature Members

        /// <remarks />
        [SoapDocumentMethod("urn:insertAccessList", RequestNamespace = "http://wstimbrature",
            ResponseNamespace = "http://wstimbrature", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        public void insertAccessList([XmlElement(IsNullable = true, ElementName = "idterminale")] string idterminale,
            [XmlElement("accessList", IsNullable = true)] string[] accessList,
            [XmlElement(ElementName = "return")] out bool @return,
            [XmlIgnore] out bool returnSpecified)
        {
            object[] results = Invoke("insertAccessList", new object[]
            {
                idterminale,
                accessList
            });
            @return = (bool) results[0];
            returnSpecified = (bool) results[1];
        }

        /// <remarks />
        [SoapDocumentMethod("urn:Prova", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("ProvaResponse", Namespace = "http://wstimbrature")]
        public ProvaResponse Prova()
        {
            object[] results = Invoke("Prova", new object[0]);
            return (ProvaResponse) results[0];
        }

        /// <remarks />
        [SoapDocumentMethod("urn:getTimbraturePeriodo", RequestNamespace = "http://wstimbrature",
            ResponseNamespace = "http://wstimbrature", Use = SoapBindingUse.Literal,
            ParameterStyle = SoapParameterStyle.Wrapped)]
        [return: XmlElement("return", IsNullable = true)]
        public Timbratura[] getTimbraturePeriodo(
            [XmlElement(IsNullable = true, ElementName = "startdate")] string startdate,
            [XmlElement(IsNullable = true, ElementName = "stopdate")] string stopdate)
        {
            object[] results = Invoke("getTimbraturePeriodo", new object[]
            {
                startdate,
                stopdate
            });
            return (Timbratura[]) results[0];
        }

        /// <remarks />
        [SoapDocumentMethod("urn:getTimbrature", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)
        ]
        [return: XmlArray("getTimbratureResponse", Namespace = "http://wstimbrature")]
        [return: XmlArrayItem("return")]
        public Timbratura[] getTimbrature()
        {
            object[] results = Invoke("getTimbrature", new object[0]);
            return (Timbratura[]) results[0];
        }

        #endregion
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://wstimbrature", TypeName = "ProvaResponse")]
    public class ProvaResponse
    {
        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "return")] public Timbratura @return;

        public ProvaResponse()
        {
        }

        public ProvaResponse(Timbratura @return)
        {
            this.@return = @return;
        }
    }

    /// <remarks />
    [GeneratedCode("System.Xml", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://wstimbrature/xsd", TypeName = "Timbratura")]
    public class Timbratura
    {
        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "datolettura")] public string datolettura;

        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "idterminale")] public string idterminale;

        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "idtesta")] public string idtesta;

        /// <remarks />
        [XmlElement(ElementName = "inout")] public int inout;

        /// <remarks />
        [XmlIgnore] public bool inoutSpecified;

        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "latitudine")] public string latitudine;

        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "longitudine")] public string longitudine;

        /// <remarks />
        [XmlElement(IsNullable = true, ElementName = "tempo")] public string tempo;

        public Timbratura()
        {
        }

        public Timbratura(string datolettura, string idterminale, string idtesta, int inout, bool inoutSpecified,
            string latitudine, string longitudine, string tempo)
        {
            this.datolettura = datolettura;
            this.idterminale = idterminale;
            this.idtesta = idtesta;
            this.inout = inout;
            this.inoutSpecified = inoutSpecified;
            this.latitudine = latitudine;
            this.longitudine = longitudine;
            this.tempo = tempo;
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void insertAccessListCompletedEventHandler(object sender, insertAccessListCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class insertAccessListCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal insertAccessListCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public bool @return
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (bool) results[0];
            }
        }

        /// <remarks />
        public bool returnSpecified
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (bool) results[1];
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void ProvaCompletedEventHandler(object sender, ProvaCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class ProvaCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal ProvaCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public ProvaResponse Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (ProvaResponse) results[0];
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void getTimbraturePeriodoCompletedEventHandler(
        object sender, getTimbraturePeriodoCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class getTimbraturePeriodoCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal getTimbraturePeriodoCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public Timbratura[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (Timbratura[]) results[0];
            }
        }
    }

    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    public delegate void getTimbratureCompletedEventHandler(object sender, getTimbratureCompletedEventArgs e);


    /// <remarks />
    [GeneratedCode("System.Web.Services", "4.0.30319.1")]
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class getTimbratureCompletedEventArgs : AsyncCompletedEventArgs
    {
        private readonly object[] results;

        internal getTimbratureCompletedEventArgs(object[] results, Exception exception, bool cancelled,
            object userState)
            :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks />
        public Timbratura[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return (Timbratura[]) results[0];
            }
        }
    }
}