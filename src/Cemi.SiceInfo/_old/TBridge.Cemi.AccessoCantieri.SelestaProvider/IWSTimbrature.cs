namespace TBridge.Cemi.AccessoCantieri.SelestaProvider
{
    public interface IWSTimbrature
    {
        void insertAccessList(string idterminale, string[] accessList, out bool @return, out bool returnSpecified);

        ProvaResponse Prova();

        Timbratura[] getTimbraturePeriodo(string startdate, string stopdate);

        Timbratura[] getTimbrature();
    }
}