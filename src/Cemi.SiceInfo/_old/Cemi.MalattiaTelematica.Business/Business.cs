﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Cemi.MalattiaTelematica.Data;
using Cemi.MalattiaTelematica.Type.Collections;
using Cemi.MalattiaTelematica.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;

namespace Cemi.MalattiaTelematica.Business
{
    public class Business
    {
        public const string URLSEMAFOROGIALLO = "~/CeServizi/images/semaforoGiallo.png";
        public const string URLSEMAFOROROSSO = "~/CeServizi/images/semaforoRosso.png";
        public const string URLSEMAFOROVERDE = "~/CeServizi/images/semaforoVerde.png";
        private readonly MalattiaTelematicaDataAccess _dataAccess = new MalattiaTelematicaDataAccess();

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public decimal GetOreLavorate(int idLavoratore, DateTime inizio, DateTime assunzione, int idAssenza)
        {
            return _dataAccess.GetOreLavorate(idLavoratore, inizio, assunzione, idAssenza);
        }

        public int GetOreDaLavorare()
        {
            return _dataAccess.GetOreDaLavorare();
        }

        public PrestazioneDomandaAssenzaCollection GetPrestazioneDomandaAssenza(EstrattoContoFilter estrattoContoFilter)
        {
            return _dataAccess.GetPrestazioneDomandaAssenza(estrattoContoFilter.PeriodoDa, estrattoContoFilter.PeriodoA,
                estrattoContoFilter.RagioneSocialeImpresa, estrattoContoFilter.CodiceFiscaleImpresa,
                estrattoContoFilter.IdImpresa);
        }


        public AssenzaErroriCollection GetAssenzaErrori(DateTime data)
        {
            return _dataAccess.GetAssenzaErrori(data);
        }

        public CertificatoMedicoErroriCollection GetCertificatoMedicoErrori(DateTime data)
        {
            return _dataAccess.GetCertificatoMedicoErrori(data);
        }

        public MalattiaInfortunioErroriCollection GetMalattiaInfortunioErrori(DateTime data)
        {
            return _dataAccess.GetMalattiaInfortunioErrori(data);
        }

        public StatisticaLiquidazioneCollection GetStatisticaLiquidazioni(DateTime inizio, DateTime fine, string tipo)
        {
            return _dataAccess.GetStatisticaLiquidazioni(inizio, fine, tipo);
        }

        public StatisticaEvaseCollection GetStatisticaEvase(DateTime inizio, DateTime fine, string tipo)
        {
            return _dataAccess.GetStatisticaEvase(inizio, fine, tipo);
        }

        public bool InsertCassaEdile(int idAssenza, string idCassaEdile)
        {
            return _dataAccess.InsertCassaEdile(idAssenza, idCassaEdile);
        }


        public bool DeleteCassaEdile(int idAssenza, string idCassaEdile, int idLavoratore)
        {
            return _dataAccess.DeleteCassaEdile(idAssenza, idCassaEdile, idLavoratore);
        }

        public bool AbilitaControllo(string stato)
        {
            bool retVal = true;
            if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
            {
                if (stato == "I" || stato == "6")
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
            else
            {
                if (stato == "I" || stato == "9" || stato == "8" || stato == "R" || stato == "7" || stato == "6")
                {
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }

            return retVal;
        }


        public void InsertFiltroRicerca(int userID, AssenzeFilter filtro, int pagina)
        {
            _dataAccess.InsertFiltroRicerca(userID, filtro, pagina);
        }

        public AssenzeFilter GetFiltroRicerca(int userID, out int pagina)
        {
            return _dataAccess.GetFiltroRicerca(userID, out pagina);
        }


        public void DeleteFiltroRicerca(int userID)
        {
            _dataAccess.DeleteFiltroRicerca(userID);
        }


        public bool UpdateMalattiaTelematicaCertificatoMedico(MalattiaTelematicaCertificatoMedico certificatoMedico)
        {
            return _dataAccess.UpdateMalattiaTelematicaCertificatoMedico(certificatoMedico.Id,
                certificatoMedico.DataInizio.Value, certificatoMedico.DataFine.Value,
                certificatoMedico.DataRilascio.Value, certificatoMedico.Immagine, certificatoMedico.NomeFile,
                certificatoMedico.Numero, certificatoMedico.Tipo, certificatoMedico.TipoAssenza,
                certificatoMedico.Ricovero);
        }

        //public void GestisciOreCNCE(AssenzaDettagli assenzaDet, string loginCNCE, string passwordCNCE, string urlCNCE)
        //{
        //    OreMensiliCNCECollection ore = GetOreFromCNCE(assenzaDet, loginCNCE, passwordCNCE, urlCNCE);

        //    foreach (OreMensiliCNCE ora in ore)
        //    {
        //        bool duplicate;

        //        InsertOreMensili(ora, out duplicate);
        //    }
        //}
//        public bool InsertOreMensili(OreMensiliCNCE ore, out bool oreDuplicate)
//        {
//            return _dataAccess.InsertOreMensili(ore, out oreDuplicate);
//        }

//        private static OreMensiliCNCECollection GetOreFromCNCE(AssenzaDettagli assenzaDet, string loginCNCE, string passwordCNCE,
//                                                           string urlCNCE)
//        {
//            OreMensiliCNCECollection ore = new OreMensiliCNCECollection();
        ///*
//            NetworkCredential Cred = new NetworkCredential();
//            Cred.UserName = loginCNCE;
//            Cred.Password = passwordCNCE;

//            // Add certificate support.
//            ServicePointManager.CertificatePolicy = new AcceptAllCertificatesPolicy();

//            string urlTemporanea = urlCNCE;

//            foreach (CassaEdile cassaEdile in assenzaDet.CasseEdili)
//            {
//                if (cassaEdile.Cnce)
//                {
//                    for (int i = 0; i < 6; i++)
//                    {
//                        // Da ripetere per tutte le casse edili selezionate e per tutti gli anni            
//                        urlTemporanea = urlTemporanea.Replace("@codiceFiscale", domanda.Lavoratore.CodiceFiscale);
//                        urlTemporanea = urlTemporanea.Replace("@nome", domanda.Lavoratore.Nome);
//                        urlTemporanea = urlTemporanea.Replace("@cognome", domanda.Lavoratore.Cognome);
//                        urlTemporanea = urlTemporanea.Replace("@cassaEdile", cassaEdile.IdCassaEdile);
//                        urlTemporanea = urlTemporanea.Replace("@annoApe",
//                                                              domanda.DataRiferimento.AddYears(-i).Year.ToString());

//                        WebClient wc1 = new WebClient();
//                        wc1.Credentials = Cred;
//                        try
//                        {
//                            using (Stream s = wc1.OpenRead(urlTemporanea))
//                            {
//                                using (StreamReader sr = new StreamReader(s))
//                                {
//                                    // Tutto questo serve solo per scatenare l'eccezione,
//                                    // perchè alla prima chiamata viene fatto dal sito un Redirect e generata l'eccezione
//                                }
//                            }
//                        }
//                        catch (WebException webExc)
//                        {
//                            WebClient wc2 = new WebClient();

//                            wc2.Credentials = Cred;

//                            using (Stream s2 = wc2.OpenRead(webExc.Response.ResponseUri))
//                            {
//                                using (StreamReader sr2 = new StreamReader(s2))
//                                {
//                                    sr2.ReadLine();
//                                    string recordLetto;

//                                    while ((recordLetto = sr2.ReadLine()) != null)
//                                    {
//                                        char[] separator = new char[1];
//                                        separator[0] = ',';
//                                        OreMensiliCNCE ora = new OreMensiliCNCE();
//                                        ore.Add(ora);

//                                        ora.IdLavoratore = domanda.Lavoratore.IdLavoratore.Value;

//                                        string[] valori = recordLetto.Split(separator, StringSplitOptions.None);
//                                        if (!string.IsNullOrEmpty(valori[0]))
//                                            ora.Anno = Int32.Parse(valori[0]);
//                                        if (!string.IsNullOrEmpty(valori[1]))
//                                            ora.Mese = Int32.Parse(valori[1]);
//                                        if (!string.IsNullOrEmpty(valori[2]))
//                                            ora.IdCassaEdile = valori[2];
//                                        if (!string.IsNullOrEmpty(valori[4]))
//                                            ora.OreLavorate = ParseOreCNCE(valori[4]);
//                                        if (!string.IsNullOrEmpty(valori[5]))
//                                            ora.OreFerie = ParseOreCNCE(valori[5]);
//                                        if (!string.IsNullOrEmpty(valori[6]))
//                                            ora.OreInfortunio = ParseOreCNCE(valori[6]);
//                                        if (!string.IsNullOrEmpty(valori[7]))
//                                            ora.OreMalattia = ParseOreCNCE(valori[7]);
//                                        if (!string.IsNullOrEmpty(valori[8]))
//                                            ora.OreCassaIntegrazione = ParseOreCNCE(valori[8]);
//                                        if (!string.IsNullOrEmpty(valori[9]))
//                                            ora.OrePermessoRetribuito = ParseOreCNCE(valori[9]);
//                                        if (!string.IsNullOrEmpty(valori[10]))
//                                            ora.OrePermessoNonRetribuito = ParseOreCNCE(valori[10]);
//                                        if (!string.IsNullOrEmpty(valori[11]))
//                                            ora.OreAltro = ParseOreCNCE(valori[11]);
//                                    }
//                                }
//                            }
//                        }

//                        urlTemporanea = urlCNCE;
//                    }
//                }
//            }

//            // Fien
//            */
//            return ore;
//        }
        private static int ParseOreCNCE(string oreCNCE)
        {
            int ore = 0;

            //talvolta nel CNCE usano il punto come separatore e talvolta indicano .5 per dire 0.5. 
            //A noi interessa solo la parte intera, sicenew non è in grado di gestire i decimali per quei campi
            string oreIntere = oreCNCE.Split('.')[0];

            if (!string.IsNullOrEmpty(oreIntere))
            {
                if (!int.TryParse(oreIntere, out ore))
                    ore = 0;
            }
            else
                ore = 0;

            return ore;
        }


        #region certificato inps

        public string GetSemaforoCertificatoMedicoINPS(MalattiaTelematicaCertificatoMedico certificaMedico,
            string codiceFiscale)
        {
            // CertificatiMediciINPS cert = new CertificatiMediciINPS();
            string semaforo = URLSEMAFOROGIALLO;
            try
            {
                HttpWebRequest webRequest =
                    (HttpWebRequest)
                    WebRequest.Create(
                        "https://serviziweb2.inps.it/AttestatiCittadinoWeb/attivaMain?cmd=immessoCertificato");

                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";

                using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    string corpo = string.Format("codicefisc={0}&numerocert={1}", codiceFiscale,
                        certificaMedico.Numero);
                    //string corpo = String.Format("codicefisc={0}&numerocert={1}", "SPRGPP63E28A202N", "116869330");
                    requestWriter.Write(corpo);
                    requestWriter.Close();
                }

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate { return true; };

                string responseData;
                using (StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    responseData = reader.ReadToEnd();
                    reader.Close();
                }


                List<string> lines = responseData.Split(new[] {"\r\n"}, StringSplitOptions.None).ToList();

                if (PulisciRiga(lines[148]) != string.Empty)
                {
                    DateTime rilascio = Convert.ToDateTime(PulisciRiga(lines[148]));
                    DateTime inizio = Convert.ToDateTime(PulisciRiga(lines[139]));
                    DateTime fine = Convert.ToDateTime(PulisciRiga(lines[157]));

                    string tipo = "";

                    if (PulisciRiga(lines[183]).Contains("checked"))
                    {
                        tipo = "I";
                    }
                    else if (PulisciRiga(lines[185]).Contains("checked"))
                    {
                        tipo = "C";
                    }
                    else if (PulisciRiga(lines[187]).Contains("checked"))
                    {
                        tipo = "R";
                    }

                    if (inizio != certificaMedico.DataInizio ||
                        fine != certificaMedico.DataFine ||
                        rilascio != certificaMedico.DataRilascio ||
                        tipo != certificaMedico.Tipo)
                    {
                        semaforo = URLSEMAFOROROSSO;
                    }
                    else
                    {
                        semaforo = URLSEMAFOROVERDE;
                    }
                }
            }
            catch (Exception exception)
            {
                //Console.WriteLine(String.Format("Eccezione: {0}", exception.Message));
            }
            return semaforo;
        }

        private static string PulisciRiga(string line)
        {
            return line.Replace("<TD>", string.Empty).Replace("</TD>", string.Empty).Replace("value=\"", string.Empty)
                .Replace("\"", string.Empty).Replace("\t", string.Empty);
        }

        public MalattiaTelematicaCertificatoMedico GetCertificatoMedicoFromINPS(string codiceFiscale, string numero)
        {
            // CertificatiMediciINPS cert = new CertificatiMediciINPS();
            MalattiaTelematicaCertificatoMedico cert = new MalattiaTelematicaCertificatoMedico();
            try
            {
                HttpWebRequest webRequest =
                    (HttpWebRequest)
                    WebRequest.Create(
                        "https://serviziweb2.inps.it/AttestatiCittadinoWeb/attivaMain?cmd=immessoCertificato");


                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";

                using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    string corpo = string.Format("codicefisc={0}&numerocert={1}", codiceFiscale, numero);
                    //string corpo = String.Format("codicefisc={0}&numerocert={1}", "NDRTMS71P21H383I", "29547847");
                    requestWriter.Write(corpo);
                    requestWriter.Close();
                }

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate { return true; };

                string responseData;
                using (StreamReader reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    responseData = reader.ReadToEnd();
                    reader.Close();
                }


                List<string> lines = responseData.Split(new[] {"\r\n"}, StringSplitOptions.None).ToList();

                bool valido = false;
                string tipo = "";

                if (PulisciRiga(lines[24]).Contains("Certificato di malattia telematico"))
                {
                    if (PulisciRiga(lines[148]) != string.Empty)
                    {
                        valido = true;

                        cert.DataRilascio = Convert.ToDateTime(PulisciRiga(lines[148]));
                        cert.DataInizio = Convert.ToDateTime(PulisciRiga(lines[139]));
                        cert.DataFine = Convert.ToDateTime(PulisciRiga(lines[157]));

                        if (PulisciRiga(lines[183]).Contains("checked"))
                        {
                            tipo = "I";
                        }
                        else if (PulisciRiga(lines[185]).Contains("checked"))
                        {
                            tipo = "C";
                        }
                        else if (PulisciRiga(lines[187]).Contains("checked"))
                        {
                            tipo = "R";
                        }
                    }
                }
                else
                {
                    if (PulisciRiga(lines[150]) != string.Empty)
                    {
                        valido = true;

                        cert.DataRilascio = Convert.ToDateTime(PulisciRiga(lines[150]));
                        cert.DataInizio = Convert.ToDateTime(PulisciRiga(lines[116]));
                        cert.DataFine = Convert.ToDateTime(PulisciRiga(lines[136]));

                        if (PulisciRiga(lines[156]).Contains("checked"))
                        {
                            tipo = "I";
                        }
                        else if (PulisciRiga(lines[158]).Contains("checked"))
                        {
                            tipo = "C";
                        }
                        else if (PulisciRiga(lines[160]).Contains("checked"))
                        {
                            tipo = "R";
                        }
                    }
                }

                if (valido)
                {
                    cert.Tipo = tipo;
                    cert.Numero = numero;
                    cert.TipoAssenza = "MA";
                    cert.NomeFile = "attestato.html";

                    // estrapolo solamente la parte relativa al certificato
                    int startIndex = responseData.IndexOf("<form method");
                    int stopIndex = responseData.LastIndexOf("</form>");

                    //calcolo la lunghezza
                    int length = stopIndex + 7 - startIndex;

                    var encoding = Encoding.UTF8;
                    cert.Immagine = encoding.GetBytes(responseData.Substring(startIndex, length));
                }
            }
            catch (Exception exception)
            {
                //Console.WriteLine(String.Format("Eccezione: {0}", exception.Message));
            }
            return cert;
        }

        #endregion
    }
}