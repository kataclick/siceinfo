﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.MalattiaTelematica.Type.Filters;
using TBridge.Cemi.Data;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Domain;
using CassaEdile = TBridge.Cemi.Type.Entities.CassaEdile;

namespace Cemi.MalattiaTelematica.Business
{
    public class BusinessEF
    {
        public List<AssenzaCertificati> GetAssenzeCertificati(AssenzeFilter filtro)
        {
            if (filtro.PeriodoA.HasValue)
                filtro.PeriodoA = filtro.PeriodoA.Value.AddMonths(1);

            List<AssenzaCertificati> ret = new List<AssenzaCertificati>();
            DateTime dataMinima = new DateTime(2010, 10, 1);

            using (SICEEntities context = new SICEEntities())
            {
                var queryAssenze =
                    from mta in
                        context.MalattiaTelematicaAssenze.Include("Lavoratore").Include("Impresa")
                    join ass in context.Assenze on
                        new
                        {
                            mta.IdCassaEdile,
                            mta.TipoProtocollo,
                            NumeroProtocollo = (int) mta.NumeroProtocollo,
                            AnnoProtocollo = (int) mta.AnnoProtocollo
                        }
                        equals
                        new
                        {
                            ass.IdCassaEdile,
                            ass.TipoProtocollo,
                            ass.NumeroProtocollo,
                            ass.AnnoProtocollo
                        }
                        into a
                    from ass in a.DefaultIfEmpty()
                    join tipoAss in context.TipiAssenze on mta.Tipo equals tipoAss.Id
                    join stato in context.TipiStatoMalattiaTelematica on mta.Stato equals stato.Id
                    join utente in context.Utenti on
                        mta.IdUtenteInCarico equals utente.Id
                        into ut
                    from utente in ut.DefaultIfEmpty()
                    where
                        (!filtro.IdAssenza.HasValue || mta.Id == filtro.IdAssenza)
                        &&
                        (!filtro.IdUtente.HasValue || filtro.IdUtente > 0 && mta.IdUtenteInCarico == filtro.IdUtente ||
                         filtro.IdUtente < 0 && !mta.IdUtenteInCarico.HasValue)
                        &&
// Impresa
                        (!filtro.IdImpresa.HasValue || mta.Impresa.Id == filtro.IdImpresa)
                        &&
                        (string.IsNullOrEmpty(filtro.RagioneSocialeImpresa) ||
                         mta.Impresa.RagioneSociale.ToUpper().Contains(filtro.RagioneSocialeImpresa))
                        &&
                        (string.IsNullOrEmpty(filtro.CodiceFiscaleImpresa) ||
                         mta.Impresa.CodiceFiscale.ToUpper() == filtro.CodiceFiscaleImpresa ||
                         mta.Impresa.PartitaIVA.ToUpper() == filtro.CodiceFiscaleImpresa)
// Lavoratore
                        &&
                        (!filtro.IdLavoratore.HasValue || mta.IdLavoratore == filtro.IdLavoratore)
                        &&
                        (string.IsNullOrEmpty(filtro.CognomeLavoratore) ||
                         mta.Lavoratore.Cognome.ToUpper().Contains(filtro.CognomeLavoratore))
                        &&
                        (string.IsNullOrEmpty(filtro.NomeLavoratore) ||
                         mta.Lavoratore.Nome.ToUpper().Contains(filtro.NomeLavoratore))
                        &&
                        (!filtro.DataNascitaLavoratore.HasValue ||
                         mta.Lavoratore.DataNascita == filtro.DataNascitaLavoratore)
                        &&
                        (!filtro.DataInvioDa.HasValue && !filtro.DataInvioA.HasValue ||
                         EntityFunctions.TruncateTime(mta.DataImmessaPervenuta.Value) >=
                         EntityFunctions.TruncateTime(filtro.DataInvioDa)
                         &&
                         EntityFunctions.TruncateTime(mta.DataImmessaPervenuta.Value) <=
                         EntityFunctions.TruncateTime(filtro.DataInvioA))
//(mta.DataImmessaPervenuta >= filtro.DataInvioDa && mta.DataImmessaPervenuta <= filtro.DataInvioA))
//(mta.DataImmessaPervenuta.Value.Day == filtro.DataInvio.Value.Day &&
//mta.DataImmessaPervenuta.Value.Month == filtro.DataInvio.Value.Month &&
//mta.DataImmessaPervenuta.Value.Year == filtro.DataInvio.Value.Year)
//)
// Assenza
                        &&
                        (string.IsNullOrEmpty(filtro.StatoAssenza) || stato.Id == filtro.StatoAssenza)
                        &&
                        (string.IsNullOrEmpty(filtro.TipoAssenza) || tipoAss.Id == filtro.TipoAssenza) &&
                        (!filtro.PeriodoDa.HasValue && mta.DataInizio >= dataMinima ||
                         mta.DataInizio >= filtro.PeriodoDa) &&
                        (!filtro.PeriodoA.HasValue || mta.DataFine <= filtro.PeriodoA)
                    //)
                    orderby mta.Impresa.RagioneSociale, mta.Lavoratore.Cognome,
                        mta.Lavoratore.Nome, mta.DataInizioMalattia, mta.DataInizio
                    select
                        new AssenzaCertificati
                        {
                            IdAssenza = mta.Id,
                            IdLavoratore = mta.IdLavoratore,
                            IdImpresa = mta.IdImpresa,
                            InizioMalattia = mta.DataInizioMalattia,
                            Inizio = mta.DataInizio,
                            Fine = mta.DataFine,
                            Assenza = ass,
                            Utente = utente,
                            Lavoratore = mta.Lavoratore,
                            TipoAssenza = tipoAss,
                            TipoStatoMalattiaTelematica = stato
                        };


                List<AssenzaCertificati> assenze = queryAssenze.ToList();

                if (filtro.PeriodoA.HasValue)
                    filtro.PeriodoA = filtro.PeriodoA.Value.AddMonths(-1);

                foreach (AssenzaCertificati assenzaCert in assenze)
                {
                    //AssenzaCertificati assenzaCert = new AssenzaCertificati {Assenza = assenza};

                    var queryCertificati = //from cm in context.CertificatiMedici
                        from mtcm in context.MalattiaTelematicaCertificatiMedici
                        //on
                        // cm.IdMalattiaTelematicaCertificatoMedico equals mtcm.IdMalattiaTelematicaCertificatoMedico
                        where mtcm.IdImpresa == assenzaCert.IdImpresa &&
                              mtcm.IdLavoratore == assenzaCert.IdLavoratore
                              //mtcm.MalattiaTelematicaAssenze.Any(
                              //    ass =>
                              //    ass.Id == assenzaCert.Assenza.IdAssenza)
                              //cm.idTipoProtocollo == assenzaCert.Assenza.TipoProtocolloMalattia 
                              //&& cm.AnnoProtocollo == assenzaCert.Assenza.AnnoProtocolloMalattia 
                              //&& cm.NumeroProtocollo == assenzaCert.Assenza.NumeroProtocolloMalattia 
                              //&& cm.IdCassaEdile == assenzaCert.Assenza.IdCassaEdile
                              &&
                              //((mtcm.DataInizio >= assenzaCert.Assenza.DataInizioAssenzaDenuncia &&
                              //  mtcm.DataInizio <= assenzaCert.Assenza.DataFineAssenzaDenuncia) ||
                              // (mtcm.DataFine >= assenzaCert.Assenza.DataInizioAssenzaDenuncia &&
                              //  mtcm.DataFine <= assenzaCert.Assenza.DataFineAssenzaDenuncia) ||
                              // (mtcm.DataInizio <= assenzaCert.Assenza.DataInizioAssenzaDenuncia &&
                              //  mtcm.DataFine >= assenzaCert.Assenza.DataFineAssenzaDenuncia)
                              //)
                              (mtcm.DataInizio >= assenzaCert.Inizio &&
                               mtcm.DataInizio <= assenzaCert.Fine ||
                               mtcm.DataFine >= assenzaCert.Inizio &&
                               mtcm.DataFine <= assenzaCert.Fine ||
                               mtcm.DataInizio <= assenzaCert.Inizio &&
                               mtcm.DataFine >= assenzaCert.Fine
                              )
                        orderby mtcm.DataInizio descending
                        select mtcm;

                    assenzaCert.Certificati = queryCertificati.ToList();

                    ret.Add(assenzaCert);
                }
            }

            return ret;
        }

        public List<AssenzaCertificati> GetAssenzeLiquidate(AssenzeFilter filtro)
        {
            if (filtro.PeriodoA.HasValue)
                filtro.PeriodoA = filtro.PeriodoA.Value.AddMonths(1);
            DateTime dataMinima = new DateTime(2010, 10, 1);

            List<AssenzaCertificati> ret = new List<AssenzaCertificati>();

            using (SICEEntities context = new SICEEntities())
            {
                var queryAssenze =
                    from mta in
                        context.MalattiaTelematicaAssenze.Include("Lavoratore").Include("Impresa")
                    join ass in context.Assenze on
                        new
                        {
                            mta.IdCassaEdile,
                            mta.TipoProtocollo,
                            NumeroProtocollo = (int) mta.NumeroProtocollo,
                            AnnoProtocollo = (int) mta.AnnoProtocollo
                        }
                        equals
                        new
                        {
                            ass.IdCassaEdile,
                            ass.TipoProtocollo,
                            ass.NumeroProtocollo,
                            ass.AnnoProtocollo
                        }
                        into a
                    from ass in a.DefaultIfEmpty()
                    join tipoAss in context.TipiAssenze on mta.Tipo equals tipoAss.Id
                    join stato in context.TipiStatoMalattiaTelematica on mta.Stato equals stato.Id
                    where
                        // Impresa
                        (!filtro.IdImpresa.HasValue || mta.Impresa.Id == filtro.IdImpresa)
                        &&
                        (string.IsNullOrEmpty(filtro.RagioneSocialeImpresa) ||
                         mta.Impresa.RagioneSociale.ToUpper().Contains(filtro.RagioneSocialeImpresa))
                        &&
                        (string.IsNullOrEmpty(filtro.CodiceFiscaleImpresa) ||
                         mta.Impresa.CodiceFiscale.ToUpper() == filtro.CodiceFiscaleImpresa ||
                         mta.Impresa.PartitaIVA.ToUpper() == filtro.CodiceFiscaleImpresa)
                        // Lavoratore
                        &&
                        (!filtro.IdLavoratore.HasValue || mta.IdLavoratore == filtro.IdLavoratore)
                        &&
                        (string.IsNullOrEmpty(filtro.CognomeLavoratore) ||
                         mta.Lavoratore.Cognome.ToUpper().Contains(filtro.CognomeLavoratore))
                        &&
                        (string.IsNullOrEmpty(filtro.NomeLavoratore) ||
                         mta.Lavoratore.Nome.ToUpper().Contains(filtro.NomeLavoratore))
                        &&
                        (!filtro.DataNascitaLavoratore.HasValue ||
                         mta.Lavoratore.DataNascita == filtro.DataNascitaLavoratore)
                        &&
                        (!filtro.DataInvioDa.HasValue && !filtro.DataInvioA.HasValue ||
                         mta.DataImmessaPervenuta >= filtro.DataInvioDa &&
                         mta.DataImmessaPervenuta <= filtro.DataInvioA)
                        //(!filtro.DataInvio.HasValue ||
                        // (EntityFunctions.TruncateTime(mta.DataImmessaPervenuta.Value) == EntityFunctions.TruncateTime(filtro.DataInvio.Value))
                        // )
                        // Assenza
                        &&
                        (string.IsNullOrEmpty(filtro.StatoAssenza) || stato.Id == filtro.StatoAssenza)
                        &&
                        (string.IsNullOrEmpty(filtro.TipoAssenza) || tipoAss.Id == filtro.TipoAssenza)
                        &&
                        (!filtro.PeriodoDa.HasValue && mta.DataInizio >= dataMinima ||
                         mta.DataInizio >= filtro.PeriodoDa)
                        &&
                        (!filtro.PeriodoA.HasValue || mta.DataFine <= filtro.PeriodoA)
                    orderby mta.Impresa.RagioneSociale, mta.Lavoratore.Cognome,
                        mta.Lavoratore.Nome, mta.DataInizio descending
                    //select ass;
                    select
                        new AssenzaCertificati
                        {
                            IdAssenza = mta.Id,
                            IdLavoratore = mta.IdLavoratore,
                            IdImpresa = mta.IdImpresa,
                            Inizio = mta.DataInizio,
                            Fine = mta.DataFine,
                            Assenza = ass,
                            Lavoratore = mta.Lavoratore,
                            TipoAssenza = tipoAss,
                            TipoStatoMalattiaTelematica = stato
                        };
                ret = queryAssenze.ToList();

                if (filtro.PeriodoA.HasValue)
                    filtro.PeriodoA = filtro.PeriodoA.Value.AddMonths(-1);
            }

            return ret;
        }

        public AssenzaDettagli GetAssenzaDettagli(int idAssenza)
        {
            AssenzaDettagli ret;

            using (SICEEntities context = new SICEEntities())
            {
                context.CommandTimeout = 60;

                var query =
                    from mta in
                        context.MalattiaTelematicaAssenze.Include("Lavoratore").Include("Impresa")
                    join ass in context.Assenze on
                        new
                        {
                            mta.IdCassaEdile,
                            mta.TipoProtocollo,
                            NumeroProtocollo = (int) mta.NumeroProtocollo,
                            AnnoProtocollo = (int) mta.AnnoProtocollo
                        }
                        equals
                        new
                        {
                            ass.IdCassaEdile,
                            ass.TipoProtocollo,
                            ass.NumeroProtocollo,
                            ass.AnnoProtocollo
                        }
                        into a
                    from ass in a.DefaultIfEmpty()
                    join tipoAss in context.TipiAssenze on mta.Tipo equals tipoAss.Id
                    join stato in context.TipiStatoMalattiaTelematica on mta.Stato equals stato.Id
                    join rip in context.RapportiImpresaPersona on
                        mta.IdLavoratore equals rip.IdLavoratore //&& ass.idImpresa
                        into rapporto
                    from rip1 in
                    (from rip in rapporto
                        where rip.IdImpresa == mta.IdImpresa &&
                              mta.DataInizio >= rip.DataInizioValiditaRapporto &&
                              mta.DataInizio < rip.DataFineValiditaRapporto
                        select rip).DefaultIfEmpty()
                    join tc in context.TipiCategoria on
                        rip1.IdCategoria equals tc.Id
                        into cat
                    from tc in cat.DefaultIfEmpty()
                    where
                        mta.Id == idAssenza
                    select
                        new AssenzaDettagli
                        {
                            IdLavoratore = mta.IdLavoratore,
                            IdImpresa = mta.IdImpresa,
                            RagioneSocialeImpresa = mta.Impresa.RagioneSociale,
                            EmailImpresa = mta.Impresa.eMailSedeAmministrazione != string.Empty
                                ? mta.Impresa.eMailSedeAmministrazione
                                : mta.Impresa.eMailCorrispondenza,
                            IdAssenza = mta.Id,
                            Cognome = mta.Lavoratore.Cognome,
                            Nome = mta.Lavoratore.Nome,
                            DataNascita =
                                mta.Lavoratore.DataNascita.HasValue
                                    ? (DateTime) mta.Lavoratore.DataNascita
                                    : DateTime.Now,
                            CodiceFiscale = mta.Lavoratore.CodiceFiscale,
                            Stato = stato.Descrizione,
                            IdStato = mta.Stato,
                            Tipo = tipoAss.Descrizione,
                            IdTipo = mta.Tipo,
                            DataInizioMalattia = ass.DataInizioMalattiaDenuncia.HasValue
                                ? ass.DataInizioMalattiaDenuncia
                                : mta.DataInizioMalattia,
                            DataInizioAssenza = ass.DataInizioAssenzaDenuncia.HasValue
                                ? ass.DataInizioAssenzaDenuncia
                                : mta.DataInizio,
                            DataFineAssenza = ass.DataFineAssenzaDenuncia.HasValue
                                ? ass.DataFineAssenzaDenuncia
                                : mta.DataFine,
                            Ricaduta = ass.Ricaduta.HasValue ? ass.Ricaduta : false,
                            DataAssunzione = rip1.DataAssunzione.HasValue
                                ? rip1.DataAssunzione
                                : rip1.DataInizioValiditaRapporto,
                            DataInizio = rip1.DataInizioValiditaRapporto,
                            DataFine = rip1.DataFineValiditaRapporto,
                            Categoria = tc.Descrizione,
                            PercentualePT = rip1.PartTimePercentuale,
                            OreSettimanali = mta.OreSettimanali,
                            Note = mta.Note,
                            Telefonata = mta.Telefonata.HasValue ? mta.Telefonata : false,
                            Email = mta.Email.HasValue ? mta.Email : false,
                            DataInvio = mta.DataImmessaPervenuta,
                            IdUtenteInCarico = mta.IdUtenteInCarico,
                            NumeroProtocollo = mta.NumeroProtocollo,
                            AnnoProtocollo = mta.AnnoProtocollo,
                            OrePerseDich = ass.orePerseDichiarate.HasValue ? (int) ass.orePerseDichiarate : 0,
                            DataInAttesa = mta.DataInAttesa,
                            DataInvioPrimoSollecito = mta.DataInvioPrimoSollecito
                            //CasseEdili = mta.MalattiaTelematicaCasseEdili.ToList()
                        };


                ret = query.SingleOrDefault();
            }


            if (ret != null)
            {
                using (SICEEntities context = new SICEEntities())
                {
                    var query2 =
                        from mtce in
                            context.MalattiaTelematicaCasseEdili.Include("CassaEdile")
                        join ace in context.CNCEACE on mtce.IdCassaEdile equals ace.IdCassaEdile
                            into aceJoin
                        from ace in aceJoin.DefaultIfEmpty()
                        where mtce.IdMalattiaTelematicaAssenza == idAssenza
                        select
                            new CassaEdile
                            {
                                IdCassaEdile = mtce.IdCassaEdile,
                                Descrizione = mtce.CassaEdile.Descrizione,
                                Cnce = ace.IdCassaEdile != null ? true : false

                                //CE = mtce,
                                //Descrizione = mtce.CassaEdile.Descrizione,
                                //Cnce = ace.IdCassaEdile != null ? true : false
                            };
                    List<CassaEdile> ce = query2.ToList();

                    CassaEdileCollection casse = new CassaEdileCollection();

                    foreach (CassaEdile c in ce)
                    {
                        casse.Add(c);
                    }
                    ret.CasseEdili = casse;
                    //ret.CasseEdili = (CassaEdileCollection)query2.ToList();
                }
            }
            return ret;
        }

        public List<MalattiaTelematicaMessaggio> GetMessaggi(DateTime? giorno, bool messaggio, string errore)
        {
            List<MalattiaTelematicaMessaggio> ret = new List<MalattiaTelematicaMessaggio>();

            using (SICEEntities context = new SICEEntities())
            {
                var queryMessaggi =
                    from mes in
                        context.MalattiaTelematicaMessaggi.Include("Lavoratore").Include("Impresa")
                    where
                        (giorno.HasValue && giorno == mes.DataImportazione || !giorno.HasValue)
                        &&
                        (messaggio && mes.DescrizioneMessaggio != null || !messaggio)
                        &&
                        (string.IsNullOrEmpty(errore) || errore == mes.GravitaSegnalazione)
                    select mes;

                ret = queryMessaggi.ToList();
            }

            return ret;
        }

        public List<MalattiaTelematicaAssenza> GetAssenzeInAttesa(int tipoRichiesta, DateTime? giornoDa, DateTime? giornoA, int? idUtente)
        {
            List<MalattiaTelematicaAssenza> ret = new List<MalattiaTelematicaAssenza>();

            using (SICEEntities context = new SICEEntities())
            {
                if (idUtente.HasValue || idUtente > 0)
                {
                    var query =
                    from mta in
                        context.MalattiaTelematicaAssenze.Include("Lavoratore").Include("Impresa")
                    //join utente in context.Utenti on
                    //    mta.IdUtenteInCarico equals utente.Id
                    //into ut
                    //from utente in ut
                    where
                        mta.Stato == "8"
                        &&
                        (tipoRichiesta == 1 &&
                         (!giornoDa.HasValue && !giornoA.HasValue ||
                          mta.DataInAttesa >= giornoDa && mta.DataInAttesa <= giornoA)
                         ||
                         tipoRichiesta == 2 &&
                         (!giornoDa.HasValue && !giornoA.HasValue ||
                          mta.DataInvioPrimoSollecito >= giornoDa && mta.DataInvioPrimoSollecito <= giornoA))
                        &&
                        (mta.IdUtenteInCarico == idUtente)
                    select mta;

                    ret = query.ToList();
                }
                else
                {
                    var query =
                        from mta in
                            context.MalattiaTelematicaAssenze.Include("Lavoratore").Include("Impresa")
                        where
                            mta.Stato == "8"
                            &&
                            (tipoRichiesta == 1 &&
                             (!giornoDa.HasValue && !giornoA.HasValue ||
                              mta.DataInAttesa >= giornoDa && mta.DataInAttesa <= giornoA)
                             ||
                             tipoRichiesta == 2 &&
                             (!giornoDa.HasValue && !giornoA.HasValue ||
                              mta.DataInvioPrimoSollecito >= giornoDa && mta.DataInvioPrimoSollecito <= giornoA))
                        select mta;

                    ret = query.ToList();
                }
            }

            return ret;
        }

        public int GetAssenzeInAttesa(int? idUtente)
        {
            int ret = 0;
            using (SICEEntities context = new SICEEntities())
            {
                var query =
                    from mta in
                        context.MalattiaTelematicaAssenze
                    join utente in context.Utenti on
                        mta.IdUtenteInCarico equals utente.Id
                        into ut
                    from utente in ut.DefaultIfEmpty()
                    where
                        mta.Stato == "8"
                        &&
                        (!idUtente.HasValue || idUtente > 0 && mta.IdUtenteInCarico == idUtente ||
                         idUtente < 0 && !mta.IdUtenteInCarico.HasValue)
                    select mta;

                ret = query.Count();
            }

            return ret;
        }

        public int GetAssenzePrimoSollecito(int? idUtente, DateTime oggi)
        {
            int ret = 0;

            DateTime check = oggi.AddMonths(-3);

            using (SICEEntities context = new SICEEntities())
            {
                var query =
                    from mta in
                        context.MalattiaTelematicaAssenze
                    join utente in context.Utenti on
                        mta.IdUtenteInCarico equals utente.Id
                        into ut
                    from utente in ut.DefaultIfEmpty()
                    where
                        mta.Stato == "8"
                        &&
                        (!idUtente.HasValue || idUtente > 0 && mta.IdUtenteInCarico == idUtente ||
                         idUtente < 0 && !mta.IdUtenteInCarico.HasValue)
                        &&
                        mta.DataInAttesa <= check
                    select mta;

                ret = query.Count();
            }

            return ret;
        }

        public int GetAssenzeRespingimento(int? idUtente, DateTime oggi)
        {
            int ret = 0;

            DateTime check = oggi.AddMonths(-3);

            using (SICEEntities context = new SICEEntities())
            {
                var query =
                    from mta in
                        context.MalattiaTelematicaAssenze
                    join utente in context.Utenti on
                        mta.IdUtenteInCarico equals utente.Id
                        into ut
                    from utente in ut.DefaultIfEmpty()
                    where
                        mta.Stato == "8"
                        &&
                        (!idUtente.HasValue || idUtente > 0 && mta.IdUtenteInCarico == idUtente ||
                         idUtente < 0 && !mta.IdUtenteInCarico.HasValue)
                        &&
                        mta.DataInvioPrimoSollecito <= check
                    select mta;

                ret = query.Count();
            }

            return ret;
        }

        public void UpdateMalattiaTelematicaAssenzeStato(int idAssenza, string stato)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assenza0 in context.MalattiaTelematicaAssenze
                    where assenza0.Id == idAssenza
                    select assenza0;
                var query2 = from tipoStato in context.TipiStatoMalattiaTelematica
                    where tipoStato.Id == stato
                    select tipoStato;

                MalattiaTelematicaAssenza assenzaU = query.Single();
                assenzaU.TipoStatoMalattiaTelematica = query2.Single();
                if (stato == "9" || stato == "7")
                {
                    assenzaU.DataImmessaPervenuta = DateTime.Now;
                }

                if (stato == "8")
                {
                    assenzaU.DataInAttesa = DateTime.Now;
                }
                else
                {
                    if (stato != "4")
                    {
                        assenzaU.DataInAttesa = null;
                    }
                }


                assenzaU.IdUtenteGestione = GestioneUtentiBiz.GetIdUtente();

                context.SaveChanges();
            }
        }

        public void UpdateMalattiaTelematicaAssenzeOreSettimanali(int idAssenza, int ore)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assenza0 in context.MalattiaTelematicaAssenze
                    where assenza0.Id == idAssenza
                    select assenza0;

                MalattiaTelematicaAssenza assenzaU = query.Single();
                assenzaU.OreSettimanali = ore;

                //assenzaU.IdUtenteGestione = GestioneUtentiBiz.GetIdUtente();

                context.SaveChanges();
            }
        }

        public void UpdateMalattiaTelematicaAssenzeNoteAggiuntive(int idAssenza, string note, bool telefonata,
            bool email)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assenza0 in context.MalattiaTelematicaAssenze
                    where assenza0.Id == idAssenza
                    select assenza0;

                MalattiaTelematicaAssenza assenzaU = query.Single();
                assenzaU.Note = note;
                assenzaU.Telefonata = telefonata;
                assenzaU.Email = email;

                //assenzaU.IdUtenteGestione = GestioneUtentiBiz.GetIdUtente();

                context.SaveChanges();
            }
        }

        public void UpdateAssenzeForzatura(int idAssenza, bool forzatura)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assenza0 in context.MalattiaTelematicaAssenze
                    where assenza0.Id == idAssenza
                    select assenza0;

                MalattiaTelematicaAssenza assenzaU = query.Single();
                assenzaU.Forzatura = forzatura;

                context.SaveChanges();
            }
        }

        public void UpdateMalattiaTelematicaAssenzeUtenteInCarico(int idAssenza, int? idUtente)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assenza0 in context.MalattiaTelematicaAssenze
                    where assenza0.Id == idAssenza
                    select assenza0;

                MalattiaTelematicaAssenza assenzaU = query.Single();
                assenzaU.IdUtenteInCarico = idUtente;

                context.SaveChanges();
            }
        }

        public void UpdateMalattiaTelematicaAssenzeDataInvioPrimoSollecito(int idAssenza, DateTime primoSollecito)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from assenza0 in context.MalattiaTelematicaAssenze
                    where assenza0.Id == idAssenza
                    select assenza0;

                MalattiaTelematicaAssenza assenzaU = query.Single();
                assenzaU.DataInvioPrimoSollecito = primoSollecito;

                //assenzaU.IdUtenteGestione = GestioneUtentiBiz.GetIdUtente();

                context.SaveChanges();
            }
        }

        public Lavoratore GetLavoratore(string codiceFiscale)
        {
            Lavoratore ret = new Lavoratore();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from lavoratore in context.Lavoratori
                    where
                        lavoratore.CodiceFiscale == codiceFiscale
                    select lavoratore;
                //select new MalattiaTelematicaAltroDocumento() {};

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public Impresa GetImpresa(string codiceINPS)
        {
            Impresa ret = new Impresa();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from impresa in context.Imprese
                    where
                        impresa.codiceINPS == codiceINPS
                    select impresa;
                //select new MalattiaTelematicaAltroDocumento() {};

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public Impresa GetImpresa(int idImpresa)
        {
            Impresa ret = new Impresa();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from impresa in context.Imprese
                    where
                        impresa.Id == idImpresa
                    select impresa;
                //select new MalattiaTelematicaAltroDocumento() {};

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public void InsertAssenzaAltraCE(MalattiaTelematicaAssenza assenza)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.MalattiaTelematicaAssenze.AddObject(assenza);

                context.SaveChanges();
            }
        }

        public string GetMalattiaTelematicaEmail(int? idImpresa, int? idConsulente)
        {
            string retVal;

            using (SICEEntities context = new SICEEntities())
            {
                if (idImpresa.HasValue)
                {
                    var queryImp = from email in context.MalattiaTelematicaImprese
                        where email.IdImpresa == idImpresa
                        select email.Email;
                    retVal = queryImp.SingleOrDefault();


                    if (string.IsNullOrEmpty(retVal))
                    {
                        queryImp = from i in context.Imprese
                            where i.Id == idImpresa
                            select
                                i.eMailSedeAmministrazione != string.Empty
                                    ? i.eMailSedeAmministrazione
                                    : i.eMailCorrispondenza;
                        retVal = queryImp.SingleOrDefault();
                    }
                }
                else
                {
                    var queryCons = from email in context.MalattiaTelematicaConsulenti
                        where email.IdConsulente == idConsulente
                        select email.Email;
                    retVal = queryCons.SingleOrDefault();


                    if (string.IsNullOrEmpty(retVal))
                    {
                        queryCons = from c in context.Consulenti
                            where c.Id == idConsulente
                            select
                                c.eMail;
                        retVal = queryCons.SingleOrDefault();
                    }
                }
            }

            return retVal;
        }

        public void UpdateMalattiaTelematicaEmail(int? idImpresa, int? idConsulente, string email)
        {
            using (SICEEntities context = new SICEEntities())
            {
                if (idImpresa.HasValue)
                {
                    var query = from e in context.MalattiaTelematicaImprese
                        where e.IdImpresa == idImpresa
                        select e;

                    foreach (var objI in query)
                    {
                        context.MalattiaTelematicaImprese.DeleteObject(objI);
                    }

                    MalattiaTelematicaImpresa imp = new MalattiaTelematicaImpresa
                    {
                        Email = email,
                        IdImpresa = idImpresa.Value
                    };

                    context.MalattiaTelematicaImprese.AddObject(imp);

                    context.SaveChanges();
                }
                else
                {
                    if (idConsulente.HasValue)
                    {
                        var queryC = from e in context.MalattiaTelematicaConsulenti
                            where e.IdConsulente == idConsulente
                            select e;

                        foreach (var objC in queryC)
                        {
                            context.MalattiaTelematicaConsulenti.DeleteObject(objC);
                        }

                        MalattiaTelematicaConsulente cons = new MalattiaTelematicaConsulente
                        {
                            Email = email,
                            IdConsulente = idConsulente.Value
                        };

                        context.MalattiaTelematicaConsulenti.AddObject(cons);

                        context.SaveChanges();
                    }
                }
            }
        }

        public MalattiaTelematicaAssenza GetMalattiaTelematicaAssenzaRicevuta(int idAssenza)
        {
            MalattiaTelematicaAssenza ret = new MalattiaTelematicaAssenza();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from ass in context.MalattiaTelematicaAssenze.Include("Impresa").Include("Lavoratore")
                    where
                        ass.Id == idAssenza
                    select ass;
                //select new MalattiaTelematicaAltroDocumento() {};

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        

        #region tabelle lookup

        public List<TipoAssenza> GetTipiAssenze()
        {
            List<TipoAssenza> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from tipoAssenza in context.TipiAssenze
                    select tipoAssenza;
                ret = query.ToList();
            }

            return ret;
        }

        public List<TipoStatoMalattiaTelematica> GetTipiStatoMalattiaTelematica()
        {
            //List<TipoStatoMalattiaTelematica> ret;

            //using (SICEEntities context = new SICEEntities())
            //{
            //    var query = from tipoStatoMalattiaTelematica in context.TipiStatoMalattiaTelematica
            //                select tipoStatoMalattiaTelematica;
            //    ret = query.ToList();
            //}

            return GetTipiStatoMalattiaTelematica(true);
        }

        public List<TipoStatoMalattiaTelematica> GetTipiStatoMalattiaTelematica(bool sospesa)
        {
            List<TipoStatoMalattiaTelematica> ret;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from tipoStatoMalattiaTelematica in context.TipiStatoMalattiaTelematica
                        where tipoStatoMalattiaTelematica.Id != "X" && tipoStatoMalattiaTelematica.Id != "5" &&
                              !sospesa || sospesa
                        orderby tipoStatoMalattiaTelematica.Descrizione
                        select tipoStatoMalattiaTelematica
                    ;
                ret = query.ToList();
            }

            return ret;
        }

        #endregion

        #region controlli

        public bool AssenzeSovrapposte(int idLavoratore, DateTime inizio, DateTime fine, int idAssenza)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    //join ass in context.Assenze on
                    //    new
                    //        {
                    //            IdCassaEdile = ass.IdCassaEdile,
                    //            TipoProtocollo = ass.TipoProtocollo,
                    //            NumeroProtocollo = ass.NumeroProtocollo,
                    //            AnnoProtocollo = ass.AnnoProtocollo
                    //        }
                    //    equals
                    //    new
                    //        {
                    //            IdCassaEdile = mta.IdCassaEdile,
                    //            TipoProtocollo = mta.TipoProtocollo,
                    //            NumeroProtocollo = (int) mta.NumeroProtocollo,
                    //            AnnoProtocollo = (int) mta.AnnoProtocollo
                    //        }
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.Id != idAssenza
                        &&
                        mta.Stato != "A"
                        &&
                        (inizio >= mta.DataInizio && inizio <= mta.DataFine ||
                         fine >= mta.DataInizio && fine <= mta.DataFine)
                    select mta;

                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        public bool PeriodiCongruenti(int idAssenza, DateTime inizio)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where
                        cert.MalattiaTelematicaAssenze.Any(ass => ass.Id == idAssenza)
                        &&
                        cert.DataInizio < inizio
                    select cert;

                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        public bool CertificatiMediciSovrapposti(int idLavoratore, DateTime inizio, DateTime fine, int idCertificato)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where
                        cert.MalattiaTelematicaAssenze.Any(ass => ass.IdLavoratore == idLavoratore)
                        &&
                        (inizio >= cert.DataInizio && inizio <= cert.DataFine ||
                         fine >= cert.DataInizio && fine <= cert.DataFine)
                        && cert.Id != idCertificato
                    select cert;
                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        public bool AssenzaPrecedenteNonAccolta(int idAssenza, int idLavoratore, int idImpresa, DateTime inizioMalattia,
            DateTime inizioAssenza)
        {
            bool ret = false;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    //join ass in context.Assenze on
                    //new
                    //{
                    //    IdCassaEdile = ass.IdCassaEdile,
                    //    TipoProtocollo = ass.TipoProtocollo,
                    //    NumeroProtocollo = ass.NumeroProtocollo,
                    //    AnnoProtocollo = ass.AnnoProtocollo
                    //}
                    //equals
                    //new
                    //{
                    //    IdCassaEdile = mta.IdCassaEdile,
                    //    TipoProtocollo = mta.TipoProtocollo,
                    //    NumeroProtocollo = (int)mta.NumeroProtocollo,
                    //    AnnoProtocollo = (int)mta.AnnoProtocollo
                    //}
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.IdImpresa == idImpresa
                        &&
                        mta.Id != idAssenza
                        &&
                        mta.DataInizioMalattia.Value == inizioMalattia
                        &&
                        mta.DataInizio.Value <= inizioAssenza
                        &&
                        (mta.Stato == "I" || mta.Stato == "9" || mta.Stato == "6" || mta.Stato == "7" ||
                         mta.Stato == "8")
                    select mta.Stato;

                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        public bool GiustificativiAssenti(int idAssenza)
        {
            bool ret = true;

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaCertificatiMedici
                    where
                        mta.MalattiaTelematicaAssenze.Any(ass => ass.Id == idAssenza)
                    select mta.Id;

                if (query.Count() > 0)
                    ret = false;
            }

            return ret;
        }


        public string StatoAssenzaPrecedente(int idAssenza, int idLavoratore, DateTime inizio)
        {
            string ret = "";

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    //join ass in context.Assenze on
                    //new
                    //{
                    //    IdCassaEdile = ass.IdCassaEdile,
                    //    TipoProtocollo = ass.TipoProtocollo,
                    //    NumeroProtocollo = ass.NumeroProtocollo,
                    //    AnnoProtocollo = ass.AnnoProtocollo
                    //}
                    //equals
                    //new
                    //{
                    //    IdCassaEdile = mta.IdCassaEdile,
                    //    TipoProtocollo = mta.TipoProtocollo,
                    //    NumeroProtocollo = (int)mta.NumeroProtocollo,
                    //    AnnoProtocollo = (int)mta.AnnoProtocollo
                    //}
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.Id != idAssenza
                        //&&
                        //mta.DataInizioMalattia.Value == inizioMalattia
                        &&
                        mta.DataInizio.Value <= inizio
                    //&&
                    //(mta.Stato == "I" || mta.Stato == "9" || mta.Stato == "R" || mta.Stato == "6" || mta.Stato == "7")
                    orderby mta.DataInizio descending
                    select mta.Stato;

                if (query.Count() > 0)
                    ret = query.FirstOrDefault();

                //if (query.Count() > 0)
                //    ret = true;
            }

            return ret;
        }

        public bool AssenzaMancante(int idAssenza, int idLavoratore, DateTime inizioMalattia, DateTime inizio,
            bool ricaduta, string tipo)
        {
            bool ret = true;

            DateTime inizio2 = ricaduta ? inizio.AddDays(-30) : inizio.AddDays(-1);

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.Id != idAssenza
                        &&
                        mta.DataInizioMalattia.Value == inizioMalattia
                        &&
                        mta.DataFine.Value >= inizio2
                        &&
                        mta.DataInizio < inizio
                        &&
                        mta.Tipo == tipo
                    select mta;
                if (query.Count() > 0)
                    ret = false;
            }

            return ret;
        }

        public bool RicadutaOltre30GG(int idAssenza, int idLavoratore, DateTime inizioMalattia, DateTime inizio)
        {
            bool ret = true;

            DateTime inizio2 = inizio.AddDays(-30);

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    where
                        mta.IdLavoratore == idLavoratore
                        &&
                        mta.Id != idAssenza
                        &&
                        mta.DataInizioMalattia.Value == inizioMalattia
                        &&
                        mta.DataFine.Value >= inizio2
                    select mta;
                if (query.Count() > 0)
                    ret = false;
            }

            return ret;
        }

        public bool CertificatiMediciContinuazione(int idLavoratore, DateTime inizio, int idCertificato)
        {
            bool ret = false;

            DateTime inizio2 = inizio.AddDays(-1);

            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where
                        cert.MalattiaTelematicaAssenze.Any(mta => mta.IdLavoratore == idLavoratore)
                        && cert.Id != idCertificato
                        && cert.DataFine.Value >= inizio2
                    select cert;

                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        public bool CertificatoMedicoFuoriAssenza(int idAssenza, DateTime inizio, DateTime fine)
        {
            bool ret = false;

            DateTime inizio2 = inizio.AddDays(-1);

            using (SICEEntities context = new SICEEntities())
            {
                var query = from mta in context.MalattiaTelematicaAssenze
                    //    join ass in context.Assenze on
                    //new
                    //{
                    //    IdCassaEdile = ass.IdCassaEdile,
                    //    TipoProtocollo = ass.TipoProtocollo,
                    //    NumeroProtocollo = ass.NumeroProtocollo,
                    //    AnnoProtocollo = ass.AnnoProtocollo
                    //}
                    //equals
                    //new
                    //{
                    //    IdCassaEdile = mta.IdCassaEdile,
                    //    TipoProtocollo = mta.TipoProtocollo,
                    //    NumeroProtocollo = (int)mta.NumeroProtocollo,
                    //    AnnoProtocollo = (int)mta.AnnoProtocollo
                    //}
                    where
                        mta.Id == idAssenza
                        && (fine < mta.DataInizio.Value || inizio > mta.DataFine.Value)
                    select mta;

                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        public bool CertificatiMediciRicaduta(int idLavoratore, DateTime inizio, int idCertificato)
        {
            bool ret = false;

            DateTime inizio2 = inizio.AddDays(-30);

            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where
                        cert.MalattiaTelematicaAssenze.Any(ass => ass.IdLavoratore == idLavoratore)
                        && cert.Id != idCertificato
                        && cert.DataFine.Value >= inizio2
                    select cert;

                if (query.Count() > 0)
                    ret = true;
            }

            return ret;
        }

        #endregion

        #region certificati medici

        public List<MalattiaTelematicaCertificatoMedico> GetCertificatiMedici(int idAssenza)
        {
            List<MalattiaTelematicaCertificatoMedico> ret = new List<MalattiaTelematicaCertificatoMedico>();

            using (SICEEntities context = new SICEEntities())
            {
                var queryAssenze =
                    //    from ass in
                    //        context.Assenze
                    //    join mta in context.MalattiaTelematicaAssenze on
                    //        new
                    //        {
                    //            IdCassaEdile = ass.IdCassaEdile,
                    //            TipoProtocollo = ass.TipoProtocollo,
                    //            NumeroProtocollo = ass.NumeroProtocollo,
                    //            AnnoProtocollo = ass.AnnoProtocollo
                    //        }
                    //        equals
                    //        new
                    //        {
                    //            IdCassaEdile = mta.IdCassaEdile,
                    //            TipoProtocollo = mta.TipoProtocollo,
                    //            NumeroProtocollo = (int)mta.NumeroProtocollo,
                    //            AnnoProtocollo = (int)mta.AnnoProtocollo
                    //        }
                    //    where mta.Id == idAssenza
                    //    select ass;

                    //Assenza assenza = queryAssenze.SingleOrDefault();
                    from mta in context.MalattiaTelematicaAssenze
                    where mta.Id == idAssenza
                    select mta;

                MalattiaTelematicaAssenza assenza = queryAssenze.SingleOrDefault();


                var query =
                    from cert in context.MalattiaTelematicaCertificatiMedici.Include("TipoCertificatoMedico")
                    where cert.IdLavoratore == assenza.IdLavoratore &&
                          cert.IdImpresa == assenza.IdImpresa &&
                          (cert.DataInizio >= assenza.DataInizio &&
                           cert.DataInizio <= assenza.DataFine ||
                           cert.DataFine >= assenza.DataInizio &&
                           cert.DataFine <= assenza.DataFine ||
                           cert.DataInizio <= assenza.DataInizio &&
                           cert.DataFine >= assenza.DataFine
                          )
                          || cert.MalattiaTelematicaAssenze.Any(ass => ass.Id == idAssenza)
                    select cert;

                ret = query.ToList();
            }


            return ret;
        }

        public MalattiaTelematicaCertificatoMedico GetCertificatoMedico(int idCertificatoMedico)
        {
            MalattiaTelematicaCertificatoMedico ret = new MalattiaTelematicaCertificatoMedico();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where
                        cert.Id == idCertificatoMedico
                    select cert;

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public Immagine GetImmagineCertificatoMedico(int idCertificato)
        {
            Immagine ret = new Immagine();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from altri in context.MalattiaTelematicaCertificatiMedici
                    where
                        altri.Id == idCertificato
                    select new Immagine
                    {
                        File = altri.Immagine,
                        NomeFile = altri.NomeFile,
                        IdArchidoc = altri.IdArchidoc
                    };

                ret = query.SingleOrDefault();
            }

            return ret;
        }


        public void InsertCertificatoMedico(MalattiaTelematicaCertificatoMedico certificato)
        {
            using (SICEEntities context = new SICEEntities())
            {
                List<MalattiaTelematicaAssenza> assenze = new List<MalattiaTelematicaAssenza>();

                foreach (MalattiaTelematicaAssenza assenza in certificato.MalattiaTelematicaAssenze)
                {
                    var query = from ass in context.MalattiaTelematicaAssenze
                        where ass.Id == assenza.Id
                        select ass;

                    assenze.Add(query.Single());
                }
                certificato.MalattiaTelematicaAssenze = assenze;

                context.MalattiaTelematicaCertificatiMedici.AddObject(certificato);

                context.SaveChanges();
            }
        }

        public void DeleteCertificatoMedico(int idCertificato)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where cert.Id == idCertificato
                    select cert;

                context.MalattiaTelematicaCertificatiMedici.DeleteObject(query.Single());
                context.SaveChanges();
            }
        }

        public void UpdateCertificatoMedico(MalattiaTelematicaCertificatoMedico certificatoMedico)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert0 in context.MalattiaTelematicaCertificatiMedici
                    where cert0.Id == certificatoMedico.Id
                    select cert0;

                MalattiaTelematicaCertificatoMedico certU = query.Single();
                certU.DataInizio = certificatoMedico.DataInizio;
                certU.DataFine = certificatoMedico.DataFine;
                certU.DataRilascio = certificatoMedico.DataRilascio;
                if (!Convert.IsDBNull(certificatoMedico.Immagine))
                {
                    certU.Immagine = certificatoMedico.Immagine;
                }
                if (!Convert.IsDBNull(certificatoMedico.NomeFile))
                {
                    certU.NomeFile = certificatoMedico.NomeFile;
                }
                certU.Numero = certificatoMedico.Numero;


                certU.Tipo = certificatoMedico.Tipo;
                certU.TipoAssenza = certificatoMedico.TipoAssenza;
                //certU.MalattiaTelematicaAssenze = certificatoMedico.MalattiaTelematicaAssenze;


                //List<MalattiaTelematicaAssenza> assenze = new List<MalattiaTelematicaAssenza>();

                foreach (MalattiaTelematicaAssenza assenza in certificatoMedico.MalattiaTelematicaAssenze)
                {
                    var query2 = from ass in context.MalattiaTelematicaAssenze
                        where ass.Id == assenza.Id
                        select ass;

                    certU.MalattiaTelematicaAssenze.Add(query2.Single());
                }
                //certU.MalattiaTelematicaAssenze = assenze;


                context.SaveChanges();
            }
        }

        public bool ExistCertificatoMedico(int idLavoratore, string numero)
        {
            bool ret = false;
            using (SICEEntities context = new SICEEntities())
            {
                var query = from cert in context.MalattiaTelematicaCertificatiMedici
                    where cert.Lavoratore.Id == idLavoratore &&
                          cert.Numero == numero
                    select cert;

                if (query.Count() > 0)
                {
                    ret = true;
                }
            }
            return ret;
        }

        #endregion

        #region giorni non indennizzabili

        public List<MalattiaTelematicaGiornoNonIndennizzabile> GetGiorniNonIndennizzabili(int idAssenza)
        {
            List<MalattiaTelematicaGiornoNonIndennizzabile> ret = new List<MalattiaTelematicaGiornoNonIndennizzabile>();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from ggNonInd in context.MalattiaTelematicaGiorniNonIndennizzabili
                    where
                        ggNonInd.IdMalattiaTelematicaAssenza == idAssenza
                    select ggNonInd;

                ret = query.ToList();
            }

            return ret;
        }

        public void AggiornaSituazioneGiorniNonIndennizzabili(int idAssenza,
            List<MalattiaTelematicaGiornoNonIndennizzabile> giorni)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from ggNonInd in context.MalattiaTelematicaGiorniNonIndennizzabili
                    where ggNonInd.IdMalattiaTelematicaAssenza == idAssenza
                    select ggNonInd;


                foreach (var obj in query)
                {
                    context.MalattiaTelematicaGiorniNonIndennizzabili.DeleteObject(obj);
                }
                //context.MalattiaTelematicaGiorniNonIndennizzabili.DeleteObject(query.ToList());
                //context.SaveChanges();


                foreach (MalattiaTelematicaGiornoNonIndennizzabile g in giorni)
                {
                    context.MalattiaTelematicaGiorniNonIndennizzabili.AddObject(g);
                }

                context.SaveChanges();
            }
        }

        #endregion

        #region altri documenti

        public List<MalattiaTelematicaAltroDocumento> GetAltriDocumenti(int idAssenza)
        {
            List<MalattiaTelematicaAltroDocumento> ret = new List<MalattiaTelematicaAltroDocumento>();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from altri in context.MalattiaTelematicaAltriDocumenti
                    where
                        altri.IdMalattiaTelematicaAssenza == idAssenza
                    select altri;
                //select new MalattiaTelematicaAltroDocumento() {};

                ret = query.ToList();
            }

            return ret;
        }

        public void InsertAltroDocumento(MalattiaTelematicaAltroDocumento altro)
        {
            using (SICEEntities context = new SICEEntities())
            {
                context.MalattiaTelematicaAltriDocumenti.AddObject(altro);

                context.SaveChanges();
            }
        }

        public Immagine GetImmagineAltroDocumento(int idAltroDocumento)
        {
            Immagine ret = new Immagine();

            using (SICEEntities context = new SICEEntities())
            {
                var query = from altri in context.MalattiaTelematicaAltriDocumenti
                    where
                        altri.Id == idAltroDocumento
                    select new Immagine
                    {
                        File = altri.Immagine,
                        NomeFile = altri.NomeFile,
                        IdArchidoc = altri.idArchidoc
                    };

                ret = query.SingleOrDefault();
            }

            return ret;
        }

        public void DeleteAltroDocumento(int idAltroDoc)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var query = from altroDoc in context.MalattiaTelematicaAltriDocumenti
                    where altroDoc.Id == idAltroDoc
                    select altroDoc;

                context.MalattiaTelematicaAltriDocumenti.DeleteObject(query.Single());
                context.SaveChanges();
            }
        }

        #endregion
    }
}