﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Exceptions
{
    public class LetturaCSVException : Exception
    {
        public LetturaCSVException(int line, Exception innerException)
        {
            Line = line;
            InnerException = innerException;
        }

        public int Line { get; set; }
        public Exception InnerException { get; set; }
    }
}