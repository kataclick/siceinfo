using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ContribuzioneINPS2
    {
        public string PartitaIVAImpresa { get; set; }

        public int Mese { get; set; }

        public int Anno { get; set; }

        public bool? Superato { get; set; }
    }
}