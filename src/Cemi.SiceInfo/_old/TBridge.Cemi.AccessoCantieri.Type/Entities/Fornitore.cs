using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Fornitore
    {
        public int IdFornitore { get; set; }

        public string RagioneSociale { get; set; }
    }
}