using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Lavoratore
    {
        //public Lavoratore()
        //{
        //}

        //public Lavoratore(
        //    int? idLavoratore, string cognome, string nome, DateTime? dataNascita, string codiceFiscale)
        //{
        //    IdLavoratore = idLavoratore;
        //    Cognome = cognome;
        //    Nome = nome;
        //    DataNascita = dataNascita;
        //    CodiceFiscale = codiceFiscale;
        //}

        public TipologiaLavoratore TipoLavoratore { get; set; }

        public int? IdLavoratore { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        //public string Impresa { get; set; }

        public string CodiceFiscale { get; set; }

        public int? IdDomandaLavoratore { get; set; }

        public DateTime? DataInizioAttivita { get; set; }

        public DateTime? DataFineAttivita { get; set; }

        public string LuogoNascita { get; set; }

        public string PaeseNascita { get; set; }

        public string ProvinciaNascita { get; set; }

        public bool Incongruenze { get; set; }

        public int? IdLavoratoreTrovato { get; set; }

        //public Boolean LavoratoreAssociato
        //{
        //    get
        //    {
        //        if (TipoLavoratore == TipologiaLavoratore.SiceNew
        //            ||
        //            IdLavoratoreTrovato.HasValue
        //            )
        //        {
        //            return true;
        //        }

        //        return false;
        //    }
        //}

        //public TipologiaContratto TipoContrattoImpresa { get; set; }

        //public Int32? IdImpresa { get; set; }

        //public String CodiceERagioneSocialeImpresa
        //{
        //    get
        //    {
        //        if (IdImpresa.HasValue)
        //        {
        //            return String.Format("{0} - {1}", IdImpresa, Impresa);
        //        }
        //        return Impresa;
        //    }
        //}

        public bool EffettuaControlli { get; set; }

        public DateTime? DataAssunzione { get; set; }

        public byte[] Foto { get; set; }

        public DateTime? DataStampaBadge { get; set; }

        public Impresa Impresa { get; set; }

        public string ContrattoApplicato { get; set; }
    }
}