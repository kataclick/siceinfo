using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ContribuzioneINPS
    {
        public string RagioneSocialeImpresaINPS { get; set; }

        public string CodiceFiscaleImpresaINPS { get; set; }

        public string PartitaIVAImpresaINPS { get; set; }

        public DateTime? DataUltimaContribuzioneINPS { get; set; }
    }
}