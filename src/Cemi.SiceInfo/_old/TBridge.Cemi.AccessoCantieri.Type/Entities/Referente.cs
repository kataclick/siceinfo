using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Referente
    {
        public Referente()
        {
        }

        public Referente(
            int? idReferente, string cognome, string nome, DateTime dataNascita, string codiceFiscale,
            string telefono, string email, bool modalitaContatto)
        {
            IdReferente = idReferente;
            Cognome = cognome;
            Nome = nome;
            DataNascita = dataNascita;
            CodiceFiscale = codiceFiscale;
            Telefono = telefono;
            Email = email;
            ModalitaContatto = modalitaContatto;
        }

        public int? IdReferente { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public bool ModalitaContatto { get; set; }

        public int? IdWhiteListReferente { get; set; }

        public TipologiaRuoloReferente? TipoRuolo { get; set; }

        public TipologiaAffiliazione? TipoAffiliazione { get; set; }
    }
}