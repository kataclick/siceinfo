using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Statistica
    {
        public ImpresaCollection ImpreseCeAppaltatrici { get; set; }

        public ImpresaCollection ImpreseCeSubappaltate { get; set; }

        public ImpresaCollection ImpreseNuoveAppaltatrici { get; set; }

        public ImpresaCollection ImpreseNuoveSubappaltate { get; set; }

        public ImpresaCollection ImpreseArtigianeAppaltatrici { get; set; }

        public ImpresaCollection ImpreseArtigianeSubappaltate { get; set; }

        public LavoratoreCollection LavoratoriCe { get; set; }

        public LavoratoreCollection LavoratoriNuovi { get; set; }

        public TimbraturaCollection Timbrature { get; set; }

        public TimbraturaCollection TimbratureFiltrate { get; set; }

        public int TotaleImpreseCeAppaltatrici { get; set; }

        public int TotaleImpreseCeSubappaltate { get; set; }

        public int TotaleImpreseNuoveAppaltatrici { get; set; }

        public int TotaleImpreseNuoveSubappaltate { get; set; }

        public int TotaleImpreseArtigianeSubappaltate { get; set; }

        public int TotaleImpreseArtigianeAppaltatrici { get; set; }

        public int TotaleLavoratoriCe { get; set; }

        public int TotaleLavoratoriNuovi { get; set; }

        public int TotaleTimbrature { get; set; }

        public int TotaleTimbratureFiltrate { get; set; }
    }
}