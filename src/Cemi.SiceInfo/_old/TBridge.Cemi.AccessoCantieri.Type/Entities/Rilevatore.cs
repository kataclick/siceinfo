using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Rilevatore
    {
        public int IdRilevatore { get; set; }

        public string Codice { get; set; }

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public int? IdWhiteListRilevatore { get; set; }

        public bool InvioWhitelist { get; set; }
    }
}