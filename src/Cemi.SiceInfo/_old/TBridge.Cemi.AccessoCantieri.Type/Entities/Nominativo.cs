using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Nominativo
    {
        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public int CodiceCe { get; set; }
    }
}