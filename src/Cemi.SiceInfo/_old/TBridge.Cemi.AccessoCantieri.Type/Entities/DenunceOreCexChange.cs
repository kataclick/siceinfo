﻿namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    public class DenunceOreCexChange
    {
        public string CodiceFiscaleLavoratore { set; get; }
        public string CassaEdileCodice { set; get; }
        public string CassaEdileDescrizione { set; get; }
        public decimal OreOrdinarie { set; get; }
        public int PeriodoMese { set; get; }
        public int PeriodoAnno { set; get; }

        public override string ToString()
        {
            return CassaEdileDescrizione;
        }
    }
}