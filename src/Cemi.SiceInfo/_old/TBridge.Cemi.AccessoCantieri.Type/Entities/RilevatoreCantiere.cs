using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class RilevatoreCantiere
    {
        public int IdRilevatore { get; set; }

        public string CodiceRilevatore { get; set; }

        public string RagioneSociale { get; set; }

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public int? IdCantiere { get; set; }

        public DateTime? DataInizio { get; set; }

        public DateTime? DataFine { get; set; }

        public string Indirizzo { get; set; }

        public string Civico { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string NomeReferente { get; set; }

        public string CognomeReferente { get; set; }

        public bool InvioWhitelist { get; set; }

        public bool Terminato { get; set; }
    }
}