using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Subappalto
    {
        public Subappalto()
        {
        }

        public Subappalto(int? idSubappalto, int idCantiere, Impresa appaltante, Impresa appaltata,
            TipologiaContratto tipoContratto)
        {
            IdSubappalto = idSubappalto;
            IdCantiere = idCantiere;
            Appaltante = appaltante;
            Appaltata = appaltata;
            TipoContratto = tipoContratto;
        }

        public int? IdSubappalto { get; set; }

        public int IdCantiere { get; set; }

        public Impresa Appaltante { get; set; }

        public Impresa Appaltata { get; set; }

        public TipologiaContratto TipoContratto { get; set; }

        public DateTime? DataInizioAttivitaAppaltata { get; set; }

        public DateTime? DataFineAttivitaAppaltata { get; set; }

        public DateTime? DataInizioAttivitaAppaltatrice { get; set; }

        public DateTime? DataFineAttivitaAppaltatrice { get; set; }

        public string AutorizzazioneAlSubappaltoAppaltata { get; set; }

        public string AutorizzazioneAlSubappaltoAppaltatrice { get; set; }

        #region ProprietÓ di appoggio per il Binding dei dati, solo lettura

        public string NomeAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    switch (Appaltante.TipoImpresa)
                    {
                        case TipologiaImpresa.SiceNew:
                            return string.Format("{0} - {1}", Appaltante.IdImpresa, Appaltante.RagioneSociale);
                        case TipologiaImpresa.Nuova:
                            return Appaltante.RagioneSociale;
                        default:
                            return string.Empty;
                    }
                }
                return string.Empty;
            }
        }

        public string IndirizzoAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    return Appaltante.IndirizzoCompleto;
                }
                return string.Empty;
            }
        }

        public string CodiceFiscaleAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    return Appaltante.CodiceFiscale;
                }
                return string.Empty;
            }
        }

        public string PartitaIvaAppaltatrice
        {
            get
            {
                if (Appaltante != null)
                {
                    return Appaltante.PartitaIva;
                }
                return string.Empty;
            }
        }

        public string NomeAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    switch (Appaltata.TipoImpresa)
                    {
                        case TipologiaImpresa.SiceNew:
                            return string.Format("{0} - {1}", Appaltata.IdImpresa, Appaltata.RagioneSociale);
                        case TipologiaImpresa.Nuova:
                            return Appaltata.RagioneSociale;
                        default:
                            return string.Empty;
                    }
                }
                return string.Empty;
            }
        }

        public string IndirizzoAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    return Appaltata.IndirizzoCompleto;
                }
                return string.Empty;
            }
        }

        public string CodiceFiscaleAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    return Appaltata.CodiceFiscale;
                }
                return string.Empty;
            }
        }

        public string PartitaIvaAppaltata
        {
            get
            {
                if (Appaltata != null)
                {
                    return Appaltata.PartitaIva;
                }
                return string.Empty;
            }
        }

        public bool Equals(object obj)
        {
            if (obj.GetType() != typeof(Subappalto))
                return base.Equals(obj);
            Subappalto sub = (Subappalto) obj;


            bool appaltante = Appaltante == null && sub.Appaltante == null;

            if (!appaltante)
            {
                if (Appaltante != null && sub.Appaltante != null)
                {
                    return Appaltante.Equals(sub.Appaltante) && Appaltata.Equals(sub.Appaltata);
                }
            }
            return Appaltata.Equals(sub.Appaltata);
        }

        #endregion
    }
}