﻿namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class DenunciaOreCexChange
    {
        public string CassaEdileCodice { set; get; }

        public string CassaEdileDescrizione { set; get; }

        public string CodiceCELavoratore { set; get; }

        public string CodiceCEImpresa { set; get; }

        public int PeriodoMese { set; get; }

        public int PeriodoAnno { set; get; }

        public override string ToString()
        {
            return CassaEdileDescrizione;
        }

        #region ore

        public decimal OreOrdinarie { set; get; }

        public decimal ImponibileGNF { set; get; }

        #endregion
    }
}