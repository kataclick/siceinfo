﻿using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Collections.CexChange;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class ControlloIdentitaCexChange
    {
        public string CodiceFiscaleLavoratore { get; set; }

        public string PartitaIvaImpresa { get; set; }

        public int Mese { get; set; }

        public int Anno { get; set; }

        public int? IdCantiere { get; set; }

        public ControlloDenunciaOreCexChangeCollection ControlliDenunceCexChange { set; get; }

        //dati whitelist
        public Lavoratore LavoratoreTimbratura { get; set; }

        //dati whitelist
        public Impresa ImpresaTimbratura { get; set; }

        //INPS (basato su Milano)
        public ContribuzioneInps2Collection ContribuzioniInps { get; set; }
    }
}