﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class ControlloDenunciaOreCexChange
    {
        public string CassaEdileCodice { set; get; }

        public string CassaEdileDescrizione { set; get; }

        public int PeriodoAnno { set; get; }

        public int PeriodoMese { set; get; }

        public bool ImpresaPresenzaDenunceInPeriodo { set; get; }

        public LavoratoreCexChange Lavoratore { set; get; }

        public ImpresaCexChange Impresa { set; get; }

        public DenunciaOreCexChange DeunciaOre { set; get; }

        public string Periodo => string.Format("{0}/{1}", PeriodoMese, PeriodoAnno);

        public bool ImpresaIscrittaInPeriodo
        {
            get
            {
                if (Impresa != null && Impresa.DataIscrizione.HasValue)
                {
                    return new DateTime(Impresa.DataIscrizione.Value.Year, Impresa.DataIscrizione.Value.Month, 1) <
                           new DateTime(PeriodoAnno, PeriodoMese, 1);
                }

                return false;
            }
        }

        public EsitoVersamentoCexChange ImpresaEsitoVersamentoInPeriodo { set; get; }
    }
}