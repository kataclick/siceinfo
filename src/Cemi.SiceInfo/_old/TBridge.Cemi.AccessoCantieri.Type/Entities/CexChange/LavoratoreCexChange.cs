﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class LavoratoreCexChange
    {
        public string CassaEdileCodice { set; get; }

        public string CassaEdileDescrizione { set; get; }

        public string CodiceCELavoratore { set; get; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascita { get; set; }

        public string CodiceFiscale { get; set; }

        public string LuogoNascita { get; set; }

        public string NazioneNascita { get; set; }

        public string ProvinciaNascita { get; set; }

        public string Sesso { get; set; }

        public int? UltimaDenunciaAnno { set; get; }

        public int? UltimaDenunciaMese { set; get; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Nome, Cognome);
        }
    }
}