﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class ImpresaCexChange
    {
        public string CassaEdileCodice { set; get; }

        public string CassaEdileDescrizione { set; get; }

        public string CodiceCEImpresa { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }

        public string RagioneSociale { get; set; }

        public string Natura { set; get; }

        public string Stato { set; get; }

        public DateTime? DataCessazione { set; get; }

        public DateTime? DataDecorrenza { set; get; }

        public DateTime? DataIscrizione { set; get; }

        public int? UltimaDenunciaAnno { set; get; }

        public int? UltimaDenunciaMese { set; get; }

        public string PosizioneBNI { set; get; }

        public bool? PosizioneBNIRegolre => string.IsNullOrEmpty(PosizioneBNI) ? (bool?) null : PosizioneBNI == "R";

        public override string ToString()
        {
            return RagioneSociale;
        }
    }
}