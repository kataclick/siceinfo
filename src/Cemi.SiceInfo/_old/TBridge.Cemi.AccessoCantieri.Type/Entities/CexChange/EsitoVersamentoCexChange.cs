﻿namespace TBridge.Cemi.AccessoCantieri.Type.Entities.CexChange
{
    public class EsitoVersamentoCexChange
    {
        public int Id { set; get; }
        public string CodiceCexChange { set; get; }
        public string Descrizione { set; get; }
    }
}