using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Indirizzo : Cemi.Type.Entities.Geocode.Indirizzo
    {
        public Indirizzo()
        {
            Stato = "Italy";
        }

        //private Indirizzo(string indirizzo, string civico, string comune, string provincia, string cap,
        //                 decimal? latitudine, decimal? longitudine) : this()
        //{
        //    Indirizzo1 = indirizzo;
        //    Civico = civico;
        //    Comune = comune;
        //    Provincia = provincia;
        //    Cap = cap;
        //    Latitudine = latitudine;
        //    Longitudine = longitudine;
        //}

        //public Indirizzo(string indirizzo, string civico, string comune, string provincia, string cap,
        //                 decimal? latitudine, decimal? longitudine, string infoAggiuntiva)
        //    : this(indirizzo, civico, comune, provincia, cap, latitudine, longitudine)
        //{
        //    InfoAggiuntiva = infoAggiuntiva;
        //}

        //public int? IdIndirizzo { get; set; }

        public string Indirizzo1
        {
            get => Via;
            set => NomeVia = value;
        }

        public string InfoAggiuntiva
        {
            get => InformazioniAggiuntive;
            set => InformazioniAggiuntive = value;
        }

        //public string IndirizzoDenominazione
        //{
        //    get { return IndirizzoBase; }
        //}

        //public string IndirizzoPerGeocoder
        //{
        //    get { return String.Format("{0}, Lombardia, {1}", IndirizzoCompleto, Stato); }
        //}

        //public string IndirizzoPerGeocoderGenerico
        //{
        //    get { return IndirizzoCompletoGeocode; }
        //}

        //public new string IndirizzoCompleto
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(InfoAggiuntiva))
        //            return string.Format("{0} {1}{2} {3} {4} ({5})", Indirizzo1, Civico, Environment.NewLine, Comune,
        //                                 Provincia, InfoAggiuntiva);
        //        return string.Format("{0} {1}{2} {3} {4}", Indirizzo1, Civico, Environment.NewLine, Comune,
        //                             Provincia);
        //    }
        //}

        //public Boolean HaCoordinate()
        //{
        //    return Georeferenziato;
        //}
    }
}