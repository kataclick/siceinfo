﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class WhiteListImpresa
    {
        public WhiteListImpresa()
        {
            Lavoratori = new LavoratoreCollection();
        }

        public Impresa Impresa { get; set; }

        public int? IdDomanda { get; set; }

        public LavoratoreCollection Lavoratori { get; set; }
    }
}