﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class WhiteList
    {
        public WhiteList()
        {
            Subappalti = new SubappaltoCollection();
        }

        public int? IdWhiteList { get; set; }

        public string Indirizzo { get; set; }

        public string Civico { get; set; }

        public string Comune { get; set; }

        public string Provincia { get; set; }

        public string Cap { get; set; }

        public string IndirizzoCompletoCantiere
        {
            get
            {
                if (!string.IsNullOrEmpty(Provincia))
                {
                    return string.Format("{0} {1} {2} ({3})", Indirizzo, Civico, Comune, Provincia);
                }
                return string.Format("{0} {1} {2}", Indirizzo, Civico, Comune);
            }
        }

        public string InfoAggiuntiva { get; set; }

        public SubappaltoCollection Subappalti { get; set; }

        public WhiteListImpresaCollection Lavoratori { get; set; }

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public string Descrizione { get; set; }

        public DateTime? DataInizio { get; set; }

        public DateTime? DataFine { get; set; }

        public int IdUtente { get; set; }

        public string LoginUtente { get; set; }

        public Guid GuidId { get; set; }

        public ImpresaCollection ListaImprese { get; set; }

        public AltraPersonaCollection ListaAltrePersone { get; set; }

        public ReferenteCollection ListaReferenti { get; set; }

        public RilevatoreCantiereCollection Rilevatori { get; set; }

        public Committente Committente { get; set; }

        public string AutorizzazioneAlSubappalto { get; set; }

        public Cemi.Type.Entities.GestioneUtenti.Committente CommittenteUtente { get; set; }
    }
}