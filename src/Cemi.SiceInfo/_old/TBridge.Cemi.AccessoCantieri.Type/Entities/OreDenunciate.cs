using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class OreDenunciate
    {
        public string IdTipoOra { get; set; }

        public int IdLavoratore { get; set; }

        public int IdImpresa { get; set; }

        public char StatoDenuncia { get; set; }

        public DateTime DataDenormalizzazione { get; set; }

        public decimal OreDichiarate { get; set; }
    }
}