using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Timbratura
    {
        public int IdTimbratura { get; set; }

        public string CodiceRilevatore { get; set; }

        public string RagioneSociale { get; set; }

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public string CodiceFiscale { get; set; }

        public string CodiceFiscaleImpresa { get; set; }

        public string PartitaIvaImpresa { get; set; }

        public bool IngressoUscita { get; set; }

        public bool Gestito { get; set; }

        public TipologiaAnomalia Anomalia { get; set; }

        public DateTime DataOra { get; set; }

        public int? IdCantiere { get; set; }

        public bool ControlliEffettuati { get; set; }

        public bool ControlloDenunciaSuperato { get; set; }

        public bool ControlloLavoratoreDenunciaSuperato { get; set; }

        public bool ControlloOreDenunciaSuperato { get; set; }

        public int? IdLavoratore { get; set; }

        public int? IdImpresa { get; set; }

        public bool ControlloDebitiSuperato { get; set; }

        public bool ControlloDebitiEffettuato { get; set; }

        public TipologiaFornitore Fornitore { get; set; }
    }
}