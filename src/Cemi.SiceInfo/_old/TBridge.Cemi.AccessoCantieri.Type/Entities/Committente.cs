﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Committente
    {
        public int? IdCommittente { get; set; }

        public string RagioneSociale { get; set; }

        public TipologiaCommittente Tipologia { get; set; }

        public string Indirizzo { get; set; }

        public string Civico { get; set; }

        public int Provincia { get; set; }

        public string ProvinciaDescrizione { get; set; }

        public long Comune { get; set; }

        public string ComuneDescrizione { get; set; }

        public string Cap { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }

        //public String Cognome { get; set; }

        //public String Nome { get; set; }
    }
}