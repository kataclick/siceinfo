using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ElementoInps
    {
        public string CodiceFiscaleLavoratoreInps { get; set; }

        public string PartitaIvaImpresaCemiInps { get; set; }
    }
}