using System;
using TBridge.Cemi.AccessoCantieri.Type.Collections;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class ControlloIdentita
    {
        public string CodiceFiscale { get; set; }

        public int Mese { get; set; }

        public int Anno { get; set; }

        public int? IdCantiere { get; set; }

        //CEMI

        public Lavoratore Lavoratore { get; set; }

        public Impresa Impresa { get; set; }

        public bool ControlliEffettuati { get; set; }

        public bool ControlloDenunciaSuperato { get; set; }

        public bool ControlloLavoratoreDenunciaSuperato { get; set; }

        public bool ControlloOreDenunciaSuperato { get; set; }

        public bool ControlloDebitiSuperato { get; set; }

        public bool ControlloDebitiEffettuato { get; set; }

        //CEXChange
        public DateTime? DataUltimaDenuncia { get; set; }

        public string CassaEdileUltimaDenuncia { get; set; }

        //CEXChange
        public DenunceOreCexChangeCollection DenunceOreCexChange { set; get; }

        //INPS
        //public ContribuzioneINPSCollection ContribuzioniINPS { get; set; }
        public ContribuzioneInps2Collection ContribuzioniInps { get; set; }

        public string PartitaIvaImpresa { get; set; }
    }
}