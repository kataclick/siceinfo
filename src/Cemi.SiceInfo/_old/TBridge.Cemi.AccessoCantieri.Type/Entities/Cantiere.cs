using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    [Serializable]
    public class Cantiere
    {
        #region ProprietÓ

        public int? IdCantiere { set; get; }

        public string Provincia { get; set; }

        public string Comune { get; set; }

        public string Cap { get; set; }

        public string Indirizzo { get; set; }

        public string IndirizzoMappa => Indirizzo + " " + Comune + " " + Provincia;

        public decimal? Latitudine { get; set; }

        public decimal? Longitudine { get; set; }

        public string AutorizzazioneAlSubappalto { get; set; }

        #endregion

        #region Costruttori

        /// <summary>
        ///     Costruttore per la serializzazione
        /// </summary>
        public Cantiere()
        {
        }

        public Cantiere(int? idCantiere, string provincia, string comune, string cap,
            string indirizzo)
        {
            IdCantiere = idCantiere;
            Provincia = provincia;
            Comune = comune;
            Cap = cap;
            Indirizzo = indirizzo;
        }

        #endregion
    }
}