﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    public class Presenze
    {
        public int Anno { get; set; }

        public int Mese { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public DateTime? DataNascita { get; set; }

        public DateTime? DataPrimoAccesso { get; set; }

        public DateTime? DataUltimoAccesso { get; set; }

        public int NumeroPresenze { get; set; }

        public string ImpresaRagioneSociale { get; set; }

        public string ImpresaPartitaIva { get; set; }
    }
}