﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Entities
{
    public class ControlloWhiteList
    {
        public Lavoratore Lavoratore { get; set; }

        public string LavoratoreCodiceFiscale
        {
            get
            {
                if (Lavoratore != null)
                {
                    return Lavoratore.CodiceFiscale;
                }

                return string.Empty;
            }
        }

        public Impresa Impresa { get; set; }

        public DateTime? DataUltimaDenuncia { get; set; }

        public DateTime? DataUltimaDenunciaLavoratore { get; set; }

        public bool RapportoImpresaLavoratore { get; set; }
    }
}