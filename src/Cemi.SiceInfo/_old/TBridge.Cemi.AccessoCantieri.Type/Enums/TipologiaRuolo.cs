namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaRuolo
    {
        ResponsabileSicurezza = 0,
        ResponsabileLavoratori = 1,
        ResponsabileLegale = 3,
        Titolare = 4,
        Socio = 5,
        Altro = 2
    }
}