namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaAnomalia
    {
        No = 0,
        NonInLista,
        PeriodoNonValido
    }
}