namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaFornitore
    {
        Tymbro = 2,
        ClickFind = 3,
        ProgettiSoluzioni = 4,
        TreTorri = 5,
        Manuale = 6,
        Trexom = 7,
        WebService = 8
    }
}