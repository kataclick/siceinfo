namespace TBridge.Cemi.AccessoCantieri.Type.Enums
{
    public enum TipologiaAnomaliaControlli
    {
        Impresa = 0,
        Denuncia,
        PresenzaLavoratore,
        OreLavoratore,
        Qualsiasi
    }
}