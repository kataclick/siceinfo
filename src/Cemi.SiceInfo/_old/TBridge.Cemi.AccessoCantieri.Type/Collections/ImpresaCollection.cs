using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class ImpresaCollection : List<Impresa>
    {
        /// <summary>
        ///     Controlla la presenza dell'impresa nella lista; l'uguaglianza si basa su idImpresa
        /// </summary>
        /// <param name="impresa"></param>
        /// <returns></returns>
        public new bool Contains(Impresa impresa)
        {
            foreach (Impresa imp in this)
            {
                //controlliamo se l'impresa � presente
                if (imp.TipoImpresa == TipologiaImpresa.SiceNew && impresa.TipoImpresa == TipologiaImpresa.SiceNew
                    && imp.IdImpresa == impresa.IdImpresa)
                {
                    return true;
                }

                if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                    && imp.IdImpresa.HasValue && impresa.IdImpresa.HasValue
                    && imp.IdImpresa == impresa.IdImpresa)
                {
                    return true;
                }

                if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                    && !imp.IdImpresa.HasValue && !impresa.IdImpresa.HasValue
                    && imp.IdTemporaneo == impresa.IdTemporaneo)
                {
                    return true;
                }
            }

            return false;
        }


        public Impresa Select(Impresa impresa)
        {
            if (impresa == null)
            {
                throw new ArgumentNullException("impresa");
            }
            foreach (Impresa imp in this)
            {
                //controlliamo se l'impresa � presente
                if (imp.TipoImpresa == TipologiaImpresa.SiceNew && impresa.TipoImpresa == TipologiaImpresa.SiceNew
                    && imp.IdImpresa == impresa.IdImpresa)
                {
                    return imp;
                }

                if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                    && imp.IdImpresa.HasValue && impresa.IdImpresa.HasValue
                    && imp.IdImpresa == impresa.IdImpresa)
                {
                    return imp;
                }

                if (imp.TipoImpresa == TipologiaImpresa.Nuova && impresa.TipoImpresa == TipologiaImpresa.Nuova
                    && !imp.IdImpresa.HasValue && !impresa.IdImpresa.HasValue
                    && imp.IdTemporaneo == impresa.IdTemporaneo)
                {
                    return imp;
                }
            }

            return null;
        }

        //public TipologiaAnomalia ControlloTimbrature(string codFisc, DateTime dataOra)
        //{
        //    foreach (Impresa imp in this)
        //    {
        //        if (imp.CodiceFiscale != null)
        //        {
        //            if (imp.CodiceFiscale == codFisc)
        //                return TipologiaAnomalia.NO;
        //        }
        //    }
        //    return TipologiaAnomalia.NonInLista;
        //}

        public Impresa GetImpresaAutonomaCodFisc(string codFisc)
        {
            Impresa imprRet = null;

            foreach (Impresa impr in this)
            {
                if (impr.CodiceFiscale != null && impr.LavoratoreAutonomo)
                    if (impr.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                    {
                        imprRet = impr;
                        break;
                    }
            }

            return imprRet;
        }
    }
}