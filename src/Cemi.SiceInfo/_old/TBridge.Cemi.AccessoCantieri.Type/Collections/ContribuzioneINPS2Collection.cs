﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class ContribuzioneInps2Collection : List<ContribuzioneINPS2>
    {
        /// <summary>
        ///     Ritorna la partitaIva dell'impresa se risulta contribuzione per il mese dato
        /// </summary>
        public string ContainsContribuzione(int mese, int anno)
        {
            foreach (ContribuzioneINPS2 cont in this)
            {
                if (cont.Superato.HasValue)
                {
                    if (cont.Superato.Value)
                        return cont.PartitaIVAImpresa;
                }
            }
            return null;
        }

        /// <summary>
        ///     Ritorna true se c'è contribuzione per quel mese con quell'impresa
        /// </summary>
        public bool? ContainsImpresaAnnoMese(string partitaIVA, int mese, int anno)
        {
            bool? ret = null;

            foreach (ContribuzioneINPS2 cont in this)
            {
                if (cont.PartitaIVAImpresa != null)
                {
                    if (cont.PartitaIVAImpresa == partitaIVA)
                    {
                        if (cont.Anno == anno && cont.Mese == mese)
                        {
                            if (cont.Superato.HasValue)
                                //if (cont.Superato.Value)
                                ret = cont.Superato.Value;
                        }
                    }
                }
            }
            return ret;
        }

        /// <summary>
        ///     Ritorna la data massima di ultima contribuzione con quella impresa
        /// </summary>
        public DateTime? ContainsImpresa(string partitaIVA, int mese, int anno)
        {
            DateTime? ret = null;
            foreach (ContribuzioneINPS2 cont in this)
            {
                if (cont.PartitaIVAImpresa != null)
                {
                    if (cont.PartitaIVAImpresa == partitaIVA)
                    {
                        if (cont.Superato.HasValue)
                        {
                            if (cont.Superato.Value)
                            {
                                if (ret.HasValue)
                                {
                                    if (new DateTime(cont.Anno, cont.Mese, 1) > ret.Value)
                                        ret = new DateTime(cont.Anno, cont.Mese, 1);
                                }
                                else
                                {
                                    ret = new DateTime(cont.Anno, cont.Mese, 1);
                                }
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}