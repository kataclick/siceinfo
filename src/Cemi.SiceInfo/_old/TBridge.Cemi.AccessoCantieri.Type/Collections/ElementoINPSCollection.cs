﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class ElementoINPSCollection : List<ElementoInps>
    {
    }
}