﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class RilevatoreCantiereCollection : List<RilevatoreCantiere>
    {
        public void Termina(int idRilevatore)
        {
            foreach (RilevatoreCantiere ril in this)
            {
                if (ril.IdRilevatore == idRilevatore && !ril.DataFine.HasValue)
                {
                    ril.DataFine = DateTime.Now;
                    ril.Terminato = true;
                    break;
                }
            }
        }

        public bool ContainsCodiceRilevatore(string codiceRilevatore)
        {
            foreach (RilevatoreCantiere ril in this)
            {
                if (ril.CodiceRilevatore.ToUpper().Trim() == codiceRilevatore.ToUpper().Trim() &&
                    (!ril.DataFine.HasValue || ril.DataFine.Value <= DateTime.Now))
                {
                    return true;
                }
            }
            return false;
        }
    }
}