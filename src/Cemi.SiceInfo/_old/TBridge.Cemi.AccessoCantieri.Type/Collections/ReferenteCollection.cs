﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class ReferenteCollection : List<Referente>
    {
        //public bool ContainsCodiceFiscale(string codFisc)
        //{
        //    foreach (Referente alp in this)
        //    {
        //        if (alp.CodiceFiscale == codFisc)
        //            return true;
        //    }
        //    return false;
        //}

        public TipologiaAnomalia ControlloTimbrature(string codFisc, DateTime dataOra)
        {
            foreach (Referente alp in this)
            {
                try
                {
                    if (alp.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                        return TipologiaAnomalia.No;
                }
                catch
                {
                }
            }
            return TipologiaAnomalia.NonInLista;
        }

        public Referente GetReferenteCodFisc(string codFisc)
        {
            Referente altrRet = null;

            foreach (Referente altr in this)
            {
                if (altr.CodiceFiscale.ToUpper().Trim() == codFisc.ToUpper().Trim())
                {
                    altrRet = altr;
                    break;
                }
            }

            return altrRet;
        }
    }
}