using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class LavoratoreCollection : List<Lavoratore>
    {
        /// <summary>
        ///     Controlla la presenza del lavoratore nella lista; l'uguaglianza si basa su idLavoratore
        /// </summary>
        /// <param name="lavoratore"></param>
        /// <returns></returns>
        public new bool Contains(Lavoratore lavoratore)
        {
            foreach (Lavoratore lav in this)
            {
                //controlliamo se il lavoratore � presente
                if (lav.IdLavoratore.HasValue && lavoratore.IdLavoratore.HasValue &&
                    lav.IdLavoratore == lavoratore.IdLavoratore)
                    return true;
            }
            return false;
        }

        public bool ContainsCodFisc(Lavoratore lavoratore)
        {
            foreach (Lavoratore lav in this)
            {
                if (lav.CodiceFiscale == lavoratore.CodiceFiscale)
                    return true;
            }
            return false;
        }

        public void RemoveCodFisc(Lavoratore lavoratore)
        {
            Lavoratore lavo = null;
            foreach (Lavoratore lav in this)
            {
                if (lav.CodiceFiscale == lavoratore.CodiceFiscale)
                {
                    lavo = lav;
                }
            }

            if (lavo != null)
                Remove(lavo);
        }

        public LavoratoreCollection GetByCodFisc(Lavoratore lavoratore)
        {
            LavoratoreCollection lavColl = new LavoratoreCollection();
            foreach (Lavoratore lav in this)
            {
                //controlliamo se il lavoratore con il dato codice fiscale � presente
                try
                {
                    if (lav.CodiceFiscale == lavoratore.CodiceFiscale)
                        lavColl.Add(lav);
                }
                catch
                {
                }
            }
            return lavColl;
        }

        public Lavoratore GetByCodFisc(string codiceFiscale)
        {
            Lavoratore lavRes = null;

            foreach (Lavoratore lav in this)
            {
                if (lav.CodiceFiscale == codiceFiscale)
                {
                    lavRes = lav;
                    break;
                }
            }

            return lavRes;
        }

        //public Lavoratore GetLavoratore(int idLavoratore)
        //{
        //    foreach (Lavoratore lav in this)
        //    {
        //        try
        //        {
        //            if (lav.IdDomandaLavoratore == idLavoratore)
        //                return lav;
        //        }
        //        catch
        //        {
        //        }
        //    }
        //    return null;
        //}
    }
}