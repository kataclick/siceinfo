using System;
using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Collections
{
    [Serializable]
    public class SubappaltoCollection : List<Subappalto>
    {
        public bool Contains(Impresa appaltante, Impresa appaltata)
        {
            foreach (Subappalto sub in this)
            {
                if (sub.Appaltante != null && sub.Appaltata != null)
                {
                    if (sub.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew &&
                        sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        if (sub.Appaltante.IdImpresa == appaltante.IdImpresa &&
                            sub.Appaltata.IdImpresa == appaltata.IdImpresa)
                        {
                            return true;
                        }
                    }

                    if (sub.Appaltante.TipoImpresa == TipologiaImpresa.Nuova &&
                        sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                    {
                        if ((sub.Appaltante.PartitaIva == appaltante.PartitaIva ||
                             sub.Appaltante.CodiceFiscale == appaltante.CodiceFiscale)
                            &&
                            (sub.Appaltata.PartitaIva == appaltata.PartitaIva ||
                             sub.Appaltata.CodiceFiscale == appaltata.CodiceFiscale))
                        {
                            return true;
                        }
                    }

                    if (sub.Appaltante.TipoImpresa == TipologiaImpresa.Nuova &&
                        sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        if ((sub.Appaltante.PartitaIva == appaltante.PartitaIva ||
                             sub.Appaltante.CodiceFiscale == appaltante.CodiceFiscale)
                            &&
                            sub.Appaltata.IdImpresa == appaltata.IdImpresa)
                        {
                            return true;
                        }
                    }

                    if (sub.Appaltante.TipoImpresa == TipologiaImpresa.SiceNew &&
                        sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                    {
                        if (sub.Appaltante.IdImpresa == appaltante.IdImpresa
                            &&
                            (sub.Appaltata.PartitaIva == appaltata.PartitaIva ||
                             sub.Appaltata.CodiceFiscale == appaltata.CodiceFiscale))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool ContainsAppaltata(Impresa appaltata)
        {
            foreach (Subappalto sub in this)
            {
                if (sub.Appaltata.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    if (sub.Appaltata.IdImpresa == appaltata.IdImpresa)
                    {
                        return true;
                    }
                }

                if (sub.Appaltata.TipoImpresa == TipologiaImpresa.Nuova)
                {
                    if (sub.Appaltata.PartitaIva == appaltata.PartitaIva
                        ||
                        sub.Appaltata.CodiceFiscale == appaltata.CodiceFiscale)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //public TipologiaAnomalia ControlloTimbrature(string codFisc, DateTime dataOra)
        //{
        //    if (codFisc == string.Empty) return TipologiaAnomalia.NonInLista;

        //    foreach (Subappalto sub in this)
        //    {
        //        if (sub.CodiceFiscaleAppaltata != null)
        //        {
        //            if (sub.CodiceFiscaleAppaltata.ToUpper().Trim() == codFisc.ToUpper().Trim())
        //                return TipologiaAnomalia.No;
        //        }

        //        if (sub.CodiceFiscaleAppaltatrice != null)
        //        {
        //            if (sub.CodiceFiscaleAppaltatrice.ToUpper().Trim() == codFisc.ToUpper().Trim())
        //                return TipologiaAnomalia.No;
        //        }
        //    }
        //    return TipologiaAnomalia.NonInLista;
        //}

        private static DateTime OnlyDate(DateTime data)
        {
            return new DateTime(data.Year, data.Month, data.Day);
        }

        public TipologiaAnomalia ControlloTimbratureImpresa(string partitaIva, DateTime dataOra)
        {
            if (string.IsNullOrEmpty(partitaIva))
                return TipologiaAnomalia.NonInLista;

            foreach (Subappalto sub in this)
            {
                if (sub.PartitaIvaAppaltata != null && sub.CodiceFiscaleAppaltata != null)
                {
                    if (sub.PartitaIvaAppaltata.ToUpper().Trim() == partitaIva.ToUpper().Trim() ||
                        sub.CodiceFiscaleAppaltata.ToUpper().Trim() == partitaIva.ToUpper().Trim())
                    {
                        if (sub.DataInizioAttivitaAppaltata.HasValue)
                        {
                            if (OnlyDate(sub.DataInizioAttivitaAppaltata.Value.Date) > OnlyDate(dataOra))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        if (sub.DataFineAttivitaAppaltata.HasValue)
                        {
                            if (OnlyDate(sub.DataFineAttivitaAppaltata.Value.Date) < OnlyDate(dataOra.Date))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        if (sub.DataInizioAttivitaAppaltata.HasValue && sub.DataFineAttivitaAppaltata.HasValue)
                        {
                            if (OnlyDate(sub.DataInizioAttivitaAppaltata.Value) <= OnlyDate(dataOra) &&
                                OnlyDate(sub.DataFineAttivitaAppaltata.Value) >= OnlyDate(dataOra))
                                return TipologiaAnomalia.No;
                        }

                        if (sub.DataInizioAttivitaAppaltata.HasValue && sub.DataFineAttivitaAppaltata.HasValue)
                        {
                            if (OnlyDate(sub.DataInizioAttivitaAppaltata.Value.Date) > OnlyDate(dataOra) ||
                                OnlyDate(sub.DataFineAttivitaAppaltata.Value.Date) < OnlyDate(dataOra))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        return TipologiaAnomalia.No;
                    }
                }

                if (sub.PartitaIvaAppaltatrice != null && sub.CodiceFiscaleAppaltatrice != null)
                {
                    if (sub.PartitaIvaAppaltatrice.ToUpper().Trim() == partitaIva.ToUpper().Trim() ||
                        sub.CodiceFiscaleAppaltatrice.ToUpper().Trim() == partitaIva.ToUpper().Trim())
                    {
                        if (sub.DataInizioAttivitaAppaltatrice.HasValue)
                        {
                            if (OnlyDate(sub.DataInizioAttivitaAppaltatrice.Value.Date) > OnlyDate(dataOra))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        if (sub.DataFineAttivitaAppaltatrice.HasValue)
                        {
                            if (OnlyDate(sub.DataFineAttivitaAppaltatrice.Value.Date) < OnlyDate(dataOra.Date))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        if (sub.DataInizioAttivitaAppaltatrice.HasValue && sub.DataFineAttivitaAppaltatrice.HasValue)
                        {
                            if (OnlyDate(sub.DataInizioAttivitaAppaltatrice.Value) <= OnlyDate(dataOra) &&
                                OnlyDate(sub.DataFineAttivitaAppaltatrice.Value) >= OnlyDate(dataOra))
                                return TipologiaAnomalia.No;
                        }

                        if (sub.DataInizioAttivitaAppaltatrice.HasValue && sub.DataFineAttivitaAppaltatrice.HasValue)
                        {
                            if (OnlyDate(sub.DataInizioAttivitaAppaltatrice.Value.Date) > OnlyDate(dataOra) ||
                                OnlyDate(sub.DataFineAttivitaAppaltatrice.Value.Date) < OnlyDate(dataOra))
                                return TipologiaAnomalia.PeriodoNonValido;
                        }

                        return TipologiaAnomalia.No;
                    }
                }
            }
            return TipologiaAnomalia.NonInLista;
        }

        public Impresa GetImpresaByCodiceFiscale(string codiceFiscale)
        {
            foreach (Subappalto sub in this)
            {
                if (sub.Appaltata != null
                    && sub.Appaltata.CodiceFiscale.ToUpper() == codiceFiscale.ToUpper())
                {
                    return sub.Appaltata;
                }
            }

            return null;
        }

        public Subappalto GetSubappaltoByCodiceFiscale(string codiceFiscale, string codiceFiscaleAppaltatrice)
        {
            foreach (Subappalto sub in this)
            {
                if (string.IsNullOrWhiteSpace(codiceFiscaleAppaltatrice))
                {
                    if (sub.Appaltata != null
                        && sub.Appaltante == null
                        && !string.IsNullOrWhiteSpace(codiceFiscale)
                        && !string.IsNullOrWhiteSpace(sub.Appaltata.CodiceFiscale)
                        && sub.Appaltata.CodiceFiscale.ToUpper().Trim() == codiceFiscale.ToUpper().Trim())
                    {
                        return sub;
                    }
                }
                else
                {
                    if (sub.Appaltata != null
                        && sub.Appaltante != null
                        && !string.IsNullOrWhiteSpace(codiceFiscale)
                        && !string.IsNullOrWhiteSpace(sub.Appaltata.CodiceFiscale)
                        && !string.IsNullOrWhiteSpace(sub.Appaltante.CodiceFiscale)
                        && sub.Appaltata.CodiceFiscale.ToUpper().Trim() == codiceFiscale.ToUpper().Trim()
                        && sub.Appaltante.CodiceFiscale.ToUpper().Trim() == codiceFiscaleAppaltatrice.ToUpper().Trim())
                    {
                        return sub;
                    }
                }
            }

            return null;
        }
    }
}