﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Dto
{
    public class AssegnazioneLettore
    {
        public int IdLettore { get; set; }
        public string Utente { get; set; }
        public string Indirizzo { get; set; }
        public string Comune { get; set; }
        public string Provincia { get; set; }
        public string Cap { get; set; }

        public string IndirizzoCantiere
        {
            get
            {
                string indCantiere = null;

                if (!string.IsNullOrEmpty(Provincia))
                {
                    indCantiere = string.Format("{0} {1} {2} ({3})", Indirizzo, Cap, Comune, Provincia);
                }
                else
                {
                    indCantiere = string.Format("{0} {1} {2}", Indirizzo, Cap, Comune);
                }

                return indCantiere;
            }
        }

        public DateTime? DataInizio { get; set; }
        public DateTime? DataFine { get; set; }
    }
}