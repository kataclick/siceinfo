﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Dto
{
    public class Lettore
    {
        public int Id { get; set; }

        public string Codice { get; set; }

        public int IdFornitore { get; set; }

        public string Fornitore { get; set; }

        public bool Assegnabile { get; set; }

        public DateTime DataInserimento { get; set; }

        public bool InvioWhiteList { get; set; }
    }
}