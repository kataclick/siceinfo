﻿using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Accesso
    {
        [DataMember]
        public DateTime DataOra { get; set; }

        [DataMember]
        public string CodiceFiscaleLavoratore { get; set; }

        [DataMember]
        public string CodiceFiscaleImpresa { get; set; }

        [DataMember]
        public bool Ingresso { get; set; }
    }
}