﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Impresa
    {
        [DataMember]
        public string RagioneSociale { get; set; }

        [DataMember]
        public string CodiceFiscale { get; set; }

        [DataMember]
        public string PartitaIVA { get; set; }

        [DataMember]
        public string Indirizzo { get; set; }

        [DataMember]
        public string Comune { get; set; }

        [DataMember]
        public string Provincia { get; set; }

        [DataMember]
        public string CAP { get; set; }

        [DataMember]
        public string ContrattoApplicato { get; set; }

        [DataMember]
        public List<Referente> Referenti { get; set; }

        [DataMember]
        public string CodiceFiscaleAppaltatrice { get; set; }

        [DataMember]
        public List<Lavoratore> Lavoratori { get; set; }
    }
}