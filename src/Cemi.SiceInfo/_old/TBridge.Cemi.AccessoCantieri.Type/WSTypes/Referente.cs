﻿using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Referente
    {
        [DataMember]
        public string Cognome { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public DateTime DataNascita { get; set; }

        [DataMember]
        public string CodiceFiscale { get; set; }

        [DataMember]
        public string Telefono { get; set; }

        [DataMember]
        public string Cellulare { get; set; }

        [DataMember]
        public string EMail { get; set; }

        [DataMember]
        public string PEC { get; set; }
    }
}