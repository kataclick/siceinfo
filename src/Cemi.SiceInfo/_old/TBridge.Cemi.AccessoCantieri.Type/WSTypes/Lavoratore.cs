﻿using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.AccessoCantieri.Type.WSTypes
{
    [DataContract]
    public class Lavoratore
    {
        [DataMember]
        public string Cognome { get; set; }

        [DataMember]
        public string Nome { get; set; }

        [DataMember]
        public string CodiceFiscale { get; set; }

        [DataMember]
        public DateTime? DataNascita { get; set; }

        [DataMember]
        public string ContrattoApplicato { get; set; }

        [DataMember]
        public DateTime? Dal { get; set; }

        [DataMember]
        public DateTime? Al { get; set; }

        [DataMember]
        public string CodiceFiscaleImpresa { get; set; }
    }
}