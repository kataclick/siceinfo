﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class PresenzeFilter
    {
        public int IdCantiere { get; set; }

        public DateTime Dal { get; set; }

        public DateTime Al { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIvaImpresa { get; set; }
    }
}