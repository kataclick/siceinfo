﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class ControlloIdentitaFilter
    {
        public int? Mese { get; set; }

        public int? Anno { get; set; }

        public int? IdImpresa { get; set; }

        public int? IdCantiere { get; set; }

        public string CodiceFiscale { get; set; }

        public TipologiaAnomaliaControlli? TipologiaAnomaliaControlli { get; set; }

        public TipologiaRuoloTimbratura? TipologiaRuoloTimbratura { get; set; }

        public string PartitaIVA { get; set; }
    }
}