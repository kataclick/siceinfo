﻿namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    public class LavoratoreFilter
    {
        public int IdCantiere { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }
    }
}