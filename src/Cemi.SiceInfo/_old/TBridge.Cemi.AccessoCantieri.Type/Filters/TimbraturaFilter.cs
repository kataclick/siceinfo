﻿using System;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class TimbraturaFilter
    {
        public DateTime? DataInizio { get; set; }

        public DateTime? DataFine { get; set; }

        public TipologiaRuoloTimbratura? TipoUtente { get; set; }

        public int? IdImpresa { get; set; }

        public int? IdAccessoCantieriWhiteList { get; set; }

        public string CodiceFiscale { get; set; }

        public int? IdCommittente { get; set; }
    }
}