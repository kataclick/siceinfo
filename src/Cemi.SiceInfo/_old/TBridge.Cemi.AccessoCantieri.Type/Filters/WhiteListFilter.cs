﻿using System;

namespace TBridge.Cemi.AccessoCantieri.Type.Filters
{
    [Serializable]
    public class WhiteListFilter
    {
        public string Indirizzo { get; set; }

        public string Comune { get; set; }

        //public DateTime? Mese { get; set; }

        public int? IdUtente { get; set; }

        //public String IvaCodFisc { get; set; }

        //public String RagSocDenominazione { get; set; }

        public int? IdCommittente { get; set; }
    }
}