﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Data;
using TBridge.Cemi.Type.Entities.Corsi.Esem;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Corsi;
using TBridge.Cemi.Type.Filters.Corsi.Esem;

namespace TBridge.Cemi.Corsi.Business
{
    public class CorsiEsemBusiness
    {
        private readonly CorsiEsemDataAccess dataAccess = new CorsiEsemDataAccess();

        public List<TipoCorso> GetTipiCorso(Utente utente)
        {
            return dataAccess.GetTipiCorso(utente);
        }

        public List<Sede> GetSedi(Utente utente)
        {
            return dataAccess.GetSedi(utente);
        }

        public List<Corso> GetCorsi(Utente utente, CorsoFilter filter)
        {
            return dataAccess.GetCorsi(utente, filter);
        }

        public List<LavoratoreCorsi> GetCorsiScadenzeImpresa(Utente utente, string codiceFiscaleImpresa, int? idImpresa)
        {
            return dataAccess.GetCorsiScadenzeImpresa(utente, codiceFiscaleImpresa, idImpresa);
        }

        public LavoratoreCorsi GetCorsiScadenzeLavoratore(Utente utente, string codiceFiscaleLavoratore,
            int? idLavoratore)
        {
            return dataAccess.GetCorsiScadenzeLavoratore(utente, codiceFiscaleLavoratore, idLavoratore);
        }

        public List<LavoratoreCorsi> GetIscrizioniImpresa(Utente utente, PartecipazioneLavoratoreImpresaFilter filtro)
        {
            List<LavoratoreCorsi> iscrizioni =
                dataAccess.GetIscrizioniImpresa(utente, filtro.ImpresaIvaFisc, filtro.IdImpresa);
            List<LavoratoreCorsi> res = new List<LavoratoreCorsi>();

            foreach (LavoratoreCorsi lc in iscrizioni)
            {
                bool add = true;

                if (!string.IsNullOrEmpty(filtro.LavoratoreCognome) &&
                    !lc.Lavoratore.Cognome.Contains(filtro.LavoratoreCognome))
                {
                    add = false;
                }

                if (!string.IsNullOrEmpty(filtro.LavoratoreNome) && !lc.Lavoratore.Nome.Contains(filtro.LavoratoreNome))
                {
                    add = false;
                }

                if (!string.IsNullOrEmpty(filtro.LavoratoreCodiceFiscale) &&
                    !lc.Lavoratore.CodiceFiscale.Contains(filtro.LavoratoreCodiceFiscale))
                {
                    add = false;
                }

                if (add)
                    res.Add(lc);
            }

            return res;
        }

        public LavoratoreCorsi GetIscrizioniLavoratore(Utente utente, int idLavoratore, string codiceFiscaleLavoratore)
        {
            return dataAccess.GetIscrizioniLavoratore(utente, idLavoratore, codiceFiscaleLavoratore);
        }

        public bool IscriviLavoratore(out string messaggio, Utente utente, Iscrizione iscrizione)
        {
            return dataAccess.IscriviLavoratore(out messaggio, utente, iscrizione);
        }

        public bool CorsiGratuita(int idLavoratore, DateTime dataIscrizione)
        {
            return dataAccess.CorsiGratuita(idLavoratore, dataIscrizione);
        }

        public bool CorsiTicket(DateTime dataCorso, DateTime dataAssunzione, bool primaEsperienza)
        {
            return dataAccess.CorsiTicket(dataCorso, dataAssunzione, primaEsperienza);
        }

        public bool CancellaIscrizione(out string messaggio, Utente utente, Iscrizione iscrizione)
        {
            return dataAccess.CancellaIscrizione(out messaggio, utente, iscrizione);
        }
    }
}