using System;
using System.Collections.Generic;
using TBridge.Cemi.Corsi.Data;
using TBridge.Cemi.Corsi.Type.DataSets;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Filters.Corsi;

namespace TBridge.Cemi.Corsi.Business
{
    public class CorsiBusiness
    {
        private readonly CorsiDataAccess dataAccess = new CorsiDataAccess();

        public LocazioneCollection GetLocazioni()
        {
            return dataAccess.GetLocazioni();
        }

        public bool InsertLocazione(Locazione locazione)
        {
            return dataAccess.InsertLocazione(locazione);
        }

        public CorsoCollection GetCorsiAll()
        {
            return dataAccess.GetCorsiAll(false, false);
        }

        public CorsoCollection GetCorsiPianificati()
        {
            return dataAccess.GetCorsiAll(true, false);
        }

        public CorsoCollection GetCorsiPianificabili()
        {
            return dataAccess.GetCorsiAll(false, true);
        }

        public Corso GetCorsoByKey(int idCorso)
        {
            return dataAccess.GetCorsoByKey(idCorso);
        }

        public bool InsertCorso(Corso corso)
        {
            return dataAccess.InsertCorso(corso);
        }

        public ProgrammazioneModuloCollection GetProgrammazioni(ProgrammazioneModuloFilter filtro)
        {
            return dataAccess.GetProgrammazioni(filtro);
        }

        public bool InsertUpdateProgrammazione(Programmazione programmazione)
        {
            if (!programmazione.IdProgrammazione.HasValue)
            {
                return dataAccess.InsertProgrammazione(programmazione);
            }
            return dataAccess.UpdateProgrammazione(programmazione);
        }

        public ProgrammazioneModuloCollection GetProgrammazione(int idProgrammazione)
        {
            return dataAccess.GetProgrammazione(idProgrammazione);
        }

        public ModuloCollection GetModuliConProgrammazioniFuture(int idCorso, DateTime dataInizio, DateTime dataFine,
            bool? prenotabili, bool overBooking)
        {
            return dataAccess.GetModuliConProgrammazioniFuture(idCorso, dataInizio, dataFine, prenotabili, overBooking);
        }

        public LavoratoreCollection GetLavoratoriSiceNew(LavoratoreFilter filtro)
        {
            return dataAccess.GetLavoratoriSiceNew(filtro);
        }

        public LavoratoreCollection GetLavoratoriAnagraficaCondivisa(LavoratoreFilter filtro)
        {
            return dataAccess.GetLavoratoriAnagraficaCondivisa(filtro);
        }

        public ImpresaCollection GetImpreseAnagraficaCondivisa(ImpresaFilter filtro)
        {
            return dataAccess.GetImpreseAnagraficaCondivisa(filtro);
        }

        public Lavoratore GetLavoratoreAnagraficaCondivisaByKey(int idLavoratore)
        {
            return dataAccess.GetLavoratoreAnagraficaCondivisaByKey(idLavoratore);
        }

        public Impresa GetImpresaAnagraficaCondivisaByKey(int idImpresa)
        {
            return dataAccess.GetImpresaAnagraficaCondivisaByKey(idImpresa);
        }

        public Lavoratore GetLavoratoreSiceNewByKey(int idLavoratore)
        {
            return dataAccess.GetLavoratoreSiceNewByKey(idLavoratore);
        }

        public bool InsertPartecipazione(Partecipazione partecipazione, bool overBooking)
        {
            return dataAccess.InsertPartecipazione(partecipazione, overBooking);
        }

        public LavoratoreCollection GetPartecipanti(PartecipanteFilter filtro)
        {
            return dataAccess.GetPartecipanti(filtro);
        }

        public ImpresaCollection GetImpreseSiceNewEAnagrafica(ImpresaFilter filtro)
        {
            return dataAccess.GetImpreseSiceNewEAnagrafica(filtro);
        }

        public Impresa GetImpresaSiceNewByKey(int idImpresa)
        {
            return dataAccess.GetImpresaSiceNewByKey(idImpresa);
        }

        public Impresa GetImpresaCorsiByKey(int idImpresa)
        {
            return dataAccess.GetImpresaCorsiByKey(idImpresa);
        }

        public ProgrammazioneModuloCollection GetPartecipazioni(PartecipazioneFilter filtro)
        {
            return dataAccess.GetPartecipazione(filtro);
        }

        public ProgrammazioneModuloCollection GetPartecipazioni(int idPartecipazione)
        {
            return dataAccess.GetPartecipazione(idPartecipazione);
        }

        public bool UpdatePartecipazioneModuloPresenza(int idPartecipazioneModulo, bool presente)
        {
            return dataAccess.UpdatePartecipazioneModuloPresenza(idPartecipazioneModulo, presente);
        }

        public string ConvertiBoolInSemaforo(bool? stato)
        {
            if (!stato.HasValue)
            {
                return URLSEMAFOROGIALLO;
            }
            if (stato.Value)
            {
                return URLSEMAFOROVERDE;
            }
            return URLSEMAFOROROSSO;
        }

        public bool UpdateProgrammazioneModuloConfermaPresenze(int idProgrammazioneModulo)
        {
            return dataAccess.UpdateProgrammazioneModuloConfermaPresenze(idProgrammazioneModulo);
        }

        public bool DeleteProgrammazione(int idProgrammazione)
        {
            return dataAccess.DeleteProgrammazione(idProgrammazione);
        }

        public bool DeleteCorso(int idCorso)
        {
            return dataAccess.DeleteCorso(idCorso);
        }

        public PartecipazioneModuloCollection GetPartecipazioniByLavoratoreImpresa(
            PartecipazioneLavoratoreImpresaFilter filtro)
        {
            return dataAccess.GetPartecipazioniByLavoratoreImpresa(filtro);
        }

        public int? GetUltimaImpresaLavoratore(int idLavoratore)
        {
            return dataAccess.GetUltimaImpresaLavoratore(idLavoratore);
        }

        public bool DeletePartecipazione(int idPartecipazioneModulo)
        {
            return dataAccess.DeletePartecipazione(idPartecipazioneModulo);
        }

        public Locazione GetLocazioneByKey(int idLocazione)
        {
            return dataAccess.GetLocazioneByKey(idLocazione);
        }

        public TitoloDiStudioCollection GetTitoliDiStudioAll()
        {
            return dataAccess.GetTitoliDiStudioAll();
        }

        public TipoIscrizioneCollection GetTipiIscrizioneAll()
        {
            return dataAccess.GetTipiIscrizioneAll();
        }

        public TipoContrattoCollection GetTipiContrattoAll()
        {
            return dataAccess.GetTipiContrattoAll();
        }

        public Lavoratore GetCorsiLavoratore(int idLavoratore)
        {
            return dataAccess.GetCorsiLavoratore(idLavoratore);
        }

        public void GetDatiAggiuntiviPartecipazione(int idPartecipazione, out bool? primaEsperienza,
            out DateTime? dataAssunzione)
        {
            dataAccess.GetDatiAggiuntiviPartecipazione(idPartecipazione, out primaEsperienza, out dataAssunzione);
        }

        public bool UpdateCorsiLavoratore(Lavoratore lavoratore, int idPartecipazione)
        {
            return dataAccess.UpdateCorsiLavoratore(lavoratore, idPartecipazione);
        }

        public bool UpdatePartecipazione(int idPartecipazione, bool? primaEsperienza, DateTime? dataAssunzione)
        {
            return dataAccess.UpdatePartecipazione(idPartecipazione, primaEsperienza, dataAssunzione, null);
        }

        public EstrazioneFormedilImpresaCollection EstrazioneFormedilImprese(EstrazioneFormedilFilter filtro)
        {
            return dataAccess.EstrazioneFormedilImprese(filtro);
        }

        public EstrazioneFormedilLavoratoreCollection EstrazioneFormedilLavoratori(EstrazioneFormedilFilter filtro)
        {
            return dataAccess.EstrazioneFormedilLavoratori(filtro);
        }

        public bool EsisteImpresaConStessaIvaFisc(string partitaIva, string codiceFiscale)
        {
            return dataAccess.EsisteImpresaConStessaIvaFisc(partitaIva, codiceFiscale);
        }

        public bool OraValida(string oraFornita, out DateTime oraTrovata)
        {
            bool res = false;
            oraTrovata = new DateTime(1900, 1, 1);

            string[] oraSplitted = oraFornita.Split(':');
            if (oraSplitted.Length == 2)
            {
                int ora;
                int minuti;

                if (int.TryParse(oraSplitted[0], out ora)
                    && int.TryParse(oraSplitted[1], out minuti))
                {
                    if (ora >= 0 && ora <= 23
                        && minuti >= 0 && minuti <= 59)
                    {
                        res = true;
                        oraTrovata = new DateTime(1900, 1, 1, ora, minuti, 0);
                    }
                }
            }

            return res;
        }

        public bool EsisteLavoratoreConStessoCodiceFiscale(string codiceFiscale)
        {
            return dataAccess.EsisteLavoratoreConStessoCodiceFiscale(codiceFiscale);
        }

        public bool DeleteLocazione(int idLocazione)
        {
            return dataAccess.DeleteLocazione(idLocazione);
        }

        public bool LavoratoreSeguito16Ore(string codiceFiscale)
        {
            return dataAccess.LavoratoreSeguito16Ore(codiceFiscale);
        }

        #region Semafori

        public const string URLSEMAFOROGIALLO = "~/CeServizi/images/semaforoGiallo.png";
        public const string URLSEMAFOROROSSO = "~/CeServizi/images/semaforoRosso.png";
        public const string URLSEMAFOROVERDE = "~/CeServizi/images/semaforoVerde.png";

        #endregion

        #region Gestione prestazioni domande corsi

        /// <summary>
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public PrestazioniDomandeCorsiCollection GetPrestazioniDomandeCorsi(PrestazioneDomandaCorsoFilter filtro)
        {
            return dataAccess.GetPrestazioniDomandeCorsi(filtro);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public StatoDomandaCollection GetStatiDomanda()
        {
            return dataAccess.GetStatiDomanda();
        }

        public void AccogliPrestazioneDomandaCorso(int idPrestazioneDomandaCorso, int idUtente)
        {
            dataAccess.AggiornaStatoPrestazioneDomandaCorso(idPrestazioneDomandaCorso, "C", idUtente);
        }

        public void RespingiPrestazioneDomandaCorso(int idPrestazioneDomandaCorso, int idUtente)
        {
            dataAccess.AggiornaStatoPrestazioneDomandaCorso(idPrestazioneDomandaCorso, "R", idUtente);
        }

        public void AnnullaPrestazioneDomandaCorso(int idPrestazioneDomandaCorso, int idUtente)
        {
            dataAccess.AggiornaStatoPrestazioneDomandaCorso(idPrestazioneDomandaCorso, "A", idUtente);
        }

        public bool AggiornaLavoratoreBeneficiarioDomandaPrestazione(int idCorsiPrestazioneDomanda, int idLavoratore,
            int idUtente)
        {
            return dataAccess.AggiornaLavoratoreBeneficiarioDomandaPrestazione(idCorsiPrestazioneDomanda, idLavoratore,
                idUtente);
        }

        #endregion

        #region Anagrafica Unica

        public Type.DataSets.Corsi AnagraficaUnicaRicercaCorsi(AnagraficaUnicaCorsiFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaCorsi(filtro);
        }

        public Iscrizioni AnagraficaUnicaRicercaIscrizioni(AnagraficaUnicaIscrizioniFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaIscrizioni(filtro);
        }

        public Lavoratori AnagraficaUnicaRicercaLavoratori(AnagraficaUnicaLavoratoriFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaLavoratori(filtro);
        }

        public Imprese AnagraficaUnicaRicercaImprese(AnagraficaUnicaImpreseFilter filtro)
        {
            return dataAccess.AnagraficaUnicaRicercaImprese(filtro);
        }

        #endregion

        #region PrestazioniDTA

        public DateTime? DataErogazionePrestazioniDTA(int anno)
        {
            return dataAccess.DataErogazionePrestazioniDTA(anno);
        }

        public List<PrestazioneDTA> GetPrestazioniDTALavoratori(int anno, DTAFilter filtro)
        {
            return dataAccess.GetPrestazioniDTALavoratori(anno, filtro);
        }

        public void SalvaPrestazioniDTALavoratori(List<PrestazioneDTALavoratore> prestazioni)
        {
            dataAccess.SalvaPrestazioniDTALavoratori(prestazioni);
        }

        public void SalvaPrestazioneDTALavoratore(PrestazioneDTALavoratore prestazione)
        {
            dataAccess.SalvaPrestazioneDTALavoratore(prestazione, null);
        }

        public void GeneraDomandePrestazioniDTALavoratori(int anno)
        {
            dataAccess.GeneraDomandePrestazioniDTALavoratori(anno);
        }

        public Dictionary<int, string> GetPrestazioniDTAAnni()
        {
            return dataAccess.GetPrestazioniDTAAnni();
        }

        #endregion
    }
}