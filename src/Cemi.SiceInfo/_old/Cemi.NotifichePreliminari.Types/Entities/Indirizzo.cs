using System;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    /// <summary>
    ///     Rappresenta un singolo indirizzo di una notifica
    /// </summary>
    [Serializable]
    public class Indirizzo : TBridge.Cemi.Type.Entities.Geocode.Indirizzo
    {
        //public Indirizzo(string indirizzo, string civico, string comune, string provincia, string cap,
        //                 decimal? latitudine, decimal? longitudine)
        //{
        //    Indirizzo1 = indirizzo;
        //    Civico = civico;
        //    Comune = comune;
        //    Provincia = provincia;
        //    Cap = cap;
        //    Latitudine = latitudine;
        //    Longitudine = longitudine;
        //}

        //public Indirizzo(string indirizzo, string civico, string comune, string provincia, string cap,
        //                 decimal? latitudine, decimal? longitudine, string infoAggiuntiva)
        //{
        //    Indirizzo1 = indirizzo;
        //    Civico = civico;
        //    Comune = comune;
        //    Provincia = provincia;
        //    Cap = cap;
        //    Latitudine = latitudine;
        //    Longitudine = longitudine;
        //    InfoAggiuntiva = infoAggiuntiva;
        //}

        public int? IdIndirizzo { get; set; }

        //public string Indirizzo1
        //{
        //    get { return Via; }
        //    set { NomeVia = value; }
        //}

        //public string InfoAggiuntiva
        //{
        //    get { return InformazioniAggiuntive; }
        //    set { InformazioniAggiuntive = value; }
        //}

        //public string IndirizzoDenominazione
        //{
        //    get { return IndirizzoBase; }
        //}

        //public string IndirizzoPerGeocoder
        //{
        //    get { return String.Format("{0}, {1}", IndirizzoCompleto, Stato); }
        //}

        //public string IndirizzoPerGeocoderGenerico
        //{
        //    get { return IndirizzoCompletoGeocode; }
        //}

        public new string IndirizzoCompleto => string.Format("{0} {1}{2}{3}", NomeVia, Environment.NewLine, Comune,
            string.IsNullOrEmpty(Provincia) ? null : string.Format(" ({0})", Provincia));

        public DateTime? DataInizioLavori { get; set; }

        public string DescrizioneDurata { get; set; }

        public int? NumeroDurata { get; set; }

        public int? NumeroMassimoLavoratori { get; set; }

        public string DurataLavoriVisualizzazione => string.Format("{0} {1}", NumeroDurata, DescrizioneDurata);

        //public new String IndirizzoCompleto
        //{
        //    get
        //    {
        //        return ;
        //    }
        //}

        //public String IndirizzoDatiAggiuntivi
        //{
        //    get
        //    {
        //        StringBuilder res = new StringBuilder();

        //        if (this.DataInizioLavori.HasValue)
        //        {
        //            res.Append(String.Format("Data Inizio Lav. <b>{0}</b><br />", this.DataInizioLavori.Value.ToShortDateString()));
        //        }
        //        if (this.NumeroMassimoLavoratori.HasValue)
        //        {
        //            res.Append(String.Format("Num. max lav. <b>{0}</b><br />", this.NumeroMassimoLavoratori.ToString()));
        //        }
        //        if (!String.IsNullOrEmpty(this.DescrizioneDurata) && this.NumeroDurata.HasValue)
        //        {
        //            res.Append(String.Format("Durata <b>{0} {1}</b><br />", this.NumeroDurata, this.DescrizioneDurata));
        //        }

        //        return res.ToString();
        //    }
        //}

        public bool HaCoordinate()
        {
            return Georeferenziato;
        }
    }
}