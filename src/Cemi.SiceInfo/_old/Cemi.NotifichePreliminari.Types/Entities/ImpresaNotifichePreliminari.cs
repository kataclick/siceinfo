using System;
using Cemi.NotifichePreliminari.Types.Enums;
using Cemi.NotifichePreliminari.Types.Helpers;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    [Serializable]
    public class ImpresaNotifichePreliminari
    {
        public int IdImpresa { set; get; }
        // public Guid IdTemporaneo { get; set; }

        //public Int32? IdImpresaTelematica { get; set; }

        // public Int32? IdImpresaAnagrafica { get; set; }

        public string RagioneSociale { get; set; }

        //public Boolean LavoratoreAutonomo { get; set; }

        public string CodiceFiscale { get; set; }

        public string PartitaIva { get; set; }

        //public String CodiceFiscale { get; set; }

        // public String AttivitaPrevalente { get; set; }

        // public Int32? IdImpresa { get; set; }

        // public String IdCassaEdile { get; set; }

        //public String MatricolaINAIL { get; set; }

        // public String MatricolaINPS { get; set; }

        // public String MatricolaCCIAA { get; set; }

        public TBridge.Cemi.Type.Entities.Indirizzo Indirizzo { get; set; }

        public string IndirizzoCompleto => Indirizzo != null ? HelperMethods.FormattaIndirizzo(Indirizzo) : null;

        //public String Comune { get; set; }

        //public String Provincia { get; set; }

        //public String Cap { get; set; }

        //public String Telefono { get; set; }

        //public String Fax { get; set; }

        public IncaricoImpresa IncaricoImpresa { set; get; }

        public IdTipoIncaricoImpresa? TipoIncaricoId =>
            IncaricoImpresa != null ? IncaricoImpresa.Id : (IdTipoIncaricoImpresa?) null;

        public string TipoIncaricoDescrizione => IncaricoImpresa != null ? IncaricoImpresa.Descrizione : null;
    }
}