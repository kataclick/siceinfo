using System;
using Cemi.NotifichePreliminari.Types.Enums;
using Cemi.NotifichePreliminari.Types.Helpers;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    [Serializable]
    public class Persona
    {
        // private string fax;
        private int? idPersona;
        // private string nominativo;
        // private string ragioneSociale;
        //private string telefono;
        //private String _ruoloDescizione;

        //public Persona(int? idPersona, string nominativo, string ragioneSociale, string indirizzo,
        //               string telefono, string fax)
        //{
        //    this.idPersona = idPersona;
        //    this.nominativo = nominativo;
        //    this.ragioneSociale = ragioneSociale;
        //    this.indirizzo = indirizzo;
        //    this.telefono = telefono;
        //    this.fax = fax;
        //}

        public int? IdPersona
        {
            get => idPersona;
            set => idPersona = value;
        }

        public string Nominativo => string.Format("{0} {1}", Nome, Cognome);

        //public string RagioneSociale
        //{
        //    get { return ragioneSociale; }
        //    set { ragioneSociale = value; }
        //}

        public string IndirizzoCompleto => Indirizzo != null ? HelperMethods.FormattaIndirizzo(Indirizzo) : null;

        //public string Telefono
        //{
        //    get { return telefono; }
        //    set { telefono = value; }
        //}

        //public string Fax
        //{
        //    get { return fax; }
        //    set { fax = value; }
        //}

        public string Nome { set; get; }

        public string Cognome { set; get; }

        public string CodiceFiscale { set; get; }

        public DateTime? DataNascita { set; get; }

        public TBridge.Cemi.Type.Entities.Indirizzo Indirizzo { set; get; }

        public RuoloPersona Ruolo { set; get; }

        public IdRuoliPersone? RuoloId => Ruolo != null ? Ruolo.Id : (IdRuoliPersone?) null;

        public string RuoloDescrizione => Ruolo != null ? Ruolo.Descrizione : null;
    }
}