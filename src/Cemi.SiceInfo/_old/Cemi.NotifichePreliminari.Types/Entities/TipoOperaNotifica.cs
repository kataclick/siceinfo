﻿namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class TipoOperaNotifica
    {
        public int Id { set; get; }

        public string Descrizione { set; get; }
    }
}