﻿using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class RuoloPersona
    {
        public RuoloPersona(string descrizione)
        {
            IdRuoliPersone ruoloId;
            Descrizione = descrizione.Trim();

            switch (Descrizione)
            {
                case "COMMITTENTE":
                    ruoloId = IdRuoliPersone.Committente;
                    break;
                case "COORDINATORE REALIZZAZIONE":
                    ruoloId = IdRuoliPersone.CoordinatoreRealizzazione;
                    break;
                case "RESPONSABILE LAVORI":
                    ruoloId = IdRuoliPersone.ResponsabileLavori;
                    break;
                case "COORDINATORE PROGETTAZIONE":
                    ruoloId = IdRuoliPersone.CoordinatoreProgettazione;
                    break;
                default:
                    ruoloId = IdRuoliPersone.NonDefinito;
                    Descrizione = "NON DEFINITO";
                    break;
            }

            Id = ruoloId;
        }

        public IdRuoliPersone Id { get; }
        public string Descrizione { get; }

        public string IdRuoloEnumToString(IdRuoliPersone id)
        {
            string retval;
            switch (id)
            {
                //case IdRuoliPersone.NonDefinito:
                //    break;
                case IdRuoliPersone.Committente:
                    retval = "COMMITTENTE";
                    break;
                case IdRuoliPersone.CoordinatoreRealizzazione:
                    retval = "COORDINATORE REALIZZAZIONE";
                    break;
                case IdRuoliPersone.CoordinatoreProgettazione:
                    retval = "COORDINATORE PROGETTAZIONE";
                    break;
                case IdRuoliPersone.ResponsabileLavori:
                    retval = "RESPONSABILE LAVORI";
                    break;
                default:
                    retval = "NON DEFINITO";
                    break;
            }

            return retval;
        }
    }
}