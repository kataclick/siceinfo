﻿using System;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    public class Cantiere
    {
        public string NumeroNotifica { set; get; }

        public DateTime DataComunicazioneNotifica { set; get; }

        //public String Indirizzo { get; set; }

        //public String Comune { get; set; }

        public TBridge.Cemi.Type.Entities.Geocode.Indirizzo Indirizzo { set; get; }

        public string Comune => Indirizzo != null ? Indirizzo.Comune : null;

        public string IndirizzoBase => Indirizzo != null ? Indirizzo.IndirizzoBase : null;

        public string Provincia => Indirizzo != null ? Indirizzo.Provincia : null;

        public string TipoOperaDescrizione { set; get; }

        public string TipoCategoriaDescrizione { set; get; }

        public string TipoTipologiaDescrizione { set; get; }

        public string AltraCategoriaDescrizione { set; get; }

        public string AltraTipologiaDescrizione { set; get; }

        public string Categoria => TipoCategoriaDescrizione.ToUpper() == "ALTRO..."
            ? AltraCategoriaDescrizione.ToUpper()
            : TipoCategoriaDescrizione.ToUpper();

        public string Tipologia => TipoTipologiaDescrizione.ToUpper() == "ALTRO..."
            ? AltraTipologiaDescrizione.ToUpper()
            : TipoTipologiaDescrizione.ToUpper();

        public decimal AmmontareComplessivo { set; get; }

        public DateTime? DataInizio { get; set; }

        public int Durata { get; set; }

        public string DescrizioneDurata { get; set; }

        public string DurataCompleta => string.Format("{0} {1}", Durata, DescrizioneDurata).Trim();

        public string CommittenteCognome { get; set; }

        public string CommittenteNome { get; set; }

        public string CommittenteCodiceFiscale { get; set; }

        public string Committente => string.Format("{0} {1}", CommittenteCognome, CommittenteNome).Trim();

        public string ImpresaRagioneSociale { get; set; }

        public string ImpresaCodiceFiscale { get; set; }

        public string ImpresaProvincia { get; set; }

        public string ImpresaCap { get; set; }

        public int? NumeroImprese { get; set; }

        public int? NumeroLavoratori { get; set; }
    }
}