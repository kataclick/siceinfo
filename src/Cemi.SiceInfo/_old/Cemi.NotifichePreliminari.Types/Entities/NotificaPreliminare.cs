using System;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Enums;

namespace Cemi.NotifichePreliminari.Types.Entities
{
    [Serializable]
    public class NotificaPreliminare
    {
        public NotificaPreliminare()
        {
            Indirizzi = new IndirizzoCollection();

            Committenti = new PersonaCollection();
            AltrePersone = new PersonaCollection();
            Imprese = new ImpresaNotifichePreliminariCollection();
            //CoordinatoriProgettazione = new PersonaCollection();
            //ResponsabiliLavori = new PersonaCollection();
            //ImpreseAffidatarie = new ImpresaNotifichePreliminariCollection();
            //ImpreseEsecutrici = new ImpresaNotifichePreliminariCollection();
            //ImpreseSenzaRelazione = new ImpresaNotifichePreliminariCollection();
        }


        public int IdNotificaCantiere { set; get; }

        public string NumeroNotifica { set; get; }

        public string ProtocolloNotifica { set; get; }

        public DateTime DataComunicazioneNotifica { set; get; }

        public DateTime? DataAggiornamentoNotifica { get; set; }

        public int? NumeroLavoratoriAutonomi { set; get; }

        public int? NumeroImprese { set; get; }

        public string TipoContrattoId { set; get; }

        public string TipoContrattoDescrizione { set; get; }

        public string ContrattoId { set; get; }

        public IdTipoOperaEnum TipoOperaId { set; get; }

        public string TipoOperaDescrizione { set; get; }

        public string TipoCategoriaDescrizione { set; get; }

        public string TipoTipologiaDescrizione { set; get; }

        public string AltraCategoriaDescrizione { set; get; }

        public string AltraTipologiaDescrizione { set; get; }

        public decimal AmmontareComplessivo { set; get; }

        public bool Articolo9011 { set; get; }

        public bool Articolo9011B { set; get; }

        public bool Articolo9011C { set; get; }

        public int? IdCantiereRegione { set; get; }


        public IndirizzoCollection Indirizzi { set; get; }

        public PersonaCollection Committenti { set; get; }

        public PersonaCollection AltrePersone { set; get; }

        public ImpresaNotifichePreliminariCollection Imprese { set; get; }

        public string TipoCategoriaDescrizioneVisualizzazione => string.IsNullOrEmpty(AltraCategoriaDescrizione)
            ? TipoCategoriaDescrizione.ToUpper()
            : AltraCategoriaDescrizione.ToUpper();

        public string TipoTipologiaDescrizioneVisualizzazione => string.IsNullOrEmpty(AltraTipologiaDescrizione)
            ? TipoTipologiaDescrizione.ToUpper()
            : AltraTipologiaDescrizione.ToUpper();


        //public PersonaCollection CoordinatoriRealizazione { set; get; }

        //public PersonaCollection CoordinatoriProgettazione { set; get; }

        //public PersonaCollection ResponsabiliLavori { set; get; }

        //public ImpresaNotifichePreliminariCollection ImpreseEsecutrici { set; get; }

        //public ImpresaNotifichePreliminariCollection ImpreseAffidatarie { set; get; }

        //public ImpresaNotifichePreliminariCollection ImpreseSenzaRelazione { set; get; }


        //public void SetTipoOperaId(Int32 id)
        //{
        //    TipoOperaId = (IdTipoOperaEnum)id;
        //}

        public string Categoria => TipoCategoriaDescrizione.ToUpper() == "ALTRO..."
            ? AltraCategoriaDescrizione.ToUpper()
            : TipoCategoriaDescrizione.ToUpper();

        public string Tipologia => TipoTipologiaDescrizione.ToUpper() == "ALTRO..."
            ? AltraTipologiaDescrizione.ToUpper()
            : TipoTipologiaDescrizione.ToUpper();
    }
}