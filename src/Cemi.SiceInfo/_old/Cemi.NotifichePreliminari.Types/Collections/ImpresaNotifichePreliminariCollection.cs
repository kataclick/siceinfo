using System;
using System.Collections.Generic;
using System.Linq;
using Cemi.NotifichePreliminari.Types.Entities;

namespace Cemi.NotifichePreliminari.Types.Collections
{
    [Serializable]
    public class ImpresaNotifichePreliminariCollection : List<ImpresaNotifichePreliminari>
    {
        public bool ContainsId(int id)
        {
            return this.FirstOrDefault(x => x.IdImpresa == id) != null;
        }
    }
}