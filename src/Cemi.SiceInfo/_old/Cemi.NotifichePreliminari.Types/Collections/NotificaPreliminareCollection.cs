using System;
using System.Collections.Generic;
using Cemi.NotifichePreliminari.Types.Entities;

namespace Cemi.NotifichePreliminari.Types.Collections
{
    [Serializable]
    public class NotificaPreliminareCollection : List<NotificaPreliminare>
    {
    }
}