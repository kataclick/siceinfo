﻿using System.Collections.Generic;
using System.Linq;
using Cemi.NotifichePreliminari.Types.Entities;

namespace Cemi.NotifichePreliminari.Types.Collections
{
    public class PersonaCollection : List<Persona>
    {
        public bool ContainsId(int id)
        {
            return this.FirstOrDefault(x => x.IdPersona == id) != null;
        }
    }
}