using System;
using System.Collections.Generic;
using System.Linq;
using Cemi.NotifichePreliminari.Types.Entities;

namespace Cemi.NotifichePreliminari.Types.Collections
{
    /// <summary>
    ///     Collezione di indirizzi
    /// </summary>
    [Serializable]
    public class IndirizzoCollection : List<Indirizzo>
    {
        public void AddUnico(Indirizzo indirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == indirizzo.IdIndirizzo)
                    return;
            }

            Add(indirizzo);
        }

        public Indirizzo GetById(int idIndirizzo)
        {
            foreach (Indirizzo ind in this)
            {
                if (ind.IdIndirizzo == idIndirizzo)
                    return ind;
            }

            return null;
        }

        public bool ContainsId(int id)
        {
            return this.FirstOrDefault(x => x.IdIndirizzo == id) != null;
        }
    }
}