﻿using TBridge.Cemi.Type.Entities;

namespace Cemi.NotifichePreliminari.Types.Helpers
{
    public class HelperMethods
    {
        //public static IdRuoliPersone RuoloDescrizioneToEnumId(String descrizione)
        //{

        //    IdRuoliPersone ruoloId;


        //    switch (descrizione.Trim())
        //    {
        //        case "COMMITTENTE":
        //            ruoloId = IdRuoliPersone.Committente;
        //            break;
        //        case "COORDINATORE REALIZZAZIONE":
        //            ruoloId = IdRuoliPersone.CoordinatoreRealizzazione;
        //            break;
        //        case "RESPONSABILE LAVORI":
        //            ruoloId = IdRuoliPersone.ResponsabileLavori;
        //            break;
        //        case "COORDINATORE PROGETTAZIONE":
        //            ruoloId = IdRuoliPersone.CoordinatoreProgettazione;
        //            break;
        //        default:
        //            ruoloId = IdRuoliPersone.NonDefinito;
        //           // _ruoloDescizione = "SCONOSCIUTO";
        //            break;
        //    }


        //    return ruoloId;
        //}

        public static string FormattaIndirizzo(Indirizzo indirizzo)
        {
            if (indirizzo == null)
            {
                return null;
            }

            return string.Format("{0}<br />{1}{2}{3}", indirizzo.NomeVia,
                string.IsNullOrEmpty(indirizzo.Cap) ? null : string.Concat(indirizzo.Cap, " "),
                indirizzo.Comune,
                string.IsNullOrEmpty(indirizzo.Provincia) ? null : string.Format(" ({0})", indirizzo.Provincia));
        }
    }
}