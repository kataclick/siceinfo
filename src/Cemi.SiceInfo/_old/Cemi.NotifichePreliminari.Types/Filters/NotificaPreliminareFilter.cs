using System;

namespace Cemi.NotifichePreliminari.Types.Filters
{
    [Serializable]
    public class NotificaPreliminareFilter
    {
        private string _cantiereCap;
        private string _CantiereComune;
        private string _cantiereIndirizzo;
        private string _cantiereProvncia;

        private string _committenteCodiceFiscale;
        private string _committenteCognome;
        private string _committenteNome;
        private string _impresaCap;
        private string _impresaCfPIva;
        private string _impresaProvincia;
        private string _impresaRagioneSociale;
        private string _naturaOpera;
        private string _numeroNotifica;


        public string NumeroNotifica
        {
            set => _numeroNotifica = string.IsNullOrEmpty(value) ? null : value;
            get => _numeroNotifica;
        }

        public string CommittenteCodiceFiscale
        {
            set => _committenteCodiceFiscale = string.IsNullOrEmpty(value) ? null : value;
            get => _committenteCodiceFiscale;
        }

        public string CommittenteCognome
        {
            set => _committenteCognome = string.IsNullOrEmpty(value) ? null : value;
            get => _committenteCognome;
        }

        public string CommittenteNome
        {
            set => _committenteNome = string.IsNullOrEmpty(value) ? null : value;
            get => _committenteNome;
        }

        public string NaturaOpera
        {
            set => _naturaOpera = string.IsNullOrEmpty(value) ? null : value.Trim();
            get => _naturaOpera;
        }

        public string CantiereIndirizzo
        {
            set => _cantiereIndirizzo = string.IsNullOrEmpty(value) ? null : value;
            get => _cantiereIndirizzo;
        }

        public string CantiereComune
        {
            set => _CantiereComune = string.IsNullOrEmpty(value) ? null : value;
            get => _CantiereComune;
        }

        public string CantiereProvncia
        {
            set => _cantiereProvncia = string.IsNullOrEmpty(value) ? null : value;
            get => _cantiereProvncia;
        }

        public string CantiereCap
        {
            set => _cantiereCap = string.IsNullOrEmpty(value) ? null : value;
            get => _cantiereCap;
        }

        public bool? CantiereIndirizzoGeolocalizzato { set; get; }

        public DateTime? DataComunicazioneDal { set; get; }
        public DateTime? DataComunicazioneAl { set; get; }
        public DateTime? DataInizioLavoriDal { set; get; }
        public DateTime? DataInizioLavoriAl { set; get; }
        public decimal? Ammontare { set; get; }

        public string ImpresaRagioneSociale
        {
            set => _impresaRagioneSociale = string.IsNullOrEmpty(value) ? null : value;
            get => _impresaRagioneSociale;
        }

        public string ImpresaCfPIva
        {
            set => _impresaCfPIva = string.IsNullOrEmpty(value) ? null : value;
            get => _impresaCfPIva;
        }

        public string ImpresaProvincia
        {
            set => _impresaProvincia = string.IsNullOrEmpty(value) ? null : value;
            get => _impresaProvincia;
        }

        public string ImpresaCap
        {
            set => _impresaCap = string.IsNullOrEmpty(value) ? null : value;
            get => _impresaCap;
        }
    }
}