using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using System.Web.UI.WebControls;
using TBridge.Cemi.Data;
using TBridge.Cemi.Data.GestioneUtenti;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.GestioneUtenti;
using Common = TBridge.Cemi.Business.Common;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public class GestioneUtentiBiz
    {
        private static GestioneUtentiBiz _gestioneUtentiBiz;

        private readonly Common _commonBiz = new Common();

        private readonly GestioneUtentiAccess _gestioneUtentiDataAccess;

        public GestioneUtentiBiz()
        {
            _gestioneUtentiDataAccess = new GestioneUtentiAccess();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected static GestioneUtentiBiz GetGestioneUtenti()
        {
            if (_gestioneUtentiBiz == null)
                _gestioneUtentiBiz = new GestioneUtentiBiz();

            return _gestioneUtentiBiz;
        }

        public LavoratoriConImpresaCollection GetLavoratoriConImpresa(FilterLavoratore filter)
        {
            return _gestioneUtentiDataAccess.GetLavoratoriConImpresa(filter);
        }

        public LavoratoriCollection GetLavoratoriConTel(FilterLavoratore filter)
        {
            return _gestioneUtentiDataAccess.GetLavoratoriConTel(filter);
        }

        public LavoratoriCollection GetLavoratoriConTelPrest(FilterLavoratorePrest filter)
        {
            return _gestioneUtentiDataAccess.GetLavoratoriConTelPrest(filter);
        }

        public bool RichiediAggiornamentoIndirizzoLavoratore(int idLavoratore, string indirizzoDenominazione,
            string indirizzoCAP, string indirizzoProvincia,
            string indirizzoComune)
        {
            return
                _gestioneUtentiDataAccess.RichiediAggiornamentoIndirizzoLavoratore(idLavoratore, indirizzoDenominazione,
                    indirizzoCAP, indirizzoProvincia,
                    indirizzoComune);
        }

        public DataSet CaricaCodiciDismessi()
        {
            return _gestioneUtentiDataAccess.CaricaCodiciDismessi();
        }

        public DataSet CaricaOperazioni(int codDismesso, int codValido)
        {
            return _gestioneUtentiDataAccess.CaricaOperazioni(codDismesso, codValido);
        }

        public ConsulentiCollection GetConsulenti(string ragioneSociale, string codiceFiscale, bool? conPIN,
            DateTime? dal, DateTime? al, int? idConsulente)
        {
            ConsulentiCollection consulenti = null;

            try
            {
                consulenti =
                    _gestioneUtentiDataAccess.GetConsulenti(ragioneSociale, codiceFiscale, conPIN, dal, al,
                        idConsulente);
            }
            catch (Exception exc)
            {
            }

            return consulenti;
        }

        public List<ConsulenteConCredenziali> GetConsulentiConCredenziali(ConsulenteFilter filter)
        {
            return _gestioneUtentiDataAccess.GetConsulentiConCredenziali(filter);
        }

        public LavoratoriConImpresaSmsCollection GetLavoratoriConImpresaSms(FilterLavoratore filter)
        {
            return _gestioneUtentiDataAccess.GetLavoratoriConImpresaSms(filter);
        }

        public List<ImpresaConCredenziali> GetImprese(ImpresaFilter filtro)
        {
            return _gestioneUtentiDataAccess.GetImprese(filtro);
        }

        public List<LavoratoreConImpresaSmsCredenziali> GetLavoratoriConImpresaSmsCredenziali(FilterLavoratore filter)
        {
            return _gestioneUtentiDataAccess.GetLavoratoriConImpresaSmsCredenziali(filter);
        }

        public void CaricaProvinceInDropDown(DropDownList dropDownProvince)
        {
            DataTable dtProvince = _commonBiz.GetProvince();

            dropDownProvince.Items.Clear();
            dropDownProvince.Items.Add(new ListItem(string.Empty, null));

            dropDownProvince.DataSource = dtProvince;
            dropDownProvince.DataTextField = "sigla";
            dropDownProvince.DataValueField = "idProvincia";

            dropDownProvince.DataBind();
        }

        public void CaricaComuniInDropDown(DropDownList dropDownComuni, int idProvincia)
        {
            dropDownComuni.Items.Clear();
            dropDownComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idProvincia > 0)
            {
                DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

                dropDownComuni.DataSource = dtComuni;
                dropDownComuni.DataTextField = "denominazione";
                dropDownComuni.DataValueField = "idComune";
            }

            dropDownComuni.DataBind();
        }

        public void CaricaCapInDropDown(DropDownList dropDownCap, long idComune)
        {
            dropDownCap.Items.Clear();
            dropDownCap.Items.Add(new ListItem(string.Empty, string.Empty));

            if (idComune > 0)
            {
                DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

                dropDownCap.DataSource = dtCAP;
                dropDownCap.DataTextField = "cap";
                dropDownCap.DataValueField = "cap";
            }

            dropDownCap.DataBind();
        }

        public void CambioSelezioneDropDownProvincia(DropDownList ddlProvincia, DropDownList ddlComune,
            DropDownList ddlCap)
        {
            int idProvincia = -1;

            if (ddlProvincia.SelectedValue != null)
                int.TryParse(ddlProvincia.SelectedValue, out idProvincia);

            CaricaComuniInDropDown(ddlComune, idProvincia);

            CaricaCapInDropDown(ddlCap, -1);
        }

        public void CambioSelezioneDropDownComune(DropDownList ddlComune, DropDownList ddlCap)
        {
            long idComune = -1;

            if (ddlComune.SelectedValue != null)
                long.TryParse(ddlComune.SelectedValue, out idComune);

            CaricaCapInDropDown(ddlCap, idComune);
        }

        public LavoratoreCredential GetLavoratoreCredential(int idLavoratore)
        {
            LavoratoreCredential credential = new LavoratoreCredential {IdLavoratore = idLavoratore};

            FilterLavoratore filter = new FilterLavoratore {IdLavoratore = idLavoratore};
            List<LavoratoreConImpresaSmsCredenziali> lavoratori = GetLavoratoriConImpresaSmsCredenziali(filter);

            if (lavoratori.Count > 0)
            {
                credential.Username = lavoratori[0].Username;
                credential.Password = lavoratori[0].Password;
            }

            return credential;
        }

        public bool ResetPassword(string login, string pin, string nuovaPassword, out int? idUtente)
        {
            return _gestioneUtentiDataAccess.ResetPassword(login, pin, nuovaPassword, out idUtente);
        }

        public static bool ControllaFormatoPassword(string password)
        {
            return !string.IsNullOrEmpty(password);
        }

        public DateTime GetScadenzaPasswordFromUsername(string username)
        {
            return _gestioneUtentiDataAccess.GetScadenzaPasswordFromUsername(username);
        }

        public void AggiornaScadenzaPassword(int idUtente, int numeroMesi)
        {
            _gestioneUtentiDataAccess.AggiornaScadenzaPassword(idUtente, numeroMesi);
        }

        public static string GetUserNamePerEdilConnect(int tipoUtente, int codice, string guid, string partitaIva)
        {
            string userName = null;

            switch (tipoUtente)
            {
                case 1:
                    // Consulente
                    using (SICEEntities context = new SICEEntities())
                    {
                        var queryConsulenti = from consulente in context.Consulenti
                            where consulente.Id == codice
                                  && consulente.CodiceFiscale == partitaIva
                                  && consulente.GuidEdilconnect == new Guid(guid)
                                  && !string.IsNullOrEmpty(consulente.Utenti.Password)
                                  && (!consulente.Utenti.DataScadenzaPassword.HasValue ||
                                      consulente.Utenti.DataScadenzaPassword > DateTime.Now)
                            select consulente;

                        try
                        {
                            Type.Domain.Consulente consulente = queryConsulenti.Single();
                            if (consulente != null)
                            {
                                userName = consulente.Utenti.Username;
                            }
                        }
                        catch
                        {
                        }
                    }
                    break;

                case 2:
                    // Impresa
                    using (SICEEntities context = new SICEEntities())
                    {
                        var queryImprese = from impresa in context.Imprese
                            where impresa.Id == codice
                                  && impresa.PartitaIVA == partitaIva
                                  && impresa.GuidEdilconnect == new Guid(guid)
                                  && !string.IsNullOrEmpty(impresa.Utente.Password)
                                  && (!impresa.Utente.DataScadenzaPassword.HasValue ||
                                      impresa.Utente.DataScadenzaPassword > DateTime.Now)
                            select impresa;

                        try
                        {
                            Type.Domain.Impresa impresa = queryImprese.Single();
                            if (impresa != null)
                            {
                                userName = impresa.Utente.Username;
                            }
                        }
                        catch
                        {
                        }
                    }
                    break;
            }

            return userName;
        }

        #region Metodi Statici

        /// <summary>
        ///     Recupera il nome dell'utente loggato
        /// </summary>
        /// <returns></returns>
        public static string GetNomeUtente()
        {
            try
            {
                Utente utente = (Utente) Membership.GetUser();
                return utente.UserName;
            }
            catch
            {
                throw new Exception("GetNomeUtente: impossibile recuperare la login dell'utente");
            }
        }

        /// <summary>
        ///     Otteniamo l'id dell'utente loggato
        /// </summary>
        /// <returns></returns>
        public static int GetIdUtente()
        {
            try
            {
                Utente utente = (Utente) Membership.GetUser();
                return utente.IdUtente;
            }
            catch
            {
                return -1;
            }
        }

        /// <summary>
        ///     Recupera l'identit� dell'utente corrente
        /// </summary>
        /// <returns></returns>
        public static Utente GetIdentitaUtenteCorrente()
        {
            Utente utente = (Utente) Membership.GetUser();

            if (utente != null)
            {
                return GetIdentitaUtente(utente.IdUtente);
            }

            return null;
        }

        /// <summary>
        ///     Recupera l'identit� dell'utente passato come parametro
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public static Utente GetIdentitaUtente(int idUtente)
        {
            return GetGestioneUtenti().GetUtente(idUtente);
        }

        public Utente GetUtente(int idUtente)
        {
            return _gestioneUtentiDataAccess.GetUtente(idUtente);
        }

        /// <summary>
        ///     Verifica se l'utente loggato � autorizzato alla funzionalit� passata come parametro, nel caso di nessun utente
        ///     loggato si considerano valide le funzionalit� dell'Utente pubblico
        /// </summary>
        /// <param name="funzionalita"></param>
        /// <returns></returns>
        [Obsolete("Utilizzare il metoto che prende in input enum")]
        public static bool Autorizzato(string funzionalita)
        {
            bool autorizzato = false;

            try
            {
                MembershipUser user = Membership.GetUser();

                if (user != null)
                {
                    autorizzato = Roles.IsUserInRole(user.UserName, funzionalita);
                }
                else
                {
                    // Se l'utente corrente non � riconosciuto autorizzo le funzionalit� dell'utente pubblico

                    // Utente pubblico idRuolo = 8
                    FunzionalitaCollection funzionalitaUtentePubblico = new GestioneUtentiBiz().GetFunzionalitaRuolo(8);
                    if (funzionalitaUtentePubblico.Any(funz => funz.Nome == funzionalita))
                    {
                        autorizzato = true;
                    }
                }
            }
            catch
            {
            }

            return autorizzato;
        }

        /// <summary>
        ///     Verifica se l'utente loggato � autorizzato alla funzionalit� passata come parametro, nel caso di nessun utente
        ///     loggato si considerano valide le funzionalit� dell'Utente pubblico
        /// </summary>
        /// <param name="funzionalita"></param>
        /// <returns></returns>
        public static bool Autorizzato(FunzionalitaPredefinite funzionalita)
        {
            return Autorizzato(funzionalita.ToString());
        }

        /// <summary>
        ///     Ritorna true se l'utente � un'impresa
        /// </summary>
        /// <returns></returns>
        public static bool IsImpresa()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Impresa))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Ritorna true se l'utente � un'impresa
        /// </summary>
        /// <returns></returns>
        public static bool IsConsulente()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Consulente))
            {
                return true;
            }

            return false;
        }

        public static bool IsLavoratore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Lavoratore))
            {
                return true;
            }

            return false;
        }

        public static bool IsFornitore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Fornitore))
            {
                return true;
            }

            return false;
        }

        public static bool IsIspettore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Ispettore))
            {
                return true;
            }

            return false;
        }

        public static bool IsSindacalista()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Sindacalista))
            {
                return true;
            }

            return false;
        }

        public static bool IsCommittente()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Committente))
            {
                return true;
            }

            return false;
        }

        public static bool IsASL()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(ASL))
            {
                return true;
            }

            return false;
        }

        public static bool IsCassaEdile()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(CassaEdile))
            {
                return true;
            }

            return false;
        }

        public static bool IsEsattore()
        {
            Utente utente = GetIdentitaUtenteCorrente();

            if (utente != null && utente.GetType() == typeof(Esattore))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///     Assegna il ruolo LavoratoreSMS all'utente del sistema individuato
        /// </summary>
        /// <param name="idUtente"></param>
        public static bool AssegnaRuoloLavoratoreSMS(int idUtente)
        {
            bool res = false;

            try
            {
                if (GetGestioneUtenti()._gestioneUtentiDataAccess.EsisteUtente(idUtente))
                {
                    int idRuolo =
                        GetGestioneUtenti()._gestioneUtentiDataAccess.GetIdRuolo(
                            RuoliPredefiniti.LavoratoreSMS.ToString());

                    GetGestioneUtenti()._gestioneUtentiDataAccess.AssociaUtenteRuolo(idUtente, idRuolo);

                    res = true;
                }
            }
            catch (Exception ex)
            {
                //
            }

            return res;
        }

        /// <summary>
        ///     Toglie il ruolo LavoratoreSMS all'utente del sistema individuato
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public static bool DisassociaRuoloLavoratoreSMS(int idUtente)
        {
            bool res = false;
            try
            {
                if (GetGestioneUtenti()._gestioneUtentiDataAccess.EsisteUtente(idUtente))
                {
                    int idRuolo =
                        GetGestioneUtenti()._gestioneUtentiDataAccess.GetIdRuolo(
                            RuoliPredefiniti.LavoratoreSMS.ToString());

                    GetGestioneUtenti()._gestioneUtentiDataAccess.DisassociaUtenteRuolo(idUtente, idRuolo);

                    //TODO da correggere quando la ExecuteNonQuery funzioner�, per ora siamo ottimisti...
                    res = true;
                }
            }
            catch (Exception ex)
            {
                //
            }

            return res;
        }

        #endregion

        #region Metodi per la gestione dell'utente/ruoli/funzionalit�

        /// <summary>
        ///     Ritorna tutte le imprese
        /// </summary>
        /// <returns></returns>
        public ImpreseCollection GetImpreseAll()
        {
            ImpreseCollection imprese = null;

            try
            {
                imprese = _gestioneUtentiDataAccess.GetImpreseAll();
                return imprese;
            }
            catch (Exception ex)
            {
                //ACTIVITY TRACKING
                //LogItemCollection logItemCollection = new LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetImpreseAll", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

                return imprese;
            }
        }

        /// <summary>
        ///     Ritorna la lista dei CAP compreso il comune e la provincia presenti nelle imprese del DB
        /// </summary>
        public List<CAP> GetImpreseCAP()
        {
            List<CAP> listaCap = null;

            try
            {
                listaCap = _gestioneUtentiDataAccess.GetImpreseCAP();
                return listaCap;
            }
            catch (Exception ex)
            {
                return listaCap;
            }
        }

        public List<Utente> GetUtenti()
        {
            List<Utente> utenti;

            try
            {
                utenti = _gestioneUtentiDataAccess.GetUtenti();
            }
            catch (Exception ex)
            {
                //Inizializzo la struttua di modo che non debba essere gestito il caso null
                utenti = new List<Utente>();
            }

            return utenti;
        }

        public Utente GetUtente(string username)
        {
            return _gestioneUtentiDataAccess.GetUtente(username);
        }

        /// <summary>
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="nuovaPassword"></param>
        /// <returns></returns>
        public bool CambiaPassword(string login, string password, string nuovaPassword)
        {
            try
            {
                Utente utente = _gestioneUtentiDataAccess.GetUtente(login, password);

                if (utente != null)
                {
                    utente.Password = nuovaPassword;

                    _gestioneUtentiDataAccess.AggiornaUtente(utente);

                    return true;
                }


                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public LavoratoriCollection GetUtentiLavoratori()
        {
            return _gestioneUtentiDataAccess.GetUtentiLavoratori();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ImpreseCollection GetUtentiImprese()
        {
            return _gestioneUtentiDataAccess.GetUtentiImprese();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ConsulentiCollection GetUtentiConsulenti()
        {
            return _gestioneUtentiDataAccess.GetUtentiConsulenti();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public SindacalistiCollection GetUtentiSindacalisti()
        {
            return _gestioneUtentiDataAccess.GetUtentiSindacalisti();
        }

        public List<ASL> GetUtentiASL()
        {
            return _gestioneUtentiDataAccess.GetUtentiASL();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public CommittenteCollection GetUtentiCommittenti()
        {
            return _gestioneUtentiDataAccess.GetUtentiCommittenti();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public DipendentiCollection GetUtentiDipendenti()
        {
            return _gestioneUtentiDataAccess.GetUtentiDipendenti();
        }

        public IspettoriCollection GetUtentiIspettori()
        {
            return _gestioneUtentiDataAccess.GetUtentiIspettori();
        }

        public EsattoriCollection GetUtentiEsattori()
        {
            return _gestioneUtentiDataAccess.GetUtentiEsattori();
        }

        /// <summary>
        ///     Ritorna l'elenco dei fornitori utenti
        /// </summary>
        /// <returns></returns>
        public FornitoriCollection GetUtentiFornitori()
        {
            return _gestioneUtentiDataAccess.GetUtentiFornitori();
        }

        /// <summary>
        ///     Ritorna l'elenco degli ospiti utenti
        /// </summary>
        /// <returns></returns>
        public OspitiCollection GetUtentiOspiti()
        {
            return _gestioneUtentiDataAccess.GetUtentiOspiti();
        }

        /// <summary>
        ///     Ritorna l'elenco degli utenti Cassa Edile
        /// </summary>
        /// <returns></returns>
        public List<CassaEdile> GetUtentiCasseEdili()
        {
            return _gestioneUtentiDataAccess.GetUtentiCasseEdili();
        }

        /// <summary>
        ///     Permette di aggiornare il ruolo e contemporaneamente le funzionalit� associate al ruolo
        /// </summary>
        /// <param name="ruolo"></param>
        /// <param name="funzionalita"></param>
        public bool AggiornaFunzionalitaRuolo(Ruolo ruolo, FunzionalitaCollection funzionalita)
        {
            try
            {
                //Aggiorniamo il ruolo
                _gestioneUtentiDataAccess.AggiornaRuolo(ruolo);

                //Aggiorniamo le funzionalit� associate al ruolo
                int res = _gestioneUtentiDataAccess.AssociaFuzionalitaRuolo(ruolo.IdRuolo, funzionalita);
                if (res > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="idUtente"></param>
        /// <param name="ruoli"></param>
        public void AssegnaRuoliUtente(int idUtente, RuoliCollection ruoli)
        {
            _gestioneUtentiDataAccess.AssociaUtenteRuoli(idUtente, ruoli);
        }

        /// <summary>
        /// </summary>
        /// <param name="utente"></param>
        /// <param name="ruoli"></param>
        public void AssegnaRuoliUtente(Utente utente, RuoliCollection ruoli)
        {
            _gestioneUtentiDataAccess.AssociaUtenteRuoli(utente.IdUtente, ruoli);
        }

        /// <summary>
        /// </summary>
        /// <param name="utente"></param>
        /// <param name="ruolo"></param>
        public void AssociaRuoloUtente(Utente utente, Ruolo ruolo)
        {
            _gestioneUtentiDataAccess.AssociaUtenteRuolo(utente.IdUtente, ruolo.IdRuolo);
        }

        /// <summary>
        /// </summary>
        /// <param name="utente"></param>
        /// <param name="ruolo"></param>
        public void DisassociaUtenteRuolo(Utente utente, Ruolo ruolo)
        {
            _gestioneUtentiDataAccess.DisassociaUtenteRuolo(utente.IdUtente, ruolo.IdRuolo);
        }

        #region Abilita disabilita utente

        /// <summary>
        /// </summary>
        /// <param name="utente"></param>
        /// <returns></returns>
        public bool DisabilitaUtente(Utente utente)
        {
            try
            {
                //Assegnamo il ruolo di utente disabilitato
                Ruolo ruoloUtenteDisabilitato =
                    _gestioneUtentiDataAccess.GetRuolo(RuoliPredefiniti.UtenteDisabilitato.ToString());

                if (ruoloUtenteDisabilitato != null)
                {
                    _gestioneUtentiDataAccess.AssociaUtenteRuolo(utente.IdUtente, ruoloUtenteDisabilitato.IdRuolo);
                    return true;
                }

                //Se non siamo usciti dal metodo con true usciamo con false per segnalare l'errore
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="utente"></param>
        /// <returns></returns>
        public bool AbilitaUtente(Utente utente)
        {
            try
            {
                //Disassociamo il ruolo di utente disabilitato
                Ruolo ruoloUtenteDisabilitato =
                    _gestioneUtentiDataAccess.GetRuolo(RuoliPredefiniti.UtenteDisabilitato.ToString());
                if (ruoloUtenteDisabilitato != null)
                {
                    int res =
                        _gestioneUtentiDataAccess.DisassociaUtenteRuolo(utente.IdUtente,
                            ruoloUtenteDisabilitato.IdRuolo);

                    if (res > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region Metodi per la registrazione degli utenti al sistema

        /// <summary>
        ///     Metodo per assegnare username e password ad utenti gi� presenti nel sistema
        /// </summary>
        /// <param name="impresa"></param>
        /// <returns></returns>
        public ErroriRegistrazione RegistraImpresa(Impresa impresa)
        {
            try
            {
                int idUtente = ChallengeImpresaPIN(impresa.IdImpresa, impresa.PIN);

                impresa.IdUtente = idUtente;

                if (idUtente != -1)
                {
                    if (!_gestioneUtentiDataAccess.EsisteUtenteRegistrato(impresa.IdUtente))
                    {
                        try
                        {
                            Membership.UpdateUser(impresa);
                            _gestioneUtentiDataAccess.AssociaUtenteRuolo(impresa.IdUtente,
                                RuoliPredefiniti.Impresa.ToString());

                            return ErroriRegistrazione.RegistrazioneEffettuata;
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.RegistrazioneGiaPresente;
                }
                return ErroriRegistrazione.ChallengeNonPassato;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(SqlException))
                {
                    if (((SqlException) ex).Number == 2627)
                        return ErroriRegistrazione.LoginPresente;
                }
                return ErroriRegistrazione.Errore;
            }

            #region old

            //try
            //{
            //    //int idUtente = ChallengeImpresa(impresa.RagioneSociale, impresa.PartitaIVA);
            //    int idUtente = ChallengeImpresaPIN(impresa.IdImpresa, impresa.PIN);

            //    impresa.IdUtente = idUtente;

            //    if (idUtente != -1)
            //    {
            //        Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //        Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            //        if (!patternUsername.IsMatch(impresa.UserName)) return ErroriRegistrazione.LoginNonCorretta;
            //        if (!patternPassword.IsMatch(impresa.Password)) return ErroriRegistrazione.PasswordNonCorretta;

            //        if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(impresa.IdUtente))
            //        {
            //            //Impresa ricosciuta, quindi deve essere utente del sistema (c'� un trigger che lo fa)
            //            if (gestioneUtentiDataAccess.RegistraUtente(impresa))
            //            {
            //                //Registriamo il ruolo Impresa di default
            //                gestioneUtentiDataAccess.AssociaUtenteRuolo(impresa.IdUtente,
            //                                                            RuoliPredefiniti.Impresa.ToString());
            //                {
            //                    //ACTIVITY TRACKING
            //                    //LogItemCollection logItemCollection2 = new LogItemCollection();
            //                    //logItemCollection2.Add("Login", impresa.Username);
            //                    //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //                    //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //                    //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //                    //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //                    //logItemCollection2.Add("PIN", impresa.PIN);
            //                    //Log.Write("Impresa registrata", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGGING);

            //                    //return true;
            //                    return ErroriRegistrazione.RegistrazioneEffettuata;
            //                }
            //            }
            //            else
            //                return ErroriRegistrazione.LoginPresente;
            //        }
            //        else
            //        {
            //            //ACTIVITY TRACKING
            //            //LogItemCollection logItemCollection2 = new LogItemCollection();
            //            //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //            //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //            //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //            //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //            //logItemCollection2.Add("PIN", impresa.PIN);
            //            //Log.Write("Utente Impresa gi� presente", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //            //return false;
            //            return ErroriRegistrazione.RegistrazioneGiaPresente;
            //        }
            //    }
            //    else
            //    {
            //        //L'impresa non ha passato la sfida quindi i suoi dati non sono nel db di cassa edile

            //        //ACTIVITY TRACKING
            //        //LogItemCollection logItemCollection2 = new LogItemCollection();
            //        //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //        //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //        //logItemCollection2.Add("PIN", impresa.PIN);
            //        //Log.Write("Impresa challenge non passato", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //        //return false;
            //        return ErroriRegistrazione.ChallengeNonPassato;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ACTIVITY TRACKING
            //    //LogItemCollection logItemCollection = new LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.RegistraImpresa", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

            //    if (ex.GetType() == typeof (SqlException))
            //    {
            //        if (((SqlException) (ex)).Number == 2627)
            //            return ErroriRegistrazione.LoginPresente;
            //    }
            //    //return false;
            //    return ErroriRegistrazione.Errore;
            //}

            #endregion
        }

        public ErroriRegistrazione RegistraConsulente(Consulente consulente)
        {
            try
            {
                int idUtente = ChallengeConsulentePIN(consulente.IdConsulente, consulente.PIN);

                consulente.IdUtente = idUtente;

                if (idUtente != -1)
                {
                    if (!_gestioneUtentiDataAccess.EsisteUtenteRegistrato(consulente.IdUtente))
                    {
                        try
                        {
                            Membership.UpdateUser(consulente);
                            _gestioneUtentiDataAccess.AssociaUtenteRuolo(consulente.IdUtente,
                                RuoliPredefiniti.Consulente.ToString());

                            return ErroriRegistrazione.RegistrazioneEffettuata;
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.RegistrazioneGiaPresente;
                }
                return ErroriRegistrazione.ChallengeNonPassato;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(SqlException))
                {
                    if (((SqlException) ex).Number == 2627)
                        return ErroriRegistrazione.LoginPresente;
                }
                return ErroriRegistrazione.Errore;
            }

            #region old

            //try
            //{
            //    //int idUtente = ChallengeImpresa(impresa.RagioneSociale, impresa.PartitaIVA);
            //    int idUtente = ChallengeConsulentePIN(consulente.IdConsulente, consulente.PIN);

            //    consulente.IdUtente = idUtente;

            //    if (idUtente != -1)
            //    {
            //        Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //        Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            //        if (!patternUsername.IsMatch(consulente.UserName)) return ErroriRegistrazione.LoginNonCorretta;
            //        if (!patternPassword.IsMatch(consulente.Password)) return ErroriRegistrazione.PasswordNonCorretta;

            //        if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(consulente.IdUtente))
            //        {
            //            //Impresa ricosciuta, quindi deve essere utente del sistema (c'� un trigger che lo fa)
            //            if (gestioneUtentiDataAccess.RegistraUtente(consulente))
            //            {
            //                //Registriamo il ruolo Impresa di default
            //                gestioneUtentiDataAccess.AssociaUtenteRuolo(consulente.IdUtente,
            //                                                            RuoliPredefiniti.Consulente.ToString());
            //                {
            //                    //ACTIVITY TRACKING
            //                    //LogItemCollection logItemCollection2 = new LogItemCollection();
            //                    //logItemCollection2.Add("Login", impresa.Username);
            //                    //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //                    //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //                    //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //                    //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //                    //logItemCollection2.Add("PIN", impresa.PIN);
            //                    //Log.Write("Impresa registrata", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGGING);

            //                    //return true;
            //                    return ErroriRegistrazione.RegistrazioneEffettuata;
            //                }
            //            }
            //            else
            //                return ErroriRegistrazione.LoginPresente;
            //        }
            //        else
            //        {
            //            //ACTIVITY TRACKING
            //            //LogItemCollection logItemCollection2 = new LogItemCollection();
            //            //logItemCollection2.Add("IdUtente", impresa.IdUtente.ToString());
            //            //logItemCollection2.Add("IdImpresa", impresa.IdImpresa.ToString());
            //            //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //            //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //            //logItemCollection2.Add("PIN", impresa.PIN);
            //            //Log.Write("Utente Impresa gi� presente", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //            //return false;
            //            return ErroriRegistrazione.RegistrazioneGiaPresente;
            //        }
            //    }
            //    else
            //    {
            //        //L'impresa non ha passato la sfida quindi i suoi dati non sono nel db di cassa edile

            //        //ACTIVITY TRACKING
            //        //LogItemCollection logItemCollection2 = new LogItemCollection();
            //        //logItemCollection2.Add("RagioneSociale", impresa.RagioneSociale);
            //        //logItemCollection2.Add("PartitaIVA", impresa.PartitaIVA);
            //        //logItemCollection2.Add("PIN", impresa.PIN);
            //        //Log.Write("Impresa challenge non passato", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //        //return false;
            //        return ErroriRegistrazione.ChallengeNonPassato;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ACTIVITY TRACKING
            //    //LogItemCollection logItemCollection = new LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.RegistraImpresa", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

            //    if (ex.GetType() == typeof (SqlException))
            //    {
            //        if (((SqlException) (ex)).Number == 2627)
            //            return ErroriRegistrazione.LoginPresente;
            //    }
            //    //return false;
            //    return ErroriRegistrazione.Errore;
            //}

            #endregion
        }

        /// <summary>
        /// </summary>
        /// <param name="lavoratore"></param>
        /// <returns></returns>
        public ErroriRegistrazione RegistraLavoratore(Lavoratore lavoratore)
        {
            try
            {
                int idUtente =
                    ChallengeLavoratore(lavoratore.Nome, lavoratore.Cognome, lavoratore.CodiceFiscale, lavoratore.Pin);

                lavoratore.IdUtente = idUtente;

                if (idUtente != -1)
                {
                    if (!_gestioneUtentiDataAccess.EsisteUtenteRegistrato(lavoratore.IdUtente))
                    {
                        try
                        {
                            Membership.UpdateUser(lavoratore);
                            _gestioneUtentiDataAccess.AssociaUtenteRuolo(lavoratore.IdUtente,
                                RuoliPredefiniti.Lavoratore.ToString());

                            return ErroriRegistrazione.RegistrazioneEffettuata;
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.RegistrazioneGiaPresente;
                }
                return ErroriRegistrazione.ChallengeNonPassato;
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(SqlException))
                {
                    if (((SqlException) ex).Number == 2627)
                        return ErroriRegistrazione.LoginPresente;
                }
                return ErroriRegistrazione.Errore;
            }

            #region old

            //try
            //{
            //    int idUtente =
            //        ChallengeLavoratore(lavoratore.Nome, lavoratore.Cognome, lavoratore.CodiceFiscale, lavoratore.Pin);

            //    lavoratore.IdUtente = idUtente;

            //    if (idUtente != -1)
            //    {
            //        Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //        Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            //        if (!patternUsername.IsMatch(lavoratore.UserName)) return ErroriRegistrazione.LoginNonCorretta;
            //        if (!patternPassword.IsMatch(lavoratore.Password)) return ErroriRegistrazione.PasswordNonCorretta;

            //        if (!gestioneUtentiDataAccess.EsisteUtenteRegistrato(lavoratore.IdUtente))
            //        {
            //            //lavoratore ricosciuta, quindi deve essere utente del sistema (c'� un trigger che lo fa)
            //            if (gestioneUtentiDataAccess.RegistraUtente(lavoratore))
            //            {
            //                //Registriamo il ruolo lavoratore di default
            //                gestioneUtentiDataAccess.AssociaUtenteRuolo(lavoratore.IdUtente,
            //                                                            RuoliPredefiniti.Lavoratore.ToString());

            //                //ACTIVITY TRACKING
            //                //LogItemCollection logItemCollection2 = new LogItemCollection();
            //                //logItemCollection2.Add("Login", lavoratore.Username);
            //                //logItemCollection2.Add("IdUtente", lavoratore.IdUtente.ToString());
            //                //logItemCollection2.Add("IdLavoratore", lavoratore.IdLavoratore.ToString());
            //                //logItemCollection2.Add("Nome", lavoratore.Nome);
            //                //logItemCollection2.Add("Cognome", lavoratore.Cognome);
            //                //logItemCollection2.Add("CodiceFiscale", lavoratore.CodiceFiscale);
            //                //Log.Write("Lavoratore registrata", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGGING);

            //                //return true;
            //                return ErroriRegistrazione.RegistrazioneEffettuata;
            //            }
            //            return ErroriRegistrazione.LoginPresente;
            //        }
            //        else
            //        {
            //            //ACTIVITY TRACKING
            //            //LogItemCollection logItemCollection2 = new LogItemCollection();
            //            //logItemCollection2.Add("Nome", lavoratore.Nome);
            //            //logItemCollection2.Add("Cognome", lavoratore.Cognome);
            //            //logItemCollection2.Add("CodiceFiscale", lavoratore.CodiceFiscale);
            //            //Log.Write("Utente Lavoratore gi� presente", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //            //return false; 
            //            return ErroriRegistrazione.RegistrazioneGiaPresente;
            //        }
            //    }
            //    else
            //    {
            //        //Il lavoratore non ha passato la sfida quindi i suoi dati non sono nel db di cassa edile

            //        //ACTIVITY TRACKING
            //        //LogItemCollection logItemCollection2 = new LogItemCollection();
            //        //logItemCollection2.Add("Nome", lavoratore.Nome);
            //        //logItemCollection2.Add("Cognome", lavoratore.Cognome);
            //        //logItemCollection2.Add("CodiceFiscale", lavoratore.CodiceFiscale);
            //        //Log.Write("Lavoratore challenge non passato", logItemCollection2, Log.categorie.GESTIONEUTENTI, Log.sezione.LOGICEXCEPTION);

            //        //return false;
            //        return ErroriRegistrazione.ChallengeNonPassato;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //ACTIVITY TRACKING
            //    //LogItemCollection logItemCollection = new LogItemCollection();
            //    //logItemCollection.Add("Eccezione", ex.Message);
            //    //Log.Write("Eccezione in: TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.RegistraLavoratore", logItemCollection, Log.categorie.GESTIONEUTENTI, Log.sezione.SYSTEMEXCEPTION);

            //    if (ex.GetType() == typeof (SqlException))
            //    {
            //        if (((SqlException) (ex)).Number == 2627)
            //            return ErroriRegistrazione.LoginPresente;
            //    }
            //    //return false;
            //    return ErroriRegistrazione.Errore;
            //}

            #endregion
        }

        /// <summary>
        ///     Inserimento di un dipendente
        /// </summary>
        /// <param name="dipendente"></param>
        /// <returns></returns>
        public ErroriRegistrazione InserisciDipendente(Dipendente dipendente)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = _gestioneUtentiDataAccess.InserisciDipendente(dipendente);

                    if (res > 0)
                    {
                        try
                        {
                            dipendente.IdUtente = res;
                            Membership.UpdateUser(dipendente);

                            try
                            {
                                _gestioneUtentiDataAccess.AssociaUtenteRuolo(dipendente.IdUtente,
                                    RuoliPredefiniti.Dipendente.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.Errore;
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        ///     Inserimento di un ispettore
        /// </summary>
        /// <param name="ispettore"></param>
        /// <returns>True se il dipendente � stato inserito correttemente</returns>
        public ErroriRegistrazione InserisciIspettore(Ispettore ispettore)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = _gestioneUtentiDataAccess.InserisciIspettore(ispettore);

                    if (res > 0)
                    {
                        try
                        {
                            ispettore.IdUtente = res;
                            Membership.UpdateUser(ispettore);

                            try
                            {
                                _gestioneUtentiDataAccess.AssociaUtenteRuolo(ispettore.IdUtente,
                                    RuoliPredefiniti.Dipendente.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.Errore;
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }

            #region old

            //try
            //{
            //    int res = gestioneUtentiDataAccess.InserisciIspettore(ispettore);

            //    if (res > 0)
            //    {
            //        //Registriamo il ruolo dipendente all'ispettore di default
            //        gestioneUtentiDataAccess.AssociaUtenteRuolo(ispettore.IdUtente,
            //                                                    RuoliPredefiniti.Dipendente.ToString());

            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}

            #endregion
        }

        /// <summary>
        ///     Inserimento di un sindacalista
        /// </summary>
        /// <param name="sindacalista"></param>
        /// <returns>True se il dipendente � stato inserito correttemente</returns>
        public ErroriRegistrazione InserisciSindacalista(Sindacalista sindacalista)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = _gestioneUtentiDataAccess.InserisciSindacalista(sindacalista);

                    if (res > 0)
                    {
                        try
                        {
                            sindacalista.IdUtente = res;
                            Membership.UpdateUser(sindacalista);

                            try
                            {
                                _gestioneUtentiDataAccess.AssociaUtenteRuolo(sindacalista.IdUtente,
                                    RuoliPredefiniti.Sindacalista.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.Errore;
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        ///     Inserimento di un committente
        /// </summary>
        /// <param name="committente"></param>
        /// <returns></returns>
        public ErroriRegistrazione InserisciCommittente(Committente committente)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = _gestioneUtentiDataAccess.InserisciCommittente(committente);

                    if (res > 0)
                    {
                        try
                        {
                            committente.IdUtente = res;
                            Membership.UpdateUser(committente);

                            try
                            {
                                _gestioneUtentiDataAccess.AssociaUtenteRuolo(committente.IdUtente,
                                    RuoliPredefiniti.Committente.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.Errore;
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        public bool ModificaIspettore(Ispettore ispettore)
        {
            try
            {
                bool res = _gestioneUtentiDataAccess.ModificaIspettore(ispettore);

                return res;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///     Inserimento di un fornitore
        /// </summary>
        /// <param name="fornitore"></param>
        /// <returns>True se il fornitore � stato inserito correttemente</returns>
        public ErroriRegistrazione InserisciFornitore(Fornitore fornitore)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = _gestioneUtentiDataAccess.InserisciFornitore(fornitore);

                    if (res > 0)
                    {
                        try
                        {
                            fornitore.IdUtente = res;
                            Membership.UpdateUser(fornitore);

                            try
                            {
                                _gestioneUtentiDataAccess.AssociaUtenteRuolo(fornitore.IdUtente,
                                    RuoliPredefiniti.Fornitore.ToString());

                                transactionScope.Complete();
                                return ErroriRegistrazione.RegistrazioneEffettuata;
                            }
                            catch (Exception)
                            {
                                return ErroriRegistrazione.RuoloNonAssociato;
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return ErroriRegistrazione.LoginNonCorretta;
                        }
                        catch (MembershipPasswordException)
                        {
                            return ErroriRegistrazione.PasswordNonCorretta;
                        }
                        catch (MembershipCreateUserException)
                        {
                            return ErroriRegistrazione.LoginPresente;
                        }
                    }
                    return ErroriRegistrazione.Errore;
                }
                catch (Exception)
                {
                    return ErroriRegistrazione.Errore;
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="ospite"></param>
        /// <returns></returns>
        public EsitoRegistrazioneUtente InserisciOspite(Ospite ospite)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                try
                {
                    int res = _gestioneUtentiDataAccess.InserisciOspite(ospite);

                    if (res > 0)
                    {
                        try
                        {
                            ospite.IdUtente = res;
                            Membership.UpdateUser(ospite);

                            try
                            {
                                _gestioneUtentiDataAccess.AssociaUtenteRuolo(ospite.IdUtente,
                                    RuoliPredefiniti.Ospite.ToString());

                                transactionScope.Complete();
                                return new EsitoRegistrazioneUtente
                                {
                                    Esito = ErroriRegistrazione.RegistrazioneEffettuata
                                };
                            }
                            catch (Exception)
                            {
                                return new EsitoRegistrazioneUtente
                                {
                                    Esito = ErroriRegistrazione.RuoloNonAssociato
                                };
                            }
                        }
                        catch (MemberAccessException)
                        {
                            return new EsitoRegistrazioneUtente
                            {
                                Esito = ErroriRegistrazione.LoginNonCorretta
                            };
                        }
                        catch (MembershipPasswordException)
                        {
                            return new EsitoRegistrazioneUtente
                            {
                                Esito = ErroriRegistrazione.PasswordNonCorretta
                            };
                        }
                        catch (MembershipCreateUserException)
                        {
                            return new EsitoRegistrazioneUtente
                            {
                                Esito = ErroriRegistrazione.LoginPresente
                            };
                        }
                    }
                    return new EsitoRegistrazioneUtente
                    {
                        Esito = ErroriRegistrazione.Errore
                    };
                }
                catch (Exception ex)
                {
                    return new EsitoRegistrazioneUtente
                    {
                        Esito = ErroriRegistrazione.Errore,
                        Exc = ex
                    };
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected int ChallengeLavoratore(string nome, string cognome, string codiceFiscale, string pin)
        {
            //ritorna l'idutente
            try
            {
                int res = _gestioneUtentiDataAccess.ChallengeLavoratore(nome, cognome, codiceFiscale, pin);

                return res;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        //protected int ChallengeImpresa(string ragioneSociale, string partitaIVA)
        //{
        //    //ritorna l'idutente
        //    try
        //    {
        //        int res = gestioneUtentiDataAccess.ChallengeImpresa(ragioneSociale, partitaIVA);

        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        return -1;
        //    }
        //}

        /// <summary>
        ///     Ritorna l'idUtente nel caso in cui nel DB sia presente un impresa con l'id passato e il PIN passato
        /// </summary>
        /// <param name="idImpresa"></param>
        /// <param name="PIN"></param>
        /// <returns></returns>
        protected int ChallengeImpresaPIN(int idImpresa, string PIN)
        {
            //ritorna l'idutente
            try
            {
                int res = _gestioneUtentiDataAccess.ChallengeImpresaPIN(idImpresa, PIN);

                return res;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        /// <summary>
        ///     Ritorna l'Utente dato il suo id
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public Utente GetUtenteById(int idUtente)
        {
            try
            {
                Utente res = _gestioneUtentiDataAccess.GetUtenteById(idUtente);

                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        ///     Ritorna l'idUtente nel caso in cui nel DB sia presente un impresa con l'id passato e il PIN passato
        /// </summary>
        /// <param name="idConsulente"></param>
        /// <param name="PIN"></param>
        /// <returns></returns>
        protected int ChallengeConsulentePIN(int idConsulente, string PIN)
        {
            //ritorna l'idutente
            try
            {
                int res = _gestioneUtentiDataAccess.ChallengeConsulentePIN(idConsulente, PIN);

                return res;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        #endregion

        #region Generazione PIN

        public string GeneraPin()
        {
            return Guid.NewGuid().ToString().ToUpper().Substring(0, 8);
        }

        /// <summary>
        ///     Genera un PIN per l'impresa
        /// </summary>
        /// <param name="id">Id dell'impresa</param>
        public string GeneraPinImpresa(int id)
        {
            string pin = GeneraPin();
            _gestioneUtentiDataAccess.AggiornaPinImpresa(id, pin);
            return pin;
        }

        /// <summary>
        ///     Genera un PIN per il lavoratore
        /// </summary>
        /// <param name="id">Id del lavoratore</param>
        public string GeneraPinLavoratore(int id)
        {
            string pin = GeneraPin();
            _gestioneUtentiDataAccess.AggiornaPinLavoratore(id, pin);
            return pin;
        }

        /// <summary>
        ///     Genera un PIN per il consulente
        /// </summary>
        /// <param name="id">Id del consulente</param>
        public string GeneraPinConsulente(int id)
        {
            string pin = GeneraPin();
            _gestioneUtentiDataAccess.AggiornaPinConsulente(id, pin);
            return pin;
        }

        #endregion

        #endregion

        #region Gestione Ruoli/Funzionalita

        /// <summary>
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaUtente(int idUtente)
        {
            try
            {
                return _gestioneUtentiDataAccess.GetFunzionalitaUtente(idUtente);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="idUtente"></param>
        /// <returns></returns>
        public RuoliCollection GetRuoliUtente(int idUtente)
        {
            try
            {
                return _gestioneUtentiDataAccess.GetRuoliUtente(idUtente);
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// </summary>
        /// <returns></returns>
        public void CreaRuolo(Ruolo ruolo, FunzionalitaCollection funzionalita)
        {
            try
            {
                int idRuolo = _gestioneUtentiDataAccess.CreaRuolo(ruolo);

                AssociaFunzionalitaRuolo(idRuolo, funzionalita);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public RuoliCollection GetRuoli()
        {
            try
            {
                return _gestioneUtentiDataAccess.GetRuoli();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <returns></returns>
        public Ruolo GetRuolo(int idRuolo)
        {
            return _gestioneUtentiDataAccess.GetRuolo(idRuolo);
        }

        /// <summary>
        /// </summary>
        /// <param name="nomeRuolo"></param>
        /// <returns></returns>
        public Ruolo GetRuolo(string nomeRuolo)
        {
            return _gestioneUtentiDataAccess.GetRuolo(nomeRuolo);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalita()
        {
            try
            {
                return _gestioneUtentiDataAccess.GetFunzionalita();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <returns></returns>
        public FunzionalitaCollection GetFunzionalitaRuolo(int idRuolo)
        {
            try
            {
                return _gestioneUtentiDataAccess.GetFunzionalitaRuolo(idRuolo);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        ///     Aggiorna solamente le funzoinalit� associate ad un ruolo senza modificare il ruolo
        /// </summary>
        /// <param name="idRuolo"></param>
        /// <param name="funzionalita"></param>
        /// <returns></returns>
        public bool AssociaFunzionalitaRuolo(int idRuolo, FunzionalitaCollection funzionalita)
        {
            try
            {
                _gestioneUtentiDataAccess.AssociaFuzionalitaRuolo(idRuolo, funzionalita);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="idRuolo"></param>
        public void CancellaRuolo(int idRuolo)
        {
            _gestioneUtentiDataAccess.CancellaRuolo(idRuolo);
        }

        #endregion
    }
}