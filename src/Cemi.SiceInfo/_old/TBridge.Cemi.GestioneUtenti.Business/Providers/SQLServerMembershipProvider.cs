using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using TBridge.Cemi.Data.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace TBridge.Cemi.GestioneUtenti.Business.Providers
{
    internal class SQLServerMembershipProvider : MembershipProvider
    {
        private GestioneUtentiAccess _gestioneUtentiDataAccess;
        private int _minRequiredPasswordLength;

        public override bool EnablePasswordRetrieval => false;

        public override bool EnablePasswordReset => false;

        public override int MinRequiredPasswordLength => _minRequiredPasswordLength;

        // MembershipProvider Methods
        public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
                throw new ArgumentNullException("config");

            // Assign the provider a default name if it doesn't have one
            if (string.IsNullOrEmpty(name))
                name = "SQLServerMembershipProvider";

            //Inizializzo lo strato di accesso al db
            _gestioneUtentiDataAccess = new GestioneUtentiAccess();

            _minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "8"));

            // Call the base class's Initialize method
            base.Initialize(name, config);
        }

        private static string GetConfigValue(string configValue, string defaultValue)
        {
            if (string.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        public override bool ValidateUser(string username, string password)
        {
            // Validate input parameters
            if (string.IsNullOrEmpty(username) ||
                string.IsNullOrEmpty(password))
                return false;

            try
            {
                // Validate the user name and password

                if (_gestioneUtentiDataAccess.EsisteUtente(username, password) &&
                    !Roles.IsUserInRole(username, "UtenteDisabilitato"))
                {
                    // NOTE: A read/write membership provider
                    // would update the user's LastLoginDate here.
                    // A fully featured provider would also fire
                    // an AuditMembershipAuthenticationSuccess
                    // Web event

                    return true;
                }

                // NOTE: A fully featured membership provider would
                // fire an AuditMembershipAuthenticationFailure
                // Web event here
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            // Utilizzo il Context per salvare l'utente corrente in modo che per ogni singola pagina
            // richiesta venga fatta solamente una richiesta al DB. Da notare che quando viene effettuato
            // un postback o si cambia pagina il Context torna ad essere nullo e quindi l'utente viene
            // nuovamente recuperato da DB
            //
            // Questo trucco � particolarmente utile per le molte chiamate al metodo "Autorizzato" sulla
            // stessa pagina. Alla prima chiamata viene memorizzato l'utente nel Context e poi recuperato
            // da li fin quando la pagina completa non viene inviata al client
            //
            // Sarebbe da verificare se pu� essere utilizzato HttpContext.Current.User al posto di creare
            // una voce ad-hoc

            Utente utente = null;

            if (!string.IsNullOrEmpty(username))
            {
                if (HttpContext.Current.Items["__Utente"] == null)
                {
                    HttpContext.Current.Items["__Utente"] = _gestioneUtentiDataAccess.GetUtente(username);
                }

                utente = HttpContext.Current.Items["__Utente"] as Utente;
            }

            //Utente utente = _gestioneUtentiDataAccess.GetUtente(username);
            return utente;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            Utente utente = (Utente) Membership.GetUser(username);

            if (utente != null)
            {
                Regex patternPassword =
                    new Regex(@"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{8,15})$");

                if (oldPassword != newPassword)
                {
                    if (ValidateUser(username, oldPassword))
                    {
                        if (patternPassword.IsMatch(newPassword))
                        {
                            if (username != newPassword)
                            {
                                utente.Password = newPassword;
                                _gestioneUtentiDataAccess.RegistraUtente(utente);
                            }
                            else
                                throw new MembershipPasswordException(
                                    "La password non pu� essere uguale allo username.");
                        }
                        else
                            throw new MembershipPasswordException(
                                "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.");
                    }
                    else
                    {
                        throw new MembershipPasswordException("La vecchia password inserita non � corretta.");
                    }
                }
                else
                {
                    throw new MembershipPasswordException(
                        "La nuova password non pu� essere uguale alla vecchia password.");
                }
            }
            else
            {
                throw new Exception("Utente non trovato.");
            }

            //Utente utente = gestioneUtentiDataAccess.GetUtente(username, oldPassword);
            //utente.Password = newPassword;
            //gestioneUtentiDataAccess.AggiornaUtente(utente);

            return true;
        }

        public override void UpdateUser(MembershipUser user)
        {
            Regex patternUsername = new Regex("([a-zA-Z0-9]{5,15})$");
            //Regex patternPassword = new Regex(@"([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{5,15})$");

            Regex patternPassword =
                new Regex(@"(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{8,15})$");

            if (!patternUsername.IsMatch(user.UserName))
                throw new MemberAccessException("Formato della username non ammesso");
            if (!patternPassword.IsMatch(((Utente) user).Password))
                throw new MembershipPasswordException(
                    "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.");

            if (user.UserName == ((Utente) user).Password)
            {
                throw new MembershipPasswordException("La password non pu� essere uguale allo username.");
            }

            try
            {
                if (!_gestioneUtentiDataAccess.RegistraUtente((Utente) user))
                {
                    throw new MembershipCreateUserException("Username gi� presente");
                }
            }
            catch (Exception)
            {
                throw new MembershipCreateUserException("Username gi� presente");
            }
        }

        #region Not Supported

        public override string ApplicationName
        {
            get => throw new NotSupportedException();
            set => throw new NotSupportedException();
        }

        public override int MaxInvalidPasswordAttempts => throw new NotSupportedException();

        public override int MinRequiredNonAlphanumericCharacters => throw new NotSupportedException();

        public override int PasswordAttemptWindow => throw new NotSupportedException();

        public override MembershipPasswordFormat PasswordFormat => throw new NotSupportedException();

        public override string PasswordStrengthRegularExpression => throw new NotSupportedException();

        public override bool RequiresQuestionAndAnswer => throw new NotSupportedException();

        public override bool RequiresUniqueEmail => throw new NotSupportedException();

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotSupportedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotSupportedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
            string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotSupportedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email,
            string passwordQuestion, string passwordAnswer, bool isApproved,
            object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotSupportedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotSupportedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
            out int totalRecords)
        {
            throw new NotSupportedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
            out int totalRecords)
        {
            throw new NotSupportedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotSupportedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotSupportedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotSupportedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotSupportedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}