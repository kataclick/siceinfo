﻿using System;
using System.Linq;
using System.Web.Security;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;
using Utente = TBridge.Cemi.Type.Entities.GestioneUtenti.Utente;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public class ImpersonateManager
    {
        public static void Impersonate(string username)
        {
            Utente utenteCorrente = (Utente) Membership.GetUser();

            if (utenteCorrente == null)
                throw new Exception("Utente non loggato!");

            using (SICEEntities context = new SICEEntities())
            {
                Type.Domain.Utente utenteImpersonato = (from utente in context.Utenti
                    where utente.Username == username
                    select utente).Single();

                ImpersonateLog impersonateLog = new ImpersonateLog
                {
                    Data = DateTime.Now,
                    IdUtenteReale = utenteCorrente.IdUtente,
                    UsernameUtenteReale = utenteCorrente.UserName,
                    IdUtenteImpersonato = utenteImpersonato.Id,
                    UsernameUtenteImpersonato = utenteImpersonato.Username
                };

                context.ImpersonateLogs.AddObject(impersonateLog);
                context.SaveChanges();
            }

            FormsAuthentication.RedirectFromLoginPage(username, false);
        }

        public static void ImpersonateEdilConnect(string username)
        {
            FormsAuthentication.RedirectFromLoginPage(username, false);
        }

        public static void ImpersonateSOLDO(string username)
        {
            FormsAuthentication.RedirectFromLoginPage(username, false);
        }
    }
}