﻿using System;
using System.Linq;
using System.Web.Security;
using TBridge.Cemi.Data;
using TBridge.Cemi.Type.Domain;

namespace TBridge.Cemi.GestioneUtenti.Business
{
    public class UtentiManager
    {
        public Utente GetUtente(string username, string password)
        {
            string passwordHash = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");

            using (SICEEntities context = new SICEEntities())
            {
                var utenteCorretto = (from utente in context.Utenti
                    where utente.Username == username && utente.Password == passwordHash
                    select utente).SingleOrDefault();

                return utenteCorretto;
            }
        }

        public Utente GetUtente(int idUtente)
        {
            using (SICEEntities context = new SICEEntities())
            {
                var ut = (from utente in context.Utenti
                    where utente.Id == idUtente
                    select utente).SingleOrDefault();

                return ut;
            }
        }

        public bool EsisteUtente(string username, string password)
        {
            Utente utente = GetUtente(username, password);
            return utente != null;
        }

        public bool AggiornaScadenzaPassword(int idUtente, int numeroMesi)
        {
            int num;

            using (SICEEntities context = new SICEEntities())
            {
                var utente = (from u in context.Utenti
                    where u.Id == idUtente
                    select u).SingleOrDefault();

                utente.DataScadenzaPassword = DateTime.Today.AddMonths(numeroMesi);

                num = context.SaveChanges();
            }

            return num > 0;
        }

        public bool ImpostaPassword(string username, string password, int numeroMesi)
        {
            int num;
            string passwordHash = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "sha1");

            using (SICEEntities context = new SICEEntities())
            {
                var utente = (from u in context.Utenti
                    where u.Username == username
                    select u).SingleOrDefault();

                utente.Password = passwordHash;
                utente.DataScadenzaPassword = DateTime.Today.AddMonths(numeroMesi);

                num = context.SaveChanges();
            }

            return num > 0;
        }

        public bool AggiornaFasciaOraria(int idUtente, DateTime dataOraInizio, DateTime dataOraFine)
        {
            int num;

            using (SICEEntities context = new SICEEntities())
            {
                var utente = (from u in context.Utenti
                    where u.Id == idUtente
                    select u).SingleOrDefault();

                utente.OraInizioValidita = dataOraInizio;
                utente.OraFineValidita = dataOraFine;

                num = context.SaveChanges();
            }

            return num > 0;
        }

        #region Gestione Pin

        public string GeneraPin()
        {
            return Guid.NewGuid().ToString().ToUpper().Substring(0, 8);
        }

        public string ImpostaPinLavoratore(int idLavoratore)
        {
            string pin = GeneraPin();

            using (SICEEntities context = new SICEEntities())
            {
                var lav = (from lavoratore in context.Lavoratori
                    where lavoratore.Id == idLavoratore
                    select lavoratore).SingleOrDefault();

                lav.Pin = pin;
                lav.DataGenerazionePin = DateTime.Now;

                context.SaveChanges();
            }

            return pin;
        }

        public string ImpostaPinLavoratoreByIdUtente(int idUtente)
        {
            string pin = GeneraPin();

            using (SICEEntities context = new SICEEntities())
            {
                var lav = (from lavoratore in context.Lavoratori
                    where lavoratore.IdUtente == idUtente
                    select lavoratore).SingleOrDefault();

                lav.Pin = pin;
                lav.DataGenerazionePin = DateTime.Now;

                context.SaveChanges();
            }

            return pin;
        }

        public string ImpostaPinImpresa(int idImpresa)
        {
            string pin = GeneraPin();

            using (SICEEntities context = new SICEEntities())
            {
                var imp = (from impresa in context.Imprese
                    where impresa.Id == idImpresa
                    select impresa).SingleOrDefault();

                imp.Pin = pin;
                imp.DataGenerazionePin = DateTime.Now;

                context.SaveChanges();
            }

            return pin;
        }

        public string ImpostaPinImpresaByIdUtente(int idUtente)
        {
            string pin = GeneraPin();

            using (SICEEntities context = new SICEEntities())
            {
                var imp = (from impresa in context.Imprese
                    where impresa.IdUtente == idUtente
                    select impresa).SingleOrDefault();

                imp.Pin = pin;
                imp.DataGenerazionePin = DateTime.Now;

                context.SaveChanges();
            }

            return pin;
        }

        public string ImpostaPinConsulente(int idConsulente)
        {
            string pin = GeneraPin();

            using (SICEEntities context = new SICEEntities())
            {
                var con = (from consulente in context.Consulenti
                    where consulente.Id == idConsulente
                    select consulente).SingleOrDefault();

                con.Pin = pin;
                con.DataGenerazionePin = DateTime.Now;

                context.SaveChanges();
            }

            return pin;
        }

        public string ImpostaPinConsulenteByIdUtente(int idUtente)
        {
            string pin = GeneraPin();

            using (SICEEntities context = new SICEEntities())
            {
                var con = (from consulente in context.Consulenti
                    where consulente.IdUtente == idUtente
                    select consulente).SingleOrDefault();

                con.Pin = pin;
                con.DataGenerazionePin = DateTime.Now;

                context.SaveChanges();
            }

            return pin;
        }

        #endregion
    }
}