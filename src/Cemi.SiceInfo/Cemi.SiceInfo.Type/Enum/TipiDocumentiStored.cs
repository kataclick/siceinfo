﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.SiceInfo.Type.Enum
{
    public enum TipiDocumentiStored
    {
        LibroUnico = 1,
        Denunce = 2,
        Uniemens = 3
    }
}
