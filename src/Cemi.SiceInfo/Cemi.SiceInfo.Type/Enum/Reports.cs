﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cemi.SiceInfo.Type.Attribute;

namespace Cemi.SiceInfo.Type.Enum
{
    public enum Reports
    {
        [Description("Imprese Adempienti")] [ReportName("ImpreseAdempienti")] ImpreseAdempeinti = 1
    }
}
