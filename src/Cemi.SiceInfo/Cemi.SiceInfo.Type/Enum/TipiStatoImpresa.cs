﻿namespace Cemi.SiceInfo.Type.Enum
{
    public enum TipiStatoImpresa
    {
        Sospensione = 1,
        Riattivazione,
        Cessazione
    }
}
