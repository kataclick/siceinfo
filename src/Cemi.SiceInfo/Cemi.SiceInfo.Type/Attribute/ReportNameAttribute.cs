﻿namespace Cemi.SiceInfo.Type.Attribute
{
    public class ReportNameAttribute : System.Attribute
    {
        public readonly string ReportName;
        public ReportNameAttribute(string reportName)
        {
            ReportName = reportName;
        }
    }
}
