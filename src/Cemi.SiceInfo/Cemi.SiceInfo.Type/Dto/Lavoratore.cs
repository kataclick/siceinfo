﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cemi.SiceInfo.Type.Dto
{
    public class Lavoratore
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string CodiceFiscale { get; set; }
        public DateTime DataNascita { get; set; }
        public string LuogoNascita { get; set; }
        public string Sesso { get; set; }
        public string Telefono { get; set; }
        public string Cellulare { get; set; }
        public string Email { get; set; }

        public Indirizzo Residenza { get; set; }
        
        public string NomeCompleto => $"{Nome} {Cognome}";
        public string DataNascitaString => DataNascita.ToShortDateString();
        public List<Prestazione> PrestazioniErogate { get; set; } = new List<Prestazione>();
        public List<Comunicazione> Comunicazioni { get; set; } = new List<Comunicazione>();
        public List<OraDichiarata> OreAccantonate { get; set; } = new List<OraDichiarata>();
    }
}
