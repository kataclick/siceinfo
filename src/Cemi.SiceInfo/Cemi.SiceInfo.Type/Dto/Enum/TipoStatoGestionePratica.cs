﻿namespace Cemi.SiceInfo.Type.Dto.Enum
{
    public class TipoStatoGestionePratica
    {
        public int Id { get; set; }
        public string Descrizione { get; set; }
    }
}
