﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cemi.SiceInfo.Type.Dto
{
    public class Comunicazione
    {
        public string Canale { get; set; }
        public DateTime? Data { get; set; }
        public string Testo { get; set; }
    }
}
