﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cemi.SiceInfo.Type.Dto
{
    public class OraDichiarata
    {
        public int IdImpresa { get; set; }
        public string ImpresaRagioneSociale { get; set; }
        public DateTime Periodo { get; set; }
        public string TipoOra { get; set; }
        public decimal Ore { get; set; }
        public bool Pagate { get; set; }
    }
}
