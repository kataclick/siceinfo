﻿using System;

namespace Cemi.SiceInfo.Type.Dto.Filters
{
    public class ImpresaRichiestaVariazioneStatoFilter
    {
        public Int32? IdImpresa { get; set; }
        public String RagioneSociale { get; set; }
        public DateTime? DataRichiestaDa { get; set; }
        public DateTime? DataRichiestaA { get; set; }
        public Int32? IdTipoStatoImpresa { get; set; }
        public Int32? IdTipoStatoGestionePratica { get; set; }
        public DateTime? DataInizioCambioStato { get; set; }
    }
}
