﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cemi.SiceInfo.Utility;

namespace Cemi.SiceInfo.Type.Dto
{
    public class Indirizzo
    {
        public string Denominazione { get; set; }
        public string Cap { get; set; }
        public string Comune { get; set; }
        public string Provincia { get; set; }

        public string IndirizzoCompleto => GenericStaticMethods.GetIndirizzoCompleto(Denominazione, Cap, Comune, Provincia);
    }
}
