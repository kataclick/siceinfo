﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cemi.SiceInfo.Type.Dto
{
    public class Prestazione
    {
        public string Tipo { get; set; }
        public DateTime? DataValuta { get; set; }
        public string StatoDomanda { get; set; }
        public decimal? ImportoErogatoLordo { get; set; }
        public decimal? ImportoErogatoNetto { get; set; }
        public string ModalitaPagamento { get; set; }
        public string IdentificativoPagamento { get; set; }
    }
}
