﻿using System;

namespace Cemi.SiceInfo.Type.Dto.ImpreseAdem
{
    public class Comune
    {
        public String CodiceCatastale { get; set; }
        public String Nome { get; set; }
    }
}