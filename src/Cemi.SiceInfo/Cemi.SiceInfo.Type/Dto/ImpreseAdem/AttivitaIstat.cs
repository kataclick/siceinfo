﻿using System;

namespace Cemi.SiceInfo.Type.Dto.ImpreseAdem
{
    public class AttivitaIstat
    {
        public String CodiceAttivita { get; set; }
        public String Descrizione { get; set; }
    }
}