namespace Cemi.SiceInfo.Type.Dto.ImpreseAdem
{
    public class Impresa
    {
        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }
    }
}