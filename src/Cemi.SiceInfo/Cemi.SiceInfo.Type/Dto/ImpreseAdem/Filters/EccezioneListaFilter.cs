namespace Cemi.SiceInfo.Type.Dto.ImpreseAdem.Filters
{
    public class EccezioneListaFilter
    {
        public int? IdImpresa { get; set; }

        public string RagioneSociale { get; set; }
    }
}