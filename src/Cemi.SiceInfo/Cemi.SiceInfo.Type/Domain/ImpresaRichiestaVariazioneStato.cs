// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning


namespace Cemi.SiceInfo.Type.Domain
{

    // ImpreseRichiesteVariazioneStato
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.32.0.0")]
    public partial class ImpresaRichiestaVariazioneStato
    {
        public int Id { get; set; } // idRichiesta (Primary key)
        public int IdImpresa { get; set; } // idImpresa
        public System.DateTime DataRichiesta { get; set; } // dataRichiesta
        public int IdUtente { get; set; } // idUtente
        public Cemi.SiceInfo.Type.Enum.TipiStatoImpresa IdTipoStatoImpresa { get; set; } // idTipoStatoImpresa
        public System.DateTime DataInizioCambioStato { get; set; } // dataInizioCambioStato
        public string Note { get; set; } // note (length: 500)
        public Cemi.SiceInfo.Type.Enum.TipiStatoGestionePratica IdTipoStatoPratica { get; set; } // idTipoStatoPratica
        public System.DateTime? DataGestioneSiceNew { get; set; } // dataGestioneSiceNew
        public bool GestitoSiceNew { get; set; } // gestitoSiceNew
        public int? IdUtenteOperatore { get; set; } // idUtenteOperatore
        public System.DateTime? DataGestioneOperatore { get; set; } // dataGestioneOperatore
        public int? IdTipoCausaleRespinta { get; set; } // idTipoCausaleRespinta
        public string NoteCemi { get; set; } // noteCemi (length: 500)

        // Reverse navigation

        /// <summary>
        /// Child Documenti (Many-to-Many) mapped by table [ImpreseRichiesteVariazioneStatoDocumenti]
        /// </summary>
        public virtual System.Collections.Generic.ICollection<Documento> Documenti { get; set; } // Many to many mapping

        // Foreign keys

        /// <summary>
        /// Parent Imprese pointed by [ImpreseRichiesteVariazioneStato].([IdImpresa]) (FK_ImpreseRichiesteVariazioneStato_Imprese)
        /// </summary>
        public virtual Impresa Impresa { get; set; } // FK_ImpreseRichiesteVariazioneStato_Imprese

        /// <summary>
        /// Parent Utenti pointed by [ImpreseRichiesteVariazioneStato].([IdUtente]) (FK_ImpreseRichiesteVariazioneStato_Utenti)
        /// </summary>
        public virtual Utente Utente { get; set; } // FK_ImpreseRichiesteVariazioneStato_Utenti

        public ImpresaRichiestaVariazioneStato()
        {
            DataRichiesta = System.DateTime.Now;
            IdTipoStatoPratica = 0;
            GestitoSiceNew = false;
            Documenti = new System.Collections.Generic.List<Documento>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
