﻿using System.Collections.Generic;
using System.Linq;
using Cemi.SiceInfo.Data;
using Cemi.SiceInfo.Type.Domain;
using Cemi.SiceInfo.Type.Dto;
using Cemi.SiceInfo.Utility;
using Lavoratore = Cemi.SiceInfo.Type.Dto.Lavoratore;

namespace Cemi.SiceInfo.Business.Imp
{
    public class LavoratoreManager : ILavoratoreManager
    {
        public Lavoratore GetLavoratoreInfo(int idLavoratore)
        {
            Lavoratore lavoratore = new Lavoratore();

            using (var context = new SiceContext())
            {
                var queryLav = from lav in context.Lavoratori
                    where lav.IdLavoratore == idLavoratore
                    select lav;

                var lavoratoreStored = queryLav.Single();
                MapAnagrafica(lavoratoreStored, lavoratore);

                var prestazioniStored = context.UspPrestazioniSelectByIdLavoratore(idLavoratore);
                lavoratore.PrestazioniErogate = MapPrestazioni(prestazioniStored);

                var oreStored = context.UspOreDenunciateSelectByLavWithPagato(idLavoratore);
                lavoratore.OreAccantonate = MapOre(oreStored);

                var querySms = from sms in context.Sms
                    join smsInvii in context.SmsInvii on sms.IdSms equals smsInvii.IdSms
                    join utenti in context.Utenti on smsInvii.IdUtente equals utenti.IdUtente
                    join lavoratori in context.Lavoratori on utenti.IdUtente equals lavoratori.IdUtente
                    where lavoratori.IdLavoratore == idLavoratore
                    orderby smsInvii.DataInvio descending
                    select new Comunicazione {Canale = "SMS", Data = smsInvii.DataInvio, Testo = sms.Testo};

                lavoratore.Comunicazioni = querySms.ToList();
            }

            return lavoratore;
        }

        public Lavoratore GetLavoratoreAnagrafica(int idLavoratore)
        {
            Lavoratore lavoratore = new Lavoratore();

            using (var context = new SiceContext())
            {
                var queryLav = from lav in context.Lavoratori
                    where lav.IdLavoratore == idLavoratore
                    select lav;

                var lavoratoreStored = queryLav.Single();
                MapAnagrafica(lavoratoreStored, lavoratore);
            }

            return lavoratore;
        }

        public List<Prestazione> GetPrestazioni(int idLavoratore)
        {
            List<Prestazione> ret;

            using (var context = new SiceContext())
            {
                var prestazioniStored = context.UspPrestazioniSelectByIdLavoratore(idLavoratore);
                ret = MapPrestazioni(prestazioniStored);
            }

            return ret;
        }

        public List<OraDichiarata> GetOreDichiarate(int idLavoratore)
        {
            List<OraDichiarata> ret;

            using (var context = new SiceContext())
            {
                var oreStored = context.UspOreDenunciateSelectByLavWithPagato(idLavoratore);
                ret = MapOre(oreStored);
            }

            return ret;
        }

        public List<Comunicazione> GetComunicazioni(int idLavoratore)
        {
            List<Comunicazione> ret;

            using (var context = new SiceContext())
            {
                var querySms = from sms in context.Sms
                    join smsInvii in context.SmsInvii on sms.IdSms equals smsInvii.IdSms
                    join utenti in context.Utenti on smsInvii.IdUtente equals utenti.IdUtente
                    join lavoratori in context.Lavoratori on utenti.IdUtente equals lavoratori.IdUtente
                    where lavoratori.IdLavoratore == idLavoratore
                    select new Comunicazione {Canale = "SMS", Data = smsInvii.DataInvio, Testo = sms.Testo};

                ret = querySms.ToList();
            }

            return ret;
        }

        private static void MapAnagrafica(Type.Domain.Lavoratore lavoratoreStored, Lavoratore lavoratore)
        {
            MyMapper<Type.Domain.Lavoratore, Lavoratore>.MapToExisting(lavoratoreStored, lavoratore);
            lavoratore.Residenza = new Indirizzo
            {
                Denominazione = lavoratoreStored.IndirizzoDenominazione,
                Comune = lavoratoreStored.IndirizzoComune,
                Cap = lavoratoreStored.IndirizzoCap,
                Provincia = lavoratoreStored.IndirizzoProvincia
            };
        }

        private static List<Prestazione> MapPrestazioni(
            List<UspPrestazioniSelectByIdLavoratoreReturnModel> prestazioniStored)
        {
            List<Prestazione> prestazioni = MyMapper<UspPrestazioniSelectByIdLavoratoreReturnModel, Prestazione>
                .MapEnumerable(prestazioniStored).ToList();
            for (int i = 0; i < prestazioniStored.Count; i++)
            {
                prestazioni[i].DataValuta = prestazioniStored[i].dataEmissioneAssegno;
                prestazioni[i].IdentificativoPagamento = prestazioniStored[i].numeroMandato;
                prestazioni[i].ImportoErogatoNetto = prestazioniStored[i].importoErogato;
                prestazioni[i].StatoDomanda = prestazioniStored[i].statoPrestazione;
                prestazioni[i].Tipo = prestazioniStored[i].tipoPrestazione;
            }
            return prestazioni;
        }

        private static List<OraDichiarata> MapOre(List<UspOreDenunciateSelectByLavWithPagatoReturnModel> oreStored)
        {
            List<OraDichiarata> oreDichiarate =
                MyMapper<UspOreDenunciateSelectByLavWithPagatoReturnModel, OraDichiarata>.MapEnumerable(oreStored)
                    .ToList();
            for (int i = 0; i < oreStored.Count; i++)
            {
                oreDichiarate[i].ImpresaRagioneSociale = oreStored[i].ragioneSociale;
                oreDichiarate[i].Ore = oreStored[i].oreDichiarate;
                oreDichiarate[i].Periodo = oreStored[i].dataDenormalizzazione;
                oreDichiarate[i].TipoOra = oreStored[i].descrizione;
            }
            return oreDichiarate;
        }
    }
}