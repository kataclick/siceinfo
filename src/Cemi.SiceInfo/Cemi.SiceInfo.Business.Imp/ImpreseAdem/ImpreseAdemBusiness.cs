using System.Collections.Generic;
using Cemi.SiceInfo.Type.Dto.ImpreseAdem;
using Cemi.SiceInfo.Type.Dto.ImpreseAdem.Filters;
using TBridge.Cemi.Data;

namespace Cemi.SiceInfo.Business.Imp
{
    public class ImpreseAdemBusiness
    {
        private readonly ImpreseAdemDataAccess _dataAccess = new ImpreseAdemDataAccess();

        public List<EccezioneLista> GetEccezioniListaImpreseAdempienti(EccezioneListaFilter filtro)
        {
            return _dataAccess.GetEccezioniListaImpreseAdempienti(filtro);
        }

        public bool InsertEccezioneListaImpreseAdempienti(EccezioneLista eccezione, out bool giaPresente)
        {
            return _dataAccess.InsertEccezioneListaImpreseAdempienti(eccezione, out giaPresente);
        }

        public bool DeleteEccezioneListaImpreseAdempienti(int idImpresa)
        {
            return _dataAccess.DeleteEccezioneListaImpreseAdempienti(idImpresa);
        }

        public void UpdateParametriImpreseRegolari(string singolo, string totale)
        {
            _dataAccess.UpdateParametriImpreseRegolari(singolo, totale);
        }

        public void GetParametriImpreseRegolari(out string singolo, out string totale)
        {
            _dataAccess.GetParametriImpreseRegolari(out singolo, out totale);
        }

        public List<AttivitaIstat> GetAttivitaIstatImpreseAdempienti()
        {
            return _dataAccess.GetAttivitaIstatImpreseAdempienti();
        }

        public List<Comune> GetComuniSedeAmministrativaImpreseRegolari()
        {
            return _dataAccess.GetComuniSedeAmministrativaImpreseRegolari();
        }

        public List<Comune> GetComuniSedeLegaleImpreseRegolari()
        {
            return _dataAccess.GetComuniSedeLegaleImpreseRegolari();
        }
    }
}