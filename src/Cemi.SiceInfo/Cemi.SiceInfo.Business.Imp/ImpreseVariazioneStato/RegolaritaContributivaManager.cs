﻿using System;
using System.Linq;
using Cemi.SiceInfo.Data;

namespace Cemi.SiceInfo.Business.Imp.ImpreseVariazioneStato
{
    public class RegolaritaContributivaManager
    {
        public static decimal GetDebitoImpresa(int idImpresa, DateTime? dataInizio = null, DateTime? dataFine = null)
        {
            decimal debito;

            using (var context = new SiceContext())
            {
                var queryDebito = from situazioneContabileImpresa in context.SituazioniContabiliImprese
                                  where (!dataInizio.HasValue || (dataInizio.HasValue && situazioneContabileImpresa.Data >= dataInizio.Value))
                                        && (!dataFine.HasValue ||(dataFine.HasValue && situazioneContabileImpresa.Data <= dataFine.Value))
                                  group situazioneContabileImpresa by situazioneContabileImpresa.IdImpresa
                                  into g
                                  where g.Key == idImpresa
                                  select g.Sum(d => d.ImportoDebitoDenunciato) - g.Sum(v => v.ImportoDebitoVersato);

                debito = queryDebito.SingleOrDefault();
            }

            return debito;
        }
    }
}