﻿using System;
using System.Linq;
using Cemi.SiceInfo.Data;
using Cemi.SiceInfo.Type.Domain;

namespace Cemi.SiceInfo.Business.Imp.ImpreseVariazioneStato
{
    public class DenunceManager
    {
        public static OreDenunciateAggregateComplete GetOreDenunciateAggregateComplete(int idImpresa, DateTime dataUltimaDenuncia)
        {
            OreDenunciateAggregateComplete oreUltimaDenuncia;

            using (var context = new SiceContext())
            {
                oreUltimaDenuncia = (from oreComplete in context.OreDenunciateAggregateComplete
                                     where
                                         oreComplete.IdImpresa == idImpresa &&
                                         oreComplete.Data == dataUltimaDenuncia
                                     select oreComplete).SingleOrDefault();
            }

            return oreUltimaDenuncia;
        }

        public static DateTime? GetDataUltimaDenunciaImpresa(int idImpresa)
        {
            DateTime? dataUltimaDenuncia;
            using (var context = new SiceContext())
            {
                dataUltimaDenuncia = (from denuncia in context.Denunce
                                      group denuncia by denuncia.IdImpresa
                                      into gd
                                      where gd.Key == idImpresa
                                      select gd.Max(d => d.DataDenuncia)).SingleOrDefault();
            }
            return dataUltimaDenuncia;
        }
    }
}