﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cemi.SiceInfo.Business.Storage;
using Cemi.SiceInfo.Data;
using Cemi.SiceInfo.Type.Domain.Storage;

namespace Cemi.SiceInfo.Business.Imp.Storage
{
    public class StorageManager : IStorageManager
    {
        public int Add(byte[] documentToStore)
        {
            int numRec = 0;

            Document document = new Document
            {
                File = documentToStore,
                DateLastUpdate = DateTime.Now
            };

            using (var context = new StorageContext())
            {
                context.Documents.Add(document);
                numRec = context.SaveChanges();
            }

            return numRec > 0 ? document.Id : -1;
        }

        public byte[] Get(int id)
        {
            using (var context = new StorageContext())
            {
                var query = from document in context.Documents
                    where document.Id == id
                    select document.File;

                return query.Single();
            }
        }

        public bool Delete(int id)
        {
            using (var context = new StorageContext())
            {
                var query = from document in context.Documents
                    where document.Id == id
                    select document;

                Document doc = query.Single();
                context.Documents.Remove(doc);

                return context.SaveChanges() > 0;
            }
        }
    }
}
