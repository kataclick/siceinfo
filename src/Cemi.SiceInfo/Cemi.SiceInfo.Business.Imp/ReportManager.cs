﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Cemi.SiceInfo.Type.Enum;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Cemi.SiceInfo.Business.Imp
{
    public class ReportManager : IReportManager
    {
        private readonly string _reportsDllNamespace = ConfigurationManager.AppSettings["reportsDllNamespace"];

        //private readonly List<MemoryStream> _streams = new List<MemoryStream>();

        public byte[] GetReportPdf(Reports report, Dictionary<string, object> reportParameters)
        {
            return GetReportPdf(report.ReportName(), reportParameters);
        }

        public byte[] GetReportPdf(string reportName, Dictionary<string, object> reportParameters)
        {
            var reportProcessor = new ReportProcessor();
            var typeReportSource = new TypeReportSource();

            string reportQualifiedName =
                Assembly.Load(_reportsDllNamespace).GetType($"{_reportsDllNamespace}.{reportName}").AssemblyQualifiedName;

            // reportToExport is the Assembly Qualified Name of the report
            typeReportSource.TypeName = reportQualifiedName;
            foreach (var param in reportParameters)
            {
                typeReportSource.Parameters.Add(param.Key, param.Value);
            }

            var reportPdf = reportProcessor.RenderReport("PDF", typeReportSource, null);

            return reportPdf.DocumentBytes;
        }

        /*
        public string GetReportHtml5(string reportName, Dictionary<string, object> reportParameters)
        {
            var reportProcessor = new ReportProcessor();
            var typeReportSource = new TypeReportSource();

            string reportQualifiedName =
                Assembly.Load(_reportsDllNamespace).GetType($"{_reportsDllNamespace}.{reportName}").AssemblyQualifiedName;

            // reportToExport is the Assembly Qualified Name of the report
            typeReportSource.TypeName = reportQualifiedName;
            foreach (var param in reportParameters)
            {
                typeReportSource.Parameters.Add(param.Key, param.Value);
            }
            // specify the output format.
            Hashtable deviceInfo = new Hashtable { ["OutputFormat"] = "HTML5" };


            if (reportProcessor.RenderReport("HTML5", typeReportSource, deviceInfo, CreateStream, out string retval))
            {
                retval = Encoding.Default.GetString(_streams[0].ToArray());
                CloseStreams();
            }

            return retval;
        }

        private void CloseStreams()
        {
            foreach (MemoryStream stream in _streams)
            {
                stream.Close();
            }
            _streams.Clear();
        }

        private MemoryStream CreateStream(string name, string extension, Encoding encoding, string mimeType)
        {
            string path = Path.GetTempPath();
            string filePath = Path.Combine(path, name + "." + extension);

            MemoryStream ms = new MemoryStream();
            FileStream fs = new FileStream(filePath, FileMode.Create);
            ms.WriteTo(fs);
            _streams.Add(ms);
            return ms;
        }
        */
    }
}
