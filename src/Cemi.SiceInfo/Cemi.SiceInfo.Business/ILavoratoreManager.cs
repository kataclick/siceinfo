﻿using System.Collections.Generic;
using Cemi.SiceInfo.Type.Dto;

namespace Cemi.SiceInfo.Business
{
    public interface ILavoratoreManager
    {
        Lavoratore GetLavoratoreInfo(int idLavoratore);
        Lavoratore GetLavoratoreAnagrafica(int idLavoratore);
        List<Prestazione> GetPrestazioni(int idLavoratore);
        List<OraDichiarata> GetOreDichiarate(int idLavoratore);
        List<Comunicazione> GetComunicazioni(int idLavoratore);
    }
}
