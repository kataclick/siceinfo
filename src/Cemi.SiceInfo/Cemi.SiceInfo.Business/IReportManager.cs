﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cemi.SiceInfo.Type.Enum;

namespace Cemi.SiceInfo.Business
{
    public interface IReportManager
    {
        byte[] GetReportPdf(Reports report, Dictionary<string, object> reportParameters);
        byte[] GetReportPdf(string reportName, Dictionary<string, object> reportParameters);
    }
}
