﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cemi.SiceInfo.Business.Storage
{
    public interface IStorageManager
    {
        int Add(byte[] documentToStore);
        byte[] Get(int id);
        bool Delete(int id);
    }
}
