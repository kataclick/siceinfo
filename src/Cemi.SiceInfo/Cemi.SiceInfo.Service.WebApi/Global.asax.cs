﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

namespace Cemi.SiceInfo.Service.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            // Register your types, for instance using the scoped lifestyle:
            InitializeContainer(container);

            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }

        private static void InitializeContainer(Container container)
        {
            string name = string.Empty;
            var diConfig = ConfigurationManager.GetSection("MyDIConfig") as NameValueCollection;
            if (diConfig != null)
            {
                name = diConfig["ImplementationAssemblyList"];
            }
            List<string> listName = name.Split(';').ToList();
            foreach (string n in listName)
            {
                List<string> assemblyInfo = n.Split('|').ToList();

                string assemblyName = assemblyInfo[0];

                var impAssembly = Assembly.Load(assemblyName);

                if (impAssembly != null)
                {
                    if (assemblyInfo.Count == 1)
                    {
                        var registrations =
                            from type in impAssembly.GetExportedTypes()
                            where type.GetInterfaces().Any()
                            select new { Services = type.GetInterfaces().ToList(), Implementation = type };

                        foreach (var reg in registrations)
                        {
                            foreach (var serv in reg.Services)
                            {
                                container.Register(serv, reg.Implementation, Lifestyle.Scoped);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i < assemblyInfo.Count; i++)
                        {
                            string className = assemblyInfo[i];
                            //string fullName = $"{assemblyName}.{className}";

                            var registrations =
                            from type in impAssembly.GetExportedTypes()
                            where type.Name == className && type.GetInterfaces().Any()
                            select new { Services = type.GetInterfaces().ToList(), Implementation = type };

                            foreach (var reg in registrations)
                            {
                                foreach (var serv in reg.Services)
                                {
                                    if (serv.IsGenericType)
                                    {
                                        container.RegisterConditional(serv.GetGenericTypeDefinition(), reg.Implementation.GetGenericTypeDefinition(), Lifestyle.Scoped, c => !c.Handled);
                                    }
                                    else
                                    {
                                        container.Register(serv, reg.Implementation, Lifestyle.Scoped);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
