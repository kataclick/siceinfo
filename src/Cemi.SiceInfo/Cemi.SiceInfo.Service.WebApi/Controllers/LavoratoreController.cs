﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cemi.SiceInfo.Business;
using Cemi.SiceInfo.Type.Dto;

namespace Cemi.SiceInfo.Service.WebApi.Controllers
{
    [RoutePrefix("api/Lavoratore")]
    public class LavoratoreController : ApiController
    {
        private readonly ILavoratoreManager _lavoratoreManager;

        public LavoratoreController(ILavoratoreManager lavoratoreManager)
        {
            _lavoratoreManager = lavoratoreManager;
        }

        // GET: api/Lavoratore
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Lavoratore/5
        [Route("{id}")]
        public Lavoratore Get(int id)
        {
            return _lavoratoreManager.GetLavoratoreInfo(id);
        }

        [Route("GetAnagrafica/{id}")]
        public Lavoratore GetAnagrafica(int id)
        {
            return _lavoratoreManager.GetLavoratoreAnagrafica(id);
        }

        [Route("GetPrestazioni/{id}")]
        public List<Prestazione> GetPrestazioni(int id)
        {
            return _lavoratoreManager.GetPrestazioni(id);
        }

        [Route("GetOre/{id}")]
        public List<OraDichiarata> GetOre(int id)
        {
            return _lavoratoreManager.GetOreDichiarate(id);
        }

        [Route("GetComunicazioni/{id}")]
        public List<Comunicazione> GetComunicazioni(int id)
        {
            return _lavoratoreManager.GetComunicazioni(id);
        }

        // POST: api/Lavoratore
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Lavoratore/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Lavoratore/5
        public void Delete(int id)
        {
        }
    }
}
