﻿using System;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web;
using Cemi.IdentityProvider.Config;
using IdentityServer3.Core.Configuration;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Cemi.IdentityProvider.Startup))]

namespace Cemi.IdentityProvider
{
    public class Startup
    {
        private readonly string _connString = ConfigurationManager.ConnectionStrings["IdentityServer"].ConnectionString;
        private readonly string _openIdAuthority = ConfigurationManager.AppSettings["OpenIdAuthority"];

        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

            app.UseIdentityServer(new IdentityServerOptions
            {
                SiteName = "Cassa Edile Identity Provider",
                SigningCertificate = LoadCertificate(),
                //RequireSsl = false,

                Factory = FactoryHelper.Configure(_connString),

                AuthenticationOptions = new AuthenticationOptions
                {
                    EnablePostSignOutAutoRedirect = true,
                    EnableSignOutPrompt = false,
                    //IdentityProviders = ConfigureIdentityProviders,
                    //EnableLocalLogin = false
                }
            });
        }

        private X509Certificate2 LoadCertificate()
        {
            //return new X509Certificate2($@"{AppDomain.CurrentDomain.BaseDirectory}\bin\Config\MyFcCert.pfx", "MyFcCert");
            return new X509Certificate2($@"{HttpRuntime.AppDomainAppPath}bin\Config\MySiceInfoCert.pfx", "MySiceInfoCert",
                X509KeyStorageFlags.MachineKeySet |
                X509KeyStorageFlags.PersistKeySet |
                X509KeyStorageFlags.Exportable);
        }
    }
}
