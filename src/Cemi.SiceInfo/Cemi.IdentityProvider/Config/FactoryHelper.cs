﻿using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Services;

namespace Cemi.IdentityProvider.Config
{
    public static class FactoryHelper
    {
        public static IdentityServerServiceFactory Configure(string connString)
        {
            var factory = new IdentityServerServiceFactory();
            /*
            var efConfig = new EntityFrameworkServiceOptions
            {
                ConnectionString = connString,
            };
            
            factory.RegisterConfigurationServices(efConfig);
            factory.RegisterOperationalServices(efConfig);

            factory.ConfigureClientStoreCache();
            factory.ConfigureScopeStoreCache();
            */
            factory.UseInMemoryClients(ClientHelper.Get());
            factory.UseInMemoryScopes(ScopeHelper.Get());

            //factory.UserService = new Registration<IUserService>(UserServiceFactory.CreateUserService(connString));
            factory.UseInMemoryUsers(UserHelper.Get());

            return factory;
        }
    }
}