﻿using System.Collections.Generic;
using IdentityServer3.Core.Models;

namespace Cemi.IdentityProvider.Config
{
    //[Obsolete("Now retrived by DB", true)]
    public static class ClientHelper
    {
        public static IEnumerable<Client> Get()
        {
            return new[]
            {
                //new Client
                //{
                //    ClientId = "identity_manager",
                //    ClientName = "Identity Manager",
                //    Enabled = true,
                //    RedirectUris = new List<string> {"https://localhost:44330/"},
                //    Flow = Flows.Implicit,
                //    RequireConsent = false,
                //    AllowedScopes = new List<string>
                //    {
                //        Constants.StandardScopes.OpenId,
                //        "identity_manager"
                //    }
                //},
                new Client
                {
                    Enabled = true,
                    ClientName = "SiceInfo Web",
                    ClientId = "siceinfoweb",
                    Flow = Flows.Implicit,

                    RedirectUris = new List<string>
                    {
                        "http://localhost:33000",
                        "http://ww3.cassaedilemilano.it"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:33000",
                        "http://ww3.cassaedilemilano.it"
                    },

                    AllowAccessToAllScopes = true
                }
                /*
                new Client
                {
                    Enabled = true,
                    ClientName = "JS Client",
                    ClientId = "js",
                    Flow = Flows.Implicit,

                    RedirectUris = new List<string>
                    {
                        "http://localhost:56668/popup.html"
                    },

                    AllowedCorsOrigins = new List<string>
                    {
                        "http://localhost:56668"
                    },

                    AllowAccessToAllScopes = true
                },
                */
            };
        }
    }
}