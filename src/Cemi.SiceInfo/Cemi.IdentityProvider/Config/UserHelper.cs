﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityServer3.Core;
using IdentityServer3.Core.Services.InMemory;

namespace Cemi.IdentityProvider.Config
{
    public static class UserHelper
    {
        public static List<InMemoryUser> Get()
        {
            var users = new List<InMemoryUser>
            {
                new InMemoryUser
                {
                    Username = "alessio",
                    Password = "123",
                    Subject = "1",

                    Claims = new[]
                    {
                        new Claim(Constants.ClaimTypes.GivenName, "Alessio"),
                        new Claim(Constants.ClaimTypes.FamilyName, "Mosto"),
                        new Claim(Constants.ClaimTypes.Email, "alessio.mosto@zenatek.it"),
                        new Claim(Constants.ClaimTypes.Role, "pippo")
                    }
                }
            };

            var user = new InMemoryUser
            {
                Username = "marco",
                Password = "123",
                Subject = "2",

                Claims = new List<Claim>
                {
                    new Claim(Constants.ClaimTypes.GivenName, "Marco"),
                    new Claim(Constants.ClaimTypes.FamilyName, "Catalano"),
                    new Claim(Constants.ClaimTypes.Email, "marco.catalano@cassaedilemilano.it"),
                    new Claim(Constants.ClaimTypes.Role, "CantieriGestione")
                }
            };

            users.Add(user);
            

            return users;
        }
    }
}