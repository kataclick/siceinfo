﻿using System.Collections.Generic;
using IdentityServer3.Core;
using IdentityServer3.Core.Models;

namespace Cemi.IdentityProvider.Config
{
    //[Obsolete("Now retrived by DB", true)]
    public static class ScopeHelper
    {
        public static List<Scope> Get()
        {
            var scopes = new List<Scope>
            {
                new Scope
                {
                    Enabled = true,
                    Name = "roles",
                    Type = ScopeType.Identity,
                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim(Constants.ClaimTypes.Role),
                        //new ScopeClaim("ruolofc"),
                        //new ScopeClaim("segreteriafc")
                    }
                },
                new Scope
                {
                    Enabled = true,
                    Name = "siceinfoapi",
                    DisplayName = "SiceInfo API",
                    Type = ScopeType.Resource
                } 
                //new Scope
                //{
                //    Name = "identity_manager",
                //    DisplayName = "Identity Manager",
                //    Description = "Authorization for Identity Manager",
                //    Type = ScopeType.Identity,
                //    ShowInDiscoveryDocument = false,
                //    Claims = new List<ScopeClaim>
                //    {
                //        new ScopeClaim(Constants.ClaimTypes.Name),
                //        new ScopeClaim(Constants.ClaimTypes.Role)
                //    }
                //}
            };

            scopes.AddRange(StandardScopes.All);

            return scopes;
        }
    }
}