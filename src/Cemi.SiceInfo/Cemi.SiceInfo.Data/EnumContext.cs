// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.6

using Cemi.SiceInfo.Data.Mapping.Enum;
using Cemi.SiceInfo.Type.Domain.Enum;

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning


namespace Cemi.SiceInfo.Data
{


    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class EnumContext : System.Data.Entity.DbContext
    {
        public System.Data.Entity.DbSet<TipoCausaleRespintaVariazioneStatoImpresa> TipiCausaliRespintaVariazioneStatoImpresa { get; set; } // TipiCausaliRespintaVariazioneStatoImpresa
        public System.Data.Entity.DbSet<TipoStatoImpresa> TipiStatoImpresa { get; set; } // TipiStatoImpresa

        static EnumContext()
        {
            System.Data.Entity.Database.SetInitializer<EnumContext>(null);
        }

        public EnumContext()
            : base("Name=CEMI")
        {
            InitializePartial();
        }

        public EnumContext(string connectionString)
            : base(connectionString)
        {
            InitializePartial();
        }

        public EnumContext(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model)
            : base(connectionString, model)
        {
            InitializePartial();
        }

        public EnumContext(System.Data.Common.DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {
            InitializePartial();
        }

        public EnumContext(System.Data.Common.DbConnection existingConnection, System.Data.Entity.Infrastructure.DbCompiledModel model, bool contextOwnsConnection)
            : base(existingConnection, model, contextOwnsConnection)
        {
            InitializePartial();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public bool IsSqlParameterNull(System.Data.SqlClient.SqlParameter param)
        {
            var sqlValue = param.SqlValue;
            var nullableValue = sqlValue as System.Data.SqlTypes.INullable;
            if (nullableValue != null)
                return nullableValue.IsNull;
            return (sqlValue == null || sqlValue == System.DBNull.Value);
        }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new TipiCausaliRespintaVariazioneStatoImpresaMap());
            modelBuilder.Configurations.Add(new TipiStatoImpresaMap());

            OnModelCreatingPartial(modelBuilder);
        }

        public static System.Data.Entity.DbModelBuilder CreateModel(System.Data.Entity.DbModelBuilder modelBuilder, string schema)
        {
            modelBuilder.Configurations.Add(new TipiCausaliRespintaVariazioneStatoImpresaMap(schema));
            modelBuilder.Configurations.Add(new TipiStatoImpresaMap(schema));
            return modelBuilder;
        }

        partial void InitializePartial();
        partial void OnModelCreatingPartial(System.Data.Entity.DbModelBuilder modelBuilder);
    }
}
// </auto-generated>
