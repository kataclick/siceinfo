﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.SiceInfo.Data
{
    partial class SiceContext
    {
        partial void InitializePartial()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
    }
}
