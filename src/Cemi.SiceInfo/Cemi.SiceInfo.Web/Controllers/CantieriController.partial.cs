﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Filters.Cantieri;

namespace Cemi.SiceInfo.Web.Controllers
{
    public partial class CantieriController
    {
        /*
        [Route("Cantieri/GetAll")]
        public ActionResult GetAll()
        {
            var ret = GetJsonResponse(new CantieriFilter { Importo = 100000000 });

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        [Route("Cantieri/GetByFilter")]
        public ActionResult GetByFilter(string filterJson)
        {
            if (filterJson == null)
                return GetAll();

            var format = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

            CantieriFilter filter = JsonConvert.DeserializeObject<CantieriFilter>(filterJson, dateTimeConverter);

            var ret = GetJsonResponse(filter);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        private static List<object> GetJsonResponse(CantieriFilter filter)
        {
            CantieriBusiness biz = new CantieriBusiness();

            CantiereCollection listaCantieri = biz.GetCantieri(filter);

            List<object> ret = new List<object>();

            foreach (var cantiere in listaCantieri)
            {
                var descrizione = new StringBuilder();
                descrizione.Append(cantiere.Indirizzo);
                descrizione.Append(" ");
                descrizione.Append(cantiere.Civico);
                descrizione.Append("<br />");
                descrizione.Append(cantiere.Comune);
                descrizione.Append(" ");
                descrizione.Append(cantiere.Provincia);
                descrizione.Append("<br />");
                // Impresa appaltatrice
                if (cantiere.ImpresaAppaltatrice != null)
                    descrizione.Append(cantiere.ImpresaAppaltatrice.RagioneSociale);
                else if (cantiere.ImpresaAppaltatriceTrovata != null)
                    descrizione.Append(cantiere.ImpresaAppaltatriceTrovata);
                descrizione.Append("<br />");
                descrizione.Append("<a href=Ceservizi/Cantieri/CantieriSchedaCantiere.aspx?idCantiere=" +
                                   cantiere.IdCantiere.Value + ">Scheda cantiere</a>");

                //ret.Add(new { latitudine = cantiere.Latitudine, longitudine = cantiere.Longitudine, cantiere.IdCantiere });
                ret.Add(new
                {
                    latitudine = cantiere.Latitudine,
                    longitudine = cantiere.Longitudine,
                    idCantiere = cantiere.IdCantiere,
                    descrizione = descrizione.ToString(),
                    impresa = cantiere.NomeImpresa,
                    provincia = cantiere.Provincia,
                    comune = cantiere.Comune,
                    indirizzo = cantiere.Indirizzo,
                    cap = cantiere.Cap,
                    codFiscImpresa = cantiere.ImpresaAppaltatrice?.CodiceFiscale,
                    idImpresa = cantiere.ImpresaAppaltatrice?.IdImpresa
                });
            }

            return ret;
        }
        */
    }
}