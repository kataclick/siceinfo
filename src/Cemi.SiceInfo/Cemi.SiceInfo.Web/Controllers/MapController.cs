﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Cemi.SiceInfo.Web.Controllers
{
    public class MapController : Controller
    {
        public ActionResult Index(string latitudine, string longitudine, string cap, string indirizzo, string provincia, string comune, string note)
        {
            //latitudine="+ latitudine +"&longitudine="+ longitudine +"&cap="+ cap +"&indirizzo="+ indirizzo +"&provincia="+ provincia +"&comune="+ comune +"&note

            var descrizione = new StringBuilder();
            // Indirizzo del cantiere
            descrizione.Append(indirizzo);
            descrizione.Append(" ");
            descrizione.Append(comune);
            descrizione.Append(" ");
            if (!string.IsNullOrEmpty(provincia))
            {
                descrizione.Append(" (");
                descrizione.Append(provincia);
                descrizione.Append(")");
            }

            // Informazioni aggiuntive
            if (!String.IsNullOrEmpty(note))
            {
                descrizione.Append(" ");
                descrizione.Append(" -  ");
                descrizione.Append(note);
            }

            ViewBag.Latitudine = latitudine;
            ViewBag.Longitudine = longitudine;
            ViewBag.Descrizione = descrizione.ToString();

            return View();
        }
    }
}
