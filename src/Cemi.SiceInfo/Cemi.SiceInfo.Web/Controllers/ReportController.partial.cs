﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using Cemi.SiceInfo.Business;

namespace Cemi.SiceInfo.Web.Controllers
{
    public partial class ReportController
    {
        private readonly IReportManager _reportManager;

        public ReportController(IReportManager reportManager)
        {
            _reportManager = reportManager;
        }

        public FileResult GetReportToPdf(string reportName, string paramKey1, string paramValue1, string paramKey2,
            string paramValue2, string paramKey3, string paramValue3, string paramKey4, string paramValue4, string paramKey5, string paramValue5)
        {
            Dictionary<string, object> reportParameters = null;

            if (!string.IsNullOrEmpty(paramKey1) && !string.IsNullOrEmpty(paramValue1))
            {
                reportParameters = new Dictionary<string, object>();
                reportParameters.Add(paramKey1, paramValue1);

                if (!string.IsNullOrEmpty(paramKey2) && !string.IsNullOrEmpty(paramValue2))
                {
                    reportParameters.Add(paramKey2, paramValue2);
                }

                if (!string.IsNullOrEmpty(paramKey3) && !string.IsNullOrEmpty(paramValue3))
                {
                    reportParameters.Add(paramKey3, paramValue3);
                }

                if (!string.IsNullOrEmpty(paramKey4) && !string.IsNullOrEmpty(paramValue4))
                {
                    reportParameters.Add(paramKey4, paramValue4);
                }

                if (!string.IsNullOrEmpty(paramKey5) && !string.IsNullOrEmpty(paramValue5))
                {
                    reportParameters.Add(paramKey5, paramValue5);
                }
            }

            byte[] pdfByte = _reportManager.GetReportPdf(reportName, reportParameters);

            return File(pdfByte, MediaTypeNames.Application.Pdf, $"{reportName}.pdf");
        }
    }
}