﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cemi.SiceInfo.Web.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            //return View("GenericError");
            return Redirect("~/CeServizi/DefaultErrore.aspx");
        }

        //public ActionResult Unauthorized()
        //{
        //    return View("Forbidden");
        //}

        public ActionResult NotFound()
        {
            //return View("NotFound");
            return Redirect("~/CeServizi/NotFound.aspx");
        }

        //public ActionResult InternalError()
        //{
        //    return View("GenericError");
        //}
    }
}
