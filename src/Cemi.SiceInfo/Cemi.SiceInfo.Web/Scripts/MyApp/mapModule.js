﻿var mapModule = (function() {

    return {
        googleKey: "AIzaSyDZE-OtiBzwc4ff4-BG9RDm1YFCN_heZIw",

        map: {},

        defaultPercHeighSize: 0.50,

        init: function (mapDiv) {
            var latitudine = 45.464044;
            var longitudine = 9.191567;
            var zoom = 10;
            var uluru = { lat: latitudine, lng: longitudine };
            mapModule.map = new google.maps.Map(document.getElementById(mapDiv), {
                zoom: zoom,
                center: uluru
            });
        },

        addMarker: function (latitudine, longitudine) {
            var uluru = { lat: latitudine, lng: longitudine };
            var marker = new google.maps.Marker({
                position: uluru,
                map: mapModule.map
            });
        },

        addMarkerWithInfo: function(latitudine, longitudine, descrizione) {
            var infowindow = new google.maps.InfoWindow({
                content: descrizione
            });

            var uluru = { lat: latitudine, lng: longitudine };
            var marker = new google.maps.Marker({
                position: uluru,
                map: mapModule.map
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
        }
    };
})();