﻿var commonModule = (function() {

    return {
        webApiBaseUrl: "baseUrl/api/",

        appBaseUrl: "appBaseUrl",

        defaultPageSize: 15,

        blockUI: function() {
            $.blockUI({
                css: {
                    border: "none",
                    padding: "15px",
                    backgroundColor: "#000",
                    '-webkit-border-radius': "10px",
                    '-moz-border-radius': "10px",
                    opacity: .5,
                    color: "#fff"
                },
                message: "<h1>Attendere prego...</h1>",
                baseZ: 20000
            });
        },

        unblockUI: function() {
            $.unblockUI();
        },

        setDefaultKendoBehavior: function() {
            $(".myDatepicker")
                .kendoDatePicker({
                    //format: "dd/MM/yyyy"
                
                });
            $(".myTabstrip")
                .kendoTabStrip({
                    animation: {
                        open: {
                            effects: "fadeIn"
                        }
                    }
                });
            $(".myPanelbar")
                .kendoPanelBar({
                    animation: {
                        open: { effects: "fadeIn" }
                    },
                    expandMode: "multiple"
                });
            $(".myTextbox.numeric").kendoNumericTextBox({});
            $(".myTextbox.numericNoDecimal")
                .kendoNumericTextBox({
                    format: "n0",
                    decimals: 0
                });
            $(".myTextbox.cap")
                .kendoNumericTextBox({
                    format: "#",
                    decimals: 0,
                    minlength: 5,
                    maxlength: 5,
                    step: 1
                });
            $(".myButton").kendoButton({});

            //wire focus of all numerictextbox widgets on the page
            $("input[type=text]").bind("focus",
                function() {
                    var input = $(this);
                    clearTimeout(input.data("selectTimeId")); //stop started time out if any

                    var selectTimeId = setTimeout(function() {
                        input.select();
                    });

                    input.data("selectTimeId", selectTimeId);
                }).blur(function(e) {
                clearTimeout($(this).data("selectTimeId")); //stop started timeout
            });
        },

        loadDropDownByUrl: function(idControl, valueField, textField, urlJson, readOnly, defaultIndex) {
            $("#" + idControl)
                .kendoDropDownList({
                    dataTextField: textField,
                    dataValueField: valueField,
                    dataSource: {
                        transport: {
                            read: {
                                dataType: "json",
                                url: urlJson
                            }
                        }
                    },
                    index: (defaultIndex == undefined ? -1 : defaultIndex)
                });

            //if (readOnly != undefined) {
            //    if (readOnly) {
            //        var control = $("#" + idControl).data("kendoDropDownList");
            //        control.input.attr("readonly", true)
            //                .on("keydown", function (e) {
            //                    if (e.keyCode === 8) {
            //                        e.preventDefault();
            //                    }
            //                });
            //    }
            //}
            if (readOnly != undefined) {
                if (readOnly) {
                    var control = $("#" + idControl).data("kendoDropDownList");
                    control.readonly();
                }
            }

        },

        loadDropDownByArray: function(idControl, valueField, textField, placeholder, values, readOnly, defaultIndex) {

            $("#" + idControl)
                .kendoDropDownList({
                    dataTextField: textField,
                    dataValueField: valueField,
                    placeholder: placeholder,
                    dataSource: values,
                    index: (defaultIndex == undefined ? 0 : defaultIndex)
                });

            if (readOnly != undefined) {
                if (readOnly) {
                    var control = $("#" + idControl).data("kendoDropDownList");
                    control.readonly();
                }
            }

        },

        loadComboBoxByUrl: function(idControl, valueField, textField, urlJson, readOnly, defaultIndex) {

            $("#" + idControl)
                .kendoComboBox({
                    dataTextField: textField,
                    dataValueField: valueField,
                    dataSource: {
                        transport: {
                            read: {
                                dataType: "json",
                                url: urlJson
                            }
                        }
                    },
                    index: (defaultIndex == undefined ? -1 : defaultIndex)
                });

            if (readOnly != undefined) {
                if (readOnly) {
                    var control = $("#" + idControl).data("kendoComboBox");
                    control.input.attr("readonly", true)
                        .on("keydown",
                            function(e) {
                                if (e.keyCode === 8) {
                                    e.preventDefault();
                                }
                            });
                }
            }

        },

        loadComboBoxByUrlFilter: function(idControl, valueField, textField, urlJson, readOnly, defaultIndex) {
            function comboboxFiltering(e) {
                //get filter descriptor
                var filter = e.filter;

                // handle the event
            }

            $("#" + idControl)
                .kendoComboBox({
                    dataTextField: textField,
                    dataValueField: valueField,
                    filter: "contains",
                    dataSource: {
                        transport: {
                            read: {
                                dataType: "json",
                                url: urlJson
                            }
                        }
                    },
                    index: (defaultIndex == undefined ? -1 : defaultIndex)
                });
            var combobox = $("#" + idControl).data("kendoComboBox");
            combobox.bind("filtering", comboboxFiltering);

            if (readOnly != undefined) {
                if (readOnly) {
                    var control = $("#" + idControl).data("kendoComboBox");
                    control.input.attr("readonly", true)
                        .on("keydown",
                            function(e) {
                                if (e.keyCode === 8) {
                                    e.preventDefault();
                                }
                            });
                }
            }

        },

        loadComboBoxByArray: function(idControl, valueField, textField, placeholder, values, readOnly, defaultIndex) {

            $("#" + idControl)
                .kendoComboBox({
                    dataTextField: textField,
                    dataValueField: valueField,
                    placeholder: placeholder,
                    dataSource: values,
                    index: (defaultIndex == undefined ? -1 : defaultIndex)
                });

            if (readOnly != undefined) {
                if (readOnly) {
                    var control = $("#" + idControl).data("kendoComboBox");
                    control.input.attr("readonly", true)
                        .on("keydown",
                            function(e) {
                                if (e.keyCode === 8) {
                                    e.preventDefault();
                                }
                            });
                }
            }

        },

        configureWindow: function(idControl, title, width, resizable, pinned) {

            if (title == undefined)
                title = "";
            if (width == undefined)
                width = "600px";
            if (resizable == undefined)
                resizable = false;
            if (pinned == undefined)
                pinned = true;

            $("#" + idControl)
                .kendoWindow({
                    animation: false,
                    width: width,
                    visible: false,
                    modal: true,
                    pinned: pinned,
                    resizable: resizable,
                    title: title
                })
                .data("kendoWindow");

        },

        openKendoWindow: function(windowId) {
            var win = $("#" + windowId).data("kendoWindow");
            win.center().open();
        },

        closeKendoWindow: function(windowId) {
            var win = $("#" + windowId).data("kendoWindow");
            win.center().close();
        },

        updateGridDataSourceWithPaging: function (idControl, urlJson, pageSize) {
            $("#" + idControl).data("kendoGrid").setDataSource(new kendo.data.DataSource({
                transport: {
                    read: {
                        dataType: "json",
                        url: urlJson
                    }
                },
                pageSize: pageSize
            }));
        },
    };
})();