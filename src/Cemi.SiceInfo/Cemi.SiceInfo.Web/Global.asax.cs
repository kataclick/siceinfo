﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace Cemi.SiceInfo.Web
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // Register your types, for instance:
            InitializeContainer(container);

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();

            //Glimpse.SimpleInjector.SimpleInjectorTab.ConfigureGlimpseAndVerifyContainer(container);
            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static void InitializeContainer(Container container)
        {
            string name = string.Empty;
            if (ConfigurationManager.GetSection("MyDIConfig") is NameValueCollection diConfig)
            {
                name = diConfig["ImplementationAssemblyList"];
            }
            List<string> listName = name.Split(';').ToList();
            foreach (string n in listName)
            {
                List<string> assemblyInfo = n.Split('|').ToList();

                string assemblyName = assemblyInfo[0];

                var impAssembly = Assembly.Load(assemblyName);

                if (impAssembly != null)
                {
                    if (assemblyInfo.Count == 1)
                    {
                        var registrations =
                            from type in impAssembly.GetExportedTypes()
                            where type.GetInterfaces().Any()
                            select new { Services = type.GetInterfaces().ToList(), Implementation = type };

                        foreach (var reg in registrations)
                        {
                            foreach (var serv in reg.Services)
                            {
                                container.Register(serv, reg.Implementation, Lifestyle.Scoped);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i < assemblyInfo.Count; i++)
                        {
                            string className = assemblyInfo[i];
                            //string fullName = $"{assemblyName}.{className}";

                            var registrations =
                            from type in impAssembly.GetExportedTypes()
                            where type.Name == className && type.GetInterfaces().Any()
                            select new { Services = type.GetInterfaces().ToList(), Implementation = type };

                            foreach (var reg in registrations)
                            {
                                foreach (var serv in reg.Services)
                                {
                                    if (serv.IsGenericType)
                                    {
                                        container.RegisterConditional(serv.GetGenericTypeDefinition(), reg.Implementation.GetGenericTypeDefinition(), Lifestyle.Scoped, c => !c.Handled);
                                    }
                                    else
                                    {
                                        container.Register(serv, reg.Implementation, Lifestyle.Scoped);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}