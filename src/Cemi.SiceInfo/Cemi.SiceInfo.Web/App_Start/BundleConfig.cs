﻿﻿using System.Web.Optimization; 
 
namespace Cemi.SiceInfo.Web 
{ 
    public class BundleConfig 
    { 
        public static void RegisterBundles(BundleCollection bundles) 
        { 
            bundles.Add(new StyleBundle("~/styleBundle/myStyle") 
                .Include("~/Content/bootstrap.min.css", new CssRewriteUrlTransform()) 
                .Include("~/Content/Inspinia/animate.css", new CssRewriteUrlTransform()) 
                .Include("~/Content/Inspinia/inspinia.css", new CssRewriteUrlTransform()) 
                .Include("~/Content/font-awesome.min.css", new CssRewriteUrlTransform()) 
                .Include("~/Content/KendoUI/kendo.common.min.css", new CssRewriteUrlTransform()) 
                .Include("~/Content/KendoUI/kendo.silver.min.css", new CssRewriteUrlTransform()) 
                .Include("~/Content/MyApp/style.css", new CssRewriteUrlTransform())); 
 
 
            bundles.Add(new ScriptBundle("~/scriptBundle/jquery").Include( 
                "~/Scripts/jquery-{version}.js", 
                "~/Scripts/jquery.blockUI.js", 
                "~/Scripts/typewatch.js")); 
 
            bundles.Add(new ScriptBundle("~/scriptBundle/bootstrap").Include( 
                "~/Scripts/bootstrap.js")); 
 
            bundles.Add(new ScriptBundle("~/scriptBundle/inspinia").Include( 
                "~/Scripts/Inspinia/Plugins/MetisMenu/metisMenu.min.js", 
                "~/Scripts/Inspinia/Plugins/Pace/pace.min.js", 
                "~/Scripts/Inspinia/Plugins/SlimScroll/jquery.slimscroll.min.js", 
                "~/Scripts/Inspinia/inspinia.js")); 
 
            bundles.Add(new ScriptBundle("~/scriptBundle/kendo").Include( 
                "~/Scripts/KendoUI/kendo.all.min.js", 
                "~/Scripts/KendoUI/cultures/kendo.culture.it-IT.min.js", 
                "~/Scripts/KendoUI/messages/kendo.messages.it-IT.min.js", 
                "~/Scripts/KendoUI/pako_deflate.min.js", 
                "~/Scripts/KendoUI/jszip.min.js")); 
 
 
            bundles.Add(new ScriptBundle("~/scriptBundle/commonModule").Include( 
                "~/Scripts/MyApp/commonModule.js"));

            bundles.Add(new ScriptBundle("~/scriptBundle/mapModule").Include(
                "~/Scripts/MyApp/mapModule.js"));
        } 
 
    } 
}