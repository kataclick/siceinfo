using System.Collections;

/// <summary>
/// Summary description for PagineModificate
/// </summary>
public class PagineModificate
{
    public Hashtable hc = new Hashtable();
    public int numpagina;

    public PagineModificate(int numpagina)
    {
        this.numpagina = numpagina;
    }

    public void modificaTupla(string identificativo, string valore)
    {
        if (hc[identificativo] == null)
        {
            hc.Add(identificativo, valore);
        }
        else
        {
            hc.Remove(identificativo);
            hc.Add(identificativo, valore);
        }
    }

    public bool haveIn(string idTupla)
    {
        if (hc[idTupla] == null)
            return false;
        return true;
    }

    public bool getValue(string idTupla)
    {
        return bool.Parse(((string) hc[idTupla]));
    }
}