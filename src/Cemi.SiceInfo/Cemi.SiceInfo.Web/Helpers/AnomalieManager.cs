﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Cemi.SiceInfo.Web.Helpers
{
    public class AnomalieManager
    {
        private readonly Database _databaseCemi;

        public AnomalieManager()
        {
            _databaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public bool RegistraAnomalia(string descrizione, int idUtente, string loginUtente)
        {
            bool ret = false;

            try
            {
                DbCommand dbCommand = _databaseCemi.GetStoredProcCommand("dbo.USP_AnomalieImpreseSegnalateInsert");
                _databaseCemi.AddInParameter(dbCommand, "@descrizione", DbType.String, descrizione);
                _databaseCemi.AddInParameter(dbCommand, "@idUtente", DbType.Int32, idUtente);
                _databaseCemi.AddInParameter(dbCommand, "@loginUtente", DbType.String, loginUtente);

                int id = (int)_databaseCemi.ExecuteScalar(dbCommand);
                if (id > 0)
                    ret = true;
            }
            catch
            {
            }

            return ret;
        }

        public DataSet CaricaAnomalie(DateTime periodoDa, DateTime periodoA)
        {
            DataSet ret = null;

            try
            {
                DbCommand dbCommand = _databaseCemi.GetStoredProcCommand("dbo.USP_AnomalieImpreseSegnalateSelectByDate");
                _databaseCemi.AddInParameter(dbCommand, "@dataDa", DbType.DateTime, periodoDa);
                _databaseCemi.AddInParameter(dbCommand, "@dataA", DbType.DateTime, periodoA);

                ret = _databaseCemi.ExecuteDataSet(dbCommand);
            }
            catch
            {
            }

            return ret;
        }
    }
}