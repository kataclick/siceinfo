﻿using System;
using System.Web.Mvc;

namespace Cemi.SiceInfo.Web.Helpers
{
    public static class HtmlHelperExtensions
    {
        public static string IsSelected(this HtmlHelper html, string action = null, string controller = null, string cssClass = null)
        {
            if (string.IsNullOrEmpty(cssClass))
                cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];
            if (string.IsNullOrEmpty(controller))
                controller = currentController;
            if (string.IsNullOrEmpty(action))
                action = currentAction;
            return string.Equals(controller, currentController, StringComparison.CurrentCultureIgnoreCase) &&
                   string.Equals(action, currentAction, StringComparison.CurrentCultureIgnoreCase)
                ? cssClass
                : string.Empty;
        }
        public static string PageClass(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }
    }
}