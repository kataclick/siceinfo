using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.Helpers
{
    public class Presenter
    {
        public static bool IsSviluppo
        {
            get
            {
                bool svil = false;

                if (ConfigurationManager.AppSettings["Sviluppo"] != null)
                {
                    if (!bool.TryParse(ConfigurationManager.AppSettings["Sviluppo"], out svil))
                    {
                        svil = false;
                    }
                }

                return svil;
            }
        }

        #region Metodi per il DataBind

        public static void CaricaElementiInDropDownConElementoVuoto(
            DropDownList dropDown,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.Items.Add(new ListItem(string.Empty, string.Empty));
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInDropDown(
            DropDownList dropDown,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInDropDown(
            DropDownList dropDown,
            DataTable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInGridView(
            GridView gridView,
            IEnumerable lista,
            Int32 pagina)
        {
            gridView.PageIndex = pagina;
            gridView.DataSource = lista;
            gridView.DataBind();
        }

        public static void CaricaElementiInListView(
            ListView listView,
            IEnumerable lista)
        {
            listView.DataSource = lista;
            listView.DataBind();
        }

        public static void CaricaElementiInCheckBoxList(
            CheckBoxList checkBoxList,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            checkBoxList.DataSource = lista;
            checkBoxList.DataTextField = nomeCampoTesto;
            checkBoxList.DataValueField = nomeCampoValore;
            checkBoxList.DataBind();
        }

        public static void CaricaElementiInBulletedList(
            BulletedList bulletedList,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            bulletedList.DataSource = lista;
            bulletedList.DataTextField = nomeCampoTesto;
            bulletedList.DataValueField = nomeCampoValore;
            bulletedList.DataBind();
        }

        public static void CaricaElementiInRadioButtonList(
            RadioButtonList radioButtonList,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore
            )
        {
            radioButtonList.DataSource = lista;
            radioButtonList.DataTextField = nomeCampoTesto;
            radioButtonList.DataValueField = nomeCampoValore;
            radioButtonList.DataBind();
        }

        public static void CaricaElementiInListBox(
            ListBox listBox,
            IEnumerable lista,
            string nomeCampoTesto,
            string nomeCampoValore)
        {
            listBox.DataSource = lista;
            listBox.DataTextField = nomeCampoTesto;
            listBox.DataValueField = nomeCampoValore;
            listBox.DataBind();
        }

        #endregion

        #region Metodi per il DataBind dei controlli Telerik

        public static void CaricaElementiInDropDown(
            RadComboBox dropDown,
            IEnumerable lista,
            String nomeCampoTesto,
            String nomeCampoValore)
        {
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInDropDownConElementoVuoto(
            RadComboBox dropDown,
            IEnumerable lista,
            String nomeCampoTesto,
            String nomeCampoValore)
        {
            dropDown.Items.Clear();
            dropDown.Items.Add(new RadComboBoxItem(string.Empty, string.Empty));
            dropDown.DataSource = lista;
            if (!string.IsNullOrEmpty(nomeCampoTesto))
                dropDown.DataTextField = nomeCampoTesto;
            if (!string.IsNullOrEmpty(nomeCampoValore))
                dropDown.DataValueField = nomeCampoValore;
            dropDown.DataBind();
        }

        public static void CaricaElementiInGridView(
            RadGrid gridView,
            IEnumerable lista)
        {
            gridView.DataSource = lista;
            gridView.DataBind();
        }

        public static void CaricaElementiInGridView(
            RadGrid gridView,
            DataTable tabella)
        {
            gridView.DataSource = tabella;
            gridView.DataBind();
        }

        public static void CaricaElementiInListView(
            RadListView listView,
            IEnumerable lista)
        {
            listView.DataSource = lista;
            listView.DataBind();
        }

        #endregion

        #region Metodi per recuperare i dati dall'interfaccia

        public static string NormalizzaCampoTesto(string campo)
        {
            return Cemi.SiceInfo.Utility.StringHelper.NormalizzaCampoTesto(campo);
        }

        public static String NormalizzaCampoTestoSoloLettere(string campo)
        {
            return Cemi.SiceInfo.Utility.StringHelper.NormalizzaCampoTestoSoloLettere(campo);
        }

        public static void SvuotaCampo(TextBox textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(RadTextBox textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(RadNumericTextBox textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(RadDateInput textBox)
        {
            textBox.Text = null;
        }

        public static void SvuotaCampo(Label label)
        {
            label.Text = null;
        }

        #endregion

        #region Metodi per mandare stringhe in output

        public static String StampaValoreMonetario(Decimal? valore)
        {
            return Cemi.SiceInfo.Utility.StringHelper.StampaValoreMonetario(valore);
        }

        public static String StampaDataFormattataClassica(DateTime? data)
        {
            return Cemi.SiceInfo.Utility.StringHelper.StampaDataFormattataClassica(data);
        }

        public static String StampaStringaExcel(String valore)
        {
            return Cemi.SiceInfo.Utility.StringHelper.StampaStringaExcel(valore);
        }

        #endregion

        #region Restituzione immagini

        public static void RestituisciFileArchidoc(string idArchidoc, byte[] file, Page page)
        {
            //Set the appropriate ContentType.
            page.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.pdf", idArchidoc));
            page.Response.ContentType = "application/pdf";
            //Write the file directly to the HTTP content output stream.
            page.Response.BinaryWrite(file);
            page.Response.Flush();
            page.Response.End();
        }

        public static void RestituisciFileArchidoc(string idArchidoc, byte[] file, string estensioneFile, Page page)
        {
            //Set the appropriate ContentType.
            page.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.{1}", idArchidoc, estensioneFile));
            //page.Response.ContentType = "application/pdf";
            switch (estensioneFile)
            {
                case ".XML":
                    page.Response.ContentType = "text/plain";
                    break;
                case ".PDF":
                    page.Response.ContentType = "application/pdf";
                    break;
                case ".TIF":
                    page.Response.ContentType = "image/tiff";
                    break;
                default:
                    page.Response.ContentType = "application/download";
                    break;

            }
            //Write the file directly to the HTTP content output stream.
            page.Response.BinaryWrite(file);
            page.Response.Flush();
            page.Response.End();
        }

        #endregion

        #region Metodi per il caricamento delle province e comuni nelle DropDown

        public static void CaricaProvince(DropDownList dropDownProvince)
        {
            Common commonBiz = new Common();
            CaricaElementiInDropDownConElementoVuoto(
                dropDownProvince,
                commonBiz.GetProvinceSiceNew(),
                "",
                "");
        }

        public static void CaricaProvince(RadComboBox dropDownProvince)
        {
            Common commonBiz = new Common();
            CaricaElementiInDropDown(
                dropDownProvince,
                commonBiz.GetProvinceSiceNew(),
                "",
                "");
        }

        public static void CaricaProvinceConElementoVuoto(RadComboBox dropDownProvince)
        {
            Common commonBiz = new Common();
            CaricaElementiInDropDownConElementoVuoto(
                dropDownProvince,
                commonBiz.GetProvinceSiceNew(),
                "",
                "");
        }

        public static void CaricaComuni(DropDownList dropDownComuni, String provincia)
        {
            if (!String.IsNullOrEmpty(provincia))
            {
                Common commonBiz = new Common();

                CaricaElementiInDropDownConElementoVuoto(
                    dropDownComuni,
                    commonBiz.GetComuniSiceNew(provincia),
                    "Comune",
                    "Comune");
            }
            else
            {
                dropDownComuni.Items.Clear();
            }
        }

        public static void CaricaComuniCodiceCatastale(DropDownList dropDownComuni, String provincia)
        {
            if (!String.IsNullOrEmpty(provincia))
            {
                Common commonBiz = new Common();

                CaricaElementiInDropDownConElementoVuoto(
                    dropDownComuni,
                    commonBiz.GetComuniSiceNew(provincia),
                    "Comune",
                    "CodiceCatastale");
            }
            else
            {
                dropDownComuni.Items.Clear();
            }
        }

        public static void CaricaComuni(RadComboBox dropDownComuni, String provincia)
        {
            if (!String.IsNullOrEmpty(provincia))
            {
                Common commonBiz = new Common();

                CaricaElementiInDropDown(
                    dropDownComuni,
                    commonBiz.GetComuniSiceNew(provincia),
                    "Comune",
                    "CodiceCatastale");
            }
            else
            {
                dropDownComuni.Items.Clear();
            }
        }

        public static void CaricaComuniCodiceCatastale(RadComboBox dropDownComuni, String provincia)
        {
            if (!String.IsNullOrEmpty(provincia))
            {
                Common commonBiz = new Common();

                CaricaElementiInDropDownConElementoVuoto(
                    dropDownComuni,
                    commonBiz.GetComuniSiceNew(provincia),
                    "Comune",
                    "CodiceCatastale");
            }
            else
            {
                dropDownComuni.Items.Clear();
            }
        }

        #endregion
    }
}