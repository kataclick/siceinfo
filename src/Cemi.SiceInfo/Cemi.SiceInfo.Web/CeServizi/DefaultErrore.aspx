﻿<%@ Page Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="DefaultErrore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.DefaultErrore" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Errore" sottoTitolo="Informazioni errore" runat="server" />
    <br />
    Errore generico. Riprovare più tardi
</asp:Content>