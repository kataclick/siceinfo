﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.RendicontiImprese
{
    public partial class OreAccantonateDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoImpresa);
            ConsulenteSelezioneImpresa1.OnImpresaSelected += new TBridge.Cemi.Type.Delegates.ImpresaSelectedEventHandler(ConsulenteSelezioneImpresa1_OnImpresaSelected);

            //if (!Page.IsPostBack)
            //{
            if (!TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            {
                ConsulenteSelezioneImpresa1.Visible = false;
            }
            else
            {
                //ButtonGeneraPerConsulente.Visible = true;

                Int32 idImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
                if (idImpresa > 0)
                {
                    ViewState["IdImpresa"] = idImpresa;
                    MenuOreAccantonate1.SetIdImpresa(idImpresa);
                    //ButtonGeneraPerConsulente.Enabled = true;
                }
                else
                {
                    //ButtonGeneraPerConsulente.Enabled = false;
                }
            }
            //}
        }

        void ConsulenteSelezioneImpresa1_OnImpresaSelected(Int32 idImpresa, String codiceRagioneSociale)
        {
            ViewState["IdImpresa"] = idImpresa;
            MenuOreAccantonate1.SetIdImpresa(idImpresa);
            ButtonGeneraPerConsulente.Enabled = true;
        }

        protected void ButtonGeneraPerConsulente_Click(object sender, EventArgs e)
        {
            if (ViewState["IdImpresa"] != null)
            {
                Context.Items["IdImpresa"] = ViewState["IdImpresa"];
                Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=evr");
            }
        }
        /*        protected void Page_Load(object sender, EventArgs e)
                {

                }

                public void SetIdImpresa(Int32 idImpresa)
                {
                    ViewState["IdImpresa"] = idImpresa;
                }

                protected void A3_Click(object sender, EventArgs e)
                {
                    if (GestioneUtentiBiz.IsConsulente())
                    {
                        if (ViewState["IdImpresa"] != null)
                        {
                            Context.Items["IdImpresa"] = ViewState["IdImpresa"];
                            Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=evr");
                        }
                    }
                    else
                    {
                        Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=evr");
                    }
                }
           */
    }
}