﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="OreAccantonateDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.RendicontiImprese.OreAccantonateDefault" %>

<%@ Register Src="../WebControls/MenuOreAccantonate.ascx" TagName="MenuOreAccantonate" TagPrefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="../WebControls/ConsulenteSelezioneImpresa.ascx" tagname="ConsulenteSelezioneImpresa" tagprefix="uc3" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuOreAccantonate ID="MenuOreAccantonate1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Rendiconti" sottoTitolo="Ore accantonate" />
    <br />
    <uc3:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
    <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
        {
    %>
    <p>
        <b>Per periodo</b>
		<br />Per verificare le ore denunciate e pagate riferite ad uno specifico periodo.</p>
		<br />
		<p><b>Per lavoratore</b>
		<br />Per consultare le ore denunciate e pagate per ogni singolo dipendente dichiarato presso il nostro Ente.</p>
		<br />
    <%
        }
    %>
	<p style="text-align:justify;"><b>Elemento Variazione per la Retribuzione (EVR)</b>
	<br />Per salvare e stampare l’estratto conto relativo alle ore denunciate nel triennio di riferimento 
	(al netto delle ore di Cassa Integrazione Guadagni) al fine di poter valutare se l’Elemento Variabile della Retribuzione (EVR) 
	debba essere corrisposto ai propri lavoratori nella misura intera o ridotta, così come previsto dagli accordi nazionali e territoriali vigenti.
	</p>
	<!-- <p style="text-align:justify;">
	<b>EVR: misura definitiva per l’anno 2014</b><br />
	In data 16 ottobre 2014 le Parti Sociali territoriali, Assimpredil Ance e le Organizzazioni sindacali dei lavoratori FeNEAL-UIL, FILCA CISL, FILLEA CGIL delle Province di 
	Milano, Lodi, Monza e Brianza, hanno sottoscritto un verbale di accordo di verifica dell’Elemento Variabile della Retribuzione (EVR) per l’anno in corso.<br />
	<b>Il valore dell’EVR individuato per l’anno 2014, pari al 37,50%</b> della misura stabilita all’articolo 3, comma 3, parte operai, del Contratto Collettivo Provinciale di Lavoro 
	del 22 dicembre 2011 (ovvero 6% dei minimi tabellari in vigore alla data del 1° gennaio 2010), <b>deve essere riconosciuto per intero</b> anche qualora la valutazione dei 
	parametri a livello aziendale (ore denunciate in Cassa Edile, al netto delle ore di Cassa Integrazione Guadagni, e volume di affari IVA) comporti un risultato negativo. 
	Di conseguenza, per l’anno in corso, è in ogni caso esclusa l’applicabilità dell’articolo 3, comma 8, parte operai, del Contratto Collettivo Provinciale di Lavoro del 22 dicembre 2011, 
	che prevede l’onere della trasmissione alla Cassa Edile e ad Assimpredil Ance dell’autodichiarazione allegata al medesimo articolo 3. <b>Cassa Edile non metterà, pertanto, a disposizione 
	l’estratto conto on-line relativo alle ore denunciate.</b><br />
	Per maggiori dettagli si rimanda alla notizia <b><i>"Elemento Variabile della Retribuzione (EVR): misura definitiva per l’anno 2014"</i></b> pubblicata nell’area istituzionale del sito web.

	</p> -->
    <asp:Button
        ID="ButtonGeneraPerConsulente"
        runat="server"
        Text="Genera EVR per l'impresa selezionata"
        Width="250px"
        Visible="false"
        Enabled="false" 
        onclick="ButtonGeneraPerConsulente_Click" />
</asp:Content>
