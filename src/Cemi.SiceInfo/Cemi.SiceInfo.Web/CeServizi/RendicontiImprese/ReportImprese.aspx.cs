﻿using System;
using System.Configuration;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.RendicontiImprese
{
    public partial class ReportImprese : System.Web.UI.Page
    {
        private readonly Common commonBiz = new Common();
        private Int32 idImpresa = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoImpresa);
            #endregion

            ConsulenteSelezioneImpresa1.OnImpresaSelected += new TBridge.Cemi.Type.Delegates.ImpresaSelectedEventHandler(ConsulenteSelezioneImpresa1_OnImpresaSelected);

            if (!Page.IsPostBack)
            {
                if (!GestioneUtentiBiz.IsConsulente())
                {
                    ConsulenteSelezioneImpresa1.Visible = false;
                }

                CaricaAnniTrattamentoCIGO();
                DropDownListAnno.SelectedValue = DateTime.Now.Year.ToString();

                CaricaReport();
            }
        }

        private void CaricaReport()
        {
            ReportViewerImprese.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            string tipoReport = Request.QueryString["tipo"];
            if (tipoReport == "orePeriodo")
            {
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportOrePeriodo";
                TitoloSottotitolo1.sottoTitolo = "Ore per periodo";
                MenuOreAccantonate1.Visible = true;
            }
            else if (tipoReport == "oreLavoratori")
            {
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportOreLavoratori";
                TitoloSottotitolo1.sottoTitolo = "Ore per lavoratori";
                MenuOreAccantonate1.Visible = true;
            }
            else if (tipoReport == "crediti")
            {
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportCrediti";
                TitoloSottotitolo1.sottoTitolo = "Crediti";
            }
            else if (tipoReport == "rapportiLavoro")
            {
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportRapportiLavoro";
                TitoloSottotitolo1.sottoTitolo = "Rapporti di Lavoro";
            }
            else if (tipoReport == "cartella")
            {
                TableLabelCartella.Visible = true;
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportCartella";
                TitoloSottotitolo1.sottoTitolo = "Riepilogo cartella";
                MenuRiepilogoPagamenti1.Visible = true;
            }
            else if (tipoReport == "premioFedelta")
            {
                TableLabelPremioFedelta.Visible = true;
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportPremioFedelta";
                TitoloSottotitolo1.sottoTitolo = "Premio fedeltà";
                MenuRiepilogoPagamenti1.Visible = true;
            }
            else if (tipoReport == "evr")
            {
                //TableLabelPremioFedelta.Visible = true;
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/EVR2012";
                TitoloSottotitolo1.sottoTitolo = "EVR";
                MenuOreAccantonate1.Visible = true;
            }
            else if (tipoReport == "trattamentoCIGO")
            {
                TableTrattamentoCIGO.Visible = true;
                TitoloSottotitolo1.sottoTitolo = "Prestazione sociale trattamento C.I.G.O.";
                MenuRiepilogoPagamenti1.Visible = true;
            }

            //int idImpresa = ((Impresa) (HttpContext.Current.User).Identity).Entity.IdImpresa;
            Int32 idImpresa;
            if (Context.Items["IdImpresa"] != null)
            {
                idImpresa = (Int32)Context.Items["IdImpresa"];
                ViewState["idImpresa"] = idImpresa;

                MenuRiepilogoPagamenti1.SetIdImpresa(idImpresa);
            }
            else
            {
                if (ViewState["idImpresa"] != null)
                {
                    idImpresa = (Int32)ViewState["idImpresa"];
                }
                else
                {
                    idImpresa = ((Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdImpresa;
                    ViewState["idImpresa"] = idImpresa;
                }
            }

            ReportParameter paramIdImpresa = new ReportParameter("idImpresa", idImpresa.ToString());
            ReportParameter[] listaParam = new ReportParameter[1];

            ReportViewerImprese.Visible = true;

            if (tipoReport == "cartella")
            {
                int? annoDaConsiderare = GetAnno();
                if (annoDaConsiderare.HasValue)
                {
                    string semestreDaConsiderare = GetSemestre();

                    //ReportViewerImprese.Visible = true;
                    TableLabelCartella.Visible = true;
                    TableLabelNoDati.Visible = false;

                    if (!String.IsNullOrEmpty(semestreDaConsiderare))
                    {
                        listaParam = new ReportParameter[3];
                        ReportParameter anno = new ReportParameter("anno", annoDaConsiderare.ToString());
                        ReportParameter semestre = new ReportParameter("semestre", semestreDaConsiderare);
                        listaParam[1] = anno;
                        listaParam[2] = semestre;
                    }
                }
                else
                {
                    ReportViewerImprese.Visible = false;
                    TableLabelCartella.Visible = false;
                    TableLabelNoDati.Visible = true;
                }
            }
            if (tipoReport == "premioFedelta")
            {
                string dataLiquidazioneDaConsiderare = commonBiz.GetDataPremioFedelta();

                //if (!String.IsNullOrEmpty(dataLiquidazioneDaConsiderare))
                DateTime dataLiquidazioneDate = DateTime.ParseExact(dataLiquidazioneDaConsiderare, "dd/M/yyyy", null);

                DateTime dateLimit = new DateTime(dataLiquidazioneDate.Year, 12, 31);
                
                // 04/01/2019 - Aggiunti 3 mesi per liquidazione posticipata - INIZIO
                dateLimit = dateLimit.AddMonths(3);
                // 04/01/2019 - Aggiunti 3 mesi per liquidazione posticipata - FINE

                if (DateTime.Now >= dataLiquidazioneDate && DateTime.Now <= dateLimit)
                {
                    listaParam = new ReportParameter[2];
                    ReportParameter dataLiquidazione = new ReportParameter("dataLiquidazione", dataLiquidazioneDaConsiderare);
                    listaParam[1] = dataLiquidazione;
                }
                else
                {
                    ReportViewerImprese.Visible = false;
                    TableLabelPremioFedelta.Visible = false;
                    TableLabelNoDatiPremioFedelta.Visible = true;
                }
            }

            if (tipoReport != "trattamentoCIGO")
            {
                listaParam[0] = paramIdImpresa;
                ReportViewerImprese.ServerReport.SetParameters(listaParam);
            }

            if (tipoReport == "evr")
            {
                //Export PDF

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = ReportViewerImprese.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", "attachment;filename=EVR.pdf");
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }

        void ConsulenteSelezioneImpresa1_OnImpresaSelected(int idImpresa, string codiceRagioneSociale)
        {
            ViewState["IdImpresa"] = idImpresa;
            MenuRiepilogoPagamenti1.SetIdImpresa(idImpresa);

            CaricaReport();
        }

        private void CaricaAnniTrattamentoCIGO()
        {
            Presenter.CaricaElementiInDropDown(
                DropDownListAnno,
                commonBiz.GetAnniTrattamentoCIGO(),
                "",
                "");
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoImpresa);
        }

        protected void ReportViewerImprese_Init(object sender, EventArgs e)
        {
            //ReportViewerImprese.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            //string tipoReport = Request.QueryString["tipo"];
            //if (tipoReport == "orePeriodo")
            //{
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportOrePeriodo";
            //    TitoloSottotitolo1.sottoTitolo = "Ore per periodo";
            //    MenuOreAccantonate1.Visible = true;
            //}
            //else if (tipoReport == "oreLavoratori")
            //{
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportOreLavoratori";
            //    TitoloSottotitolo1.sottoTitolo = "Ore per lavoratori";
            //    MenuOreAccantonate1.Visible = true;
            //}
            //else if (tipoReport == "crediti")
            //{
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportCrediti";
            //    TitoloSottotitolo1.sottoTitolo = "Crediti";
            //}
            //else if (tipoReport == "rapportiLavoro")
            //{
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportRapportiLavoro";
            //    TitoloSottotitolo1.sottoTitolo = "Rapporti di Lavoro";
            //}
            //else if (tipoReport == "cartella")
            //{
            //    TableLabelCartella.Visible = true;
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportCartella";
            //    TitoloSottotitolo1.sottoTitolo = "Riepilogo cartella";
            //    MenuRiepilogoPagamenti1.Visible = true;
            //}
            //else if (tipoReport == "premioFedelta")
            //{
            //    TableLabelPremioFedelta.Visible = true;
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportPremioFedelta";
            //    TitoloSottotitolo1.sottoTitolo = "Premio fedeltà";
            //    MenuRiepilogoPagamenti1.Visible = true;
            //}
            //else if (tipoReport == "evr")
            //{
            //    //TableLabelPremioFedelta.Visible = true;
            //    ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/EVR2012";
            //    TitoloSottotitolo1.sottoTitolo = "EVR";
            //    MenuOreAccantonate1.Visible = true;
            //}
            //else if (tipoReport == "trattamentoCIGO")
            //{
            //    TableTrattamentoCIGO.Visible = true;
            //    TitoloSottotitolo1.sottoTitolo = "Prestazione sociale trattamento C.I.G.O.";
            //    MenuRiepilogoPagamenti1.Visible = true;
            //}

            ////int idImpresa = ((Impresa) (HttpContext.Current.User).Identity).Entity.IdImpresa;
            //Int32 idImpresa;
            //if (Context.Items["IdImpresa"] != null)
            //{
            //    idImpresa = (Int32) Context.Items["IdImpresa"];
            //    ViewState["idImpresa"] = idImpresa;
            //}
            //else
            //{
            //    if (ViewState["idImpresa"] != null)
            //    {
            //        idImpresa = (Int32) ViewState["idImpresa"];
            //    }
            //    else
            //    {
            //        idImpresa = ((Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdImpresa;
            //        ViewState["idImpresa"] = idImpresa;
            //    }
            //}

            //ReportParameter paramIdImpresa = new ReportParameter("idImpresa", idImpresa.ToString());
            //ReportParameter[] listaParam = new ReportParameter[1];

            //ReportViewerImprese.Visible = true;

            //if (tipoReport == "cartella")
            //{
            //    int? annoDaConsiderare = GetAnno();
            //    if (annoDaConsiderare.HasValue)
            //    {
            //        string semestreDaConsiderare = GetSemestre();

            //        //ReportViewerImprese.Visible = true;
            //        TableLabelCartella.Visible = true;
            //        TableLabelNoDati.Visible = false;

            //        if (!String.IsNullOrEmpty(semestreDaConsiderare))
            //        {
            //            listaParam = new ReportParameter[3];
            //            ReportParameter anno = new ReportParameter("anno", annoDaConsiderare.ToString());
            //            ReportParameter semestre = new ReportParameter("semestre", semestreDaConsiderare);
            //            listaParam[1] = anno;
            //            listaParam[2] = semestre;
            //        }
            //    }
            //    else
            //    {
            //        ReportViewerImprese.Visible = false;
            //        TableLabelCartella.Visible = false;
            //        TableLabelNoDati.Visible = true;
            //    }
            //}
            //if (tipoReport == "premioFedelta")
            //{
            //    string dataLiquidazioneDaConsiderare = commonBiz.GetDataPremioFedelta();

            //    //if (!String.IsNullOrEmpty(dataLiquidazioneDaConsiderare))
            //    DateTime dataLiquidazioneDate = DateTime.Parse(dataLiquidazioneDaConsiderare);

            //    DateTime dateLimit = new DateTime(dataLiquidazioneDate.Year, 12, 31);

            //    if (DateTime.Now >= dataLiquidazioneDate && DateTime.Now <= dateLimit)
            //    {
            //        listaParam = new ReportParameter[2];
            //        ReportParameter dataLiquidazione = new ReportParameter("dataLiquidazione", dataLiquidazioneDaConsiderare);
            //        listaParam[1] = dataLiquidazione;
            //    }
            //    else
            //    {
            //        ReportViewerImprese.Visible = false;
            //        TableLabelPremioFedelta.Visible = false;
            //        TableLabelNoDatiPremioFedelta.Visible = true;
            //    }
            //}

            //if (tipoReport != "trattamentoCIGO")
            //{
            //    listaParam[0] = paramIdImpresa;
            //    ReportViewerImprese.ServerReport.SetParameters(listaParam);
            //}

            //if (tipoReport == "evr")
            //{
            //    //Export PDF

            //    Warning[] warnings;
            //    string[] streamids;
            //    string mimeType;
            //    string encoding;
            //    string extension;

            //    byte[] bytes = ReportViewerImprese.ServerReport.Render(
            //    "PDF", null, out mimeType, out encoding, out extension,
            //    out streamids, out warnings);

            //    Response.Clear();
            //    Response.Buffer = true;
            //    Response.ContentType = "application/pdf";

            //    Response.AppendHeader("Content-Disposition", "attachment;filename=EVR.pdf");
            //    Response.BinaryWrite(bytes);

            //    Response.Flush();
            //    Response.End();
            //}
        }

        private static int? GetAnno()
        {
            int? anno = null;
            DateTime data = DateTime.Today;
            if (data.Month == 1)
                anno = data.Year - 1;
            if ((data.Month >= 6 && data.Month <= 9) || data.Month >= 11)
                anno = data.Year;

            return anno;
        }

        private static string GetSemestre()
        {
            string semestre = null;

            DateTime data = DateTime.Today;
            if (data.Month == 1 || data.Month >= 11)
                semestre = "SECONDO";
            if (data.Month >= 6 && data.Month <= 9)
                semestre = "PRIMO";

            return semestre;
        }

        protected void ButtonTrattamentoCIGO_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ReportViewerImprese.ServerReport.ReportPath = "/ReportImpreseCE/ReportATCIGO";

                ReportParameter[] listaParam = new ReportParameter[2];
                ReportParameter anno = new ReportParameter("anno", DropDownListAnno.SelectedValue);
                listaParam[0] = anno;
                ReportParameter idImpresa = new ReportParameter("idImpresa", ((Int32)ViewState["idImpresa"]).ToString());
                listaParam[1] = idImpresa;
                ReportViewerImprese.ServerReport.SetParameters(listaParam);

                //Export PDF

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = ReportViewerImprese.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=trattamentoCIGO{0}.pdf", DropDownListAnno.SelectedValue));
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }
    }
}