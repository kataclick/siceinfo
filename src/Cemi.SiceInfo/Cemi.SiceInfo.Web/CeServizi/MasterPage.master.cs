﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Gestione Menu Header

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.BollettinoFrecciaRichiesta)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.BollettinoFrecciaStatistiche))
            {
                RadSlidingPanePagamenti.Visible = true;
            }

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.InfoImpresa))
            {
                RadSlidingPaneRendiconti.Visible = true;
            }

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.InfoLavoratore))
            {
                RadSlidingPaneRendicontiLavoratore.Visible = true;
            }

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestione)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriProgrammazioneGestione)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrev)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatistichePerIspettore)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatisticheGenerali)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere)
                || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriEstrazioneLodi)
                )
            {
                RadSlidingPaneCantieri.Visible = true;
            }

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestione)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestioneCE)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheConferma)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheSblocco)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheVisioneCE)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DownloadInfoSindacati)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DownloadLavoratoriAttivi)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DownloadDelegaCartacea))
            {
                RadSlidingPaneSindacati.Visible = true;
            }

            if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePIN)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IISLogStatistiche)
                  || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SMSInfoGestisciSMSScadenza))
            {
                RadSlidingPaneAmministrazione.Visible = true;
            }

            if (new GestioneReport().GetListaReport(GestioneUtentiBiz.GetIdUtente()).Count > 0)
            {
                RadSlidingPaneReport.Visible = true;
            }

            //if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieRichiestePersonale))
            //{
            //    RadSlidingPaneLavoraConNoi.Visible = true;
            //}

            #endregion

            HtmlContainerControl controlBody = FindControl("body") as HtmlContainerControl;

            if (controlBody != null)
                controlBody.Attributes.Add("style",
                                           "background-repeat: repeat-x; background-image:url(" +
                                           ResolveUrl("images/sfondo2.gif") + ");");

            Utente utente = (Utente)Membership.GetUser();

            if (utente != null)
            {
                if (utente.IdUtente > 0)
                {
                    GestioneUtentiBiz biz = new GestioneUtentiBiz();
                    RuoliCollection ruoli = biz.GetRuoliUtente(utente.IdUtente);
                    if (ruoli.Count == 1 &&
                        (ruoli[0].Nome == "ImpresaPit16Ore" || ruoli[0].Nome == "ConsulentePit16Ore" ||
                         ruoli[0].Nome == "IscrizioneLavoratoreSintesi"))
                    {
                        LoginMain.Visible = false;
                    }
                }

                int giorniPreavviso = Convert.ToInt32(ConfigurationManager.AppSettings["GiorniPreavvisoScadenzaPassword"]);

                DateTime dataScadenzaPassword =
                    new GestioneUtentiBiz().GetScadenzaPasswordFromUsername(utente.UserName);
                int diffScadenzaPassword = Convert.ToInt32(dataScadenzaPassword.Subtract(DateTime.Today).TotalDays);
                if (diffScadenzaPassword >= 0 && diffScadenzaPassword < giorniPreavviso)
                    PanelScadenzaPassword.Visible = true;
            }
        }

        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    if (!String.IsNullOrEmpty(TextBoxMailControl.Text))
        //    {
        //        throw new Exception("Presunto bot");
        //    }
        //}
    }
}