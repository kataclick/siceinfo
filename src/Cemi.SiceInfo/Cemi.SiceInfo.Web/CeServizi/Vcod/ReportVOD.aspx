﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master"  AutoEventWireup="true" CodeBehind="ReportVOD.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Vcod.ReportVOD" %>

<%@ Register Src="../WebControls/MenuVOD.ascx" TagName="MenuVOD" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Congruità Ore Denunciate" Visible="true" />
    <br />
    <asp:Panel ID="PanelElencoImpreseFascia" runat="server">
        <asp:PlaceHolder runat="server" ID="PlaceHolder1" />
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Data da:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDaElencoImprese" runat="server" Width="200px" />
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Data a:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataAElencoImprese" runat="server" Width="200px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Operai (&gt;=):"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOperai" runat="server" MinValue="0"
                        Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Part-time (&gt;=):"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxPartTime" runat="server" MaxValue="100"
                        MinValue="0" Type="Percent" Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Cod. consulente:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxIdConsulente" runat="server" MinValue="0">
                        <NumberFormat DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Cod. impresa:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresaElencoImprese" runat="server"
                        MaxValue="100000" MinValue="0" DataType="System.Int32">
                        <NumberFormat DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ore ferie (&gt;=):
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOreFerie" runat="server" MinValue="0" >
                        <NumberFormat DecimalDigits="2" DecimalSeparator="."  GroupSeparator=""/>
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    Ore permessi retribuiti (&gt;=):
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOrePermessiRetribuiti" runat="server" MinValue="0" >
                        <NumberFormat DecimalDigits="2" DecimalSeparator="."  GroupSeparator=""/>
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Ore CIG straordinaria (&gt;=):
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOreCigStraordinaria" runat="server" MinValue="0">
                        <NumberFormat DecimalDigits="2" DecimalSeparator="."  GroupSeparator=""/>
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    Ore CIG in deroga (&gt;=):
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOreCigDeroga" runat="server" MinValue="0">
                        <NumberFormat DecimalDigits="2" DecimalSeparator="."  GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
             <tr>
                <td>
                    Ore CIG Ordinaria (&gt;=):
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOreCigo" runat="server" MinValue="0" >
                        <NumberFormat DecimalDigits="2" DecimalSeparator="."  GroupSeparator=""/>
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    Ore CIGO maltempo (&gt;=):
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxOraCigoMaltempo" runat="server" MinValue="0" >
                        <NumberFormat DecimalDigits="2" DecimalSeparator="."  GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Cod. esattore:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxIdEsattoreElencoImprese" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutti" />
                            <telerik:RadComboBoxItem runat="server" Text="Non assegnato" Value="N" />
                            <telerik:RadComboBoxItem runat="server" Text="R1" Value="R1" />
                            <telerik:RadComboBoxItem runat="server" Text="R2" Value="R2" />
                            <telerik:RadComboBoxItem runat="server" Text="R3" Value="R3" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Fascia:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxFascia" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutte" />
                            <telerik:RadComboBoxItem runat="server" Text="Fascia A" Value="A" />
                            <telerik:RadComboBoxItem runat="server" Text="Fascia M" Value="M" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Stato:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxStato" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutti" />
                            <telerik:RadComboBoxItem runat="server" Text="Attiva" Value="ATTIVA" />
                            <telerik:RadComboBoxItem runat="server" Text="Sospesa" Value="SOSPESA" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Esito controllo da denuncia:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxEsitoControllo" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutti" />
                            <telerik:RadComboBoxItem runat="server" Text="E1" Value="E1" />
                            <telerik:RadComboBoxItem runat="server" Text="E2" Value="E2" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="3"/>
                <td>
                    <asp:Button ID="ButtonVisualizzaElencoImprese" runat="server" Text="Visualizza" OnClick="ButtonVisualizzaElencoImprese_Click" />
                    <asp:Button ID="ButtonEsportaConIndirizzi" runat="server" Text="Esporta con Indirizzi"
                        OnClick="ButtonEsportaConIndirizzi_Click" />
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    <asp:Panel ID="PanelSchedaImpresa" runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Data da:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDaSchedaImpresa" runat="server" Width="200px" />
                </td>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Data a:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataASchedaImpresa" runat="server" Width="200px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label16" runat="server" Text="Cod. impresa:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxIdEsattoreSchedaImpresa" runat="server" Visible="false">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutti" />
                            <telerik:RadComboBoxItem runat="server" Text="Non assegnato" Value="N" />
                            <telerik:RadComboBoxItem runat="server" Text="R1" Value="R1" />
                            <telerik:RadComboBoxItem runat="server" Text="R2" Value="R2" />
                            <telerik:RadComboBoxItem runat="server" Text="R3" Value="R3" />
                        </Items>
                    </telerik:RadComboBox>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresaSchedaImpresa" runat="server"
                        MaxValue="100000" MinValue="0" DataType="System.Int32">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="ButtonVisualizzaSchedaImpresa" runat="server" Text="Visualizza" OnClick="ButtonVisualizzaSchedaImpresa_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonTornaElencoImprese" runat="server" Text="Torna all'elenco imprese"
                        OnClick="ButtonTornaElencoImprese_Click" />
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    <asp:Panel ID="PanelSchedaVCOD" runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="Label17" runat="server" Text="Data da:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDaSchedaVCOD" runat="server" Width="200px" />
                </td>
                <td>
                    <asp:Label ID="Label18" runat="server" Text="Data a:"></asp:Label>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataASchedaVCOD" runat="server" Width="200px" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label19" runat="server" Text="Cod. esattore:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxIdEsattoreSchedaVCOD" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Tutti" />
                            <telerik:RadComboBoxItem runat="server" Text="Non assegnato" Value="N" />
                            <telerik:RadComboBoxItem runat="server" Text="R1" Value="R1" />
                            <telerik:RadComboBoxItem runat="server" Text="R2" Value="R2" />
                            <telerik:RadComboBoxItem runat="server" Text="R3" Value="R3" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:Label ID="Label20" runat="server" Text="Cod. impresa:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresaSchedaVCOD" DataType="System.Int32"
                        MinValue="0" MaxValue="100000" runat="server">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label21" runat="server" Text="Max ore perm. ind.:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOrePermessiIndividuali" runat="server"
                        DataType="System.Int32" MaxValue="2000" MinValue="0" Value="88">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <asp:Label ID="Label22" runat="server" Text="Max ore ferie:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreFerie" runat="server" MaxValue="2000"
                        MinValue="0" Value="160">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>

                <td>
                    <asp:Label ID="Label24" runat="server" Text="Max ore assenza giust.:"></asp:Label>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaOreAssGiust" runat="server" MaxValue="2000"
                        MinValue="0" Value="50">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
                 <td>
                    <asp:Label ID="Label25" runat="server" Text="Max ore assenza non giust.:"></asp:Label>
                    <br />
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreAssNonGiust" runat="server"
                        DataType="System.Int32" MaxValue="2000" MinValue="0" Value="50">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label23" runat="server" Text="Max ore CIG (Totale):"></asp:Label>
                </td>
                <td>
                    
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreCIG" runat="server" DataType="System.Int32"
                        MaxValue="2000" MinValue="0" Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
               <td>
                   <asp:Label ID="Label13" runat="server" Text="Max ore CIG straordinaria:"></asp:Label>
               </td>
               <td>
                    
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreCIGStraord" runat="server" DataType="System.Int32"
                    MaxValue="2000" MinValue="0" Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
               </td>
            </tr>
            <tr>
                 <td>
                    <asp:Label ID="Label14" runat="server" Text="Max ore CIG deroga:"></asp:Label>
                </td>
                <td>
                    
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreCIGDeroga" runat="server" DataType="System.Int32"
                        MaxValue="2000" MinValue="0" Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
                </td>
               <td>
                   <asp:Label ID="Label15" runat="server" Text="Max ore CIG Ordinaria:"></asp:Label>
               </td>
               <td>
                    
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreCIGO" runat="server" DataType="System.Int32"
                    MaxValue="2000" MinValue="0" Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
               </td>
            </tr>
            <tr>
               <td>
                   <asp:Label ID="Label26" runat="server" Text="Max ore CIGO Maltempo:"></asp:Label>
               </td>
               <td>
                    
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxMaxOreCIGOMaltempo" runat="server" DataType="System.Int32"
                    MaxValue="2000" MinValue="0" Value="0">
                        <NumberFormat DecimalDigits="0" />
                    </telerik:RadNumericTextBox>
               </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonTornaSchedaImpresa" runat="server" Text="Torna alla scheda impresa"
                        OnClick="ButtonTornaSchedaImpresa_Click" />
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="ButtonVisualizzaVCOD" runat="server" Text="Visualizza" OnClick="ButtonVisualizzaVCOD_Click" />
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    <table style="height: 600px; width: 940px;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerVOD" runat="server" OnInit="ReportViewerVOD_Init"
                    ProcessingMode="Remote" Width="740px" Height="490px" OnPageNavigation="void_name"
                    DocumentMapCollapsed="True" PromptAreaCollapsed="True" ShowParameterPrompts="false" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuVOD ID="MenuVOD1" runat="server"></uc2:MenuVOD>
</asp:Content>

