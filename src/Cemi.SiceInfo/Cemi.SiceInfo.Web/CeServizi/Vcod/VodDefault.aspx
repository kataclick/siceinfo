﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="VodDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Vcod.VodDefault" %>

<%@ Register Src="../WebControls/MenuVOD.ascx" TagName="MenuVOD" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuVOD ID="MenuVOD1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifica Congruità Ore Denunciate"
        Visible="true" />
    <p>
        La funzione consente di effettuare delle verifiche sulla congruità dei dati dichiarati
        dalle imprese iscritte nella denuncia mensile di manodopera occupata.
    </p>
</asp:Content>

