﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Vcod
{
    public partial class VodDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.VodElencoImpreseConsulenti);
            funzionalita.Add(FunzionalitaPredefinite.VodElencoImpreseMedie);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        }
    }
}