﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneAssenza.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.GestioneAssenza"  MaintainScrollPositionOnPostback="true"  %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/DettagliAssenza.ascx" TagName="DettagliAssenza" TagPrefix="uc2" %>
<%@ Register Src="WebControls/ControlliAssenzaImpresa.ascx" TagName="ControlliAssenzaImpresa"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/GestioneGiustificativi.ascx" TagName="GestioneGiustificativi"
    TagPrefix="uc4" %>
<%@ Register Src="WebControls/GiustificativiLista.ascx" TagName="GiustificativiLista"
    TagPrefix="uc5" %>
<%@ Register Src="WebControls/AltriDocumentiLista.ascx" TagName="AltriDocumentiLista"
    TagPrefix="uc6" %>
<%@ Register Src="WebControls/SituazioneAssenza.ascx" TagName="SituazioneAssenza" TagPrefix="uc7" %>
<%@ Register Src="WebControls/GestioneAltriDocumenti.ascx" TagName="GestioneAltriDocumenti"
    TagPrefix="uc8" %>
<%@ Register src="../WebControls/MenuMalattiaTelematica.ascx" tagname="MenuMalattiaTelematica" tagprefix="uc9" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <telerik:RadScriptBlock>
        <script type="text/javascript">
            //the following code use radconfirm to mimic the blocking of the execution thread.
            //The approach has the following limitations:
            //1. It works inly for elements that have *click* method, e.g. links and buttons
            //2. It cannot be used in if(!confirm) checks
            window.blockConfirm = function (text, mozEvent, oWidth, oHeight, callerObj, oTitle) {
                var ev = mozEvent ? mozEvent : window.event; //Moz support requires passing the event argument manually 
                //Cancel the event 
                ev.cancelBubble = true;
                ev.returnValue = false;
                if (ev.stopPropagation) ev.stopPropagation();
                if (ev.preventDefault) ev.preventDefault();

                //Determine who is the caller 
                var callerObj = ev.srcElement ? ev.srcElement : ev.target;

                //Call the original radconfirm and pass it all necessary parameters 
                if (callerObj) {
                    //Show the confirm, then when it is closing, if returned value was true, automatically call the caller's click method again. 
                    var callBackFn = function (arg) {
                        if (arg) {
                            callerObj["onclick"] = "";
                            if (callerObj.click) callerObj.click(); //Works fine every time in IE, but does not work for links in Moz 
                            else if (callerObj.tagName == "A") //We assume it is a link button! 
                            {
                                try {
                                    eval(callerObj.href)
                                }
                                catch (e) { }
                            }
                        }
                    }

                    radconfirm(text, callBackFn, oWidth, oHeight, callerObj, oTitle);
                }
                return false;
            } 
        </script>
    </telerik:RadScriptBlock>
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Caricamento giustificativi" />
    <uc2:DettagliAssenza ID="DettagliAssenza1" runat="server" />
    <br />
    <uc3:ControlliAssenzaImpresa ID="ControlliAssenzaImpresa1" runat="server" />
    <br />
    <%--  <table class="borderedTable" cellspacing="0" cellpadding="0">--%>
    <div>
        <br />
        <uc7:SituazioneAssenza ID="SituazioneAssenza1" runat="server" />
        <br />
        <asp:Button ID="ButtonSalvaNonIndennizzabili" runat="server" Text="Conferma Non Indennizzabili"
            OnClick="ButtonSalvaNonIndennizzabili_Click" />
    </div>
    <br />
    <table class="borderedTable">
        <tr>
            <td valign="top">
                <uc5:GiustificativiLista ID="GiustificativiLista1" runat="server" />
            </td>
            <td valign="top">
                <uc6:AltriDocumentiLista ID="AltriDocumentiLista1" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <table class="borderedTable" id="tableNote" runat="server">
        <%--<colgroup>
            <col style="width: 100px" />
            <col style="width: 100px" />
            <col style="width: 100px" />
            <col />
        </colgroup>--%>
        <tr>
            <td style="width: 100px">
                <asp:Label ID="LabelNote" runat="server" Text="Note:"></asp:Label>
            </td>
            <td colspan="3">
                <asp:TextBox ID="TextBoxNote" runat="server" TextMode="MultiLine" MaxLength="1000"
                    Height="40px" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:CheckBox ID="CheckBoxTelefonata" runat="server" Text="Telefonata"></asp:CheckBox>
            </td>
            <td style="width: 100px">
                <asp:Button runat="server" Text="Invia e-mail" ID="RadButtonInviaMail" OnClick="ButtonInviaMail_Click" />
            </td>
            <td style="width: 100px">
                <asp:CheckBox ID="CheckBoxEmail" runat="server" Text="Invio e-mail"></asp:CheckBox>
            </td>
            <td align="right">
                <asp:Button runat="server" Text="Salva note" ID="RadButtonSalvaNote" OnClick="ButtonNote_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Data 'In Attesa'
            </td>
            <td style="width: 100px">
                <asp:TextBox runat="server" ID="TextBoxInAttesa" ReadOnly="True" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Data 'Invio Primo Sollectio'
            </td>
            <td>
                <asp:TextBox runat="server" ID="TextBoxPrimoSollecito" ReadOnly="True" />
            </td>
        </tr>
    </table>
    <br />
    <table class="standardTable" id="tableButton" runat="server">
        <tr>
            <td style="width: 150px">
                <asp:Button runat="server" Text="Accogli domanda" ID="ButtonAccogli" OnClick="ButtonAccogli_Click" Width="170px" />
            </td>
            <td style="width: 150px">
                <asp:Button runat="server" Text="Torna alla lista" ID="ButtonIndietro" OnClick="ButtonIndietro_Click" Width="170px" />
            </td>
            <td style="width: 150px">
            </td>
            <td style="width: 150px">
            </td>
            <td style="width: 150px">
            </td>
            <td style="width: 150px">
            </td>
        </tr>
        <tr id="trButtonBackOffice" visible=" false" runat="server">
            <td style="width: 150px">
                <asp:Button runat="server" Text="Annulla assenza" ID="ButtonAnnullaAssenza" OnClick="ButtonAnnullaAssenza_Click"
                    OnClientClick="return blockConfirm('Sei sicuro di voler annullare l\'assenza?', event, 330, 100,'','Conferma annullamento');" 
                    Width="170px" />
            </td>
            <td style="width: 150px">
                <asp:Button runat="server" Text="Riporta Immessa Pervenuta" ID="ButtonImmessaPervenuta" OnClick="ButtonImmessaPervenuta_Click" Width="170px" />
            </td>
            <td style="width: 150px; text-align:center">
                <asp:Button runat="server" Text="In attesa" ID="ButtonInAttesa" OnClick="ButtonInAttesa_Click" Width="120px" />
            </td>
            <td style="width: 150px; text-align:center">
                <asp:Button runat="server" Text="I° Sollecito" ID="ButtonPrimoSollecito" OnClick="ButtonPrimoSollecito_Click" Width="120px" />
            </td>
            <td style="width: 150px; text-align:center">
                <asp:Button runat="server" Text="Respingi" ID="ButtonRespingi" OnClick="ButtonRespingi_Click" Width="120px" />
            </td>
            <td style="width: 150px; text-align:center">
                <asp:Button runat="server" Text="Respingi Sempre" ID="ButtonRespingiSempre" OnClick="ButtonRespingi_Click" Width="120px" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:Panel ID="PanelConferma" runat="server" Visible="false">
        <asp:Label ID="LabelConferma" runat="server" Font-Bold="True" Text="La domanda sta per essere inviata a Cassa Edile e non potrà più essere modificata. "></asp:Label>
        <asp:Button ID="RadButtonConferma" runat="server" Text="Conferma" OnClick="ButtonConferma_Click"
            Style="height: 21px"></asp:Button>
        <asp:Button ID="RadButtonAnnulla" runat="server" Text="Annulla" OnClick="ButtonAnnulla_Click">
        </asp:Button>
    </asp:Panel>
    <br />
    <asp:Label ID="LabelMessaggio" runat="server" Font-Bold="True"></asp:Label>
    <br />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelGiustificativi" runat="server" Visible="False">
        <uc4:GestioneGiustificativi ID="GestioneGiustificativi1" runat="server" />
    </asp:Panel>
    <asp:Panel ID="PanelAltriDocumenti" runat="server" Visible="False">
        <uc8:GestioneAltriDocumenti ID="GestioneAltriDocumenti1" runat="server" />
    </asp:Panel>
    <br />
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="MenuDettaglio">
    <uc9:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>

