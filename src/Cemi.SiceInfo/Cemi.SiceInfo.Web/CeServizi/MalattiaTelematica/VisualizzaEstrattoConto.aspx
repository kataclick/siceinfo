﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="VisualizzaEstrattoConto.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.VisualizzaEstrattoConto" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortuni0" sottoTitolo="Visualizzazione Estratto Conto Malattia" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerEstrattoConto" runat="server" ProcessingMode="Remote"
                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" Height="550pt"
                    Width="550pt" ShowParameterPrompts="false" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

