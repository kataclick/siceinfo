﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RicercaMessaggi.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.RicercaMessaggi"
     %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Ricerca messaggi" />
    <br />
    <div class="standardDiv">
        <table class="standardTable">
            <colgroup>
                <col />
                <col />
                <col />
                <col width="100px" />
            </colgroup>
            <tr id="Tr1" runat="server">
                <td>
                    Giorno
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerGiorno" runat="server">
                    </telerik:RadDatePicker>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data"
                        ValidationGroup="Ricerca" ControlToValidate="RadDatePickerGiorno">*</asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <asp:CheckBox ID="CheckBoxDescrizione" runat="server" Text="Con messaggio" Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Gravità errore
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxErrore" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" />
                            <telerik:RadComboBoxItem runat="server" Text="Errore" Value="ERR" />
                            <telerik:RadComboBoxItem runat="server" Text="Informativo" Value="INF" />
                            <telerik:RadComboBoxItem runat="server" Text="Avvertimento" Value="AVV" />
                        </Items>
                    </telerik:RadComboBox>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data"
                        ValidationGroup="Ricerca" ControlToValidate="RadDatePickerGiorno">*</asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                        ValidationGroup="Ricerca" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                        ValidationGroup="Ricerca" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="standardDiv">
        <div>
            <asp:Button ID="ButtonExport" runat="server" Text="Export to Excel" OnClick="ButtonExport_OnClick" />
        </div>
        <br />
        <telerik:RadGrid ID="RadGridMessaggi" runat="server" GridLines="None" OnItemDataBound="RadGridMessaggi_ItemDataBound"
            Width="100%" AllowPaging="True" OnPageIndexChanged="RadGridMessaggi_PageIndexChanged"
            CellSpacing="0">
            <MasterTableView DataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" HeaderText="Impresa"
                        UniqueName="impresa" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelIdImpresa" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Lavoratore"
                        UniqueName="lavoratore" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelLavoratore" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Assenza"
                        UniqueName="assenza" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProtocollo" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelPeriodo" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelImportoRichiesto" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelImportoLiquidabile" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="Descrizione" UniqueName="descrizione" DataField="DescrizioneMessaggio">
                        <ItemStyle Width="100px" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Gravità" UniqueName="gravita" DataField="GravitaSegnalazione">
                        <ItemStyle Width="100px" Font-Bold="True" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter dataImportazione column" HeaderText="Data Importazione"
                        UniqueName="dataImportazione" DataField="DataImportazione" DataFormatString="{0:dd/MM/yyyy}"
                        ItemStyle-VerticalAlign="Top">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>

