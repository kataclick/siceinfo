﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RicercaAssenzaInAttesa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.RicercaAssenzaInAttesa" 
     %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Ricerca assenze in attesa" />
    <br />
    <div class="standardDiv">
        <table class="standardTable">
            <colgroup>
                <col />
                <col />
                <col />
                <col width="100px" />
            </colgroup>
            <tr>
                <td>
                    Periodo valido per
                </td>
                <td>
                    <asp:RadioButton ID="RadioButtonInAttesa" runat="server" Text="In attesa" Checked="True"
                        GroupName="periodo" />
                    <asp:RadioButton ID="RadioButtonPrimoSollecito" runat="server" Text="Primo Sollecito"
                        GroupName="periodo" />
                </td>
                <td colspan="2">                
                </td>
            </tr>
            <tr id="Tr1" runat="server">
                <td>
                    Giorno da
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerGiornoDa" runat="server">
                    </telerik:RadDatePicker>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data"
                        ValidationGroup="Ricerca" ControlToValidate="RadDatePickerGiorno">*</asp:RequiredFieldValidator>--%>
                </td>
                <td>
                    Giorno a
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerGiornoA" runat="server">
                    </telerik:RadDatePicker>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data"
                        ValidationGroup="Ricerca" ControlToValidate="RadDatePickerGiorno">*</asp:RequiredFieldValidator>--%>
                </td>
            </tr>
            <tr>
                <td>
                    Utente assegnato: 
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboboxUtente" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="0" />
                            <telerik:RadComboBoxItem Text="In Carico" Value="1" Selected="true" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click" ValidationGroup="Ricerca" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td colspan="2">
                    <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore" ValidationGroup="Ricerca" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div class="standardDiv">
        <telerik:RadGrid ID="RadGridAssenze" runat="server" GridLines="None" OnItemDataBound="RadGridAssenze_ItemDataBound"
            Width="100%" AllowPaging="True" OnPageIndexChanged="RadGridAssenze_PageIndexChanged"
            OnItemCommand="RadGridAssenze_OnItemCommand" CellSpacing="0">
            <MasterTableView DataKeyNames="Id">
                <Columns>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" HeaderText="Impresa"
                        UniqueName="impresa" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelIdImpresa" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Lavoratore"
                        UniqueName="lavoratore" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelLavoratore" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Assenza"
                        UniqueName="assenza" ItemStyle-VerticalAlign="Top">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelProtocollo" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelPeriodo" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelImportoRichiesto" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelImportoLiquidabile" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Data In Attesa"
                        UniqueName="InAttesa" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelInAttesa" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonPrimoSollecito" runat="server" Text="Invio I° sollecito" CommandName="PrimoSollecito">
                                        </asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter assenza column" HeaderText="Data I° sollecito"
                        UniqueName="PrimoSollecito" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                        ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelPrimoSollecito" runat="server" Font-Bold="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonRespingimento" runat="server" Text="Respingi" CommandName="Respingimento">
                                        </asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridTemplateColumn>
                    <%--<telerik:GridBoundColumn HeaderText="" UniqueName="descrizione" DataField="DescrizioneMessaggio">
                        <ItemStyle Width="100px" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Gravità" UniqueName="gravita" DataField="GravitaSegnalazione">
                        <ItemStyle Width="100px" Font-Bold="True" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter dataImportazione column" HeaderText="Data Importazione"
                        UniqueName="dataImportazione" DataField="DataImportazione" DataFormatString="{0:dd/MM/yyyy}"
                        ItemStyle-VerticalAlign="Top">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridBoundColumn>--%>
                </Columns>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
