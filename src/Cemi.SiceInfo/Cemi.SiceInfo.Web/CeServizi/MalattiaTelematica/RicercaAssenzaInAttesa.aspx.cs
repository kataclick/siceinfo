﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using System.Globalization;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica
{
    public partial class RicercaAssenzaInAttesa : System.Web.UI.Page
    {
        BusinessEF biz = new BusinessEF();


        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeInAttesa);

            #endregion

            //if (!Page.IsPostBack)
            //    RadDatePickerGiornoDa.SelectedDate = DateTime.Now.Date;
        }

        protected void RadButtonOk_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                //if (OnFiltroSelected != null)
                //{
                //    AssenzeFilter filtro = CreaFiltro();
                //    OnFiltroSelected(filtro);
                //}
                CaricaAssenzeInAttesa();
            }
        }

        private void CaricaAssenzeInAttesa()
        {
            int tipoRichiesta = 1;

            if (RadioButtonPrimoSollecito.Checked)
            {
                tipoRichiesta = 2;
            }

            int? idUtente = null;

            if (RadComboboxUtente.SelectedValue == "1")
            {
                idUtente = GestioneUtentiBiz.GetIdUtente();
            }
                

            Presenter.CaricaElementiInGridView(RadGridAssenze,
                                               biz.GetAssenzeInAttesa(tipoRichiesta, RadDatePickerGiornoDa.SelectedDate, RadDatePickerGiornoA.SelectedDate, idUtente));
        }


        protected void RadGridAssenze_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaAssenzeInAttesa();
        }

        protected void RadGridAssenze_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                MalattiaTelematicaAssenza assenza = (MalattiaTelematicaAssenza)e.Item.DataItem;
                Label lIdImpresa = (Label)e.Item.FindControl("LabelIdImpresa");
                Label lImpresa = (Label)e.Item.FindControl("LabelImpresa");
                Label lIdLavoratore = (Label)e.Item.FindControl("LabelIdLavoratore");
                Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
                Label lProtocollo = (Label)e.Item.FindControl("LabelProtocollo");
                Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
                Label lInAttesa = (Label)e.Item.FindControl("LabelInAttesa");
                Label lPrimoSollecito = (Label)e.Item.FindControl("LabelPrimoSollecito");

                Button bPrimoSollecito = (Button)e.Item.FindControl("ButtonPrimoSollecito");
                bPrimoSollecito.Visible = false;
                Button bRespingimento = (Button)e.Item.FindControl("ButtonRespingimento");
                bRespingimento.Visible = false;
                //Label LImportoRichiesto = (Label)e.Item.FindControl("LabelImportoRichiesto");
                //Label LImportoLiquidabile = (Label)e.Item.FindControl("LabelImportoLiquidabile");

                lIdImpresa.Text = String.Format("Codice: {0}", assenza.IdImpresa);
                lImpresa.Text = assenza.Impresa.RagioneSociale;
                lIdLavoratore.Text = String.Format("Codice: {0}", assenza.IdLavoratore);
                lLavoratore.Text = String.Format("{0} {1}", assenza.Lavoratore.Cognome, assenza.Lavoratore.Nome);
                lProtocollo.Text = String.Format("{0} {1}/{2}/{3}", assenza.IdCassaEdile, assenza.TipoProtocollo, assenza.NumeroProtocollo.ToString(), assenza.AnnoProtocollo.ToString());
                lPeriodo.Text = String.Format("{0} - {1}", assenza.DataInizio.Value.ToShortDateString(), assenza.DataFine.Value.ToShortDateString());

                if (assenza.DataInAttesa != null)
                {
                    lInAttesa.Text = assenza.DataInAttesa.Value.ToShortDateString();
                    bPrimoSollecito.Visible = true;

                    //if (assenza.DataInAttesa.Value.AddMonths(3) < DateTime.Now)
                    //{
                    bPrimoSollecito.Enabled = assenza.DataInAttesa.Value.AddMonths(3) < DateTime.Now;
                    //}
                }

                if (assenza.DataInvioPrimoSollecito != null)
                {
                    lPrimoSollecito.Text = assenza.DataInvioPrimoSollecito.Value.ToShortDateString();
                    bRespingimento.Visible = true;
                    bPrimoSollecito.Enabled = false;
                }
                //LImportoRichiesto.Text = String.Format("Importo richiesto: {0}", messaggi.ImportoRichiesto.HasValue ? ((decimal)messaggi.ImportoRichiesto).ToString("c2") : "");
                //LImportoLiquidabile.Text = String.Format("Importo liquidabile: {0}", messaggi.ImportoLiquidabile.HasValue ? ((decimal)messaggi.ImportoLiquidabile).ToString("c2") : "");

            }

        }

        protected void RadGridAssenze_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "PrimoSollecito":
                    {
                        Int32 idAssenza = (Int32)
                                                 RadGridAssenze.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];


                        Context.Items["IdAssenza"] = idAssenza;
                        Context.Items["TipoRichiesta"] = 2;
                        //Context.Items["FiltroIdImpresa"]  = ViewState["FiltroIdImpresa"];
                        //Context.Items["FiltroRagioneSociale"] = ViewState["FiltroRagioneSociale"];
                        //Context.Items["FiltroCodiceFiscale"] = ViewState["FiltroCodiceFiscale"];
                        //Context.Items["FiltroIdLavoratore"] = ViewState["FiltroIdLavoratore"];
                        //Context.Items["FiltroCognome"] = ViewState["FiltroCognome"];
                        //Context.Items["FiltroNome"] = ViewState["FiltroNome"];
                        //Context.Items["FiltroDataNascita"] = ViewState["FiltroDataNascita"];
                        //Context.Items["FiltroStatoAssenza"] = ViewState["FiltroStatoAssenza"];
                        //Context.Items["FiltroTipoAssenza"] = ViewState["FiltroTipoAssenza"];
                        //Context.Items["FiltroPeriodoDa"] = ViewState["FiltroPeriodoDa"];
                        //Context.Items["FiltroPeriodoA"] = ViewState["FiltroPeriodoA"];

                        Server.Transfer("~/CeServizi/MalattiaTelematica/RichiestaDocumenti.aspx");
                    }
                    break;
                case "Respingimento":
                    {
                        Int32 idAssenza = (Int32)
                                                 RadGridAssenze.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];



                    }
                    break;


            }
        }
    }
}