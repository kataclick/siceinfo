﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RicercaAssenzeBackOffice.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls.RicercaAssenzeBackOffice" %>

<%@ Register Src="RicercaAssenzeFiltri.ascx" TagName="RicercaAssenzeFiltri" TagPrefix="uc1" %>
<div class="standardDiv">
    <uc1:RicercaAssenzeFiltri ID="RicercaAssenzeFiltri1" runat="server" />
</div>
<div class="standardDiv">
    <telerik:RadGrid ID="RadGridAssenze" runat="server" GridLines="None" OnItemDataBound="RadGridAssenze_ItemDataBound"
        Width="100%" OnSelectedIndexChanged="RadGridAssenze_SelectedIndexChanged" AllowPaging="True"
        OnPageIndexChanged="RadGridAssenze_PageIndexChanged" OnItemCommand="RadGridAssenze_ItemCommand"
        CellSpacing="0">
        <MasterTableView DataKeyNames="IdAssenza,Utente.Id">
            <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Codice Lav." UniqueName="idLavoratore" DataField="Lavoratore.Id">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Cognome" UniqueName="cognomeLavoratore" DataField="Lavoratore.Cognome">
                    <ItemStyle Width="100px" Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Nome" UniqueName="nomeLavoratore" DataField="Lavoratore.Nome">
                    <ItemStyle Width="100px" Font-Bold="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Stato" UniqueName="stato" DataField="TipoStatoMalattiaTelematica.Descrizione">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Tipo assenza" UniqueName="tipoAssenza" DataField="TipoAssenza.Descrizione">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Tipo" UniqueName="tipo" DataField="Tipo">
                    <ItemStyle Width="30px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Inizio Malattia" UniqueName="inizioMalattia"
                    DataField="InizioMalattia" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="periodo">
                    <ItemTemplate>
                        <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Giustificativi" UniqueName="giustificativi">
                    <ItemTemplate>
                        <telerik:RadGrid ID="RadGridGiustificativi" runat="server" Width="100%" GridLines="None"
                            ShowHeader="False">
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to Pdf" />
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="DataInizio" UniqueName="dataInizio" DataFormatString="{0:dd/MM/yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DataFine" UniqueName="dataFine" DataFormatString="{0:dd/MM/yyyy}">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                            </HeaderContextMenu>
                        </telerik:RadGrid>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Select" ImageUrl="../../images/edit.png"
                    UniqueName="selezione">
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn HeaderText="In carico" UniqueName="inCarico" DataField="Utente.UserName">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="" UniqueName="periodo">
                    <ItemTemplate>
                        <asp:Button ID="ButtonInCarico" runat="server" Text="In carico" CommandName="inCarico" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
</div>
