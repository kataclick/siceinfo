﻿using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.MalattiaTelematica.Type.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls
{
    public partial class RicercaAssenzeBackOffice : System.Web.UI.UserControl
    {
        private readonly BusinessEF bizEF = new BusinessEF();
        private readonly Cemi.MalattiaTelematica.Business.Business biz = new Cemi.MalattiaTelematica.Business.Business();

        protected void Page_Load(object sender, EventArgs e)
        {
            RicercaAssenzeFiltri1.OnFiltroSelected += RicercaAssenzeFiltri1_OnFiltroSelected;

            //if (!Page.IsPostBack)
            //{
            //    String func = "";
            //    //String idLavoratore = "";
            //    //String cognome = "";
            //    //String nome = "";
            //    //String dataNascita = "";
            //    //String statoAssenza = "";
            //    //String tipoAssenza = "";
            //    //String periodoDa = "";
            //    //String periodoA = "";
            //    //String ragioneSociale = "";
            //    //String codiceFiscale = "";
            //    //String idImpresa = "";

            //    if (Request.QueryString["func"] != null)
            //    {
            //        func = Request.QueryString["func"].ToString();
            //    }
            //    if (func != "")
            //    {
            //        //if (Request.QueryString["idLavoratore"] != null)
            //        //{
            //        //    idLavoratore = Request.QueryString["idLavoratore"].ToString();
            //        //}

            //        //if (Request.QueryString["cognome"] != null)
            //        //{
            //        //    cognome = Request.QueryString["cognome"].ToString();
            //        //}
            //        //if (Request.QueryString["nome"] != null)
            //        //{
            //        //    nome = Request.QueryString["nome"].ToString();
            //        //}
            //        //if (Request.QueryString["dataNascita"] != null)
            //        //{
            //        //    dataNascita = Request.QueryString["dataNascita"].ToString();
            //        //}
            //        //if (Request.QueryString["statoAssenza"] != null)
            //        //{
            //        //    statoAssenza = Request.QueryString["statoAssenza"].ToString();
            //        //}
            //        //if (Request.QueryString["tipoAssenza"] != null)
            //        //{
            //        //    tipoAssenza = Request.QueryString["tipoAssenza"].ToString();
            //        //}
            //        //if (Request.QueryString["periodoDa"] != null)
            //        //{
            //        //    periodoDa = Request.QueryString["periodoDa"].ToString();
            //        //}
            //        //if (Request.QueryString["periodoA"] != null)
            //        //{
            //        //    periodoA = Request.QueryString["periodoA"].ToString();
            //        //}
            //        //if (Request.QueryString["ragioneSociale"] != null)
            //        //{
            //        //    ragioneSociale = Request.QueryString["ragioneSociale"].ToString();
            //        //}
            //        //if (Request.QueryString["codiceFiscale"] != null)
            //        //{
            //        //    codiceFiscale = Request.QueryString["codiceFiscale"].ToString();
            //        //}
            //        //if (Request.QueryString["idImpresa"] != null)
            //        //{
            //        //    idImpresa = Request.QueryString["idImpresa"].ToString();
            //        //}





            //        //RicercaAssenzeFiltri1.ImpostaFiltro(idImpresa, ragioneSociale, codiceFiscale, idLavoratore, cognome, nome, dataNascita,statoAssenza,tipoAssenza, periodoDa, periodoA);
            //    }


            //}
            if (!Page.IsPostBack)
            {
                Int32 pagina;

                AssenzeFilter filtro = null;

                try
                {
                    filtro = biz.GetFiltroRicerca(GestioneUtentiBiz.GetIdUtente(), out pagina);
                }
                catch
                {
                    biz.DeleteFiltroRicerca(GestioneUtentiBiz.GetIdUtente());
                }

                if (filtro != null /*&& (!filtro.Vuoto() || Request.QueryString["back"] != null)*/)
                {
                    RicercaAssenzeFiltri1.ImpostaFiltro(filtro.IdImpresa.ToString(),
                        filtro.RagioneSocialeImpresa,
                        filtro.CodiceFiscaleImpresa,
                        filtro.IdLavoratore.ToString(),
                        filtro.CognomeLavoratore,
                        filtro.NomeLavoratore,
                        filtro.DataNascitaLavoratore.ToString(),
                        filtro.StatoAssenza,
                        filtro.TipoAssenza,
                        filtro.PeriodoDa.HasValue ? filtro.PeriodoDa.Value.ToString("MM/yyyy") : null,
                        filtro.PeriodoA.HasValue ? filtro.PeriodoA.Value.ToString("MM/yyyy") : null,
                        filtro.DataInvioDa.HasValue ? filtro.DataInvioDa.Value.ToString("dd/MM/yyyy") : null,
                        filtro.DataInvioA.HasValue ? filtro.DataInvioA.Value.ToString("dd/MM/yyyy") : null);

                    RicercaAssenzeFiltri1_OnFiltroSelected(filtro);
                    //CaricaAssenzeCertificati();
                }
            }
        }

        private void RicercaAssenzeFiltri1_OnFiltroSelected(AssenzeFilter filtro)
        {
            ViewState["filtro"] = filtro;

            CaricaAssenzeCertificati();

        }

        private void CaricaAssenzeCertificati()
        {
            AssenzeFilter filtro = (AssenzeFilter)ViewState["filtro"];

            Presenter.CaricaElementiInGridView(RadGridAssenze, bizEF.GetAssenzeCertificati(filtro));

        }

        protected void RadGridAssenze_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                AssenzaCertificati assenzaCertificati = (AssenzaCertificati)e.Item.DataItem;
                Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
                RadGrid rgGiustificativi = (RadGrid)e.Item.FindControl("RadGridGiustificativi");
                Button bInCarico = (Button)e.Item.FindControl("ButtonInCarico");

                // Periodo)
                if (assenzaCertificati.Inizio.HasValue && assenzaCertificati.Fine.HasValue)
                {
                    lPeriodo.Text = String.Format("{0} - {1}",
                        assenzaCertificati.Inizio.Value.ToString("dd/MM/yyyy"),
                        assenzaCertificati.Fine.Value.ToString("dd/MM/yyyy"));
                }

                if (assenzaCertificati.TipoStatoMalattiaTelematica.Id == "L" ||
                    assenzaCertificati.TipoStatoMalattiaTelematica.Id == "D")
                {
                    bInCarico.Enabled = false;
                }
                else
                {
                    bInCarico.Enabled = true;
                }

                if (assenzaCertificati.Utente != null)
                {
                    bInCarico.Text = "Rilascia";
                    //bInCarico.Enabled = true;
                }
                else
                {
                    bInCarico.Text = "In carico";
                    //bInCarico.Enabled = false;

                }

                // Giustificativi
                Presenter.CaricaElementiInGridView(
                    rgGiustificativi,
                    assenzaCertificati.Certificati);
            }
        }
        protected void RadGridAssenze_SelectedIndexChanged(object sender, EventArgs e)
        {
            AssenzeFilter filtro = (AssenzeFilter)ViewState["filtro"];

            Int32 indiceSelezionato = Int32.Parse(RadGridAssenze.SelectedIndexes[0]);
            Int32 idAssenza =
                (Int32)RadGridAssenze.MasterTableView.DataKeyValues[indiceSelezionato]["IdAssenza"];

            Context.Items["IdAssenza"] = idAssenza;
            //Context.Items["FiltroIdImpresa"] = filtro.IdImpresa.ToString();
            //Context.Items["FiltroRagioneSociale"] = filtro.RagioneSocialeImpresa;
            //Context.Items["FiltroCodiceFiscale"] = filtro.CodiceFiscaleImpresa;
            //Context.Items["FiltroIdLavoratore"] = filtro.IdLavoratore.ToString();
            //Context.Items["FiltroCognome"] = filtro.CognomeLavoratore;
            //Context.Items["FiltroNome"] = filtro.NomeLavoratore;
            //Context.Items["FiltroDataNascita"] = filtro.DataNascitaLavoratore.ToString();
            //Context.Items["FiltroDataInvio"] = filtro.DataInvio.ToString();
            //Context.Items["FiltroStatoAssenza"] = filtro.StatoAssenza;
            //Context.Items["FiltroTipoAssenza"] = filtro.TipoAssenza;
            //if (filtro.PeriodoDa != null)
            //{
            //    Context.Items["FiltroPeriodoDa"] = filtro.PeriodoDa.Value.ToString("MM/yyyy");
            //}
            //else
            //{
            //    Context.Items["FiltroPeriodoDa"] = "";
            //}
            //if (filtro.PeriodoA != null)
            //{
            //    Context.Items["FiltroPeriodoA"] = filtro.PeriodoA.Value.ToString("MM/yyyy");
            //}
            //else
            //{
            //    Context.Items["FiltroPeriodoA"] = "";
            //}
            Server.Transfer("~/CeServizi/MalattiaTelematica/GestioneAssenza.aspx");
        }

        protected void RadGridAssenze_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaAssenzeCertificati();
        }


        /*
         int idDomanda = (int) GridViewDomande.DataKeys[e.RowIndex].Values["IdDomanda"];
            int? idUtenteInCarico = (int?) GridViewDomande.DataKeys[e.RowIndex].Values["IdUtenteInCarico"];
            //UtenteAbilitato utente = ((UtenteAbilitato) HttpContext.Current.User.Identity);

            if (biz.UpdateDomandaSetPresaInCarico(idDomanda, GestioneUtentiBiz.GetIdUtente(), !idUtenteInCarico.HasValue))
            {
                CaricaDomande();
            }
         * 
         */

        protected void RadGridAssenze_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "inCarico")
            {
                Int32 idAssenza = (Int32)RadGridAssenze.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdAssenza"];
                Int32? idUtenteInCarico = (Int32?)RadGridAssenze.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Utente.Id"];

                if (idUtenteInCarico != null)
                {
                    bizEF.UpdateMalattiaTelematicaAssenzeUtenteInCarico(idAssenza, null);
                }
                else
                {
                    bizEF.UpdateMalattiaTelematicaAssenzeUtenteInCarico(idAssenza, GestioneUtentiBiz.GetIdUtente());

                }

                CaricaAssenzeCertificati();
            }
        }
    }
}