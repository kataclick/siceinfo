﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.MalattiaTelematica.Type.Filters;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls
{
    public partial class SituazioneAssenza : System.Web.UI.UserControl
    {
        private readonly BusinessEF biz = new BusinessEF();
        private readonly Common bizCommon = new Common();



        protected void Page_Init(object sender, EventArgs e)
        {
            Label lblGrid = new Label();
            lblGrid.Text = "";
            lblGrid.Font.Bold = true;
            lblGrid.Font.Size = FontUnit.Point(9);
            lblGrid.ID = "lblGrid";
            this.Controls.Add(lblGrid);

            #region Creazione del RadGrid di stato assenza
            RadGrid rdAssenza = new RadGrid();
            rdAssenza.Skin = String.Empty;
            rdAssenza.ID = "RadGridAssenza";
            rdAssenza.ItemDataBound += new GridItemEventHandler(RadGridAssenza_ItemDataBound);

            GridTemplateColumn descr = new GridTemplateColumn();
            descr.UniqueName = "descrizione";
            descr.HeaderText = "";// ((DateTime)ViewState["Periodo"]).ToString("MMM yyyy");
            descr.ItemTemplate = new AssenzaGrigliaDescrizioneTemplate();

            rdAssenza.MasterTableView.Columns.Add(descr);

            for (int i = 0; i < 31; i++)
            {
                GridTemplateColumn grid = new GridTemplateColumn();
                grid.UniqueName = i.ToString();
                grid.ItemTemplate = new AssenzaGrigliaTemplate(i.ToString());
                grid.HeaderText = (i + 1).ToString();
                grid.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                grid.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                //grid.ItemStyle.BorderColor = Color.LightGray;
                //grid.ItemStyle.BorderStyle = BorderStyle.Solid;
                //grid.ItemStyle.BorderWidth = new Unit("1px");
                //grid.ItemStyle.CssClass = "prova";

                rdAssenza.MasterTableView.Columns.Add(grid);
            }

            this.Controls.Add(rdAssenza);
            #endregion

            Label lOrePerse = new Label();
            lOrePerse.ID = "OrePerseDichiarate";
            lOrePerse.Text = "Ore di scoperto dichiarate in MUT";
            this.Controls.Add(lOrePerse);

            Label lOrePerseValue = new Label();
            lOrePerseValue.ID = String.Format("OrePerseDichiarateValue");
            lOrePerseValue.Text = "";
            lOrePerseValue.Font.Bold = true;
            this.Controls.Add(lOrePerseValue);

            Table table = new Table();
            table.Style.Add("padding", "0px");
            //table.Width = new Unit("50%");

            TableRow row1 = new TableRow();
            TableCell cell1 = new TableCell();
            TableCell cell2 = new TableCell();

            cell1.Style.Add("Width", "200px");
            cell2.Style.Add("Width", "50px");

            cell1.HorizontalAlign = HorizontalAlign.Left;
            cell2.HorizontalAlign = HorizontalAlign.Left;
            cell1.Controls.Add(lOrePerse);
            cell2.Controls.Add(lOrePerseValue);
            row1.Cells.Add(cell1);
            row1.Cells.Add(cell2);
            table.Rows.Add(row1);
            this.Controls.Add(table);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void ConfermaGiorni()
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];
            List<DateTime> ggNonIndennizabili = new List<DateTime>();
            List<MalattiaTelematicaGiornoNonIndennizzabile> gg = new List<MalattiaTelematicaGiornoNonIndennizzabile>();
            List<AssenzaGriglia> ass = (List<AssenzaGriglia>)ViewState["ass"];

            RadGrid RadGridAssenza = (RadGrid)this.FindControl("RadGridAssenza");
            for (int i = 0; i < 31; i++)
            {
                String s = String.Format("NonIndennizzabile{0}", i);
                CheckBox c = (CheckBox)RadGridAssenza.MasterTableView.Items[0].FindControl(s);

                if (c.Checked)
                {
                    gg.Add(new MalattiaTelematicaGiornoNonIndennizzabile()
                    {
                        Giorno = new DateTime(ass[0].DataInizioAssenza.Year, ass[0].DataInizioAssenza.Month, i + 1),
                        IdMalattiaTelematicaAssenza = idAssenza,
                        IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente(),
                        DataInserimentoRecord = DateTime.Now
                    });
                    ggNonIndennizabili.Add(new DateTime(ass[0].DataInizioAssenza.Year, ass[0].DataInizioAssenza.Month, i + 1));
                }
            }

            biz.AggiornaSituazioneGiorniNonIndennizzabili(idAssenza, gg);
        }

        public void CaricaDati(AssenzaDettagli assDet, List<MalattiaTelematicaCertificatoMedico> certificatiMedici)
        {
            AssenzaGriglia assenzaGriglia = new AssenzaGriglia();

            assenzaGriglia.Tipo = assDet.Tipo;
            assenzaGriglia.DataInizioAssenza = assDet.DataInizioAssenza.Value;
            assenzaGriglia.DataFineAssenza = assDet.DataFineAssenza.Value;

            assenzaGriglia.OrePerseDichiarate = assDet.OrePerseDich.Value;

            foreach (MalattiaTelematicaCertificatoMedico cert in certificatiMedici)
            {
                assenzaGriglia.GiustificativiPeriodo.Add(new GiustificativiPeriodoGriglia()
                {
                    Tipo = cert.Tipo,
                    DataInizio = cert.DataInizio.Value,
                    DataFine = cert.DataFine.Value,
                    DataRilascio = cert.DataRilascio.Value,
                    Ricovero = cert.Ricovero
                });
            }

            foreach (MalattiaTelematicaGiornoNonIndennizzabile ggNonInd in biz.GetGiorniNonIndennizzabili(assDet.IdAssenza))
            {
                assenzaGriglia.GiorniNonIndennizzabili.Add(ggNonInd.Giorno);
            }

            List<AssenzaGriglia> ass = new List<AssenzaGriglia>();
            ass.Add(assenzaGriglia);

            ViewState["ass"] = ass;
            ViewState["IdAssenza"] = assDet.IdAssenza;

            CaricaDati();
        }

        private void CaricaDati()
        {
            RadGrid RadGridAssenza = (RadGrid)this.FindControl("RadGridAssenza");

            if (ViewState["ass"] != null)
                Presenter.CaricaElementiInGridView(RadGridAssenza, (List<AssenzaGriglia>)ViewState["ass"]);
        }

        protected void RadGridAssenza_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                AssenzaGriglia assenzaGriglia = (AssenzaGriglia)e.Item.DataItem;
                List<Festivita> fest = bizCommon.GetFestivita(assenzaGriglia.DataInizioAssenza.Year);


                Label lblGrid = (Label)this.FindControl("lblGrid");
                lblGrid.Text = assenzaGriglia.DataInizioAssenza.ToString("MMMM yyyy").ToUpper();


                Label lOrePerseValue = (Label)this.FindControl(String.Format("OrePerseDichiarateValue"));
                lOrePerseValue.Text = assenzaGriglia.OrePerseDichiarate.ToString();

                //int giorniScoperto = 0;

                for (Int32 i = 0; i < 31; i++)
                {
                    TextBox tbAssenza = (TextBox)e.Item.FindControl(String.Format("Assenza{0}", i));
                    //tbAssenza.Width = Unit.Pixel(8);
                    TextBox tbGiustificativo = (TextBox)e.Item.FindControl(String.Format("Giustificativo{0}", i));
                    CheckBox cbNonIndennizzabile = (CheckBox)e.Item.FindControl(String.Format("NonIndennizzabile{0}", i));
                    Boolean isFestivo = false;

                    try
                    {
                        DateTime giornoAttuale = new DateTime(assenzaGriglia.DataInizioAssenza.Year, assenzaGriglia.DataInizioAssenza.Month, i + 1);
                        //DateTime giornoSuccessivo = giornoAttuale.AddDays(1);

                        if (/*giornoAttuale.DayOfWeek == DayOfWeek.Saturday ||*/
                            (giornoAttuale.DayOfWeek == DayOfWeek.Sunday && assenzaGriglia.Tipo.ToUpper() == "MALATTIA") ||
                            fest.Contains(new Festivita() { Giorno = giornoAttuale }))
                        {
                            isFestivo = true;
                        }



                        Boolean assenza = (assenzaGriglia.DataInizioAssenza <= giornoAttuale &&
                                           giornoAttuale <= assenzaGriglia.DataFineAssenza);

                        int GGgiust = 0;
                        int GGnonInd = 0;
                        Boolean giustificativo = false;
                        Boolean nonInd = true;
                        foreach (GiustificativiPeriodoGriglia gi in assenzaGriglia.GiustificativiPeriodo)
                        {
                            //if (gi.DataRilascio <= giornoSuccessivo && giornoSuccessivo <= gi.DataFine)
                            //{
                            //    if (gi.DataInizio <= giornoSuccessivo && giornoSuccessivo <= gi.DataFine)
                            //    {
                            //        giustificativoSucc = true;
                            //    }
                            //}
                            //if (gi.DataRilascio <= giornoAttuale && giornoAttuale <= gi.DataFine)
                            //{
                            //    if (gi.DataInizio <= giornoAttuale && giornoAttuale <= gi.DataFine)
                            //    {
                            //        giustificativo = true;
                            //        break;
                            //    }
                            //}

                            if (gi.DataInizio <= giornoAttuale && giornoAttuale <= gi.DataFine)
                            {
                                GGgiust++;
                                //giustificativo = true;

                                if (NonIndennizzabile(giornoAttuale, gi.DataInizio, gi.DataFine, gi.DataRilascio, gi.Ricovero))
                                    GGnonInd++;

                                //break;
                            }

                        }

                        if (GGgiust > 0) giustificativo = true;
                        if ((GGnonInd == GGgiust && GGnonInd > 0) || GGgiust == 0) nonInd = true;
                        else nonInd = false;


                        Boolean nonIndennizzabile = true;
                        nonIndennizzabile = assenza;

                        // se è festivo o sabato o domenica non lo considero non indennizzabile.
                        if (isFestivo && nonIndennizzabile)
                            nonIndennizzabile = false;

                        if (nonIndennizzabile)
                        {
                            nonIndennizzabile = nonInd;
                            if (!nonIndennizzabile)
                            {
                                nonIndennizzabile = assenzaGriglia.GiorniNonIndennizzabili.Contains(giornoAttuale);
                            }
                            //else
                            //{
                            //    if (assenza)
                            //    {
                            //        giorniScoperto++;
                            //    }
                            //    if (giorniScoperto <= 1)
                            //    {
                            //        if (giustificativoSucc)
                            //        {
                            //            nonIndennizzabile = false;
                            //            nonIndennizzabile = assenzaGriglia.GiorniNonIndennizzabili.Contains(giornoAttuale);
                            //        }
                            //    }
                            //}
                        }



                        tbAssenza.BackColor = assenza ? Color.IndianRed : Color.White;
                        if (giustificativo)
                        {
                            if (assenza)
                            {
                                tbGiustificativo.BackColor = Color.GreenYellow;
                            }
                            else
                            {
                                tbGiustificativo.BackColor = Color.DarkGreen;
                            }
                        }
                        else
                        {
                            tbGiustificativo.BackColor = Color.White;
                        }

                        //tbGiustificativo.BackColor = giustificativo ? Color.GreenYellow : Color.White;
                        cbNonIndennizzabile.Checked = nonIndennizzabile;

                        if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
                        {
                            cbNonIndennizzabile.Enabled = assenza;
                            cbNonIndennizzabile.Enabled = !nonInd;
                        }

                    }
                    catch
                    {
                        tbAssenza.Enabled = false;
                        tbAssenza.BackColor = Color.LightGray;
                        tbGiustificativo.Enabled = false;
                        tbGiustificativo.BackColor = Color.LightGray;
                        cbNonIndennizzabile.Enabled = false;
                    }

                }
            }
        }

        private Boolean NonIndennizzabile(DateTime giornoAttuale, DateTime dataInizioCert, DateTime dataFineCert, DateTime dataRilascio, Boolean? ricovero)
        {
            Boolean outVal = true;

            Boolean isRcovero = ricovero.HasValue ? ricovero.Value : false;

            if (!isRcovero)
            {
                if (dataInizioCert.AddDays(2) <= dataRilascio)
                {
                    dataInizioCert = dataRilascio;
                }
            }

            if (giornoAttuale >= dataInizioCert && giornoAttuale <= dataFineCert)
            {
                outVal = false;
            }
            else
            {
                outVal = true;
            }
            return outVal;
        }
    }

    public class AssenzaGrigliaTemplate : ITemplate
    {
        public String progressivo { get; set; }

        protected TextBox textBoxAssenza;
        protected TextBox textBoxGiustificativo;
        protected CheckBox boolNonIndennizzabile;

        public AssenzaGrigliaTemplate(String progressivo)
        {
            this.progressivo = progressivo;
        }

        public void InstantiateIn(Control container)
        {
            textBoxAssenza = new TextBox();
            textBoxAssenza.ID = String.Format("Assenza{0}", progressivo);
            textBoxAssenza.Width = new Unit("11px");
            textBoxAssenza.Enabled = false;

            textBoxGiustificativo = new TextBox();
            textBoxGiustificativo.ID = String.Format("Giustificativo{0}", progressivo);
            textBoxGiustificativo.Width = new Unit("11px");
            textBoxGiustificativo.Enabled = false;

            boolNonIndennizzabile = new CheckBox();
            boolNonIndennizzabile.ID = String.Format("NonIndennizzabile{0}", progressivo);
            //boolNonIndennizzabile.Width = new Unit("10px");

            Table table = new Table();
            //table.CellPadding = 0;
            //table.CellSpacing = 0;
            table.Style.Add("padding", "0px");
            table.Width = new Unit("100%");

            TableRow row1 = new TableRow();
            TableRow row2 = new TableRow();
            TableRow row3 = new TableRow();

            TableCell cell1 = new TableCell();
            TableCell cell2 = new TableCell();
            TableCell cell3 = new TableCell();

            cell1.HorizontalAlign = HorizontalAlign.Center;
            cell2.HorizontalAlign = HorizontalAlign.Center;
            cell3.HorizontalAlign = HorizontalAlign.Center;
            cell1.Style.Add("padding", "0px");
            cell2.Style.Add("padding", "0px");
            cell3.Style.Add("padding", "0px");
            //cell3.Style.Add("width","9px");


            cell1.Controls.Add(textBoxAssenza);
            cell2.Controls.Add(textBoxGiustificativo);
            cell3.Controls.Add(boolNonIndennizzabile);

            row1.Cells.Add(cell1);
            row2.Cells.Add(cell2);
            row3.Cells.Add(cell3);
            table.Rows.Add(row1);
            table.Rows.Add(row2);
            table.Rows.Add(row3);

            container.Controls.Add(table);

        }
    }

    public class AssenzaGrigliaDescrizioneTemplate : ITemplate
    {
        protected LiteralControl lControlAssenza;
        protected LiteralControl lControlGiustificativo;
        protected LiteralControl lControlNonIndennizzabile;

        public AssenzaGrigliaDescrizioneTemplate()
        {
        }

        public void InstantiateIn(Control container)
        {
            lControlAssenza = new LiteralControl();
            lControlAssenza.ID = "Assenza";
            lControlAssenza.Text = "Assenza";

            lControlGiustificativo = new LiteralControl();
            lControlGiustificativo.ID = "Giustificativo";
            lControlGiustificativo.Text = "Giustificativi";

            lControlNonIndennizzabile = new LiteralControl();
            lControlNonIndennizzabile.ID = "NonIndennizzabile";
            lControlNonIndennizzabile.Text = "Non indennizzabile";
            //lControlNonIndennizzabile.



            Table table = new Table();
            //table.CellPadding = 0;
            //table.CellSpacing = 0; 
            table.Style.Add("padding", "0px");
            table.Width = new Unit("115px");

            TableRow row1 = new TableRow();
            TableRow row2 = new TableRow();
            TableRow row3 = new TableRow();

            TableCell cell1 = new TableCell();
            TableCell cell2 = new TableCell();
            TableCell cell3 = new TableCell();
            //cell3.Width = new Unit("10px");

            cell1.Controls.Add(lControlAssenza);
            cell2.Controls.Add(lControlGiustificativo);
            cell3.Controls.Add(lControlNonIndennizzabile);

            row1.Cells.Add(cell1);
            row2.Cells.Add(cell2);
            row3.Cells.Add(cell3);
            table.Rows.Add(row1);
            table.Rows.Add(row2);
            table.Rows.Add(row3);

            container.Controls.Add(table);

        }
    }
}