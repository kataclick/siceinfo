﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AltriDocumentiLista.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls.AltriDocumentiLista" %>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" />
<telerik:RadScriptBlock>
    <script type="text/javascript">
        //the following code use radconfirm to mimic the blocking of the execution thread.
        //The approach has the following limitations:
        //1. It works inly for elements that have *click* method, e.g. links and buttons
        //2. It cannot be used in if(!confirm) checks
        window.blockConfirm = function (text, mozEvent, oWidth, oHeight, callerObj, oTitle) {
            var ev = mozEvent ? mozEvent : window.event; //Moz support requires passing the event argument manually 
            //Cancel the event 
            ev.cancelBubble = true;
            ev.returnValue = false;
            if (ev.stopPropagation) ev.stopPropagation();
            if (ev.preventDefault) ev.preventDefault();

            //Determine who is the caller 
            var callerObj = ev.srcElement ? ev.srcElement : ev.target;

            //Call the original radconfirm and pass it all necessary parameters 
            if (callerObj) {
                //Show the confirm, then when it is closing, if returned value was true, automatically call the caller's click method again. 
                var callBackFn = function (arg) {
                    if (arg) {
                        callerObj["onclick"] = "";
                        if (callerObj.click) callerObj.click(); //Works fine every time in IE, but does not work for links in Moz 
                        else if (callerObj.tagName == "A") //We assume it is a link button! 
                        {
                            try {
                                eval(callerObj.href)
                            }
                            catch (e) { }
                        }
                    }
                }

                radconfirm(text, callBackFn, oWidth, oHeight, callerObj, oTitle);
            }
            return false;
        } 
    </script>
</telerik:RadScriptBlock>
<table class="borderedTable">
    <tr>
        <td style="font-weight: bold">
            Elenco altri documenti
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadGrid ID="RadGridAltriDocumenti" runat="server" GridLines="None" CellSpacing="0"
                OnItemCommand="RadGridAltriDocumenti_ItemCommand" 
                onitemdatabound="RadGridAltriDocumenti_ItemDataBound">
                <MasterTableView DataKeyNames="Id">
                    <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="Descrizione" UniqueName="descrizione" DataField="Descrizione">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="visualizza" FilterControlAltText="Filter visualizza column"
                            ImageUrl="../../images/pdf24.png" UniqueName="visualizza">
                            <ItemStyle Width="10px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                            UniqueName="elimina">
                            <ItemTemplate>
                                <asp:ImageButton CssClass="PostbackButton" ID="ButtonElimina" runat="server" ImageUrl="../../images/editdelete.png"
                                    OnClientClick="return blockConfirm('Sei sicuro di voler eliminare il documento?', event, 330, 100,'','Conferma eliminazione');"
                                    OnClick="ButtonElimina_Click" CommandName="elimina" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
                <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
                </HeaderContextMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="RadButtonAggiungiDocumenti" runat="server" Text="Aggiungi altri documenti"
                OnClick="RadButtonAggiungiDocumenti_Click">
            </asp:Button>
        </td>
    </tr>
</table>
