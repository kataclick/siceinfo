﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Delegates;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;
using System.IO;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Business.Archidoc;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls
{
    public partial class GiustificativiLista : System.Web.UI.UserControl
    {
        public event CertificatiSelectedEventHandler OnCertificatoSelected;
        public event CertificatiReturnedEventHandler OnCertificatiReturned;

        private readonly BusinessEF bizEF = new BusinessEF();
        private readonly Cemi.MalattiaTelematica.Business.Business biz = new Cemi.MalattiaTelematica.Business.Business();

        protected void Page_Load(object sender, EventArgs e)
        {
        }


        public void CaricaGiustificativi(List<MalattiaTelematicaCertificatoMedico> certificatiMedici, int idAssenza, string codiceFiscale, Boolean abilita)
        {
            ViewState["IdAssenza"] = idAssenza;
            ViewState["CF"] = codiceFiscale;
            ViewState["AbilitaControlli"] = abilita;

            Presenter.CaricaElementiInGridView(RadGridGiustificativi, certificatiMedici);

            RadButtonAggiungiGiustificativi.Enabled = abilita;
        }


        protected void RadGridGiustificativi_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                MalattiaTelematicaCertificatoMedico cert = (MalattiaTelematicaCertificatoMedico)e.Item.DataItem;
                Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
                Label lGG = (Label)e.Item.FindControl("LabelGG");
                Image iSemaforo = (Image)e.Item.FindControl("ImageSemaforo");
                ImageButton ibElimina = (ImageButton)e.Item.FindControl("ButtonElimina");
                // Periodo e GG
                if (cert.DataInizio.HasValue && cert.DataFine.HasValue)
                {
                    lPeriodo.Text = String.Format("{0} - {1}",
                        cert.DataInizio.Value.ToString("dd/MM/yyyy"),
                        cert.DataFine.Value.ToString("dd/MM/yyyy"));

                    TimeSpan diff = cert.DataFine.Value.Subtract(cert.DataInizio.Value);
                    lGG.Text = (diff.Days + 1).ToString();
                }

                if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
                {
                    iSemaforo.Visible = false;
                }
                else
                {
                    iSemaforo.Visible = true;
                    iSemaforo.ImageUrl = biz.GetSemaforoCertificatoMedicoINPS(cert, ViewState["CF"].ToString());
                }
                ibElimina.Enabled = (Boolean)ViewState["AbilitaControlli"];
            }
        }

        protected void RadGridGiustificativi_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "elimina":
                    {
                        Int32 idCertificato = (Int32)
                                                 RadGridGiustificativi.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                        bizEF.DeleteCertificatoMedico(idCertificato);
                        //int idAssenza = (Int32) ViewState["IdAssenza"];
                        //List<MalattiaTelematicaCertificatoMedico> cert = biz.GetCertificatiMedici(idAssenza);

                        //CaricaGiustificativi(cert, idAssenza);
                        if (OnCertificatiReturned != null)
                        {
                            OnCertificatiReturned(true);
                        }
                        //CaricaGiustificativi(aricaGiustificativi());
                    }
                    break;
                case "visualizza":
                    {
                        Int32 idCertificato = (Int32)
                                                 RadGridGiustificativi.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];

                        RestituisciFile(bizEF.GetImmagineCertificatoMedico(idCertificato));
                    }
                    break;

                case "modifica":
                    {
                        Int32 idCertificato = (Int32)
                                                 RadGridGiustificativi.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Id"];


                        if (OnCertificatoSelected != null)
                        {
                            OnCertificatoSelected(idCertificato);
                        }
                    }
                    break;
            }
        }

        private void RestituisciFile(Immagine img)
        {
            string idArchidoc = img.IdArchidoc;

            if (idArchidoc != null)
            {
                // Caricare il documento tramite il Web Service Archidoc
                try
                {
                    IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                    byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                    if (file != null)
                    {
                        Presenter.RestituisciFileArchidoc(idArchidoc, file, Path.GetExtension(img.NomeFile).ToUpper(), this.Page);
                    }
                }
                catch
                {
                    //    LabelErroreVisualizzazioneDocumento.Visible = true;
                }

            }
            else
            {
                if (img.NomeFile != null)
                {
                    try
                    {
                        //Set the appropriate ContentType.
                        Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", img.NomeFile));
                        switch (Path.GetExtension(img.NomeFile).ToUpper())
                        {
                            case ".XML":
                                Response.ContentType = "text/plain";
                                break;
                            case ".PDF":
                                Response.ContentType = "application/pdf";
                                break;
                            default:
                                Response.ContentType = "application/download";
                                break;

                        }
                        //Write the file directly to the HTTP content output stream.
                        Response.BinaryWrite(img.File);
                        Response.Flush();
                        Response.End();
                    }
                    catch
                    {

                    }
                }
            }
        }

        //protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        MalattiaTelematicaCertificatoMedico cert = (MalattiaTelematicaCertificatoMedico)e.Item.DataItem;
        //        Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
        //        Label lGG = (Label)e.Item.FindControl("LabelGG");

        //        // Periodo e GG
        //        if (cert.DataInizio.HasValue && cert.DataFine.HasValue)
        //        {
        //            lPeriodo.Text = String.Format("{0} - {1}",
        //                cert.DataInizio.Value.ToString("dd/MM/yyyy"),
        //                cert.DataFine.Value.ToString("dd/MM/yyyy"));

        //            TimeSpan diff = cert.DataFine.Value.Subtract(cert.DataInizio.Value);
        //            lGG.Text = (diff.Days + 1).ToString();
        //        }


        //    }
        //}

        protected void RadButtonAggiungiGiustificativi_Click(object sender, EventArgs e)
        {
            if (OnCertificatoSelected != null)
            {
                OnCertificatoSelected(-1);
            }
        }

        protected void ButtonElimina_Click(object sender, EventArgs e)
        {

        }
    }
}