﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ControlliAssenzaImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls.ControlliAssenzaImpresa" %>

<table class="borderedTable">
    <colgroup>
        <col style="width:300px" />
        <col style="width:40px" />
        <col style="width:160px" />
        <col style="width:30px" />
        <col />
    </colgroup>
    <tr style="height: 25px">
        <td>
            Congruenza periodi assenza/certificati
        </td>
        <td>
            <asp:Image ID="ImageCongruenzaPeriodi" runat="server" ImageUrl="../../images/semaforoVerde.png" />
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr style="height: 25px">
        <td>
            Sovrapposizione periodi di assenza
        </td>
        <td>
            <asp:Image ID="ImageSovrapposizioneAssenza" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr style="height: 25px">
        <td>
            Ore lavorate
        </td>
        <td>
            <asp:Image ID="ImageControlloOre" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
        </td>
        <td>
            <asp:Button ID="ButtonControlloOreLavorate" runat="server" Text="Ore altre Casse Edili"
                Width="200px" OnClick="ButtonControlloOreLavorate_Click" />
        </td>
        <td>
        </td>
        <td style="font-weight: bold">
            <asp:Label ID="LabelOreLavorate" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td>
            Impresa regolare
        </td>
        <td>
            <asp:Image ID="ImageControlloImpresaRegolare" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
        </td>
        <td>
            <asp:Button ID="ButtonFreccia" runat="server" Text="Bollettino Freccia" Width="200px" />
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr style="height: 25px">
        <td>
            Rapporto di Lavoro
        </td>
        <td>
            <asp:Image ID="ImageControlloRapportoLavoro" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr style="height: 25px">
        <td>
            Ricaduta malattia oltre 30 gg
        </td>
        <td>
            <asp:Image ID="ImageRicadutaOltre30GG" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
            <asp:Label ID="LabelRicadutaOltre30GG" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr style="height: 25px">
        <td>
            Continuazione/Ricaduta senza evento precedente
        </td>
        <td>
            <asp:Image ID="ImageAltraCE" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
        </td>
        <td>
            <asp:Button ID="ButtonAltreCE" runat="server" Text="Aggiungi Assenza presso altra CE"
                OnClick="ButtonAltreCE_Click" />
        </td>
        <td>
        </td>
        <td>
            <asp:Label ID="LabelAltraCE" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
</table>
<asp:Panel ID="PanelAltraCE" runat="server" Visible="false">
    <br />
    <table class="borderedTable">
        <colgroup>
            <col />
            <col width="400px" />
        </colgroup>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelTitolo" runat="server" Text="Inserimento assenza altra Cassa Edile"
                    Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <colgroup>
                        <col width="180px" />
                        <col />
                        <col />
                    </colgroup>
                    <tr>
                        <td>
                            Cassa Edile
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="DropDownListCassaEdile" runat="server" AppendDataBoundItems="true"
                                Width="400px">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Periodo da richiesto"
                                ValidationGroup="altraCE" ControlToValidate="DropDownListCassaEdile">**</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Periodo dal
                        </td>
                        <td colspan="2">
                            <telerik:RadDatePicker ID="RadDatePickerPeriodoDa" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Periodo da richiesto"
                                ControlToValidate="RadDatePickerPeriodoDa" ValidationGroup="altraCE">**</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Periodo al
                        </td>
                        <td colspan="2">
                            <telerik:RadDatePicker ID="RadDatePickerPeriodoA" runat="server">
                            </telerik:RadDatePicker>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Periodo a richiesto"
                                ControlToValidate="RadDatePickerPeriodoA" ValidationGroup="altraCE">**</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadDatePickerPeriodoDa"
                                ControlToValidate="RadDatePickerPeriodoA" EnableClientScript="False" ErrorMessage="Limiti periodi errati"
                                Operator="GreaterThanEqual" ValidationGroup="altraCE" Type="Date">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryAltraCe" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="altraCE" />
                        </td>
                        <td>
                            <asp:Button ID="RadButtonConfermaAltraCe" runat="server" Text="Conferma" ValidationGroup="altraCE"
                                OnClick="RadButtonConfermaAltraCe_Click">
                            </asp:Button>
                            <asp:Button ID="RadButtonAnnullaAltraCe" runat="server" OnClick="RadButtonAnnullaAltraCe_Click"
                                Text="Annulla">
                            </asp:Button>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="LabelMessaggio" runat="server" />
                        </td>
                        <td>
                            <asp:Button ID="RadButtonOK" runat="server" OnClick="RadButtonOK_Click" Text="OK"
                                Width="100%" Font-Bold="False" Visible="False">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="font-weight: bold; color: #FF0000">
                - Inserire Cassa Edile<br />
                - Inserire il periodo (dal - al)<br />
                - Premere il pulsante &quot;Conferma&quot;<br />
                - Premere il pulsante &quot;Torna alla lista&quot;<br />
                - Selezionare il periodo di assenza presso altra Cassa Edile<br />
                - Inserire i giustificativi<br />
                - Inviare domanda a Cassa Edile<br />
                - Successivamente ritornare a gestire l&#39;assenza che segue questo periodo
            </td>
        </tr>
    </table>
</asp:Panel>
