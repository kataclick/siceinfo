﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GestioneGiustificativi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls.GestioneGiustificativi" %>

<table class="borderedTable">
        <colgroup>
            <col style="width:200px" />
            <col style="width:200px" />
            <col style="width:200px" />
            <col />
        </colgroup>
        <tr>
            <td>
                <asp:Label ID="LabelTitolo" runat="server" Text="Certificato medico" Font-Bold="True"></asp:Label>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Tipo assenza:
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonMalattia" runat="server" GroupName="Assenza" Text="Malattia"
                    AutoPostBack="false" Checked="True" />
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonInfortunio" runat="server" GroupName="Assenza" Text="Infortunio"
                    AutoPostBack="false" />
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonMalattiaProfessionale" runat="server" GroupName="Assenza"
                    Text="Malattia professionale" AutoPostBack="false" />
            </td>
        </tr>
        <tr>
            <td>
                Numero certificato:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxNumero" runat="server" MaxLength="15">
                </telerik:RadTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Numero certificato richiesto"
                    ValidationGroup="CertificatoMedico" ControlToValidate="RadTextBoxNumero">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="ButtonCaricaDatiInps" runat="server" Text="Carica dati INPS"  OnClick="ButtonCaricaDatiInps_Click"/>
            </td>
            <td>
        
            </td>
        </tr>
        <tr>
            <td>
                Data inizio:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerInizio" runat="server">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Data inizio richiesta"
                    ValidationGroup="CertificatoMedico" ControlToValidate="RadDatePickerInizio">*</asp:RequiredFieldValidator>
            </td>
            <td>
                Data fine:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerFine" runat="server">
                </telerik:RadDatePicker>
                <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadDatePickerInizio"
                    ControlToValidate="RadDatePickerFine" EnableClientScript="False" ErrorMessage="Limiti periodi errati"
                    Operator="GreaterThanEqual" ValidationGroup="CertificatoMedico">**
                </asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Data fine richiesta"
                    ValidationGroup="CertificatoMedico" ControlToValidate="RadDatePickerFine">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Data rilascio:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerRilascio" runat="server">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Data rilascio richiesta"
                    ValidationGroup="CertificatoMedico" ControlToValidate="RadDatePickerRilascio">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxRicovero" runat="server" Text="Ricovero/Dimissioni" />
                </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Tipo certificato:
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonInizio" runat="server" GroupName="Certificato" Text="Inizio"
                    AutoPostBack="false" Checked="True" />
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonContinuazione" runat="server" GroupName="Certificato"
                    Text="Continuazione" AutoPostBack="false" />
            </td>
            <td>
                <asp:RadioButton ID="RadioButtonRicaduta" runat="server" GroupName="Certificato"
                    Text="Ricaduta" AutoPostBack="false" />
            </td>
        </tr>
        <tr>
            <td>
                Immagine documento
            </td>
            <td colspan="2">
                <asp:FileUpload ID="FileUploadCertificatoMedico" runat="server" Width="100%" />
                <asp:CustomValidator ID="CustomValidatorAllegato" runat="server" ErrorMessage="Selezionare un file"
                    ValidationGroup="CertificatoMedico" OnServerValidate="CustomValidatorAllegato_ServerValidate">**
                </asp:CustomValidator>
            </td>
            <td>
                <asp:Image ID="ImageAllegato" runat="server" ImageUrl="../../images/allegato.gif" />
                <asp:Label ID="LabelFileName" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="RadButtonConferma" runat="server" Text="Conferma" ValidationGroup="CertificatoMedico"
                    OnClick="RadButtonConferma_Click">
                </asp:Button>
                <asp:Button ID="RadButtonIndietro" runat="server" Text="Indietro" OnClick="RadButtonIndietro_Click">
                </asp:Button>
                <asp:CustomValidator ID="CustomValidatorDate" runat="server" ErrorMessage="Date fuori periodo assenza"
                    ValidationGroup="CertificatoMedico" OnServerValidate="CustomValidatorDate_ServerValidate">&nbsp;
                </asp:CustomValidator>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummaryCertificato" runat="server" CssClass="messaggiErrore"
                    ValidationGroup="CertificatoMedico" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelMessaggio" runat="server" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:Button ID="RadButtonOK" runat="server" Text="OK" OnClick="RadButtonOK_Click"
                    Width="100%" Font-Bold="False" Visible="False">
                </asp:Button>
            </td>
            <td>
            </td>
        </tr>

</table>
