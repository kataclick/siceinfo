﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GestioneAltriDocumenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.WebControls.GestioneAltriDocumenti" %>

<table class="borderedTable">
    <tr>
        <td>
            <asp:Label ID="LabelTitolo" runat="server" Text="Altri documenti" Font-Bold="True"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Descrizione
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxNome" runat="server">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadTextBoxNome"
                ErrorMessage="Nome documento richiesto" ValidationGroup="AltroDocumento">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Selezione immagine
        </td>
        <td>
            <asp:FileUpload ID="FileUploadAltroDocumento" runat="server" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Selezionare un file"
                ControlToValidate="FileUploadAltroDocumento" ValidationGroup="AltroDocumento">**
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="RadButtonConferma" runat="server" Text="Conferma" ValidationGroup="AltroDocumento"
                OnClick="RadButtonConferma_Click"></asp:Button>
            <asp:Button ID="RadButtonIndietro" runat="server" Text="Indietro" OnClick="RadButtonIndietro_Click">
            </asp:Button>
        </td>
        <td>
            <asp:ValidationSummary ID="ValidationSummaryAltroDocumento" runat="server" CssClass="messaggiErrore"
                ValidationGroup="AltroDocumento" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelMessaggio" runat="server"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
</table>
