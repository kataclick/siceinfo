﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true"  CodeBehind="RicercaAssenze.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.RicercaAssenze" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/RicercaAssenzeImprese.ascx" TagName="RicercaAssenzeImprese"
    TagPrefix="uc2" %>
<%@ Register src="../WebControls/MenuMalattiaTelematica.ascx" tagname="MenuMalattiaTelematica" tagprefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Gestione assenze" />
    <br />
    <uc2:RicercaAssenzeImprese ID="RicercaAssenzeImprese1" runat="server" />
  
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
