﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica
{
    public partial class VisualizzaEstrattoConto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Int32 numeroProtocollo = 0;
                Int32 annoProtocollo = 0;
                Int32 idAssenza = -1;

                if (Context.Items["NumeroProtocollo"] != null)
                {
                    ViewState["NumeroProtocollo"] = (int)Context.Items["NumeroProtocollo"]; ;
                }

                if (Context.Items["AnnoProtocollo"] != null)
                {
                    ViewState["AnnoProtocollo"] = (int)Context.Items["AnnoProtocollo"]; ;
                }

                if (Context.Items["IdAssenza"] != null)
                {
                    ViewState["IdAssenza"] = (int)Context.Items["IdAssenza"]; ;
                }
                numeroProtocollo = (Int32)ViewState["NumeroProtocollo"];
                annoProtocollo = (Int32)ViewState["AnnoProtocollo"];
                idAssenza = (Int32)ViewState["IdAssenza"];


                ReportViewerEstrattoConto.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewerEstrattoConto.ServerReport.ReportPath = "/ReportMalattiaTelematica/EstrattoConto";

                ReportParameter[] listaParam = new ReportParameter[3];
                listaParam[0] = new ReportParameter("numeroProt", numeroProtocollo.ToString());
                listaParam[1] = new ReportParameter("annoProt", annoProtocollo.ToString());
                listaParam[2] = new ReportParameter("idAssenza", idAssenza.ToString());
                ReportViewerEstrattoConto.ServerReport.SetParameters(listaParam);


                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                //PDF

                byte[] bytes = ReportViewerEstrattoConto.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);


                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", "attachment;filename=EstrattoConto.pdf");
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }
    }
}