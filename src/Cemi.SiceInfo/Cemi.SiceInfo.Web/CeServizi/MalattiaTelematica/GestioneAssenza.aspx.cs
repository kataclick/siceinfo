﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Business;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica
{
    public partial class GestioneAssenza : System.Web.UI.Page
    {
        private readonly Cemi.MalattiaTelematica.Business.Business _biz = new Cemi.MalattiaTelematica.Business.Business();
        private readonly BusinessEF _bizEf = new BusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            GiustificativiLista1.OnCertificatoSelected += GiustificativiLista1_OnCertificatoSelected;
            AltriDocumentiLista1.OnAltroDocumentoSelected += AltriDocumentiLista1_OnAltroDocumentoSelected;

            GestioneAltriDocumenti1.OnAltriDocumentiReturned += GestioneAltriDocumenti1_OnAltriDocumentiReturned;
            GestioneGiustificativi1.OnCertificatiReturned += GestioneGiustificativi1_OnCertificatiReturned;
            GiustificativiLista1.OnCertificatiReturned += GestioneGiustificativi1_OnCertificatiReturned;


            if (!Page.IsPostBack)
            {
                int idAssenza = -1;
                int provenienza = 1;

                if (Context.Items["IdAssenza"] != null)
                {
                    idAssenza = (int)Context.Items["IdAssenza"];
                    ViewState["IdAssenza"] = idAssenza;
                }
                if (Context.Items["Provenienza"] != null)
                {
                    provenienza = (int)Context.Items["Provenienza"];
                }
                ViewState["Provenienza"] = provenienza;

                if (Request.QueryString["IdAssenza"] != null)
                {
                    idAssenza = Convert.ToInt32(Request.QueryString["IdAssenza"]);
                    ViewState["IdAssenza"] = idAssenza;
                }

                //if (Context.Items["FiltroIdLavoratore"] != null)
                //{
                //    ViewState["FiltroIdLavoratore"] = Context.Items["FiltroIdLavoratore"];
                //}
                //if (Context.Items["FiltroCognome"] != null)
                //{
                //    ViewState["FiltroCognome"] = Context.Items["FiltroCognome"];
                //}
                //if (Context.Items["FiltroNome"] != null)
                //{
                //    ViewState["FiltroNome"] = Context.Items["FiltroNome"];
                //}
                //if (Context.Items["FiltroDataNascita"] != null)
                //{
                //    ViewState["FiltroDataNascita"] = Context.Items["FiltroDataNascita"];
                //}
                //if (Context.Items["FiltroStatoAssenza"] != null)
                //{
                //    ViewState["FiltroStatoAssenza"] = Context.Items["FiltroStatoAssenza"];
                //}
                //if (Context.Items["FiltroTipoAssenza"] != null)
                //{
                //    ViewState["FiltroTipoAssenza"] = Context.Items["FiltroTipoAssenza"];
                //}
                //if (Context.Items["FiltroPeriodoDa"] != null)
                //{
                //    ViewState["FiltroPeriodoDa"] = Context.Items["FiltroPeriodoDa"];
                //}
                //if (Context.Items["FiltroPeriodoA"] != null)
                //{
                //    ViewState["FiltroPeriodoA"] = Context.Items["FiltroPeriodoA"];
                //}
                //if (Context.Items["FiltroRagioneSociale"] != null)
                //{
                //    ViewState["FiltroRagioneSociale"] = Context.Items["FiltroRagioneSociale"];
                //}
                //if (Context.Items["FiltroCodiceFiscale"] != null)
                //{
                //    ViewState["FiltroCodiceFiscale"] = Context.Items["FiltroCodiceFiscale"];
                //}
                //if (Context.Items["FiltroIdImpresa"] != null)
                //{
                //    ViewState["FiltroIdImpresa"] = Context.Items["FiltroIdImpresa"];
                //}


                //if (Request.QueryString["idImpresa"] != null)
                //{
                //    ViewState["FiltroIdImpresa"] = Request.QueryString["idImpresa"];
                //} 
                //if (Request.QueryString["codiceFiscale"] != null)
                //{
                //    ViewState["FiltroCodiceFiscale"] = Request.QueryString["codiceFiscale"];
                //}
                //if (Request.QueryString["ragioneSociale"] != null)
                //{
                //    ViewState["FiltroRagioneSociale"] = Request.QueryString["ragioneSociale"];
                //}
                //if (Request.QueryString["periodoA"] != null)
                //{
                //    ViewState["FiltroPeriodoA"] = Request.QueryString["periodoA"];
                //}
                //if (Request.QueryString["periodoDa"] != null)
                //{
                //    ViewState["FiltroPeriodoDa"] = Request.QueryString["periodoDa"];
                //}
                //if (Request.QueryString["tipoAssenza"] != null)
                //{
                //    ViewState["FiltroTipoAssenza"] = Request.QueryString["tipoAssenza"];
                //}
                //if (Request.QueryString["statoAssenza"] != null)
                //{
                //    ViewState["FiltroStatoAssenza"] = Request.QueryString["statoAssenza"];
                //}
                //if (Request.QueryString["dataNascita"] != null)
                //{
                //    ViewState["FiltroDataNascita"] = Request.QueryString["dataNascita"];
                //}
                //if (Request.QueryString["nome"] != null)
                //{
                //    ViewState["FiltroNome"] = Request.QueryString["nome"];
                //}
                //if (Request.QueryString["cognome"] != null)
                //{
                //    ViewState["FiltroCognome"] = Request.QueryString["cognome"];
                //}
                //if (Request.QueryString["idLavoratore"] != null)
                //{
                //    ViewState["FiltroIdLavoratore"] = Request.QueryString["idLavoratore"];
                //}
                if (Request.QueryString["inviata"] != null)
                {
                    ViewState["EmailInviata"] = Request.QueryString["inviata"];
                }


                AssenzaDettagli assenzaDettagli = _bizEf.GetAssenzaDettagli(idAssenza);

                ViewState["IdLavoratore"] = assenzaDettagli.IdLavoratore;
                ViewState["IdAssenza"] = assenzaDettagli.IdAssenza;
                ViewState["Ricaduta"] = assenzaDettagli.Ricaduta;
                ViewState["DataInizioMalattia"] = assenzaDettagli.DataInizioMalattia;
                ViewState["DataInizioAssenza"] = assenzaDettagli.DataInizioAssenza;
                ViewState["DataFineAssenza"] = assenzaDettagli.DataFineAssenza;
                ViewState["DataAssunzione"] = assenzaDettagli.DataAssunzione;
                ViewState["IdStato"] = assenzaDettagli.IdStato;
                ViewState["IdImpresa"] = assenzaDettagli.IdImpresa;
                //ViewState["IdUtenteInCarico"] = assenzaDettagli.IdUtenteInCarico;
                ViewState["InizioRapporto"] = assenzaDettagli.DataInizio;
                ViewState["FineRapporto"] = assenzaDettagli.DataFine;
                ViewState["IdTipo"] = assenzaDettagli.IdTipo;

                List<MalattiaTelematicaCertificatoMedico> cert = _bizEf.GetCertificatiMedici(idAssenza);

                Boolean abilita = _biz.AbilitaControllo(assenzaDettagli.IdStato);//, assenzaDettagli.IdUtenteInCarico);

                GiustificativiLista1.CaricaGiustificativi(cert, idAssenza, assenzaDettagli.CodiceFiscale, abilita);

                SituazioneAssenza1.CaricaDati(assenzaDettagli, cert);

                DettagliAssenza1.CaricaAssenza(assenzaDettagli);

                GestioneGiustificativi1.CaricaAssenza(assenzaDettagli);

                GestioneAltriDocumenti1.CaricaAssenza(assenzaDettagli);

                AltriDocumentiLista1.CaricaAltriDocumenti(idAssenza, abilita);

                ControlliAssenzaImpresa1.CaricaAssenza(assenzaDettagli);

                GestioneModifica(assenzaDettagli.IdStato, assenzaDettagli.IdUtenteInCarico);

                TextBoxNote.Text = assenzaDettagli.Note;
                CheckBoxTelefonata.Checked = (Boolean)assenzaDettagli.Telefonata;
                CheckBoxEmail.Checked = (Boolean)assenzaDettagli.Email;


                if (assenzaDettagli.DataInAttesa != null)
                    TextBoxInAttesa.Text = assenzaDettagli.DataInAttesa.Value.ToShortDateString();

                if (assenzaDettagli.DataInvioPrimoSollecito != null)
                    TextBoxPrimoSollecito.Text = assenzaDettagli.DataInvioPrimoSollecito.Value.ToShortDateString();

                if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
                {
                    trButtonBackOffice.Visible = false;

                    //ButtonInAttesa.Visible = false;
                    //ButtonAnnullaAssenza.Visible = false;
                    //ButtonImmessaPervenuta.Visible = false;

                    tableNote.Visible = false;

                    ButtonAccogli.Text = "Invia domanda a Cassa Edile Milano";
                }
                else
                {
                    trButtonBackOffice.Visible = true;
                    //ButtonInAttesa.Visible = true;
                    //ButtonAnnullaAssenza.Visible = true;
                    //ButtonImmessaPervenuta.Visible = true;

                    if (assenzaDettagli.IdStato == "8")
                    {
                        ButtonInAttesa.Enabled = false;

                        if (assenzaDettagli.DataInAttesa != null)
                        {
                            ButtonPrimoSollecito.Enabled = assenzaDettagli.DataInAttesa.Value < DateTime.Now.AddMonths(-3);
                            ButtonRespingi.Enabled = false;
                        }


                        if (assenzaDettagli.DataInvioPrimoSollecito != null)
                        {
                            ButtonPrimoSollecito.Enabled = false;

                            ButtonRespingi.Enabled = assenzaDettagli.DataInvioPrimoSollecito.Value < DateTime.Now.AddMonths(-3);
                        }

                    }

                    if (assenzaDettagli.IdStato == "A")
                    {
                        ButtonAnnullaAssenza.Enabled = false;
                    }

                    if (assenzaDettagli.IdStato == "C")
                        ButtonImmessaPervenuta.Enabled = true;

                    tableNote.Visible = true;

                    ButtonAccogli.Text = "Accogli domanda";
                }
            }
        }

        private void GestioneModifica(string stato, Int32? idUtenteInCarico)
        {
            Boolean abilitaControlli = _biz.AbilitaControllo(stato);
            ButtonSalvaNonIndennizzabili.Enabled = abilitaControlli;
            ButtonAccogli.Enabled = abilitaControlli;

            ButtonInAttesa.Enabled = abilitaControlli;
            ButtonAnnullaAssenza.Enabled = abilitaControlli;
            ButtonImmessaPervenuta.Enabled = abilitaControlli;
            ButtonPrimoSollecito.Enabled = abilitaControlli;
            ButtonRespingi.Enabled = abilitaControlli;


            if (!GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
            {
                if (idUtenteInCarico != null)
                {
                    if (GestioneUtentiBiz.GetIdUtente() != idUtenteInCarico)
                    {
                        ButtonAccogli.Enabled = false;
                        ButtonInAttesa.Enabled = false;
                        ButtonAnnullaAssenza.Enabled = false;
                        ButtonImmessaPervenuta.Enabled = false;
                        ButtonPrimoSollecito.Enabled = false;
                        ButtonRespingi.Enabled = false;
                    }
                }
            }
            if (abilitaControlli)
            {
                // disabilito se impresa o consulente.
                if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
                {
                    abilitaControlli = false;
                }
            }

            DettagliAssenza1.EnabledOreSettimanali(abilitaControlli);
        }

        private void GiustificativiLista1_OnCertificatoSelected(Int32 id)
        {
            PanelGiustificativi.Visible = true;
            PanelAltriDocumenti.Visible = false;

            GestioneGiustificativi1.CaricaGiustificativo(id);
        }


        private void AltriDocumentiLista1_OnAltroDocumentoSelected()
        {
            PanelGiustificativi.Visible = false;
            PanelAltriDocumenti.Visible = true;
        }

        private void GestioneAltriDocumenti1_OnAltriDocumentiReturned(Boolean carica)
        {
            PanelGiustificativi.Visible = false;
            PanelAltriDocumenti.Visible = false;

            if (carica)
            {
                Int32 idAssenza = (Int32)ViewState["IdAssenza"];
                AssenzaDettagli assenzaDettagli = _bizEf.GetAssenzaDettagli(idAssenza);
                AltriDocumentiLista1.CaricaAltriDocumenti(idAssenza, _biz.AbilitaControllo(assenzaDettagli.IdStato));
            }
        }


        private void GestioneGiustificativi1_OnCertificatiReturned(Boolean carica)
        {
            PanelGiustificativi.Visible = false;
            PanelAltriDocumenti.Visible = false;

            if (carica)
            {
                Int32 idAssenza = (Int32)ViewState["IdAssenza"];

                AssenzaDettagli assenzaDettagli = _bizEf.GetAssenzaDettagli(idAssenza);

                ControlliAssenzaImpresa1.CaricaAssenza(assenzaDettagli);

                List<MalattiaTelematicaCertificatoMedico> cert = _bizEf.GetCertificatiMedici(idAssenza);

                GiustificativiLista1.CaricaGiustificativi(cert, idAssenza, assenzaDettagli.CodiceFiscale,
                                                          _biz.AbilitaControllo(assenzaDettagli.IdStato));

                SituazioneAssenza1.CaricaDati(assenzaDettagli, cert);
            }
        }

        protected void ButtonSalvaNonIndennizzabili_Click(object sender, EventArgs e)
        {
            SituazioneAssenza1.ConfermaGiorni();
        }

        protected void ButtonAccogli_Click(object sender, EventArgs e)
        {
            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
            {
                PanelConferma.Visible = true;
            }
            else
            {
                ConfermaDomanda();
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            PanelConferma.Visible = false;
            LabelMessaggio.Text = "";
        }

        private void ConfermaDomanda()
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];
            Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
            Int32 idImpresa = (Int32)ViewState["IdImpresa"];
            DateTime dataInizioMalattia = (DateTime)ViewState["DataInizioMalattia"];
            DateTime dataInizioAssenza = (DateTime)ViewState["DataInizioAssenza"];
            DateTime dataFineAssenza = (DateTime)ViewState["DataFineAssenza"];
            DateTime? inizioRapporto = (DateTime?)ViewState["InizioRapporto"];
            DateTime? fineRapporto = (DateTime?)ViewState["FineRapporto"];
            Boolean ricaduta = (Boolean)ViewState["Ricaduta"];
            DateTime? dataAssunzione = (DateTime?)ViewState["DataAssunzione"];
            String IdStato = (String)ViewState["IdStato"];
            String tipo = (String)ViewState["IdTipo"];
            Boolean salta = false;
            LabelMessaggio.Text = "";

            if (IdStato != "R" && IdStato != "6" && IdStato != "7")
            {
                //if (dataAssunzione != null)
                //{
                if (!(dataInizioAssenza >= inizioRapporto) || !(dataInizioAssenza <= fineRapporto) ||
                    !(dataFineAssenza >= inizioRapporto) || !(dataFineAssenza <= fineRapporto) ||
                    dataAssunzione == null)
                {
                    salta = true;
                    LabelMessaggio.Text = "Rapporto di lavoro non valido.";
                }

                //}
                //else
                //{
                //    salta = true;
                //    LabelMessaggio.Text = "Rapporto di lavoro non valido.";
                //}


            }
            //if (!salta)
            //{
            //    if (RegolaritaContributivaManager.GetDebitoImpresa(idImpresa, dataInizioMalattia.AddMonths(-4), dataInizioMalattia) >= 30.0M)
            //    {
            //        salta = true;
            //        LabelMessaggio.Text = "Attenzione impresa non regolare.";
            //    }
            //}

            // 
            if (!salta)
            {
                if (_bizEf.AssenzaPrecedenteNonAccolta(idAssenza, idLavoratore, idImpresa, dataInizioMalattia, dataInizioAssenza))
                {
                    salta = true;
                    LabelMessaggio.Text = "Attenzione non è stato confermato il periodo di assenza precedente.";
                }
            }

            if (!salta)
            {
                if (dataInizioMalattia.Date != dataInizioAssenza.Date)
                {
                    if (_bizEf.AssenzaMancante(idAssenza, idLavoratore, dataInizioMalattia, dataInizioAssenza, ricaduta, tipo))
                    {
                        salta = true;
                        LabelMessaggio.Text = "Continuazione/Ricaduta senza evento precedente.";
                    }
                }
            }

            if (!salta)
            {
                if (_bizEf.PeriodiCongruenti(idAssenza, dataInizioMalattia))
                {
                    salta = true;
                    LabelMessaggio.Text = "Data inizio certificato antecedente all'inizio della malattia/infortunio.";
                }
            }

            if (GestioneUtentiBiz.IsConsulente() || GestioneUtentiBiz.IsImpresa())
            {
                salta = false;


                if (_bizEf.GetCertificatiMedici(idAssenza).Count == 0)
                {
                    //    sw_es
                    //}
                    //if (_bizEf.GiustificativiAssenti(idAssenza))
                    //{
                    salta = true;
                    LabelMessaggio.Text = "Attenzione non è stato insertito alcun giustificativo.";
                }
            }
            else
            {
                if (IdStato == "6")
                {
                    salta = false;
                }
            }
            if (!salta)
            {
                foreach (MalattiaTelematicaCertificatoMedico certificatoMedico in _bizEf.GetCertificatiMedici(idAssenza))
                {
                    MalattiaTelematicaCertificatoMedico newCertificatoMedico = new MalattiaTelematicaCertificatoMedico();
                    newCertificatoMedico.Id = certificatoMedico.Id;
                    newCertificatoMedico.DataInizio = certificatoMedico.DataInizio;
                    newCertificatoMedico.DataFine = certificatoMedico.DataFine;
                    newCertificatoMedico.DataRilascio = certificatoMedico.DataRilascio;
                    newCertificatoMedico.Immagine = certificatoMedico.Immagine;
                    newCertificatoMedico.NomeFile = certificatoMedico.NomeFile;
                    newCertificatoMedico.Numero = certificatoMedico.Numero;
                    newCertificatoMedico.Tipo = certificatoMedico.Tipo;
                    newCertificatoMedico.TipoAssenza = certificatoMedico.TipoAssenza;

                    List<MalattiaTelematicaAssenza> assenze = new List<MalattiaTelematicaAssenza>();
                    assenze.Add(new MalattiaTelematicaAssenza { Id = idAssenza });
                    newCertificatoMedico.MalattiaTelematicaAssenze = assenze;

                    _bizEf.UpdateCertificatoMedico(newCertificatoMedico);
                }
                // forzatura giorni non indennizabili
                SituazioneAssenza1.ConfermaGiorni();

                DettagliAssenza1.ConfermaOreSettimanali();
                String statoUPD = "";
                if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
                {
                    statoUPD = "9";

                    if (IdStato == "6")
                    {
                        statoUPD = "7";
                    }

                    _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, statoUPD);
                }
                else
                {
                    SalvaNote();

                    /*statoUPD = "C";

                    if (IdStato == "7")
                    {
                        statoUPD = "R";
                    }*/

                    if (IdStato == "I")
                    {
                        statoUPD = "9";
                    }
                    if (IdStato == "9" || IdStato == "8")
                    {
                        statoUPD = "C";
                    }

                    if (IdStato == "6")
                    {
                        statoUPD = "7";
                    }
                    if (IdStato == "7")
                    {
                        statoUPD = "R";
                    }


                    _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, statoUPD);
                }

                //TornaAllaLista();
                if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
                {
                    Context.Items["IdAssenza"] = idAssenza;
                    Server.Transfer("~/CeServizi/MalattiaTelematica/VisualizzaRicevuta.aspx");
                }
                else
                {
                    TornaAllaLista();
                }
            }
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            ConfermaDomanda();
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            TornaAllaLista();
        }

        private void TornaAllaLista()
        {
            if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
            {
                Server.Transfer("~/CeServizi/MalattiaTelematica/RicercaAssenze.aspx");
                //Response.Redirect("~/MalattiaTelematica/RicercaAssenze.aspx?func=GestioneAssenze" +
                //        "&idLavoratore=" + ViewState["FiltroIdLavoratore"].ToString() +
                //        "&cognome=" + ViewState["FiltroCognome"].ToString() +
                //        "&nome=" + ViewState["FiltroNome"].ToString() +
                //        "&dataNascita=" + ViewState["FiltroDataNascita"].ToString() +

                //        "&statoAssenza=" + ViewState["FiltroStatoAssenza"].ToString() +
                //        "&tipoAssenza=" + ViewState["FiltroTipoAssenza"].ToString() +
                //        "&periodoDa=" + ViewState["FiltroPeriodoDa"].ToString() +
                //        "&periodoA=" + ViewState["FiltroPeriodoA"].ToString() +

                //        "&idImpresa=" + ViewState["FiltroIdImpresa"].ToString() +
                //        "&ragioneSociale=" + ViewState["FiltroRagioneSociale"].ToString() +
                //        "&codiceFiscale=" + ViewState["FiltroCodiceFiscale"].ToString()
                //        );
            }
            else
            {
                Server.Transfer("~/CeServizi/MalattiaTelematica/RicercaAssenzeBackOffice.aspx");
                //Response.Redirect("~/MalattiaTelematica/RicercaAssenzeBackOffice.aspx?func=GestioneAssenze" +
                //        "&idLavoratore=" + ViewState["FiltroIdLavoratore"].ToString() +
                //        "&cognome=" + ViewState["FiltroCognome"].ToString() +
                //        "&nome=" + ViewState["FiltroNome"].ToString() +
                //        "&dataNascita=" + ViewState["FiltroDataNascita"].ToString() +

                //        "&statoAssenza=" + ViewState["FiltroStatoAssenza"].ToString() +
                //        "&tipoAssenza=" + ViewState["FiltroTipoAssenza"].ToString() +
                //        "&periodoDa=" + ViewState["FiltroPeriodoDa"].ToString() +
                //        "&periodoA=" + ViewState["FiltroPeriodoA"].ToString() +

                //        "&idImpresa=" + ViewState["FiltroIdImpresa"].ToString() +
                //        "&ragioneSociale=" + ViewState["FiltroRagioneSociale"].ToString() +
                //        "&codiceFiscale=" + ViewState["FiltroCodiceFiscale"].ToString()
                //        );
            }
        }

        protected void ButtonInAttesa_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];

            _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "8");

            TornaAllaLista();
        }

        protected void ButtonAnnullaAssenza_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];

            _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "A");

            TornaAllaLista();
        }

        protected void ButtonImmessaPervenuta_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];

            _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "9");

            TornaAllaLista();
        }

        protected void ButtonNote_Click(object sender, EventArgs e)
        {
            SalvaNote();
        }

        private void SalvaNote()
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];

            _bizEf.UpdateMalattiaTelematicaAssenzeNoteAggiuntive(idAssenza, TextBoxNote.Text, CheckBoxTelefonata.Checked,
                                                                CheckBoxEmail.Checked);
        }

        protected void ButtonInviaMail_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];

            Context.Items["IdAssenza"] = idAssenza;
            Context.Items["TipoRichiesta"] = 1;
            //Context.Items["FiltroIdImpresa"]  = ViewState["FiltroIdImpresa"];
            //Context.Items["FiltroRagioneSociale"] = ViewState["FiltroRagioneSociale"];
            //Context.Items["FiltroCodiceFiscale"] = ViewState["FiltroCodiceFiscale"];
            //Context.Items["FiltroIdLavoratore"] = ViewState["FiltroIdLavoratore"];
            //Context.Items["FiltroCognome"] = ViewState["FiltroCognome"];
            //Context.Items["FiltroNome"] = ViewState["FiltroNome"];
            //Context.Items["FiltroDataNascita"] = ViewState["FiltroDataNascita"];
            //Context.Items["FiltroStatoAssenza"] = ViewState["FiltroStatoAssenza"];
            //Context.Items["FiltroTipoAssenza"] = ViewState["FiltroTipoAssenza"];
            //Context.Items["FiltroPeriodoDa"] = ViewState["FiltroPeriodoDa"];
            //Context.Items["FiltroPeriodoA"] = ViewState["FiltroPeriodoA"];

            Server.Transfer("~/CeServizi/MalattiaTelematica/RichiestaDocumenti.aspx");
        }

        protected void ButtonRespingi_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];
            Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
            Int32 idImpresa = (Int32)ViewState["IdImpresa"];
            DateTime dataInizioMalattia = (DateTime)ViewState["DataInizioMalattia"];
            DateTime dataInizioAssenza = (DateTime)ViewState["DataInizioAssenza"];

            // Modifica Mura
            //_bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "4");
            if (_bizEf.AssenzaPrecedenteNonAccolta(idAssenza, idLavoratore, idImpresa, dataInizioMalattia, dataInizioAssenza))
            {
                LabelMessaggio.Text = "Attenzione non è stato gestito il periodo di assenza precedente.";
            }
            else
            {
                _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "4");

                if ((int)ViewState["Provenienza"] == 1)
                {
                    TornaAllaLista();
                }
                else
                {
                    Server.Transfer("~/CeServizi/MalattiaTelematica/RicercaAssenzaInAttesa.aspx");
                }
            }
        }

        protected void ButtonRespingiSempre_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];
            Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
            Int32 idImpresa = (Int32)ViewState["IdImpresa"];
            DateTime dataInizioMalattia = (DateTime)ViewState["DataInizioMalattia"];
            DateTime dataInizioAssenza = (DateTime)ViewState["DataInizioAssenza"];

            // Modifica Mura
            //_bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "4");
            //if (_bizEf.AssenzaPrecedenteNonAccolta(idAssenza, idLavoratore, idImpresa, dataInizioMalattia, dataInizioAssenza))
            //{
            //    LabelMessaggio.Text = "Attenzione non è stato gestito il periodo di assenza precedente.";
            //}
            //else
            //{
                _bizEf.UpdateMalattiaTelematicaAssenzeStato(idAssenza, "4");

                if ((int)ViewState["Provenienza"] == 1)
                {
                    TornaAllaLista();
                }
                else
                {
                    Server.Transfer("~/CeServizi/MalattiaTelematica/RicercaAssenzaInAttesa.aspx");
                }
            //}
        }

        protected void ButtonPrimoSollecito_Click(object sender, EventArgs e)
        {
            Int32 idAssenza = (Int32)ViewState["IdAssenza"];

            Context.Items["IdAssenza"] = idAssenza;
            Context.Items["TipoRichiesta"] = (int)ViewState["Provenienza"] == 1 ? 21 : 20;

            //Context.Items["FiltroIdImpresa"]  = ViewState["FiltroIdImpresa"];
            //Context.Items["FiltroRagioneSociale"] = ViewState["FiltroRagioneSociale"];
            //Context.Items["FiltroCodiceFiscale"] = ViewState["FiltroCodiceFiscale"];
            //Context.Items["FiltroIdLavoratore"] = ViewState["FiltroIdLavoratore"];
            //Context.Items["FiltroCognome"] = ViewState["FiltroCognome"];
            //Context.Items["FiltroNome"] = ViewState["FiltroNome"];
            //Context.Items["FiltroDataNascita"] = ViewState["FiltroDataNascita"];
            //Context.Items["FiltroStatoAssenza"] = ViewState["FiltroStatoAssenza"];
            //Context.Items["FiltroTipoAssenza"] = ViewState["FiltroTipoAssenza"];
            //Context.Items["FiltroPeriodoDa"] = ViewState["FiltroPeriodoDa"];
            //Context.Items["FiltroPeriodoA"] = ViewState["FiltroPeriodoA"];

            Server.Transfer("~/CeServizi/MalattiaTelematica/RichiestaDocumenti.aspx");
        }
    }
}