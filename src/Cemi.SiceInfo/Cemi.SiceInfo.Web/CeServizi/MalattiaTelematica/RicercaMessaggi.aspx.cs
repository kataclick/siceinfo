﻿using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;
using xi = Telerik.Web.UI.ExportInfrastructure;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica
{
    public partial class RicercaMessaggi : System.Web.UI.Page
    {
        BusinessEF biz = new BusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.MalattiaTelematicaRicercaMessaggi);

            #endregion

            if (!Page.IsPostBack)
                RadDatePickerGiorno.SelectedDate = DateTime.Now.Date;
        }

        protected void RadButtonOk_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                //if (OnFiltroSelected != null)
                //{
                //    AssenzeFilter filtro = CreaFiltro();
                //    OnFiltroSelected(filtro);
                //}
                CaricaMessaggi();
            }
        }

        private void CaricaMessaggi()
        {
            Presenter.CaricaElementiInGridView(RadGridMessaggi,
                                               biz.GetMessaggi(RadDatePickerGiorno.SelectedDate, CheckBoxDescrizione.Checked,
                                                               RadComboBoxErrore.SelectedValue));
        }


        protected void RadGridMessaggi_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaMessaggi();
        }

        protected void RadGridMessaggi_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                MalattiaTelematicaMessaggio messaggi = (MalattiaTelematicaMessaggio)e.Item.DataItem;
                Label lIdImpresa = (Label)e.Item.FindControl("LabelIdImpresa");
                Label lImpresa = (Label)e.Item.FindControl("LabelImpresa");
                Label lIdLavoratore = (Label)e.Item.FindControl("LabelIdLavoratore");
                Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
                Label lProtocollo = (Label)e.Item.FindControl("LabelProtocollo");
                Label lPeriodo = (Label)e.Item.FindControl("LabelPeriodo");
                Label LImportoRichiesto = (Label)e.Item.FindControl("LabelImportoRichiesto");
                Label LImportoLiquidabile = (Label)e.Item.FindControl("LabelImportoLiquidabile");

                lIdImpresa.Text = String.Format("Codice: {0}", messaggi.IdImpresa);
                lImpresa.Text = messaggi.Impresa.RagioneSociale;
                lIdLavoratore.Text = String.Format("Codice: {0}", messaggi.IdLavoratore);
                lLavoratore.Text = String.Format("{0} {1}", messaggi.Lavoratore.Cognome, messaggi.Lavoratore.Nome);
                lProtocollo.Text = String.Format("{0} {1}/{2}/{3}", messaggi.IdCassaEdile, messaggi.TipoProtocollo, messaggi.numeroProtocollo.ToString(), messaggi.AnnoProtocollo.ToString());
                lPeriodo.Text = String.Format("{0} - {1}", messaggi.DataInizioAssenzaDenuncia.Value.ToShortDateString(), messaggi.DataFineAssenzaDenuncia.Value.ToShortDateString());

                LImportoRichiesto.Text = String.Format("Importo richiesto: {0}", messaggi.ImportoRichiesto.HasValue ? ((decimal)messaggi.ImportoRichiesto).ToString("c2") : "");
                LImportoLiquidabile.Text = String.Format("Importo liquidabile: {0}", messaggi.ImportoLiquidabile.HasValue ? ((decimal)messaggi.ImportoLiquidabile).ToString("c2") : "");

            }

        }



        protected void ButtonExport_OnClick(object sender, EventArgs eventArgs)
        {
            CaricaMessaggi();

            RadGridMessaggi.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridMessaggi.ExportSettings.IgnorePaging = true;
            RadGridMessaggi.ExportSettings.ExportOnlyData = true;
            RadGridMessaggi.ExportSettings.OpenInNewWindow = true;
            RadGridMessaggi.MasterTableView.ExportToExcel();
        }
    }
}