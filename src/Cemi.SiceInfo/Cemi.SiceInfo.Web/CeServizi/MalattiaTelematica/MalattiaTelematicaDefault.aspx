﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="MalattiaTelematicaDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.MalattiaTelematicaDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Malattia / Infortunio" />
    <table class="borderedTable" runat="server" id="TableConteggi">
        <tr>
            <td style="font-weight: bold">
                In attesa
            </td>
            <td>
                <asp:TextBox runat="server" ReadOnly="True" ID="TextBoxInAttesa" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">
                Invio primo sollecito
            </td>
            <td>
                <asp:TextBox runat="server" ReadOnly="True" ID="TextBoxPrimoSollecito" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">
                Respingimento
            </td>
            <td>
                <asp:TextBox runat="server" ReadOnly="True" ID="TextBoxRespingimento" Width="200px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <p class="DefaultPage">
        La funzione consente di richiedere il rimborso per il trattamento economico erogato
        al lavoratore dipendente in caso di assenza per malattia e/o infortunio.
    </p>
    <p class="DefaultPage" runat="server" ID="TestoEmail" Visible="True">
        Nel riquadro “<strong>E-mail</strong>” viene proposto in automatico l’indirizzo di posta elettronica
        disponibile sui sistemi informatici del nostro Ente. Tale indirizzo verrà utilizzato
        per gestire l’eventuale corrispondenza relativa alle domande di rimborso. Nel caso
        in cui si desideri variare l’informazione, riportare nel campo bianco il nuovo indirizzo
        di posta elettronica e premere il tasto “Salva e-mail”.
    </p>
    <table class="borderedTable" runat="server" id="TableEmail">
        <tr>
            <td style="font-weight: bold">
                E-mail
            </td>
            <td>
                <asp:TextBox runat="server" ReadOnly="True" ID="TextBoxEmailOriginale" Width="200px"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox runat="server" ID="TextBoxEmailModifica" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="EmailValidator" runat="server" Display="None"
                    ForeColor="Red" ValidationGroup="SalvaEmail" ErrorMessage="E-mail formalmente errata."
                    ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*"
                    ControlToValidate="TextBoxEmailModifica">
                </asp:RegularExpressionValidator>
            </td>
            <td style="font-weight: bold">
                <asp:Button runat="server" ID="ButtonSalvaEmail" Text="Salva e-mail" OnClick="ButtonSalvaEmail_Click"
                    ValidationGroup="SalvaEmail" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummaryReason" runat="server" ValidationGroup="SalvaEmail"
                    ForeColor="Red" DisplayMode="List" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    <p class="DefaultPage">
        <strong>Gestione assenze e caricamento file xml</strong><br />
        <br />
        Le voci in oggetto consentono di scegliere l’assenza del lavoratore dipendente da
        giustificare e di inserire i relativi certificati medici o altri documenti da far
        pervenire in Cassa Edile al fine dell’ottenimento del rimborso.
        <br />
        <br />
        <strong>Estratto Conto</strong>
        <br />
        <br />
        La voce consente di:
        <ul>
            <li>ricercare per periodo (mese/anno); </li>
            <li>visualizzare; </li>
            <li>salvare o stampare </li>
        </ul>
        gli attestati relativi agli estratti conto dei rimborsi erogati da Cassa Edile relativamente
        ai trattamenti economici per:
        <ul>
            <li>malattia; </li>
            <li>infortunio e malattia professionale. </li>
        </ul>
        Si precisa che gli attestati sono ricercabili dal mese di novembre 2010 a seguire.
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" GroupingText="AVVISI" HorizontalAlign="Justify">            
            <br />
            <strong>IMPORTANTE:</strong> verificare la correttezza dell’indirizzo di posta elettronica sopra indicato. Per motivi tecnici si segnala che non sono ammessi indirizzi di Posta Elettronica Certificata (PEC).            
            <%--<br />
            <asp:Panel ID="Panel3" runat="server" HorizontalAlign="Justify" BorderStyle="Solid" BorderWidth="1" BorderColor="LightGray" style="padding:5px;">
                <strong>Disabilitata l’importazione automatica dei dati del certificato</strong>
                <br />
                <br />
                A causa di una modifica introdotta sul sito web dell’INPS, non è al momento funzionante il recupero automatico delle informazioni da certificato medico (<i>Gestione assenze</i> → <i>Caricamento giustificativi</i> → <i>Certificato medico</i>).
                <br />
                <br />
                Per poter inoltrare con successo una domanda telematica di rimborso malattia / infortunio, è, pertanto, necessario compilare manualmente i campi riportati all’interno della sezione <i>“Certificato medico”</i> (es. data inizio, data fine, data rilascio, ecc.) ed allegare l’immagine del giustificativo. 
                <br />
                <br />
                Ci scusiamo per i disagi causati indipendentemente dalla nostra volontà.
                <br />
                <br />
                Cassa Edile di Milano, Lodi, Monza e Brianza
            </asp:Panel>--%>
        </asp:Panel>
        <br /> 

        <!--
            <span style="color: #CD071E;"><strong>AVVISO</strong></span>
        <br />
        <br />
        Si informa che il <span style="color: #CD071E;"><strong>CARICAMENTO DEI CERTIFICATI
            MEDICI</strong></span> può essere effettuato, a seconda della tipologia di giustificativo,
        alle seguenti voci del menu:
        <ul>
            <li><b>"Gestione assenze"</b>: alla finestra <b>"Elenco giustificativi"</b> selezionare
                la voce <b>"Aggiungi giustificativi"</b>, inserire il numero di certificato e premere
                il pulsante <b>"Carica dati INPS"</b>;</li>
        </ul>
        oppure
        <ul>
            <li><b>"Caricamento file xml"</b> per l’invio del giustificativo nel solo formato "xml".</li>
        </ul>
            -->
        <br />
        Cliccare <a href="http://ww2.cassaedilemilano.it/LinkClick.aspx?fileticket=z2MlxEIa8nM%3d" target="_blank">qui</a> per scaricare il manuale utente.<br />
        <%--Cliccare <a href="http://ww2.cassaedilemilano.it/Portals/0/pdf/Malattia%20Telematica%20-%20Manuale%20utente_20_09_2017%20M.O.%208.5.3.4.pdf" target="_blank">qui</a> per scaricare il manuale utente.<br />--%>
    </p>
</asp:Content>

