﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="UploadCertificati.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.UploadCertificati"
     %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Caricamento certificati medici da file XML" />
    <br />
    <td>
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList_SelectedIndexChanged"
            BorderWidth="1px" Width="300px" Visible="False">
            <asp:ListItem Value="0" Selected="True">Da file XML</asp:ListItem>
            <asp:ListItem Value="1">Da servizio Inps</asp:ListItem>
        </asp:RadioButtonList>
    </td>
    <br />
    <asp:Panel ID="PanelInps" runat="server" Visible="false">
        <div>
            <table>
                <tr runat="server" id="trConsulente">
                    <td colspan="3">
                        <uc1:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
                        <asp:CustomValidator ID="customValidator3" runat="server" ValidationGroup="Ricerca"
                            ErrorMessage="Selezionare un'impresa" OnServerValidate="customValidatorSelezionaImpresa">*
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr runat="server" id="trImpresa">
                    <td>
                        Codice Impresa
                    </td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxIdImpresa" runat="server" Type="Number"
                            DataType="System.Int32" MinValue="1">
                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                        </telerik:RadNumericTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorIdImpresa" runat="server" ErrorMessage="Inserire il codice impresa"
                            ValidationGroup="Ricerca" ControlToValidate="RadNumericTextBoxIdImpresa">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Codice Fiscale Lavoratore
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server">
                        </telerik:RadTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatoCodiceFiscale" runat="server"
                            ErrorMessage="Inserire il codice fiscale" ControlToValidate="RadTextBoxCodiceFiscale"
                            ValidationGroup="Ricerca">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Numero Certificato
                    </td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxCertificato" runat="server" Type="Number"
                            DataType="System.Int32" MinValue="1">
                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                        </telerik:RadNumericTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorNumeroCertificato" runat="server"
                            ErrorMessage="Inserire il numero certificato" ValidationGroup="Ricerca" ControlToValidate="RadNumericTextBoxCertificato">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Button ID="ButtonInterroga" runat="server" OnClick="ButtonInterroga_Click" Text="Interroga Inps"
                            ValidationGroup="Ricerca" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                            ValidationGroup="Ricerca" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </asp:Panel>
    <asp:Panel ID="PanelXml" runat="server" Visible="true">
        <div>
            <table>
                <tr>
                    <td>
                        Selezione file XML
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUploadXML" runat="server" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonOk" runat="server" OnClick="ButtonOk_Click" Text="Conferma" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
    </asp:Panel>
    <telerik:RadGrid ID="RadGridCertificati" runat="server" CellSpacing="0" GridLines="None"
        OnItemDataBound="RadGridCertificati_ItemDataBound" OnItemCommand="RadGridCertificati_ItemCommand"
        Visible="false">
        <MasterTableView DataKeyNames="IdLavoratore,IdImpresa,Numero,Tipo,DataRilascio,DataInizio,DataFine,Documento">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridTemplateColumn FilterControlAltText="Filter idImpresa column" HeaderText="Cod. Impresa"
                    UniqueName="idImpresa" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:Image ID="ImageImpresa" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                        <asp:Label ID="LabelIdImpresa" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter impresa column" HeaderText="Impresa"
                    UniqueName="impresa" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelPIVA" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelCodiceINPS" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter idLavoratore column" HeaderText="Cod.Lavoratore"
                    UniqueName="idLavoratore" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:Image ID="ImageLavoratore" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                        <asp:Label ID="LabelIdLavoratore" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter lavoratore column" HeaderText="Lavoratore"
                    UniqueName="lavoratore" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelLavoratore" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNascita" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="Numero" FilterControlAltText="Filter numero column"
                    HeaderText="Numero certificato" UniqueName="numero" ItemStyle-VerticalAlign="Top">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlAltText="Filter tipo column" HeaderText="Tipo"
                    UniqueName="tipo" DataField="Tipo" ItemStyle-VerticalAlign="Top">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlAltText="Filter dataInizio column" HeaderText="Data inizio"
                    UniqueName="dataInizio" DataField="DataInizio" DataFormatString="{0:dd/MM/yyyy}"
                    ItemStyle-VerticalAlign="Top">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlAltText="Filter dataFine column" HeaderText="Data fine"
                    UniqueName="dataFine" DataField="DataFine" DataFormatString="{0:dd/MM/yyyy}"
                    ItemStyle-VerticalAlign="Top">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn FilterControlAltText="Filter dataRilascio column" HeaderText="Data rilascio"
                    UniqueName="dataRilascio" DataField="DataRilascio" DataFormatString="{0:dd/MM/yyyy}"
                    ItemStyle-VerticalAlign="Top">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Documento" FilterControlAltText="Filter documento column"
                    UniqueName="documento" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn FilterControlAltText="Filter insert column" HeaderText=""
                    UniqueName="buttonInsert" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="ImageCertificatoEsistente" runat="server" ImageUrl="../images/segnalazione.png"
                                        ToolTip="Certificato già inserito" />
                                </td>
                                <td>
                                    <asp:Button ButtonType="PushButton" runat="server" CommandName="insert" FilterControlAltText="Filter insert column"
                                        ID="buttonInsert" UniqueName="insert" Text="Aggiungi" ItemStyle-VerticalAlign="Top">
                                    </asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="LabelCertificatoEsistente" runat="server" Text="Già inserito" Visible="False"
                                        Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <br />
    <asp:Label ID="Label1" runat="server" 
        Text="Ricordarsi di accedere alla funzionalità 'Gestione assenze' e confermare la relativa assenza." 
        Font-Bold="True" ForeColor="#CC0000"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
