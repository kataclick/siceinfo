﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using System.Globalization;
using Telerik.Web.UI;
using System.Xml.Serialization;
using System.Text;
using System.Xml.Linq;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica
{
    public partial class UploadCertificati : System.Web.UI.Page
    {
        BusinessEF bizEF = new BusinessEF();
        Cemi.MalattiaTelematica.Business.Business biz = new Cemi.MalattiaTelematica.Business.Business();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.MalattiaTelematicaUploadCertificati);

            #endregion


            trConsulente.Visible = GestioneUtentiBiz.IsConsulente();
            if (!GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
            {
                trImpresa.Visible = true;
            }
            else
            {
                trImpresa.Visible = false;
            }
        }

        protected void ButtonOk_Click(object sender, EventArgs e)
        {
            //DataSet ds = new DataSet();
            listaAttestati attestati;

            if (FileUploadXML.HasFile)
            {
                using (Stream sr = FileUploadXML.PostedFile.InputStream)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(listaAttestati));
                    attestati = (listaAttestati)serializer.Deserialize(sr);

                    ViewState["Attestati"] = attestati;
                    //ds.ReadXml(sr);
                }
                CaricaXML();

            }

        }


        /*private void CaricaXML(DataSet ds)
        {

            Int32 idAttestato;
            Int32? idImpresa;
            String numeroCertificato;
            String matricolaINPS = "";
            DateTime dataRilascio;
            DateTime dataInizio;
            DateTime dataFine;
            String tipo;

            Int32? idLavoratore = -1;
            String nome;
            String cognome;
            String cf;
            DateTime dtNascita;


            List<GiustificativiUpload> giustificativi = new List<GiustificativiUpload>();

            foreach (DataRow rowAtt in ds.Tables["attestato"].Rows)
            {
                //idImpresa = 0;
                matricolaINPS = rowAtt["matricolaINPS"].ToString();
                Impresa imp = biz.GetImpresa(matricolaINPS);
                if (imp != null)
                {
                    idImpresa = imp.Id;
                }
                else
                {
                    idImpresa = null;
                }
                numeroCertificato = rowAtt["idCertificato"].ToString();
                dataRilascio = DateTime.ParseExact(rowAtt["dataRilascio"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                dataInizio = DateTime.ParseExact(rowAtt["dataInizio"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                dataFine = DateTime.ParseExact(rowAtt["dataFine"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                tipo = rowAtt["tipoCertificato"].ToString();

                idAttestato = (Int32)rowAtt["Attestato_ID"];
                DataRow[] lavoratori = ds.Tables["lavoratore"].Select("Attestato_ID = " + idAttestato);

                foreach (DataRow rowLav in lavoratori)
                {
                    idLavoratore = 0;
                    Lavoratore lav = biz.GetLavoratore(rowLav["codiceFiscale"].ToString());
                    if (lav != null)
                    {
                        idLavoratore = lav.Id;

                        nome = lav.Nome;
                        cognome = lav.Cognome;
                        dtNascita = lav.DataNascita.Value;
                        cf = lav.CodiceFiscale;
                    }
                    else
                    {
                        idLavoratore = null;

                        nome = rowLav["nome"].ToString();
                        cognome = rowLav["cognome"].ToString();
                        dtNascita = DateTime.ParseExact(rowLav["dataNascita"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        cf = rowLav["codiceFiscale"].ToString();

                    }



                    GiustificativiUpload giustificativo = new GiustificativiUpload();
                    //impresa
                    giustificativo.IdImpresa = idImpresa;
                    giustificativo.CodiceINPS = matricolaINPS;
                    if (imp != null)
                    {
                        giustificativo.RagioneSociale = imp.RagioneSociale;
                        giustificativo.PartitaIVA = imp.PartitaIVA;
                    }
                    else
                    {
                        giustificativo.RagioneSociale = "";
                        giustificativo.PartitaIVA = "";
                    }
                    //lavoratore
                    giustificativo.IdLavoratore = idLavoratore;
                    giustificativo.Nome = nome;
                    giustificativo.Cognome = cognome;
                    giustificativo.DataNascita = dtNascita;
                    giustificativo.CodiceFiscale = cf;
                    //certifiato
                    giustificativo.Numero = numeroCertificato;
                    giustificativo.DataInizio = dataInizio;
                    giustificativo.DataFine = dataFine;
                    giustificativo.DataRilascio = dataRilascio;
                    giustificativo.Tipo = tipo;

                    giustificativo.NomeDocumento = "";
                    giustificativo.Documento = "";

                    giustificativi.Add(giustificativo);

                }

            }


            Presenter.CaricaElementiInGridView(RadGridCertificati, giustificativi);
        }*/
        private void CaricaXML()
        {
            //Int32 idAttestato;
            Int32? idImpresa;
            String numeroCertificato;
            String matricolaINPS = "";
            DateTime dataRilascio;
            DateTime dataInizio;
            DateTime dataFine;
            String tipo;

            Int32? idLavoratore = -1;
            String nome;
            String cognome;
            String cf;
            DateTime dtNascita;

            listaAttestati attestati = (listaAttestati)ViewState["Attestati"];

            List<GiustificativiUpload> giustificativi = new List<GiustificativiUpload>();


            foreach (listaAttestatiAttestato attestato in attestati.Items)
            {
                matricolaINPS = attestato.matricolaINPS;
                Impresa imp = bizEF.GetImpresa(matricolaINPS);
                if (imp != null)
                {
                    idImpresa = imp.Id;
                }
                else
                {
                    idImpresa = null;
                }
                numeroCertificato = attestato.idCertificato;
                dataRilascio = DateTime.ParseExact(attestato.dataRilascio, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                dataInizio = DateTime.ParseExact(attestato.dataInizio, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                dataFine = DateTime.ParseExact(attestato.dataFine, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                tipo = attestato.tipoCertificato;

                foreach (listaAttestatiAttestatoLavoratore lavoratore in attestato.lavoratore)
                {
                    idLavoratore = 0;
                    Lavoratore lav = bizEF.GetLavoratore(lavoratore.codiceFiscale);
                    if (lav != null)
                    {
                        idLavoratore = lav.Id;

                        nome = lav.Nome;
                        cognome = lav.Cognome;
                        dtNascita = lav.DataNascita.Value;
                        cf = lav.CodiceFiscale;
                    }
                    else
                    {
                        idLavoratore = null;

                        nome = lavoratore.nome;
                        cognome = lavoratore.cognome;
                        dtNascita = DateTime.ParseExact(lavoratore.dataNascita, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                        cf = lavoratore.codiceFiscale;

                    }

                    GiustificativiUpload giustificativo = new GiustificativiUpload();
                    //impresa
                    giustificativo.IdImpresa = idImpresa;
                    giustificativo.CodiceINPS = matricolaINPS;
                    if (imp != null)
                    {
                        giustificativo.RagioneSociale = imp.RagioneSociale;
                        giustificativo.PartitaIVA = imp.PartitaIVA;
                    }
                    else
                    {
                        giustificativo.RagioneSociale = "";
                        giustificativo.PartitaIVA = "";
                    }
                    //lavoratore
                    giustificativo.IdLavoratore = idLavoratore;
                    giustificativo.Nome = nome;
                    giustificativo.Cognome = cognome;
                    giustificativo.DataNascita = dtNascita;
                    giustificativo.CodiceFiscale = cf;
                    //certifiato
                    giustificativo.Numero = numeroCertificato;
                    giustificativo.DataInizio = dataInizio;
                    giustificativo.DataFine = dataFine;
                    giustificativo.DataRilascio = dataRilascio;
                    giustificativo.Tipo = tipo;


                    XmlSerializer serializer = new XmlSerializer(typeof(listaAttestatiAttestato));
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
                    settings.Indent = false;
                    settings.OmitXmlDeclaration = true;

                    StringWriter writer = new StringWriter();
                    XmlWriter xmlWriter = XmlWriter.Create(writer, settings);
                    //w.Formatting = Formatting.Indented;
                    //w.Settings.Encoding = Encoding.UTF8;

                    serializer.Serialize(xmlWriter, attestato);

                    giustificativo.NomeDocumento = "attestato.xml";

                    var encoding = System.Text.Encoding.UTF8;
                    giustificativo.Documento = encoding.GetBytes(writer.GetStringBuilder().ToString());

                    giustificativi.Add(giustificativo);

                }
            }

            RadGridCertificati.Visible = true;
            Label1.Visible = true;
            Presenter.CaricaElementiInGridView(RadGridCertificati, giustificativi);
        }
        private void CaricaINPS(MalattiaTelematicaCertificatoMedico cert)
        {
            List<GiustificativiUpload> giustificativi = new List<GiustificativiUpload>();
            GiustificativiUpload giustificativo = new GiustificativiUpload();

            if (cert.Numero != null)
            {
                //impresa
                if (GestioneUtentiBiz.IsImpresa())
                {
                    giustificativo.IdImpresa = ((TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa)GestioneUtentiBiz.GetIdentitaUtente(GestioneUtentiBiz.GetIdUtente())).IdImpresa;
                }
                else if (GestioneUtentiBiz.IsConsulente())
                {
                    giustificativo.IdImpresa = ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata();
                }
                else
                {
                    giustificativo.IdImpresa = Convert.ToInt32(RadNumericTextBoxIdImpresa.Text);
                }


                Impresa imp = bizEF.GetImpresa(giustificativo.IdImpresa.Value);
                giustificativo.CodiceINPS = imp.codiceINPS;
                if (imp != null)
                {
                    giustificativo.RagioneSociale = imp.RagioneSociale;
                    giustificativo.PartitaIVA = imp.PartitaIVA;
                }
                else
                {
                    giustificativo.RagioneSociale = "";
                    giustificativo.PartitaIVA = "";
                }
                //lavoratore
                Int32? idLavoratore = -1;
                String nome;
                String cognome;
                String cf;
                DateTime dtNascita;
                Lavoratore lav = bizEF.GetLavoratore(RadTextBoxCodiceFiscale.Text);
                if (lav != null)
                {
                    idLavoratore = lav.Id;

                    nome = lav.Nome;
                    cognome = lav.Cognome;
                    dtNascita = lav.DataNascita.Value;
                    cf = lav.CodiceFiscale;
                }
                else
                {
                    idLavoratore = null;

                    nome = "";
                    cognome = "";
                    dtNascita = new DateTime(1900, 1, 1);
                    cf = "";// TextBoxCodiceFiscale.Text;

                }
                giustificativo.IdLavoratore = idLavoratore;
                giustificativo.Nome = nome;
                giustificativo.Cognome = cognome;
                giustificativo.DataNascita = dtNascita;
                giustificativo.CodiceFiscale = cf;
                //certifiato
                giustificativo.Numero = cert.Numero;
                giustificativo.DataInizio = cert.DataInizio.Value;
                giustificativo.DataFine = cert.DataFine.Value;
                giustificativo.DataRilascio = cert.DataRilascio.Value;
                giustificativo.Tipo = cert.Tipo;
                giustificativo.NomeDocumento = cert.NomeFile;
                giustificativo.Documento = cert.Immagine;

                giustificativi.Add(giustificativo);

            }
            RadGridCertificati.Visible = true;
            Label1.Visible = true;
            Presenter.CaricaElementiInGridView(RadGridCertificati, giustificativi);
        }

        protected void RadioButtonList_SelectedIndexChanged(Object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedValue == "0") // da file xml
            {
                PanelInps.Visible = false;
                PanelXml.Visible = true;
            }
            else
            {
                PanelInps.Visible = true;
                PanelXml.Visible = false;
            }

            RadGridCertificati.Visible = false;
            Label1.Visible = false;

        }


        /*
        private MalattiaTelematicaCertificatoMedico CreaCertificato()
        {
            MalattiaTelematicaCertificatoMedico cert = new MalattiaTelematicaCertificatoMedico();

            cert.DataInizio = new DateTime(RadDatePickerInizio.SelectedDate.Value.Year,
                                           RadDatePickerInizio.SelectedDate.Value.Month,
                                           RadDatePickerInizio.SelectedDate.Value.Day);
            cert.DataFine = new DateTime(RadDatePickerFine.SelectedDate.Value.Year,
                                         RadDatePickerFine.SelectedDate.Value.Month,
                                         RadDatePickerFine.SelectedDate.Value.Day);

            cert.DataRilascio = new DateTime(RadDatePickerRilascio.SelectedDate.Value.Year,
                                             RadDatePickerRilascio.SelectedDate.Value.Month,
                                             RadDatePickerRilascio.SelectedDate.Value.Day);

            //cert.DataRicezione = new DateTime(RadDatePickerRicezione.SelectedDate.Value.Year,
            //                                  RadDatePickerRicezione.SelectedDate.Value.Month,
            //                                  RadDatePickerRicezione.SelectedDate.Value.Day);

            cert.Numero = RadTextBoxNumero.Text;

            if (RadioButtonMalattia.Checked)
                cert.TipoAssenza = "MA";
            else if (RadioButtonMalattiaProfessionale.Checked)
                cert.TipoAssenza = "MP";
            else if (RadioButtonInfortunio.Checked)
                cert.TipoAssenza = "IN";

            if (RadioButtonInizio.Checked)
                cert.Tipo = "I";
            else if (RadioButtonRicaduta.Checked)
                cert.Tipo = "R";
            else if (RadioButtonContinuazione.Checked)
                cert.Tipo = "C";

            cert.IdImpresa = (Int32)ViewState["IdImpresa"];
            cert.IdLavoratore = (Int32)ViewState["IdLavoratore"];
            cert.DataInserimentoRecord = DateTime.Now;

            if (FileUploadCertificatoMedico.HasFile)
            {
                using (Stream sr = FileUploadCertificatoMedico.PostedFile.InputStream)
                {
                    byte[] img = new byte[sr.Length];
                    sr.Read(img, 0, (int)sr.Length);
                    sr.Seek(0, SeekOrigin.Begin);
                    cert.Immagine = img;
                    cert.NomeFile = FileUploadCertificatoMedico.FileName;
                }
            }

            List<MalattiaTelematicaAssenza> assenze = new List<MalattiaTelematicaAssenza>();
            assenze.Add(new MalattiaTelematicaAssenza { Id = (Int32)ViewState["IdAssenza"] });
            cert.MalattiaTelematicaAssenze = assenze;
            return cert;
        }*/
        protected void RadGridCertificati_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                GiustificativiUpload giustificativi = (GiustificativiUpload)e.Item.DataItem;
                Label lIdImpresa = (Label)e.Item.FindControl("LabelIdImpresa");
                Image iImpresa = (Image)e.Item.FindControl("ImageImpresa");
                Label lImpresa = (Label)e.Item.FindControl("LabelImpresa");
                Label lPIVA = (Label)e.Item.FindControl("LabelPIVA");
                Label lCodINPS = (Label)e.Item.FindControl("LabelCodiceINPS");
                Label lIdLavoratore = (Label)e.Item.FindControl("LabelIdLavoratore");
                Image iLavoratore = (Image)e.Item.FindControl("ImageLavoratore");
                Label lLavoratore = (Label)e.Item.FindControl("LabelLavoratore");
                Label lNascita = (Label)e.Item.FindControl("LabelNascita");
                Label lCodFisc = (Label)e.Item.FindControl("LabelCodiceFiscale");
                Image iCertificato = (Image)e.Item.FindControl("ImageCertificatoEsistente");
                Label lCetificato = (Label)e.Item.FindControl("LabelCertificatoEsistente");

                Button bInsert = (Button)e.Item.FindControl("buttonInsert");

                if (giustificativi.IdImpresa == null || giustificativi.IdLavoratore == null)
                    bInsert.Enabled = false;

                if (giustificativi.IdLavoratore.HasValue)
                {
                    if (bizEF.ExistCertificatoMedico(giustificativi.IdLavoratore.Value, giustificativi.Numero))
                    {
                        bInsert.Enabled = false;

                        iCertificato.Visible = true;
                        lCetificato.Visible = true;
                    }
                    else
                    {
                        iCertificato.Visible = false;
                        lCetificato.Visible = false;
                    }
                }
                else
                {
                    iCertificato.Visible = false;
                }
                //  PushButton bInsert = (Button)e.item.FindControl("But");

                if (giustificativi.IdImpresa != null)
                {
                    lIdImpresa.Text = giustificativi.IdImpresa.ToString();
                    iImpresa.ImageUrl = "~/CeServizi/images/semaforoVerde.png";
                }
                lImpresa.Text = giustificativi.RagioneSociale;
                lPIVA.Text = String.Format("Partita IVA: {0}", giustificativi.PartitaIVA);
                lCodINPS.Text = String.Format("Codice INPS: {0}", giustificativi.CodiceINPS);
                if (giustificativi.IdLavoratore != null)
                {
                    lIdLavoratore.Text = giustificativi.IdLavoratore.ToString();
                    iLavoratore.ImageUrl = "~/CeServizi/images/semaforoVerde.png";
                }
                lLavoratore.Text = String.Format("{0} {1}", giustificativi.Cognome, giustificativi.Nome);
                lNascita.Text = string.Format("Nato il: {0}", giustificativi.DataNascita.ToShortDateString());
                lCodFisc.Text = String.Format("Codice fiscale: {0}", giustificativi.CodiceFiscale);

            }

        }
        protected void RadGridCertificati_ItemCommand(object sender, GridCommandEventArgs e)
        {

            if (e.CommandName == "insert")
            {
                Int32 idImpresa = (Int32)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
                Int32 idLavoratore =
                    (Int32)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdLavoratore"];
                String tipo = (String)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Tipo"];
                String numero = (String)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Numero"];
                DateTime rilascio =
                    (DateTime)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["DataRilascio"];
                DateTime inizio =
                    (DateTime)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["DataInizio"];
                DateTime fine =
                    (DateTime)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["DataFine"];
                MalattiaTelematicaCertificatoMedico cert = new MalattiaTelematicaCertificatoMedico();
                cert.IdImpresa = idImpresa;
                cert.IdLavoratore = idLavoratore;
                cert.DataInizio = inizio;
                cert.DataFine = fine;
                cert.DataRilascio = rilascio;
                cert.Numero = numero;
                cert.Tipo = tipo;
                cert.TipoAssenza = "MA";
                if (RadioButtonList1.SelectedValue == "0")
                {
                    cert.NomeFile = "attestato.xml";
                }
                else
                {
                    cert.NomeFile = "attestato.html";
                }
                cert.DataInserimentoRecord = DateTime.Now;
                cert.IdUtenteInserimento = GestioneUtentiBiz.GetIdUtente();
                //var xdoc = XDocument.Parse((String)RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Documento"]);
                //var encoding = System.Text.Encoding.UTF8;// System.Text.Encoding.GetEncoding(xdoc.Document.Declaration.Encoding);
                cert.Immagine = (byte[])RadGridCertificati.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Documento"];

                bizEF.InsertCertificatoMedico(cert);

                if (RadioButtonList1.SelectedValue == "0") // da file xml
                {
                    CaricaXML();
                }
                else
                {
                    CaricaINPS(cert);
                }


            }
        }
        protected void ButtonInterroga_Click(object sender, EventArgs e)
        {
            MalattiaTelematicaCertificatoMedico cert = biz.GetCertificatoMedicoFromINPS(RadTextBoxCodiceFiscale.Text, RadNumericTextBoxCertificato.Text);

            CaricaINPS(cert);


        }

        protected void customValidatorSelezionaImpresa(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            if (ConsulenteSelezioneImpresa1.GetIdImpresaSelezionata() < 0)
            {
                args.IsValid = false;
            }
        }
    }
}