﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using Telerik.Web.UI;
using TBridge.Cemi.Type.Domain;
using System.Globalization;
using Cemi.MalattiaTelematica.Business;
using Cemi.MalattiaTelematica.Type.Entities;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica
{
    public partial class StatisticaLiquidazioni : System.Web.UI.Page
    {
        Cemi.MalattiaTelematica.Business.Business biz = new Cemi.MalattiaTelematica.Business.Business();
        private readonly BusinessEF bizEF = new BusinessEF();


        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.MalattiaTelematicaStatisticaLiquidazioni);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaTipiAssenza();
            }
        }

        protected void RadButtonOk_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                //if (OnFiltroSelected != null)
                //{
                //    AssenzeFilter filtro = CreaFiltro();
                //    OnFiltroSelected(filtro);
                //}
                CaricaStatistica();
            }
        }

        private void CaricaTipiAssenza()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(RadComboBoxTipo, bizEF.GetTipiAssenze(), "Descrizione", "Id");
        }

        private void CaricaStatistica()
        {
            Presenter.CaricaElementiInGridView(RadGridLiquidazioni, biz.GetStatisticaLiquidazioni(RadDatePickerPeriodoDa.SelectedDate.Value.Date, RadDatePickerPeriodoA.SelectedDate.Value.Date, RadComboBoxTipo.SelectedValue));
        }


        protected void RadGridLiquidazioni_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaStatistica();
        }
    }
}