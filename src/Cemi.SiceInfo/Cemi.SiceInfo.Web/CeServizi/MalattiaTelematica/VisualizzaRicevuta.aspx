﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="VisualizzaRicevuta.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.VisualizzaRicevuta" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Visualizzazione Ricevuta" />
    <br />
    <table>
        <tr>
            <td>
                <asp:Button runat="server" ID="ButtonStampa" Text="Stampa" OnClick="ButtonStampa_OnClick" />
                <asp:Button runat="server" ID="ButtonIndietro" Text="Indietro" OnClick="ButtonIndietro_OnClick" />
                <asp:Button runat="server" ID="ButtonTornaLista" Text="Torna alla lista" OnClick="ButtonTornaLista_OnClick" />
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td colspan="2" align="center" style="font-weight: bold; font-size: large; height: 39px;">
                RICEVUTA
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="font-weight: bold; height: 46px;" valign="top">
                di presentazione della domanda di rimborso malattia / infortunio on-line
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Richiedente
            </td>
            <td style="height: 25px">
                <asp:Label ID="LabelRichiedente" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Codice impresa
            </td>
            <td>
                <asp:Label ID="LabelCodiceImpresa" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                P.IVA / C.F.
            </td>
            <td>
                <asp:Label ID="LabelPiva" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Domanda di
            </td>
            <td>
                <asp:Label ID="LabelDomanda" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Cognome e Nome del lavoratore
            </td>
            <td>
                <asp:Label ID="LabelCognomeNome" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Codice di iscrizione del lavoratore
            </td>
            <td>
                <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Periodi di malattia / infortunio richiesti
            </td>
            <td>
                <asp:Label ID="LabelPeriodi" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 198px; height: 25px;">
                Data di presentazione
            </td>
            <td>
                <asp:Label ID="LabelDataImmessaPervenuta" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; height: 46px;">
                La presente ricevuta non deve essere trasmessa alla sede Cassa Edile di Milano,
                Lodi, Monza e Brianza.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <rsweb:ReportViewer ID="ReportViewerRicevuta" runat="server" ProcessingMode="Remote"
                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" Height="550pt"
                    Width="550pt" ShowParameterPrompts="false" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

