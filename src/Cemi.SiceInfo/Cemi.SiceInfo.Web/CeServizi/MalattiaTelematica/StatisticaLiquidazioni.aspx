﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="StatisticaLiquidazioni.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.MalattiaTelematica.StatisticaLiquidazioni" 
    Theme="CETheme2009" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuMalattiaTelematica.ascx" TagName="MenuMalattiaTelematica"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Malattia / Infortunio"
        sottoTitolo="Statistica per liquidazioni" />
    <br />
    <table class="standardTable">
        <tr runat="server">
            <td>
                Periodo da (gg/mm/aaaa)
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerPeriodoDa" runat="server">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Immettere una data valida"
                    ValidationGroup="Ricerca" ControlToValidate="RadDatePickerPeriodoDa">*</asp:RequiredFieldValidator>
            </td>
            <td>
                Periodo a (gg/mm/aaaa)
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerPeriodoA" runat="server">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Immettere una data valida"
                    ValidationGroup="Ricerca" ControlToValidate="RadDatePickerPeriodoA">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="compareValidatorPeriodo" runat="server" ControlToCompare="RadDatePickerPeriodoDa"
                    ControlToValidate="RadDatePickerPeriodoA" EnableClientScript="False" ErrorMessage="Limiti periodi errati"
                    Operator="GreaterThanEqual" ValidationGroup="Ricerca" Type="Date">*
                </asp:CompareValidator>
            </td>
        </tr>
        <tr runat="server">
            <td>
                Tipo Assenza
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipo" runat="server" AppendDataBoundItems="true">
                </telerik:RadComboBox>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="RadButtonOk" runat="server" Text="Ricerca" OnClick="RadButtonOk_Click"
                    ValidationGroup="Ricerca" />
            </td>
        </tr>
        <tr runat="server">
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:ValidationSummary ID="ValidationSummaryRicerca" runat="server" CssClass="messaggiErrore"
                    ValidationGroup="Ricerca" />
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <div class="standardDiv">
        <telerik:RadGrid ID="RadGridLiquidazioni" runat="server" GridLines="None" Width="100%"
            AllowPaging="True" OnPageIndexChanged="RadGridLiquidazioni_PageIndexChanged"
            CellSpacing="0">
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to Pdf"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn FilterControlAltText="Filter dataLiquidazione column" HeaderText="Data Liquidazione"
                        UniqueName="dataLiquidazione" DataField="data" DataFormatString="{0:dd/MM/yyyy}"
                        ItemStyle-VerticalAlign="Top">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Importo" UniqueName="importo" DataField="Importo"
                        DataFormatString="{0:C2}">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Numero imprese" UniqueName="numeroImprese" DataField="numeroImprese">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Numero Lavoratori" UniqueName="numeroLavoratori"
                        DataField="numeroLavoratori">
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
            <HeaderContextMenu EnableImageSprites="True" CssClass="GridContextMenu GridContextMenu_Default">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuMalattiaTelematica ID="MenuMalattiaTelematica1" runat="server" />
</asp:Content>
