﻿<%@ Page Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="DefaultErroreAutenticazione.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.DefaultErroreAutenticazione" %>

<%@ Register src="WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>


    <asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
        <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" Titolo="Gestione autenticazione"
        Sottotitolo="Accesso alla funzionalità non consentito"
        runat="server" />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Il ruolo assegnato non possiede i diritti necessari per accedere a questa funzionalità. 
        Contattare l'amministratore di sistema per avere ulteriori informazioni."></asp:Label></asp:Content>
