﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class ReportAccessoCantieriAnomalie : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            var funzionalita = new List<FunzionalitaPredefinite>
            {
                FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                FunzionalitaPredefinite.AccessoCantieriPerCommittente
            };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            if (!Page.IsPostBack)
                if (Context.Items["IdDomanda"] != null)
                {
                    var idDomanda = (int) Context.Items["IdDomanda"];
                    var soloAnomalie = (int) Context.Items["soloAnomalie"];

                    if (soloAnomalie == 3)
                    {
                        ReportViewer1.ServerReport.ReportServerUrl =
                            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                        ReportViewer1.ServerReport.ReportPath =
                            "/ReportAccessoCantieri/ReportAccessoCantieriTimbratureFiltro";

                        DateTime? dataInizio = null;
                        if (Context.Items["dataInizio"] != null)
                            dataInizio = (DateTime?) Context.Items["dataInizio"];

                        DateTime? dataFine = null;
                        if (Context.Items["dataFine"] != null)
                            dataFine = (DateTime?) Context.Items["dataFine"];

                        int? idImpresa = null;
                        if (Context.Items["idImpresa"] != null)
                            idImpresa = (int?) Context.Items["idImpresa"];

                        var ragioneSocialeImpresa = string.Empty;
                        if (Context.Items["ragioneSocialeImpresa"] != null)
                            ragioneSocialeImpresa = (string) Context.Items["ragioneSocialeImpresa"];

                        int? tipoUtente = null;
                        if (Context.Items["tipoUtente"] != null)
                            tipoUtente = (int?) Context.Items["tipoUtente"];

                        var codiceFiscale = string.Empty;
                        if (Context.Items["codiceFiscale"] != null)
                            codiceFiscale = (string) Context.Items["codiceFiscale"];

                        var listaParam = new ReportParameter[7];

                        listaParam[0] = new ReportParameter("idCantiere", idDomanda.ToString());

                        string nothing = null;

                        if (dataInizio != null)
                            listaParam[1] = new ReportParameter("dataInizio", dataInizio.ToString());
                        else
                            listaParam[1] = new ReportParameter("dataInizio", nothing);

                        if (dataFine != null)
                            listaParam[2] = new ReportParameter("dataFine", dataFine.ToString());
                        else
                            listaParam[2] = new ReportParameter("dataFine", nothing);

                        if (idImpresa != null)
                            listaParam[3] = new ReportParameter("idImpresa", idImpresa.ToString());
                        else
                            listaParam[3] = new ReportParameter("idImpresa", nothing);

                        if (ragioneSocialeImpresa != string.Empty)
                            listaParam[4] = new ReportParameter("ragioneSocialeImpresa", ragioneSocialeImpresa);
                        else
                            listaParam[4] = new ReportParameter("ragioneSocialeImpresa", nothing);

                        if (tipoUtente != null)
                            listaParam[5] = new ReportParameter("tipoUtente", tipoUtente.ToString());
                        else
                            listaParam[5] = new ReportParameter("tipoUtente", nothing);

                        if (codiceFiscale != string.Empty)
                            listaParam[6] = new ReportParameter("codiceFiscale", codiceFiscale);
                        else
                            listaParam[6] = new ReportParameter("codiceFiscale", nothing);

                        ReportViewer1.ServerReport.SetParameters(listaParam);
                    }
                    else
                    {
                        ReportViewer1.ServerReport.ReportServerUrl =
                            new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                        ReportViewer1.ServerReport.ReportPath =
                            "/ReportAccessoCantieri/ReportAccessoCantieriTimbrature";

                        var listaParam = new ReportParameter[2];
                        listaParam[0] = new ReportParameter("idCantiere", idDomanda.ToString());
                        listaParam[1] = new ReportParameter("soloAnomalie", soloAnomalie.ToString());

                        ReportViewer1.ServerReport.SetParameters(listaParam);
                    }

                    #region Export diretto in pdf

                    //Warning[] warnings;
                    //string[] streamids;
                    //string mimeType;
                    //string encoding;
                    //string extension;

                    //byte[] bytes = ReportViewer1.ServerReport.Render(
                    //    "PDF", null, out mimeType, out encoding, out extension,
                    //    out streamids, out warnings);

                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/pdf";

                    //Response.AppendHeader("Content-Disposition", "attachment;filename=TimbratureAnomalie.pdf");
                    //Response.BinaryWrite(bytes);

                    //Response.Flush();
                    //Response.End();

                    #endregion
                }
        }
    }
}