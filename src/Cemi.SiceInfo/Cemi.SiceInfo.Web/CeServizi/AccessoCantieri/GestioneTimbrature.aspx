﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneTimbrature.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.GestioneTimbrature" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/AccessoCantieriTimbrature.ascx" TagName="AccessoCantieriTimbrature"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione timbrature"
        titolo="Accesso ai Cantieri" />
    <br />
        <uc3:AccessoCantieriTimbrature ID="AccessoCantieriTimbrature1" runat="server" />
    <br />
</asp:Content>
