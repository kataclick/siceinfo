﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class AccessoCantieriDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
            {
                FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                FunzionalitaPredefinite.AccessoCantieriPerImpresa,
                FunzionalitaPredefinite.AccessoCantieriAmministrazione,
                FunzionalitaPredefinite.AccessoCantieriControlli,
                FunzionalitaPredefinite.AccessoCantieriPerCommittente
            };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion
        }
    }
}