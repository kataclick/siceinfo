﻿<%@ Page Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportAccessoCantieriAnomalie.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.ReportAccessoCantieriAnomalie" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <br />
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione timbrature"
                           titolo="Accesso ai Cantieri" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" ShowDocumentMapButton="false"
                                    ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False" Height="550pt"
                                    Width="550pt">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
