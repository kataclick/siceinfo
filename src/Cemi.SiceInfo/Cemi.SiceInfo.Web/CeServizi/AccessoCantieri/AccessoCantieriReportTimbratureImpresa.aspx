﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="AccessoCantieriReportTimbratureImpresa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.AccessoCantieriReportTimbratureImpresa" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/AccessoCantieriTimbratureImpresa.ascx" TagName="TimbratureImpresa"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Benvenuto nella sezione Accesso ai Cantieri"
        titolo="Accesso ai Cantieri" />
    <br />
    <uc3:TimbratureImpresa ID="TimbratureImpresa1" runat="server" sottoTitolo="Stampa lavoratori in cantiere per impresa"
        titolo="Accesso ai Cantieri" />
</asp:Content>