﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Collections.Generic;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class GestioneTimbrature : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                                                             FunzionalitaPredefinite.AccessoCantieriPerCommittente
                                                         };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion
        }
    }
}