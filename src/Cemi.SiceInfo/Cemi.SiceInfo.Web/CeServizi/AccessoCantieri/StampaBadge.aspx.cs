﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using ICSharpCode.SharpZipLib.Zip;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class StampaBadge : System.Web.UI.Page
    {
        private readonly Common _commonBiz = new Common();
        private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriAmministrazione);

            #endregion

            SelezioneCantiere1.OnCantiereSelected += SelezioneCantiere1_OnCantiereSelected;
        }

        private void SelezioneCantiere1_OnCantiereSelected(WhiteList whiteList)
        {
            ViewState["idDomanda"] = whiteList.IdWhiteList;

            if (whiteList.IdWhiteList != null)
            {
                ImpresaCollection imprese = biz.GetImpreseSelezionateInSubappalto(whiteList.IdWhiteList.Value);
                ViewState["Imprese"] = imprese;

                CaricaImprese(imprese);
            }

            PanelWhiteList.Visible = true;
        }

        private void CaricaImprese(ImpresaCollection imprese)
        {
            WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);

            SubappaltoCollection subColl = domanda.Subappalti;
            ImpresaCollection impColl = new ImpresaCollection();

            if (subColl != null)
            {
                foreach (Subappalto sub in subColl)
                {
                    if (sub.Appaltante != null)
                    {
                        if (!impColl.Contains(sub.Appaltante)) impColl.Add(sub.Appaltante);
                    }

                    if (sub.Appaltata != null)
                    {
                        if (!impColl.Contains(sub.Appaltata)) impColl.Add(sub.Appaltata);
                    }
                }
            }

            impColl.Sort();

            //DropDownListImpresa.Items.Clear();
            //DropDownListImpresa.Items.Add(new ListItem(String.Empty, String.Empty));
            //DropDownListImpresa.Items.Add(new ListItem("Tutte (le imprese)", "TUTTE"));

            RadComboBoxImpresa.Items.Clear();
            RadComboBoxImpresa.Items.Add(new RadComboBoxItem(String.Empty, String.Empty));
            RadComboBoxImpresa.Items.Add(new RadComboBoxItem("Tutte (le imprese)", "TUTTE"));

            foreach (Impresa imp in impColl)
            {
                //DropDownListImpresa.Items.Add(
                //    new ListItem(imp.LavoratoreAutonomo ? String.Format("Aut. - {0}", imp.CodiceERagioneSociale) : imp.CodiceERagioneSociale,
                //                 string.Format("{0}|{1}|{2}|{3}", ((int) imp.TipoImpresa), imp.IdImpresa, imp.IdTemporaneo,
                //                               imp.CodiceFiscale)));

                if (imp.DataStampaBadge.HasValue)
                {
                    RadComboBoxImpresa.Items.Add(
                        new RadComboBoxItem(imp.LavoratoreAutonomo ? String.Format("Aut. (B) - {0}", imp.CodiceERagioneSociale) : imp.CodiceERagioneSociale,
                                     string.Format("{0}|{1}|{2}|{3}", ((int)imp.TipoImpresa), imp.IdImpresa, imp.IdTemporaneo,
                                                   imp.CodiceFiscale)));
                }
                else
                {
                    RadComboBoxImpresa.Items.Add(
                        new RadComboBoxItem(imp.LavoratoreAutonomo ? String.Format("Aut. - {0}", imp.CodiceERagioneSociale) : imp.CodiceERagioneSociale,
                                     string.Format("{0}|{1}|{2}|{3}", ((int)imp.TipoImpresa), imp.IdImpresa, imp.IdTemporaneo,
                                                   imp.CodiceFiscale)));
                }
            }

            if (imprese.Count == 0)
            {
                LabelImprese.Text = "Non ci sono imprese da selezionare";
                //DropDownListImpresa.Visible = false;
                RadComboBoxImpresa.Visible = false;
                LabelImpresaSelezionata.Visible = false;
            }
            else
            {
                LabelImprese.Text = "Imprese ";
                //DropDownListImpresa.Visible = true;
                RadComboBoxImpresa.Visible = true;
                LabelImpresaSelezionata.Visible = true;
            }

            LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";
            DropDownListFiltro.SelectedIndex = 0;
            RadGridLavoratori.DataSource = null;
            RadGridLavoratori.DataBind();
            PanelLavoratori.Visible = false;
        }

        protected void DropDownListImpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
            {
                SelezionaImpresaDaDropDowList();
                LabelNoSel.Visible = false;
            }
            else
            {
                ViewState["Impresa"] = null;
                LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";
            }
        }

        protected void RadGridLavoratori_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Lavoratore lav = (Lavoratore)e.Item.DataItem;
                Label lLavoratoreNomeCognome = (Label)e.Item.FindControl("LabelLavoratoreNomeCognome");
                Label lLavoratoreLuogoNascita = (Label)e.Item.FindControl("LabelLavoratoreLuogoNascita");
                Label lLavoratoreDataNascita = (Label)e.Item.FindControl("LabelLavoratoreDataNascita");
                Label lCodice = (Label)e.Item.FindControl("LabelCodice");
                Label lRagioneSociale = (Label)e.Item.FindControl("RagioneSociale");
                Label lPIva = (Label)e.Item.FindControl("PIva");
                Label lDataAssunzione = (Label)e.Item.FindControl("DataAssunzione");
                Label lAutorizzazioneAlSubappalto = (Label)e.Item.FindControl("AutorizzazioneAlSubappalto");
                Image imageFoto = (Image)e.Item.FindControl("ImageFoto");
                Label lDataStampaBadge = (Label)e.Item.FindControl("DataStampaBadge");

                if (lav != null)
                {
                    ListDictionary nazioni = _commonBiz.GetNazioni();

                    string paeseNascita = string.Empty;
                    string comuneNascita = string.Empty;
                    string provinciaNascita = string.Empty;

                    if (!string.IsNullOrEmpty(lav.PaeseNascita))
                    {
                        foreach (DictionaryEntry naz in nazioni)
                        {
                            if (naz.Key.ToString() == lav.PaeseNascita)
                            {
                                paeseNascita = naz.Value.ToString();
                                break;
                            }
                        }
                    }

                    if (lav.DataStampaBadge.HasValue)
                        lDataStampaBadge.Text = lav.DataStampaBadge.Value.ToString("dd/MM/yyyy");

                    if (lav.TipoLavoratore == TipologiaLavoratore.SiceNew)
                    {
                        if (!string.IsNullOrEmpty(lav.LuogoNascita))
                        {
                            comuneNascita = lav.LuogoNascita;
                        }

                        lCodice.Text = lav.IdLavoratore.ToString();
                        lLavoratoreNomeCognome.Text = string.Format("{0} {1}", lav.Nome, lav.Cognome);

                        lLavoratoreLuogoNascita.Text = comuneNascita;

                        if (!string.IsNullOrEmpty(lav.ProvinciaNascita))
                        {
                            if (lav.ProvinciaNascita != "EE")
                            {
                                provinciaNascita = lav.ProvinciaNascita;
                                lLavoratoreLuogoNascita.Text = string.Format("{0} ({1}), ITALIA", comuneNascita,
                                                                             provinciaNascita);
                            }
                        }

                        if (lav.DataNascita != null)
                            lLavoratoreDataNascita.Text = string.Format("{0}", lav.DataNascita.Value.ToShortDateString());
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lav.LuogoNascita))
                        {
                            comuneNascita = _commonBiz.GetComuneSiceNew(lav.LuogoNascita).Comune;
                            provinciaNascita = _commonBiz.GetComuneSiceNew(lav.LuogoNascita).Provincia;
                        }

                        lCodice.Text = string.Empty;

                        lLavoratoreNomeCognome.Text = string.Format("{0} {1}", lav.Nome, lav.Cognome);

                        if (paeseNascita.Trim().ToUpper() == "ITALIA")
                        {
                            lLavoratoreLuogoNascita.Text = string.Format("{0} ({1}), {2}", comuneNascita, provinciaNascita,
                                                                         paeseNascita);
                        }
                        else
                        {
                            lLavoratoreLuogoNascita.Text = paeseNascita;
                        }

                        if (lav.DataNascita != null)
                            lLavoratoreDataNascita.Text = string.Format("{0}", lav.DataNascita.Value.ToShortDateString());
                    }

                    if (lav.DataAssunzione.HasValue)
                        lDataAssunzione.Text = lav.DataAssunzione.Value.ToShortDateString();

                    WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);

                    //Impresa impSel = (Impresa) ViewState["Impresa"];
                    Impresa impSel = lav.Impresa;

                    lRagioneSociale.Text = impSel.RagioneSociale;
                    lPIva.Text = impSel.CodiceFiscale;

                    lAutorizzazioneAlSubappalto.Text = domanda.AutorizzazioneAlSubappalto;

                    if (AccessoCantieriBusiness.BadgeReady(lav, lAutorizzazioneAlSubappalto.Text))
                    {
                        ((CheckBox)(e.Item as GridDataItem)["ClientSelectColumn"].Controls[0]).Enabled = true;
                        (e.Item as GridDataItem)["ClientSelectColumn"].Controls[0].Visible = true;

                        e.Item.ToolTip = "Badge Ready!";
                    }
                    else
                    {
                        (e.Item as GridDataItem)["ClientSelectColumn"].Controls[0].Visible = false;
                        e.Item.Enabled = false;
                        ((CheckBox)(e.Item as GridDataItem)["ClientSelectColumn"].Controls[0]).Enabled = false;
                        ((CheckBox)(e.Item as GridDataItem)["ClientSelectColumn"].Controls[0]).Checked = false;
                        (e.Item as GridDataItem)["ClientSelectColumn"].Controls[0].Visible = false;

                        StringBuilder sbDatiMancanti = new StringBuilder();
                        sbDatiMancanti.AppendLine("Mancano i seguenti dati:");
                        if (String.IsNullOrEmpty(lav.Nome))
                        {
                            sbDatiMancanti.AppendLine("- nome");
                        }
                        if (String.IsNullOrEmpty(lav.Cognome))
                        {
                            sbDatiMancanti.AppendLine("- cognome");
                        }
                        if (((String.IsNullOrEmpty(lav.PaeseNascita) || lav.PaeseNascita == "1") &&
                             String.IsNullOrEmpty(lav.LuogoNascita)))
                        {
                            sbDatiMancanti.AppendLine("- paese e/o luogo di nascita");
                        }
                        if (!lav.DataNascita.HasValue)
                        {
                            sbDatiMancanti.AppendLine("- data di nascita");
                        }
                        if (!lav.DataAssunzione.HasValue)
                        {
                            sbDatiMancanti.AppendLine("- data di assunzione");
                        }
                        if (String.IsNullOrEmpty(lAutorizzazioneAlSubappalto.Text))
                        {
                            sbDatiMancanti.AppendLine("- autorizzazione al subappalto");
                        }
                        if (lav.Foto == null)
                        {
                            sbDatiMancanti.AppendLine("- foto");
                        }
                    }

                    if (lav.Foto != null)
                    {
                        imageFoto.ImageUrl = "~/CeServizi/images/semaforoVerde.png";
                    }
                    else
                    {
                        imageFoto.ImageUrl = "~/CeServizi/images/semaforoRosso.png";
                    }
                }
            }
        }

        //private void SelezionaImpresaDaDropDowList()
        //{
        //    if (DropDownListImpresa.SelectedValue == "TUTTE")
        //    {
        //        Impresa impresa = new Impresa();
        //        impresa.RagioneSociale = "Tutte";

        //        ImpresaSelezionata(impresa);
        //        CaricaDatiLavoratori(0);
        //    }
        //    else
        //    {
        //        string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
        //        TipologiaImpresa tipoImpresa = (TipologiaImpresa) Int32.Parse(tipoId[0]);

        //        Impresa impresa = new Impresa { TipoImpresa = tipoImpresa };

        //        if (tipoImpresa == TipologiaImpresa.SiceNew)
        //        {
        //            impresa.IdImpresa = Int32.Parse(tipoId[1]);
        //        }
        //        else
        //        {
        //            if (String.IsNullOrEmpty(tipoId[1]))
        //            {
        //                Guid gid = new Guid(tipoId[2]);
        //                impresa.IdTemporaneo = gid;
        //            }
        //            else
        //            {
        //                impresa.IdImpresa = Int32.Parse(tipoId[1]);
        //            }
        //        }

        //        impresa.CodiceFiscale = tipoId[3];

        //        // Porcata per gestire il fatto che nella DropDown c'è anche l'ID    
        //        String ragioneSociale = DropDownListImpresa.SelectedItem.Text;
        //        if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
        //        {
        //            String[] splitted = ragioneSociale.Split('-');
        //            ragioneSociale = String.Empty;

        //            for (int i = 1; i < splitted.Length; i++)
        //            {
        //                ragioneSociale += splitted[i];

        //                if (i > 1)
        //                {
        //                    ragioneSociale += "-";
        //                }
        //            }
        //        }

        //        impresa.RagioneSociale = ragioneSociale;

        //        ImpresaSelezionata(impresa);

        //        WhiteList domanda = biz.GetDomandaByKey((Int32) ViewState["idDomanda"]);

        //        SubappaltoCollection subColl = domanda.Subappalti;
        //        ImpresaCollection impColl = new ImpresaCollection();

        //        if (subColl != null)
        //        {
        //            foreach (Subappalto sub in subColl)
        //            {
        //                if (sub.Appaltante != null)
        //                {
        //                    if (!impColl.Contains(sub.Appaltante))
        //                        impColl.Add(sub.Appaltante);
        //                }

        //                if (sub.Appaltata != null)
        //                {
        //                    if (!impColl.Contains(sub.Appaltata))
        //                        impColl.Add(sub.Appaltata);
        //                }
        //            }
        //        }

        //        Impresa lavAutonomo = impColl.GetImpresaAutonomaCodFisc(impresa.CodiceFiscale);

        //        if (lavAutonomo == null)
        //        {
        //            CaricaDatiLavoratori(0);
        //        }
        //        else
        //        {
        //            CaricaDatiLavoratoreAutonomo(lavAutonomo);
        //        }
        //    }
        //}

        private void SelezionaImpresaDaDropDowList()
        {
            if (RadComboBoxImpresa.SelectedValue == "TUTTE")
            {
                Impresa impresa = new Impresa();
                impresa.RagioneSociale = "Tutte";

                ImpresaSelezionata(impresa);
                CaricaDatiLavoratori(0);
            }
            else
            {
                string[] tipoId = RadComboBoxImpresa.SelectedValue.Split('|');
                TipologiaImpresa tipoImpresa = (TipologiaImpresa)Int32.Parse(tipoId[0]);

                Impresa impresa = new Impresa { TipoImpresa = tipoImpresa };

                if (tipoImpresa == TipologiaImpresa.SiceNew)
                {
                    impresa.IdImpresa = Int32.Parse(tipoId[1]);
                }
                else
                {
                    if (String.IsNullOrEmpty(tipoId[1]))
                    {
                        Guid gid = new Guid(tipoId[2]);
                        impresa.IdTemporaneo = gid;
                    }
                    else
                    {
                        impresa.IdImpresa = Int32.Parse(tipoId[1]);
                    }
                }

                impresa.CodiceFiscale = tipoId[3];

                // Porcata per gestire il fatto che nella DropDown c'è anche l'ID    
                String ragioneSociale = RadComboBoxImpresa.SelectedItem.Text;
                if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                {
                    String[] splitted = ragioneSociale.Split('-');
                    ragioneSociale = String.Empty;

                    for (int i = 1; i < splitted.Length; i++)
                    {
                        ragioneSociale += splitted[i];

                        if (i > 1)
                        {
                            ragioneSociale += "-";
                        }
                    }
                }

                impresa.RagioneSociale = ragioneSociale;

                ImpresaSelezionata(impresa);

                WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);

                SubappaltoCollection subColl = domanda.Subappalti;
                ImpresaCollection impColl = new ImpresaCollection();

                if (subColl != null)
                {
                    foreach (Subappalto sub in subColl)
                    {
                        if (sub.Appaltante != null)
                        {
                            if (!impColl.Contains(sub.Appaltante))
                                impColl.Add(sub.Appaltante);
                        }

                        if (sub.Appaltata != null)
                        {
                            if (!impColl.Contains(sub.Appaltata))
                                impColl.Add(sub.Appaltata);
                        }
                    }
                }

                Impresa lavAutonomo = impColl.GetImpresaAutonomaCodFisc(impresa.CodiceFiscale);

                if (lavAutonomo == null)
                {
                    CaricaDatiLavoratori(0);
                }
                else
                {
                    CaricaDatiLavoratoreAutonomo(lavAutonomo);
                }
            }
        }

        private void ImpresaSelezionata(Impresa impresa)
        {
            ViewState["Impresa"] = impresa;
            LabelImpresaSelezionata.Text = string.Format("{0} : {1}", "Impresa selezionata", impresa.RagioneSociale);
        }

        private void CaricaDatiLavoratoreAutonomo(Impresa lavAutonomo)
        {
            ViewState["Autonomo"] = lavAutonomo;

            PanelLavoratori.Visible = false;
            PanelLavoratoriAutonomi.Visible = true;

            LabelNomeCognomeAutonomo.Text = string.Format("{0} {1}", lavAutonomo.Nome, lavAutonomo.Cognome);

            string luogoNascita = string.Empty;

            if (!string.IsNullOrEmpty(lavAutonomo.PaeseNascita))
            {
                if (lavAutonomo.PaeseNascita !=
                    "1")
                {
                    ListDictionary nazioni = _commonBiz.GetNazioni();

                    if (
                        !string.IsNullOrEmpty(
                            lavAutonomo.PaeseNascita.Trim().
                                ToUpper()))
                    {
                        foreach (DictionaryEntry naz in nazioni)
                        {
                            if (naz.Key.ToString() ==
                                lavAutonomo.PaeseNascita.Trim().
                                    ToUpper())
                            {
                                luogoNascita = naz.Value.ToString();
                                break;
                            }
                        }
                    }
                }

                else
                {
                    if (
                        !String.IsNullOrEmpty(
                            lavAutonomo.ProvinciaNascita))
                    {
                        luogoNascita =
                            String.Format("{0} ({1})",
                                          _commonBiz.GetComuneSiceNew(
                                              lavAutonomo.LuogoNascita).Comune,
                                          lavAutonomo.ProvinciaNascita);
                    }
                    else
                    {
                        luogoNascita =
                            _commonBiz.GetComuneSiceNew(
                                lavAutonomo.LuogoNascita).Comune;
                    }
                }
            }

            LabelLuogoNascitaAutonomo.Text = luogoNascita;

            if (lavAutonomo.DataNascita.HasValue)
                LabelDataNascitaAutonomo.Text = lavAutonomo.DataNascita.Value.ToShortDateString();
            else
                LabelDataNascitaAutonomo.Text = string.Empty;
            LabelRagioneSocialeAutonomo.Text = lavAutonomo.RagioneSociale;
            LabelPIvaAutonomo.Text = lavAutonomo.PartitaIva;
            LabelCodiceFiscaleAutonomo.Text = lavAutonomo.CodiceFiscale;
            if (lavAutonomo.Foto != null)
            {
                ImageFotoAutonomo.ImageUrl = "~/CeServizi/images/semaforoVerde.png";
            }
            else
            {
                ImageFotoAutonomo.ImageUrl = "~/CeServizi/images/semaforoRosso.png";
            }

            WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);
            LabelAutSubappaltoAutonomo.Text = domanda.AutorizzazioneAlSubappalto;
            LabelCommittenteAutonomo.Text = lavAutonomo.Committente;
            //if (lavAutonomo.DataAssunzione.HasValue)
            //    LabelDataAssunzioneAutonomo.Text = lavAutonomo.DataAssunzione.Value.ToShortDateString();
            //else
            //    LabelDataAssunzioneAutonomo.Text = string.Empty;
            if (lavAutonomo.DataStampaBadge.HasValue)
                LabelDataStampaBadge.Text = lavAutonomo.DataStampaBadge.Value.ToShortDateString();
            else
                LabelDataStampaBadge.Text = string.Empty;
            if
                (
                !string.IsNullOrEmpty(lavAutonomo.Nome) &&
                !string.IsNullOrEmpty(lavAutonomo.Cognome) &&
                ((!String.IsNullOrEmpty(lavAutonomo.PaeseNascita) && lavAutonomo.PaeseNascita != "1") ||
                     !String.IsNullOrEmpty(lavAutonomo.LuogoNascita)) &&
                lavAutonomo.DataNascita.HasValue &&
                //lavAutonomo.DataAssunzione.HasValue &&
                !string.IsNullOrEmpty(domanda.AutorizzazioneAlSubappalto) &&
                lavAutonomo.Foto != null
                )
            {
                ButtonStampaAutonomo.Enabled = true;
            }
            else
            {
                ButtonStampaAutonomo.Enabled = false;
            }
        }

        private void CaricaDatiLavoratori(Int32 page)
        {
            PanelLavoratori.Visible = true;
            PanelLavoratoriAutonomi.Visible = false;

            WhiteListImpresaCollection domandeImprese = biz.GetLavoratoriInDomanda((Int32)ViewState["idDomanda"]);

            int? idImpresa = null;
            Guid? idTemporaneo = null;

            if (RadComboBoxImpresa.SelectedValue == "TUTTE")
            {
                LavoratoreCollection tuttiILavoratori = new LavoratoreCollection();

                foreach (WhiteListImpresa impresa in domandeImprese)
                {
                    foreach (Lavoratore lavTemp in impresa.Lavoratori)
                    {
                        lavTemp.Impresa = impresa.Impresa;
                    }

                    tuttiILavoratori.AddRange(impresa.Lavoratori);
                }

                LavoratoreCollection lavoratoriFiltrati = FiltraLista(tuttiILavoratori);
                lavoratoriFiltrati.Sort(delegate (Lavoratore c1, Lavoratore c2) { return c1.Cognome.CompareTo(c2.Cognome); });

                Presenter.CaricaElementiInGridView(
                    RadGridLavoratori,
                    lavoratoriFiltrati);
            }
            else
            {
                string[] tipoId = RadComboBoxImpresa.SelectedValue.Split('|');
                TipologiaImpresa tipoImpresa = (TipologiaImpresa)Int32.Parse(tipoId[0]);

                if (tipoImpresa == TipologiaImpresa.SiceNew)
                {
                    idImpresa = Int32.Parse(tipoId[1]);
                }
                else
                {
                    if (String.IsNullOrEmpty(tipoId[1]))
                    {
                        Guid gid = new Guid(tipoId[2]);
                        idTemporaneo = gid;
                    }
                    else
                    {
                        idImpresa = Int32.Parse(tipoId[1]);
                    }
                }

                WhiteListImpresa domandaImpresa = domandeImprese.SelezionaDomandaImpresa(tipoImpresa, idImpresa, idTemporaneo,
                                                                                         null);
                if (domandaImpresa != null)
                {
                    if (page == 0)
                    {
                        RadGridLavoratori.CurrentPageIndex = 0;
                    }

                    foreach (Lavoratore lavTemp in domandaImpresa.Lavoratori)
                    {
                        lavTemp.Impresa = domandaImpresa.Impresa;
                    }

                    LavoratoreCollection lavoratoriFiltrati = FiltraLista(domandaImpresa.Lavoratori);

                    Presenter.CaricaElementiInGridView(
                        RadGridLavoratori,
                        lavoratoriFiltrati);
                }
            }
        }

        private LavoratoreCollection FiltraLista(LavoratoreCollection lavoratori)
        {
            LavoratoreCollection listaFiltrata = new LavoratoreCollection();

            WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);

            if (lavoratori != null)
            {
                foreach (Lavoratore lavoratore in lavoratori)
                {
                    switch (DropDownListFiltro.SelectedValue)
                    {
                        case "TUTTI":
                            listaFiltrata.Add(lavoratore);
                            break;
                        case "NOSTAMPA":
                            if (!lavoratore.DataStampaBadge.HasValue)
                            {
                                listaFiltrata.Add(lavoratore);
                            }
                            break;
                        case "STAMPA":
                            DateTime? dataStampa = RadDateInputStampatiIl.SelectedDate;

                            if (lavoratore.DataStampaBadge.HasValue
                                && (!dataStampa.HasValue || dataStampa.Value.Date == lavoratore.DataStampaBadge.Value.Date))
                            {
                                listaFiltrata.Add(lavoratore);
                            }
                            break;
                        case "DASTAMPARE":
                            if (!lavoratore.DataStampaBadge.HasValue && AccessoCantieriBusiness.BadgeReady(lavoratore, domanda.AutorizzazioneAlSubappalto))
                            {
                                listaFiltrata.Add(lavoratore);
                            }
                            break;
                    }
                }
            }

            return listaFiltrata;
        }

        protected void ButtonStampaAutonomo_Click(object sender, EventArgs e)
        {
            string fileName = string.Format("{0}.txt", LabelPIvaAutonomo.Text);

            Impresa impSel = (Impresa)ViewState["Autonomo"];

            using (MemoryStream outputFile = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(outputFile))
                {
                    sw.WriteLine(
                        "COGNOME;NOME;CODICE_FISCALE;DATA_NASCITA;LUOGO_NASCITA;RAGIONE_SOCIALE_IMPRESA;PARTITA_IVA_IMPRESA;DATA_ASSUNZIONE;AUTORIZZAZIONE_SUBAPPALTO;LAVORATORE_AUTONOMO;COMMITTENTE");
                    sw.WriteLine(ToTxtAutonomo((Impresa)ViewState["Autonomo"]));
                }

                if (impSel.IdImpresa != null) biz.UpdateDataStampaBadgeAutonomo(impSel.IdImpresa.Value);

                RestituisciFile(outputFile.ToArray(), fileName);
            }
        }

        private string ToTxtAutonomo(Impresa lavAutonomo)
        {
            WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);
            string luogoNascita = string.Empty;
            if (lavAutonomo.PaeseNascita !=
                "1")
            {
                ListDictionary nazioni = _commonBiz.GetNazioni();

                if (
                    !string.IsNullOrEmpty(
                        lavAutonomo.PaeseNascita.Trim().
                            ToUpper()))
                {
                    foreach (DictionaryEntry naz in nazioni)
                    {
                        if (naz.Key.ToString() ==
                            lavAutonomo.PaeseNascita.Trim().
                                ToUpper())
                        {
                            luogoNascita = naz.Value.ToString();
                            break;
                        }
                    }
                }
            }

            else
            {
                if (
                    !String.IsNullOrEmpty(
                        lavAutonomo.ProvinciaNascita))
                {
                    luogoNascita =
                        String.Format("{0} ({1})",
                                      _commonBiz.GetComuneSiceNew(
                                          lavAutonomo.LuogoNascita).Comune,
                                      lavAutonomo.ProvinciaNascita);
                }
                else
                {
                    luogoNascita =
                        _commonBiz.GetComuneSiceNew(
                            lavAutonomo.LuogoNascita).Comune;
                }
            }

            if (lavAutonomo.DataNascita != null)
                //if (lavAutonomo.DataAssunzione != null)
                return
                    lavAutonomo.Cognome + ";" +
                    lavAutonomo.Nome + ";" +
                    lavAutonomo.CodiceFiscale + ";" +
                    lavAutonomo.DataNascita.Value.ToShortDateString() + ";" +
                    luogoNascita + ";" +
                    lavAutonomo.RagioneSociale + ";" +
                    lavAutonomo.PartitaIva + ";" +
                    //lavAutonomo.DataAssunzione.Value.ToShortDateString() + ";" +
                    "" + ";" +
                    domanda.AutorizzazioneAlSubappalto + ";" +
                    "SI" + ";" +
                    lavAutonomo.Committente;
            return null;
        }

        private static string ToTxtLavoratore(string cognome, string nome, string codiceFiscale, string dataNascita,
                                              string luogoNascita, string ragSoc,
                                              string pIva, string dataAssunzione, string autAlSubappalto)
        {
            return
                cognome + ";" +
                nome + ";" +
                codiceFiscale + ";" +
                dataNascita + ";" +
                luogoNascita + ";" +
                ragSoc + ";" +
                pIva + ";" +
                dataAssunzione + ";" +
                autAlSubappalto + ";" +
                "NO" + ";" +
                string.Empty;
        }

        protected void ButtonResettaDataStampa_Click(object sender, EventArgs e)
        {
            foreach (GridDataItem item in RadGridLavoratori.SelectedItems)
            {
                if (item["ClientSelectColumn"].Controls[0].Visible)
                {
                    Impresa impSel = (Impresa)RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["Impresa"];

                    TipologiaLavoratore tipoLavoratore =
                        (TipologiaLavoratore)
                        RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["TipoLavoratore"];

                    Int32? idLavoratore = null;
                    Int32? idAccessoCantieriLavoratore = null;
                    Int32? idImpresa = null;
                    Int32? idAccessoCantieriImpresa = null;

                    if (tipoLavoratore == TipologiaLavoratore.SiceNew)
                    {
                        idLavoratore =
                            Int32.Parse(
                                RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["IdLavoratore"].ToString
                                    ());
                    }
                    else
                    {
                        idAccessoCantieriLavoratore =
                            Int32.Parse(
                                RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["IdLavoratore"].ToString
                                    ());
                    }

                    if (impSel.TipoImpresa == TipologiaImpresa.SiceNew)
                    {
                        idImpresa = impSel.IdImpresa;
                    }
                    else
                    {
                        idAccessoCantieriImpresa = impSel.IdImpresa;
                    }

                    biz.ResetDataStampaBadge((Int32)ViewState["idDomanda"], idLavoratore, idAccessoCantieriLavoratore,
                                                      idImpresa, idAccessoCantieriImpresa);
                }
            }

            CaricaDatiLavoratori(RadGridLavoratori.CurrentPageIndex);
        }

        protected void ButtonStampaLavoratori_Click(object sender, EventArgs e)
        {
            Impresa impFile = (Impresa)ViewState["Impresa"];

            String fileName = String.Empty;
            if (impFile != null && impFile.RagioneSociale.ToUpper() != "TUTTE")
            {
                fileName = String.Format("{0}.txt",
                                            impFile.CodiceFiscale);
            }
            else
            {
                fileName = "badge.txt";
            }

            bool ok = false;
            LavoratoreCollection lavoratori = new LavoratoreCollection();

            using (MemoryStream outputFile = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(outputFile))
                {
                    sw.WriteLine(
                        "COGNOME;NOME;CODICE_FISCALE;DATA_NASCITA;LUOGO_NASCITA;RAGIONE_SOCIALE_IMPRESA;PARTITA_IVA_IMPRESA;DATA_ASSUNZIONE;AUTORIZZAZIONE_SUBAPPALTO;LAVORATORE_AUTONOMO;COMMITTENTE");
                    foreach (GridDataItem item in RadGridLavoratori.SelectedItems)
                    {
                        if (item["ClientSelectColumn"].Controls[0].Visible)
                        {
                            Impresa impSel = (Impresa)RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["Impresa"];

                            TipologiaLavoratore tipoLavoratore =
                                (TipologiaLavoratore)
                                RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["TipoLavoratore"];
                            string nome = RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["Nome"].ToString();
                            string cognome =
                                RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["Cognome"].ToString();

                            string codiceFiscale =
                                RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["CodiceFiscale"].ToString();

                            string luogoNascita = string.Empty;
                            if (tipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                if (
                                    !String.IsNullOrEmpty(
                                        item.OwnerTableView.DataKeyValues[item.ItemIndex]["ProvinciaNascita"] as String))
                                {
                                    luogoNascita =
                                        String.Format("{0} ({1})",
                                                      item.OwnerTableView.DataKeyValues[item.ItemIndex]["LuogoNascita"],
                                                      item.OwnerTableView.DataKeyValues[item.ItemIndex]["ProvinciaNascita"]);
                                }
                                else
                                {
                                    luogoNascita =
                                        item.OwnerTableView.DataKeyValues[item.ItemIndex]["LuogoNascita"].ToString();
                                }
                            }
                            else
                            {
                                if (
                                    item.OwnerTableView.DataKeyValues[item.ItemIndex]["PaeseNascita"].ToString().Trim().ToUpper() !=
                                    "1")
                                {
                                    ListDictionary nazioni = _commonBiz.GetNazioni();

                                    if (
                                        !string.IsNullOrEmpty(
                                            item.OwnerTableView.DataKeyValues[item.ItemIndex]["PaeseNascita"].ToString().Trim().
                                                ToUpper()))
                                    {
                                        foreach (DictionaryEntry naz in nazioni)
                                        {
                                            if (naz.Key.ToString() ==
                                                item.OwnerTableView.DataKeyValues[item.ItemIndex]["PaeseNascita"].ToString().
                                                    Trim().
                                                    ToUpper())
                                            {
                                                luogoNascita = naz.Value.ToString();
                                                break;
                                            }
                                        }
                                    }
                                }

                                else
                                {
                                    if (
                                        !String.IsNullOrEmpty(
                                            item.OwnerTableView.DataKeyValues[item.ItemIndex]["ProvinciaNascita"] as String))
                                    {
                                        luogoNascita =
                                            String.Format("{0} ({1})",
                                                          _commonBiz.GetComuneSiceNew(
                                                              item.OwnerTableView.DataKeyValues[item.ItemIndex]["LuogoNascita"].
                                                                  ToString()).Comune,
                                                          item.OwnerTableView.DataKeyValues[item.ItemIndex]["ProvinciaNascita"]);
                                    }
                                    else
                                    {
                                        luogoNascita =
                                            _commonBiz.GetComuneSiceNew(
                                                item.OwnerTableView.DataKeyValues[item.ItemIndex]["LuogoNascita"].ToString()).
                                                Comune;
                                    }
                                }
                            }

                            string dataNascita = string.Empty;
                            if (RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["DataNascita"] != null)
                                dataNascita =
                                    DateTime.Parse(
                                        RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["DataNascita"].ToString())
                                        .
                                        ToShortDateString();

                            string dataAssunzione = string.Empty;
                            if (RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["DataAssunzione"] != null)
                                dataAssunzione =
                                    DateTime.Parse(
                                        RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["DataAssunzione"].
                                            ToString())
                                        .ToShortDateString();

                            if (RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["Foto"] != null)
                            {
                                Lavoratore lav = new Lavoratore
                                {
                                    CodiceFiscale =
                                                             RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex][
                                                                 "CodiceFiscale"].ToString(),
                                    Foto =
                                                             (byte[])
                                                             RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex][
                                                                 "Foto"]
                                };

                                lavoratori.Add(lav);
                            }

                            WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);

                            string ragSoc = impSel.RagioneSociale;
                            string pIva = impSel.CodiceFiscale;

                            string autAlSubappalto = domanda.AutorizzazioneAlSubappalto;

                            sw.WriteLine(ToTxtLavoratore(cognome, nome, codiceFiscale, dataNascita, luogoNascita, ragSoc, pIva,
                                                         dataAssunzione,
                                                         autAlSubappalto));

                            Int32? idLavoratore = null;
                            Int32? idAccessoCantieriLavoratore = null;

                            if (tipoLavoratore == TipologiaLavoratore.SiceNew)
                            {
                                idLavoratore =
                                    Int32.Parse(
                                        RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["IdLavoratore"].ToString
                                            ());
                            }
                            else
                            {
                                idAccessoCantieriLavoratore =
                                    Int32.Parse(
                                        RadGridLavoratori.MasterTableView.DataKeyValues[item.ItemIndex]["IdLavoratore"].ToString
                                            ());
                            }

                            Int32? idImpresa = null;
                            Int32? idAccessoCantieriImpresa = null;

                            if (impSel.TipoImpresa == TipologiaImpresa.SiceNew)
                            {
                                idImpresa = impSel.IdImpresa;
                            }
                            else
                            {
                                idAccessoCantieriImpresa = impSel.IdImpresa;
                            }

                            biz.UpdateDataStampaBadge((Int32)ViewState["idDomanda"], idLavoratore, idAccessoCantieriLavoratore,
                                                      idImpresa, idAccessoCantieriImpresa);

                            ok = true;
                        }
                    }
                }

                if (ok)
                {
                    LabelNoSel.Visible = false;
                    RestituisciFile(lavoratori, outputFile.ToArray(), fileName);
                }
                else
                {
                    LabelNoSel.Visible = true;
                }
            }
        }

        protected void RadGridLavoratori_PageIndexChanged1(object sender, GridPageChangedEventArgs e)
        {
            CaricaDatiLavoratori(e.NewPageIndex);
        }

        private void RestituisciFile(LavoratoreCollection lavoratori, byte[] txt, string fileName)
        {
            Impresa impSel = (Impresa)ViewState["Impresa"];

            String strZipFileName = String.Empty;
            if (impSel != null && impSel.RagioneSociale.ToUpper() != "TUTTE")
            {
                strZipFileName = string.Format("{0}.{1}.zip", impSel.CodiceFiscale.Trim(),
                                                  DateTime.Now.ToString("ddMMyyyy"));
            }
            else
            {
                strZipFileName = String.Format("{0}.zip", DateTime.Now.ToString("ddMMyyyy"));
            }

            using (MemoryStream outputFile = new MemoryStream())
            {
                using (ZipOutputStream s = new ZipOutputStream(outputFile))
                {
                    foreach (Lavoratore lav in lavoratori)
                    {
                        ZipEntry myEntry = new ZipEntry(string.Format("{0}.{1}", lav.CodiceFiscale, "jpg"));
                        s.PutNextEntry(myEntry);
                        s.Write(lav.Foto, 0, lav.Foto.Length);
                    }

                    ZipEntry myEntryTxt = new ZipEntry(fileName);
                    s.PutNextEntry(myEntryTxt);
                    s.Write(txt, 0, txt.Length);

                    s.Finish();

                    byte[] zipFile = outputFile.ToArray();

                    s.Close();

                    Response.AddHeader("content-disposition",
                                       String.Format("attachment; filename={0}", Path.GetFileName(strZipFileName)));
                    Response.ContentType = "application/zip";
                    Response.BinaryWrite(zipFile);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        private void RestituisciFile(byte[] txt, string filename)
        {
            Impresa impSel = (Impresa)ViewState["Autonomo"];

            WhiteList domanda = biz.GetDomandaByKey((Int32)ViewState["idDomanda"]);

            SubappaltoCollection subcoll = domanda.Subappalti;

            foreach (Subappalto sub in subcoll)
            {
                if (sub.Appaltante != null)
                {
                    if (sub.Appaltante.RagioneSociale.Trim() == impSel.RagioneSociale.Trim())
                    {
                        impSel = sub.Appaltante;
                        break;
                    }
                }

                if (sub.Appaltata != null)
                {
                    if (sub.Appaltata.RagioneSociale.Trim() == impSel.RagioneSociale.Trim())
                    {
                        impSel = sub.Appaltata;
                        break;
                    }
                }
            }

            string strZipFileName = string.Format("{0}.{1}.zip", impSel.CodiceFiscale.Trim(),
                                                  DateTime.Now.ToString("ddMMyyyy"));

            using (MemoryStream outputFile = new MemoryStream())
            {
                using (ZipOutputStream s = new ZipOutputStream(outputFile))
                {
                    ZipEntry myEntry = new ZipEntry(string.Format("{0}.{1}", impSel.CodiceFiscale, "jpg"));
                    s.PutNextEntry(myEntry);
                    s.Write(impSel.Foto, 0, impSel.Foto.Length);

                    ZipEntry myEntryTxt = new ZipEntry(filename);
                    s.PutNextEntry(myEntryTxt);
                    s.Write(txt, 0, txt.Length);

                    s.Finish();

                    byte[] zipFile = outputFile.ToArray();

                    s.Close();

                    Response.AddHeader("content-disposition",
                                       String.Format("attachment; filename={0}", strZipFileName));
                    Response.ContentType = "application/zip";
                    Response.BinaryWrite(zipFile);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        protected void DropDownListFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            CaricaDatiLavoratori(RadGridLavoratori.MasterTableView.CurrentPageIndex);

            if (DropDownListFiltro.SelectedValue == "STAMPA")
            {
                RadDateInputStampatiIl.Enabled = true;
                ButtonAggiornaDataStampa.Enabled = true;
            }
            else
            {
                RadDateInputStampatiIl.Clear();
                RadDateInputStampatiIl.Enabled = false;
                ButtonAggiornaDataStampa.Enabled = false;
            }
        }

        protected void ButtonAggiornaDataStampa_Click(object sender, EventArgs e)
        {
            CaricaDatiLavoratori(RadGridLavoratori.MasterTableView.CurrentPageIndex);
        }

        protected void RadComboBoxImpresa_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(RadComboBoxImpresa.SelectedValue))
            {
                SelezionaImpresaDaDropDowList();
                LabelNoSel.Visible = false;
            }
            else
            {
                ViewState["Impresa"] = null;
                LabelImpresaSelezionata.Text = "Nessuna impresa selezionata";
            }
        }
    }
}