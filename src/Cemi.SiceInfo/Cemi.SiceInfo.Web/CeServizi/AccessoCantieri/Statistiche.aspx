﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="Statistiche.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.Statistiche" %>

<%@ Register Src="../WebControls/MenuAccessoCantieri.ascx" TagName="MenuAccessoCantieri"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <br />
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Statistiche"
        titolo="Accesso ai Cantieri" />
    <br />
    <br />
    <%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">--%>
    <div>
        <telerik:RadTabStrip ID="RadTabStripStatistiche" runat="server" MultiPageID="RadMultiPageStatistiche"
            SelectedIndex="0" Width="100%">
            <Tabs>
                <telerik:RadTab Text="Cantieri" Width="100px">
                </telerik:RadTab>
                <telerik:RadTab Text="Timbrature" Width="100px">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPageStatistiche" runat="server" SelectedIndex="0"
            CssClass="radWizard" Width="96%" RenderSelectedPageOnly="False">
            <telerik:RadPageView ID="RadPageViewCantieri" runat="server">
                <div>
                    <br />
                    <b>Cantieri </b>
                </div>
                <br />
                <div>
                    <asp:Panel ID="PanelCantieri" runat="server">
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Totale imprese appaltatrici censite in Cassa Edile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleImpreseCEAppaltatrici" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Totale imprese subappaltate censite in Cassa Edile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleImpreseCESubappaltate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="Totale imprese appaltatrici non censite in Cassa Edile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleImpreseNuoveAppaltatrici" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text="Totale imprese subappaltate non censite in Cassa Edile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleImpreseNuoveSubappaltate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="Totale imprese artigiane appaltatrici:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleImpreseArtigianeAppaltatrici" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Totale imprese artigiane subappaltate:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleImpreseArtigianeSubappaltate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text="Totale lavoratori censiti in Cassa Edile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleLavoratoriCE" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Totale lavoratori non censiti in Cassa Edile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleLavoratoriNuovi" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label9" runat="server" Text="Totale timbrature:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="LabelTotaleTimbrature" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Panel ID="PanelRicerca" runat="server">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Indirizzo cantiere
                                    </td>
                                    <td>
                                        Comune cantiere
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaCantieri"
                                            OnClick="ButtonVisualizza_Click" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:GridView ID="GridViewCantieri" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataKeyNames="idWhiteList,indirizzo,comune,provincia" OnPageIndexChanging="GridViewCantieri_PageIndexChanging"
                                OnSelectedIndexChanging="GridViewCantieri_SelectedIndexChanging" OnRowDataBound="GridViewCantieri_RowDataBound"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="Cantiere">
                                        <ItemTemplate>
                                            <table class="standardTable">
                                                <tr>
                                                    <td class="accessoCantieriTemplateTableTd">
                                                        Ind:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelIndirizzo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Com:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelComune" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Prov:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="LabelProvincia" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" DeleteText="" HeaderText="Seleziona"
                                        ShowSelectButton="True">
                                        <ItemStyle Width="10px" />
                                    </asp:CommandField>
                                </Columns>
                                <EmptyDataTemplate>
                                    Nessun cantiere presente
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </asp:Panel>
                    </asp:Panel>
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewTimbrature" runat="server">
                <div>
                    <b>Timbrature </b>
                </div>
                <br />
                <div>
                    <asp:Panel ID="Paneltimbrature" runat="server">
                        <asp:Panel ID="PanelFiltroTimb" runat="server" Visible="True">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Data inizio (gg/mm/aaaa)
                                    </td>
                                    <td>
                                        Data fine (gg/mm/aaaa)
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadDatePicker ID="RadDatePickerDataInizio" runat="server" Width="150px" />
                                    </td>
                                    <td>
                                        <telerik:RadDatePicker ID="RadDatePickerDataFine" runat="server" Width="150px" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Cantiere
                                    </td>
                                    <td>
                                        Fornitore
                                    </td>
                                    <td>
                                        Codice rilevatore
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DropDownListCantiere" runat="server" AppendDataBoundItems="True"
                                            AutoPostBack="False" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListFornitore" runat="server" AppendDataBoundItems="True"
                                            AutoPostBack="True" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListRilevatore" runat="server" AppendDataBoundItems="True"
                                            AutoPostBack="False" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="ButtonVisualizzaTimb" runat="server" OnClick="ButtonVisualizzaTimb_Click"
                                            Text="Ricerca" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Totale timbrature:
                                    </td>
                                    <td>
                                        <asp:Label ID="LabelTotaleTimbratureFiltrate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <br />
                        </asp:Panel>
                    </asp:Panel>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <%-- </telerik:RadAjaxPanel>--%>
</asp:Content>

