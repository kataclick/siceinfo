﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriCantiere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriCantiere" %>
<%@ Register Src="../../WebControls/SelezioneIndirizzoLibero.ascx" TagName="SelezioneIndirizzoLibero"
    TagPrefix="uc1" %>
<table class="filledtable">
    <tr>
        <td>
            Indirizzo
        </td>
    </tr>
</table>
<uc1:SelezioneIndirizzoLibero ID="SelezioneIndirizzoLiberoCantiere" runat="server" />
<asp:Label ID="LabelNessunIndirizzo" runat="server" Font-Size="X-Small">Per inserire un indirizzo immettere i dati nei campi e cliccare sul pulsante &quot;Geocodifica&quot;</asp:Label>
<br />
<br />
<table class="filledtable">
    <tr>
        <td>
            Lavori
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td class="accessoCantieriTd">
            Data inizio lavori (gg/mm/aaaa)<b>*</b>:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDateDataInizioLavori" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="RadDateDataInizioLavori"
                ErrorMessage="Inserire una data per l'inizio dei lavori" ValidationGroup="stop">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data fine lavori (gg/mm/aaaa)<b>*</b>:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDateDataFineLavori" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="RadDateDataFineLavori"
                ErrorMessage="Inserire una data per la fine dei lavori." ValidationGroup="stop">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidatorDataLavori" runat="server" ControlToValidate="RadDateDataFineLavori"
                ValidationGroup="stop" ErrorMessage="La data di inizio lavori non può essere superiore a quella di fine lavori."
                OnServerValidate="CustomValidatorDataLavori_ServerValidate">
                    *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Descrizione lavori (opere appaltate) nel cantiere:
        </td>
        <td>
            <asp:TextBox ID="TextBoxDescrizione" runat="server" Height="60px" Width="200px" TextMode="MultiLine"
                MaxLength="255"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Autorizzazione n°: 
            <br />
            <small>(obbligatorio per la stampa dei badge per i lavoratori)</small>
        </td>
        <td>
            <asp:TextBox ID="TextBoxAutorizzazioneAlSubappalto" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td>
        </td>
    </tr>
</table>
