﻿using System;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;
using System.Text.RegularExpressions;
using Cemi.SiceInfo.Utility;
using Cemi.SiceInfo.Web.Helpers;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriImpreseSubappalti : System.Web.UI.UserControl
    {
        //private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        /// <summary>
        ///   Proprietà che costruisce il committente. Se sono presenti errori ritorna null
        /// </summary>
        private readonly Common _bizCommon = new Common();

        protected string PostBackString;

        public Impresa Impresa
        {
            get
            {
                Page.Validate("ValidaInserimentoImpresa");

                if (Page.IsValid)
                {
                    return CreaImpresa();
                }
                return null;
            }
        }

        public string Errore { get; private set; }

        //private static int IndiceComuneInDropDown(DropDownList dropDownListComuni, string codiceCatastale)
        //{
        //    int res = 0;

        //    for (int i = 0; i < dropDownListComuni.Items.Count; i++)
        //    {
        //        string valoreCombo = dropDownListComuni.Items[i].Value;
        //        string[] splitted = valoreCombo.Split('|');
        //        if (splitted.Length > 1)
        //        {
        //            if (splitted[0] == codiceCatastale)
        //            {
        //                res = i;
        //                break;
        //            }
        //        }
        //    }

        //    return res;
        //}

        public void ImpostaCommittenteArtigiano(string ragioneSociale)
        {
            TextBoxCommittente.Text = ragioneSociale;
        }

        //public void ImpostaImpresa(Impresa impresa)
        //{
        //    ViewState["IdImpresa"] = impresa.IdImpresa;
        //    TextBoxPartitaIva.Text = impresa.PartitaIva;
        //    TextBoxIndirizzo.Text = impresa.Indirizzo;

        //    CheckBoxArtigiano.Checked = impresa.LavoratoreAutonomo;
        //    TextBoxCognome.Text = impresa.Cognome;
        //    TextBoxNome.Text = impresa.Nome;
        //    if (impresa.DataNascita.HasValue)
        //        RadDateDataNascita.SelectedDate = impresa.DataNascita.Value;
        //    TextBoxNumeroCameraCommercio.Text = impresa.NumeroCameraCommercio;


        //    if (!string.IsNullOrEmpty(impresa.PaeseNascita))
        //    {
        //        DropDownListPaeseNascita.SelectedValue = impresa.PaeseNascita;

        //        if (DropDownListPaeseNascita.SelectedValue == "1")
        //        {
        //            // ITALIA
        //            if (!string.IsNullOrEmpty(impresa.ProvinciaNascita) &&
        //                DropDownListProvinciaNascita.Items.FindByValue(impresa.ProvinciaNascita) !=
        //                null)
        //            {
        //                DropDownListProvinciaNascita.SelectedValue = impresa.ProvinciaNascita;
        //                AccessoCantieriBusiness.CaricaComuni(DropDownListComuneNascita, impresa.ProvinciaNascita, null);
        //            }
        //            if (!string.IsNullOrEmpty(impresa.LuogoNascita))
        //                DropDownListComuneNascita.SelectedIndex =
        //                    IndiceComuneInDropDown(DropDownListComuneNascita,
        //                                           impresa.LuogoNascita);

        //            DropDownListPaeseNascita.Enabled = true;

        //            DropDownListProvinciaNascita.Enabled = true;
        //            DropDownListComuneNascita.Enabled = true;
        //        }
        //        else
        //        {
        //            DropDownListPaeseNascita.Enabled = true;

        //            DropDownListProvinciaNascita.Enabled = false;
        //            DropDownListComuneNascita.Enabled = false;
        //        }
        //    }


        //    if (impresa.DataAssunzione.HasValue)
        //        RadDateDataNascita.SelectedDate = impresa.DataAssunzione.Value;
        //    TextBoxCommittente.Text = impresa.Committente;

        //    if (impresa.Foto != null)
        //    {
        //        ViewState["Foto"] = impresa.Foto;

        //        RadBinaryImageFoto.DataValue = impresa.Foto;
        //        RadBinaryImageFoto.DataBind();
        //    }
        //    else
        //    {
        //        ViewState["Foto"] = null;

        //        RadBinaryImageFoto.ImageUrl = "~/images/noImg.JPG";
        //        RadBinaryImageFoto.DataBind();
        //    }

        //    // Provincia            
        //    ListItem prov = DropDownListProvincia.Items.FindByText(impresa.Provincia);
        //    if (prov != null)
        //    {
        //        DropDownListProvincia.SelectedValue = prov.Value;
        //        CaricaComuni(Int32.Parse(prov.Value));
        //    }

        //    // Comune
        //    ListItem com = DropDownListComuni.Items.FindByText(impresa.Comune);
        //    if (com != null)
        //    {
        //        DropDownListComuni.SelectedValue = com.Value;
        //        CaricaCAP(Int64.Parse(com.Value));
        //    }

        //    // Cap
        //    ListItem cap = DropDownListCAP.Items.FindByText(impresa.Cap);
        //    if (cap != null)
        //    {
        //        DropDownListCAP.SelectedValue = cap.Value;
        //    }

        //    if (DropDownListTipoAttivita.Items.FindByText(impresa.TipoAttivita) != null)
        //    {
        //        DropDownListTipoAttivita.SelectedValue = impresa.TipoAttivita;
        //    }
        //    else
        //    {
        //        DropDownListTipoAttivita.Text = "Sconosciuta";
        //    }
        //}

        public void Reset()
        {
            Presenter.SvuotaCampo(TextBoxIdImpresa);
            Presenter.SvuotaCampo(TextBoxIndirizzo);
            Presenter.SvuotaCampo(TextBoxPartitaIva);
            Presenter.SvuotaCampo(TextBoxRagioneSociale);
            Presenter.SvuotaCampo(TextBoxCodiceFiscale);
            Presenter.SvuotaCampo(TextBoxCognome);
            Presenter.SvuotaCampo(TextBoxNome);
            RadDateDataNascita.Clear();
            Presenter.SvuotaCampo(TextBoxNumeroCameraCommercio);


            RadDateDataNascita.Clear();
            Presenter.SvuotaCampo(TextBoxCommittente);

            CheckBoxArtigiano.Checked = false;

            RadUploadFoto.CssClass =
                DropDownListProvinciaNascita.CssClass =
                DropDownListComuneNascita.CssClass =
                DropDownListPaeseNascita.CssClass =
                //RadDatePickerDataAssunzione.CssClass =
                TextBoxCognome.CssClass =
                TextBoxNome.CssClass =
                RadDateDataNascita.CssClass =
                TextBoxNumeroCameraCommercio.CssClass =
                "campoDisabilitato";

            RadUploadFoto.Enabled =
                ButtonRemoveFoto.Enabled =
                DropDownListPaeseNascita.Enabled =
                //RadDatePickerDataAssunzione.Enabled =
                TextBoxCognome.Enabled =
                TextBoxNome.Enabled =
                RadDateDataNascita.Enabled =
                TextBoxNumeroCameraCommercio.Enabled =
                CheckBoxArtigiano.Checked;

            CaricaTipologieContratto();
            CaricaProvince();
            CaricaComuni(-1);
            CaricaCap(-1);
        }

        private Impresa CreaImpresa()
        {
            Impresa impresa = new Impresa();

            if (ViewState["IdImpresa"] != null)
            {
                impresa.IdImpresa = (int?)ViewState["IdImpresa"];
            }

            impresa.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
            impresa.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);

            impresa.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
            if (!string.IsNullOrEmpty(TextBoxPartitaIva.Text.Trim()))
                impresa.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxPartitaIva.Text);

            if (!string.IsNullOrEmpty(DropDownListProvincia.SelectedItem.ToString()))
                impresa.Provincia = DropDownListProvincia.SelectedItem.Text;
            if (DropDownListComuni.SelectedItem != null)
                impresa.Comune = DropDownListComuni.SelectedItem.Text;
            if (DropDownListCAP.SelectedItem != null)
                impresa.Cap = DropDownListCAP.SelectedValue;

            if (!string.IsNullOrEmpty(DropDownListTipologiaContratto.SelectedValue))
            {
                impresa.TipologiaContratto =
                    (TipologiaContratto)
                    Enum.Parse(typeof(TipologiaContratto), DropDownListTipologiaContratto.SelectedValue);
            }

            if (!string.IsNullOrEmpty(DropDownListTipoAttivita.SelectedValue))
                impresa.TipoAttivita = DropDownListTipoAttivita.SelectedValue;

            impresa.TipoImpresa = TipologiaImpresa.Nuova;
            impresa.IdTemporaneo = Guid.NewGuid();

            impresa.LavoratoreAutonomo = CheckBoxArtigiano.Checked;

            if (CheckBoxArtigiano.Checked)
            {
                impresa.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
                impresa.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
                if (RadDateDataNascita.SelectedDate != null) impresa.DataNascita = RadDateDataNascita.SelectedDate.Value;
                impresa.NumeroCameraCommercio = Presenter.NormalizzaCampoTesto(TextBoxNumeroCameraCommercio.Text);

                if (DropDownListPaeseNascita.SelectedValue != null)
                {
                    if (DropDownListPaeseNascita.SelectedValue == "1")
                    {
                        if (DropDownListProvinciaNascita.SelectedValue != null)
                        {
                            impresa.ProvinciaNascita =
                                DropDownListProvinciaNascita.SelectedValue;
                        }

                        if (DropDownListComuneNascita.SelectedValue != null)
                        {
                            impresa.LuogoNascita =
                                AccessoCantieriBusiness.GetCodiceCatastaleDaCombo(
                                    DropDownListComuneNascita.SelectedValue);
                        }
                    }

                    impresa.PaeseNascita = DropDownListPaeseNascita.SelectedValue;
                }
                else
                {
                    impresa.LuogoNascita = null;
                    impresa.PaeseNascita = null;
                    impresa.ProvinciaNascita = null;
                }


                //if (RadDatePickerDataAssunzione.SelectedDate != null)
                //    impresa.DataAssunzione = RadDatePickerDataAssunzione.SelectedDate.Value;
                impresa.Committente = Presenter.NormalizzaCampoTesto(TextBoxCommittente.Text);

                if (RadUploadFoto.UploadedFiles.Count > 0)
                {
                    byte[] bytes = new byte[RadUploadFoto.UploadedFiles[0].ContentLength];
                    RadUploadFoto.UploadedFiles[0].InputStream.Read(bytes, 0, (int) RadUploadFoto.UploadedFiles[0].ContentLength);

                    ViewState["Foto"] = bytes;

                    RadBinaryImageFoto.DataValue = bytes;
                    RadBinaryImageFoto.DataBind();
                }

                if (ViewState["FotoMod"] != null)
                {
                    ViewState["Foto"] = ViewState["FotoMod"];
                    RadBinaryImageFoto.DataValue = (byte[])ViewState["Foto"];
                    RadBinaryImageFoto.DataBind();
                }

                impresa.Foto = (byte[])ViewState["Foto"];
            }

            return impresa;
        }


        protected void ButtonRemoveFoto_Click(object sender, EventArgs e)
        {
            ViewState["Foto"] = null;

            RadBinaryImageFoto.ImageUrl = "~/CeServizi/images/noImg.JPG";
            RadBinaryImageFoto.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PostBackString = Page.ClientScript.GetPostBackEventReference(this, null);

            if (!Page.IsPostBack)
            {
                CaricaProvince();
                CaricaTipologieContratto();
                CaricaTipologieAttivita();

                CaricaCombo();
            }
            else
            {
                if (RadUploadFoto.UploadedFiles.Count > 0)
                {
                    byte[] bytes = new byte[RadUploadFoto.UploadedFiles[0].ContentLength];
                    RadUploadFoto.UploadedFiles[0].InputStream.Read(bytes, 0, (int) RadUploadFoto.UploadedFiles[0].ContentLength);

                    ViewState["FotoMod"] = bytes;

                    RadBinaryImageFoto.DataValue = bytes;
                    RadBinaryImageFoto.DataBind();
                }
            }
        }

        private void CaricaCombo()
        {
            // Province
            StringCollection province = _bizCommon.GetProvinceSiceNew();

            DropDownListProvinciaNascita.Items.Clear();
            DropDownListProvinciaNascita.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListProvinciaNascita.DataSource = province;
            DropDownListProvinciaNascita.DataBind();

            // Paese
            ListDictionary nazioni = _bizCommon.GetNazioni();

            DropDownListPaeseNascita.Items.Clear();
            DropDownListPaeseNascita.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListPaeseNascita.DataSource = nazioni;
            DropDownListPaeseNascita.DataTextField = "Value";
            DropDownListPaeseNascita.DataValueField = "Key";
            DropDownListPaeseNascita.DataBind();
        }

        private void CaricaTipologieContratto()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListTipologiaContratto,
                Enum.GetNames(typeof(TipologiaContratto)),
                "",
                "");
        }

        public void SetValidationGroup(string validationGroup)
        {
            RequiredFieldValidatorRagSoc.ValidationGroup = validationGroup;
            RegularExpressionValidatorPIVA.ValidationGroup = validationGroup;
            CustomValidatorCodiceFiscale.ValidationGroup = validationGroup;
            CustomValidatorCognome.ValidationGroup = validationGroup;
            CustomValidatorDataNascita.ValidationGroup = validationGroup;
            CustomValidatorFoto.ValidationGroup = validationGroup;
            CustomValidatorItaliano.ValidationGroup = validationGroup;
            CustomValidatorNome.ValidationGroup = validationGroup;
            CustomValidatorPartitaIva.ValidationGroup = validationGroup;
            CustomValidatorTipologiaContratto.ValidationGroup = validationGroup;
            RequiredFieldValidatorCodiceFiscale.ValidationGroup = validationGroup;
            RequiredFieldValidatorPartitaIVA.ValidationGroup = validationGroup;
        }

        protected void CheckBoxArtigiano_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxArtigiano.Checked)
            {
                RadUploadFoto.CssClass =
                    DropDownListProvinciaNascita.CssClass =
                    DropDownListComuneNascita.CssClass =
                    DropDownListPaeseNascita.CssClass =
                    //RadDatePickerDataAssunzione.CssClass =
                    TextBoxCognome.CssClass =
                    TextBoxNome.CssClass =
                    RadDateDataNascita.CssClass =
                    TextBoxNumeroCameraCommercio.CssClass = string.Empty;
            }
            else
            {
                RadUploadFoto.CssClass =
                    DropDownListProvinciaNascita.CssClass =
                    DropDownListComuneNascita.CssClass =
                    DropDownListPaeseNascita.CssClass =
                    //RadDatePickerDataAssunzione.CssClass =
                    TextBoxCognome.CssClass =
                    TextBoxNome.CssClass =
                    RadDateDataNascita.CssClass =
                    TextBoxNumeroCameraCommercio.CssClass = "campoDisabilitato";
            }

            TextBoxCognome.Text =
                TextBoxNome.Text =
                TextBoxNumeroCameraCommercio.Text = string.Empty;

            RadDateDataNascita.Clear();
            //RadDatePickerDataAssunzione.Clear();

            RadUploadFoto.Enabled =
                ButtonRemoveFoto.Enabled =
                DropDownListPaeseNascita.Enabled =
                //RadDatePickerDataAssunzione.Enabled =
                TextBoxCognome.Enabled =
                TextBoxNome.Enabled =
                RadDateDataNascita.Enabled =
                TextBoxNumeroCameraCommercio.Enabled = CheckBoxArtigiano.Checked;
        }

        protected void CustomValidatorCognome_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxCognome.Text.Trim())))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorTipologiaContratto_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if ((CheckBoxArtigiano.Checked) && (DropDownListTipologiaContratto.SelectedValue == ""))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorNome_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxNome.Text.Trim())))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorDataNascita_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if ((CheckBoxArtigiano.Checked) && (!RadDateDataNascita.SelectedDate.HasValue))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        //protected void CustomValidatorNumeroCameraCommercio_ServerValidate(object source, ServerValidateEventArgs args)
        //{
        //    if ((CheckBoxArtigiano.Checked) && (string.IsNullOrEmpty(TextBoxNumeroCameraCommercio.Text.Trim())))
        //        args.IsValid = false;
        //    else
        //        args.IsValid = true;
        //}

        protected void CustomValidatorPartitaIva_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!String.IsNullOrEmpty(TextBoxPartitaIva.Text) && TextBoxPartitaIva.Text.Length == 11)
            {
                args.IsValid = PartitaIvaManager.VerificaPartitaIva(TextBoxPartitaIva.Text);
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;

            try
            {
                if (!String.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
                {
                    if (!CheckBoxArtigiano.Checked)
                    {
                        if (TextBoxCodiceFiscale.Text.Length == 11)
                        {
                            args.IsValid = PartitaIvaManager.VerificaPartitaIva(TextBoxCodiceFiscale.Text);
                        }
                        else if (TextBoxCodiceFiscale.Text.Length == 16)
                        {
                            args.IsValid = Regex.IsMatch(TextBoxCodiceFiscale.Text,
                                                         @"^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$");
                        }
                    }
                    else
                    {
                        if (TextBoxCodiceFiscale.Text.Length == 16)
                        {
                            // Vecchia validazione
                            //args.IsValid = Regex.IsMatch(TextBoxCodiceFiscale.Text,
                            //                             @"^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$");

                            if (RadDateDataNascita.SelectedDate.HasValue)
                            {
                                DateTime dataNascita = RadDateDataNascita.SelectedDate.Value;

                                if (!string.IsNullOrEmpty(TextBoxNome.Text) && !string.IsNullOrEmpty(TextBoxCognome.Text))
                                {
                                    string maschio = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                                               TextBoxCognome.Text, "M",
                                                                                                               dataNascita);
                                    string femmina = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                                               TextBoxCognome.Text, "F",
                                                                                                               dataNascita);

                                    if (maschio == TextBoxCodiceFiscale.Text.Substring(0, 11).ToUpper() ||
                                        femmina == TextBoxCodiceFiscale.Text.Substring(0, 11).ToUpper())
                                    {
                                        args.IsValid = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        protected void CustomValidatorFoto_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = RadUploadFoto.InvalidFiles.Count == 0;
        }

        private void CaricaTipologieAttivita()
        {
            DropDownListTipoAttivita.DataSource = AccessoCantieriBiz.GetTipologieAttivita();
            DropDownListTipoAttivita.Text = "Sconosciuta";
            DropDownListTipoAttivita.DataBind();
        }

        protected void CustomValidatorItaliano_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (DropDownListPaeseNascita.SelectedValue == "1"
                && (String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue)
                    || String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue)))
            {
                args.IsValid = false;
            }
        }

        #region Biz properties

        public AccessoCantieriBusiness AccessoCantieriBiz { private get; set; }

        #endregion

        #region Metodi per la gestione delle province, comuni, cap

        private void CaricaProvince()
        {
            AccessoCantieriBiz.CaricaProvinceInDropDown(DropDownListProvincia);
        }

        private void CaricaComuni(int idProvincia)
        {
            AccessoCantieriBiz.CaricaComuniInDropDown(DropDownListComuni, idProvincia);
        }

        private void CaricaCap(Int64 idComune)
        {
            AccessoCantieriBiz.CaricaCapInDropDown(DropDownListCAP, idComune);
        }

        protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            AccessoCantieriBiz.CambioSelezioneDropDownProvincia(DropDownListProvincia, DropDownListComuni,
                                                                DropDownListCAP);
        }

        protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
        {
            AccessoCantieriBiz.CambioSelezioneDropDownComune(DropDownListComuni, DropDownListCAP);
        }

        protected void DropDownListPaeseNascita_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Se Italia attivo ricerca province, comuni,...
            // altrimenti visualizzo solo i comuni e prendo quelli internazionali
            if (DropDownListPaeseNascita.SelectedValue == "1")
            {
                DropDownListProvinciaNascita.Enabled = true;
                DropDownListComuneNascita.Enabled = true;
            }
            else
            {
                DropDownListComuneNascita.ClearSelection();
                DropDownListProvinciaNascita.ClearSelection();

                DropDownListProvinciaNascita.Enabled = false;
                DropDownListComuneNascita.Enabled = false;
            }
        }

        protected void DropDownListProvinciaNascita_SelectedIndexChanged(object sender, EventArgs e)
        {
            AccessoCantieriBusiness.CaricaComuni(DropDownListComuneNascita, DropDownListProvinciaNascita.SelectedValue, null);
        }

        #endregion
    }
}