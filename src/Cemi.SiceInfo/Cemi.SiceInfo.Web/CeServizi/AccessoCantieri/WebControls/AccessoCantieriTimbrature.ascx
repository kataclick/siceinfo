﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriTimbrature.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriTimbrature" %>
<%@ Register Src="AccessoCantieriRilevatoriInserimento.ascx" TagName="AccessoCantieriRilevatoriInserimento"
    TagPrefix="uc7" %>
<%@ Register src="SelezioneCantiere.ascx" tagname="SelezioneCantiere" tagprefix="uc1" %>
<table class="standardTable" width="100%">
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Cantiere"></asp:Label>
            <uc1:SelezioneCantiere ID="SelezioneCantiere1" runat="server" />
            <br />
            <asp:Panel ID="PanelRilevatori" runat="server" Width="100%">
                <b>
                    <asp:Label ID="LabelTimbrature" runat="server" Font-Bold="True" Text="Elenco timbrature per il cantiere" Visible="False"></asp:Label>
                </b>
                <br />
                <asp:Panel ID="PanelFiltroTimb" runat="server" Visible="False">
                    <table class="standardTable" width="100%">
                        <tr>
                            <td>
                                Impresa
                            </td>
                            <td>                               
                                Codice fiscale
                            </td>
                            <td>
                                Ruolo
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownListImpresa" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="False" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListRuolo" runat="server" AppendDataBoundItems="True" AutoPostBack="False" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                 Data inizio (gg/mm/aaaa)
                                <asp:CompareValidator
                                    ID="CompareValidatorDataInizio"
                                    runat="server"
                                    ControlToValidate="TextBoxDataInizio"
                                    Type="Date"
                                    Operator="DataTypeCheck"
                                    ErrorMessage="Formato Data inizio errato"
                                    ForeColor="Red"
                                    ValidationGroup="ricercaTimbrature">
                                    *
                                </asp:CompareValidator>
                                <asp:RequiredFieldValidator 
                                    ID="RequiredFieldValidatorDataInizio" 
                                    runat="server" 
                                    ControlToValidate="TextBoxDataInizio"
                                    ForeColor="Red"
                                    ErrorMessage="Specificare la data di inizio"
                                    ValidationGroup="ricercaTimbrature">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                Data fine (gg/mm/aaaa)
                                <asp:CompareValidator
                                    ID="CompareValidatorDataFine"
                                    runat="server"
                                    ControlToValidate="TextBoxDataFine"
                                    Type="Date"
                                    Operator="DataTypeCheck"
                                    ErrorMessage="Formato Data fine errato"
                                    ForeColor="Red"
                                    ValidationGroup="ricercaTimbrature">
                                    *
                                </asp:CompareValidator>
                                <asp:RequiredFieldValidator 
                                    ID="RequiredFieldValidatorDataFine" 
                                    runat="server" 
                                    ControlToValidate="TextBoxDataFine"
                                    ForeColor="Red"
                                    ErrorMessage="Specificare la data di fine"
                                    ValidationGroup="ricercaTimbrature">
                                    *
                                </asp:RequiredFieldValidator>
                                <asp:CustomValidator 
                                    OnServerValidate="ValidateDuration"
                                    ForeColor="Red"
                                    ErrorMessage="Il periodo di ricerca non può superare i 31 giorni"
                                    ValidationGroup="ricercaTimbrature"
                                    runat="server" >
                                    *
                                </asp:CustomValidator>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBoxDataInizio" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxDataFine" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:ValidationSummary
                                    ID="ValidationSummaryRicercaTimbrature"
                                    runat="server"
                                    ValidationGroup="ricercaTimbrature"
                                    ForeColor="Red" />
                            </td>
<!--                            <td>
                                &nbsp;
                            </td>
                            
                            <td>
                                &nbsp;
                            </td>-->
                            <td align="right">
                                <asp:Button ID="ButtonVisualizzaTimb" runat="server" OnClick="ButtonVisualizzaTimb_Click"
                                    Text="Ricerca" ValidationGroup="ricercaTimbrature" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <br />
                </asp:Panel>
                <asp:GridView ID="GridViewTimbrature" runat="server" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="IdTimbratura" OnRowDataBound="GridViewTimbrature_RowDataBound"
                    AllowSorting="True"
                    AllowPaging="True" OnPageIndexChanging="GridViewTimbrature_PageIndexChanging"
                    PageSize="15">
                    <Columns>
                        <asp:TemplateField HeaderText="Rilevatore" Visible="False">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Codice:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCodiceRilevatore" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fornitore:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelFornitore" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Posizione GPS" Visible="False">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Lat.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelLatitudine" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Long.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelLongitudine" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="codiceFiscale" HeaderText="Codice fiscale" Visible="False" />
                        <asp:BoundField DataField="dataOra" HeaderText="Data e ora" />
                        <asp:TemplateField HeaderText="Ingresso/Uscita">
                            <ItemTemplate>
                                <asp:Label ID="LabelIngressoUscita" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Persona">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td class="accessoCantieriTemplateTableTd">
                                            Nome:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelNomeCognome" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cod. Fisc.:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Ruolo:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Codice CE:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCodiceCE" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Anomalie">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelAnomalia" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelAnomaliaRiga2" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Label ID="LabelAnomaliaRiga3" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna timbratura presente
                    </EmptyDataTemplate>
                    <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                </asp:GridView>
                <br />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAggiungiRilevatore" runat="server" Visible="False">
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Button ID="ButtonStampaSelezione" runat="server" Text="Stampa lista selezionata" OnClick="ButtonStampaSelezione_Click"
                            Visible="False" Width="150px" ValidationGroup="ricercaTimbrature"/>
                    </td>
                    <td>
                        <asp:Button ID="ButtonStampa" runat="server" Text="Stampa anomalie" Visible="False"
                            OnClick="ButtonStampa_Click" Width="150px" />
                    </td>
                    
                </tr>
            </table>
        </td>
    </tr>
</table>

