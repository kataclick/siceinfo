﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriReferenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriReferenti" %>
<%@ Register Src="AccessoCantieriReferentiInserimento.ascx" TagName="AccessoCantieriReferentiInserimento"
    TagPrefix="uc7" %>
<table class="standardTable">
    <tr>
        <td>
            <br />
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco referenti del cantiere"></asp:Label>
            <br />
            <br />
            <asp:GridView ID="GridViewReferenti" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewReferenti_RowDataBound"
                Width="100%" OnRowDeleting="GridViewReferenti_RowDeleting" DataKeyNames="IdReferente">
                <Columns>
                    <asp:TemplateField HeaderText="Referente">
                        <ItemTemplate>
                            <asp:Label ID="LabelReferente" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="codiceFiscale" HeaderText="Codice Fiscale" />
                    <asp:TemplateField HeaderText="Ruolo">
                        <ItemTemplate>
                            <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contatto">
                        <ItemTemplate>
                            <asp:Label ID="LabelContatto" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ButtonType="Image" DeleteText="" ShowDeleteButton="True" DeleteImageUrl="../../images/pallinoX.png">
                        <ItemStyle Width="10px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna referente presente
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <br />
            <asp:Label ID="LabelGiaPresente" runat="server" ForeColor="Red" Text="Referente già presente"
                Visible="False"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <table class="filledtable">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Referente"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td>
                        <br />
                        <uc7:AccessoCantieriReferentiInserimento ID="AccessoCantieriReferentiInserimento1"
                            runat="server" />
                        <br />
                        <br />
                        <asp:Label ID="LabelInserimentoReferente" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <asp:Button ID="ButtonAggiungiReferente" runat="server" Text="Aggiungi" Width="225px"
                            OnClick="ButtonaggiungiReferente_Click" CausesValidation="False" Enabled="True"
                            Style="text-align: center" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
