﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using UtenteImpresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriTimbratureImpresa : System.Web.UI.UserControl
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaCantieri();
                //CaricaImprese(null);
            }
        }

        protected void ButtonVisualizzaTimb_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                TimbraturaFilter filtro = new TimbraturaFilter
                {
                    IdAccessoCantieriWhiteList = null
                };

                try
                {
                    filtro.DataInizio = DateTime.Parse(TextBoxDataInizio.Text);
                }
                catch
                {
                    filtro.DataInizio = null;
                }

                try
                {
                    filtro.DataFine = DateTime.Parse(TextBoxDataFine.Text).AddDays(1);
                }
                catch
                {
                    filtro.DataFine = null;
                }

                filtro.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);

                if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text.Trim()))
                    filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

                if (DropDownListCantiere.SelectedIndex != 0)
                    filtro.IdAccessoCantieriWhiteList = Int32.Parse(DropDownListCantiere.SelectedItem.Value);

                CaricaGridTimbratureFilter(filtro);
            }
        }

        private void CaricaGridTimbratureFilter(TimbraturaFilter filtro)
        {
            TimbraturaCollection timbratureIdDomanda = _biz.GetTimbratureByFilter(filtro);

            GridViewTimbrature.DataSource = timbratureIdDomanda;

            GridViewTimbrature.DataBind();

            ButtonStampaSelezione.Visible = (timbratureIdDomanda.Count > 0);
        }

        private void CaricaCantieri()
        {
            UtenteImpresa impresa =
                (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            Int32? idImpresa = impresa.IdImpresa;

            CantiereCollection cantiereCollection = _biz.GetCantieriByIdImpresa(idImpresa.Value);

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCantiere,
                cantiereCollection,
                "indirizzo",
                "idCantiere");
        }

        private void CaricaImprese(Int32? idCantiere)
        {
            UtenteImpresa impresa =
                (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            Int32? idImpresa = impresa.IdImpresa;

            ImpresaCollection imprese = new ImpresaCollection();

            if (idCantiere.HasValue)
            {
                Impresa impresaAc = new Impresa
                {
                    RagioneSociale = impresa.RagioneSociale,
                    IdImpresa = impresa.IdImpresa
                };
                imprese.Add(impresaAc);
            }

            ImpresaCollection impTot = new ImpresaCollection();
            ImpresaCollection retImp = new ImpresaCollection();

            impTot.AddRange(imprese);
            retImp.AddRange(imprese);


            CantiereCollection cantiereCollection = null;

            if (idCantiere.HasValue)
            {
                cantiereCollection = new CantiereCollection();
                cantiereCollection.Add(new Cantiere()
                {
                    IdCantiere = idCantiere
                });
            }
            else
            {
                //cantiereCollection = _biz.GetCantieriByIdImpresa(idImpresa.Value);
                cantiereCollection = new CantiereCollection();
            }

            foreach (Cantiere can in cantiereCollection)
            {
                do
                {
                    if (can.IdCantiere != null) impTot = GetImprese(impTot, can.IdCantiere.Value);

                    foreach (Impresa imp in impTot)
                    {
                        if (!retImp.Contains(imp))
                            retImp.Add(imp);
                    }
                } while (impTot.Count != 0);
            }

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListImprese,
                retImp,
                "ragioneSociale",
                "idImpresa");
        }

        private ImpresaCollection GetImprese(ImpresaCollection imprese, Int32 idCantiere)
        {
            ImpresaCollection retImprese = new ImpresaCollection();

            foreach (Impresa imp in imprese)
            {
                if (imp.IdImpresa != null)
                {
                    ImpresaCollection impColl = _biz.GetSubappaltateByIdImpresa(imp.IdImpresa.Value, idCantiere);

                    foreach (Impresa imp2 in impColl)
                    {
                        retImprese.Add(imp2);
                    }
                }
            }

            return retImprese;
        }


        protected void GridViewTimbrature_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Timbratura timbratura = (Timbratura)e.Row.DataItem;

                Label lCodiceRilevatore = (Label)e.Row.FindControl("LabelCodiceRilevatore");
                Label lFornitore = (Label)e.Row.FindControl("LabelFornitore");

                Label lLatitudine = (Label)e.Row.FindControl("LabelLatitudine");
                Label lLongitudine = (Label)e.Row.FindControl("LabelLongitudine");

                Label lNomeCognome = (Label)e.Row.FindControl("LabelNomeCognome");
                Label lCodiceFiscale = (Label)e.Row.FindControl("LabelCodiceFiscale");

                Label lRuolo = (Label)e.Row.FindControl("LabelRuolo");
                Label lCodiceCe = (Label)e.Row.FindControl("LabelCodiceCE");

                Label lIngressoUscita = (Label)e.Row.FindControl("LabelIngressoUscita");

                Label lAnomalia = (Label)e.Row.FindControl("LabelAnomalia");

                //Label lIndirizzoCantiere = (Label) e.Row.FindControl("LabelIndirizzoCantiere");
                //Label lComuneCantiere = (Label) e.Row.FindControl("LabelComuneCantiere");
                //Label lProvinciaCantiere = (Label) e.Row.FindControl("LabelProvinciaCantiere");

                lCodiceRilevatore.Text = timbratura.CodiceRilevatore;
                lFornitore.Text = timbratura.RagioneSociale;
                lLatitudine.Text = timbratura.Latitudine.ToString();
                lLongitudine.Text = timbratura.Longitudine.ToString();

                //if (timbratura.IdCantiere != null)
                //{
                //    WhiteList cantiere = _biz.GetDomandaByKey(timbratura.IdCantiere.Value);

                //    lIndirizzoCantiere.Text = cantiere.Indirizzo;
                //    lComuneCantiere.Text = cantiere.Comune;
                //    lProvinciaCantiere.Text = cantiere.Provincia;
                //}

                if (timbratura.IngressoUscita)
                    lIngressoUscita.Text = "Ingresso";
                else
                    lIngressoUscita.Text = "Uscita";

                if (timbratura.IdCantiere != null)
                {
                    WhiteList whiteListIdDomanda = _biz.GetDomandaByKey(timbratura.IdCantiere.Value);

                    WhiteListImpresaCollection wliColl =
                        _biz.GetLavoratoriInDomanda(timbratura.IdCantiere.Value);

                    whiteListIdDomanda.Lavoratori = wliColl;

                    ImpresaCollection imprese =
                        _biz.GetImpreseAutonomeSelezionateInSubappalto(timbratura.IdCantiere.Value);
                    whiteListIdDomanda.ListaImprese = imprese;

                    string codFisc = timbratura.CodiceFiscale;

                    lNomeCognome.Text = "Non disponibile";
                    lCodiceFiscale.Text = codFisc;
                    lRuolo.Text = "Non disponibile";
                    lCodiceCe.Text = "Non disponibile";

                    int codiceCe = _biz.GetCodiceCe(codFisc, null, timbratura.IdCantiere);
                    if (codiceCe != -1)
                        lCodiceCe.Text = codiceCe.ToString();
                    else
                        lCodiceCe.Text = "Non iscritto";

                    Lavoratore lav = null;

                    if (whiteListIdDomanda.Lavoratori != null)
                        lav = whiteListIdDomanda.Lavoratori.GetLavoratoreCodFisc(codFisc);

                    if (lav != null)
                    {
                        lNomeCognome.Text = string.Format("{0} {1}", lav.Nome, lav.Cognome);
                        lCodiceFiscale.Text = lav.CodiceFiscale;
                        lRuolo.Text = "Lavoratore";
                    }

                    if (codiceCe != -1)
                    {
                        Nominativo nominativo = _biz.GetNominativo(codiceCe);
                        lNomeCognome.Text = string.Format("{0} {1}", nominativo.Nome, nominativo.Cognome);
                    }

                    switch (timbratura.Anomalia)
                    {
                        case (TipologiaAnomalia)1:
                            lAnomalia.Text = "Non in lista";
                            lNomeCognome.Text = "Non disponibile";
                            lRuolo.Text = "Non disponibile";
                            lCodiceCe.Text = "Non disponibile";
                            break;
                        case (TipologiaAnomalia)2:
                            lAnomalia.Text = "Periodo non valido";
                            lNomeCognome.Text = "Non disponibile";
                            lRuolo.Text = "Non disponibile";
                            lCodiceCe.Text = "Non disponibile";
                            break;
                        default:
                            lAnomalia.Text = "";
                            break;
                    }

                    if (lCodiceCe.Text == "Non disponibile")
                    {
                        if (!string.IsNullOrEmpty(codFisc))
                        {
                            if (codFisc.Length == 16)
                            {
                                if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(codFisc))
                                {
                                    lNomeCognome.ForeColor = Color.Red;
                                    lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text,
                                                                      "Cod. fisc. non valido");
                                }
                                else
                                {
                                    lNomeCognome.ForeColor = Color.Black;
                                }
                            }
                            else
                            {
                                lNomeCognome.ForeColor = Color.Red;
                                lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                            }
                        }
                        else
                        {
                            lNomeCognome.ForeColor = Color.Red;
                            lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                        }
                    }
                }
            }
        }

        //protected void ButtonStampa_Click(object sender, EventArgs e)
        //{

        //}

        //protected void GridViewTimbrature_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //}

        //protected void ButtonStampaTutto_Click(object sender, EventArgs e)
        //{

        //}

        protected void ButtonStampaSelezione_Click(object sender, EventArgs e)
        {
            DateTime? dataInizio;
            DateTime? dataFine;

            try
            {
                dataInizio = DateTime.Parse(TextBoxDataInizio.Text);
            }
            catch
            {
                dataInizio = null;
            }

            try
            {
                dataFine = DateTime.Parse(TextBoxDataFine.Text).AddDays(1);
            }
            catch
            {
                dataFine = null;
            }

            Int32? idImpresa = null;
            if (DropDownListImprese.SelectedItem.ToString() != string.Empty)
                idImpresa = Int32.Parse(DropDownListImprese.SelectedValue);

            string codiceFiscale = TextBoxCodiceFiscale.Text;

            Context.Items["dataInizio"] = dataInizio;
            if (dataFine.HasValue)
                Context.Items["dataFine"] = dataFine.Value.AddDays(-1);
            else
                Context.Items["dataFine"] = null;
            Context.Items["idImpresa"] = idImpresa;
            Context.Items["ragioneSocialeImpresa"] = DropDownListImprese.SelectedItem.ToString();
            Context.Items["codiceFiscale"] = codiceFiscale;

            Server.Transfer("~/CeServizi/AccessoCantieri/ReportAccessoCantieriImprese.aspx");
        }

        protected void GridViewTimbrature_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TimbraturaFilter filtro = new TimbraturaFilter { IdAccessoCantieriWhiteList = null };

            try
            {
                filtro.DataInizio = DateTime.Parse(TextBoxDataInizio.Text);
            }
            catch
            {
                filtro.DataInizio = null;
            }

            try
            {
                filtro.DataFine = DateTime.Parse(TextBoxDataFine.Text).AddDays(1);
            }
            catch
            {
                filtro.DataFine = null;
            }

            Int32? idImpresa = null;
            if (DropDownListImprese.SelectedItem.ToString() != string.Empty)
                idImpresa = Int32.Parse(DropDownListImprese.SelectedValue);

            Int32? ruolo = 0;

            filtro.TipoUtente = (TipologiaRuoloTimbratura?)ruolo;

            filtro.IdImpresa = idImpresa;

            filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

            if (DropDownListCantiere.SelectedIndex != 0)
                filtro.IdAccessoCantieriWhiteList = Int32.Parse(DropDownListCantiere.SelectedItem.Value);

            CaricaGridTimbratureFilter(filtro);

            GridViewTimbrature.PageIndex = e.NewPageIndex;
            GridViewTimbrature.DataBind();
        }

        protected void DropDownListCantiere_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32? idCantiere = null;

            if (!String.IsNullOrEmpty(DropDownListCantiere.SelectedValue))
            {
                idCantiere = Int32.Parse(DropDownListCantiere.SelectedValue);
            }

            CaricaImprese(idCantiere);
        }
    }
}