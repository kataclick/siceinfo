﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriRicercaImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriRicercaImpresa" %>
<asp:Panel ID="PanelRicercaImprese" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledTable">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Imprese"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <asp:Panel ID="Panel1" runat="server">
                    <table class="standardTable">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ragione sociale
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxRagioneSociale"
                                    ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                Indirizzo
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxIndirizzo"
                                    ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                Comune
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxComune"
                                    ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="165px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="100" Width="165px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="255" Width="165px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cod.
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxCodice"
                                    ErrorMessage="numerico" ValidationExpression="^\d{1,}$" ValidationGroup="ricercaImprese"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                Partita Iva / Codice fiscale
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="255" Width="165px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxIvaFiscale" runat="server" MaxLength="16" Width="165px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td align="right">
                                <asp:Button ID="ButtonChiudi" runat="server" CausesValidation="False" OnClick="ButtonChiudi_Click"
                                    Text="X" Visible="False" Width="21px" />
                                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                    Text="Ricerca" ValidationGroup="ricercaImprese" Width="62px" />
                                <asp:Button ID="ButtonNuovo" runat="server" CausesValidation="False" OnClick="ButtonNuovo_Click"
                                    Text="Nuovo" Width="62px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel2" runat="server" Width="100%">
                    <asp:GridView ID="GridViewImprese" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyNames="IdImpresa,TipoImpresa" OnPageIndexChanging="GridViewImprese_PageIndexChanging"
                        OnRowDataBound="GridViewImprese_RowDataBound" OnSelectedIndexChanging="GridViewImprese_SelectedIndexChanging"
                        OnSorting="GridViewImprese_Sorting" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="IdImpresa" HeaderText="Cod.">
                                <ItemStyle Width="30px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="campoPiccolo">
                                                &nbsp;Cod fisc:
                                                <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                                                <br />
                                                &nbsp;P. Iva:
                                                <asp:Label ID="LabelPartitaIva" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                                ShowSelectButton="True">
                                <ItemStyle Width="10px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna impresa trovata
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
