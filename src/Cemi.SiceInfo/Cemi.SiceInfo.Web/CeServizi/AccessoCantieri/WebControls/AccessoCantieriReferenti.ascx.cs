﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriReferenti : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ViewState["referenti"] == null)
                {
                    ViewState["referenti"] = new ReferenteCollection();
                }

                CaricaGridReferenti();
            }
        }

        private void CaricaGridReferenti()
        {
            if (ViewState["referenti"] != null)
            {
                GridViewReferenti.DataSource = ViewState["referenti"];
                GridViewReferenti.DataBind();
            }
        }

        public void CaricaReferenti(ReferenteCollection referenti)
        {
            ViewState["referenti"] = referenti;
        }

        public ReferenteCollection GetReferenti()
        {
            return (ReferenteCollection)ViewState["referenti"];
        }

        protected void GridViewReferenti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Referente alt = (Referente)e.Row.DataItem;
                Label lReferente = (Label)e.Row.FindControl("LabelReferente");
                Label lRuolo = (Label)e.Row.FindControl("LabelRuolo");
                Label lContatto = (Label)e.Row.FindControl("LabelContatto");

                if (alt != null)
                {
                    lReferente.Text = string.Format("{0} {1}", alt.Cognome, alt.Nome);

                    if (alt.ModalitaContatto)
                        lContatto.Text = alt.Telefono;
                    else
                        lContatto.Text = alt.Email;

                    switch (alt.TipoRuolo)
                    {
                        case TipologiaRuoloReferente.Cantiere:
                            lRuolo.Text = "Referente cantiere";
                            break;
                        case TipologiaRuoloReferente.Committente:
                            lRuolo.Text = "Referente committente";
                            break;
                        case TipologiaRuoloReferente.Impresa:
                            lRuolo.Text = "Referente impresa";
                            break;
                        default:
                            lRuolo.Text = "Altro";
                            break;
                    }
                }
            }
        }

        protected void GridViewReferenti_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ReferenteCollection referenti = (ReferenteCollection)ViewState["referenti"];

            referenti.RemoveAt(e.RowIndex);

            ViewState["referenti"] = referenti;

            CaricaGridReferenti();
        }

        protected void ButtonaggiungiReferente_Click(object sender, EventArgs e)
        {
            LabelGiaPresente.Visible = false;

            Referente referente = AccessoCantieriReferentiInserimento1.Referente;

            if (referente != null)
            {
                ReferenteCollection referenti = (ReferenteCollection)ViewState["referenti"];

                referenti.Add(referente);

                ViewState["referenti"] = referenti;

                CaricaGridReferenti();

                AccessoCantieriReferentiInserimento1.Reset();

                LabelInserimentoReferente.Text = string.Empty;
            }

            else
                LabelInserimentoReferente.Text = "Non tutti i campi sono corretti";
        }
    }
}