﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RicercaLavoratore.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.RicercaLavoratore" %>

<asp:Panel ID="PanelRicercaCantieri" runat="server" DefaultButton="ButtonVisualizza" Width="100%">
<table class="standardTable">
    <tr>
        <td>
            Cognome
        </td>
        <td>
            Nome
        </td>
        <td>
            Codice fiscale
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="150px"></asp:TextBox>
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorIdCantiere"
                runat="server"
                ValidationGroup="ricercaLavoratori"
                ErrorMessage="Selezionare un cantiere"
                ForeColor="Red" 
                onservervalidate="CustomValidatorIdCantiere_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="ricercaLavoratori" CssClass="messaggiErrore" runat="server" />
        </td>
        <td align="right" colspan="3">
            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaLavoratori"
                OnClick="ButtonVisualizza_Click" />
        </td>
    </tr>
</table>
<br />
<telerik:RadGrid ID="RadGridLavoratori" runat="server" AllowPaging="True" PageSize="5"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Visible="False" 
        onitemdatabound="RadGridLavoratori_ItemDataBound" 
        onpageindexchanged="RadGridLavoratori_PageIndexChanged">
    <ClientSettings>
        <Selecting CellSelectionMode="None" />
    </ClientSettings>
    <MasterTableView>
        <CommandItemSettings ExportToPdfText="Export to PDF" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridTemplateColumn UniqueName="Lavoratore">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td colspan="2">
                                <b>
                                <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100px">
                                Data nascita:
                            </td>
                            <td>
                                <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Codice fiscale:
                            </td>
                            <td>
                                <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="350px" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="Impresa">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td colspan="2">
                                <b>
                                <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100px">
                                Partita Iva:
                            </td>
                            <td>
                                <asp:Label ID="LabelPartitaIva" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Codice fiscale:
                            </td>
                            <td>
                                <asp:Label ID="LabelImpCodiceFiscale" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Dal:
                            </td>
                            <td>
                                <asp:Label ID="LabelDataInizioAttivita" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Al:
                            </td>
                            <td>
                                <asp:Label ID="LabelDataFineAttivita" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
</asp:Panel>