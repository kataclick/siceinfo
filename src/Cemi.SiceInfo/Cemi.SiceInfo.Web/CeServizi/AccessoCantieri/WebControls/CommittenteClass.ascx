﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommittenteClass.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.CommittenteClass" %>

<style type="text/css">
    .style1
    {
        width: 120px;
    }
</style>

<table class="standardTable">
    <tr>
        <td>
            <b>
                Committente
            associato</b></td>
    </tr>
    <tr>
        <td>
            <asp:MultiView
                ID="MultiViewCommittente"           
                runat="server"
                ActiveViewIndex="0">
                <asp:View
                    ID="ViewCommittenteNonSelezionato"
                    runat="server">
                    Nessun committente associato
                </asp:View>
                <asp:View
                    ID="ViewCommittenteSelezionato"
                    runat="server">
                    <table class="standardTable">
                        <tr>
                            <td class="style1">
                                Cognome:
                            </td>
                            <td>
                                <asp:Label
                                    ID="LabelCognome"
                                    runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nome:
                            </td>
                            <td>
                                <asp:Label
                                    ID="LabelNome"
                                    runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Ragione Sociale:
                            </td>
                            <td>
                                <b>
                                    <asp:Label
                                        ID="LabelRagioneSociale"
                                        runat="server">
                                    </asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Partita IVA:
                            </td>
                            <td>
                                <asp:Label
                                    ID="LabelPartitaIva"
                                    runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Codice Fiscale:
                            </td>
                            <td>
                                <asp:Label
                                    ID="LabelCodiceFiscale"
                                    runat="server">
                                </asp:Label>
                            </td>
                        </tr>
                    </table>    
                </asp:View>
            </asp:MultiView>
        </td>
    </tr>
</table>