﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Delegates;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;
using Committente = TBridge.Cemi.Type.Entities.GestioneUtenti.Committente;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class RicercaCantieri : System.Web.UI.UserControl
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
        public event WhiteListSelectedEventHandler OnCantiereSelected;
        private Boolean isCommittente;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.isCommittente = GestioneUtentiBiz.IsCommittente();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaCantieri(0);
                RadGridCantieri.Visible = true;
            }
        }

        private void CaricaCantieri(Int32 pagina)
        {
            WhiteListFilter filtro = CreaFiltro();

            WhiteListCollection domande = _biz.GetDomandeByFilter(filtro);

            Presenter.CaricaElementiInGridView(
                RadGridCantieri,
                domande);
        }

        private WhiteListFilter CreaFiltro()
        {
            WhiteListFilter filtro = new WhiteListFilter
            {
                Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text),
                Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text)
            };

            if (!GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione))
            {
                if (GestioneUtentiBiz.IsCommittente())
                {
                    filtro.IdCommittente = ((Committente)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdCommittente;
                }
                else
                {
                    filtro.IdUtente = GestioneUtentiBiz.GetIdUtente();
                }
            }

            return filtro;
        }

        protected void RadGridCantieri_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                WhiteList domanda = (WhiteList)e.Item.DataItem;

                Label lIndirizzo = (Label)e.Item.FindControl("LabelIndirizzo");
                Label lComune = (Label)e.Item.FindControl("LabelComune");
                Label lProvincia = (Label)e.Item.FindControl("LabelProvincia");

                lIndirizzo.Text = String.Format("{0} {1}", domanda.Indirizzo, domanda.Civico);
                lComune.Text = domanda.Comune;
                lProvincia.Text = domanda.Provincia;
            }
        }

        protected void RadGridCantieri_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaCantieri(0);
        }

        protected void RadGridCantieri_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SELEZIONA":
                    Int32 idDomanda = (Int32)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["idWhiteList"];

                    if (OnCantiereSelected != null)
                    {
                        OnCantiereSelected(idDomanda);
                    }
                    break;
            }
        }
    }
}