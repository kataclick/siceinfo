﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriAssegnazioneRilevatori.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriAssegnazioneRilevatori" %>
<table class="standardTable">
<asp:panel ID="panelHidden" runat="server" Visible="false">
    <tr>
        <td>
            <br />
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco rilevatori"></asp:Label>
            <br />
            <br />
            <br />
            <asp:GridView ID="GridViewRilevatori" runat="server" AutoGenerateColumns="False"
                Width="100%" DataKeyNames="IdRilevatore,CodiceRilevatore,RagioneSociale,DataInizio,DataFine"
                OnRowCommand="GridViewRilevatori_RowCommand" OnRowDataBound="GridViewRilevatori_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Rilevatore">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSocieta" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelCantiere" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelReferente" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Assegna"
                        ShowCancelButton="False" ShowSelectButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    Nessun rilevatore presente
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <br />
        </td>
    </tr>
</asp:panel>
    <tr>
        <td>
            <b>Elenco rilevatori associati al cantiere</b><br />
            <br />
            <asp:GridView ID="GridViewRilevatoriCantiere" runat="server" AutoGenerateColumns="False"
                Width="100%" DataKeyNames="IdRilevatore,CodiceRilevatore,RagioneSociale,DataInizio,DataFine"
                OnRowCommand="GridViewRilevatoriCantiere_RowCommand" 
                OnRowDataBound="GridViewRilevatoriCantiere_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText="Rilevatore">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelCodice0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelSocieta0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stato">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelCantiere0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelReferente0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelPeriodo0" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Termina"
                        ShowCancelButton="False" ShowSelectButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    Nessun rilevatore presente
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
