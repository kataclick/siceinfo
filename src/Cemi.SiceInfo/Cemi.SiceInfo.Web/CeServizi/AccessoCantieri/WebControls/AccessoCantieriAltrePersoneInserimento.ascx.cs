﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriAltrePersoneInserimento : System.Web.UI.UserControl
    {
        public AltraPersona AltraPersona
        {
            get
            {
                Page.Validate("altraPersona");
                AltraPersona altraPersona = null;

                if (Page.IsValid)
                {
                    altraPersona = CreaAltraPersona();
                }

                return altraPersona;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaRuoli();
            }
        }

        private void CaricaRuoli()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownList1,
                Enum.GetNames(typeof(TipologiaRuolo)),
                "",
                "");
        }

        private AltraPersona CreaAltraPersona()
        {
            if (RadDateDataNascita.SelectedDate != null)
            {
                AltraPersona altraPersona = new AltraPersona
                {
                    Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text),
                    Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text),
                    DataNascita = RadDateDataNascita.SelectedDate.Value,
                    CodiceFiscale =
                                                        Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text),
                    Telefono = Presenter.NormalizzaCampoTesto(TextBoxTelefono.Text),
                    Email = Presenter.NormalizzaCampoTesto(TextBoxEmail.Text)
                };

                if (DropDownList1.SelectedIndex != 0)
                    altraPersona.TipoRuolo =
                        (TipologiaRuolo)Enum.Parse(typeof(TipologiaRuolo), DropDownList1.SelectedValue);

                return altraPersona;
            }
            return null;
        }

        public void Reset()
        {
            Presenter.SvuotaCampo(TextBoxCognome);
            Presenter.SvuotaCampo(TextBoxNome);
            Presenter.SvuotaCampo(TextBoxCodiceFiscale);
            Presenter.SvuotaCampo(TextBoxTelefono);
            Presenter.SvuotaCampo(TextBoxEmail);
            RadDateDataNascita.Clear();
            CaricaRuoli();
        }


        protected void CustomValidatorCodiceFiscale_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if ((RadDateDataNascita.SelectedDate.HasValue) && (TextBoxCodiceFiscale.Text.Length == 16))
            {
                DateTime dataNascita = RadDateDataNascita.SelectedDate.Value;
                if (!string.IsNullOrEmpty(TextBoxNome.Text) && !string.IsNullOrEmpty(TextBoxCognome.Text))
                {
                    string maschio = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                               TextBoxCognome.Text, "M",
                                                                                               dataNascita);
                    string femmina = CodiceFiscaleManager.CalcolaPrimi11CaratteriCodiceFiscale(TextBoxNome.Text,
                                                                                               TextBoxCognome.Text, "F",
                                                                                               dataNascita);

                    if (maschio.Substring(0, 6) != TextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper() &&
                        femmina.Substring(0, 6) != TextBoxCodiceFiscale.Text.Substring(0, 6).ToUpper())
                    {
                        args.IsValid = false;
                    }
                }


                String codiceMaschile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "M", dataNascita, "A000");
                String codiceFemminile = CodiceFiscaleManager.CalcolaCodiceFiscale("QWR", "QWR", "F", dataNascita, "A000");

                if (codiceMaschile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                    &&
                    codiceFemminile.Substring(6, 5).ToUpper() != TextBoxCodiceFiscale.Text.Substring(6, 5).ToUpper()
                    )
                {
                    args.IsValid = false;
                }
            }
        }
    }
}