﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using Telerik.Web.UI;

using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class PresenzeWC : System.Web.UI.UserControl
    {
        private readonly AccessoCantieriBusiness biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere);

            #endregion

            if (!Page.IsPostBack)
            {
                RadDateInputDal.SelectedDate = DateTime.Now.Date;
                RadDateInputAl.SelectedDate = DateTime.Now.Date;
            }
        }

        public void SetIdCantiere(Int32 idCantiere)
        {
            ViewState["IdCantiere"] = idCantiere;
            PanelGriglia.Visible = false;
        }

        protected void RadGridCantieri_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            CaricaPresenze();
        }

        protected void RadGridCantieri_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Presenze presenze = (Presenze)e.Item.DataItem;

                Label lCognomeNome = (Label)e.Item.FindControl("LabelCognomeNome");
                Label lDataNascita = (Label)e.Item.FindControl("LabelDataNascita");
                Label lCodiceFiscale = (Label)e.Item.FindControl("LabelCodiceFiscale");
                Label lRagioneSociale = (Label)e.Item.FindControl("LabelRagioneSociale");
                Label lPartitaIva = (Label)e.Item.FindControl("LabelPartitaIva");

                lCognomeNome.Text = !String.IsNullOrWhiteSpace(String.Format("{0} {1}", presenze.Cognome, presenze.Nome).Trim()) ? String.Format("{0} {1}", presenze.Cognome, presenze.Nome).Trim() : "--";
                if (presenze.DataNascita.HasValue)
                {
                    lDataNascita.Text = presenze.DataNascita.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    lDataNascita.Text = "--";
                }
                lCodiceFiscale.Text = presenze.CodiceFiscale;
                lRagioneSociale.Text = !String.IsNullOrWhiteSpace(presenze.ImpresaRagioneSociale) ? presenze.ImpresaRagioneSociale : "--";
                lPartitaIva.Text = !String.IsNullOrWhiteSpace(presenze.ImpresaPartitaIva) ? presenze.ImpresaPartitaIva : "--";
            }
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                PresenzeFilter filtro = CreaFiltro();
                ViewState["Filtro"] = filtro;

                List<Impresa> imprese = biz.GetPresenzeImprese(filtro);
                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    RadComboBoxImpresa,
                    imprese,
                    "RagioneSociale",
                    "PartitaIva");

                PanelGriglia.Visible = true;
                CaricaPresenze();
            }
        }

        private void CaricaPresenze()
        {
            PresenzeFilter filtro = (PresenzeFilter)ViewState["Filtro"];
            List<Presenze> presenze = biz.GetPresenze(filtro);

            Presenter.CaricaElementiInGridView(
                RadGridCantieri,
                presenze);
        }

        private PresenzeFilter CreaFiltro()
        {
            PresenzeFilter filtro = new PresenzeFilter();

            filtro.IdCantiere = (Int32)ViewState["IdCantiere"];
            filtro.Dal = RadDateInputDal.SelectedDate.Value;
            filtro.Al = RadDateInputAl.SelectedDate.Value;
            filtro.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);

            //if (consideraImpresa && !String.IsNullOrWhiteSpace(RadComboBoxImpresa.SelectedValue))
            //{
            //    filtro.PartitaIvaImpresa = RadComboBoxImpresa.SelectedValue;
            //}

            return filtro;
        }

        protected void RadComboBoxImpresa_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            PresenzeFilter filtro = (PresenzeFilter)ViewState["Filtro"];
            filtro.PartitaIvaImpresa = e.Value;
            ViewState["Filtro"] = filtro;

            CaricaPresenze();
        }

        protected void ButtonEstrai_Click(object sender, EventArgs e)
        {
            PresenzeFilter filtro = (PresenzeFilter)ViewState["Filtro"];
            Context.Items["Filtro"] = filtro;

            Server.Transfer("~/CeServizi/AccessoCantieri/ReportPresenze.aspx");
        }

        protected void CustomValidatorDal_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!RadDateInputDal.SelectedDate.HasValue)
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorAl_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!RadDateInputAl.SelectedDate.HasValue)
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorRangeValido_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadDateInputDal.SelectedDate.HasValue
                && RadDateInputAl.SelectedDate.HasValue
                && RadDateInputDal.SelectedDate.Value > RadDateInputAl.SelectedDate.Value)
            {
                args.IsValid = false;
            }
        }
    }
}