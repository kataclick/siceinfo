﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Business;
//using TBridge.Cemi.Presenter;
using Cemi.SiceInfo.Web.Helpers;
using Telerik.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using UtenteImpresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriControlliCEMIImpresa : System.Web.UI.UserControl
    {
        private readonly AccessoCantieriBusiness _accessoCantieriBiz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaAnomalie();
                CaricaRuoli();

                CaricaCantieri();
                //CaricaImprese(null);
            }
        }

        private void CaricaCantieri()
        {
            UtenteImpresa impresa =
                (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            Int32? idImpresa = impresa.IdImpresa;

            CantiereCollection cantiereCollection = _accessoCantieriBiz.GetCantieriByIdImpresa(idImpresa.Value);

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCantiere,
                cantiereCollection,
                "indirizzo",
                "idCantiere");
        }

        private void CaricaImprese(Int32? idCantiere)
        {
            UtenteImpresa impresa =
                (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            Int32? idImpresa = impresa.IdImpresa;

            ImpresaCollection imprese = new ImpresaCollection();

            if (idCantiere.HasValue)
            {
                Impresa impresaAc = new Impresa
                {
                    RagioneSociale = impresa.RagioneSociale,
                    IdImpresa = impresa.IdImpresa
                };
                imprese.Add(impresaAc);
            }

            ImpresaCollection impTot = new ImpresaCollection();
            ImpresaCollection retImp = new ImpresaCollection();

            impTot.AddRange(imprese);
            retImp.AddRange(imprese);


            CantiereCollection cantiereCollection = null;

            if (idCantiere.HasValue)
            {
                cantiereCollection = new CantiereCollection();
                cantiereCollection.Add(new Cantiere()
                {
                    IdCantiere = idCantiere
                });
            }
            else
            {
                //cantiereCollection = _biz.GetCantieriByIdImpresa(idImpresa.Value);
                cantiereCollection = new CantiereCollection();
            }

            foreach (Cantiere can in cantiereCollection)
            {
                do
                {
                    if (can.IdCantiere != null)
                        impTot = GetImprese(impTot, can.IdCantiere.Value);

                    foreach (Impresa imp in impTot)
                    {
                        if (!retImp.Contains(imp))
                            retImp.Add(imp);
                    }
                } while (impTot.Count != 0);
            }

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListImpresa,
                retImp,
                "ragioneSociale",
                "idImpresa");
        }

        private ImpresaCollection GetImprese(ImpresaCollection imprese, Int32 idCantiere)
        {
            ImpresaCollection retImprese = new ImpresaCollection();

            foreach (Impresa imp in imprese)
            {
                if (imp.IdImpresa != null)
                {
                    ImpresaCollection impColl = _accessoCantieriBiz.GetSubappaltateByIdImpresa(imp.IdImpresa.Value, idCantiere);

                    foreach (Impresa imp2 in impColl)
                    {
                        retImprese.Add(imp2);
                    }
                }
            }

            return retImprese;
        }

        private void CaricaGridTimbratureFilter(Int32 pagina)
        {
            RadGridTimbrature.Visible = true;

            ControlloIdentitaFilter filtro = CreaFiltro();
            ControlloIdentitaCollection controlloIdentitaCollectionApp = _accessoCantieriBiz.GetControlloIdentita(filtro);
            if (filtro.IdCantiere != null)
            {
                WhiteListImpresaCollection wlImp = _accessoCantieriBiz.GetLavoratoriInDomanda(filtro.IdCantiere.Value);

                ControlloIdentitaCollection controlloIdentitaCollection = new ControlloIdentitaCollection();
                foreach (ControlloIdentita ctI in controlloIdentitaCollectionApp)
                {
                    if (wlImp.EffettuaControlli(ctI.CodiceFiscale, ctI.PartitaIvaImpresa))
                    {
                        controlloIdentitaCollection.Add(ctI);
                    }
                }

                foreach (ControlloIdentita ctrlId in controlloIdentitaCollection)
                {
                    if (ctrlId.IdCantiere.HasValue)
                    {
                        //LavoratoreCollection lavColl = biz.GetLavoratoreControlli(ctrlId.IdCantiere.Value, ctrlId.CodiceFiscale,
                        //                                                          null);

                        if (!string.IsNullOrEmpty(ctrlId.PartitaIvaImpresa))
                            ctrlId.Impresa = _accessoCantieriBiz.GetImpresaControlli(ctrlId.IdCantiere.Value,
                                                                                     ctrlId.CodiceFiscale,
                                                                                     ctrlId.PartitaIvaImpresa);

                        LavoratoreCollection lavColl;
                        if (ctrlId.Impresa != null)
                        {
                            if (ctrlId.Impresa.IdImpresa.HasValue)
                            {
                                lavColl = _accessoCantieriBiz.GetLavoratoreControlli(ctrlId.IdCantiere.Value,
                                                                                     ctrlId.CodiceFiscale,
                                                                                     ctrlId.Impresa.IdImpresa.Value);
                            }
                            else
                            {
                                lavColl = _accessoCantieriBiz.GetLavoratoreControlli(ctrlId.IdCantiere.Value,
                                                                                     ctrlId.CodiceFiscale,
                                                                                     null);
                            }
                        }
                        else
                        {
                            lavColl = _accessoCantieriBiz.GetLavoratoreControlli(ctrlId.IdCantiere.Value,
                                                                                 ctrlId.CodiceFiscale,
                                                                                 null);
                        }

                        Lavoratore lavSel = null;
                        foreach (Lavoratore lav in lavColl)
                        {
                            if ((lav.Incongruenze == false) && (lav.IdLavoratore != null))
                                lavSel = lav;
                        }

                        if (lavSel == null)
                            foreach (Lavoratore lav in lavColl)
                            {
                                if (lav.Incongruenze && (lav.IdLavoratore != null))
                                    lavSel = lav;
                            }

                        if (lavSel == null)
                            foreach (Lavoratore lav in lavColl)
                            {
                                if (!string.IsNullOrEmpty(lav.Nome))
                                    lavSel = lav;
                            }

                        ctrlId.Lavoratore = lavSel;

                        //if (!string.IsNullOrEmpty(ctrlId.PartitaIvaImpresa))
                        //    ctrlId.Impresa = biz.GetImpresaControlli(ctrlId.IdCantiere.Value, ctrlId.CodiceFiscale,
                        //                                         ctrlId.PartitaIvaImpresa);
                    }
                }

                if (pagina == 0)
                {
                    RadGridTimbrature.CurrentPageIndex = 0;
                }

                Presenter.CaricaElementiInGridView(
                    RadGridTimbrature,
                    controlloIdentitaCollection);

                ButtonEsportaPDF.Visible = (controlloIdentitaCollection != null && controlloIdentitaCollection.Count > 0);
            }
        }

        private void CaricaAnomalie()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListAnomalie,
                Enum.GetNames(typeof(TipologiaAnomaliaControlli)),
                "",
                "");
        }

        private void CaricaRuoli()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListRuoli,
                Enum.GetNames(typeof(TipologiaRuoloTimbratura)),
                "",
                "");
        }

        protected void ButtonVisualizzaTimb_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaGridTimbratureFilter(0);
            }
        }

        //private void RicercaCantieri1_OnCantiereSelected(Int32 idWhiteList)
        //{
        //    WhiteList whiteListIdDomanda = _accessoCantieriBiz.GetDomandaByKey(idWhiteList);
        //    ViewState["IdDomanda"] = idWhiteList;

        //    String indirizzo = whiteListIdDomanda.Indirizzo;
        //    String comune = whiteListIdDomanda.Comune;
        //    String provincia = whiteListIdDomanda.Provincia;

        //    LabelCantiere.Visible = true;
        //    PanelFiltroTimb.Visible = true;
        //    LabelCantiere.Text = string.Format("Cantiere selezionato: {0} {1} {2}", indirizzo, comune, provincia);

        //    PanelAggiungiRilevatore.Visible = true;

        //    //CaricaGridTimbratureFilter(0);

        //    PanelRilevatori.Visible = true;

        //    ImpresaCollection imprese = new ImpresaCollection();
        //    foreach (Subappalto sub in whiteListIdDomanda.Subappalti)
        //    {
        //        if (sub.Appaltante != null)
        //            if (!imprese.Contains(sub.Appaltante))
        //                imprese.Add(sub.Appaltante);

        //        if (sub.Appaltata != null)
        //            if (!imprese.Contains(sub.Appaltata))
        //                imprese.Add(sub.Appaltata);
        //    }

        //    Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListImpresa, imprese,
        //                                                       "RagioneSociale", "idImpresa");
        //}

        private ControlloIdentitaFilter CreaFiltro()
        {
            ViewState["IdDomanda"] = Int32.Parse(DropDownListCantiere.SelectedValue);

            ControlloIdentitaFilter filtro = new ControlloIdentitaFilter
            {
                IdCantiere = Int32.Parse(DropDownListCantiere.SelectedValue)
            };

            try
            {
                filtro.Mese = Int32.Parse(TextBoxMeseAnno.Text.Split('/')[0]);
            }
            catch
            {
                filtro.Mese = null;
            }

            try
            {
                filtro.Anno = Int32.Parse(TextBoxMeseAnno.Text.Split('/')[1]);
            }
            catch
            {
                filtro.Anno = null;
            }

            if (!String.IsNullOrEmpty(DropDownListImpresa.SelectedValue))
            {
                Int32? idImpresa = Int32.Parse(DropDownListImpresa.SelectedValue);
                filtro.IdImpresa = idImpresa;
            }

            if (DropDownListAnomalie.SelectedItem != null && DropDownListAnomalie.SelectedItem.ToString() != string.Empty)
            {
                Int32? anomalia = (DropDownListAnomalie.SelectedIndex - 1);
                filtro.TipologiaAnomaliaControlli = new TipologiaAnomaliaControlli();
                filtro.TipologiaAnomaliaControlli = (TipologiaAnomaliaControlli?)anomalia;
            }

            if (DropDownListRuoli.SelectedItem != null && DropDownListRuoli.SelectedItem.ToString() != string.Empty)
            {
                Int32? ruolo = (DropDownListRuoli.SelectedIndex - 1);
                filtro.TipologiaRuoloTimbratura = new TipologiaRuoloTimbratura();
                filtro.TipologiaRuoloTimbratura = (TipologiaRuoloTimbratura?)ruolo;
            }

            filtro.CodiceFiscale = TextBoxCodiceFiscale.Text;

            return filtro;
        }

        protected void RadGridTimbrature_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                ControlloIdentita controlloIdentita = (ControlloIdentita)e.Item.DataItem;

                Label lMeseAnno = (Label)e.Item.FindControl("LabelMeseAnno");
                Label lNomeCognome = (Label)e.Item.FindControl("LabelNomeCognome");
                Label lDataNascita = (Label)e.Item.FindControl("LabelDataNascita");
                Label lCodiceCe = (Label)e.Item.FindControl("LabelCodiceCE");
                Label lDenuncia = (Label)e.Item.FindControl("LabelDenuncia");
                Label lLavoratoreDenuncia = (Label)e.Item.FindControl("LabelLavoratoreDenuncia");
                Label lOreLavorate = (Label)e.Item.FindControl("LabelOreLavorate");
                Label lRegolarita = (Label)e.Item.FindControl("LabelRegolarita");
                Label lCodiceImpresa = (Label)e.Item.FindControl("LabelCodiceImpresa");
                Label lRagioneSocialeImpresa = (Label)e.Item.FindControl("LabelRagioneSocialeImpresa");
                Label labelStatoImpresa = (Label)e.Item.FindControl("LabelStatoImpresa");
                Label lIvaCodFiscImpresa = (Label)e.Item.FindControl("LabelIvaCodFiscImpresa");
                Label lUltimaDenuncia = (Label)e.Item.FindControl("LabelUltimaDenuncia");
                Label lCe = (Label)e.Item.FindControl("LabelCE");
                Label lPosizioneContributiva = (Label)e.Item.FindControl("LabelPosizionecontributiva");
                Label lRuolo = (Label)e.Item.FindControl("LabelRuolo");

                string impresa = string.Empty;
                string lavoratore = string.Empty;

                #region ruolo

                WhiteList whiteListIdDomanda =
                    _accessoCantieriBiz.GetDomandaByKey(Int32.Parse(ViewState["IdDomanda"].ToString()));

                WhiteListImpresaCollection wliColl =
                    _accessoCantieriBiz.GetLavoratoriInDomanda(Int32.Parse(ViewState["IdDomanda"].ToString()));

                whiteListIdDomanda.Lavoratori = wliColl;

                AltraPersonaCollection altrePers =
                    _accessoCantieriBiz.GetAltrePersone(Int32.Parse(ViewState["IdDomanda"].ToString()));
                whiteListIdDomanda.ListaAltrePersone = altrePers;

                ReferenteCollection referenti =
                    _accessoCantieriBiz.GetReferenti(Int32.Parse(ViewState["IdDomanda"].ToString()));
                whiteListIdDomanda.ListaReferenti = referenti;

                ImpresaCollection imprese =
                    _accessoCantieriBiz.GetImpreseAutonomeSelezionateInSubappalto(
                        Int32.Parse(ViewState["IdDomanda"].ToString()));
                whiteListIdDomanda.ListaImprese = imprese;

                lRuolo.Text = "Non disponibile";

                Lavoratore lav = null;
                AltraPersona altr = null;
                Referente refe = null;
                Impresa impr = null;

                if (whiteListIdDomanda.Lavoratori != null)
                    lav = whiteListIdDomanda.Lavoratori.GetLavoratoreCodFisc(controlloIdentita.CodiceFiscale);
                if (whiteListIdDomanda.ListaAltrePersone != null)
                    altr = whiteListIdDomanda.ListaAltrePersone.GetAltraPersonaCodFisc(controlloIdentita.CodiceFiscale);
                if (whiteListIdDomanda.ListaReferenti != null)
                    refe = whiteListIdDomanda.ListaReferenti.GetReferenteCodFisc(controlloIdentita.CodiceFiscale);
                if (whiteListIdDomanda.ListaImprese != null)
                    impr = whiteListIdDomanda.ListaImprese.GetImpresaAutonomaCodFisc(controlloIdentita.CodiceFiscale);


                if (lav != null)
                {
                    lRuolo.Text = "Lavoratore";
                }
                else if (altr != null)
                {
                    if (altr.TipoRuolo == TipologiaRuolo.ResponsabileSicurezza)
                        lRuolo.Text = "Persona abilitata (resp. sicurezza)";
                    if (altr.TipoRuolo == TipologiaRuolo.ResponsabileLavoratori)
                        lRuolo.Text = "Persona abilitata (resp. lavoratori)";
                    if (altr.TipoRuolo == TipologiaRuolo.ResponsabileLegale)
                        lRuolo.Text = "Persona abilitata (resp. leg. impresa)";
                    if (altr.TipoRuolo == TipologiaRuolo.Titolare)
                        lRuolo.Text = "Persona abilitata (titolare impresa)";
                    if (altr.TipoRuolo == TipologiaRuolo.Socio)
                        lRuolo.Text = "Persona abilitata (socio impresa)";
                    if (altr.TipoRuolo == TipologiaRuolo.Altro)
                        lRuolo.Text = "Persona abilitata";
                }
                else if (refe != null)
                {
                    if (refe.TipoRuolo == TipologiaRuoloReferente.Cantiere)
                        lRuolo.Text = "Referente cantiere";
                    if (refe.TipoRuolo == TipologiaRuoloReferente.Committente)
                        lRuolo.Text = "Referente committente";
                    if (refe.TipoRuolo == TipologiaRuoloReferente.Impresa)
                        lRuolo.Text = "Referente impresa";
                    if (refe.TipoRuolo == TipologiaRuoloReferente.Altro)
                        lRuolo.Text = "Referente altro";
                }
                else if (impr != null)
                {
                    lRuolo.Text = "Ditta artigiana";
                }

                #endregion

                if (controlloIdentita.Lavoratore != null)
                {
                    if (controlloIdentita.Lavoratore.Incongruenze)
                    {
                        lNomeCognome.ToolTip =
                            "Presenza di incongruenze con i rapporti di lavoro dichiarati per il lavoratore e/o lavoratore al momento non presente in whitelist";
                        lNomeCognome.ForeColor = Color.Red;
                    }
                    else
                    {
                        lNomeCognome.ToolTip = "Lavoratore";
                        lNomeCognome.ForeColor = Color.Black;
                    }

                    lNomeCognome.Text = string.Format("{0} {1}", controlloIdentita.Lavoratore.Nome,
                                                      controlloIdentita.Lavoratore.Cognome);
                    lCodiceCe.Text = controlloIdentita.Lavoratore.IdLavoratore.ToString();
                    if (controlloIdentita.Lavoratore.DataNascita != null)
                        lDataNascita.Text = controlloIdentita.Lavoratore.DataNascita.Value.ToShortDateString();

                    lavoratore = string.Format("{0} {1}", controlloIdentita.Lavoratore.Nome,
                                               controlloIdentita.Lavoratore.Cognome);
                }


                if (controlloIdentita.Impresa != null)
                {
                    if (controlloIdentita.Impresa.IdImpresa != null)
                    {
                        lCodiceImpresa.ForeColor = Color.Black;
                        lCodiceImpresa.Text = controlloIdentita.Impresa.IdImpresa.ToString();
                    }
                    else
                    {
                        lNomeCognome.ForeColor = Color.Red;
                        lCodiceImpresa.ForeColor = Color.Red;
                        lCodiceImpresa.Text = "--";
                    }
                    lRagioneSocialeImpresa.Text = controlloIdentita.Impresa.RagioneSociale;

                    string stato = controlloIdentita.Impresa.Stato;
                    DateTime? dataSospensione = controlloIdentita.Impresa.DataSospensione;
                    DateTime? dataCessazione = controlloIdentita.Impresa.DataCessazione;
                    labelStatoImpresa.Text = stato;
                    if (stato == "SOSPESA" || stato == "SOSP.UFF.")
                    {
                        labelStatoImpresa.ForeColor = Color.Red;
                        if (dataSospensione != null)
                            labelStatoImpresa.Text = String.Format("{0} dal {1}", stato,
                                                                   dataSospensione.Value.ToShortDateString());
                    }
                    else if (stato == "CESSATA")
                    {
                        labelStatoImpresa.ForeColor = Color.Red;
                        if (dataCessazione != null)
                            labelStatoImpresa.Text = String.Format("{0} dal {1}", stato,
                                                                   dataCessazione.Value.ToShortDateString());
                    }
                    else if (stato == "ATTIVA")
                    {
                        labelStatoImpresa.ForeColor = Color.Black;
                    }
                    else
                    {
                        labelStatoImpresa.ForeColor = Color.Red;
                        labelStatoImpresa.Text = "--";
                    }
                    impresa = controlloIdentita.Impresa.RagioneSociale;

                    if (controlloIdentita.Impresa.PartitaIva != null)
                        lIvaCodFiscImpresa.Text = controlloIdentita.Impresa.PartitaIva;
                    else
                        lIvaCodFiscImpresa.Text = controlloIdentita.Impresa.CodiceFiscale;
                }

                lMeseAnno.Text = string.Format("{0}/{1}", controlloIdentita.Mese, controlloIdentita.Anno);
                string mese = string.Format("{0}/{1}", controlloIdentita.Mese, controlloIdentita.Anno);


                if (controlloIdentita.DataUltimaDenuncia.HasValue)
                {
                    lUltimaDenuncia.ForeColor = Color.Black;
                    lUltimaDenuncia.Text = controlloIdentita.DataUltimaDenuncia.Value.ToString("MM/yyyy");
                }
                else
                {
                    lUltimaDenuncia.ForeColor = Color.Red;
                    lUltimaDenuncia.Text = "--";
                }

                if (controlloIdentita.CassaEdileUltimaDenuncia != null)
                {
                    lCe.ForeColor = Color.Black;
                    lCe.Text = controlloIdentita.CassaEdileUltimaDenuncia;
                }
                else
                {
                    lCe.ForeColor = Color.Red;
                    lCe.Text = "--";
                }

                #region INPS new

                lPosizioneContributiva.ForeColor = Color.Red;
                lPosizioneContributiva.Text = "--";
                lPosizioneContributiva.ToolTip = "Nessuna valutazione";

                if (controlloIdentita.ContribuzioniInps != null && controlloIdentita.ContribuzioniInps.Count > 0)
                {
                    DateTime? dataContrib =
                        controlloIdentita.ContribuzioniInps.ContainsImpresa(lIvaCodFiscImpresa.Text.Trim(),
                                                                            controlloIdentita.Mese, controlloIdentita.Anno);
                    bool? regolare =
                        controlloIdentita.ContribuzioniInps.ContainsImpresaAnnoMese(lIvaCodFiscImpresa.Text.Trim(),
                                                                                    controlloIdentita.Mese,
                                                                                    controlloIdentita.Anno);
                    if (dataContrib.HasValue)
                    {
                        if (regolare.HasValue)
                        {
                            if (regolare.Value)
                            {
                                lPosizioneContributiva.ForeColor = Color.Green;
                                lPosizioneContributiva.Text = "OK";
                                lPosizioneContributiva.ToolTip = "Posizione regolare";
                            }
                            else
                            {
                                if ((dataContrib.Value.Year < controlloIdentita.Anno) ||
                                    (dataContrib.Value.Month < controlloIdentita.Mese &&
                                     dataContrib.Value.Year == controlloIdentita.Anno))
                                {
                                    lPosizioneContributiva.ForeColor = Color.DarkOrange;
                                    lPosizioneContributiva.Text = string.Format("KO (OK fino al {0}/{1})",
                                                                                dataContrib.Value.Month,
                                                                                dataContrib.Value.Year);
                                    lPosizioneContributiva.ToolTip = "Posizione irregolare";
                                }
                                else
                                {
                                    lPosizioneContributiva.ForeColor = Color.Red;
                                    lPosizioneContributiva.ToolTip = "Posizione irregolare";
                                    lPosizioneContributiva.Text = "KO";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (regolare.HasValue)
                        {
                            if (!regolare.Value)
                            {
                                lPosizioneContributiva.ForeColor = Color.Red;
                                lPosizioneContributiva.ToolTip = "Posizione irregolare";
                                lPosizioneContributiva.Text = "KO";
                            }
                        }
                        else
                        {
                            lPosizioneContributiva.ForeColor = Color.Red;
                            lPosizioneContributiva.Text = "--";
                            lPosizioneContributiva.ToolTip = "Nessuna valutazione";
                        }
                    }
                }

                //if ((controlloIdentita.Anno < 2010) || ((controlloIdentita.Anno == 2010) && (controlloIdentita.Mese < 4)))
                //{
                //    lPosizioneContributiva.ForeColor = Color.Red;
                //    lPosizioneContributiva.Text = "--";
                //    lPosizioneContributiva.ToolTip = "Nessuna valutazione";
                //}

                #endregion

                lDenuncia.ToolTip = string.Format("Presenza della denuncia effettuata dall'impresa {0} per il mese {1}",
                                                  impresa, mese);
                lLavoratoreDenuncia.ToolTip =
                    string.Format("Presenza del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                                  lavoratore, impresa, mese);
                lOreLavorate.ToolTip =
                    string.Format(
                        "Presenza di ore lavorate del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                        lavoratore, impresa, mese);
                if (controlloIdentita.ControlliEffettuati)
                {
                    if (controlloIdentita.ControlloDenunciaSuperato)
                    {
                        lDenuncia.ForeColor = Color.Green;
                        lDenuncia.Text = "OK";
                    }
                    else
                    {
                        lDenuncia.ToolTip =
                            string.Format("NON è presente una denuncia effettuata dall'impresa {0} per il mese {1}",
                                          impresa, mese);
                        lDenuncia.ForeColor = Color.Red;
                        lDenuncia.Text = "KO";
                    }

                    bool rappLeg = false;

                    if (controlloIdentita.ControlloLavoratoreDenunciaSuperato)
                    {
                        lLavoratoreDenuncia.ForeColor = Color.Green;
                        lLavoratoreDenuncia.Text = "OK";
                    }
                    else
                    {
                        lLavoratoreDenuncia.ToolTip =
                            string.Format(
                                "NON è presente il lavoratore {0} nella denuncia effettuata dall impresa {1} per il mese {2}",
                                lavoratore, impresa, mese);
                        lLavoratoreDenuncia.ForeColor = Color.Red;
                        lLavoratoreDenuncia.Text = "KO";

                        try
                        {
                            if (controlloIdentita.Impresa.IdImpresa.HasValue)
                            {
                                Denuncia denuncia = _accessoCantieriBiz.GetDenuncia(controlloIdentita.Anno,
                                                                                    controlloIdentita.Mese,
                                                                                    controlloIdentita.Impresa.IdImpresa.
                                                                                        Value);

                                if (denuncia != null)
                                {
                                    if (controlloIdentita.Lavoratore.IdLavoratore.HasValue)
                                    {
                                        RapportoLavoratoreImpresa rapporto =
                                            _accessoCantieriBiz.GetRapportoLavoratoreImpresa(
                                                controlloIdentita.Impresa.IdImpresa.Value,
                                                controlloIdentita.Lavoratore.IdLavoratore.Value,
                                                denuncia.DataDenuncia);

                                        if (!string.IsNullOrEmpty(rapporto.Tipo))
                                        {
                                            lLavoratoreDenuncia.ToolTip =
                                                string.Format("Rappresentante legale dell'impresa {0}", impresa);
                                            lLavoratoreDenuncia.ForeColor = Color.Green;
                                            lLavoratoreDenuncia.Text = "Rapp. leg.";

                                            rappLeg = true;
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                        }
                    }

                    if (controlloIdentita.ControlloOreDenunciaSuperato)
                    {
                        lOreLavorate.ForeColor = Color.Green;
                        lOreLavorate.Text = "OK";
                    }
                    else
                    {
                        lOreLavorate.ToolTip =
                            string.Format(
                                "NON sono presenti ore lavorate del lavoratore {0} nella denuncia effettuata dall'impresa {1} per il mese {2}",
                                lavoratore, impresa, mese);
                        lOreLavorate.ForeColor = Color.Red;
                        lOreLavorate.Text = "KO";

                        if (rappLeg)
                        {
                            lOreLavorate.ToolTip = string.Format("Rappresentante legale dell'impresa {0}", impresa);
                            lOreLavorate.ForeColor = Color.Green;
                            lOreLavorate.Text = "Rapp. Leg.";
                        }
                    }

                    if (String.IsNullOrEmpty(lCodiceCe.Text))
                    {
                        //lDenuncia.ToolTip = "Non valutabile";
                        lLavoratoreDenuncia.ToolTip = "Non valutabile";
                        lOreLavorate.ToolTip = "Non valutabile";

                        //lDenuncia.ForeColor = Color.Red;
                        lLavoratoreDenuncia.ForeColor = Color.Red;
                        lOreLavorate.ForeColor = Color.Red;

                        //lDenuncia.Text = "--";
                        lLavoratoreDenuncia.Text = "--";
                        lOreLavorate.Text = "--";
                    }
                }
                else
                {
                    lDenuncia.ToolTip = "Non valutabile";
                    lLavoratoreDenuncia.ToolTip = "Non valutabile";
                    lOreLavorate.ToolTip = "Non valutabile";

                    lDenuncia.ForeColor = Color.Red;
                    lLavoratoreDenuncia.ForeColor = Color.Red;
                    lOreLavorate.ForeColor = Color.Red;

                    lDenuncia.Text = "--";
                    lLavoratoreDenuncia.Text = "--";
                    lOreLavorate.Text = "--";
                }

                if (controlloIdentita.Impresa != null)
                {
                    if (controlloIdentita.Impresa.IdImpresa.HasValue)
                    {
                        if (controlloIdentita.Impresa.DataIscrizione.HasValue)
                        {
                            if (
                                new DateTime(controlloIdentita.Impresa.DataIscrizione.Value.Year,
                                             controlloIdentita.Impresa.DataIscrizione.Value.Month, 1) >
                                new DateTime(controlloIdentita.Anno, controlloIdentita.Mese, 1))
                            {
                                lDenuncia.ToolTip = "Impresa non ancora iscritta";
                                lLavoratoreDenuncia.ToolTip = "Impresa non ancora iscritta";
                                lOreLavorate.ToolTip = "Impresa non ancora iscritta";

                                lDenuncia.ForeColor = Color.Red;
                                lLavoratoreDenuncia.ForeColor = Color.Red;
                                lOreLavorate.ForeColor = Color.Red;

                                lDenuncia.Text = "--";
                                lLavoratoreDenuncia.Text = "--";
                                lOreLavorate.Text = "--";
                            }
                        }
                    }
                }

                lRegolarita.ToolTip = string.Format("Regolarità dell'impresa {0} per BNI", impresa);
                if (controlloIdentita.ControlliEffettuati && controlloIdentita.ControlloDenunciaSuperato &&
                    controlloIdentita.ControlloDebitiEffettuato)
                {
                    if (controlloIdentita.ControlloDebitiSuperato)
                    {
                        lRegolarita.ForeColor = Color.Green;
                        lRegolarita.Text = "OK";
                    }
                    else
                    {
                        if (DateTime.Now >= new DateTime(controlloIdentita.Anno, controlloIdentita.Mese, 1).AddMonths(3))
                        {
                            lRegolarita.ToolTip = string.Format("Risulta dal controllo di irregolarità");
                            lRegolarita.ForeColor = Color.Red;
                            lRegolarita.Text = "KO";
                        }
                        else
                        {
                            lRegolarita.ToolTip = "Non valutabile";
                            lRegolarita.ForeColor = Color.Red;
                            lRegolarita.Text = "--";
                        }
                    }
                }
                else
                {
                    lRegolarita.ToolTip = "Non valutabile";
                    lRegolarita.ForeColor = Color.Red;
                    lRegolarita.Text = "--";
                }


                if (controlloIdentita.Impresa != null)
                {
                    if (controlloIdentita.Impresa.IdImpresa.HasValue)
                    {
                        if (controlloIdentita.Impresa.DataIscrizione.HasValue)
                        {
                            if (
                                new DateTime(controlloIdentita.Impresa.DataIscrizione.Value.Year,
                                             controlloIdentita.Impresa.DataIscrizione.Value.Month, 1) >
                                new DateTime(controlloIdentita.Anno, controlloIdentita.Mese, 1))
                            {
                                lRegolarita.ToolTip = "Impresa non ancora iscritta";
                                lRegolarita.ForeColor = Color.Red;
                                lRegolarita.Text = "--";
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(lCodiceCe.Text))
                {
                    if (!string.IsNullOrEmpty(controlloIdentita.CodiceFiscale))
                    {
                        if (controlloIdentita.CodiceFiscale.Length == 16)
                        {
                            if (!CodiceFiscaleManager.VerificaCodiceControlloCodiceFiscale(controlloIdentita.CodiceFiscale))
                            {
                                lNomeCognome.ForeColor = Color.Red;
                                lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                            }
                        }
                        else
                        {
                            lNomeCognome.ForeColor = Color.Red;
                            lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                        }
                    }
                    else
                    {
                        lNomeCognome.ForeColor = Color.Red;
                        lNomeCognome.Text = string.Format("{0} [{1}]", lNomeCognome.Text, "Cod. fisc. non valido");
                    }
                }
            }
        }

        protected void ButtonEsportaPDF_Click(object sender, EventArgs e)
        {
            RadGridTimbrature.AllowPaging = false;
            CaricaGridTimbratureFilter(0);

            RadGridTimbrature.Columns.FindByUniqueName("periodo").HeaderStyle.Width = new Unit("40px");
            RadGridTimbrature.Columns.FindByUniqueName("periodo").ItemStyle.Width = new Unit("40px");
            RadGridTimbrature.Columns.FindByUniqueName("impresa").HeaderStyle.Width = new Unit("180px");
            RadGridTimbrature.Columns.FindByUniqueName("impresa").ItemStyle.Width = new Unit("180px");
            RadGridTimbrature.Columns.FindByUniqueName("codiceFiscale").HeaderStyle.Width = new Unit("80px");
            RadGridTimbrature.Columns.FindByUniqueName("codiceFiscale").ItemStyle.Width = new Unit("80px");
            RadGridTimbrature.Columns.FindByUniqueName("controlliCEMI").HeaderStyle.Width = new Unit("180px");
            RadGridTimbrature.Columns.FindByUniqueName("controlliCEMI").ItemStyle.Width = new Unit("180px");

            foreach (GridHeaderItem colHeader in RadGridTimbrature.MasterTableView.GetItems(GridItemType.Header))
            {
                colHeader.Style["text-align"] = "left";
                colHeader.Style["vertical-align"] = "top";
                colHeader.Style["font-size"] = "8px";
                colHeader.Style["font-weight"] = "bold";
            }

            foreach (GridDataItem dItem in RadGridTimbrature.Items)
            {
                dItem.Style["font-family"] = "Courier New";
                dItem.Style["text-align"] = "left";
                dItem.Style["vertical-align"] = "top";
                dItem.Style["font-size"] = "6px";

                Label lDenuncia = (Label)dItem.FindControl("LabelDenuncia");
                lDenuncia.Style["color"] = lDenuncia.ForeColor.Name;
            }

            //RadGridTimbrature.ExportSettings.IgnorePaging = true;
            RadGridTimbrature.ExportSettings.Pdf.PageTitle =
                String.Format("Controlli Accesso Cantieri - stampato il {0} alle {1}", DateTime.Now.ToString("dd/MM/yyyy"),
                              DateTime.Now.ToString("HH:mm"));
            RadGridTimbrature.MasterTableView.ExportToPdf();
        }

        protected void RadGridTimbrature_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaGridTimbratureFilter(e.NewPageIndex);
        }

        protected void DropDownListCantiere_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32? idCantiere = null;

            if (!String.IsNullOrEmpty(DropDownListCantiere.SelectedValue))
            {
                idCantiere = Int32.Parse(DropDownListCantiere.SelectedValue);
            }

            CaricaImprese(idCantiere);
        }
    }
}