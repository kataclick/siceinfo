﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Delegates;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriRicercaImpresa : System.Web.UI.UserControl
    {
        private const int INDICECODICE = 0;

        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        //private string _impresaTrovata;

        //public string ImpresaTrovata
        //{
        //    get { return _impresaTrovata; }
        //    set
        //    {
        //        _impresaTrovata = value;
        //        TextBoxRagioneSociale.Text = "%" + _impresaTrovata;
        //    }
        //}

        //public string RagioneSociale
        //{
        //    get { return TextBoxRagioneSociale.Text; }
        //}

        public event EventHandler OnNuovaImpresaSelected;
        public event ImpreseSelectedEventHandler OnImpresaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void Reset()
        {
            GridViewImprese.DataSource = null;
            GridViewImprese.DataBind();

            TextBoxRagioneSociale.Text = string.Empty;
            TextBoxIndirizzo.Text = string.Empty;
            TextBoxComune.Text = string.Empty;
            TextBoxIvaFiscale.Text = string.Empty;
        }

        public void CaricaImpreseRicerca(string codFisc, string pIva)
        {
            LabelErrore.Text = string.Empty;
            int? codice = null;
            string ragioneSociale = null;
            string comune = null;
            string indirizzo = null;
            string ivaCodFisc = null;

            if (!string.IsNullOrEmpty(pIva))
            {
                TextBoxIvaFiscale.Text = pIva;
            }
            else
            {
                TextBoxIvaFiscale.Text = codFisc;
            }

            if (!string.IsNullOrEmpty(TextBoxIvaFiscale.Text))
                ivaCodFisc = TextBoxIvaFiscale.Text;

            ImpresaCollection listaImprese =
                _biz.GetimpreseOrdinate(codice, ragioneSociale, comune, indirizzo, ivaCodFisc, null, null);
            GridViewImprese.DataSource = listaImprese;
            GridViewImprese.DataBind();
        }

        private void CaricaImprese()
        {
            if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text.Trim()) &&
                string.IsNullOrEmpty(TextBoxIndirizzo.Text.Trim()) && string.IsNullOrEmpty(TextBoxComune.Text.Trim()) &&
                string.IsNullOrEmpty(TextBoxIvaFiscale.Text.Trim()) && string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
            {
                LabelErrore.Text = "Digitare un filtro";
            }
            else
            {
                LabelErrore.Text = string.Empty;
                int? codice = null;
                string ragioneSociale = null;
                string comune = null;
                string indirizzo = null;
                string ivaCodFisc = null;

                string ragSocRic = TextBoxRagioneSociale.Text.Trim();
                if (!string.IsNullOrEmpty(ragSocRic))
                    ragioneSociale = ragSocRic;

                string comRic = TextBoxComune.Text.Trim();
                if (!string.IsNullOrEmpty(comRic))
                    comune = comRic;

                string indRic = TextBoxIndirizzo.Text.Trim();
                if (!string.IsNullOrEmpty(indRic))
                    indirizzo = indRic;

                string ivaRic = TextBoxIvaFiscale.Text;
                if (!string.IsNullOrEmpty(ivaRic))
                    ivaCodFisc = ivaRic;

                if (!string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
                    codice = Int32.Parse(TextBoxCodice.Text);

                ImpresaCollection listaImprese =
                    _biz.GetimpreseOrdinate(codice, ragioneSociale, comune, indirizzo, ivaCodFisc, null, null);
                GridViewImprese.DataSource = listaImprese;
                GridViewImprese.DataBind();
            }
        }

        private void CaricaImprese(string sortExpression)
        {
            int? codice = null;
            string ragioneSociale = null;
            string comune = null;
            string indirizzo = null;
            string ivaCodFisc = null;

            string ragSocRic = TextBoxRagioneSociale.Text.Trim();
            if (!string.IsNullOrEmpty(ragSocRic))
                ragioneSociale = ragSocRic;

            string comRic = TextBoxComune.Text.Trim();
            if (!string.IsNullOrEmpty(comRic))
                comune = comRic;

            string indRic = TextBoxIndirizzo.Text.Trim();
            if (!string.IsNullOrEmpty(indRic))
                indirizzo = indRic;

            string ivaRic = TextBoxIvaFiscale.Text;
            if (!string.IsNullOrEmpty(ivaRic))
                ivaCodFisc = ivaRic;

            if (!string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
                codice = Int32.Parse(TextBoxCodice.Text);

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "DESC";
                else
                    direct = "ASC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            ImpresaCollection listaImprese =
                _biz.GetimpreseOrdinate(codice, ragioneSociale, comune, indirizzo, ivaCodFisc, sortExpression, direct);
            GridViewImprese.DataSource = listaImprese;
            GridViewImprese.PageIndex = 0;
            GridViewImprese.DataBind();
        }

        private void CaricaImpresePreservaOrdine(string sortExpression)
        {
            int? codice = null;
            string ragioneSociale = null;
            string comune = null;
            string indirizzo = null;
            string ivaCodFisc = null;

            string ragSocRic = TextBoxRagioneSociale.Text.Trim();
            if (!string.IsNullOrEmpty(ragSocRic))
                ragioneSociale = ragSocRic;

            string comRic = TextBoxComune.Text.Trim();
            if (!string.IsNullOrEmpty(comRic))
                comune = comRic;

            string indRic = TextBoxIndirizzo.Text.Trim();
            if (!string.IsNullOrEmpty(indRic))
                indirizzo = indRic;

            string ivaRic = TextBoxIvaFiscale.Text;
            if (!string.IsNullOrEmpty(ivaRic))
                ivaCodFisc = ivaRic;

            if (!string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
                codice = Int32.Parse(TextBoxCodice.Text);

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "ASC";
                else
                    direct = "DESC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            ImpresaCollection listaImprese =
                _biz.GetimpreseOrdinate(codice, ragioneSociale, comune, indirizzo, ivaCodFisc, sortExpression, direct);
            GridViewImprese.DataSource = listaImprese;
            GridViewImprese.PageIndex = 0;
            GridViewImprese.DataBind();
        }

        protected void GridViewImprese_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idImpresa = (int)GridViewImprese.DataKeys[e.NewSelectedIndex]["IdImpresa"];

            Impresa impresa =
                _biz.GetimpreseOrdinate(idImpresa, null, null, null, null, null, null)[0];

            if (OnImpresaSelected != null)
                OnImpresaSelected(impresa);
        }

        /// <summary>
        ///   Evento di ricerca
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            GridViewImprese.Columns[INDICECODICE].Visible = true;

            CaricaImprese();
        }

        protected void GridViewImprese_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');

                if (ord[1] == "DESC")
                    CaricaImpresePreservaOrdine(ord[0]);
                else
                    CaricaImpresePreservaOrdine(ord[0]);
            }
            else
                CaricaImprese();

            GridViewImprese.PageIndex = e.NewPageIndex;
            GridViewImprese.DataBind();
        }

        protected void GridViewImprese_Sorting(object sender, GridViewSortEventArgs e)
        {
            CaricaImprese(e.SortExpression);
        }

        protected void ButtonChiudi_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        protected void ButtonNuovo_Click(object sender, EventArgs e)
        {
            if (OnNuovaImpresaSelected != null)
                OnNuovaImpresaSelected(this, null);
        }

        protected void GridViewImprese_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Impresa impresa = (Impresa)e.Row.DataItem;

                Label lRagioneSociale = (Label)e.Row.FindControl("LabelRagioneSociale");
                Label lPartitaIva = (Label)e.Row.FindControl("LabelPartitaIva");
                Label lCodiceFiscale = (Label)e.Row.FindControl("LabelCodiceFiscale");


                lRagioneSociale.Text = impresa.RagioneSociale;
                lPartitaIva.Text = impresa.PartitaIva;
                lCodiceFiscale.Text = impresa.CodiceFiscale;
            }
        }
    }
}