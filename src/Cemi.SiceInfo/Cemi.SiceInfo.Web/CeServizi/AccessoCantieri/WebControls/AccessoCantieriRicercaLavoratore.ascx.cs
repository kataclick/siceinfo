﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Delegates;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriRicercaLavoratore : System.Web.UI.UserControl
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
        public event LavoratoriSelectedEventHandler OnLavoratoreSelected;
        public event EventHandler OnNuovoLavoratoreSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["DataInizio"] != null || ViewState["DataFine"] != null)
            {
                String dataInizio = String.Empty;
                String dataFine = String.Empty;

                if (ViewState["DataInizio"] != null)
                {
                    dataInizio = ((DateTime)ViewState["DataInizio"]).ToString("dd/MM/yyyy");
                }
                if (ViewState["DataFine"] != null)
                {
                    dataFine = ((DateTime)ViewState["DataFine"]).ToString("dd/MM/yyyy");
                }

                if (!String.IsNullOrEmpty(ButtonNuovo.Attributes["onclick"]))
                {
                    ButtonNuovo.Attributes.Remove("onclick");
                }
                ButtonNuovo.Attributes.Add("onclick",
                                           String.Format("openLavoratoreNuovo(1, '{0}', '{1}'); return false;", dataInizio,
                                                         dataFine));
            }
        }

        protected String RecuperaPostBackCode()
        {
            return Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
        }

        public void CaricaDataInizio(DateTime? dataInizio)
        {
            if (dataInizio.HasValue)
            {
                ViewState["DataInizio"] = dataInizio.Value;
            }
        }

        public void CaricaDataFine(DateTime? dataFine)
        {
            if (dataFine.HasValue)
            {
                ViewState["DataFine"] = dataFine.Value;
            }
        }

        private void CaricaLavoratori()
        {
            string cognome = null;
            string nome = null;
            string codFisc = null;
            DateTime? dataNascita = null;
            DateTime dataNascitaD;

            Impresa impresaSelezionata = (Impresa)ViewState["impresaSelezionata"];

            int? idImpresa = impresaSelezionata.IdImpresa;

            string cogRic = TextBoxCognome.Text.Trim();
            if (!string.IsNullOrEmpty(cogRic))
                cognome = cogRic;

            string nomRic = TextBoxNome.Text.Trim();
            if (!string.IsNullOrEmpty(nomRic))
                nome = nomRic;

            if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
                DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
                dataNascita = dataNascitaD;

            string codFiscRic = TextBoxCodiceFiscale.Text.Trim();
            if (!string.IsNullOrEmpty(codFiscRic))
                codFisc = codFiscRic;

            //Valla: farlo con i filter
            LavoratoreCollection listaLavoratori =
                _biz.GetLavoratoriOrdinati(null, cognome, nome, dataNascita, codFisc, null, null, null, null, idImpresa);

            if (listaLavoratori.Count > 0)
            {
                GridViewLavoratori.DataSource = listaLavoratori;
                GridViewLavoratori.DataBind();
                GridViewLavoratori.Visible = true;
            }
            else
            {
                GridViewLavoratori.Visible = false;
                LabelErrore.Visible = true;
                LabelErrore.Text = "Nessun lavoratore trovato";
            }
        }

        private void CaricaLavoratori(string sortExpression)
        {
            string cognome = null;
            string nome = null;
            string codFisc = null;
            DateTime? dataNascita = null;
            DateTime dataNascitaD;

            Impresa impresaSelezionata = (Impresa)ViewState["impresaSelezionata"];
            int? idImpresa = impresaSelezionata.IdImpresa;

            if (!string.IsNullOrEmpty(TextBoxCognome.Text))
                cognome = TextBoxCognome.Text;

            if (!string.IsNullOrEmpty(TextBoxNome.Text))
                nome = TextBoxNome.Text;

            if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
                DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
                dataNascita = dataNascitaD;

            string codFiscRic = TextBoxCodiceFiscale.Text.Trim();
            if (!string.IsNullOrEmpty(codFiscRic))
                codFisc = codFiscRic;

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "DESC";
                else
                    direct = "ASC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            LavoratoreCollection listaLavoratori =
                _biz.GetLavoratoriOrdinati(null, cognome, nome, dataNascita, codFisc, null, null, sortExpression, direct,
                                           idImpresa);

            if (listaLavoratori.Count > 0)
            {
                GridViewLavoratori.DataSource = listaLavoratori;
                GridViewLavoratori.PageIndex = 0;
                GridViewLavoratori.DataBind();
                GridViewLavoratori.Visible = true;
            }
            else
            {
                GridViewLavoratori.Visible = false;
                LabelErrore.Visible = true;
                LabelErrore.Text = "Nessun lavoratore trovato";
            }
        }

        private void CaricaLavoratoriPreservaOrdine(string sortExpression)
        {
            string cognome = null;
            string nome = null;
            string codFisc = null;
            DateTime? dataNascita = null;
            DateTime dataNascitaD;

            Impresa impresaSelezionata = (Impresa)ViewState["impresaSelezionata"];
            int? idImpresa = impresaSelezionata.IdImpresa;

            if (!string.IsNullOrEmpty(TextBoxCognome.Text))
                cognome = TextBoxCognome.Text;

            if (!string.IsNullOrEmpty(TextBoxNome.Text))
                nome = TextBoxNome.Text;

            if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
                DateTime.TryParse(TextBoxDataNascita.Text, out dataNascitaD))
                dataNascita = dataNascitaD;

            string codFiscRic = TextBoxCodiceFiscale.Text.Trim();
            if (!string.IsNullOrEmpty(codFiscRic))
                codFisc = codFiscRic;

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "ASC";
                else
                    direct = "DESC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            LavoratoreCollection listaLavoratori =
                _biz.GetLavoratoriOrdinati(null, cognome, nome, dataNascita, codFisc, null, null, sortExpression, direct,
                                           idImpresa);

            if (listaLavoratori.Count > 0)
            {
                GridViewLavoratori.DataSource = listaLavoratori;
                GridViewLavoratori.PageIndex = 0;
                GridViewLavoratori.DataBind();
                GridViewLavoratori.Visible = true;
            }
            else
            {
                GridViewLavoratori.Visible = false;
                LabelErrore.Visible = true;
                LabelErrore.Text = "Nessun lavoratore trovato";
            }
        }

        protected void GridViewLavoratori_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idLavoratore = (int)GridViewLavoratori.DataKeys[e.NewSelectedIndex]["IdLavoratore"];

            Impresa impresaSelezionata = (Impresa)ViewState["impresaSelezionata"];
            int? idImpresa = impresaSelezionata.IdImpresa;

            Lavoratore lavoratore =
                _biz.GetLavoratoriOrdinati(idLavoratore, null, null, null, null, null, null, null, null, idImpresa)[0];

            if (OnLavoratoreSelected != null)
            {
                OnLavoratoreSelected(lavoratore);
                Reset();
            }
        }

        public void Reset()
        {
            TextBoxCognome.Text = string.Empty;
            TextBoxNome.Text = string.Empty;
            TextBoxDataNascita.Text = string.Empty;
            TextBoxCodiceFiscale.Text = string.Empty;

            GridViewLavoratori.DataSource = null;
            GridViewLavoratori.DataBind();
            GridViewLavoratori.Visible = false;

            LabelErrore.Visible = false;
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaLavoratori();
            }
        }

        public void SetImpresaSelezionata(Impresa impresa)
        {
            ViewState["impresaSelezionata"] = impresa;
        }

        protected void GridViewLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');

                if (ord[1] == "DESC")
                    CaricaLavoratoriPreservaOrdine(ord[0]);
                else
                    CaricaLavoratoriPreservaOrdine(ord[0]);
            }
            else
                CaricaLavoratori();

            GridViewLavoratori.PageIndex = e.NewPageIndex;
            GridViewLavoratori.DataBind();
        }

        protected void GridViewLavoratori_Sorting(object sender, GridViewSortEventArgs e)
        {
            CaricaLavoratori(e.SortExpression);
        }

        protected void ButtonNuovo_Click(object sender, EventArgs e)
        {
            if (OnNuovoLavoratoreSelected != null)
                OnNuovoLavoratoreSelected(this, null);
        }

        protected void GridViewLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Lavoratore lavoratore = (Lavoratore)e.Row.DataItem;

                Button bSeleziona = (Button)e.Row.FindControl("ButtonSeleziona");

                DateTime dataInizio = DateTime.MinValue;
                if (ViewState["DataInizio"] != null)
                {
                    dataInizio = (DateTime)ViewState["DataInizio"];
                }
                DateTime dataFine = DateTime.MaxValue;
                if (ViewState["DataFine"] != null)
                {
                    dataFine = (DateTime)ViewState["DataFine"];
                }

                Impresa impresaSelezionata = (Impresa)ViewState["impresaSelezionata"];
                bSeleziona.Attributes.Add("onclick",
                                          String.Format("openLavoratore(1, {0}, {1}, '{2}', '{3}'); return false;",
                                                        lavoratore.IdLavoratore, impresaSelezionata.IdImpresa,
                                                        dataInizio.ToString("dd/MM/yyyy"), dataFine.ToString("dd/MM/yyyy")));
            }
        }

    }
}