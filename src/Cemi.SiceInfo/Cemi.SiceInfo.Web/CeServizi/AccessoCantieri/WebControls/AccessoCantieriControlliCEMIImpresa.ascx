﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriControlliCEMIImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriControlliCEMIImpresa" %>

<%@ Register Src="AccessoCantieriRilevatoriInserimento.ascx" TagName="AccessoCantieriRilevatoriInserimento" TagPrefix="uc7" %>
<%@ Register src="RicercaCantieri.ascx" tagname="RicercaCantieri" tagprefix="uc1" %>


<script type="text/javascript" language="javascript">
    function requestStart(sender, args)
    {
        if (args.get_eventTarget().indexOf("DownloadPDF") > 0)
            args.set_enableAjax(false);
    }
</script>

<table class="standardTable">
    <tr>
        <td>
            <asp:Panel ID="PanelRilevatori" runat="server">
                <br />
                <asp:Panel ID="PanelFiltroTimb" runat="server" Visible="True">
                    <table class="borderedTable">
                        <tr>
                            <td>Cantiere<b>*</b>
                            </td>
                            <td>Impresa<b>*</b>
                            </td>
                            <td>Mese / Anno (MM/aaaa)
                            </td>
                            <td>Codice fiscale
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownListCantiere" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="True" Width="150px"
                                    OnSelectedIndexChanged="DropDownListCantiere_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidatorCantiere"
                                    runat="server"
                                    ControlToValidate="DropDownListCantiere"
                                    ValidationGroup="ricercaControlli"
                                    ForeColor="Red"
                                    ErrorMessage="Selezionare un cantiere">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListImpresa" runat="server" AppendDataBoundItems="True"
                                    Width="150px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator
                                    ID="RequiredFieldValidatorImpresa"
                                    runat="server"
                                    ControlToValidate="DropDownListImpresa"
                                    ValidationGroup="ricercaControlli"
                                    ForeColor="Red"
                                    ErrorMessage="Selezionare un'impresa">
                                    *
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <%--<telerik:RadDatePicker ID="RadDatePickerMeseAnno" Runat="server" 
                                    Culture="it-IT">
                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                                        ViewSelectorText="x">
                                    </Calendar>
                                    <DateInput DisplayDateFormat="MM/yyyy">
                                    </DateInput>
                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                </telerik:RadDatePicker>--%>
                                <asp:TextBox ID="TextBoxMeseAnno" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Anomalie
                            </td>
                            <td>Ruolo
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownListAnomalie" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="False" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListRuoli" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="False" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;
                            </td>
                            <td align="right">
                                <asp:Button ID="ButtonVisualizzaTimb" runat="server" OnClick="ButtonVisualizzaTimb_Click"
                                    Text="Ricerca" ValidationGroup="ricercaControlli" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:ValidationSummary
                                    ID="ValidationSummaryRicerca"
                                    runat="server"
                                    CssClass="messaggiErrore"
                                    ValidationGroup="ricercaControlli" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <asp:Button
                    ID="ButtonEsportaPDF"
                    runat="server"
                    Text="PDF"
                    Width="150px"
                    OnClick="ButtonEsportaPDF_Click"
                    Visible="false" />
                <telerik:RadGrid
                    ID="RadGridTimbrature"
                    runat="server"
                    Width="100%"
                    GridLines="None"
                    AllowPaging="True"
                    PageSize="15" OnItemDataBound="RadGridTimbrature_ItemDataBound"
                    Font-Size="Small" OnPageIndexChanged="RadGridTimbrature_PageIndexChanged"
                    Visible="false">
                    <ExportSettings IgnorePaging="false" OpenInNewWindow="true" ExportOnlyData="false" FileName="ControlliAccessoCantieri">
                        <Pdf PageWidth="297mm" PageHeight="210mm" PageTitle="Controlli Accesso Cantieri" Title="ControlliAccessoCantieri" PageHeaderMargin="10mm" PageBottomMargin="10mm" PageLeftMargin="10mm" PageRightMargin="10mm" />
                    </ExportSettings>
                    <MasterTableView>
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Periodo" UniqueName="periodo">
                                <ItemStyle Width="30px" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <asp:Label ID="LabelMeseAnno" runat="server"></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="impresa">
                                <ItemStyle Width="200px" />
                                <ItemTemplate>
                                    <table width="200px" style="text-align: left;">
                                        <colgroup>
                                            <col width="60px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Cod. C.E.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelCodiceImpresa" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>P.IVA/Cod.Fisc.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelIvaCodFiscImpresa" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Rag.Soc.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelRagioneSocialeImpresa" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Stato attuale:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelStatoImpresa" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Regolarità.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelRegolarita" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Denuncia:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelDenuncia" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Cod. fisc. lavoratore"
                                UniqueName="codiceFiscale" DataField="CodiceFiscale">
                                <ItemStyle Width="40px" VerticalAlign="Top" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Controllo CEMI"
                                UniqueName="controlliCEMI">
                                <ItemStyle Width="200px" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <table width="200px" style="text-align: left;">
                                        <colgroup>
                                            <col width="60px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Nome:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelNomeCognome" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Cod. C.E.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelCodiceCE" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Nato il:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Ruolo:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Presenza:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelLavoratoreDenuncia" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Ore:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelOreLavorate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Controllo Casse Edili lombarde"
                                UniqueName="controlliCEXChange">
                                <ItemStyle Width="120px" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <table width="120px" style="text-align: left;">
                                        <colgroup>
                                            <col width="60px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Ultima Denuncia:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelUltimaDenuncia" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>C.E.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelCE" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Controllo INPS"
                                UniqueName="controlloINPS">
                                <ItemStyle Width="120px" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <table width="120px">
                                        <colgroup>
                                            <col width="60px" />
                                            <col />
                                        </colgroup>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Posizione Contrib.:</b>
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelPosizionecontributiva" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"
                        EnableImageSprites="True">
                    </HeaderContextMenu>
                </telerik:RadGrid>
                <br />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAggiungiRilevatore" runat="server" Visible="False">
            </asp:Panel>
        </td>
    </tr>
</table>
