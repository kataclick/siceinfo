﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriLavoratoriTutteImprese : System.Web.UI.UserControl
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaImpreseLavoratori(Int32 idDomanda)
        {
            ViewState["IdDomanda"] = idDomanda;
            WhiteListImpresaCollection impreseLavoratori = _biz.GetLavoratoriInDomandaNoFoto(idDomanda);
            CaricaImpreseLavoratori(impreseLavoratori, idDomanda);
        }

        public void CaricaImpreseLavoratori(WhiteListImpresaCollection impreseLavoratori, Int32? idDomanda)
        {
            if (idDomanda.HasValue)
                ViewState["IdDomanda"] = idDomanda;

            ViewState["ImpreseLavoratori"] = impreseLavoratori;

            Presenter.CaricaElementiInGridView(
                GridViewImpreseLavoratori,
                impreseLavoratori,
                0);
        }

        protected void GridViewImpreseLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                WhiteListImpresa domandaImpresa = (WhiteListImpresa)e.Row.DataItem;
                GridView gvLavoratori = (GridView)e.Row.FindControl("GridViewLavoratori");

                Presenter.CaricaElementiInGridView(
                    gvLavoratori,
                    domandaImpresa.Lavoratori,
                    0);
            }
        }

        protected void GridViewImpreseLavoratori_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            WhiteListImpresaCollection impreseLavoratori;

            impreseLavoratori = ViewState["ImpreseLavoratori"] as WhiteListImpresaCollection;

            if (impreseLavoratori == null)
            {
                Int32 idDomanda = (Int32)ViewState["IdDomanda"];
                impreseLavoratori = _biz.GetLavoratoriInDomanda(idDomanda);
            }

            Presenter.CaricaElementiInGridView(
                GridViewImpreseLavoratori,
                impreseLavoratori,
                e.NewPageIndex);
        }
    }
}