﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriAltrePersone.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriAltrePersone" %>
<%@ Register Src="AccessoCantieriAltrePersoneInserimento.ascx" TagName="AccessoCantieriAltrePersoneinserimento" TagPrefix="uc7" %>
<table class="standardTable">
    <tr>
        <td>
            <br />
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco altre persone abiltate all'accesso"></asp:Label>
            <br />
            <br />
            <br />
            <asp:GridView ID="GridViewAltrePersone" runat="server" AutoGenerateColumns="False"
                OnRowDataBound="GridViewAltrePersone_RowDataBound" Width="100%" OnRowDeleting="GridViewAltrePersone_RowDeleting"
                DataKeyNames="IdAltraPersona">
                <Columns>
                    <asp:TemplateField HeaderText="AltraPersona">
                        <ItemTemplate>
                            <asp:Label ID="LabelAltraPersona" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="codiceFiscale" HeaderText="Codice Fiscale" />
                    <asp:TemplateField HeaderText="Ruolo">
                        <ItemTemplate>
                            <asp:Label ID="LabelRuolo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText=""
                        ShowDeleteButton="True" DeleteImageUrl="../../images/editdelete.png" HeaderText="Cancellazione">
                        <ItemStyle Width="10px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna altra persona presente
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <br />
            <asp:Label ID="LabelGiaPresente" runat="server" ForeColor="Red" Text="Altra persona già presente"
                Visible="False"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <table class="filledtable">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Altra persona abilitata all'accesso"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td>
                        <br />
                        <uc7:AccessoCantieriAltrePersoneinserimento ID="AccessoCantieriAltrePersoneInserimento1"
                            runat="server" />
                        <br />
                        <br />
                        <asp:Label ID="LabelInserimentoAltraPersona" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <asp:Button ID="ButtonaggiungiAltraPersona" runat="server" Text="Aggiungi" Width="225px"
                            OnClick="ButtonaggiungiAltraPersona_Click" CausesValidation="False" Enabled="True"
                            Style="text-align: center" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
