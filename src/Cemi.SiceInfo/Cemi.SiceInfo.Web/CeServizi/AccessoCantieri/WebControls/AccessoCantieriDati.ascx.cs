﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriDati : System.Web.UI.UserControl
    {
        private const string TESTOMOSTRA = "Mostra";
        private const string TESTONASCONDI = "Nascondi";
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["MostraDatiGenerali"] = false;
                ViewState["MostraImprese"] = false;
                ViewState["MostraImpresePeriodi"] = false;
                ViewState["MostraLavoratori"] = true;
                ViewState["MostraAltrePersone"] = true;
                ViewState["MostraRilevatori"] = true;
                ViewState["MostraReferenti"] = true;

                GestisciBottoni();
            }
        }

        private void GestisciBottoni()
        {
            bool mostraDatiGenerali = (bool)ViewState["MostraDatiGenerali"];
            bool mostraImprese = (bool)ViewState["MostraImprese"];
            bool mostraImpresePeriodi = (bool)ViewState["MostraImpresePeriodi"];
            bool mostraLavoratori = (bool)ViewState["MostraLavoratori"];
            bool mostraAltrePersone = (bool)ViewState["MostraAltrePersone"];
            bool mostraRilevatori = (bool)ViewState["MostraRilevatori"];
            bool mostraReferenti = (bool)ViewState["MostraReferenti"];

            if (mostraDatiGenerali)
            {
                ButtonMostraNascondiDatiGenerali.Text = TESTONASCONDI;
                divDatiGenerali.Visible = true;
            }
            else
            {
                ButtonMostraNascondiDatiGenerali.Text = TESTOMOSTRA;
                divDatiGenerali.Visible = false;
            }

            if (mostraImprese)
            {
                ButtonMostraNascondiAppalti.Text = TESTONASCONDI;
                divAppalti.Visible = true;
            }
            else
            {
                ButtonMostraNascondiAppalti.Text = TESTOMOSTRA;
                divAppalti.Visible = false;
            }

            if (mostraImpresePeriodi)
            {
                ButtonMostraNascondiImpresePeriodi.Text = TESTONASCONDI;
                divImpresePeriodi.Visible = true;
            }
            else
            {
                ButtonMostraNascondiImpresePeriodi.Text = TESTOMOSTRA;
                divImpresePeriodi.Visible = false;
            }

            if (mostraLavoratori)
            {
                ButtonMostraNascondiLavoratori.Text = TESTONASCONDI;
                divLavoratori.Visible = true;
            }
            else
            {
                ButtonMostraNascondiLavoratori.Text = TESTOMOSTRA;
                divLavoratori.Visible = false;
            }

            if (mostraAltrePersone)
            {
                ButtonMostraNascondiAltrePersone.Text = TESTONASCONDI;
                divAltrePersone.Visible = true;
            }
            else
            {
                ButtonMostraNascondiAltrePersone.Text = TESTOMOSTRA;
                divAltrePersone.Visible = false;
            }

            if (mostraRilevatori)
            {
                ButtonMostraNascondiRilevatori.Text = TESTONASCONDI;
                divRilevatori.Visible = true;
            }
            else
            {
                ButtonMostraNascondiRilevatori.Text = TESTOMOSTRA;
                divRilevatori.Visible = false;
            }

            if (mostraReferenti)
            {
                ButtonMostraNascondiReferenti.Text = TESTONASCONDI;
                divReferenti.Visible = true;
            }
            else
            {
                ButtonMostraNascondiReferenti.Text = TESTOMOSTRA;
                divReferenti.Visible = false;
            }
        }

        public void CaricaDomanda(WhiteList whiteList)
        {
            SvuotaCampi();

            ViewState["Subappalti"] = whiteList.Subappalti;
            if (whiteList.IdWhiteList.HasValue)
            {
                if (ViewState["imprese"] == null)
                    ViewState["imprese"] = _biz.GetImpreseSelezionateInSubappalto(whiteList.IdWhiteList.Value);
            }

            LabelIndirizzo.Text = whiteList.Indirizzo;
            LabelCivico.Text = whiteList.Civico;
            LabelComune.Text = whiteList.Comune;
            LabelProvincia.Text = whiteList.Provincia;
            LabelCap.Text = whiteList.Cap;

            if (!string.IsNullOrEmpty(whiteList.Descrizione))
                LabelDescrizione.Text = whiteList.Descrizione;

            if (whiteList.DataInizio.HasValue)
                LabelDataInizio.Text = whiteList.DataInizio.Value.ToString("dd/MM/yyyy");
            if (whiteList.DataFine.HasValue)
                LabelDataFine.Text = whiteList.DataFine.Value.ToString("dd/MM/yyyy");

            CaricaSubappalti(whiteList.Subappalti, 0);

            if (ViewState["altrePersone"] == null)
            {
                if (whiteList.IdWhiteList != null)
                {
                    AltraPersonaCollection apc = _biz.GetAltrePersone(whiteList.IdWhiteList.Value);
                    CaricaAltrePersone(apc);
                }
            }
            else
            {
                CaricaAltrePersone((AltraPersonaCollection)ViewState["altrePersone"]);
            }

            if (ViewState["rilevatori"] == null)
            {
                if (whiteList.IdWhiteList != null)
                {
                    RilevatoreCantiereCollection apc = _biz.GetRilevatoriCantieri(whiteList.IdWhiteList.Value);
                    CaricaRilevatori(apc);
                }
            }
            else
            {
                CaricaAltrePersone((AltraPersonaCollection)ViewState["altrePersone"]);
            }

            AccessoCantieriLavoratoriImpresa1.Inizializza(whiteList, (ImpresaCollection)ViewState["imprese"]);

            if (whiteList.IdWhiteList.HasValue)
            {
                if (whiteList.Lavoratori != null)
                {
                    AccessoCantieriLavoratoriTutteImprese1.CaricaImpreseLavoratori(whiteList.Lavoratori,
                                                                                   whiteList.IdWhiteList.Value);
                }
                else
                {
                    AccessoCantieriLavoratoriTutteImprese1.CaricaImpreseLavoratori(whiteList.IdWhiteList.Value);
                }
            }
            else
            {
                AccessoCantieriLavoratoriTutteImprese1.CaricaImpreseLavoratori(whiteList.Lavoratori, null);
            }

            if (ViewState["referenti"] == null)
            {
                if (whiteList.IdWhiteList != null)
                {
                    ReferenteCollection apc = _biz.GetReferenti(whiteList.IdWhiteList.Value);
                    CaricaReferenti(apc);
                }
            }
            else
            {
                CaricaReferenti((ReferenteCollection)ViewState["referenti"]);
            }
        }

        public void CaricaImprese(ImpresaCollection imprese)
        {
            ViewState["imprese"] = imprese;
        }

        private void CaricaGrigliaImprese()
        {
            ImpresaCollection impColl = new ImpresaCollection();
            SubappaltoCollection subColl = (SubappaltoCollection)ViewState["Subappalti"];

            if (subColl != null)
            {
                foreach (Subappalto sub in subColl)
                {
                    if (sub.Appaltante != null)
                    {
                        if (!impColl.Contains(sub.Appaltante)) impColl.Add(sub.Appaltante);
                    }

                    if (sub.Appaltata != null)
                    {
                        if (!impColl.Contains(sub.Appaltata)) impColl.Add(sub.Appaltata);
                    }
                }
            }

            GridViewImpresePeriodi.DataSource = impColl;
            GridViewImpresePeriodi.DataBind();
        }

        public void CaricaAltrePersone(AltraPersonaCollection altrePersone)
        {
            ViewState["altrePersone"] = altrePersone;
            if (altrePersone != null)
                Presenter.CaricaElementiInGridView(
                    GridViewAltrePersone,
                    altrePersone, 0);
        }

        public void CaricaReferenti(ReferenteCollection referenti)
        {
            ViewState["referenti"] = referenti;
            if (referenti != null)
                Presenter.CaricaElementiInGridView(
                    GridViewReferenti,
                    referenti, 0);
        }

        public void CaricaRilevatori(RilevatoreCantiereCollection ril)
        {
            ViewState["rilevatori"] = ril;
            if (ril != null)
                Presenter.CaricaElementiInGridView(
                    GridViewRilevatori,
                    ril, 0);
        }

        private void CaricaSubappalti(SubappaltoCollection subappalti, Int32 pagina)
        {
            Presenter.CaricaElementiInListView(
                ListViewSubappalti,
                subappalti);

            Presenter.CaricaElementiInGridView(
                GridViewSubappalti,
                subappalti,
                pagina);

            CaricaGrigliaImprese();
        }

        private void SvuotaCampi()
        {
            Presenter.SvuotaCampo(LabelIndirizzo);
            Presenter.SvuotaCampo(LabelCivico);
            Presenter.SvuotaCampo(LabelComune);
            Presenter.SvuotaCampo(LabelProvincia);
            Presenter.SvuotaCampo(LabelCap);

            Presenter.CaricaElementiInListView(
                ListViewSubappalti,
                null);

            Presenter.SvuotaCampo(LabelDescrizione);

            Presenter.SvuotaCampo(LabelDataInizio);
            Presenter.SvuotaCampo(LabelDataFine);

            Presenter.CaricaElementiInGridView(GridViewAltrePersone, null, 0);
            Presenter.CaricaElementiInGridView(GridViewReferenti, null, 0);

            //da fare referente 
        }

        protected void GridViewImpresePeriodi_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Impresa imp = (Impresa)e.Row.DataItem;
                Label lImpresa = (Label)e.Row.FindControl("LabelImpresa");
                Label lPeriodo = (Label)e.Row.FindControl("LabelPeriodo");

                if (imp != null)
                {
                    lImpresa.Text = imp.CodiceERagioneSociale;

                    SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["Subappalti"];

                    foreach (Subappalto sub in subappalti)
                    {
                        if (sub.Appaltante != null)
                        {
                            if (sub.Appaltante.PartitaIva == imp.PartitaIva)
                            {
                                if (sub.DataInizioAttivitaAppaltatrice.HasValue)
                                    lPeriodo.Text = sub.DataInizioAttivitaAppaltatrice.Value.ToString("dd/MM/yyyy") + " - ";
                                if (sub.DataFineAttivitaAppaltatrice.HasValue)
                                    lPeriodo.Text = lPeriodo.Text +
                                                    sub.DataFineAttivitaAppaltatrice.Value.ToString("dd/MM/yyyy");

                                break;
                            }
                        }

                        if (sub.Appaltata != null)
                        {
                            if (sub.Appaltata.PartitaIva == imp.PartitaIva)
                            {
                                if (sub.DataInizioAttivitaAppaltata.HasValue)
                                    lPeriodo.Text = sub.DataInizioAttivitaAppaltata.Value.ToString("dd/MM/yyyy") + " - ";
                                if (sub.DataFineAttivitaAppaltata.HasValue)
                                    lPeriodo.Text = lPeriodo.Text +
                                                    sub.DataFineAttivitaAppaltata.Value.ToString("dd/MM/yyyy");

                                break;
                            }
                        }
                    }
                }
            }
        }

        protected void GridViewSubappalti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Subappalto subappalto = (Subappalto)e.Row.DataItem;
                Label lAtaRagioneSociale = (Label)e.Row.FindControl("LabelAtaRagioneSociale");
                Label lAtaPartitaIva = (Label)e.Row.FindControl("LabelAtaPartitaIva");
                Label lAtaCodiceFiscale = (Label)e.Row.FindControl("LabelAtaCodiceFiscale");
                Label lAtaIndirizzo = (Label)e.Row.FindControl("LabelAtaIndirizzo");
                Label lAtaArtigiano = (Label)e.Row.FindControl("LabelAtaArtigiano");

                Label lAnteRagioneSociale = (Label)e.Row.FindControl("LabelAnteRagioneSociale");
                Label lAntePartitaIva = (Label)e.Row.FindControl("LabelAntePartitaIva");
                Label lAnteCodiceFiscale = (Label)e.Row.FindControl("LabelAnteCodiceFiscale");
                Label lAnteIndirizzo = (Label)e.Row.FindControl("LabelAnteIndirizzo");
                Label lAnteArtigiano = (Label)e.Row.FindControl("LabelAnteArtigiano");

                // Appaltata
                lAtaRagioneSociale.Text = subappalto.NomeAppaltata;
                if (!String.IsNullOrEmpty(subappalto.PartitaIvaAppaltata))
                    lAtaPartitaIva.Text = String.Format(" P.Iva: {0}", subappalto.PartitaIvaAppaltata);
                if (!String.IsNullOrEmpty(subappalto.CodiceFiscaleAppaltata))
                    lAtaCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", subappalto.CodiceFiscaleAppaltata);
                lAtaIndirizzo.Text = subappalto.IndirizzoAppaltata;

                try
                {
                    if (subappalto.Appaltata.LavoratoreAutonomo)
                        lAtaArtigiano.Text = "Artigiano autonomo senza dipendenti";
                    else
                        lAtaArtigiano.Text = string.Empty;
                }
                catch
                {
                    lAtaArtigiano.Text = string.Empty;
                }

                // Appaltante
                lAnteRagioneSociale.Text = subappalto.NomeAppaltatrice;
                if (!String.IsNullOrEmpty(subappalto.PartitaIvaAppaltatrice))
                    lAntePartitaIva.Text = String.Format(" P.Iva: {0}", subappalto.PartitaIvaAppaltatrice);
                if (!String.IsNullOrEmpty(subappalto.CodiceFiscaleAppaltatrice))
                    lAnteCodiceFiscale.Text = String.Format(" Cod.fisc.: {0}", subappalto.CodiceFiscaleAppaltatrice);
                lAnteIndirizzo.Text = subappalto.IndirizzoAppaltatrice;

                try
                {
                    if (subappalto.Appaltante.LavoratoreAutonomo)
                        lAnteArtigiano.Text = "Artigiano autonomo senza dipendenti";
                    else
                        lAnteArtigiano.Text = string.Empty;
                }
                catch
                {
                    lAnteArtigiano.Text = string.Empty;
                }
            }
        }

        protected void GridViewSubappalti_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["Subappalti"];
            CaricaSubappalti(subappalti, e.NewPageIndex);
        }

        protected void ButtonMostraNascondiDatiGenerali_Click(object sender, EventArgs e)
        {
            bool mostraDatiGenerali = (bool)ViewState["MostraDatiGenerali"];
            ViewState["MostraDatiGenerali"] = !mostraDatiGenerali;

            GestisciBottoni();
        }

        protected void ButtonMostraNascondiAppalti_Click(object sender, EventArgs e)
        {
            bool mostraImprese = (bool)ViewState["MostraImprese"];
            ViewState["MostraImprese"] = !mostraImprese;

            GestisciBottoni();
        }

        protected void ButtonMostraNascondiImpresePeriodi_Click(object sender, EventArgs e)
        {
            bool mostraImpresePeriodi = (bool)ViewState["MostraImpresePeriodi"];
            ViewState["MostraImpresePeriodi"] = !mostraImpresePeriodi;

            GestisciBottoni();
        }

        protected void ButtonMostraNascondiLavoratori_Click(object sender, EventArgs e)
        {
            bool mostraLavoratori = (bool)ViewState["MostraLavoratori"];
            ViewState["MostraLavoratori"] = !mostraLavoratori;

            GestisciBottoni();
        }

        protected void GridViewAltrePersone_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                AltraPersona alt = (AltraPersona)e.Row.DataItem;
                Label lAltraPersona = (Label)e.Row.FindControl("LabelAltraPersona");
                Label lRuolo = (Label)e.Row.FindControl("LabelRuolo");

                if (alt != null)
                {
                    lAltraPersona.Text = string.Format("{0} {1}", alt.Cognome, alt.Nome);

                    switch (alt.TipoRuolo)
                    {
                        case TipologiaRuolo.ResponsabileLavoratori:
                            lRuolo.Text = "Responsabile lavoratori";
                            break;
                        case TipologiaRuolo.ResponsabileSicurezza:
                            lRuolo.Text = "Responsabile sicurezza";
                            break;
                        case TipologiaRuolo.ResponsabileLegale:
                            lRuolo.Text = "Responsabile legale impresa";
                            break;
                        case TipologiaRuolo.Titolare:
                            lRuolo.Text = "Titolare impresa";
                            break;
                        case TipologiaRuolo.Socio:
                            lRuolo.Text = "Socio impresa";
                            break;
                        default:
                            lRuolo.Text = "Altro";
                            break;
                    }
                }
            }
        }

        protected void GridViewReferenti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Referente alt = (Referente)e.Row.DataItem;
                Label lReferente = (Label)e.Row.FindControl("LabelReferente");
                Label lRuolo = (Label)e.Row.FindControl("LabelRuolo");
                Label lContatto = (Label)e.Row.FindControl("LabelContatto");

                if (alt != null)
                {
                    lReferente.Text = string.Format("{0} {1}", alt.Cognome, alt.Nome);

                    if (alt.ModalitaContatto)
                        lContatto.Text = alt.Telefono;
                    else
                        lContatto.Text = alt.Email;

                    switch (alt.TipoRuolo)
                    {
                        case TipologiaRuoloReferente.Cantiere:
                            lRuolo.Text = "Referente cantiere";
                            break;
                        case TipologiaRuoloReferente.Committente:
                            lRuolo.Text = "Referente committente";
                            break;
                        case TipologiaRuoloReferente.Impresa:
                            lRuolo.Text = "Referente impresa";
                            break;
                        default:
                            lRuolo.Text = "Altro";
                            break;
                    }
                }
            }
        }

        protected void ButtonMostraNascondiAltrePersone_Click(object sender, EventArgs e)
        {
            bool mostraAltrePersone = (bool)ViewState["MostraAltrePersone"];
            ViewState["MostraAltrePersone"] = !mostraAltrePersone;

            GestisciBottoni();
        }

        protected void ButtonMostraNascondiReferenti_Click(object sender, EventArgs e)
        {
            bool mostraReferenti = (bool)ViewState["MostraReferenti"];
            ViewState["MostraReferenti"] = !mostraReferenti;

            GestisciBottoni();
        }

        protected void ButtonMostraNascondiRilevatori_Click(object sender, EventArgs e)
        {
            bool mostraRilevatori = (bool)ViewState["MostraRilevatori"];
            ViewState["MostraRilevatori"] = !mostraRilevatori;

            GestisciBottoni();
        }

        protected void GridViewRilevatori_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RilevatoreCantiere rilevatoreCantiere = (RilevatoreCantiere)e.Row.DataItem;

                Label lCodice = (Label)e.Row.FindControl("LabelCodice0");
                Label lSocieta = (Label)e.Row.FindControl("LabelSocieta0");

                Label lPeriodo = (Label)e.Row.FindControl("LabelPeriodo0");

                lCodice.Text = string.Format("Codice: {0}", rilevatoreCantiere.CodiceRilevatore);
                lSocieta.Text = string.Format("Codice: {0}", rilevatoreCantiere.RagioneSociale);

                if (rilevatoreCantiere.DataInizio != null)
                    lPeriodo.Text = string.Format("Inizio periodo: {0}",
                                                  rilevatoreCantiere.DataInizio.Value.ToShortDateString());

                if (rilevatoreCantiere.DataFine.HasValue)
                {
                    lPeriodo.Text = lPeriodo.Text + " " + string.Format("Fine periodo: {0}",
                                                                        rilevatoreCantiere.DataFine.Value.
                                                                            ToShortDateString());
                }
            }
        }
    }
}