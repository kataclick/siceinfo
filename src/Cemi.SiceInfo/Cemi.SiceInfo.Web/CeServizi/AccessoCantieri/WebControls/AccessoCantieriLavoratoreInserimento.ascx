﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriLavoratoreInserimento.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriLavoratoreInserimento" %>
<asp:Panel ID="PanelDatiAggiornamento" runat="server" Width="220px" Visible="False">
    <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
    <asp:TextBox ID="TextBoxIdLavoratore" runat="server" Enabled="False" ReadOnly="True"
        Width="100px"></asp:TextBox></asp:Panel>
<table class="standardTable">
    <tr>
        <td>
            Cognome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxCognome"
                ErrorMessage="Digitare un cognome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNome"
                ErrorMessage="Digitare un nome" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data di nascita (gg/mm/aaaa)*
        </td>
        <%--<td>
            <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Digitare una data di nascita" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataNascita"
                ErrorMessage="Formato data errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>
        </td>--%>
        <td>
            <telerik:RadDatePicker ID="RadDateDataNascita" runat="server" Width="200px">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="RadDateDataNascita"
                ErrorMessage="Digitare una data di nascita" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
<%--     <tr>
        <td>
            Paese di nascita:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListLavoratorePaeseNascita" runat="server" Width="300px"
                AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListLavoratorePaeseNascita_SelectedIndexChanged"
                AutoPostBack="True" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPaeseNascita" runat="server"
                ErrorMessage="Paese di nascita mancante" ControlToValidate="DropDownListLavoratorePaeseNascita"
                ValidationGroup="legaleRappresentante">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidatorLuogoDiNascita" runat="server" ErrorMessage="Luogo di nascita mancante"
                OnServerValidate="CustomValidatorLuogoDiNascita_ServerValidate" ValidationGroup="legaleRappresentante">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Luogo di nascita:
        </td>
        <td colspan="2">
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Panel ID="PanelLuogoItalia" runat="server" Enabled="False">
                            <table class="standardTable">
                                <tr>
                                    <td colspan="3">
                                        Italiano
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Provincia
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListLavoratoreProvinciaNascita" runat="server" Width="250px"
                                            AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListLavoratoreProvinciaNascita_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Comune
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListLavoratoreComuneNascita" runat="server" Width="250px"
                                            AppendDataBoundItems="True" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelLuogoEstero" runat="server" Enabled="False">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        Estero
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DropDownListLavoratoreComuneNascitaEstero" runat="server" AppendDataBoundItems="true"
                                            Width="300px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>--%>
    <tr>
        <td>
            Codice fiscale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Digitare un codice fiscale" ValidationGroup="lavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Formato codice fiscale errato" ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$"
                ValidationGroup="lavoratore">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ErrorMessage="Codice fiscale non coerente con la data di nascita" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate"
                ValidationGroup="lavoratore">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                ValidationGroup="lavoratore" />
        </td>
    </tr>
</table>
* campi obbligatori