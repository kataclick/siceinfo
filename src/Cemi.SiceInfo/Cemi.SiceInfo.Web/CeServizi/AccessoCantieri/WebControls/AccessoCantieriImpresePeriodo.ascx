﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriImpresePeriodo.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriImpresePeriodo" %>
<table class="standardTable">
    <tr>
        <td>
            <br />
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco imprese"></asp:Label>
            <br />
            <asp:GridView ID="GridViewImprese" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewImprese_RowDataBound"
                Width="100%" OnRowCommand="GridViewImprese_RowCommand" DataKeyNames="PartitaIva">
                <Columns>
                    <asp:TemplateField HeaderText="Impresa">
                        <ItemTemplate>
                            <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Periodo">
                        <ItemTemplate>
                            <asp:Label ID="LabelPeriodo" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Modifica" Text="Modifica" ButtonType="Image" HeaderText="Modifica"
                        ImageUrl="../../images/edit.png">
                        <ItemStyle Width="10px" />
                    </asp:ButtonField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna impresa presente
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
            <br />
            <asp:Panel ID="PanelModifica" runat="server" Width="100%" Visible="False">
                <table class="filledtable">
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Modifica periodo attività impresa"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            Data Inizio attività (gg/mm/aaaa):
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDateDataInizioAttivitaModifica" runat="server" Width="200px">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorDataInizio" runat="server" ControlToValidate="RadDateDataInizioAttivitaModifica"
                                ErrorMessage="Periodo non corente con le date del cantiere." OnServerValidate="CustomValidatorDataInizio_ServerValidate"
                                ValidationGroup="stop">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data fine attività (gg/mm/aaaa):
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDateDataFineAttivitaModifica" runat="server" Width="200px">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorDataFine" runat="server" ControlToValidate="RadDateDataFineAttivitaModifica"
                                ErrorMessage="Periodo non corente con le date del cantiere." OnServerValidate="CustomValidatorDataFine_ServerValidate"
                                ValidationGroup="stop">*</asp:CustomValidator>
                            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToCompare="RadDateDataFineAttivitaModifica"
                                ControlToValidate="RadDateDataInizioAttivitaModifica" ErrorMessage="La data di inizio dell'attività non può essere superiore a quella di fine."
                                Operator="LessThan" Type="Date" ValidationGroup="stop" CssClass="messaggiErrore">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <asp:Panel ID="PanelBadge" runat="server" Visible="true" Width="100%"> 
                        <td colspan="3">
                            <b>Per la stampa del badge </b><small>(in mancanza di uno dei seguenti dati non sarà
                                possibile stampare il badge) </small>
                        </td>
                        </asp:Panel>
                    </tr>
                    <asp:Panel ID="PanelAutonomo" runat="server" Width="100%" Visible="False">
                        <tr>
                            <td>
                                Paese di nascita:
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListPaeseNascita" runat="server" AppendDataBoundItems="True"
                                    OnSelectedIndexChanged="DropDownListPaeseNascita_SelectedIndexChanged" AutoPostBack="True"
                                    Width="200px" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Provincia di nascita:
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListProvinciaNascita" runat="server" AppendDataBoundItems="True"
                                    Enabled="false" Width="200" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvinciaNascita_SelectedIndexChanged" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Comune di nascita:
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListComuneNascita" runat="server" AppendDataBoundItems="True"
                                    Enabled="false" Width="200" />
                            </td>
                            <td>
                                <asp:CustomValidator ID="CustomValidatorItaliano" runat="server" ValidationGroup="stop"
                                    ErrorMessage="Devono essere selezionate la provincia ed il comune di nascita"
                                    OnServerValidate="CustomValidatorItaliano_ServerValidate">
                                    *
                                </asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Committente:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCommittente" runat="server" Width="200px" Enabled="false"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                       <%-- <tr>
                            <td>
                                Data assunzione (gg\mm\aaaa):
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="RadDatePickerDataAssunzione" runat="server" Width="200px">
                                </telerik:RadDatePicker>
                            </td>
                            <td>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                Fotografia:
                            </td>
                            <td>
                                <telerik:RadUpload ID="RadUploadFoto" AllowedFileExtensions=".jpg" OnClientFileSelected="OnClientFileSelectedHandler"
                                    runat="server" MaxFileInputsCount="1" MaxFileSize="102400" ControlObjectsVisibility="None"
                                    InitialFileInputsCount="1" Width="200px">
                                </telerik:RadUpload>
                                <asp:CustomValidator ID="CustomValidatorFoto" runat="server" ErrorMessage="Possono essere importati solo file con estensione .jpg e con dimensione inferiore a 100kB"
                                    OnServerValidate="CustomValidatorFoto_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
                                <telerik:RadBinaryImage runat="server" ID="RadBinaryImageFoto" Height="60px" Width="55px"
                                    AutoAdjustImageControlSize="False" ResizeMode="Fit" ImageUrl="../../images/noImg.JPG" />
                            </td>
                            <td>
                                <asp:ImageButton ID="ButtonRemoveFoto" runat="server" CausesValidation="False" 
                                    OnClick="ButtonRemoveFoto_Click" ImageUrl="../../images/pallinoX.png" />
                            </td>
                        </tr>
                        <script type="text/javascript">


                            function OnClientFileSelectedHandler(sender, eventArgs) {
                                var input = eventArgs.get_fileInputField();
                                if (sender.isExtensionValid(input.value)) {
                                    var radImg = document.getElementById('<%= RadBinaryImageFoto.ClientID %>');
                                    if (radImg) {
                                        radImg.src = input.value;
                                        <%= PostBackString %>
                                    }
                                }
                            }
                                   

                        </script>
                    </asp:Panel>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="stop"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonModifica" runat="server" CausesValidation="True" Text="Salva"
                                ValidationGroup="stop" Width="225px" OnClick="ButtonModifica_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <br />
        </td>
    </tr>
</table>
