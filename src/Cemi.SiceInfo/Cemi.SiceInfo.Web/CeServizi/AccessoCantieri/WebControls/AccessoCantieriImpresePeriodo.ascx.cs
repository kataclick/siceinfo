﻿using System;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;
using TBridge.Cemi.Business;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class AccessoCantieriImpresePeriodo : System.Web.UI.UserControl
    {
        //private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();
        private readonly Common _bizCommon = new Common();
        protected string PostBackString;

        protected void Page_Load(object sender, EventArgs e)
        {
            PostBackString = Page.ClientScript.GetPostBackEventReference(this, null);

            if (!Page.IsPostBack)
                CaricaCombo();
            else
            {
                if (RadUploadFoto.UploadedFiles.Count > 0)
                {
                    byte[] bytes = new byte[RadUploadFoto.UploadedFiles[0].ContentLength];
                    RadUploadFoto.UploadedFiles[0].InputStream.Read(bytes, 0, (int) RadUploadFoto.UploadedFiles[0].ContentLength);

                    ViewState["FotoMod"] = bytes;

                    RadBinaryImageFoto.DataValue = bytes;
                    RadBinaryImageFoto.DataBind();
                }
            }
        }

        private static int IndiceComuneInDropDown(DropDownList dropDownListComuni, string codiceCatastale)
        {
            int res = 0;

            for (int i = 0; i < dropDownListComuni.Items.Count; i++)
            {
                string valoreCombo = dropDownListComuni.Items[i].Value;
                string[] splitted = valoreCombo.Split('|');
                if (splitted.Length > 1)
                {
                    if (splitted[0] == codiceCatastale)
                    {
                        res = i;
                        break;
                    }
                }
            }

            return res;
        }

        private void CaricaCombo()
        {
            // Province
            StringCollection province = _bizCommon.GetProvinceSiceNew();

            DropDownListProvinciaNascita.Items.Clear();
            DropDownListProvinciaNascita.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListProvinciaNascita.DataSource = province;
            DropDownListProvinciaNascita.DataBind();

            // Paese
            ListDictionary nazioni = _bizCommon.GetNazioni();

            DropDownListPaeseNascita.Items.Clear();
            DropDownListPaeseNascita.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListPaeseNascita.DataSource = nazioni;
            DropDownListPaeseNascita.DataTextField = "Value";
            DropDownListPaeseNascita.DataValueField = "Key";
            DropDownListPaeseNascita.DataBind();
        }

        private void CaricaGrigliaImprese()
        {
            ImpresaCollection impColl = new ImpresaCollection();
            SubappaltoCollection subColl = (SubappaltoCollection)ViewState["subappalti"];

            if (subColl != null)
            {
                foreach (Subappalto sub in subColl)
                {
                    if (sub.Appaltante != null)
                    {
                        if (!impColl.Contains(sub.Appaltante)) impColl.Add(sub.Appaltante);
                    }

                    if (sub.Appaltata != null)
                    {
                        if (!impColl.Contains(sub.Appaltata)) impColl.Add(sub.Appaltata);
                    }
                }
            }

            GridViewImprese.DataSource = impColl;
            GridViewImprese.DataBind();
        }

        public void CaricaImprese(SubappaltoCollection subappalti)
        {
            ViewState["subappalti"] = subappalti;
            CaricaGrigliaImprese();
        }

        protected void GridViewImprese_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Impresa imp = (Impresa)e.Row.DataItem;
                Label lImpresa = (Label)e.Row.FindControl("LabelImpresa");
                Label lPeriodo = (Label)e.Row.FindControl("LabelPeriodo");
                //Label lAutorizzazioneAlSubappalto = (Label) e.Row.FindControl("LabelAutorizzazioneAlSubappalto");

                if (imp != null)
                {
                    lImpresa.Text = imp.CodiceERagioneSociale;

                    SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];

                    foreach (Subappalto sub in subappalti)
                    {
                        if (sub.Appaltante != null)
                        {
                            if (sub.Appaltante.PartitaIva == imp.PartitaIva)
                            {
                                if (sub.DataInizioAttivitaAppaltatrice.HasValue)
                                    lPeriodo.Text = sub.DataInizioAttivitaAppaltatrice.Value.ToString("dd/MM/yyyy") + " - ";
                                if (sub.DataFineAttivitaAppaltatrice.HasValue)
                                    lPeriodo.Text = lPeriodo.Text +
                                                    sub.DataFineAttivitaAppaltatrice.Value.ToString("dd/MM/yyyy");

                                //lAutorizzazioneAlSubappalto.Text = sub.AutorizzazioneAlSubappaltoAppaltatrice;

                                break;
                            }
                        }

                        if (sub.Appaltata != null)
                        {
                            if (sub.Appaltata.PartitaIva == imp.PartitaIva)
                            {
                                if (sub.DataInizioAttivitaAppaltata.HasValue)
                                    lPeriodo.Text = sub.DataInizioAttivitaAppaltata.Value.ToString("dd/MM/yyyy") + " - ";
                                if (sub.DataFineAttivitaAppaltata.HasValue)
                                    lPeriodo.Text = lPeriodo.Text +
                                                    sub.DataFineAttivitaAppaltata.Value.ToString("dd/MM/yyyy");

                                break;
                            }
                        }
                    }
                }
            }
        }


        protected void ButtonModifica_Click(object sender, EventArgs e)
        {
            Page.Validate("stop");

            if (Page.IsValid)
            {
                SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];
                Impresa impSel = (Impresa)ViewState["impSel"];

                foreach (Subappalto sub in subappalti)
                {
                    if (sub.Appaltante != null)
                    {
                        if (sub.Appaltante.PartitaIva == impSel.PartitaIva)
                        {
                            sub.DataFineAttivitaAppaltatrice = RadDateDataFineAttivitaModifica.SelectedDate;
                            sub.DataInizioAttivitaAppaltatrice = RadDateDataInizioAttivitaModifica.SelectedDate;

                            if (sub.Appaltante.LavoratoreAutonomo)
                            {
                                if (DropDownListPaeseNascita.SelectedValue != null)
                                {
                                    if (DropDownListPaeseNascita.SelectedValue == "1")
                                    {
                                        if (DropDownListProvinciaNascita.SelectedValue != null)
                                        {
                                            sub.Appaltante.ProvinciaNascita =
                                                DropDownListProvinciaNascita.SelectedValue;
                                        }

                                        if (DropDownListComuneNascita.SelectedValue != null)
                                        {
                                            sub.Appaltante.LuogoNascita =
                                                AccessoCantieriBusiness.GetCodiceCatastaleDaCombo(
                                                    DropDownListComuneNascita.SelectedValue);
                                        }
                                    }

                                    sub.Appaltante.PaeseNascita = DropDownListPaeseNascita.SelectedValue;
                                }
                                else
                                {
                                    sub.Appaltante.LuogoNascita = null;
                                    sub.Appaltante.PaeseNascita = null;
                                    sub.Appaltante.ProvinciaNascita = null;
                                }


                                //if (RadDatePickerDataAssunzione.SelectedDate != null)
                                //    sub.Appaltante.DataAssunzione = RadDatePickerDataAssunzione.SelectedDate.Value;
                                sub.Appaltante.Committente = Presenter.NormalizzaCampoTesto(TextBoxCommittente.Text);

                                if (RadUploadFoto.UploadedFiles.Count > 0)
                                {
                                    byte[] bytes = new byte[RadUploadFoto.UploadedFiles[0].ContentLength];
                                    RadUploadFoto.UploadedFiles[0].InputStream.Read(bytes, 0,
                                                                                    (int) RadUploadFoto.UploadedFiles[0].
                                                                                        ContentLength);

                                    ViewState["Foto"] = bytes;

                                    RadBinaryImageFoto.DataValue = bytes;
                                    RadBinaryImageFoto.DataBind();
                                }

                                if (ViewState["FotoMod"] != null)
                                {
                                    ViewState["Foto"] = ViewState["FotoMod"];
                                    RadBinaryImageFoto.DataValue = (byte[])ViewState["Foto"];
                                    RadBinaryImageFoto.DataBind();
                                }

                                sub.Appaltante.Foto = (byte[])ViewState["Foto"];
                            }
                        }
                    }

                    if (sub.Appaltata != null)
                    {
                        if (sub.Appaltata.PartitaIva == impSel.PartitaIva)
                        {
                            sub.DataFineAttivitaAppaltata = RadDateDataFineAttivitaModifica.SelectedDate;
                            sub.DataInizioAttivitaAppaltata = RadDateDataInizioAttivitaModifica.SelectedDate;

                            //sub.AutorizzazioneAlSubappaltoAppaltata = TextBoxAutorizzazioneAlSubappalto.Text;

                            if (sub.Appaltata.LavoratoreAutonomo)
                            {
                                if (DropDownListPaeseNascita.SelectedValue != null)
                                {
                                    if (DropDownListPaeseNascita.SelectedValue == "1")
                                    {
                                        if (DropDownListProvinciaNascita.SelectedValue != null)
                                        {
                                            sub.Appaltata.ProvinciaNascita =
                                                DropDownListProvinciaNascita.SelectedValue;
                                        }

                                        if (DropDownListComuneNascita.SelectedValue != null)
                                        {
                                            sub.Appaltata.LuogoNascita =
                                                AccessoCantieriBusiness.GetCodiceCatastaleDaCombo(
                                                    DropDownListComuneNascita.SelectedValue);
                                        }
                                    }

                                    sub.Appaltata.PaeseNascita = DropDownListPaeseNascita.SelectedValue;
                                }
                                else
                                {
                                    sub.Appaltata.LuogoNascita = null;
                                    sub.Appaltata.PaeseNascita = null;
                                    sub.Appaltata.ProvinciaNascita = null;
                                }


                                //if (RadDatePickerDataAssunzione.SelectedDate != null)
                                //    sub.Appaltata.DataAssunzione = RadDatePickerDataAssunzione.SelectedDate.Value;
                                sub.Appaltata.Committente = Presenter.NormalizzaCampoTesto(TextBoxCommittente.Text);

                                if (RadUploadFoto.UploadedFiles.Count > 0)
                                {
                                    byte[] bytes = new byte[RadUploadFoto.UploadedFiles[0].ContentLength];
                                    RadUploadFoto.UploadedFiles[0].InputStream.Read(bytes, 0,
                                                                                    (int) RadUploadFoto.UploadedFiles[0].
                                                                                        ContentLength);

                                    ViewState["Foto"] = bytes;

                                    RadBinaryImageFoto.DataValue = bytes;
                                    RadBinaryImageFoto.DataBind();
                                }

                                if (ViewState["FotoMod"] != null)
                                {
                                    ViewState["Foto"] = ViewState["FotoMod"];
                                    RadBinaryImageFoto.DataValue = (byte[])ViewState["Foto"];
                                    RadBinaryImageFoto.DataBind();
                                }

                                sub.Appaltata.Foto = (byte[])ViewState["Foto"];
                            }
                        }
                    }
                }

                ViewState["subappalti"] = subappalti;
                CaricaGrigliaImprese();

                ViewState["impSel"] = null;
                PanelModifica.Visible = false;
            }
        }

        protected void CustomValidatorDataInizio_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime? dataInizio = null;

            if (RadDateDataInizioAttivitaModifica.SelectedDate.HasValue)
                dataInizio = RadDateDataInizioAttivitaModifica.SelectedDate.Value;

            DateTime dataInizioCantiere = DateTime.Parse(ViewState["dataInizio"].ToString());

            if (dataInizio.HasValue)
            {
                if ((dataInizio < dataInizioCantiere))
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
        }

        protected void CustomValidatorDataFine_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime? dataFine = null;

            if (RadDateDataFineAttivitaModifica.SelectedDate.HasValue)
                dataFine = RadDateDataFineAttivitaModifica.SelectedDate.Value;

            DateTime dataFineCantiere = DateTime.Parse(ViewState["dataFine"].ToString());

            if (dataFine.HasValue)
            {
                if ((dataFine > dataFineCantiere))
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
        }

        public SubappaltoCollection GetSubappalti()
        {
            return (SubappaltoCollection)ViewState["subappalti"];
        }

        protected void ButtonRemoveFoto_Click(object sender, EventArgs e)
        {
            ViewState["Foto"] = null;

            RadBinaryImageFoto.ImageUrl = "~/CeServizi/images/noImg.JPG";
            RadBinaryImageFoto.DataBind();
        }

        protected void GridViewImprese_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modifica")
            {
                ResetImpresa();
                Int32 index = Int32.Parse(e.CommandArgument.ToString());

                string pIvaSel = GridViewImprese.DataKeys[index].Value.ToString();

                Impresa impSel = null;

                SubappaltoCollection subappalti = (SubappaltoCollection)ViewState["subappalti"];

                foreach (Subappalto sub in subappalti)
                {
                    if (sub.Appaltante != null)
                    {
                        if (sub.PartitaIvaAppaltatrice == pIvaSel)
                        {
                            impSel = sub.Appaltante;
                            break;
                        }
                    }

                    if (sub.Appaltata != null)
                    {
                        if (sub.PartitaIvaAppaltata == pIvaSel)
                        {
                            impSel = sub.Appaltata;
                            break;
                        }
                    }
                }

                ViewState["impSel"] = impSel;

                if (impSel != null)
                    PanelBadge.Visible = impSel.TipoImpresa != TipologiaImpresa.SiceNew;

                foreach (Subappalto sub in subappalti)
                {
                    if (sub.Appaltante != null)
                    {
                        if (sub.Appaltante == impSel)
                        {
                            if (sub.DataFineAttivitaAppaltatrice.HasValue)
                                RadDateDataFineAttivitaModifica.SelectedDate = sub.DataFineAttivitaAppaltatrice.Value;
                            if (sub.DataInizioAttivitaAppaltatrice.HasValue)
                                RadDateDataInizioAttivitaModifica.SelectedDate = sub.DataInizioAttivitaAppaltatrice.Value;

                            if (sub.Appaltante.LavoratoreAutonomo)
                            {
                                if (!string.IsNullOrEmpty(sub.Appaltante.PaeseNascita))
                                {
                                    DropDownListPaeseNascita.SelectedValue = sub.Appaltante.PaeseNascita;

                                    if (DropDownListPaeseNascita.SelectedValue == "1")
                                    {
                                        // ITALIA
                                        if (!string.IsNullOrEmpty(sub.Appaltante.ProvinciaNascita) &&
                                            DropDownListProvinciaNascita.Items.FindByValue(sub.Appaltante.ProvinciaNascita) !=
                                            null)
                                        {
                                            DropDownListProvinciaNascita.SelectedValue = sub.Appaltante.ProvinciaNascita;
                                            AccessoCantieriBusiness.CaricaComuni(DropDownListComuneNascita,
                                                                                 sub.Appaltante.ProvinciaNascita,
                                                                                 null);
                                        }
                                        if (!string.IsNullOrEmpty(sub.Appaltante.LuogoNascita))
                                            DropDownListComuneNascita.SelectedIndex =
                                                IndiceComuneInDropDown(DropDownListComuneNascita,
                                                                       sub.Appaltante.LuogoNascita);

                                        DropDownListPaeseNascita.Enabled = true;

                                        DropDownListProvinciaNascita.Enabled = true;
                                        DropDownListComuneNascita.Enabled = true;
                                    }
                                    else
                                    {
                                        DropDownListPaeseNascita.Enabled = true;

                                        DropDownListProvinciaNascita.Enabled = false;
                                        DropDownListComuneNascita.Enabled = false;
                                    }
                                }

                                TextBoxCommittente.Text = sub.Appaltata.RagioneSociale;

                                //if (sub.Appaltante.DataAssunzione.HasValue)
                                //    RadDatePickerDataAssunzione.SelectedDate = sub.Appaltante.DataAssunzione.Value;
                            }
                            if (sub.Appaltante.Foto != null)
                            {
                                ViewState["Foto"] = sub.Appaltante.Foto;

                                RadBinaryImageFoto.DataValue = sub.Appaltante.Foto;
                                RadBinaryImageFoto.DataBind();
                            }
                            else
                            {
                                ViewState["Foto"] = null;

                                RadBinaryImageFoto.ImageUrl = "~/CeServizi/images/noImg.JPG";
                                RadBinaryImageFoto.DataBind();
                            }

                            break;
                        }
                    }

                    if (sub.Appaltata != null)
                    {
                        if (sub.Appaltata == impSel)
                        {
                            if (sub.DataFineAttivitaAppaltata.HasValue)
                                RadDateDataFineAttivitaModifica.SelectedDate = sub.DataFineAttivitaAppaltata.Value;
                            if (sub.DataInizioAttivitaAppaltata.HasValue)
                                RadDateDataInizioAttivitaModifica.SelectedDate = sub.DataInizioAttivitaAppaltata.Value;

                            if (sub.Appaltata.LavoratoreAutonomo)
                            {
                                if (!string.IsNullOrEmpty(sub.Appaltata.PaeseNascita))
                                {
                                    DropDownListPaeseNascita.SelectedValue = sub.Appaltata.PaeseNascita;

                                    if (DropDownListPaeseNascita.SelectedValue == "1")
                                    {
                                        // ITALIA
                                        if (!string.IsNullOrEmpty(sub.Appaltata.ProvinciaNascita) &&
                                            DropDownListProvinciaNascita.Items.FindByValue(sub.Appaltata.ProvinciaNascita) !=
                                            null)
                                        {
                                            DropDownListProvinciaNascita.SelectedValue = sub.Appaltata.ProvinciaNascita;
                                            AccessoCantieriBusiness.CaricaComuni(DropDownListComuneNascita,
                                                                                 sub.Appaltata.ProvinciaNascita, null);
                                        }
                                        if (!string.IsNullOrEmpty(sub.Appaltata.LuogoNascita))
                                            DropDownListComuneNascita.SelectedIndex =
                                                IndiceComuneInDropDown(DropDownListComuneNascita,
                                                                       sub.Appaltata.LuogoNascita);

                                        DropDownListPaeseNascita.Enabled = true;

                                        DropDownListProvinciaNascita.Enabled = true;
                                        DropDownListComuneNascita.Enabled = true;
                                    }
                                    else
                                    {
                                        DropDownListPaeseNascita.Enabled = true;

                                        DropDownListProvinciaNascita.Enabled = false;
                                        DropDownListComuneNascita.Enabled = false;
                                    }
                                }

                                //if (sub.Appaltata.DataAssunzione.HasValue)
                                //    RadDatePickerDataAssunzione.SelectedDate = sub.Appaltata.DataAssunzione.Value;
                            }
                            if (sub.Appaltata.Foto != null)
                            {
                                ViewState["Foto"] = sub.Appaltata.Foto;

                                RadBinaryImageFoto.DataValue = sub.Appaltata.Foto;
                                RadBinaryImageFoto.DataBind();
                            }
                            else
                            {
                                ViewState["Foto"] = null;

                                RadBinaryImageFoto.ImageUrl = "~/CeServizi/images/noImg.JPG";
                                RadBinaryImageFoto.DataBind();
                            }


                            break;
                        }
                    }
                }

                PanelModifica.Visible = true;

                PanelAutonomo.Visible = impSel.LavoratoreAutonomo;
            }
        }

        private void ResetImpresa()
        {
            RadDateDataInizioAttivitaModifica.SelectedDate = null;
            RadDateDataFineAttivitaModifica.SelectedDate = null;

            DropDownListPaeseNascita.SelectedIndex = 0;
            DropDownListProvinciaNascita.Enabled = false;
            DropDownListProvinciaNascita.SelectedIndex = 0;
            DropDownListComuneNascita.Enabled = false;
            Presenter.CaricaComuni(DropDownListComuneNascita, null);
            TextBoxCommittente.Text = null;
            //RadDatePickerDataAssunzione.SelectedDate = null;

            ViewState["Foto"] = null;
            ViewState["FotoMod"] = null;
        }

        protected void DropDownListPaeseNascita_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Se Italia attivo ricerca province, comuni,...
            // altrimenti visualizzo solo i comuni e prendo quelli internazionali
            if (DropDownListPaeseNascita.SelectedValue == "1")
            {
                DropDownListProvinciaNascita.Enabled = true;
                DropDownListComuneNascita.Enabled = true;
            }
            else
            {
                DropDownListComuneNascita.ClearSelection();
                DropDownListProvinciaNascita.ClearSelection();

                DropDownListProvinciaNascita.Enabled = false;
                DropDownListComuneNascita.Enabled = false;
            }
        }

        protected void DropDownListProvinciaNascita_SelectedIndexChanged(object sender, EventArgs e)
        {
            AccessoCantieriBusiness.CaricaComuni(DropDownListComuneNascita, DropDownListProvinciaNascita.SelectedValue, null);
        }

        protected void CustomValidatorFoto_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = RadUploadFoto.InvalidFiles.Count == 0;
        }

        protected void CustomValidatorItaliano_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (DropDownListPaeseNascita.SelectedValue == "1"
                && (String.IsNullOrEmpty(DropDownListProvinciaNascita.SelectedValue)
                    || String.IsNullOrEmpty(DropDownListComuneNascita.SelectedValue)))
            {
                args.IsValid = false;
            }
        }

        public void CaricaDataInizio(DateTime? dataInizio)
        {
            ViewState["dataInizio"] = dataInizio;
        }

        public void CaricaDataFine(DateTime? dataFine)
        {
            ViewState["dataFine"] = dataFine;
        }
    }
}