﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RicercaCommittenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.RicercaCommittenti" %>

<style type="text/css">
    .style1
    {
        width: 80px;
    }
    .style2
    {
        width: 120px;
    }
</style>

<asp:Panel ID="PanelRicercaCantieri" runat="server" DefaultButton="ButtonVisualizza" Width="100%">
<table class="filledtable">
    <tr>
        <td style="height: 16px">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Committenti"></asp:Label>
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            Cognome
        </td>
        <td>
            Nome
        </td>
        <td>
            Ragione Sociale
        </td>
        <td>
            Codice Fiscale
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
        </td>
        <td align="right" colspan="3">
            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" ValidationGroup="ricercaCommittente"
                OnClick="ButtonVisualizza_Click" />
        </td>
    </tr>
</table>
<br />
<telerik:RadGrid ID="RadGridCommittenti" runat="server" AllowPaging="True" PageSize="5"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Visible="False" 
        onitemcommand="RadGridCommittenti_ItemCommand" 
        onitemdatabound="RadGridCommittenti_ItemDataBound" 
        onpageindexchanged="RadGridCommittenti_PageIndexChanged">
    <ClientSettings>
        <Selecting CellSelectionMode="None" />
        <Selecting CellSelectionMode="None" />
    </ClientSettings>
    <MasterTableView DataKeyNames="IdCommittente">
        <CommandItemSettings ExportToPdfText="Export to PDF" />
        <CommandItemSettings ExportToPdfText="Export to PDF" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridTemplateColumn FilterControlAltText="Filter Persona column" 
                HeaderText="Persona" UniqueName="Persona">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td class="style1">
                                Cognome:
                            </td>
                            <td>
                                <b>
                                <asp:Label ID="LabelCognome" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Nome:
                            </td>
                            <td>
                                <b>
                                <asp:Label ID="LabelNome" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="200px" VerticalAlign="Top" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn FilterControlAltText="Filter Aziena column" 
                HeaderText="Ente/Azienda" UniqueName="Azienda">
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td class="style2">
                                Ragione Sociale:
                            </td>
                            <td>
                                <b>
                                <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Codice Fiscale:
                            </td>
                            <td>
                                <asp:Label ID="LabelCodiceFiscale" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Partita IVA:
                            </td>
                            <td>
                                <asp:Label ID="LabelPartitaIva" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle VerticalAlign="Top" />
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn>
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td align="center">
                                <asp:ImageButton ID="ImageButtonSeleziona" runat="server" 
                                    CommandName="SELEZIONA" ImageUrl="../../images/lente.png" ToolTip="Seleziona" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <small>Seleziona </small>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </telerik:GridTemplateColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
</asp:Panel>