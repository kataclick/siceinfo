﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Delegates;
using TBridge.Cemi.Type.Filters;
using TBridge.Cemi.Type.Domain;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class RicercaCommittenti : System.Web.UI.UserControl
    {
        private readonly CommittentiManager _biz = new CommittentiManager();
        public event CommittenteSelectedEventHandler OnCommittenteSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaCommittenti(0);
                RadGridCommittenti.Visible = true;
            }
        }

        private void CaricaCommittenti(Int32 pagina)
        {
            CommittenteFilter filtro = CreaFiltro();
            List<Committente> committenti = _biz.GetCommittenti(filtro);

            Presenter.CaricaElementiInGridView(
                RadGridCommittenti,
                committenti);
        }

        private CommittenteFilter CreaFiltro()
        {
            CommittenteFilter filtro = new CommittenteFilter
            {
                Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text),
                Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text),
                RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text),
                CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text)
            };

            return filtro;
        }

        protected void RadGridCommittenti_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Committente committente = (Committente)e.Item.DataItem;

                Label lCognome = (Label)e.Item.FindControl("LabelCognome");
                Label lNome = (Label)e.Item.FindControl("LabelNome");
                Label lRagioneSociale = (Label)e.Item.FindControl("LabelRagioneSociale");
                Label lCodiceFiscale = (Label)e.Item.FindControl("LabelCodiceFiscale");
                Label lPartitaIva = (Label)e.Item.FindControl("LabelPartitaIva");

                lCognome.Text = committente.Cognome;
                lNome.Text = committente.Nome;
                lRagioneSociale.Text = committente.RagioneSociale;
                lCodiceFiscale.Text = committente.CodiceFiscale;
                lPartitaIva.Text = committente.PartitaIva;
            }
        }

        protected void RadGridCommittenti_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaCommittenti(0);
        }

        protected void RadGridCommittenti_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SELEZIONA":
                    Int32 idCommittente = (Int32)e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdCommittente"];

                    if (OnCommittenteSelected != null)
                    {
                        Committente committente = _biz.GetCommittente(idCommittente);
                        OnCommittenteSelected(committente);
                    }
                    break;
            }
        }
    }
}