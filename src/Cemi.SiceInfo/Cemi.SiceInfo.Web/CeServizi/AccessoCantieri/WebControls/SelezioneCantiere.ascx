﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelezioneCantiere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.SelezioneCantiere" %>

<%@ Register Src="RicercaCantieri.ascx" TagName="RicercaCantieri" TagPrefix="uc3" %>
<%@ Register Src="DatiSinteticiCantiere.ascx" TagName="DatiSinteticiCantiere"
    TagPrefix="uc4" %>

<asp:Panel ID="PanelRicerca" runat="server" Visible="true" Width="100%">
    <uc3:RicercaCantieri ID="RicercaCantieri1" runat="server" />
</asp:Panel>
<asp:Panel ID="PanelCantiere" runat="server" Visible="false" Width="100%">
    <uc4:DatiSinteticiCantiere ID="DatiSinteticiCantiere1" runat="server" />
    <asp:Button ID="ButtonAltroCantiere" runat="server" Width="150px" Text="Cambia cantiere"
        OnClick="ButtonAltroCantiere_Click" />
</asp:Panel>