﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriImpreseSubappalti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriImpreseSubappalti" %>
<asp:Panel ID="PanelInserimentoImpresa" runat="server" Visible="true" Width="100%">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
         <%--<Triggers>
            <asp:PostBackTrigger ControlID="ButtonSave" />
        </Triggers>--%>
        <ContentTemplate>
            <asp:Panel ID="PanelCodiceImpresa" runat="server" Visible="False" Width="100%">
                <asp:Label ID="LabelIdCantiere" runat="server" Text="Codice" Width="100px"></asp:Label>
                <asp:TextBox ID="TextBoxIdImpresa" runat="server" Enabled="False" ReadOnly="True"
                    Width="100px"></asp:TextBox></asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Width="100%">
                <table class="standardTable">
                    <tr>
                        <td>
                            Ragione sociale<b>*</b>:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRagSoc" runat="server" ControlToValidate="TextBoxRagioneSociale"
                                ErrorMessage="Digitare una ragione sociale" ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice fiscale<b>*</b>:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="200px"></asp:TextBox>
                        </td>
                        <td>

                            <%--ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$|^\d{11}$"--%>

                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodFisc" runat="server"
                                ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Codice fiscale errato"
                                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]$"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RegularExpressionValidator>--%>

                            <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ErrorMessage="Codice fiscale non valido"
                                ValidationGroup="ValidaInserimentoImpresa" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate">*</asp:CustomValidator>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                                ControlToValidate="TextBoxCodiceFiscale" ErrorMessage="Digitare un codice fiscale"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Partita IVA<b>*</b>:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxPartitaIva" runat="server" MaxLength="11" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPIVA" runat="server"
                                ControlToValidate="TextBoxPartitaIva" ErrorMessage="Partita IVA errata" ValidationExpression="^\d{11}$"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPartitaIVA" runat="server"
                                ControlToValidate="TextBoxPartitaIva" ErrorMessage="Digitare una partita IVA"
                                ValidationGroup="ValidaInserimentoImpresa">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorPartitaIva" runat="server" ErrorMessage="Partita IVA non valida"
                                ValidationGroup="ValidaInserimentoImpresa" OnServerValidate="CustomValidatorPartitaIva_ServerValidate">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tipologia contratto (* se artigiano):
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipologiaContratto" runat="server" Width="200px"
                                AppendDataBoundItems="True">
                            </asp:DropDownList>
                        </td>
                         <td>
                            <asp:CustomValidator ID="CustomValidatorTipologiaContratto" runat="server" ErrorMessage="Se è stato specificato il lavoratore autonomo è necessario inserire la tipologia di contratto"
                                OnServerValidate="CustomValidatorTipologiaContratto_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Attività:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipoAttivita" runat="server" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Indirizzo:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIndirizzo" runat="server" Height="41px" MaxLength="255" TextMode="MultiLine"
                                Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Provincia:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListProvincia" runat="server" AppendDataBoundItems="True"
                                AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvincia_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListComuni" runat="server" AppendDataBoundItems="True"
                                AutoPostBack="True" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                                Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            CAP:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListCAP" runat="server" AppendDataBoundItems="True"
                                Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
                <table class="borderedTable">
                    <tr>
                        <td>
                            <asp:CheckBox ID="CheckBoxArtigiano" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxArtigiano_CheckedChanged"
                                Text="Artigiano autonomo senza dipendenti" />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" CssClass="messaggiErrore" Text="[Può essere inserito solo come impresa subappaltata]"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Informazioni di base </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cognome<b>*</b>:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" CssClass="campoDisabilitato" Enabled="False"
                                MaxLength="30" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorCognome" runat="server" ErrorMessage="Digitare un cognome per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorCognome_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nome<b>*</b>:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" CssClass="campoDisabilitato" Enabled="False"
                                MaxLength="30" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorNome" runat="server" ErrorMessage="Digitare un nome per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorNome_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Data di nascita (gg\mm\aaaa)<b>*</b>:
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDateDataNascita" runat="server" Width="200px" CssClass="campoDisabilitato"
                                Enabled="False">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorDataNascita" runat="server" ErrorMessage="Digitare una data di nascita per l'artigiano autonomo"
                                OnServerValidate="CustomValidatorDataNascita_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            N° posiz. Camera di Commercio:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNumeroCameraCommercio" runat="server" CssClass="campoDisabilitato"
                                Enabled="False" MaxLength="10" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Per la stampa del badge </b><small>(in mancanza di uno dei seguenti dati non sarà
                                possibile stampare il badge) </small>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Paese di nascita:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListPaeseNascita" runat="server" AppendDataBoundItems="True"
                                OnSelectedIndexChanged="DropDownListPaeseNascita_SelectedIndexChanged" AutoPostBack="True"
                                Width="200px" Enabled="False" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <small>Se viene selezionato "Italia" occorre indicare provincia e comune di nascita
                            </small>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Provincia di nascita:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListProvinciaNascita" runat="server" AppendDataBoundItems="True"
                                Enabled="false" Width="200" AutoPostBack="True" OnSelectedIndexChanged="DropDownListProvinciaNascita_SelectedIndexChanged" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune di nascita:
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListComuneNascita" runat="server" AppendDataBoundItems="True"
                                Enabled="false" Width="200" />
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorItaliano" runat="server" ValidationGroup="ValidaInserimentoImpresa"
                                ErrorMessage="Devono essere selezionate la provincia ed il comune di nascita"
                                OnServerValidate="CustomValidatorItaliano_ServerValidate">
                                *
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Committente:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCommittente" runat="server" CssClass="campoDisabilitato"
                                Enabled="False" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <%--<asp:Panel ID="PanelAssunzione" runat="server">--%>
                   <%-- <tr>
                        <td>
                            Data assunzione (gg\mm\aaaa):
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDatePickerDataAssunzione" runat="server" Width="200px"
                                CssClass="campoDisabilitato" Enabled="False">
                            </telerik:RadDatePicker>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>--%>
                    <%--</asp:Panel>--%>
                    <tr>
                        <td>
                            Fotografia:
                        </td>
                        <td>
                            <telerik:RadUpload ID="RadUploadFoto" AllowedFileExtensions=".jpg" OnClientFileSelected="OnClientFileSelectedHandler"
                                runat="server" MaxFileInputsCount="1" MaxFileSize="102400" ControlObjectsVisibility="None"
                                Width="200px" Enabled="False">
                                <Localization Add="Aggiungi foto" Select="Seleziona" />
                            </telerik:RadUpload>
                            <asp:CustomValidator ID="CustomValidatorFoto" runat="server" ErrorMessage="Possono essere importati solo file con estensione .jpg e con dimensione inferiore a 100kB"
                                OnServerValidate="CustomValidatorFoto_ServerValidate" ValidationGroup="ValidaInserimentoImpresa">*</asp:CustomValidator>
                            <telerik:RadBinaryImage runat="server" ID="RadBinaryImageFoto" Height="60px" Width="55px"
                                AutoAdjustImageControlSize="False" ResizeMode="Fit" ImageUrl="../../images/noImg.JPG" />
                            <asp:ImageButton ID="ButtonRemoveFoto" runat="server" CausesValidation="False" Text="Elimina foto"
                                OnClick="ButtonRemoveFoto_Click" ImageUrl="../../images/pallinoX.png" />
                        </td>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <script type="text/javascript">

                        function OnClientFileSelectedHandler(sender, eventArgs) {
                            var input = eventArgs.get_fileInputField();
                            if (sender.isExtensionValid(input.value)) {
                                var radImg = document.getElementById('<%= RadBinaryImageFoto.ClientID %>');
                                if (radImg) {
                                    radImg.src = input.value;
                                     <%= PostBackString %>
                                }
                            }
                        }
                                
                    </script>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="ValidaInserimentoImpresa"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <b>*</b> campi obbligatori
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
