﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriLavoratoriTutteImprese.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriLavoratoriTutteImprese" %>
<asp:GridView ID="GridViewImpreseLavoratori" runat="server" Width="100%" AutoGenerateColumns="False"
    AllowPaging="True" DataKeyNames="Impresa" OnPageIndexChanging="GridViewImpreseLavoratori_PageIndexChanging"
    OnRowDataBound="GridViewImpreseLavoratori_RowDataBound" PageSize="5">
    <Columns>
        <asp:BoundField DataField="Impresa" HeaderText="Impresa">
            <ItemStyle Width="150px" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="Lavoratori">
            <ItemTemplate>
                <asp:GridView ID="GridViewLavoratori" runat="server" AutoGenerateColumns="False"
                    Width="100%" PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:BoundField DataField="DataNascita" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data di nascita"
                            HtmlEncode="False">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Cod. fisc.">
                            <ItemStyle Width="120px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dataInizioAttivita" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Inizio" >
                        <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="dataFineAttivita" DataFormatString="{0:dd/MM/yyyy}" 
                            HeaderText="Fine" >
                        <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:CheckBoxField DataField="EffettuaControlli" 
                            HeaderText="Effettua Controlli" ReadOnly="True">
                        <ItemStyle Width="10px" />
                        </asp:CheckBoxField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun lavoratore presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        Nessun dato estratto
    </EmptyDataTemplate>
</asp:GridView>