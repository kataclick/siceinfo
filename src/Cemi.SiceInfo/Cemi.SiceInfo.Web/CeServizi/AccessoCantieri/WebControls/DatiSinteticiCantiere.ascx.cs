﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls
{
    public partial class DatiSinteticiCantiere : System.Web.UI.UserControl
    {
        private Int32? IdWhiteList
        {
            get
            {
                if (ViewState["IdWhiteList"] != null)
                {
                    return (Int32)ViewState["IdWhiteList"];
                }

                return null;
            }
            set { ViewState["IdWhiteList"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDatiCantiere(WhiteList whiteList)
        {
            IdWhiteList = whiteList.IdWhiteList;
            LabelIndirizzo.Text = String.Format("{0} {1}", whiteList.Indirizzo, whiteList.Civico);
            LabelComune.Text = whiteList.Comune;
            LabelProvincia.Text = whiteList.Provincia;
        }

        public void Reset()
        {
            IdWhiteList = null;
            Presenter.SvuotaCampo(LabelIndirizzo);
            Presenter.SvuotaCampo(LabelComune);
            Presenter.SvuotaCampo(LabelProvincia);
        }
    }
}