﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccessoCantieriControlliCexChange.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.WebControls.AccessoCantieriControlliCexChange" %>

<%@ Register src="RicercaCantieri.ascx" tagname="RicercaCantieri" tagprefix="uc1" %>

<style type="text/css">

    #TableCex ul {
        margin: 0;  
        padding-left: 5px;
    }
    
    #TableCex li {
   
    }
</style>
<script type="text/javascript" language="javascript">
    function requestStart(sender, args) {
        if (args.get_eventTarget().indexOf("DownloadPDF") > 0)
            args.set_enableAjax(false);
    }
</script>

<table class="standardTable">
    <tr>
        <td>
            <br />
            <%--<asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Cantieri"></asp:Label>
            <br />--%>
            <asp:Panel ID="PanelRicerca" runat="server">
                <uc1:RicercaCantieri ID="RicercaCantieri1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="PanelRilevatori" runat="server">
                <br />
                <asp:Label ID="LabelCantiere" runat="server" Text="Cantiere selezionato: " Visible="False"
                    Font-Bold="True"></asp:Label>
                <br />
                <asp:Label ID="LabelCantiere0" runat="server" Font-Bold="True" Text="Elenco timbrature per il cantiere"
                    Visible="False"></asp:Label>
                <br />
                <asp:Panel ID="PanelFiltroTimb" runat="server" Visible="False">
                    <table class="borderedTable">
                        <tr>
                            <td>
                                Impresa
                            </td>
                            <td>
                                Mese / Anno (MM/aaaa)
                            </td>
                            <td>
                                Codice fiscale
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="DropDownListImpresa" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="False" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <%--<telerik:RadDatePicker ID="RadDatePickerMeseAnno" Runat="server" 
                                    Culture="it-IT">
                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" 
                                        ViewSelectorText="x">
                                    </Calendar>
                                    <DateInput DisplayDateFormat="MM/yyyy">
                                    </DateInput>
                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                </telerik:RadDatePicker>--%>
                                <asp:TextBox ID="TextBoxMeseAnno" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Partita IVA Impresa (da timbrature)&nbsp;
                            </td>
                            <td style="visibility:hidden">
                                Ruolo
                            </td>
                            <td style="visibility:hidden">
                                Anomalie</td>
                            <td style="visibility:hidden">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="TextBoxPIVA" runat="server" MaxLength="11" Width="150px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListRuoli" runat="server" AppendDataBoundItems="True"
                                    AutoPostBack="False" Width="150px" Visible="false">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownListAnomalie" runat="server" 
                                    AppendDataBoundItems="True" AutoPostBack="False" Width="150px" Visible="false">
                                </asp:DropDownList>
&nbsp;
                            </td>
                            <td align="right">
                                <asp:Button ID="ButtonVisualizzaTimb" runat="server" OnClick="ButtonVisualizzaTimb_Click"
                                    Text="Ricerca" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <asp:Button
                    ID="ButtonEsportaPDF"
                    runat="server"
                    Text="PDF"
                    Width="150px" 
                    onclick="ButtonEsportaPDF_Click"
                    Visible="false" />
                <telerik:RadGrid
                    ID="RadGridTimbrature"
                    runat="server"
                    Width="100%" 
                    GridLines="None"
                    AllowPaging="True"
                    PageSize="15" onitemdatabound="RadGridTimbrature_ItemDataBound" 
                    Font-Size="Small" onpageindexchanged="RadGridTimbrature_PageIndexChanged"
                    Visible="false">
                    <ExportSettings IgnorePaging="false" OpenInNewWindow="true" ExportOnlyData="false" FileName="ControlliAccessoCantieri">
                        <Pdf PageWidth="297mm" PageHeight="210mm" PageTitle="Controlli Accesso Cantieri" Title="ControlliAccessoCantieri" PageHeaderMargin="10mm" PageBottomMargin="10mm" PageLeftMargin="10mm" PageRightMargin="10mm" />
                    </ExportSettings>
                    <MasterTableView>
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>

                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Timbratura" UniqueName="timbratura"  >
                                <ItemStyle Width="80px" VerticalAlign="Top" />
                                <HeaderStyle Width="80px"  />
                                <ItemTemplate>
                                    <table width="80px">
                                        <colgroup>
                                            <col  />
                                        </colgroup>
                                        <tr id="rowPeriodo" runat="server" style="padding-bottom:0px;  font-size:8pt">
                                            <td  style="padding-bottom:0px;">
                                                Periodo
                                            </td>
                                        </tr>
                                        <tr style="padding-top:0px">
                                            <td style="padding-top:0px">
                                               <b><asp:Label ID="LabelMeseAnno" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr id="rowLavoratore" runat="server" style="padding-bottom:0px;font-size:8pt">
                                            <td style="padding-bottom:0px" >
                                                Lavoratore
                                            </td>
                                        </tr>
                                        <tr  style="padding-top:0px; padding-bottom:0px">
                                            <td style="padding-top:0px; padding-bottom:0px">
                                                <asp:Label ID="LabelWlCodFiscLav" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="padding-top:0px">
                                            <td style="padding-top:0px">
                                                <b><asp:Label ID="LabelWlNomeCognomeLav" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                         <tr id="rowImpresa" runat="server" style="padding-bottom:0px;font-size:8pt">
                                            <td style="padding-bottom:0px">
                                                Impresa
                                            </td>
                                        </tr>
                                        <tr style="padding-top:0px; padding-bottom:0px">
                                            <td style="padding-top:0px; padding-bottom:0px">
                                               <asp:Label ID="LabelWlPivaImpresa" runat="server"></asp:Label> 
                                            </td>
                                        </tr>
                                        <tr style="padding-top:0px">
                                            <td style="padding-top:0px">
                                               <b><asp:Label ID="LabelWlRagSocImpresa" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>


                            <telerik:GridTemplateColumn HeaderText="Controllo CexChange" UniqueName="datiCexChange" >
                                <ItemStyle Width="450px" VerticalAlign="Top"  />
                                 <HeaderStyle Width="450px" HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <telerik:RadGrid ID="RadGridCexChange" runat="server" OnItemDataBound="RadGridCecChange_ItemDataBound"  >
                                    <MasterTableView >
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="CassaEdileDescrizione" UniqueName="cassaEdile" HeaderStyle-Width="70px" ItemStyle-Width="70px" HeaderText="C.E."  />
                                            <telerik:GridTemplateColumn HeaderText="Controllo Impresa" UniqueName="impresa">
                                                <ItemStyle Width="200px" />
                                                <HeaderStyle Width="200px" />
                                                <ItemTemplate>
                                                    <table width="200px" style="text-align:left;">
                                                        <colgroup>
                                                            <col width="60px" />
                                                            <col />
                                                        </colgroup>
                                                        <tr>
                                                            <td class="accessoCantieriTemplateTableTd">
                                                                <b>Cod. C.E.:</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LabelCodiceImpresa" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                       <%-- <tr>
                                                            <td class="accessoCantieriTemplateTableTd">
                                                                <b>P.IVA/Cod.Fisc.:</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LabelIvaCodFiscImpresa" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td class="accessoCantieriTemplateTableTd">
                                                                <b>Rag.Soc.:</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LabelRagioneSocialeImpresa" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="accessoCantieriTemplateTableTd">
                                                                <b>Stato attuale:</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LabelStatoImpresa" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                               <b>Denuncia:</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LabelDenuncia" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="accessoCantieriTemplateTableTd">
                                                                <b>Regolarità:</b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="LabelRegolarita" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Controllo Lavoratore" 
                                                    UniqueName="controlliCEMI">
                                                    <ItemStyle Width="180px" VerticalAlign="Top" />
                                                    <HeaderStyle Width="180px" />
                                                    <ItemTemplate>
                                                        <table  width="180px" style="text-align:left;">
                                                            <colgroup>
                                                                <col width="60px" />
                                                                <col />
                                                            </colgroup>
                                                            <tr>
                                                                <td>
                                                                    <b>Cod. C.E.:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelCodiceCE" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="accessoCantieriTemplateTableTd">
                                                                    <b>Nome:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelNomeCognome" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Nato il:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelDataNascita" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                      
                                                            <tr>
                                                                <td>
                                                                    <b>Presenza:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelLavoratoreDenuncia" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Ore:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="LabelOreLavorate" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    </telerik:RadGrid>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridTemplateColumn HeaderText="Controllo INPS" 
                                UniqueName="controlloINPS">
                                <ItemStyle Width="80px" VerticalAlign="Top" />
                                <HeaderStyle Width="80px" />
                                <ItemTemplate>
                                    <table width="80px">
                                        <colgroup>
                                            <col width="80px" />
                                        </colgroup>
                                        <tr>
                                            <td class="accessoCantieriTemplateTableTd">
                                                <b>Posizione Contrib.:</b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelPosizionecontributiva" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                          
                        </Columns>
                    </MasterTableView>
                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" 
                        EnableImageSprites="True">
                    </HeaderContextMenu>
                </telerik:RadGrid>
                <br />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelAggiungiRilevatore" runat="server" Visible="False">
            </asp:Panel>
        </td>
    </tr>
</table>
