﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Collections;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class Statistiche : System.Web.UI.Page
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.AccessoCantieriAmministrazione);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaStatistica();

                WhiteListFilter filtro = CreaFiltro();
                WhiteListCollection domande = _biz.GetDomandeByFilter(filtro);
                RilevatoreCollection rilevatori = _biz.GetRilevatori();
                FornitoreCollection fornitori = _biz.GetFornitori();

                Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListCantiere, domande,
                                                                   "IndirizzoCompletoCantiere", "IdWhitelist");
                Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListRilevatore, rilevatori, "Codice", "Codice");
                Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListFornitore, fornitori, "RagioneSociale",
                                                                   "IdFornitore");
            }
        }

        private void CaricaStatistica()
        {
            Statistica statistica = _biz.GetStatistica(null, null, null, null, null, null, null);

            LabelTotaleImpreseCEAppaltatrici.Text = statistica.TotaleImpreseCeAppaltatrici.ToString();
            LabelTotaleImpreseCESubappaltate.Text = statistica.TotaleImpreseCeSubappaltate.ToString();
            LabelTotaleImpreseNuoveAppaltatrici.Text = statistica.TotaleImpreseNuoveAppaltatrici.ToString();
            LabelTotaleImpreseNuoveSubappaltate.Text = statistica.TotaleImpreseNuoveSubappaltate.ToString();
            LabelTotaleImpreseArtigianeSubappaltate.Text = statistica.TotaleImpreseArtigianeSubappaltate.ToString();
            LabelTotaleImpreseArtigianeAppaltatrici.Text = statistica.TotaleImpreseArtigianeAppaltatrici.ToString();
            LabelTotaleLavoratoriCE.Text = statistica.TotaleLavoratoriCe.ToString();
            LabelTotaleLavoratoriNuovi.Text = statistica.TotaleLavoratoriNuovi.ToString();
            LabelTotaleTimbrature.Text = statistica.TotaleTimbrature.ToString();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GridViewCantieri.Visible = true;

                CaricaAttestati(0);
            }
        }


        private void CaricaAttestati(Int32 pagina)
        {
            WhiteListFilter filtro = CreaFiltro();

            WhiteListCollection domande = _biz.GetDomandeByFilter(filtro);
            Presenter.CaricaElementiInGridView(
                GridViewCantieri,
                domande,
                pagina);
        }

        private WhiteListFilter CreaFiltro()
        {
            WhiteListFilter filtro = new WhiteListFilter
            {
                Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text),
                Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text)
            };

            return filtro;
        }

        protected void GridViewCantieri_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Page.Validate("ricercaAttestati");

            if (Page.IsValid)
            {
                CaricaAttestati(e.NewPageIndex);
            }
        }

        protected void GridViewCantieri_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            if (GridViewCantieri != null)
            {
                Int32 idDomanda = (Int32)GridViewCantieri.DataKeys[e.NewSelectedIndex].Values["idWhiteList"];
                ViewState["IdDomanda"] = idDomanda;
                Context.Items["IdDomanda"] = idDomanda;
            }

            Server.Transfer("~/CeServizi/AccessoCantieri/VisualizzaWhiteList.aspx");
        }

        protected void GridViewCantieri_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                WhiteList domanda = (WhiteList)e.Row.DataItem;

                Label lIndirizzo = (Label)e.Row.FindControl("LabelIndirizzo");
                Label lComune = (Label)e.Row.FindControl("LabelComune");
                Label lProvincia = (Label)e.Row.FindControl("LabelProvincia");

                lIndirizzo.Text = domanda.Indirizzo;
                lComune.Text = domanda.Comune;
                lProvincia.Text = domanda.Provincia;
            }
        }

        protected void ButtonVisualizzaTimb_Click(object sender, EventArgs e)
        {
            int? idCantiere = null;
            DateTime? dataDa = null;
            DateTime? dataA = null;
            int? idFornitore = null;
            string codiceRilevatore = null;


            if (DropDownListCantiere.SelectedIndex > 0)
                idCantiere = Int32.Parse(DropDownListCantiere.SelectedValue);

            if (RadDatePickerDataInizio.SelectedDate.HasValue)
                dataDa = RadDatePickerDataInizio.SelectedDate.Value;

            if (RadDatePickerDataFine.SelectedDate.HasValue)
                dataA = RadDatePickerDataFine.SelectedDate.Value;

            if (DropDownListFornitore.SelectedIndex > 0)
                idFornitore = Int32.Parse(DropDownListFornitore.SelectedValue);

            if (DropDownListRilevatore.SelectedIndex > 0)
                codiceRilevatore = DropDownListRilevatore.SelectedValue;


            Statistica statistica = _biz.GetStatistica(idCantiere, null, null, dataDa, dataA, idFornitore, codiceRilevatore);


            LabelTotaleTimbratureFiltrate.Text = statistica.TotaleTimbratureFiltrate.ToString();
        }
    }
}