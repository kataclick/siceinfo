﻿using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Collections.Generic;
using TBridge.Cemi.AccessoCantieri.Type.Filters;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class ReportPresenze : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                                                         };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            if (!Page.IsPostBack)
            {
                if (Context.Items["Filtro"] != null)
                {
                    PresenzeFilter filtro = (PresenzeFilter)Context.Items["Filtro"];

                    ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                    ReportViewer1.ServerReport.ReportPath = "/ReportAccessoCantieri/ReportPresenze";
                    List<ReportParameter> listaParam = new List<ReportParameter>();
                    listaParam.Add(new ReportParameter("idCantiere", filtro.IdCantiere.ToString()));
                    listaParam.Add(new ReportParameter("dal", filtro.Dal.ToString("dd/MM/yyyy")));
                    listaParam.Add(new ReportParameter("al", filtro.Al.ToString("dd/MM/yyyy")));
                    //if (!String.IsNullOrWhiteSpace(filtro.CodiceFiscale))
                    //{
                    listaParam.Add(new ReportParameter("codiceFiscale", filtro.CodiceFiscale));
                    //}
                    //if (!String.IsNullOrWhiteSpace(filtro.PartitaIvaImpresa))
                    //{
                    listaParam.Add(new ReportParameter("partitaIva", filtro.PartitaIvaImpresa));
                    //}

                    ReportViewer1.ServerReport.SetParameters(listaParam);

                    #region Export diretto in pdf

                    Warning[] warnings;
                    string[] streamids;
                    string mimeType;
                    string encoding;
                    string extension;

                    byte[] bytes = ReportViewer1.ServerReport.Render(
                        "PDF", null, out mimeType, out encoding, out extension,
                        out streamids, out warnings);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";

                    Response.AppendHeader("Content-Disposition", "attachment;filename=Presenze.pdf");
                    Response.BinaryWrite(bytes);

                    Response.Flush();
                    Response.End();

                    #endregion
                }
            }
        }
    }
}