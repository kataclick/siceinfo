﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="DatiAggiuntiviLavoratore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.DatiAggiuntiviLavoratore" %>
<%@ Register src="WebControls/DatiAggiuntiviLavoratore.ascx" tagname="DatiAggiuntiviLavoratore" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Info Lavoratore</title>
</head>
<body id="body1" runat="server">
    <form id="form1" runat="server">
    <div>
        <script type="text/javascript">
            function CloseAndRebindNew(args) {
                GetRadWindow().BrowserWindow.refreshGridNew(args);
                GetRadWindow().close();
            }

            function CloseAndRebindEdit(args) {
                GetRadWindow().BrowserWindow.refreshGridEdit(args);
                GetRadWindow().close();
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

                return oWindow;
            }

            function Cancel() {
                GetRadWindow().close();
            }
        </script>
        
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
        <uc1:DatiAggiuntiviLavoratore ID="DatiAggiuntiviLavoratore1" runat="server" />
        <asp:Button
            ID="ButtonConferma"
            runat="server"
            Text="Conferma"
            ValidationGroup="lavoratore"
            Width="150px" 
            onclick="ButtonConferma_Click" />
        <asp:Button
            ID="ButtonAnnulla"
            runat="server"
            Text="Annulla"
            Width="150px"
            OnClientClick="Cancel();" onclick="ButtonAnnulla_Click" />
    </div>
    </form>
</body>
</html>

