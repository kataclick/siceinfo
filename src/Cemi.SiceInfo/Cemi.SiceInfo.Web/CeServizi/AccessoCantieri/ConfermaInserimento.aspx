﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfermaInserimento.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.ConfermaInserimento" %>

<%@ Register src="../WebControls/MenuAccessoCantieri.ascx" tagname="MenuAccessoCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso Cantieri" sottoTitolo="Conferma richiesta" />
    <br />
    <p>
        La lista è stata correttamente inserita/modificata.
    </p>
    <p>
        E' possibile controllare le liste inserite tramite la funzionalità "Riepilogo cantieri" nel menu laterale sinistro.
    </p>
</asp:Content>
