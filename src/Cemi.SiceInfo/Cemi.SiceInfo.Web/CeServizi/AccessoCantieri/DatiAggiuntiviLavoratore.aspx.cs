﻿using System;
using System.Web.UI;
using TBridge.Cemi.AccessoCantieri.Business;
using TBridge.Cemi.AccessoCantieri.Type.Entities;
using TBridge.Cemi.AccessoCantieri.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class DatiAggiuntiviLavoratore : System.Web.UI.Page
    {
        private readonly AccessoCantieriBusiness _biz = new AccessoCantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["dataInizio"] != null && Request.QueryString["dataInizio"] != String.Empty &&
                    Request.QueryString["dataInizio"] != "undefined")
                {
                    DateTime dataInizio = DateTime.ParseExact(Request.QueryString["dataInizio"], "dd/MM/yyyy", null);
                    DatiAggiuntiviLavoratore1.CaricaDataInizio(dataInizio);
                }
                if (Request.QueryString["dataFine"] != null && Request.QueryString["dataFine"] != String.Empty &&
                    Request.QueryString["dataFine"] != "undefined")
                {
                    DateTime dataFine = DateTime.ParseExact(Request.QueryString["dataFine"], "dd/MM/yyyy", null);
                    DatiAggiuntiviLavoratore1.CaricaDataFine(dataFine);
                }
                //if (Request.QueryString["idCantiere"] != null && Request.QueryString["idCantiere"] != String.Empty &&
                //    Request.QueryString["idCantiere"] != "undefined")
                //{
                //    Int32 idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
                //    DatiAggiuntiviLavoratore1.CaricaIdCantiere(idCantiere);
                //}
                //if (Request.QueryString["idImpresa"] != null && Request.QueryString["idImpresa"] != String.Empty &&
                //    Request.QueryString["idImpresa"] != "undefined")
                //{
                //    Int32 idImpresa = Int32.Parse(Request.QueryString["idImpresa"]);
                //    DatiAggiuntiviLavoratore1.CaricaIdImpresa(idImpresa);
                //}
                //if (Request.QueryString["tipoImpresa"] != null && Request.QueryString["tipoImpresa"] != String.Empty &&
                //    Request.QueryString["tipoImpresa"] != "undefined")
                //{
                //    TipologiaImpresa tipoImpresa = (TipologiaImpresa)Int32.Parse(Request.QueryString["tipoImpresa"]);
                //    DatiAggiuntiviLavoratore1.CaricaTipoImpresa(tipoImpresa);
                //}

                // Nuovo
                if (Request.QueryString["idLavoratore"] != null
                    && Request.QueryString["idImpresa"] != null
                    && Request.QueryString["idLavoratore"] != "undefined"
                    && Request.QueryString["idImpresa"] != "undefined")
                {
                    Int32 idLavoratore = Int32.Parse(Request.QueryString["idLavoratore"]);
                    Int32 idImpresa = Int32.Parse(Request.QueryString["idImpresa"]);
                    Lavoratore lavoratore =
                        _biz.GetLavoratoriOrdinati(idLavoratore, null, null, null, null, null, null, null, null, idImpresa)[
                            0
                            ];

                    DatiAggiuntiviLavoratore1.CaricaLavoratore(lavoratore, false);
                }

                // Edit
                if (Request.QueryString["index"] != null)
                {
                    Int32 index = Int32.Parse(Request.QueryString["index"]);
                    ViewState["index"] = index;
                    Session["index"] = index;
                    WhiteListImpresa wlImpresa = (WhiteListImpresa)Session["LavoratoriImpresa"];
                    Lavoratore lavoratore = wlImpresa.Lavoratori[index];

                    ViewState["edit"] = true;

                    if (wlImpresa.IdDomanda.HasValue)
                        DatiAggiuntiviLavoratore1.CaricaIdCantiere(wlImpresa.IdDomanda.Value);
                    DatiAggiuntiviLavoratore1.CaricaTipoImpresa(wlImpresa.Impresa.TipoImpresa);
                    if (wlImpresa.Impresa.IdImpresa.HasValue)
                        DatiAggiuntiviLavoratore1.CaricaIdImpresa(wlImpresa.Impresa.IdImpresa.Value);
                    DatiAggiuntiviLavoratore1.CaricaLavoratore(lavoratore, true);
                }
            }
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {

                if (ViewState["edit"] == null)
                {
                    Session["AccessoCantieriLavoratore"] = DatiAggiuntiviLavoratore1.GetLavoratorePhoto();

                    body1.Attributes.Add("onload", "CloseAndRebindNew();");
                }
                else
                {
                    Session["AccessoCantieriLavoratore"] = DatiAggiuntiviLavoratore1.GetLavoratore();

                    WhiteListImpresa wlImpresa = (WhiteListImpresa)Session["LavoratoriImpresa"];
                    Lavoratore lav = DatiAggiuntiviLavoratore1.GetLavoratore();
                    Int32 idCantiere = Int32.Parse(Session["IdCantiere"].ToString());

                    byte[] pht = DatiAggiuntiviLavoratore1.GetPhotoLavoratore();

                    if (lav.IdLavoratore.HasValue && wlImpresa.Impresa.IdImpresa.HasValue)
                    {
                        _biz.UpdateFotoLavoratoreByLavoratoreImpresaCantiere(lav.IdLavoratore.Value, lav.TipoLavoratore, wlImpresa.Impresa.IdImpresa.Value, wlImpresa.Impresa.TipoImpresa, /*wlImpresa.IdDomanda*/idCantiere, pht);
                    }
                    else
                    {
                        Session["AccessoCantieriLavoratore"] = DatiAggiuntiviLavoratore1.GetLavoratore();
                    }

                    body1.Attributes.Add("onload", "CloseAndRebindEdit();");
                }
            }
        }
        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            DatiAggiuntiviLavoratore1.ResetPhotoLavoratore();
        }

    }
}