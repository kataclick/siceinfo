﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ControlliCEMIImprese.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.ControlliCEMIImprese" %>

<%@ Register src="../WebControls/MenuAccessoCantieri.ascx" tagname="MenuAccessoCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/AccessoCantieriControlliCEMIImpresa.ascx" tagname="AccessoCantieriControlliCEMIImpresa" tagprefix="uc3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso ai Cantieri" sottoTitolo="Controlli CEMI" />
    <br />
    <uc3:AccessoCantieriControlliCEMIImpresa ID="AccessoCantieriControlliCEMIImpresa1" 
        runat="server" />
</asp:Content>