﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class ImportLavoratori : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["IdWhiteList"] = Request.QueryString["idWhiteList"];
            }
        }

        protected void ButtonVerificaLavoratori_Click(object sender, EventArgs e)
        {
            if (FileUploadCsv.HasFile
                && Path.GetExtension(FileUploadCsv.FileName).ToUpper() == ".CSV")
            {
                LabelErrore.Visible = false;


            }
            else
            {
                LabelErrore.Visible = true;
            }
        }

        protected void ButtonImportaLavoratori_Click(object sender, EventArgs e)
        {
            if (FileUploadCsv.HasFile
                && Path.GetExtension(FileUploadCsv.FileName).ToUpper() == ".CSV")
            {
                LabelErrore.Visible = false;
            }
            else
            {
                LabelErrore.Visible = true;
            }
        }
    }
}