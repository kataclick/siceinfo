﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="Presenze.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.PresenzeClass" %>

<%@ Register src="../WebControls/MenuAccessoCantieri.ascx" tagname="MenuAccessoCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/SelezioneCantiere.ascx" tagname="SelezioneCantiere" tagprefix="uc3" %>
<%@ Register src="WebControls/PresenzeWC.ascx" tagname="Presenze" tagprefix="uc4" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuAccessoCantieri ID="MenuAccessoCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Accesso ai Cantieri" sottoTitolo="Presenze" />
    <br />
    <uc3:SelezioneCantiere ID="SelezioneCantiere1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" Runat="Server">
    <uc4:Presenze ID="Presenze1" runat="server" Visible="false" />
</asp:Content>
