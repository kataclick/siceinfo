﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="ImportLavoratori.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AccessoCantieri.ImportLavoratori" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="standardTable">
            <tr>
                <td>
                    <b>
                        File (CSV):
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:FileUpload ID="FileUploadCsv" runat="server" Width="225px" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonVerificaLavoratori" runat="server" Text="Verifica"
                        Width="150px" CausesValidation="true" ValidationGroup="import"
                        onclick="ButtonVerificaLavoratori_Click" />
                    &nbsp;
                    <asp:Button ID="ButtonImportaLavoratori" runat="server" Text="Importa"
                        Width="150px" CausesValidation="true" ValidationGroup="import"
                        onclick="ButtonImportaLavoratori_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                    <asp:Label
                        ID="LabelErrore"
                        runat="server"
                        ForeColor="Red"
                        Text="Selezionare un file CSV"
                        Visible="false">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Risultati...
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

