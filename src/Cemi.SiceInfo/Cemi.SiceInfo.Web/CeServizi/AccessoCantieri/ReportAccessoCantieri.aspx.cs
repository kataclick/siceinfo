﻿using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Collections.Generic;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AccessoCantieri
{
    public partial class ReportAccessoCantieri : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.AccessoCantieriGestioneCantiere,
                                                             FunzionalitaPredefinite.AccessoCantieriPerCommittente
                                                         };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDomanda"] != null)
                {
                    int idDomanda = (int)Context.Items["IdDomanda"];

                    ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                    ReportViewer1.ServerReport.ReportPath = "/ReportAccessoCantieri/ReportAccessoCantieriWhiteList";
                    ReportParameter[] listaParam = new ReportParameter[1];
                    listaParam[0] = new ReportParameter("idWhiteList", idDomanda.ToString());

                    ReportViewer1.ServerReport.SetParameters(listaParam);

                    #region Export diretto in pdf

                    //Warning[] warnings;
                    //string[] streamids;
                    //string mimeType;
                    //string encoding;
                    //string extension;

                    //byte[] bytes = ReportViewer1.ServerReport.Render(
                    //    "PDF", null, out mimeType, out encoding, out extension,
                    //    out streamids, out warnings);

                    //Response.Clear();
                    //Response.Buffer = true;
                    //Response.ContentType = "application/pdf";

                    //Response.AppendHeader("Content-Disposition", "attachment;filename=WhiteList.pdf");
                    //Response.BinaryWrite(bytes);

                    //Response.Flush();
                    //Response.End();

                    #endregion
                }
            }
        }
    }
}