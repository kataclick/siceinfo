﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="Edilconnect.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Edilconnect.Edilconnect" %>

<%@ Register Src="~/CeServizi/WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Edilconnect"
        sottoTitolo="Gestione cantieri" />
    <br />
    Attendere...
    <%--Con l’accesso al portale “Edilconnect” l’impresa principale dell’appalto (affidataria o appaltatrice) inserisce tutti i cantieri nuovi, aperti nelle Province della Regione Lombardia, soggetti e non alla verifica di congruità a prescindere dalla tipologia di appalto (pubblico / privato) e dall’importo dei lavori (importo inferiore / superiore a € 100.000,00 per i cantieri privati).
	<br />
    <br />
    <b><i>Cantieri soggetti alla verifica di congruità</i></b>
    <br />
    <br />
    Per i cantieri soggetti alla “verifica sperimentale di congruità” si precisa che quest’ultima consiste
	nella verifica dell’incidenza della manodopera
    denunciata sul valore complessivo dell’opera. Lo scopo è accertare che il quantitativo
    di manodopera impiegata nello svolgimento dei lavori sia adeguato alla tipologia
    e all’importo complessivo dell’opera.
    <br />
    <br />
    L’indicazione del cantiere deve essere effettuata sempre solo ed esclusivamente
    dall’impresa principale dell’appalto (affidataria o appaltatrice) o dallo studio
    di consulenza delegato.
    <br />
    <br />
    Si precisa che i canteri soggetti a verifica di congruità sono:
    <ul>
        <li><b>i cantieri pubblici</b> situati in una Provincia lombarda con data inizio lavori uguale
            o successiva al 1/12/2012;</li>
        <li><b>i cantieri privati</b> di importo complessivo pari o superiore a € 100.000,00 situati
            in una Provincia lombarda con data inizio lavori uguale o successiva al 1/12/2012.</li>
    </ul>
    <b>I cantieri con data inizio antecedente al 1/12/2012 non devono essere indicati</b> nel
    portale Edilconnect.
    <br />
    <br />
    Oltre all’inserimento del cantiere, il portale "Edilconnect" consente ai soggetti
    interessati sopra citati di:
    <ul>
        <li>gestire i subappalti;</li>
        <li>controllare l’avanzamento degli importi di manodopera registrati dalle Casse Edili
            per tutte le imprese attive nel cantiere ed attuare eventuali provvedimenti correttivi
            prima della conclusione dello stesso (in caso di mancato raggiungimento del valore
            minimo atteso ai fini della verifica di congruità).</li>
    </ul>--%>
    
</asp:Content>
