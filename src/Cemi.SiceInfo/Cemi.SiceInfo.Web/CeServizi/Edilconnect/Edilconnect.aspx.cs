﻿using System;
using TBridge.Cemi.GestioneUtenti.Business;

namespace Cemi.SiceInfo.Web.CeServizi.Edilconnect
{
    public partial class Edilconnect : System.Web.UI.Page
    {
        //private const String CODICECE = "111113";
        //private const String PIVA = "02646210134";
        //private const String GUID = "12345678901234567890";
        //tipoUtente: 1=consulente; 2=impresa
        //private const String LOGINCE = "MI00";
        //private const String PASSWORD = "1qazxsw2";

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            if (!GestioneUtentiBiz.IsImpresa() && !GestioneUtentiBiz.IsConsulente())
                throw new Exception("Pagina non valida per l'utente");
            #endregion

            Server.Transfer("~/CeServizi/Default.aspx");
        }
    }
}