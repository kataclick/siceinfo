﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese
{
    public partial class GestioneRecapiti : System.Web.UI.Page
    {
        private readonly Common _commonBiz = new Common();
        private Consulente _consulente;
        private int _idImpresa;
        private string _ragioneSociale;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRichiestaVariazioneRecapiti);

            ConsulenteSelezioneImpresa1.OnImpresaSelected += OnImpresaSelected;

            _idImpresa = 0;

            if (GestioneUtentiBiz.IsImpresa())
            {
                ConsulenteSelezioneImpresa1.Visible = false;
                Impresa impresa = (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                _idImpresa = impresa.IdImpresa;
            }
            if (GestioneUtentiBiz.IsConsulente())
            {
                _consulente = (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                _commonBiz.ConsulenteImpresaSelezionata(_consulente.IdConsulente, out _idImpresa, out _ragioneSociale);
            }

            if (!Page.IsPostBack)
            {
                if (_idImpresa != -1)
                {
                    Recapiti1.Reset();
                    Recapiti1.CaricaRecapitiImpresa(_idImpresa);
                    Recapiti1.Visible = true;
                }
            }
        }

        private void OnImpresaSelected(int idImpresa, string ragioneSociale)
        {
            _commonBiz.ConsulenteImpresaSelezionata(_consulente.IdConsulente, out _idImpresa, out _ragioneSociale);

            if (_idImpresa != -1)
            {
                Recapiti1.Reset();
                Recapiti1.CaricaRecapitiImpresa(_idImpresa);
                Recapiti1.Visible = true;
            }
            else
            {
                Recapiti1.Visible = false;
            }
        }
    }
}