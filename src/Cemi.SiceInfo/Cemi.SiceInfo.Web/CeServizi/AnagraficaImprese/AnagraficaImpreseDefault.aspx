﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="AnagraficaImpreseDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.AnagraficaImpreseDefault" %>

<%@ Register Src="../WebControls/MenuAnagraficaImpreseVariazioni.ascx" TagName="MenuAnagraficaImpreseVariazioni"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Sportello web"
        sottoTitolo="Variazione recapiti e stato" />
    <p>
        Per comunicare in modo rapido ed efficace l’aggiornamento dei dati relativi ai recapiti
        della sede legale, amministrativa e di corrispondenza di un’impresa iscritta.
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuAnagraficaImpreseVariazioni ID="MenuAnagraficaImpreseVariazioni1" runat="server" />
</asp:Content>
