﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RichiesteCambioStatoImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.WebControls.RichiesteCambioStatoImpresa" %>

<%@ Import Namespace="Cemi.SiceInfo.Type.Domain" %> 
<%@ Import Namespace="Cemi.SiceInfo.Type.Enum" %>
<asp:Panel ID="PanelRicercaStatoImpresa" runat="server" DefaultButton="ButtonRicerca">
    <table class="filledtable">
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Variazioni stato impresa"></asp:Label>
            </td>
        </tr>
    </table>
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td>
                    Codice Impresa
                </td>
                <td>
                    Ragione Sociale
                </td>
                <td>
                    Data Richiesta Da
                </td>
                <td>
                    Data Richiesta A
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadNumericTextBox ID="RadTextBoxCodiceImpresa" runat="server" MaxLength="10">
                        <NumberFormat AllowRounding="False" DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" MaxLength="100">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataRichiestaDa" runat="server">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataRichiestaA" runat="server">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="RadDatePickerDataRichiestaDa"
                        ControlToValidate="RadDatePickerDataRichiestaA" EnableClientScript="False" ErrorMessage="Limiti date errate"
                        Operator="GreaterThanEqual" ValidationGroup="ricercaRichiestaVariazioneStato">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Stato Impresa Richiesto
                </td>
                <td>
                    Stato Richiesta
                </td>
                <td>
                    Data Inizio Cambio Stato
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxStatoImpresa" runat="server" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxStatoRichiesta" runat="server" AppendDataBoundItems="true">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataInizioCambioStato" runat="server">
                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                        </Calendar>
                        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy">
                        </DateInput>
                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                    </telerik:RadDatePicker>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummaryErrore" runat="server" CssClass="messaggiErrore"
                        ValidationGroup="ricercaRichiestaVariazioneStato" />
                </td>
                <td>
                    <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="ricercaRichiestaVariazioneStato"
                        OnClick="ButtonRicerca_Click" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Richieste cambio stato trovate"></asp:Label>
        <telerik:RadGrid ID="RadGridRichiesteCambioStato" runat="server" AllowPaging="True"
            AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="RadGridRichiesteCambioStato_SelectedIndexChanged"
            OnPageIndexChanged="RadGridRichiesteCambioStato_PageIndexChanged" PageSize="5">
            <MasterTableView DataKeyNames="Id">
                <CommandItemSettings ExportToPdfText="Export to Pdf" />
                <Columns>
                    <telerik:GridBoundColumn DataField="Id" HeaderText="Id" />
                    <telerik:GridBoundColumn DataField="IdImpresa" HeaderText="Cod. Impresa" />
                    <telerik:GridBoundColumn DataField="Impresa.RagioneSociale" HeaderText="Ragione Sociale">
                        <ItemStyle Font-Bold="True" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DataRichiesta" HeaderText="Data Richiesta" DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="false" />
                    <telerik:GridBoundColumn DataField="StatoImpresaDesc" HeaderText="Stato Impresa Richiesto" />
                    <telerik:GridBoundColumn DataField="DataInizioCambioStato" HeaderText="Data Inizio Cambio Stato"
                        DataType="System.String" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false" />
                    <telerik:GridTemplateColumn HeaderText="Stato Pratica">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="LabelVariazioneStatoPratica"><%# (((ImpresaRichiestaVariazioneStato)Container.DataItem).DataGestioneOperatore.HasValue) ? $"{((ImpresaRichiestaVariazioneStato) Container.DataItem).IdTipoStatoPratica.Description()} ({((ImpresaRichiestaVariazioneStato) Container.DataItem).DataGestioneOperatore.Value})" : ((ImpresaRichiestaVariazioneStato)Container.DataItem).IdTipoStatoPratica.Description() %> </asp:Label> 
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn CommandName="Select" Text="Dettaglio" UniqueName="column"
                        ButtonType="PushButton">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
            </HeaderContextMenu>
        </telerik:RadGrid>
    </div>
</asp:Panel>
