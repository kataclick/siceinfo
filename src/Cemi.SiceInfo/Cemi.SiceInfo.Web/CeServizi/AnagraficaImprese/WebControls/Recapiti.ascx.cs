﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using Indirizzo = TBridge.Cemi.Type.Entities.Geocode.Indirizzo;
using TipoRecapito = TBridge.Cemi.Type.Enums.TipoRecapito;
using System.Collections.Generic;
using Cemi.SiceInfo.Web.Helpers;

namespace Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.WebControls
{
    public partial class Recapiti : System.Web.UI.UserControl
    {
        private const Int32 Indicedati = 0;
        private const Int32 Indicemodifica = 1;
        private const Int32 Indicemodificaincorso = 2;
        private readonly ImpresaRecapitiManager _impresaManager = new ImpresaRecapitiManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Per prevenire click multipli

            #region sede legale

            //StringBuilder sb = new StringBuilder();
            //sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            //sb.Append(
            //    "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            //sb.Append("this.value = 'Attendere...';");
            //sb.Append("this.disabled = true;");
            //sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonModificaSedeLegale, null));
            //sb.Append(";");
            //sb.Append("return true;");
            //ButtonModificaSedeLegale.Attributes.Add("onclick", sb.ToString());

            //((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            //    ButtonModificaSedeLegale);

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb2.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb2.Append(
                "if (Page_ClientValidate('" + ButtonSalvaSedeLegale.ValidationGroup +
                "') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb2.Append("this.value = 'Attendere...';");
            sb2.Append("this.disabled = true;");
            sb2.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalvaSedeLegale, null));
            sb2.Append(";");
            sb2.Append("return true;");
            ButtonSalvaSedeLegale.Attributes.Add("onclick", sb2.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
                ButtonSalvaSedeLegale);

            //StringBuilder sb3 = new StringBuilder();
            //sb3.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb3.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            //sb3.Append(
            //    "if (Page_ClientValidate('" + ButtonAnnullaSedeLegale.ValidationGroup + "') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            //sb3.Append("this.value = 'Attendere...';");
            //sb3.Append("this.disabled = true;");
            //sb3.Append(Page.ClientScript.GetPostBackEventReference(ButtonAnnullaSedeLegale, null));
            //sb3.Append(";");
            //sb3.Append("return true;");
            //ButtonAnnullaSedeLegale.Attributes.Add("onclick", sb3.ToString());

            //((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            //    ButtonAnnullaSedeLegale);

            #endregion

            #region sede amministrativa

            //StringBuilder sb4 = new StringBuilder();
            //sb4.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb4.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            //sb4.Append(
            //    "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            //sb4.Append("this.value = 'Attendere...';");
            //sb4.Append("this.disabled = true;");
            //sb4.Append(Page.ClientScript.GetPostBackEventReference(ButtonModificaSedeAmministrativa, null));
            //sb4.Append(";");
            //sb4.Append("return true;");
            //ButtonModificaSedeAmministrativa.Attributes.Add("onclick", sb4.ToString());

            //((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            //    ButtonModificaSedeAmministrativa);

            StringBuilder sb5 = new StringBuilder();
            sb5.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb5.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb5.Append(
                "if (Page_ClientValidate('" + ButtonSalvaSedeAmministrativa.ValidationGroup +
                "') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb5.Append("this.value = 'Attendere...';");
            sb5.Append("this.disabled = true;");
            sb5.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalvaSedeAmministrativa, null));
            sb5.Append(";");
            sb5.Append("return true;");
            ButtonSalvaSedeAmministrativa.Attributes.Add("onclick", sb5.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
                ButtonSalvaSedeAmministrativa);

            //StringBuilder sb6 = new StringBuilder();
            //sb6.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb6.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            //sb6.Append(
            //    "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            //sb6.Append("this.value = 'Attendere...';");
            //sb6.Append("this.disabled = true;");
            //sb6.Append(Page.ClientScript.GetPostBackEventReference(ButtonAnnullaSedeAmministrativa, null));
            //sb6.Append(";");
            //sb6.Append("return true;");
            //ButtonAnnullaSedeAmministrativa.Attributes.Add("onclick", sb6.ToString());

            //((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            //    ButtonAnnullaSedeAmministrativa);

            #endregion

            #region corrispondenza

            //StringBuilder sb7 = new StringBuilder();
            //sb7.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb7.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            //sb7.Append(
            //    "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            //sb7.Append("this.value = 'Attendere...';");
            //sb7.Append("this.disabled = true;");
            //sb7.Append(Page.ClientScript.GetPostBackEventReference(ButtonModificaCorrispondenza, null));
            //sb7.Append(";");
            //sb7.Append("return true;");
            //ButtonModificaCorrispondenza.Attributes.Add("onclick", sb7.ToString());

            //((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            //    ButtonModificaCorrispondenza);

            StringBuilder sb8 = new StringBuilder();
            sb8.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb8.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb8.Append(
                "if (Page_ClientValidate('" + ButtonSalvaCorrispondenza.ValidationGroup +
                "') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb8.Append("this.value = 'Attendere...';");
            sb8.Append("this.disabled = true;");
            sb8.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalvaCorrispondenza, null));
            sb8.Append(";");
            sb8.Append("return true;");
            ButtonSalvaCorrispondenza.Attributes.Add("onclick", sb8.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
                ButtonSalvaCorrispondenza);

            //StringBuilder sb9 = new StringBuilder();
            //sb9.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb9.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            //sb9.Append(
            //    "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            //sb9.Append("this.value = 'Attendere...';");
            //sb9.Append("this.disabled = true;");
            //sb9.Append(Page.ClientScript.GetPostBackEventReference(ButtonAnnullaCorrispondenza, null));
            //sb9.Append(";");
            //sb9.Append("return true;");
            //ButtonAnnullaCorrispondenza.Attributes.Add("onclick", sb9.ToString());

            //((RadScriptManager) Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(
            //    ButtonAnnullaCorrispondenza);

            #endregion

            #endregion
        }

        public void CaricaRecapitiImpresa(int idImpresa)
        {
            ImpresaRecapiti impresaRecapiti = _impresaManager.GetImpresaRecapitiById(idImpresa);
            TBridge.Cemi.Type.Domain.Impresa impresa = _impresaManager.GetImpresa(idImpresa);
            ViewState["NaturaGiuridica"] = impresa.idNaturaGiuridica;

            RadTextBoxIdImpresa.Text = impresaRecapiti.IdImpresa.ToString();
            RadTextBoxCf.Text = impresaRecapiti.CodiceFiscale;
            RadTextBoxRagionesociale.Text = impresaRecapiti.RagioneSociale;

            RadTextBoxSedeLegaleIndirizzo.Text = impresaRecapiti.IndirizzoSedeLegale.IndirizzoCompleto;
            RadTextBoxSedeLegalePresso.Text = impresaRecapiti.PressoSedeLegale;
            RadTextBoxSedeLegaleTelefono.Text = impresaRecapiti.TelefonoSedeLegale;
            RadTextBoxSedeLegaleEmail.Text = impresaRecapiti.EmailSedeLegale;
            RadTextBoxSedeLegaleFax.Text = impresaRecapiti.FaxSedeLegale;
            RadTextBoxSedeLegalePEC.Text = impresaRecapiti.PECSedeLegale;
            ViewState["PEC"] = impresaRecapiti.PECSedeLegale;

            RadTextBoxSedeAmministrativaIndirizzo.Text = impresaRecapiti.IndirizzoSedeAmministrativa.IndirizzoCompleto;
            RadTextBoxSedeAmministrativaPresso.Text = impresaRecapiti.PressoSedeAmministrativa;
            RadTextBoxSedeAmministrativaTelefono.Text = impresaRecapiti.TelefonoSedeAmministrativa;
            RadTextBoxSedeAmministrativaEmail.Text = impresaRecapiti.EmailSedeAmministrativa;
            RadTextBoxSedeAmministrativaFax.Text = impresaRecapiti.FaxSedeAmministrativa;

            RadTextBoxCorrispondenzaIndirizzo.Text = impresaRecapiti.IndirizzoCorrispondenza.IndirizzoCompleto;
            RadTextBoxCorrispondenzaPresso.Text = impresaRecapiti.PressoCorrispondenza;
            RadTextBoxCorrispondenzaTelefono.Text = impresaRecapiti.TelefonoCorrispondenza;
            RadTextBoxCorrispondenzaEmail.Text = impresaRecapiti.EmailCorrispondenza;
            RadTextBoxCorrispondenzaFax.Text = impresaRecapiti.FaxCorrispondenza;

            Indirizzo indirizzo = new Indirizzo();
            indirizzo.NomeVia = impresaRecapiti.IndirizzoSedeLegale.Via;
            indirizzo.Civico = impresaRecapiti.IndirizzoSedeLegale.Civico;
            indirizzo.Cap = impresaRecapiti.IndirizzoSedeLegale.Cap;
            indirizzo.Provincia = impresaRecapiti.IndirizzoSedeLegale.Provincia;
            indirizzo.Comune = impresaRecapiti.IndirizzoSedeLegale.Comune;

            SelezioneIndirizzo1.PreCaricaIndirizzo(indirizzo);

            indirizzo = new Indirizzo();
            indirizzo.NomeVia = impresaRecapiti.IndirizzoSedeAmministrativa.Via;
            indirizzo.Civico = impresaRecapiti.IndirizzoSedeAmministrativa.Civico;
            indirizzo.Cap = impresaRecapiti.IndirizzoSedeAmministrativa.Cap;
            indirizzo.Provincia = impresaRecapiti.IndirizzoSedeAmministrativa.Provincia;
            indirizzo.Comune = impresaRecapiti.IndirizzoSedeAmministrativa.Comune;

            SelezioneIndirizzo2.PreCaricaIndirizzo(indirizzo);

            indirizzo = new Indirizzo();
            indirizzo.NomeVia = impresaRecapiti.IndirizzoCorrispondenza.Via;
            indirizzo.Civico = impresaRecapiti.IndirizzoCorrispondenza.Civico;
            indirizzo.Cap = impresaRecapiti.IndirizzoCorrispondenza.Cap;
            indirizzo.Provincia = impresaRecapiti.IndirizzoCorrispondenza.Provincia;
            indirizzo.Comune = impresaRecapiti.IndirizzoCorrispondenza.Comune;

            SelezioneIndirizzo3.PreCaricaIndirizzo(indirizzo);

            RadTextBoxSedeLegaleTelefonoModifica.Text = impresaRecapiti.TelefonoSedeLegale;
            RadTextBoxSedeLegaleEmailModifica.Text = impresaRecapiti.EmailSedeLegale;
            RadTextBoxSedeLegaleFaxModifica.Text = impresaRecapiti.FaxSedeLegale;

            RadTextBoxSedeAmministrativaTelefonoModifica.Text = impresaRecapiti.TelefonoSedeAmministrativa;
            RadTextBoxSedeAmministrativaEmailModifica.Text = impresaRecapiti.EmailSedeAmministrativa;
            RadTextBoxSedeAmministrativaFaxModifica.Text = impresaRecapiti.FaxSedeAmministrativa;

            RadTextBoxCorrispondenzaTelefonoModifica.Text = impresaRecapiti.TelefonoCorrispondenza;
            RadTextBoxCorrispondenzaEmailModifica.Text = impresaRecapiti.EmailCorrispondenza;
            RadTextBoxCorrispondenzaFaxModifica.Text = impresaRecapiti.FaxCorrispondenza;

            VerificaRichiestePendenti(idImpresa);
        }

        private void VerificaRichiestePendenti(int idImpresa)
        {
            List<ImpresaRichiestaVariazioneRecapiti> richiesteLegali = _impresaManager.GetRichiesteVariazioniPendenti(idImpresa, 1);
            if (richiesteLegali.Count > 0)
            {
                LabelRichiestaLegale.Visible = true;

                if (!String.IsNullOrEmpty(richiesteLegali[richiesteLegali.Count - 1].ImpresaRecapito.Pec))
                {
                    LabelRichiestaPEC.Visible = true;
                }
            }

            List<ImpresaRichiestaVariazioneRecapiti> richiesteAmministrative = _impresaManager.GetRichiesteVariazioniPendenti(idImpresa, 2);
            if (richiesteAmministrative.Count > 0)
            {
                LabelRichiestaAmministrativa.Visible = true;

                if (!String.IsNullOrEmpty(richiesteAmministrative[richiesteAmministrative.Count - 1].ImpresaRecapito.Pec))
                {
                    LabelRichiestaPEC.Visible = true;
                }
            }

            List<ImpresaRichiestaVariazioneRecapiti> richiesteCorrispondenza = _impresaManager.GetRichiesteVariazioniPendenti(idImpresa, 3);
            if (richiesteCorrispondenza.Count > 0)
            {
                LabelRichiestaCorrispondeza.Visible = true;

                if (!String.IsNullOrEmpty(richiesteCorrispondenza[richiesteCorrispondenza.Count - 1].ImpresaRecapito.Pec))
                {
                    LabelRichiestaPEC.Visible = true;
                }
            }
        }

        public void Reset()
        {
            RadMultiPageSedeLegale.SelectedIndex = Indicedati;
            RadMultiPageSedeAmministrativa.SelectedIndex = Indicedati;
            RadMultiPageCorrispondenza.SelectedIndex = Indicedati;

            AbilitaSedeLegale(true);
            AbilitaSedeAmministrativa(true);
            AbilitaCorrispondenza(true);
        }

        private ImpresaRichiestaVariazioneRecapiti CreaRichiestaIndirizzo(Indirizzo indirizzo, TipoRecapito tipoRecapito)
        {
            ImpresaRichiestaVariazioneRecapiti impresaRichiestaVariazioneRecapiti =
                new ImpresaRichiestaVariazioneRecapiti();

            impresaRichiestaVariazioneRecapiti.ImpresaRecapito = new ImpresaRecapito();
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.IdTipoRecapito = (int)tipoRecapito;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Cap = indirizzo != null ? indirizzo.Cap : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Civico = indirizzo != null ? indirizzo.Civico : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.ComuneCodiceCatastale = indirizzo != null
                                                                                           ? indirizzo.
                                                                                                 ComuneCodiceCatastale
                                                                                           : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Comune = indirizzo != null ? indirizzo.Comune : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Provincia = indirizzo != null
                                                                               ? indirizzo.Provincia
                                                                               : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Via = indirizzo != null ? indirizzo.Via : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Latitudine = indirizzo != null
                                                                                ? indirizzo.Latitudine
                                                                                : null;
            impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Longitudine = indirizzo != null
                                                                                 ? indirizzo.Longitudine
                                                                                 : null;

            impresaRichiestaVariazioneRecapiti.IdImpresa = Convert.ToInt32(RadTextBoxIdImpresa.Text);
            impresaRichiestaVariazioneRecapiti.IdUtente = GestioneUtentiBiz.GetIdUtente();
            impresaRichiestaVariazioneRecapiti.DataRichiesta = DateTime.Now;
            impresaRichiestaVariazioneRecapiti.Gestito = false; //assegnazione superflua, solo per "logica"

            return impresaRichiestaVariazioneRecapiti;
        }

        private void AbilitazionePEC(Boolean abilita, Boolean conferma)
        {
            if (abilita)
            {
                tablePEC.Attributes.Add("class", "borderedTableSelected");
                RadTextBoxSedeLegalePEC.Enabled = true;
                RadTextBoxSedeLegalePEC.ReadOnly = false;

                if (ViewState["PECVariata"] != null)
                {
                    RadTextBoxSedeLegalePEC.Text = ViewState["PECVariata"] as String;
                }

                if (LabelRichiestaPEC.Visible)
                {
                    ViewState["richiestaPEC"] = true;
                }

                LabelRichiestaPEC.Visible = false;
            }
            else
            {
                tablePEC.Attributes.Add("class", String.Empty);
                RadTextBoxSedeLegalePEC.ReadOnly = true;
                RadTextBoxSedeLegalePEC.Enabled = false;

                if (ViewState["richiestaPEC"] != null)
                {
                    LabelRichiestaPEC.Visible = true;
                }

                if (!conferma)
                {
                    if (ViewState["PEC"] != null)
                    {
                        RadTextBoxSedeLegalePEC.Text = ViewState["PEC"] as String;
                    }
                    else
                    {
                        Presenter.SvuotaCampo(RadTextBoxSedeLegalePEC);
                    }
                }
            }
        }

        #region Metodi Sede Legale

        private void AbilitaSedeLegale(Boolean abilitato)
        {
            ButtonModificaSedeLegale.Enabled = abilitato;
        }

        protected void ButtonSalvaSedeLegale_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ViewState["ControlliSedeLegaleNonBloccanti"] == null)
                {
                    Page.Validate("controlliSedeLegaleNonBloccanti");

                    if (!Page.IsValid)
                    {
                        ViewState["ControlliSedeLegaleNonBloccanti"] = true;
                        return;
                    }
                }

                RadMultiPageSedeLegale.SelectedIndex = Indicemodificaincorso;

                Indirizzo indirizzo = SelezioneIndirizzo1.GetIndirizzo();

                ImpresaRichiestaVariazioneRecapiti impresaRichiestaVariazioneRecapiti = CreaRichiestaIndirizzo(indirizzo,
                                                                                                      TipoRecapito.Legale);

                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Presso = RadTextBoxSedeLegalePressoModifica.Text.Trim();

                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Email =
                    !string.IsNullOrEmpty(RadTextBoxSedeLegaleEmailModifica.Text.Trim())
                        ? RadTextBoxSedeLegaleEmailModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Telefono =
                    !string.IsNullOrEmpty(RadTextBoxSedeLegaleTelefonoModifica.Text.Trim())
                        ? RadTextBoxSedeLegaleTelefonoModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Fax =
                    !string.IsNullOrEmpty(RadTextBoxSedeLegaleFaxModifica.Text.Trim())
                        ? RadTextBoxSedeLegaleFaxModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec =
                    !string.IsNullOrEmpty(RadTextBoxSedeLegalePEC.Text.Trim())
                        ? RadTextBoxSedeLegalePEC.Text.Trim()
                        : null;

                if (!String.IsNullOrEmpty(impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec))
                {
                    ViewState["PECVariata"] = impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec;
                }

                _impresaManager.SaveRichiestaVariazioneRecapiti(impresaRichiestaVariazioneRecapiti);

                AbilitaSedeLegale(true);
                AbilitaSedeAmministrativa(true);
                AbilitaCorrispondenza(true);

                AbilitazionePEC(false, true);

                ViewState["ControlliSedeLegaleNonBloccanti"] = null;
            }
        }

        protected void ButtonAnnullaSedeLegale_Click(object sender, EventArgs e)
        {
            AbilitaSedeLegale(true);
            AbilitaSedeAmministrativa(true);
            AbilitaCorrispondenza(true);
            RadMultiPageSedeLegale.SelectedIndex = Indicedati;

            AbilitazionePEC(false, false);
        }

        protected void ButtonModificaSedeLegale_Click(object sender, EventArgs e)
        {
            AbilitaSedeAmministrativa(false);
            AbilitaCorrispondenza(false);
            RadMultiPageSedeLegale.SelectedIndex = Indicemodifica;

            AbilitazionePEC(true, false);
        }

        #endregion

        #region  Metodi Sede Amministrativa

        private void AbilitaSedeAmministrativa(Boolean abilitato)
        {
            ButtonModificaSedeAmministrativa.Enabled = abilitato;
        }

        protected void ButtonSalvaSedeAmministrativa_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ViewState["ControlliSedeAmministrativaNonBloccanti"] == null)
                {
                    Page.Validate("controlliSedeAmministrativaNonBloccanti");

                    if (!Page.IsValid)
                    {
                        ViewState["ControlliSedeAmministrativaNonBloccanti"] = true;
                        return;
                    }
                }

                RadMultiPageSedeAmministrativa.SelectedIndex = Indicemodificaincorso;

                Indirizzo indirizzo = SelezioneIndirizzo2.GetIndirizzo();

                ImpresaRichiestaVariazioneRecapiti impresaRichiestaVariazioneRecapiti = CreaRichiestaIndirizzo(indirizzo,
                                                                                                      TipoRecapito.
                                                                                                          Amministrativo);

                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Presso = RadTextBoxSedeAmministrativaPressoModifica.Text.Trim();

                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Email =
                    !string.IsNullOrEmpty(RadTextBoxSedeAmministrativaEmailModifica.Text.Trim())
                        ? RadTextBoxSedeAmministrativaEmailModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Telefono =
                    !string.IsNullOrEmpty(RadTextBoxSedeAmministrativaTelefonoModifica.Text.Trim())
                        ? RadTextBoxSedeAmministrativaTelefonoModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Fax =
                    !string.IsNullOrEmpty(RadTextBoxSedeAmministrativaFaxModifica.Text.Trim())
                        ? RadTextBoxSedeAmministrativaFaxModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec =
                    !string.IsNullOrEmpty(RadTextBoxSedeLegalePEC.Text.Trim())
                        ? RadTextBoxSedeLegalePEC.Text.Trim()
                        : null;

                if (!String.IsNullOrEmpty(impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec))
                {
                    ViewState["PECVariata"] = impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec;
                }

                _impresaManager.SaveRichiestaVariazioneRecapiti(impresaRichiestaVariazioneRecapiti);

                AbilitaSedeLegale(true);
                AbilitaSedeAmministrativa(true);
                AbilitaCorrispondenza(true);

                AbilitazionePEC(false, true);

                ViewState["ControlliSedeAmministrativaNonBloccanti"] = null;
            }
        }

        protected void ButtonAnnullaSedeAmministrativa_Click(object sender, EventArgs e)
        {
            AbilitaSedeLegale(true);
            AbilitaSedeAmministrativa(true);
            AbilitaCorrispondenza(true);
            RadMultiPageSedeAmministrativa.SelectedIndex = Indicedati;

            AbilitazionePEC(false, false);
        }

        protected void ButtonModificaSedeAmministrativa_Click(object sender, EventArgs e)
        {
            AbilitaSedeLegale(false);
            AbilitaCorrispondenza(false);
            RadMultiPageSedeAmministrativa.SelectedIndex = Indicemodifica;

            AbilitazionePEC(true, false);
        }

        #endregion

        #region Metodi Corrispondenza

        private void AbilitaCorrispondenza(Boolean abilitato)
        {
            ButtonModificaCorrispondenza.Enabled = abilitato;
        }

        protected void ButtonSalvaCorrispondenza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ViewState["ControlliCorrispondenzaNonBloccanti"] == null)
                {
                    Page.Validate("controlliCorrispondenzaNonBloccanti");

                    if (!Page.IsValid)
                    {
                        ViewState["ControlliCorrispondenzaNonBloccanti"] = true;
                        return;
                    }
                }

                RadMultiPageCorrispondenza.SelectedIndex = Indicemodificaincorso;

                Indirizzo indirizzo = SelezioneIndirizzo3.GetIndirizzo();

                ImpresaRichiestaVariazioneRecapiti impresaRichiestaVariazioneRecapiti = CreaRichiestaIndirizzo(indirizzo,
                                                                                                      TipoRecapito.
                                                                                                          Corrispondenza);

                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Presso = RadTextBoxCorrispondenzaPressoModifica.Text.Trim();

                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Email =
                    !string.IsNullOrEmpty(RadTextBoxCorrispondenzaEmailModifica.Text.Trim())
                        ? RadTextBoxCorrispondenzaEmailModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Telefono =
                    !string.IsNullOrEmpty(RadTextBoxCorrispondenzaTelefonoModifica.Text.Trim())
                        ? RadTextBoxCorrispondenzaTelefonoModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Fax =
                    !string.IsNullOrEmpty(RadTextBoxCorrispondenzaFaxModifica.Text.Trim())
                        ? RadTextBoxCorrispondenzaFaxModifica.Text.Trim()
                        : null;
                impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec =
                    !string.IsNullOrEmpty(RadTextBoxSedeLegalePEC.Text.Trim())
                        ? RadTextBoxSedeLegalePEC.Text.Trim()
                        : null;

                if (!String.IsNullOrEmpty(impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec))
                {
                    ViewState["PECVariata"] = impresaRichiestaVariazioneRecapiti.ImpresaRecapito.Pec;
                }

                _impresaManager.SaveRichiestaVariazioneRecapiti(impresaRichiestaVariazioneRecapiti);

                AbilitaSedeLegale(true);
                AbilitaSedeAmministrativa(true);
                AbilitaCorrispondenza(true);

                AbilitazionePEC(false, true);

                ViewState["ControlliCorrispondenzaNonBloccanti"] = null;
            }
        }

        protected void ButtonAnnullaCorrispondenza_Click(object sender, EventArgs e)
        {
            AbilitaSedeLegale(true);
            AbilitaSedeAmministrativa(true);
            AbilitaCorrispondenza(true);
            RadMultiPageCorrispondenza.SelectedIndex = Indicedati;

            AbilitazionePEC(false, false);
        }

        protected void ButtonModificaCorrispondenza_Click(object sender, EventArgs e)
        {
            AbilitaSedeLegale(false);
            AbilitaSedeAmministrativa(false);
            RadMultiPageCorrispondenza.SelectedIndex = Indicemodifica;

            AbilitazionePEC(true, false);
        }

        protected void CustomValidatorIndirizzoSedeLegale_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = SelezioneIndirizzo1.IndirizzoConfermato();
        }

        protected void CustomValidatorIndirizzoSedeAmministrativa_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = SelezioneIndirizzo2.IndirizzoConfermato();
        }

        protected void CustomValidatorIndirizzoCorrispondenza_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = SelezioneIndirizzo3.IndirizzoConfermato();
        }

        #endregion

        protected void CustomValidatorSedeLegaleEmailModifica_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Int32 idImpresa = Convert.ToInt32(RadTextBoxIdImpresa.Text);
            String email = RadTextBoxSedeLegaleEmailModifica.Text;

            if (_impresaManager.EmailUgualeAConsulente(idImpresa, email))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorSedeAmministrativaEmailModifica_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Int32 idImpresa = Convert.ToInt32(RadTextBoxIdImpresa.Text);
            String email = RadTextBoxSedeAmministrativaEmailModifica.Text;

            if (_impresaManager.EmailUgualeAConsulente(idImpresa, email))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorCorrispondenzaEmailModifica_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Int32 idImpresa = Convert.ToInt32(RadTextBoxIdImpresa.Text);
            String email = RadTextBoxCorrispondenzaEmailModifica.Text;

            if (_impresaManager.EmailUgualeAConsulente(idImpresa, email))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorPEC_ServerValidate(object source, ServerValidateEventArgs args)
        {
            String naturaGiuridica = ViewState["NaturaGiuridica"] as String;
            args.IsValid = true;

            if (naturaGiuridica != "01")
            {
                if (String.IsNullOrWhiteSpace(RadTextBoxSedeLegalePEC.Text))
                {
                    args.IsValid = false;
                }
            }
        }
    }
}