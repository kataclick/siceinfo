﻿using System;
using Cemi.SiceInfo.Business.Imp;
using Cemi.SiceInfo.Business.Imp.ImpreseVariazioneStato;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Delegates;
using TBridge.Cemi.Type.Filters;
using Telerik.Web.UI;
using ImpresaRichiestaVariazioneStatoFilter = Cemi.SiceInfo.Type.Dto.Filters.ImpresaRichiestaVariazioneStatoFilter;

namespace Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.WebControls
{
    public partial class RichiesteCambioStatoImpresa : System.Web.UI.UserControl
    {
        private readonly ImpresaVariazioneStatoManager _impresaVariazioneStatoManager = new ImpresaVariazioneStatoManager();
        public event RichiestaVariazioneStatoImpresaSelectedEventHandler OnRichiestaSelected;
        public event RicercaRichiesteVariazioneStatoImpresaSelectedEventHandler OnRicercaRichiestaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaTipoStatoImpresa();
                CaricaTipoStatoRichiesta();
                RadDatePickerDataRichiestaDa.SelectedDate = DateTime.Today.AddMonths(-1);
            }
        }

        private void CaricaTipoStatoImpresa()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                RadComboBoxStatoImpresa,
                _impresaVariazioneStatoManager.GetTipiStatoImpresa(),
                "Descrizione",
                "Id");
        }

        private void CaricaTipoStatoRichiesta()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                RadComboBoxStatoRichiesta,
                _impresaVariazioneStatoManager.GetTipiStatoGestionePratica(),
                "Descrizione",
                "Id");
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaRicerca();

                if (OnRicercaRichiestaSelected != null)
                {
                    OnRicercaRichiestaSelected();
                }
            }
        }

        public void CaricaRicerca()
        {
            ImpresaRichiestaVariazioneStatoFilter filtro = CreaFiltro();

            Presenter.CaricaElementiInGridView(
                RadGridRichiesteCambioStato,
                _impresaVariazioneStatoManager.GetImpreseRichiesteVariazioneStato(filtro));
        }

        public ImpresaRichiestaVariazioneStatoFilter CreaFiltro()
        {
            ImpresaRichiestaVariazioneStatoFilter filtro = new ImpresaRichiestaVariazioneStatoFilter();

            if (!String.IsNullOrEmpty(RadTextBoxCodiceImpresa.Text))
            {
                filtro.IdImpresa = Int32.Parse(RadTextBoxCodiceImpresa.Text);
            }

            if (!String.IsNullOrEmpty(RadTextBoxRagioneSociale.Text))
            {
                filtro.RagioneSociale = RadTextBoxRagioneSociale.Text;
            }
            filtro.DataRichiestaDa = RadDatePickerDataRichiestaDa.SelectedDate;
            filtro.DataRichiestaA = RadDatePickerDataRichiestaA.SelectedDate;

            if (!String.IsNullOrEmpty(RadComboBoxStatoRichiesta.SelectedValue))
            {
                filtro.IdTipoStatoGestionePratica = Int32.Parse(RadComboBoxStatoRichiesta.SelectedValue);
            }

            if (!String.IsNullOrEmpty(RadComboBoxStatoImpresa.SelectedValue))
            {
                filtro.IdTipoStatoImpresa = Int32.Parse(RadComboBoxStatoImpresa.SelectedValue);
            }

            filtro.DataInizioCambioStato = RadDatePickerDataInizioCambioStato.SelectedDate;

            return filtro;
        }

        protected void RadGridRichiesteCambioStato_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 indiceSelezionato = Int32.Parse(RadGridRichiesteCambioStato.SelectedIndexes[0]);
            Int32 idRichiesta =
                (Int32)RadGridRichiesteCambioStato.MasterTableView.DataKeyValues[indiceSelezionato]["Id"];

            if (OnRichiestaSelected != null)
            {
                OnRichiestaSelected(idRichiesta);
            }
        }

        protected void RadGridRichiesteCambioStato_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            CaricaRicerca();
        }
    }
}