﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Recapiti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.WebControls.Recapiti" %>

<%@ Register Src="../../WebControls/SelezioneIndirizzo.ascx" TagName="SelezioneIndirizzo"
    TagPrefix="uc1" %>
<table class="standardTable">
    <tr>
        <td>
            <div>
                <table class="borderedTable">
                    <tr>
                        <td class="anagraficaImpreseTd">
                            <b>Codice Impresa</b>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxIdImpresa" runat="server" EmptyMessage="Seleziona un'impresa"
                                Width="250px" Enabled="False" ReadOnly="True" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="anagraficaImpreseTd">
                            <b>Ragione Sociale</b>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxRagionesociale" runat="server" EmptyMessage="Seleziona un'impresa"
                                Width="250px" Enabled="False" ReadOnly="True" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="anagraficaImpreseTd">
                            <b>Codice Fiscale/P. IVA</b>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxCf" runat="server" EmptyMessage="Seleziona un'impresa"
                                Width="250px" Enabled="False" ReadOnly="True" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table id="tablePEC" runat="server" width="100%" cellspacing="1" cellpadding="0">
                                <tr>
                                    <td class="anagraficaImpreseTd">
                                        <b>PEC</b>
                                        <asp:Label ID="LabelRichiestaPEC" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                                    </td>
                                    <td>
                                        <telerik:RadTextBox ID="RadTextBoxSedeLegalePEC" runat="server" Width="250px" Enabled="False"
                                            ReadOnly="True" />
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorSedeLegalePEC"
                                            runat="server"
                                            ControlToValidate="RadTextBoxSedeLegalePEC"
                                            ValidationGroup="controlliSedeLegale"
                                            ErrorMessage="Inserire l'indirizzo di posta elettronica certificata">
                                            *
                                        </asp:RequiredFieldValidator>
                                        <%--<asp:CustomValidator
                                            ID="CustomValidatorSedeLegalePEC"
                                            runat="server"
                                            ValidationGroup="controlliSedeLegale"
                                            ErrorMessage="Inserire l'indirizzo di posta elettronica certificata" 
                                            onservervalidate="CustomValidatorPEC_ServerValidate">
                                            *
                                        </asp:CustomValidator>--%>
                                        <asp:RegularExpressionValidator 
                                            ID="RegularExpressionValidatorSedeLegalePEC" 
                                            runat="server" 
                                            ControlToValidate="RadTextBoxSedeLegalePEC"
                                            ErrorMessage="Indirizzo di posta elettornica certificata non valido" 
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="controlliSedeLegale" 
                                            CssClass="messaggiErrore">
                                            *
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorSedeAmmiPEC"
                                            runat="server"
                                            ControlToValidate="RadTextBoxSedeLegalePEC"
                                            ValidationGroup="controlliSedeAmministrativa"
                                            ErrorMessage="Inserire l'indirizzo di posta elettronica certificata">
                                            *
                                        </asp:RequiredFieldValidator>
                                        <%--<asp:CustomValidator
                                            ID="CustomValidatorSedeAmmiPEC"
                                            runat="server"
                                            ValidationGroup="controlliSedeAmministrativa"
                                            ErrorMessage="Inserire l'indirizzo di posta elettronica certificata" 
                                            onservervalidate="CustomValidatorPEC_ServerValidate">
                                            *
                                        </asp:CustomValidator>--%>
                                        <asp:RegularExpressionValidator 
                                            ID="RegularExpressionValidatorSedeAmmiPEC" 
                                            runat="server" 
                                            ControlToValidate="RadTextBoxSedeLegalePEC"
                                            ErrorMessage="Indirizzo di posta elettornica certificata non valido" 
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="controlliSedeAmministrativa" 
                                            CssClass="messaggiErrore">
                                            *
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator
                                            ID="RequiredFieldValidatorCorriPEC"
                                            runat="server"
                                            ControlToValidate="RadTextBoxSedeLegalePEC"
                                            ValidationGroup="controlliCorrispondenza"
                                            ErrorMessage="Inserire l'indirizzo di posta elettronica certificata">
                                            *
                                        </asp:RequiredFieldValidator>
                                        <%--<asp:CustomValidator
                                            ID="CustomValidatorCorriPEC"
                                            runat="server"
                                            ValidationGroup="controlliCorrispondenza"
                                            ErrorMessage="Inserire l'indirizzo di posta elettronica certificata" 
                                            onservervalidate="CustomValidatorPEC_ServerValidate">
                                            *
                                        </asp:CustomValidator>--%>
                                        <asp:RegularExpressionValidator 
                                            ID="RegularExpressionValidatorCorriPEC" 
                                            runat="server" 
                                            ControlToValidate="RadTextBoxSedeLegalePEC"
                                            ErrorMessage="Indirizzo di posta elettornica certificata non valido" 
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ValidationGroup="controlliCorrispondenza" 
                                            CssClass="messaggiErrore">
                                            *
                                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadMultiPage ID="RadMultiPageSedeLegale" runat="server" Width="100%" SelectedIndex="0"
                RenderSelectedPageOnly="true">
                <telerik:RadPageView ID="RadPageViewSedeLegaleDati" runat="server">
                    <div>
                        <table class="borderedTable">
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    <b>Sede Legale</b> <asp:Label ID="LabelRichiestaLegale" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaSedeLegale" runat="server" CausesValidation="false" Text="Modifica" OnClick="ButtonModificaSedeLegale_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Indirizzo
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeLegaleIndirizzo" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" TextMode="MultiLine" Style="overflow: hidden" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Presso
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeLegalePresso" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Telefono
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeLegaleTelefono" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Fax
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeLegaleFax" runat="server" Width="250px" Enabled="False"
                                        ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Email
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeLegaleEmail" runat="server" Width="250px" Enabled="False"
                                        ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewSedeLegaleModifica" runat="server">
                    <div>
                        <table class="borderedTableSelected">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                <b>Sede Legale</b>
                                            </td>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:SelezioneIndirizzo ID="SelezioneIndirizzo1" runat="server" />
                                    <asp:CustomValidator ID="CustomValidatorIndirizzoSedeLegale" runat="server" ValidationGroup="controlliSedeLegale"
                                        OnServerValidate="CustomValidatorIndirizzoSedeLegale_ServerValidate" ErrorMessage="Confermare l'indirizzo">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="borderedTableSelected">
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Presso
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeLegalePressoModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Telefono*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeLegaleTelefonoModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="RadTextBoxSedeLegaleTelefonoModifica"
                                                    ErrorMessage="Il telefono inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliSedeLegale"
                                                    CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSedeLegaleTelefonoModifica"
                                                    runat="server" ControlToValidate="RadTextBoxSedeLegaleTelefonoModifica" ErrorMessage="Inserire un telefono per la sede legale"
                                                    ValidationGroup="controlliSedeLegale">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Fax*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeLegaleFaxModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="RadTextBoxSedeLegaleFaxModifica"
                                                    ErrorMessage="Il fax inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliSedeLegale"
                                                    CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSedeLegaleFaxModifica" runat="server"
                                                    ControlToValidate="RadTextBoxSedeLegaleFaxModifica" ErrorMessage="Inserire un fax per la sede legale"
                                                    ValidationGroup="controlliSedeLegale">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Email*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeLegaleEmailModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="RadTextBoxSedeLegaleEmailModifica"
                                                    ErrorMessage="Indirizzo Email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="controlliSedeLegale" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSedeLegaleEmailModifica" runat="server"
                                                    ControlToValidate="RadTextBoxSedeLegaleEmailModifica" ErrorMessage="Inserire una email per la sede legale"
                                                    ValidationGroup="controlliSedeLegale">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidatorSedeLegaleEmailModifica" runat="server"
                                                    ControlToValidate="RadTextBoxSedeLegaleEmailModifica" ErrorMessage="L'indirizzo email fornito per la sede legale corrisponde a quello del consulente. E' richiesto l'indirizzo email dell'impresa. Se si vuole procedere comunque premere nuovamente il bottone 'Salva'."
                                                    ValidationGroup="controlliSedeLegaleNonBloccanti" 
                                                    onservervalidate="CustomValidatorSedeLegaleEmailModifica_ServerValidate">*</asp:CustomValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                            <td>
                                                <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" CssClass="messaggiErrore"
                                                    ValidationGroup="controlliSedeLegale" />
                                                <asp:ValidationSummary ID="ValidationSummaryErroriNonBloccanti" runat="server" CssClass="messaggiErrore"
                                                    ValidationGroup="controlliSedeLegaleNonBloccanti" />
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="ButtonSalvaSedeLegale" runat="server" Text="Salva" OnClick="ButtonSalvaSedeLegale_Click"
                                                                ValidationGroup="controlliSedeLegale" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="ButtonAnnullaSedeLegale" runat="server" Text="Annulla" OnClick="ButtonAnnullaSedeLegale_Click"
                                                                CausesValidation="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewSedeLegaleModificaInCorso" runat="server">
                    <div>
                        <table class="borderedTable">
                            <tr>
                                <td>
                                    <b>Sede legale</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    La modifica eseguita è stata acquisita correttamente. E’ in corso l’aggiornamento
                                    dei dati che saranno resi disponibili a breve.
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadMultiPage ID="RadMultiPageSedeAmministrativa" runat="server" Width="100%"
                SelectedIndex="0" RenderSelectedPageOnly="true">
                <telerik:RadPageView ID="RadPageViewSedeAmministrativaDati" runat="server">
                    <div>
                        <table class="borderedTable">
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    <b>Sede Amministrativa</b> <asp:Label ID="LabelRichiestaAmministrativa" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaSedeAmministrativa" runat="server" Text="Modifica"
                                        OnClick="ButtonModificaSedeAmministrativa_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Indirizzo
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaIndirizzo" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" TextMode="MultiLine" Style="overflow: hidden" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Presso
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaPresso" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Telefono
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaTelefono" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Fax
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaFax" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Email
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaEmail" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewSedeAmministrativaModifica" runat="server">
                    <div>
                        <table class="borderedTableSelected">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                <b>Sede Amministrativa</b>
                                            </td>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:SelezioneIndirizzo ID="SelezioneIndirizzo2" runat="server" />
                                    <asp:CustomValidator ID="CustomValidatorIndirizzoSedeAmministrativa" runat="server"
                                        ValidationGroup="controlliSedeAmministrativa" OnServerValidate="CustomValidatorIndirizzoSedeAmministrativa_ServerValidate"
                                        ErrorMessage="Confermare l'indirizzo">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="borderedTableSelected">
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Presso
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaPressoModifica" runat="server"
                                                    Width="250px" Enabled="True" ReadOnly="False" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Telefono*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaTelefonoModifica" runat="server"
                                                    Width="250px" Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="RadTextBoxSedeAmministrativaTelefonoModifica"
                                                    ErrorMessage="Il telefono inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliSedeAmministrativa"
                                                    CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSedeAmministrativaTelefonoModifica"
                                                    runat="server" ControlToValidate="RadTextBoxSedeAmministrativaTelefonoModifica"
                                                    ErrorMessage="Inserire un telefono per la sede amministrativa" ValidationGroup="controlliSedeAmministrativa">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Fax*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaFaxModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="RadTextBoxSedeAmministrativaFaxModifica"
                                                    ErrorMessage="Il fax inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliSedeAmministrativa"
                                                    CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSedeAmministrativaFaxModifica"
                                                    runat="server" ControlToValidate="RadTextBoxSedeAmministrativaFaxModifica" ErrorMessage="Inserire un fax per la sede amministrativa"
                                                    ValidationGroup="controlliSedeAmministrativa">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Email*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxSedeAmministrativaEmailModifica" runat="server"
                                                    Width="250px" Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="RadTextBoxSedeAmministrativaEmailModifica"
                                                    ErrorMessage="Indirizzo Email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="controlliSedeAmministrativa" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorSedeAmministrativaEmailModifica"
                                                    runat="server" ControlToValidate="RadTextBoxSedeAmministrativaEmailModifica"
                                                    ErrorMessage="Inserire una email per la sede amministrativa" ValidationGroup="controlliSedeAmministrativa">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidatorSedeAmministrativaEmailModifica" runat="server"
                                                    ControlToValidate="RadTextBoxSedeAmministrativaEmailModifica" ErrorMessage="L'indirizzo email fornito per la sede amministrativa corrisponde a quello del consulente. E' richiesto l'indirizzo email dell'impresa. Se si vuole procedere comunque premere nuovamente il bottone 'Salva'."
                                                    ValidationGroup="controlliSedeAmministrativaNonBloccanti" 
                                                    onservervalidate="CustomValidatorSedeAmministrativaEmailModifica_ServerValidate">*</asp:CustomValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                            <td>
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="messaggiErrore"
                                                    ValidationGroup="controlliSedeAmministrativa" />
                                                <asp:ValidationSummary ID="ValidationSummary2ErroriNonBloccanti" runat="server" CssClass="messaggiErrore"
                                                    ValidationGroup="controlliSedeAmministrativaNonBloccanti" />
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="ButtonSalvaSedeAmministrativa" runat="server" Text="Salva" OnClick="ButtonSalvaSedeAmministrativa_Click"
                                                                ValidationGroup="controlliSedeAmministrativa" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="ButtonAnnullaSedeAmministrativa" runat="server" Text="Annulla" OnClick="ButtonAnnullaSedeAmministrativa_Click"
                                                                CausesValidation="False" ValidationGroup="controlliSedeAmministrativa" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewSedeAmministrativaModificaInCorso" runat="server">
                    <div>
                        <table class="borderedTable">
                            <tr>
                                <td>
                                    <b>Sede Amministrativa</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    La modifica eseguita è stata acquisita correttamente. E’ in corso l’aggiornamento
                                    dei dati che saranno resi disponibili a breve.
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadMultiPage ID="RadMultiPageCorrispondenza" runat="server" Width="100%"
                SelectedIndex="0" RenderSelectedPageOnly="true">
                <telerik:RadPageView ID="RadPageViewCorrispondenzaDati" runat="server">
                    <div>
                        <table class="borderedTable">
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    <b>Corrispondenza</b> <asp:Label ID="LabelRichiestaCorrispondeza" runat="server" Text="(Variazione in corso)" ForeColor="Red" Font-Italic="true" Visible="false" />
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaCorrispondenza" runat="server" Text="Modifica" OnClick="ButtonModificaCorrispondenza_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Indirizzo
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCorrispondenzaIndirizzo" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" TextMode="MultiLine" Style="overflow: hidden" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Presso
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCorrispondenzaPresso" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Telefono
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCorrispondenzaTelefono" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Fax
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCorrispondenzaFax" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="anagraficaImpreseTd">
                                    Email
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="RadTextBoxCorrispondenzaEmail" runat="server" Width="250px"
                                        Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewCorrispondenzaModifica" runat="server">
                    <div>
                        <table class="borderedTableSelected">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                <b>Corrispondenza</b>
                                            </td>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:SelezioneIndirizzo ID="SelezioneIndirizzo3" runat="server" />
                                    <asp:CustomValidator ID="CustomValidatorIndirizzoCorrispondenza" runat="server" ValidationGroup="controlliCorrispondenza"
                                        OnServerValidate="CustomValidatorIndirizzoCorrispondenza_ServerValidate" ErrorMessage="Confermare l'indirizzo">
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="borderedTableSelected">
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Presso
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxCorrispondenzaPressoModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Telefono*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxCorrispondenzaTelefonoModifica" runat="server"
                                                    Width="250px" Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="RadTextBoxCorrispondenzaTelefonoModifica"
                                                    ErrorMessage="Il telefono inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliCorrispondenza"
                                                    CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCorrispondenzaTelefonoModifica"
                                                    runat="server" ControlToValidate="RadTextBoxCorrispondenzaTelefonoModifica" ErrorMessage="Inserire un telefono per la corrispondenza"
                                                    ValidationGroup="controlliCorrispondenza">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Fax*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxCorrispondenzaFaxModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="RadTextBoxCorrispondenzaFaxModifica"
                                                    ErrorMessage="Il fax inserito non è valido" ValidationExpression="\d{6,}" ValidationGroup="controlliCorrispondenza"
                                                    CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCorrispondenzaFaxModifica"
                                                    runat="server" ControlToValidate="RadTextBoxCorrispondenzaFaxModifica" ErrorMessage="Inserire un fax per la corrispondenza"
                                                    ValidationGroup="controlliCorrispondenza">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                                Email*
                                            </td>
                                            <td>
                                                <telerik:RadTextBox ID="RadTextBoxCorrispondenzaEmailModifica" runat="server" Width="250px"
                                                    Enabled="True" ReadOnly="False" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="RadTextBoxCorrispondenzaEmailModifica"
                                                    ErrorMessage="Indirizzo Email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="controlliCorrispondenza" CssClass="messaggiErrore">*</asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCorrispondenzaEmailModifica"
                                                    runat="server" ControlToValidate="RadTextBoxCorrispondenzaEmailModifica" ErrorMessage="Inserire una email per la corrispondenza"
                                                    ValidationGroup="controlliCorrispondenza">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="CustomValidatorCorrispondenzaEmailModifica" runat="server"
                                                    ControlToValidate="RadTextBoxCorrispondenzaEmailModifica" ErrorMessage="L'indirizzo email fornito per la corrispondenza corrisponde a quello del consulente. E' richiesto l'indirizzo email dell'impresa. Se si vuole procedere comunque premere nuovamente il bottone 'Salva'."
                                                    ValidationGroup="controlliCorrispondenzaNonBloccanti" 
                                                    onservervalidate="CustomValidatorCorrispondenzaEmailModifica_ServerValidate">*</asp:CustomValidator>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="anagraficaImpreseTd">
                                            </td>
                                            <td>
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="messaggiErrore"
                                                    ValidationGroup="controlliCorrispondenza" />
                                                <asp:ValidationSummary ID="ValidationSummary1NonBloccanti" runat="server" CssClass="messaggiErrore"
                                                    ValidationGroup="controlliCorrispondenzaNonBloccanti" />
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="ButtonSalvaCorrispondenza" runat="server" Text="Salva" OnClick="ButtonSalvaCorrispondenza_Click"
                                                                ValidationGroup="controlliCorrispondenza" />
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="ButtonAnnullaCorrispondenza" runat="server" Text="Annulla" OnClick="ButtonAnnullaCorrispondenza_Click"
                                                                CausesValidation="False" ValidationGroup="controlliCorrispondenza" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageViewCorrispondenzaModificaInCorso" runat="server">
                    <div>
                        <table class="borderedTable">
                            <tr>
                                <td>
                                    <b>Corrispondenza</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    La modifica eseguita è stata acquisita correttamente. E’ in corso l’aggiornamento
                                    dei dati che saranno resi disponibili a breve.
                                </td>
                            </tr>
                        </table>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </td>
    </tr>
</table>
