﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneRecapiti.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.GestioneRecapiti" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/Recapiti.ascx" TagName="Recapiti"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa"
    TagPrefix="uc3" %>
<%@ Register src="../WebControls/MenuAnagraficaImpreseVariazioni.ascx" tagname="MenuAnagraficaImpreseVariazioni" tagprefix="uc4" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Recapiti Impresa"
        sottoTitolo="Richiesta variazione" />
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <table class="borderedTable">
            <tr>
                <td>
                    In questa pagina l’impresa può effettuare variazioni relative ai dati della propria
                    sede legale, amministrativa e di corrispondenza. Il consulente delegato, previa
                    selezione dell’impresa interessata, può operare in nome e per conto di quest’ultima
                    le modifiche sopra indicate.
                    <br />
                    <br />
                    I dati possono essere solo variati e non rimossi, ad eccezione dell’informazione
                    contenuta nel campo “presso”. Si precisa, infine, che le modifiche relative al numero
                    di telefono e fax devono contenere solo numeri.
                </td>
            </tr>
        </table>
        <br />
        <uc3:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
        <br />
        <uc2:Recapiti ID="Recapiti1" runat="server" Visible="False" />
    </telerik:RadAjaxPanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc4:MenuAnagraficaImpreseVariazioni ID="MenuAnagraficaImpreseVariazioni1" 
        runat="server" />
</asp:Content>


