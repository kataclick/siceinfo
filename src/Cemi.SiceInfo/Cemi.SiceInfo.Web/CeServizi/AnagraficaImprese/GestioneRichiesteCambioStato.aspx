﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneRichiesteCambioStato.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.GestioneRichiesteCambioStato" %>

<%@ Register Src="WebControls/RichiesteCambioStatoImpresa.ascx" TagName="RichiesteCambioStatoImpresa"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuAnagraficaImpreseVariazioni.ascx" TagName="MenuAnagraficaImpreseVariazioni"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Variazioni Impresa" sottoTitolo="Gestione Variazione Stato" />
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <uc1:RichiesteCambioStatoImpresa ID="RichiesteCambioStatoImpresa1" runat="server" />
        <br />
        <asp:Panel ID="PanelDettaglioRchiesta" runat="server" Visible="false" CssClass="borderedTable">
            <table class="standardTable">
                <tr>
                    <td colspan="4">
                        <b>Dettaglio richiesta variazione stato </b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Id Richiesta:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxIdRichiesta" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Data ultima denuncia:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataUltimaDenuncia" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Data richiesta:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataRichiesta" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Ore ultima denuncia:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxOreUltimaDenuncia" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Utente:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxUtente" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Ore lavorate ultima denuncia:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxOreLavorateUltimaDenuncia" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Impresa:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxImpresa" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Data iscrizione:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataIscrizione" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Stato impresa attuale:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxStatoImpresaAttuale" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Data sospensione:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataSospensione" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Stato impresa richiesto:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxStatoImpresaRichiesto" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Data ripresa:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataRipresa" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Data inizio cambio stato:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDataInizioCambioStato" runat="server" ReadOnly="True" />
                    </td>
                    <td>
                        Debito:
                    </td>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxDebito" runat="server" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Note Impresa:
                    </td>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxNote" runat="server" Width="100%" TextMode="MultiLine" Rows="3" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Note CEMI:
                    </td>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxNoteCemi" runat="server" Width="100%" TextMode="MultiLine" Rows="3" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonNoteCemiSave" runat="server" Text="Salva nota" OnClick="RadButtonSalvaNotaCemi_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Stato pratica:
                    </td>
                    <td colspan="3">
                        <telerik:RadTextBox ID="RadTextBoxStatoPratica" runat="server" ReadOnly="True" Width="160px" />
                    </td>
                </tr>
                <tr>                    
                    <td>
                        Libro unico
                    </td>
                    <td colspan="3">
                        <telerik:RadGrid ID="RadGridLibroUnico" runat="server" AllowPaging="false" OnSelectedIndexChanged="RadGridLibroUnico_SelectedIndexChanged"
                        AutoGenerateColumns="False" GridLines="None">
                            <MasterTableView DataKeyNames="Id,DocumentaleId,Filename" ShowHeader="false">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Filename" HeaderText="Nome File" />
                                    <telerik:GridButtonColumn CommandName="Select" Text="Visualizza" UniqueName="column" ButtonType="PushButton" />
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <asp:Label
                            ID="LabelLibroUnicoNonDisponibile"
                            runat="server"
                            ForeColor="Gray"
                            Text="non disponibile" Visible="false">
                        </asp:Label>
                        
                    </td>

                </tr>
                <tr>
                    <td colspan="4"><hr /></td>
                </tr>
                <tr>
                    <td>
                        UniEmens
                    </td>
                    <td colspan="3">
                        <telerik:RadGrid ID="RadGridUniemens" runat="server" AllowPaging="false" OnSelectedIndexChanged="RadGridUniemens_SelectedIndexChanged"
                        AutoGenerateColumns="False" GridLines="None">
                            <MasterTableView DataKeyNames="Id,DocumentaleId,Filename" ShowHeader="false">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Filename" HeaderText="Nome File" />
                                    <telerik:GridButtonColumn CommandName="Select" Text="Visualizza" UniqueName="column" ButtonType="PushButton" />
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <asp:Label
                            ID="LabelUniemensNonDisponibile"
                            runat="server"
                            ForeColor="Gray"
                            Text="non disponibile" Visible="false">
                        </asp:Label>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="4"><hr /></td>
                </tr>
                <tr>
                    <td>
                        Denunce altre Casse Edili
                    </td>
                    <td colspan="3">
                        <telerik:RadGrid ID="RadGridDenunce" runat="server" AllowPaging="false" OnSelectedIndexChanged="RadGridDenunce_SelectedIndexChanged"
                        AutoGenerateColumns="False" GridLines="None">
                            <MasterTableView DataKeyNames="Id,DocumentaleId,Filename" ShowHeader="false">
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Filename" HeaderText="Nome File" />
                                    <telerik:GridButtonColumn CommandName="Select" Text="Visualizza" UniqueName="column" ButtonType="PushButton" />
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <asp:Label
                            ID="LabelDenunceNonDisponibile"
                            runat="server"
                            ForeColor="Gray"
                            Text="non disponibile" Visible="false">
                        </asp:Label>
                        
                    </td>
                </tr>
                
                <tr>
                    <td colspan="4">
                    </td>
                </tr>
                <tr runat="server" id="RowCausaleRespinta">
                    <td>Motivo Rifiuto &nbsp;</td>
                    <td colspan="3">
                        <telerik:RadTextBox runat="server" TextMode="MultiLine" ID="RadTextBoxCausaleRespinta"  Width="160px" Rows="3" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="RadButtonAccettaRichiesta" runat="server" Text="Accetta" Width="30%" OnClick="RadButtonAccettaRichiesta_Click" />
                        &nbsp;
                        <asp:Button ID="RadButtonSospendiRichiesta" runat="server" Text="Sospendi" Width="30%" OnClick="RadButtonSospendiRichiesta_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <telerik:RadComboBox ID="DropDownListCausaliRespinta"  runat="server" Width="450px" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="DropDownListCausaliRespinta_SelectedIndexChanged" OnItemCreated="DropDownListCausaliRespinta_ItemCreated" />
                        <br />
                        <asp:Button ID="RadButtonRifiutaRichiesta" runat="server" OnClick="RadButtonRifiutaRichiesta_Click" Text="Rifiuta" Width="30%" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </telerik:RadAjaxPanel>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuAnagraficaImpreseVariazioni ID="MenuAnagraficaImpreseVariazioni1" runat="server" />
</asp:Content>
