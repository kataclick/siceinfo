﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneStato.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.AnagraficaImprese.GestioneStato" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa"
    TagPrefix="uc2" %>
<%@ Register src="WebControls/StatoImpresaWC.ascx" tagname="StatoImpresa" tagprefix="uc3" %>
<%@ Register src="../WebControls/MenuAnagraficaImpreseVariazioni.ascx" tagname="MenuAnagraficaImpreseVariazioni" tagprefix="uc4" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Stato Impresa" sottoTitolo="Sospensione/Riattivazione/Cessazione Impresa" />
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <table class="borderedTable">
        <tr>
            <td>
                <p>
                In questa pagina l’impresa o il consulente del lavoro, delegato ad operare in nome e per conto dell’impresa assistita, può richiedere la variazione dell’ultimo stato della posizione aziendale risultante in Cassa Edile, semplicemente selezionando l’opzione desiderata (riattivazione, sospensione) dalla voce “<b>Tipo richiesta</b>” e la data a partire dalla quale far decorrere la modifica, tramite l’utilizzo del calendario (“<b>Data inizio cambio stato</b>”).
                </p>
                <p>
                Il campo note può essere utilizzato per specificare il motivo della richiesta.
                </p>
                <p>
                Per completare l’attività basta, infine, premere il pulsante “<b>Invia richiesta</b>”. La richiesta verrà vagliata dagli incaricati Cassa Edile che forniranno riscontro via e-mail, al richiedente, circa l’esito della valutazione.
                </p>
                <p>
                <b>N.B.</b> In caso di definitiva cessazione dell’attività è necessario darne comunicazione a Cassa Edile inviando via fax al numero 02.58316406 la visura camerale con l’apposita dicitura.
                </p>
            </td>
        </tr>
    </table>
    <br />
    <uc2:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
    <br />
    <uc3:StatoImpresa ID="StatoImpresa1" runat="server" />
    <br />
    </telerik:RadAjaxPanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc4:MenuAnagraficaImpreseVariazioni ID="MenuAnagraficaImpreseVariazioni1" 
        runat="server" />
</asp:Content>
