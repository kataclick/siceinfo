﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class AnagraficaCorsi : System.Web.UI.Page
    {
        private const Int32 INDICEELIMINA = 3;

        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaCorsi(0);
            }
        }

        private void CaricaCorsi(Int32 pagina)
        {
            Presenter.CaricaElementiInGridView(
                GridViewCorsi,
                biz.GetCorsiAll(),
                pagina);
        }

        protected void ButtonNuovoCorso_Click(object sender, EventArgs e)
        {
            PanelNuovoCorso.Visible = true;
            ViewState["Moduli"] = new ModuloCollection();
            CaricaModuli();
        }

        protected void ButtonInserimentoNuovoModulo_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Modulo nuovoModulo = CreaModulo();

                ModuloCollection moduli = (ModuloCollection)ViewState["Moduli"];
                moduli.Add(nuovoModulo);
                ViewState["Moduli"] = moduli;

                TextBoxDurataComplessiva.Text = moduli.DurataComplessiva.ToString();

                PanelNuovoModulo.Visible = false;
                CaricaModuli();
            }
        }

        private void CaricaModuli()
        {
            Presenter.CaricaElementiInGridView(
                GridViewModuli,
                (ModuloCollection)ViewState["Moduli"],
                0);
        }

        private Modulo CreaModulo()
        {
            Modulo modulo = new Modulo();
            ModuloCollection moduli = (ModuloCollection)ViewState["Moduli"];

            modulo.Descrizione = Presenter.NormalizzaCampoTesto(TextBoxModuloDescrizione.Text);
            modulo.OreDurata = Int16.Parse(TextBoxModuloDurata.Text);
            modulo.Progressivo = ((Int16)(moduli.Count + 1));
            SvuotaCampiModulo();

            return modulo;
        }

        private void SvuotaCampiModulo()
        {
            Presenter.SvuotaCampo(TextBoxModuloDescrizione);
            Presenter.SvuotaCampo(TextBoxModuloDurata);
        }

        private void SvuotaCampiCorso()
        {
            Presenter.SvuotaCampo(TextBoxCodice);
            Presenter.SvuotaCampo(TextBoxDescrizione);
            ViewState.Remove("Moduli");
            SvuotaCampiModulo();
        }

        protected void ButtonNuovoModulo_Click(object sender, EventArgs e)
        {
            PanelNuovoModulo.Visible = true;
        }

        private Corso CreaCorso()
        {
            Corso corso = new Corso();

            corso.Codice = Presenter.NormalizzaCampoTesto(TextBoxCodice.Text);
            corso.Descrizione = Presenter.NormalizzaCampoTesto(TextBoxDescrizione.Text);
            if (!String.IsNullOrEmpty(TextBoxVersione.Text))
            {
                corso.Versione = Presenter.NormalizzaCampoTesto(TextBoxVersione.Text);
            }
            corso.DurataComplessiva = Int16.Parse(TextBoxDurataComplessiva.Text);
            corso.Moduli = (ModuloCollection)ViewState["Moduli"];
            corso.IscrizioneLibera = CheckBoxIscrizioneLibera.Checked;

            return corso;
        }

        protected void ButtonInserimentoCorso_Click1(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Corso nuovoCorso = CreaCorso();

                if (biz.InsertCorso(nuovoCorso))
                {
                    SvuotaCampiCorso();

                    PanelNuovoModulo.Visible = false;
                    PanelNuovoCorso.Visible = false;

                    CaricaCorsi(0);
                }
            }
        }

        protected void CustomValidatorModuli_ServerValidate1(object source, ServerValidateEventArgs args)
        {
            ModuloCollection moduli = (ModuloCollection)ViewState["Moduli"];

            if (moduli.Count == 0)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void GridViewCorsi_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Corso corso = (Corso)e.Row.DataItem;
                GridView gvModuli = (GridView)e.Row.FindControl("GridViewModuli");
                Label lDescrizione = (Label)e.Row.FindControl("LabelDescrizione");
                Label lDurata = (Label)e.Row.FindControl("LabelDurata");
                Label lVersione = (Label)e.Row.FindControl("LabelVersione");
                CheckBox cbIscrizioneLibera = (CheckBox)e.Row.FindControl("CheckBoxIscrizioneLibera");

                lDescrizione.Text = corso.Descrizione;
                lDurata.Text = String.Format("{0} ore", corso.DurataComplessiva);
                lVersione.Text = String.Format("Versione {0}", corso.Versione);
                cbIscrizioneLibera.Checked = corso.IscrizioneLibera;

                if (corso.NumeroPianificazioni > 0)
                {
                    e.Row.Cells[INDICEELIMINA].Enabled = false;
                }

                Presenter.CaricaElementiInGridView(
                    gvModuli,
                    corso.Moduli,
                    0);
            }
        }

        protected void GridViewCorsi_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CaricaCorsi(e.NewPageIndex);
        }

        protected void GridViewCorsi_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Int32 idCorso = (Int32)GridViewCorsi.DataKeys[e.RowIndex].Values["IdCorso"];

            if (biz.DeleteCorso(idCorso))
            {
                CaricaCorsi(GridViewCorsi.PageIndex);
            }
        }
    }
}