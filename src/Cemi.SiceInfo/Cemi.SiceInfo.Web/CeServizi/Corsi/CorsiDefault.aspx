﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CorsiDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.CorsiDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc2" %>
<%@ Register Src="WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Benvenuto nella sezione Corsi" />
    <br />
    <uc3:PitHeader ID="PitHeader1" runat="server" />
    <br />
    <p class="DefaultPage" style="text-align:justify">
        Benvenuto nella sezione "Iscrizione Corsi" dove è possibile effettuare l’iscrizione dei lavoratori
        ai moduli formativi proposti dal sistema bilaterale territoriale tramite 
        l&#39;apposito tasto &quot;Iscrizione lavoratori&quot; visibile nel menu verticale.
    </p>
    <p style="text-align:justify">
        Grazie al tasto “Corsi dei lavoratori” è possibile ricercare il corso al quale
        è stato iscritto un lavoratore.
    </p>
    <p>
        <b>
            Corso di formazione 16 ore – MICS Moduli Integrati per Costruire in Sicurezza
        </b>
    </p>
    <p style="text-align:justify">
        Il corso è attinente le basi professionali del lavoro in edilizia e la formazione alla 
        sicurezza ed ha lo scopo di migliorare la professionalità dei lavoratori (lavorare bene 
        e lavorare in sicurezza) e di adempiere agli obblighi di formazione previsti (Contratto 
        Collettivo Nazionale di Lavoro di settore, Decreto Legislativo 9 aprile 2008, n. 81 
        (Testo Unico) e Conferenza Stato Regioni del 21 dicembre 2011).
    </p>
    <p>
        <b>Il modulo formativo si rivolge ai lavoratori</b>:
    </p>
    <ul>
        <li style="text-align:justify">
            al primo ingresso nel settore edile (senza precedenti esperienze di lavoro 
            regolare in cantiere edile);
        </li>
        <li style="text-align:justify">
            già operanti nel settore edile ma sprovvisti di un attestato valido comprovante 
            l’avvenuta formazione di base alla sicurezza della durata di 16 ore
        </li>
    </ul>
    <p style="text-align:justify">
    che devono ricevere una formazione base alla sicurezza della durata di 16 ore. <br />
    Il corso si svolge presso l’Ente Scuola Edile Milanese (ESEM), in collaborazione con Cassa Edile e CPT - Sicurezza in Edilizia.<br />
    Si ricorda che il corso ha una validità di 5 anni entro i quali il lavoratore deve effettuare un aggiornamento pari a 6 ore.<br />
    La partecipazione al corso è totalmente <b>GRATUITA</b>.
    </p>
    <p><b>Lingua di erogazione del corso: ITALIANA</b></p>
</asp:Content>
