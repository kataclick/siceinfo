﻿using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Corsi;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class EstrazioneFormedil : System.Web.UI.Page
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiEstrazioneFormedil);

            #endregion
        }

        protected void CalendarSettimana_SelectionChanged(object sender, EventArgs e)
        {
            if (CalendarSettimana.SelectedDates.Count == 1)
            {
                DateTime giornoSelezionato = CalendarSettimana.SelectedDate;
                DateTime giorno = giornoSelezionato.AddDays(-(double)giornoSelezionato.DayOfWeek + 1);

                CalendarSettimana.SelectedDates.Clear();
                for (Int32 i = 0; i < 7; i++)
                {
                    CalendarSettimana.SelectedDates.Add(giorno);
                    giorno = giorno.AddDays(1);
                }
            }
        }

        private EstrazioneFormedilFilter CreaFiltro()
        {
            EstrazioneFormedilFilter filtro = new EstrazioneFormedilFilter();

            filtro.Dal = CalendarSettimana.SelectedDates[0];
            filtro.Al = CalendarSettimana.SelectedDates[6];

            return filtro;
        }

        protected void ButtonImprese_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                EstrazioneFormedilFilter filtro = CreaFiltro();
                EstrazioneFormedilImpresaCollection estrazioni = biz.EstrazioneFormedilImprese(filtro);

                String stampa = PreparaStampaImprese(estrazioni);
                RestituisciExcel(stampa, "EstrazioneImprese");
            }
        }

        protected void ButtonLavoratori_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                EstrazioneFormedilFilter filtro = CreaFiltro();
                EstrazioneFormedilLavoratoreCollection estrazioni = biz.EstrazioneFormedilLavoratori(filtro);

                String stampa = PreparaStampaLavoratori(estrazioni);
                RestituisciExcel(stampa, "EstrazioneLavoratori");
            }
        }

        private string PreparaStampaLavoratori(EstrazioneFormedilLavoratoreCollection estrazioni)
        {
            GridView gv = new GridView();
            gv.ID = "gvEstrazioni";
            gv.AutoGenerateColumns = false;

            BoundField bc0 = new BoundField();
            bc0.HeaderText = "Corso";
            bc0.DataField = "Corso";
            BoundField bc0bis = new BoundField();
            bc0bis.HeaderText = "Date";
            bc0bis.DataField = "DateCorso";
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Cognome";
            bc1.DataField = "Cognome";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Nome";
            bc2.DataField = "Nome";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "Sesso";
            bc3.DataField = "Sesso";
            BoundField bc3bis = new BoundField();
            bc3bis.HeaderText = "Codice Fiscale";
            bc3bis.DataField = "CodiceFiscale";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Cittadinanza";
            bc4.DataField = "Cittadinanza";
            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Data di nascita";
            bc5.DataField = "DataNascita";
            bc5.HtmlEncode = false;
            bc5.DataFormatString = "{0:dd/MM/yyyy}";
            BoundField bc6 = new BoundField();
            bc6.HeaderText = "Provincia di residenza";
            bc6.DataField = "ProvinciaResidenza";
            BoundField bc8 = new BoundField();
            bc8.HeaderText = "Cod. Titolo di Studio";
            bc8.DataField = "TitoloDiStudio";
            BoundField bc9 = new BoundField();
            bc9.HeaderText = "Denominazione Azienda di appartenenza";
            bc9.DataField = "Azienda";

            gv.Columns.Add(bc0);
            gv.Columns.Add(bc0bis);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc3bis);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            gv.Columns.Add(bc8);
            gv.Columns.Add(bc9);

            gv.DataSource = estrazioni;
            gv.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            return sw.ToString();
        }

        private string PreparaStampaImprese(EstrazioneFormedilImpresaCollection estrazioni)
        {
            GridView gv = new GridView();
            gv.ID = "gvEstrazioni";
            gv.AutoGenerateColumns = false;

            BoundField bc0 = new BoundField();
            bc0.HeaderText = "Corso";
            bc0.DataField = "Corso";
            BoundField bc0bis = new BoundField();
            bc0bis.HeaderText = "Date";
            bc0bis.DataField = "DateCorso";
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Denominazione Azienda";
            bc1.DataField = "RagioneSociale";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Codice Fiscale";
            bc2.DataField = "CodiceFiscale";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "Codice Tipo Contratto applicato";
            bc3.DataField = "TipoContratto";
            BoundField bc3bis = new BoundField();
            bc3bis.HeaderText = "Codice Tipo Iscrizione";
            bc3bis.DataField = "TipoIscrizione";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Dimensione";
            bc4.DataField = "NumeroDipendenti";
            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Comune";
            bc5.DataField = "Comune";
            BoundField bc6 = new BoundField();
            bc6.HeaderText = "Provincia";
            bc6.DataField = "Provincia";
            BoundField bc7 = new BoundField();
            bc7.HeaderText = "N. lavoratori totali dell'Azienda, presenti al corso";
            bc7.DataField = "NumeroLavoratori";

            gv.Columns.Add(bc0);
            gv.Columns.Add(bc0bis);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc3bis);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            gv.Columns.Add(bc7);

            gv.DataSource = estrazioni;
            gv.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            return sw.ToString();
        }

        protected void CustomValidatorSettimana_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (CalendarSettimana.SelectedDates.Count == 7)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        private void RestituisciExcel(String excel, String nomeFile)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.xls", nomeFile));
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(excel);
            Response.End();
        }
    }
}