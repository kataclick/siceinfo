﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="PrestazioniDTA.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.PrestazioniDTA" %>

<%@ Register src="../WebControls/MenuCorsi.ascx" tagname="MenuCorsi" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Corsi" sottoTitolo="Prestazioni DTA" />
    <br />
    Anno formativo:
    <asp:TextBox
        ID="TextBoxAnno"
        runat="server"
        MaxLength="4"
        Width="50px"
        Visible="false">
    </asp:TextBox>
    <asp:DropDownList
        ID="DropDownListAnno"
        runat="server"
        Width="100px">
    </asp:DropDownList>
    <br />
    <asp:Button
        ID="ButtonCarica"
        runat="server"
        Text="Carica"
        Width="100px" onclick="ButtonCarica_Click" />
    <br />
    <br />
    <asp:Panel
        ID="PanelPrestazioniDTA"
        runat="server"
        Visible="false">
        <asp:Label
            ID="LabelErogazione"
            runat="server"
            Visible="false"
            Text="Le prestazioni per l'anno X sono state erogate il Y">
        </asp:Label>
        <br />
        <%--<telerik:RadAjaxPanel
            ID="RadAjaxLoadingPanelPrestazioni"
            runat="server"
            Width="100%">--%>
            <br />
            <b>
                Riassunto lavoratori
            </b>
            <table class="borderedTable">
                <tr>
                    <td style="width: 150px">
                        Totale:
                    </td>
                    <td>
                        <asp:Label
                            ID="LabelLavoratoriTotale"
                            runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        Iscritti alla Cassa Edile:
                    </td>
                    <td>
                        <asp:Label
                            ID="LabelLavoratoriIscritti"
                            runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        Con diritto:
                    </td>
                    <td>
                        <asp:Label
                            ID="LabelLavoratoriConDiritto"
                            runat="server">
                        </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        Selezionate:
                    </td>
                    <td>
                        <b>
                            <asp:Label
                                ID="LabelDomandeSelezionate"
                                runat="server"></asp:Label>
                        </b>
                    </td>
                </tr>
            </table>
            <br />
            <table class="standardTable" runat="server">
                <tr>
                    <td style="width: 80px">
                        Corso:
                    </td>
                    <td colspan="2">
                        <asp:DropDownList
                            ID="DropDownListCorso"
                            runat="server"
                            Width="200px">
                            <asp:ListItem>Tutti</asp:ListItem>
                            <asp:ListItem>C.9.01</asp:ListItem>
                            <asp:ListItem>C.9.02</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 80px">
                        Diritto:
                    </td>
                    <td colspan="2">
                        <asp:DropDownList
                            ID="DropDownListDiritto"
                            runat="server"
                            Width="200px">
                            <asp:ListItem>Tutti</asp:ListItem>
                            <asp:ListItem Value="true">Sì</asp:ListItem>
                            <asp:ListItem Value="false">No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 80px">
                        Cognome:
                    </td>
                    <td style="width: 250px">
                        <asp:TextBox
                            ID="TextBoxCognome"
                            runat="server"
                            MaxLength="50"
                            Width="200px">
                        </asp:TextBox>
                    </td>
                    <td>
                        <asp:Button
                            ID="ButtonFiltra"
                            runat="server"
                            Text="Filtra"
                            Width="100px" onclick="ButtonFiltra_Click" />
                        &nbsp;<asp:Button
                            ID="ButtonExcel"
                            runat="server"
                            Text="Excel"
                            Width="100px" onclick="ButtonExcel_Click" />
                    </td>
                </tr>
            </table>
            <telerik:RadGrid
                ID="RadGridPrestazioniDTA"
                runat="server"
                Width="100%" GridLines="None" AllowPaging="True" 
                OnItemDataBound="RadGridPrestazioniDTA_ItemDataBound" 
                AutoGenerateColumns="False" CellSpacing="0" 
                onpageindexchanged="RadGridPrestazioniDTA_PageIndexChanged">
                <ExportSettings>
                    <Pdf PageWidth="" />
                </ExportSettings>
                <MasterTableView DataKeyNames="IdLavoratore">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn UniqueName="Selezione">
                            <ItemTemplate>
                                  <asp:CheckBox ID="CheckBoxSelection" runat="server" AutoPostBack="true"
                                      oncheckedchanged="CheckBoxSelection_CheckedChanged">
                                   </asp:CheckBox>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter Diritto column" 
                            HeaderText="Diritto" UniqueName="Diritto">
                            <ItemTemplate>
                                <asp:Image ID="ImageDiritto" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="40px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter Lavoratore column" 
                            HeaderText="Lavoratore" UniqueName="Lavoratore">
                            <ItemStyle Width="280px" />
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td style="width: 50px">
                                            <b>
                                            <asp:Label ID="LabelIdLavoratore" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                        <td>
                                            <b>
                                            <asp:Label ID="LabelCognomeNome" runat="server">
                                                </asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <small>
                                            <asp:Label ID="LabelCodiceFiscale" runat="server">
                                                </asp:Label>
                                            </small>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <small>
                                            <asp:Label ID="LabelDataNascita" runat="server">
                                                </asp:Label>
                                            </small>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter DataFineCorso column" 
                            HeaderText="Corso" UniqueName="Corso">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <b>
                                                <asp:Label ID="Label1" runat="server" 
                                                    Text='<%# Eval("CodiceCorso") %>'></asp:Label>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelDataFineCorso" runat="server" 
                                                Text='<%# Eval("DataFineCorso", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridCheckBoxColumn DataField="DenunciaPresente" 
                            FilterControlAltText="Filter Denuncia column" HeaderText="Denuncia" 
                            ReadOnly="True" UniqueName="Denuncia">
                            <ItemStyle Width="40px" />
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridCheckBoxColumn DataField="IbanPresente" 
                            FilterControlAltText="Filter Iban column" HeaderText="Iban" 
                            ReadOnly="True" UniqueName="Iban">
                            <ItemStyle Width="40px" />
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter Domanda column" 
                            HeaderText="Domanda" UniqueName="Domanda">
                            <ItemTemplate>
                                <asp:MultiView
                                    ID="MultiViewDomanda"
                                    runat="server">
                                    <asp:View
                                        ID="ViewNoDomanda"
                                        runat="server">
                                        Domanda non confermata
                                    </asp:View>
                                    <asp:View
                                        ID="ViewDomanda"
                                        runat="server">
                                        <table class="standardTable">
                                            <tr>
                                                <td style="width: 40px">
                                                    Prot:
                                                </td>
                                                <td>
                                                    <b>
                                                        <asp:Label
                                                            ID="LabelProtocollo"
                                                            runat="server">
                                                        </asp:Label>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40px">
                                                    Stato:
                                                </td>
                                                <td>
                                                    <asp:Label
                                                        ID="LabelStato"
                                                        runat="server">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </asp:MultiView>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle PageSizeControlType="RadComboBox" />
                </MasterTableView>
                <PagerStyle PageSizeControlType="RadComboBox" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        <%--</telerik:RadAjaxPanel>--%>
        <br />
        <br />
        <asp:Button
            ID="ButtonSalva"
            runat="server"
            Text="Salva situazione"
            Width="150px" 
            Visible="false"
            onclick="ButtonSalva_Click" />
        <asp:Button
            ID="ButtonConferma"
            runat="server"
            Text="Conferma prestazioni"
            Width="150px" 
            Visible="false" onclick="ButtonConferma_Click" />
    </asp:Panel>
</asp:Content>


