﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="PianificazioneCorsi.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.PianificazioneCorsi" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/CorsiRicercaPianificazioni.ascx" TagName="CorsiRicercaPianificazioni"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/CorsiDatiPianificazione.ascx" TagName="CorsiDatiPianificazione"
    TagPrefix="uc4" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Pianificazione corsi" />
    <br />
    Selezionando la sede, la tipologia ed il periodo di interesse del corso, è possibile
    visualizzare l’elenco dei moduli formativi già pianificati ed inserirne di nuovi.
    <br />
    <br />
    <uc3:CorsiRicercaPianificazioni ID="CorsiRicercaPianificazioni1" runat="server" />
    <asp:Button ID="ButtonNuovaPianificazione" runat="server" Text="Nuova pianificazione..."
        Width="150px" OnClick="ButtonNuovaPianificazione_Click" />
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PaneNuovaPianificazione" runat="server" Visible="false">
        <br />
        <table class="filledtable">
            <tr>
                <td>
                    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Nuova pianificazione"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    Corso:
                </td>
                <td>
                    <asp:Panel ID="PanelAggiornamento" runat="server" Visible="false">
                        <b>
                            <asp:Label ID="LabelCorso" runat="server"></asp:Label>
                        </b>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:Button ID="ButtonEliminaPianificazione" runat="server" Text="Cancella pianificazione"
                                        Width="150px" OnClick="ButtonEliminaPianificazione_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelMessaggioModificaVietata" runat="server" Visible="false" Text="Esistono degli iscritti al corso, non è possibile effettuare modifiche"
                                        ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:DropDownList ID="DropDownListCorso" runat="server" AppendDataBoundItems="True"
                        Width="400px" AutoPostBack="True" OnSelectedIndexChanged="DropDownListCorso_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="PanelDatiNuovaPianificazione" runat="server" Visible="false">
                        <uc4:CorsiDatiPianificazione ID="CorsiDatiPianificazione1" runat="server" />
                        <asp:Button ID="ButtonSalvaPianificazione" runat="server" Text="Salva pianificazione"
                            Width="150px" ValidationGroup="salvaPianificazione" OnClick="ButtonSalvaPianificazione_Click" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
</asp:Content>
