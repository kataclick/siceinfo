﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ModificaLavoratore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.ModificaLavoratore" %>

<%@ Register Src="WebControls/CorsiDatiLavoratore.ascx" TagName="CorsiDatiLavoratore" TagPrefix="uc1" %>
<%@ Register src="WebControls/DatiLavoratoreSiceNew.ascx" tagname="DatiLavoratoreSiceNew" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>

    <telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.radWindow;
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function CloseRadWindow() {
            var oWindow = GetRadWindow();
            oWindow.argument = null;
            oWindow.close();
            return false;
        }

        function CloseRadWindowAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function CloseAndRebindEdit(args) {
            GetRadWindow().BrowserWindow.refreshGridEdit(args);
            GetRadWindow().close();
        }
    </script>
</telerik:RadScriptBlock>

    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Modifica dati lavoratore"></asp:Label>
                </td>
            </tr>
        </table>
        <uc1:CorsiDatiLavoratore ID="CorsiDatiLavoratore1" runat="server" Visible="false" />
        <uc2:DatiLavoratoreSiceNew ID="DatiLavoratoreSiceNew1" runat="server" Visible="false" />
        <asp:Button ID="ButtonModifica" runat="server" Width="150px" Text="Salva" ValidationGroup="datiLavoratore" OnClick="ButtonModifica_Click" />
        <asp:Button ID="ButtonAnnulla" runat="server" Width="150px" Text="Annulla" OnClientClick="CloseRadWindow()" OnClick="ButtonAnnulla_Click" />
    </div>
    </form>
</body>
</html>
