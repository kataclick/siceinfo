﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Entities;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Geocoding = TBridge.Cemi.Business.Cantieri.CantieriGeocoding;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class AnagraficaLocazioni : System.Web.UI.Page
    {
        private const Int32 INDICEELIMINA = 4;
        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaLocazioni(0);
            }
        }

        private void CaricaLocazioni(Int32 pagina)
        {
            Presenter.CaricaElementiInGridView(
                GridViewLocazioni,
                biz.GetLocazioni(),
                pagina);
        }

        protected void ButtonGeocodifica_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Locazione locazione = CreaLocazione();
                LabelIndirizzoFornito.Text = locazione.IndirizzoCompleto;

                GeocodificaIndirizzo(locazione);
            }
        }

        private void GeocodificaIndirizzo(Locazione locazione)
        {
            IndirizzoCollection indirizzi = Geocoding.GeoCodeGoogleMultiplo(locazione.IndirizzoPerGeocoder);
            ViewState["IndirizzoGeocoficati"] = indirizzi;

            Presenter.CaricaElementiInGridView(
                GridViewIndirizzi,
                indirizzi,
                0);

            PanelSceltaIndirizzo.Visible = true;
        }

        private Locazione CreaLocazione()
        {
            Locazione locazione = new Locazione();

            locazione.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
            locazione.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
            locazione.Provincia = Presenter.NormalizzaCampoTesto(TextBoxProvincia.Text);
            locazione.Note = Presenter.NormalizzaCampoTesto(TextBoxNote.Text);

            return locazione;
        }

        protected void ButtonNuovaLocazione_Click(object sender, EventArgs e)
        {
            PanelNuovaLocazione.Visible = true;
        }

        protected void GridViewIndirizzi_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            IndirizzoCollection indirizzi = (IndirizzoCollection)ViewState["IndirizzoGeocoficati"];
            Locazione locazione = ConvertiIndirizzoInLocazione(indirizzi[e.NewSelectedIndex]);

            if (biz.InsertLocazione(locazione))
            {
                ResetPagina();
            }
        }

        private void ResetPagina()
        {
            Presenter.CaricaElementiInGridView(
                GridViewIndirizzi,
                null,
                0);

            Presenter.SvuotaCampo(LabelIndirizzoFornito);
            Presenter.SvuotaCampo(TextBoxIndirizzo);
            Presenter.SvuotaCampo(TextBoxComune);
            Presenter.SvuotaCampo(TextBoxProvincia);
            Presenter.SvuotaCampo(TextBoxNote);

            PanelSceltaIndirizzo.Visible = false;
            PanelNuovaLocazione.Visible = false;

            CaricaLocazioni(0);
        }

        private Locazione ConvertiIndirizzoInLocazione(Indirizzo indirizzo)
        {
            Locazione locazione = new Locazione();

            locazione.Indirizzo =
                Presenter.NormalizzaCampoTesto(String.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico));
            locazione.Comune = Presenter.NormalizzaCampoTesto(indirizzo.Comune);
            locazione.Provincia = Presenter.NormalizzaCampoTesto(indirizzo.Provincia);
            locazione.Cap = indirizzo.Cap;
            locazione.Latitudine = indirizzo.Latitudine;
            locazione.Longitudine = indirizzo.Longitudine;
            locazione.Note = Presenter.NormalizzaCampoTesto(TextBoxNote.Text);

            return locazione;
        }

        protected void ButtonAggiungiIndirizzo_Click(object sender, EventArgs e)
        {
            Locazione locazione = CreaLocazione();

            if (biz.InsertLocazione(locazione))
            {
                ResetPagina();
            }
        }

        protected void GridViewLocazioni_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Int32 idLocazione = (Int32)GridViewLocazioni.DataKeys[e.RowIndex].Values["IdLocazione"];

            if (biz.DeleteLocazione(idLocazione))
            {
                CaricaLocazioni(0);
            }
        }

        protected void GridViewLocazioni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Locazione locazione = (Locazione)e.Row.DataItem;

                if (locazione.EsistePianificazione)
                {
                    e.Row.Cells[INDICEELIMINA].Enabled = false;
                }
            }
        }
    }
}