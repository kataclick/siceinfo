﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Corsi;
using UtenteLavoratore = TBridge.Cemi.Type.Entities.GestioneUtenti.Lavoratore;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class LavoratoreCorsi : System.Web.UI.Page
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiVisualizzazione);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaCorsiLavoratore(0);
            }
        }

        private void CaricaCorsiLavoratore(Int32 pagina)
        {
            //UtenteLavoratore lavoratore =
            //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore)HttpContext.Current.User.Identity).
            //            Entity;
            UtenteLavoratore lavoratore =
                (UtenteLavoratore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            PartecipazioneFilter filtro = new PartecipazioneFilter();
            filtro.IdLavoratore = lavoratore.IdLavoratore;

            Presenter.CaricaElementiInGridView(
                GridViewCorsi,
                biz.GetPartecipazioni(filtro),
                pagina);
        }

        protected void GridViewCorsi_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            CaricaCorsiLavoratore(e.NewPageIndex);
        }
    }
}