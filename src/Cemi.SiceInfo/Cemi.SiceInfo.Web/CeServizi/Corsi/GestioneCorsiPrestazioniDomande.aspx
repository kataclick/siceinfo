﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneCorsiPrestazioniDomande.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.GestioneCorsiPrestazioniDomande" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc4" %>
<%@ Register Src="WebControls/CorsiRicercaPrestazioniDomande.ascx" TagName="CorsiRicercaPrestazioniDomande" TagPrefix="uc2" %>
<%@ Register Src="WebControls/CorsiRicercaLavoratore.ascx" TagName="CorsiRicercaLavoratore" TagPrefix="uc3" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Domande prestazioni"
        sottoTitolo="Prestazioni erogate per il corso 16 ORE" />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="PanelRicercaDomande" runat="server">
                <uc2:CorsiRicercaPrestazioniDomande ID="CorsiRicercaPrestazioniDomande1" runat="server" />
            </asp:Panel>
            <asp:Panel ID="PanelRicercaLavoratore" runat="server" Visible="false">
                <asp:Label ID="Label1" runat="server" Text="Riceca Lavoratore" Font-Bold="True"></asp:Label>
                <uc3:CorsiRicercaLavoratore ID="CorsiRicercaLavoratore1" runat="server" />
                <asp:Button ID="ButtonAnnullaRicercaLavoratore" runat="server" Text="Annulla" OnClick="ButtonAnnullaRicercaLavoratore_Click" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc4:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>