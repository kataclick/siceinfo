﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IscrizioneCorsi.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.IscrizioneCorsi" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc4" %>
<%@ Register Src="WebControls/CorsiRicercaLavoratore.ascx" TagName="CorsiRicercaLavoratore" TagPrefix="uc2" %>
<%@ Register Src="WebControls/CorsiSelezioneProgrammazione.ascx" TagName="CorsiSelezioneProgrammazione" TagPrefix="uc3" %>
<%@ Register Src="WebControls/CorsiDatiLavoratore.ascx" TagName="CorsiDatiLavoratore" TagPrefix="uc5" %>
<%@ Register Src="WebControls/CorsiRicercaImpresa.ascx" TagName="CorsiRicercaImpresa" TagPrefix="uc6" %>
<%@ Register Src="WebControls/CorsiDatiImpresa.ascx" TagName="CorsiDatiImpresa" TagPrefix="uc7" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc4:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Iscrizione lavoratori" />
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Corso"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    <uc3:CorsiSelezioneProgrammazione ID="CorsiSelezioneProgrammazione1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td >
                    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Lavoratore"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td >
                    Lavoratore
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLavoratore" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox>
                    <asp:Button ID="ButtonSelezionaLavoratore" runat="server" Text="Seleziona lavoratore"
                        Width="200px" Enabled="False" OnClick="ButtonSelezionaLavoratore_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="MultiViewLavoratore" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewLavoratoreSelezione" runat="server">
                            <uc2:CorsiRicercaLavoratore ID="CorsiRicercaLavoratore1" runat="server" />
                        </asp:View>
                        <asp:View ID="ViewLavoratoreNuovo" runat="server">
                            <uc5:CorsiDatiLavoratore ID="CorsiDatiLavoratore1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummaryDatiLavoratore" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="datiLavoratore" />
                            <asp:Button ID="ButtonNuovoLavoratore" runat="server" Text="Salva lavoratore" Width="200px"
                                ValidationGroup="datiLavoratore" OnClick="ButtonNuovoLavoratore_Click" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Impresa"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    Impresa
                </td>
                <td>
                    <asp:TextBox ID="TextBoxImpresa" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox>
                    <asp:Button ID="ButtonSelezionaImpresa" runat="server" Text="Seleziona impresa" Width="200px"
                        Enabled="False" OnClick="ButtonSelezionaImpresa_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="MultiViewImpresa" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewImpresaSelezione" runat="server">
                            <uc6:CorsiRicercaImpresa ID="CorsiRicercaImpresa1" runat="server" />
                        </asp:View>
                        <asp:View ID="ViewImpresaNuovo" runat="server">
                            <uc7:CorsiDatiImpresa ID="CorsiDatiImpresa1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummaryDatiImpresa" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="datiImpresa" />
                            <asp:Button ID="ButtonNuovaImpresa" runat="server" Text="Salva impresa" Width="200px"
                                ValidationGroup="datiImpresa" OnClick="ButtonNuovaImpresa_Click" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <asp:CustomValidator ID="CustomValidatorProgrammazioni" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Programmazione non selezionata" OnServerValidate="CustomValidatorProgrammazioni_ServerValidate">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidatorLavoratore" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Lavoratore non selezionato/inserito" OnServerValidate="CustomValidatorLavoratore_ServerValidate">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Impresa non selezionata/inserita" OnServerValidate="CustomValidatorImpresa_ServerValidate">&nbsp;</asp:CustomValidator>
    <br />
    <asp:Label
        ID="Label16OreNonPresente"
        runat="server"
        Visible="False"
        
        Text="Il lavoratore indicato non risulta aver effettuato il corso delle 16 ore prima. Se si vuole comunque effettuare l'iscrizione premere nuovamente il bottone 'Iscrivi lavoratore'." 
        ForeColor="Red"></asp:Label>
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red" Text="Iscrizione effettuata correttamente"
        Visible="true"></asp:Label>
    <asp:Label ID="LabelLavoratoreGiaIscritto" runat="server" Text="Esiste già un'iscrizione del lavoratore ad uno o più moduli selezionati"
        ForeColor="Red" Visible="false"></asp:Label>
    <asp:Label ID="LabelDisponibilitaEsaurita" runat="server" Text="E' stata esaurita la disponibilità per il corso selezionato"
        ForeColor="Red" Visible="false"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummaryIscrizione" runat="server" ValidationGroup="iscrizione"
        CssClass="messaggiErrore" />
    <br />
    <asp:Button ID="ButtonIscriviLavoratore" runat="server" Text="Iscrivi lavoratore"
        ValidationGroup="iscrizione" OnClick="ButtonIscriviLavoratore_Click" />
    <br />
</asp:Content>
