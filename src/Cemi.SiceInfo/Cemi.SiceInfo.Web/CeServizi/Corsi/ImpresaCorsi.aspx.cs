﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class ImpresaCorsi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiVisualizzazione);

            #endregion
        }
    }
}