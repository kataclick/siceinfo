﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IscrizioneCorsiImpresa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.IscrizioneCorsiImpresa" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%@ Register Src="WebControls/CorsiRicercaLavoratore.ascx" TagName="CorsiRicercaLavoratore" TagPrefix="uc8" %>
<%@ Register Src="WebControls/CorsiSelezioneProgrammazione.ascx" TagName="CorsiSelezioneProgrammazione" TagPrefix="uc3" %>
<%@ Register Src="WebControls/CorsiDatiLavoratore.ascx" TagName="CorsiDatiLavoratore" TagPrefix="uc5" %>
<%@ Register Src="WebControls/CorsiRicercaImpresa.ascx" TagName="CorsiRicercaImpresa" TagPrefix="uc6" %>
<%@ Register Src="WebControls/CorsiDatiImpresa.ascx" TagName="CorsiDatiImpresa" TagPrefix="uc7" %>
<%@ Register Src="WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc4" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Iscrizione lavoratori" />
    <br />
    <uc4:PitHeader ID="PitHeader1" runat="server" />
    <br />
    Selezionare il corso desiderato ed il periodo di interesse (schedulazioni dal 
    gg/mm/aaaa al gg/mm/aaaa) e premere sul tasto &quot;Aggiorna&quot;. Si visualizzerà un 
    nuovo riquadro denominato &quot;Modulo&quot; che riporta le programmazioni disponibili, 
    ovvero le attività formative pianificate per sede con evidenza delle date.
    <br />
    L’impresa troverà i propri dati anagrafici pre-compilati nell’apposito 
    spazio
    e dovrà semplicemente, a corso scelto, ricercare o inserire il proprio lavoratore.
    Il consulente abilitato selezionerà l’impresa interessata ed il relativo dipendente.
    <br />
    Ad avvenuta iscrizione, il sistema genererà la ricevuta di conferma che potrà essere
    stampata dal richiedente unitamente alla cartina geografica che indica la sede logistica
    dove si svolgerà il corso.
    <br />
    <br />
    <p>
        <b>
            Corso di formazione 16 ore – MICS Moduli Integrati per Costruire in Sicurezza
        </b>
    </p>
    <p>
        Il corso è attinente le basi professionali del lavoro in edilizia e la formazione alla 
        sicurezza ed ha lo scopo di migliorare la professionalità dei lavoratori (lavorare bene 
        e lavorare in sicurezza) e di adempiere agli obblighi di formazione previsti (Contratto 
        Collettivo Nazionale di Lavoro di settore, Decreto Legislativo 9 aprile 2008, n. 81 
        (Testo Unico) e Conferenza Stato Regioni del 21 dicembre 2011).
    </p>
    <p>
        Si precisa che 
        <b>
            il modulo formativo si rivolge ai lavoratori
        </b>
        :
    </p>
    <ul>
        <li>
            al primo ingresso nel settore edile (senza precedenti esperienze di lavoro 
            regolare in cantiere edile);
        </li>
        <li>
            già operanti nel settore edile ma sprovvisti di un attestato valido comprovante 
            l’avvenuta formazione di base alla sicurezza della durata di 16 ore.
        </li>
    </ul>
    <br />
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Corso"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    <uc3:CorsiSelezioneProgrammazione ID="CorsiSelezioneProgrammazione1" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" ForeColor="White" Text="Lavoratore"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td style="width: 100px">
                    Lavoratore
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLavoratore" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox>
                    <asp:Button ID="ButtonSelezionaLavoratore" runat="server" Text="Seleziona lavoratore"
                        Width="200px" Enabled="False" OnClick="ButtonSelezionaLavoratore_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="MultiViewLavoratore" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewLavoratoreSelezione" runat="server">
                            <uc8:CorsiRicercaLavoratore ID="CorsiRicercaLavoratore1" runat="server" />
                        </asp:View>
                        <asp:View ID="ViewLavoratoreNuovo" runat="server">
                            <uc5:CorsiDatiLavoratore ID="CorsiDatiLavoratore1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummaryDatiLavoratore" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="datiLavoratore" />
                            <asp:Button ID="ButtonNuovoLavoratore" runat="server" Text="Salva lavoratore" Width="200px"
                                ValidationGroup="datiLavoratore" OnClick="ButtonNuovoLavoratore_Click" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Impresa"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td style="width: 100px">
                    Impresa
                </td>
                <td>
                    <asp:TextBox ID="TextBoxImpresa" runat="server" TextMode="MultiLine" Width="400px"
                        Height="80px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
	<br />
	<div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">L&#39;impresa dichiara:</td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>                
                <td>
                    <ul>
                        <li>di assumere / aver assunto la persona come operaio</li>
                        <li>che applicherà / sta applicando al lavoratore un Ccnl del settore edile</li>
                        <li>che il lavoratore verrà denunciato / è attualmente denunciato sulla Cassa Edile di Milano</li>
                    </ul>                    
                </td>
            </tr>
            <tr>                
                <td align="left">
                    <asp:CustomValidator ID="ValidatorAccettazioni" ErrorMessage="Spuntare la dichiarazione di assunzione" runat="server" ValidationGroup="iscrizione" OnServerValidate="validatorAccettazioni_ServerValidate">*&nbsp;</asp:CustomValidator>
                    <asp:CheckBox ID="CheckAccettazioni" runat="server" />&nbsp;Accetta
                </td>
            </tr>
        </table>
    </div>
    <br />
    <asp:CustomValidator ID="CustomValidatorProgrammazioni" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Programmazione non selezionata" OnServerValidate="CustomValidatorProgrammazioni_ServerValidate">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidatorLavoratore" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Lavoratore non selezionato/inserito" OnServerValidate="CustomValidatorLavoratore_ServerValidate">&nbsp;</asp:CustomValidator>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="iscrizione"
        ErrorMessage="Impresa non selezionata/inserita" OnServerValidate="CustomValidatorImpresa_ServerValidate">&nbsp;</asp:CustomValidator>
    <br />
    <asp:Label
        ID="Label16OreNonPresente"
        runat="server"
        Visible="False"
        
        Text="Il lavoratore indicato non risulta aver effettuato il corso delle 16 ore prima. Se si vuole comunque effettuare l'iscrizione premere nuovamente il bottone 'Iscrivi lavoratore'." 
        ForeColor="Red"></asp:Label>
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red" Text="Iscrizione effettuata correttamente"
        Visible="true"></asp:Label>
    <asp:Label ID="LabelLavoratoreGiaIscritto" runat="server" Text="Esiste già un'iscrizione del lavoratore ad uno o più moduli selezionati"
        ForeColor="Red" Visible="false"></asp:Label>
    <asp:Label ID="LabelDisponibilitaEsaurita" runat="server" Text="E' stata esaurita la disponibilità per il corso selezionato"
        ForeColor="Red" Visible="false"></asp:Label>
    <asp:ValidationSummary ID="ValidationSummaryIscrizione" runat="server" ValidationGroup="iscrizione"
        CssClass="messaggiErrore" />
    <br />
    <asp:Button ID="ButtonIscriviLavoratore" runat="server" Text="Iscrivi lavoratore"
        ValidationGroup="iscrizione" OnClick="ButtonIscriviLavoratore_Click" />
    <br />
</asp:Content>
