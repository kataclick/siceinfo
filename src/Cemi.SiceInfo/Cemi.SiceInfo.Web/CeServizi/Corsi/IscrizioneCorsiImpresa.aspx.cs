﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Enums.Corsi;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Exceptions.Corsi;
using UtenteImpresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;
using LavoratoreIscrizioneLavoratori = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Lavoratore;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class IscrizioneCorsiImpresa : System.Web.UI.Page
    {
        private const Int32 INDICENUOVOLAVORATORE = 1;
        private const Int32 INDICESELEZIONELAVORATORE = 0;

        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiIscrizioneImpresa);

            #endregion

            CorsiRicercaLavoratore1.OnLavoratoreSelected += CorsiRicercaLavoratore1_OnLavoratoreSelected;
            CorsiRicercaLavoratore1.OnLavoratoreNuovo += CorsiRicercaLavoratore1_OnLavoratoreNuovo;

            if (!Page.IsPostBack)
            {
                //UtenteImpresa impresa =
                //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Impresa)HttpContext.Current.User.Identity).
                //        Entity;
                UtenteImpresa impresa =
                    (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                SelezionaImpresa(impresa);

                if (Context.Items["lavoratoreIscrizioneLavoratori"] != null)
                {
                    LavoratoreIscrizioneLavoratori lavIscri =
                        (LavoratoreIscrizioneLavoratori)Context.Items["lavoratoreIscrizioneLavoratori"];

                    Lavoratore lav = new Lavoratore();
                    lav.Nome = lavIscri.Nome;
                    lav.Cognome = lavIscri.Cognome;
                    lav.CodiceFiscale = lavIscri.CodiceFiscale;
                    lav.DataNascita = lavIscri.DataNascita;
                    lav.IdLavoratore = lavIscri.IdLavoratore;
                    lav.TipoLavoratore = (TipologiaLavoratore)lavIscri.TipoLavoratore;

                    SelezionaLavoratore(lav);
                }
            }

            LabelMessaggio.Visible = false;

            //if (Context.Items["impresaIscrizioneLavoratori"] != null)
            //    SelezionaImpresa((Impresa)Context.Items["impresaIscrizioneLavoratori"]);

        }

        protected void ButtonIscriviLavoratore_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Partecipazione partecipazione = CreaPartecipazione();
                LabelMessaggio.Visible = false;

                String codiceFiscaleLavoratore = partecipazione.Lavoratore.CodiceFiscale;
                String codiceCorso = partecipazione.Corso.Codice;

                if (codiceCorso == "8 ORE"
                    && !Label16OreNonPresente.Visible
                    && !biz.LavoratoreSeguito16Ore(codiceFiscaleLavoratore))
                {
                    Label16OreNonPresente.Visible = true;
                }
                else
                {
                    Label16OreNonPresente.Visible = false;

                    try
                    {
                        if (biz.InsertPartecipazione(partecipazione, false))
                        {
                            ResetCampi();
                            LabelMessaggio.Visible = true;

                            Context.Items["IdPartecipazione"] = partecipazione.IdPartecipazione;
                            Context.Items["Cognome"] = partecipazione.Lavoratore.Cognome;
                            Context.Items["Nome"] = partecipazione.Lavoratore.Nome;
                            if (partecipazione.Impresa.TipoImpresa == TipologiaImpresa.SiceNew)
                            {
                                Context.Items["CodiceImpresa"] = partecipazione.Impresa.IdImpresa;
                            }
                            Context.Items["RagioneSociale"] = partecipazione.Impresa.RagioneSociale;
                            Server.Transfer("~/CeServizi/Corsi/RicevutaIscrizione.aspx");
                        }
                    }
                    catch (LavoratoreGiaIscrittoException exc1)
                    {
                        LabelLavoratoreGiaIscritto.Visible = true;
                    }
                    catch (DisponibilitaEsauritaException exc2)
                    {
                        LabelDisponibilitaEsaurita.Visible = true;
                    }
                }
            }
        }

        private void ResetCampi()
        {
            ViewState["Lavoratore"] = null;
            Presenter.SvuotaCampo(TextBoxLavoratore);

            LabelLavoratoreGiaIscritto.Visible = false;
            LabelDisponibilitaEsaurita.Visible = false;
        }

        protected void ButtonNuovoLavoratore_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Lavoratore lavoratore = CorsiDatiLavoratore1.GetLavoratore();

                if (!biz.EsisteLavoratoreConStessoCodiceFiscale(lavoratore.CodiceFiscale))
                {
                    SelezionaLavoratore(lavoratore);
                    CorsiDatiLavoratore1.ResetCampi();
                }
                else
                {
                    MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
                    CorsiRicercaLavoratore1.ForzaRicerca(lavoratore.CodiceFiscale);
                }
            }
        }

        protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
        {
            MultiViewLavoratore.Visible = true;
            MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
            ButtonSelezionaLavoratore.Enabled = false;
        }

        private void SelezionaImpresa(UtenteImpresa utenteImpresa)
        {
            Impresa impresa = new Impresa();
            impresa.TipoImpresa = TipologiaImpresa.SiceNew;
            impresa.IdImpresa = utenteImpresa.IdImpresa;
            impresa.RagioneSociale = utenteImpresa.RagioneSociale;
            impresa.PartitaIva = utenteImpresa.PartitaIVA;
            impresa.CodiceFiscale = utenteImpresa.CodiceFiscale;

            ViewState["Impresa"] = impresa;

            if (impresa != null)
            {
                TextBoxImpresa.Text = String.Format("{0} {1}\n{2}\n{3}",
                                                    impresa.IdImpresa,
                                                    impresa.RagioneSociale,
                                                    impresa.PartitaIva,
                                                    impresa.CodiceFiscale);
            }
            else
            {
                TextBoxImpresa.Text = String.Empty;
            }
        }

        protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ViewState["Lavoratore"] != null)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorImpresa_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ViewState["Impresa"] != null)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Partecipazione partecipazione = CreaPartecipazione();

            if (partecipazione != null && partecipazione.PartecipazioneModuli.Count != 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        private Partecipazione CreaPartecipazione()
        {
            Partecipazione partecipazione = new Partecipazione();
            partecipazione.PartecipazioneModuli = new PartecipazioneModuloCollection();
            ProgrammazioneModuloCollection programmazioni = CorsiSelezioneProgrammazione1.GetProgrammazioniSelezionate();

            partecipazione.Lavoratore = (Lavoratore)ViewState["Lavoratore"];
            partecipazione.Impresa = (Impresa)ViewState["Impresa"];
            partecipazione.PrenotazioneImpresa = true;

            partecipazione.Corso = CorsiSelezioneProgrammazione1.GetCorsoSelezionato();

            foreach (ProgrammazioneModulo prog in programmazioni)
            {
                PartecipazioneModulo partecipazioneModulo = new PartecipazioneModulo();
                partecipazione.PartecipazioneModuli.Add(partecipazioneModulo);

                partecipazioneModulo.Programmazione = prog;
            }

            return partecipazione;
        }

        private void CorsiRicercaLavoratore1_OnLavoratoreNuovo()
        {
            MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
            ButtonSelezionaLavoratore.Enabled = true;
        }

        private void CorsiRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
        {
            SelezionaLavoratore(lavoratore);
        }

        private void SelezionaLavoratore(Lavoratore lavoratore)
        {
            ViewState["Lavoratore"] = lavoratore;


            MultiViewLavoratore.Visible = false;
            ButtonSelezionaLavoratore.Enabled = true;
            Label16OreNonPresente.Visible = false;

            if (lavoratore != null)
            {
                if (lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
                {
                    TextBoxLavoratore.Text = String.Format("{0} - {1} {2}\n{3}\n{4}\nPrima esperienza: {5}\nData assunzione: {6}",
                                                           lavoratore.IdLavoratore,
                                                           lavoratore.Cognome,
                                                           lavoratore.Nome,
                                                           lavoratore.DataNascita.ToShortDateString(),
                                                           lavoratore.CodiceFiscale,
                                                           lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "Sì" : "No",
                                                           lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
                }
                else
                {
                    TextBoxLavoratore.Text = String.Format("{0} {1}\n{2}\n{3}\nPrima esperienza: {4}\nData assunzione: {5}",
                                                           lavoratore.Cognome,
                                                           lavoratore.Nome,
                                                           lavoratore.DataNascita.ToShortDateString(),
                                                           lavoratore.CodiceFiscale,
                                                           lavoratore.PrimaEsperienza.HasValue && lavoratore.PrimaEsperienza.Value ? "Sì" : "No",
                                                           lavoratore.DataAssunzione.HasValue ? lavoratore.DataAssunzione.Value.ToString("dd/MM/yyyy") : String.Empty);
                }
            }
        }

        protected void validatorAccettazioni_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = CheckAccettazioni.Checked;
        }
    }
}