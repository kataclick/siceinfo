﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class StatisticheAttestati : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiStatisticheAttestati);

            #endregion

            //if (!Page.IsPostBack)
            //{
            //    ReportViewerCorsi.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            //    ReportViewerCorsi.ServerReport.ReportPath = "/ReportCorsi/ReportStatisticaAttestati";
            //}
        }

        protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
        {
            ReportViewerCorsi.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerCorsi.ServerReport.ReportPath = "/ReportCorsi/ReportStatisticaAttestati";

            List<ReportParameter> listaParam = new List<ReportParameter>();
            if (RadDatePickerDataDa.SelectedDate != null)
            {
                ReportParameter param1 = new ReportParameter("dal",
                                             RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param1);
            }
            if (RadDatePickerDataA.SelectedDate != null)
            {
                ReportParameter param2 = new ReportParameter("al", RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param2);
            }

            ReportViewerCorsi.ServerReport.SetParameters(listaParam.ToArray());
        }
    }
}