﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="EstrazioneFormedil.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.EstrazioneFormedil" %>

<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Estrazione per Formedil" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Settimana
            </td>
            <td>
                <asp:Calendar ID="CalendarSettimana" runat="server" OnSelectionChanged="CalendarSettimana_SelectionChanged"
                    SelectionMode="DayWeek"></asp:Calendar>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorSettimana" runat="server" ValidationGroup="estrazione"
                    OnServerValidate="CustomValidatorSettimana_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="ButtonImprese" runat="server" Text="Imprese" Width="150px" OnClick="ButtonImprese_Click"
                    ValidationGroup="estrazione" />
                &nbsp;
                <asp:Button ID="ButtonLavoratori" runat="server" Text="Lavoratori" Width="150px"
                    OnClick="ButtonLavoratori_Click" ValidationGroup="estrazione" />
            </td>
        </tr>
    </table>
</asp:Content>