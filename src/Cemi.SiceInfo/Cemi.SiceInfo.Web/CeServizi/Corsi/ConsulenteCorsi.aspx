﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConsulenteCorsi.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.ConsulenteCorsi" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCorsi.ascx" TagName="MenuCorsi" TagPrefix="uc2" %>
<%@ Register Src="WebControls/CorsiRicercaLavoratoreImpresaConsulente.ascx" TagName="CorsiRicercaLavoratoreImpresaConsulente" TagPrefix="uc3" %>
<%@ Register Src="WebControls/PitHeader.ascx" TagName="PitHeader" TagPrefix="uc4" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCorsi ID="MenuCorsi1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione Corsi" sottoTitolo="Corsi dei lavoratori" />
    <br />
    <uc4:PitHeader ID="PitHeader1" runat="server" />
    <br />
    <uc3:CorsiRicercaLavoratoreImpresaConsulente ID="CorsiRicercaLavoratoreImpresaConsulente1"
        runat="server" />
    <br />
</asp:Content>