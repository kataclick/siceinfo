﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="RicevutaIscrizione.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.RicevutaIscrizione" %>
<%@ Import Namespace="TBridge.Cemi.Type.Entities.Corsi" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body style="background-color: White;">
    <form id="form1" runat="server">
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=6.2&mkt=it-IT"></script>
            <script type="text/javascript">

                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.464044;
                var longitudine = 9.191567;
                var zoom = 17;

                function LoadMap(lat,lon) {
                    if (lat)
                        latitudine = lat;
                    if (lon)
                        longitudine = lon;
                    
                    var LA = new VELatLong(latitudine, longitudine);
                    map = new VEMap('myMap');

                    map.LoadMap(LA, zoom, VEMapStyle.Road, false, VEMapMode.Mode2D, true, 1);
                    map.SetScaleBarDistanceUnit(VEDistanceUnit.Kilometers);
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pinPoint;
                    var position = new VELatLong(lat, lon);
                    if (img)
                        pinPoint = new VEPushpin(id, position, img, 'Dettaglio: ', descrizione);
                    else
                        pinPoint = new VEPushpin(id, position, null, 'Dettaglio: ', descrizione);

                    map.AddPushpin(pinPoint);
                }
            </script>
        </telerik:RadCodeBlock>
    <div>
        <telerik:RadScriptManager ID="RadScriptManagerMain" runat="server">
        </telerik:RadScriptManager>
        <asp:Image ID="ImageLoghi" runat="server" ImageUrl="../images/loghiCeEsemCpt.jpg"
            Width="400px" />
        <br />
        <br />
    </div>
    <div>
        <b>CONFERMA DI AVVENUTA ISCRIZIONE </b>
        <br />
        Si prega di stampare la presente ricevuta e di presentarla al personale Esem/Cpt
        <br />
        <br />
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="LabelCognome" runat="server"></asp:Label>
                    &nbsp;
                    <asp:Label ID="LabelNome" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelCodice" runat="server"></asp:Label>
                    &nbsp;
                    <asp:Label ID="LabelRagioneSociale" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <b>
            <asp:Label ID="LabelCorso" runat="server"></asp:Label>
        </b>
        <br />
        <br />
        <asp:ListView ID="ListViewIscrizioni" runat="server">
            <LayoutTemplate>
                <table width="600px">
                    <asp:Panel ID="itemPlaceholder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <!-- Modulo -->
                        <%# ((ProgrammazioneModulo)Container.DataItem).Modulo.Descrizione %>
                    </td>
                    <td>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <!-- Schedulazione -->
                                    <%# String.Format("{0} - {1}", ((ProgrammazioneModulo)Container.DataItem).Schedulazione.Value.ToString("dd/MM/yyyy HH:mm"), ((ProgrammazioneModulo)Container.DataItem).SchedulazioneFine.Value.ToString("HH:mm")) %>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!-- Locazione -->
                                    <%# ((ProgrammazioneModulo)Container.DataItem).Locazione.IndirizzoCompleto %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div>
     <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
        </div>
    </div>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Torna alla Home dei Corsi" OnClick="ButtonIndietro_Click" />
    </telerik:RadAjaxPanel>
    </form>
</body>
</html>
