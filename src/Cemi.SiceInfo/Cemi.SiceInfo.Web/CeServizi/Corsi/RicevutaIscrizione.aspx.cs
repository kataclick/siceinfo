﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class RicevutaIscrizione : System.Web.UI.Page
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CorsiGestione);
            funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneImpresa);
            funzionalita.Add(FunzionalitaPredefinite.CorsiIscrizioneConsulente);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            ((RadScriptManager)Page.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonIndietro);

            if (!Page.IsPostBack)
            {
                Int32 idPartecipazione = (Int32)Context.Items["IdPartecipazione"];
                StringBuilder sb = new StringBuilder();
                CaricaPartecipazioni(idPartecipazione, sb);
                RadAjaxPanel1.ResponseScripts.Add(sb.ToString());
            }
        }

        private void CaricaPartecipazioni(int idPartecipazione, StringBuilder stringBuilder)
        {
            ProgrammazioneModuloCollection programmazioni = biz.GetPartecipazioni(idPartecipazione);

            if (programmazioni != null && programmazioni.Count > 0)
            {
                LabelCorso.Text = programmazioni[0].Corso.Descrizione;
            }

            LabelCognome.Text = Context.Items["Cognome"].ToString();
            LabelNome.Text = Context.Items["Nome"].ToString();
            if (Context.Items["CodiceImpresa"] != null)
            {
                LabelCodice.Text = Context.Items["CodiceImpresa"].ToString();
            }
            LabelRagioneSociale.Text = Context.Items["RagioneSociale"].ToString();

            if (programmazioni[0].Locazione.Latitudine.HasValue && programmazioni[0].Locazione.Longitudine.HasValue)
            {
                var descrizione = new StringBuilder();

                // Indirizzo del cantiere
                descrizione.Append(programmazioni[0].Locazione.Indirizzo);
                descrizione.Append("<br />");
                descrizione.Append(programmazioni[0].Locazione.Comune);
                descrizione.Append(" ");
                if (!string.IsNullOrEmpty(programmazioni[0].Locazione.Provincia))
                {
                    descrizione.Append(" (");
                    descrizione.Append(programmazioni[0].Locazione.Provincia);
                    descrizione.Append(")");
                }

                // Informazioni aggiuntive
                descrizione.Append("<br />");
                descrizione.Append("<br />");
                descrizione.Append(programmazioni[0].Locazione.Note);

                stringBuilder.Append(String.Format("LoadMap('{0}','{1}');", programmazioni[0].Locazione.Latitudine.ToString().Replace(',', '.'), programmazioni[0].Locazione.Longitudine.ToString().Replace(',', '.')));

                stringBuilder.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", 0,
                                                           programmazioni[0].Locazione.Latitudine.ToString().Replace(',', '.'),
                                                           programmazioni[0].Locazione.Longitudine.ToString().Replace(',', '.'), descrizione,
                                                           null));
            }

            Presenter.CaricaElementiInListView(
                ListViewIscrizioni,
                programmazioni);
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Corsi/CorsiDefault.aspx");
        }
    }
}