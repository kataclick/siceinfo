﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Corsi;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls
{
    public partial class CorsiRicercaLavoratoreImpresaConsulente : System.Web.UI.UserControl
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();
        private Int32 idConsulente;

        protected void Page_Load(object sender, EventArgs e)
        {
            //idConsulente = (((TBridge.Cemi.GestioneUtenti.Business.Identities.Consulente)HttpContext.Current.User.Identity).Entity).IdConsulente;
            idConsulente = ((Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdConsulente;
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CercaIscrizioni(0);
            }
        }

        private void CercaIscrizioni(int pagina)
        {
            PartecipazioneLavoratoreImpresaFilter filtro = CreaFiltro();

            Presenter.CaricaElementiInGridView(
                GridViewPartecipazioni,
                biz.GetPartecipazioniByLavoratoreImpresa(filtro),
                pagina);
        }

        private PartecipazioneLavoratoreImpresaFilter CreaFiltro()
        {
            PartecipazioneLavoratoreImpresaFilter filtro = new PartecipazioneLavoratoreImpresaFilter();

            filtro.LavoratoreCognome = TextBoxCognome.Text;
            filtro.LavoratoreNome = TextBoxNome.Text;
            filtro.LavoratoreCodiceFiscale = TextBoxCodiceFiscale.Text;

            if (!String.IsNullOrEmpty(TextBoxCodice.Text))
            {
                filtro.IdImpresa = Int32.Parse(TextBoxCodice.Text);
            }
            filtro.ImpresaRagioneSociale = TextBoxRagioneSociale.Text;
            filtro.ImpresaIvaFisc = TextBoxIvaFisc.Text;
            filtro.IdConsulente = idConsulente;

            return filtro;
        }

        protected void GridViewPartecipazioni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PartecipazioneModulo part = (PartecipazioneModulo)e.Row.DataItem;

                Label lLavoratore = (Label)e.Row.FindControl("LabelLavoratore");
                Label lImpresa = (Label)e.Row.FindControl("LabelImpresa");
                Label lCorso = (Label)e.Row.FindControl("LabelCorso");
                Label lModulo = (Label)e.Row.FindControl("LabelModulo");
                Label lSchedulazione = (Label)e.Row.FindControl("LabelSchedulazione");
                Label lLocazione = (Label)e.Row.FindControl("LabelLocazione");
                CheckBox cbPresente = (CheckBox)e.Row.FindControl("CheckBoxPresente");
                CheckBox cbAttestato = (CheckBox)e.Row.FindControl("CheckBoxAttestato");
                Panel pStato = (Panel)e.Row.FindControl("PanelStato");

                lLavoratore.Text = part.Lavoratore.CognomeNome;
                lImpresa.Text = part.Impresa.CodiceERagioneSociale;

                lCorso.Text = part.Programmazione.Corso.Descrizione;
                lModulo.Text = part.Programmazione.Modulo.Descrizione;
                lSchedulazione.Text = part.Programmazione.Schedulazione.Value.ToShortDateString();
                lLocazione.Text = part.Programmazione.Locazione.IndirizzoCompleto;

                cbPresente.Checked = part.Lavoratore.Presente;
                cbAttestato.Checked = part.Lavoratore.Attestato;

                if (part.Programmazione.PresenzeConfermate)
                {
                    pStato.Visible = true;
                }
            }
        }

        protected void GridViewPartecipazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CercaIscrizioni(e.NewPageIndex);
        }
    }
}