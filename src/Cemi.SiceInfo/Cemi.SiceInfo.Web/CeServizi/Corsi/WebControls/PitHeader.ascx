﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PitHeader.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls.PitHeader" %>

<table style="background-color: #242424; font-family: Arial; font-size: x-large;
    color: White;" width="97%">
    <tr>
        <td align="left" style="vertical-align: middle;">
            <asp:Image ID="Image1" runat="server" ImageUrl="../../images/logo16oreMICSSmall.jpg" />
        </td>
        <td align="center" valign="middle" style="font-size: 18pt; vertical-align: middle;">
            16 Ore - MICS</td>
        <td align="right" style="vertical-align: middle;">
            <asp:Image ID="Image3" runat="server" ImageUrl="../../images/treLoghi.jpg" />
            <asp:Image ID="Image2" runat="server" ImageUrl="../../images/logoformedil.jpg" />
        </td>
    </tr>
</table>