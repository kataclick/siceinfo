﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CorsiSelezioneProgrammazione.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls.CorsiSelezioneProgrammazione" %>

<table class="standardTable">
    <tr>
        <td>
            Schedulazioni dal:
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBoxDataInizio" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataInizio" runat="server"
                ControlToValidate="TextBoxDataInizio" ValidationGroup="caricaSchedulazioni">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorDataInizio" runat="server" ControlToValidate="TextBoxDataInizio"
                ValidationGroup="caricaSchedulazioni" Type="Date" Operator="DataTypeCheck">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Schedulazioni al:
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBoxDataFine" runat="server" Width="150px" MaxLength="10"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataFine" runat="server" ControlToValidate="TextBoxDataFine"
                ValidationGroup="caricaSchedulazioni">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorDataFine" runat="server" ControlToValidate="TextBoxDataFine"
                ValidationGroup="caricaSchedulazioni" Type="Date" Operator="DataTypeCheck">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidatorDataInizioDataFine" runat="server" ControlToValidate="TextBoxDataInizio"
                ControlToCompare="TextBoxDataFine" ValidationGroup="caricaSchedulazioni" Type="Date"
                Operator="LessThan">*</asp:CompareValidator>
            &nbsp;
            <asp:Button ID="ButtonAggiornaProgrammazioni" runat="server" ValidationGroup="caricaSchedulazioni"
                Width="150px" OnClick="ButtonAggiornaProgrammazioni_Click" Text="Aggiorna" />
        </td>
    </tr>
    <tr>
        <td>
            Corso:
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListCorso" runat="server" Width="400px" AppendDataBoundItems="true"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownListCorso_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCorso" runat="server" ControlToValidate="DropDownListCorso"
                ValidationGroup="caricaSchedulazioni">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:GridView ID="GridViewProgrammazioniDisponibili" runat="server" AutoGenerateColumns="False"
                Width="100%" OnRowDataBound="GridViewProgrammazioniDisponibili_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="Descrizione" HeaderText="Modulo">
                        <ItemStyle Width="200px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Programmazioni disponibili">
                        <ItemTemplate>
                            <asp:RadioButtonList ID="RadioButtonListProgrammazioni" runat="server" OnSelectedIndexChanged="RadioButtonListProgrammazioni_SelectedIndexChanged">
                            </asp:RadioButtonList>
                            <asp:CustomValidator ID="CustomValidatorProgrammazioni" runat="server" ValidationGroup="iscrizione"
                                OnServerValidate="CustomValidatorProgrammazioni_ServerValidate">Selezionare una programmazione</asp:CustomValidator>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna programmazione disponibile
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
