﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CorsiDatiImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls.CorsiDatiImpresa" %>

<table  class="borderedTable">
    <tr>
        <td>
            Ragione sociale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="300px" MaxLength="255"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorRagioneSociale" runat="server"
                ControlToValidate="TextBoxRagioneSociale" ValidationGroup="datiImpresa">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Partita IVA*
        </td>
        <td>
            <asp:TextBox ID="TextBoxPartitaIva" runat="server" Width="300px" MaxLength="11"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPartitaIva" runat="server"
                ControlToValidate="TextBoxPartitaIva" ValidationGroup="datiImpresa">*</asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidatorPartitaIva" runat="server"
                ControlToValidate="TextBoxPartitaIva" ValidationGroup="datiImpresa" 
                onservervalidate="CustomValidatorPartitaIva_ServerValidate">*</asp:CustomValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPIVA" runat="server"
                ControlToValidate="TextBoxPartitaIva" ValidationExpression="^\d{11}$"
                ValidationGroup="datiImpresa">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px" MaxLength="16"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                ControlToValidate="TextBoxCodiceFiscale" ValidationGroup="datiImpresa">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                ValidationGroup="datiImpresa">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Tipo contratto
        </td>
        <td>
            <asp:DropDownList ID="DropDownListTipoContratto" runat="server" Width="300px"
                AppendDataBoundItems="True"></asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr runat="server" visible="false">
        <td>
            Tipo iscrizione
        </td>
        <td>
            <asp:DropDownList ID="DropDownListTipoIscrizione" runat="server" Width="300px"
                AppendDataBoundItems="True"></asp:DropDownList>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            N° dipendenti
        </td>
        <td>
            <asp:TextBox ID="TextBoxNumeroDipendenti" runat="server" Width="300px" MaxLength="4"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorNumeroDipendenti" runat="server" ControlToValidate="TextBoxNumeroDipendenti"
                Type="Integer" MinimumValue="1" MaximumValue="9999" ValidationGroup="datiImpresa">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Provincia sede legale
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListProvincia" runat="server" Width="300px"
                AppendDataBoundItems="True" AutoPostBack="True" 
                onselectedindexchanged="DropDownListProvinciaNascita_SelectedIndexChanged"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Comune sede legale
        </td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownListComune" runat="server" Width="300px"
                AppendDataBoundItems="True" ></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            * campi obbligatori
        </td>
    </tr>
</table>