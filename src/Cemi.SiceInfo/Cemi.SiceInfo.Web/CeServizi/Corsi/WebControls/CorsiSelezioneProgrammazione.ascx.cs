﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls
{
    public partial class CorsiSelezioneProgrammazione : System.Web.UI.UserControl
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();
        private Boolean utenteConsulente;
        private Boolean utenteImpresa;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GestioneUtentiBiz.IsImpresa())
            {
                utenteImpresa = true;
            }

            if (GestioneUtentiBiz.IsConsulente())
            {
                utenteConsulente = true;
            }

            if (!Page.IsPostBack)
            {
                CaricaCorsi();

                if (!utenteImpresa && !utenteConsulente)
                {
                    TextBoxDataInizio.Text = DateTime.Now.AddDays(-1).ToShortDateString();
                    TextBoxDataFine.Text = DateTime.Now.AddDays(30).ToShortDateString();
                }
                else
                {
                    TextBoxDataInizio.Text = DateTime.Now.ToShortDateString();
                    TextBoxDataInizio.Enabled = false;
                    TextBoxDataFine.Text = DateTime.Now.AddDays(30).ToShortDateString();
                }
            }
        }

        private void CaricaCorsi()
        {
            CorsoCollection corsi = null;

            if (utenteImpresa || utenteConsulente)
            {
                corsi = biz.GetCorsiPianificati();
            }
            else
            {
                corsi = biz.GetCorsiAll();
            }

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCorso,
                corsi,
                "NomeCompleto",
                "IdCorso");
        }

        private void CaricaProgrammazioni(ModuloCollection moduli)
        {
            Presenter.CaricaElementiInGridView(
                GridViewProgrammazioniDisponibili,
                moduli,
                0);
        }

        protected void GridViewProgrammazioniDisponibili_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Modulo modulo = (Modulo)e.Row.DataItem;
                RadioButtonList rblProgrammazioni = (RadioButtonList)e.Row.FindControl("RadioButtonListProgrammazioni");

                if (!utenteImpresa && !utenteConsulente)
                {
                    Presenter.CaricaElementiInRadioButtonList(
                        rblProgrammazioni,
                        modulo.Programmazioni,
                        "SchedulazioneStringa",
                        "IdComposto");
                }
                else
                {
                    Presenter.CaricaElementiInRadioButtonList(
                        rblProgrammazioni,
                        modulo.Programmazioni,
                        "SchedulazioneStringaImpresa",
                        "IdComposto");
                }

                if (modulo.Programmazioni != null && modulo.Programmazioni.Count > 0)
                {
                    // Se la programmazione non è libera devo limitare la selezione

                    ViewState["IscrizioneLibera"] = modulo.Programmazioni[0].Corso.IscrizioneLibera;
                    if (!modulo.Programmazioni[0].Corso.IscrizioneLibera)
                    {
                        rblProgrammazioni.AutoPostBack = true;
                    }
                }
            }
        }

        protected void DropDownListCorso_SelectedIndexChanged(object sender, EventArgs e)
        {
            Page.Validate("caricaSchedulazioni");

            if (Page.IsValid)
            {
                CaricaProgrammazioniInterna();
            }
        }

        private void CaricaProgrammazioniInterna()
        {
            Int32 idCorso = Int32.Parse(DropDownListCorso.SelectedValue);
            DateTime dataInizio = DateTime.Parse(TextBoxDataInizio.Text.Replace('.', '/'));
            DateTime dataFine = DateTime.Parse(TextBoxDataFine.Text.Replace('.', '/'));

            ModuloCollection moduli = biz.GetModuliConProgrammazioniFuture(idCorso, dataInizio, dataFine,
                                                                           utenteImpresa || utenteConsulente,
                                                                           !utenteImpresa && !utenteConsulente);
            CaricaProgrammazioni(moduli);
        }

        public ProgrammazioneModuloCollection GetProgrammazioniSelezionate()
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            foreach (GridViewRow row in GridViewProgrammazioniDisponibili.Rows)
            {
                RadioButtonList rbProgrammazioni = (RadioButtonList)row.FindControl("RadioButtonListProgrammazioni");

                if (!String.IsNullOrEmpty(rbProgrammazioni.SelectedValue))
                {
                    ProgrammazioneModulo progModulo = new ProgrammazioneModulo();
                    programmazioni.Add(progModulo);
                    progModulo.IdProgrammazioneModulo =
                        ProgrammazioneModulo.IdProgrammazioneModuloDaIdComposto(rbProgrammazioni.SelectedValue);
                }
            }

            return programmazioni;
        }

        public Corso GetCorsoSelezionato()
        {
            Corso corso = null;

            if (!String.IsNullOrEmpty(DropDownListCorso.SelectedValue))
            {
                Int32 idCorso = Int32.Parse(DropDownListCorso.SelectedValue);
                corso = biz.GetCorsoByKey(idCorso);
            }

            return corso;
        }

        protected void CustomValidatorProgrammazioni_ServerValidate(object source, ServerValidateEventArgs args)
        {
            RadioButtonList rbProgrammazioni =
                (RadioButtonList)((CustomValidator)source).FindControl("RadioButtonListProgrammazioni");

            if (rbProgrammazioni.SelectedIndex >= 0)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void ButtonAggiornaProgrammazioni_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaProgrammazioniInterna();
            }
        }

        protected void RadioButtonListProgrammazioni_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(Boolean)ViewState["IscrizioneLibera"])
            {
                RadioButtonList rblProgrammazioni = (RadioButtonList)sender;

                if (!String.IsNullOrEmpty(rblProgrammazioni.SelectedValue))
                {
                    Int32 idProgrammazione =
                        ProgrammazioneModulo.IdProgrammazioneDaIdComposto(rblProgrammazioni.SelectedValue);

                    SelezionaLaProgrammazione(idProgrammazione);
                }
            }
        }

        private void SelezionaLaProgrammazione(int idProgrammazione)
        {
            foreach (GridViewRow row in GridViewProgrammazioniDisponibili.Rows)
            {
                RadioButtonList rblProgrammazioni = (RadioButtonList)row.FindControl("RadioButtonListProgrammazioni");
                SelezionaLaProgrammazione(idProgrammazione, rblProgrammazioni);
            }
        }

        private void SelezionaLaProgrammazione(int idProgrammazione, RadioButtonList rblProgrammazioni)
        {
            rblProgrammazioni.SelectedIndex = -1;

            foreach (ListItem programmazione in rblProgrammazioni.Items)
            {
                if (ProgrammazioneModulo.IdProgrammazioneDaIdComposto(programmazione.Value) == idProgrammazione)
                {
                    programmazione.Selected = true;
                    return;
                }
            }
        }
    }
}