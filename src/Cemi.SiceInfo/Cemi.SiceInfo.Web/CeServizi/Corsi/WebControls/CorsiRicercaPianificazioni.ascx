﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CorsiRicercaPianificazioni.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls.CorsiRicercaPianificazioni" %>

<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonRicerca">
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Dal (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorDal" runat="server" ValidationGroup="cercaPianificazione"
                                Type="Date" Operator="DataTypeCheck" ErrorMessage="Dal non valido" ControlToValidate="TextBoxDal">*</asp:CompareValidator>
                        </td>
                        <td>
                            Al (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorAl" runat="server" ValidationGroup="cercaPianificazione"
                                Type="Date" Operator="DataTypeCheck" ErrorMessage="Al non valido" ControlToValidate="TextBoxAl">*</asp:CompareValidator>
                        </td>
                        <td>
                            Corso
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="DropDownListCorso" runat="server" Width="100%" AppendDataBoundItems="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Locazione
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DropDownList ID="DropDownListLocazione" runat="server" Width="360px" AppendDataBoundItems="true">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="cercaPianificazione"
                                OnClick="ButtonRicerca_Click" Width="100px" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Pianificazioni trovate"></asp:Label><br />
                <asp:GridView ID="GridViewFatturePianificazioni" runat="server" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="IdProgrammazione,IdProgrammazioneModulo" OnSelectedIndexChanging="GridViewFatturePianificazioni_SelectedIndexChanging"
                    AllowPaging="True" OnPageIndexChanging="GridViewFatturePianificazioni_PageIndexChanging"
                    OnRowDataBound="GridViewFatturePianificazioni_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Schedulazione">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="LabelSchedulazione" runat="server"></asp:Label>
                                </b>
                                <br />
                                <asp:Label ID="LabelLocazione" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Corso" HeaderText="Corso" />
                        <asp:BoundField DataField="Modulo" HeaderText="Modulo" />
                        <asp:TemplateField HeaderText="Presenze conf.">
                            <ItemTemplate>
                                <asp:Image ID="ImageConfermaPresenze" runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona"
                            ShowSelectButton="True">
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna pianificazione trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
