﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Filters.Corsi;
using UtenteImpresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls
{
    public partial class CorsiImpresaRicercaPartecipanti : System.Web.UI.UserControl
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void CaricaCorsiImpresa(Int32 pagina)
        {
            //UtenteImpresa impresa =
            //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Impresa)HttpContext.Current.User.Identity).
            //            Entity;
            UtenteImpresa impresa =
                (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            PartecipazioneLavoratoreImpresaFilter filtro = CreaFiltro();
            filtro.IdImpresa = impresa.IdImpresa;

            Presenter.CaricaElementiInGridView(
                GridViewPartecipazioni,
                biz.GetPartecipazioniByLavoratoreImpresa(filtro),
                pagina);
        }

        private PartecipazioneLavoratoreImpresaFilter CreaFiltro()
        {
            PartecipazioneLavoratoreImpresaFilter filtro = new PartecipazioneLavoratoreImpresaFilter();

            filtro.LavoratoreCognome = TextBoxCognome.Text;
            filtro.LavoratoreNome = TextBoxNome.Text;
            filtro.LavoratoreCodiceFiscale = TextBoxCodiceFiscale.Text;

            return filtro;
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaCorsiImpresa(0);
            }
        }

        protected void GridViewPartecipazioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Page.Validate("cercaIscrizioni");

            if (Page.IsValid)
            {
                CaricaCorsiImpresa(e.NewPageIndex);
            }
        }

        protected void GridViewPartecipazioni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PartecipazioneModulo part = (PartecipazioneModulo)e.Row.DataItem;

                Label lLavoratore = (Label)e.Row.FindControl("LabelLavoratore");
                Label lCorso = (Label)e.Row.FindControl("LabelCorso");
                Label lModulo = (Label)e.Row.FindControl("LabelModulo");
                Label lSchedulazione = (Label)e.Row.FindControl("LabelSchedulazione");
                Label lLocazione = (Label)e.Row.FindControl("LabelLocazione");
                CheckBox cbPresente = (CheckBox)e.Row.FindControl("CheckBoxPresente");
                CheckBox cbAttestato = (CheckBox)e.Row.FindControl("CheckBoxAttestato");
                Panel pStato = (Panel)e.Row.FindControl("PanelStato");

                lLavoratore.Text = part.Lavoratore.CognomeNome;

                lCorso.Text = part.Programmazione.Corso.Descrizione;
                lModulo.Text = part.Programmazione.Modulo.Descrizione;
                lSchedulazione.Text = String.Format("{0} - {1}",
                                                    part.Programmazione.Schedulazione.Value.ToString("dd/MM/yyyy hh:mm"),
                                                    part.Programmazione.SchedulazioneFine.Value.ToString("hh:mm"));
                lLocazione.Text = part.Programmazione.Locazione.IndirizzoCompleto;

                cbPresente.Checked = part.Lavoratore.Presente;
                cbAttestato.Checked = part.Lavoratore.Attestato;

                if (part.Programmazione.PresenzeConfermate)
                {
                    pStato.Visible = true;
                }
            }
        }
    }
}