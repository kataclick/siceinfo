﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.Type.Collections.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls
{
    public partial class CorsiDatiPianificazione : System.Web.UI.UserControl
    {
        private CorsiBusiness biz = new CorsiBusiness();

        private LocazioneCollection locazioni = null;

        private Boolean update = false;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaCorso(Int32 idCorso)
        {
            Corso corso = biz.GetCorsoByKey(idCorso);
            ProgrammazioneModuloCollection programmazioni = CreaProgrammazioneDaModuli(corso.Moduli);

            ViewState["IdCorso"] = idCorso;
            ViewState["IscrizioneLibera"] = corso.IscrizioneLibera;

            Presenter.CaricaElementiInGridView(
                GridViewPianificazione,
                programmazioni,
                0);
        }

        private ProgrammazioneModuloCollection CreaProgrammazioneDaModuli(ModuloCollection moduloCollection)
        {
            ProgrammazioneModuloCollection programmazioni = new ProgrammazioneModuloCollection();

            foreach (Modulo modulo in moduloCollection)
            {
                ProgrammazioneModulo progMod = new ProgrammazioneModulo();
                programmazioni.Add(progMod);

                progMod.Modulo = modulo;
            }

            return programmazioni;
        }

        protected void CustomValidatorOra_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox tbOra =
                (TextBox)
                ((GridViewRow)(((DataControlFieldCell)((CustomValidator)source).Parent)).Parent).FindControl("TextBoxOra");
            args.IsValid = false;

            DateTime ora;
            if (biz.OraValida(tbOra.Text, out ora))
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorOraFine_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox tbOra =
                (TextBox)
                ((GridViewRow)(((DataControlFieldCell)((CustomValidator)source).Parent)).Parent).FindControl(
                    "TextBoxOraFine");
            args.IsValid = false;

            DateTime ora;
            if (biz.OraValida(tbOra.Text, out ora))
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorOraInizioMinoreOraFine_ServerValidate(object source, ServerValidateEventArgs args)
        {
            TextBox tbOraInizio =
                (TextBox)
                ((GridViewRow)(((DataControlFieldCell)((CustomValidator)source).Parent)).Parent).FindControl("TextBoxOra");
            TextBox tbOraFine =
                (TextBox)
                ((GridViewRow)(((DataControlFieldCell)((CustomValidator)source).Parent)).Parent).FindControl(
                    "TextBoxOraFine");
            args.IsValid = false;

            DateTime oraInizio;
            DateTime oraFine;
            if (biz.OraValida(tbOraInizio.Text, out oraInizio) && biz.OraValida(tbOraFine.Text, out oraFine))
            {
                if (oraInizio < oraFine)
                {
                    args.IsValid = true;
                }
            }
        }

        public Programmazione GetPianificazioni()
        {
            Programmazione programmazione = new Programmazione();
            programmazione.ProgrammazioniModuli = new ProgrammazioneModuloCollection();
            programmazione.Corso = new Corso();
            if (ViewState["IdCorso"] != null)
            {
                programmazione.Corso.IdCorso = (Int32)ViewState["IdCorso"];
            }

            Page.Validate("salvaPianificazione");
            if (Page.IsValid)
            {
                foreach (GridViewRow row in GridViewPianificazione.Rows)
                {
                    DropDownList ddLocazione = (DropDownList)row.FindControl("DropDownListLocazione");
                    TextBox tbGiorno = (TextBox)row.FindControl("TextBoxGiorno");
                    TextBox tbOra = (TextBox)row.FindControl("TextBoxOra");
                    TextBox tbOraFine = (TextBox)row.FindControl("TextBoxOraFine");
                    TextBox tbMaxPartecipanti = (TextBox)row.FindControl("TextBoxMaxPartecipanti");
                    CheckBox cbPrenotabile = (CheckBox)row.FindControl("CheckBoxPrenotabile");

                    ProgrammazioneModulo prog = new ProgrammazioneModulo();
                    programmazione.ProgrammazioniModuli.Add(prog);

                    // Se presente recupero l'id della programmazione modulo
                    prog.IdProgrammazioneModulo =
                        (Int32?)GridViewPianificazione.DataKeys[row.RowIndex].Values["IdProgrammazioneModulo"];
                    programmazione.IdProgrammazione =
                        (Int32?)GridViewPianificazione.DataKeys[row.RowIndex].Values["IdProgrammazione"];
                    prog.IdProgrammazione =
                        (Int32?)GridViewPianificazione.DataKeys[row.RowIndex].Values["IdProgrammazione"];

                    // Modulo da programmare
                    prog.Modulo = new Modulo();
                    prog.Modulo.IdModulo = (Int32)GridViewPianificazione.DataKeys[row.RowIndex].Values["IdModulo"];

                    // Locazione
                    prog.Locazione = new Locazione();
                    prog.Locazione.IdLocazione = Int32.Parse(ddLocazione.SelectedValue);

                    // Max partecipanti
                    prog.MaxPartecipanti = Int32.Parse(tbMaxPartecipanti.Text);

                    // Schedulazione
                    DateTime giorno = DateTime.Parse(tbGiorno.Text);
                    DateTime ora = DateTime.Parse(String.Format("01/01/1900 {0}", tbOra.Text));
                    DateTime oraFine = DateTime.Parse(String.Format("01/01/1900 {0}", tbOraFine.Text));
                    prog.Schedulazione = new DateTime(giorno.Year, giorno.Month, giorno.Day, ora.Hour, ora.Minute, 0);
                    prog.SchedulazioneFine = new DateTime(giorno.Year, giorno.Month, giorno.Day, oraFine.Hour,
                                                          oraFine.Minute, 0);

                    // Prenotabile
                    prog.Prenotabile = cbPrenotabile.Checked;
                }
            }

            return programmazione;
        }

        protected void GridViewPianificazione_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lProgressivo = (Label)e.Row.FindControl("LabelProgressivo");
                Label lModulo = (Label)e.Row.FindControl("LabelModulo");
                CheckBox cbPrenotabile = (CheckBox)e.Row.FindControl("CheckBoxPrenotabile");

                #region Caricamento drop down e validator

                DropDownList ddLocazioni = (DropDownList)e.Row.FindControl("DropDownListLocazione");
                CompareValidator cvDataFutura = (CompareValidator)e.Row.FindControl("CompareValidatorGiornoFuturo");

                // La data di schedulazione deve essere maggiore della data odierna
                cvDataFutura.ValueToCompare = DateTime.Now.ToShortDateString();

                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    ddLocazioni,
                    locazioni,
                    "IndirizzoCompleto",
                    "IdLocazione");

                #endregion

                ProgrammazioneModulo prog = (ProgrammazioneModulo)e.Row.DataItem;

                lProgressivo.Text = prog.Modulo.Progressivo.ToString();
                lModulo.Text = prog.Modulo.Descrizione;

                TextBox tbGiorno = (TextBox)e.Row.FindControl("TextBoxGiorno");
                if (prog.Schedulazione.HasValue)
                {
                    tbGiorno.Text = prog.Schedulazione.Value.ToShortDateString();
                }

                TextBox tbOra = (TextBox)e.Row.FindControl("TextBoxOra");
                if (prog.Schedulazione.HasValue)
                {
                    tbOra.Text = String.Format("{0}:{1}", prog.Schedulazione.Value.ToString("HH"),
                                               prog.Schedulazione.Value.ToString("mm"));
                }

                TextBox tbOraFine = (TextBox)e.Row.FindControl("TextBoxOraFine");
                if (prog.SchedulazioneFine.HasValue)
                {
                    tbOraFine.Text = String.Format("{0}:{1}", prog.SchedulazioneFine.Value.ToString("HH"),
                                                   prog.SchedulazioneFine.Value.ToString("mm"));
                }

                TextBox tbMaxPartecipanti = (TextBox)e.Row.FindControl("TextBoxMaxPartecipanti");
                tbMaxPartecipanti.Text = prog.MaxPartecipanti.ToString();

                if (prog.Locazione != null)
                {
                    ddLocazioni.SelectedValue = prog.Locazione.IdLocazione.ToString();
                }

                cbPrenotabile.Checked = prog.Prenotabile;
            }
        }

        protected void GridViewPianificazione_DataBinding(object sender, EventArgs e)
        {
            locazioni = biz.GetLocazioni();
        }

        public void SvuotaCampi()
        {
            Presenter.CaricaElementiInGridView(
                GridViewPianificazione,
                null,
                0);
        }

        public void CaricaProgrammazioni(ProgrammazioneModuloCollection programmazioniCorso)
        {
            update = true;

            if (programmazioniCorso.Count > 0)
            {
                ViewState["IscrizioneLibera"] = programmazioniCorso[0].Corso.IscrizioneLibera;
            }

            Presenter.CaricaElementiInGridView(
                GridViewPianificazione,
                programmazioniCorso,
                0);
        }

        protected void CheckBoxPrenotabile_CheckedChanged(object sender, EventArgs e)
        {
            Boolean iscrizioneLibera = (Boolean)ViewState["IscrizioneLibera"];
            CheckBox cbPrenotabile = (CheckBox)sender;

            if (!iscrizioneLibera)
            {
                foreach (GridViewRow row in GridViewPianificazione.Rows)
                {
                    CheckBox cbRiga = (CheckBox)row.FindControl("CheckBoxPrenotabile");
                    cbRiga.Checked = cbPrenotabile.Checked;
                }
            }
        }
    }
}