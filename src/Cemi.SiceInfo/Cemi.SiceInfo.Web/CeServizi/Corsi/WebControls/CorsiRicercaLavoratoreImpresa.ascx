﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CorsiRicercaLavoratoreImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Corsi.WebControls.CorsiRicercaLavoratoreImpresa" %>

<asp:Panel ID="PanelRicercaLavoratoreImpresa" runat="server" DefaultButton="ButtonRicerca">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td colspan="4">
                            <b>Lavoratore </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelPadding2" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <b>Impresa </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice
                            <asp:RangeValidator ID="RangeValidatorCodice" runat="server" ControlToValidate="TextBoxCodice"
                                ValidationGroup="cercaIscrizioni" Type="Integer" MinimumValue="1" MaximumValue="9999999">*</asp:RangeValidator>
                        </td>
                        <td>
                            Ragione sociale
                        </td>
                        <td>
                            Cod.fisc./P.Iva
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxIvaFisc" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="cercaIscrizioni"
                                Width="100px" OnClick="ButtonRicerca_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Iscrizioni trovate"></asp:Label><br />
                <asp:GridView ID="GridViewPartecipazioni" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="True" OnPageIndexChanging="GridViewPartecipazioni_PageIndexChanging"
                    OnRowDataBound="GridViewPartecipazioni_RowDataBound" PageSize="5" DataKeyNames="DataPianificazione,IdCorso,IdLocazione"
                    OnSelectedIndexChanging="GridViewPartecipazioni_SelectedIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Lavoratore">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="LabelLavoratore" runat="server"></asp:Label>
                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Impresa">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Iscrizione">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            Corso:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelCorso" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Modulo:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelModulo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Schedulazione:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelSchedulazione" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Locazione:
                                        </td>
                                        <td>
                                            <asp:Label ID="LabelLocazione" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxPresente" runat="server" Enabled="false" Text="Presente" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxAttestato" runat="server" Enabled="false" Text="Attestato" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Vai alla pian."
                            ShowSelectButton="True">
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna iscrizione trovata
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
