﻿using System;
using System.Web.UI;
using TBridge.Cemi.Corsi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Delegates.Corsi;
using TBridge.Cemi.Type.Entities.Corsi;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Corsi
{
    public partial class GestioneCorsiPrestazioniDomande : System.Web.UI.Page
    {
        private readonly CorsiBusiness biz = new CorsiBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CorsiGestione);

            #endregion

            #region Registrazioni ad eventi dei controlli

            CorsiRicercaPrestazioniDomande1.OnRicercaLavoratoreSelected +=
                new RicercaLavoratoreSelectedEventHandler(CorsiRicercaPrestazioniDomande1_OnRicercaLavoratoreSelected);
            CorsiRicercaLavoratore1.OnLavoratoreSelected +=
                new LavoratoreSelectedEventHandler(CorsiRicercaLavoratore1_OnLavoratoreSelected);

            #endregion

            if (!Page.IsPostBack)
            {
            }
        }

        private void CorsiRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
        {
            PanelRicercaDomande.Visible = true;
            PanelRicercaLavoratore.Visible = false;

            //UtenteAbilitato utente = ((UtenteAbilitato)HttpContext.Current.User.Identity);
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

            int idCorsiPrestazioneDomanda = (Int32)ViewState["idCorsiPrestazioneDomanda"];

            biz.AggiornaLavoratoreBeneficiarioDomandaPrestazione(idCorsiPrestazioneDomanda, lavoratore.IdLavoratore.Value,
                                                                 idUtente);

            //Ricarichiamo gli aggiornamenti apportati 
            CorsiRicercaPrestazioniDomande1.Refresh();
        }

        private void CorsiRicercaPrestazioniDomande1_OnRicercaLavoratoreSelected(int idCorsiPrestazioneDomanda)
        {
            PanelRicercaDomande.Visible = false;
            PanelRicercaLavoratore.Visible = true;

            CorsiRicercaLavoratore1.ResetRicerca();
            //CorsiRicercaLavoratore1.SetFiltro();

            ViewState["idCorsiPrestazioneDomanda"] = idCorsiPrestazioneDomanda;
        }

        protected void ButtonAnnullaRicercaLavoratore_Click(object sender, EventArgs e)
        {
            PanelRicercaDomande.Visible = true;
            PanelRicercaLavoratore.Visible = false;
        }
    }
}