﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Reporting;
using TBridge.Cemi.Business.Reporting.ReportingService2005;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class InserimentoReport : System.Web.UI.Page
    {
        private readonly GestioneReport _biz = new GestioneReport();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti);

            if (!Page.IsPostBack)
            {
                List<Report> reports = GetReportList();

                RadComboBoxReport.DataSource = reports;
                RadComboBoxReport.DataBind();
                //Presenter.CaricaElementiInDropDown(RadComboBoxReport,reports,"Nome","Path");
                //RadComboBoxReport.Items.Insert(0, new RadComboBoxItem("- Selezionare un report -"));

                RadComboBoxReport.SelectedIndex = 0;
            }
        }

        protected void RadGridReport_DeleteCommand(object source, GridCommandEventArgs e)
        {
            PanelModifica.Visible = false;
            int idReport = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
            _biz.DeleteUtenteReport(null, idReport);
            _biz.DeleteReport(idReport);
        }

        protected void RadGridReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            CaricaReport();
        }

        private void CaricaReport()
        {
            RadGridReport.DataSource = _biz.GetListaReport();
        }

        protected void ButtonClearFilter_Click(object sender, ImageClickEventArgs e)
        {
            RadGridReport.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridReport.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridReport.MasterTableView.Rebind();
        }

        protected void ButtonInserisci_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                TBridge.Cemi.Type.Domain.Report report = new TBridge.Cemi.Type.Domain.Report
                {
                    Nome = RadTextBoxNome.Text,
                    Path = RadComboBoxReport.SelectedValue,
                    Theme = RadComboBoxTema.SelectedValue,
                    DataInserimentoRecord = DateTime.Now
                };

                try
                {
                    if (_biz.InsertReport(report) == 0)
                    {
                        PanelErroreInserimento.Visible = true;
                    }
                    else
                    {
                        PanelErroreInserimento.Visible = false;
                        RadGridReport.MasterTableView.Rebind();
                        _biz.AssociaUtenteReport(1, _biz.GetReport(report.Nome).Id);
                    }
                }
                catch
                {
                    PanelErroreInserimento.Visible = true;
                }
            }
        }

        public List<Report> GetReportList()
        {
            BusinessReporting bizReport = new BusinessReporting();

            string wsReporting2005 = ConfigurationManager.AppSettings["WSReportingServices2005"];
            string wsExecution2005 = ConfigurationManager.AppSettings["WSReportingExecution2005"];

            var report = from item in bizReport.GetReports(wsReporting2005, wsExecution2005)
                         where item.Type == ItemTypeEnum.Report
                         select new Report { Path = item.Path, Nome = item.Name };

            return report.ToList();
        }

        protected void CustomValidatorReport_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadComboBoxReport.SelectedIndex >= 0)
            {
                args.IsValid = true;
            }
        }

        protected void RadComboBox_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Text = ((Report)e.Item.DataItem).Nome;
            e.Item.Value = ((Report)e.Item.DataItem).Path;
        }

        protected void ButtonModifica_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                TBridge.Cemi.Type.Domain.Report report = new TBridge.Cemi.Type.Domain.Report
                {
                    Id = Convert.ToInt32(RadNumericTextBoxId.Value),
                    Nome = RadTextBoxNomeModifica.Text,
                    Theme = RadComboBoxTemaModifica.SelectedValue,
                };

                try
                {
                    if (_biz.UpdateReport(report) > 0)
                    {
                        RadGridReport.MasterTableView.Rebind();
                        PanelModifica.Visible = false;
                    }
                    else
                    {
                        PanelErroreAggiornamento.Visible = true;
                    }
                }
                catch
                {
                    PanelErroreAggiornamento.Visible = true;
                }
            }
        }

        protected void RadGridReport_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Aggiorna")
            {
                int idReport = Convert.ToInt32(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"].ToString());
                TBridge.Cemi.Type.Domain.Report report = _biz.GetReport(idReport);

                RadNumericTextBoxId.Value = idReport;
                RadTextBoxNomeModifica.Text = report.Nome;
                RadComboBoxTemaModifica.FindItemByValue(report.Theme).Selected = true;

                PanelModifica.Visible = true;
                PanelErroreAggiornamento.Visible = false;
            }
        }
    }
}