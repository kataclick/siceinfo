﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiGestionePINConsulenti.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiGestionePINConsulenti" %>

<%@ Register Src="../WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%@ Register src="../WebControls/MenuGestionePin.ascx" tagname="MenuGestionePin" tagprefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc3:MenuGestionePin ID="MenuGestionePin1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione PIN consulenti"
        titolo="Gestione utenti" />
    <br />
    <table class="standardTable">
        <tr>
            <td align="left" style="width: 119px">
                Codice:
            </td>
            <td align="left" style="width: 219px">
                <asp:TextBox ID="TextBoxCodice" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxCodice"
                    ErrorMessage="Formato non valido" Operator="GreaterThan" ValueToCompare="0" Type="Integer"
                    ValidationGroup="ricercaPINConsulenti"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                Ragione sociale:
            </td>
            <td align="left" style="width: 219px">
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                Codice fiscale:
            </td>
            <td align="left" style="width: 219px">
                <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px" MaxLength="16"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                Con PIN:
            </td>
            <td align="left" style="width: 219px">
                <asp:DropDownList ID="DropDownListTipoRicerca" runat="server" Width="306px">
                    <asp:ListItem Value="NONDEF">Entrambi</asp:ListItem>
                    <asp:ListItem Value="SENZAPIN">Consulenti senza PIN</asp:ListItem>
                    <asp:ListItem Value="CONPIN">Consulenti con PIN</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 119px">
                PIN generato
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 119px">
                Dal (gg/mm/aaaa):
            </td>
            <td style="width: 219px" align="left">
                <asp:TextBox ID="TextBoxDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDal"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"
                    ValidationGroup="ricercaPINConsulenti"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 119px">
                Al (gg/mm/aaaa):
            </td>
            <td style="width: 219px" align="left">
                <asp:TextBox ID="TextBoxAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxAl"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"
                    ValidationGroup="ricercaPINConsulenti"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Iscritto al sito web
            </td>
            <td align="left" style="width: 432px">
                <asp:DropDownList ID="DropDownListIscrittoWeb" runat="server" Width="306px">
                    <asp:ListItem Value="NONDEF">Tutti</asp:ListItem>
                    <asp:ListItem Value="ISCRITTI">Iscritti</asp:ListItem>
                    <asp:ListItem Value="NONISCRITTI">Non iscritti</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
                <asp:Button ID="ButtonFiltra" runat="server" OnClick="ButtonFiltra_Click" Text="Ricerca"
                    ValidationGroup="ricercaPINConsulenti" />
            </td>
        </tr>
    </table>
    <strong>
        <br />
        Risultati della ricerca</strong>&nbsp;<br />
    <asp:GridView ID="GridViewConsulenti" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="IdConsulente" PageSize="20" Width="100%" OnPageIndexChanging="GridViewConsulenti_PageIndexChanging"
        OnRowCommand="GridViewConsulenti_RowCommand" 
        onrowdatabound="GridViewConsulenti_RowDataBound">
        <Columns>
            <asp:BoundField DataField="IdConsulente" HeaderText="Codice" />
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
            <asp:BoundField DataField="EMail" HeaderText="eMail">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Username" DataField="Username" />
            <asp:BoundField DataField="PIN" HeaderText="PIN" SortExpression="PIN">
                <ItemStyle Width="10%" />
                <HeaderStyle Width="10%" />
            </asp:BoundField>
            <asp:BoundField DataField="dataGenerazionePIN" HeaderText="Data generazione PIN"
                DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
            <asp:TemplateField HeaderText="Inserisci nel file">
                <ItemStyle Width="5%" />
                <HeaderStyle Width="5%" />
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxNelFile" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="Genera"
                Text="Genera PIN">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="15%" />
            </asp:ButtonField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="ButtonImpersona" runat="server" CausesValidation="false" 
                        CommandName="Impersona" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" Text="Impersona" />
                </ItemTemplate>
                <ControlStyle CssClass="bottoneGriglia" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessun consulente trovato
        </EmptyDataTemplate>
    </asp:GridView>
    <table width="80%">
        <tr>
            <td align="right" style="width: 517px">
                <asp:LinkButton ID="LinkButtonSelezionaTutti" runat="server" Visible="False" OnClick="LinkButtonSelezionaTutti_Click">Seleziona tutti</asp:LinkButton>
                &nbsp; &nbsp;
                <asp:LinkButton ID="LinkButtonDeselezionaTutti" runat="server" Visible="False" OnClick="LinkButtonDeselezionaTutti_Click">Deseleziona tutti</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td style="width: 517px">
                <table class="standardTable">
                    <tr>
                        <td align="left">
                            <asp:Label ID="LabelEsporta" runat="server" Text="Esporta Excel" Visible="False"></asp:Label>&nbsp;
                        </td>
                        <td style="width: 141px">
                        </td>
                        <td align="left">
                            <asp:Label ID="LabelGenera" runat="server" Text="Genera PIN" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="ButtonEsportaExcel" runat="server" Text="Solo selezionati" Visible="False"
                                Width="120px" OnClick="ButtonEsportaExcel_Click" />
                        </td>
                        <td align="left" style="width: 141px">
                        </td>
                        <td>
                            <asp:Button ID="ButtonGeneraPIN" runat="server" Text="Nella pagina" Visible="False"
                                Width="120px" OnClick="ButtonGeneraPIN_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Button ID="ButtonFileTutti" runat="server" Text="Tutti" Visible="False" Width="120px"
                                OnClick="ButtonFileTutti_Click" />
                        </td>
                        <td align="left" style="width: 141px">
                        </td>
                        <td>
                            <asp:Button ID="ButtonGeneraTutti" runat="server" Text="Tutti" Visible="False" Width="120px"
                                OnClick="ButtonGeneraTutti_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
</asp:Content>
