﻿using Cemi.SiceInfo.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using TBridge.Cemi.Business.SmsServiceReference;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
//using TBridge.Cemi.GestioneUtenti.Business;
//using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneSmsLavoratore : System.Web.UI.Page
    {
        private readonly GestioneUtentiBiz _gestioneUtenti = new GestioneUtentiBiz();
        private readonly LavoratoriManager _lavoratoriManager = new LavoratoriManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori);

            if (!Page.IsPostBack)
                LoadInfo();


            #region Per prevenire click multipli

            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            stringBuilder.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            stringBuilder.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            stringBuilder.Append("this.value = 'Attendere...';");
            stringBuilder.Append("this.disabled = true;");
            stringBuilder.Append(Page.ClientScript.GetPostBackEventReference(RadButtonGeneraPin, null));
            stringBuilder.Append(";");
            stringBuilder.Append("return true;");
            RadButtonGeneraPin.Attributes.Add("onclick", stringBuilder.ToString());

            stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            stringBuilder.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            stringBuilder.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            stringBuilder.Append("this.value = 'Attendere...';");
            stringBuilder.Append("this.disabled = true;");
            stringBuilder.Append(Page.ClientScript.GetPostBackEventReference(RadButtonInviaPinSms, null));
            stringBuilder.Append(";");
            stringBuilder.Append("return true;");
            RadButtonInviaPinSms.Attributes.Add("onclick", stringBuilder.ToString());

            stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            stringBuilder.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            stringBuilder.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            stringBuilder.Append("this.value = 'Attendere...';");
            stringBuilder.Append("this.disabled = true;");
            stringBuilder.Append(Page.ClientScript.GetPostBackEventReference(RadButtonModificaNumero, null));
            stringBuilder.Append(";");
            stringBuilder.Append("return true;");
            RadButtonModificaNumero.Attributes.Add("onclick", stringBuilder.ToString());

            stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            stringBuilder.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            stringBuilder.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            stringBuilder.Append("this.value = 'Attendere...';");
            stringBuilder.Append("this.disabled = true;");
            stringBuilder.Append(Page.ClientScript.GetPostBackEventReference(RadButtonDisabilitaNumero, null));
            stringBuilder.Append(";");
            stringBuilder.Append("return true;");
            RadButtonDisabilitaNumero.Attributes.Add("onclick", stringBuilder.ToString());

            stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            stringBuilder.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            stringBuilder.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            stringBuilder.Append("this.value = 'Attendere...';");
            stringBuilder.Append("this.disabled = true;");
            stringBuilder.Append(Page.ClientScript.GetPostBackEventReference(RadButtonSalvaNumero, null));
            stringBuilder.Append(";");
            stringBuilder.Append("return true;");
            RadButtonSalvaNumero.Attributes.Add("onclick", stringBuilder.ToString());

            #endregion

        }

        private void LoadInfo()
        {
            int idLav;
            if (Int32.TryParse(Request["idLavoratore"], out idLav))
            {
                Lavoratore lavoratore = _lavoratoriManager.GetLavoratoreById(idLav);
                RadTextBoxIdLav.Text = lavoratore.Id.ToString();
                RadTextBoxPin.Text = lavoratore.Pin;
                RadTextBoxUsername.Text = lavoratore.Utente != null && lavoratore.Utente.Password != null ? lavoratore.Utente.Username : String.Empty;
                LabelUsername.Text = lavoratore.DataInvioSmsUsername != null ? $"Username inviato in data {lavoratore.DataInvioSmsUsername.ToString()}" : String.Empty;
                if (lavoratore.IdUtente != null)
                {
                    SmsRecapitoUtente recapito = _lavoratoriManager.GetRecapitoSmsLavoratore(lavoratore.IdUtente.Value);
                    RadTextBoxTelefono.Text = recapito != null ? recapito.Numero : String.Empty;
                }
            }

            RadTextBoxTelefono.ReadOnly = true;
            RadButtonModificaNumero.Enabled = true;
            RadButtonSalvaNumero.Enabled = false;
            RadButtonDisabilitaNumero.Enabled = !String.IsNullOrEmpty(RadTextBoxTelefono.Text);
        }

        protected void RadButtonModificaNumero_Click(object sender, EventArgs e)
        {
            RadTextBoxTelefono.ReadOnly = false;
            RadButtonSalvaNumero.Enabled = true;
            RadButtonModificaNumero.Enabled = false;
            RadButtonDisabilitaNumero.Enabled = !String.IsNullOrEmpty(RadTextBoxTelefono.Text);
        }

        protected void RadButtonSalvaNumero_Click(object sender, EventArgs e)
        {
            //verifico che il numero imputato sia valido
            string numero = RadTextBoxTelefono.Text;
            if (!String.IsNullOrEmpty(numero))
            {
                numero = CellulareHelper.PulisciNumeroCellulare(numero);

                if (CellulareHelper.NumeroCellulareValido(numero))
                {
                    //numero ok
                    //int ret = _smsBusiness.RegistraLavoratoreSms(Convert.ToInt32(RadTextBoxIdLav.Text), numero);

                    //string tipoErroreSms = string.Empty;

                    //switch (ret)
                    //{
                    //    case -1:
                    //        tipoErroreSms = "Lavoratore non presente";
                    //        break;
                    //    case -2:
                    //        tipoErroreSms = "Numero di telefono già in uso";
                    //        break;
                    //    case -3:
                    //        tipoErroreSms = "Lavoratore già registrato al servizio";
                    //        break;
                    //}

                    //if (ret != 0)
                    //{
                    //    LabelError.Text =
                    //        string.Format(
                    //            "Errore durante l'iscrizione al servizio sms per il lavoratore con codice {0}\n{1}Codice errore: {2} - {3}",
                    //            RadTextBoxIdLav.Text, Environment.NewLine, ret, tipoErroreSms);
                    //}
                    //else
                    //{
                    //    LabelError.Text = "Numero registrato";
                    //    LoadInfo();
                    //}

                    Int32? idLavoratoreStessoRecapito;
                    bool ret = _lavoratoriManager.SaveRecapitoSmsLavoratore(Convert.ToInt32(RadTextBoxIdLav.Text), numero, out idLavoratoreStessoRecapito);
                    if (ret)
                    {
                        LabelError.Text = "Numero registrato";
                        //TODO: Inviare SMS di notifica!!!
                        LoadInfo();
                    }
                    else
                    {
                        if (idLavoratoreStessoRecapito.HasValue)
                        {
                            LabelError.Text = String.Format("Il numero di telefono è già associato al lavoratore {0}", idLavoratoreStessoRecapito);
                        }
                        else
                        {
                            LabelError.Text = "Errore durante l'operazione";
                        }
                    }
                }
                else
                {
                    LabelError.Text = "Fornire un numero valido";
                }
            }
            else
            {
                LabelError.Text = "Compilare il campo";
            }
        }

        protected void RadButtonGeneraPin_Click(object sender, EventArgs e)
        {
            _gestioneUtenti.GeneraPinLavoratore(Convert.ToInt32(RadTextBoxIdLav.Text));
            LoadInfo();
        }

        protected void RadButtonInviaPinSms_Click(object sender, EventArgs e)
        {
            string pin = HttpUtility.HtmlDecode(RadTextBoxPin.Text);
            string numeroTelefono = HttpUtility.HtmlDecode(RadTextBoxTelefono.Text);
            if (pin.Length != 8)
            {
                LabelError.Text = "Generare prima un PIN!";
            }
            else if (String.IsNullOrEmpty(numeroTelefono))
            {
                LabelError.Text = "Indicare un numero di telefono!";
            }
            else
            {
                numeroTelefono = CellulareHelper.PulisciNumeroCellulare(numeroTelefono);

                if (CellulareHelper.NumeroCellulareValido(numeroTelefono))
                {
                    //numero ok
                    int? idUtente = null;
                    Lavoratore lavoratore = _lavoratoriManager.GetLavoratoreById(Convert.ToInt32(RadTextBoxIdLav.Text));
                    if (lavoratore.IdUtente != null)
                    {
                        idUtente = lavoratore.IdUtente.Value;
                    }

                    SmsDaInviare smsDaInviare = new SmsDaInviare
                    {
                        Data = DateTime.Now,
                        Tipologia = TipologiaSms.Pin,
                        Testo =
                                                            String.Format(ConfigurationManager.AppSettings["PINLAVORATORE"],
                                                                          pin),
                        Destinatari =
                                                            (new List<SmsDaInviare.Destinatario>
                                                                 {
                                                                 new SmsDaInviare.Destinatario
                                                                     {
                                                                         Numero = numeroTelefono,
                                                                         IdUtenteSiceInfo = idUtente
                                                                     }
                                                                 }).ToArray()
                    };

                    SmsServiceClient client = new SmsServiceClient();
                    bool invio = client.InviaSms(smsDaInviare);

                    if (invio)
                    {
                        _lavoratoriManager.SaveNotificaInvioSmsPin(Convert.ToInt32(RadTextBoxIdLav.Text));

                        LabelError.Text = String.Format("PIN inviato via SMS al numero {0}", numeroTelefono);
                    }
                    else
                    {
                        LabelError.Text = "Errore durante l'invio dell'SMS";
                    }
                }
                else
                {
                    LabelError.Text = "Numero non valido";
                }
            }
        }

        protected void RadButtonDisabilitaNumero_Click(object sender, EventArgs e)
        {
            Lavoratore lavoratore = _lavoratoriManager.GetLavoratoreById(Convert.ToInt32(RadTextBoxIdLav.Text));
            if (lavoratore.IdUtente != null)
            {
                _lavoratoriManager.DisabilitaRecapito(lavoratore.IdUtente.Value);
            }

            LoadInfo();
        }

        protected void RadButtonInviaUsernameSms_Click(object sender, EventArgs e)
        {
            string user = HttpUtility.HtmlDecode(RadTextBoxUsername.Text);
            string numeroTelefono = HttpUtility.HtmlDecode(RadTextBoxTelefono.Text);
            if (user.Length < 1 || user == null)
            {
                LabelError.Text = "L'utente non possiede uno username!";
            }
            else if (String.IsNullOrEmpty(numeroTelefono))
            {
                LabelError.Text = "Indicare un numero di telefono!";
            }
            else
            {
                numeroTelefono = CellulareHelper.PulisciNumeroCellulare(numeroTelefono);

                if (CellulareHelper.NumeroCellulareValido(numeroTelefono))
                {
                    //numero ok
                    int? idUtente = null;
                    Lavoratore lavoratore = _lavoratoriManager.GetLavoratoreById(Convert.ToInt32(RadTextBoxIdLav.Text));
                    if (lavoratore.IdUtente != null)
                    {
                        idUtente = lavoratore.IdUtente.Value;
                    }

                    SmsDaInviare smsDaInviare = new SmsDaInviare
                    {
                        Data = DateTime.Now,
                        Tipologia = TipologiaSms.Username,
                        Testo =
                                                            String.Format(ConfigurationManager.AppSettings["USERLAVORATORE"],
                                                                          user),
                        Destinatari =
                                                            (new List<SmsDaInviare.Destinatario>
                                                                 {
                                                                 new SmsDaInviare.Destinatario
                                                                     {
                                                                         Numero = numeroTelefono,
                                                                         IdUtenteSiceInfo = idUtente
                                                                     }
                                                                 }).ToArray()
                    };

                    SmsServiceClient client = new SmsServiceClient();
                    bool invio = client.InviaSms(smsDaInviare);

                    if (invio)
                    {
                        _lavoratoriManager.SaveNotificaInvioSmsUsername(Convert.ToInt32(RadTextBoxIdLav.Text));

                        LabelError.Text = String.Format("Username inviato via SMS al numero {0}", numeroTelefono);
                    }
                    else
                    {
                        LabelError.Text = "Errore durante l'invio dell'SMS";
                    }
                }
                else
                {
                    LabelError.Text = "Numero non valido";
                }
            }
        }
    }
}