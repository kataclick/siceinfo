﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiCambiaPassword.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiCambiaPassword" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo2" titolo="Gestione utenti" sottoTitolo="Cambio Password"
        runat="server" />
    <br />
    <asp:Panel ID="PanelPasswordScaduta" runat="server" Visible="false" BorderColor="Black"
        BorderStyle="Solid" Font-Bold="true" ForeColor="Black" BackColor="LightYellow"
        BorderWidth="1pt">
        <table class="standardTable">
            <tr>
                <td align="center">
                    <br />
                    ATTENZIONE:
                </td>
            </tr>
            <tr>
                <td align="center">
                    Il periodo di validità della tua password è teminato.<br />
                    Aggiornala per poter continuare ad utilizzare i servizi offerti dal sito.
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="panelCambioPassword" runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Username:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxUsername" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Vecchia password:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxVecchiaPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Nuova password:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNuovaPassword" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Ridigita nuova password:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNuovaPasswordRedigit" runat="server" TextMode="Password"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
                    dal username e dalla password attuale e deve contenere almeno una lettera e un numero.<br />
                    Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
                    pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
                    ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
                    dal sistema.
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="ButtonCambiaPassword" runat="server" Text="Conferma cambio password"
                        OnClick="ButtonCambiaPassword_Click" /><br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="LabelResponse" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>

