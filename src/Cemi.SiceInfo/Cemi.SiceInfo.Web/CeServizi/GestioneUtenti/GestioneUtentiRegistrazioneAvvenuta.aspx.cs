﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneUtentiRegistrazioneAvvenuta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.GestioneUtentiRegistrazione,
                                                             FunzionalitaPredefinite.GestioneUtentiGestisciUtenti
                                                         };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        }
    }
}