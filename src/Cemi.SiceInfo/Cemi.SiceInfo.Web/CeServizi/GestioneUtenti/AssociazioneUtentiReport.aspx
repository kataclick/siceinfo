﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="AssociazioneUtentiReport.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.AssociazioneUtentiReport" %>
<%@ Register Src="../WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc14" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/ListaUtentiLavoratori.ascx" TagName="ListaUtentiLavoratori"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/ListaUtentiImprese.ascx" TagName="ListaUtentiImprese"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/ListaUtentiDipendenti.ascx" TagName="ListaUtentiDipendenti"
    TagPrefix="uc4" %>
<%@ Register Src="WebControls/ListaUtentiConsulenti.ascx" TagName="ListaUtentiConsulenti"
    TagPrefix="uc5" %>
<%@ Register Src="WebControls/ListaUtentiSindacalisti.ascx" TagName="ListaUtentiSindacalisti"
    TagPrefix="uc6" %>
<%@ Register Src="WebControls/ListaUtentiCommittenti.ascx" TagName="ListaUtentiCommittenti"
    TagPrefix="uc7" %>
<%@ Register Src="WebControls/ListaUtentiEsattori.ascx" TagName="ListaUtentiEsattori"
    TagPrefix="uc8" %>
<%@ Register Src="WebControls/ListaUtentiFornitori.ascx" TagName="ListaUtentiFornitori"
    TagPrefix="uc9" %>
<%@ Register Src="WebControls/ListaUtentiIspettori.ascx" TagName="ListaUtentiIspettori"
    TagPrefix="uc10" %>
<%@ Register Src="WebControls/ListaUtentiAsl.ascx" TagName="ListaUtentiAsl"
    TagPrefix="uc11" %>
<%@ Register Src="WebControls/ListaUtentiOspiti.ascx" TagName="ListaUtentiOspiti"
    TagPrefix="uc12" %>
<%@ Register Src="WebControls/ListaUtentiCasseedili.ascx" TagName="ListaUtentiCasseEdili"
    TagPrefix="uc13" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Gestione utenti" sottoTitolo="Gestione Utenti - Report"
        runat="server" />
    <br />
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
   
    <asp:Label ID="Label1" runat="server" Text="Seleziona il report da associare: "></asp:Label>
    <telerik:RadComboBox ID="RadComboBoxReport" MarkFirstMatch="true" EnableScreenBoundaryDetection="true"
        AutoPostBack="true" runat="server" OnItemDataBound="RadComboBox_ItemDataBound" OnSelectedIndexChanged="RadComboBoxReport_SelectedIndexChanged"
        Width="600px">
        <HeaderTemplate>
            <ul>
                <li class="col1">Nome</li>
                <li class="col2">Path</li>
            </ul>
        </HeaderTemplate>
        <ItemTemplate>
            <ul>
                <li class="col1">
                    <%# DataBinder.Eval(Container.DataItem, "Nome") %></li>
                <li class="col2">
                    <%# DataBinder.Eval(Container.DataItem, "Path") %></li>
            </ul>
        </ItemTemplate>
    </telerik:RadComboBox>
    <br />
    <br />
    <asp:Label ID="LabelReportSelezionato" runat="server" Text="Report selezionato:" Font-Bold="true"></asp:Label>
    <br /><br />
    <div class="borderedDiv">
        <asp:Label ID="Label3" runat="server" Text="Seleziona tipologia di utente: "></asp:Label>
        <telerik:RadComboBox ID="DropDownListTipoUtente" MarkFirstMatch="true" EnableScreenBoundaryDetection="false"
            AutoPostBack="true" runat="server" OnSelectedIndexChanged="DropDownListTipoUtente_SelectedIndexChanged"
            Width="132px">
            <Items>
                <telerik:RadComboBoxItem Value="Lavoratore" Text="Lavoratore" Selected="true" />
                <telerik:RadComboBoxItem Value="Impresa" Text="Impresa" />
                <telerik:RadComboBoxItem Value="Dipendente" Text="Dipendente" />
                <telerik:RadComboBoxItem Value="Ospite" Text="Ospite" />
                <telerik:RadComboBoxItem Value="Fornitore" Text="Fornitore" />
                <telerik:RadComboBoxItem Value="Ispettore" Text="Ispettore" />
                <telerik:RadComboBoxItem Value="Esattore" Text="Esattore" />
                <telerik:RadComboBoxItem Value="Consulente" Text="Consulente" />
                <telerik:RadComboBoxItem Value="Sindacalista" Text="Sindacalista" />
                <telerik:RadComboBoxItem Value="CassaEdile" Text="CassaEdile" />
                <telerik:RadComboBoxItem Value="ASL" Text="ASL" />
                <telerik:RadComboBoxItem Value="Committente" Text="Committente" />
            </Items>
        </telerik:RadComboBox>
        <br />
        <br />
        <asp:Label ID="LabelTipoUtenteVisualizzato" runat="server" Text="Tipologia utente visualizzata:"
            Font-Bold="True"></asp:Label>
        <br />
        <br />
        <telerik:RadMultiPage ID="RadMultiPage1" RenderSelectedPageOnly="true" runat="server"
            SelectedIndex="0">
            <telerik:RadPageView ID="RadPageview1" runat="server">
                <uc1:ListaUtentiLavoratori ID="ListaUtentiLavoratori1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageview2" runat="server">
                <uc3:ListaUtentiImprese ID="ListaUtentiImprese1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageview3" runat="server">
                <uc4:ListaUtentiDipendenti ID="ListaUtentiDipendenti1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView4" runat="server">
                <uc12:ListaUtentiOspiti ID="ListaUtentiOspiti1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView5" runat="server">
                <uc9:ListaUtentiFornitori ID="ListaUtentiFornitori1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView6" runat="server">
                <uc10:ListaUtentiIspettori ID="ListaUtentiIspettori1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView7" runat="server">
                <uc8:ListaUtentiEsattori ID="ListaUtentiEsattori1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView8" runat="server">
                <uc5:ListaUtentiConsulenti ID="ListaUtentiConsulenti1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView9" runat="server">
                <uc6:ListaUtentiSindacalisti ID="ListaUtentiSindacalisti1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView11" runat="server">
                <uc13:ListaUtentiCasseEdili ID="ListaUtentiCasseEdili1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView12" runat="server">
                <uc11:ListaUtentiAsl ID="ListaUtentiAsl1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView10" runat="server">
                <uc7:ListaUtentiCommittenti ID="ListaUtentiCommittenti1" runat="server" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    </telerik:RadAjaxPanel>
    <asp:Panel ID="PanelError" runat="server" Visible="false">Nessun report disponibile. Clicca <a href="InserimentoReport.aspx">qui</a> per inserire un report.</asp:Panel>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc14:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>

