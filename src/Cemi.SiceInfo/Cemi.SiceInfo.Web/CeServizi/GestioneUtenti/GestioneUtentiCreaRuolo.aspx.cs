﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneUtentiCreaRuolo : System.Web.UI.Page
    {
        private GestioneUtentiBiz _gestioneUtentiBiz;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti);

            _gestioneUtentiBiz = new GestioneUtentiBiz();

            if (!Page.IsPostBack)
            {
                //Carichiamo le funzionalità nella checklist
                FunzionalitaCollection funzionalita = _gestioneUtentiBiz.GetFunzionalita();

                for (int j = 0; j < funzionalita.Count; j++)
                {
                    if (funzionalita[j].Assegnabile)
                        CheckBoxListFunzionalita.Items.Add(
                            new ListItem(funzionalita[j].Descrizione, funzionalita[j].IdFunzionalita.ToString()));
                }
            }
        }

        protected void ButtonCreaRuolo_Click(object sender, EventArgs e)
        {
            FunzionalitaCollection
                funzionalita = new FunzionalitaCollection();

            for (int k = 0; k < CheckBoxListFunzionalita.Items.Count; k++)
            {
                if (CheckBoxListFunzionalita.Items[k].Selected)
                {
                    funzionalita.Add(new Funzionalita(Int32.Parse(CheckBoxListFunzionalita.Items[k].Value), "", ""));
                }
            }

            //Controlliamo che il ruolo sia definito correttamente
            if (TextBoxNomeRuolo.Text != string.Empty && TextBoxDescrizioneRuolo.Text != string.Empty
                && ControllaFunzionalitaSelezionate())
            {
                Ruolo ruolo = new Ruolo(TextBoxNomeRuolo.Text, TextBoxDescrizioneRuolo.Text);

                _gestioneUtentiBiz.CreaRuolo(ruolo, funzionalita);

                string messaggio = "Ruolo creato con successo.";
                SettaMessaggio(messaggio);
            }
            else
            {
                string messaggio = "Completare correttamente i campi prima creare il ruolo.";
                SettaMessaggio(messaggio);
            }

            ResettaCampi();
        }

        /// <summary>
        /// Controlla che le funzionalità selezionate siano corrette. In questa implementazione il controllo ritorna
        /// true se almeno una funzionalità è stata selezionata
        /// </summary>
        /// <returns></returns>
        private bool ControllaFunzionalitaSelezionate()
        {
            for (int k = 0; k < CheckBoxListFunzionalita.Items.Count; k++)
            {
                if (CheckBoxListFunzionalita.Items[k].Selected)
                {
                    return true;
                }
            }

            return false;
        }

        private void ResettaCampi()
        {
            //
            for (int k = 0; k < CheckBoxListFunzionalita.Items.Count; k++)
            {
                CheckBoxListFunzionalita.Items[k].Selected = false;
            }

            TextBoxDescrizioneRuolo.Text = string.Empty;
            TextBoxNomeRuolo.Text = string.Empty;
        }

        public void SettaMessaggio(string messaggio)
        {
            Control mess = Master.FindControl("Messaggio1");
            mess.Visible = true;
            ((Label)(mess).FindControl("Label1")).Text = messaggio;
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Response.Redirect("GestioneUtentiDefault.aspx");
        }
    }
}