﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiUtenti_Ruoli.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiUtenti_Ruoli"  %>
<%@ Register Src="../WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/GestioneUtentiListaASL.ascx" TagName="GestioneUtentiListaASL"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/GestioneUtentiListaCasseEdili.ascx" TagName="GestioneUtentiListaCasseEdili"
    TagPrefix="uc4" %>
<%@ Register Src="WebControls/GestioneUtentiListaCommittenti.ascx" TagName="GestioneUtentiListaCommittenti"
    TagPrefix="uc5" %>
<%@ Register Src="WebControls/GestioneUtentiListaConsulenti.ascx" TagName="GestioneUtentiListaConsulenti"
    TagPrefix="uc6" %>
<%@ Register Src="WebControls/GestioneUtentiListaDipendenti.ascx" TagName="GestioneUtentiListaDipendenti"
    TagPrefix="uc7" %>
<%@ Register Src="WebControls/GestioneUtentiListaEsattori.ascx" TagName="GestioneUtentiListaEsattori"
    TagPrefix="uc8" %>
<%@ Register Src="WebControls/GestioneUtentiListaFornitori.ascx" TagName="GestioneUtentiListaFornitori"
    TagPrefix="uc9" %>
<%@ Register Src="WebControls/GestioneUtentiListaImprese.ascx" TagName="GestioneUtentiListaImprese"
    TagPrefix="uc10" %>
<%@ Register Src="WebControls/GestioneUtentiListaIspettori.ascx" TagName="GestioneUtentiListaIspettori"
    TagPrefix="uc11" %>
<%@ Register Src="WebControls/GestioneUtentiListaLavoratori.ascx" TagName="GestioneUtentiListaLavoratori"
    TagPrefix="uc12" %>
<%@ Register Src="WebControls/GestioneUtentiListaOspiti.ascx" TagName="GestioneUtentiListaOspiti"
    TagPrefix="uc13" %>
<%@ Register Src="WebControls/GestioneUtentiListaSindacalisti.ascx" TagName="GestioneUtentiListaSindacalisti"
    TagPrefix="uc14" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Gestione utenti" sottoTitolo="Gestione Utenti - Ruoli"
        runat="server" />
    <br />
    <asp:Label ID="Label3" runat="server" Text="Seleziona tipologia di utente: "></asp:Label>
    <telerik:RadComboBox ID="DropDownListTipoUtente" MarkFirstMatch="true" EnableScreenBoundaryDetection="false"
        AutoPostBack="true" runat="server" OnSelectedIndexChanged="DropDownListTipoUtente_SelectedIndexChanged"
        Width="132px">
        <Items>
            <telerik:RadComboBoxItem Value="Lavoratore" Text="Lavoratore" Selected="true" />
            <telerik:RadComboBoxItem Value="Impresa" Text="Impresa" />
            <telerik:RadComboBoxItem Value="Dipendente" Text="Dipendente" />
            <telerik:RadComboBoxItem Value="Ospite" Text="Ospite" />
            <telerik:RadComboBoxItem Value="Fornitore" Text="Fornitore" />
            <telerik:RadComboBoxItem Value="Ispettore" Text="Ispettore" />
            <telerik:RadComboBoxItem Value="Esattore" Text="Esattore" />
            <telerik:RadComboBoxItem Value="Consulente" Text="Consulente" />
            <telerik:RadComboBoxItem Value="Sindacalista" Text="Sindacalista" />
            <telerik:RadComboBoxItem Value="CassaEdile" Text="CassaEdile" />
            <telerik:RadComboBoxItem Value="ASL" Text="ASL" />
            <telerik:RadComboBoxItem Value="Committente" Text="Committente" />
        </Items>
    </telerik:RadComboBox>
    <asp:Button ID="ButtonVai" runat="server" OnClick="ButtonVai_Click" Text="Vai" /><br />
    <br />
    <asp:Label ID="LabelTipoUtenteVisualizzato" runat="server" Text="Tipologia utente visualizzata:"
        Font-Bold="True"></asp:Label>
    <br />
    <br />
    <telerik:RadMultiPage ID="RadMultiPage1" RenderSelectedPageOnly="true" runat="server"
        SelectedIndex="0">
        <telerik:RadPageView ID="RadPageview1" runat="server">
            <uc12:GestioneUtentiListaLavoratori ID="GestioneUtentiListaLavoratori1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageview2" runat="server">
            <uc10:GestioneUtentiListaImprese ID="GestioneUtentiListaImprese1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageview3" runat="server">
            <uc7:GestioneUtentiListaDipendenti ID="GestioneUtentiListaDipendenti1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView4" runat="server">
            <uc13:GestioneUtentiListaOspiti ID="GestioneUtentiListaOspiti1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView5" runat="server">
            <uc9:GestioneUtentiListaFornitori ID="GestioneUtentiListaFornitori1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView6" runat="server">
            <uc11:GestioneUtentiListaIspettori ID="GestioneUtentiListaIspettori1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView7" runat="server">
            <uc8:GestioneUtentiListaEsattori ID="GestioneUtentiListaEsattori1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView8" runat="server">
            <uc6:GestioneUtentiListaConsulenti ID="GestioneUtentiListaConsulenti1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView9" runat="server">
            <uc14:GestioneUtentiListaSindacalisti ID="GestioneUtentiListaSindacalisti1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView11" runat="server">
            <uc4:GestioneUtentiListaCasseEdili ID="GestioneUtentiListaCasseEdili1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView12" runat="server">
            <uc3:GestioneUtentiListaASL ID="GestioneUtentiListaASL1" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView10" runat="server">
            <uc5:GestioneUtentiListaCommittenti ID="GestioneUtentiListaCommittenti1" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>

