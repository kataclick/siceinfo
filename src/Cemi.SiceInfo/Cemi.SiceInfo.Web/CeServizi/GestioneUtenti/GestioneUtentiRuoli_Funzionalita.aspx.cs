﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using Cemi.SiceInfo.Web.CeServizi.WebControls;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneUtentiRuoli_Funzionalita : System.Web.UI.Page
    {
        private GestioneUtentiBiz _gestioneUtentiBiz;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti);

            _gestioneUtentiBiz = new GestioneUtentiBiz();

            if (!Page.IsPostBack)
            {
                AggiornaListaRuoli();
            }

            Resetta();
        }

        private void AggiornaListaRuoli()
        {
            DropDownListRuoli.Items.Clear();

            //Carichiamo la lista dei ruoli presenti nel sistema
            RuoliCollection ruoli = _gestioneUtentiBiz.GetRuoli();

            for (int i = 0; i < ruoli.Count; i++)
            {
                if (ruoli[i].Modificabile)
                    DropDownListRuoli.Items.Add(new ListItem(ruoli[i].Nome, ruoli[i].IdRuolo.ToString()));
            }

            //Carichiamo la lista di funzionalità del sistema
            FunzionalitaCollection funzionalita = _gestioneUtentiBiz.GetFunzionalita();

            CheckBoxListFunzionalita.Items.Clear();
            for (int j = 0; j < funzionalita.Count; j++)
            {
                if (funzionalita[j].Assegnabile)
                    CheckBoxListFunzionalita.Items.Add(
                        new ListItem(funzionalita[j].Descrizione, funzionalita[j].IdFunzionalita.ToString()));
            }

            //Controlliamo e settiamo le funzionalità del ruolo selezionato
            //this.CheckFunzionalita();
        }

        private void CheckFunzionalita(Ruolo ruolo)
        {
            ButtonEliminaRuolo.Enabled = !ruolo.Predefinito;

            //if (ruolo.Predefinito)
            //    this.ButtonEliminaRuolo.Enabled = false;

            /* controllo le funzionalità associate all'utente */
            FunzionalitaCollection funzionalitaAbilitate =
                _gestioneUtentiBiz.GetFunzionalitaRuolo(Int32.Parse(DropDownListRuoli.SelectedItem.Value));

            for (int k = 0; k < CheckBoxListFunzionalita.Items.Count; k++)
            {
                CheckBoxListFunzionalita.Items[k].Selected = false;
                for (int x = 0; x < funzionalitaAbilitate.Count; x++)
                {
                    if (CheckBoxListFunzionalita.Items[k].Value == funzionalitaAbilitate[x].IdFunzionalita.ToString())
                    {
                        CheckBoxListFunzionalita.Items[k].Selected = true;
                        break;
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            FunzionalitaCollection funzionalita = new FunzionalitaCollection();

            for (int k = 0; k < CheckBoxListFunzionalita.Items.Count; k++)
            {
                if (CheckBoxListFunzionalita.Items[k].Selected)
                {
                    funzionalita.Add(new Funzionalita(
                                         Int32.Parse(CheckBoxListFunzionalita.Items[k].Value), "", ""));
                }
            }

            int idRuolo = Int32.Parse(DropDownListRuoli.SelectedItem.Value);

            //Controlliamo che il ruolo sia definito correttamente
            if (TextBoxNomeRuolo.Text != string.Empty && TextBoxDescrizioneRuolo.Text != string.Empty
                && ControllaFunzionalitaSelezionate())
            {
                Ruolo ruolo = new Ruolo(idRuolo, TextBoxNomeRuolo.Text, TextBoxDescrizioneRuolo.Text);

                _gestioneUtentiBiz.AggiornaFunzionalitaRuolo(ruolo, funzionalita);

                //this.LabelResponse.Text = "Ruolo aggironato con successo";

                string messaggio = "Ruolo aggiornato con successo.";
                SettaMessaggio(messaggio);

                AggiornaListaRuoli();

                Resetta();
            }
            else
            {
                //this.LabelResponse.Text = "Il ruolo non è stato ridefinito correttamente. Riprovare.";
                string messaggio = "Errore durante l'aggiornamento del ruolo.";
                SettaMessaggio(messaggio);
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelezionaRuoloDaModificare();
        }

        private void SelezionaRuoloDaModificare()
        {
            //Cancelliamo e nascondiamo il testo del messaggio della master pages usato per indicare l'avvenuto salvataggio
            CancellaMessaggio();

            //carichiamo il nuovo ruolo da modificare
            MessaggioTitolo.Visible = true;
            MessaggioTitolo.Titolo = "Ruolo selezionato: " + DropDownListRuoli.SelectedItem.Text;

            Ruolo ruoloSelezionato = _gestioneUtentiBiz.GetRuolo(Int32.Parse(DropDownListRuoli.SelectedItem.Value));
            if (ruoloSelezionato != null)
            {
                TextBoxNomeRuolo.Text = ruoloSelezionato.Nome;
                TextBoxDescrizioneRuolo.Text = ruoloSelezionato.Descrizione;

                CheckFunzionalita(ruoloSelezionato);
            }
            else
            {
                TextBoxNomeRuolo.Text = string.Empty;
                TextBoxDescrizioneRuolo.Text = string.Empty;
            }

            Panel1.Visible = true;
        }

        /// <summary>
        /// Controlla che le funzionalità selezionate siano corrette. In questa implementazione il controllo ritorna
        /// true se almeno una funzionalità è stata selezionata
        /// </summary>
        /// <returns></returns>
        private bool ControllaFunzionalitaSelezionate()
        {
            for (int k = 0; k < CheckBoxListFunzionalita.Items.Count; k++)
            {
                if (CheckBoxListFunzionalita.Items[k].Selected)
                {
                    return true;
                }
            }

            return false;
        }

        protected void ButtonEliminaRuolo_Click(object sender, EventArgs e)
        {
            int idRuolo = Int32.Parse(DropDownListRuoli.SelectedItem.Value);

            //Controlliamo che il ruolo sia definito correttamente
            _gestioneUtentiBiz.CancellaRuolo(idRuolo);

            AggiornaListaRuoli();

            Resetta();
        }

        public void SettaMessaggio(string messaggio)
        {
            try
            {

                Messaggio mess = (Messaggio)Master.FindControl("Messaggio1");
                mess.Visible = true;
                ((Label)(mess).FindControl("Label1")).Text = messaggio;
            }
            catch
            {
            }
        }

        public void CancellaMessaggio()
        {
            try
            {

                Messaggio mess = (Messaggio)Master.FindControl("Messaggio1");
                mess.Visible = false;
                ((Label)(mess).FindControl("Label1")).Text = string.Empty;
            }
            catch
            {
            }
        }

        private void Resetta()
        {
            Panel1.Visible = false;
            MessaggioTitolo.Visible = false;
        }

        protected void ButtonModificaRuolo_Click(object sender, EventArgs e)
        {
            SelezionaRuoloDaModificare();
        }
    }
}