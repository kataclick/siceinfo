﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrazioneImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls.RegistrazioneImpresa" %>
<%@ Register Src="../../WebControls/LiberatoriaPrivacy.ascx" TagName="LiberatoriaPrivacy" TagPrefix="uc1" %>
Tutti i campi sono obbligatori<br />
<br />
<table width="600pt">
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" Text="Codice cassa edile:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodice" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxCodice"
                ErrorMessage="*"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCodice"
                ErrorMessage="Codice numerico" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label2" runat="server" Text="PIN:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPIN" runat="server" MaxLength="8"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBoxPIN"
                ErrorMessage="*" Width="1px"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxPIN"
                ErrorMessage="Formato PIN non corretto" ValidationExpression="^[A-Za-z0-9]{8}$"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelUsername" runat="server" Text="Username:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxLogin" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxLogin"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPassword" runat="server" Text="Password:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxPassword"
                ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPasswordRidigitata" runat="server" Text="Ridigita password:"
                Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPasswordRidigitata" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxPasswordRidigitata"
                ErrorMessage="*"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPassword"
                ControlToValidate="TextBoxPasswordRidigitata" ErrorMessage="Password diversa"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
			dal username e deve contenere almeno una lettera e un numero.<br />
			Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
			pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
			ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
			dal sistema.
        </td>
    </tr>
    <tr>
        <td colspan="3" height="5pt">
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <uc1:LiberatoriaPrivacy ID="LiberatoriaPrivacy1" runat="server"></uc1:LiberatoriaPrivacy>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <asp:Button ID="ButtonRegistraImpresa" runat="server" OnClick="ButtonRegistraImpresa_Click"
                Text="Registra Impresa" />
            <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
                CausesValidation="False" />
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <asp:Label ID="LabelResult" ForeColor="red" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<br />
