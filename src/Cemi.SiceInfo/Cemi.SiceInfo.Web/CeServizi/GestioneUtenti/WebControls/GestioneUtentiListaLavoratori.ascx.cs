﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls
{
    public partial class GestioneUtentiListaLavoratori : System.Web.UI.UserControl
    {
        private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadGridUtenti_SelectedIndexChanging(object sender, EventArgs eventArgs)
        {
            int idUtente =
                (Int32)
                RadGridLavoratori.MasterTableView.DataKeyValues[
                    Int32.Parse(RadGridLavoratori.SelectedIndexes[0])]["IdUtente"];
            GestioneUtentiListaRuoli1.CaricaRuoliUtenteSelezionato(idUtente);
        }


        protected void ButtonVai_Click(object sender, EventArgs e)
        {
            CaricaListaUtenti();
        }

        protected void RadGridUtenti_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtenti();
        }

        private void BindUtenti()
        {
            RadGridLavoratori.DataSource = gu.GetUtentiLavoratori();
        }

        private void CaricaListaUtenti()
        {
            GestioneUtentiListaRuoli1 = new GestioneUtentiListaRuoli();
            BindUtenti();
        }

        protected void ButtonClearFilters_Click(object sender, ImageClickEventArgs e)
        {
            RadGridLavoratori.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridLavoratori.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridLavoratori.MasterTableView.Rebind();
        }

        protected void RadGrid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ResetPwd")
            {
                string username = e.Item.Cells[2].Text;
                Response.Redirect(String.Format("ResetPassword.aspx?username={0}", Server.UrlEncode(username)));
            }
        }
    }
}