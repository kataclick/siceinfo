﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls
{
    public partial class GestioneUtentiListaRuoli : System.Web.UI.UserControl
    {
        private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

        private Int32 IdUtente
        {
            get { return (Int32)ViewState["idUtente"]; }
            set { ViewState["idUtente"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PanelUtente.Visible = false;
        }

        protected void ButtonAggiornaRuoliUtente_Click(object sender, EventArgs e)
        {
            RuoliCollection ruoli = new RuoliCollection();

            //Assegnamo i ruoli "normali"
            for (int k = 0; k < CheckBoxListRuoli.Items.Count; k++)
            {
                if (CheckBoxListRuoli.Items[k].Selected)
                {
                    ruoli.Add(new Ruolo(
                                  Int32.Parse(CheckBoxListRuoli.Items[k].Value), "", ""));
                }
            }

            //Assegnamo il ruolo utente disabilitato
            if (!CheckBoxUtenteAttivo.Checked)
            {
                ruoli.Add(new Ruolo(
                              gu.GetRuolo(RuoliPredefiniti.UtenteDisabilitato.ToString()).IdRuolo, "", ""));
            }

            gu.AssegnaRuoliUtente(IdUtente, ruoli);

            //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
            //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
            //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
            //TBridge.Cemi.ActivityTracking.Log.Write("UtenteAggiornato", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.GESTIONEREPORT, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);

            string messaggio = "Profilo utente aggiornato.";
            SettaMessaggio(messaggio);
        }

        public void SettaMessaggio(string messaggio)
        {
            try
            {
                //WebControls_Messaggio mess = (WebControls_Messaggio)Master.FindControl("Messaggio1");
                //mess.Visible = true;
                //((Label)(mess).FindControl("Label1")).Text = messaggio;
            }
            catch
            {
            }
        }

        private void CheckRuoli(RuoliCollection ruoliUtente)
        {
            CheckBoxUtenteAttivo.Checked = true;

            for (int k = 0; k < CheckBoxListRuoli.Items.Count; k++)
            {
                CheckBoxListRuoli.Items[k].Selected = false;

                for (int x = 0; x < ruoliUtente.Count; x++)
                {
                    if (CheckBoxListRuoli.Items[k].Value == ruoliUtente[x].IdRuolo.ToString())
                    {
                        CheckBoxListRuoli.Items[k].Selected = true;
                        break;
                    }

                    if (ruoliUtente[x].Nome == RuoliPredefiniti.UtenteDisabilitato.ToString() ||
                        ruoliUtente[x].Nome == RuoliPredefiniti.InApprovazione.ToString())
                        CheckBoxUtenteAttivo.Checked = false;
                }
            }
        }

        public void CaricaRuoliUtenteSelezionato(int idUtente)
        {
            IdUtente = idUtente;
            CheckBoxListRuoli.Items.Clear();

            try
            {
                PanelUtente.Visible = true;


                //int idUtente = int.Parse(row.Cells[1].Text);

                if (IdUtente != -1)
                {
                    RuoliCollection ruoli = gu.GetRuoli();

                    //Carichiamo la lista dei ruoli associati all'utente
                    RuoliCollection ruoliUtente = gu.GetRuoliUtente(IdUtente);

                    for (int j = 0; j < ruoli.Count; j++)
                    {
                        if (ruoli[j].Modificabile)
                            CheckBoxListRuoli.Items.Add(
                                new ListItem(ruoli[j].Nome + ": " + ruoli[j].Descrizione, ruoli[j].IdRuolo.ToString()));
                    }

                    //Controlliamo e settiamo le funzionalità del ruolo selezionato
                    CheckRuoli(ruoliUtente);
                }
            }
            catch (Exception ex)
            {
                //
                PanelUtente.Visible = false;
            }
        }
    }
}