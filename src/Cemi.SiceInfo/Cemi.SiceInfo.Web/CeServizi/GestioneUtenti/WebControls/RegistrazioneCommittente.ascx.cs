﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls
{
    public partial class RegistrazioneCommittente : System.Web.UI.UserControl
    {
        private readonly GestioneUtentiBiz bizGU = new GestioneUtentiBiz();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaTipologieCommittenti();
                CaricaProvince();
            }
        }

        private void CaricaTipologieCommittenti()
        {
            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCommittenteTipologia,
                Enum.GetNames(typeof(TipologiaCommittente)),
                "",
                "");
        }

        private void CaricaProvince()
        {
            bizGU.CaricaProvinceInDropDown(DropDownListProvincia);
        }

        private void CaricaComuni(int idProvincia)
        {
            bizGU.CaricaComuniInDropDown(DropDownListComuni, idProvincia);
        }

        private void CaricaCAP(Int64 idComune)
        {
            bizGU.CaricaCapInDropDown(DropDownListCAP, idComune);
        }

        protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            bizGU.CambioSelezioneDropDownProvincia(DropDownListProvincia,
                                                   DropDownListComuni,
                                                   DropDownListCAP);
        }

        protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
        {
            bizGU.CambioSelezioneDropDownComune(DropDownListComuni, DropDownListCAP);
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("GestioneUtentiDefault.aspx");
        }

        protected void ButtonRegistraCommittente_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text))
                {
                    Committente committente = CreaCommittente();

                    ////Inseriamo il committente nel sistema

                    switch (bizGU.InserisciCommittente(committente))
                    {
                        case ErroriRegistrazione.RegistrazioneEffettuata:
                            SvuotaCampi();

                            LabelResult.Text = "Registrazione completata";

                            Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                            break;
                        case ErroriRegistrazione.ChallengeNonPassato:
                            LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                            break;
                        case ErroriRegistrazione.LoginPresente:
                            LabelResult.Text =
                                "La username scelta è già presente nel sistema, sceglierne un'altra e riprovare.";
                            break;
                        case ErroriRegistrazione.RegistrazioneGiaPresente:
                            LabelResult.Text = "La registrazione risulta come già effettuata.";
                            break;
                        case ErroriRegistrazione.Errore:
                            LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare più tardi.";
                            break;
                        case ErroriRegistrazione.LoginNonCorretta:
                            LabelResult.Text =
                                "La username fornita contiene caratteri non validi o la sua lunghezza non è compresa fra 5 e 15 caratteri.";
                            break;
                        case ErroriRegistrazione.PasswordNonCorretta:
                            LabelResult.Text =
                                "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                            break;
                        default:
                            LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare più tardi.";
                            break;
                    }

                    #region old
                    //if (bizGU.InserisciCommittente(committente))
                    //{
                    //    SvuotaCampi();
                    //    LabelResult.Text = "Registrazione completata";

                    //    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                    //}
                    //else
                    //{
                    //    LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare.";
                    //}
                    #endregion
                }
                else
                {
                    LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
                }
            }
        }

        private void SvuotaCampi()
        {
            Presenter.SvuotaCampo(TextBoxNome);
            Presenter.SvuotaCampo(TextBoxCognome);
            Presenter.SvuotaCampo(TextBoxRagioneSociale);
            Presenter.SvuotaCampo(TextBoxPartitaIva);
            Presenter.SvuotaCampo(TextBoxCodiceFiscale);
            Presenter.SvuotaCampo(TextBoxIndirizzo);
            DropDownListProvincia.SelectedIndex = 0;
            CaricaComuni(-1);
            Presenter.SvuotaCampo(TextBoxTelefono);
            Presenter.SvuotaCampo(TextBoxFax);
            Presenter.SvuotaCampo(TextBoxEmail);

            Presenter.SvuotaCampo(TextBoxEmail);
            Presenter.SvuotaCampo(TextBoxLogin);
            Presenter.SvuotaCampo(TextBoxPassword);
            DropDownListCommittenteTipologia.SelectedIndex = 0;
        }

        private Committente CreaCommittente()
        {
            //Committente committente = new Committente();
            Committente committente = new Committente(
                0,
                TextBoxLogin.Text
                );

            committente.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
            committente.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
            committente.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
            committente.PartitaIva = Presenter.NormalizzaCampoTesto(TextBoxPartitaIva.Text);
            committente.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
            committente.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
            if (!String.IsNullOrEmpty(DropDownListComuni.SelectedValue))
                committente.Comune = Int64.Parse(DropDownListComuni.SelectedValue);
            if (!String.IsNullOrEmpty(DropDownListProvincia.SelectedValue))
                committente.Provincia = Int32.Parse(DropDownListProvincia.SelectedValue);
            if (!String.IsNullOrEmpty(DropDownListCAP.SelectedValue))
                committente.Cap = DropDownListCAP.SelectedValue;
            committente.Telefono = Presenter.NormalizzaCampoTesto(TextBoxTelefono.Text);
            committente.Fax = Presenter.NormalizzaCampoTesto(TextBoxFax.Text);
            committente.Email = Presenter.NormalizzaCampoTesto(TextBoxEmail.Text);

            committente.Email = TextBoxEmail.Text;
            //committente.Username = TextBoxLogin.Text;
            committente.Password = TextBoxPassword.Text;

            committente.Tipologia = (TipologiaCommittente)
                                    Enum.Parse(typeof(TipologiaCommittente), DropDownListCommittenteTipologia.SelectedValue);

            return committente;
        }
    }
}