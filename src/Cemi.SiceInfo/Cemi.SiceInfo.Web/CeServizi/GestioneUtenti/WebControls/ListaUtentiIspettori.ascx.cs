﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControl
{
    public partial class ListaUtentiIspettori : System.Web.UI.UserControl
    {
        private readonly GestioneReport biz = new GestioneReport();
        private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

        private Int32 IdReport
        {
            get { return ViewState["idReport"] == null ? 0 : (int)ViewState["idReport"]; }
            set { ViewState["idReport"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RadGridIspettoriAssociati.Rebind();
                RadGridIspettoriAssociabili.Rebind();
            }
        }

        public void ImpostaReport(int idReport)
        {
            IdReport = idReport;
            RadGridIspettoriAssociati.MasterTableView.Rebind();
        }

        protected void RadGridUtenti_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtenti();
        }

        protected void RadGridUtentiAssociati_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtentiAssociati();
        }

        private void BindUtenti()
        {
            RadGridIspettoriAssociabili.DataSource = gu.GetUtentiIspettori();
        }

        private void BindUtentiAssociati()
        {
            RadGridIspettoriAssociati.DataSource = biz.GetUtentiReportIspettori(IdReport);
        }

        protected void RadGridUtentiAssociati_DeleteCommand(object source, GridCommandEventArgs e)
        {
            int idUtente =
                Convert.ToInt32(
                    (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUtente"].ToString());

            biz.DeleteUtenteReport(idUtente, IdReport);
        }

        protected void ButtonClearFiltersAssociabili_Click(object sender, ImageClickEventArgs e)
        {
            RadGridIspettoriAssociabili.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridIspettoriAssociabili.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridIspettoriAssociabili.MasterTableView.Rebind();
        }

        protected void ButtonClearFiltersAssociati_Click(object sender, ImageClickEventArgs e)
        {
            RadGridIspettoriAssociati.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridIspettoriAssociati.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridIspettoriAssociati.MasterTableView.Rebind();
        }

        protected void RadGridUtentiAssociabili_SelectedIndexChanging(object sender, EventArgs eventArgs)
        {
            int idUtente =
                (Int32)
                RadGridIspettoriAssociabili.MasterTableView.DataKeyValues[
                    Int32.Parse(RadGridIspettoriAssociabili.SelectedIndexes[0])]["IdUtente"];
            biz.AssociaUtenteReport(idUtente, IdReport);
            RadGridIspettoriAssociati.Rebind();
        }
    }
}