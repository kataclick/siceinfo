﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls
{
    public partial class RegistrazioneIspettore : System.Web.UI.UserControl
    {
        private readonly CantieriBusiness bizCA = new CantieriBusiness();
        private readonly GestioneUtentiBiz bizGU = new GestioneUtentiBiz();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (DropDownListZonaCantiere.Items.Count == 0)
                CaricaZone();
        }

        private void CaricaZone()
        {
            ZonaCollection listaZone = bizCA.GetZone(null);

            DropDownListZonaCantiere.Items.Clear();
            DropDownListZonaCantiere.Items.Insert(0, new ListItem(" Tutti", "0"));

            DropDownListZonaCantiere.DataSource = listaZone;
            DropDownListZonaCantiere.DataTextField = "Nome";
            DropDownListZonaCantiere.DataValueField = "IdZona";

            DropDownListZonaCantiere.DataBind();
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("GestioneUtentiDefault.aspx");
        }

        protected void ButtonRegistraIspettore_Click(object sender, EventArgs e)
        {
            if (GestioneUtentiBiz.ControllaFormatoPassword(TextBoxPassword.Text)
                && TextBoxPassword.Text == TextBoxPasswordRidigitata.Text
                && DropDownListZonaCantiere.SelectedValue != string.Empty)
            {
                //
                //Ispettore ispettore = new Ispettore();
                Ispettore ispettore = new Ispettore(
                    0,
                    TextBoxLogin.Text
                    );

                ispettore.Nome = TextBoxNome.Text.Trim();
                ispettore.Cognome = TextBoxCognome.Text.Trim();
                ispettore.Cellulare = TextBoxCellulare.Text.Trim();

                //TODO controllare che sia sempre presente un ID zona (è possibile che non esistano zone?)
                ispettore.IdCantieriZona = int.Parse(DropDownListZonaCantiere.SelectedValue);

                ispettore.Operativo = CheckBoxAttivo.Checked;

                //ispettore.Username = TextBoxLogin.Text;
                ispettore.Password = TextBoxPassword.Text;

                //Inseriamo il dipendente nel sistema
                switch (bizGU.InserisciIspettore(ispettore))
                {
                    case ErroriRegistrazione.RegistrazioneEffettuata:
                        TextBoxNome.Text = string.Empty;
                        TextBoxCognome.Text = string.Empty;
                        CheckBoxAttivo.Checked = true;
                        TextBoxLogin.Text = string.Empty;
                        TextBoxPassword.Text = string.Empty;
                        TextBoxPasswordRidigitata.Text = string.Empty;
                        TextBoxCellulare.Text = string.Empty;

                        LabelResult.Text = "Registrazione completata";

                        Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                        break;
                    case ErroriRegistrazione.ChallengeNonPassato:
                        LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                        break;
                    case ErroriRegistrazione.LoginPresente:
                        LabelResult.Text =
                            "La username scelta è già presente nel sistema, sceglierne un'altra e riprovare.";
                        break;
                    case ErroriRegistrazione.RegistrazioneGiaPresente:
                        LabelResult.Text = "La registrazione risulta come già effettuata.";
                        break;
                    case ErroriRegistrazione.Errore:
                        LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare più tardi.";
                        break;
                    case ErroriRegistrazione.LoginNonCorretta:
                        LabelResult.Text =
                            "La username fornita contiene caratteri non validi o la sua lunghezza non è compresa fra 5 e 15 caratteri.";
                        break;
                    case ErroriRegistrazione.PasswordNonCorretta:
                        LabelResult.Text =
                            "La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                        break;
                    default:
                        LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare più tardi.";
                        break;
                }
                #region old
                //if (bizGU.InserisciIspettore(ispettore))
                //{
                //    TextBoxNome.Text = string.Empty;
                //    TextBoxCognome.Text = string.Empty;
                //    CheckBoxAttivo.Checked = true;
                //    TextBoxLogin.Text = string.Empty;
                //    TextBoxPassword.Text = string.Empty;
                //    TextBoxPasswordRidigitata.Text = string.Empty;
                //    TextBoxCellulare.Text = string.Empty;

                //    LabelResult.Text = "Registrazione completata";

                //    Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                //}
                //else
                //{
                //    LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare.";
                //}
                #endregion
            }
            else
            {
                LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
            }
        }

    }
}