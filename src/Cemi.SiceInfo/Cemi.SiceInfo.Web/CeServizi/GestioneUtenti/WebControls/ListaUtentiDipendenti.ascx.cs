﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControl
{
    public partial class ListaUtentiDipendenti : System.Web.UI.UserControl
    {
        private readonly GestioneReport biz = new GestioneReport();
        private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

        private Int32 IdReport
        {
            get { return ViewState["idReport"] == null ? 0 : (int)ViewState["idReport"]; }
            set { ViewState["idReport"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RadGridDipendentiAssociati.Rebind();
                RadGridDipendentiAssociabili.Rebind();
            }
        }

        public void ImpostaReport(int idReport)
        {
            IdReport = idReport;
            RadGridDipendentiAssociati.MasterTableView.Rebind();
        }

        protected void RadGridUtenti_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtenti();
        }

        protected void RadGridUtentiAssociati_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtentiAssociati();
        }

        private void BindUtenti()
        {
            RadGridDipendentiAssociabili.DataSource = gu.GetUtentiDipendenti();
        }

        private void BindUtentiAssociati()
        {
            RadGridDipendentiAssociati.DataSource = biz.GetUtentiReportDipendenti(IdReport);
        }

        protected void RadGridUtentiAssociati_DeleteCommand(object source, GridCommandEventArgs e)
        {
            int idUtente =
                Convert.ToInt32(
                    (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUtente"].ToString());

            biz.DeleteUtenteReport(idUtente, IdReport);
        }

        protected void ButtonClearFiltersAssociabili_Click(object sender, ImageClickEventArgs e)
        {
            RadGridDipendentiAssociabili.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridDipendentiAssociabili.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridDipendentiAssociabili.MasterTableView.Rebind();
        }

        protected void ButtonClearFiltersAssociati_Click(object sender, ImageClickEventArgs e)
        {
            RadGridDipendentiAssociati.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridDipendentiAssociati.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridDipendentiAssociati.MasterTableView.Rebind();
        }

        protected void RadGridUtentiAssociabili_SelectedIndexChanging(object sender, EventArgs eventArgs)
        {
            int idUtente =
                (Int32)
                RadGridDipendentiAssociabili.MasterTableView.DataKeyValues[
                    Int32.Parse(RadGridDipendentiAssociabili.SelectedIndexes[0])]["IdUtente"];
            biz.AssociaUtenteReport(idUtente, IdReport);
            RadGridDipendentiAssociati.Rebind();
        }
    }
}