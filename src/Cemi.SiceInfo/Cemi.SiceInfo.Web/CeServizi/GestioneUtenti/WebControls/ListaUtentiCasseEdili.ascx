﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaUtentiCasseEdili.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControl.ListaUtentiCasseEdili" %>
<div>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
    <asp:Label ID="Label1" runat="server" Text="Utenti associati"></asp:Label> <br />
        <telerik:RadGrid Width="100%" ID="RadGridCasseEdiliAssociati" runat="server" AllowFilteringByColumn="true"
            EnableLinqExpressions="false" OnNeedDataSource="RadGridUtentiCasseEdiliAssociati_NeedDataSource"
            SortingSettings-SortToolTip="" AllowSorting="true" OnDeleteCommand="RadGridUtentiCasseEdiliAssociati_DeleteCommand">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="true" AllowFilteringByColumn="true"
                DataKeyNames="IdUtente" EditMode="InPlace">
                <Columns>
                    <telerik:GridBoundColumn DataField="Username" HeaderText="Username" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idUtente" HeaderText="IdUtente" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idCassaEdile" HeaderText="IdCassaEdile" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="descrizione" HeaderText="Descrizione" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridTemplateColumn>
                        <FilterTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="../../images/pallinoX.png"
                                AlternateText="Cancella filtro" ToolTip="Cancella filtro" OnClick="ButtonClearFiltersCasseEdiliAssociati_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../../images/pallinoX.png"
                        CommandName="Delete" Text="Elimina associazione">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
        &nbsp;<br />
        &nbsp;<br />
        <asp:Label ID="Label2" runat="server" Text="Utenti associabili"></asp:Label> <br />
        <telerik:RadGrid Width="100%" ID="RadGridCasseEdiliAssociabili" runat="server" AllowFilteringByColumn="true"
            EnableLinqExpressions="false" OnNeedDataSource="RadGridUtentiCasseEdili_NeedDataSource"
            OnSelectedIndexChanged="RadGridUtentiCasseEdiliAssociabili_SelectedIndexChanging" SortingSettings-SortToolTip=""
            AllowSorting="true">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="true" AllowFilteringByColumn="true"
                DataKeyNames="IdUtente" EditMode="InPlace">
                <Columns>
                    <telerik:GridBoundColumn DataField="Username" HeaderText="Username" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idUtente" HeaderText="IdUtente" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="idCassaEdile" HeaderText="IdCassaEdile" ShowFilterIcon="false"
                        CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="true" />
                    <telerik:GridBoundColumn DataField="descrizione" HeaderText="Descrizione" ShowFilterIcon="false"
                        CurrentFilterFunction="Contains" AutoPostBackOnFilter="true" />
                    <telerik:GridTemplateColumn>
                        <FilterTemplate>
                            <asp:ImageButton ID="btnShowAll" runat="server" ImageUrl="../../images/pallinoX.png"
                                AlternateText="Cancella filtro" ToolTip="Cancella filtro" OnClick="ButtonClearFiltersCasseEdiliAssociabili_Click"
                                Style="vertical-align: middle" />
                        </FilterTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="PushButton" Text="Associa" CommandName="Select">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
    </telerik:RadAjaxPanel>
</div>
