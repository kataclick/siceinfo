﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GestioneUtentiListaRuoli.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls.GestioneUtentiListaRuoli" %>
<asp:Panel ID="PanelUtente" runat="server">
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Profilo Utente selezionato"></asp:Label><br />
    <br />
    <asp:CheckBox ID="CheckBoxUtenteAttivo" runat="server" Text="Utente attivo" /><br />
    <br />
    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Ruoli assegnati all'utente"></asp:Label><br />
    <asp:CheckBoxList ID="CheckBoxListRuoli" runat="server">
    </asp:CheckBoxList>
    <br />
    <asp:Button ID="ButtonAggiornaUtente" runat="server" OnClick="ButtonAggiornaRuoliUtente_Click" Text="Aggiorna utente" />
</asp:Panel>