﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControl
{
    public partial class ListaUtentiCasseEdili : System.Web.UI.UserControl
    {
        private readonly GestioneReport biz = new GestioneReport();
        private readonly GestioneUtentiBiz gu = new GestioneUtentiBiz();

        private Int32 IdReport
        {
            get { return ViewState["idReport"] == null ? 0 : (int)ViewState["idReport"]; }
            set { ViewState["idReport"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RadGridCasseEdiliAssociati.Rebind();
                RadGridCasseEdiliAssociabili.Rebind();
            }
        }

        public void ImpostaReport(int idReport)
        {
            IdReport = idReport;
            RadGridCasseEdiliAssociati.MasterTableView.Rebind();
        }

        protected void RadGridUtentiCasseEdili_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtentiCasseEdili();
        }

        protected void RadGridUtentiCasseEdiliAssociati_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            BindUtentiCasseEdiliAssociati();
        }

        private void BindUtentiCasseEdili()
        {
            RadGridCasseEdiliAssociabili.DataSource = gu.GetUtentiCasseEdili();
        }

        private void BindUtentiCasseEdiliAssociati()
        {
            RadGridCasseEdiliAssociati.DataSource = biz.GetUtentiReportCasseEdili(IdReport);
        }

        protected void RadGridUtentiCasseEdiliAssociati_DeleteCommand(object source, GridCommandEventArgs e)
        {
            int idUtente =
                Convert.ToInt32(
                    (e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUtente"].ToString());

            biz.DeleteUtenteReport(idUtente, IdReport);
        }

        protected void ButtonClearFiltersCasseEdiliAssociabili_Click(object sender, ImageClickEventArgs e)
        {
            RadGridCasseEdiliAssociabili.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridCasseEdiliAssociabili.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridCasseEdiliAssociabili.MasterTableView.Rebind();
        }

        protected void ButtonClearFiltersCasseEdiliAssociati_Click(object sender, ImageClickEventArgs e)
        {
            RadGridCasseEdiliAssociati.MasterTableView.FilterExpression = string.Empty;

            foreach (GridColumn column in RadGridCasseEdiliAssociati.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    GridBoundColumn boundColumn = column as GridBoundColumn;
                    boundColumn.CurrentFilterValue = string.Empty;
                }
            }

            RadGridCasseEdiliAssociati.MasterTableView.Rebind();
        }

        protected void RadGridUtentiCasseEdiliAssociabili_SelectedIndexChanging(object sender, EventArgs eventArgs)
        {
            int idUtente =
                (Int32)
                RadGridCasseEdiliAssociabili.MasterTableView.DataKeyValues[
                    Int32.Parse(RadGridCasseEdiliAssociabili.SelectedIndexes[0])]["IdUtente"];
            biz.AssociaUtenteReport(idUtente, IdReport);
            RadGridCasseEdiliAssociati.Rebind();
        }
    }
}