﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GestioneRegistrazione.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls.GestioneRegistrazione" %>
<%@ Register Src="../../WebControls/Messaggio.ascx" TagName="Messaggio" TagPrefix="uc1" %>
<asp:Label ID="Label1" runat="server" Text="Effettua la registrazione come:"></asp:Label>
<asp:DropDownList ID="DropDownListTipoUtente" runat="server" OnSelectedIndexChanged="DropDownListTipoUtente_SelectedIndexChanged"
    AutoPostBack="True">
    <asp:ListItem></asp:ListItem>
    <asp:ListItem>Lavoratore</asp:ListItem>
    <asp:ListItem>Impresa</asp:ListItem>
    <asp:ListItem>Consulente</asp:ListItem>
</asp:DropDownList>
<br />
<br />
<asp:Button ID="ButtonRegistra" runat="server" OnClick="ButtonRegistra_Click" Text="Vai alla pagina di registrazione" />