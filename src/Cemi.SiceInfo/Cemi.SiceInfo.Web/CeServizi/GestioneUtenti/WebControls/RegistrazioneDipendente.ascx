﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrazioneDipendente.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.WebControls.RegistrazioneDipendente" %>
<%@ Register Src="../../WebControls/LiberatoriaPrivacy.ascx" TagName="LiberatoriaPrivacy" TagPrefix="uc1" %>
Tutti i campi sono obbligatori<br />
<br />
<table width="600pt">
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" Text="Nome:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                ControlToValidate="TextBoxNome"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label2" runat="server" Text="Cognome:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="TextBoxCognome"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelEmail" runat="server" Text="Email:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxEmail" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                ControlToValidate="TextBoxEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxEmail"
                ErrorMessage="Formato email non valido" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelUsername" runat="server" Text="Username:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxLogin" runat="server"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                ControlToValidate="TextBoxLogin"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPassword" runat="server" Text="Password:" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                ControlToValidate="TextBoxPassword"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelPasswordRidigitata" runat="server" Text="Ridigita password:"
                Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxPasswordRidigitata" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                ControlToValidate="TextBoxPasswordRidigitata"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxPassword"
                ControlToValidate="TextBoxPasswordRidigitata" ErrorMessage="Password diversa"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
                dal username e deve contenere almeno una lettera e un numero.<br />
                Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
                pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
                ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
                dal sistema.
        </td>
    </tr>
    <tr>
        <td colspan="3" height="5pt">
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <asp:Button ID="ButtonRegistraDipendente" runat="server" OnClick="ButtonRegistraDipendente_Click"
                Text="Registra dipendente" />
            <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" OnClick="ButtonIndietro_Click"
                CausesValidation="False" />
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <asp:Label ID="LabelResult" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>
<br />
