﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiRegistrazioneSindacalista.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiRegistrazioneSindacalista" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RegistrazioneSindacalista.ascx" TagName="RegistrazioneSindacalista"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Registrazione sindacalista"
        titolo="Gestione utenti" />
    <br />
    <uc3:RegistrazioneSindacalista ID="RegistrazioneSindacalista1" runat="server" />
</asp:Content>
