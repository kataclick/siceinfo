﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneUtentiCambiaPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["username"] != null)
                TextBoxUsername.Text = Server.UrlDecode(Request["username"]);

            string scaduta = null;
            if (Request["scaduta"] != null)
                scaduta = Request["scaduta"];

            if (scaduta == "true")
            {
                //rendere visibile messaggio password scaduta
                PanelPasswordScaduta.Visible = true;
            }

            MembershipUser user = Membership.GetUser();
            if (user != null)
            {
                TextBoxUsername.Text = user.UserName;
            }
        }

        protected void ButtonCambiaPassword_Click(object sender, EventArgs e)
        {
            LabelResponse.Text = string.Empty;

            if (!String.IsNullOrEmpty(TextBoxUsername.Text))
            {
                Utente utente = (Utente)Membership.GetUser(TextBoxUsername.Text);

                if (utente != null)
                {
                    if (TextBoxNuovaPassword.Text == TextBoxNuovaPasswordRedigit.Text)
                    {
                        try
                        {
                            utente.ChangePassword(TextBoxVecchiaPassword.Text, TextBoxNuovaPassword.Text);

                            //Utente utenteTmp = GestioneUtentiBiz.GetIdentitaUtente(utente.IdUtente);

                            //if (utenteTmp is Dipendente || utenteTmp is Ispettore || utenteTmp is Esattore)
                            //{
                            //Aggiorno scadenza a x mesi
                            int numeroMesi = Convert.ToInt32(ConfigurationManager.AppSettings["MesiValiditaPassword"]);
                            new UtentiManager().AggiornaScadenzaPassword(utente.IdUtente, numeroMesi);
                            //}

                            TextBoxNuovaPassword.Text = string.Empty;
                            TextBoxNuovaPasswordRedigit.Text = string.Empty;
                            TextBoxVecchiaPassword.Text = string.Empty;

                            LabelResponse.Text = "Password modificata correttamente.";
                            panelCambioPassword.Visible = false;

                            FormsAuthentication.RedirectFromLoginPage(TextBoxUsername.Text, false);
                        }
                        catch (MembershipPasswordException ex)
                        {
                            LabelResponse.Text = ex.Message;
                        }
                        catch (Exception exception)
                        {
                            LabelResponse.Text =
                                "Il cambiamento della password non è andato a buon fine. Digitare correttamente la vecchia password.";
                        }
                    }
                    else
                    {
                        LabelResponse.Text = "La nuova password non è stata digitata correttamente.";
                    }
                }
            }
        }
    }
}