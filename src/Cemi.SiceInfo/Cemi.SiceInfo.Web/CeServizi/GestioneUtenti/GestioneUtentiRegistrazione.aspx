﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiRegistrazione.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiRegistrazione" %>

<%@ Register Src="WebControls/GestioneRegistrazione.ascx" TagName="GestioneRegistrazione" TagPrefix="uc7" %>
<%@ Register Src="../WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti" TagPrefix="uc6" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1"  titolo="Gestione utenti" sottoTitolo="Registrazione" runat="server" />
    <br />
    <uc7:GestioneRegistrazione ID="GestioneRegistrazione1" runat="server" />
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc6:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
