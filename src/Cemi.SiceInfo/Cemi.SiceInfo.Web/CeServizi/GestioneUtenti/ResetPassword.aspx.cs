﻿using System;
using System.Configuration;
using System.Drawing;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti);

            if (Request["username"] != null)
                TextBoxLogin.Text = Server.UrlDecode(Request["username"]);
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            Page.Validate("cambioPassword");

            if (Page.IsValid)
            {
                UtentiManager utentiManager = new UtentiManager();
                int numeroMesi = Convert.ToInt32(ConfigurationManager.AppSettings["MesiValiditaPassword"]);
                bool ret = utentiManager.ImpostaPassword(TextBoxLogin.Text, TextBoxNuovaPassword.Text, numeroMesi);

                if (ret)
                {
                    PanelDati.Visible = false;
                    LabelResponse.Visible = true;
                    LabelResponse.ForeColor = Color.Black;
                    LabelResponse.Text =
                        "Reset della password effettuato con successo.";
                }
                else
                {
                    PanelDati.Visible = true;
                    LabelResponse.Visible = true;
                    LabelResponse.Text = "Impossibile effettuare l'operazione.";
                }
            }
        }
    }
}