﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiRegistrazioneImpresa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiRegistrazioneImpresa" %>
<%@ Register Src="WebControls/GestioneRegistrazione.ascx" TagName="GestioneRegistrazione"
    TagPrefix="uc2" %>

<%@ Register Src="WebControls/RegistrazioneImpresa.ascx" TagName="RegistrazioneImpresa"
    TagPrefix="uc1" %>
    
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1"  titolo="Gestione utenti" sottoTitolo="Registrazione impresa" runat="server" />
    <br />
    <uc2:GestioneRegistrazione ID="GestioneRegistrazione1" runat="server" Visible="false" />
    <br />
    <uc1:RegistrazioneImpresa ID="RegistrazioneImpresa1" runat="server" />
</asp:Content>
