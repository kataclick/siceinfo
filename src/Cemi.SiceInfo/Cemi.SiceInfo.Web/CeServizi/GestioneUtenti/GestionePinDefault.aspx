﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GestionePinDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestionePinDefault" MasterPageFile="~/CeServizi/MasterPage.master" %>

<%@ Register src="../WebControls/MenuGestionePin.ascx" tagname="MenuGestionePin" tagprefix="uc1" %>



<asp:Content ID="Content1" runat="server" contentplaceholderid="MainPage">
    <p>
    In questa area è possibile gestire i PIN di Lavoratori, Imprese e Consulenti</p>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc1:MenuGestionePin ID="MenuGestionePin1" runat="server" />
</asp:Content>
