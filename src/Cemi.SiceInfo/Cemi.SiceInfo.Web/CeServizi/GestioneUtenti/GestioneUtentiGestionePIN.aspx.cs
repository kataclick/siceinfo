﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Web;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneUtentiGestionePIN : System.Web.UI.Page
    {
        private readonly GestioneUtentiBiz _gu = new GestioneUtentiBiz();
        //private ImpreseCollection imprese;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestionePIN);

            if (!Page.IsPostBack)
            {
                CaricaCap();
            }
        }

        private void CaricaImprese()
        {
            ImpresaFilter filtro = GetFiltro();

            List<ImpresaConCredenziali> imprese = _gu.GetImprese(filtro);
            GridViewImprese.DataSource = imprese;
            GridViewImprese.DataBind();

            if (DropDownListTipoRicerca.SelectedValue == "SENZAPIN" ||
                DropDownListTipoRicerca.SelectedValue == "ISCRITTESENZAPIN" ||
                DropDownListTipoRicerca.SelectedValue == "FABBSENZAPIN")
            {
                ButtonGeneraPIN.Visible = true;
                ButtonGeneraTutti.Visible = true;

                LabelGenera.Visible = true;
            }
            else
            {
                ButtonGeneraPIN.Visible = false;
                ButtonGeneraTutti.Visible = false;

                LabelGenera.Visible = false;
            }

            if (imprese.Count == 0)
            {
                ButtonEsportaExcel.Visible = false;
                ButtonGeneraPIN.Visible = false;
                LinkButtonDeselezionaTutti.Visible = false;
                LinkButtonSelezionaTutti.Visible = false;
                ButtonGeneraTutti.Visible = false;
                ButtonFileTutti.Visible = false;

                LabelEsporta.Visible = false;
                LabelGenera.Visible = false;
            }
            else
            {
                ButtonEsportaExcel.Visible = true;
                LinkButtonSelezionaTutti.Visible = true;
                LinkButtonDeselezionaTutti.Visible = true;
                ButtonFileTutti.Visible = true;

                LabelEsporta.Visible = true;
            }
        }

        private ImpresaFilter GetFiltro()
        {
            DateTime dal;
            DateTime al;
            DateTime iscrittaDal;
            DateTime iscrittaAl;

            // Recupero i filtri di ricerca
            ImpresaFilter filtro = new ImpresaFilter();
            if (!string.IsNullOrEmpty(TextBoxCodImp.Text))
                filtro.Codice = Int32.Parse(TextBoxCodImp.Text);
            filtro.RagioneSociale = TextBoxRagioneSociale.Text.Trim().ToUpper();
            filtro.PartitaIVA = TextBoxPIVA.Text.Trim().ToUpper();
            filtro.CodiceFiscale = TextBoxCodiceFiscale.Text.Trim().ToUpper();
            filtro.Cap = DropDownListCAP.SelectedValue;
            if ((!string.IsNullOrEmpty(TextBoxDal.Text)) && (DateTime.TryParse(TextBoxDal.Text, out dal)))
                filtro.PinDal = dal;
            if ((!string.IsNullOrEmpty(TextBoxAl.Text)) && (DateTime.TryParse(TextBoxAl.Text, out al)))
                filtro.PinAl = new DateTime(al.Year, al.Month, al.Day, 23, 59, 59);
            if ((!string.IsNullOrEmpty(TextBoxIscrittaDal.Text)) &&
                (DateTime.TryParse(TextBoxIscrittaDal.Text, out iscrittaDal)))
                filtro.IscrittaDal = iscrittaDal;
            if ((!string.IsNullOrEmpty(TextBoxIscrittaAl.Text)) &&
                (DateTime.TryParse(TextBoxIscrittaAl.Text, out iscrittaAl)))
                filtro.IscrittaAl = iscrittaAl;
            if (DropDownListIscrittaWeb.SelectedValue == "ISCRITTE")
            {
                filtro.IscrittaSitoWeb = true;
            }
            else if (DropDownListIscrittaWeb.SelectedValue == "NONISCRITTE")
            {
                filtro.IscrittaSitoWeb = false;
            }
            else
            {
                filtro.IscrittaSitoWeb = null;
            }

            // Imposto i filtri booleani
            switch (DropDownListTipoRicerca.SelectedValue)
            {
                case "SENZAPIN":
                    filtro.ConPin = false;
                    break;
                case "CONPIN":
                    filtro.ConPin = true;
                    break;
                case "NUOVESENZAPIN":
                    filtro.ConPin = false;
                    filtro.NuoveIscritte = true;
                    break;
                case "ISCRITTESENZAPIN":
                    filtro.ConDenuncia = true;
                    filtro.ConPin = false;
                    break;
                case "FABBSENZAPIN":
                    filtro.ConFabbisogno = true;
                    filtro.ConPin = false;
                    break;
            }
            return filtro;
        }

        private void CaricaCap()
        {
            List<CAP> listaCap = _gu.GetImpreseCAP();

            DropDownListCAP.DataSource = listaCap;
            DropDownListCAP.DataTextField = "Testo";
            DropDownListCAP.DataValueField = "Cap";

            ListItem li = new ListItem(string.Empty, string.Empty);
            DropDownListCAP.Items.Add(li);
            DropDownListCAP.SelectedValue = string.Empty;

            DropDownListCAP.DataBind();
        }

        protected void ButtonFileTutti_Click(object sender, EventArgs e)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            //FiltraImprese();
            CaricaImprese();

            //Togliere
            //stringaCostruzione.Append("<HTML><BODY>");

            // Titoli
            stringaCostruzione.Append(
                "Ragione Sociale\tCodice Cassa Edile\tPIN\tCodice Fiscale\tPartita IVA\tIndirizzo Sede Legale\tCAP Sede Legale\tComune Sede Legale\tProvincia Sede Legale\tIndirizzo Sede Amministrazione\tCAP Sede Amministrazione\tComune Sede Amministrazione\tProvincia Sede Amministrazione\tPresso Sede Amministrazione\n");

            List<ImpresaConCredenziali> lista = (List<ImpresaConCredenziali>)GridViewImprese.DataSource;

            foreach (ImpresaConCredenziali imp in lista)
            {
                // Recupero i dati da memorizzare nel file

                string indirizzoLegale = string.Empty;
                string capLegale = string.Empty;
                string comuneLegale = string.Empty;
                string provinciaLegale = string.Empty;
                string indirizzoAmministrazione = string.Empty;
                string capAmministrazione = string.Empty;
                string comuneAmministrazione = string.Empty;
                string provinciaAmministrazione = string.Empty;
                string pressoAmministrazione = string.Empty;
                string pin = string.Empty;
                string codiceFiscale = string.Empty;
                string partitaIva = string.Empty;

                string ragioneSociale = imp.RagioneSociale;
                int id = imp.IdImpresa;

                if (imp.PIN != null)
                    pin = imp.PIN;
                if (imp.CodiceFiscale != null)
                    codiceFiscale = imp.CodiceFiscale;
                if (imp.PartitaIVA != null)
                    partitaIva = imp.PartitaIVA;
                if (imp.IndirizzoSedeLegale != null)
                    indirizzoLegale = imp.IndirizzoSedeLegale;
                if (imp.CapSedeLegale != null)
                    capLegale = imp.CapSedeLegale;
                if (imp.LocalitaSedeLegale != null)
                    comuneLegale = imp.LocalitaSedeLegale;
                if (imp.ProvinciaSedeLegale != null)
                    provinciaLegale = imp.ProvinciaSedeLegale;
                if (imp.IndirizzoSedeAmministrazione != null)
                    indirizzoAmministrazione = imp.IndirizzoSedeAmministrazione;
                if (imp.CapSedeAmministrazione != null)
                    capAmministrazione = imp.CapSedeAmministrazione;
                if (imp.LocalitaSedeAmministrazione != null)
                    comuneAmministrazione = imp.LocalitaSedeAmministrazione;
                if (imp.ProvinciaSedeAmministrazione != null)
                    provinciaAmministrazione = imp.ProvinciaSedeAmministrazione;
                if (imp.PressoSedeAmministrazione != null)
                    pressoAmministrazione = imp.PressoSedeAmministrazione;

                stringaCostruzione.Append(ragioneSociale + "\t" + id + "\t" + pin + "\t" +
                                          FormattaValori(partitaIva) + "\t" + FormattaValori(codiceFiscale) + "\t" +
                                          indirizzoLegale + "\t" + FormattaValori(capLegale) + "\t" + comuneLegale + "\t" +
                                          provinciaLegale + "\t" + indirizzoAmministrazione + "\t" +
                                          FormattaValori(capAmministrazione) + "\t" + comuneAmministrazione + "\t" +
                                          provinciaAmministrazione + "\t" + pressoAmministrazione + "\n");
            }
            //stringaCostruzione.Append("</BODY><HTML>");

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=PIN.xls");
            //Response.ContentType = "application/pdf";
            //Response.ContentType = "text/html";
            //Response.ContentType = "text/plain";
            //Response.ContentType = "application/octet-stream";
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        private static string FormattaValori(string testo)
        {
            return "=\"" + testo + "\"";
        }

        protected void ButtonGeneraTutti_Click(object sender, EventArgs e)
        {
            //FiltraImprese();
            CaricaImprese();

            List<ImpresaConCredenziali> lista = (List<ImpresaConCredenziali>)GridViewImprese.DataSource;

            foreach (ImpresaConCredenziali imp in lista)
            {
                _gu.GeneraPinImpresa(imp.IdImpresa);
            }

            if (GridViewImprese.Rows.Count > 0)
            {
                DropDownListTipoRicerca.SelectedValue = "CONPIN";

                CaricaImprese();
                //FiltraImprese();
            }
        }

        protected void GridViewImprese_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Genera")
            {
                int id;

                int index = Convert.ToInt32(e.CommandArgument);

                if (Int32.TryParse(GridViewImprese.Rows[index].Cells[1].Text, out id))
                {
                    _gu.GeneraPinImpresa(id);

                    CaricaImprese();
                    //FiltraImprese();
                }
            }
            else if (e.CommandName == "Impersona")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                ImpersonateManager.Impersonate(HttpUtility.HtmlDecode(GridViewImprese.Rows[index].Cells[2].Text));
            }
        }

        protected void ButtonFiltra_Click(object sender, EventArgs e)
        {
            GridViewImprese.PageIndex = 0;
            CaricaImprese();
        }

        protected void GridViewImprese_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //FiltraImprese();
            GridViewImprese.PageIndex = e.NewPageIndex;
            CaricaImprese();
            GridViewImprese.DataBind();
        }

        protected void ButtonEsportaExcel_Click(object sender, EventArgs e)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            // Titoli
            stringaCostruzione.Append(
                "Ragione Sociale\tCodice Cassa Edile\tPIN\tCodice Fiscale\tPartita IVA\tIndirizzo Sede Legale\tCAP Sede Legale\tComune Sede Legale\tProvincia Sede Legale\tIndirizzo Sede Amministrazione\tCAP Sede Amministrazione\tComune Sede Amministrazione\tProvincia Sede Amministrazione\tPresso Sede Amministrazione\n");

            for (int i = 0; i < GridViewImprese.Rows.Count; i++)
            {
                // Recupero la check box nella griglia
                CheckBox cbNelFile = (CheckBox)GridViewImprese.Rows[i].FindControl("CheckBoxNelFile");

                if (cbNelFile.Checked)
                {
                    // Recupero i dati da memorizzare nel file

                    string indirizzoLegale = string.Empty;
                    string capLegale = string.Empty;
                    string comuneLegale = string.Empty;
                    string provinciaLegale = string.Empty;
                    string indirizzoAmministrazione = string.Empty;
                    string capAmministrazione = string.Empty;
                    string comuneAmministrazione = string.Empty;
                    string provinciaAmministrazione = string.Empty;
                    string pressoAmministrazione = string.Empty;
                    string pin = string.Empty;
                    string codiceFiscale = string.Empty;
                    string partitaIva = string.Empty;

                    string ragioneSociale = GridViewImprese.Rows[i].Cells[0].Text;
                    string id = GridViewImprese.Rows[i].Cells[1].Text;

                    if (GridViewImprese.DataKeys[i].Values["PIN"] != null)
                        pin = GridViewImprese.DataKeys[i].Values["PIN"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["codiceFiscale"] != null)
                        codiceFiscale = GridViewImprese.DataKeys[i].Values["codiceFiscale"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["partitaIVA"] != null)
                        partitaIva = GridViewImprese.DataKeys[i].Values["partitaIVA"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["indirizzoSedeLegale"] != null)
                        indirizzoLegale = GridViewImprese.DataKeys[i].Values["indirizzoSedeLegale"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["capSedeLegale"] != null)
                        capLegale = GridViewImprese.DataKeys[i].Values["capSedeLegale"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["localitaSedeLegale"] != null)
                        comuneLegale = GridViewImprese.DataKeys[i].Values["localitaSedeLegale"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["provinciaSedeLegale"] != null)
                        provinciaLegale = GridViewImprese.DataKeys[i].Values["provinciaSedeLegale"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["indirizzoSedeAmministrazione"] != null)
                        indirizzoAmministrazione =
                            GridViewImprese.DataKeys[i].Values["indirizzoSedeAmministrazione"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["capSedeAmministrazione"] != null)
                        capAmministrazione = GridViewImprese.DataKeys[i].Values["capSedeAmministrazione"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["localitaSedeAmministrazione"] != null)
                        comuneAmministrazione = GridViewImprese.DataKeys[i].Values["localitaSedeAmministrazione"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["provinciaSedeAmministrazione"] != null)
                        provinciaAmministrazione =
                            GridViewImprese.DataKeys[i].Values["provinciaSedeAmministrazione"].ToString();
                    if (GridViewImprese.DataKeys[i].Values["pressoSedeAmministrazione"] != null)
                        pressoAmministrazione = GridViewImprese.DataKeys[i].Values["pressoSedeAmministrazione"].ToString();

                    stringaCostruzione.Append(ragioneSociale + "\t" + id + "\t" + pin + "\t" + partitaIva + "\t" +
                                              codiceFiscale + "\t" + indirizzoLegale + "\t" + capLegale + "\t" +
                                              comuneLegale + "\t" + provinciaLegale + "\t" + indirizzoAmministrazione + "\t" +
                                              capAmministrazione + "\t" + comuneAmministrazione + "\t" +
                                              provinciaAmministrazione + "\t" + pressoAmministrazione + "\n");
                }
            }

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=PIN.xls");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        protected void ButtonGeneraPIN_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridViewImprese.Rows.Count; i++)
            {
                int id;

                if (Int32.TryParse(GridViewImprese.Rows[i].Cells[1].Text, out id))
                {
                    _gu.GeneraPinImpresa(id);
                }
            }

            if (GridViewImprese.Rows.Count > 0)
            {
                DropDownListTipoRicerca.SelectedValue = "CONPIN";

                CaricaImprese();
                //FiltraImprese();
            }
        }

        protected void LinkButtonSelezionaTutti_Click(object sender, EventArgs e)
        {
            //FiltraImprese();
            CaricaImprese();

            for (int i = 0; i < GridViewImprese.Rows.Count; i++)
            {
                // Recupero la check box nella griglia
                CheckBox cbNelFile = (CheckBox)GridViewImprese.Rows[i].FindControl("CheckBoxNelFile");
                cbNelFile.Checked = true;
            }
        }

        protected void LinkButtonDeselezionaTutti_Click(object sender, EventArgs e)
        {
            //FiltraImprese();
            CaricaImprese();

            for (int i = 0; i < GridViewImprese.Rows.Count; i++)
            {
                // Recupero la check box nella griglia
                CheckBox cbNelFile = (CheckBox)GridViewImprese.Rows[i].FindControl("CheckBoxNelFile");
                cbNelFile.Checked = false;
            }
        }

        protected void GridViewImprese_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImpresaConCredenziali impresa = (ImpresaConCredenziali)e.Row.DataItem;
                Button bImpersona = (Button)e.Row.FindControl("ButtonImpersona");

                if (String.IsNullOrEmpty(impresa.Username))
                {
                    bImpersona.Enabled = false;
                }
            }
        }
    }
}