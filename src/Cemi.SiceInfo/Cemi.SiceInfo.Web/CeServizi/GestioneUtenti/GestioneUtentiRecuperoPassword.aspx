﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiRecuperoPassword.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiRecuperoPassword" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Gestione utenti" sottoTitolo="Recupero password"
        runat="server" />
    <br />
    <asp:Panel ID="PanelDati" runat="server">
        <table class="standardTable">
            <tr>
                <td style="width: 170px">
                    Username:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLogin" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxLogin"
                        ErrorMessage="Inserire una username" ValidationGroup="cambioPassword">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 170px">
                    PIN:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxPIN" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxPIN"
                        ErrorMessage="Inserire un PIN" ValidationGroup="cambioPassword">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 170px">
                    Nuova password:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNuovaPassword" runat="server" TextMode="Password" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxNuovaPassword"
                        ErrorMessage="Inserire una password" ValidationGroup="cambioPassword">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPassword" runat="server"
                        ControlToValidate="TextBoxNuovaPassword" ErrorMessage="La password deve essere diversa dallo username, deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri."
                        ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9-!#$%&'()*+,./:;<=>?@[\\\]_`{|}~]{8,15})$"
                        ValidationGroup="cambioPassword">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 170px;">
                    Ridigita nuova password:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRidigitaPassword" runat="server" TextMode="Password" Width="150px"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxRidigitaPassword"
                        ErrorMessage="Inserire la password una seconda volta per conferma" ValidationGroup="cambioPassword">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBoxNuovaPassword"
                        ControlToValidate="TextBoxRidigitaPassword" ErrorMessage="Le due password inserite non corrispondono."
                        ValidationGroup="cambioPassword">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <strong>N.B.</strong> La password deve essere lunga almeno 8 caratteri, deve differire
					dal username e deve contenere almeno una lettera e un numero.<br />
					Si ricorda che i caratteri scritti in MAIUSCOLO o minuscolo sono differenti; occorre
					pertanto prestare attenzione alla distinzione tra MAIUSCOLE e minuscole eventualmente
					ricomprese nella password scelta per non correre il rischio di non essere riconosciuti
					dal sistema.
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="ButtonConferma" runat="server" Text="Conferma" OnClick="ButtonConferma_Click" /><br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table class="standardTable">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelResponse" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <%--<tr>
            <td>
                <asp:Panel ID="PanelLogin" runat="server" Visible="False">
                    <table class="standardTable">
                        <tr>
                            <td>
                                Username:
                            </td>
                            <td>
                                <asp:Label ID="LabelLogin" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Password:
                            </td>
                            <td>
                                <asp:Label ID="LabelPassword" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>--%>
        <tr>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="cambioPassword"
                    CssClass="messaggiErrore" />
            </td>
        </tr>
    </table>
</asp:Content>


