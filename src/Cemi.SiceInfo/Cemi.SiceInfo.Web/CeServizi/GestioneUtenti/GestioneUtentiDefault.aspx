﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneUtentiDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneUtenti.GestioneUtentiDefault" %>

<%@ Register Src="../WebControls/MenuGestioneUtenti.ascx" TagName="MenuGestioneUtenti" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo2" titolo="Gestione utenti" sottoTitolo="Benvenuto nella sezione per la gestione utenti"
        runat="server" />
    <br />
    <p class="DefaultPage">
        Questa sezione da l'opportunità di gestire gli utenti del sistema SiceInfo e i
        ruoli definiti nel sistema stesso. Si ha quindi la possibilità di inserire nuovi
        utenti, abilitare il profilo di nuovi ospiti o disabilitare l'accesso ad utenti
        del sistema.
    </p>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" runat="Server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuGestioneUtenti ID="MenuGestioneUtenti1" runat="server" />
</asp:Content>
