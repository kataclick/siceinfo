﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Web;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.GestioneUtenti
{
    public partial class GestioneUtentiGestionePINConsulenti : System.Web.UI.Page
    {
        private readonly GestioneUtentiBiz _biz = new GestioneUtentiBiz();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti);
        }

        protected void ButtonFiltra_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaConsulenti();
            }
        }

        private List<ConsulenteConCredenziali> CaricaConsulenti()
        {
            ConsulenteFilter filter = GetFilter();

            List<ConsulenteConCredenziali> consulenti = _biz.GetConsulentiConCredenziali(filter);
            GridViewConsulenti.DataSource = consulenti;
            GridViewConsulenti.DataBind();

            if (consulenti != null && consulenti.Count > 0)
            {
                LinkButtonDeselezionaTutti.Visible = true;
                LinkButtonSelezionaTutti.Visible = true;
                ButtonEsportaExcel.Visible = true;
                ButtonFileTutti.Visible = true;
                ButtonGeneraPIN.Visible = true;
                ButtonGeneraTutti.Visible = true;
                LabelEsporta.Visible = true;
                LabelGenera.Visible = true;
            }
            else
            {
                LinkButtonDeselezionaTutti.Visible = false;
                LinkButtonSelezionaTutti.Visible = false;
                ButtonEsportaExcel.Visible = false;
                ButtonFileTutti.Visible = false;
                ButtonGeneraPIN.Visible = false;
                ButtonGeneraTutti.Visible = false;
                LabelEsporta.Visible = false;
                LabelGenera.Visible = false;
            }

            return consulenti;
        }

        private ConsulenteFilter GetFilter()
        {
            string ragioneSociale = null;
            string codiceFiscale = null;
            bool? conPin = null;
            DateTime? dal = null;
            DateTime? al = null;
            int? codice = null;

            if (!string.IsNullOrEmpty(TextBoxCodice.Text))
                codice = Int32.Parse(TextBoxCodice.Text);

            if (!string.IsNullOrEmpty(TextBoxRagioneSociale.Text.Trim()))
                ragioneSociale = TextBoxRagioneSociale.Text;

            if (!string.IsNullOrEmpty(TextBoxCodiceFiscale.Text))
                codiceFiscale = TextBoxCodiceFiscale.Text;

            if (DropDownListTipoRicerca.SelectedValue == "CONPIN")
            {
                conPin = true;

                if (!string.IsNullOrEmpty(TextBoxDal.Text.Trim()))
                    dal = DateTime.Parse(TextBoxDal.Text);
                if (!string.IsNullOrEmpty(TextBoxAl.Text.Trim()))
                    al = DateTime.Parse(TextBoxAl.Text);
            }
            if (DropDownListTipoRicerca.SelectedValue == "SENZAPIN")
                conPin = false;


            ConsulenteFilter filter = new ConsulenteFilter();
            filter.RagioneSociale = ragioneSociale;
            filter.CodiceFiscale = codiceFiscale;
            filter.ConPIN = conPin;
            filter.IdConsulente = codice;
            filter.PinGeneratoAl = al;
            filter.PinGeneratoDal = dal;
            if (DropDownListIscrittoWeb.SelectedValue == "ISCRITTI")
            {
                filter.IscrittoSitoWeb = true;
            }
            else if (DropDownListIscrittoWeb.SelectedValue == "NONISCRITTI")
            {
                filter.IscrittoSitoWeb = false;
            }
            else
            {
                filter.IscrittoSitoWeb = null;
            }

            return filter;
        }

        protected void GridViewConsulenti_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewConsulenti.PageIndex = e.NewPageIndex;
            CaricaConsulenti();
        }

        protected void ButtonEsportaExcel_Click(object sender, EventArgs e)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            // Titoli
            stringaCostruzione.Append(
                "IdConsulente\tRagione sociale\teMail\tPIN\tData generazione\tIndirizzo\tComune\tCap\tProvincia\n");

            for (int i = 0; i < GridViewConsulenti.Rows.Count; i++)
            {
                // Recupero la check box nella griglia
                CheckBox cbNelFile = (CheckBox)GridViewConsulenti.Rows[i].FindControl("CheckBoxNelFile");

                if (cbNelFile.Checked)
                {
                    // Recupero i dati da memorizzare nel file
                    int idConsulente = (int)GridViewConsulenti.DataKeys[i].Value;
                    Consulente consulente = _biz.GetConsulentiConCredenziali(new ConsulenteFilter { IdConsulente = idConsulente })[0];

                    stringaCostruzione.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\n", consulente.IdConsulente,
                                                    consulente.RagioneSociale, consulente.EMail, consulente.PIN,
                                                    consulente.DataGenerazionePIN, consulente.Indirizzo, consulente.Comune,
                                                    consulente.Cap, consulente.Provincia);
                }
            }

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=PIN.xls");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        protected void ButtonFileTutti_Click(object sender, EventArgs e)
        {
            //Faccio la richiesta passando il filtro
            List<ConsulenteConCredenziali> consulenti = CaricaConsulenti();

            GridView gv = new GridView();
            gv.AutoGenerateColumns = false;

            BoundField bc0 = new BoundField();
            bc0.HeaderText = "Id consulente";
            bc0.DataField = "IdConsulente";
            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Ragione sociale";
            bc1.DataField = "RagioneSociale";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "eMail";
            bc2.DataField = "Email";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "PIN";
            bc3.DataField = "Pin";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Data generazione";
            bc4.DataField = "dataGenerazionePIN";

            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Indirizzo";
            bc5.DataField = "Indirizzo";
            BoundField bc6 = new BoundField();
            bc6.HeaderText = "Comune";
            bc6.DataField = "Comune";
            BoundField bc7 = new BoundField();
            bc7.HeaderText = "CAP";
            bc7.DataField = "Cap";
            BoundField bc8 = new BoundField();
            bc8.HeaderText = "Provincia";
            bc8.DataField = "Provincia";

            gv.Columns.Add(bc0);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            gv.Columns.Add(bc7);
            gv.Columns.Add(bc8);

            gv.DataSource = consulenti;
            gv.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=PIN.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void ButtonGeneraPIN_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GridViewConsulenti.Rows.Count; i++)
            {
                int id = (int)GridViewConsulenti.DataKeys[i].Value;

                _biz.GeneraPinConsulente(id);
            }

            if (GridViewConsulenti.Rows.Count > 0)
            {
                DropDownListTipoRicerca.SelectedValue = "CONPIN";

                CaricaConsulenti();
            }
        }

        protected void ButtonGeneraTutti_Click(object sender, EventArgs e)
        {
            CaricaConsulenti();

            ConsulentiCollection lista = (ConsulentiCollection)GridViewConsulenti.DataSource;

            foreach (Consulente cons in lista)
            {
                _biz.GeneraPinConsulente(cons.IdConsulente);
            }

            if (GridViewConsulenti.Rows.Count > 0)
            {
                DropDownListTipoRicerca.SelectedValue = "CONPIN";

                CaricaConsulenti();
            }
        }

        protected void LinkButtonDeselezionaTutti_Click(object sender, EventArgs e)
        {
            CaricaConsulenti();

            for (int i = 0; i < GridViewConsulenti.Rows.Count; i++)
            {
                // Recupero la check box nella griglia
                CheckBox cbNelFile = (CheckBox)GridViewConsulenti.Rows[i].FindControl("CheckBoxNelFile");
                cbNelFile.Checked = false;
            }
        }

        protected void LinkButtonSelezionaTutti_Click(object sender, EventArgs e)
        {
            CaricaConsulenti();

            for (int i = 0; i < GridViewConsulenti.Rows.Count; i++)
            {
                // Recupero la check box nella griglia
                CheckBox cbNelFile = (CheckBox)GridViewConsulenti.Rows[i].FindControl("CheckBoxNelFile");
                cbNelFile.Checked = true;
            }
        }

        protected void GridViewConsulenti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Genera")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int idConsulente = (int)GridViewConsulenti.DataKeys[index].Value;
                _biz.GeneraPinConsulente(idConsulente);
                CaricaConsulenti();
            }
            else if (e.CommandName == "Impersona")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                ImpersonateManager.Impersonate(HttpUtility.HtmlDecode(GridViewConsulenti.Rows[index].Cells[3].Text));
            }
        }

        protected void GridViewConsulenti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ConsulenteConCredenziali consulente = (ConsulenteConCredenziali)e.Row.DataItem;
                Button bImpersona = (Button)e.Row.FindControl("ButtonImpersona");

                if (String.IsNullOrEmpty(consulente.Username))
                {
                    bImpersona.Enabled = false;
                }
            }
        }
    }
}