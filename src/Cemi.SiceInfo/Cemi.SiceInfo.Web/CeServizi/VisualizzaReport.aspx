﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="VisualizzaReport.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.VisualizzaReport" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainPage">
    <rsweb:ReportViewer ID="ReportViewerContenitore" runat="server" OnInit="ReportViewer_Init"
        ProcessingMode="Remote" Width="100%" Height="490px" DocumentMapCollapsed="True"
        PromptAreaCollapsed="True">
    </rsweb:ReportViewer>
    <asp:Panel ID="PanelListaReport" runat="server" Visible="false">
        <asp:Label ID="Label1" runat="server" Text="Report consultabili: " Font-Bold="true" />
        <br />
        <br />
        <telerik:RadGrid Width="100%" ID="RadGridReport" runat="server" AllowFilteringByColumn="false"
            EnableLinqExpressions="false" OnItemCommand="RadGridReport_OnItemCommand" SortingSettings-SortToolTip=""
            AllowSorting="true" PageSize="15">
            <GroupingSettings CaseSensitive="false" />
            <MasterTableView AllowPaging="true" AllowMultiColumnSorting="false" AllowFilteringByColumn="false"
                DataKeyNames="Nome" EditMode="InPlace">
                <Columns>
                    <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" AutoPostBackOnFilter="true" />
                    <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Link" Text="Visualizza report">
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings EnableRowHoverStyle="true" />
        </telerik:RadGrid>
    </asp:Panel>
</asp:Content>
