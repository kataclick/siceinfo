﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string defaultPage = "default.aspx";
            string rawUrl = Request.RawUrl; //get current url

            //if current url doesn't contains default page name then add
            //default page name, and append query string as it is, if any
            if (rawUrl.ToLower().IndexOf(defaultPage, StringComparison.Ordinal) < 0)
            {
                string newUrl;
                if (rawUrl.IndexOf("?", StringComparison.Ordinal) >= 0)
                {
                    // URL contains query string
                    string[] urlParts = rawUrl.Split("?".ToCharArray(), 2);

                    newUrl = urlParts[0] + defaultPage + "?" + urlParts[1];
                }
                else
                {
                    newUrl = (rawUrl.EndsWith("/")) ? rawUrl + defaultPage : rawUrl + "/" + defaultPage;
                }

                Response.Redirect(newUrl);
            }

            Utente utente = GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            if (utente is Impresa)
            {
                PanelAutenticato.Visible = false;
                PanelImpresa.Visible = true;
                PanelLavoratore.Visible = false;
                PanelPubblico.Visible = false;
            }
            else if (utente is Consulente)
            {
                PanelAutenticato.Visible = false;
                PanelImpresa.Visible = true;
                PanelLavoratore.Visible = false;
                PanelPubblico.Visible = false;
            }
            else if (utente is Lavoratore)
            {
                PanelAutenticato.Visible = false;
                PanelImpresa.Visible = false;
                PanelLavoratore.Visible = true;
                PanelPubblico.Visible = false;
            }
            else if (utente != null)
            {
                PanelAutenticato.Visible = true;
                PanelImpresa.Visible = false;
                PanelLavoratore.Visible = false;
                PanelPubblico.Visible = false;
            }
        }
    }
}