﻿<%@ Page Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="NotFound.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotFound" %>

<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Errore" sottoTitolo="Informazioni errore" runat="server" />
    <br />
    Risorsa non trovata. Verificare l'URL richiesto.
</asp:Content>