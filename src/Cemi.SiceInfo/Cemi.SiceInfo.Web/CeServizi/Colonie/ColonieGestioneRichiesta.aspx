﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieGestioneRichiesta.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieGestioneRichiesta" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuColonie.ascx" tagname="MenuColonie" tagprefix="uc3" %>
<%@ Register src="WebControls/ColonieRiassuntoRichiesta.ascx" tagname="RiassuntoRichiesta" tagprefix="uc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc3:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Villaggi Vacanze" sottoTitolo="Gestione Richiesta" />
    <br />
    <uc2:RiassuntoRichiesta ID="RiassuntoRichiesta1" runat="server" />
</asp:Content>
