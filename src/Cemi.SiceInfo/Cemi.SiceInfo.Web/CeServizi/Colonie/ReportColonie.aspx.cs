﻿using System;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ReportColonie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["tipoReport"] != null)
            {
                GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneMatrice);
                // CHECK LIST IMBARCO
                if (Request.QueryString["tipoReport"] == "CheckList")
                {
                    int idTurno = Int32.Parse(Request.QueryString["idTurno"]);

                    ReportViewerColonie.ServerReport.ReportServerUrl =
                        new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

                    ReportViewerColonie.ServerReport.ReportPath = "/ReportColonie/ReportCheckListImbarco";
                    ReportParameter[] listaParam = new ReportParameter[1];
                    listaParam[0] = new ReportParameter("idTurno", idTurno.ToString());

                    ReportViewerColonie.ServerReport.SetParameters(listaParam);
                }
            }
        }
    }
}