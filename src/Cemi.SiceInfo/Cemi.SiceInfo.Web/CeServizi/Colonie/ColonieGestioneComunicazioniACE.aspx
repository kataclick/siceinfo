﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieGestioneComunicazioniACE.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieGestioneComunicazioniACE" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione comunicazioni ACE"
        titolo="Colonie" />
    &nbsp;<br />
    <asp:Panel ID="PanelMail" runat="server" Visible="true" Width="100%">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Vacanza attiva"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Anno
                </td>
                <td>
                    <asp:Label ID="LabelAnno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo vacanza
                </td>
                <td>
                    <asp:Label ID="LabelTipoVacanza" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    Apertura fase prenotazioni
                </td>
                <td>
                    <asp:Button ID="ButtonAperturaPrenotazioni" Text="Invia" runat="server" OnClick="ButtonAperturaPrenotazioni_Click"
                        Width="150px" />
                </td>
            </tr>
            <tr>
                <td>
                    Conferma prenotazioni e Apertura fase inserimento domande
                </td>
                <td>
                    <asp:Button ID="ButtonAperturaDomande" Text="Invia" runat="server" OnClick="ButtonAperturaDomande_Click"
                        Width="150px" />
                </td>
            </tr>
            <tr>
                <td>
                    Chiusura fase inserimento domande
                </td>
                <td>
                    <asp:Button ID="ButtonChiusuraDomande" Text="Invia" runat="server" OnClick="ButtonChiusuraDomande_Click"
                        Width="150px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red" Text="Mail inviate"
                        Visible="false"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="PanelVacanzaNonAttiva" runat="server" Visible="false" Width="100%">
        <asp:Label ID="LabelVacanzaNonAttiva" runat="server" Text="Non esistono vacanze attive."></asp:Label>
    </asp:Panel>
</asp:Content>
