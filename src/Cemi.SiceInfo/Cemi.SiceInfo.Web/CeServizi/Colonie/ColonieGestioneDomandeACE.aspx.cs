﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieGestioneDomandeACE : System.Web.UI.Page
    {
        #region Attributi

        private readonly ColonieBusiness biz = new ColonieBusiness();
        private MatriceTurni matrice;
        private Vacanza vacanzaAttiva;

        #endregion

        #region Eventi pagina

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneDomandeACE);

            #endregion

            vacanzaAttiva = biz.GetVacanzaAttiva();

            ColonieRicercaDomandeACEPerConferma1.OnDomandaSelected += ColonieRicercaDomandeACEPerConferma1_OnDomandaSelected;

            // Abilito o meno la ricerca in base alla presenza di una vacanza attiva
            if (vacanzaAttiva != null)
            {
                AbilitaRicerca(true);

                if (!Page.IsPostBack)
                    CaricaCasseEdili();
                if (!string.IsNullOrEmpty(DropDownListCassaEdile.SelectedValue))
                    CaricaSituazione(DropDownListCassaEdile.SelectedValue);

                // Imposto il controllo per la ricerca con i parametri obbligatori.
                ColonieRicercaDomandeACEPerConferma1.IdVacanza = vacanzaAttiva.IdVacanza.Value;
                ColonieRicercaDomandeACEPerConferma1.IdCassaEdile = DropDownListCassaEdile.SelectedValue;

                if (!Page.IsPostBack)
                {
                    TurnoCollection turni = biz.GetTurni(null, vacanzaAttiva.IdVacanza.Value, null, null, null);
                    ColonieRicercaDomandeACEPerConferma1.CaricaTurni(turni);
                }
            }
            else
            {
                AbilitaRicerca(false);
            }
        }

        #endregion

        #region Funzioni di caricamento

        private void CaricaCasseEdili()
        {
            biz.CaricaCasseEdili(DropDownListCassaEdile, vacanzaAttiva.IdVacanza.Value);
        }

        private void CaricaSituazione(string idCassaEdile)
        {
            SituazioneACECollection situazione = biz.GetSituazioneDomande(idCassaEdile, vacanzaAttiva.IdVacanza.Value);
            GridViewSituazione.DataSource = situazione;
            GridViewSituazione.DataBind();
        }

        private void Reset()
        {
            ColonieRicercaDomandeACEPerConferma1.Reset();
            ColonieDatiDomanda1.Visible = false;
        }

        #endregion

        #region Eventi dei controlli

        protected void DropDownListCassaEdile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DropDownListCassaEdile.SelectedValue))
            {
                CaricaSituazione(DropDownListCassaEdile.SelectedValue);
                Reset();
            }
        }

        protected void GridViewSituazione_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                SituazioneACE situazione = (SituazioneACE)e.Row.DataItem;
                Label lTurno = (Label)e.Row.FindControl("LabelTurno");
                Label lPostiTotali = (Label)e.Row.FindControl("LabelPostiTotali");
                Label lPostiDisponibili = (Label)e.Row.FindControl("LabelPostiDisponibili");
                Label lPostiOccupati = (Label)e.Row.FindControl("LabelPostiOccupati");

                if (situazione.DomandeInCarico > 0)
                {
                    if (situazione.PostiAccettatiRichiesti > situazione.PostiPrenotati)
                        e.Row.ForeColor = Color.Red;
                }

                lTurno.Text = situazione.Turno;
                lPostiTotali.Text = situazione.PostiDisponibili.ToString();

                if (matrice != null)
                {
                    foreach (RigaMatriceTurni riga in matrice)
                    {
                        foreach (Turno tur in riga.Turni)
                        {
                            if (tur.IdTurno.Value == situazione.IdTurno)
                            {
                                lPostiOccupati.Text = tur.PostiOccupati.ToString();
                                lPostiDisponibili.Text = ((situazione.PostiDisponibili - tur.PostiOccupati)).ToString();
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(lPostiOccupati.Text))
                {
                    lPostiOccupati.Text = "0";
                    lPostiDisponibili.Text = situazione.PostiDisponibili.ToString();
                }
            }
        }

        protected void GridViewSituazione_DataBinding(object sender, EventArgs e)
        {
            matrice = biz.GetMatriceRichiesteDisponibilita(vacanzaAttiva.Anno, vacanzaAttiva.TipoVacanza.IdTipoVacanza);
        }

        private void ColonieRicercaDomandeACEPerConferma1_OnDomandaSelected(DomandaACE domanda)
        {
            ColonieDatiDomanda1.CaricaDomanda(domanda, true, null);
            ColonieDatiDomanda1.Visible = true;
        }

        #endregion

        /// <summary>
        /// Abilita la ricerca o mostra un messaggio.
        /// </summary>
        /// <param name="abilita"></param>
        private void AbilitaRicerca(bool abilita)
        {
            PanelRicerca.Visible = abilita;
            PanelNessunaVacanzaAttiva.Visible = !abilita;
        }
    }
}