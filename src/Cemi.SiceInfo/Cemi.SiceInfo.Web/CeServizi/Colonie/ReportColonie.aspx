﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportColonie.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ReportColonie" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Check List partenza"
        titolo="Colonie" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:reportviewer id="ReportViewerColonie" runat="server"
        processingmode="Remote" width="100%"></rsweb:reportviewer>
        </td></tr></table>
    <br />
</asp:Content>

