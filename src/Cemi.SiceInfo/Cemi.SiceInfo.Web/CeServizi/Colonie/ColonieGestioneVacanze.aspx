﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieGestioneVacanze.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieGestioneVacanze" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione vacanze"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Anno
            </td>
            <td colspan="2">
                <asp:TextBox ID="TextBoxAnnoRicerca" runat="server" MaxLength="4"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ValidationExpression="^\d{4,4}$" ValidationGroup="ricerca" ControlToValidate="TextBoxAnnoRicerca"></asp:RegularExpressionValidator>
                <asp:Button ID="ButtonVisualizza" runat="server" Text="Visualizza" ValidationGroup="ricerca"
                    OnClick="ButtonVisualizza_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Vacanze"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="GridViewVacanze" runat="server" AutoGenerateColumns="False" Width="100%"
                    OnSelectedIndexChanging="GridViewVacanze_SelectedIndexChanging" DataKeyNames="IdVacanza"
                    OnRowEditing="GridViewVacanze_RowEditing" OnRowDeleting="GridViewVacanze_RowDeleting">
                    <Columns>
                        <asp:BoundField HeaderText="Anno" DataField="Anno" />
                        <asp:BoundField HeaderText="Tipo Vacanza" DataField="StringaTipoVacanza" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Visualizza"
                            ShowSelectButton="True">
                            <ItemStyle Width="50px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Turni"
                            ShowEditButton="True">
                            <ItemStyle Width="50px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Abilitazioni"
                            ShowDeleteButton="True">
                            <ItemStyle Width="50px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna vacanza presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonNuova" runat="server" Text="Nuova vacanza" OnClick="ButtonNuova_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelDettagli" runat="server" Width="100%" Visible="False">
        <table class="standardTable">
            <tr>
                <td colspan="3">
                    <asp:Label ID="Label2" runat="server" Text="Dettagli" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Anno
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px" MaxLength="4"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxAnno"
                        ErrorMessage="Formato anno errato" ValidationExpression="^\d{4,4}$" ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxAnno"
                        ErrorMessage="Digitare un anno" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo vacanza
                </td>
                <td style="width: 390px">
                    <asp:DropDownList ID="DropDownListTipoVacanza" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListTipoVacanza"
                        ErrorMessage="Selezionare un tipo vacanza" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Costo disabile
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxCostoDisabile" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                    €
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxCostoDisabile"
                        ErrorMessage="Formato errato" ValidationExpression="^\d+(\,\d\d)?$" ValidationGroup="dettagli"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxCostoDisabile"
                            ErrorMessage="Digitare un costo disabile" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Costo penalità
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxCostoPenalita" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                    €
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBoxCostoPenalita"
                        ErrorMessage="Formato errato" ValidationExpression="^\d+(\,\d\d)?$" ValidationGroup="dettagli"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBoxCostoPenalita"
                            ErrorMessage="Digitare un costo penalità" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Limite penalità (gg)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxLimitePenalita" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBoxLimitePenalita"
                        ErrorMessage="Formato errato" ValidationExpression="^\d{1,2}$" ValidationGroup="dettagli"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator6" runat="server" ControlToValidate="TextBoxLimitePenalita"
                            ErrorMessage="Digitare un limite penalità" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Anno limite inferiore bimbi
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxLimiteInferiore" runat="server" Width="300px" MaxLength="4"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                        ControlToValidate="TextBoxLimiteInferiore" ErrorMessage="Formato errato" ValidationExpression="^\d{4}$"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBoxLimiteInferiore"
                            ErrorMessage="Digitare un anno" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Anno limite superiore bimbi
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxLimiteSuperiore" runat="server" Width="300px" MaxLength="4"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                        ControlToValidate="TextBoxLimiteSuperiore" ErrorMessage="Formato errato" ValidationExpression="^\d{4}$"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                            ID="RequiredFieldValidator8" runat="server" ControlToValidate="TextBoxLimiteSuperiore"
                            ErrorMessage="Digitare un anno" ValidationGroup="dettagli"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Attiva
                </td>
                <td style="width: 390px">
                    <asp:CheckBox ID="CheckBoxAttiva" runat="server" />
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    Data inizio prenotazioni (gg/mm/aaaa)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxDataInizioPrenotazioni" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TextBoxDataInizioPrenotazioni"
                        ErrorMessage="Formato errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Data fine prenotazioni (gg/mm/aaaa)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxDataFinePrenotazioni" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBoxDataFinePrenotazioni"
                        ErrorMessage="Formato errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    Data inizio inserimento domande ACE (gg/mm/aaaa)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxDataInizioDomande" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBoxDataInizioDomande"
                        ErrorMessage="Formato errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Data fine inserimento domande ACE (gg/mm/aaaa)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxDataFineDomande" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                        ControlToValidate="TextBoxDataFineDomande" ErrorMessage="Formato errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    Data inizio inserimento richieste personale (gg/mm/aaaa)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxDataInizioRichieste" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizioRichieste" runat="server" ControlToValidate="TextBoxDataInizioDomande"
                        ErrorMessage="Formato errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Data fine inserimento richieste personale (gg/mm/aaaa)
                </td>
                <td style="width: 390px">
                    <asp:TextBox ID="TextBoxDataFineRichieste" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFineRichieste" runat="server"
                        ControlToValidate="TextBoxDataFineRichieste" ErrorMessage="Formato errato" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                        ValidationGroup="dettagli"></asp:RegularExpressionValidator>
                    <asp:CompareValidator ID="RegularExpressionValidatorDataInizioFine" runat="server"
                        ControlToValidate="TextBoxDataInizioRichieste" ErrorMessage="La data di fine richieste deve essere successiva a quella di inizio" 
                        ValidationGroup="dettagli" Type="Date" Operator="LessThanEqual" ControlToCompare="TextBoxDataFineRichieste"></asp:CompareValidator>
                    <asp:CustomValidator ID="CustomValidatorDataInizioFine" runat="server"
                        ErrorMessage="Non può essere valorizzata solo una tra le date di inizio richieste e quella di fine" 
                        ValidationGroup="dettagli" 
                        onservervalidate="CustomValidatorDataInizioFine_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button ID="ButtonOperazione" runat="server" Text="Operazione" OnClick="ButtonOperazione_Click"
                        ValidationGroup="dettagli" />
                    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

