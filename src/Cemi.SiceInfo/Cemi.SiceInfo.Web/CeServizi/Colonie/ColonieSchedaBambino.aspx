﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieSchedaBambino.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieSchedaBambino" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Scheda partecipante"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td colspan="2">
                <table class="standardTable">
                    <tr>
                        <td style="width: 97px">
                            <asp:Label ID="Label5" runat="server" Text="Soggiorno" Font-Bold="True"></asp:Label>
                        </td>
                        <td style="width: 143px">
                            <asp:Label ID="LabelTipoVacanza" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LabelAnno" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label4" runat="server" Text="Vacanza" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Località
                        </td>
                        <td>
                            <asp:Label ID="LabelLocalita" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Turno
                        </td>
                        <td>
                            <asp:Label ID="LabelTurno" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text="Corredo n°" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                <asp:Label ID="LabelNumeroCorredo" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="350">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label1" runat="server" Text="Partecipante" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Cognome
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelCognomeP" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Nome
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelNomeP" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Indirizzo
                        </td>
                        <td>
                            <asp:Label ID="LabelIndirizzoP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            CAP
                        </td>
                        <td>
                            <asp:Label ID="LabelCAPP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Località
                        </td>
                        <td>
                            <asp:Label ID="LabelComuneP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Provincia
                        </td>
                        <td>
                            <asp:Label ID="LabelProvinciaP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Sesso
                        </td>
                        <td>
                            <asp:Label ID="LabelSessoP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Data di nascita
                        </td>
                        <td>
                            <asp:Label ID="LabelDataNascitaP" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Tessera sanitaria n°
                        </td>
                        <td>
                            <asp:Label ID="LabelTesseraSanitaria" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table width="350">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label2" runat="server" Text="Genitore" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Cod. lavoratore
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelIdLavoratore" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Cognome
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelCognomeL" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Nome
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="LabelNomeL" runat="server"></asp:Label>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Indirizzo
                        </td>
                        <td>
                            <asp:Label ID="LabelIndirizzoL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            CAP
                        </td>
                        <td>
                            <asp:Label ID="LabelCAPL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Località
                        </td>
                        <td>
                            <asp:Label ID="LabelComuneL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Provincia
                        </td>
                        <td>
                            <asp:Label ID="LabelProvinciaL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Telefono abitazione
                        </td>
                        <td>
                            <asp:Label ID="LabelTelefonoL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Telefono cellulare
                        </td>
                        <td>
                            <asp:Label ID="LabelCellulareL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            C.E./EDILCASSA
                        </td>
                        <td>
                            <asp:Label ID="LabelCassa" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="350">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Dati sanitari"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Portatore handicap
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxHandicap" runat="server" Enabled="False" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Intolleranze alimentari
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxIntolleranze" runat="server" Enabled="False" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Dieta
                        </td>
                        <td>
                            <asp:Label ID="LabelDieta" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Allergie
                        </td>
                        <td>
                            <asp:Label ID="LabelAllergie" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Protesi e ausili
                        </td>
                        <td>
                            <asp:Label ID="LabelProtesi" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Terapie in corso
                        </td>
                        <td>
                            <asp:Label ID="LabelTerapie" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table width="350">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Parenti partecipanti"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Nominativo
                        </td>
                        <td>
                            <asp:Label ID="LabelParenteNominativo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Parentela
                        </td>
                        <td>
                            <asp:Label ID="LabelParentela" runat="server">Fratello / sorella</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 150px">
                            Corredo n°
                        </td>
                        <td>
                            <asp:Label ID="LabelParenteCorredo" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="350">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="Accompagnatore"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 110px">
                            Cognome
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreCognome" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 110px">
                            Nome
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreNome" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 110px">
                            Sesso
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreSesso" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 110px">
                            Data di nascita
                        </td>
                        <td>
                            <asp:Label ID="LabelAccompagnatoreDataNascita" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

