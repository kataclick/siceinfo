﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Villaggi vacanza"
        titolo="Villaggi vacanza" />
    <br />
    Benvenuto nella sezione Colonie
    <p class="DefaultPage">
        Tra le prestazioni erogate da Cassa Edile di Milano, Lodi, Monza e Brianza rientra il soggiorno estivo
        gratuito per i figli dei lavoratori aventi diritto di età compresa tra i 5 anni e
        i 15 anni.<br />
        I turni, della durata di 14 giorni ciascuno, sono stabiliti dalla Cassa Edile ed iniziano alla fine di
		giugno e terminano alla fine di agosto.<br /><br />
        <b>La permanenza ed il viaggio sono completamente gratuiti.</b>
    </p>
    <p class="DefaultPage">
        Utilizzando il menù verticale sulla sinistra del video è possibile indicare il numero
        preventivato di partecipanti tramite l'apposita funzione "Gestione prenotazioni", inserire
        le domande del numero di partecipanti confermato e gestire le stesse (Gestione domande).
    </p>
</asp:Content>
