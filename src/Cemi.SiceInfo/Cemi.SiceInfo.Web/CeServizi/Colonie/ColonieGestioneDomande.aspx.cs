﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieGestioneDomande : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneDomande);

            if (!Page.IsPostBack)
            {
                TextBoxAnno.Text = DateTime.Now.Year.ToString();
                CaricaTipiVacanza();
                CaricaMotiviMancatoImbarco();
                CaricaCasseEdili();
            }
        }

        private void CaricaCasseEdili()
        {
            DropDownListCassaEdile.Items.Clear();
            DropDownListCassaEdile.Items.Add(new ListItem("Tutti", "TUTTI"));
            DropDownListCassaEdile.Items.Add(new ListItem("CE Milano", "MILANO"));

            DropDownListCassaEdile.DataSource = biz.GetCasseEdili();
            DropDownListCassaEdile.DataTextField = "IdCassaEdile";
            DropDownListCassaEdile.DataValueField = "IdCassaEdile";
            DropDownListCassaEdile.DataBind();
        }

        private void CaricaMotiviMancatoImbarco()
        {
            DropDownListMancatoImbarco.Items.Clear();
            DropDownListMancatoImbarco.Items.Add(new ListItem(string.Empty, string.Empty));

            DropDownListMancatoImbarco.DataSource = biz.GetMotiviMancatoImbarco();
            DropDownListMancatoImbarco.DataBind();
        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipiVacanza.DataSource = tipiVacanza;
            DropDownListTipiVacanza.DataTextField = "Descrizione";
            DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipiVacanza.DataBind();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            CaricaTurni();
        }

        private void CaricaTurni()
        {
            if (ControlloCampiServer())
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);

                ViewState["anno"] = anno;
                ViewState["idTipoVacanza"] = idTipoVacanza;

                TurnoCollection turni = biz.GetTurni(null, null, null, anno, idTipoVacanza);
                DropDownListTurno.DataSource = turni;
                DropDownListTurno.DataTextField = "DescrizioneTurno";
                DropDownListTurno.DataValueField = "IdCombo";
                DropDownListTurno.DataBind();

                LabelNonAssegnate.Visible = true;
                DomandaCollection domandeNonAssegnate = biz.GetDomandeLiquidateNonAssegnate(idTipoVacanza, anno, null, null);
                GridViewDomandeNonAssegnate.DataSource = domandeNonAssegnate;
                GridViewDomandeNonAssegnate.DataBind();

                if (DropDownListTurno.SelectedIndex != -1)
                {
                    CaricaDomande(Int32.Parse(DropDownListTurno.SelectedValue), null, null);
                    ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                    ButtonAutobus.Enabled = true;
                    PanelFiltro.Visible = true;
                }
                else
                {
                    ButtonAutobus.Enabled = false;
                    PanelFiltro.Visible = false;
                }
            }
        }

        private void CaricaDomande(int idTurno, string cognome, string cassaEdile)
        {
            string ceRicerca = null;

            if (cassaEdile != string.Empty && cassaEdile != "TUTTI")
            {
                if (cassaEdile == "MILANO")
                    ceRicerca = "MILANO";
                else
                    ceRicerca = cassaEdile;
            }

            bool? annullate = null;
            if (DropDownListStatoDomande.SelectedValue == "ANNULLATE")
                annullate = true;
            else if (DropDownListStatoDomande.SelectedValue == "NOANNULLATE")
                annullate = false;

            DomandaCollection domande = biz.GetDomandeLiquidate(null, null, cognome, idTurno, annullate, ceRicerca, null);

            LabelPartecipanti.Visible = true;
            GridViewDomande.DataSource = domande;
            GridViewDomande.DataBind();

            CaricaAutobus(idTurno);
        }

        private void CaricaAutobus(int idTurno)
        {
            DropDownListAutobus.Items.Clear();
            DropDownListAutobus.Items.Add(new ListItem(string.Empty, string.Empty));

            AutobusCollection autobus = biz.GetAutobus(idTurno);

            DropDownListAutobus.DataSource = autobus;
            DropDownListAutobus.DataTextField = "TestoCombo";
            DropDownListAutobus.DataValueField = "IdCombo";
            DropDownListAutobus.DataBind();
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            int anno;
            if (string.IsNullOrEmpty(TextBoxAnno.Text) || !Int32.TryParse(TextBoxAnno.Text, out anno))
            {
                res = false;
                errori.Append("Anno non valido o non presente" + Environment.NewLine);
            }

            if (DropDownListTipiVacanza.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo vacanza" + Environment.NewLine);
            }

            if (!res)
                LabelErrore.Text = errori.ToString();
            else
                LabelErrore.Text = string.Empty;
            return res;
        }

        private bool ControlloCampiServerInserimento()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();
            int idTurno = (int)ViewState["IdTurno"];
            Turno turno = biz.GetTurni(idTurno, null, null, null, null)[0];

            DateTime rientroAnticipato;
            if (!string.IsNullOrEmpty(TextBoxRientroAnticipato.Text))
            {
                if (!DateTime.TryParse(TextBoxRientroAnticipato.Text, out rientroAnticipato))
                {
                    res = false;
                    errori.Append("Data rientro anticipato non valida" + Environment.NewLine);
                }
                else
                {
                    if (rientroAnticipato < turno.Dal || rientroAnticipato > turno.Al)
                    {
                        res = false;
                        errori.Append("Data rientro anticipato non inclusa nella durata del turno" + Environment.NewLine);
                    }
                }
            }

            DateTime richiestaAnnullamento;
            if (!string.IsNullOrEmpty(TextBoxRichiestaAnnullamento.Text) &&
                !DateTime.TryParse(TextBoxRichiestaAnnullamento.Text, out richiestaAnnullamento))
            {
                res = false;
                errori.Append("Data richiesta annullamento non valida" + Environment.NewLine);
            }

            DateTime partenzaPosticipata;
            if (!string.IsNullOrEmpty(TextBoxPartenzaPosticipata.Text))
            {
                if (!DateTime.TryParse(TextBoxPartenzaPosticipata.Text, out partenzaPosticipata))
                {
                    res = false;
                    errori.Append("Data partenza posticipata non valida" + Environment.NewLine);
                }
                else
                {
                    if (partenzaPosticipata < turno.Dal || partenzaPosticipata > turno.Al)
                    {
                        res = false;
                        errori.Append("Data partenza posticipata non inclusa nella durata del turno" + Environment.NewLine);
                    }
                }
            }

            if (!res)
                LabelErroreInserimento.Text = errori.ToString();
            else
                LabelErroreInserimento.Text = string.Empty;
            return res;
        }

        protected void DropDownListTurno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListTurno.SelectedIndex != -1)
            {
                CaricaDomande(Int32.Parse(DropDownListTurno.SelectedValue), TextBoxCognome.Text,
                              DropDownListCassaEdile.SelectedValue);
                GridViewDomande.PageIndex = 0;
                ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                ButtonAutobus.Enabled = true;
                PanelFiltro.Visible = true;
            }

            PanelDettagli.Visible = false;
            SvuotaDettagli();
        }

        private void SvuotaDettagli()
        {
            TextBoxRichiestaAnnullamento.Text = string.Empty;
            TextBoxRientroAnticipato.Text = string.Empty;
            CheckBoxImbarcato.Checked = false;
            CheckBoxPresenteColonia.Checked = false;
            DropDownListAutobus.SelectedValue = string.Empty;
            DropDownListMancatoImbarco.SelectedValue = string.Empty;
            LabelDomanda.Text = string.Empty;
            TextBoxPartenzaPosticipata.Text = string.Empty;

            LabelErroreInserimento.Text = string.Empty;
        }

        protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idDomanda = (int)GridViewDomande.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];
            int idTurno = (int)GridViewDomande.DataKeys[e.NewSelectedIndex].Values["IdTurno"];

            string cognome = (string)GridViewDomande.DataKeys[e.NewSelectedIndex].Values["Cognome"];
            string nome = (string)GridViewDomande.DataKeys[e.NewSelectedIndex].Values["Nome"];

            LabelDomanda.Text = cognome + " " + nome;

            CaricaDettagli(idDomanda, idTurno);

            PanelDettagli.Visible = true;
        }

        private void CaricaDettagli(int idDomanda, int idTurno)
        {
            TurnoDomanda turnoDomanda = biz.GetTurnoDomanda(idDomanda, idTurno);
            ViewState["TurnoDomanda"] = turnoDomanda;
            SvuotaDettagli();

            if (turnoDomanda.Imbarcato.HasValue)
            {
                CheckBoxImbarcato.Checked = turnoDomanda.Imbarcato.Value;
                if (CheckBoxImbarcato.Checked)
                {
                    DropDownListAutobus.Enabled = true;
                    DropDownListMancatoImbarco.Enabled = false;
                    CheckBoxPresenteColonia.Enabled = false;
                    DropDownListMancatoImbarco.SelectedValue = string.Empty;
                    TextBoxPartenzaPosticipata.Enabled = false;
                    TextBoxPartenzaPosticipata.Text = string.Empty;

                    PanelImbarcato.Visible = true;
                    PanelNonImbarcato.Visible = false;
                }
                else
                {
                    DropDownListAutobus.Enabled = false;
                    DropDownListMancatoImbarco.Enabled = true;
                    CheckBoxPresenteColonia.Enabled = true;
                    TextBoxPartenzaPosticipata.Enabled = true;

                    PanelImbarcato.Visible = false;
                    PanelNonImbarcato.Visible = true;
                }
            }
            else
            {
                DropDownListAutobus.Enabled = false;
                DropDownListMancatoImbarco.Enabled = true;
                CheckBoxPresenteColonia.Enabled = true;
                TextBoxPartenzaPosticipata.Enabled = true;
            }
            if (turnoDomanda.Autobus != null)
                DropDownListAutobus.SelectedValue = turnoDomanda.Autobus.IdAutobus.ToString();
            if (turnoDomanda.PresenteColonia.HasValue)
                CheckBoxPresenteColonia.Checked = turnoDomanda.PresenteColonia.Value;
            if (turnoDomanda.RientroAnticipato.HasValue)
                TextBoxRientroAnticipato.Text = turnoDomanda.RientroAnticipato.Value.ToShortDateString();
            if (turnoDomanda.RichiestaAnnullamento.HasValue)
                TextBoxRichiestaAnnullamento.Text = turnoDomanda.RichiestaAnnullamento.Value.ToShortDateString();
            if (turnoDomanda.PartenzaPosticipata.HasValue)
                TextBoxPartenzaPosticipata.Text = turnoDomanda.PartenzaPosticipata.Value.ToShortDateString();
            if (!string.IsNullOrEmpty(turnoDomanda.MancatoImbarco))
            {
                if (DropDownListMancatoImbarco.Items.FindByValue(turnoDomanda.MancatoImbarco) != null)
                    DropDownListMancatoImbarco.SelectedValue = turnoDomanda.MancatoImbarco;
                else
                    DropDownListMancatoImbarco.SelectedItem.Text = turnoDomanda.MancatoImbarco;
            }

            if (!CheckBoxPresenteColonia.Checked)
            {
                TextBoxRientroAnticipato.Text = string.Empty;
                TextBoxRientroAnticipato.Enabled = false;

                if (!CheckBoxImbarcato.Checked)
                    TextBoxPartenzaPosticipata.Enabled = true;
                else
                {
                    TextBoxPartenzaPosticipata.Text = string.Empty;
                    TextBoxPartenzaPosticipata.Enabled = false;
                }
            }
            else
            {
                TextBoxRientroAnticipato.Enabled = true;
            }


            CheckBoxHandicap.Checked = turnoDomanda.PortatoreHandicap;
            CheckBoxIntolleranze.Checked = turnoDomanda.IntolleranzeAlimentari;

            if (turnoDomanda.TipoDomanda == TipoDomanda.CE)
                AbilitaRigheHandicapIntolleranze(true);
            else
                AbilitaRigheHandicapIntolleranze(false);
        }

        protected void CheckBoxImbarcato_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxImbarcato.Checked)
            {
                DropDownListAutobus.Enabled = true;
                DropDownListMancatoImbarco.Enabled = false;
                CheckBoxPresenteColonia.Checked = true;
                CheckBoxPresenteColonia.Enabled = false;
                DropDownListMancatoImbarco.SelectedValue = string.Empty;
                TextBoxPartenzaPosticipata.Enabled = false;
                TextBoxPartenzaPosticipata.Text = string.Empty;
                TextBoxRientroAnticipato.Enabled = true;

                PanelImbarcato.Visible = true;
                PanelNonImbarcato.Visible = false;
            }
            else
            {
                DropDownListAutobus.Enabled = false;
                DropDownListMancatoImbarco.Enabled = true;
                CheckBoxPresenteColonia.Enabled = true;
                TextBoxPartenzaPosticipata.Enabled = true;
                DropDownListAutobus.SelectedValue = string.Empty;

                PanelImbarcato.Visible = false;
                PanelNonImbarcato.Visible = true;
            }
        }

        protected void ButtonAutobus_Click(object sender, EventArgs e)
        {
            if (ViewState["IdTurno"] != null)
                Response.Redirect("~/CeServizi/Colonie/ColonieGestioneAutobus.aspx?idTurno=" + ViewState["IdTurno"]);
        }

        protected void ButtonAggiorna_Click(object sender, EventArgs e)
        {
            if (ControlloCampiServerInserimento())
            {
                TurnoDomanda turnoDomanda = CreaTurnoDomanda();

                if (biz.UpdateTurnoDomanda(turnoDomanda))
                {
                    PanelDettagli.Visible = false;
                    SvuotaDettagli();
                    CaricaDomande((int)ViewState["IdTurno"], null, DropDownListCassaEdile.SelectedValue);
                }
                else
                    LabelErroreInserimento.Text = "Errore nell'aggiornamento";
            }
        }

        private TurnoDomanda CreaTurnoDomanda()
        {
            TurnoDomanda turnoDomandaPrec = (TurnoDomanda)ViewState["TurnoDomanda"];
            TurnoDomanda turnoDomanda = null;

            int? idAutobus = null;
            Autobus autobus = null;
            DateTime? rientroAnticipato = null;
            DateTime? richiestaAnnullamento = null;
            bool? imbarcato = null;
            string mancatoImbarco = null;
            bool? presenteColonia = null;
            bool handicap = false;
            bool intolleranze = false;
            DateTime? partenzaPosticipata = null;

            imbarcato = CheckBoxImbarcato.Checked;
            if (CheckBoxImbarcato.Checked)
            {
                if (DropDownListAutobus.SelectedIndex != -1 && !string.IsNullOrEmpty(DropDownListAutobus.SelectedValue))
                {
                    idAutobus = Int32.Parse(DropDownListAutobus.SelectedValue);
                    autobus = new Autobus(idAutobus, null, null, -1);
                }
            }
            else
            {
                if (DropDownListMancatoImbarco.SelectedIndex != -1 &&
                    !string.IsNullOrEmpty(DropDownListMancatoImbarco.SelectedValue))
                {
                    mancatoImbarco = DropDownListMancatoImbarco.SelectedItem.Text;
                }
            }

            if (!string.IsNullOrEmpty(TextBoxRientroAnticipato.Text))
                rientroAnticipato = DateTime.Parse(TextBoxRientroAnticipato.Text);
            if (!string.IsNullOrEmpty(TextBoxRichiestaAnnullamento.Text))
                richiestaAnnullamento = DateTime.Parse(TextBoxRichiestaAnnullamento.Text);
            if (!string.IsNullOrEmpty(TextBoxPartenzaPosticipata.Text))
                partenzaPosticipata = DateTime.Parse(TextBoxPartenzaPosticipata.Text);

            mancatoImbarco = DropDownListMancatoImbarco.SelectedValue;
            presenteColonia = CheckBoxPresenteColonia.Checked;

            handicap = CheckBoxHandicap.Checked;
            intolleranze = CheckBoxIntolleranze.Checked;

            turnoDomanda = new TurnoDomanda(turnoDomandaPrec.IdTurno, turnoDomandaPrec.IdDomanda, autobus,
                                            imbarcato, rientroAnticipato, richiestaAnnullamento, mancatoImbarco,
                                            presenteColonia, handicap,
                                            intolleranze, partenzaPosticipata);

            if (rigaHandicap.Visible)
                turnoDomanda.TipoDomanda = TipoDomanda.CE;
            else
                turnoDomanda.TipoDomanda = TipoDomanda.ACE;

            return turnoDomanda;
        }

        protected void ButtonFiltro_Click(object sender, EventArgs e)
        {
            if (DropDownListTurno.SelectedIndex != -1)
            {
                CaricaDomande(Int32.Parse(DropDownListTurno.SelectedValue), TextBoxCognome.Text,
                              DropDownListCassaEdile.SelectedValue);
                ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                ButtonAutobus.Enabled = true;
                PanelFiltro.Visible = true;
            }

            PanelDettagli.Visible = false;
            SvuotaDettagli();
        }

        protected void GridViewDomande_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int idDomanda = (int)GridViewDomande.DataKeys[e.RowIndex].Values["IdDomanda"];
            int idTurno = (int)GridViewDomande.DataKeys[e.RowIndex].Values["IdTurno"];
            Response.Redirect("~/CeServizi/Colonie/ColonieSchedaBambino.aspx?idDomanda=" + idDomanda + "&idTurno=" +
                              idTurno);
        }

        protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int idTurno = (int)ViewState["IdTurno"];

            GridViewDomande.PageIndex = e.NewPageIndex;
            CaricaDomande(idTurno, TextBoxCognome.Text, DropDownListCassaEdile.SelectedValue);
            GridViewDomande.DataBind();
        }

        protected void CheckBoxPresenteColonia_CheckedChanged(object sender, EventArgs e)
        {
            if (!CheckBoxPresenteColonia.Checked)
            {
                TextBoxRientroAnticipato.Text = string.Empty;
                TextBoxRientroAnticipato.Enabled = false;
            }
            else
            {
                TextBoxRientroAnticipato.Enabled = true;

                if (!CheckBoxImbarcato.Checked)
                    TextBoxPartenzaPosticipata.Enabled = true;
                else
                {
                    TextBoxPartenzaPosticipata.Text = string.Empty;
                    TextBoxPartenzaPosticipata.Enabled = false;
                }
            }
        }

        protected void GridViewDomandeNonAssegnate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (ViewState["anno"] != null && ViewState["idTipoVacanza"] != null)
            {
                int anno = (int)ViewState["anno"];
                int idTipoVacanza = (int)ViewState["idTipoVacanza"];

                LabelNonAssegnate.Visible = true;
                DomandaCollection domandeNonAssegnate = biz.GetDomandeLiquidateNonAssegnate(idTipoVacanza, anno, null, null);
                GridViewDomandeNonAssegnate.DataSource = domandeNonAssegnate;
                GridViewDomandeNonAssegnate.PageIndex = e.NewPageIndex;
                GridViewDomandeNonAssegnate.DataBind();
            }
        }

        protected void GridViewDomandeNonAssegnate_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string cognome = (string)GridViewDomandeNonAssegnate.DataKeys[e.NewSelectedIndex].Values["Cognome"];
            string nome = (string)GridViewDomandeNonAssegnate.DataKeys[e.NewSelectedIndex].Values["Nome"];
        }

        protected void GridViewDomandeNonAssegnate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Domanda domanda = (Domanda)e.Row.DataItem;

                Button buttonAssegna = (Button)e.Row.FindControl("ButtonAssegna");
                buttonAssegna.Attributes.Add("onclick",
                                             "window.open('" + ResolveUrl("~/CeServizi/Colonie/ColonieModificaPartecipantiTurno.aspx") +
                                             "?idTurno=-1&idVacanza="
                                             + domanda.IdVacanza + "&cognome=" + domanda.Cognome +
                                             "', '', 'menubar=no,height=670,width=750'); return false;");
            }
        }

        protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Domanda domanda = (Domanda)e.Row.DataItem;
                Button bScheda = (Button)e.Row.FindControl("ButtonScheda");
                CheckBox cbPortatoreHandicap = (CheckBox)e.Row.FindControl("CheckBoxPortatoreHandicap");
                Label lAccompagnatore = (Label)e.Row.FindControl("LabelAccompagnatore");

                cbPortatoreHandicap.Checked = domanda.PortatoreHandicap;
                if (domanda.PortatoreHandicap)
                {
                    e.Row.ForeColor = Color.Gray;
                }

                if (!string.IsNullOrEmpty(domanda.AnnullamentoStringa))
                    bScheda.Enabled = false;
            }
        }

        private void AbilitaRigheHandicapIntolleranze(bool abilita)
        {
            rigaDivisorio.Visible = abilita;
            rigaHandicap.Visible = abilita;
            rigaIntolleranze.Visible = abilita;
        }
    }
}