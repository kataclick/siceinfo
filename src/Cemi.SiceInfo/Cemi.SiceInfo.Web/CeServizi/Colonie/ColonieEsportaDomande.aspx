﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieEsportaDomande.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieEsportaDomande" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Esportazione domande"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxAnno"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 26px">
                &nbsp;<asp:Button ID="Button1" runat="server" Text="Visualizza turni" ValidationGroup="visualizzazione"
                    OnClick="ButtonVisualizza_Click" />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Turno"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTurno" runat="server" Width="300px" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownListTurno_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <table class="standardTable">
        <tr>
            <td>
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td style="height: 163px">
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataKeyNames="IdDomanda,IdTurno" AllowPaging="True" OnPageIndexChanging="GridViewDomande_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="stato" HeaderText="Stato" />
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presente
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button ID="ButtonEsportaAccettate" runat="server" Text="Esporta domande accettate"
        OnClick="ButtonEsportaAccettate_Click" Enabled="False" Width="220px" />
    <asp:Button ID="ButtonEsportaNonAccettate" runat="server" Text="Esporta domande non accettate"
        OnClick="ButtonEsportaNonAccettate_Click" Enabled="False" Width="230px" />
    <asp:Button ID="buttonEsportaPartecipanti" runat="server" Text="Esporta schede partecipanti"
        OnClick="ButtonEsportaSchedaPartecipanti_Click" Enabled="False" Width="220px" /><br />
    <br />
</asp:Content>
