﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieGestioneVacanze : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneMatrice);

            if (!Page.IsPostBack)
            {
                CaricaTipiVacanza();

                TextBoxAnnoRicerca.Text = DateTime.Now.Year.ToString();
            }
        }

        protected void ButtonNuova_Click(object sender, EventArgs e)
        {
            PanelDettagli.Visible = true;
            ButtonOperazione.Text = "Inserisci";
            LabelErrore.Text = string.Empty;
            SvuotaTutto();
        }

        private void SvuotaTutto()
        {
            ViewState["Vacanza"] = null;

            DropDownListTipoVacanza.SelectedIndex = 0;
            TextBoxAnno.Text = string.Empty;
            //TextBoxCostoGiornaliero.Text = string.Empty;
            TextBoxCostoPenalita.Text = string.Empty;
            TextBoxCostoDisabile.Text = string.Empty;
            TextBoxLimitePenalita.Text = string.Empty;
            CheckBoxAttiva.Checked = false;
            TextBoxLimiteInferiore.Text = string.Empty;
            TextBoxLimiteSuperiore.Text = string.Empty;
            TextBoxDataInizioPrenotazioni.Text = string.Empty;
            TextBoxDataFinePrenotazioni.Text = string.Empty;
            TextBoxDataInizioDomande.Text = string.Empty;
            TextBoxDataFineDomande.Text = string.Empty;
            LabelErrore.Text = string.Empty;
        }

        protected void GridViewVacanze_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridViewVacanze.SelectedIndex = e.NewSelectedIndex;
            int idVacanza = (int)GridViewVacanze.DataKeys[e.NewSelectedIndex].Value;
            PanelDettagli.Visible = true;
            ButtonOperazione.Text = "Modifica";

            CaricaVacanza(idVacanza);
        }

        private void CaricaVacanza(int idVacanza)
        {
            Vacanza vacanza = biz.GetVacanze(null, idVacanza)[0];
            ViewState["Vacanza"] = vacanza;

            TextBoxAnno.Text = vacanza.Anno.ToString();
            //TextBoxCostoGiornaliero.Text = vacanza.CostoGiornaliero.ToString("F2");
            //TextBoxCostoGiornalieroAccompagnatore.Text = vacanza.CostoGiornalieroAccompagnatore.ToString("F2");
            TextBoxCostoPenalita.Text = vacanza.Penalita.ToString("F2");
            TextBoxCostoDisabile.Text = vacanza.CostoDisabile.ToString("F2");
            TextBoxLimitePenalita.Text = vacanza.LimitePenalita.ToString();
            DropDownListTipoVacanza.SelectedValue = vacanza.TipoVacanza.IdTipoVacanza.ToString();
            CheckBoxAttiva.Checked = vacanza.Attiva;
            if (vacanza.DataInizioPrenotazioni.HasValue)
                TextBoxDataInizioPrenotazioni.Text = vacanza.DataInizioPrenotazioni.Value.ToShortDateString();
            if (vacanza.DataFinePrenotazioni.HasValue)
                TextBoxDataFinePrenotazioni.Text = vacanza.DataFinePrenotazioni.Value.ToShortDateString();
            if (vacanza.DataInizioDomandeACE.HasValue)
                TextBoxDataInizioDomande.Text = vacanza.DataInizioDomandeACE.Value.ToShortDateString();
            if (vacanza.DataFineDomandeACE.HasValue)
                TextBoxDataFineDomande.Text = vacanza.DataFineDomandeACE.Value.ToShortDateString();
            TextBoxLimiteInferiore.Text = vacanza.LimiteInferioreBimbi.Year.ToString();
            TextBoxLimiteSuperiore.Text = vacanza.LimiteSuperioreBimbi.Year.ToString();
            if (vacanza.DataInizioRichieste.HasValue)
                TextBoxDataInizioRichieste.Text = vacanza.DataInizioRichieste.Value.ToShortDateString();
            if (vacanza.DataFineRichieste.HasValue)
                TextBoxDataFineRichieste.Text = vacanza.DataFineRichieste.Value.ToShortDateString();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SvuotaTutto();
                PanelDettagli.Visible = false;
                CaricaVacanze();
            }
        }

        private void CaricaVacanze()
        {
            int? anno = null;
            int annoTemp;
            if (!string.IsNullOrEmpty(TextBoxAnnoRicerca.Text) && Int32.TryParse(TextBoxAnnoRicerca.Text, out annoTemp))
                anno = annoTemp;

            VacanzaCollection vacanze = biz.GetVacanze(anno, null);
            GridViewVacanze.DataSource = vacanze;
            GridViewVacanze.DataBind();
        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipoVacanza.DataSource = tipiVacanza;
            DropDownListTipoVacanza.DataTextField = "Descrizione";
            DropDownListTipoVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipoVacanza.DataBind();
        }

        protected void ButtonOperazione_Click(object sender, EventArgs e)
        {
            Page.Validate("dettagli");

            if (Page.IsValid)
            {
                if (ControlloCampiServer())
                {
                    Vacanza nuovaVacanza = CreaVacanza();

                    if (ViewState["Vacanza"] != null)
                    {
                        // Modifica
                        Vacanza vecchiaVacanza = (Vacanza)ViewState["Vacanza"];
                        nuovaVacanza.IdVacanza = vecchiaVacanza.IdVacanza;
                    }

                    if (biz.InsertUpdateVacanza(nuovaVacanza))
                    {
                        if (ViewState["Vacanza"] != null)
                        {
                            LabelErrore.Text = "Aggiornamento effettuato";
                            CaricaVacanza(nuovaVacanza.IdVacanza.Value);
                        }
                        else
                        {
                            LabelErrore.Text = "Inserimento effettuato";
                            SvuotaTutto();
                            CaricaVacanze();
                        }
                    }
                    else
                    {
                        if (ViewState["Vacanza"] != null)
                            LabelErrore.Text = "Errore nell'aggiornamento";
                        else
                            LabelErrore.Text = "Errore nell'inserimento";
                    }
                }
            }
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            if (DropDownListTipoVacanza.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo vacanza" + Environment.NewLine);
            }

            int anno;
            if (string.IsNullOrEmpty(TextBoxAnno.Text) || !Int32.TryParse(TextBoxAnno.Text, out anno))
            {
                res = false;
                errori.Append("Digitare un anno o formato errato" + Environment.NewLine);
            }

            //decimal costoGiornaliero;
            //if (string.IsNullOrEmpty(TextBoxCostoGiornaliero.Text) ||
            //    !decimal.TryParse(TextBoxCostoGiornaliero.Text, out costoGiornaliero))
            //{
            //    res = false;
            //    errori.Append("Digitare un costo giornaliero o formato errato" + Environment.NewLine);
            //}

            decimal costoDisabile;
            if (string.IsNullOrEmpty(TextBoxCostoDisabile.Text) ||
                !decimal.TryParse(TextBoxCostoDisabile.Text, out costoDisabile))
            {
                res = false;
                errori.Append("Digitare un costo disabile o formato errato" + Environment.NewLine);
            }

            decimal costoPenale;
            if (string.IsNullOrEmpty(TextBoxCostoPenalita.Text) ||
                !decimal.TryParse(TextBoxCostoPenalita.Text, out costoPenale))
            {
                res = false;
                errori.Append("Digitare un costo penale o formato errato" + Environment.NewLine);
            }

            int limite;
            if (string.IsNullOrEmpty(TextBoxLimitePenalita.Text) || !Int32.TryParse(TextBoxLimitePenalita.Text, out limite))
            {
                res = false;
                errori.Append("Digitare un limite penalità o formato errato" + Environment.NewLine);
            }

            if (!string.IsNullOrEmpty(TextBoxDataInizioPrenotazioni.Text) ^
                !string.IsNullOrEmpty(TextBoxDataFinePrenotazioni.Text))
            {
                res = false;
                errori.Append("Valorizzare entrambe le date per le prenotazioni" + Environment.NewLine);
            }
            else if (!string.IsNullOrEmpty(TextBoxDataInizioPrenotazioni.Text) &&
                     !string.IsNullOrEmpty(TextBoxDataFinePrenotazioni.Text))
            {
                DateTime dataInizio = DateTime.Parse(TextBoxDataInizioPrenotazioni.Text);
                DateTime dataFine = DateTime.Parse(TextBoxDataFinePrenotazioni.Text);

                if (dataInizio >= dataFine)
                {
                    res = false;
                    errori.Append("La data di inizio delle prenotazioni deve essere precedente a quella di fine" +
                                  Environment.NewLine);
                }
            }

            if (!string.IsNullOrEmpty(TextBoxDataInizioDomande.Text) ^ !string.IsNullOrEmpty(TextBoxDataFineDomande.Text))
            {
                res = false;
                errori.Append("Valorizzare entrambe le date per l'inserimento delle domande ACE" + Environment.NewLine);
            }
            else if (!string.IsNullOrEmpty(TextBoxDataInizioDomande.Text) &&
                     !string.IsNullOrEmpty(TextBoxDataFineDomande.Text))
            {
                DateTime dataInizio = DateTime.Parse(TextBoxDataInizioDomande.Text);
                DateTime dataFine = DateTime.Parse(TextBoxDataFineDomande.Text);

                if (dataInizio >= dataFine)
                {
                    res = false;
                    errori.Append(
                        "La data di inizio per l'inserimento delle domande ACE deve essere precedente a quella di fine" +
                        Environment.NewLine);
                }
            }

            if (!res)
                LabelErrore.Text = errori.ToString();
            return res;
        }

        private Vacanza CreaVacanza()
        {
            int anno = -1;
            decimal costoDisabile = -1;
            decimal costoPenalita = -1;
            int limite = -1;
            int idTipoVacanza = -1;
            TipoVacanza tipoVacanza = null;

            anno = Int32.Parse(TextBoxAnno.Text);
            costoDisabile = decimal.Parse(TextBoxCostoDisabile.Text);
            costoPenalita = decimal.Parse(TextBoxCostoPenalita.Text);
            limite = Int32.Parse(TextBoxLimitePenalita.Text);
            idTipoVacanza = Int32.Parse(DropDownListTipoVacanza.SelectedItem.Value);
            tipoVacanza = new TipoVacanza(idTipoVacanza, string.Empty);

            Vacanza vacanza = new Vacanza(null, anno, tipoVacanza, 0,
                                          costoPenalita, costoDisabile, limite);

            if (!string.IsNullOrEmpty(TextBoxDataInizioPrenotazioni.Text))
                vacanza.DataInizioPrenotazioni = DateTime.Parse(TextBoxDataInizioPrenotazioni.Text);
            if (!string.IsNullOrEmpty(TextBoxDataFinePrenotazioni.Text))
                vacanza.DataFinePrenotazioni = DateTime.Parse(TextBoxDataFinePrenotazioni.Text);
            if (!string.IsNullOrEmpty(TextBoxDataInizioDomande.Text))
                vacanza.DataInizioDomandeACE = DateTime.Parse(TextBoxDataInizioDomande.Text);
            if (!string.IsNullOrEmpty(TextBoxDataFineDomande.Text))
                vacanza.DataFineDomandeACE = DateTime.Parse(TextBoxDataFineDomande.Text);
            vacanza.Attiva = CheckBoxAttiva.Checked;
            vacanza.LimiteInferioreBimbi = new DateTime(Int32.Parse(TextBoxLimiteInferiore.Text), 1, 1);
            vacanza.LimiteSuperioreBimbi = new DateTime(Int32.Parse(TextBoxLimiteSuperiore.Text), 1, 1);
            vacanza.CostoGiornalieroAccompagnatore = 0;

            if (!String.IsNullOrEmpty(TextBoxDataInizioRichieste.Text))
            {
                vacanza.DataInizioRichieste = DateTime.Parse(TextBoxDataInizioRichieste.Text);
            }
            if (!String.IsNullOrEmpty(TextBoxDataFineRichieste.Text))
            {
                vacanza.DataFineRichieste = DateTime.Parse(TextBoxDataFineRichieste.Text);
            }

            return vacanza;
        }

        protected void GridViewVacanze_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int idVacanza = (int)GridViewVacanze.DataKeys[e.NewEditIndex].Value;
            Response.Redirect("~/CeServizi/Colonie/ColonieGestioneTurni.aspx?idVacanza=" + idVacanza);
        }

        protected void GridViewVacanze_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int idVacanza = (int)GridViewVacanze.DataKeys[e.RowIndex].Value;
            Response.Redirect("~/CeServizi/Colonie/AbilitazioneCasseEdili.aspx?idVacanza=" + idVacanza);
        }

        protected void CustomValidatorDataInizioFine_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!String.IsNullOrEmpty(TextBoxDataInizioRichieste.Text)
                ^ !String.IsNullOrEmpty(TextBoxDataFineRichieste.Text))
            {
                args.IsValid = false;
            }
        }
    }
}