﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls
{
    public partial class ColonieRicercaDomandeACEEffettive : System.Web.UI.UserControl
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();
        private Int32 idVacanza;
        private String idCassaEdile = string.Empty;

        private const Int32 GIORNITOLLERANZA = 30;
        private DateTime dataPresenzeOk;

        public void SetParametri(Int32 _idVacanza, string _idCassaEdile)
        {
            idVacanza = _idVacanza;
            ViewState["IdVacanzaAttiva"] = idVacanza;

            idCassaEdile = _idCassaEdile;
            ViewState["IdCassaEdile"] = idCassaEdile;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            idVacanza = (Int32)ViewState["IdVacanzaAttiva"];
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            CaricaDomande();
        }

        public void CaricaDomande()
        {
            int? idDestinazione = null;
            int? idTurno = null;
            string cognome = null;

            if (!string.IsNullOrEmpty(TextBoxCognome.Text))
                cognome = TextBoxCognome.Text;

            idCassaEdile = (String)ViewState["IdCassaEdile"];

            GridViewDomande.DataSource = biz.GetDomandeACEEffettive(idVacanza, idDestinazione, cognome, idTurno, null, idCassaEdile);

            GridViewDomande.DataBind();
        }

        protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gvTurni = (GridView)e.Row.FindControl("GridViewTurni");
                DomandaACEEffettiva domanda = (DomandaACEEffettiva)e.Row.DataItem;

                gvTurni.DataSource = domanda.Turni;
                gvTurni.DataBind();
            }
        }



        protected void GridViewTurni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TurnoDomanda turno = (TurnoDomanda)e.Row.DataItem;

                Label lTurno = (Label)e.Row.FindControl("LabelTurno");
                Label lPresenza = (Label)e.Row.FindControl("LabelPresente");
                Label lRientroAnticipato = (Label)e.Row.FindControl("LabelRientroAnticipato");
                Label lRichiestaAnnullamento = (Label)e.Row.FindControl("LabelRichiestaAnnullamento");
                Label lPartenzaPosticipata = (Label)e.Row.FindControl("LabelPartenzaPosticipata");

                if (!string.IsNullOrEmpty(turno.DescrizioneCompleta))
                    lTurno.Text = turno.DescrizioneCompleta;

                if (DateTime.Now >= dataPresenzeOk.AddDays(GIORNITOLLERANZA))
                {
                    if (turno.PresenteColonia.HasValue && turno.PresenteColonia.Value)
                    {
                        lPresenza.Text = "Sì";
                        if (turno.RientroAnticipato.HasValue)
                            lRientroAnticipato.Text = turno.RientroAnticipato.Value.ToString("dd/MM/yyyy");
                        else
                            lRientroAnticipato.Text = "-";
                        if (turno.PartenzaPosticipata.HasValue)
                            lPartenzaPosticipata.Text = turno.PartenzaPosticipata.Value.ToString("dd/MM/yyyy");
                        else
                            lPartenzaPosticipata.Text = "-";

                        lRichiestaAnnullamento.Text = "-";
                    }
                    else
                    {
                        lPresenza.Text = "No";
                        if (turno.RichiestaAnnullamento.HasValue)
                        {
                            lRichiestaAnnullamento.Text = turno.RichiestaAnnullamento.Value.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lRichiestaAnnullamento.Text = "-";
                        }
                        lRientroAnticipato.Text = "-";
                        lPartenzaPosticipata.Text = "-";
                    }
                }
                else
                {
                    if (turno.RichiestaAnnullamentoEsplicita)
                    {
                        lRichiestaAnnullamento.Text = turno.RichiestaAnnullamento.Value.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lRichiestaAnnullamento.Text = "-";
                    }
                    lPresenza.Text = "-";
                    lRientroAnticipato.Text = "-";
                    lPartenzaPosticipata.Text = "-";
                }
            }
        }

        protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewDomande.PageIndex = e.NewPageIndex;
            CaricaDomande();
        }

        private void CaricaDataPresenzeOk()
        {
            Vacanza vacanza = biz.GetVacanzaAttiva();
            TurnoCollection turni = biz.GetTurni(null, vacanza.IdVacanza, null, null, null);

            foreach (Turno turno in turni)
            {
                if (turno.Al > dataPresenzeOk)
                {
                    dataPresenzeOk = turno.Al;
                }
            }
        }

        protected void GridViewDomande_DataBinding(object sender, EventArgs e)
        {
            CaricaDataPresenzeOk();
        }
    }
}