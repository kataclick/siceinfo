﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColonieRicercaDomandeACEPerConferma.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.ColonieRicercaDomandeACEPerConferma" %>

<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca domande"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Turno
                        </td>
                        <td>
                            Stato
                        </td>
                        <td>
                            Cognome lav.
                        </td>
                        <td>
                            Cognome bamb.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownListTurno" runat="server" AppendDataBoundItems="True"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListStato" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognomeLavoratore" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognomeBambino" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            &nbsp;
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante l'operazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco domande"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanelRicerca" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PanelGridView" runat="server" Visible="true" Width="100%">
                            <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                                AllowPaging="True" DataKeyNames="IdDomanda" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                                OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging" OnRowDataBound="GridViewDomande_RowDataBound"
                                OnRowDeleting="GridViewDomande_RowDeleting" OnRowCommand="GridViewDomande_RowCommand">
                                <EmptyDataTemplate>
                                    Nessuna domanda trovata
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="StringaLavoratore" HeaderText="Lavoratore" />
                                    <asp:BoundField DataField="StringaPartecipante" HeaderText="Partecipante" />
                                    <asp:BoundField DataField="StatoDomanda" HeaderText="Stato">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NumeroCorredo" HeaderText="N&#176; corredo">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="StringaCassaEdile" HeaderText="Cassa Edile" />
                                    <asp:TemplateField HeaderText="Problemi">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelProblemi" runat="server" ForeColor="Red"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Seleziona">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxSelezionato" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="50px" />
                                    </asp:TemplateField>
                                    <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="visualizza"
                                        Text="Visualizza">
                                        <ItemStyle Width="50px" />
                                    </asp:ButtonField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Accetta"
                                        ShowSelectButton="True">
                                        <ItemStyle Width="50px" />
                                    </asp:CommandField>
                                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rifiuta"
                                        ShowDeleteButton="True">
                                        <ItemStyle Width="50px" />
                                    </asp:CommandField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="PanelRifiuta" runat="server" Visible="false" Width="100%">
                            <table class="borderedTable">
                                <tr>
                                    <td>
                                        Proseguendo verrà rifiutata la domanda di
                                        <asp:Label ID="LabelPartecipante" runat="server" Font-Bold="true"></asp:Label>
                                        <asp:Label ID="LabelIdDomanda" runat="server" Font-Bold="True" Visible="False"></asp:Label><br />
                                        Immettere una motivazione
                                        <asp:TextBox ID="TextBoxMotivazione" runat="server" Width="300px" MaxLength="255"></asp:TextBox>
                                        <br />
                                        <asp:Button ID="ButtonConfermaRifiuta" runat="server" Text="Conferma" OnClick="ButtonConfermaRifiuta_Click" />&nbsp;
                                        <asp:Button ID="ButtonAnnullaRifiuta" runat="server" Text="Annulla" OnClick="ButtonAnnullaRifiuta_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonAccettaSelezionati" runat="server" Text="Accetta selezionati"
                    OnClick="ButtonAccettaSelezionati_Click" Visible="False" />&nbsp;
                <asp:Button ID="ButtonRifiutaSelezionati" runat="server" Text="Rifiuta selezionati"
                    OnClick="ButtonRifiutaSelezionati_Click" Visible="False" />
            </td>
        </tr>
    </table>
</asp:Panel>
