﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Delegates.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Filters.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls
{
    public partial class ColonieRicercaCassaEdile : System.Web.UI.UserControl
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();
        private int? idVacanza = null;

        public int? IdVacanza
        {
            get { return idVacanza; }
            set { idVacanza = value; }
        }

        public event CassaEdileSelectEventHandler OnCassaEdileSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            GridViewCasseEdili.PageIndex = 0;
            CaricaCasseEdili();
        }

        public void ForzaRicerca()
        {
            CaricaCasseEdili();
        }

        private void CaricaCasseEdili()
        {
            CassaEdileFilter filtro = new CassaEdileFilter();

            if (!string.IsNullOrEmpty(TextBoxCodice.Text))
                filtro.IdCassaEdile = TextBoxCodice.Text;
            if (!string.IsNullOrEmpty(TextBoxDescrizione.Text))
                filtro.Descrizione = TextBoxDescrizione.Text;
            filtro.IdVacanza = idVacanza;

            CassaEdileCollection casseEdili = biz.GetCasseEdiliNonAbilitate(filtro);
            GridViewCasseEdili.DataSource = casseEdili;
            GridViewCasseEdili.DataBind();
        }

        protected void GridViewCasseEdili_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewCasseEdili.PageIndex = e.NewPageIndex;
            CaricaCasseEdili();
        }

        protected void GridViewCasseEdili_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string idCassaEdile = (string)GridViewCasseEdili.DataKeys[e.NewSelectedIndex].Value;
            CassaEdileFilter filtro = new CassaEdileFilter();
            filtro.IdCassaEdile = idCassaEdile;

            CassaEdile cassaEdile = biz.GetCasseEdiliNonAbilitate(filtro)[0];

            if (OnCassaEdileSelected != null)
                OnCassaEdileSelected(cassaEdile);
        }
    }
}