﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using System.Collections;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Colonie;


namespace Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls
{
    public partial class ColonieDatiColloquio : System.Web.UI.UserControl
    {
        private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaMansioni();
                CaricaValutazioni();

                if (ViewState["Mansioni"] == null)
                {
                    ViewState["Mansioni"] = new List<ColoniePersonaleMansione>();
                }
                CaricaMansioniSelezionate();
            }
        }

        public void CaricaRichiesta(Int32 idRichiesta)
        {
            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);
            Presenter.CaricaElementiInBulletedList(
                BulletedListMansioniRichieste,
                richiesta.ColoniePersonaleMansioni,
                "Descrizione",
                "Id");
        }

        private void CaricaMansioniSelezionate()
        {
            Presenter.CaricaElementiInListBox(
                ListBoxMansioni,
                (List<ColoniePersonaleMansione>)ViewState["Mansioni"],
                "Descrizione",
                "Id");
        }

        private void CaricaMansioni()
        {
            if (RadComboBoxMansione.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDown(
                    RadComboBoxMansione,
                    bizEF.GetPersonaleMansioni(),
                    "Descrizione",
                    "Id");
            }
        }

        private void CaricaValutazioni()
        {
            if (RadComboBoxValutazione.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    RadComboBoxValutazione,
                    bizEF.GetPersonaleValutazioni(),
                    "Descrizione",
                    "Id");
            }
        }

        public void CaricaColloquio(ColoniePersonaleColloquio colloquio)
        {
            CaricaValutazioni();

            ViewState["IdColloquio"] = colloquio.Id;
            RadDateTimePickerData.SelectedDate = colloquio.Data;

            List<ColoniePersonaleMansione> mansioni = new List<ColoniePersonaleMansione>();
            foreach (ColoniePersonaleMansione mans in colloquio.ColoniePersonaleMansioni)
            {
                mansioni.Add(mans);
            }
            ViewState["Mansioni"] = mansioni;
            CaricaMansioniSelezionate();

            if (colloquio.IdValutazione.HasValue)
            {
                RadComboBoxValutazione.SelectedValue = colloquio.IdValutazione.ToString();
            }
        }

        public ColoniePersonaleColloquio GetColloquio()
        {
            ColoniePersonaleColloquio colloquio = new ColoniePersonaleColloquio();

            if (ViewState["IdColloquio"] != null)
            {
                colloquio.Id = (Int32)ViewState["IdColloquio"];
            }
            colloquio.Data = RadDateTimePickerData.SelectedDate.Value;
            colloquio.ColoniePersonaleMansioni = (List<ColoniePersonaleMansione>)ViewState["Mansioni"];
            if (!String.IsNullOrEmpty(RadComboBoxValutazione.SelectedValue))
            {
                ColoniePersonaleValutazione valutazione = new ColoniePersonaleValutazione();
                valutazione.Id = Int32.Parse(RadComboBoxValutazione.SelectedItem.Value);
                valutazione.Descrizione = RadComboBoxValutazione.SelectedItem.Text;

                colloquio.Valutazione = valutazione;
            }

            return colloquio;
        }

        #region Custom Validators
        protected void CustomValidatorData_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime? dataSelezionata = RadDateTimePickerData.SelectedDate;

            // Controllo che la data sia nel futuro solo per i colloqui nuovi
            if (ViewState["IdColloquio"] == null)
            {
                if (!dataSelezionata.HasValue
                    || dataSelezionata.Value < DateTime.Now
                    || dataSelezionata.Value.Hour < 5
                    || dataSelezionata.Value.Hour > 22)
                {
                    args.IsValid = false;
                }
            }
        }

        protected void CustomValidatorValidatorMansioni_ServerValidate(object source, ServerValidateEventArgs args)
        {
            List<ColoniePersonaleMansione> mansioni = (List<ColoniePersonaleMansione>)ViewState["Mansioni"];

            if (mansioni.Count == 0)
            {
                args.IsValid = false;
            }
        }
        #endregion

        protected void ButtonAggiungiMansione_Click(object sender, EventArgs e)
        {
            if (RadComboBoxMansione.SelectedItem != null)
            {
                ColoniePersonaleMansione mansione = new ColoniePersonaleMansione()
                {
                    Id = Int32.Parse(RadComboBoxMansione.SelectedItem.Value),
                    Descrizione = RadComboBoxMansione.SelectedItem.Text
                };

                AggiungiMansione(mansione);
            }
        }

        private void AggiungiMansione(ColoniePersonaleMansione mansione)
        {
            Boolean inserisci = true;

            List<ColoniePersonaleMansione> mansioni = (List<ColoniePersonaleMansione>)ViewState["Mansioni"];
            foreach (ColoniePersonaleMansione mans in mansioni)
            {
                if (mansione.Id == mans.Id)
                {
                    inserisci = false;
                    break;
                }
            }

            if (inserisci)
            {
                mansioni.Add(mansione);
                ViewState["Mansioni"] = mansioni;

                CaricaMansioniSelezionate();
            }
        }

        protected void ButtonEliminaSelezionato_Click(object sender, EventArgs e)
        {
            if (ListBoxMansioni.SelectedIndex >= 0)
            {
                List<ColoniePersonaleMansione> mansioni = (List<ColoniePersonaleMansione>)ViewState["Mansioni"];
                mansioni.RemoveAt(ListBoxMansioni.SelectedIndex);
                ViewState["Mansioni"] = mansioni;
                CaricaMansioniSelezionate();
            }
        }
    }
}