﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Delegates.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.Colonie;
using TBridge.Cemi.Type.Filters.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls
{
    public partial class ColonieRicercaDomandeACEPerConferma : System.Web.UI.UserControl
    {
        #region Costanti

        private int INDICEACCETTA = 8;
        private int INDICEPARTECIPANTE = 1;
        private int INDICERIFIUTA = 9;
        private int INDICESELEZIONA = 6;

        #endregion

        #region Eventi

        public event DomandaACESelectedEventHandler OnDomandaSelected;

        #endregion

        #region Attributi

        private ColonieBusiness biz = new ColonieBusiness();
        private string idCassaEdile = null;
        private int idVacanza;
        private Vacanza vacanza = null;

        #endregion

        #region Proprietà

        public int IdVacanza
        {
            get { return idVacanza; }
            set { idVacanza = value; }
        }

        public string IdCassaEdile
        {
            get { return idCassaEdile; }
            set { idCassaEdile = value; }
        }

        #endregion

        #region Eventi pagina

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaStati();
            }

            if (idVacanza > 0)
                vacanza = biz.GetVacanze(null, idVacanza)[0];
        }

        #endregion

        #region Funzioni per il caricamento dei filtri

        /// <summary>
        /// Carica i turni disponibili per il filtro di ricerca, deve essere chiamata dalla pagina che ospita il controllo
        /// </summary>
        /// <param name="turni"></param>
        public void CaricaTurni(TurnoCollection turni)
        {
            DropDownListTurno.Items.Clear();

            DropDownListTurno.Items.Add(new ListItem("Tutti", string.Empty));
            DropDownListTurno.DataSource = turni;
            DropDownListTurno.DataTextField = "DescrizioneTurno";
            DropDownListTurno.DataValueField = "IdCombo";
            DropDownListTurno.DataBind();
        }

        /// <summary>
        /// Carica gli stati delle domande per il filtro di ricerca
        /// </summary>
        private void CaricaStati()
        {
            DropDownListStato.Items.Clear();

            DropDownListStato.Items.Add(new ListItem("Tutti", string.Empty));
            DropDownListStato.DataSource = Enum.GetNames(typeof(StatoDomandaACE));
            DropDownListStato.DataBind();
        }

        #endregion

        #region Eventi controlli

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            AbilitaGridView(true);

            if (Page.IsValid)
            {
                GridViewDomande.Visible = true;
                ButtonAccettaSelezionati.Visible = true;
                //ButtonRifiutaSelezionati.Visible = true;
                GridViewDomande.PageIndex = 0;
                CaricaDomande();
            }
        }

        protected void ButtonAccettaSelezionati_Click(object sender, EventArgs e)
        {
            ModificaStatoDomandeSelezionate(true);
            CaricaDomande();
        }

        protected void ButtonRifiutaSelezionati_Click(object sender, EventArgs e)
        {
            ModificaStatoDomandeSelezionate(false);
            CaricaDomande();
        }

        protected void ButtonConfermaRifiuta_Click(object sender, EventArgs e)
        {
            int idDomanda = Int32.Parse(LabelIdDomanda.Text);
            string motivazione = null;

            if (!string.IsNullOrEmpty(TextBoxMotivazione.Text.Trim()))
                motivazione = TextBoxMotivazione.Text.Trim().ToUpper();

            if (biz.UpdateDomandaACERifiuta(idDomanda, motivazione))
            {
                AbilitaGridView(true);
                CaricaDomande();
                LabelErrore.Visible = false;
            }
            else
                LabelErrore.Visible = true;
        }

        protected void ButtonAnnullaRifiuta_Click(object sender, EventArgs e)
        {
            AbilitaGridView(true);
            CaricaDomande();
        }

        #region GridViewDomande

        protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewDomande.PageIndex = e.NewPageIndex;
            CaricaDomande();
        }

        protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idDomanda = (int)GridViewDomande.DataKeys[e.NewSelectedIndex].Value;

            if (biz.UpdateDomandaACEAccetta(idDomanda))
            {
                CaricaDomande();
                LabelErrore.Visible = false;
            }
            else
                LabelErrore.Visible = true;
        }

        protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DomandaACE domanda = (DomandaACE)e.Row.DataItem;
                Label lProblemi = (Label)e.Row.FindControl("LabelProblemi");
                StringBuilder problemi = new StringBuilder();

                if (domanda.StatoDomanda != StatoDomandaACE.DaValutare)
                {
                    e.Row.Cells[INDICESELEZIONA].Enabled = false;
                    e.Row.Cells[INDICEACCETTA].Enabled = false;
                    e.Row.Cells[INDICERIFIUTA].Enabled = false;

                    if (domanda.StatoDomanda == StatoDomandaACE.Rifiutata)
                    {
                        e.Row.ForeColor = Color.Red;
                        if (!string.IsNullOrEmpty(domanda.MotivazioneRifiuto))
                        {
                            problemi.Append("- ");
                            problemi.Append(domanda.MotivazioneRifiuto);
                            problemi.Append("<br />");
                        }
                    }
                }

                if (!biz.EtaBimboOk(vacanza, domanda.Bambino.DataNascita))
                    problemi.Append("- Età bimbo non regolare");

                lProblemi.Text = problemi.ToString();
            }
        }

        protected void GridViewDomande_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int idDomanda = (int)GridViewDomande.DataKeys[e.RowIndex].Value;
            LabelIdDomanda.Text = idDomanda.ToString();
            AbilitaGridView(false);
            LabelPartecipante.Text = GridViewDomande.Rows[e.RowIndex].Cells[INDICEPARTECIPANTE].Text;
        }

        protected void GridViewDomande_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "visualizza")
            {
                int idDomanda = (int)GridViewDomande.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Value;

                DomandaACEFilter filtro = new DomandaACEFilter();
                filtro.IdDomanda = idDomanda;
                DomandaACE domanda = biz.GetDomandeACE(filtro)[0];

                if (OnDomandaSelected != null)
                    OnDomandaSelected(domanda);
            }
        }

        #endregion

        #endregion

        private void CaricaDomande()
        {
            DomandaACEFilter filtro = new DomandaACEFilter();

            if (!string.IsNullOrEmpty(DropDownListTurno.SelectedValue))
                filtro.IdTurno = Int32.Parse(DropDownListTurno.SelectedValue);

            if (!string.IsNullOrEmpty(TextBoxCognomeLavoratore.Text.Trim()))
                filtro.CognomeLavoratore = TextBoxCognomeLavoratore.Text.Trim();

            if (!string.IsNullOrEmpty(TextBoxCognomeBambino.Text.Trim()))
                filtro.CognomeBambino = TextBoxCognomeBambino.Text.Trim();

            if (!string.IsNullOrEmpty(DropDownListStato.SelectedValue))
                filtro.Stato = (StatoDomandaACE)Enum.Parse(typeof(StatoDomandaACE), DropDownListStato.SelectedValue);

            filtro.IdVacanza = idVacanza;

            if (!string.IsNullOrEmpty(idCassaEdile))
                filtro.IdCassaEdile = idCassaEdile;

            DomandaACECollection domande = biz.GetDomandeACE(filtro);
            GridViewDomande.DataSource = domande;
            GridViewDomande.DataBind();
        }

        private bool ModificaStatoDomandeSelezionate(bool accetta)
        {
            int daAggiornare = 0;
            int aggiornati = 0;
            bool res = true;

            foreach (GridViewRow row in GridViewDomande.Rows)
            {
                CheckBox cbSelezionato = (CheckBox)row.FindControl("CheckBoxSelezionato");
                if (cbSelezionato.Checked)
                {
                    int idDomandaACE = (int)GridViewDomande.DataKeys[row.RowIndex].Value;
                    daAggiornare++;

                    if (accetta)
                    {
                        if (biz.UpdateDomandaACEAccetta(idDomandaACE))
                            aggiornati++;
                    }
                    else
                    {
                        if (biz.UpdateDomandaACERifiuta(idDomandaACE, null))
                            aggiornati++;
                    }
                }
            }

            if (daAggiornare == aggiornati)
                LabelErrore.Visible = false;
            else
                LabelErrore.Visible = true;

            if (daAggiornare == 0)
                res = false;

            return res;
        }

        public void Reset()
        {
            GridViewDomande.Visible = false;
            ButtonAccettaSelezionati.Visible = false;
            //ButtonRifiutaSelezionati.Visible = false;
        }

        private void AbilitaGridView(bool abilita)
        {
            PanelGridView.Visible = abilita;
            PanelRifiuta.Visible = !abilita;
        }
    }
}