﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using System.Configuration;
using System.Net;
using Microsoft.Reporting.WebForms;
using System.IO;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls
{
    public partial class ColonieRiassuntoProposta : System.Web.UI.UserControl
    {
        private const Int32 INDICENOPROPOSTA = 0;
        private const Int32 INDICEPROPOSTA = 1;

        private ColonieBusinessEF bizEF = new ColonieBusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Click multipli
            StringBuilder sbProp = new StringBuilder();
            sbProp.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sbProp.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sbProp.Append(
                "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sbProp.Append("this.value = 'Attendere...';");
            sbProp.Append("this.disabled = true;");
            sbProp.Append(Page.ClientScript.GetPostBackEventReference(ButtonPropostaInvia, null));
            sbProp.Append(";");
            sbProp.Append("return true;");
            ButtonPropostaInvia.Attributes.Add("onclick", sbProp.ToString());

            StringBuilder sbConv = new StringBuilder();
            sbConv.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sbConv.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sbConv.Append(
                "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sbConv.Append("this.value = 'Attendere...';");
            sbConv.Append("this.disabled = true;");
            sbConv.Append(Page.ClientScript.GetPostBackEventReference(ButtonPropostaInviaConvocazione, null));
            sbConv.Append(";");
            sbConv.Append("return true;");
            ButtonPropostaInviaConvocazione.Attributes.Add("onclick", sbConv.ToString());
            #endregion
        }

        protected String RecuperaPostBackCode()
        {
            return this.Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
        }

        public void CaricaProposta(Int32 idRichiesta, ColoniePersonaleProposta proposta)
        {
            Int32? idProposta = null;

            ViewState["IdRichiesta"] = idRichiesta;

            if (proposta == null)
            {
                MultiViewProposta.ActiveViewIndex = INDICENOPROPOSTA;
            }
            else
            {
                MultiViewProposta.ActiveViewIndex = INDICEPROPOSTA;
                idProposta = proposta.Id;
                ViewState["IdProposta"] = idProposta;
                LabelDataInizioRapporto.Text = proposta.DataInizioRapporto.ToString("dd/MM/yyyy");
                LabelDataFineRapporto.Text = proposta.DataFineRapporto.ToString("dd/MM/yyyy");
                LabelMansione.Text = proposta.Mansione.Descrizione;
                LabelLivello.Text = proposta.Retribuzione.Livello;
                LabelAccompagnatore.Text = proposta.Accompagnatore ? "Sì" : "No";
                LabelAccettata.Text = proposta.Accettata.HasValue ? (proposta.Accettata.Value ? "Sì" : "No") : "In Attesa di Risposta";
                Presenter.CaricaElementiInBulletedList(
                    BulletedListTurni,
                    proposta.Turni,
                    "Descrizione",
                    "Id");

                if (proposta.Accettata.HasValue && proposta.Accettata.Value)
                {
                    ButtonPropostaInviaConvocazione.Enabled = true;
                }
                else
                {
                    ButtonPropostaInviaConvocazione.Enabled = false;
                }

                if (proposta.Inviata.HasValue)
                {
                    LabelInviata.Text = String.Format("In data {0:dd/MM/yyyy}", proposta.Inviata.Value);
                }
                else
                {
                    LabelInviata.Text = "No";
                }

                if (proposta.InviataConvocazione.HasValue)
                {
                    LabelInviataConvocazione.Text = String.Format("In data {0:dd/MM/yyyy}", proposta.InviataConvocazione.Value);
                }
                else
                {
                    LabelInviataConvocazione.Text = "No";
                }

                if (proposta.LuogoAppuntamento != null)
                {
                    LabelLuogoAppuntamento.Text = proposta.LuogoAppuntamento.Descrizione;
                }
                else
                {
                    LabelLuogoAppuntamento.Text = "VILLAGGIO VACANZE CASSA EDILE";
                }

                LabelOraAppuntamento.Text = proposta.OraAppuntamento.ToString("HH:mm");
            }

            string comando = String.Format(
                        "openRadWindowProposta({0}, {1}); return false;",
                        idRichiesta,
                        idProposta.HasValue ? idProposta.Value : -1);

            ButtonPropostaInserisci.Attributes.Add("OnClick", comando);
            ButtonPropostaModifica.Attributes.Add("OnClick", comando);
        }

        public void DisabilitaTutto()
        {
            ButtonPropostaInserisci.Enabled = false;
            ButtonPropostaModifica.Enabled = false;
            ButtonPropostaInvia.Enabled = false;
            ButtonPropostaInviaConvocazione.Enabled = false;
            ButtonPropostaVisualizza.Enabled = false;
        }

        protected void ButtonPropostaVisualizza_Click(object sender, EventArgs e)
        {
            Context.Items["IdRichiesta"] = ViewState["IdRichiesta"];
            Server.Transfer("~/CeServizi/Colonie/ColonieProposta.aspx");
        }

        protected void ButtonPropostaInvia_Click(object sender, EventArgs e)
        {
            Int32 idRichiesta = (Int32)ViewState["IdRichiesta"];
            Int32 idProposta = (Int32)ViewState["IdProposta"];
            InviaEmailProposta(idRichiesta, idProposta);
            bizEF.UpdatePropostaInvio(idProposta);

            ColoniePersonaleProposta proposta = bizEF.GetProposta(idProposta);
            CaricaProposta(idRichiesta, proposta);
        }

        private void InviaEmailProposta(Int32 idRichiesta, Int32 idProposta)
        {
            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            email.Destinatari = new List<EmailAddress>();
            //email.Destinatari.Add(new EmailAddress("alessio.mura@itsinfinity.com", "alessio.mura@itsinfinity.com"));
            email.Destinatari.Add(new EmailAddress(richiesta.Email, richiesta.Email));

            email.DestinatariBCC = new List<EmailAddress>();
            if (!Presenter.IsSviluppo)
            {
                email.DestinatariBCC.Add(new EmailAddress("personale.colonie@cassaedilemilano.it", "personale.colonie@cassaedilemilano.it"));
            }

            var sbPlain = new StringBuilder();
            var sbHtml = new StringBuilder();

            #region Creiamo il plaintext della mail
            sbPlain.AppendLine(String.Format("Gentile {0} {1},", richiesta.Nome, richiesta.Cognome));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("facendo seguito alla sua candidatura (Prot. Num. {0}) le inviamo la proposta di assunzione per il nostro soggiorno estivo.", richiesta.Id));
            //sbPlain.Append(Environment.NewLine);
            //sbPlain.AppendLine("La informiamo che il 4° turno (dal 07/08/2015 al 20/08/2015) verrà effettuato solo al raggiungimento di un numero minimo di partecipanti. Per questo motivo, Le chiediamo fin d’ora, di manifestare la sua disponibilità per un eventuale 4° turno.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("La invitiamo a rispondere a questa email confermando la Sua disponibilità o comunicando la Sua rinuncia, indicando nell'oggetto il numero di richiesta {0}.", richiesta.Id));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Con i migliori saluti,");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Ufficio Personale");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");
            sbPlain.AppendLine("Via S. Luca, 6");
            sbPlain.AppendLine("20122 Milano (Mi)");

            #endregion
            email.BodyPlain = sbPlain.ToString();

            #region Creiamo l'htlm text della mail
            sbHtml.Append(String.Format("<span style=\"font-family: Calibri; font-size: 11pt\">Gentile <b>{0} {1}</b>,", richiesta.Nome, richiesta.Cognome));
            sbHtml.Append("<br /><br />");
            sbHtml.Append(String.Format("facendo seguito alla sua candidatura (Prot. Num. {0}) le inviamo la proposta di assunzione per il nostro soggiorno estivo.", richiesta.Id));
            //sbHtml.Append("<br /><br />");
            //sbHtml.Append("La informiamo che il 4° turno (dal 07/08/2015 al 20/08/2015) verrà effettuato solo al raggiungimento di un numero minimo di partecipanti. Per questo motivo, Le chiediamo fin d’ora, di manifestare la sua disponibilità per un eventuale 4° turno.");
            sbHtml.Append("<br /><br />");
            sbHtml.Append(String.Format("La invitiamo a rispondere a questa email confermando la Sua disponibilità o comunicando la Sua rinuncia, <b>indicando nell'oggetto il numero di richiesta {0}</b>.", richiesta.Id));
            sbHtml.Append("<br /><br />");
            sbHtml.Append("Con i migliori saluti,");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<strong>Ufficio Personale</strong>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<strong>Cassa Edile di Milano, Lodi, Monza e Brianza</strong><br />Via S. Luca, 6<br />20122 Milano (Mi)");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("Web <a href=\"http://www.cassaedilemilano.it\">www.cassaedilemilano.it</a></span>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<img src=\"http://ww2.cassaedilemilano.it/Portals/0/images/Logo%20mail%20aziendale.jpg\" />");

            #endregion
            email.BodyHTML = sbHtml.ToString();

            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = "personale.colonie@cassaedilemilano.it";
            email.Mittente.Nome = "Ufficio Personale";

            email.Oggetto = String.Format("Proposta assunzione soggiorni estivi (Prot. Num. {0})", richiesta.Id);

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.Normal;

            // Report proposta
            ReportViewerProposta.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerProposta.ServerReport.ReportPath = "/ReportColonie/ReportPropostaImmagine";

            ReportParameter[] listaParam = new ReportParameter[1];
            listaParam[0] = new ReportParameter("id", idRichiesta.ToString());
            ReportViewerProposta.ServerReport.SetParameters(listaParam);

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            //PDF
            byte[] bytes = ReportViewerProposta.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            // Allegato contributo viaggio
            byte[] bytesContrViaggio = null;
            using (BinaryReader binReader = new BinaryReader(File.Open(HttpContext.Current.Request.MapPath("~/CeServizi/Colonie/Static/Allegato1.pdf"), FileMode.Open)))
            {
                bytesContrViaggio = new byte[binReader.BaseStream.Length];
                binReader.Read(bytesContrViaggio, 0, (Int32)binReader.BaseStream.Length);
            }

            // Allegato richiesta certificato penale
            byte[] bytesCertPen = null;
            using (BinaryReader binReader = new BinaryReader(File.Open(HttpContext.Current.Request.MapPath("~/CeServizi/Colonie/Static/Allegato2.pdf"), FileMode.Open)))
            {
                bytesCertPen = new byte[binReader.BaseStream.Length];
                binReader.Read(bytesCertPen, 0, (Int32)binReader.BaseStream.Length);
            }

            email.Allegati = new List<EmailAttachment>();
            email.Allegati.Add(new EmailAttachment("Proposta.pdf", bytes));
            email.Allegati.Add(new EmailAttachment("Allegato1.pdf", bytesContrViaggio));
            email.Allegati.Add(new EmailAttachment("Allegato2.pdf", bytesCertPen));

            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            var service = new EmailInfoService();
            var credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;
            service.InviaEmail(email);
        }

        protected void ButtonPropostaInviaConvocazione_Click(object sender, EventArgs e)
        {
            Int32 idRichiesta = (Int32)ViewState["IdRichiesta"];
            Int32 idProposta = (Int32)ViewState["IdProposta"];
            InviaEmailConvocazione(idRichiesta, idProposta);
            bizEF.UpdatePropostaConvocazione(idProposta);

            ColoniePersonaleProposta proposta = bizEF.GetProposta(idProposta);
            CaricaProposta(idRichiesta, proposta);
        }

        private void InviaEmailConvocazione(int idRichiesta, int idProposta)
        {
            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            email.Destinatari = new List<EmailAddress>();
            //email.Destinatari.Add(new EmailAddress("alessio.mura@itsinfinity.com", "alessio.mura@itsinfinity.com"));
            email.Destinatari.Add(new EmailAddress(richiesta.Email, richiesta.Email));

            email.DestinatariBCC = new List<EmailAddress>();
            email.DestinatariBCC.Add(new EmailAddress("personale.colonie@cassaedilemilano.it", "personale.colonie@cassaedilemilano.it"));

            var sbPlain = new StringBuilder();
            var sbHtml = new StringBuilder();

            #region Creiamo il plaintext della mail
            sbPlain.AppendLine(String.Format("Gentile {0} {1},", richiesta.Nome, richiesta.Cognome));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("facciamo seguito alla nostra precedente comunicazione per confermarLe la Sua assunzione dal {0:dd/MM/yyyy} al {1:dd/MM/yyyy}.",
                richiesta.ColoniePersonaleProposte.Single().DataInizioRapporto,
                richiesta.ColoniePersonaleProposte.Single().DataFineRapporto));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("La invitiamo, quindi, a presentarsi il giorno {0:dd/MM/yyyy} alle ore {1} presso",
                richiesta.ColoniePersonaleProposte.Single().DataInizioRapporto,
                richiesta.ColoniePersonaleProposte.Single().OraAppuntamento.ToString("HH:mm")));
            sbPlain.Append(Environment.NewLine);
            if (richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento == null)
            {
                sbPlain.AppendLine("VILLAGGIO VACANZE CASSA EDILE");
                sbPlain.AppendLine(String.Format("{0} - {1} {2} ({3})",
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Indirizzo.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Cap.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Comune.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Provincia.ToUpper()));
            }
            else
            {
                sbPlain.AppendLine(richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Descrizione.ToUpper());
                sbPlain.AppendLine(String.Format("{0} - {1} {2} ({3})",
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Indirizzo.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Cap.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Comune.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Provincia.ToUpper()));
            }
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("All'arrivo al Villaggio Vacanze dovrà consegnare:");
            sbPlain.AppendLine("- Carta d'identità;");
            sbPlain.AppendLine("- Codice fiscale;");
            sbPlain.AppendLine("- Modulo IBAN per accredito retribuzione (presente in allegato).");
            switch (richiesta.ColoniePersonaleProposte.Single().IdMansione)
            {
                // Cuoco, Aiuto cuoco, Inserviente cucina, Inserviente generico, Guardorobiere, Aiuto guardarobiere
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    sbPlain.Append(Environment.NewLine);
                    sbPlain.AppendLine("Per il personale assunto per le mansioni di servizio (cucina, guardaroba, inservienti) sono richiesti:");
                    sbPlain.AppendLine("- Calzoni neri tipo : bermuda – ciclisti – pinocchietto;");
                    sbPlain.AppendLine("- N° 2 magliette bianche tipo T-shirt.");
                    break;
                // Infermiere professionale
                case 5:
                    sbPlain.Append(Environment.NewLine);
                    sbPlain.AppendLine("Per il personale assunto per le mansioni di infermeria è richiesta la divisa.");
                    break;
            }
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Il contratto di assunzione in originale Le sarà consegnato direttamente presso il villaggio vacanze.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Con i migliori saluti,");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Ufficio Personale");
            sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");

            #endregion
            email.BodyPlain = sbPlain.ToString();

            #region Creiamo l'htlm text della mail
            sbHtml.Append(String.Format("<span style=\"font-family: Calibri; font-size: 11pt\">Gentile <b>{0} {1}</b>,", richiesta.Nome, richiesta.Cognome));
            sbHtml.Append("<br /><br />");
            sbHtml.Append(String.Format("facciamo seguito alla nostra precedente comunicazione per confermarLe la Sua assunzione dal {0:dd/MM/yyyy} al {1:dd/MM/yyyy}.",
                richiesta.ColoniePersonaleProposte.Single().DataInizioRapporto,
                richiesta.ColoniePersonaleProposte.Single().DataFineRapporto));
            sbHtml.Append("<br /><br />");
            sbHtml.Append(String.Format("La invitiamo, quindi, a presentarsi il giorno <b>{0:dd/MM/yyyy}</b> alle ore <b>{1}</b> presso",
                richiesta.ColoniePersonaleProposte.Single().DataInizioRapporto,
                richiesta.ColoniePersonaleProposte.Single().OraAppuntamento.ToString("HH:mm")));
            sbHtml.Append("<br /><br />");
            if (richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento == null)
            {
                sbHtml.Append("<b>VILLAGGIO VACANZE CASSA EDILE");
                sbHtml.Append("<br />");
                sbHtml.Append(String.Format("{0} - {1} {2} ({3})</b>",
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Indirizzo.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Cap.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Comune.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().Turni.Aggregate((curmin, x) => (curmin == null || x.Dal < curmin.Dal) ? x : curmin).ColonieDestinazione.Provincia.ToUpper()));
            }
            else
            {
                sbHtml.Append(String.Format("<b>{0}", richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Descrizione.ToUpper()));
                sbHtml.Append("<br />");
                sbHtml.Append(String.Format("{0} - {1} {2} ({3})</b>",
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Indirizzo.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Cap.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Comune.ToUpper(),
                    richiesta.ColoniePersonaleProposte.Single().LuogoAppuntamento.Provincia.ToUpper()));
            }
            sbHtml.Append("<br /><br />");
            sbHtml.Append("All'arrivo al Villaggio Vacanze dovrà consegnare:");
            sbHtml.Append("<br />");
            sbHtml.Append("<ul>");
            sbHtml.Append("<li>");
            sbHtml.AppendLine("Carta d'identità;");
            sbHtml.Append("</li>");
            sbHtml.Append("<li>");
            sbHtml.AppendLine("Codice fiscale;");
            sbHtml.Append("</li>");
            sbHtml.Append("<li>");
            sbHtml.Append("Modulo IBAN per accredito retribuzione (presente in allegato).");
            sbHtml.Append("</li>");
            sbHtml.Append("</ul>");
            switch (richiesta.ColoniePersonaleProposte.Single().IdMansione)
            {
                // Cuoco, Aiuto cuoco, Inserviente cucina, Inserviente generico, Guardorobiere, Aiuto guardarobiere
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    sbHtml.Append("<br />");
                    sbHtml.AppendLine("Per il personale assunto per le mansioni di servizio (cucina, guardaroba, inservienti) sono richiesti:");
                    sbHtml.Append("<ul>");
                    sbHtml.Append("<li>");
                    sbHtml.AppendLine("- Calzoni neri tipo : bermuda – ciclisti – pinocchietto;");
                    sbHtml.Append("</li>");
                    sbHtml.Append("<li>");
                    sbHtml.AppendLine("- N° 2 magliette bianche tipo T-shirt.");
                    sbHtml.Append("</li>");
                    sbHtml.Append("</ul>");
                    break;
                // Infermiere professionale
                case 5:
                    sbHtml.Append("<br />");
                    sbHtml.AppendLine("Per il personale assunto per le mansioni di infermeria è richiesta la divisa.");
                    sbHtml.Append("<br />");
                    break;
            }
            sbHtml.Append("<br />");
            sbHtml.Append("Il contratto di assunzione in originale Le sarà consegnato direttamente presso il villaggio vacanze.");
            sbHtml.Append("<br />");
            sbHtml.Append("<br />");
            sbHtml.Append("Con i migliori saluti,</span>");
            sbHtml.Append("<br /><br />");
            /*
            sbHtml.Append("Ufficio Personale");
            sbHtml.Append("<br />");
            sbHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            */
            sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Ufficio Personale</strong></span>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Cassa Edile di Milano, Lodi, Monza e Brianza</strong><br />Via S. Luca, 6<br />20122 Milano (Mi)</span>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\">Web <a href=\"http://www.cassaedilemilano.it\">www.cassaedilemilano.it</a>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<img src=\"http://ww2.cassaedilemilano.it/Portals/0/images/Logo%20mail%20aziendale.jpg\" />");
            #endregion

            email.BodyHTML = sbHtml.ToString();

            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = "personale.colonie@cassaedilemilano.it";
            email.Mittente.Nome = "Ufficio Personale";

            email.Oggetto = String.Format("Comunicazione data partenza soggiorni estivi (Prot. Num. {0})", richiesta.Id);

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.Normal;

            // Allegato IBAN
            byte[] bytesIBAN = null;
            using (BinaryReader binReader = new BinaryReader(File.Open(HttpContext.Current.Request.MapPath("~/CeServizi/Colonie/Static/Iban.pdf"), FileMode.Open)))
            {
                bytesIBAN = new byte[binReader.BaseStream.Length];
                binReader.Read(bytesIBAN, 0, (Int32)binReader.BaseStream.Length);
            }

            // Allegato accompagnatori
            byte[] bytesAccompagnatori = null;
            using (BinaryReader binReader = new BinaryReader(File.Open(HttpContext.Current.Request.MapPath("~/CeServizi/Colonie/Static/Accompagnatori.pdf"), FileMode.Open)))
            {
                bytesAccompagnatori = new byte[binReader.BaseStream.Length];
                binReader.Read(bytesAccompagnatori, 0, (Int32)binReader.BaseStream.Length);
            }

            // Allegato educatori
            byte[] bytesEducatori = null;
            try
            {
                using (BinaryReader binReader = new BinaryReader(File.Open(HttpContext.Current.Request.MapPath("~/CeServizi/Colonie/Static/Educatori.pdf"), FileMode.Open)))
                {
                    bytesEducatori = new byte[binReader.BaseStream.Length];
                    binReader.Read(bytesEducatori, 0, (Int32)binReader.BaseStream.Length);
                }
            }
            catch
            {
            }

            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            email.Allegati = new List<EmailAttachment>();
            email.Allegati.Add(new EmailAttachment("Iban.pdf", bytesIBAN));
            if (richiesta.ColoniePersonaleProposte.Single().Accompagnatore)
            {
                email.Allegati.Add(new EmailAttachment("Accompagnatori.pdf", bytesAccompagnatori));
            }
            if (bytesEducatori != null && richiesta.ColoniePersonaleProposte.Single().IdMansione == 1) // Educatori
            {
                email.Allegati.Add(new EmailAttachment("Educatori.pdf", bytesEducatori));
            }

            var service = new EmailInfoService();
            var credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;
            service.InviaEmail(email);
        }
    }
}