﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VacanzaAttivaRiassunto.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.VacanzaAttivaRiassunto" %>

<table class="filledtable">
    <tr>
        <td>
            Vacanza
        </td>
    </tr>
</table>
<table class="borderedTable">
    <colgroup>
        <col width="200px" />
        <col />
    </colgroup>
    <tr>
        <td>
            Anno:
        </td>
        <td>
            <b>
                <asp:Label
                    ID="LabelAnno"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Tipo:
        </td>
        <td>
            <b>
                <asp:Label
                    ID="LabelTipo"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
</table>