﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColonieRicercaDomande.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.ColonieRicercaDomande" %>
<table class="standardTable">
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Destinazione
                    </td>
                    <td>
                        Cognome
                    </td>
                    <td>
                        Turno
                    </td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="DropDownListDestinazione" runat="server" Width="100%" AppendDataBoundItems="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListTurno" runat="server" Width="100%" AppendDataBoundItems="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                            Text="Ricerca" Width="100%" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox 
                            ID="CheckBoxNonAssegnate"
                            runat="server"
                            Text="Non Assegnate" AutoPostBack="True" 
                            oncheckedchanged="CheckBoxNonAssegnate_CheckedChanged" />
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        &nbsp; &nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" DataKeyNames="IdDomanda"
                Width="100%" OnRowDataBound="GridViewDomande_RowDataBound" OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging"
                AllowPaging="True" OnPageIndexChanging="GridViewDomande_PageIndexChanging" PageSize="2">
                <Columns>
                    <asp:BoundField HeaderText="Cognome" DataField="Cognome" />
                    <asp:BoundField HeaderText="Nome" DataField="Nome" />
                    <asp:BoundField HeaderText="Data di nascita" DataField="StringaDataNascita">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Turni">
                        <ItemTemplate>
                            <asp:GridView ID="GridViewTurni" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewTurni_RowDataBound"
                                DataKeyNames="IdTurno" ShowHeader="False" Width="150px">
                                <Columns>
                                    <asp:BoundField DataField="DescrizioneTurno" HeaderText="Turno">
                                        <ItemStyle Font-Size="XX-Small" Height="10px" VerticalAlign="Bottom" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxScelta" runat="server" Height="10px" Width="10px" Style="margin: 0px;
                                                vertical-align: top;" />
                                        </ItemTemplate>
                                        <ItemStyle Font-Size="XX-Small" VerticalAlign="Top" HorizontalAlign="Center" Height="15px" />
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle Height="10px" />
                                <RowStyle Height="10px" />
                                <AlternatingRowStyle Height="10px" />
                            </asp:GridView>
                        </ItemTemplate>
                        <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Salva"
                        ShowSelectButton="True">
                        <ItemStyle Width="30px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna domanda trovata
                </EmptyDataTemplate>
            </asp:GridView>
        </td>
    </tr>
</table>
<asp:Button ID="ButtonAggiorna" runat="server" OnClick="ButtonVisualizza_Click" Text="Aggiorna totali" />
<br />
<asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label>
<br />
<asp:Label ID="LabelSovrapposizioni" runat="server" ForeColor="Red"></asp:Label>
