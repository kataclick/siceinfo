﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColonieDatiProposta.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.ColonieDatiProposta" %>

<style type="text/css">
    .primaColonnaProposta
    {
        width: 180px;
    }
</style>

<table class="standardTable">
    <tr>
        <td class="primaColonnaProposta">
            Data Inizio Rapporto<b>*</b>:
        </td>
        <td>
            <telerik:RadDatePicker
                ID="RadDateTimePickerDataInizio"
                runat="server"
                Width="200px">
            </telerik:RadDatePicker>
            <asp:CustomValidator
                ID="CustomValidatorDataInizio"
                runat="server"
                ValidationGroup="proposta"
                ControlToValidate="RadDateTimePickerDataInizio"
                ErrorMessage="Inserire una data di inizio rapporto valida"
                ForeColor="Red" 
                onservervalidate="CustomValidatorDataInizio_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaProposta">
            Data Fine Rapporto<b>*</b>:
        </td>
        <td>
            <telerik:RadDatePicker
                ID="RadDateTimePickerDataFine"
                runat="server"
                Width="200px">
            </telerik:RadDatePicker>
            <asp:CustomValidator
                ID="CustomValidatorDataFine"
                runat="server"
                ValidationGroup="proposta"
                ControlToValidate="RadDateTimePickerDataFine"
                ErrorMessage="Inserire una data di fine rapporto valida"
                ForeColor="Red" 
                onservervalidate="CustomValidatorDataFine_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaProposta">
            Mansione<b>*</b>:
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxMansione"
                runat="server"
                Width="200px"
                EmptyMessage="Selezionare una mansione"
                AutoPostBack="true" 
                onselectedindexchanged="RadComboBoxMansione_SelectedIndexChanged">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorMansione"
                runat="server"
                ValidationGroup="proposta"
                ControlToValidate="RadComboBoxMansione"
                ErrorMessage="Selezionare la mansione"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaProposta">
            Livello<b>*</b>:
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxLivello"
                runat="server"
                Width="200px"
                EmptyMessage="Selezionare un livello"
                Enabled="false">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorLivello"
                runat="server"
                ValidationGroup="proposta"
                ControlToValidate="RadComboBoxLivello"
                ErrorMessage="Selezionare il livello"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaProposta">
            Accompagnatore<b>*</b>:
        </td>
        <td>
            <asp:CheckBox
                ID="CheckBoxAccompagnatore"
                runat="server" />
        </td>
    </tr>
    <tr>
        <td class="primaColonnaProposta">
            Protocollo<b>*</b>:
        </td>
        <td>
            <asp:TextBox
                ID="TextBoxProtocollo"
                runat="server"
                MaxLength="30"
                Width="190px" />
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorProtocollo"
                runat="server"
                ValidationGroup="proposta"
                ControlToValidate="TextBoxProtocollo"
                ErrorMessage="Indicare il protocollo"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaProposta">
            Accettazione<b>*</b>:
        </td>
        <td>
            <asp:RadioButton
                ID="RadioButtonInAttesa"
                runat="server"
                GroupName="accettata"
                Text="In Attesa"
                Checked="true" />
            <asp:RadioButton
                ID="RadioButtonSi"
                runat="server"
                GroupName="accettata"
                Text="Sì" />
            <asp:RadioButton
                ID="RadioButtonNo"
                runat="server"
                GroupName="accettata"
                Text="No" />
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
            Turni<b>*</b>
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxTurno"
                runat="server"
                Width="200px"
                EmptyMessage="Selezionare un turno">
            </telerik:RadComboBox>
            <br />
            <asp:Button
                ID="ButtonAggiungiTurno"
                runat="server"
                Text="Aggiungi" 
                onclick="ButtonAggiungiTurno_Click" 
                Height="18px"
                Width="100%" />
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
        </td>
        <td>
            <asp:ListBox
                ID="ListBoxTurni"
                runat="server"
                Width="200px">
            </asp:ListBox>
            <asp:CustomValidator
                ID="CustomValidatorValidatorTurni"
                runat="server"
                ValidationGroup="proposta"
                ErrorMessage="Selezionare almeno un turno"
                ForeColor="Red" 
                onservervalidate="CustomValidatorValidatorTurni_ServerValidate">
                *
            </asp:CustomValidator>
            <br />
            <asp:Button
                ID="ButtonEliminaSelezionato"
                runat="server"
                Text="Canc. selez." 
                Height="18px"
                Width="100%" onclick="ButtonEliminaSelezionato_Click" />
        
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
            Luogo appuntamento
            <small>
                (lasciare vuoto se appuntamento in colonia)
            </small>
            :
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxLuogoAppuntamento"
                runat="server"
                Width="200px"
                AppendDataBoundItems="true">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td class="primaColonnaColloquio">
            Ora appuntamento:
        </td>
        <td>
            <telerik:RadTimePicker
                ID="RadTimePickerOraAppuntamento"
                runat="server"
                Width="200px">
            </telerik:RadTimePicker>
            <asp:CustomValidator
                ID="CustomValidatorValidatorOraAppuntamento"
                runat="server"
                ValidationGroup="proposta"
                ErrorMessage="Indicare un'ora valida"
                ForeColor="Red" 
                onservervalidate="CustomValidatorValidatorOraAppuntamento_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>