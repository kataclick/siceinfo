﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColonieRicercaDomandeACE.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.ColonieRicercaDomandeACE" %>

<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonVisualizza">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Turno
                        </td>
                        <td>
                            Stato
                        </td>
                        <td>
                            Cognome lav.
                        </td>
                        <td>
                            Cognome bamb.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownListTurno" runat="server" AppendDataBoundItems="True"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListStato" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognomeLavoratore" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognomeBambino" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="ButtonEsporta" runat="server" Text="Esporta" OnClick="ButtonEsporta_Click" />&nbsp;
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante l'operazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco domande"></asp:Label><br />
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" DataKeyNames="IdDomanda" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging" OnRowEditing="GridViewDomande_RowEditing"
                    OnRowDataBound="GridViewDomande_RowDataBound" OnRowDeleting="GridViewDomande_RowDeleting">
                    <EmptyDataTemplate>
                        Nessuna domanda trovata
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="StringaLavoratore" HeaderText="Lavoratore" />
                        <asp:BoundField DataField="StringaPartecipante" HeaderText="Partecipante" />
                        <asp:BoundField DataField="StatoDomanda" HeaderText="Stato">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NumeroCorredo" HeaderText="N&#176; corredo">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Visualizza"
                            ShowSelectButton="True">
                            <ItemStyle Width="40px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Modifica"
                            ShowEditButton="True">
                            <ItemStyle Width="40px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina"
                            ShowDeleteButton="True">
                            <ItemStyle Width="40px" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
                &nbsp; &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
