﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Business.EmailClient;
using System.Text;
using Microsoft.Reporting.WebForms;
using System.Configuration;
using System.IO;
using System.Net;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls
{
    public partial class ColonieRiassuntoColloquio : System.Web.UI.UserControl
    {
        private const Int32 INDICENOCOLLOQUIO = 0;
        private const Int32 INDICECOLLOQUIO = 1;

        private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Click multipli
            StringBuilder sbProp = new StringBuilder();
            sbProp.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sbProp.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sbProp.Append(
                "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sbProp.Append("this.value = 'Attendere...';");
            sbProp.Append("this.disabled = true;");
            sbProp.Append(Page.ClientScript.GetPostBackEventReference(ButtonPropostaInvia, null));
            sbProp.Append(";");
            sbProp.Append("return true;");
            ButtonPropostaInvia.Attributes.Add("onclick", sbProp.ToString());
            #endregion
        }

        protected String RecuperaPostBackCode()
        {
            return this.Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
        }

        public void CaricaRichiesta(Int32 idRichiesta)
        {
            ViewState["IdRichiesta"] = idRichiesta;
        }

        public void CaricaColloquio(Int32 idRichiesta, ColoniePersonaleColloquio colloquio)
        {
            Int32? idColloquio = null;
            ViewState["IdRichiesta"] = idRichiesta;

            if (colloquio == null)
            {
                MultiViewColloquio.ActiveViewIndex = INDICENOCOLLOQUIO;
            }
            else
            {
                ViewState["IdColloquio"] = colloquio.Id;
                idColloquio = colloquio.Id;
                MultiViewColloquio.ActiveViewIndex = INDICECOLLOQUIO;
                LabelData.Text = colloquio.Data.ToString("dd/MM/yyyy HH:mm");
                Presenter.CaricaElementiInBulletedList(
                    BulletedListMansioni,
                    colloquio.ColoniePersonaleMansioni,
                    "Descrizione",
                    "Id");

                if (colloquio.Inviato.HasValue)
                {
                    LabelInviato.Text = String.Format("In data {0:dd/MM/yyyy}", colloquio.Inviato.Value);
                }
                else
                {
                    LabelInviato.Text = "No";
                }

                LabelValutazione.Text = colloquio.Valutazione != null ? colloquio.Valutazione.Descrizione : "Da Valutare";
            }

            string comando = String.Format(
                        "openRadWindowColloquio({0}, {1}); return false;",
                        idRichiesta,
                        idColloquio.HasValue ? idColloquio.Value : -1);

            ButtonColloquioInserisci.Attributes.Add("OnClick", comando);
            ButtonColloquioModifica.Attributes.Add("OnClick", comando);
        }

        public void DisabilitaTutto()
        {
            ButtonColloquioInserisci.Enabled = false;
            ButtonColloquioModifica.Enabled = false;
            ButtonPropostaInvia.Enabled = false;
        }

        protected void ButtonPropostaInvia_Click(object sender, EventArgs e)
        {
            Int32 idRichiesta = (Int32)ViewState["IdRichiesta"];
            Int32 idColloquio = (Int32)ViewState["IdColloquio"];
            InviaEmailConferma(idRichiesta);
            bizEF.UpdateColloquioInvio(idColloquio);
            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);
            CaricaColloquio(idRichiesta, richiesta.ColoniePersonaleColloquio.First());
        }

        private void InviaEmailConferma(Int32 idRichiesta)
        {
            ColoniePersonaleRichiesta richiesta = bizEF.GetPersonaleRichiesta(idRichiesta);

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            email.Destinatari = new List<EmailAddress>();
            //email.Destinatari.Add(new EmailAddress("alessio.mura@itsinfinity.com", "alessio.mura@itsinfinity.com"));
            email.Destinatari.Add(new EmailAddress(richiesta.Email, richiesta.Email));

            email.DestinatariBCC = new List<EmailAddress>();
            email.DestinatariBCC.Add(new EmailAddress("personale.colonie@cassaedilemilano.it", "personale.colonie@cassaedilemilano.it"));

            var sbPlain = new StringBuilder();
            var sbHtml = new StringBuilder();

            #region Creiamo il plaintext della mail
            sbPlain.AppendLine(String.Format("Gentile {0} {1},", richiesta.Nome, richiesta.Cognome));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("In relazione alla Sua candidatura per i nostri soggiorni estivi (Prot. Num. {0}), La invitiamo a presentarsi, per un colloquio preliminare, presso la nostra sede di Via San Luca n°6 - Milano", richiesta.Id));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("il giorno {0:dd/MM/yyyy}", richiesta.ColoniePersonaleColloquio.Single().Data));
            sbPlain.AppendLine(String.Format("alle ore {0:HH:mm}", richiesta.ColoniePersonaleColloquio.Single().Data));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("In allegato troverà la seguente documentazione che dovrà compilare e restituire al momento dell'incontro:");
            sbPlain.AppendLine("- Questionario;");
            sbPlain.AppendLine("- Dichiarazione trattamento dati personali (privacy);");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Requisito preferenziale per la valutazione sarà la padronanza di almeno una delle attività elencate nel suddetto questionario.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine(String.Format("La invitiamo a confermare l'avvenuta ricezione della presente e la partecipazione alla selezione rispondendo a questa e-mail indicando il numero di richiesta {0}.", richiesta.Id));
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Completiamo la documentazione allegando l’Informativa sull'attività di Cassa Edile.");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Con i migliori saluti,");
            sbPlain.Append(Environment.NewLine);
            sbPlain.AppendLine("Ufficio Personale");
            sbPlain.AppendLine("Cassa Edile di Milano, Lodi, Monza e Brianza");

            #endregion
            email.BodyPlain = sbPlain.ToString();

            #region Creiamo l'htlm text della mail
            sbHtml.Append(String.Format("<span style=\"font-family: Calibri; font-size: 11pt\">Gentile <b>{0} {1}</b>,", richiesta.Nome, richiesta.Cognome));
            sbHtml.Append("<br /><br />");
            sbHtml.AppendLine(String.Format("In relazione alla Sua candidatura per i nostri soggiorni estivi (Prot. Num. {0}), La invitiamo a presentarsi, per un colloquio preliminare, presso la nostra sede di <b>Via San Luca n°6 - Milano</b>", richiesta.Id));
            sbHtml.Append("<br /><br />");
            sbHtml.AppendLine(String.Format("il giorno <b>{0:dd/MM/yyyy}</b>", richiesta.ColoniePersonaleColloquio.Single().Data));
            sbHtml.Append("<br />");
            sbHtml.AppendLine(String.Format("alle ore <b>{0:HH:mm}</b>", richiesta.ColoniePersonaleColloquio.Single().Data));
            sbHtml.Append("<br /><br />");
            sbHtml.AppendLine("In allegato troverà la seguente documentazione che dovrà compilare e restituire al momento dell'incontro:");
            sbHtml.Append("<br />");
            sbHtml.Append("<ul>");
            sbHtml.Append("<li>");
            sbHtml.AppendLine("Questionario;");
            sbHtml.Append("</li>");
            sbHtml.Append("<li>");
            sbHtml.AppendLine("Dichiarazione trattamento dati personali (privacy);");
            sbHtml.Append("</li>");
            sbHtml.Append("</ul>");
            sbHtml.Append("<br />");
            sbHtml.AppendLine("Requisito preferenziale per la valutazione sarà la padronanza di almeno una delle attività elencate nel suddetto questionario.");
            sbHtml.Append("<br /><br />");
            sbHtml.AppendLine(String.Format("La invitiamo a confermare l'avvenuta ricezione della presente e la partecipazione alla selezione <b>rispondendo a questa e-mail indicando il numero di richiesta {0}.</b>", richiesta.Id));
            sbHtml.Append("<br /><br />");
            sbHtml.Append("Completiamo la documentazione allegando l’Informativa sull'attività di Cassa Edile.");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("Con i migliori saluti,</span>");
            sbHtml.Append("<br /><br />");
            /*
            sbHtml.Append("Ufficio Personale");
            sbHtml.Append("<br />");
            sbHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            */
            sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Ufficio Personale</strong></span>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\"><strong>Cassa Edile di Milano, Lodi, Monza e Brianza</strong><br />Via S. Luca, 6<br />20122 Milano (Mi)</span>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<span style=\"font-family: Calibri; font-size: 11pt\">Web <a href=\"http://www.cassaedilemilano.it\">www.cassaedilemilano.it</a>");
            sbHtml.Append("<br /><br />");
            sbHtml.Append("<img src=\"http://ww2.cassaedilemilano.it/Portals/0/images/Logo%20mail%20aziendale.jpg\" />");

            #endregion
            email.BodyHTML = sbHtml.ToString();

            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = "personale.colonie@cassaedilemilano.it";
            email.Mittente.Nome = "Ufficio Personale";

            email.Oggetto = String.Format("Convocazione colloquio preliminare soggiorni estivi (Prot. Num. {0})", richiesta.Id);

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.Normal;

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            // Report privacy
            ReportViewerPrivacy.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerPrivacy.ServerReport.ReportPath = "/ReportColonie/ReportPrivacy";

            ReportParameter[] listaParamPrivacy = new ReportParameter[1];
            listaParamPrivacy[0] = new ReportParameter("id", idRichiesta.ToString());
            ReportViewerPrivacy.ServerReport.SetParameters(listaParamPrivacy);

            //PDF
            byte[] bytesPrivacy = ReportViewerPrivacy.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            // Report questionario
            ReportViewerPrivacy.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerPrivacy.ServerReport.ReportPath = "/ReportColonie/ReportSchedaCandidato";

            ReportParameter[] listaParamQuestionario = new ReportParameter[1];
            listaParamQuestionario[0] = new ReportParameter("id", idRichiesta.ToString());
            ReportViewerPrivacy.ServerReport.SetParameters(listaParamQuestionario);

            //PDF
            byte[] bytesQuestionario = ReportViewerPrivacy.ServerReport.Render(
                "PDF", null, out mimeType, out encoding, out extension,
                out streamids, out warnings);

            // Allegato informativa
            byte[] bytesInformativa = null;
            using (BinaryReader binReader = new BinaryReader(File.Open(HttpContext.Current.Request.MapPath("~/CeServizi/Colonie/Static/Informativa.pdf"), FileMode.Open)))
            {
                bytesInformativa = new byte[binReader.BaseStream.Length];
                binReader.Read(bytesInformativa, 0, (Int32)binReader.BaseStream.Length);
            }

            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            email.Allegati = new List<EmailAttachment>();
            email.Allegati.Add(new EmailAttachment("Questionario.pdf", bytesQuestionario));
            email.Allegati.Add(new EmailAttachment("Privacy.pdf", bytesPrivacy));
            email.Allegati.Add(new EmailAttachment("Informativa.pdf", bytesInformativa));

            var service = new EmailInfoService();
            var credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;
            service.InviaEmail(email);
        }
    }
}