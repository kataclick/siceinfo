﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ColonieRiassuntoRichiesta.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.ColonieRiassuntoRichiesta" %>

<%@ Register src="ColonieRiassuntoColloquio.ascx" tagname="RiassuntoColloquio" tagprefix="uc1" %>
<%@ Register src="ColonieRiassuntoProposta.ascx" tagname="RiassuntoProposta" tagprefix="uc2" %>

<style type="text/css">
    .primaColonna
    {
        width: 180px;
    }
    .secondaColonna
    {
        width: 180px;
    }
    .terzaColonna
    {
        width: 100px;
        text-align:right;
    }
    .quartaColonna
    {
    }
    
    .bold
    {
        font-weight:bold;
        color:Black;
    }
    
    .noBold
    {
        font-weight:bold;
        color:Gray;
    }
</style>

<script type="text/javascript" id="telerikClientEvents1">
//<![CDATA[

	function emailKeyPressed() {
	    var tbEmail = document.getElementById('<%= TextBoxEmail.ClientID %>');
	    tbEmail.className = "noBold";
	}
//]]>
</script>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function refresh(arg) {
            <%= PostBackString %>
        }

        function openRadWindowRespingi(idRichiesta, idColloquio) {
            var oWindow = radopen("ColonieRespingiRichiesta.aspx?idRichiesta=" + idRichiesta, null);
            oWindow.set_title("Respingi");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(300, 180);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openRadWindowNote(idRichiesta, note) {
            var oWindow = radopen("ColonieRespingiRichiesta.aspx?idRichiesta=" + idRichiesta + "&note=" + note, null);
            oWindow.set_title("Note");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(300, 180);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadCodeBlock>

Stato:
<b>
    <asp:Label
        ID="LabelStato"
        runat="server">
    </asp:Label>
</b>
<br />
<asp:Button
    ID="ButtonRespingi"
    runat="server"
    Text="Respingi"
    Width="150px" 
    onclick="ButtonRespingi_Click" />
&nbsp;
<asp:Button
    ID="ButtonInviaRifiuto"
    runat="server"
    Text="Invia rifiuto"
    Width="150px"
    Enabled="false" onclick="ButtonInviaRifiuto_Click" />
&nbsp;
<small>
    <asp:Label
        ID="LabelInviatoRifiuto"
        runat="server">
    </asp:Label>
</small>
<br />
<br />
Note:
<b>
<asp:Label
    ID="LabelRespintaNote"
    runat="server">
</asp:Label>
</b>
<br />
<asp:Button
    ID="ButtonNote"
    runat="server"
    Text="Modifica"
    Width="150px" />
<br />
<br />
<table class="filledtable">
    <tr>
        <td>
            Precedenti candidature
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            <small>
                <asp:Label
                    ID="LabelNessunaCandidaturaPrecedente"
                    runat="server"
                    Text="Nessuna candidatura trovata."
                    Visible="true">
                </asp:Label>
            </small>
            <asp:BulletedList
                ID="BulletedListPrecedentiCandidature"
                runat="server"
                Visible="false">
            </asp:BulletedList>
        </td>
    </tr>
</table>
<br />
<br />
<table class="filledtable">
    <tr>
        <td>
            Richiesta
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>
                <asp:Label
                    ID="LabelCognomeNome"
                    runat="server">
                </asp:Label>
            </b>
        </td>
        <td rowspan="7" colspan="2" class="terzaColonna">
            <telerik:RadBinaryImage
                ID="RadBinaryImageFoto"
                runat="server"
                Height="120px"
                Width="120px" 
                ResizeMode="Crop" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="1px" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>
                <asp:Label
                    ID="LabelSesso"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>
                <asp:Label
                    ID="LabelNascita"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Codice Fiscale:</small>
        </td>
        <td class="secondaColonna">
            <small><asp:Label
                ID="LabelCodiceFiscale"
                runat="server">
            </asp:Label></small>
        </td>
    </tr>     
    <tr>
        <td class="primaColonna">
            <small>Comunitario:</small>
        </td>
        <td class="secondaColonna">
            <small><asp:Label
                ID="LabelComunitario"
                runat="server">
            </asp:Label></small>
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Recapito telefonico:</small>
        </td>
        <td class="secondaColonna">
            <b>
                <small><asp:Label
                    ID="LabelTelefono"
                    runat="server">
                </asp:Label></small>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <small>Email:</small>
        <%--</td>
        <td class="secondaColonna">--%>
            <%--<b>
                <small>
                    <asp:Label
                        ID="LabelEmail"
                        runat="server">
                    </asp:Label>
                </small>
            </b>--%>
            <asp:TextBox
                ID="TextBoxEmail"
                runat="server"
                Width="200px"
                CssClass="bold">
            </asp:TextBox>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorEmail"
                runat="server"
                ControlToValidate="TextBoxEmail"
                ValidationGroup="salvaEmail"
                ForeColor="Red">
                *
            </asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator
                ID="RegularExpressionValidatorEmail"
                runat="server"
                ControlToValidate="TextBoxEmail"
                ValidationGroup="salvaEmail"
                ForeColor="Red" 
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                *
            </asp:RegularExpressionValidator>
            <%--<br />--%>
            <asp:Button
                ID="ButtonSalvaEmail"
                runat="server"
                Height="20px"
                Width="80px"
                Text="Salva" 
                ValidationGroup="salvaEmail"
                onclick="ButtonSalvaEmail_Click" />
        </td>
        <td rowspan="8">
            <b>
                Turni
            </b>
            <br />
            <small>
            <asp:BulletedList
                ID="BulletedListTurni"
                runat="server">
            </asp:BulletedList>
            </small>
            <br />
            <b>
                Mansioni
            </b>
            <br />
            <small>
            <asp:BulletedList
                ID="BulletedListMansioni"
                runat="server">
            </asp:BulletedList>
            </small>
            <br />
            <b>
                Esperienze
            </b>
            <br />
            <small>
            <asp:BulletedList
                ID="BulletedListEsperienze"
                runat="server">
            </asp:BulletedList>
            </small>
        </td>
        <td rowspan="8">
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Titolo di Studio:</small>
        </td>
        <td class="secondaColonna">
                <small><asp:Label
                    ID="LabelTitoloStudio"
                    runat="server">
                </asp:Label></small>
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Note Studi:</small>
        </td>
        <td class="secondaColonna">
                <small><asp:Label
                    ID="LabelNote"
                    runat="server">
                </asp:Label></small>
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Esperienza CEMI:</small>
        </td>
        <td class="secondaColonna">
                <small><asp:Label
                    ID="LabelEsperienzaCEMI"
                    runat="server">
                </asp:Label></small>
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Residenza:</small>
        </td>
        <td class="secondaColonna">
            <small><asp:Label
                ID="LabelResidenza"
                runat="server">
            </asp:Label></small>
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Domicilio:</small>
        </td>
        <td class="secondaColonna">
                <small><asp:Label
                    ID="LabelDomicilio"
                    runat="server">
                </asp:Label></small>
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Curriculum:</small>
        </td>
        <td class="secondaColonna">
            <asp:ImageButton
                ID="ImageButtonCurriculum"
                runat="server" 
                Enabled="false"
                ImageUrl="../../images/pdf24BN.png" onclick="ImageButtonCurriculum_Click" />
        </td>
    </tr>
    <tr>
        <td class="primaColonna">
            <small>Scheda:</small>
        </td>
        <td class="secondaColonna">
            <asp:ImageButton
                ID="ImageButtonScheda"
                runat="server"
                ImageUrl="../../images/pdf24.png" 
                onclick="ImageButtonScheda_Click" />
        </td>
    </tr>
</table>
<br />
<table class="filledtable">
    <tr>
        <td>
            Colloquio
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            <uc1:RiassuntoColloquio ID="RiassuntoColloquio1" runat="server" />
        </td>
    </tr>
</table>
<br />
<table class="filledtable">
    <tr>
        <td>
            Proposta
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            <uc2:RiassuntoProposta ID="RiassuntoProposta1" runat="server" />
        </td>
    </tr>
</table>