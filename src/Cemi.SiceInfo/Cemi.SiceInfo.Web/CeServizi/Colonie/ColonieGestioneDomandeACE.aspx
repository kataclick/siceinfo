﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieGestioneDomandeACE.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieGestioneDomandeACE" %>

<%@ Register Src="WebControls/ColonieDatiDomanda.ascx" TagName="ColonieDatiDomanda"
    TagPrefix="uc5" %>
<%@ Register Src="WebControls/ColonieRicercaDomandeACEPerConferma.ascx" TagName="ColonieRicercaDomandeACEPerConferma"
    TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/ColonieRicercaDomandeACE.ascx" TagName="ColonieRicercaDomandeACE"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione domande Altre Casse Edili"
        titolo="Colonie" />
    <br />
    <asp:Panel ID="PanelNessunaVacanzaAttiva" runat="server" Width="100%">
        Nessuna vacanza attiva.
    </asp:Panel>
    <asp:Panel ID="PanelRicerca" runat="server" Width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    Cassa Edile
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListCassaEdile" runat="server" Width="300px" AutoPostBack="True"
                        OnSelectedIndexChanged="DropDownListCassaEdile_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridViewSituazione" runat="server" AutoGenerateColumns="False"
                        OnDataBinding="GridViewSituazione_DataBinding" OnRowDataBound="GridViewSituazione_RowDataBound"
                        Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Turno">
                                <ItemTemplate>
                                    <table class="standardTable">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label ID="LabelTurno" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Posti tot.:
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelPostiTotali" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Posti disp.:
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelPostiDisponibili" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Posti occ. tot.:
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelPostiOccupati" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PostiPrenotati" HeaderText="Posti a disposizione (a seguito prenotazione)" />
                            <asp:BoundField DataField="DomandeAccettate" HeaderText="Domande confermate" />
                            <asp:BoundField DataField="AccompagnatoriAccettati" HeaderText="Accompagnatori confermati" />
                            <asp:BoundField DataField="DomandeInCarico" HeaderText="Domande in carico">
                                <ItemStyle ForeColor="Gray" />
                            </asp:BoundField>
                            <asp:BoundField DataField="AccompagnatoriInCarico" HeaderText="Accompagnatori in carico">
                                <ItemStyle ForeColor="Gray" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DomandeRifiutate" HeaderText="Domande rifiutate" />
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun dato estratto
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="standardTable">
                <tr>
                    <td>
                        <uc4:ColonieRicercaDomandeACEPerConferma ID="ColonieRicercaDomandeACEPerConferma1"
                            runat="server"></uc4:ColonieRicercaDomandeACEPerConferma>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc5:ColonieDatiDomanda ID="ColonieDatiDomanda1" runat="server" Visible="false" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

