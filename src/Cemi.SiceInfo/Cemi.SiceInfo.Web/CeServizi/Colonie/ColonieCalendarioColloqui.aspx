﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieCalendarioColloqui.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieCalendarioColloqui" %>

<%@ Register src="../WebControls/MenuColonie.ascx" tagname="MenuColonie" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/VacanzaAttivaRiassunto.ascx" tagname="VacanzaAttivaRiassunto" tagprefix="uc3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Villaggi Vacanza" sottoTitolo="Calendario Colloqui" />
    <br />
    <uc3:VacanzaAttivaRiassunto ID="VacanzaAttivaRiassunto1" runat="server" />
    <br />
    <asp:ImageButton
        ID="ImageButtonPDF"
        runat="server" ImageUrl="../images/pdf24.png" 
        onclick="ImageButtonPDF_Click"
        ToolTip="Esporta il calendario" />
    <telerik:RadScheduler ID="RadSchedulerColloqui" runat="server" 
        AppointmentStyleMode="Default" Culture="it-IT" FirstDayOfWeek="Monday" 
        LastDayOfWeek="Friday" SelectedView="WeekView" Width="100%"
        DataSubjectField="NomeCompleto" DataDescriptionField="ElencoMansioni"
        DataStartField="DataColloquioInizio" 
        DataEndField="DataColloquioFine" DataKeyField="Id" Height="600px" 
        MinutesPerRow="15" OverflowBehavior="Expand" 
        onappointmentdatabound="RadSchedulerColloqui_AppointmentDataBound" 
        Localization-ConfirmDeleteText="Sei sicuro di voler eliminare quest'attività?" 
        Localization-ConfirmDeleteTitle="Conferma cancellazione" 
        Localization-ConfirmCancel="Annulla" Localization-Cancel="Elimina" 
        DayEndTime="19:30:00" DayStartTime="08:00:00" ShowFooter="False" 
        WorkDayEndTime="17:30:00" WorkDayStartTime="08:30:00"
        AllowDelete="False" AllowEdit="False" AllowInsert="False" 
        HoursPanelTimeFormat="HH:mmtt">
        <ExportSettings OpenInNewWindow="true" FileName="CalendarioColloqui">
            <Pdf Author="Cassa Edile Milano, Lodi, Monza e Brianza" Creator="Cassa Edile Milano, Lodi, Monza e Brianza" />
        </ExportSettings>
        <AdvancedForm Modal="true" />
        <Localization Cancel="Elimina" ConfirmDeleteTitle="Conferma cancellazione" 
            ConfirmDeleteText="Sei sicuro di voler eliminare quest&#39;attivit&#224;?" 
            ConfirmCancel="Annulla" ShowMore="altro..."></Localization>
        <TimelineView UserSelectable="false" />
        <WeekView DayEndTime="19:30:00" DayStartTime="08:00:00" />
        <AppointmentTemplate>
            <asp:Panel
                ID="PanelAttivita"
                runat="server">
                <div class="rsAptSubject">
                    <%# Eval("Subject")%>
                </div>
                <div>
                    <%# Eval("Description")%>
                </div>
            </asp:Panel>
        </AppointmentTemplate>
    </telerik:RadScheduler>
</asp:Content>


