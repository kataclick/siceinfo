﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieCalcoloCosti.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieCalcoloCosti" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Calcolo costi - penalità"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxAnno"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="ButtonVisualizza" runat="server" Text="Visualizza turni" ValidationGroup="visualizzazione"
                    OnClick="ButtonVisualizza_Click" />
            </td>
            <td>
                <asp:Button ID="ButtonCalcolaCostiVacanza" runat="server" OnClick="ButtonCalcolaCostiVacanza_Click"
                    Text="Calcola costi vacanza" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="ButtonEsportaCostiVacanza" runat="server" OnClick="ButtonEsportaCostiVacanza_Click"
                    Text="Esporta costi vacanza" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;<asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Elenco turni"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Turno"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListTurno" runat="server" Width="300px" AutoPostBack="True"
                    OnSelectedIndexChanged="DropDownListTurno_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="ButtonCalcola" runat="server" Enabled="False" OnClick="ButtonCalcola_Click"
                    Text="Calcola costi turno sel." />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="ButtonEsporta" runat="server" Enabled="False" OnClick="ButtonEsporta_Click"
                    Text="Esporta costi turno sel." />
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="GridViewCosti" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewCosti_RowDataBound"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="CassaEdile" HeaderText="Cassa edile" />
            <asp:TemplateField HeaderText="Costi">
                <ItemTemplate>
                    <asp:GridView ID="GridViewCosto" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewCosto_RowDataBound"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="TipoCosto" />
                            <asp:TemplateField HeaderText="Numero persone">
                                <ItemTemplate>
                                    <asp:Label ID="LabelNumeroPersone" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Periodo di permanenza">
                                <ItemTemplate>
                                    <asp:Label ID="LabelPeriodoPermanenza" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Totale giorni">
                                <ItemTemplate>
                                    <asp:Label ID="LabelTotaleGiorni" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quota individuale">
                                <ItemTemplate>
                                    <asp:Label ID="LabelQuotaIndividuale" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Totale">
                                <ItemTemplate>
                                    <asp:Label ID="LabelTotale" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun costo trovato<br />
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>
