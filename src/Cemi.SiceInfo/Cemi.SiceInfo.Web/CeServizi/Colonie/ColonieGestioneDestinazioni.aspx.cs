﻿using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieGestioneDestinazioni : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneExport);

            if (!Page.IsPostBack)
            {
                CaricaTipiVacanza();
                CaricaDestinazioni();
            }
        }

        private void CaricaDestinazioni()
        {
            DestinazioneCollection destinazioni = biz.GetDestinazioni(null, null);
            GridViewDestinazioni.DataSource = destinazioni;
            GridViewDestinazioni.DataBind();
        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipiVacanza.DataSource = tipiVacanza;
            DropDownListTipiVacanza.DataTextField = "Descrizione";
            DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipiVacanza.DataBind();

            if (DropDownListTipiVacanza.SelectedIndex != -1)
            {
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);
                CaricaTipiDestinazione(idTipoVacanza);
            }
        }

        private void CaricaTipiDestinazione(int idTipoVacanza)
        {
            TipoDestinazioneCollection tipiDestinazione = biz.GetTipiDestinazione(idTipoVacanza);

            DropDownListTipiDestinazione.DataSource = tipiDestinazione;
            DropDownListTipiDestinazione.DataTextField = "Descrizione";
            DropDownListTipiDestinazione.DataValueField = "IdTipoDestinazione";
            DropDownListTipiDestinazione.DataBind();
        }

        protected void ButtonNuovo_Click(object sender, EventArgs e)
        {
            PanelInserisciModifica.Visible = true;
            Label4.Visible = false;
            CheckBoxAttiva.Visible = false;
            ButtonOperazione.Text = "Inserisci";
        }

        protected void DropDownListTipiVacanza_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListTipiVacanza.SelectedIndex != -1)
            {
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);
                CaricaTipiDestinazione(idTipoVacanza);
            }
        }

        protected void ButtonOperazione_Click(object sender, EventArgs e)
        {
            if (ControlloCampiServer())
            {
                Destinazione destinazione = CreaDestinazione();

                if (biz.InsertUpdateDestinazione(destinazione))
                {
                    AzzeraCampi();
                    CaricaDestinazioni();
                }
                else
                    LabelErrore.Text = "Errore durante l'operazione";
            }
        }

        private void AzzeraCampi()
        {
            PanelInserisciModifica.Visible = false;
            TextBoxLuogo.Text = string.Empty;
            CheckBoxAttiva.Checked = true;
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            if (DropDownListTipiVacanza.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo vacanza" + Environment.NewLine);
            }

            if (DropDownListTipiDestinazione.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo destinazione" + Environment.NewLine);
            }

            if (string.IsNullOrEmpty(TextBoxLuogo.Text))
            {
                res = false;
                errori.Append("Il campo luogo non può essere vuoto" + Environment.NewLine);
            }

            if (!res)
                LabelErrore.Text = errori.ToString();
            return res;
        }

        private Destinazione CreaDestinazione()
        {
            Destinazione destinazione = null;

            int? idDestinazione = null;
            TipoVacanza tipoVacanza = null;
            TipoDestinazione tipoDestinazione = null;
            string luogo = null;
            bool attiva = false;

            if (!string.IsNullOrEmpty(TextBoxIdDestinazione.Text))
                idDestinazione = Int32.Parse(TextBoxIdDestinazione.Text);

            tipoVacanza =
                new TipoVacanza(Int32.Parse(DropDownListTipiVacanza.SelectedItem.Value),
                                DropDownListTipiVacanza.SelectedItem.Text);
            tipoDestinazione =
                new TipoDestinazione(Int32.Parse(DropDownListTipiDestinazione.SelectedItem.Value),
                                     DropDownListTipiDestinazione.SelectedItem.Text, tipoVacanza);
            luogo = TextBoxLuogo.Text;
            attiva = CheckBoxAttiva.Checked;
            destinazione = new Destinazione(idDestinazione, tipoDestinazione, luogo, attiva);

            return destinazione;
        }
    }
}