﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieRespingiRichiesta : System.Web.UI.Page
    {
        private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Int32 idRichiesta = Int32.Parse(Request.QueryString["idRichiesta"]);
                ViewState["IdRichiesta"] = idRichiesta;

                TextBoxNote.Text = Request.QueryString["note"];
            }
        }

        protected void ButtonRespingi_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Int32 idRichiesta = (Int32)ViewState["IdRichiesta"];
                String note = Presenter.NormalizzaCampoTesto(TextBoxNote.Text);

                //bizEF.RespingiRichiesta(idRichiesta, note);
                bizEF.SalvaNote(idRichiesta, note);

                body1.Attributes.Add("onload", "CloseAndRebind();");
            }
        }
    }
}