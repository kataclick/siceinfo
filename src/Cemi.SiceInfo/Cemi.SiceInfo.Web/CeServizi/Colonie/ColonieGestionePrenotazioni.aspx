﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieGestionePrenotazioni.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieGestionePrenotazioni" %>

<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione prenotazioni"
        titolo="Colonie" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Anno"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Formato anno errato"
                    ControlToValidate="TextBoxAnno" ValidationExpression="^\d{4,4}$" ValidationGroup="visualizzazione"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxAnno"
                    ErrorMessage="*"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Tipo vacanza"></asp:Label>
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DropDownListTipiVacanza" runat="server" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Visualizza situazione" ValidationGroup="visualizzazione" />
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="PanelVisualizza" runat="server" Visible="False" Width="100%">
        Visualizza
        <asp:DropDownList ID="DropDownListVisualizza" runat="server" AutoPostBack="True"
            CausesValidation="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"
            ValidationGroup="visualizzazione" Width="300px">
            <asp:ListItem Selected="True" Value="TUTTE">Tutte</asp:ListItem>
            <asp:ListItem Value="ACCETTATE">Accettate</asp:ListItem>
            <asp:ListItem Value="NONACCETTATE">Non accettate</asp:ListItem>
            <asp:ListItem Value="NONVERIFICATE">Da verificare</asp:ListItem>
        </asp:DropDownList>
    </asp:Panel>
    <br />
    <asp:GridView ID="GridViewPrenotazioni" runat="server" AutoGenerateColumns="False"
        OnRowDataBound="GridViewPrenotazioni_RowDataBound" Width="100%" AllowPaging="True"
        OnPageIndexChanging="GridViewPrenotazioni_PageIndexChanging" PageSize="3">
        <Columns>
            <asp:BoundField DataField="DescrizioneTurno" HeaderText="Turno">
                <ItemStyle Width="120px" />
            </asp:BoundField>
            <asp:BoundField DataField="PostiTurno" HeaderText="Posti tot.">
                <ItemStyle Width="70px" />
            </asp:BoundField>
            <asp:BoundField DataField="PostiDisponibili" HeaderText="Posti disp." />
            <asp:TemplateField>
                <ItemTemplate>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:Panel ID="PanelTotali" runat="server" Width="200px" Visible="false">
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                Confermati (totale):
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelPostiConcessi" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Richiesti e ancora da verificare (totale):
                                            </td>
                                            <td>
                                                <asp:Label ID="LabelPostiRichiestiDaVerificare" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:GridView ID="GridViewPerCassa" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewPerCassa_RowDataBound"
                                    Width="100%" DataKeyNames="IdPrenotazione" OnSelectedIndexChanging="GridViewPerCassa_SelectedIndexChanging">
                                    <Columns>
                                        <asp:BoundField DataField="IdCassaEdile" HeaderText="Cassa edile" />
                                        <asp:TemplateField HeaderText="Stato">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posti rich.">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelPostiRichiesti" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Posti conf.">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelPostiConcessi" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="ButtonOperazione" runat="server" CommandName="Select" Text="Operaz." />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        Nessuna prenotazione presente
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <ItemStyle Width="530px" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna prenotazione
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
    <asp:Panel ID="PanelDettagli" runat="server" Width="100%" Visible="false">
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Dettagli prenotazione"></asp:Label><br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="standardTable">
                    <tr>
                        <td>
                            Turno:
                        </td>
                        <td colspan="2">
                            <asp:Label ID="LabelTurno" runat="server" Width="300px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cassa edile:
                        </td>
                        <td colspan="2">
                            <asp:Label ID="LabelCassaEdile" runat="server" Width="300px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Posti richiesti:
                        </td>
                        <td colspan="2">
                            <asp:Label ID="LabelPostiRichiesti" runat="server" Width="300px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Accettata:
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="CheckBoxAccettata" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxAccettata_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Posti concessi:
                        </td>
                        <td>
                            &nbsp;<asp:TextBox ID="TextBoxPostiConcessi" runat="server" Enabled="false" Width="300px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Formato numerico"
                                ValidationExpression="^\d+$" ValidationGroup="modifica" ControlToValidate="TextBoxPostiConcessi"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table class="standardTable">
            <tr>
                <td>
                    <asp:Button ID="ButtonSalva" runat="server" Text="Salva" Height="28px" Width="128px"
                        ValidationGroup="modifica" OnClick="ButtonSalva_Click" />
                    <asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla" Height="28px" Width="128px"
                        OnClick="ButtonAnnulla_Click" CausesValidation="False" />
                    <asp:Label ID="LabelErroreModifica" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

