﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieEsportaDomande : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneExport);

            if (!Page.IsPostBack)
            {
                TextBoxAnno.Text = DateTime.Now.Year.ToString();
                CaricaTipiVacanza();
            }
        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipiVacanza.DataSource = tipiVacanza;
            DropDownListTipiVacanza.DataTextField = "Descrizione";
            DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipiVacanza.DataBind();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            CaricaTurni();

            buttonEsportaPartecipanti.Enabled = true;
            ButtonEsportaNonAccettate.Enabled = true;
            ButtonEsportaAccettate.Enabled = true;
        }

        private void CaricaTurni()
        {
            if (ControlloCampiServer())
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);

                TurnoCollection turni = biz.GetTurni(null, null, null, anno, idTipoVacanza);
                DropDownListTurno.DataSource = turni;
                DropDownListTurno.DataTextField = "DescrizioneTurno";
                DropDownListTurno.DataValueField = "IdCombo";
                DropDownListTurno.DataBind();

                if (DropDownListTurno.SelectedIndex != -1)
                {
                    CaricaDomande(Int32.Parse(DropDownListTurno.SelectedValue));
                    ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                    //ButtonAutobus.Enabled = true;
                    //PanelFiltro.Visible = true;
                }
                else
                {
                    //ButtonAutobus.Enabled = false;
                    //PanelFiltro.Visible = false;
                }
            }
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            int anno;
            if (string.IsNullOrEmpty(TextBoxAnno.Text) || !Int32.TryParse(TextBoxAnno.Text, out anno))
            {
                res = false;
                errori.Append("Anno non valido o non presente" + Environment.NewLine);
            }

            if (DropDownListTipiVacanza.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo vacanza" + Environment.NewLine);
            }

            if (!res)
                LabelErrore.Text = errori.ToString();
            else
                LabelErrore.Text = string.Empty;
            return res;
        }

        protected void DropDownListTurno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListTurno.SelectedIndex != -1)
            {
                GridViewDomande.PageIndex = 0;
                CaricaDomande(Int32.Parse(DropDownListTurno.SelectedValue));
                ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                //ButtonAutobus.Enabled = true;
                //PanelFiltro.Visible = true;
            }

            //PanelDettagli.Visible = false;
            //SvuotaDettagli();
        }

        private void CaricaDomande(int idTurno)
        {
            DomandaCollection domande = biz.GetDomande(null, null, null, idTurno, false);

            GridViewDomande.DataSource = domande;
            GridViewDomande.DataBind();
        }


        protected void ButtonEsportaAccettate_Click(object sender, EventArgs e)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            // Titoli
            stringaCostruzione.Append(
                "Sesso Indirizzo\tNome Indirizzo\tCognome Indirizzo\tIndirizzo\tCAP Indirizzo\tComune Indirizzo\tProvincia Indirizzo\tCognome Partecipante\tNome Partecipante\tTipo Vacanza\tLuogo Vacanza\tData Inizio\tData Fine\tNumero Corredo\n");

            DomandeStatoCollection domande = biz.GetDomandeAccettate((int)ViewState["IdTurno"]);

            if (domande != null)
            {
                foreach (DomandeStato ds in domande)
                {
                    string inidirizzoSesso = string.Empty;
                    string indirizzoCognome = string.Empty;
                    string indirizzoNome = string.Empty;
                    string indirizzoDenominazione = string.Empty;
                    string indirizzoCAP = string.Empty;
                    string indirizzoComune = string.Empty;
                    string indirizzoProvincia = string.Empty;
                    string cognomePartecipante = string.Empty;
                    string nomePartecipante = string.Empty;
                    string tipoVacanza = string.Empty;
                    string luogoVacanza = string.Empty;
                    string dataInizio = string.Empty;
                    string dataFine = string.Empty;
                    string numeroCorredo = string.Empty;

                    if (ds.IndirizzoSesso != null)
                        inidirizzoSesso = ds.IndirizzoSesso;
                    if (ds.IndirizzoCognome != null)
                        indirizzoCognome = ds.IndirizzoCognome;
                    if (ds.IndirizzoNome != null)
                        indirizzoNome = ds.IndirizzoNome;
                    if (ds.IndirizzoDenominazione != null)
                        indirizzoDenominazione = ds.IndirizzoDenominazione;
                    if (ds.IndirizzoCAP != null)
                        indirizzoCAP = ds.IndirizzoCAP;
                    if (ds.IndirizzoComune != null)
                        indirizzoComune = ds.IndirizzoComune;
                    if (ds.IndirizzoProvincia != null)
                        indirizzoProvincia = ds.IndirizzoProvincia;
                    if (ds.CognomePartecipante != null)
                        cognomePartecipante = ds.CognomePartecipante;
                    if (ds.NomePartecipante != null)
                        nomePartecipante = ds.NomePartecipante;
                    if (ds.TipoVacanza != null)
                        tipoVacanza = ds.TipoVacanza;
                    if (ds.LuogoVacanza != null)
                        luogoVacanza = ds.LuogoVacanza;
                    if (ds.DataInizio != null)
                        dataInizio = "\"" + ((DateTime)ds.DataInizio).ToShortDateString() + "\"";
                    if (ds.DataFine != null)
                        dataFine = "\"" + ((DateTime)ds.DataFine).ToShortDateString() + "\"";
                    if (ds.NumeroCorredo != null)
                        numeroCorredo = ds.NumeroCorredo.ToString();

                    stringaCostruzione.Append(inidirizzoSesso + "\t" +
                                              indirizzoCognome + "\t" +
                                              indirizzoNome + "\t" +
                                              indirizzoDenominazione + "\t" +
                                              indirizzoCAP + "\t" +
                                              indirizzoComune + "\t" +
                                              indirizzoProvincia + "\t" +
                                              cognomePartecipante + "\t" +
                                              nomePartecipante + "\t" +
                                              tipoVacanza + "\t" +
                                              luogoVacanza + "\t" +
                                              dataInizio + "\t" +
                                              dataFine + "\t" +
                                              numeroCorredo + "\n");
                }
            }

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=DomandeAccettate.xls");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        protected void ButtonEsportaNonAccettate_Click(object sender, EventArgs e)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            // Titoli
            stringaCostruzione.Append(
                "Sesso Indirizzo\tNome Indirizzo\tCognome Indirizzo\tIndirizzzo\tCAP Indirizzo\tComune Indirizzo\tProvincia Indirizzo\tCausale\n");

            DomandeStatoCollection domande = biz.GetDomandeNonAccettate((int)ViewState["IdTurno"]);

            if (domande != null)
            {
                foreach (DomandeStato ds in domande)
                {
                    string inidirizzoSesso = string.Empty;
                    string indirizzoCognome = string.Empty;
                    string indirizzoNome = string.Empty;
                    string indirizzoDenominazione = string.Empty;
                    string indirizzoCAP = string.Empty;
                    string indirizzoComune = string.Empty;
                    string indirizzoProvincia = string.Empty;
                    string causale = string.Empty;
                    //string cognomePartecipante = string.Empty;
                    //string nomePartecipante = string.Empty;
                    //string tipoVacanza = string.Empty;
                    //string luogoVacanza = string.Empty;
                    //string dataInizio = string.Empty;
                    //string dataFine = string.Empty;
                    //string numeroCorredo = string.Empty;

                    if (ds.IndirizzoSesso != null)
                        inidirizzoSesso = ds.IndirizzoSesso;
                    if (ds.IndirizzoCognome != null)
                        indirizzoCognome = ds.IndirizzoCognome;
                    if (ds.IndirizzoNome != null)
                        indirizzoNome = ds.IndirizzoNome;
                    if (ds.IndirizzoDenominazione != null)
                        indirizzoDenominazione = ds.IndirizzoDenominazione;
                    if (ds.IndirizzoCAP != null)
                        indirizzoCAP = ds.IndirizzoCAP;
                    if (ds.IndirizzoComune != null)
                        indirizzoComune = ds.IndirizzoComune;
                    if (ds.IndirizzoProvincia != null)
                        indirizzoProvincia = ds.IndirizzoProvincia;
                    if (ds.Causale != null)
                        causale = ds.Causale;
                    //if (ds.CognomePartecipante != null)
                    //    cognomePartecipante = ds.CognomePartecipante;
                    //if (ds.NomePartecipante != null)
                    //    nomePartecipante = ds.NomePartecipante;
                    //if (ds.TipoVacanza != null)
                    //    tipoVacanza = ds.TipoVacanza;
                    //if (ds.LuogoVacanza != null)
                    //    luogoVacanza = ds.LuogoVacanza;
                    //if (ds.DataInizio != null)
                    //    dataInizio = "\"" + ((DateTime)ds.DataInizio).ToShortDateString() + "\"";
                    //if (ds.DataFine != null)
                    //    dataFine = "\"" + ((DateTime)ds.DataFine).ToShortDateString() + "\"";
                    //if (ds.NumeroCorredo != null)
                    //    numeroCorredo = ds.NumeroCorredo.ToString();

                    stringaCostruzione.Append(inidirizzoSesso + "\t" +
                                              indirizzoCognome + "\t" +
                                              indirizzoNome + "\t" +
                                              indirizzoDenominazione + "\t" +
                                              indirizzoCAP + "\t" +
                                              indirizzoComune + "\t" +
                                              indirizzoProvincia + "\t" +
                                              causale + "\n");
                    //cognomePartecipante + "\t" +
                    //nomePartecipante + "\t" +
                    //tipoVacanza + "\t" +
                    //luogoVacanza + "\t" +
                    //dataInizio + "\t" +
                    //dataFine + "\t" +
                    //numeroCorredo + "\n");
                }
            }

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=DomandeNonAccettate.xls");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        protected void ButtonEsportaSchedaPartecipanti_Click(object sender, EventArgs e)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            // Titoli
            string s = "Tipo Vacanza";
            s += "\tAnno Vacanza";
            s += "\tLuogo Vacanza";
            s += "\tTurno";
            s += "\tNumero Corredo";
            s += "\tCognome Partecipante";
            s += "\tNome Partecipante";
            s += "\tIndirizzo Partecipante";
            s += "\tCAP Partecipante";
            s += "\tComune Partecipante";
            s += "\tProvincia Partecipante";
            s += "\tSesso Partecipante";
            s += "\tData Nascita Partecipante";
            s += "\tNr Tessera Sanitaria Partecipante";
            s += "\tIntolleranze Alimentari Partecipante";
            s += "\tPortatore Handicap Partecipante";
            s += "\tAllergie Partecipante";
            s += "\tTerapie In Corso Partecipante";
            s += "\tDieta Partecipante";
            s += "\tProtesi Ausili Partecipante";
            s += "\tCognome Genitore";
            s += "\tNome Genitore";
            s += "\tIndirizzo Genitore";
            s += "\tCAP Genitore";
            s += "\tComune Genitore";
            s += "\tProvincia Genitore";
            s += "\tTelefono Abitazione Genitore";
            s += "\tTelefono Cellulare Genitore";
            s += "\tProvenienza Genitore";
            s += "\tCognome Parente Partecipante";
            s += "\tNome Parente Partecipante";
            s += "\tNumero Corredo Parente Partecipante";
            s += "\tCognome Accompagnatore";
            s += "\tNome Accompagnatore";
            s += "\tSesso Accompagnatore";
            s += "\tData Nascita Accompagnatore";
            s += "\tTaglia";
            s += "\n";
            stringaCostruzione.Append(s);

            SchedaBambinoCollection schedaBambino = biz.GetSchedaBambino(null, (int)ViewState["IdTurno"]);

            if (schedaBambino != null)
            {
                foreach (SchedaBambino sb in schedaBambino)
                {
                    string tipoVacanza = string.Empty;
                    string annoVacanza = string.Empty;
                    string luogoVacanza = string.Empty;
                    string turno = string.Empty;
                    string numeroCorredo = string.Empty;
                    string cognomePartecipante = string.Empty;
                    string nomePartecipante = string.Empty;
                    string indirizzoPartecipante = string.Empty;
                    string capPartecipante = string.Empty;
                    string comunePartecipante = string.Empty;
                    string provinciaPartecipante = string.Empty;
                    string sessoPartecipante = string.Empty;
                    string datanascitaPartecipante = string.Empty;
                    string nrTesseraSanitariaPartecipante = string.Empty;
                    string intolleranzeAlimentariPartecipante = string.Empty;
                    string portatoreHandicapPartecipante = string.Empty;
                    string allergiePartecipante = string.Empty;
                    string dietaPartecipante = string.Empty;
                    string protesiPartecipante = string.Empty;
                    string terapiePartecipante = string.Empty;
                    string cognomeGenitore = string.Empty;
                    string nomeGenitore = string.Empty;
                    string indirizzoGenitore = string.Empty;
                    string capGenitore = string.Empty;
                    string comuneGenitore = string.Empty;
                    string provinciaGenitore = string.Empty;
                    string telefonoAbitazioneGenitore = string.Empty;
                    string telefonoCellulareGenitore = string.Empty;
                    string provenienzaGenitore = string.Empty;
                    string cognomeParentePartecipante = string.Empty;
                    string nomeParentePartecipante = string.Empty;
                    string numeroCorredoParentePartecipante = string.Empty;
                    string cognomeAccompagnatore = string.Empty;
                    string nomeAccompagnatore = string.Empty;
                    string sessoAccompagnatore = string.Empty;
                    string dataNascitaAccompagnatore = string.Empty;
                    string taglia = string.Empty;

                    if (sb.TipoVacanza != null)
                        tipoVacanza = Presenter.StampaStringaExcel(sb.TipoVacanza);
                    //if (sb.Anno != null)
                    annoVacanza = sb.Anno.ToString();
                    if (sb.TipoDestinazione != null)
                        luogoVacanza = Presenter.StampaStringaExcel(sb.TipoDestinazione);
                    //if (sb.ProgressivoTurno != null)
                    turno = sb.ProgressivoTurno.ToString();
                    //if (sb.NumeroCorredo != null)
                    numeroCorredo = sb.NumeroCorredo.ToString();
                    if (sb.Partecipante.Cognome != null)
                        cognomePartecipante = sb.Partecipante.Cognome;
                    if (sb.Partecipante.Nome != null)
                        nomePartecipante = sb.Partecipante.Nome;
                    if (sb.Lavoratore.IndirizzoDenominazione != null)
                    {
                        indirizzoPartecipante = Presenter.StampaStringaExcel(sb.Lavoratore.IndirizzoDenominazione);
                        indirizzoGenitore = Presenter.StampaStringaExcel(sb.Lavoratore.IndirizzoDenominazione);
                    }
                    if (sb.Lavoratore.IndirizzoCAP != null)
                    {
                        capPartecipante = sb.Lavoratore.IndirizzoCAP;
                        capGenitore = sb.Lavoratore.IndirizzoCAP;
                    }
                    if (sb.Lavoratore.IndirizzoComune != null)
                    {
                        comunePartecipante = Presenter.StampaStringaExcel(sb.Lavoratore.IndirizzoComune);
                        comuneGenitore = Presenter.StampaStringaExcel(sb.Lavoratore.IndirizzoComune);
                    }
                    if (sb.Lavoratore.IndirizzoProvincia != null)
                    {
                        provinciaPartecipante = sb.Lavoratore.IndirizzoProvincia;
                        provinciaGenitore = sb.Lavoratore.IndirizzoProvincia;
                    }
                    if (sb.Partecipante.Sesso != null)
                        sessoPartecipante = sb.Partecipante.Sesso;
                    if (sb.Partecipante.DataNascita != null)
                        datanascitaPartecipante = "\"" + ((DateTime)sb.Partecipante.DataNascita).ToShortDateString() + "\"";
                    if (sb.Partecipante.NumeroTesseraSanitaria != null)
                        nrTesseraSanitariaPartecipante = Presenter.StampaStringaExcel(sb.Partecipante.NumeroTesseraSanitaria);

                    if (sb.Partecipante.IntolleranzeAlimentari)
                        intolleranzeAlimentariPartecipante = "SI";
                    else
                        intolleranzeAlimentariPartecipante = "NO";

                    if (sb.Partecipante.PortatoreHandicap)
                        portatoreHandicapPartecipante = "SI";
                    else
                        portatoreHandicapPartecipante = "NO";

                    if (sb.Partecipante.Dieta != null)
                        dietaPartecipante = Presenter.StampaStringaExcel(sb.Partecipante.Dieta);
                    if (sb.Partecipante.Allergie != null)
                        allergiePartecipante = Presenter.StampaStringaExcel(sb.Partecipante.Allergie);
                    if (sb.Partecipante.TerapieInCorso != null)
                        terapiePartecipante = Presenter.StampaStringaExcel(sb.Partecipante.TerapieInCorso);
                    if (sb.Partecipante.ProtesiAusili != null)
                        protesiPartecipante = Presenter.StampaStringaExcel(sb.Partecipante.ProtesiAusili);


                    cognomeGenitore = sb.Lavoratore.Cognome;
                    nomeGenitore = sb.Lavoratore.Nome;
                    telefonoAbitazioneGenitore = Presenter.StampaStringaExcel(sb.Lavoratore.Telefono);
                    telefonoCellulareGenitore = Presenter.StampaStringaExcel(sb.Lavoratore.Cellulare);
                    provenienzaGenitore = sb.Lavoratore.IdCassaEdile;

                    if (sb.CognomeParente != null)
                        cognomeParentePartecipante = sb.CognomeParente;
                    if (sb.NomeParente != null)
                        nomeParentePartecipante = sb.NomeParente;
                    if (sb.NumeroCorredoParente != null)
                        numeroCorredoParentePartecipante = sb.NumeroCorredoParente.ToString();

                    if (sb.Accompagnatore != null)
                    {
                        cognomeAccompagnatore = sb.Accompagnatore.Cognome;
                        nomeAccompagnatore = sb.Accompagnatore.Nome;
                        dataNascitaAccompagnatore = sb.Accompagnatore.DataNascita.ToShortDateString();
                        sessoAccompagnatore = sb.Accompagnatore.Sesso.ToString();
                    }

                    taglia = sb.Taglia;

                    stringaCostruzione.Append(tipoVacanza + "\t" +
                                              annoVacanza + "\t" +
                                              luogoVacanza + "\t" +
                                              turno + "\t" +
                                              numeroCorredo + "\t" +
                                              cognomePartecipante + "\t" +
                                              nomePartecipante + "\t" +
                                              indirizzoPartecipante + "\t" +
                                              capPartecipante + "\t" +
                                              comunePartecipante + "\t" +
                                              provinciaPartecipante + "\t" +
                                              sessoPartecipante + "\t" +
                                              datanascitaPartecipante + "\t" +
                                              nrTesseraSanitariaPartecipante + "\t" +
                                              intolleranzeAlimentariPartecipante + "\t" +
                                              portatoreHandicapPartecipante + "\t" +
                                              allergiePartecipante + "\t" +
                                              terapiePartecipante + "\t" +
                                              dietaPartecipante + "\t" +
                                              protesiPartecipante + "\t" +
                                              cognomeGenitore + "\t" +
                                              nomeGenitore + "\t" +
                                              indirizzoGenitore + "\t" +
                                              capGenitore + "\t" +
                                              comuneGenitore + "\t" +
                                              provinciaGenitore + "\t" +
                                              telefonoAbitazioneGenitore + "\t" +
                                              telefonoCellulareGenitore + "\t" +
                                              provenienzaGenitore + "\t" +
                                              cognomeParentePartecipante + "\t" +
                                              nomeParentePartecipante + "\t" +
                                              numeroCorredoParentePartecipante + "\t" +
                                              cognomeAccompagnatore + "\t" +
                                              nomeAccompagnatore + "\t" +
                                              sessoAccompagnatore + "\t" +
                                              dataNascitaAccompagnatore + "\t" +
                                              taglia + "\n");
                }
            }

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=SchedaPartecipanti.xls");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int idTurno = (int)ViewState["IdTurno"];

            GridViewDomande.PageIndex = e.NewPageIndex;

            CaricaDomande(idTurno);
        }
    }
}