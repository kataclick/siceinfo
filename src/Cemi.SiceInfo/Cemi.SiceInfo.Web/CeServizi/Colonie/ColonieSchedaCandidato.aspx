﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieSchedaCandidato.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieSchedaCandidato" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%--<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Villaggi Vacanze" sottoTitolo="Scheda Candidato" />
    <br />
    <rsweb:ReportViewer ID="ReportViewerSchedaCandidato" runat="server" ProcessingMode="Remote"
        width="100%">
    </rsweb:ReportViewer>
</asp:Content>