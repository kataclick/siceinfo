﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Domain;
using System.Text;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Colonie;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieEstrazioneInaz : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();
        private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

        private const String carattereSeparatore = ";";

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieEstrazioneINAZ);
            #endregion

            if (!Page.IsPostBack)
            {
                CaricaMansioni();
            }
        }

        private void CaricaMansioni()
        {
            List<ColoniePersonaleMansione> mansioni = bizEF.GetPersonaleMansioni();

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                RadComboBoxMansione,
                mansioni,
                "Descrizione",
                "Id");
        }

        protected void ButtonEstrazioneInaz_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                EstrazioneFilter filtro = CreaFiltro();
                List<ColoniePersonaleRichiesta> richieste = bizEF.GetRichiestePerEstrazione(filtro);

                GeneraFile(richieste);
            }
        }

        private EstrazioneFilter CreaFiltro()
        {
            EstrazioneFilter filtro = new EstrazioneFilter();

            filtro.IdVacanza = biz.GetVacanzaAttiva().IdVacanza.Value;
            filtro.DataAssunzioneDal = RadDatePickerDal.SelectedDate;
            filtro.DataAssunzioneAl = RadDatePickerAl.SelectedDate;

            if (!String.IsNullOrEmpty(RadComboBoxMansione.SelectedValue))
            {
                filtro.IdMansione = Int32.Parse(RadComboBoxMansione.SelectedValue);
            }

            return filtro;
        }

        private void GeneraFile(List<ColoniePersonaleRichiesta> richieste)
        {
            StringBuilder sbFile = new StringBuilder();

            foreach (ColoniePersonaleRichiesta richiesta in richieste)
            {
                sbFile.AppendLine(
                    String.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}{0}{13}{0}{14}{0}{15}{0}{16}{0}{17}{0}{18}{0}{19}{0}{20}{0}{21}{0}{22}{0}{23}{0}{24}{0}",
                        carattereSeparatore,
                        richiesta.CodiceFiscale,
                        Presenter.StampaStringaExcel(richiesta.Cognome),
                        Presenter.StampaStringaExcel(richiesta.Nome),
                        richiesta.Sesso,
                        richiesta.DataNascita.ToString("dd/MM/yyyy"),
                        richiesta.LuogoNascita,
                        richiesta.ProvinciaNascita,
                        richiesta.Comunitario ? "C" : "E",
                        richiesta.Telefono,
                        richiesta.Email,
                        Presenter.StampaStringaExcel(richiesta.ResidenzaIndirizzo),
                        Presenter.StampaStringaExcel(richiesta.ResidenzaCivico),
                        richiesta.ResidenzaComune,
                        richiesta.ResidenzaProvincia,
                        !String.IsNullOrEmpty(richiesta.DomicilioIndirizzo) ? Presenter.StampaStringaExcel(richiesta.DomicilioIndirizzo) : String.Empty,
                        !String.IsNullOrEmpty(richiesta.DomicilioCivico) ? Presenter.StampaStringaExcel(richiesta.DomicilioCivico) : String.Empty,
                        richiesta.DomicilioComune != null ? richiesta.DomicilioComune : String.Empty,
                        !String.IsNullOrEmpty(richiesta.DomicilioProvincia) ? richiesta.DomicilioProvincia : String.Empty,
                        richiesta.ColoniePersonaleTitoloStudio.Descrizione,
                        Presenter.StampaStringaExcel(richiesta.Note),
                        richiesta.ColoniePersonaleProposte.Single().DataInizioRapporto.ToString("dd/MM/yyyy"),
                        richiesta.ColoniePersonaleProposte.Single().DataFineRapporto.ToString("dd/MM/yyyy"),
                        richiesta.ColoniePersonaleProposte.Single().Mansione.Descrizione,
                        richiesta.ColoniePersonaleProposte.Single().Retribuzione.Livello)
                    );
            }

            Response.AddHeader("content-disposition",
                                   String.Format("attachment; filename=EstrazioneINAZ.csv"));
            Response.ContentType = "text/csv";
            Response.Write(sbFile.ToString());
            Response.Flush();
            Response.End();
            //LabelRisultato.Text = sbFile.ToString();
        }

        protected void CustomValidatorDalAl_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadDatePickerDal.SelectedDate.HasValue
                && RadDatePickerAl.SelectedDate.HasValue
                && RadDatePickerAl.SelectedDate.Value < RadDatePickerDal.SelectedDate.Value)
            {
                args.IsValid = false;
            }
        }
    }
}