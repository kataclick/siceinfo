﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Colonie;
using CassaEdile = TBridge.Cemi.Type.Entities.GestioneUtenti.CassaEdile;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieIscrizioneAltreCasseEdili : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();
        private string idCassaEdile;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieInserimentoDomandeACE);

            if (GestioneUtentiBiz.IsCassaEdile())
            {
                //TBridge.Cemi.GestioneUtenti.Type.Entities.CassaEdile cassaEdile =
                //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.CassaEdile)HttpContext.Current.User.Identity).
                //            Entity;
                CassaEdile cassaEdile =
                    (CassaEdile)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                idCassaEdile = cassaEdile.IdCassaEdile;
            }

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDomanda"] != null)
                {
                    int idDomanda = (int)Context.Items["IdDomanda"];

                    DomandaACEFilter filtro = new DomandaACEFilter();
                    filtro.IdDomanda = idDomanda;
                    DomandaACE domanda = biz.GetDomandeACE(filtro)[0];

                    ColonieDatiDomanda1.CaricaDomanda(domanda, false, idCassaEdile);
                }
                else
                {
                    Vacanza vacanzaAttiva = biz.GetVacanzaAttiva();

                    if (vacanzaAttiva != null)
                    {
                        if (biz.CassaEdileAbilitata(vacanzaAttiva.IdVacanza.Value, idCassaEdile))
                        {
                            DateTime ora = DateTime.Now;

                            if (vacanzaAttiva.DataInizioDomandeACE.HasValue && vacanzaAttiva.DataFineDomandeACE.HasValue &&
                                vacanzaAttiva.DataInizioDomandeACE.Value <= ora &&
                                ora <= vacanzaAttiva.DataFineDomandeACE.Value.AddDays(1))
                            {
                                LabelMessaggio.Visible = false;
                                ColonieDatiDomanda1.Visible = true;
                                ColonieDatiDomanda1.ImpostaVacanza(vacanzaAttiva);
                            }
                            else
                            {
                                LabelMessaggio.Visible = true;
                                ColonieDatiDomanda1.Visible = false;

                                if (vacanzaAttiva.DataInizioDomandeACE.HasValue && vacanzaAttiva.DataFineDomandeACE.HasValue)
                                {
                                    if (vacanzaAttiva.DataInizioDomandeACE > ora)
                                        LabelMessaggio.Text =
                                            String.Format("Sarà possibile inserire le domande a partire dal {0}",
                                                          vacanzaAttiva.DataInizioDomandeACE.Value.ToShortDateString());
                                    else
                                        LabelMessaggio.Text =
                                            String.Format("Il periodo per l'inserimento delle domande è scaduto il {0}",
                                                          vacanzaAttiva.DataFineDomandeACE.Value.ToShortDateString());
                                }
                                else
                                    LabelMessaggio.Text = "Non è ancora possibile inserire le domande.";
                            }
                        }
                        else
                        {
                            LabelMessaggio.Visible = true;
                            ColonieDatiDomanda1.Visible = false;
                            LabelMessaggio.Text =
                                "La cassa edile non è abilitata ad inserire le domande. Contattare Cassa Edile di Milano.";
                        }
                    }
                    else
                    {
                        LabelMessaggio.Visible = true;
                        ColonieDatiDomanda1.Visible = false;
                        LabelMessaggio.Text = "Non esistono vacanze attive, non è possibile inserire le domanda.";
                    }
                }
            }
        }
    }
}