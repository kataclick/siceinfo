﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Business.EmailClient;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieGestioneComunicazioniACE : System.Web.UI.Page
    {
        private ColonieBusiness biz = new ColonieBusiness();
        private Vacanza vacanzaAttiva = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieComunicazioniACE);

            #endregion

            vacanzaAttiva = biz.GetVacanzaAttiva();
            VacanzaAttiva();

            #region Click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonAperturaDomande, null) + ";");
            sb.Append("return true;");
            ButtonAperturaDomande.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAperturaDomande);

            StringBuilder sb1 = new StringBuilder();
            sb1.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb1.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb1.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb1.Append("this.value = 'Attendere...';");
            sb1.Append("this.disabled = true;");
            sb1.Append(Page.ClientScript.GetPostBackEventReference(ButtonAperturaPrenotazioni, null) + ";");
            sb1.Append("return true;");
            ButtonAperturaPrenotazioni.Attributes.Add("onclick", sb1.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAperturaPrenotazioni);

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb2.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb2.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb2.Append("this.value = 'Attendere...';");
            sb2.Append("this.disabled = true;");
            sb2.Append(Page.ClientScript.GetPostBackEventReference(ButtonChiusuraDomande, null) + ";");
            sb2.Append("return true;");
            ButtonChiusuraDomande.Attributes.Add("onclick", sb2.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonChiusuraDomande);

            #endregion
        }

        private void VacanzaAttiva()
        {
            bool bVacanzaAttiva = false;
            if (vacanzaAttiva != null)
            {
                bVacanzaAttiva = true;
                LabelAnno.Text = vacanzaAttiva.Anno.ToString();
                LabelTipoVacanza.Text = vacanzaAttiva.TipoVacanza.Descrizione;
            }

            PanelMail.Visible = bVacanzaAttiva;
            PanelVacanzaNonAttiva.Visible = !bVacanzaAttiva;
        }

        protected void ButtonAperturaPrenotazioni_Click(object sender, EventArgs e)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();
            email.Mittente = new EmailAddress();

            email.Mittente.Nome = "Cassa Edile di Milano";
            email.Mittente.Indirizzo = "villaggiovacanze@cassaedilemilano.it";
            email.Oggetto = "Villaggi vacanza - Fase di prenotazione";

            // HTML
            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("Si informa che a partire dal giorno ");
            if (vacanzaAttiva.DataInizioPrenotazioni.HasValue)
            {
                sbTestoHtml.Append(vacanzaAttiva.DataInizioPrenotazioni.Value.ToShortDateString());
            }
            sbTestoHtml.Append(" è possibile indicare il numero preventivato di partecipanti tramite ");
            sbTestoHtml.Append("il comando \"<b>Prenotazioni</b>\" della funzione informatica \"<b>Colonie</b>\" ");
            sbTestoHtml.Append(
                "disponibile nell'area <b>Servizi</b> del sito <a href='http://www.cassaedilemilano.it'>www.cassaedilemilano.it</a>, ");
            sbTestoHtml.Append(
                "previa autenticazione mediante username e password scelte dall'utente all'atto della registrazione.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Si avvisa che tale informazione potrà essere comunicata <b>entro e non oltre il giorno ");
            if (vacanzaAttiva.DataFinePrenotazioni.HasValue)
            {
                sbTestoHtml.Append(vacanzaAttiva.DataFinePrenotazioni.Value.ToShortDateString());
            }
            sbTestoHtml.Append("</b>.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cassa Edile di Milano si riserverà di far sapere se il numero di adesioni comunicato ");
            sbTestoHtml.Append("è accoglibile in base al numero complessivo di richieste pervenute ed alla disponibilità ");
            sbTestoHtml.Append("della struttura.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cordiali saluti.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Ufficio Prestazioni/Liquidazioni");
            sbTestoHtml.Append("Cassa Edile Milano");

            // PLAIN
            StringBuilder sbTestoPlain = new StringBuilder();
            sbTestoPlain.Append("Si informa che a partire dal giorno <b>");
            if (vacanzaAttiva.DataInizioPrenotazioni.HasValue)
            {
                sbTestoPlain.Append(vacanzaAttiva.DataInizioPrenotazioni.Value.ToShortDateString());
            }
            sbTestoPlain.Append("</b> è possibile indicare il numero preventivato di partecipanti tramite ");
            sbTestoPlain.Append("il comando \"Prenotazioni\" della funzione informatica \"Colonie\" ");
            sbTestoPlain.Append("disponibile nell'area Servizi del sito www.cassaedilemilano.it, ");
            sbTestoPlain.Append(
                "previa autenticazione mediante username e password scelte dall'utente all'atto della registrazione.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Si avvisa che tale informazione potrà essere comunicata entro e non oltre il giorno ");
            if (vacanzaAttiva.DataFinePrenotazioni.HasValue)
            {
                sbTestoPlain.Append(vacanzaAttiva.DataFinePrenotazioni.Value.ToShortDateString());
            }
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Cassa Edile di Milano si riserverà di far sapere se il numero di adesioni comunicato ");
            sbTestoPlain.Append("è accoglibile in base al numero complessivo di richieste pervenute ed alla disponibilità ");
            sbTestoPlain.Append("della struttura.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Cordiali saluti.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Ufficio Prestazioni/Liquidazioni");
            sbTestoPlain.Append("Cassa Edile Milano");

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            //email.DestinatariBCC = new List<EmailAddress>(1);
            //EmailAddress destNascosto = new EmailAddress();
            //destNascosto.Nome = "Giuliano Trifoglio";
            //destNascosto.Indirizzo = "giuliano.trifoglio@itsinfinity.com";
            //email.DestinatariBCC.Add(destNascosto);

            EmailInfoService service = new EmailInfoService();
            service.Credentials = new NetworkCredential(emailUserName, emailPassword);

            CassaEdileCollection casseEdili = biz.GetAbilitazioniVacanza(vacanzaAttiva.IdVacanza.Value);
            foreach (CassaEdile cassaEdile in casseEdili)
            {
                if (!String.IsNullOrEmpty(cassaEdile.Email))
                {
                    email.Destinatari = new List<EmailAddress>(1);
                    EmailAddress dest1 = new EmailAddress();
                    dest1.Nome = cassaEdile.Descrizione;
                    dest1.Indirizzo = cassaEdile.Email;

                    // Per Prove
                    // dest1.Indirizzo = "alessio.mura@itsinfinity.com";

                    email.Destinatari.Add(dest1);

                    service.InviaEmail((EmailMessageSerializzabile)email);
                }
            }

            LabelMessaggio.Visible = true;
        }

        protected void ButtonAperturaDomande_Click(object sender, EventArgs e)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();
            email.Mittente = new EmailAddress();

            email.Mittente.Nome = "Cassa Edile di Milano";
            email.Mittente.Indirizzo = "villaggiovacanze@cassaedilemilano.it";
            email.Oggetto = "Villaggi vacanza - Fase di inserimento delle domande";

            // HTML
            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("Si comunica che a partire dalla data <b>");
            if (vacanzaAttiva.DataInizioDomandeACE.HasValue)
            {
                sbTestoHtml.Append(vacanzaAttiva.DataInizioDomandeACE.Value.ToShortDateString());
            }
            sbTestoHtml.Append("</b> è possibile visualizzare le prenotazioni confermate e inserire le domande ");
            sbTestoHtml.Append(
                "dei partecipanti aventi diritto utilizzando l'apposito comando \"<b>Inserimento domande ACE</b>\" ");
            sbTestoHtml.Append("della funzione informatica \"<b>Colonie</b>\".");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Sarà possibile effettuare eventuali variazioni anagrafiche inerenti il lavoratore e ");
            sbTestoHtml.Append("il/la figlio/a finchè la domanda non verrà confermata. A quel punto verrà generato ");
            sbTestoHtml.Append("un numero di corredo per ciascun partecipante che sarà visualizzabile da parte ");
            sbTestoHtml.Append("dell'utente richiedente.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("In caso di superamento del numero di posti previsto, verrà inviata una segnalazione ");
            sbTestoHtml.Append("relativa al numero di domande che potrebbe non essere accolto.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Le domande potranno essere inserite <b>entro e non oltre il ");
            if (vacanzaAttiva.DataFineDomandeACE.HasValue)
            {
                sbTestoHtml.Append(vacanzaAttiva.DataFineDomandeACE.Value.ToShortDateString());
            }
            sbTestoHtml.Append("</b>");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Seguirà una comunicazione relativa all'ammontare della quota individuale giornaliera ");
            sbTestoHtml.Append("di adesione");

            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cordiali saluti.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Ufficio Prestazioni/Liquidazioni");
            sbTestoHtml.Append("Cassa Edile Milano");

            // PLAIN
            StringBuilder sbTestoPlain = new StringBuilder();
            sbTestoPlain.Append("Si comunica che a partire dalla data ");
            if (vacanzaAttiva.DataInizioDomandeACE.HasValue)
            {
                sbTestoPlain.Append(vacanzaAttiva.DataInizioDomandeACE.Value.ToShortDateString());
            }
            sbTestoPlain.Append(" è possibile visualizzare le prenotazioni confermate e inserire le domande ");
            sbTestoPlain.Append(
                "dei partecipanti aventi diritto utilizzando l'apposito comando \"Inserimento domande ACE\" ");
            sbTestoPlain.Append("della funzione informatica \"Colonie\".");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Sarà possibile effettuare eventuali variazioni anagrafiche inerenti il lavoratore e ");
            sbTestoPlain.Append("il/la figlio/a finchè la domanda non verrà confermata. A quel punto verrà generato ");
            sbTestoPlain.Append("un numero di corredo per ciascun partecipante che sarà visualizzabile da parte ");
            sbTestoPlain.Append("dell'utente richiedente.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("In caso di superamento del numero di posti previsto, verrà inviata una segnalazione ");
            sbTestoPlain.Append("relativa al numero di domande che potrebbe non essere accolto.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Le domande potranno essere inserite entro e non oltre il ");
            if (vacanzaAttiva.DataFineDomandeACE.HasValue)
            {
                sbTestoPlain.Append(vacanzaAttiva.DataFineDomandeACE.Value.ToShortDateString());
            }
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Seguirà una comunicazione relativa all'ammontare della quota individuale giornaliera ");
            sbTestoPlain.Append("di adesione");

            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Cordiali saluti.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Ufficio Prestazioni/Liquidazioni");
            sbTestoPlain.Append("Cassa Edile Milano");

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            //email.DestinatariBCC = new List<EmailAddress>(1);
            //EmailAddress destNascosto = new EmailAddress();
            //destNascosto.Nome = "Giuliano Trifoglio";
            //destNascosto.Indirizzo = "giuliano.trifoglio@itsinfinity.com";
            //email.DestinatariBCC.Add(destNascosto);

            EmailInfoService service = new EmailInfoService();
            service.Credentials = new NetworkCredential(emailUserName, emailPassword);

            CassaEdileCollection casseEdili = biz.GetAbilitazioniVacanza(vacanzaAttiva.IdVacanza.Value);
            foreach (CassaEdile cassaEdile in casseEdili)
            {
                if (!String.IsNullOrEmpty(cassaEdile.Email))
                {
                    email.Destinatari = new List<EmailAddress>(1);
                    EmailAddress dest1 = new EmailAddress();
                    dest1.Nome = cassaEdile.Descrizione;
                    dest1.Indirizzo = cassaEdile.Email;

                    // Per Prove
                    //dest1.Indirizzo = "alessio.mura@itsinfinity.com";

                    email.Destinatari.Add(dest1);

                    service.InviaEmail((EmailMessageSerializzabile)email);
                }
            }

            LabelMessaggio.Visible = true;
        }

        protected void ButtonChiusuraDomande_Click(object sender, EventArgs e)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            EmailMessageSerializzabile email = new EmailMessageSerializzabile();
            email.Mittente = new EmailAddress();

            email.Mittente.Nome = "Cassa Edile di Milano";
            email.Mittente.Indirizzo = "villaggiovacanze@cassaedilemilano.it";
            email.Oggetto = "Villaggi vacanza - Chiusura fase di inserimento delle domande";

            // HTML
            StringBuilder sbTestoHtml = new StringBuilder();
            sbTestoHtml.Append("Si informa che a partire dal giorno ");
            if (vacanzaAttiva.DataFineDomandeACE.HasValue)
            {
                sbTestoHtml.Append(vacanzaAttiva.DataFineDomandeACE.Value.ToShortDateString());
            }
            sbTestoHtml.Append(" non sarà più possibile inserire le domande di iscrizione. ");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Si ricorda che tramite la funzione \"<b>Gestione domande ACE");
            sbTestoHtml.Append("</b>\" è possibile visualizzare in tempo reale lo stato delle ");
            sbTestoHtml.Append("domande inserite: confermate, da valutare, rifiutate.");

            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Cordiali saluti.");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("<br />");
            sbTestoHtml.Append("Ufficio Prestazioni/Liquidazioni");
            sbTestoHtml.Append("Cassa Edile Milano");

            // PLAIN
            StringBuilder sbTestoPlain = new StringBuilder();
            sbTestoPlain.Append("Si informa che a partire dal giorno ");
            if (vacanzaAttiva.DataFineDomandeACE.HasValue)
            {
                sbTestoPlain.Append(vacanzaAttiva.DataFineDomandeACE.Value.ToShortDateString());
            }
            sbTestoPlain.Append(" non sarà più possibile inserire le domande di iscrizione. ");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Si ricorda che tramite la funzione \"Gestione domande ACE");
            sbTestoPlain.Append("\" è possibile visualizzare in tempo reale lo stato delle ");
            sbTestoPlain.Append("domande inserite: confermate, da valutare, rifiutate.");

            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Cordiali saluti.");
            sbTestoPlain.AppendLine();
            sbTestoPlain.AppendLine();
            sbTestoPlain.Append("Ufficio Prestazioni/Liquidazioni");
            sbTestoPlain.Append("Cassa Edile Milano");

            email.BodyPlain = sbTestoPlain.ToString();
            email.BodyHTML = sbTestoHtml.ToString();
            email.Priorita = MailPriority.Normal;
            email.DataSchedulata = DateTime.Now;

            //email.DestinatariBCC = new List<EmailAddress>(1);
            //EmailAddress destNascosto = new EmailAddress();
            //destNascosto.Nome = "Giuliano Trifoglio";
            //destNascosto.Indirizzo = "giuliano.trifoglio@itsinfinity.com";
            //email.DestinatariBCC.Add(destNascosto);

            EmailInfoService service = new EmailInfoService();
            service.Credentials = new NetworkCredential(emailUserName, emailPassword);

            CassaEdileCollection casseEdili = biz.GetAbilitazioniVacanza(vacanzaAttiva.IdVacanza.Value);
            foreach (CassaEdile cassaEdile in casseEdili)
            {
                if (!String.IsNullOrEmpty(cassaEdile.Email))
                {
                    email.Destinatari = new List<EmailAddress>(1);
                    EmailAddress dest1 = new EmailAddress();
                    dest1.Nome = cassaEdile.Descrizione;
                    dest1.Indirizzo = cassaEdile.Email;

                    // Per Prove
                    // dest1.Indirizzo = "alessio.mura@itsinfinity.com";

                    email.Destinatari.Add(dest1);

                    service.InviaEmail((EmailMessageSerializzabile)email);
                }
            }

            LabelMessaggio.Visible = true;
        }
    }
}