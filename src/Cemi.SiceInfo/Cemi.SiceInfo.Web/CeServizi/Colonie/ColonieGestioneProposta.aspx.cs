﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Domain;
using System.Text;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieGestioneProposta : System.Web.UI.Page
    {
        private readonly ColonieBusinessEF bizEF = new ColonieBusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale);
            #endregion

            #region Click multipli
            StringBuilder sbProp = new StringBuilder();
            sbProp.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sbProp.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sbProp.Append(
                "if (Page_ClientValidate('proposta') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sbProp.Append("this.value = 'Attendere...';");
            sbProp.Append("this.disabled = true;");
            sbProp.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null));
            sbProp.Append(";");
            sbProp.Append("return true;");
            ButtonConferma.Attributes.Add("onclick", sbProp.ToString());
            #endregion

            if (!Page.IsPostBack)
            {
                Int32 idRichiesta = Int32.Parse(Request.QueryString["idRichiesta"]);
                Int32 idProposta = Int32.Parse(Request.QueryString["idProposta"]);

                ViewState["IdRichiesta"] = idRichiesta;
                if (idProposta > 0)
                {
                    ViewState["IdProposta"] = idProposta;
                    ColoniePersonaleProposta proposta = bizEF.GetProposta(idProposta);
                    DatiProposta1.CaricaProposta(proposta);
                }
            }
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Int32 idRichiesta = (Int32)ViewState["IdRichiesta"];
                Int32? idProposta = ViewState["IdProposta"] as Int32?;

                ColoniePersonaleProposta proposta = DatiProposta1.GetProposta();
                proposta.IdRichiesta = idRichiesta;
                if (!idProposta.HasValue)
                {
                    proposta.DataInserimentoRecord = DateTime.Now;
                }

                if (!idProposta.HasValue)
                {
                    bizEF.InsertProposta(proposta);
                }
                else
                {
                    bizEF.UpdateProposta(proposta);
                }

                body1.Attributes.Add("onload", "CloseAndRebind();");
            }
        }
    }
}