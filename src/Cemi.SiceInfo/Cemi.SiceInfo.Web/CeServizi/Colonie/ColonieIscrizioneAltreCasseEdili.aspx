﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieIscrizioneAltreCasseEdili.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieIscrizioneAltreCasseEdili" %>

<%@ Register Src="WebControls/ColonieDatiDomanda.ascx" TagName="ColonieDatiDomanda"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Iscrizione"
        titolo="Villaggi vacanza" />
    &nbsp;&nbsp;
    <br />
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red" Visible="False"></asp:Label><br />
    <asp:Panel ID="PanelDati" runat="server" Width="100%">
        <uc3:ColonieDatiDomanda ID="ColonieDatiDomanda1" runat="server" />
    </asp:Panel>
    <br />
</asp:Content>
