﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="ColonieGestioneProposta.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieGestioneProposta" %>

<%@ Register src="WebControls/ColonieDatiProposta.ascx" tagname="DatiProposta" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body id="body1" runat="server">
    <form id="form1" runat="server">
    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refresh(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function Cancel() {
            GetRadWindow().close();
        }
    </script>

    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <table>
            <tr>
                <td colspan="2">
                    <uc1:DatiProposta ID="DatiProposta1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button
                        ID="ButtonConferma"
                        runat="server"
                        Text="Conferma"
                        ValidationGroup="proposta"
                        Width="120px" 
                        onclick="ButtonConferma_Click" />
                </td>
                <td>
                    <asp:ValidationSummary
                        ID="ValidationSummaryProposta"
                        runat="server"
                        CssClass="messaggiErrore"
                        ValidationGroup="proposta" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
