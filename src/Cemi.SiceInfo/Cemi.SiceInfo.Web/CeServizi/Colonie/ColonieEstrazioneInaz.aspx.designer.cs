﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cemi.SiceInfo.Web.CeServizi.Colonie {
    
    
    public partial class ColonieEstrazioneInaz {
        
        /// <summary>
        /// MenuColonie1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.MenuColonie MenuColonie1;
        
        /// <summary>
        /// TitoloSottotitolo1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.TitoloSottotitolo TitoloSottotitolo1;
        
        /// <summary>
        /// VacanzaAttivaRiassunto1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.Colonie.WebControls.VacanzaAttivaRiassunto VacanzaAttivaRiassunto1;
        
        /// <summary>
        /// PanelEstrazioneInaz control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelEstrazioneInaz;
        
        /// <summary>
        /// CustomValidatorDalAl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator CustomValidatorDalAl;
        
        /// <summary>
        /// RadDatePickerDal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadDatePicker RadDatePickerDal;
        
        /// <summary>
        /// RadDatePickerAl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadDatePicker RadDatePickerAl;
        
        /// <summary>
        /// RadComboBoxMansione control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadComboBox RadComboBoxMansione;
        
        /// <summary>
        /// ButtonEstrazioneInaz control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonEstrazioneInaz;
        
        /// <summary>
        /// ValidationSummaryEstrazione control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ValidationSummary ValidationSummaryEstrazione;
        
        /// <summary>
        /// LabelRisultato control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelRisultato;
    }
}
