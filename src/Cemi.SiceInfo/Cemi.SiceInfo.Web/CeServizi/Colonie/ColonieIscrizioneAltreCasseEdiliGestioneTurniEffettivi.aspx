﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ColonieIscrizioneAltreCasseEdiliGestioneTurniEffettivi.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Colonie.ColonieIscrizioneAltreCasseEdiliGestioneTurniEffettivi" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuColonie.ascx" TagName="MenuColonie" TagPrefix="uc2" %>
<%@ Register Src="WebControls/ColonieRicercaDomandeACE.ascx" TagName="ColonieRicercaDomandeACE"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/ColonieDatiDomanda.ascx" TagName="ColonieDatiDomanda"
    TagPrefix="uc4" %>
<%@ Register Src="WebControls/ColonieRicercaDomandeACEEffettive.ascx" TagName="ColonieRicercaDomandeACEEffettive"
    TagPrefix="uc5" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuColonie ID="MenuColonie1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione turni effettivi"
        titolo="Villaggi vacanze" />
    <br />
    <asp:Panel ID="PanelAbilitata" runat="server" Visible="true">
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Vacanza attiva"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Anno
                </td>
                <td>
                    <asp:Label ID="LabelVacanzaAnno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo
                </td>
                <td>
                    <asp:Label ID="LabelVacanzaTipo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Vacanza riassunto situazione"></asp:Label>
                    <br />
                    <br />
                    <asp:GridView ID="GridViewCosti" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewCosti_RowDataBound"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CassaEdile" HeaderText="Cassa edile" />
                            <asp:TemplateField HeaderText="Periodi di permanenza">
                                <ItemTemplate>
                                    <asp:GridView ID="GridViewCosto" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewCosto_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="TipoCosto" />
                                            <asp:TemplateField HeaderText="Numero persone">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelNumeroPersone" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Periodo di permanenza">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelPeriodoPermanenza" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Totale giorni">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelTotaleGiorni" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Nessuna prenotazione trovata<<br />
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna domanda confermata.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Label
                        ID="LabelMessaggio"
                        runat="server">
                    </asp:Label>
                    <br />
                    <hr />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Ricerca partecipanti"></asp:Label>
                    <br />
                    <uc5:ColonieRicercaDomandeACEEffettive ID="ColonieRicercaDomandeACEEffettive1" runat="server" />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
