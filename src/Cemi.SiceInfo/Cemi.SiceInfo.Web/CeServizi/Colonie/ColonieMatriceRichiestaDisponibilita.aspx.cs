﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieMatriceRichiestaDisponibilita : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieGestioneMatrice);

            if (!Page.IsPostBack)
            {
                TextBoxAnno.Text = DateTime.Now.Year.ToString();
                CaricaTipiVacanza();
            }

            CaricaMatrice();
        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipiVacanza.DataSource = tipiVacanza;
            DropDownListTipiVacanza.DataTextField = "Descrizione";
            DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipiVacanza.DataBind();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            CaricaMatrice();
        }

        private void CaricaMatrice()
        {
            if (ControlloCampiServer())
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedItem.Value);

                MatriceTurni matrice = biz.GetMatriceRichiesteDisponibilita(anno, idTipoVacanza);
                GridViewMatrice.DataSource = matrice;
                GridViewMatrice.DataBind();
            }
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            int anno;
            if (string.IsNullOrEmpty(TextBoxAnno.Text) || !Int32.TryParse(TextBoxAnno.Text, out anno))
            {
                res = false;
                errori.Append("Anno non valido o non presente" + Environment.NewLine);
            }

            if (DropDownListTipiVacanza.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo vacanza" + Environment.NewLine);
            }

            if (!res)
                LabelErrore.Text = errori.ToString();
            else
                LabelErrore.Text = string.Empty;
            return res;
        }

        protected void GridViewMatrice_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RigaMatriceTurni riga = (RigaMatriceTurni)e.Row.DataItem;

                GridView gvTurni = (GridView)e.Row.FindControl("GridViewTurni");
                gvTurni.DataSource = riga.Turni;
                gvTurni.DataBind();
            }
        }

        //protected void GridViewTurni_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        //{
        //    int idTurno = (int)GridViewMatrice.DataKeys[e.NewSelectedIndex].Value;
        //    Response.Redirect(string.Format("~/CeServizi/Colonie/ColonieModificaTurno.aspx?idTurno={0}", idTurno));
        //}

        protected void GridViewTurni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Turno turno = (Turno)e.Row.DataItem;

                // Imposto l'apertura del popup al bottone Modifica
                Button buttonModifica = (Button)e.Row.FindControl("ButtonModifica");
                buttonModifica.Attributes.Add("onclick",
                                              "window.open('" +
                                              ResolveUrl("~/CeServizi/Colonie/ColonieModificaPartecipantiTurno.aspx") + "?idTurno=" +
                                              turno.IdTurno + "&idVacanza=" + turno.IdVacanza + (turno.IdTurno == -1 ? "&nonAssegnate=true" : "") +
                                              "', '', 'menubar=no,height=670,width=750'); return false;");

                Button buttonDettagli = (Button)e.Row.FindControl("ButtonDettagli");
                buttonDettagli.Attributes.Add("onclick",
                                              "window.open('" + ResolveUrl("~/CeServizi/Colonie/ColonieDettagliPerTurno.aspx") +
                                              "?idTurno=" + turno.IdTurno + "&descTurno=" +
                                              turno.DescrizioneTurno +
                                              "', 'Dettagli', 'menubar=no,height=300,width=200,scrollbars=yes'); return false;");

                Label labelPostiOcc = (Label)e.Row.FindControl("LabelPostiOccupati");
                labelPostiOcc.Text = turno.PostiOccupati.ToString();

                Button buttonCheckList = (Button)e.Row.FindControl("ButtonCheckList");
                Label labelProgressivo = (Label)e.Row.FindControl("LabelProgressivo");
                if (turno.ProgressivoTurno != -1)
                {
                    labelProgressivo.Text = turno.ProgressivoTurno.ToString();

                    if (turno.PostiOccupati == 0)
                        buttonCheckList.Enabled = false;
                }
                else
                {
                    buttonCheckList.Enabled = false;
                    buttonDettagli.Enabled = false;
                }

                DateTime dtStd = new DateTime(1900, 1, 1);
                Label labelDal = (Label)e.Row.FindControl("LabelDal");
                if (turno.Dal != dtStd)
                    labelDal.Text = turno.Dal.ToShortDateString();

                Label labelAl = (Label)e.Row.FindControl("LabelAl");
                if (turno.Al != dtStd)
                    labelAl.Text = turno.Al.ToShortDateString();

                if (turno.PostiDisponibili < turno.PostiOccupati)
                {
                    //labelPostiOcc.ForeColor = Color.Red;
                    e.Row.ForeColor = Color.Red;
                }
            }
        }

        protected void ButtonCheckList_Click(object sender, EventArgs e)
        {
            int idTurno = -1;

            Button buttonCheckList = (Button)sender;
            GridView gvTurni = (GridView)(buttonCheckList.NamingContainer).NamingContainer;

            idTurno = (int)gvTurni.DataKeys[((GridViewRow)buttonCheckList.NamingContainer).RowIndex].Value;

            Response.Redirect("~/CeServizi/Colonie/ReportColonie.aspx?tipoReport=CheckList&idTurno=" + idTurno);
        }
    }
}