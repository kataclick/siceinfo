﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;
using CassaEdile = TBridge.Cemi.Type.Entities.GestioneUtenti.CassaEdile;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieInserimentoPrenotazione : System.Web.UI.Page
    {
        private const string PRENOTABILE = "Prenotabile";
        private readonly ColonieBusiness biz = new ColonieBusiness();
        private CassaEdile cassaEdile;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieInserimentoDomandeACE);

            // Questa pagina è accessibile solo da chi è loggato come cassa edile, e nella stringa cassa edile ci deve finire il suo codice
            // Recupero la cassa edile loggata
            if (GestioneUtentiBiz.IsCassaEdile())
            {
                //cassaEdile = ((TBridge.Cemi.GestioneUtenti.Business.Identities.CassaEdile)HttpContext.Current.User.Identity).Entity;
                cassaEdile = (CassaEdile)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            }

            if (!Page.IsPostBack)
            {
                TextBoxAnno.Text = DateTime.Now.Year.ToString();
                CaricaTipiVacanza();
            }

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('postiPrenotati') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalva, null) + ";");
            sb.Append("return true;");
            ButtonSalva.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);

        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipiVacanza.DataSource = tipiVacanza;
            DropDownListTipiVacanza.DataTextField = "Descrizione";
            DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipiVacanza.DataBind();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            LabelNonAttiva.Text = string.Empty;

            CaricaMatrice();
        }

        private void CaricaMatrice()
        {
            if (Page.IsValid)
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);
                Vacanza vacanza = biz.GetVacanza(anno, idTipoVacanza);

                if (vacanza != null)
                {
                    if (biz.CassaEdileAbilitata(vacanza.IdVacanza.Value, cassaEdile.IdCassaEdile))
                    {
                        DateTime adesso = DateTime.Now;

                        PrenotazioniCassa prenotazioni = biz.GetPrenotazioniDellaCassa(cassaEdile.IdCassaEdile, anno,
                                                                                       idTipoVacanza);
                        GridViewPrenotazioni.DataSource = prenotazioni.Prenotazioni;
                        GridViewPrenotazioni.DataBind();

                        PanelDettagli.Visible = true;

                        if (prenotazioni.Prenotazioni.Count != 0)
                            ButtonSalva.Visible = true;
                        else
                            ButtonSalva.Visible = false;

                        if (!vacanza.DataInizioPrenotazioni.HasValue && !vacanza.DataFinePrenotazioni.HasValue)
                        {
                            PanelDettagli.Enabled = false;
                            LabelNonAttiva.Text = "Prenotazioni non attive per la vacanza selezionata";
                        }
                        else if (adesso < vacanza.DataInizioPrenotazioni)
                        {
                            PanelDettagli.Enabled = false;
                            LabelNonAttiva.Text =
                                String.Format("Sarà possibile inserire il numero delle prenotazioni dal {0} al {1} escluso",
                                              vacanza.DataInizioPrenotazioni.Value.ToShortDateString(),
                                              vacanza.DataFinePrenotazioni.Value.AddDays(1).ToShortDateString());
                        }
                        else if (adesso >= vacanza.DataFinePrenotazioni.Value.AddDays(1))
                        {
                            PanelDettagli.Enabled = false;
                            LabelNonAttiva.Text = "Prenotazioni non più attive per la vacanza selezionata";
                        }
                        else
                        {
                            LabelEstremiVacanza.Text =
                                String.Format("Sarà possibile inserire il numero delle prenotazioni fino al {0} escluso",
                                              vacanza.DataFinePrenotazioni.Value.AddDays(1).ToShortDateString());
                        }
                    }
                    else
                    {
                        GridViewPrenotazioni.DataSource = null;
                        GridViewPrenotazioni.DataBind();
                        LabelNonAttiva.Text =
                            "La cassa edile non è abilitata ad effettuare le prenotazioni. Contattare Cassa Edile di Milano";
                    }
                }
                else
                {
                    GridViewPrenotazioni.DataSource = null;
                    GridViewPrenotazioni.DataBind();
                    LabelNonAttiva.Text = "Vacanza inesistente";
                }
            }
        }

        protected void GridViewPrenotazioni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox tbPostiRichiesti = (TextBox)e.Row.FindControl("TextBoxPostiRichiesti");
                Label lPostiConcessi = (Label)e.Row.FindControl("LabelPostiConcessi");
                Label lStato = (Label)e.Row.FindControl("LabelStato");
                Prenotazione prenotazione = (Prenotazione)e.Row.DataItem;

                if (prenotazione.IdPrenotazione.HasValue)
                {
                    tbPostiRichiesti.Enabled = false;
                    tbPostiRichiesti.Text = prenotazione.PostiRichiesti.ToString();

                    if (prenotazione.Accettata.HasValue)
                    {
                        if (!prenotazione.Accettata.Value)
                        {
                            e.Row.ForeColor = Color.LightSalmon;
                            lStato.Text = "Rifiutata";
                        }
                        else
                        {
                            if (prenotazione.PostiConcessi.HasValue &&
                                prenotazione.PostiConcessi.Value != prenotazione.PostiRichiesti)
                                lStato.Text = "Variata";
                            else
                                lStato.Text = "Confermata";

                            e.Row.ForeColor = Color.Gray;
                        }

                        if (prenotazione.PostiConcessi.HasValue)
                            lPostiConcessi.Text = prenotazione.PostiConcessi.ToString();
                        else if (prenotazione.Accettata.Value)
                            lPostiConcessi.Text = prenotazione.PostiRichiesti.ToString();
                    }
                    else
                    {
                        lStato.Text = "In verifica";
                        e.Row.ForeColor = Color.Gray;
                    }
                }
                else
                {
                    lStato.Text = PRENOTABILE;
                    ButtonSalva.Enabled = true;
                }
            }
        }

        protected void ButtonSalva_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                PrenotazioneCollection prenotazioni = new PrenotazioneCollection();

                for (int i = 0; i < GridViewPrenotazioni.Rows.Count; i++)
                {
                    GridViewRow riga = GridViewPrenotazioni.Rows[i];
                    Label lStato = (Label)riga.FindControl("LabelStato");

                    // Solo se è prenotabile controllo se è stata chiesta una prenotazione
                    if (lStato.Text == PRENOTABILE)
                    {
                        TextBox tbPostiRichiesti = (TextBox)riga.FindControl("TextBoxPostiRichiesti");
                        if (!string.IsNullOrEmpty(tbPostiRichiesti.Text))
                        {
                            // E' stato impostato un valore
                            int postiRichiesti = Int32.Parse(tbPostiRichiesti.Text);
                            int idTurno = (int)GridViewPrenotazioni.DataKeys[riga.RowIndex].Values["IdTurno"];

                            Prenotazione prenotazione = new Prenotazione();
                            prenotazione.Turno = new Turno();
                            prenotazione.Turno.IdTurno = idTurno;
                            prenotazione.IdCassaEdile = cassaEdile.IdCassaEdile;
                            prenotazione.PostiRichiesti = postiRichiesti;

                            prenotazioni.Add(prenotazione);
                        }
                    }
                }

                if (biz.InsertPrenotazioni(prenotazioni))
                {
                    LabelErroreAgg.Text = string.Empty;
                    CaricaMatrice();
                }
                else
                {
                    LabelErroreAgg.Text = "Errore durante il salvataggio";
                }
            }
        }
    }
}