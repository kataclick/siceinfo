﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Colonie;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Colonie;
using TBridge.Cemi.Type.Entities.Colonie;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieCalcoloCosti : System.Web.UI.Page
    {
        private readonly ColonieBusiness biz = new ColonieBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ColonieCalcoloCosti);

            if (!Page.IsPostBack)
            {
                TextBoxAnno.Text = DateTime.Now.Year.ToString();
                CaricaTipiVacanza();
            }
        }

        private void CaricaTipiVacanza()
        {
            TipoVacanzaCollection tipiVacanza = biz.GetTipiVacanza();

            DropDownListTipiVacanza.DataSource = tipiVacanza;
            DropDownListTipiVacanza.DataTextField = "Descrizione";
            DropDownListTipiVacanza.DataValueField = "IdTipoVacanza";
            DropDownListTipiVacanza.DataBind();
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            CaricaTurni();
        }

        private void CaricaTurni()
        {
            if (ControlloCampiServer())
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);

                TurnoCollection turni = biz.GetTurni(null, null, null, anno, idTipoVacanza);
                DropDownListTurno.DataSource = turni;
                DropDownListTurno.DataTextField = "DescrizioneTurno";
                DropDownListTurno.DataValueField = "IdCombo";
                DropDownListTurno.DataBind();

                if (DropDownListTurno.SelectedIndex != -1)
                {
                    ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                    ButtonCalcola.Enabled = true;
                    ButtonEsporta.Enabled = true;
                }
                else
                {
                    ButtonCalcola.Enabled = false;
                    ButtonEsporta.Enabled = false;
                }
            }
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            int anno;
            if (string.IsNullOrEmpty(TextBoxAnno.Text) || !Int32.TryParse(TextBoxAnno.Text, out anno))
            {
                res = false;
                errori.Append("Anno non valido o non presente" + Environment.NewLine);
            }

            if (DropDownListTipiVacanza.SelectedIndex == -1)
            {
                res = false;
                errori.Append("Selezionare un tipo vacanza" + Environment.NewLine);
            }

            if (!res)
                LabelErrore.Text = errori.ToString();
            else
                LabelErrore.Text = string.Empty;
            return res;
        }

        protected void DropDownListTurno_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListTurno.SelectedIndex != -1)
            {
                ViewState["IdTurno"] = Int32.Parse(DropDownListTurno.SelectedValue);
                ButtonCalcola.Enabled = true;
                ButtonEsporta.Enabled = true;
            }
            else
            {
                ButtonCalcola.Enabled = false;
                ButtonEsporta.Enabled = false;
            }

            GridViewCosti.DataSource = null;
            GridViewCosti.DataBind();
        }

        protected void ButtonCalcola_Click(object sender, EventArgs e)
        {
            if (ViewState["IdTurno"] != null)
            {
                int idTurno = (int)ViewState["IdTurno"];
                CostoPerCassaCollection costi = biz.CalcolaCostiPerTurno(idTurno);

                GridViewCosti.DataSource = costi;
                GridViewCosti.DataBind();
            }
        }

        protected void GridViewCosti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gvCosto = (GridView)e.Row.FindControl("GridViewCosto");

                CostoPerCassa costo = (CostoPerCassa)e.Row.DataItem;
                gvCosto.DataSource = costo.Costi;
                gvCosto.DataBind();
            }
        }

        protected void GridViewCosto_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CostoPerTipologia costo = (CostoPerTipologia)e.Row.DataItem;

                Label lNumeroPersone = (Label)e.Row.FindControl("LabelNumeroPersone");
                Label lPeriodoPermanenza = (Label)e.Row.FindControl("LabelPeriodoPermanenza");
                Label lTotaleGiorni = (Label)e.Row.FindControl("LabelTotaleGiorni");
                Label lQuotaIndividuale = (Label)e.Row.FindControl("LabelQuotaIndividuale");
                Label lTotale = (Label)e.Row.FindControl("LabelTotale");

                if (costo.NumeroPersone != 0)
                    lNumeroPersone.Text = costo.NumeroPersone.ToString();
                if (costo.PeriodoPermanenza != 0)
                    lPeriodoPermanenza.Text = costo.PeriodoPermanenza.ToString();
                if (costo.TotaleGiorni != 0)
                    lTotaleGiorni.Text = costo.TotaleGiorni.ToString();
                if (costo.QuotaIndividuale != 0)
                    lQuotaIndividuale.Text = costo.QuotaIndividuale.ToString();
                if (costo.Totale != 0)
                    lTotale.Text = costo.Totale.ToString();
            }
        }

        protected void ButtonEsporta_Click(object sender, EventArgs e)
        {
            // Esportazione della matrice dei costi
            if (ViewState["IdTurno"] != null)
            {
                int idTurno = (int)ViewState["IdTurno"];
                CostoPerCassaCollection costi = biz.CalcolaCostiPerTurno(idTurno);

                Esporta(costi);
            }
        }

        protected void ButtonCalcolaCostiVacanza_Click(object sender, EventArgs e)
        {
            if (ControlloCampiServer())
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);

                Vacanza vacanza = biz.GetVacanza(anno, idTipoVacanza);

                if (vacanza != null)
                {
                    CostoPerCassaCollection costi = biz.CalcolaCosti(vacanza.IdVacanza.Value);

                    GridViewCosti.DataSource = costi;
                    GridViewCosti.DataBind();

                    LabelErrore.Text = string.Empty;
                }
                else
                {
                    LabelErrore.Text = "Nessuna vacanza trovata";
                }
            }
        }

        private void Esporta(CostoPerCassaCollection costi)
        {
            StringBuilder stringaCostruzione = new StringBuilder();

            foreach (CostoPerCassa costoCassa in costi)
            {
                stringaCostruzione.Append("\n");
                stringaCostruzione.Append(costoCassa.CassaEdile + "\n");
                // Titoli
                stringaCostruzione.Append(
                    "Tipo costo\tNumero persone\tPeriodo di permanenza\tTotale giorni\tQuota individuale\tTotale\n");

                foreach (CostoPerTipologia voce in costoCassa.Costi)
                {
                    stringaCostruzione.Append(voce.TipoCosto);
                    stringaCostruzione.Append("\t");
                    if (voce.NumeroPersone != 0)
                        stringaCostruzione.Append(voce.NumeroPersone.ToString());
                    stringaCostruzione.Append("\t");
                    if (voce.PeriodoPermanenza != 0)
                        stringaCostruzione.Append(voce.PeriodoPermanenza.ToString());
                    stringaCostruzione.Append("\t");
                    if (voce.TotaleGiorni != 0)
                        stringaCostruzione.Append(voce.TotaleGiorni.ToString());
                    stringaCostruzione.Append("\t");
                    if (voce.QuotaIndividuale != 0)
                        stringaCostruzione.Append(voce.QuotaIndividuale.ToString());
                    stringaCostruzione.Append("\t");
                    if (voce.Totale != 0)
                        stringaCostruzione.Append(voce.Totale.ToString());
                    stringaCostruzione.Append("\n");
                }
            }

            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=Costi.xls");
            Response.Charset = "";
            EnableViewState = false;
            Response.Write(stringaCostruzione.ToString());
            Response.End();
        }

        protected void ButtonEsportaCostiVacanza_Click(object sender, EventArgs e)
        {
            if (ControlloCampiServer())
            {
                int anno = Int32.Parse(TextBoxAnno.Text);
                int idTipoVacanza = Int32.Parse(DropDownListTipiVacanza.SelectedValue);

                Vacanza vacanza = biz.GetVacanza(anno, idTipoVacanza);

                if (vacanza != null)
                {
                    CostoPerCassaCollection costi = biz.CalcolaCosti(vacanza.IdVacanza.Value);
                    Esporta(costi);
                    LabelErrore.Text = string.Empty;
                }
                else
                {
                    LabelErrore.Text = "Nessuna vacanza trovata";
                }
            }
        }
    }
}