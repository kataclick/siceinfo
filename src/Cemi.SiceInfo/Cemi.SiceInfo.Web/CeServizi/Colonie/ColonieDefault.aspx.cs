﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Colonie
{
    public partial class ColonieDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.ColonieGestioneDomande);
            funzionalita.Add(FunzionalitaPredefinite.ColonieGestioneExport);
            funzionalita.Add(FunzionalitaPredefinite.ColonieGestioneMatrice);
            funzionalita.Add(FunzionalitaPredefinite.ColonieGestioneVacanze);
            funzionalita.Add(FunzionalitaPredefinite.ColonieSchedaPartecipante);
            funzionalita.Add(FunzionalitaPredefinite.ColonieInserimentoDomandeACE);
            funzionalita.Add(FunzionalitaPredefinite.ColonieRichiestePersonale);
            funzionalita.Add(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale);
            funzionalita.Add(FunzionalitaPredefinite.ColonieEstrazioneINAZ);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        }
    }
}