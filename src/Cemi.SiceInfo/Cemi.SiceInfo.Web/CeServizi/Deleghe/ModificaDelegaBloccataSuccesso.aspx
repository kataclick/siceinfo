﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ModificaDelegaBloccataSuccesso.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.ModificaDelegaBloccataSuccesso" %>


<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/DelegheBloccateStoricoModifiche.ascx" TagName="DelegheBloccateStoricoModifiche"
    TagPrefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento effettuato"
        titolo="Deleghe sindacali" />
    <br />
    La delega è stata correttamente modificata.<br />
    <br />
    Valori attuali:<br />
    <asp:GridView ID="GridViewDelegaAttuale" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="LavoratoreCognome" HeaderText="Cognome" />
            <asp:BoundField DataField="LavoratoreNome" HeaderText="Nome" />
            <asp:BoundField DataField="DataNascitaLavoratore" HeaderText="Data di Nascita" />
        </Columns>
    </asp:GridView>
    <br />
    Di seguito lo storico di tutte le modifiche effettuate:<br />
    <uc3:DelegheBloccateStoricoModifiche ID="DelegheBloccateStoricoModifiche1" runat="server" />
    <br />
</asp:Content>
