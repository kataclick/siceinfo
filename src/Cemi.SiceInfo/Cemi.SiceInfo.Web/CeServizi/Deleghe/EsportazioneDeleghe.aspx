﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="EsportazioneDeleghe.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.EsportazioneDeleghe" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Esportazione"
        titolo="Gestione Deleghe" />
    <br />
    In questa pagina possono essere esportate, in formato Excel, le deleghe precedentemente confermate tramite l’immissione del mese e dell’anno di interesse (mm/aaaa).
    <br />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Sindacato:
            </td>
            <td><asp:DropDownList ID="DropDownListSindacato" runat="server" Width="300px" AppendDataBoundItems="True" Enabled="False">
            </asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                Comprensorio:
            </td>
            <td>
                <asp:DropDownList ID="DropDownListComprensorio" runat="server" Width="300px" AppendDataBoundItems="True"></asp:DropDownList>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorComprensorio" runat="server"
                    ControlToValidate="DropDownListComprensorio" ErrorMessage="Selezionare un comprensorio"
                    ValidationGroup="esportazione"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                Data (mm/aaaa):
            </td>
            <td>
                <asp:TextBox ID="TextBoxData" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorData" runat="server"
                    ErrorMessage="Data non valida" ValidationExpression="^\d{2}/\d{4}$" ControlToValidate="TextBoxData" ValidationGroup="esportazione"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonEsporta" runat="server" Text="Esporta" ValidationGroup="esportazione" OnClick="ButtonEsporta_Click" />
            </td>
            <td>
                
            </td>
        </tr>
    </table>
</asp:Content>


