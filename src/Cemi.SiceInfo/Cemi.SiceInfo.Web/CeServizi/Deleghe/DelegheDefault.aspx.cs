﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class DelegheDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.DelegheConferma);
            funzionalita.Add(FunzionalitaPredefinite.DelegheGestione);
            funzionalita.Add(FunzionalitaPredefinite.DelegheGestioneCE);
            funzionalita.Add(FunzionalitaPredefinite.DelegheSblocco);
            funzionalita.Add(FunzionalitaPredefinite.DelegheVisioneCE);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        }
    }
}