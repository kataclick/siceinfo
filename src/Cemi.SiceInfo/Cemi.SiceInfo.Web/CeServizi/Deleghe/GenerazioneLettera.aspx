﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GenerazioneLettera.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.GenerazioneLettera" %>

<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Generazione lettere"
        titolo="Gestione Deleghe" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Numero di protocollo
            </td>
            <td>
                <asp:TextBox ID="textBoxProtocollo" Width="100%" runat="server" />
                /DIR/gs&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorProtocollo" runat="server"
                    ControlToValidate="textBoxProtocollo" ErrorMessage="*" ValidationGroup="Scadute" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="textBoxProtocollo"
                    ErrorMessage="*" ValidationGroup="NonIscritti" />
            </td>
        </tr>
        <tr>
            <td>
                Sindacato
            </td>
            <td>
                <asp:DropDownList ID="DropDownListSindacato" runat="server" Width="100%" AppendDataBoundItems="True">
                </asp:DropDownList>
                &nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListSindacato"
                    ErrorMessage="*" ValidationGroup="Scadute"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="DropDownListSindacato"
                    ErrorMessage="*" ValidationGroup="NonIscritti"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Comprensorio
            </td>
            <td>
                <asp:DropDownList ID="DropDownListComprensorio" runat="server" Width="100%" AppendDataBoundItems="True">
                </asp:DropDownList>
                &nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="DropDownListComprensorio"
                    ErrorMessage="*" ValidationGroup="Scadute" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="DropDownListComprensorio"
                    ErrorMessage="*" ValidationGroup="NonIscritti"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Data Adesione (mm/yyyy)
            </td>
            <td>
                <asp:TextBox ID="textBoxDataAdesione" Width="100%" runat="server" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="textBoxDataAdesione"
                    ErrorMessage="*" ValidationGroup="Scadute"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="textBoxDataAdesione"
                    ErrorMessage="Formato non valido" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="Scadute"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Lettera Deleghe Scadute
            </td>
            <td>
                <asp:Button ID="ButtonGeneraLetteraScadute" runat="server" Text="Genera" OnClick="ButtonGeneraLetteraScadute_Click"
                    ValidationGroup="Scadute" />
            </td>
        </tr>
        <tr>
            <td>
                Allegato Deleghe Scadute
            </td>
            <td>
                <asp:Button ID="ButtonGeneraAllegatoScadute" runat="server" Text="Genera" OnClick="ButtonGeneraAllegatoScadute_Click"
                    ValidationGroup="Scadute" />
            </td>
        </tr>
        <tr>
            <td>
                Data modifica stato (mm/yyyy)
            </td>
            <td>
                <asp:TextBox ID="textBoxDataModificaStato" Width="100%" runat="server" />&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="textBoxDataModificaStato"
                    ErrorMessage="*" ValidationGroup="NonIscritti"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="textBoxDataModificaStato"
                    ErrorMessage="Formato non valido" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="NonIscritti"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                Lettera Non Iscritti
            </td>
            <td>
                <asp:Button ID="ButtonGeneraLetteraNonIscritti" runat="server" Text="Genera" OnClick="ButtonGeneraLetteraNonIscritti_Click"
                    ValidationGroup="NonIscritti" />
            </td>
        </tr>
        <tr>
            <td>
                Allegato Non Iscritti
            </td>
            <td>
                <asp:Button ID="ButtonGeneraAllegatoNonIscritti" runat="server" Text="Genera" OnClick="ButtonGeneraAllegatoNonIscritti_Click"
                    ValidationGroup="NonIscritti" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelError" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>

