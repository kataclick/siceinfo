﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="DelegheBloccate.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.DelegheBloccate" %>

<%@ Register Src="WebControls/DelegheDatiDelega.ascx" TagName="DelegheDatiDelega"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Deleghe bloccate"
        titolo="Deleghe sindacali" />
    <br />
    <asp:GridView ID="GridViewDelegheBloccate" runat="server" AutoGenerateColumns="False"
        width="100%" AllowPaging="True" DataKeyNames="IdDelega" OnPageIndexChanging="GridViewDelegheBloccate_PageIndexChanging" OnSelectedIndexChanging="GridViewDelegheBloccate_SelectedIndexChanging" OnRowDeleting="GridViewDelegheBloccate_RowDeleting">
        <Columns>
            <asp:BoundField DataField="DataInserimento" DataFormatString="{0:MM/yyyy}" 
                HeaderText="Mese ins." />
            <asp:TemplateField HeaderText="Sindacato">
                <ItemTemplate>
                    <asp:Label ID="LabelSindacato" runat="server" Text='<%# Bind("Sindacato") %>'></asp:Label>
                    <br />
                    <asp:Label ID="LabelComprensorio" runat="server" Text='<%# Bind("ComprensorioSindacale") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="130px" />
            </asp:TemplateField>
            <asp:BoundField DataField="NomeLavoratore" HeaderText="Lavoratore" 
                ItemStyle-Font-Bold="true" >
<ItemStyle Font-Bold="True"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="DataNascitaLavoratore" HeaderText="Data di nascita" >
            <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="ImpresaLavoratore" HeaderText="Impresa" >
            <ItemStyle Width="130px" />
            </asp:BoundField>
            <asp:BoundField DataField="CodiceSblocco" HeaderText="Codice sblocco" >
            <ItemStyle Width="40px" />
            </asp:BoundField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                SelectText="Dettagli" ShowSelectButton="True" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
            <ItemStyle Width="10px" />
            </asp:CommandField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                DeleteText="Elimina" ShowDeleteButton="True" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
            <ItemStyle Width="10px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna delega bloccata presente
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelDettagli" runat="server" width="100%" Visible="False">
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Dettagli delega"></asp:Label><br />
        <uc3:DelegheDatiDelega ID="DelegheDatiDelega1" runat="server" />
    </asp:Panel>
</asp:Content>


