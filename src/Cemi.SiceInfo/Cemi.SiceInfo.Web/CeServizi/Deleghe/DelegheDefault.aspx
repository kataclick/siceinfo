﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="DelegheDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.DelegheDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Benvenuto nella sezione Deleghe Sindacali"
        titolo="Deleghe sindacali" />
    <br />
    <p class="DefaultPage">
        In questa pagina è possibile inserire le deleghe sindacali dei lavoratori.
        <br />
        Il periodo in cui sarà possibile effettuare questa operazione è la seconda metà
        di tutti i mesi dell’anno. Selezionando le voci del menù verticale posto alla sinistra
        della videata è possibile effettuare tutte le attività disponibili per la gestione
        delle deleghe sindacali: "Inserimento delega", "Gestione deleghe" (per la modifica
        e la cancellazione delle deleghe), "Sblocco deleghe" (per i casi di omonimia), "Conferma
        mensile", "Esportazione" delle deleghe confermate in formato Excel e "Storico" per
        la visualizzazione delle deleghe confermate.
    </p>
</asp:Content>
