﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DelegheRicercaDeleghe.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.WebControls.DelegheRicercaDeleghe" %>

<asp:Panel ID="PanelRicercaDeleghe" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td>
                Ricerca deleghe
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                Comprensorio
            </td>
            <td>
                Cognome
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxCognome"
                    ErrorMessage="min 3 car." ValidationExpression="^[\S ]{2,}$" ValidationGroup="ricercaDeleghe"></asp:RegularExpressionValidator>
            </td>
            <td>
                Nome
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxNome"
                    ErrorMessage="min 3 car." ValidationExpression="^[\S ]{2,}$" ValidationGroup="ricercaDeleghe"></asp:RegularExpressionValidator>
            </td>
            <td>
                Data di nascita (gg/mm/aaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                    runat="server" ValidationGroup="ricercaDeleghe" ControlToValidate="TextBoxDataNascita"
                    ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="DropDownListComprensorio" runat="server" Width="100%" AppendDataBoundItems="True">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr id="Riga1Agg" runat="server">
            <td>
                Dal mese conf.(mm/aaaa)
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBoxMeseDal"
                    ErrorMessage="*" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="ricercaDeleghe"></asp:RegularExpressionValidator>
            </td>
            <td>
                Al mese conf.(mm/aaaa)
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBoxMeseAl"
                    ErrorMessage="*" ValidationExpression="^\d{2}/\d{4}$" ValidationGroup="ricercaDeleghe"></asp:RegularExpressionValidator>
            </td>
            <td>
                Stato
            </td>
            <td>
                Sindacato
            </td>
        </tr>
        <tr id="Riga2Agg" runat="server">
            <td>
                <asp:TextBox ID="TextBoxMeseDal" runat="server" Width="100%" MaxLength="7"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxMeseAl" runat="server" Width="100%" MaxLength="7"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListStato" runat="server" Width="100%" AppendDataBoundItems="True">
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="DropDownListSindacato" runat="server" Width="100%" AppendDataBoundItems="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="Riga3Agg" runat="server">
            <td>
                Delega da
            </td>
            <td>
                Codice Delega
            </td>
            <td>
                <asp:Label ID="LabelMeseModStato" runat="server" Text="Mese mod stato (mm/aaaa)"></asp:Label>
            </td>
            
            <td>
            </td>
        </tr>
        <tr id="Riga4Agg" runat="server" style="vertical-align:middle !important">
            <td>
                <asp:TextBox ID="TextBoxDelegaRecuperataDa" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td style="vertical-align:middle !important">
                MI - <asp:TextBox ID="TextBoxCodiceDelega" runat="server" MaxLength="8" Width="85%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxMeseModStato" runat="server" MaxLength="7" Width="100%"></asp:TextBox>
            </td>
            
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Valorizzare entrambe le date (Dal mese conf. e Al mese conf.)"
                    OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="ricercaDeleghe"></asp:CustomValidator><br />
                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Dal mese conf. deve essere inferiore o uguale a Al mese conf."
                    OnServerValidate="CustomValidator2_ServerValidate" ValidationGroup="ricercaDeleghe"></asp:CustomValidator>
            </td>
            <td align="right" colspan="2">
                <asp:Button ID="ButtonEsporta" runat="server" OnClick="ButtonEsporta_Click" Text="Esporta Excel"
                    ValidationGroup="ricercaDeleghe" />&nbsp;
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Ricerca" ValidationGroup="ricercaDeleghe" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GridViewDeleghe" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="IdDelega,IdArchidoc,NomeAllegatoLettera,NomeAllegatoBusta" OnSelectedIndexChanging="GridViewDeleghe_SelectedIndexChanging"
                    OnRowDeleting="GridViewDeleghe_RowDeleting" OnPageIndexChanging="GridViewDeleghe_PageIndexChanging"
                    OnRowDataBound="GridViewDeleghe_RowDataBound" 
                    OnRowEditing="GridViewDeleghe_RowEditing" 
                    onrowcommand="GridViewDeleghe_RowCommand"
                    OnRowCreated="GridViewDeleghe_RowCreated">
                    <EmptyDataTemplate>
                        Nessuna delega trovata
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="NomeLavoratore" HeaderText="Lavoratore" />
                        <asp:BoundField DataField="LavoratoreId" HeaderText="Cod.">
                            <ItemStyle Width="20px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DataNascitaLavoratore" HeaderText="Data di nascita" />
                        <asp:BoundField DataField="Sindacato" HeaderText="Sindacato" />
                        <asp:BoundField HeaderText="Comprensorio" DataField="ComprensorioStringa" />
                        <asp:BoundField DataField="DataConferma" DataFormatString="{0:MM/yyyy}" HeaderText="Mese conf."
                            HtmlEncode="False" />
                        <asp:BoundField DataField="OperatoreTerritorio" HeaderText="Delega da" />
                        <asp:BoundField DataField="Stato" HeaderText="Stato" />
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectText="Dettagli" ShowSelectButton="True"
                            SelectImageUrl="../../images/edit.png">
                            <ItemStyle Width="20px" />
                        </asp:CommandField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Delete" ImageUrl="../../images/pallinoX.png"
                                    OnClick="ImageButton1_Click" />
                            </ItemTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton2" runat="server" CommandName="Edit" ImageUrl="../../images/pdf.gif"
                                    OnClick="ImageButton2_Click"/>
                            </ItemTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton3" runat="server" CommandName="Lettera" ImageUrl="../../images/word.gif"
                                    OnClick="ImageButton3_Click" />
                            </ItemTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButtonBusta" runat="server" CommandName="Lettera" ImageUrl="../../images/icona_busta.png"
                                    OnClick="ImageButtonBusta_Click" Width="21px" />
                            </ItemTemplate>
                           <%-- <ItemStyle Width="20px" />--%>
                        </asp:TemplateField>
                       <%-- <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" CommandName="Edit" ImageUrl="~/images/pdf.gif"  />
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" CommandName="Lettera" ImageUrl="~/images/word.gif" />--%>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
