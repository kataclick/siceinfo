﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.Type.Entities.Deleghe;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe.WebControls
{
    public partial class DelegheBloccateStoricoModifiche : System.Web.UI.UserControl
    {
        private readonly DelegheBusiness biz = new DelegheBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            //
        }

        public void CaricaStoricoDelega(int idDelega)
        {
            List<StoricoDelegaBloccata> delegheBloccate = biz.GetStoricoDelegaBloccata(idDelega);
            GridViewStoricoDelegaBloccata.DataSource = delegheBloccate;
            GridViewStoricoDelegaBloccata.DataBind();
        }
    }
}