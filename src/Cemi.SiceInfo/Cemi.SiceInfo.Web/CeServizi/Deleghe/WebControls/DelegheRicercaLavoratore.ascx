﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DelegheRicercaLavoratore.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.WebControls.DelegheRicercaLavoratore" %>

<asp:Panel ID="PanelRicercaLavoratori" runat="server" DefaultButton="ButtonVisualizza">
<table class="filledtable">
    <tr>
        <td style="height: 16px">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca lavoratori"></asp:Label>
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            <table width="720">
                <tr>
                    <td>
                        Cognome
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxCognome"
                            ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator></td>
                    <td>
                        Nome
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxNome"
                            ErrorMessage="min 3 car." ValidationExpression="^[\S ]{3,}$" ValidationGroup="ricercaLavoratori"></asp:RegularExpressionValidator></td>
                    <td>
                        Data di nascita (gg/mm/aaaa)<asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            runat="server" ControlToValidate="TextBoxDataNascita" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator></td>
                    <td>
                        Codice fiscale
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="255" Width="100%"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="100" Width="100%"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="255" Width="100%"></asp:TextBox></td>
                    <td>
                        <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label></td>
                    <td>
                    </td>
                    <td align="right" colspan="2">
                        <asp:Button ID="ButtonVisualizza" runat="server" 
                            OnClick="ButtonVisualizza_Click" Text="Ricerca" ValidationGroup="ricercaLavoratori" /><asp:Button ID="ButtonChiudi" runat="server"  OnClick="ButtonChiudi_Click"
                            Text="X" CausesValidation="False" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="LabelEsitoRicerca" runat="server" Font-Bold="True" Text="Esito Ricerca" Visible="False"></asp:Label></td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewLavoratori" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" DataKeyNames="IdLavoratore" OnPageIndexChanging="GridViewLavoratori_PageIndexChanging"
                OnSelectedIndexChanging="GridViewLavoratori_SelectedIndexChanging"
                width="100%">
                <Columns>
                    <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                    <asp:BoundField DataField="Nome" HeaderText="Nome" />
                    <asp:BoundField DataField="DataNascita" HeaderText="Data di nascita" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False" />
                    <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" />
                    <%--<asp:BoundField DataField="ResidenzaCompleto" HeaderText="Indirizzo" />--%>
                    <asp:BoundField DataField="Impresa" HeaderText="Ultima impr." />
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona" ShowSelectButton="True" />
                </Columns>
                <EmptyDataTemplate>
                    Nessun lavoratore trovato
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Label ID="LabelSeleziona" runat="server" Text='Premere il bottone "Seleziona" posto in corrispondenza del nominativo della persona ricercata per l’acquisizione automatica dei dati nei campi sotto riportati.'
                Visible="False"></asp:Label></td>
    </tr>
</table>
</asp:Panel>

