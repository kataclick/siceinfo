﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DelegheBloccateStoricoModifiche.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.WebControls.DelegheBloccateStoricoModifiche" %>

<asp:GridView ID="GridViewStoricoDelegaBloccata" runat="server" AutoGenerateColumns="False">
    <Columns>
        <asp:BoundField DataField="DataVariazione" HeaderText="Data variazione" />
        <asp:BoundField DataField="Utente" HeaderText="Utente" />
        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
        <asp:BoundField DataField="Nome" HeaderText="Nome" />
        <asp:BoundField DataField="DataNascita" DataFormatString="{0:d}" 
            HeaderText="Data di nascita" />
    </Columns>
</asp:GridView>
