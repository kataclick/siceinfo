﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Enums.Deleghe;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe.WebControls
{
    public partial class DelegheDettaglioDelega : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LabelModifica.Visible = false;
            ButtonModifica.Visible = false;
            ButtonModifica.Enabled = false;
        }

        public void CaricaDelegaConIndirizzoAnagrafico(Delega delega)
        {
            CaricaDelega(delega);

            Presenter.SvuotaCampo(LabelResidenzaIndirizzo);
            Presenter.SvuotaCampo(LabelResidenzaProvincia);
            Presenter.SvuotaCampo(LabelResidenzaComune);
            Presenter.SvuotaCampo(LabelResidenzaCap);

            LabelResidenzaIndirizzo.Text = delega.AnagraficaResidenzaIndirizzo;
            LabelResidenzaProvincia.Text = delega.AnagraficaResidenzaProvincia;
            LabelResidenzaComune.Text = delega.AnagraficaResidenzaComune;
            LabelResidenzaCap.Text = delega.AnagraficaResidenzaCap;
        }

        public void CaricaDelega(Delega delega)
        {
            LabelComprensorio.Text = delega.ComprensorioSindacale.ToString();
            LabelDelegaRecuperataDa.Text = delega.OperatoreTerritorio;

            LabelCognome.Text = delega.Lavoratore.Cognome;
            LabelNome.Text = delega.Lavoratore.Nome;
            LabelDataNascita.Text = delega.Lavoratore.DataNascita.Value.ToShortDateString();
            LabelImpresaCodice.Text = delega.Lavoratore.IdImpresa.ToString();
            LabelImpresa.Text = delega.Lavoratore.Impresa;

            LabelResidenzaIndirizzo.Text = delega.Lavoratore.Residenza.IndirizzoDenominazione + " " + delega.Lavoratore.Residenza.Civico;
            LabelResidenzaProvincia.Text = delega.Lavoratore.Residenza.Provincia;
            LabelResidenzaComune.Text = delega.Lavoratore.Residenza.Comune;
            LabelResidenzaCap.Text = delega.Lavoratore.Residenza.Cap;

            LabelCantiereIndirizzo.Text = delega.Cantiere.IndirizzoDenominazione;
            LabelCantiereProvincia.Text = delega.Cantiere.Provincia;
            LabelCantiereComune.Text = delega.Cantiere.Comune;
            LabelCantiereCap.Text = delega.Cantiere.Cap;

            if (delega.Stato.HasValue && delega.Stato.Value == StatoDelega.Bloccata)
            {
                LabelModifica.Visible = true;
                ButtonModifica.Visible = true;
                ButtonModifica.Enabled = true;
                ViewState["IdDelega"] = delega.IdDelega;
            }
        }

        protected void ButtonModifica_Click(object sender, EventArgs e)
        {
            Context.Items["IdDelega"] = ViewState["IdDelega"];
            Server.Transfer("~/CeServizi/Deleghe/InserimentoDelega.aspx?modalita=modificaBloccata", true);
        }
    }
}