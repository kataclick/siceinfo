﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class ModificaDelegaBloccataSuccesso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione);

            Delega delega = (Delega)Context.Items["Delega"];
            List<Delega> deleghe = new List<Delega> { delega };
            GridViewDelegaAttuale.DataSource = deleghe;
            GridViewDelegaAttuale.DataBind();

            if (delega.IdDelega.HasValue)
                DelegheBloccateStoricoModifiche1.CaricaStoricoDelega(delega.IdDelega.Value);
        }
    }
}