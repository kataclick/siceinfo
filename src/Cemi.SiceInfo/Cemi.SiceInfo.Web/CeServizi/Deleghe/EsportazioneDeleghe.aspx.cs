﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using OfficeOpenXml;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Deleghe;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class EsportazioneDeleghe : System.Web.UI.Page
    {
        private const int INDICEPROGRESSIVO = 0;
        private readonly DelegheBusiness biz = new DelegheBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione);

            if (!Page.IsPostBack)
            {
                Sindacalista sindacalista = (Sindacalista)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                CaricaSindacati();
                CaricaComprensori();

                DropDownListSindacato.SelectedValue = sindacalista.Sindacato.Id;

                if (sindacalista.ComprensorioSindacale != null)
                {
                    DropDownListComprensorio.SelectedValue = sindacalista.ComprensorioSindacale.Id;
                    DropDownListComprensorio.Enabled = false;
                }

                if (Request.QueryString["data"] != null)
                {
                    TextBoxData.Text = Request.QueryString["data"];

                    if (!string.IsNullOrEmpty(DropDownListComprensorio.SelectedValue) &&
                        !string.IsNullOrEmpty(DropDownListSindacato.SelectedValue))
                        EsportaDeleghe();
                }
                else
                    TextBoxData.Text = DateTime.Now.ToString("MM/yyyy");
            }
        }

        private void CaricaComprensori()
        {
            DropDownListComprensorio.Items.Clear();

            DropDownListComprensorio.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListComprensorio.DataSource = biz.GetComprensori();
            DropDownListComprensorio.DataTextField = "Descrizione";
            DropDownListComprensorio.DataValueField = "Id";
            DropDownListComprensorio.DataBind();
        }

        private void CaricaSindacati()
        {
            DropDownListSindacato.Items.Clear();

            DropDownListSindacato.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListSindacato.DataSource = biz.GetSindacati();
            DropDownListSindacato.DataTextField = "Descrizione";
            DropDownListSindacato.DataValueField = "Id";
            DropDownListSindacato.DataBind();
        }

        protected void ButtonEsporta_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                EsportaDeleghe();
            }
        }

        private void EsportaDeleghe()
        {
            DateTime data = DateTime.ParseExact(TextBoxData.Text, "MM/yyyy", null);

            DelegheFilter filtro = new DelegheFilter
            {
                Sindacato = DropDownListSindacato.SelectedValue,
                ComprensorioSindacale = DropDownListComprensorio.SelectedValue,
                Confermate = true,
                MeseConferma = data
            };

            List<Delega> deleghe = biz.GetDeleghe(filtro);

            #region Vecchia estrazione Excel
            /*

            string stampa = PreparaStampa(deleghe);

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Deleghe.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(stampa);
            Response.End();
            */
            #endregion

            GridView gv = new GridView();
            gv.ID = "gvDeleghe";
            gv.AutoGenerateColumns = false;
            gv.RowDataBound += gv_RowDataBound;

            TemplateField tc0 = new TemplateField();
            tc0.HeaderText = "Progressivo";

            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Sindacato";
            bc1.DataField = "Sindacato";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Comprensorio";
            bc2.DataField = "ComprensorioSindacale";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "Cognome";
            bc3.DataField = "LavoratoreCognome";
            BoundField bc3bis = new BoundField();
            bc3bis.HeaderText = "Nome";
            bc3bis.DataField = "LavoratoreNome";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Data nascita";
            bc4.DataField = "DataNascitaLavoratore";
            bc4.DataFormatString = "{0:dd/MM/yyyy}";
            bc4.HtmlEncode = false;
            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Impresa";
            bc5.DataField = "ImpresaLavoratore";
            BoundField bc6 = new BoundField();
            bc6.HeaderText = "Codice Delega";
            bc6.DataField = "codiceDelega";

            gv.Columns.Add(tc0);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc3bis);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);

            gv.DataSource = deleghe;
            gv.DataBind();

            if (gv.Rows.Count > 0)
            {
                EstraiExcel(gv, "Esportazione_Deleghe");
            }


        }

        private void EstraiExcel(GridView gv, String NomeExcel)
        {
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add(NomeExcel);
            var totalCols = gv.Rows[0].Cells.Count;
            var totalRows = gv.Rows.Count;
            var headerRow = gv.HeaderRow;
            for (var i = 1; i <= totalCols; i++)
            {
                workSheet.Cells[1, i].Value = Server.HtmlDecode(headerRow.Cells[i - 1].Text);
                workSheet.Cells[1, i].Style.Font.Bold = true;
                workSheet.Cells[1, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            }
            for (var j = 1; j <= totalRows; j++)
            {
                for (var i = 1; i <= totalCols; i++)
                {
                    GridViewRow row = gv.Rows[j - 1];
                    workSheet.Cells[j + 1, i].Value = Server.HtmlDecode(row.Cells[i - 1].Text);
                }
            }

            using (var memoryStream = new MemoryStream())
            {
                Response.ClearContent();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                String ne = String.Format("attachment;  filename={0}.xlsx", NomeExcel);
                Response.AddHeader("content-disposition", ne);
                excel.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }

        private static string PreparaStampa(List<Delega> deleghe)
        {
            GridView gv = new GridView();
            gv.ID = "gvDeleghe";
            gv.AutoGenerateColumns = false;
            gv.RowDataBound += gv_RowDataBound;

            TemplateField tc0 = new TemplateField();
            tc0.HeaderText = "Progressivo";

            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Sindacato";
            bc1.DataField = "Sindacato";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Comprensorio";
            bc2.DataField = "ComprensorioSindacale";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "Cognome";
            bc3.DataField = "LavoratoreCognome";
            BoundField bc3bis = new BoundField();
            bc3bis.HeaderText = "Nome";
            bc3bis.DataField = "LavoratoreNome";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Data nascita";
            bc4.DataField = "DataNascitaLavoratore";
            bc4.DataFormatString = "{0:dd/MM/yyyy}";
            bc4.HtmlEncode = false;
            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Impresa";
            bc5.DataField = "ImpresaLavoratore";

            gv.Columns.Add(tc0);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc3bis);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);

            gv.DataSource = deleghe;
            gv.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            return sw.ToString();
        }

        private static void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[INDICEPROGRESSIVO].Text = (e.Row.RowIndex + 1).ToString();
            }
        }
    }
}