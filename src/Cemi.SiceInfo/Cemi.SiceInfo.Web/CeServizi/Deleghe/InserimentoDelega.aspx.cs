﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Indirizzo = TBridge.Cemi.Type.Entities.Geocode.Indirizzo;
using Lavoratore = TBridge.Cemi.Type.Entities.Deleghe.Lavoratore;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class InserimentoDelega : System.Web.UI.Page
    {
        private readonly DelegheBusiness _biz = new DelegheBusiness();



        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione);

            //Sindacalista sindacalista =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista) HttpContext.Current.User.Identity).Entity;
            Sindacalista sindacalista =
                (Sindacalista)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            DelegheRicercaLavoratore1.OnLavoratoreSelected += DelegheRicercaLavoratore1_OnLavoratoreSelected;

            Page.MaintainScrollPositionOnPostBack = true;

            if (!Page.IsPostBack)
            {
                if (_biz.ApertaFaseInserimento())
                {
                    BulletedListErrori.DataSource = null;
                    BulletedListErrori.Visible = false;

                    if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "modificaBloccata")
                    {
                        PanelInserimentoCompleto.Visible = true;
                        PanelGiaConfermate.Visible = false;


                        CaricaComprensori();

                        // Modalità modifica
                        int idDelega = (int)Context.Items["IdDelega"];
                        ViewState["IdDelega"] = idDelega;
                        ViewState["modalita"] = "modificaBloccata";
                        ButtonInserisciDelega.Text = "Modifica";
                        ButtonReset.Visible = false;
                        CaricaDelega(idDelega);
                        DisabilitaPerModificaBloccata();
                    }
                    else if (
                        !_biz.DelegheConfermate(DateTime.Now.Month, DateTime.Now.Year, sindacalista.Sindacato,
                                                sindacalista.ComprensorioSindacale))
                    {
                        PanelInserimentoCompleto.Visible = true;
                        PanelGiaConfermate.Visible = false;

                        CaricaComprensori();

                        if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "modifica")
                        {
                            // Modalità modifica
                            int idDelega = (int)Context.Items["IdDelega"];
                            ViewState["IdDelega"] = idDelega;
                            ButtonInserisciDelega.Text = "Modifica";

                            RequiredFieldValidatorCodiceNumber.Enabled = true;
                            RegularExpressionCodiceNumber.Enabled = true;
                            RequiredFieldValidatorCodiceChar.Enabled = true;
                            RegularExpressionCodiceChar.Enabled = true;

                            ButtonReset.Visible = false;
                            CaricaDelega(idDelega);
                        }
                        else
                        {
                            if (sindacalista.ComprensorioSindacale != null)
                            {
                                DropDownListComprensorio.SelectedValue = sindacalista.ComprensorioSindacale.Id;
                                DropDownListComprensorio.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        PanelInserimentoCompleto.Visible = false;
                        PanelGiaConfermate.Visible = true;

                        LabelGiaConfermate.Text = "Non è possibile inserire nuove deleghe, sono già state confermate.";
                    }
                }
                else
                {
                    PanelInserimentoCompleto.Visible = false;
                    PanelGiaConfermate.Visible = true;

                    LabelGiaConfermate.Text = "In questo periodo non è possibile l'inserimento o modifica delle deleghe.";
                }
            }

            #region Per prevenire click multipli

            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append(
                "if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciDelega, null));
            sb.Append(";");
            sb.Append("return true;");
            ButtonInserisciDelega.Attributes.Add("onclick", sb.ToString());

            #endregion

            if (LabelResidenzaIncompleta.Visible == true)
            {
                Page.SetFocus("LabelResidenzaIncompleta");
            }
        }

        private void DisabilitaPerModificaBloccata()
        {
            DropDownListComprensorio.Enabled = false;
            TextBoxRecuperoDelega.Enabled = false;
            TextBoxCellulare.Enabled = false;
            TextBoxImpresaCodice.Enabled = false;
            TextBoxImpresa.Enabled = false;

            TextBoxCodiceNumber.Enabled = false;
            TextBoxCodiceChar.Enabled = false;
            RequiredFieldValidatorCodiceNumber.Enabled = false;
            RegularExpressionCodiceNumber.Enabled = false;
            RequiredFieldValidatorCodiceChar.Enabled = false;
            RegularExpressionCodiceChar.Enabled = false;

            SelezioneIndirizzoDeleghe1.DisabilitaIndirizzo();

            /* VECCHIA RESIDENZA
            TextBoxResidenzaIndirizzo.Enabled = false;
            TextBoxResidenzaProvincia.Enabled = false;
            TextBoxResidenzaComune.Enabled = false;
            TextBoxResidenzaCap.Enabled = false;
            */

            TextBoxCantiereIndirizzo.Enabled = false;
            TextBoxCantiereProvincia.Enabled = false;
            TextBoxCantiereComune.Enabled = false;
            TextBoxCantiereCap.Enabled = false;
        }

        private void CaricaDelega(int idDelega)
        {
            Delega delega = _biz.GetDelega(idDelega);

            DropDownListComprensorio.SelectedValue = delega.ComprensorioSindacale.Id;
            TextBoxRecuperoDelega.Text = delega.OperatoreTerritorio;
            if(delega.CodiceDelega != null)
            {
                TextBoxCodiceNumber.Text = delega.CodiceDelega.Substring(2, 5);
                TextBoxCodiceChar.Text = delega.CodiceDelega.Substring(7, 2);
            }            

            TextBoxCognome.Text = delega.Lavoratore.Cognome;
            TextBoxNome.Text = delega.Lavoratore.Nome;
            if (delega.Lavoratore.DataNascita != null)
                TextBoxDataNascita.Text = delega.Lavoratore.DataNascita.Value.ToShortDateString();
            TextBoxCellulare.Text = delega.Lavoratore.Cellulare;
            TextBoxImpresaCodice.Text = delega.Lavoratore.IdImpresa.ToString();
            TextBoxImpresa.Text = delega.Lavoratore.Impresa;

            Indirizzo indirizzo = new Indirizzo();
            indirizzo.NomeVia = delega.Lavoratore.Residenza.IndirizzoDenominazione;
            indirizzo.Civico = delega.Lavoratore.Residenza.Civico;
            indirizzo.Provincia = delega.Lavoratore.Residenza.Provincia;
            indirizzo.Comune = delega.Lavoratore.Residenza.Comune;
            indirizzo.Cap = delega.Lavoratore.Residenza.Cap;

            SelezioneIndirizzoDeleghe1.CaricaDatiIndirizzo(indirizzo);

            /* VECCHIA RESIDENZA
            TextBoxResidenzaIndirizzo.Text = delega.Lavoratore.Residenza.IndirizzoDenominazione;
            TextBoxResidenzaProvincia.Text = delega.Lavoratore.Residenza.Provincia;
            TextBoxResidenzaComune.Text = delega.Lavoratore.Residenza.Comune;
            TextBoxResidenzaCap.Text = delega.Lavoratore.Residenza.Cap;
            */

            TextBoxCantiereIndirizzo.Text = delega.Cantiere.IndirizzoDenominazione;
            TextBoxCantiereProvincia.Text = delega.Cantiere.Provincia;
            TextBoxCantiereComune.Text = delega.Cantiere.Comune;
            TextBoxCantiereCap.Text = delega.Cantiere.Cap;

            ViewState["dataAdesione"] = delega.DataAdesione;
            if (delega.CodiceDelega != null)
                ViewState["codiceDelega"] = delega.CodiceDelega.Trim();
            else
                ViewState["codiceDelega"] = null;
        }

        private void CaricaComprensori()
        {
            DropDownListComprensorio.Items.Clear();

            DropDownListComprensorio.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListComprensorio.DataSource = _biz.GetComprensori(true);
            DropDownListComprensorio.DataTextField = "Descrizione";
            DropDownListComprensorio.DataValueField = "Id";
            DropDownListComprensorio.DataBind();
        }

        private void DelegheRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
        {
            CaricaDatiLavoratore(lavoratore);
            DelegheRicercaLavoratore1.Visible = false;
        }

        private void CaricaDatiLavoratore(Lavoratore lavoratore)
        {
            if (Request.QueryString["modalita"] == null && Request.QueryString["modalita"] != "modificaBloccata")
                SvuotaCampi();

            TextBoxCognome.Text = string.Empty;
            TextBoxCognome.Text = lavoratore.Cognome;
            TextBoxNome.Text = string.Empty;
            TextBoxNome.Text = lavoratore.Nome;
            if (lavoratore.DataNascita.HasValue)
            {
                TextBoxDataNascita.Text = string.Empty;
                TextBoxDataNascita.Text = lavoratore.DataNascita.Value.ToShortDateString();
            }
            if (ViewState["modalita"] == null)
            {
                TextBoxImpresa.Text = string.Empty;
                TextBoxImpresa.Text = lavoratore.Impresa;
                //TextBoxResidenzaIndirizzo.Text = lavoratore.Residenza.IndirizzoDenominazione;
                //TextBoxResidenzaProvincia.Text = lavoratore.Residenza.Provincia;
                //TextBoxResidenzaComune.Text = lavoratore.Residenza.Comune;
                //TextBoxResidenzaCap.Text = lavoratore.Residenza.Cap;
                TextBoxImpresaCodice.Text = string.Empty;
                TextBoxImpresaCodice.Text = lavoratore.IdImpresa.ToString();
                //TextBoxCellulare.Text = lavoratore.Cellulare;
            }
        }

        protected void ButtonRicercaAnagrafica_Click(object sender, EventArgs e)
        {
            DelegheRicercaLavoratore1.Visible = true;
        }

        protected void ButtonInserisciDelega_Click(object sender, EventArgs e)
        {
            InserisciDelega();
        }

        private void InserisciDelega()
        {
            LabelErrore.Visible = false;
            LabelNonInseribile.Visible = false;
            LabelResidenzaIncompleta.Visible = false;
            bool sbloccoOmonimie = false;

            //Page.Validate("inserimento");
            //SelezioneIndirizzoDeleghe1.AbilitaValidationGroup(false);
            //SelezioneIndirizzoDeleghe1.FindControl("ValidationSummaryIndirizzo").Visible = false;

            if (Page.IsValid)
            {
                if (SelezioneIndirizzoDeleghe1.CheckCompilazione())
                {
                    Delega delega = CreaDelega();

                    StringCollection listaProblemi;
                    if (!delega.IdDelega.HasValue)
                    {
                        // Effettua i controlli da fare lato server
                        listaProblemi = _biz.ControllaDelega(delega, out sbloccoOmonimie);
                    }
                    else
                    {
                        listaProblemi = new StringCollection();
                        if (ViewState["codiceDelega"] != null && delega.CodiceDelega.Trim() != ((string)ViewState["codiceDelega"]).Trim())
                        {
                            if (_biz.EsisteCodiceDelega(delega.CodiceDelega) > 0)
                            {
                                listaProblemi.Add("Codice Delega già presente a sistema.");
                            }
                        }
                    }

                    if (ViewState["modalita"] != null && (String)ViewState["modalita"] == "modificaBloccata"
                            && _biz.CheckModificaDelegaNonInseribile(delega))
                    {
                        // Se modifico una delega con i dati di un lavoratore che ha una delega bloccata per
                        // lo stesso mese blocco tutto
                        LabelNonInseribile.Visible = true;
                    }
                    else
                    {
                        if (listaProblemi.Count == 0)
                        {
                            BulletedListErrori.DataSource = null;
                            BulletedListErrori.Visible = false;

                            if (ViewState["modalita"] != null && (string)ViewState["modalita"] == "modificaBloccata")
                            {
                                if (_biz.CheckModificaDelegaNonInseribile(delega))
                                {
                                    // Se modifico una delega con i dati di un lavoratore che ha una delega bloccata per
                                    // lo stesso mese blocco tutto
                                    LabelNonInseribile.Visible = true;
                                }
                                else
                                {
                                    int idUtente = GestioneUtentiBiz.GetIdUtente();

                                    if (_biz.ForzaUpdateDelegaBloccata(delega, idUtente))
                                    {
                                        Context.Items["Delega"] = delega;
                                        //Response.Redirect("~/Deleghe/ModificaDelegaBloccataSuccesso.aspx");
                                        Server.Transfer("~/CeServizi/Deleghe/ModificaDelegaBloccataSuccesso.aspx");
                                    }
                                    else
                                        LabelErrore.Visible = true;
                                }
                            }
                            else
                            {
                                if (_biz.InsertUpdateDelega(delega))
                                    Response.Redirect("~/CeServizi/Deleghe/InserimentoDelegaSuccesso.aspx");
                                else
                                    LabelErrore.Visible = true;
                            }
                        }
                        else
                        {
                            BulletedListErrori.Visible = true;
                            BulletedListErrori.DataSource = listaProblemi;
                            BulletedListErrori.DataBind();

                            ProponiForzatura(sbloccoOmonimie);
                        }
                    }
                }
                else
                {
                    LabelResidenzaIncompleta.Visible = true;
                    Page.MaintainScrollPositionOnPostBack = true;
                    Page.SetFocus("LabelResidenzaIncompleta");
                }
            }
        }

        private void ProponiForzatura(bool proponi)
        {
            PanelForzatura.Visible = proponi;
            PanelBloccoCampiForzatura.Enabled = !proponi;
        }

        private Delega CreaDelega()
        {
            Delega delega = new Delega();

            if (ViewState["IdDelega"] != null)
                delega.IdDelega = (int)ViewState["IdDelega"];

            //TBridge.Cemi.Deleghe.Type.Entities.Indirizzo indirizzo = new TBridge.Cemi.Deleghe.Type.Entities.Indirizzo();
            Indirizzo indirizzo = new Indirizzo();
            indirizzo = SelezioneIndirizzoDeleghe1.GetIndirizzo();

            if (!string.IsNullOrEmpty(TextBoxCodiceNumber.Text.Trim()) && !string.IsNullOrEmpty(TextBoxCodiceChar.Text.Trim()))
                delega.CodiceDelega = $"MI{TextBoxCodiceNumber.Text}{TextBoxCodiceChar.Text.Trim().ToUpper()}".Trim();
            delega.Lavoratore.Cognome = TextBoxCognome.Text.Trim().ToUpper();
            delega.Lavoratore.Nome = TextBoxNome.Text.Trim().ToUpper();
            delega.Lavoratore.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
            delega.Lavoratore.Cellulare = TextBoxCellulare.Text.Trim().ToUpper();
            if (!string.IsNullOrEmpty(TextBoxImpresaCodice.Text.Trim()))
                delega.Lavoratore.IdImpresa = Int32.Parse(TextBoxImpresaCodice.Text);
            delega.Lavoratore.Impresa = TextBoxImpresa.Text.Trim().ToUpper();

            /* VECCHIA RESIDENZA
            delega.Lavoratore.Residenza.IndirizzoDenominazione = TextBoxResidenzaIndirizzo.Text.Trim().ToUpper();
            //delega.Lavoratore.Residenza.Civico = TextBoxResidenzaCivico.Text.Trim().ToUpper();
            delega.Lavoratore.Residenza.Provincia = TextBoxResidenzaProvincia.Text.Trim().ToUpper();
            delega.Lavoratore.Residenza.Comune = TextBoxResidenzaComune.Text.Trim().ToUpper();
            delega.Lavoratore.Residenza.Cap = TextBoxResidenzaCap.Text.Trim().ToUpper();
            */

            delega.Lavoratore.Residenza.IndirizzoDenominazione = indirizzo.NomeVia;
            delega.Lavoratore.Residenza.Civico = indirizzo.Civico;
            delega.Lavoratore.Residenza.Cap = indirizzo.Cap;
            delega.Lavoratore.Residenza.Comune = indirizzo.Comune;
            delega.Lavoratore.Residenza.Provincia = indirizzo.Provincia;

            delega.Cantiere.IndirizzoDenominazione = TextBoxCantiereIndirizzo.Text.Trim().ToUpper();
            //delega.Cantiere.Civico = TextBoxCantiereCivico.Text.Trim().ToUpper();
            delega.Cantiere.Provincia = TextBoxCantiereProvincia.Text.Trim().ToUpper();
            delega.Cantiere.Comune = TextBoxCantiereComune.Text.Trim().ToUpper();
            delega.Cantiere.Cap = TextBoxCantiereCap.Text.Trim().ToUpper();

            //Sindacalista sindacalista =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista) HttpContext.Current.User.Identity).Entity;
            Sindacalista sindacalista =
                (Sindacalista)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            delega.ComprensorioSindacale = new ComprensorioSindacale { Id = DropDownListComprensorio.SelectedItem.Value };

            delega.Sindacato = sindacalista.Sindacato;
            delega.OperatoreTerritorio = TextBoxRecuperoDelega.Text.Trim().ToUpper();
            delega.OperatoreInserimento = sindacalista;

            //Replace spazi multipli con singolo spazio (richiesta malata del 10/07/2012)
            delega.Lavoratore.Cognome = Regex.Replace(delega.Lavoratore.Cognome, @"\s+", " ");
            delega.Lavoratore.Nome = Regex.Replace(delega.Lavoratore.Nome, @"\s+", " ");

            if (ViewState["dataAdesione"] != null)
            {
                delega.DataAdesione = (DateTime)ViewState["dataAdesione"];
            }

            return delega;
        }

        protected void ButtonReset_Click(object sender, EventArgs e)
        {
            SvuotaCampi();
        }

        private void SvuotaCampi()
        {
            TextBoxCodiceChar.Text = string.Empty;
            TextBoxCodiceNumber.Text = string.Empty;
            TextBoxRecuperoDelega.Text = string.Empty;
            TextBoxCognome.Text = string.Empty;
            TextBoxNome.Text = string.Empty;
            TextBoxDataNascita.Text = string.Empty;
            TextBoxImpresa.Text = string.Empty;
            TextBoxImpresaCodice.Text = string.Empty;
            TextBoxCellulare.Text = string.Empty;

            //TextBoxResidenzaIndirizzo.Text = string.Empty;
            //TextBoxResidenzaProvincia.Text = string.Empty;
            //TextBoxResidenzaComune.Text = string.Empty;
            //TextBoxResidenzaCap.Text = string.Empty;

            SelezioneIndirizzoDeleghe1.Reset();

            TextBoxCantiereIndirizzo.Text = string.Empty;
            TextBoxCantiereProvincia.Text = string.Empty;
            TextBoxCantiereComune.Text = string.Empty;
            TextBoxCantiereCap.Text = string.Empty;

            LabelErrore.Visible = false;
            LabelGiaConfermate.Visible = false;
            LabelResidenzaIncompleta.Visible = false;
        }

        protected void ButtonForza_Click(object sender, EventArgs e)
        {
            Delega delega = CreaDelega();
            delega.CodiceSblocco = _biz.GeneraCodiceSbloccoDelega();

            if (_biz.InsertUpdateDelega(delega))
                Response.Redirect("~/CeServizi/Deleghe/InserimentoDelegaBloccata.aspx");
            else
                LabelErrore.Visible = true;
        }

        protected void ButtonForzaAnnulla_Click(object sender, EventArgs e)
        {
            ProponiForzatura(false);
        }
        
    }
}