﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="InserimentoDelegaSuccesso.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.InserimentoDelegaSuccesso" %>

<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento effettuato"
        titolo="Deleghe sindacali" />
    <br />
    La delega è stata correttamente inserita nel sistema
</asp:Content>
