﻿using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.Deleghe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class ConfermaConfermaDeleghe : System.Web.UI.Page
    {
        private readonly DelegheBusiness biz = new DelegheBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione);

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.GetPostBackEventReference(ButtonConferma));
            sb.Append(";");
            ButtonConferma.Attributes.Add("onclick", sb.ToString());

            if (!Page.IsPostBack)
            {
                //Sindacalista sindacalista =
                //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista) HttpContext.Current.User.Identity).
                //        Entity;
                Sindacalista sindacalista =
                    (Sindacalista)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                if (biz.ApertaFaseInserimento())
                {
                    if (
                        !biz.DelegheConfermate(DateTime.Now.Month, DateTime.Now.Year, sindacalista.Sindacato,
                                               sindacalista.ComprensorioSindacale) &&
                        sindacalista.ComprensorioSindacale != null)
                    {
                        int delegheNonConfermate = biz.EsistonoDelegheNonConfermate(sindacalista.Sindacato,
                                                                                    sindacalista.ComprensorioSindacale);
                        if (delegheNonConfermate > 0)
                        {
                            PanelConferma.Visible = true;
                            PanelErrore.Visible = false;

                            LabelSindacato.Text = sindacalista.Sindacato.Descrizione;
                            LabelComprensorio.Text = sindacalista.ComprensorioSindacale.Descrizione;
                            LabelNumeroDeleghe.Text = delegheNonConfermate.ToString();
                        }
                        else
                        {
                            PanelConferma.Visible = false;
                            PanelErrore.Visible = true;

                            LabelErrore.Text = "Non è stata inserita nessuna delega.";
                        }
                    }
                    else
                    {
                        PanelConferma.Visible = false;
                        PanelErrore.Visible = true;

                        if (sindacalista.ComprensorioSindacale != null)
                        {
                            LabelErrore.Text =
                                String.Format("Le deleghe del {0} sono già state confermate per questo mese.",
                                              sindacalista.ComprensorioSindacale.Descrizione);
                        }
                    }
                }
                else
                {
                    PanelConferma.Visible = false;
                    PanelErrore.Visible = true;

                    LabelErrore.Text = "In questo periodo non è possibile la conferma delle deleghe.";
                }
            }
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            //Sindacalista sindacalista =
            //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Sindacalista) HttpContext.Current.User.Identity).Entity;
            Sindacalista sindacalista =
                (Sindacalista)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            if (biz.ConfermaDeleghe(sindacalista.Sindacato, sindacalista.ComprensorioSindacale, StatoDelega.InAttesa))
            {
                LabelErroreConferma.Visible = false;
                Response.Redirect(
                    String.Format("~/CeServizi/Deleghe/EsportazioneDeleghe.aspx?data={0}", DateTime.Now.ToString("MM/yyyy")));
            }
            else
            {
                LabelErroreConferma.Visible = true;
            }
        }
    }
}