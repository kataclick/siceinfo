﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="InserimentoDelegaBloccata.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.InserimentoDelegaBloccata" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento effettuato"
        titolo="Deleghe sindacali"  />
    <br />
    La delega è stata inserita nel sistema e bloccata. Per sbloccarla contattare Cassa
    Edile di Milano per avere il codice di sblocco.
</asp:Content>
