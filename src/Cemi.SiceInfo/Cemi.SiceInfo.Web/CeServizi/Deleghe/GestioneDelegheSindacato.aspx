﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneDelegheSindacato.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.GestioneDelegheSindacato" %>

<%@ Register Src="WebControls/DelegheDettaglioDelega.ascx" TagName="DelegheDettaglioDelega"
    TagPrefix="uc4" %>

<%@ Register Src="WebControls/DelegheRicercaDeleghe.ascx" TagName="DelegheRicercaDeleghe"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Visualizzazione"
        titolo="Gestione Deleghe" />
    <br />
    <asp:Panel ID="PanelRicerca" runat="server" width="100%">
            <asp:Label ID="LabelInformazioniNormale" runat="server">Con la funzione "Ricerca" è possibile visualizzare la delega da modificare o cancellare. L’icona con il foglio e la matita collocata alla destra del video posta in corrispondenza della delega ricercata ne consente la modifica, mentre l’icona con il pallino rosso e la crocetta ne permette la cancellazione.</asp:Label>
            <asp:Label ID="LabelInformazioniStorico" runat="server" Visible="False">Cliccando sull’icona del foglio con la matita viene visualizzato il dettaglio della delega.</asp:Label>
            <br />
            <br />
            <asp:Panel ID="PanelLegenda" runat="server" Visible="false" width="100%">
                
                        <asp:LinkButton ID="LinkButtonMostraLegenda" runat="server" Text="Mostra/Nascondi legenda Stati" OnClick="LinkButtonMostraLegenda_Click"></asp:LinkButton>
                        <br />
                        <asp:Panel ID="PanelPuntiLegenda" runat="server" Visible="false" width="100%">
                            <ul>
                                <li>
                                    InAttesa (la delega è in attesa di essere valutata da Cassa Edile di Milano)
                                </li>
                                <li>
                                    NonCorretta (la delega arrivata a Cassa Edile Milano in formato cartaceo non corrisponde a quella inserita a sistema)
                                </li>
                                <li>
                                    NoCartaceo (il formato cartaceo non è stato trovato tra le deleghe arrivate alla Cassa Edile di Milano)
                                </li>
                                <li>
                                    RicevutaCE (il formato cartaceo della delega è stato ricevuto da Cassa Edile di Milano e corrisponde alla delega inserita a sistema ed è in fase di valutazione)
                                </li>
                                <li>
                                    Confermata (la delega è valida)
                                </li>
                                <li>
                                    Rifiutata (la delega è stata rifiutata perché il lavoratore risulta già iscritto)
                                </li>
                                <li>
                                    Bloccata (la delega è stata bloccata in attesa dell’iscrizione del lavoratore presso Cassa Edile di Milano)
                                </li>
                                <li>
                                    Sospesa (la delega è stata presentata per lo stesso mese e per lo stesso lavoratore da più Organizzazioni Sindacali)
                                </li>
                                <li>
                                    Scaduta (la delega è rimasta bloccata per più di 12 mesi)
                                </li>
                                <li>
                                    Cessata (trascorsi 60 mesi dall’ultima denuncia riguardante il lavoratore oggetto della delega, quest’ultima viene cessata)
                                </li>
                            </ul>
                        </asp:Panel>
                    
            </asp:Panel>
            <uc3:DelegheRicercaDeleghe id="DelegheRicercaDeleghe1" runat="server">
            </uc3:DelegheRicercaDeleghe>
            <br />
    </asp:Panel>
    <asp:Panel ID="PanelMessaggio" runat="server" width="100%">
        <asp:Label ID="LabelMessaggio" runat="server"></asp:Label></asp:Panel>
    
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelDettagli" runat="server" Visible="False" width="100%">
        <br />
        <uc4:DelegheDettaglioDelega ID="DelegheDettaglioDelega1" runat="server" />
        <br />
    </asp:Panel>
</asp:Content>


