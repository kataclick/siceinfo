﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="InserimentoDelega.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Deleghe.InserimentoDelega" %>

<%@ Register Src="../WebControls/MenuDeleghe.ascx" TagName="MenuDeleghe" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/DelegheRicercaLavoratore.ascx" TagName="DelegheRicercaLavoratore"
    TagPrefix="uc3" %>
<%@ Register Src="WebControls/SelezioneIndirizzoDeleghe.ascx" TagName="SelezioneIndirizzoDeleghe" TagPrefix="uc4" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuDeleghe ID="MenuDeleghe1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Inserimento delega"
        titolo="Deleghe sindacali" />
    <br />
    <asp:Panel ID="PanelInserimentoCompleto" runat="server" width="100%">
        <asp:Panel ID="PanelBloccoCampiForzatura" runat="server" Visible="true" width="100%">
        Cliccare sul comando "Cerca lavoratore in anagrafica" per la ricerca e l’acquisizione automatica dei dati personali dei lavoratori già registrati nel database Cassa Edile di Milano oppure compilare i campi sotto riportati.
        <br />
        <br />
        Premere "Inserisci"/"Modifica" per confermare i campi compilati oppure "Reset" per la cancellazione dei dati inseriti.
        <asp:Button ID="ButtonRicercaAnagrafica" runat="server" OnClick="ButtonRicercaAnagrafica_Click"
            Text="Cerca lavoratore in anagrafica" /><br />
        <uc3:DelegheRicercaLavoratore ID="DelegheRicercaLavoratore1" runat="server" Visible="false" />
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Dati delega"></asp:Label>
        <br />
        <asp:Panel ID="PanelInserimento" runat="server" DefaultButton="ButtonInserisciDelega" width="100%">
        <table class="standardTable">
            <tr>
                <td>
                    Comprensorio*:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListComprensorio" runat="server" Width="400px" AppendDataBoundItems="True">
                    </asp:DropDownList></td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorComprensorio" runat="server" ControlToValidate="DropDownListComprensorio"
                        ErrorMessage="Comprensorio non selezionato" ValidationGroup="inserimento">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td>
                    Delega da:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxRecuperoDelega" runat="server" Width="400px"> </asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr style="vertical-align:middle">
                <td>
                    Codice Delega*:
                </td>
                <td style="vertical-align:middle">
                    MI - <asp:TextBox ID="TextBoxCodiceNumber" runat="server" Width="70px" MaxLength="5" /> - <asp:TextBox ID="TextBoxCodiceChar" runat="server" Width="50px" MaxLength="2" />
                </td>
                <td style="text-align:left">
                    <%--<asp:CustomValidator ID="CustomValidatorCodiceNumber" runat="server" ControlToValidate="TextBoxCodiceNumber" ErrorMessage="<i>Codice delega</i>: codice (numerico) mancante" OnServerValidate="CustomValidatorCodiceNumber_ServerValidate">*</asp:CustomValidator>--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceNumber" runat="server" ControlToValidate="TextBoxCodiceNumber"
                        ErrorMessage="<i>Codice delega</i>: codice (numerico) mancante" ValidationGroup="inserimento"><span style="color:red">*</span></asp:RequiredFieldValidator><asp:RegularExpressionValidator 
                            ID="RegularExpressionCodiceNumber" 
                            runat="server" 
                            ErrorMessage="<i>Codice delega</i>: sono ammessi solo <b>5 caratteri</b> numerici" 
                            ValidationGroup="inserimento"
                            ControlToValidate="TextBoxCodiceNumber" 
                            ValidationExpression="^\d{5,}$" ><span style="color:red">*</span></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceChar" runat="server" ControlToValidate="TextBoxCodiceChar"
                        ErrorMessage="<i>Codice delega</i>: codice (alfabetco) mancante" ValidationGroup="inserimento"><span style="color:red">*</span></asp:RequiredFieldValidator><asp:RegularExpressionValidator 
                            ID="RegularExpressionCodiceChar" 
                            runat="server" 
                            ErrorMessage="<i>Codice delega</i>: sono ammessi solo <b>2 caratteri</b> alfabetici" 
                            ValidationGroup="inserimento"
                            ControlToValidate="TextBoxCodiceChar" 
                            ValidationExpression="^[A-Za-z]{2,}$" ><span style="color:red">*</span></asp:RegularExpressionValidator>                    
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    Cognome*:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCognome" runat="server" MaxLength="30" Width="400"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognome" runat="server" ErrorMessage="Cognome mancante" ControlToValidate="TextBoxCognome"  ValidationGroup="inserimento" ><span style="color:red">*</span></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Nome*:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxNome" runat="server" MaxLength="30" Width="400"></asp:TextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server" ErrorMessage="Nome mancante" ControlToValidate="TextBoxNome"  ValidationGroup="inserimento" ><span style="color:red">*</span></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Data di nascita (gg/mm/aaaa)*:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxDataNascita" runat="server" MaxLength="10" Width="400"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataNascita" runat="server" ErrorMessage="Data di nascita non valida" ValidationGroup="inserimento" ControlToValidate="TextBoxDataNascita" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"><span style="color:red">*</span></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataNascita" runat="server" ErrorMessage="Data di nascita mancante" ControlToValidate="TextBoxDataNascita"  ValidationGroup="inserimento" ><span style="color:red">*</span></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Cellulare:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCellulare" runat="server" MaxLength="20" Width="400"></asp:TextBox>
                </td>
                <td>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxCellulare"
                        ErrorMessage="Cellulare non valido, immettere il numero telefonico senza caratteri speciali"
                        ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="inserimento"><span style="color:red">*</span></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td>
                    Codice impresa:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxImpresaCodice" runat="server" MaxLength="6" Width="400"></asp:TextBox>
                </td>
                <td>
                    <asp:RangeValidator
                        ID="RangeValidatorImpresaCodice"
                        runat="server"
                        ControlToValidate="TextBoxImpresaCodice"
                        MinimumValue="1"
                        MaximumValue="999999"
                        Type="Integer"
                        ErrorMessage="Codice impresa non valido"
                        ValidationGroup="inserimento"><span style="color:red">*</span></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Ragione sociale impresa:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxImpresa" runat="server" MaxLength="255" Width="400"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <uc4:SelezioneIndirizzoDeleghe ID="SelezioneIndirizzoDeleghe1" runat="server" />
                </td>
            </tr>
                    <%--<table class="standardTable">
                        <tr>
                            <td>
                                Indirizzo:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxResidenzaIndirizzo" runat="server" MaxLength="67" Width="300"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Provincia:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxResidenzaProvincia" runat="server" MaxLength="2" Width="300"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Comune:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxResidenzaComune" runat="server" MaxLength="66" Width="300"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cap:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxResidenzaCap" runat="server" MaxLength="5" Width="300"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RegularExpressionValidator id="RegularExpressionValidatorResidenzaCap" runat="server" ErrorMessage="Cap di residenza non valido" ValidationGroup="inserimento" ControlToValidate="TextBoxResidenzaCap" ValidationExpression="^\d{5}$">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    Cantiere</td>
                <td>
                    <table class="standardTable">
                        <tr>
                            <td>
                                Indirizzo:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCantiereIndirizzo" runat="server" MaxLength="67" Width="300"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Provincia:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCantiereProvincia" runat="server" MaxLength="2" Width="300"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Comune:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCantiereComune" runat="server" MaxLength="66" Width="300"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cap:
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxCantiereCap" runat="server" MaxLength="5" Width="300"></asp:TextBox>
                            </td>
                            <td>
                                <asp:RegularExpressionValidator id="RegularExpressionValidatorCantiereCap" runat="server" ErrorMessage="Cap del cantiere non valido" ValidationGroup="inserimento" ControlToValidate="TextBoxCantiereCap" ValidationExpression="^\d{5}$"><span style="color:red">*</span></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    * campi obbligatori
                </td>
            </tr>
            <tr>
                <td>
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:Button ID="ButtonInserisciDelega" runat="server" Text="Inserisci" ValidationGroup="inserimento" OnClick="ButtonInserisciDelega_Click" Width="156px" />   
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="ButtonReset" runat="server" Text="Reset" CausesValidation="false" Width="156px" OnClick="ButtonReset_Click" />   
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="2">
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummaryErrori" runat="server" ValidationGroup="inserimento" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante l'operazione." Visible="False"></asp:Label>
                                <asp:Label ID="LabelNonInseribile" runat="server" ForeColor="Red" Text="Impossibile effettuare la modifica. Nominativo già presente per questo mese." Visible="False"></asp:Label>
                                <asp:Label ID="LabelResidenzaIncompleta" runat="server" ForeColor="Red" Text="Indirizzo di residenza <b>non completo</b> o <b>non geocodificato</b>." Visible="false"></asp:Label>
                                <asp:BulletedList ID="BulletedListErrori" runat="server" CssClass="messaggiErrore" ForeColor="Red" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </asp:Panel>
        </asp:Panel>       
            
        <asp:Panel ID="PanelForzatura" runat="server" Visible="false" width="100%">
            
            <br />
            Se si è sicuri che si tratti di un caso di omonimia, selezionare "Forza per omonimia" e rivolgersi a Cassa Edile di Milano per ottenere il codice di sblocco per la delega riferita al lavoratore in oggetto; oppure "Annulla" per tornare all’inserimento.
            <br />
            <br />
            <asp:Button ID="ButtonForza" runat="server" Text="Forza per omonimia" Width="156px" OnClick="ButtonForza_Click" />
            <asp:Button ID="ButtonForzaAnnulla" runat="server" Text="Annulla" Width="156px" OnClick="ButtonForzaAnnulla_Click" />
            <br />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="PanelGiaConfermate" runat="server" width="100%">
        <asp:Label ID="LabelGiaConfermate" runat="server" Text=""></asp:Label>
    </asp:Panel>
</asp:Content>


