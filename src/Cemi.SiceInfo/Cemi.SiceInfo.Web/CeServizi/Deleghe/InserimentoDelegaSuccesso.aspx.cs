﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class InserimentoDelegaSuccesso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestione);
        }
    }
}