﻿using System;
using System.Configuration;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Enums.Deleghe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class GestioneDelegheCE : System.Web.UI.Page
    {
        private readonly DelegheBusiness biz = new DelegheBusiness();
        private string RADICESITO = "http://88.36.100.165";
        //private IDymoAddIn5 DymoAddIn;
        //private IDymoLabels DymoLabels;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestioneCE);

            DelegheRicercaDeleghe1.OnDelegaSelected += DelegheRicercaDeleghe1_OnDelegaSelected;
            DelegheRicercaDeleghe1.OnDelegaSearched += DelegheRicercaDeleghe1_OnDelegaSearched;

            ButtonStampaEtichetta.Attributes.Add("onclick", "return PrintBtnClicked();");
            ButtonRicevutaCE.Attributes.Add("onclick", "return PrintBtnClicked();");

            if (ConfigurationManager.AppSettings["IndirizzoSito"] != null)
                RADICESITO = ConfigurationManager.AppSettings["IndirizzoSito"];

            //try
            //{
            //    DymoAddIn = new DymoAddInClass();
            //    DymoLabels = new DymoLabelsClass();

            //    DymoAddIn.SelectPrinter(DymoAddIn.GetCurrentPrinterName());
            //    DymoAddIn.Open(Server.MapPath("~/Deleghe/FormatoEtichette/delegaCE.LWL"));

            //    LabelStampaEtichette.Visible = false;
            //}
            //catch 
            //{
            //    LabelStampaEtichette.Visible = true;
            //}

            //ButtonStampaEtichetta.Visible = !LabelStampaEtichette.Visible;
        }

        private void DelegheRicercaDeleghe1_OnDelegaSearched()
        {
            PanelDettagli.Visible = false;
        }

        private void DelegheRicercaDeleghe1_OnDelegaSelected(Delega delega)
        {
            PanelDettagli.Visible = true;
            LabelErrore.Text = null;

            DelegheDatiDelega1.CaricaDelega(delega);
            TextBoxId.Text = delega.IdDelega.ToString();
            if (delega.IdDelega.HasValue)
                inputId.Value = delega.IdDelega.Value.ToString("D9");
            inputFileEtichetta.Value = String.Format("{0}{1}", RADICESITO,
                                                     ResolveUrl("~/CeServizi/Deleghe/FormatoEtichette/DelegaCE.LWL"));
            inputStampata.Value = delega.Stampata.ToString();

            DateTime meseConferma = delega.DataConferma.Value;
            DateTime meseConfermaNorm = new DateTime(meseConferma.Year, meseConferma.Month, 1);
            meseConfermaNorm = meseConfermaNorm.AddMonths(1);
            meseConfermaNorm = meseConfermaNorm.AddDays(-1);
            inputMeseConferma.Value = meseConfermaNorm.ToShortDateString();

            SelezionaOperazioniEffettuabili(delega.Stato.Value);

            RegisterAnchorScript();
        }

        private void SelezionaOperazioniEffettuabili(StatoDelega stato)
        {
            switch (stato)
            {
                case StatoDelega.InAttesa:
                    StatoBottoni(true);
                    break;
                default:
                    StatoBottoni(false);
                    break;
            }
        }

        private void StatoBottoni(bool stato)
        {
            ButtonNoCartaceo.Visible = stato;
            ButtonNonCorretta.Visible = stato;
            ButtonRicevutaCE.Visible = stato;
        }

        protected void ButtonNonCorretta_Click(object sender, EventArgs e)
        {
            int idDelega = Int32.Parse(TextBoxId.Text);

            if (!biz.CambiaStatoDelega(idDelega, StatoDelega.NonCorretta))
            {
                LabelErrore.Text = "Errore durante il cambio dello stato";
            }
            else
            {
                LabelErrore.Text = null;
                DelegheDatiDelega1.ImpostaStato(StatoDelega.NonCorretta.ToString());
                StatoBottoni(false);
                DelegheRicercaDeleghe1.ForzaRicerca();
                PanelDettagli.Visible = false;
                LabelErrore.Text = "Stato modificato con successo";
            }
        }

        protected void ButtonRicevutaCE_Click(object sender, EventArgs e)
        {
            int idDelega = Int32.Parse(TextBoxId.Text);

            if (!biz.CambiaStatoDelega(idDelega, StatoDelega.RicevutaCE))
            {
                LabelErrore.Text = "Errore durante il cambio dello stato";
            }
            else
            {
                LabelErrore.Text = null;
                DelegheDatiDelega1.ImpostaStato(StatoDelega.RicevutaCE.ToString());
                DelegheRicercaDeleghe1.ForzaRicerca();
                PanelDettagli.Visible = false;
                LabelErrore.Text = "Stato modificato con successo";
            }
        }

        private void RegisterAnchorScript()
        {
            //Script for Bookmark Positioning
            StringBuilder Script = new StringBuilder();
            //Script.Append("<script language=JavaScript id='BookMarkScript'>");
            //Script.Append("var hashValue='#PanelDettagli';");
            //Script.Append("if(location.hash!=hashValue) ");
            //Script.Append("location.hash=hashValue;");
            //Script.Append("</script>");
            Script.Append("<script type=\"text/javascript\">document.location=\"#APanelDettagli\";</script>");
            if (!ClientScript.IsStartupScriptRegistered("BookMarkScript"))
                ClientScript.RegisterStartupScript(typeof(Page), "BookMarkScript", Script.ToString());
        }

        protected void ButtonNoCartaceo_Click(object sender, EventArgs e)
        {
            int idDelega = Int32.Parse(TextBoxId.Text);

            if (!biz.CambiaStatoDelega(idDelega, StatoDelega.NoCartaceo))
            {
                LabelErrore.Text = "Errore durante il cambio dello stato";
            }
            else
            {
                LabelErrore.Text = null;
                DelegheDatiDelega1.ImpostaStato(StatoDelega.NoCartaceo.ToString());
                DelegheRicercaDeleghe1.ForzaRicerca();
                PanelDettagli.Visible = false;
                LabelErrore.Text = "Stato modificato con successo";
            }
        }

        //protected void ButtonStampaEtichetta_Click(object sender, EventArgs e)
        //{
        //    //if (DymoLabels != null && DymoAddIn != null)
        //    //{
        //    //    int idDelega = Int32.Parse(LabelId.Text);

        //    //    DymoLabels.SetField("data", DateTime.Now.ToShortDateString());
        //    //    DymoLabels.SetField("codiceBarre", idDelega.ToString("D12"));

        //    //    try
        //    //    {
        //    //        // ATTENTION: This call is very important if you're making mutiple calls to the Print() or Print2() function!
        //    //        // It's a good idea to always wrap StartPrintJob() and EndPrintJob() around a call to Print() or Print2() function.
        //    //        DymoAddIn.StartPrintJob();

        //    //        // print two copies
        //    //        DymoAddIn.Print(1, false);

        //    //        // ATTENTION: Every StartPrintJob() must have a matching EndPrintJob()
        //    //        DymoAddIn.EndPrintJob();

        //    //        LabelErrore.Text = string.Empty;
        //    //    }
        //    //    catch
        //    //    {
        //    //        LabelErrore.Text = "Si è verificato un errore durante la stampa";
        //    //    }
        //    //}
        //}

        protected void ButtonStampaEtichetta_Click(object sender, EventArgs e)
        {
            Int32 idDelega = Int32.Parse(TextBoxId.Text);
            biz.ImpostaStampa(idDelega);
        }
    }
}