﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type;
using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Enums.Deleghe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Deleghe;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class GenerazioneLettera : System.Web.UI.Page
    {
        private readonly DelegheBusiness biz = new DelegheBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheGestioneCE);

            if (!Page.IsPostBack)
            {
                CaricaComprensori();
                CaricaSindacati();
                LabelError.Text = string.Empty;
            }
        }

        private void CaricaComprensori()
        {
            DropDownListComprensorio.Items.Clear();

            DropDownListComprensorio.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListComprensorio.DataSource = biz.GetComprensori(true);
            DropDownListComprensorio.DataTextField = "Descrizione";
            DropDownListComprensorio.DataValueField = "Id";
            DropDownListComprensorio.DataBind();
        }

        private void CaricaSindacati()
        {
            DropDownListSindacato.Items.Clear();

            DropDownListSindacato.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListSindacato.DataSource = biz.GetSindacati();
            DropDownListSindacato.DataTextField = "Descrizione";
            DropDownListSindacato.DataValueField = "Id";
            DropDownListSindacato.DataBind();
        }

        private void ResponseWord(TipologiaLettera tipologiaLettera, string fileWord)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}.docx", tipologiaLettera));
            Response.ContentType = "application/vnd.ms-word";
            Response.WriteFile(fileWord);
            Response.Flush();
            Response.End();
        }

        private void ResponseExcel(TipologiaLettera tipologiaLettera, string excelString)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.xls", tipologiaLettera));
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(excelString);
            Response.End();
        }

        private static string PreparaExcel(List<Delega> deleghe, TipologiaLettera tipologiaLettera)
        {
            GridView gv = new GridView();
            gv.ID = "gvDeleghe";
            gv.AutoGenerateColumns = false;
            gv.RowDataBound += gv_RowDataBound;

            TemplateField tc0 = new TemplateField();
            tc0.HeaderText = "Progressivo";

            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Sindacato";
            bc1.DataField = "Sindacato";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Comprensorio";
            bc2.DataField = "ComprensorioSindacale";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "Lavoratore";
            bc3.DataField = "NomeLavoratore";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Data nascita";
            bc4.DataField = "DataNascitaLavoratore";
            bc4.DataFormatString = "{0:dd/MM/yyyy}";
            bc4.HtmlEncode = false;
            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Impresa";
            bc5.DataField = "ImpresaLavoratore";
            BoundField bc6 = new BoundField();
            bc6.HeaderText = "Data adesione";
            bc6.DataField = "DataAdesione";
            bc6.DataFormatString = "{0:dd/MM/yyyy}";
            bc6.HtmlEncode = false;

            gv.Columns.Add(tc0);
            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            if (tipologiaLettera == TipologiaLettera.NonIscritti)
            {
                //devo aggiungere motivo
                BoundField bc7 = new BoundField();
                bc7.HeaderText = "Motivazione";
                bc7.DataField = "Stato";
                gv.Columns.Add(bc7);
            }

            gv.DataSource = deleghe;
            gv.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            return sw.ToString();
        }

        private static void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Text = (e.Row.RowIndex + 1).ToString();
            }
        }

        private List<Delega> GetDelegheScadute()
        {
            DelegheFilter filtro = new DelegheFilter();
            filtro.Sindacato = DropDownListSindacato.SelectedValue;
            filtro.ComprensorioSindacale = DropDownListComprensorio.SelectedValue;
            filtro.Stato = StatoDelega.Scaduta;
            filtro.Confermate = true;
            //Common common = new Common();
            //int mesi = common.GetMesiDelegheScadute();
            //DateTime data = DateTime.Today.AddMonths(-mesi);
            //filtro.MeseConferma = new DateTime(data.Year, data.Month, 1).AddMonths(-1);
            DateTime data = DateTime.ParseExact(textBoxDataAdesione.Text, "MM/yyyy", null);
            filtro.MeseConferma = new DateTime(data.Year, data.Month, 1);

            return biz.GetDeleghe(filtro);
        }

        private List<Delega> GetDelegheNonIscritti()
        {
            List<Delega> deleghe = new List<Delega>();

            DelegheFilter filtro = new DelegheFilter();
            filtro.Sindacato = DropDownListSindacato.SelectedValue;
            filtro.ComprensorioSindacale = DropDownListComprensorio.SelectedValue;
            filtro.Confermate = true;
            //filtro.MeseConferma = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
            if (!String.IsNullOrEmpty(textBoxDataAdesione.Text))
            {
                DateTime data = DateTime.ParseExact(textBoxDataAdesione.Text, "MM/yyyy", null);
                filtro.MeseConferma = new DateTime(data.Year, data.Month, 1);
            }

            if (!String.IsNullOrEmpty(textBoxDataModificaStato.Text))
            {
                DateTime data = DateTime.ParseExact(textBoxDataModificaStato.Text, "MM/yyyy", null);
                filtro.MeseModificaStato = new DateTime(data.Year, data.Month, 1);
            }

            //filtro.Stato = StatoDelega.Bloccata;
            //deleghe.AddRange(biz.GetDeleghe(filtro));

            filtro.Stato = StatoDelega.Rifiutata;
            deleghe.AddRange(biz.GetDeleghe(filtro));

            filtro.Stato = StatoDelega.Sospesa;
            deleghe.AddRange(biz.GetDeleghe(filtro));

            filtro.Stato = StatoDelega.Annullata;
            deleghe.AddRange(biz.GetDeleghe(filtro));

            return deleghe;
        }

        #region Events

        protected void ButtonGeneraLetteraScadute_Click(object sender, EventArgs e)
        {
            List<Delega> deleghe = GetDelegheScadute();
            if (deleghe.Count > 0)
            {
                TipologiaLettera tipoLettera = TipologiaLettera.Scadute;
                LetteraParam param = new LetteraParam
                {
                    Protocollo = $"{textBoxProtocollo.Text}/DIR/gs",
                    Sindacato = DropDownListSindacato.SelectedValue,
                    ComprensorioSindacale = DropDownListComprensorio.SelectedValue,
                    DataAdesione = DateTime.ParseExact(textBoxDataAdesione.Text, "MM/yyyy", null).AddMonths(1).AddDays(-1)
                };
                
                string lettera = biz.GeneraLettera(tipoLettera, param);
                ResponseWord(tipoLettera, lettera);
            }
            else
            {
                LabelError.Text = "Non vi sono deleghe scadute";
            }
        }

        protected void ButtonGeneraAllegatoScadute_Click(object sender, EventArgs e)
        {
            List<Delega> deleghe = GetDelegheScadute();
            if (deleghe.Count > 0)
            {
                string excel = PreparaExcel(deleghe, TipologiaLettera.Scadute);
                ResponseExcel(TipologiaLettera.Scadute, excel);
            }
            else
            {
                LabelError.Text = "Non vi sono deleghe scadute";
            }
        }

        protected void ButtonGeneraLetteraNonIscritti_Click(object sender, EventArgs e)
        {
            List<Delega> deleghe = GetDelegheNonIscritti();
            if (deleghe.Count > 0)
            {
                TipologiaLettera tipoLettera = TipologiaLettera.NonIscritti;
                LetteraParam param = new LetteraParam();
                param.Protocollo = string.Format("{0}/DIR/gs", textBoxProtocollo.Text);
                param.Sindacato = DropDownListSindacato.SelectedValue;
                param.ComprensorioSindacale = DropDownListComprensorio.SelectedValue;
                //param.DataAdesione = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);
                //param.DataAdesione = DateTime.ParseExact(textBoxDataAdesione.Text, "MM/yyyy", null).AddMonths(1).AddDays(-1);
                //param.DataAdesione = DateTime.ParseExact(textBoxDataAdesione.Text, "MM/yyyy", null);
                //param.DataModificaStato = DateTime.ParseExact(textBoxDataModificaStato.Text, "MM/yyyy", null);

                if (!String.IsNullOrEmpty(textBoxDataAdesione.Text))
                {
                    DateTime data = DateTime.ParseExact(textBoxDataAdesione.Text, "MM/yyyy", null);
                    param.DataAdesione = new DateTime(data.Year, data.Month, 1);
                }

                if (!String.IsNullOrEmpty(textBoxDataModificaStato.Text))
                {
                    DateTime data = DateTime.ParseExact(textBoxDataModificaStato.Text, "MM/yyyy", null);
                    param.DataModificaStato = new DateTime(data.Year, data.Month, 1);
                }

                string lettera = biz.GeneraLettera(tipoLettera, param);
                ResponseWord(tipoLettera, lettera);
            }
            else
            {
                LabelError.Text = "Non vi sono deleghe non iscritte";
            }
        }

        protected void ButtonGeneraAllegatoNonIscritti_Click(object sender, EventArgs e)
        {
            List<Delega> deleghe = GetDelegheNonIscritti();
            if (deleghe.Count > 0)
            {
                string excel = PreparaExcel(deleghe, TipologiaLettera.NonIscritti);
                ResponseExcel(TipologiaLettera.NonIscritti, excel);
            }
            else
            {
                LabelError.Text = "Non vi sono deleghe non iscritte";
            }
        }

        #endregion
    }
}