﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business.Deleghe;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.Deleghe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Deleghe
{
    public partial class EstrazioneArchiDoc : System.Web.UI.Page
    {
        private readonly DelegheBusiness biz = new DelegheBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DelegheEstrazioneMetadati);
        }

        protected void ButtonGeneraFile_Click(object sender, EventArgs e)
        {
            StringBuilder fileTxt = new StringBuilder();
            List<Delega> deleghe = biz.GetDeleghePerArchiDoc();

            foreach (Delega delega in deleghe)
            {
                fileTxt.AppendLine(String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}",
                                                 delega.IdDelega,
                                                 delega.Lavoratore.Cognome,
                                                 delega.Lavoratore.Nome,
                                                 delega.Lavoratore.CodiceFiscale,
                                                 delega.Lavoratore.DataNascita.Value.ToShortDateString(),
                                                 delega.Sindacato.Id,
                                                 delega.ComprensorioSindacale.Id,
                                                 delega.DataConferma.Value.ToShortDateString(),
                                                 delega.Lavoratore.IdLavoratore,
                                                 delega.DataAdesione.Value.ToShortDateString(),
                                                 delega.CodiceDelega));
                                                 
            }

            Response.ContentType = "text";
            Response.AddHeader("Content-Disposition", "attachment; filename=deleghe.txt");
            Response.Charset = "";

            //Response.Write(fileTxt.ToString());

            byte[] ASCIIBytes = Encoding.ASCII.GetBytes(fileTxt.ToString());

            ASCIIEncoding enc = new ASCIIEncoding();
            string str = enc.GetString(ASCIIBytes);

            Response.Write(str);
            Response.End();
        }
    }
}