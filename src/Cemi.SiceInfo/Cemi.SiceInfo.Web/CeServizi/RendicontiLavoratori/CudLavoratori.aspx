﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CudLavoratori.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.RendicontiLavoratori.CudLavoratori" %>

<%@ register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Reportistica Lavoratore" sottoTitolo="Certificazione Unica (CU)" />
    <br />
    <asp:MultiView
        ID="MultiViewCud"
        runat="server">
        <asp:View
            ID="ViewCudPresente"
            runat="server">
            Il modello <b>CU</b> (Certificazione Unica dei redditi di lavoro dipendente ed assimilati, di lavoro autonomo e “redditi diversi”) dell'anno 
            <b>
                <asp:Label
                    ID="LabelAnnoDisponibile1"
                    runat="server"
                    CssClass="pippo">
                </asp:Label>
            </b>,
            relativo ai redditi percepiti nell'anno
            <asp:Label
                ID="LabelAnnoRedditi1"
                runat="server">
            </asp:Label>, è <b>disponibile</b>. 
            <br /><br />
            Clicca sul bottone "Scarica" per aprire, stampare e salvare il documento.
            <br /><br />
            <asp:Button
                ID="ButtonDownload"
                runat="server"
                Text="Scarica"
                Width="150px" OnClick="ButtonDownload_Click" />
            <br /><br />
            Si ricorda che la dichiarazione dei redditi è obbligatoria in caso di ricezione di più modelli CU rilasciati da diversi sostituti d’imposta (datore di lavoro, Cassa Edile, ecc.).
        </asp:View>
        <asp:View
            ID="ViewCudNonPresente"
            runat="server">
            Il modello <b>CU</b> (Certificazione Unica dei redditi di lavoro dipendente ed assimilati, di lavoro autonomo e “redditi diversi”) dell'anno 
            <b>
                <asp:Label
                    ID="LabelAnnoDisponibile2"
                    runat="server">
                </asp:Label>
            </b>,
            relativo ai redditi percepiti nell'anno
            <asp:Label
                ID="LabelAnnoRedditi2"
                runat="server">
            </asp:Label>,
            <b>non</b> è <b>disponibile</b> in quanto non hai ricevuto prestazioni o somme soggette a ritenuta IRPEF (es. Anzianità Professionale Edile, Premio di Fedeltà, ecc.).
        </asp:View>
    </asp:MultiView>
</asp:Content>
