﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cemi.SiceInfo.Web.CeServizi.RendicontiLavoratori {
    
    
    public partial class CudLavoratori {
        
        /// <summary>
        /// TitoloSottotitolo1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.TitoloSottotitolo TitoloSottotitolo1;
        
        /// <summary>
        /// MultiViewCud control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.MultiView MultiViewCud;
        
        /// <summary>
        /// ViewCudPresente control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ViewCudPresente;
        
        /// <summary>
        /// LabelAnnoDisponibile1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnnoDisponibile1;
        
        /// <summary>
        /// LabelAnnoRedditi1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnnoRedditi1;
        
        /// <summary>
        /// ButtonDownload control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonDownload;
        
        /// <summary>
        /// ViewCudNonPresente control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.View ViewCudNonPresente;
        
        /// <summary>
        /// LabelAnnoDisponibile2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnnoDisponibile2;
        
        /// <summary>
        /// LabelAnnoRedditi2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnnoRedditi2;
    }
}
