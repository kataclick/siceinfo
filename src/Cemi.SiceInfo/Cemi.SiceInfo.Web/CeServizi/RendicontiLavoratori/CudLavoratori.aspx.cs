﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.RendicontiLavoratori
{
    public partial class CudLavoratori : Page
    {
        private readonly CudManager _cudManager = new CudManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.InfoLavoratore);
            #endregion

            if (!Page.IsPostBack)
            {
                CaricaCud();
            }
        }

        private void CaricaCud()
        {
            Lavoratore lavoratore = (Lavoratore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            Cud cud = _cudManager.GetCud(lavoratore.IdLavoratore);

            if (cud != null)
            {
                if (!String.IsNullOrWhiteSpace(cud.IdArchidoc))
                {
                    LabelAnnoDisponibile1.Text = (cud.Anno + 1).ToString();
                    LabelAnnoRedditi1.Text = cud.Anno.ToString();
                    ViewState["IdArchidoc"] = cud.IdArchidoc;
                    ViewState["AnnoRedditi"] = cud.Anno;

                    MultiViewCud.SetActiveView(ViewCudPresente);
                }
                else
                {
                    LabelAnnoDisponibile2.Text = (cud.Anno + 1).ToString();
                    LabelAnnoRedditi2.Text = cud.Anno.ToString();

                    MultiViewCud.SetActiveView(ViewCudNonPresente);
                }
            }
        }

        protected void ButtonDownload_Click(object sender, EventArgs e)
        {
            String idArchidoc = (String)ViewState["IdArchidoc"];
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            Int32 annoRedditi = (Int32)ViewState["AnnoRedditi"];

            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            byte[] file = servizioArchidoc.GetDocument(idArchidoc);
            if (file != null)
            {
                _cudManager.CudScaricato(idUtente, annoRedditi);
                Presenter.RestituisciFileArchidoc(idArchidoc, file, this);
            }
        }
    }
}