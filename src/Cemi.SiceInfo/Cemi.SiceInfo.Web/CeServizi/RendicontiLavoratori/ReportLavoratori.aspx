﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportLavoratori.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.RendicontiLavoratori.ReportLavoratori" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%--<%@ Register Src="WebControls/MenuLavoratori.ascx" TagName="MenuLavoratori" TagPrefix="uc1" %>--%>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Rendiconti lavoratore" />
    <br />
    <table class="borderedTable" id="TableLabelOre" runat="server" visible="false">
        <tr>
            <td>
                <asp:Label ID="LabelOre" runat="server">
                    In questa pagina web puoi consultare le ore accantonate ovvero le ore di lavoro ordinario 
					prestate presso ogni singola impresa, con dettaglio del numero e della tipologia di ore 
					per periodo e l’indicazione dell’eventuale versamento.
					<p>Cliccare sul simbolo + riportato accanto alla ragione sociale dell’impresa per visualizzare i periodi di lavoro.<br/>
					Selezionare il simbolo + collocato accanto ad ogni singolo periodo per consultare le informazioni di dettaglio 
					sopra elencate (numero e tipologia di ore e indicazione del versamento).</p>
					<p><i>Le informazioni riportate hanno carattere puramente informativo</i></p> 

                </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelRapporto" runat="server" visible="false">
        <tr>
            <td>
                <asp:Label ID="LabelRapporto" runat="server">
                    In questa pagina web puoi visualizzare i rapporti di lavoro che hai avuto nel tempo con imprese iscritte a 
					Cassa Edile di Milano, Lodi, Monza e Brianza con indicazione di: data inizio/fine rapporto, 
					tipologia di contratto applicato, categoria, qualifica e mansione.<br/>
					Per quanto concerne l’ultimo rapporto di lavoro, ancora in corso, non è riportata la data di fine rapporto.
					<p>Cliccare sul simbolo + riportato accanto alla ragione sociale dell’impresa per visualizzare le informazioni di dettaglio sopra elencate.</p>
					<p><i>Le informazioni riportate hanno carattere puramente informativo</i></p>

                </asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable" id="TableLabelPrestazioni" runat="server" visible="false">
        <tr>
            <td>
                <asp:Label ID="LabelPrestazioni" runat="server">
                    In questa pagina web puoi verificare i pagamenti effettuati in tuo favore con dettaglio riferito a:
					causale (pagamento erogato a titolo di prestazione assistenziale, trattamento economico per 
					ferie e gratifica natalizia, Anzianità Professionale Edile), data e stato pagamento, 
					importo erogato lordo e netto, modalità e numero di pagamento.
					<p><i>Le informazioni riportate hanno carattere puramente informativo</i></p>
                </asp:Label>
            </td>
        </tr>
    </table>
    <table style="height:600pt; width:550pt;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerLavoratori" runat="server" OnInit="ReportViewerLavoratori_Init"
                    ProcessingMode="Remote" Height="550pt" Width="550pt" ShowParameterPrompts="false" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <%--<uc1:MenuLavoratori ID="MenuLavoratori1" runat="server" />--%>
</asp:Content>
