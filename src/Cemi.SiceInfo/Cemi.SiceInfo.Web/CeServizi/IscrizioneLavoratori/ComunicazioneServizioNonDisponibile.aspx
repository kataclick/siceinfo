﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ComunicazioneServizioNonDisponibile.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.ComunicazioneServizioNonDisponibile" %>

<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Errore comunicazione" />
    <br />
    <table class="standardTable">
        <tr>
            <td>Il servizio non è momentaneamente disponibile, riprovare più tardi
            </td>
        </tr>
    </table>
</asp:Content>
