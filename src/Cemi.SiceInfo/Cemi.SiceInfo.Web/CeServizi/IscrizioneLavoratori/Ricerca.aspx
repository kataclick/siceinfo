﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="Ricerca.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.Ricerca" %>

<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RicercaNotificaPubblica.ascx" TagName="RicercaNotificaPubblica"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/ConsulenteSelezioneImpresa.ascx" TagName="ConsulenteSelezioneImpresa"
    TagPrefix="uc4" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori"
        sottoTitolo="Ricerca" />
    <br />
    <uc4:ConsulenteSelezioneImpresa ID="ConsulenteSelezioneImpresa1" runat="server" />
    <br />
    <uc3:RicercaNotificaPubblica ID="RicercaNotificaPubblica1" runat="server" />
</asp:Content>