﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneCE.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.GestioneCE" %>

<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/RicercaNotifica.ascx" tagname="RicercaNotifica" tagprefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo 
        ID="TitoloSottotitolo1" 
        runat="server"
        titolo="Notifiche lavoratori"
        sottoTitolo="Gestione" />
    <br />
    <uc3:RicercaNotifica ID="RicercaNotifica1" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>