﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class ComunicazioneErrore : System.Web.UI.Page
    {
        private const string DEFAULT_ERROR_MESSAGE = "Il servizio non è momentaneamente disponibile, riprovare più tardi.";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request["ErrorMessage"]))
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception($"Exception for ComunicazioneErrore - {Request["ErrorMessage"]}"));
            }

            #region Autorizzazioni
            System.Collections.Generic.List<FunzionalitaPredefinite> autorizzazioni = new System.Collections.Generic.List<FunzionalitaPredefinite>();
            autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
            autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
            GestioneAutorizzazionePagine.PaginaAutorizzata(autorizzazioni);

            Utente utente = (Utente)Membership.GetUser();

            if (utente != null)
            {
                if (utente.IdUtente > 0)
                {
                    GestioneUtentiBiz biz = new GestioneUtentiBiz();
                    RuoliCollection ruoli = biz.GetRuoliUtente(utente.IdUtente);
                    if (ruoli.Count == 1 &&
                        (ruoli[0].Nome == "IscrizioneLavoratoreSintesi"))
                        MenuIscrizioneLavoratori1.Visible = false;
                }
            }
            #endregion

            String error = Context.Items["ErrorMessage"] as String;
            if (String.IsNullOrEmpty(error))
            {
                error = Request.QueryString["ErrorMessage"];
            }

            if (!String.IsNullOrEmpty(error))
            {
                if (error[error.Length - 1] != '.')
                {
                    error = String.Concat(error, ".");
                }

                labelError.Text = error.ToString();
            }
            else
            {
                labelError.Text = DEFAULT_ERROR_MESSAGE;
            }
        }
    }
}