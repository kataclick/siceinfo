﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class ComunicazioneImpresaErrata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Collections.Generic.List<FunzionalitaPredefinite> autorizzazioni = new System.Collections.Generic.List<FunzionalitaPredefinite>();
            autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
            autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
            GestioneAutorizzazionePagine.PaginaAutorizzata(autorizzazioni);
        }
    }
}