﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.IscrizioneLavoratori.Type.Exceptions;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class DettaglioDichiarazione : System.Web.UI.Page
    {
        private readonly IscrizioneLavoratoriManager manager = new IscrizioneLavoratoriManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE);

            #endregion

            //ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDichiarazione"] != null)
                {
                    Int32 idDichiarazione = (Int32)Context.Items["IdDichiarazione"];
                    CaricaDichiarazione(idDichiarazione);
                }
                else
                {
                    Response.Redirect("~/CeServizi/IscrizioneLavoratori/GestioneCE.aspx");
                }
            }
        }

        private void CaricaDichiarazione(Int32 idDichiarazione)
        {
            Boolean controlliLavoratore = false;
            Boolean controlliIndirizzo = false;
            Boolean controlliRapporto = false;
            Dichiarazione dichiarazione = manager.GetDichiarazione(idDichiarazione);

            ViewState["IdDichiarazione"] = idDichiarazione;
            ViewState["Stato"] = dichiarazione.Stato;
            AbilitazionePulsantiCambioStato(dichiarazione.Stato);

            RiepilogoDettagliDichiarazione1.CaricaDichiarazione(dichiarazione);
            controlliLavoratore = RiepilogoDettagliLavoratore1.CaricaDichiarazione(dichiarazione);
            RiepilogoDettagliImpresa1.CaricaDichiarazione(dichiarazione);
            controlliIndirizzo = RiepilogoDettagliIndirizzo1.CaricaDichiarazione(dichiarazione);
            controlliRapporto = RiepilogoDettagliRapportoDiLavoro1.CaricaDichiarazione(dichiarazione);

            if (dichiarazione.Consulente != null)
            {
                divConsulente.Visible = true;
                RiepilogoDettagliConsulente1.CaricaDichiarazione(dichiarazione);
            }
            else
            {
                divConsulente.Visible = false;
            }
            RiepilogoDettagliCantiere1.CaricaDichiarazione(dichiarazione);
            RiepilogoDettagliRapportoDiLavoro1.CaricaDichiarazione(dichiarazione);



            ButtonApprova.Enabled = ButtonApprova.Enabled
                                    && controlliLavoratore
                                    && controlliIndirizzo
                                    && (dichiarazione.Attivita == TipoAttivita.Assunzione || controlliRapporto);

            //if (dichiarazione.Attivita == TipoAttivita.Cessazione)
            //{
            //    ButtonApprova.Enabled = false;
            //    ButtonInAttesaDocumentazione.Enabled = false;
            //    ButtonRifiuta.Enabled = false;
            //    if (dichiarazione.Lavoratore.TipoLavoratore == TipologiaLavoratore.SiceNew)
            //        ButtonDettaglio.Enabled = false;
            //}
        }

        private void AbilitazionePulsantiCambioStato(TipoStatoGestionePratica tipoStatoGestionePratica)
        {
            switch (tipoStatoGestionePratica)
            {
                case TipoStatoGestionePratica.DaValutare:
                    ButtonApprova.Enabled = true;
                    ButtonRifiuta.Enabled = true;
                    ButtonInAttesaDocumentazione.Enabled = true;
                    ButtonDettaglio.Enabled = true;
                    break;
                case TipoStatoGestionePratica.Approvata:
                    ButtonApprova.Enabled = false;
                    ButtonRifiuta.Enabled = false;
                    ButtonInAttesaDocumentazione.Enabled = false;
                    ButtonDettaglio.Enabled = false;
                    break;
                case TipoStatoGestionePratica.Rifiutata:
                    ButtonApprova.Enabled = false;
                    ButtonRifiuta.Enabled = false;
                    ButtonInAttesaDocumentazione.Enabled = false;
                    ButtonDettaglio.Enabled = false;
                    break;
                case TipoStatoGestionePratica.InAttesaDiDocumentazione:
                    ButtonApprova.Enabled = true;
                    ButtonRifiuta.Enabled = true;
                    ButtonInAttesaDocumentazione.Enabled = false;
                    ButtonDettaglio.Enabled = true;
                    break;
            }
        }

        protected void ButtonDettaglio_Click(object sender, EventArgs e)
        {
            Context.Items["IdDichiarazione"] = ViewState["IdDichiarazione"];
            Server.Transfer("~/CeServizi/IscrizioneLavoratori/ControlliLavoratore.aspx");
        }

        protected void ButtonApprova_Click(object sender, EventArgs e)
        {
            CambiaStatoDichiarazione(TipoStatoGestionePratica.Approvata);
        }

        protected void ButtonInAttesaDocumentazione_Click(object sender, EventArgs e)
        {
            CambiaStatoDichiarazione(TipoStatoGestionePratica.InAttesaDiDocumentazione);
        }

        protected void ButtonRifiuta_Click(object sender, EventArgs e)
        {
            CambiaStatoDichiarazione(TipoStatoGestionePratica.Rifiutata);
        }

        private void CambiaStatoDichiarazione(TipoStatoGestionePratica nuovoStato)
        {
            Int32 idDichiarazione = (Int32)ViewState["IdDichiarazione"];
            TipoStatoGestionePratica stato = (TipoStatoGestionePratica)ViewState["Stato"];

            try
            {
                manager.CambiaStatoDichiarazione(idDichiarazione, stato, nuovoStato, GestioneUtentiBiz.GetIdUtente());
                LabelErroreCambioStato.Visible = false;
                CaricaDichiarazione(idDichiarazione);
            }
            catch (StatoGiaVariatoException exc)
            {
                LabelErroreCambioStato.Visible = true;
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/IscrizioneLavoratori/GestioneCE.aspx");

        }
    }
}