﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Utility;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.TuteScarpe.Data;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.Type.Entities;
using Telerik.Web.UI;
using Geocoding = TBridge.Cemi.Business.Cantieri.CantieriGeocoding;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class IscrizioneLavoratoreInformazioniAggiuntive : System.Web.UI.UserControl
    {
        private readonly TSDataAccess TSdata = new TSDataAccess();
        private readonly Common commonBiz = new Common();

        //public bool IscrizioneCorsiAbilitata
        //{
        //    get { return ViewState["IscrizioneCorsiAbilitata"] != null ? (bool)ViewState["IscrizioneCorsiAbilitata"] : true; }
        //    set { ViewState["IscrizioneCorsiAbilitata"] = value; }
        //}

        public bool IscrizioneCorsiAbilitata
        {
            get { return PanelIscrizioneCorsi.Visible; }
            set { PanelIscrizioneCorsi.Visible = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaCombo();
            }

            //PanelIscrizioneCorsi.Visible = this.IscrizioneCorsiAbilitata;
        }

        private void CaricaCombo()
        {
            CaricaTagliePantaloni();
            CaricaTaglieFelpaHusky();
            CaricaTaglieScarpe();
            CaricaSindacati();
            CaricaTipiCartaPrepagata();
        }

        private void CaricaTipiCartaPrepagata()
        {
            if (RadComboBoxTipologiaPrepagata.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDown(
                    RadComboBoxTipologiaPrepagata,
                    commonBiz.GetTipiCartaPrepagata(),
                    "Descrizione",
                    "IdTipoCartaPrepagata");

                RadComboBoxTipologiaPrepagata.Items.Insert(0, new RadComboBoxItem("- Selezionare la tipologia -"));
            }
        }

        private void CaricaTagliePantaloni()
        {
            TagliaList listaTaglie = TSdata.GetTaglieArticolo("PJ");

            Presenter.CaricaElementiInDropDown(
                RadComboBoxTagliaPantaloni,
                listaTaglie,
                "descrizioneTaglia",
                "idTaglia");
            RadComboBoxTagliaPantaloni.Items.Insert(0, new RadComboBoxItem("- Selezionare la Taglia -"));
        }

        private void CaricaTaglieFelpaHusky()
        {
            TagliaList listaTaglie = TSdata.GetTaglieArticolo("FE");

            Presenter.CaricaElementiInDropDown(
                RadComboBoxTagliaFelpaHusky,
                listaTaglie,
                "descrizioneTaglia",
                "idTaglia");
            RadComboBoxTagliaFelpaHusky.Items.Insert(0, new RadComboBoxItem("- Selezionare la Taglia -"));
        }

        private void CaricaTaglieScarpe()
        {
            TagliaList listaTaglie = TSdata.GetTaglieArticolo("SB");

            Presenter.CaricaElementiInDropDown(
                RadComboBoxTagliaScarpe,
                listaTaglie,
                "descrizioneTaglia",
                "idTaglia");
            RadComboBoxTagliaScarpe.Items.Insert(0, new RadComboBoxItem("- Selezionare la Misura -"));
        }

        private void CaricaSindacati()
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxSindacato,
                commonBiz.GetSindacati(),
                "descrizione",
                "id");
            RadComboBoxSindacato.Items.Insert(0, new RadComboBoxItem("- Selezionare il sindacato -"));
        }

        public void CaricaLavoratore(Lavoratore lavoratore)
        {
            PanelInformazioniAggiuntive.Enabled = false;
        }

        public void CompletaLavoratore(Lavoratore lavoratore)
        {
            if (!String.IsNullOrEmpty(RadTextBoxIBAN.Text))
            {
                lavoratore.IBAN = Presenter.NormalizzaCampoTesto(RadTextBoxIBAN.Text);
            }
            lavoratore.CoIntestato = (RadioButtonListCoIntestato.SelectedValue == "SI");
            if (!String.IsNullOrEmpty(RadTextBoxCognomeCointestatario.Text))
            {
                lavoratore.CognomeCointestatario = Presenter.NormalizzaCampoTesto(RadTextBoxCognomeCointestatario.Text);
            }
            lavoratore.RichiestaInfoCartaPrepagata = (RadioButtonListInfoPrepagata.SelectedValue == "SI");
            if (!String.IsNullOrEmpty(RadComboBoxTagliaFelpaHusky.SelectedValue))
            {
                lavoratore.IdTagliaFelpaHusky = Int32.Parse(RadComboBoxTagliaFelpaHusky.SelectedValue);
            }
            if (!String.IsNullOrEmpty(RadComboBoxTagliaPantaloni.SelectedValue))
            {
                lavoratore.IdTagliaPantaloni = Int32.Parse(RadComboBoxTagliaPantaloni.SelectedValue);
            }
            if (!String.IsNullOrEmpty(RadComboBoxTagliaScarpe.SelectedValue))
            {
                lavoratore.IdTagliaScarpe = Int32.Parse(RadComboBoxTagliaScarpe.SelectedValue);
            }
            if (!String.IsNullOrEmpty(RadComboBoxSindacato.SelectedValue))
            {
                lavoratore.IdSindacato = RadComboBoxSindacato.SelectedValue;
            }
            if (RadDateInputDataAdesione.SelectedDate != null)
            {
                lavoratore.DataAdesione = RadDateInputDataAdesione.SelectedDate;
            }
            if (RadDateInputDataDisdetta.SelectedDate != null)
            {
                lavoratore.DataDisdetta = RadDateInputDataDisdetta.SelectedDate;
            }
            if (!String.IsNullOrEmpty(RadTextBoxNumeroDelega.Text))
            {
                lavoratore.NumeroDelega = Presenter.NormalizzaCampoTesto(RadTextBoxNumeroDelega.Text);
            }
            lavoratore.RichiestaIscrizioneCorsi16Ore = (RadioButtonListIscrizioneCorsi16Ore.SelectedValue == "SI");
            lavoratore.Note = Presenter.NormalizzaCampoTesto(RadTextBoxNote.Text);

            if (RadComboBoxTipologiaPrepagata.SelectedIndex > 0)
            {
                lavoratore.TipoPrepagata = new TipoCartaPrepagata();
                lavoratore.TipoPrepagata.IdTipoCartaPrepagata = RadComboBoxTipologiaPrepagata.SelectedValue;
                lavoratore.TipoPrepagata.Descrizione = RadComboBoxTipologiaPrepagata.Text;
            }
        }

        protected void RadioButtonListCoIntestato_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonListCoIntestato.SelectedValue == "SI")
            {
                RadTextBoxCognomeCointestatario.Enabled = true;
            }
            else
            {
                RadTextBoxCognomeCointestatario.Text = null;
                RadTextBoxCognomeCointestatario.Enabled = false;
            }
        }

        protected void RadioButtonListInfoPrepagata_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonListInfoPrepagata.SelectedValue == "SI")
            {
                RadComboBoxTipologiaPrepagata.Enabled = true;
            }
            else
            {
                RadComboBoxTipologiaPrepagata.SelectedIndex = 0;
                RadComboBoxTipologiaPrepagata.Enabled = false;
            }
        }

        #region Custom Validators

        protected void CustomValidatorBancaIBAN_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string IBAN = Presenter.NormalizzaCampoTesto(RadTextBoxIBAN.Text);

            bool ibanCorretto = false;

            try
            {
                ibanCorretto = IbanManager.VerificaCodiceIban(IBAN);
            }
            catch
            {
            }

            // Controllo che il codice IBAN sia corretto
            if (!string.IsNullOrEmpty(IBAN) && (!ibanCorretto || IBAN.ToUpper() == "IT73J0615513000000000012345"))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorCognomeCointestatario1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadioButtonListCoIntestato.SelectedValue == "SI"
                && String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(RadTextBoxCognomeCointestatario.Text)))
            {
                args.IsValid = false;
            }
        }


        protected void CustomValidatorCognomeCointestatario2_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadioButtonListCoIntestato.SelectedValue == "NO"
                && !String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(RadTextBoxCognomeCointestatario.Text)))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorTagliaFelpaHusky_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadComboBoxTagliaFelpaHusky.SelectedItem == null
                && !String.IsNullOrEmpty(RadComboBoxTagliaFelpaHusky.Text))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorTagliaPantaloni_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadComboBoxTagliaPantaloni.SelectedItem == null
                && !String.IsNullOrEmpty(RadComboBoxTagliaPantaloni.Text))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorTagliaScarpe_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadComboBoxTagliaScarpe.SelectedItem == null
                && !String.IsNullOrEmpty(RadComboBoxTagliaScarpe.Text))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorTipoPrepagata_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Se viene selezionata l'opzione di informazioni sulla prepagata ma non viene scelta quale prepagata diamo errore

            if (RadComboBoxTipologiaPrepagata.SelectedIndex <= 0
                && RadioButtonListInfoPrepagata.SelectedIndex == 0)
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorCointestato_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Se non viene fornito l'IBAN ma flaggato sì per il cointestatario diamo errore

            if (String.IsNullOrEmpty(RadTextBoxIBAN.Text)
                && RadioButtonListCoIntestato.SelectedValue == "SI")
            {
                args.IsValid = false;
            }
        }

        #endregion
    }
}