﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreDatiAnagrafici.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.IscrizioneLavoratoreDatiAnagrafici" %>

<telerik:RadScriptBlock runat="server" ID="RadSriptCodiceFiscale">

    <script type="text/javascript">
    function CFUpperCaseOnly(sender, eventArgs) {
        window.setTimeout("$find('<%= RadTextBoxCodiceFiscale.ClientID %>').set_value($find('<%= RadTextBoxCodiceFiscale.ClientID %>').get_value().toUpperCase());", 50);
    }
    </script>

</telerik:RadScriptBlock>
<asp:Panel ID="PanelDatiAnagrafici" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Dati anagrafici</b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Codice:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxCodice" runat="server" Width="250px" Enabled="False">
                </telerik:RadTextBox>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Cognome<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" Width="250px" MaxLength="30">
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognome" runat="server" ControlToValidate="RadTextBoxCognome"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Cognome mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Nome<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxNome" runat="server" Width="250px" MaxLength="30">
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server" ControlToValidate="RadTextBoxNome"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Nome mancante">
                *
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Sesso<b>*</b>:
            </td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListSesso" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True">M</asp:ListItem>
                    <asp:ListItem>F</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data di nascita (gg/mm/aaaa)<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataNascita" runat="server" Width="250px" />
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataNascita" runat="server"
                    ControlToValidate="RadDatePickerDataNascita" ValidationGroup="datiAnagrafici"
                    ErrorMessage="Data di Nascita mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:CustomValidator ID="CustomValidatorDataNascita" runat="server" ControlToValidate="RadDatePickerDataNascita"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Lavoratore con età inferiore a 16 anni. Controllare la data di nascita."
                    OnServerValidate="CustomValidatorDataNascita_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Codice fiscale<b>*</b>:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" Width="250px" MaxLength="16">
                    <ClientEvents OnValueChanged="CFUpperCaseOnly" />
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                    ControlToValidate="RadTextBoxCodiceFiscale" ValidationGroup="datiAnagrafici"
                    ErrorMessage="Codice fiscale mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:CustomValidator ID="CustomValidatorCodiceFiscale" runat="server" ControlToValidate="RadTextBoxCodiceFiscale"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Codice fiscale non valido" OnServerValidate="CustomValidatorCodiceFiscale_ServerValidate">
                *
                </asp:CustomValidator>
                <asp:CustomValidator ID="CustomValidatorCodiceFiscaleCarattereControllo" runat="server"
                    ControlToValidate="RadTextBoxCodiceFiscale" ValidationGroup="datiAnagrafici"
                    ErrorMessage="Codice di controllo del Codice fiscale non valido" OnServerValidate="CodiceFiscaleCarattereControllo_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Italiano<b>*</b>:
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxItaliano" runat="server" AutoPostBack="True" Checked="True"
                    OnCheckedChanged="CheckBoxItaliano_CheckedChanged" />
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Provincia di nascita<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxProvinciaNascita" runat="server" AutoPostBack="true"
                    EmptyMessage="Selezionare la provincia" Width="250px" OnSelectedIndexChanged="RadComboBoxProvinciaNascita_SelectedIndexChanged"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorProvinciaNascita" runat="server" ControlToValidate="RadComboBoxProvinciaNascita"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare la provincia di nascita"
                    OnServerValidate="CustomValidatorProvinciaNascita_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Comune di nascita (o Nazione)<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxComuneNascita" runat="server" EmptyMessage="- Selezionare il comune -"
                    Width="250px" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorComuneNascita" runat="server" ControlToValidate="RadComboBoxComuneNascita"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare il comune di nascita"
                    OnServerValidate="CustomValidatorComuneNascita_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Stato civile:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxStatoCivile" runat="server" EmptyMessage="Selezionare lo stato civile"
                    Width="250px" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <%--<asp:CustomValidator ID="CustomValidatorStatoCivile" runat="server" ControlToValidate="RadComboBoxStatoCivile"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare lo stato civile" OnServerValidate="CustomValidatorStatoCivile_ServerValidate">
                *
                </asp:CustomValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Cittadinanza<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxCittadinanza" runat="server" EmptyMessage="Selezionare la cittadinanza"
                    Width="250px" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorCittadinanza" runat="server" ControlToValidate="RadComboBoxCittadinanza"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare la cittadinanza" OnServerValidate="CustomValidatorCittadinanza_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Livello di istruzione:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxIstruzione" runat="server" Width="250px" EmptyMessage="Selezionare il livello di istruzione"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <%--<asp:CustomValidator ID="CustomValidatorIstruzione" runat="server" ControlToValidate="RadComboBoxIstruzione"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare il livello di istruzione"
                    OnServerValidate="CustomValidatorIstruzione_ServerValidate">
                *
                </asp:CustomValidator>--%>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Lingua parlata:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxLingua" runat="server" Width="250px" EmptyMessage="Selezionare la lingua parlata"
                    MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <%--<asp:CustomValidator ID="CustomValidatorLingua" runat="server" ControlToValidate="RadComboBoxLingua"
                    ValidationGroup="datiAnagrafici" ErrorMessage="Selezionare la lingua" OnServerValidate="CustomValidatorLingua_ServerValidate">
                *
                </asp:CustomValidator>--%>
            </td>
        </tr>
    </table>
</asp:Panel>
