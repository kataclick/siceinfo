﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class RiepilogoDettagliConsulente : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDichiarazione(Dichiarazione dichiarazione)
        {
            Consulente consulente = dichiarazione.Consulente;

            LabelRagioneSociale.Text = consulente.RagioneSociale;
            LabelCodiceFiscale.Text = consulente.CodiceFiscale;
            LabelTelefono.Text = consulente.Telefono;
            LabelFax.Text = consulente.Fax;
            LabelEMail.Text = consulente.Email;
        }
    }
}