﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RicercaNotificaPubblica.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.RicercaNotificaPubblica" %>

<asp:Panel ID="PanelRicercaNotifiche" runat="server" DefaultButton="ButtonRicerca">
    <table class="standardTable">
        <tr>
            <td>
                <b>Ricerca notifiche</b>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td colspan="2">
                            <b>Periodo </b>
                        </td>
                        <td colspan="2">
                            <b>Attività </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Dal
                        </td>
                        <td>
                            Al
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadDatePicker ID="RadDatePickerDataInvioDa" runat="server" Width="110px" />
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDatePickerDataInvioA" runat="server" Width="110px" />
                        </td>
                        <td colspan="2">
                            <asp:RadioButtonList ID="RadioButtonListAttivita" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="3">
                                <asp:ListItem Value="Assunzione">Assunzione</asp:ListItem>
                                <asp:ListItem Value="Cessazione">Cessazione</asp:ListItem>
                                <asp:ListItem Selected="True" Value="0">Tutte</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Impresa </b>
                        </td>
                        <td colspan="2">
                            <b>Lavoratore </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice
                        </td>
                        <td>
                            Partita Iva
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                        <td>
                            Cognome
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxCodiceImpresa" runat="server" Width="130px"
                                NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" NumberFormat-AllowRounding="false">
                                <NumberFormat AllowRounding="False" DecimalDigits="0" GroupSeparator="" />
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxPartitaIVA" runat="server" MaxLength="11" Width="130px">
                            </telerik:RadTextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" ValidationGroup="ricerca"
                                ErrorMessage="*" ControlToValidate="RadTextBoxPartitaIVA" ValidationExpression="^\d{11}$">
                            </asp:RegularExpressionValidator>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" MaxLength="16" Width="130px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" MaxLength="30" Width="130px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <b>Stato </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:RadioButtonList ID="RadioButtonListStato" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="0">Da valutare</asp:ListItem>
                                <asp:ListItem Value="1">Approvata</asp:ListItem>
                                <asp:ListItem Value="2">In attesa di documentazione</asp:ListItem>
                                <asp:ListItem Selected="True" Value="T">Tutte</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" OnClick="ButtonRicerca_Click"
                    Width="150px" ValidationGroup="ricerca" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadGrid ID="RadGridNotifiche" runat="server" Width="100%" AllowPaging="True"
                    OnPageIndexChanged="RadGridNotifiche_PageIndexChanged" GridLines="None" Visible="False"
                    OnItemDataBound="RadGridNotifiche_ItemDataBound">
                    <MasterTableView DataKeyNames="IdDichiarazione">
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px" />
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px" />
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Attività" UniqueName="Attività" DataField="Attivita">
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Data" UniqueName="Data" DataField="DataInvio"
                                DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime" HtmlEncode="False">
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Lavoratore" UniqueName="Lavoratore">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="LabelLavoratoreCognomeNome" runat="server">
                                        </asp:Label>
                                    </b>
                                    <br />
                                    <small>
                                        <asp:Label ID="LabelLavoratoreDataNascita" runat="server">
                                        </asp:Label>
                                        <br />
                                        <asp:Label ID="LabelLavoratoreCodiceFiscale" runat="server">
                                        </asp:Label>
                                    </small>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="LabelImpresaCodiceRagioneSociale" runat="server">
                                        </asp:Label>
                                    </b>
                                    <br />
                                    <small>
                                        <asp:Label ID="LabelImpresaPartitaIva" runat="server">
                                        </asp:Label>
                                        <br />
                                        <asp:Label ID="LabelImpresaCodiceFiscale" runat="server">
                                        </asp:Label>
                                    </small>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Stato" UniqueName="Stato">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="LabelStato" runat="server">
                                        </asp:Label>
                                    </b>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
