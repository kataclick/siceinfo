﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities.Sintesi.Original;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi
{
    public partial class IscrizioneLavoratoreDatiAnagraficiDocumentiSintesi : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDatiSintesi(DatiLavoratore lavoratore, int idImpresa)
        {
            IscrizioneLavoratoreDatiAnagrafici1.CaricaDatiSintesi(lavoratore, idImpresa);
            IscrizioneLavoratoreDocumenti1.CaricaDatiSintesi(lavoratore.extraCE);
        }

        public void CaricaAttivita(TipoAttivita tipoAttivita)
        {
            IscrizioneLavoratoreDatiAnagrafici1.TipoAttivita = tipoAttivita;
            PanelDocumenti.Enabled = false;
            PanelVisualizzazione.Enabled = false;
            if (tipoAttivita.Equals(TipoAttivita.Assunzione))
            {
                PanelDocumenti.Enabled = true;
                PanelVisualizzazione.Enabled = true;
            }
        }

        public Lavoratore GetLavoratore()
        {
            Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagrafici1.GetLavoratore();
            IscrizioneLavoratoreDocumenti1.CompletaLavoratore(lavoratore);

            return lavoratore;
        }
    }
}