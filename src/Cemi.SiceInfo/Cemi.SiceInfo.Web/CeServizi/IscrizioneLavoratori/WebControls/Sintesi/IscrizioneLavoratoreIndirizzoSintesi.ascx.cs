﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using TBridge.Cemi.Type.Entities.Sintesi.Original;
using Telerik.Web.UI;
using Indirizzo = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Indirizzo;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi
{
    public partial class IscrizioneLavoratoreIndirizzoSintesi : System.Web.UI.UserControl
    {
        private readonly Common commonBiz = new Common();
        private ComuneSiceNew Comune { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDatiSintesi(IndirizzoConRecapitiItaliano indirizzoCantiere)
        {
            Comune = commonBiz.GetComuneSiceNew(indirizzoCantiere.Comune);
            RadTextBoxCap.Text = indirizzoCantiere.cap;
            RadTextBoxIndirizzo.Text = indirizzoCantiere.Indirizzo;
            RadComboBoxComune.Items.Insert(0, new RadComboBoxItem(Comune.Comune, Comune.CodiceCatastale));
            RadComboBoxComune.SelectedIndex = 0;
            RadTextBoxProvincia.Text = Comune.Provincia;
        }

        public void CaricaDatiSintesi(IndirizzoItaliano indirizzoCantiere)
        {
            Comune = commonBiz.GetComuneSiceNew(indirizzoCantiere.Comune);
            RadTextBoxCap.Text = indirizzoCantiere.cap;
            RadTextBoxIndirizzo.Text = indirizzoCantiere.Indirizzo;
            RadComboBoxComune.Items.Insert(0, new RadComboBoxItem(Comune.Comune, Comune.CodiceCatastale));
            RadComboBoxComune.SelectedIndex = 0;
            RadTextBoxProvincia.Text = Comune.Provincia;
        }

        public Indirizzo GetIndirizzo()
        {
            Indirizzo indirizzo =
                new Indirizzo();

            indirizzo.Indirizzo1 = Presenter.NormalizzaCampoTesto(RadTextBoxIndirizzo.Text);
            indirizzo.Civico = Presenter.NormalizzaCampoTesto(RadTextBoxCivico.Text);
            indirizzo.Provincia = RadTextBoxProvincia.Text;
            indirizzo.Comune = RadComboBoxComune.Text;
            if (!String.IsNullOrEmpty(RadComboBoxComune.Text))
            {
                indirizzo.ComuneDescrizione = RadComboBoxComune.Text;
                indirizzo.Comune = RadComboBoxComune.SelectedValue;
            }
            indirizzo.Cap = RadTextBoxCap.Text;

            return indirizzo;
        }
    }
}