﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreIndirizzoSintesi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi.IscrizioneLavoratoreIndirizzoSintesi" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        function onSelectedIndexChanged(sender, eventArgs) {
            sender.selectText(0, 1);
        } 
    </script>

</telerik:RadScriptBlock>
<asp:Panel ID="PanelIndirizzo" runat="server" CssClass="borderedTable">
    <div style="float: left; width: 100%;">
        <table class="standardTable">
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Via/Piazza<b>*</b>:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server" Width="250px" Enabled="false">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Civico:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxCivico" runat="server" Width="250px" Enabled="false">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Provincia<b>*</b>:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxProvincia" runat="server" Width="250px" Enabled="false">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Comune<b>*</b>:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxComune" runat="server" Width="250px" Enabled="false">
                    </telerik:RadComboBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Cap:
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxCap" runat="server" Width="250px" Enabled="false">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                </td>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelTemporaneo" runat="server">
</telerik:RadAjaxLoadingPanel>
