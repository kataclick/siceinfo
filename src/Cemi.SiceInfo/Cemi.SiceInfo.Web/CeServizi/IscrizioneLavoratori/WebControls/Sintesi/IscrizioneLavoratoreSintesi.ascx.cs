﻿using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities.Sintesi;
using TBridge.Cemi.Type.Entities.Sintesi.Original;
using Telerik.Web.UI;
using Consulente = TBridge.Cemi.Type.Entities.GestioneUtenti.Consulente;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi
{
    public partial class IscrizioneLavoratoreSintesi : System.Web.UI.UserControl
    {
        private const Int32 INDICESTEPATTIVITA = 0;
        private const Int32 INDICESTEPCANTIERE = 2;
        private const Int32 INDICESTEPCONFERMA = 7;
        private const Int32 INDICESTEPDATIANAGRAFICI = 3;
        private const Int32 INDICESTEPDATILOCALIZZAZIONE = 4;
        private const Int32 INDICESTEPINFOAGGIUNTIVE = 6;
        private const Int32 INDICESTEPRAPPORTOLAVORO = 5;

        private const String VALIDATIONGROUPATTIVITA = "attivita";
        private const String VALIDATIONGROUPCANTIERE = "cantiere";
        private const String VALIDATIONGROUPCONFERMA = "conferma";
        private const String VALIDATIONGROUPDATIANAGRAFICI = "datiAnagrafici";
        private const String VALIDATIONGROUPDATILOCALIZZAZIONE = "datiLocalizzazione";
        private const String VALIDATIONGROUPINFOAGGIUNTIVE = "infoAggiuntive";
        private const String VALIDATIONGROUPRAPPORTODILAVORO = "rapportoDiLavoro";

        private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
        private readonly SintesiManager sintesiManager = new SintesiManager();

        private Boolean[] TABATTIVI
        {
            get { return (Boolean[])ViewState["TABATTIVI"]; }
            set { ViewState["TABATTIVI"] = value; }
        }

        private Int32 indiceCorrenteTab
        {
            get { return (Int32)ViewState["IndiceCorrenteTab"]; }
            set { ViewState["IndiceCorrenteTab"] = value; }
        }

        public RapportoLavoro RapportoLavoro
        {
            get { return (RapportoLavoro)ViewState["RapportoLavoro"]; }
            set { ViewState["RapportoLavoro"] = value; }
        }

        public bool IscrizioneCorsiAbilitata
        {
            get { return IscrizioneLavoratoreInformazioniAggiuntive.IscrizioneCorsiAbilitata; }
            set { IscrizioneLavoratoreInformazioniAggiuntive.IscrizioneCorsiAbilitata = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Inizializzazione TABATTIVI

                Boolean[] nuoviTabAttivi = new Boolean[RadTabStripIscrizioneLavoratore.Tabs.Count];
                for (Int32 i = 0; i < RadTabStripIscrizioneLavoratore.Tabs.Count; i++)
                {
                    nuoviTabAttivi[i] = true;
                }
                TABATTIVI = nuoviTabAttivi;

                indiceCorrenteTab = 0;

                #endregion

                CaricaRapportoDiLavoro();

                // Per prevenire click multipli
                #region Click multipli

                // Per prevenire click multipli
                StringBuilder sb = new StringBuilder();
                sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
                sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
                sb.Append("if (Page_ClientValidate('");
                sb.Append(VALIDATIONGROUPCONFERMA);
                sb.Append("') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
                sb.Append("this.value = 'Attendere...';");
                sb.Append("this.disabled = true;");
                sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonAvanti, null) + ";");
                sb.Append("return true;");

                ButtonAvanti.Attributes.Add("onclick", sb.ToString());

                ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAvanti);

                #endregion
            }

            GestisciAbilitazionePassi();
        }

        public void CaricaRapportoDiLavoro(RapportoLavoro rapportoLavoro)
        {
            RapportoLavoro = rapportoLavoro;
            CaricaRapportoDiLavoro();
        }

        private void CaricaRapportoDiLavoro()
        {
            if (RapportoLavoro == null)
            {
                return;
            }
            #region Attività

            if (RapportoLavoro.Item is RapportoLavoroInizioRapporto)
                IscrizioneLavoratoreAttivita1.SetAttivita(TipoAttivita.Assunzione);
            else if (RapportoLavoro.Item is Cessazione)
                IscrizioneLavoratoreAttivita1.SetAttivita(TipoAttivita.Cessazione);
            else if (RapportoLavoro.Item is Proroga)
                IscrizioneLavoratoreAttivita1.SetAttivita(TipoAttivita.Proroga);
            else if (RapportoLavoro.Item is RapportoLavoroTrasformazione)
                IscrizioneLavoratoreAttivita1.SetAttivita(TipoAttivita.Trasformazione);

            SceltaModalita(IscrizioneLavoratoreAttivita1.GetAttivita());

            #endregion

            #region Impresa
            if (RapportoLavoro.Item is Contratto)
            {
                IscrizioneLavoratoreImpresa1.CaricaDatiSintesi(RapportoLavoro.DatoreLavoro, ((Contratto)RapportoLavoro.Item));
            }

            if (RapportoLavoro.Item is Cessazione)
            {
                IscrizioneLavoratoreImpresa1.CaricaDatiSintesi(RapportoLavoro.DatoreLavoro, ((Cessazione)RapportoLavoro.Item).Contratto);
            }

            #endregion

            #region Cantiere

            IscrizioneLavoratoreCantiere1.CaricaDatiSintesi(RapportoLavoro.DatoreLavoro.SedeLavoro);

            #endregion

            #region Dati

            IscrizioneLavoratoreDatiAnagraficiDocumenti1.CaricaDatiSintesi(RapportoLavoro.Lavoratore, IscrizioneLavoratoreImpresa1.GetIdImpresa());

            #endregion

            #region Indirizzi

            IscrizioneLavoratoreDatiLocalizzazione1.CaricaDatiSintesi(RapportoLavoro.Lavoratore.IndirizzoLavoratore);

            #endregion

            #region Rapp. di Lavoro

            IscrizioneLavoratoreRapportoDiLavoro1.CaricaDatiSintesi(RapportoLavoro.Item, IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore().IdLavoratore, IscrizioneLavoratoreImpresa1.GetIdImpresa());

            #endregion
        }

        private void GestisciAbilitazionePassi()
        {
            foreach (RadTab tab in RadTabStripIscrizioneLavoratore.Tabs)
            {
                if (tab.Index > RadTabStripIscrizioneLavoratore.SelectedIndex)
                {
                    tab.Enabled = false;
                }
                else
                {
                    tab.Enabled = TABATTIVI[tab.Index];
                }
            }
        }

        private void ModalitaProroga()
        {
            #region Abilitazione/Disabilitazione Step

            Boolean[] nuoviTabAttivi = TABATTIVI;

            nuoviTabAttivi[INDICESTEPATTIVITA] = true;
            nuoviTabAttivi[INDICESTEPCANTIERE] = false;
            nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
            nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
            nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
            nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = false;
            nuoviTabAttivi[INDICESTEPCONFERMA] = true;

            TABATTIVI = nuoviTabAttivi;

            #endregion
            IscrizioneLavoratoreDatiAnagraficiDocumenti1.CaricaAttivita(TipoAttivita.Proroga);
            IscrizioneLavoratoreDatiLocalizzazione1.CaricaAtiivita(TipoAttivita.Proroga);
            IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Proroga);
        }

        private void ModalitaTrasformazione()
        {
            #region Abilitazione/Disabilitazione Step

            Boolean[] nuoviTabAttivi = TABATTIVI;

            nuoviTabAttivi[INDICESTEPATTIVITA] = true;
            nuoviTabAttivi[INDICESTEPCANTIERE] = false;
            nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
            nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
            nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
            nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = false;
            nuoviTabAttivi[INDICESTEPCONFERMA] = true;

            TABATTIVI = nuoviTabAttivi;

            #endregion

            IscrizioneLavoratoreDatiAnagraficiDocumenti1.CaricaAttivita(TipoAttivita.Trasformazione);
            IscrizioneLavoratoreDatiLocalizzazione1.CaricaAtiivita(TipoAttivita.Trasformazione);
            IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Trasformazione);
        }

        private void ModalitaCessazione()
        {
            #region Abilitazione/Disabilitazione Step

            Boolean[] nuoviTabAttivi = TABATTIVI;

            nuoviTabAttivi[INDICESTEPATTIVITA] = true;
            nuoviTabAttivi[INDICESTEPCANTIERE] = false;
            nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
            nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
            nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
            nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = false;
            nuoviTabAttivi[INDICESTEPCONFERMA] = true;

            TABATTIVI = nuoviTabAttivi;

            #endregion


            IscrizioneLavoratoreDatiAnagraficiDocumenti1.CaricaAttivita(TipoAttivita.Cessazione);
            IscrizioneLavoratoreDatiLocalizzazione1.CaricaAtiivita(TipoAttivita.Cessazione);
            IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Cessazione);
        }

        private void SceltaModalita(TipoAttivita tipoEvento)
        {
            IscrizioneLavoratoreRapportoDiLavoro1.CaricaTipoAttivita(tipoEvento);

            switch (tipoEvento)
            {
                case TipoAttivita.Assunzione:
                    ModalitaAssunzione();
                    break;
                case TipoAttivita.Cessazione:
                    ModalitaCessazione();
                    break;
                case TipoAttivita.Trasformazione:
                    ModalitaTrasformazione();
                    break;
                case TipoAttivita.Proroga:
                    ModalitaProroga();
                    break;
            }
        }


        private void ModalitaAssunzione()
        {
            #region Abilitazione/Disabilitazione Step

            Boolean[] nuoviTabAttivi = TABATTIVI;

            nuoviTabAttivi[INDICESTEPATTIVITA] = true;
            nuoviTabAttivi[INDICESTEPCANTIERE] = true;
            nuoviTabAttivi[INDICESTEPDATIANAGRAFICI] = true;
            nuoviTabAttivi[INDICESTEPDATILOCALIZZAZIONE] = true;
            nuoviTabAttivi[INDICESTEPRAPPORTOLAVORO] = true;
            nuoviTabAttivi[INDICESTEPINFOAGGIUNTIVE] = true;
            nuoviTabAttivi[INDICESTEPCONFERMA] = true;

            TABATTIVI = nuoviTabAttivi;

            #endregion
            IscrizioneLavoratoreDatiAnagraficiDocumenti1.CaricaAttivita(TipoAttivita.Assunzione);
            IscrizioneLavoratoreDatiLocalizzazione1.CaricaAtiivita(TipoAttivita.Assunzione);
            IscrizioneLavoratoreRiepilogo1.CaricaAttivita(TipoAttivita.Assunzione);
        }

        protected void RadTabStripIscrizioneLavoratore_TabClick(object sender, RadTabStripEventArgs e)
        {
            if (RadTabStripIscrizioneLavoratore.SelectedIndex > indiceCorrenteTab)
            {
                RadTabStripIscrizioneLavoratore.SelectedIndex = indiceCorrenteTab;
                RadMultiPageIscrizioneLavoratore.SelectedIndex = indiceCorrenteTab;
            }

            VerificaVisualizzazioneTastoIndietro();
            VerificaVisualizzazioneTastoAvanti();

            indiceCorrenteTab = RadTabStripIscrizioneLavoratore.SelectedIndex;
            GestisciAbilitazionePassi();
        }

        protected void ButtonAvanti_Click(object sender, EventArgs e)
        {
            switch (RadTabStripIscrizioneLavoratore.SelectedIndex)
            {
                case INDICESTEPATTIVITA:
                    Page.Validate(VALIDATIONGROUPATTIVITA);
                    break;
                case INDICESTEPCANTIERE:
                    Page.Validate(VALIDATIONGROUPCANTIERE);
                    break;
                case INDICESTEPDATIANAGRAFICI:
                    Page.Validate(VALIDATIONGROUPDATIANAGRAFICI);
                    break;
                case INDICESTEPDATILOCALIZZAZIONE:
                    Page.Validate(VALIDATIONGROUPDATILOCALIZZAZIONE);
                    break;
                case INDICESTEPRAPPORTOLAVORO:
                    if (!IscrizioneLavoratoreAttivita1.GetAttivita().Equals(TipoAttivita.Assunzione))
                        CreaRiassunto();
                    Page.Validate(VALIDATIONGROUPRAPPORTODILAVORO);
                    break;
                case INDICESTEPINFOAGGIUNTIVE:
                    {
                        CreaRiassunto();
                    }
                    Page.Validate(VALIDATIONGROUPINFOAGGIUNTIVE);
                    break;
                case INDICESTEPCONFERMA:
                    Page.Validate(VALIDATIONGROUPCONFERMA);
                    if (Page.IsValid)
                    {
                        Dichiarazione dichiarazione = CreaDichiarazione();
                        dichiarazione.IdUtente = GestioneUtentiBiz.GetIdUtente();

                        DichiarazioneCollection dichiarazioni =
                            biz.GetDichiarazioniByImpresaCodFiscDataInizioDataCessazione(dichiarazione.IdImpresa,
                                                                                         dichiarazione.Lavoratore.CodiceFiscale,
                                                                                         dichiarazione.RapportoDiLavoro.
                                                                                             DataInizioValiditaRapporto,
                                                                                         dichiarazione.RapportoDiLavoro.
                                                                                             DataCessazione);
                        if (dichiarazioni.Count == 0)
                        {
                            if (biz.InsertDichiarazione(dichiarazione))
                            {
                                //Context.Items["Dichiarazione"] = dichiarazione;
                                //Response.Redirect("~/IscrizioneLavoratori/ComunicazioneEffettuata.aspx");
                                //Server.Transfer("~/IscrizioneLavoratori/ComunicazioneEffettuata.aspx");
                                //string idComunicazione = Request["idComunicazione"];
                                //Guid guid = new Guid(idComunicazione);

                                if (dichiarazione.IdDichiarazione != null)
                                    sintesiManager.SalvaDatiComunicazione(null, RapportoLavoro.codiceComunicazione, RapportoLavoro.codiceComunicazionePrec, dichiarazione.IdDichiarazione.Value);
                                Session["Dichiarazione"] = dichiarazione;

                                RadAjaxPanel1.Redirect("~/CeServizi/IscrizioneLavoratori/ComunicazioneEffettuata.aspx");
                            }
                        }
                        else
                        {
                            RadAjaxPanel1.Redirect("~/CeServizi/IscrizioneLavoratori/ComunicazioneGiaEffettuata.aspx");
                        }
                    }

                    break;
            }

            //Il redirect parte dopo il postback. Quindi devo evitare che venga eseguito questo codice nell'ultimo step per prevenire errori.
            if (RadTabStripIscrizioneLavoratore.SelectedIndex < 7)
            {
                if (Page.IsValid)
                {
                    do
                    {

                        RadTabStripIscrizioneLavoratore.SelectedIndex++;
                        RadMultiPageIscrizioneLavoratore.SelectedIndex++;
                    } while (!TABATTIVI[RadTabStripIscrizioneLavoratore.SelectedIndex]);

                    VerificaVisualizzazioneTastoIndietro();
                    VerificaVisualizzazioneTastoAvanti();
                }

                indiceCorrenteTab = RadTabStripIscrizioneLavoratore.SelectedIndex;
                GestisciAbilitazionePassi();
            }
            else
            {
                RadTabStripIscrizioneLavoratore.Visible = false;
            }
        }

        private void CreaRiassunto()
        {
            Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
            IscrizioneLavoratoreDatiLocalizzazione1.CompletaLavoratore(lavoratore);
            IscrizioneLavoratoreRiepilogo1.ControllaDatiAnagraficiDiscordanti(lavoratore);
            CompletaRiassunto();
            if (!IscrizioneLavoratoreRiepilogo1.HasAvvertimenti)
                IscrizioneLavoratoreRiepilogo1.FindControl("LabelAvvertimenti").Visible = false;
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            do
            {
                RadTabStripIscrizioneLavoratore.SelectedIndex--;
                RadMultiPageIscrizioneLavoratore.SelectedIndex--;
            } while (!TABATTIVI[RadTabStripIscrizioneLavoratore.SelectedIndex]);

            VerificaVisualizzazioneTastoIndietro();
            VerificaVisualizzazioneTastoAvanti();

            indiceCorrenteTab = RadTabStripIscrizioneLavoratore.SelectedIndex;
            GestisciAbilitazionePassi();
        }

        private void CompletaRiassunto()
        {
            TipoAttivita attivita = IscrizioneLavoratoreAttivita1.GetAttivita();
            Lavoratore lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
            Impresa impresa = biz.GetImpresa(IscrizioneLavoratoreImpresa1.GetIdImpresa());
            IscrizioneLavoratoreDatiLocalizzazione1.CompletaLavoratore(lavoratore);
            RapportoDiLavoro rapporto = IscrizioneLavoratoreRapportoDiLavoro1.GetRapportoDiLavoro();

            IscrizioneLavoratoreRiepilogo1.CaricaAttivita(attivita);
            IscrizioneLavoratoreRiepilogo1.CaricaImpresa(impresa);
            IscrizioneLavoratoreRiepilogo1.CaricaLavoratore(lavoratore);
            IscrizioneLavoratoreRiepilogo1.CaricaRapportoDiLavoro(rapporto);
            switch (attivita)
            {
                case TipoAttivita.Cessazione:
                    IscrizioneLavoratoreRiepilogo1.CaricaDatiCessazione(rapporto);
                    break;
                case TipoAttivita.Trasformazione:
                    IscrizioneLavoratoreRiepilogo1.CaricaDatiTrasformazione(rapporto);
                    break;
            }
        }

        private void VerificaVisualizzazioneTastoIndietro()
        {
            if (RadTabStripIscrizioneLavoratore.SelectedIndex == 0 && RadMultiPageIscrizioneLavoratore.SelectedIndex == 0)
            {
                ButtonIndietro.Visible = false;
            }
            else
            {
                ButtonIndietro.Visible = true;
            }
        }

        private void VerificaVisualizzazioneTastoAvanti()
        {
            Int32 indiceUltimoPasso = RadTabStripIscrizioneLavoratore.Tabs.Count - 1;

            if (RadTabStripIscrizioneLavoratore.SelectedIndex == indiceUltimoPasso
                && RadMultiPageIscrizioneLavoratore.SelectedIndex == indiceUltimoPasso)
            {
                ButtonAvanti.Text = "Conferma";
            }
            else
            {
                ButtonAvanti.Text = "Avanti";
            }
        }

        private Dichiarazione CreaDichiarazione()
        {
            Dichiarazione dichiarazione = new Dichiarazione();

            dichiarazione.Attivita = IscrizioneLavoratoreAttivita1.GetAttivita();
            dichiarazione.IdImpresa = IscrizioneLavoratoreImpresa1.GetIdImpresa();
            dichiarazione.Lavoratore = IscrizioneLavoratoreDatiAnagraficiDocumenti1.GetLavoratore();
            IscrizioneLavoratoreDatiLocalizzazione1.CompletaLavoratore(dichiarazione.Lavoratore);
            IscrizioneLavoratoreInformazioniAggiuntive.CompletaLavoratore(dichiarazione.Lavoratore);
            dichiarazione.RapportoDiLavoro = IscrizioneLavoratoreRapportoDiLavoro1.GetRapportoDiLavoro();

            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                dichiarazione.IdConsulente = consulente.IdConsulente;
            }

            switch (dichiarazione.Attivita)
            {
                case TipoAttivita.Assunzione:
                    dichiarazione.Cantiere = IscrizioneLavoratoreCantiere1.GetIndirizzo();
                    break;
            }

            return dichiarazione;
        }
    }
}