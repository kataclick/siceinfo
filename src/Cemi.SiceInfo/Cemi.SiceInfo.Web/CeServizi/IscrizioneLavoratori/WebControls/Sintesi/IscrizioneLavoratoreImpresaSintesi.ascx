﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreImpresaSintesi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi.IscrizioneLavoratoreImpresaSintesi" %>

<table class="borderedTable">
    <tr>
        <td colspan="2">
            In caso di dati non corretti contattare la Cassa Edile di Milano.
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Codice:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCodice" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Ragione sociale:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Partita IVA:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxPartitaIva" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Tipologia contrattuale:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTipologiaContrattuale" runat="server" Width="250px"
                Enabled="false">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Sede legale:
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxProvincia" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxComune" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxCap" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxTelefono" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxFax" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            E-Mail:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxEMail" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Stato:
        </td>
        <td>
            <table style="width:250px">
                <tr>
                    <td  style="width:30px">
                         <asp:Image ID="ImageStato"  runat="server" ImageUrl="../../../images/semaforoGiallo.png" />     
                    </td>
                    <td>    
                        <asp:Label ID="LabelStato" runat="server" Enabled="false" />
                    </td>
                </tr>
            </table>
               
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
        </td>
    </tr>
    <tr id="RowImpresaNonAttiva" runat="server">
        <td colspan="2" style="text-align:center">
            <table >
                <tr>
                    <td style="width:50px">
                        <asp:Image ID="Image1"  runat="server" ImageUrl="../../../images/ip_icon_02_Warning_.png" />    
                    </td>
                    <td style="color:Red">
                        <asp:Label ID="LabelErroreImpresa" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
