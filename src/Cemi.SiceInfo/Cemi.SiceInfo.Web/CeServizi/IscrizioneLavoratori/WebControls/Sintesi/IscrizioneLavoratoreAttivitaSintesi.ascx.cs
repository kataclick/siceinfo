﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Delegates;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi
{
    public partial class IscrizioneLavoratoreAttivitaSintesi : System.Web.UI.UserControl
    {
        public event TipoAttivitaSelectedEventHandler OnAttivitaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void SetAttivita(TipoAttivita attivita)
        {
            switch (attivita)
            {
                case TipoAttivita.Assunzione:
                    RadioButtonListTipoAttivita.SelectedIndex = 0;
                    break;
                case TipoAttivita.Cessazione:
                    RadioButtonListTipoAttivita.SelectedIndex = 1;
                    break;
                case TipoAttivita.Proroga:
                    RadioButtonListTipoAttivita.SelectedIndex = 0;
                    break;
                case TipoAttivita.Trasformazione:
                    RadioButtonListTipoAttivita.SelectedIndex = 0;
                    break;
            }
        }

        public TipoAttivita GetAttivita()
        {
            return
                (TipoAttivita)
                Enum.Parse(typeof(TipoAttivita), RadioButtonListTipoAttivita.SelectedValue);
        }
    }
}