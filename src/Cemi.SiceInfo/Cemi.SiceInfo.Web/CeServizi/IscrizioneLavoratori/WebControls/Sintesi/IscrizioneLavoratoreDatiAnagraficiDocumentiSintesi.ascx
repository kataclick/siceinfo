﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreDatiAnagraficiDocumentiSintesi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi.IscrizioneLavoratoreDatiAnagraficiDocumentiSintesi" %>

<%@ Register Src="IscrizioneLavoratoreDatiAnagraficiSintesi.ascx" TagName="IscrizioneLavoratoreDatiAnagraficiSintesi"
    TagPrefix="uc4" %>
<%@ Register Src="IscrizioneLavoratoreDocumentiSintesi.ascx" TagName="IscrizioneLavoratoreDocumentiSintesi"
    TagPrefix="uc8" %>
<asp:Panel ID="PanelVisualizzazione" runat="server" Width="100%">
    <uc4:IscrizioneLavoratoreDatiAnagraficiSintesi ID="IscrizioneLavoratoreDatiAnagrafici1"
        runat="server" />
</asp:Panel>
<br />
<asp:Panel ID="PanelDocumenti" runat="server" Width="100%">
    <uc8:IscrizioneLavoratoreDocumentiSintesi ID="IscrizioneLavoratoreDocumenti1" runat="server" />
</asp:Panel>