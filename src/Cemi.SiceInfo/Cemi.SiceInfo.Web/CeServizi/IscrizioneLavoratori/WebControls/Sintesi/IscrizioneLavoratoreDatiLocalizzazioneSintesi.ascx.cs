﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities.Sintesi.Original;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.Sintesi
{
    public partial class IscrizioneLavoratoreDatiLocalizzazioneSintesi : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaAtiivita(TipoAttivita tipoAttivita)
        {
            PanelAltriDati.Enabled = false;
            if (tipoAttivita.Equals(TipoAttivita.Assunzione))
                PanelAltriDati.Enabled = true;
        }

        public void CaricaDatiSintesi(IndirizzoItaliano indirizzoLavoratore)
        {
            IscrizioneLavoratoreIndirizzo1.CaricaDatiSintesi(indirizzoLavoratore);
        }

        public void CompletaLavoratore(Lavoratore lavoratore)
        {
            lavoratore.Indirizzo = IscrizioneLavoratoreIndirizzo1.GetIndirizzo();
            lavoratore.NumeroTelefono = Presenter.NormalizzaCampoTesto(RadTexBoxTelefono.Text);
            lavoratore.NumeroTelefonoCellulare = Presenter.NormalizzaCampoTesto(RadTexBoxCellulare.Text);
            lavoratore.Email = RadTexBoxEmail.Text;
        }

        protected void CustomValidatorServizioSms_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (CheckBoxServizioSms.Checked && string.IsNullOrEmpty(RadTexBoxCellulare.Text))
            {
                args.IsValid = false;
            }
        }
    }
}