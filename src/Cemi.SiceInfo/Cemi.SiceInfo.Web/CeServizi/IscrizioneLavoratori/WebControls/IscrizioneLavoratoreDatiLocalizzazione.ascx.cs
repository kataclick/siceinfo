﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using Consulente = TBridge.Cemi.Type.Entities.GestioneUtenti.Consulente;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class IscrizioneLavoratoreDatiLocalizzazione : System.Web.UI.UserControl
    {
        private Impresa Impresa
        {
            get
            {
                if (ViewState["Impresa"] != null)
                {
                    return (Impresa)ViewState["Impresa"];
                }
                else
                {
                    return null;
                }
            }
            set { ViewState["Impresa"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //CaricaProvinciaCantiereMilano();
        }

        public void CaricaProvince()
        {
            IscrizioneLavoratoreIndirizzo1.CaricaProvince();
        }

        public void CaricaImpresa(Impresa impresa)
        {
            Impresa = impresa;
        }

        public void ImpostaValidationGroup(String validationGroup)
        {
            AbilitaValidationGroup(true);
            IscrizioneLavoratoreIndirizzo1.ImpostaValidationGroup(validationGroup);
        }

        public void CaricaDatiLavoratore(Lavoratore lavoratore)
        {
            Reset();
            IscrizioneLavoratoreIndirizzo1.CaricaDatiIndirizzo(lavoratore.Indirizzo);

            PanelAltriDati.Enabled = false;
            RadTexBoxTelefono.Enabled = false;
            RadTexBoxCellulare.Enabled = false;
            RadTexBoxEmail.Enabled = false;
            RadTexBoxTelefono.Text = lavoratore.NumeroTelefono;
            RadTexBoxCellulare.Text = lavoratore.NumeroTelefonoCellulare;
            RadTexBoxEmail.Text = lavoratore.Email;
        }

        public void Reset()
        {
            //IscrizioneLavoratoreIndirizzo1.Reset();
            RadTexBoxTelefono.Enabled = true;
            RadTexBoxTelefono.Text = null;
            RadTexBoxCellulare.Enabled = true;
            RadTexBoxCellulare.Text = null;
            RadTexBoxEmail.Enabled = true;
            RadTexBoxEmail.Text = null;
        }

        public void CompletaLavoratore(Lavoratore lavoratore)
        {
            lavoratore.Indirizzo = IscrizioneLavoratoreIndirizzo1.GetIndirizzo();
            lavoratore.NumeroTelefono = Presenter.NormalizzaCampoTesto(RadTexBoxTelefono.Text);
            lavoratore.NumeroTelefonoCellulare = Presenter.NormalizzaCampoTesto(RadTexBoxCellulare.Text);
            lavoratore.Email = RadTexBoxEmail.Text;
        }

        public void AbilitaValidationGroup(bool abilita)
        {
            RegularExpressionValidatorTelefono.Enabled = abilita;
            RegularExpressionValidatorEmail.Enabled = abilita;
            RegularExpressionValidatorCellulare.Enabled = abilita;
            IscrizioneLavoratoreIndirizzo1.AbilitaValidationGroup(abilita);
        }

        protected void CustomValidatorServizioSms_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (CheckBoxServizioSms.Checked && String.IsNullOrEmpty(RadTexBoxCellulare.Text))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorCantiereIndirizziImpresaConsulenteLavoratore_ServerValidate(object source,
                                                                                                  ServerValidateEventArgs
                                                                                                      args)
        {
            // Controllo che l'indirizzo del cantiere non sia uguale a:
            // - sede legale impresa
            // - sede amministrativa impresa
            // - sede consulente (se è consulente ad inserire)

            args.IsValid = true;

            Indirizzo indirizzo = IscrizioneLavoratoreIndirizzo1.GetIndirizzo();

            Consulente consulente;
            try
            {
                consulente =
                    (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            }
            catch
            {
                consulente = null;
            }

            //TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa impresa = IscrizioneLavoratoreImpresa1.GetImpresa();
            Impresa impresa = Impresa;

            if (indirizzo != null)
            {
                if (((impresa.SedeLegaleIndirizzo == string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)) &&
                     (impresa.SedeLegaleComune == indirizzo.ComuneDescrizione) &&
                     (impresa.SedeLegaleProvincia == indirizzo.Provincia))
                    ||
                    ((impresa.SedeAmministrativaIndirizzo ==
                      string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)) &&
                     (impresa.SedeAmministrativaComune == indirizzo.ComuneDescrizione) &&
                     (impresa.SedeAmministrativaProvincia == indirizzo.Provincia))
                    ||
                    ((consulente != null) &&
                     (consulente.Indirizzo == string.Format("{0} {1}", indirizzo.Indirizzo1, indirizzo.Civico)))
                    )
                {
                    args.IsValid = false;
                }
            }
        }

        public Boolean IndirizzoConfermato()
        {
            return IscrizioneLavoratoreIndirizzo1.IndirizzoConfermato();
        }
    }
}