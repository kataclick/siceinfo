﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class RiepilogoDettagliImpresa : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }


        public void CaricaDichiarazione(Dichiarazione dichiarazione)
        {
            Impresa impresa = dichiarazione.Impresa;

            LabelCodice.Text = impresa.IdImpresa.ToString();
            LabelRagioneSociale.Text = impresa.RagioneSociale;
            LabelPartitaIVA.Text = impresa.PartitaIva;
            LabelCodiceFiscale.Text = impresa.CodiceFiscale;
            if (impresa.Contratto != null)
                LabelTipologiaContrattuale.Text = impresa.Contratto.Descrizione;
            LabelTelefono.Text = impresa.SedeLegaleTelefono;
            LabelFax.Text = impresa.SedeLegaleFax;
            LabelEMail.Text = impresa.SedeLegaleEmail;
            LabelDataUltimaDenuncia.Text = impresa.DataUltimaDenuncia.HasValue ? impresa.DataUltimaDenuncia.Value.ToString("MM/yyyy") : "N.D.";
            LabelStato.Text = impresa.Stato;
            if (String.Equals(impresa.Stato, StatoImpresa.ATTIVA.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                ImageRuolo.ImageUrl = IscrizioneLavoratoriManager.URLSEMAFOROVERDE;
            }
        }
    }
}