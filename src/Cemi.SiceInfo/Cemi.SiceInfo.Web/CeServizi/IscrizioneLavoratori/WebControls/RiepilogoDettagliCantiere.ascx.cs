﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class RiepilogoDettagliCantiere : System.Web.UI.UserControl
    {
        private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDichiarazione(Dichiarazione dichiarazione)
        {
            LabelIndirizzo.Text = string.Format(dichiarazione.Cantiere.IndirizzoCompleto);

            EffettuaControlli(dichiarazione);
        }

        private void EffettuaControlli(Dichiarazione dichiarazione)
        {
            switch (dichiarazione.Attivita)
            {
                case TipoAttivita.Assunzione:
                    tableControlli.Visible = true;

                    ImageControlloCantiere.ImageUrl = biz.CantiereInNotifica(dichiarazione);
                    break;
                default:
                    tableControlli.Visible = false;
                    break;
            }
        }
    }
}