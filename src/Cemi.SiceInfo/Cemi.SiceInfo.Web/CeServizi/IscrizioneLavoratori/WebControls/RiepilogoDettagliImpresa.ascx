﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiepilogoDettagliImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.RiepilogoDettagliImpresa" %>

<style type="text/css">
    .style1
    {
        width: 30px;
    }
</style>
<table class="borderedTable" style="width:100%">
    <tr>
        <td colspan="3">
            <b>Dati impresa</b>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Codice
        </td>
        <td colspan="2">
            <b>
                <asp:Label ID="LabelCodice" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Ragione sociale
        </td>
        <td colspan="2">
            <b>
                <asp:Label ID="LabelRagioneSociale" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>
            Partita IVA
            </small>
        </td>
        <td colspan="2">
            <small>
                <asp:Label ID="LabelPartitaIVA" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>
            Codice fiscale
            </small>
        </td>
        <td  colspan="2">
            <small>
                <asp:Label ID="LabelCodiceFiscale" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>
            Tipologia contrattuale
            </small>
        </td>
        <td colspan="2">
            <small>
                <asp:Label ID="LabelTipologiaContrattuale" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>
            Telefono
            </small>
        </td>
        <td colspan="2">
            <small>
                <asp:Label ID="LabelTelefono" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>
            Fax
            </small>
        </td>
        <td colspan="2">
            <small>
                <asp:Label ID="LabelFax" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>
            E-Mail
            </small>
        </td>
        <td colspan="2">
            <small>
                <asp:Label ID="LabelEMail" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            <small>Stato</small>
        </td>
        <td class="style1" >
            <asp:Image ID="ImageRuolo"  runat="server" ImageUrl="../../images/semaforoGiallo.png" />          
        </td>
        <td>
            <small>
                <asp:Label ID="LabelStato" runat="server" />
            </small>
        </td>
    </tr>
    <tr>
        <td>
            <small>Competenza ultima denuncia</small>
        </td>
        <td colspan="2">
            <small>
                <asp:Label ID="LabelDataUltimaDenuncia" runat="server" />
            </small>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
</table>
