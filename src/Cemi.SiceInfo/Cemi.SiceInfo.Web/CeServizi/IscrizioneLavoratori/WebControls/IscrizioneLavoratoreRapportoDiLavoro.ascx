﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreRapportoDiLavoro.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.IscrizioneLavoratoreRapportoDiLavoro" %>

<table class="borderedTable">
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data comunicazione (gg/mm/aaaa):
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDataInvio" runat="server" Width="250px" MaxLength="10"
                Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
</table>
<br />
<asp:Panel ID="PanelDatiRapporto" runat="server">
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Rapporto di lavoro </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data assunzione<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDateInputDataAssunzione" runat="server" Width="250px" />
            </td>
            <td>
                <asp:Label ID="ErrorLabel" ForeColor="Red" runat="server"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataAssunzione" runat="server"
                    ControlToValidate="RadDateInputDataAssunzione" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Data assunzione mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidatorDataAssunzione" runat="server" ControlToValidate="RadDateInputDataAssunzione"
                    ErrorMessage="Data assunzione non valida" Display="Dynamic" MaximumValue="2050-12-31-00-00-00"
                    MinimumValue="1900-01-01-00-00-00" ValidationGroup="rapportoDiLavoro">
                *
                </asp:RangeValidator>
                <asp:CustomValidator ID="CustomValidatorDataAssunzione" runat="server" ErrorMessage="La data assunzione non può essere successiva a quella di inizio rapporto"
                    ValidationGroup="rapportoDiLavoro" OnServerValidate="CustomValidatorDataAssunzione_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Data inizio rapporto di lavoro<b>*</b>:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDateInputDataInizioRapportoLavoro" runat="server" Width="250px" />
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataInizioRapportoLavoro" runat="server"
                    ControlToValidate="RadDateInputDataInizioRapportoLavoro" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Data inizio rapporto di lavoro mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidatorDataInizioRapportoLavoro" runat="server" ControlToValidate="RadDateInputDataInizioRapportoLavoro"
                    ErrorMessage="Data inizio rapporto di lavoro non valida" Display="Dynamic" ValidationGroup="rapportoDiLavoro">
                *
                </asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                <asp:Label ID="LabelDataFineRapporto" runat="server">Data fine rapporto di lavoro:</asp:Label>
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerDataFineRapporto" runat="server"
                    Width="250px" />
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataFineRapporto" Enabled="false"
                    runat="server" ControlToValidate="RadDatePickerDataFineRapporto" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Data inizio rapporto di lavoro mancante">
                *
                </asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidatorDataFineRapporto" Enabled="false" runat="server"
                    ControlToCompare="RadDateInputDataInizioRapportoLavoro" ControlToValidate="RadDatePickerDataFineRapporto"
                    ErrorMessage="Data fine rapporto di lavoro non valida" Operator="GreaterThan"
                    Display="Dynamic" ValidationGroup="rapportoDiLavoro">
                *
                </asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="LabelDataAssunzione" runat="server" ForeColor="Gray" Font-Size="Smaller"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Contratto applicato<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipologiaContrattuale" runat="server" Width="250px"
                    EmptyMessage="Selezionare una tipologia contrattuale" MarkFirstMatch="true" AllowCustomText="false"
                    AutoPostBack="True" EnableScreenBoundaryDetection="false" OnSelectedIndexChanged="RadComboBoxTipologiaContrattuale_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorTipologiaContrattuale" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una tipologia contrattuale" OnServerValidate="CustomValidatorTipologiaContrattuale_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
    <br />
    <table runat="server" width="100%" class="borderedTable">
        <tr>
            <td colspan="2">
                <b>Tipologia di rapporto </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Tipologia contrattuale di assunzione:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxTipologiaInizioRapporto" runat="server" Width="250px"
                    OnSelectedIndexChanged="RadComboBoxTipologiaInizioRapporto_SelectedIndexChanged"
                    AutoPostBack="True" EmptyMessage="Selezionare la tipologia" MarkFirstMatch="true"
                    EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Tipologia orario Part-time:
            </td>
            <td>
                <asp:RadioButtonList ID="RadioButtonListPartTime" runat="server" RepeatDirection="Horizontal"
                    AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListPartTime_SelectedIndexChanged">
                    <asp:ListItem>SI</asp:ListItem>
                    <asp:ListItem Selected="True">NO</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                % Part-time:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxOrarioMedioSettimanale" runat="server" Width="250px"
                    MaxLength="5" Enabled="False">
                </telerik:RadTextBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorPercentualePartTime" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Inserire una percentuale Part time" OnServerValidate="CustomValidatorPercentualePartTime_ServerValidate">
                *
                </asp:CustomValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Inserire una percentuale Part time corretta (0 - 1,0)"
                    MaximumValue="1,0" MinimumValue="0" ValidationGroup="rapportoDiLavoro" ControlToValidate="RadTextBoxOrarioMedioSettimanale"
                    Type="Double">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Livello di inquadramento:
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxLivelloInquadramento" runat="server" Width="250px">
                </telerik:RadTextBox>
            </td>
        </tr>
    </table>
    <br />
    <table class="borderedTable">
        <tr>
            <td colspan="3">
                <b>Tipologia di rapporto </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Categoria<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxCategoria" runat="server" Width="250px" EmptyMessage="Selezionare una categoria"
                    MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="True" EnableScreenBoundaryDetection="false"
                    OnSelectedIndexChanged="RadComboBoxCategoria_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorTipologiaCategoria" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una categoria" OnServerValidate="CustomValidatorTipologiaCategoria_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Qualifica<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxQualifica" runat="server" Width="250px" EmptyMessage="Selezionare la qualifica"
                    MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="false" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorQualifica" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una qualifica" OnServerValidate="CustomValidatorQualifica_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="iscrizioneLavoratoriTd">
                Mansione<b>*</b>:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxMansione" runat="server" Width="250px" EmptyMessage="Selezionare la mansione"
                    MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="false" EnableScreenBoundaryDetection="false">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:CustomValidator ID="CustomValidatorMansione" runat="server" ValidationGroup="rapportoDiLavoro"
                    ErrorMessage="Selezionare una mansione" OnServerValidate="CustomValidatorMansione_ServerValidate">
                *
                </asp:CustomValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<telerik:RadMultiPage ID="RadMultiPageAttivita" runat="server" Width="100%" SelectedIndex="0"
    RenderSelectedPageOnly="true">
    <telerik:RadPageView ID="RadPageViewAssunzione" runat="server">
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewCessazione" runat="server">
        <table class="borderedTable">
            <tr>
                <td colspan="3">
                    <b>Dati cessazione </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Data cessazione (gg/mm/aaaa)<b>*</b>:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDateInputDataCessazione" runat="server" Width="250px" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataCessazione" runat="server"
                        ControlToValidate="RadDateInputDataCessazione" ValidationGroup="rapportoDiLavoro"
                        ErrorMessage="Data cessazione mancante" Enabled="false">
                        *
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidatorDataCessazione" runat="server" ControlToValidate="RadDateInputDataCessazione"
                        ErrorMessage="Data cessazione non valida" Display="Dynamic" MaximumValue="2050-12-31-00-00-00"
                        MinimumValue="1900-01-01-00-00-00" ValidationGroup="rapportoDiLavoro" Enabled="false">
                        *
                    </asp:RangeValidator>
                    <asp:CustomValidator ID="CustomValidatorDataCessazione" runat="server" ValidationGroup="rapportoDiLavoro"
                        ErrorMessage="La data di cessazione del rapporto non può essere precedente alla data di inizio rapporto"
                        OnServerValidate="CustomValidatorDataCessazione_ServerValidate" Enabled="false">
                        *
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    0 Causa<b>*</b>:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxCausa" runat="server" Width="250px" EmptyMessage="Selezionare una causa"
                        MarkFirstMatch="true" AllowCustomText="false" AutoPostBack="false" EnableScreenBoundaryDetection="false">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCausa" runat="server" ControlToValidate="RadComboBoxCausa"
                        ValidationGroup="rapportoDiLavoro" ErrorMessage="Selezionare una causa" Enabled="false">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewTrasformazione" runat="server">
        <table class="borderedTable">
            <tr>
                <td colspan="3">
                    <b>Dati trasformazione </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Data trasformazione<b>*</b>:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDateInputDataTrasformazione" runat="server" Width="250px" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataTrasformazione" runat="server"
                        ControlToValidate="RadDateInputDataTrasformazione" ValidationGroup="rapportoDiLavoro"
                        ErrorMessage="Data trasformazione mancante" Enabled="false">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="iscrizioneLavoratoriTd">
                    Codice trasformazione<b>*</b>:
                </td>
                <td>
                    <telerik:RadComboBox ID="RadComboBoxCodiceTrasformazione" runat="server" Width="250px"
                        EmptyMessage="Selezionare un codice trasformazione" Filter="Contains" MarkFirstMatch="true"
                        AllowCustomText="true">
                    </telerik:RadComboBox>
                </td>
            </tr>
        </table>
    </telerik:RadPageView>
    <telerik:RadPageView ID="RadPageViewProroga" runat="server">
    </telerik:RadPageView>
</telerik:RadMultiPage>
