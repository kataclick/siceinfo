﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.IscrizioneLavoratoreImpresa" %>

<table class="borderedTable">
    <tr>
        <td colspan="2">
            In caso di dati non corretti contattare la Cassa Edile di Milano.
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Codice:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCodice" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Ragione sociale:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxRagioneSociale" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Partita IVA:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxPartitaIva" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Tipologia contrattuale:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTipologiaContrattuale" runat="server" Width="250px"
                EmptyMessage="Selezionare una tipologia contrattuale" Filter="Contains" MarkFirstMatch="true"
                AllowCustomText="true" Enabled="false">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Sede legale
        </td>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxProvincia" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxComune" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadTextBox ID="RadTextBoxCap" runat="server" Width="250px" Enabled="false">
                        </telerik:RadTextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxTelefono" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxFax" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            E-Mail:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxEMail" runat="server" Width="250px" Enabled="false">
            </telerik:RadTextBox>
        </td>
    </tr>
</table>
