﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RicercaLavoratore.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.RicercaLavoratore" %>

<asp:Panel ID="PanelRicercaLavoratore" runat="server" DefaultButton="ButtonRicerca">
    <table class="borderedTable">
        <tr>
            <td>
                <b>Ricerca da anagrafica</b>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Codice
                        </td>
                        <td>
                            Cognome
                        </td>
                        <td>
                            Nome
                        </td>
                        <td>
                            Codice fiscale
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxCodice" runat="server" MaxLength="8"
                                Width="100%" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" NumberFormat-AllowRounding="false">
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxCognome" runat="server" MaxLength="30" Width="100%">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxNome" runat="server" MaxLength="30" Width="100%">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" MaxLength="16" Width="100%">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right" colspan="2">
                            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" OnClick="ButtonRicerca_Click"
                                Style="margin-top: 15px; margin-left: 15px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <telerik:RadGrid ID="RadGridLavoratori" runat="server" Width="100%" AllowPaging="True"
                                OnPageIndexChanged="RadGridLavoratori_PageIndexChanged" GridLines="None" PageSize="5"
                                Visible="False">
                                <MasterTableView DataKeyNames="IdLavoratore">
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px" />
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px" />
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="IdLavoratore" HeaderText="Codice" UniqueName="column">
                                            <ItemStyle Width="50px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Cognome" HeaderText="Cognome" UniqueName="column1">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Nome" HeaderText="Nome" UniqueName="column2">
                                            <ItemStyle Width="200px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DataNascita" HeaderText="Data Nascita" UniqueName="column3"
                                            DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                                            <ItemStyle Width="50px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CodiceFiscale" HeaderText="Codice Fiscale" UniqueName="column4">
                                            <ItemStyle Width="50px" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:Button ID="ButtonConferma" runat="server" Text="Seleziona" Width="150px" Visible="false"
                                OnClick="ButtonConferma_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
