﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiepilogoDettagliConsulente.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.RiepilogoDettagliConsulente" %>

<style type="text/css">
    .style2
    {
        width: 300px;
    }
</style>
<table width="98%" class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Dati consulente</b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style2">
            Ragione sociale
        </td>
        <td>
            <b>
                <asp:Label ID="LabelRagioneSociale" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <small>Codice fiscale </small>
        </td>
        <td>
            <small>
                <asp:Label ID="LabelCodiceFiscale" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <small>Telefono </small>
        </td>
        <td>
            <small>
                <asp:Label ID="LabelTelefono" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <small>Fax </small>
        </td>
        <td>
            <small>
                <asp:Label ID="LabelFax" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <small>E-Mail </small>
        </td>
        <td>
            <small>
                <asp:Label ID="LabelEMail" runat="server">
                </asp:Label>
            </small>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
</table>
