﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneLavoratoreDatiAnagraficiDocumenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.IscrizioneLavoratoreDatiAnagraficiDocumenti" %>

<%@ Register Src="RicercaLavoratore.ascx" TagName="RicercaLavoratore" TagPrefix="uc5" %>
<%@ Register Src="IscrizioneLavoratoreDatiAnagrafici.ascx" TagName="IscrizioneLavoratoreDatiAnagrafici"
    TagPrefix="uc4" %>
<%@ Register Src="IscrizioneLavoratoreDocumenti.ascx" TagName="IscrizioneLavoratoreDocumenti"
    TagPrefix="uc8" %>
<asp:Panel ID="PanelRicerca" runat="server" Width="100%">
    <uc5:RicercaLavoratore ID="RicercaLavoratore1" runat="server" />
</asp:Panel>
<br />
<asp:Panel ID="PanelVisualizzazione" runat="server" Width="100%">
    <uc4:IscrizioneLavoratoreDatiAnagrafici ID="IscrizioneLavoratoreDatiAnagrafici1"
        runat="server" />
</asp:Panel>
<br />
<asp:Panel ID="PanelDocumenti" runat="server" Width="100%">
    <uc8:IscrizioneLavoratoreDocumenti ID="IscrizioneLavoratoreDocumenti1" runat="server" />
</asp:Panel>