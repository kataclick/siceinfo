﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Collections;
using TBridge.Cemi.IscrizioneLavoratori.Type.Delegates;
using TBridge.Cemi.IscrizioneLavoratori.Type.Filters;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class RicercaLavoratore : System.Web.UI.UserControl
    {
        private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
        private readonly Common commonBiz = new Common();
        public event LavoratoreSelectedEventHandler OnLavoratoreSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            //RadNumericTextBoxCodice.DataType = typeof (Int32);
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            CaricaLavoratori();
        }

        private void CaricaLavoratori()
        {
            RadGridLavoratori.Visible = true;
            LavoratoreFilter filtro = GetFiltro();

            Int32 idImpresa = -1;
            String ragSocImpresa = null;
            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                commonBiz.ConsulenteImpresaSelezionata(consulente.IdConsulente, out idImpresa, out ragSocImpresa);
            }
            if (GestioneUtentiBiz.IsImpresa())
            {
                Impresa impresa =
                    (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                idImpresa = impresa.IdImpresa;
            }

            LavoratoreCollection lavoratori = biz.GetLavoratori(filtro, idImpresa);
            RadGridLavoratori.DataSource = lavoratori;
            RadGridLavoratori.DataBind();

            if (lavoratori.Count > 0)
            {
                ButtonConferma.Visible = true;
            }
            else
            {
                ButtonConferma.Visible = false;
            }
        }

        private LavoratoreFilter GetFiltro()
        {
            LavoratoreFilter filtro = new LavoratoreFilter();

            if (!String.IsNullOrEmpty(RadNumericTextBoxCodice.Text))
            {
                filtro.Codice = Int32.Parse(RadNumericTextBoxCodice.Text);
            }

            if (!String.IsNullOrEmpty(RadTextBoxCognome.Text))
            {
                filtro.Cognome = Presenter.NormalizzaCampoTesto(RadTextBoxCognome.Text);
            }

            if (!String.IsNullOrEmpty(RadTextBoxNome.Text))
            {
                filtro.Nome = Presenter.NormalizzaCampoTesto(RadTextBoxNome.Text);
            }

            if (!String.IsNullOrEmpty(RadTextBoxCodiceFiscale.Text))
            {
                filtro.CodiceFiscale = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);
            }

            return filtro;
        }

        protected void RadGridLavoratori_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaLavoratori();
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            if (RadGridLavoratori.SelectedIndexes.Count == 1)
            {
                Int32 idLavoratore =
                    (Int32)
                    RadGridLavoratori.MasterTableView.DataKeyValues[Int32.Parse(RadGridLavoratori.SelectedIndexes[0])][
                        "IdLavoratore"];
                if (Parent.FindControl("PanelVisualizzazione") != null)
                    Parent.FindControl("PanelVisualizzazione").Visible = true;

                if (OnLavoratoreSelected != null)
                {
                    OnLavoratoreSelected(idLavoratore);
                }
            }
        }

        public void Reset()
        {
            ResetFiltro();
            RadGridLavoratori.Visible = false;
            ButtonConferma.Visible = false;
        }

        private void ResetFiltro()
        {
            RadNumericTextBoxCodice.Text = null;
            RadTextBoxCodiceFiscale.Text = null;
            RadTextBoxCognome.Text = null;
            RadTextBoxNome.Text = null;
        }
    }
}