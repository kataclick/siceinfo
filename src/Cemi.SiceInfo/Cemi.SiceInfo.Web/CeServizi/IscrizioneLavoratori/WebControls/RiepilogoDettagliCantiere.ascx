﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiepilogoDettagliCantiere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.RiepilogoDettagliCantiere" %>

<style type="text/css">
    .style1
    {
        width: 30px;
    }
</style>
<table class="borderedTable">
    <tr>
        <td>
            <b>Dati cantiere</b>
        </td>
        <td>
            <table id="tableControlli" runat="server" class="standardTable">
                <tr>
                    <td class="style1">
                        <asp:Image ID="ImageControlloCantiere" runat="server" ImageUrl="../../images/semaforoGiallo.png" />
                    </td>
                    <td>
                        <small>Cantiere nelle notifiche preliminari </small>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Indirizzo
        </td>
        <td>
            <b>
                <asp:Label ID="LabelIndirizzo" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
</table>
