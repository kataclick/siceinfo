﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Type.Delegates;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class IscrizioneLavoratoreAttivita : System.Web.UI.UserControl
    {
        public event TipoAttivitaSelectedEventHandler OnAttivitaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadioButtonListTipoAttivita_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OnAttivitaSelected != null)
            {
                TipoAttivita tipoEvento =
                    (TipoAttivita)
                    Enum.Parse(typeof(TipoAttivita), RadioButtonListTipoAttivita.SelectedValue);
                OnAttivitaSelected(tipoEvento);
            }
        }

        public TipoAttivita GetAttivita()
        {
            return
                (TipoAttivita)
                Enum.Parse(typeof(TipoAttivita), RadioButtonListTipoAttivita.SelectedValue);
        }
    }
}