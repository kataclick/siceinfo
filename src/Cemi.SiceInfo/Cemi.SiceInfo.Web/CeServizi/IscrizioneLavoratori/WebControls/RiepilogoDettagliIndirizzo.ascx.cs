﻿using System;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls
{
    public partial class RiepilogoDettagliIndirizzo : System.Web.UI.UserControl
    {
        private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public Boolean CaricaDichiarazione(Dichiarazione dichiarazione)
        {
            Boolean abilitaTastoAccetta = false;

            LabelIndirizzo.Text = dichiarazione.Lavoratore.IndirizzoCompleto;
            LabelTelefono.Text = dichiarazione.Lavoratore.NumeroTelefono;
            LabelCellulare.Text = dichiarazione.Lavoratore.NumeroTelefonoCellulare;
            LabelEmail.Text = dichiarazione.Lavoratore.Email;
            CheckBoxSms.Checked = dichiarazione.Lavoratore.AderisceServizioSMS;

            EffettuaControlli(dichiarazione);

            if (ImageControlloIndirizzo.ImageUrl == IscrizioneLavoratoriManager.URLSEMAFOROVERDE)
            {
                abilitaTastoAccetta = true;
            }

            return abilitaTastoAccetta;
        }

        private void EffettuaControlli(Dichiarazione dichiarazione)
        {
            switch (dichiarazione.Attivita)
            {
                case TipoAttivita.Assunzione:
                case TipoAttivita.Cessazione:
                    tableControlli.Visible = true;

                    Lavoratore lavoratore = null;
                    if (dichiarazione.IdLavoratoreSelezionato.HasValue)
                    {
                        lavoratore = biz.GetLavoratore(dichiarazione.IdLavoratoreSelezionato.Value);
                    }

                    ImageControlloIndirizzo.ImageUrl = biz.IndirizzoDiverso(dichiarazione, lavoratore);
                    ImageControlloCellulare.ImageUrl = biz.CellulareDiverso(dichiarazione, lavoratore);
                    ImageControlloEmail.ImageUrl = biz.EmailDiversa(dichiarazione, lavoratore);
                    break;
                default:
                    tableControlli.Visible = false;
                    break;
            }
        }
    }
}