﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RiepilogoDettagliDichiarazione.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.WebControls.RiepilogoDettagliDichiarazione" %>
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Dati richiesta</b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            ID
        </td>
        <td>
            <b>
                <asp:Label ID="LabelID" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Attività
        </td>
        <td>
            <b>
                <asp:Label ID="LabelTipoAttivita" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
        </td>
        <td>
            <b>
                <asp:Label ID="LabelRettifica" runat="server" >
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data inserimento
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDataInserimento" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Stato
        </td>
        <td>
            <b>
                <asp:Label ID="LabelStato" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="iscrizioneLavoratoriTd">
            Data cambio stato
        </td>
        <td>
            <b>
                <asp:Label ID="LabelDataCambioStato" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
    <tr >
        <td class="iscrizioneLavoratoriTd">
            Utente cambio stato
        </td>
           <td>
            <b>
                <asp:Label ID="LabelUtenteCambioStato" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td>
            Provenienza dei dati
        </td>
        <td>
            <b>
                <asp:Label ID="LabelProvenienza" runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
</table>
