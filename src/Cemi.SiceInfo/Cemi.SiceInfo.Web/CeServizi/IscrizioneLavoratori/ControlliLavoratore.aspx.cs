﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
//using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class ControlliLavoratore : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE);

            #endregion

            if (!Page.IsPostBack)
            {
                ViewState["IdDichiarazione"] = Context.Items["IdDichiarazione"];
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Context.Items["IdDichiarazione"] = ViewState["IdDichiarazione"];
            Server.Transfer("~/CeServizi/IscrizioneLavoratori/DettaglioDichiarazione.aspx");
        }
    }
}