﻿using System;
using System.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class ComunicazioneServizioNonDisponibile : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request["ErrorMessage"]))
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception($"Exception for ComunicazioneNonDisponibile - {Request["ErrorMessage"]}"));
            }
        }
    }
}