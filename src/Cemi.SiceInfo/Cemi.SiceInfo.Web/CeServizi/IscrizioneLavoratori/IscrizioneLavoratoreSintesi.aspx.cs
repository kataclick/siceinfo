﻿using System;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.Type.Entities.Sintesi;
using TBridge.Cemi.Type.Entities.Sintesi.Original;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class IscrizioneLavoratoreSintesi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FormsAuthentication.SetAuthCookie("UtenteSintesi", false);
                string idComunicazione = Request["idComunicazione"];
                SintesiManager sintesiManager = new SintesiManager();

                SintesiServiceResult result = sintesiManager.GetRapportoLavoro(idComunicazione, out var rapportoLavoro);
                switch (result.Error)
                {
                    case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.NoError:
                        if (!(rapportoLavoro.Item is Contratto))
                        {
                            Response.Redirect("~/CeServizi/IscrizioneLavoratori/ComunicazioneNonGestita.aspx");
                        }
                        else if (sintesiManager.DichiarazioneExistByIdComunicazioneSintesi(idComunicazione))
                        {
                            Response.Redirect("~/CeServizi/IscrizioneLavoratori/ComunicazioneGiaEffettuata.aspx");
                        }
                        else if (!sintesiManager.IsDataAssunzioneValida(rapportoLavoro))
                        {
                            Response.Redirect("~/CeServizi/IscrizioneLavoratori/ComunicazioneDataAssunzioneNonValida.aspx");
                        }

                        IscrizioneLavoratore1.RapportoLavoro = rapportoLavoro;
                        IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;
                        Master.FindControl("LoginMain").Visible = false;

                        break;
                    case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.ServiceCallFailed:
                    case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.InvalidResponse:
                        Response.Redirect("~/CeServizi/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx");
                        break;
                    case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.ServiceErrorMessage:
                        //Context.Items["ErrorMessage"] = result.Message;
                        Response.Redirect(String.Format("~/CeServizi/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage={0}", result.Message));
                        break;
                    case TBridge.Cemi.IscrizioneLavoratori.Type.Enums.SintesiServiceErrors.InvalidId:
                        String msg = "Id comunicazione non valido.";
                        Response.Redirect(String.Format("~/CeServizi/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage={0}", msg));
                        break;
                    default:
                        break;
                }

            }
        }
    }
}