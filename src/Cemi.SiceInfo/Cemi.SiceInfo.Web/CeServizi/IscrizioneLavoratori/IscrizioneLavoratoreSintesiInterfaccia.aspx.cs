﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using TBridge.Cemi.IscrizioneLavoratori.Type.Enums;
using TBridge.Cemi.Type.Entities.Sintesi.Original;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Consulente = TBridge.Cemi.Type.Entities.GestioneUtenti.Consulente;
using Impresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class IscrizioneLavoratoreSintesiInterfaccia : Page
    {
        private readonly SintesiManager _sintesiManager = new SintesiManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite
                .IscrizioneLavoratoriIscrizioneSintesiInterfaccia);

            #endregion
        }


        protected void ButtonCaricaComunicazione_OnClick(object sender, EventArgs e)
        {
            CaricaRapportoDiLavoro();
        }


        private void CaricaRapportoDiLavoro()
        {
            string idComunicazione = TextBoxCodiceComunicazione.Text;

            SintesiServiceResult result = _sintesiManager.GetRapportoLavoro(idComunicazione, out var rapportoLavoro);
            switch (result.Error)
            {
                case SintesiServiceErrors.NoError:
                    if (!(rapportoLavoro.Item is Contratto))
                    {
                        Server.Transfer("~/CeServizi/IscrizioneLavoratori/ComunicazioneNonGestita.aspx");
                    }
                    else if (!IsComuniazioneValidaPerImpresa(rapportoLavoro))
                    {
                        Server.Transfer("~/CeServizi/IscrizioneLavoratori/ComunicazioneImpresaErrata.aspx");
                    }
                    else if (!IsComuniazioneValidaPerConsulente(rapportoLavoro))
                    {
                        Context.Items["ErrorMessage"] =
                            "La comunicazione risulta associata ad un'impresa non di competenza.";
                        Server.Transfer($"~/CeServizi/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage=NoError_{result.Message}");
                    }
                    else if (_sintesiManager.DichiarazioneExistByIdComunicazioneSintesi(idComunicazione))
                    {
                        Server.Transfer("~/CeServizi/IscrizioneLavoratori/ComunicazioneGiaEffettuata.aspx");
                    }
                    else if (!_sintesiManager.IsDataAssunzioneValida(rapportoLavoro))
                    {
                        Server.Transfer("~/CeServizi/IscrizioneLavoratori/ComunicazioneDataAssunzioneNonValida.aspx");
                    }
                    else
                    {
                        IscrizioneLavoratore1.IscrizioneCorsiAbilitata = false;
                        IscrizioneLavoratore1.CaricaRapportoDiLavoro(rapportoLavoro);
                        PanelCodiceComunicaione.Visible = false;
                        IscrizioneLavoratore1.Visible = true;
                    }
                    break;
                case SintesiServiceErrors.ServiceCallFailed:
                    Server.Transfer($"~/CeServizi/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx?ErrorMessage=ServiceCallFailed_{result.Message}");
                    break;
                case SintesiServiceErrors.InvalidResponse:
                    Server.Transfer($"~/CeServizi/IscrizioneLavoratori/ComunicazioneServizioNonDisponibile.aspx?ErrorMessage=InvalidReponse_{result.Message}");
                    break;
                case SintesiServiceErrors.ServiceErrorMessage:
                    Context.Items["ErrorMessage"] = result.Message;
                    Server.Transfer($"~/CeServizi/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage=ServiceErrorMessage_{result.Message}");
                    break;
                case SintesiServiceErrors.InvalidId:
                    Context.Items["ErrorMessage"] = "Id comunicazione non valido.";
                    Server.Transfer($"~/CeServizi/IscrizioneLavoratori/ComunicazioneErrore.aspx?ErrorMessage=InvalidId_{result.Message}");
                    break;
                default:
                    break;
            }
        }


        private bool IsComuniazioneValidaPerImpresa(RapportoLavoro rapportoLavoro)
        {
            bool result = true;

            if (GestioneUtentiBiz.IsImpresa())
            {
                Impresa impresaCorrente = (Impresa) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                result = string.Equals(rapportoLavoro.DatoreLavoro.codiceFiscale, impresaCorrente.CodiceFiscale,
                    StringComparison.CurrentCultureIgnoreCase);
            }

            return result;
        }

        private bool IsComuniazioneValidaPerConsulente(RapportoLavoro rapportoLavoro)
        {
            bool result = true;

            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulenteCorrente = (Consulente) GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                result = _sintesiManager.IsImpresaCompetenzaConsulente(consulenteCorrente.IdConsulente,
                    rapportoLavoro.DatoreLavoro.codiceFiscale);
            }

            return result;
        }
    }
}