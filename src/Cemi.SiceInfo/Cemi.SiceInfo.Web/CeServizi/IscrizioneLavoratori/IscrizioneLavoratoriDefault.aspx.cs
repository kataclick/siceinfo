﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class IscrizioneLavoratoriDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
            funzionalita.Add(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE);
            funzionalita.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion
        }
    }
}