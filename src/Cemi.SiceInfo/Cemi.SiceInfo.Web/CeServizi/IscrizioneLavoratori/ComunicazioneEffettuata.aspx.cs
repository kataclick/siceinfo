﻿using System;
using System.Web.Security;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
//using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.IscrizioneLavoratori.Business;
using TBridge.Cemi.IscrizioneLavoratori.Type.Entities;
using Impresa = TBridge.Cemi.IscrizioneLavoratori.Type.Entities.Impresa;
using System.Collections.Generic;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori
{
    public partial class ComunicazioneEffettuata : System.Web.UI.Page
    {
        private readonly IscrizioneLavoratoriManager biz = new IscrizioneLavoratoriManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            System.Collections.Generic.List<FunzionalitaPredefinite> autorizzazioni = new System.Collections.Generic.List<FunzionalitaPredefinite>();
            autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione);
            autorizzazioni.Add(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia);
            GestioneAutorizzazionePagine.PaginaAutorizzata(autorizzazioni);

            Utente utente = (Utente)Membership.GetUser();

            if (utente != null)
            {
                if (utente.IdUtente > 0)
                {
                    GestioneUtentiBiz bizUtenti = new GestioneUtentiBiz();
                    RuoliCollection ruoli = bizUtenti.GetRuoliUtente(utente.IdUtente);
                    if (ruoli.Count == 1 &&
                        (ruoli[0].Nome == "IscrizioneLavoratoreSintesi"))
                    {
                        MenuIscrizioneLavoratori1.Visible = false;
                        LabelNuovaComunicazione.Visible = false;
                    }
                }
            }


            #endregion

            if (!Page.IsPostBack)
            {
                Dichiarazione dichiarazione = CaricaDichiarazione();
                if (dichiarazione != null)
                    ButtonCorsi.Visible = dichiarazione.Lavoratore.RichiestaIscrizioneCorsi16Ore;
            }
        }

        private Dichiarazione CaricaDichiarazione()
        {
            //if (Context.Items["Dichiarazione"] != null)
            if (Session["Dichiarazione"] != null)
                //return (Dichiarazione)Context.Items["Dichiarazione"];
                return (Dichiarazione)Session["Dichiarazione"];

            return null;
        }
        protected void ButtonCorsi_Click(object sender, EventArgs e)
        {
            Dichiarazione dichiarazione = CaricaDichiarazione();

            if (dichiarazione != null)
            {
                Impresa impresa = biz.GetImpresa(dichiarazione.IdImpresa);

                Context.Items["impresaIscrizioneLavoratori"] = impresa;
                Context.Items["lavoratoreIscrizioneLavoratori"] = dichiarazione.Lavoratore;
            }

            if (GestioneUtentiBiz.IsConsulente())
            {
                if (dichiarazione != null)
                {
                    Context.Items["consulenteIscrizioneLavoratori"] = dichiarazione.Consulente;
                }

                Server.Transfer("~/CeServizi/Corsi/IscrizioneCorsiConsulente.aspx");
            }
            else
                Server.Transfer("~/CeServizi/Corsi/IscrizioneCorsiImpresa.aspx");

        }
    }
}