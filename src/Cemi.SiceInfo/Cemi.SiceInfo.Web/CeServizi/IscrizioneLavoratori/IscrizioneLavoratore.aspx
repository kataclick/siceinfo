﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IscrizioneLavoratore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.IscrizioneLavoratore" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="WebControls/IscrizioneLavoratore.ascx" TagName="IscrizioneLavoratore" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuIscrizioneLavoratori.ascx" TagName="MenuIscrizioneLavoratori" TagPrefix="uc3" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Iscrizione" titolo="Notifiche lavoratori" />
    <br />
    <uc2:IscrizioneLavoratore ID="IscrizioneLavoratore1" runat="server" />
</asp:Content>
<asp:Content ID="Content8" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>
