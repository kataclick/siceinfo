﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ComunicazioneGiaEffettuata.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneLavoratori.ComunicazioneGiaEffettuata" %>

<%@ Register src="../WebControls/MenuIscrizioneLavoratori.ascx" tagname="MenuIscrizioneLavoratori" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>


<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuIscrizioneLavoratori ID="MenuIscrizioneLavoratori1" runat="server" />
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche lavoratori" sottoTitolo="Comunicazione già effettuata" />
    <br />
    <div> 
    <table class="standardTable">
    <tr>
    <td>
    Esiste già una comunicazione con i dati inseriti.
    </td>
    </tr>
    </table>
</div>

</asp:Content>
