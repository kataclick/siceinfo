﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="GestioneSmsLavoratore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.GestioneSmsLavoratore" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <table class="standardTable">
        <tr>
            <td>
                Cod. Lavoratore
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxIdLav" runat="server" ReadOnly="true" />
            </td>
        </tr>
        <tr>
            <td>
                Telefono
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxTelefono" runat="server" ReadOnly="true" />
            </td>
            <td>
                <asp:Button ID="RadButtonModificaNumero" runat="server" Text="Modifica" Enabled="true" OnClick="RadButtonModificaNumero_Click" />
            </td>
            <td>
                <asp:Button ID="RadButtonSalvaNumero" runat="server" Text="Salva" Enabled="false" OnClick="RadButtonSalvaNumero_Click" />
            </td>
            <td>
                <asp:Button ID="RadButtonDisabilitaNumero" runat="server" Text="Disabilita" Enabled="true" OnClick="RadButtonDisabilitaNumero_Click" />
            </td>
        </tr>
        <tr>
            <td>
                PIN
            </td>
            <td>
                <telerik:RadTextBox ID="RadTextBoxPin" runat="server" ReadOnly="true" />
            </td>
            <td>
                <asp:Button ID="RadButtonGeneraPin" runat="server" Text="Genera" OnClick="RadButtonGeneraPin_Click" />
            </td>
            <td>
                <asp:Button ID="RadButtonInviaPinSms" runat="server" Text="Invia SMS" OnClick="RadButtonInviaPinSms_Click" />
            </td>
        </tr>        
    </table>
    <br />
    <br />
    <asp:Label ID="LabelError" runat="server" ForeColor="Red" />
    </form>
</body>
</html>

