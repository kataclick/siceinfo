﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportBollettinoFreccia.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia.ReportBollettinoFreccia" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa bollettino freccia"
        titolo="BollettinoFreccia" />
    <br />
    <table height="600px" width="740px"><tr><td>
    <rsweb:ReportViewer ID="ReportViewerBollettinoFreccia" runat="server" ProcessingMode="Remote"
        width="100%">
    </rsweb:ReportViewer>
    </td></tr></table>
    <br />
</asp:Content>
