﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RichiestaBollettinoFrecciaDettaglioReport.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia.RichiestaBollettinoFrecciaDettaglioReport" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa Dettaglio"
        titolo="Bollettino Freccia - MAV" />
    <br />
    <br />
    <rsweb:ReportViewer ID="ReportViewerDettaglio" runat="server" ProcessingMode="Remote" Height="550pt" Width="550pt" ShowParameterPrompts="false" />
</asp:Content>
<asp:Content ID="Content6" runat="server" contentplaceholderid="MenuDettaglio">
</asp:Content>
