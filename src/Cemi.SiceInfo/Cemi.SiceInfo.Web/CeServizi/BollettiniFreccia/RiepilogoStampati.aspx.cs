﻿using System;
using System.Collections.Generic;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Filters;
using Telerik.Web.UI;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia
{
    public partial class RiepilogoStampati : System.Web.UI.Page
    {
        private readonly Common _bizCommon = new Common();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaStatistiche);

            #endregion

            if (!Page.IsPostBack)
            {
                RadTextBoxAnno.MaxValue = DateTime.Today.Year;
                RadDatePickerDataInizio.SelectedDate = DateTime.Today.AddMonths(-1);
                CaricaStatisticheBollettiniStampatiPagati();
                CaricaDati();
                ButtonFiltra.Focus();
            }
        }

        private void CaricaDati()
        {
            CaricaTipiCanalePagamento();
            CaricaBollettiniStampati();
        }

        private void CaricaTipiCanalePagamento()
        {
            List<TipoCanalePagamentoBolletino> tipiPagmento = _bizCommon.GetTipiCanalePagamentoBollettino(true);
            RadCombBoxTipoCanalePagamento.DataSource = tipiPagmento;
            RadCombBoxTipoCanalePagamento.DataBind();
        }

        private void CaricaBollettiniStampati()
        {
            BollettinoStampatoFilter filtro = new BollettinoStampatoFilter
            {
                Anno = (Int32?)RadTextBoxAnno.Value,
                Mese = (Int32?)RadTextBoxMese.Value,
                DataInizio = RadDatePickerDataInizio.SelectedDate,
                DataFine = RadDatePickerDataFine.SelectedDate,
                RagioneSociale = RadTextBoxRagioneSociale.Text,
                CanalePagamento = GetFiltroTipoCanalePagamento(),
                IdImpresa = (Int32?)RadNumericTextBoxIdImpresa.Value
            };

            List<BollettinoStampato> bollettini = _bizCommon.GetBollettiniStampati(filtro);
            GridViewBollettiniFreccia.DataSource = bollettini;
            GridViewBollettiniFreccia.DataBind();
        }

        private void CaricaStatisticheBollettiniStampatiPagati()
        {
            _bizCommon.GetUtentiBollettiniStampati(out var numeroImprese, out var numeroConsulenti);
            LabelNumeroImprese.Text = $"Imprese: {numeroImprese}";
            LabelNumeroConsulenti.Text = $"Consulenti: {numeroConsulenti}";

            LabelBolletiniStampabili.Text = _bizCommon.GetBollettiniFrecciaStampabili(null, null).ToString();

            BollettiniFrecciaStatistichePagati bollettiniFrecciaStatistichePagati = _bizCommon.GetBollettiniStatistichePagati();

            LabelBolletiniStampati.Text =
                $"{bollettiniFrecciaStatistichePagati.NumeroBollettiniUniciStampati} di cui {bollettiniFrecciaStatistichePagati.NumeroBollettiniUniciStampatiPagati} pagati per Tot. {bollettiniFrecciaStatistichePagati.ImportoPagato:C}";
        }

        private TipiCanalePagamento? GetFiltroTipoCanalePagamento()
        {
            TipiCanalePagamento? tipo = null;
            if (int.TryParse(RadCombBoxTipoCanalePagamento.SelectedValue, out var id) && id > 0)
            {
                tipo = (TipiCanalePagamento)id;
            }

            return tipo;
        }

        protected void ButtonFiltra_Click(object sender, EventArgs e)
        {
            CustomValidatorStampa.Visible = false;
            CustomValidatorStampa1.Visible = false;
            LabelFiltro.Visible = true;
            Page.Validate("filtro");
            if (Page.IsValid)
            {
                LabelFiltro.Visible = false;
                CaricaBollettiniStampati();
            }
        }

        protected void GridViewBollettiniFreccia_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaBollettiniStampati();
        }
    }
}