﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia
{
    public partial class RichiestaBollettinoFrecciaDettaglio : System.Web.UI.Page
    {
        private readonly Common bizCommon = new Common();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaRichiesta);

            #endregion

            if (!Page.IsPostBack)
            {
                Int32 idImpresa = Int32.Parse(Request.QueryString["idImpresa"]);
                Int32 anno = Int32.Parse(Request.QueryString["anno"]);
                Int32 mese = Int32.Parse(Request.QueryString["mese"]);
                Int32 sequenza = Int32.Parse(Request.QueryString["sequenza"]);

                LabelAnno.Text = Request.QueryString["anno"];
                LabelMese.Text = Request.QueryString["mese"];
                LabelSequenza.Text = sequenza == 1 ? "Ordinaria" : "Integrativa";

                Bollettino bollettino = bizCommon.GetDettaglioBollettino(idImpresa, anno, mese, sequenza);

                LabelTotaleDenunciato.Text = Presenter.StampaValoreMonetario(bollettino.ImportoDovuto1 + bollettino.ImportoDovuto2 + bollettino.ImportoDovuto3);
                LabelContributoAggiuntivo.Text = Presenter.StampaValoreMonetario(bollettino.ImportoDovuto5);
                LabelInteressi.Text = Presenter.StampaValoreMonetario(bollettino.ImportoDovuto4);
                LabelTotaleDovuto.Text = Presenter.StampaValoreMonetario(bollettino.ImportoDovuto1 + bollettino.ImportoDovuto2 + bollettino.ImportoDovuto3 + bollettino.ImportoDovuto4 + bollettino.ImportoDovuto5);
                LabelTotaleVersato.Text = Presenter.StampaValoreMonetario(bollettino.Importo);
                LabelDovutoSaldo.Text = Presenter.StampaValoreMonetario(bollettino.ImportoDovuto1 + bollettino.ImportoDovuto2 + bollettino.ImportoDovuto3 + bollettino.ImportoDovuto4 + bollettino.ImportoDovuto5 - bollettino.Importo);
            }
        }

        protected void ButtonPDF_Click(object sender, EventArgs e)
        {
            Context.Items["idImpresa"] = Request.QueryString["idImpresa"];
            Context.Items["anno"] = Request.QueryString["anno"];
            Context.Items["mese"] = Request.QueryString["mese"];
            Context.Items["sequenza"] = Request.QueryString["sequenza"];

            Server.Transfer("RichiestaBollettinoFrecciaDettaglioReport.aspx");
        }

    }
}