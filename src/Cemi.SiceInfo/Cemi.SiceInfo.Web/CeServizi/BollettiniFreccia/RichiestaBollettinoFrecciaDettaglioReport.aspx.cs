﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia
{
    public partial class RichiestaBollettinoFrecciaDettaglioReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaRichiesta);

            #endregion

            if (!Page.IsPostBack)
            {
                Int32 idImpresa = Int32.Parse(Context.Items["idImpresa"].ToString());
                Int32 anno = Int32.Parse(Request.QueryString["anno"].ToString());
                Int32 mese = Int32.Parse(Request.QueryString["mese"].ToString());
                Int32 sequenza = Int32.Parse(Request.QueryString["sequenza"].ToString());

                ReportViewerDettaglio.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewerDettaglio.ServerReport.ReportPath = "/ReportBollettinoFreccia/ReportBollettinoDettaglio";

                List<ReportParameter> parametri = new List<ReportParameter>();
                parametri.Add(new ReportParameter("idImpresa", idImpresa.ToString()));
                parametri.Add(new ReportParameter("anno", anno.ToString()));
                parametri.Add(new ReportParameter("mese", mese.ToString()));
                parametri.Add(new ReportParameter("sequenza", sequenza.ToString()));
                ReportViewerDettaglio.ServerReport.SetParameters(parametri.ToArray());

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;
                byte[] bytes = null;

                //PDF
                bytes = ReportViewerDettaglio.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=DettaglioPagamento{0}{1}{2}.pdf", anno, mese, sequenza));
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }

    }
}