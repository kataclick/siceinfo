﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RichiestaBollettinoFreccia.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia.RichiestaBollettinoFreccia" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<%--<%@ Register Src="../WebControls/MenuImprese.ascx" TagName="MenuImprese" TagPrefix="uc2" %>--%>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    
<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function openRadWindowDettaglio(idImpresa, anno, mese, sequenza) {
            var oWindow = radopen("RichiestaBollettinoFrecciaDettaglio.aspx?idImpresa=" + idImpresa + "&anno=" + anno + "&mese=" + mese + "&sequenza=" + sequenza, null);
            oWindow.set_title("Dettaglio pagamento");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(400, 350);
            oWindow.center();
        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="../images/favicon.png" />

    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa Modulo"
        titolo="MAV" />
    <br />
    In caso di visualizzazione del/i periodo/i a debito procedere con la stampa del relativo modulo di pagamento. Il pagamento andrà effettuato in banca.
    Si specifica che il pagamento può essere eseguito utilizzando il MAV (Modulo Avviso Pagamento).
    <br />
    <br />
    Si ricorda che:
    <ul>
        <li style="margin-bottom: 5pt;"><b>ogni singolo modulo riguarda una sola denuncia</b> (mese/anno).
            Non ci sono moduli cumulativi per più denunce; </li>
        <li style="margin-bottom: 5pt;">in caso di denuncia integrativa viene reso disponibile il relativo modulo
            di pagamento;</li>
        <li style="margin-bottom: 5pt;">è possibile visionare il dettaglio dell’importo relativo alla singola 
            denuncia sia essa ordinaria che integrativa;</li>
        <li style="margin-bottom: 5pt;"><b>l'importo</b> relativo alla singola denuncia (mese/anno) <b>è
            aggiornato con frequenza giornaliera</b> per il ricalcolo corretto degli interessi dovuti
            a garanzia della copertura totale del debito (senza generazione di differenze di
            importo);</li>
        <li style="margin-bottom: 5pt;">la visualizzazione del periodo a debito pagato verrà
            eliminata a registrazione del <b>pagamento avvenuto</b> presso Cassa Edile.</li>
    </ul>
    <br />
    <asp:Panel ID="PanelConsulente" runat="server" Visible="False" Width="100%">
        <asp:Label ID="LabelImpresa" runat="server" Text="Impresa:"></asp:Label>&nbsp; &nbsp;<asp:DropDownList
            ID="DropDownListImprese" runat="server" AppendDataBoundItems="True" Width="284px">
        </asp:DropDownList>
        <asp:Button ID="ButtonSeleziona" runat="server" OnClick="ButtonSeleziona_Click" Text="Seleziona" /><br />
        <br />
        Impresa selezionata:&nbsp;
        <asp:Label ID="LabelImpresaSelezionata" runat="server" Font-Bold="True"></asp:Label>
    </asp:Panel>

    <table class="filledtable">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Denunce"></asp:Label>
                    </td>
                </tr>
            </table>
           
<telerik:RadGrid ID="RadGridDenunce" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" 
        PageSize="12" 
        OnRowCommand="GridViewDenunce_RowCommand" 
        onpageindexchanged="RadGridDenunce_PageIndexChanged" 
        CellSpacing="0" GridLines="None" 
        onitemcommand="RadGridDenunce_ItemCommand"
        onitemdatabound="RadGridDenunce_ItemDataBound">
        <ExportSettings>
            <Pdf PageWidth="" />
        </ExportSettings>
        <MasterTableView DataKeyNames="Anno,Mese,Sequenza,Importo,CIP">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="anno" HeaderText="Anno">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="mese" HeaderText="Mese">
                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="denunciaSequenza" 
                    FilterControlAltText="Filter Sequenza column" HeaderText="Denuncia" 
                    UniqueName="denunciaSequenza">
                    <ItemTemplate>
                        <asp:Label ID="LabelDenunciaSequenza" runat="server" Text='<%# Eval("Sequenza") %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="importo" HeaderText="Importo" DataFormatString="{0:C}">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridBoundColumn>
                <%--<telerik:GridButtonColumn Text="Stampa Freccia" ButtonType="PushButton"
                    CommandName="StampaFrecciaDenuncia" ButtonCssClass="bottoneGriglia">
                    <ItemStyle Width="100px" />
                </telerik:GridButtonColumn>--%>
                <telerik:GridButtonColumn Text="Stampa MAV" ButtonType="PushButton"
                    CommandName="StampaMavDenuncia" ButtonCssClass="bottoneGriglia">
                    <ItemStyle Width="100px" />
                </telerik:GridButtonColumn>
            </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
        </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>

    <asp:GridView ID="GridViewDenunce" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" OnPageIndexChanging="GridViewDenunce_PageIndexChanging"
        PageSize="12" DataKeyNames="Anno,Mese,Sequenza,Importo,CIP" 
        OnRowCommand="GridViewDenunce_RowCommand"
        Visible="false">
        <Columns>
            <asp:BoundField DataField="anno" HeaderText="Anno">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="mese" HeaderText="Mese">
                <ItemStyle Width="50px" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Sequenza" HeaderText="Denuncia">
                <ItemStyle Width="30px" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="importo" HeaderText="Importo" DataFormatString="{0:C}">
            <HeaderStyle HorizontalAlign="Right" />
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <%--<asp:BoundField DataField="CIP" HeaderText="Identificativo del bollettino" />--%>
           <%--<asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="Stampa Freccia" ButtonType="Button"
                CommandName="StampaFreccia">
                <ControlStyle CssClass="bottoneGriglia" Width="100px"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>--%>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="Stampa MAV" ButtonType="Button"
                CommandName="StampaMav">
                <ControlStyle CssClass="bottoneGriglia" Width="100px"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="MAV Intesa Sanpaolo" ButtonType="Button"
                CommandName="StampaMavProxima">
                <ControlStyle CssClass="bottoneGriglia" Width="100px"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
    <br />

    <table class="filledtable">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Pagamenti scaduti"></asp:Label>
                    </td>
                </tr>
            </table>
    <telerik:RadGrid ID="RadGridBollettiniFreccia" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" 
        PageSize="12" 
        OnRowCommand="GridViewBollettiniFreccia_RowCommand" 
        onpageindexchanged="RadGridBollettiniFreccia_PageIndexChanged" 
        CellSpacing="0" GridLines="None" 
        onitemcommand="RadGridBollettiniFreccia_ItemCommand" 
        onitemdatabound="RadGridBollettiniFreccia_ItemDataBound">
        <ExportSettings>
            <Pdf PageWidth="" />
        </ExportSettings>
        <MasterTableView DataKeyNames="Anno,Mese,Sequenza,Importo,CIP">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column"></RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column"></ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="anno" HeaderText="Anno">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="mese" HeaderText="Mese">
                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn DataField="Sequenza" 
                    FilterControlAltText="Filter Sequenza column" HeaderText="Denuncia" 
                    UniqueName="Sequenza">
                    <ItemTemplate>
                        <asp:Label ID="LabelSequenza" runat="server" Text='<%# Eval("Sequenza") %>'></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="importo" HeaderText="Importo" DataFormatString="{0:C}">
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Dettaglio" UniqueName="dettaglio">
                    <ItemTemplate>
                        <asp:ImageButton
                            ID="ImageButtonDettaglio"
                            runat="server"
                            ToolTip="Dettaglio"
                            ImageUrl="../images/lente.png" />
                    </ItemTemplate>
                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                </telerik:GridTemplateColumn>
                <%--<telerik:GridButtonColumn Text="Stampa Freccia" ButtonType="PushButton"
                    CommandName="StampaFreccia" ButtonCssClass="bottoneGriglia">
                    <ItemStyle Width="100px" />
                </telerik:GridButtonColumn>--%>
                <telerik:GridButtonColumn Text="MAV Popolare Sondrio" ButtonType="PushButton"
                    CommandName="StampaMav" ButtonCssClass="bottoneGriglia">
                    <ItemStyle Width="100px" />
                </telerik:GridButtonColumn>
                <telerik:GridButtonColumn Text="MAV Intesa Sanpaolo" ButtonType="PushButton"
                    CommandName="StampaMavProxima" ButtonCssClass="bottoneGriglia">
                    <ItemStyle Width="100px" />
                </telerik:GridButtonColumn>
            </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>
        </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
</telerik:RadGrid>

    <asp:GridView ID="GridViewBollettiniFreccia" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" OnPageIndexChanging="GridViewBollettiniFreccia_PageIndexChanging"
        PageSize="12" DataKeyNames="Anno,Mese,Sequenza,Importo,CIP" 
        OnRowCommand="GridViewBollettiniFreccia_RowCommand"
        Visible="false">
        <Columns>
            <asp:BoundField DataField="anno" HeaderText="Anno">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="mese" HeaderText="Mese">
                <ItemStyle Width="50px" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Sequenza" HeaderText="Denuncia">
                <ItemStyle Width="30px" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="importo" HeaderText="Importo" DataFormatString="{0:C}">
            <HeaderStyle HorizontalAlign="Right" />
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <%--<asp:BoundField DataField="CIP" HeaderText="Identificativo del bollettino" />--%>
           <%--<asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="Stampa Freccia" ButtonType="Button"
                CommandName="StampaFreccia">
                <ControlStyle CssClass="bottoneGriglia" Width="100px"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>--%>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="MAV Popolare Sondrio" ButtonType="Button"
                CommandName="StampaMav">
                <ControlStyle CssClass="bottoneGriglia" Width="100px"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="MAV Intesa Sanpaolo" ButtonType="Button"
                CommandName="StampaMavProxima" >
                <ControlStyle CssClass="bottoneGriglia" Width="100px"></ControlStyle>
                <ItemStyle Width="100px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>



    <br />
</asp:Content>
