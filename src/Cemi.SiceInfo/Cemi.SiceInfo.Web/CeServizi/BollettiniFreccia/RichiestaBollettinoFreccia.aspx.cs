﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Impresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;
using UtenteImpresa = TBridge.Cemi.Type.Entities.GestioneUtenti.Impresa;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia
{
    public partial class RichiestaBollettinoFreccia : Page
    {
        private readonly Common bizCommon = new Common();
        private MavManager mavManager;
        private NotificaBusiness notificaBiz;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.BollettinoFrecciaRichiesta);

            #endregion

            mavManager = new MavManager(Request.MapPath(Request.ApplicationPath));

            if (!Page.IsPostBack)
            {
                if (GestioneUtentiBiz.IsImpresa())
                {
                    PanelConsulente.Visible = false;
                    UtenteImpresa impresa =
                        (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    int idImpresa = impresa.IdImpresa;

                    CaricaBollettini(0, idImpresa);
                    CaricaDenunce(0, idImpresa);
                }

                if (GestioneUtentiBiz.IsConsulente())
                {
                    PanelConsulente.Visible = true;

                    notificaBiz = new NotificaBusiness();

                    Consulente consulente =
                        (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    int idConsulente = consulente.IdConsulente;

                    CaricaImpreseAssociateAlConsulente(idConsulente);
                }
            }
        }


        protected void ButtonSeleziona_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DropDownListImprese.SelectedValue))
            {
                LabelImpresaSelezionata.Text = DropDownListImprese.SelectedItem.Text;
                CaricaBollettini(0, Int32.Parse(DropDownListImprese.SelectedValue));
                CaricaDenunce(0, Int32.Parse(DropDownListImprese.SelectedValue));

                //DropDownListImprese.SelectedIndex = 0;
            }
            else
            {
                Presenter.CaricaElementiInGridView(
                    GridViewBollettiniFreccia,
                    null,
                    0);

                Presenter.CaricaElementiInGridView(
                    GridViewDenunce,
                    null,
                    0);

            }
        }




        private void CaricaBollettini(Int32 pagina, int idImpresa)
        {
            List<Bollettino> bollettini = bizCommon.GetDatiBollettiniFrecciaMav(idImpresa);

            Presenter.CaricaElementiInGridView(
                GridViewBollettiniFreccia,
                bollettini,
                pagina);

            Presenter.CaricaElementiInGridView(
                RadGridBollettiniFreccia,
                bollettini);
        }

        private void CaricaDenunce(Int32 pagina, int idImpresa)
        {
            List<Bollettino> denunce = bizCommon.GetDatiDenunce(idImpresa);

            Presenter.CaricaElementiInGridView(
                GridViewDenunce,
                denunce,
                pagina);

            Presenter.CaricaElementiInGridView(
                RadGridDenunce,
                denunce);
        }

        private void CaricaImpreseAssociateAlConsulente(int idConsulente)
        {
            DropDownListImprese.Items.Clear();

            DropDownListImprese.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListImprese.DataSource = notificaBiz.ImpreseConsulente(idConsulente);
            DropDownListImprese.DataTextField = "NomeComposto";
            DropDownListImprese.DataValueField = "IdImpresa";
            DropDownListImprese.DataBind();
        }


        #region RecuperoCrediti

        #region GridView

        protected void GridViewBollettiniFreccia_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (GestioneUtentiBiz.IsImpresa())
            {
                UtenteImpresa impresa =
                    (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idImpresa = impresa.IdImpresa;

                CaricaBollettini(e.NewPageIndex, idImpresa);
            }
            else
            {
                CaricaBollettini(e.NewPageIndex, Int32.Parse(DropDownListImprese.SelectedValue));
            }
        }

        protected void GridViewBollettiniFreccia_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "StampaFreccia")
            {
                BollettinoFreccia bollettino = new BollettinoFreccia();

                bollettino.Anno =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Anno"];
                bollettino.Mese =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Mese"];
                bollettino.Sequenza =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Sequenza"];
                bollettino.Cip =
                    GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["CIP"].ToString();
                bollettino.Importo =
                    (Decimal)
                    GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                bollettino.TipoCanalePagamento = TipiCanalePagamento.BollettinoFreccia;

                int idImpresa;
                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    idImpresa = impresa.IdImpresa;
                }
                catch
                {
                    idImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }

                bollettino.IdImpresa = idImpresa;

                Context.Items["idUtente"] = idUtente;
                Context.Items["bollettino"] = bollettino;

                bizCommon.RegistraRichiestaBollettino(bollettino, idUtente);

                Server.Transfer("~/CeServizi/BollettiniFreccia/ReportBollettinoFreccia.aspx");
            }
            else if (e.CommandName == "StampaMav")
            {
                BollettinoMav mav = new BollettinoMav();
                mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavPopolareSondrio;

                mav.Anno =
                   (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Anno"];
                mav.Mese =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Mese"];
                mav.Sequenza =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Sequenza"];
                mav.Importo =
                    (Decimal)
                    GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    mav.IdImpresa = impresa.IdImpresa;
                }
                catch
                {
                    mav.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }


                Byte[] pdfByteArray;
                String fileName = String.Format("BollettinoMav_{0}_{1}_{2}_{3}.pdf", mav.Anno, mav.Mese.ToString().PadLeft(2, '0'), mav.Sequenza.ToString().PadLeft(2, '0'), mav.IdImpresa.ToString().PadLeft(6, '0'));

                if (mavManager.GetMavPdf(mav, out pdfByteArray))
                {
                    bizCommon.RegistraRichiestaBollettino(mav, idUtente);

                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.AddHeader("Content-Disposition", String.Format("attachment; filename={0};", fileName));
                    response.AddHeader("Content-Length", pdfByteArray.Length.ToString());
                    // Set the HTTP MIME type of the output stream
                    response.ContentType = "application/pdf";

                    response.BinaryWrite(pdfByteArray);
                    response.Flush();
                    response.End();

                }

            }
            else if (e.CommandName == "StampaMavProxima")
            {
                BollettinoMav mav = new BollettinoMav();
                mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavBancaProxima;

                mav.Anno =
                   (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Anno"];
                mav.Mese =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Mese"];
                mav.Sequenza =
                    (Int32)GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Sequenza"];
                mav.Importo =
                    (Decimal)
                    GridViewBollettiniFreccia.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    mav.IdImpresa = impresa.IdImpresa;
                }
                catch
                {
                    mav.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }


                Byte[] pdfByteArray;
                String fileName = String.Format("BollettinoMav_{0}_{1}_{2}_{3}.pdf", mav.Anno, mav.Mese.ToString().PadLeft(2, '0'), mav.Sequenza.ToString().PadLeft(2, '0'), mav.IdImpresa.ToString().PadLeft(6, '0'));

                if (mavManager.GetMavPdfProxima(mav, out pdfByteArray))
                {
                    bizCommon.RegistraRichiestaBollettino(mav, idUtente);

                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.AddHeader("Content-Disposition", String.Format("attachment; filename={0};", fileName));
                    response.AddHeader("Content-Length", pdfByteArray.Length.ToString());
                    // Set the HTTP MIME type of the output stream
                    response.ContentType = "application/pdf";

                    response.BinaryWrite(pdfByteArray);
                    response.Flush();
                    response.End();

                }

            }
        }

        #endregion

        #region RadGrid

        protected void RadGridBollettiniFreccia_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            if (GestioneUtentiBiz.IsImpresa())
            {
                UtenteImpresa impresa =
                    (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idImpresa = impresa.IdImpresa;

                CaricaBollettini(e.NewPageIndex, idImpresa);
            }
            else
            {
                CaricaBollettini(e.NewPageIndex, Int32.Parse(DropDownListImprese.SelectedValue));
            }
        }

        protected void RadGridBollettiniFreccia_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "StampaFreccia")
            {
                BollettinoFreccia bollettino = new BollettinoFreccia();
                bollettino.Anno =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Anno"];
                bollettino.Mese =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Mese"];
                bollettino.Sequenza =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Sequenza"];
                bollettino.Cip =
                    RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["CIP"].ToString();
                bollettino.Importo =
                    (Decimal)
                    RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                bollettino.TipoCanalePagamento = TipiCanalePagamento.BollettinoFreccia;

                int idImpresa;
                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    idImpresa = impresa.IdImpresa;
                }
                catch
                {
                    idImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }

                bollettino.IdImpresa = idImpresa;

                Context.Items["idUtente"] = idUtente;
                Context.Items["bollettino"] = bollettino;

                bizCommon.RegistraRichiestaBollettino(bollettino, idUtente);

                Server.Transfer("~/CeServizi/BollettiniFreccia/ReportBollettinoFreccia.aspx");
            }
            else if (e.CommandName == "StampaMav")
            {
                BollettinoMav mav = new BollettinoMav();
                //mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavPopolareSondrio;
                mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavPopolareSondrio;

                mav.Anno =
                   (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Anno"];
                mav.Mese =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Mese"];
                mav.Sequenza =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Sequenza"];
                mav.Importo =
                    (Decimal)
                    RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    mav.IdImpresa = impresa.IdImpresa;
                }
                catch
                {
                    mav.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }


                Byte[] pdfByteArray;
                String fileName = String.Format("BollettinoMav_{0}_{1}_{2}_{3}.pdf", mav.Anno, mav.Mese.ToString().PadLeft(2, '0'), mav.Sequenza.ToString().PadLeft(2, '0'), mav.IdImpresa.ToString().PadLeft(6, '0'));

                if (mavManager.GetMavPdf(mav, out pdfByteArray))
                {
                    bizCommon.RegistraRichiestaBollettino(mav, idUtente);

                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.AddHeader("Content-Disposition", String.Format("attachment; filename={0};", fileName));
                    response.AddHeader("Content-Length", pdfByteArray.Length.ToString());
                    // Set the HTTP MIME type of the output stream
                    response.ContentType = "application/pdf";

                    response.BinaryWrite(pdfByteArray);
                    response.Flush();
                    response.End();

                }

            }
            else if (e.CommandName == "StampaMavProxima")
            {
                BollettinoMav mav = new BollettinoMav();
                mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavBancaProxima;

                mav.Anno =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Anno"];
                mav.Mese =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Mese"];
                mav.Sequenza =
                    (Int32)RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Sequenza"];
                mav.Importo =
                    (Decimal)
                    RadGridBollettiniFreccia.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    mav.IdImpresa = impresa.IdImpresa;
                }
                catch
                {
                    mav.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }


                Byte[] pdfByteArray;
                String fileName = String.Format("BollettinoMav_{0}_{1}_{2}_{3}.pdf", mav.Anno, mav.Mese.ToString().PadLeft(2, '0'), mav.Sequenza.ToString().PadLeft(2, '0'), mav.IdImpresa.ToString().PadLeft(6, '0'));

                if (mavManager.GetMavPdfProxima(mav, out pdfByteArray))
                {
                    bizCommon.RegistraRichiestaBollettino(mav, idUtente);

                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.AddHeader("Content-Disposition", String.Format("attachment; filename={0};", fileName));
                    response.AddHeader("Content-Length", pdfByteArray.Length.ToString());
                    // Set the HTTP MIME type of the output stream
                    response.ContentType = "application/pdf";

                    response.BinaryWrite(pdfByteArray);
                    response.Flush();
                    response.End();

                }

            }
        }

        protected void RadGridBollettiniFreccia_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Bollettino bollettino = (Bollettino)e.Item.DataItem;

                Label lSequenza = (Label)e.Item.FindControl("LabelSequenza");
                ImageButton ibDettaglio = (ImageButton)e.Item.FindControl("ImageButtonDettaglio");

                ibDettaglio.Attributes.Add(
                    "onClick",
                    String.Format(
                        "openRadWindowDettaglio({0}, {1}, {2}, {3}); return false;",
                        bollettino.IdImpresa,
                        bollettino.Anno,
                        bollettino.Mese,
                        bollettino.Sequenza));

                if (bollettino.Sequenza == 1)
                {
                    lSequenza.Text = "Ordinaria";
                }
                else
                {
                    lSequenza.Text = "Integrativa";
                }
            }
        }

        #endregion

        #endregion

        #region Denunce

        #region GridView

        protected void GridViewDenunce_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (GestioneUtentiBiz.IsImpresa())
            {
                UtenteImpresa impresa =
                    (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idImpresa = impresa.IdImpresa;

                CaricaDenunce(e.NewPageIndex, idImpresa);
            }
            else
            {
                CaricaDenunce(e.NewPageIndex, Int32.Parse(DropDownListImprese.SelectedValue));
            }
        }

        protected void GridViewDenunce_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "StampaFreccia")
            {
                BollettinoFreccia denuncia = new BollettinoFreccia();

                denuncia.Anno =
                    (Int32)GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Anno"];
                denuncia.Mese =
                    (Int32)GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Mese"];
                denuncia.Sequenza =
                    (Int32)GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Sequenza"];
                denuncia.Cip =
                    GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["CIP"].ToString();
                denuncia.Importo =
                    (Decimal)
                    GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                denuncia.TipoCanalePagamento = TipiCanalePagamento.BollettinoFreccia;

                int idImpresa;
                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    idImpresa = impresa.IdImpresa;
                }
                catch
                {
                    idImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }

                denuncia.IdImpresa = idImpresa;

                Context.Items["idUtente"] = idUtente;
                Context.Items["bollettino"] = denuncia;

                bizCommon.RegistraRichiestaBollettinoDenuncia(denuncia, idUtente);

                Server.Transfer("~/CeServizi/BollettiniFreccia/ReportBollettinoFreccia.aspx");
            }
            else if (e.CommandName == "StampaMav")
            {
                BollettinoMav mav = new BollettinoMav();
                mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavPopolareSondrio;

                mav.Anno =
                   (Int32)GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Anno"];
                mav.Mese =
                    (Int32)GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Mese"];
                mav.Sequenza =
                    (Int32)GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Sequenza"];
                mav.Importo =
                    (Decimal)
                    GridViewDenunce.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Values["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    mav.IdImpresa = impresa.IdImpresa;
                }
                catch
                {
                    mav.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }


                Byte[] pdfByteArray;
                String fileName = String.Format("BollettinoMav_{0}_{1}_{2}_{3}.pdf", mav.Anno, mav.Mese.ToString().PadLeft(2, '0'), mav.Sequenza.ToString().PadLeft(2, '0'), mav.IdImpresa.ToString().PadLeft(6, '0'));

                if (mavManager.GetMavPdf(mav, out pdfByteArray))
                {
                    bizCommon.RegistraRichiestaBollettinoDenuncia(mav, idUtente);

                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.AddHeader("Content-Disposition", String.Format("attachment; filename={0};", fileName));
                    response.AddHeader("Content-Length", pdfByteArray.Length.ToString());
                    // Set the HTTP MIME type of the output stream
                    response.ContentType = "application/pdf";

                    response.BinaryWrite(pdfByteArray);
                    response.Flush();
                    response.End();

                }

            }
        }

        #endregion

        #region RadGrid

        protected void RadGridDenunce_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            if (GestioneUtentiBiz.IsImpresa())
            {
                UtenteImpresa impresa =
                    (UtenteImpresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                int idImpresa = impresa.IdImpresa;

                CaricaDenunce(e.NewPageIndex, idImpresa);
            }
            else
            {
                CaricaDenunce(e.NewPageIndex, Int32.Parse(DropDownListImprese.SelectedValue));
            }
        }

        protected void RadGridDenunce_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "StampaFrecciaDenuncia")
            {
                BollettinoFreccia denunce = new BollettinoFreccia();
                denunce.Anno =
                    (Int32)RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Anno"];
                denunce.Mese =
                    (Int32)RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Mese"];
                denunce.Sequenza =
                    (Int32)RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Sequenza"];
                denunce.Cip =
                    RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["CIP"].ToString();
                denunce.Importo =
                    (Decimal)
                    RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                denunce.TipoCanalePagamento = TipiCanalePagamento.BollettinoFreccia;

                int idImpresa;
                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    idImpresa = impresa.IdImpresa;
                }
                catch
                {
                    idImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }

                denunce.IdImpresa = idImpresa;

                Context.Items["idUtente"] = idUtente;
                Context.Items["bollettino"] = denunce;

                bizCommon.RegistraRichiestaBollettinoDenuncia(denunce, idUtente);

                Server.Transfer("~/CeServizi/BollettiniFreccia/ReportBollettinoFrecciaDenuncia.aspx");
            }
            else if (e.CommandName == "StampaMavDenuncia")
            {
                BollettinoMav mav = new BollettinoMav();
                mav.TipoCanalePagamento = TBridge.Cemi.Type.Enums.TipiCanalePagamento.MavPopolareSondrio;

                mav.Anno =
                   (Int32)RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Anno"];
                mav.Mese =
                    (Int32)RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Mese"];
                mav.Sequenza =
                    (Int32)RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Sequenza"];
                mav.Importo =
                    (Decimal)
                    RadGridDenunce.MasterTableView.DataKeyValues[e.Item.ItemIndex]["Importo"];
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                try
                {
                    Impresa impresa =
                        (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    mav.IdImpresa = impresa.IdImpresa;
                }
                catch
                {
                    mav.IdImpresa = Int32.Parse(DropDownListImprese.SelectedValue);
                }


                Byte[] pdfByteArray;
                String fileName = String.Format("BollettinoMav_{0}_{1}_{2}_{3}.pdf", mav.Anno, mav.Mese.ToString().PadLeft(2, '0'), mav.Sequenza.ToString().PadLeft(2, '0'), mav.IdImpresa.ToString().PadLeft(6, '0'));

                if (mavManager.GetMavDenunciaPdf(mav, out pdfByteArray))
                {
                    bizCommon.RegistraRichiestaBollettinoDenuncia(mav, idUtente);

                    System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                    response.ClearContent();
                    response.Clear();
                    response.AddHeader("Content-Disposition", String.Format("attachment; filename={0};", fileName));
                    response.AddHeader("Content-Length", pdfByteArray.Length.ToString());
                    // Set the HTTP MIME type of the output stream
                    response.ContentType = "application/pdf";

                    response.BinaryWrite(pdfByteArray);
                    response.Flush();
                    response.End();

                }

            }
        }

        protected void RadGridDenunce_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Bollettino bollettino = (Bollettino)e.Item.DataItem;

                Label lSequenza = (Label)e.Item.FindControl("LabelDenunciaSequenza");

                /*
                ImageButton ibDettaglio = (ImageButton)e.Item.FindControl("ImageButtonDettaglio");

                ibDettaglio.Attributes.Add(
                    "onClick",
                    String.Format(
                        "openRadWindowDettaglio({0}, {1}, {2}, {3}); return false;",
                        bollettino.IdImpresa,
                        bollettino.Anno,
                        bollettino.Mese,
                        bollettino.Sequenza));
                */
                if (bollettino.Sequenza == 1)
                {
                    lSequenza.Text = "Ordinaria";
                }
                else
                {
                    lSequenza.Text = "Integrativa";
                }
            }
        }



        #endregion

        #endregion
    }
    }