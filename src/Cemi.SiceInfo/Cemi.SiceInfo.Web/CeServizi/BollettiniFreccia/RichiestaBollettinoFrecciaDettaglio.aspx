﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="RichiestaBollettinoFrecciaDettaglio.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.BollettiniFreccia.RichiestaBollettinoFrecciaDettaglio" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 150px;
        }
        .style2
        {
            text-align:right;
        }
        .style3
        {
            width: 70px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager
        ID="RadScriptManager1"
        runat="server">
    </telerik:RadScriptManager>

    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function Cancel() {
            GetRadWindow().close();
        }
    </script>
    <div>
        <table class="standardTable">
            <tr>
                <td class="style1">
                    Anno:
                </td>
                <td>
                    <b>
                    <asp:Label
                        ID="LabelAnno"
                        runat="server">
                    </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Mese:
                </td>
                <td>
                    <b>
                    <asp:Label
                        ID="LabelMese"
                        runat="server">
                    </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Denuncia:
                </td>
                <td>
                    <b>
                    <asp:Label
                        ID="LabelSequenza"
                        runat="server">
                    </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Totale denunciato:
                </td>
                <td class="style2">
                    <asp:Label
                        ID="LabelTotaleDenunciato"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Contributo aggiuntivo:
                </td>
                <td class="style2">
                    <asp:Label
                        ID="LabelContributoAggiuntivo"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Interessi:
                </td>
                <td class="style2">
                    <asp:Label
                        ID="LabelInteressi"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Totale dovuto:
                </td>
                <td class="style2">
                    <asp:Label
                        ID="LabelTotaleDovuto"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Totale versato:
                </td>
                <td class="style2">
                    <asp:Label
                        ID="LabelTotaleVersato"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td class="style1">
                    Dovuto a saldo:
                </td>
                <td class="style2">
                    <b>
                        <asp:Label
                            ID="LabelDovutoSaldo"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
        </table>
        <br />
        <asp:Button
            ID="ButtonPDF"
            runat="server"
            Text="PDF"
            Width="100px" onclick="ButtonPDF_Click" />
        &nbsp;&nbsp;
        <asp:Button
            ID="ButtonChiudi"
            runat="server"
            Text="Chiudi"
            Width="100px" 
            OnClientClick="Cancel()" />
    </div>
    </form>
</body>
</html>
