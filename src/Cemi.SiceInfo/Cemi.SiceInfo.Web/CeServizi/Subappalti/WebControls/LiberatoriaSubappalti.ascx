﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LiberatoriaSubappalti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Subappalti.WebControls.LiberatoriaSubappalti" %>
<table class="standardTable">
    <tr>
        <td colspan="2">
            CONSENSO ALL'ACCESSO DEL SERVIZIO SUBAPPALTI
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div align="justify">
                <br />
                Prima di avviare la ricerca, la sottoscritta impresa
                <asp:Label ID="LabelRagioneSocialeImpresa" runat="server" Font-Bold="True"></asp:Label>
                dichiara sotto la propria responsabilità che l’utilizzo del programma subappalti
                è legittimato dalla sussistenza di rapporti contrattuali con i nominativi delle
                imprese subappaltatrici ricercate (art. 24 D. Lgs. 196/2003) ovvero è stato espressamente
                autorizzato.
                <br />
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <center>
                <asp:RadioButtonList ID="RadioButtonListPrivacy" runat="server" RepeatDirection="Horizontal"
                    Width="300px">
                    <asp:ListItem>Dichiara</asp:ListItem>
                    <asp:ListItem Selected="True">Non Dichiara</asp:ListItem>
                </asp:RadioButtonList>
                &nbsp;</center>
        </td>
    </tr>
</table>