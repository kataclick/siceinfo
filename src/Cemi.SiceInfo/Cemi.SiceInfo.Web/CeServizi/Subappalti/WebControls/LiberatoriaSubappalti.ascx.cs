﻿using System;
using System.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.Subappalti.WebControls
{
    public partial class LiberatoriaSubappalti : System.Web.UI.UserControl
    {
        public string RagioneSocialeImpresa
        {
            set
            {
                //ragioneSocialeImpresa = value; 
                LabelRagioneSocialeImpresa.Text = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}