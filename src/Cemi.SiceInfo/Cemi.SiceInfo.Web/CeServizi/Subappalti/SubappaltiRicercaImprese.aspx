﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="SubappaltiRicercaImprese.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Subappalti.SubappaltiRicercaImprese" %>

<%@ Register Src="../WebControls/MenuSubappalti.ascx" TagName="SubappaltiMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Richiesta" ContentPlaceHolderID="MainPage" Runat="Server">
<uc2:TitoloSottotitolo ID="TitoloSottotitolo1"  titolo="Verifiche Subappalti" sottoTitolo="Ricerca Impresa" runat="server" />
   
    &nbsp;<table class="standardTable">
        <tr>
            <td colspan = "3">
    <asp:Label ID="lblIstruzioni" runat="server" Text="Selezionare i parametri per la ricerca impresa."></asp:Label></td>
        </tr>
        <tr>
            <td>
    <asp:Label ID="lblCodice" runat="server" Text="N. codice Cassa Edile :"></asp:Label></td>
            <td style="width: 207px">
    <asp:TextBox ID="txtCodice" runat="server" Width="151px"></asp:TextBox></td>
            <td>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCodice"
        ErrorMessage="Il codice deve essere un valore numerico" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
    <asp:Label ID="lblRagioneSociale" runat="server" Text="Ragione Sociale :"></asp:Label></td>
            <td style="width: 207px">
    <asp:TextBox ID="txtRagioneSociale" runat="server" Width="151px"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
    <asp:Button ID="btnRicerca" runat="server" OnClick="btnRicerca_Click" Text="Ricerca" Width="134px" /></td>
            <td style="width: 207px">
    <asp:Label ID="lblErrMessage" runat="server" ForeColor="Red" Text="Almeno uno dei due parametri deve essere valorizzato"
        Visible="False"></asp:Label></td>
            <td style="text-align: left">
    </td>
        </tr>
    </table>

</asp:Content>

<asp:Content ID="Lista" ContentPlaceHolderID="MainPage2" Runat="Server">
    <br />
    <asp:Label ID="LabelRicerca" runat="server" Font-Bold="True" Text="Risultato ricerca:"
        Visible="False"></asp:Label>&nbsp;<br />
    <br />
    <asp:GridView ID="gvListaImprese" runat="server" Width="100%" AllowPaging="True" AutoGenerateColumns="False"
        CaptionAlign="Left" OnSelectedIndexChanged="gvListaImprese_SelectedIndexChanged" OnPageIndexChanging="gvListaImprese_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="idImpresa" HeaderText="Codice" >
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="ragioneSociale" HeaderText="Ragione Sociale" />
            <asp:BoundField DataField="telefonosedeAmministrazione"
                HeaderText="Telefono" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="cellulare" HeaderText="Cellulare" >
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="faxSedeAmministrazione" HeaderText="Fax" />
            <asp:BoundField DataField="eMailSedeAmministrazione" HeaderText="E-mail" />
            <asp:BoundField DataField="sitoWeb" HeaderText="Sito web" />
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" CommandName="select" 
                Text="Vedi Scheda" HeaderText="Azione" ButtonType="Button">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:SubappaltiMenu ID="SubappaltiMenu1" runat="server" />
</asp:Content>

