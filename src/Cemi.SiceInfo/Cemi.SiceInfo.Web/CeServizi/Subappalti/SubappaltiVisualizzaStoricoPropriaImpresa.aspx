﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="SubappaltiVisualizzaStoricoPropriaImpresa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Subappalti.SubappaltiVisualizzaStoricoPropriaImpresa" %>

<%@ Register Src="../WebControls/MenuSubappalti.ascx" TagName="SubappaltiMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Parametri" ContentPlaceHolderID="MainPage" Runat="Server">
<uc2:TitoloSottotitolo ID="TitoloSottotitolo2"  titolo="Verifiche Subappalti" sottoTitolo="Chi mi ha cercato" runat="server" />

    <br />
    <table class="standardTable">
        <tr>
            <td>
    <asp:Label ID="lblImpresa" runat="server" Text="Codice Impresa ricercante :"></asp:Label></td>
            <td>
    <asp:TextBox ID="txtIdImpresa" runat="server"></asp:TextBox></td>
            <td>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtIdImpresa"
        ErrorMessage="Il codice deve essere un valore numerico" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
    <asp:Label ID="lblDataDa" runat="server" Text="Da data (gg/mm/aaaa):"></asp:Label></td>
            <td>
    <asp:TextBox ID="txtDataDa" runat="server"></asp:TextBox></td>
            <td>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDataDa"
        Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
    <asp:Label ID="lblDataA" runat="server" Text="A data (gg/mm/aaaa) esclusa:"></asp:Label></td>
            <td>
    <asp:TextBox ID="txtDataA" runat="server"></asp:TextBox></td>
            <td>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtDataA"
        Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
    <asp:Button ID="btnRicerca" runat="server" OnClick="btnRicerca_Click" Text="Ricerca" Width="154px" /></td>
            <td>
            </td>
            <td style="text-align: left">
    </td>
        </tr>
    </table>


</asp:Content>

<asp:Content ID="Risultati" ContentPlaceHolderID="MainPage2" Runat="Server">
    <br />
    <asp:Label ID="LabelRicerca" runat="server" Font-Bold="True" Text="Risultati ricerca:"
        Visible="False"></asp:Label><br />
    <br />
    <asp:GridView ID="gvStorico" runat="server" Width="97%" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="gvStorico_PageIndexChanging">
        
        <Columns>
            <asp:BoundField DataField="ragioneSocialeImpresaRicercante" HeaderText="Impresa ricercante" />
            <asp:BoundField DataField="ragioneSocialeImpresaRicercata" HeaderText="Impresa ricercata" />
            <asp:BoundField DataField="dataOra" HeaderText="Data e ora" />
            <asp:BoundField DataField="criteri" HeaderText="Lavoratori" />
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:SubappaltiMenu ID="SubappaltiMenu1" runat="server" />
</asp:Content>

