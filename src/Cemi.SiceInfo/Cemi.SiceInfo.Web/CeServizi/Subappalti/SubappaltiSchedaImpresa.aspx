﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="SubappaltiSchedaImpresa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Subappalti.SubappaltiSchedaImpresa" %>

<%@ Register Src="../WebControls/SchedaImpresa.ascx" TagName="SchedaImpresa" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/Messaggio.ascx" TagName="Messaggio" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuSubappalti.ascx" TagName="SubappaltiMenu" TagPrefix="uc4" %>

<asp:Content ID="Scheda" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Verifiche Subappalti" sottoTitolo="Scheda impresa" runat="server" />
    <uc1:SchedaImpresa ID="SchedaImpresa1" runat="server" UsaControlli="true"></uc1:SchedaImpresa>
    <br />
</asp:Content>
<asp:Content ID="Richiesta" ContentPlaceHolderID="MainPage2" runat="Server">

    <script type="text/javascript">
    function disabilitaNome()
    {
        var txtCodice="<%=txtCodice.ClientID%>";
        var txtNome="<%=txtNome.ClientID%>";
        var txtCognome="<%=txtCognome.ClientID%>";
        
        elemCod = document.getElementById(txtCodice);
        elemNome = document.getElementById(txtNome);
        elemCogn = document.getElementById(txtCognome);
        
        if (elemCod.value != "")
        {
            elemCod.disabled = false;
            elemNome.disabled = true;
            elemCogn.disabled = true;
        }
        else if (elemCod.value == "")
        {
            elemCod.disabled = false;
            elemNome.disabled = false;
            elemCogn.disabled = false;
        }
        
        return true;
    }
    
    function disabilitaCodice()
    {
        var txtCodice="<%=txtCodice.ClientID%>";
        var txtNome="<%=txtNome.ClientID%>";
        var txtCognome="<%=txtCognome.ClientID%>";
        
        elemCod = document.getElementById(txtCodice);
        elemNome = document.getElementById(txtNome);
        elemCogn = document.getElementById(txtCognome);
        
        if (elemNome.value != "" || elemCogn.value != "")
        {
            elemCod.disabled = true;
            elemNome.disabled = false;
            elemCogn.disabled = false;
        }
        else if (elemNome.value == "" && elemCogn.value == "")
        {
            elemCod.disabled = false;
            elemNome.disabled = false;
            elemCogn.disabled = false;
        }
    }
    </script>

    <uc3:messaggio ID="Messaggio1" runat="server" TestoMessaggio="Selezionare i parametri per la ricerca dipendenti"
        Titolo="Ricerca lavoratori" />
    <br />
    <table class="standardTable">
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:RadioButton ID="RadioButtonTesto" runat="server" Checked="True" Text="Nuova ricerca"
                    GroupName="GruppoCognome" />
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="lblCodice" runat="server" Text="N. posizione lavoratore:"></asp:Label>
                        </td>
                        <td>
                            <input id="txtCodice" runat="server" onkeydown="javascript:disabilitaNome();" onkeyup="javascript:disabilitaNome();"
                                onkeypress="javascript:disabilitaNome();" onpaste="javascript:disabilitaNome();" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Cognome :"></asp:Label>
                        </td>
                        <td>
                            <input id="txtCognome" runat="server" onkeydown="javascript:disabilitaCodice();"
                                onkeyup="javascript:disabilitaCodice();" onkeypress="javascript:disabilitaCodice();"
                                onpaste="javascript:disabilitaCodice();" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNome" runat="server" Text="Nome :"></asp:Label>
                        </td>
                        <td>
                            <input id="txtNome" runat="server" onkeydown="javascript:disabilitaCodice();" onkeyup="javascript:disabilitaCodice();"
                                onkeypress="javascript:disabilitaCodice();" onpaste="javascript:disabilitaCodice();" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCodice"
                    ErrorMessage="Il codice deve essere un valore numerico" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="RadioButtonLista" runat="server" Text="Ricerche precedenti"
                    GroupName="GruppoCognome" />
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:Label ID="lblRagioneSociale" runat="server" Text="Cognome :"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="DropDownListCognomi" runat="server" Width="154px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnRicerca" runat="server" OnClick="btnRicerca_Click" Text="Ricerca"
                    Width="128px" />
            </td>
            <td>
                <asp:Label ID="lblErrMessage" runat="server" ForeColor="Red" Text="Almeno un parametro tra codice o cognome deve essere valorizzato"
                    Visible="False"></asp:Label>
                <asp:Label ID="lblErrMessagePrec" runat="server" ForeColor="Red" Text="Selezionare un cognome dalla lista"
                    Visible="False"></asp:Label>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelRicerca" runat="server" Font-Bold="True" Text="Risultato ricerca per:"
        Visible="False"></asp:Label>
    <asp:Label ID="LabelCognomeRicercato" runat="server" Width="157px"></asp:Label><br />
    <asp:Label ID="LabelUltimoMeseDenunciato" runat="server"></asp:Label><br />
    <asp:GridView ID="gvLavoratori" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" OnPageIndexChanging="gvLavoratori_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="idLavoratore" HeaderText="Posizione" />
            <asp:BoundField DataField="cognome" HeaderText="Cognome" />
            <asp:BoundField DataField="nome" HeaderText="Nome" />
            <asp:BoundField DataField="codiceFiscale" HeaderText="Codice fiscale" />
            <asp:BoundField DataField="dataDenuncia" HeaderText="Periodo denuncia" DataFormatString="{0:MM/yyyy}"
                HtmlEncode="False" />
            <asp:BoundField DataField="dataCessazione" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Notifica cessazione"
                HtmlEncode="False" />
        </Columns>
        <EmptyDataTemplate>
            Nominativo non denunciato
        </EmptyDataTemplate>
        <EmptyDataRowStyle ForeColor="Red" Font-Bold="True" />
    </asp:GridView>
    <br />
    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Notifiche"></asp:Label>
    <asp:GridView ID="GridViewNotifiche" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" OnPageIndexChanging="gvLavoratori_PageIndexChanging" PageSize="5">
        <Columns>
            <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" />
            <asp:BoundField DataField="DataAssunzione" HeaderText="Data assunzione" DataFormatString="{0:dd/MM/yyyy}"
                HtmlEncode="False" />
        </Columns>
        <EmptyDataTemplate>
            Nominativo non notificato&nbsp;
        </EmptyDataTemplate>
        <EmptyDataRowStyle ForeColor="Red" Font-Bold="True" />
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc4:SubappaltiMenu ID="SubappaltiMenu1" runat="server" />
</asp:Content>

