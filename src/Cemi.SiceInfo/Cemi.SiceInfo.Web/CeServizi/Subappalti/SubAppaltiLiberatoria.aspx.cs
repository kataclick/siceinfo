﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Subappalti
{
    public partial class SubAppaltiLiberatoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Controllo l'autorizzazione dell'utente a vedere la pagina ...
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.SubappaltiRicerca);

            //otteniamo il nome dell'impresa
            //GestioneUtentiBiz gu = new GestioneUtentiBiz();

            //TBridge.Cemi.GestioneUtentiBiz.Data.Entities.Impresa 
            //    impresa = gu.GetUtenteImpresa(TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());

            //Alla funzione subappalti possono anche accedere altre entità che non sono imprese. La liberatoria è pensata per le imprese, quindi chi non 
            //fa parte di questa entità non ha senso che veda il disclaimer
            if (GestioneUtentiBiz.IsImpresa())
            {
                //LiberatoriaSubappalti1.RagioneSocialeImpresa = ((Impresa)
                //                                                (HttpContext.Current.User).Identity).Entity.RagioneSociale;
                LiberatoriaSubappalti1.RagioneSocialeImpresa = ((Impresa)
                                                                GestioneUtentiBiz.GetIdentitaUtenteCorrente()).
                    RagioneSociale;
            }
            else
            {
                Response.Redirect("SubappaltiSchedaImpresa.aspx");
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Response.Redirect("SubappaltiRicercaImprese.aspx");
        }

        protected void ButtonProsegui_Click(object sender, EventArgs e)
        {
            try
            {
                //Activity tracking: memorizziamo la scelta dell'utente
                //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
                //logItemCollection.Add("IdUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetIdUtente().ToString());
                //logItemCollection.Add("LoginUtente", TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.GetNomeUtente());
                //logItemCollection.Add("LiberatoriaSubappalti", ((RadioButtonList)LiberatoriaSubappalti1.FindControl("RadioButtonListPrivacy")).SelectedValue);
                //TBridge.Cemi.ActivityTracking.Log.Write("Visualizzazione delle anomalie", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SUBAPPALTI, TBridge.Cemi.ActivityTracking.Log.sezione.LOGGING);

                if (((RadioButtonList)LiberatoriaSubappalti1.FindControl("RadioButtonListPrivacy")).SelectedValue ==
                    "Dichiara")
                {
                    Response.Redirect("SubappaltiSchedaImpresa.aspx");
                }
                else
                {
                    //FeedBack
                    LabelResult.Text =
                        "Non è possibile proseguire con la ricerca se non si sottoscrive la dichiarazione soprastante";
                }
            }
            catch (Exception ex)
            {
                //TBridge.Cemi.ActivityTracking.LogItemCollection logItemCollection = new TBridge.Cemi.ActivityTracking.LogItemCollection();
                //logItemCollection.Add("Eccezione", ex.Message);
                //TBridge.Cemi.ActivityTracking.Log.Write("Eccezione in: SubAppaltiLiberatoria.aspx", logItemCollection, TBridge.Cemi.ActivityTracking.Log.categorie.SUBAPPALTI, TBridge.Cemi.ActivityTracking.Log.sezione.SYSTEMEXCEPTION);
            }
        }
    }
}