﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="SubAppaltiDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Subappalti.SubAppaltiDefault" %>

<%@ Register Src="../WebControls/MenuSubappalti.ascx" TagName="SubappaltiMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:SubappaltiMenu ID="SubappaltiMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" titolo="Verifiche Subappalti" sottoTitolo="Benvenuto nella sezione Verifiche Subappalti"
        runat="server" />
    <br />
    <div class="DefaultPage">
        La funzione <strong>Verifiche Subappalti</strong> permette all’utente registrato
        di verificare l’iscrizione di un’impresa subappaltatrice alla Cassa Edile di Milano,
        Lodi, Monza e Brianza. La funzione dà, inoltre, evidenza del nome e del cognome
        del lavoratore dipendente dell’impresa subappaltatrice denunciato in Cassa Edile.<br />
        Condizione indispensabile per poter accedere al servizio è l’attestazione da parte
        del soggetto che effettua la ricerca della sussistenza di rapporti contrattuali
        con l’azienda o le aziende ricercate, che a loro volta potranno sapere quale impresa
        ha eseguito l’interrogazione sul proprio conto, in quanto la funzione traccia e
        memorizza tutte le informazioni ricercate.<br />
        La ricerca dell’impresa potrà avvenire per ragione sociale, anche parziale, o per
        codice di iscrizione alla Cassa Edile e riporterà i dati aggiornati fino all’ultimo
        periodo denunciato.
        <br />
        <br />
        Per conoscere le imprese in regola con i versamenti contributivi in Cassa Edile
        di Milano, Lodi, Monza e Brianza (non regolarità ai fini DURC) occorre consultare
        l’elenco delle <strong>Imprese Adempienti</strong>, funzione ad accesso pubblico.
        <br />
        <br />
        Per verificare la regolarità contributiva delle imprese non iscritte in Cassa Edile
        di Milano, Lodi, Monza e Brianza, si raccomanda l’acquisizione del DURC. Per conoscere
        i certificati di regolarità contributiva positivi emessi da Cassa Edile di Milano,
        Lodi, Monza e Brianza basta consultare la funzione ad accesso pubblico <strong>Elenco DURC
        Regolari</strong>.
        <br />
        <br />
        Per avere informazioni sulla regolarità della forza lavoro che accede al cantiere
        è possibile richiedere il servizio di monitoraggio telematico degli accessi al cantiere
        inviando una mail all’indirizzo <a href="mailto:accessoincantiere@cassaedilemilano.it">
            accessoincantiere@cassaedilemilano.it</a>
    </div>
</asp:Content>
