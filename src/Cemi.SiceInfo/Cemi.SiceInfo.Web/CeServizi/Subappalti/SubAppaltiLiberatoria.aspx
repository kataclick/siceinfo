﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="SubAppaltiLiberatoria.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Subappalti.SubAppaltiLiberatoria" %>

<%@ Register Src="../WebControls/MenuSubappalti.ascx" TagName="SubappaltiMenu" TagPrefix="uc4" %>

<%@ Register Src="../WebControls/MenuImpreseRegolari.ascx" TagName="ImpreseRegolariMenu"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="WebControls/LiberatoriaSubappalti.ascx" TagName="LiberatoriaSubappalti"
    TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc4:SubappaltiMenu ID="SubappaltiMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Verifiche Subappalti" sottoTitolo="Consenso per l'accesso al servizio" />
    <br />
    <uc2:LiberatoriaSubappalti ID="LiberatoriaSubappalti1" runat="server" />
    <div align=center>
    <asp:Button ID="ButtonProsegui" runat="server" OnClick="ButtonProsegui_Click" Text="Prosegui"
        Width="108px" />
    <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla"
        Width="108px" /><br />
    </div>
    <asp:Label ID="LabelResult" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>
