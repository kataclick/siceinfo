﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="VisualizzazioneMappa.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.VisualizzazioneMappa" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title></title>
    <script src="../Scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script src="https://www.bing.com/api/maps/mapcontrol?callback=LoadMap" async defer></script>
            <script type="text/javascript">
                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.464044;
                var longitudine = 9.191567;
                var zoom = 10;

                function LoadMap() {
                    map = new Microsoft.Maps.Map('#myMap', {
                        credentials: 'AkAg3s1r8wAGdbJns9DCog3AcDYwwWmFgQCE0P-O6-kIxD_NjIGWmx47miyUlQLF',
                        center: new Microsoft.Maps.Location(latitudine, longitudine),
                        mapTypeId: Microsoft.Maps.MapTypeId.road,
                        zoom: zoom
                    });
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pin;
                    var position = new Microsoft.Maps.Location(lat, lon);
                    if (img)
                        pin = new Microsoft.Maps.Pushpin(position,
                            {
                                icon: img,
                                //anchor: new Microsoft.Maps.Point(12, 39)
                            });
                    else
                        pin = new Microsoft.Maps.Pushpin(position,
                            {
                                //icon: img
                                //anchor: new Microsoft.Maps.Point(12, 39)
                            });
                    
                    var infobox = new Microsoft.Maps.Infobox(position, {
                        title: 'Dettaglio',
                        description: descrizione, visible: false
                    });
                    infobox.setMap(map);
                    Microsoft.Maps.Events.addHandler(pin, 'click', function () {
                        infobox.setOptions({ visible: true });
                    });
                    
                    //Add the pushpin to the map
                    map.entities.push(pin);
                }

                function LoadMapCenter(latitudine, longitudine) {
                    map = new Microsoft.Maps.Map('#myMap', {
                        credentials: 'AkAg3s1r8wAGdbJns9DCog3AcDYwwWmFgQCE0P-O6-kIxD_NjIGWmx47miyUlQLF',
                        center: new Microsoft.Maps.Location(latitudine, longitudine),
                        mapTypeId: Microsoft.Maps.MapTypeId.road,
                        zoom: zoom
                    });
                }
            </script>
        </telerik:RadCodeBlock>
        
        <div>
            <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
            </div>
            <br />
        </div>
        <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
            <script type="text/javascript">
                //$(function() {
                    var latitudine = 45.464044;
                    var longitudine = 9.191567;
                    LoadMap();
                //});
            </script>
        </telerik:RadCodeBlock>
    </telerik:RadAjaxPanel>
    </form>
</body>
</html>

