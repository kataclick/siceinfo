﻿<%@ Page Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="Impersonate.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Impersonate" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="MainPage">
    <asp:Panel
        ID="PanelImpersona"
        runat="server"
        DefaultButton="ButtonImpersonifica">
        <telerik:RadTextBox ID="RadTextBoxUsername" runat="server" Width="220px" />
        <asp:Button ID="ButtonImpersonifica" runat="server" Text="Impersona" 
                    onclick="ButtonImpersonifica_Click" />
    </asp:Panel>
</asp:Content>
