﻿using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi
{
    public partial class ReportIISLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IISLogStatistiche);
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IISLogStatistiche);
        }

        protected void ReportViewerIISLog_Init(object sender, EventArgs e)
        {
            ReportViewerIISLog.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);

            string tipoReport = Request.QueryString["tipo"];
            if (tipoReport == "browser")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Browser Statistics";
                TitoloSottotitolo1.sottoTitolo = "Statistiche browser";
            }
            else if (tipoReport == "country")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Country Statistics";
                TitoloSottotitolo1.sottoTitolo = "Statistiche nazione/regione";
            }
            else if (tipoReport == "giornoMese")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Day of Month";
                TitoloSottotitolo1.sottoTitolo = "Statistiche giorno del mese";
            }
            else if (tipoReport == "giornoSettimana")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Day of Week";
                TitoloSottotitolo1.sottoTitolo = "Statistiche giorno della settimana";
            }
            else if (tipoReport == "globali")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Global Statistics";
                TitoloSottotitolo1.sottoTitolo = "Statistiche globali";
            }
            else if (tipoReport == "ore")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Hourly Statistics";
                TitoloSottotitolo1.sottoTitolo = "Statistiche orarie";
            }
            else if (tipoReport == "durata")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Lenght Statistics";
                TitoloSottotitolo1.sottoTitolo = "Statistiche durata";
            }
            else if (tipoReport == "OS")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/OS Statistics";
                TitoloSottotitolo1.sottoTitolo = "Statistiche sistemi operativi";
            }
            else if (tipoReport == "pagine")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Pages";
                TitoloSottotitolo1.sottoTitolo = "Statistiche pagine";
            }
            else if (tipoReport == "riepilogo")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Site Summary";
                TitoloSottotitolo1.sottoTitolo = "Statistiche di riepilogo";
            }
            else if (tipoReport == "temporali")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Time Period";
                TitoloSottotitolo1.sottoTitolo = "Statistiche temporali";
            }
            else if (tipoReport == "visitatori")
            {
                ReportViewerIISLog.ServerReport.ReportPath = "/ReportIISLog/Visitors";
                TitoloSottotitolo1.sottoTitolo = "Statistiche visitatori";
            }

            //int idLavoratore = ((TBridge.Cemi.GestioneUtentiBiz.Business.Identities.Lavoratore)(HttpContext.Current.User).Identity).Entity.IdLavoratore;

            //ReportParameter param = new ReportParameter("idLavoratore", idLavoratore.ToString());
            //ReportParameter[] listaParam = new ReportParameter[1];
            //listaParam[0] = param;

            //ReportViewerLavoratori.ServerReport.SetParameters(listaParam);
        }
    }
}