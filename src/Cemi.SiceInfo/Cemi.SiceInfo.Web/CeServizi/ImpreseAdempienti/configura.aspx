﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="configura.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.ImpreseAdempienti.configura" %>

<%@ Register Src="../WebControls/MenuImpreseRegolari.ascx" TagName="ImpreseRegolariMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">    
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1"  titolo="Imprese adempienti" sottoTitolo="Configurazione parametri di generazione della lista" runat="server" />
    
    <br />
        <asp:Label ID="Label1" runat="server" Text="Minimo"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;<asp:RangeValidator
            ID="RangeValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Il valore deve essere di tipo numerico"
            MaximumValue="Double.MAX" MinimumValue="0"></asp:RangeValidator><br />
        <asp:Label ID="Label2" runat="server" Text="Totale"></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBox2"
            ErrorMessage="Il valore deve essere di tipo numerico" MaximumValue="Double.MAX"
            MinimumValue="0"></asp:RangeValidator><br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Modifica" />
    </asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="MainPage2" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:ImpreseRegolariMenu ID="ImpreseRegolariMenu1" runat="server" />
</asp:Content>

