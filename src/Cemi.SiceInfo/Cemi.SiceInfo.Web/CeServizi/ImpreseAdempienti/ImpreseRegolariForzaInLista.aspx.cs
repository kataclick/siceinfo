﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Business.Imp;
using Cemi.SiceInfo.Type.Dto.ImpreseAdem;
using Cemi.SiceInfo.Type.Dto.ImpreseAdem.Filters;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Impresa = TBridge.Cemi.Type.Entities.Cantieri.Impresa;

namespace Cemi.SiceInfo.Web.CeServizi.ImpreseAdempienti
{
    public partial class ImpreseRegolariForzaInLista : Page
    {
        private readonly ImpreseAdemBusiness _biz = new ImpreseAdemBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRegolariModificaLista);

            CantieriRicercaImpresa1.ModalitaImpreseRegolari();
            CantieriRicercaImpresa1.OnImpresaSelected += CantieriRicercaImpresa1_OnImpresaSelected;

            CaricaEccezioni();
        }

        private void CaricaEccezioni()
        {
            EccezioneListaFilter filtro = new EccezioneListaFilter();

            if (!string.IsNullOrEmpty(TextBoxIdImpresa.Text.Trim()))
                filtro.IdImpresa = int.Parse(TextBoxIdImpresa.Text);
            if (!string.IsNullOrEmpty(TextBoxRagioneSociale.Text))
                filtro.RagioneSociale = TextBoxRagioneSociale.Text;

            List<EccezioneLista> eccezioni = _biz.GetEccezioniListaImpreseAdempienti(filtro);

            GridViewEccezioni.DataSource = eccezioni;
            GridViewEccezioni.DataBind();
        }

        private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
        {
            LabelGiaPresente.Visible = false;

            EccezioneLista eccezione = new EccezioneLista
            {
                Impresa =
                {
                    IdImpresa = impresa.IdImpresa.Value,
                    RagioneSociale = impresa.RagioneSociale
                },
                Stato = RadioButtonForzata.Checked
            };

            if (_biz.InsertEccezioneListaImpreseAdempienti(eccezione, out var giaPresente))
            {
                CaricaEccezioni();
                LabelErrore.Visible = false;
            }
            else
            {
                if (!giaPresente)
                    LabelErrore.Visible = true;
                else
                    LabelGiaPresente.Visible = true;
            }
        }

        protected void ButtonCerca_Click(object sender, EventArgs e)
        {
            GridViewEccezioni.PageIndex = 0;
            CaricaEccezioni();
        }

        protected void GridViewEccezioni_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int idImpresa = (int) GridViewEccezioni.DataKeys[e.RowIndex].Value;

            if (_biz.DeleteEccezioneListaImpreseAdempienti(idImpresa))
            {
                CaricaEccezioni();
                LabelErrore.Visible = false;
            }
            else
            {
                LabelErrore.Visible = true;
            }
        }

        protected void GridViewEccezioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewEccezioni.PageIndex = e.NewPageIndex;
            CaricaEccezioni();
        }
    }
}