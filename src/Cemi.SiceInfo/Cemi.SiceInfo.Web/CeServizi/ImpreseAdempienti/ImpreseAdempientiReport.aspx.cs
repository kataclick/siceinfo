﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Cemi.SiceInfo.Business.Imp;
using Cemi.SiceInfo.Web.Helpers;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.ImpreseAdempienti
{
    public partial class ImpreseAdempientiReport : System.Web.UI.Page
    {
        private readonly ImpreseAdemBusiness biz = new ImpreseAdemBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRegolariRicercaLista);

            if (!Page.IsPostBack)
            {
                CaricaComboBox();
                Common commonFunction = new Common();
                TitoloSottotitolo1.sottoTitolo = "Dati aggiornati al " + commonFunction.GetDataImpreseRegolari();
            }
        }

        private void CaricaComboBox()
        {
            CaricaComuniSedeLegale();
            CaricaComuniSedeAmministrativa();
            CaricaAttivitaIstat();
        }

        private void CaricaAttivitaIstat()
        {
            if (RadComboBoxAttivitaIstat.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDown(
                    RadComboBoxAttivitaIstat,
                    biz.GetAttivitaIstatImpreseAdempienti(),
                    "Descrizione",
                    "CodiceAttivita");
                RadComboBoxAttivitaIstat.Items.Insert(0, new RadComboBoxItem("- Selezionare l'attività ISTAT -"));
            }
        }

        private void CaricaComuniSedeAmministrativa()
        {
            if (RadComboBoxComuneSedeAmministrativa.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDown(
                    RadComboBoxComuneSedeAmministrativa,
                    biz.GetComuniSedeAmministrativaImpreseRegolari(),
                    "Nome",
                    "CodiceCatastale");
                RadComboBoxComuneSedeAmministrativa.Items.Insert(0, new RadComboBoxItem("- Selezionare un comune -"));
            }
        }

        private void CaricaComuniSedeLegale()
        {
            if (RadComboBoxComuneSedeLegale.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDown(
                    RadComboBoxComuneSedeLegale,
                    biz.GetComuniSedeLegaleImpreseRegolari(),
                    "Nome",
                    "CodiceCatastale");
                RadComboBoxComuneSedeLegale.Items.Insert(0, new RadComboBoxItem("- Selezionare un comune -"));
            }
        }

        protected void ReportViewer1_Init(object sender, EventArgs e)
        {
            ReportViewerImpreseAdem.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerImpreseAdem.ServerReport.ReportPath = "/ImpreseRegolari/ImpreseRegolariLista";
        }

        protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
        {
            const string nothing = null;

            ReportViewerImpreseAdem.Reset();
            ReportViewerImpreseAdem.ProcessingMode = ProcessingMode.Remote;
            ReportViewerImpreseAdem.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerImpreseAdem.ServerReport.ReportPath = "/ImpreseRegolari/ImpreseRegolariLista";
            ReportViewerImpreseAdem.ServerReport.Refresh();

            List<ReportParameter> listaParam = new List<ReportParameter>();

            ReportParameter param1 = new ReportParameter("ragioneSociale");

            ReportParameter param2 = new ReportParameter("idAttivitaISTAT");

            ReportParameter param3 = new ReportParameter("codiceCatastaleSedeLegale");

            ReportParameter param4 = new ReportParameter("codiceCatastaleSedeAmministrativa");

            ReportParameter param5 = new ReportParameter("partitaIVA");

            if (!String.IsNullOrEmpty(RadTextBoxRagioneSociale.Text))
            {
                param1.Values.Add(RadTextBoxRagioneSociale.Text);
            }
            else
            {
                param1.Values.Add(nothing);
            }
            if (!String.IsNullOrEmpty(RadComboBoxAttivitaIstat.SelectedValue))
            {
                param2.Values.Add(RadComboBoxAttivitaIstat.SelectedValue);
            }
            else
            {
                param2.Values.Add(nothing);
            }
            if (!String.IsNullOrEmpty(RadComboBoxComuneSedeLegale.SelectedValue))
            {
                param3.Values.Add(RadComboBoxComuneSedeLegale.SelectedValue);
            }
            else
            {
                param3.Values.Add(nothing);
            }
            if (!String.IsNullOrEmpty(RadComboBoxComuneSedeAmministrativa.SelectedValue))
            {
                param4.Values.Add(RadComboBoxComuneSedeAmministrativa.SelectedValue);
            }
            else
            {
                param4.Values.Add(nothing);
            }
            if (!String.IsNullOrWhiteSpace(RadTextBoxPartitaIVA.Text))
            {
                param5.Values.Add(RadTextBoxPartitaIVA.Text);
            }
            else
            {
                param5.Values.Add(nothing);
            }
            listaParam.Add(param1);
            listaParam.Add(param2);
            listaParam.Add(param3);
            listaParam.Add(param4);
            listaParam.Add(param5);

            ReportViewerImpreseAdem.ServerReport.SetParameters(listaParam.ToArray());
        }
    }
}