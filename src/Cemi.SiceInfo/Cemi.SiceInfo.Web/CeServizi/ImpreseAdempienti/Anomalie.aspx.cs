﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.ImpreseAdempienti
{
    public partial class Anomalie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.ImpreseRegolariGestisciAnomalie);
        }

        protected void GridViewAnomalie_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewAnomalie.DataSource = LoadAnomalie();
            GridViewAnomalie.PageIndex = e.NewPageIndex;
            GridViewAnomalie.DataBind();
        }

        protected void GridViewAnomalie_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet source = LoadAnomalie();
            DataView dv = new DataView(source.Tables[0]);
            string direction;
            if (e.SortDirection == SortDirection.Ascending)
                direction = "ASC";
            else
                direction = "DESC";
            dv.Sort = String.Format("{0} {1}", e.SortExpression, direction);
            GridViewAnomalie.DataSource = dv;
            GridViewAnomalie.DataBind();
        }

        protected void ButtonFiltra_Click(object sender, EventArgs e)
        {
            GridViewAnomalie.DataSource = LoadAnomalie();
            GridViewAnomalie.DataBind();
        }

        private DataSet LoadAnomalie()
        {
            DataSet ds = null;

            DateTime dataDa;
            DateTime dataA;

            if (DateTime.TryParse(TextBoxDa.Text, out dataDa) && DateTime.TryParse(TextBoxA.Text, out dataA))
            {
                AnomalieManager anomalieManager = new AnomalieManager();
                ds = anomalieManager.CaricaAnomalie(dataDa, dataA);
            }

            return ds;
        }
    }
}