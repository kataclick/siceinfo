﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.ImpreseAdempienti
{
    public partial class ImpreseRegolariDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.ImpreseRegolariApprovaLista);
            funzionalita.Add(FunzionalitaPredefinite.ImpreseRegolariGestisciAnomalie);
            funzionalita.Add(FunzionalitaPredefinite.ImpreseRegolariImpostaParametri);
            funzionalita.Add(FunzionalitaPredefinite.ImpreseRegolariModificaLista);
            funzionalita.Add(FunzionalitaPredefinite.ImpreseRegolariRicercaLista);
            funzionalita.Add(FunzionalitaPredefinite.ImpreseRegolariSegnalaAnomalie);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
        }
    }
}