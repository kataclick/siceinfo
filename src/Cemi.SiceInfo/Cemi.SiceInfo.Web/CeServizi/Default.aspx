﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Default" MasterPageFile="~/CeServizi/MasterPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainPage" runat="Server">
    <asp:Panel ID="PanelPubblico" runat="server" CssClass="DefaultPage" Visible="true">
        <p>
            Benvenuti nell’area <strong>Servizi on-line</strong> del nostro sito internet che
            è organizzata nelle tre sezioni sopra esposte:<strong> Vetrina regolarità</strong>,
            <strong>Sportello web</strong> e <strong>Iscrizione impresa</strong>.
        </p>
        <p>
            Ognuna di queste sezioni contiene delle <strong>funzioni pubbliche</strong> alle
            quali l’utente può accedere senza effettuare la registrazione in quest’area del
            sito e delle <strong>funzioni private</strong> che, invece, possono essere consultate/utilizzate
            da parte dell’utente solo a registrazione eseguita. Per ulteriori dettagli sulla
            procedura operativa da seguire per portare a termine con successo la registrazione
            si rimanda alle informazioni pubblicate nella pagina web <strong>"Servizi on-line"</strong>
            – <strong>"Area Privata"</strong> del sito istituzionale.
        </p>
        <p><span style="color: #CD071E;"><strong>FUNZIONI PUBBLICHE</strong></span></p>
        <span style="color: #CD071E;"><strong>Vetrina regolarità</strong></span>

            <ul>
                <li><strong>Imprese adempienti</strong> per consultare l’elenco delle imprese iscritte
                    che hanno ottemperato ai propri obblighi contributivi e sono, quindi, in regola
                    con gli adempimenti ai soli fini Cassa Edile di Milano, Lodi, Monza e Brianza</li>
            </ul>

        <span style="color: #CD071E;"><strong>Sportello web</strong></span>

            <ul>
                <li><strong>Richiesta prestazioni</strong> per inoltrare una domanda di rimborso per
                    se stessi e, laddove previsto, per i familiari fiscalmente a carico (moglie e figli 
					fiscalmente a carico di almeno uno dei due coniugi) in modo
                    rapido e sicuro via web. L’utilizzo della funzione nell’area privata consente di
                    salvare i dati inseriti in qualsiasi momento della compilazione della richiesta</li>
				
            </ul>

        <span style="color: #CD071E;"><strong>Iscrizione impresa</strong></span>

            <ul>
                <li><strong>Iscrizione</strong> per effettuare la domanda di iscrizione presso il nostro
                    Ente</li>
            </ul>

    </asp:Panel>
    <asp:Panel ID="PanelImpresa" runat="server" CssClass="DefaultPage" Visible="false">
        <p>
            Benvenuto
            <%=Membership.GetUser().UserName%>!
        </p>
        <p>
            In quest’area potrai accedere alle funzioni private attribuite al tuo profilo sotto
            riportate:
        </p>
        <span style="color: #CD071E;"><strong>Vetrina regolarità</strong></span>
        <p>
            La sezione, oltre alla funzione pubblica <strong>Imprese Adempienti</strong> contiene le seguenti funzioni private:
            <ul>
                <li><strong>Verifiche subappalti</strong> per verificare l'iscrizione di un'impresa
                    subappaltatrice e dei suoi dipendenti presso il nostro Ente</li>
                <li><strong>Accesso cantiere</strong> per il monitoraggio telematico della presenza
                    e della regolarità dei rapporti di lavoro della manodopera presente in cantiere</li>
            </ul>
        </p>
        <br /><span style="color: #CD071E;"><strong>Pagamenti</strong></span>
        <p>
            La sezione riguarda:
            <ul>
                <li><strong>Pagamenti scaduti</strong> per stampare, solo in caso di ritardo nei versamenti,
                    il bollettino di pagamento sempre aggiornato</li>
            </ul>
            <em>La sezione è fruibile da parte di imprese e consulenti del lavoro</em>
        </p>
        <br /><span style="color: #CD071E;"><strong>Sportello web</strong></span>
        <p>
            Oltre alla funzione pubblica <strong>Prestazioni</strong>, la sezione ospita le
            seguenti <strong>funzioni private</strong>:
            <ul>
                <li><strong>Tute e Scarpe</strong> per effettuare l’ordine della fornitura di indumenti
                    e calzature da lavoro annualmente garantita ai lavoratori iscritti</li>
                 <li>
                    <strong>Notifiche lavoratori</strong> per l’acquisizione in automatico dei dati inseriti 
                    nelle Comunicazioni Obbligatorie di instaurazione di rapporto di lavoro 
                    (tramite il portale SINTESI delle Province) ed il loro trasferimento nella denuncia telematica mensile di manodopera occupata (MUT)</li>
                <li><strong>Iscrizione Corsi</strong> per effettuare l’iscrizione dei lavoratori ai
                    moduli formativi proposti dal sistema bilaterale territoriale</li>
                <li><strong>Variazione recapiti impresa</strong> per comunicare in modo rapido ed efficace
                    l’aggiornamento dei dati relativi ai recapiti della sede legale, amministrativa
                    e di corrispondenza di un’impresa iscritta</li>
                <li><strong>Variazione stato impresa</strong> per richiedere la modifica dello stato
                    riferito alla posizione aziendale presso il nostro Ente (riattivazione, sospensione,
                    cessazione)</li>
                <li><strong>Richiesta prestazioni</strong> per inoltrare una domanda di rimborso per
                    se stessi e, laddove previsto, per i familiari fiscalmente a carico (moglie e figli 
					fiscalmente a carico di almeno uno dei due coniugi) in modo
                    rapido e sicuro via web. L’utilizzo della funzione nell’area privata consente di
                    salvare i dati inseriti in qualsiasi momento della compilazione della richiesta</li>
                <li><strong>Malattia / Infortunio</strong> per richiedere on-line i rimborsi relativi ai trattamenti 
                    economici erogati dall’impresa al lavoratore dipendente e ricercare, visualizzare e 
                    stampare gli attestati relativi alle richieste di rimborso inoltrate a Cassa Edile ed 
                    indennizzate.</li>
            </ul>
            <em>La sezione è fruibile da parte di imprese e consulenti del lavoro</em>
        </p>
        <br /><span style="color: #CD071E;"><strong>Rendiconti</strong></span>
        <p>
            In questa sezione è possibile verificare:
            <ul>
                <li>le <strong>ore accantonate</strong> (denunciate e pagate) riferite ad uno specifico
                    periodo o ad un singolo dipendente dichiarato presso il nostro Ente</li>
                <li>i <strong>rapporti di lavoro</strong> dipendente dichiarati presso il nostro Ente</li>
                <li>il <strong>riepilogo pagamenti</strong> (i prospetti degli importi versati a ogni
                    singolo dipendente in base agli accantonamenti effettuati per trattamento economico
                    per ferie e gratifica natalizia e per Premio di Fedeltà)</li>
            </ul>
        </p>
    </asp:Panel>
    <asp:Panel ID="PanelLavoratore" runat="server" CssClass="DefaultPage" Visible="false">
        <p>
            Benvenuto
            <%=Membership.GetUser().UserName%>!
        </p>
        <p>
            In quest’area potrai accedere alle funzioni private attribuite al tuo profilo raggruppate 
			sotto le seguenti sezioni:
        </p>
        <span style="color: #CD071E;"><strong>Sportello web</strong></span>
        <p>
            <ul>
                <li><strong>Richiesta prestazioni</strong> per inoltrare una domanda di rimborso per
                    se stessi e, laddove previsto, per i familiari fiscalmente a carico (moglie e figli 
					fiscalmente a carico di almeno uno dei due coniugi) in modo
                    rapido e sicuro via web. L’utilizzo della funzione nell’area privata consente di
                    salvare i dati inseriti in qualsiasi momento della compilazione della richiesta</li>
                <li><strong>Anagrafica</strong> per visualizzare le informazioni inerenti
                    i propri dati anagrafici dichiarati presso il nostro Ente</li>
            </ul>
        </p>
        <span style="color: #CD071E;"><strong>Rendiconti</strong></span>
        <p>
            <ul>
                <li><strong>Rapporti di lavoro</strong> per consultare i rapporti di lavoro instaurati
                    nel tempo con imprese iscritte presso Cassa Edile di Milano, Lodi, Monza e Brianza</li>
                <li><strong>Ore accantonate</strong> per visualizzare le ore dichiarate e versate dal
                    proprio datore di lavoro in Cassa Edile di Milano, Lodi, Monza e Brianza</li>
                <li><strong>Pagamenti erogati</strong> per prendere visione dei pagamenti eseguiti da
                    Cassa Edile di Milano, Lodi, Monza e Brianza per trattamento economico per ferie,
                    gratifica natalizia, A.P.E. (Anzianità Professionale Edile) ed eventuali prestazioni
                    assistenziali richieste</li>
                <li><strong>Certificazione Unica (CU)</strong> per scaricare e stampare il file relativo 
                    al modello di attestazione dei redditi di lavoro dipendente ed assimilati, 
                    di lavoro autonomo e “redditi diversi”</li>
            </ul>
        </p>
    </asp:Panel>
    <asp:Panel ID="PanelAutenticato" runat="server" CssClass="DefaultPage" Visible="false">
        <p>
            Benvenuto
            <%=Membership.GetUser().UserName%>!
        </p>
        <p>
            Il menu orizzontale in alto riporta le sezioni disponibili per la tua utenza. Clicca
            su ognuna di essa per accedere alle funzionalità correlate.</p>
    </asp:Panel>
    <p>&nbsp;</p>
    <p><span style="color: #CD071E;"><strong><a href="RendicontiLavoratori/Richiesta_codice_PIN_rev.1.pdf" target="_blank">ISTRUZIONI PER SCARICARE LA CU</a></strong></span></p>
    <p>&nbsp;</p>
</asp:Content>
