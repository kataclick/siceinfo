﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeStatisticheRiassunto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeStatistiche);
        }

        protected void ReportViewerRiassunto_Init(object sender, EventArgs e)
        {
            ReportViewerRiassunto.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            //ReportViewerRiassunto.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportComplessivo";
        }

        protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
        {
            ReportViewerRiassunto.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportComplessivo";

            List<ReportParameter> listaParam = new List<ReportParameter>();
            if (RadDatePickerDataDa.SelectedDate != null)
            {
                ReportParameter param1 = new ReportParameter("dal",
                                             RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param1);
            }
            if (RadDatePickerDataA.SelectedDate != null)
            {
                ReportParameter param2 = new ReportParameter("al", RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param2);
            }

            ReportViewerRiassunto.ServerReport.SetParameters(listaParam.ToArray());
        }
    }
}