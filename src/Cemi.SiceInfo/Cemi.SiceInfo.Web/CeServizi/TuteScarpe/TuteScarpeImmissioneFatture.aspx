﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeImmissioneFatture.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeImmissioneFatture" %>

<%--<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione" TagPrefix="uc1" %>--%>
<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Immissione fatture" />
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 120px" align="left">
                <asp:Label ID="Label3" runat="server" Text="File:"></asp:Label></td>
            <td align="left">
    <asp:FileUpload ID="FileUploadFattura" runat="server" Width="518px" /></td>
        </tr>
        
        <tr>
            <td>
    
    <asp:Button ID="ButtonUpload" runat="server" Text="Verifica fattura" Width="117px" OnClick="ButtonUpload_Click" ValidationGroup="Verifica" /></td>
            <td>
    
    <asp:RequiredFieldValidator 
 id="RequiredFieldValidator1" runat="server" 
 ErrorMessage="Selezionare un file" 
 ControlToValidate="FileUploadFattura" ValidationGroup="Verifica"></asp:RequiredFieldValidator>
            <asp:Label ID="LabelTXT" runat="server" ForeColor="Red" Text="Selezionare un file txt"
                Visible="False"></asp:Label></td>
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Errore" Visible="False"></asp:Label></td>
        </tr>
        
        <tr>
            <td align="left">
                <asp:Label ID="LabelFornitore" runat="server" Text="Fornitore"></asp:Label></td>
            <td align="left">
                <asp:DropDownList ID="DropDownListFornitori" runat="server" Visible="False" 
                    Width="260px">
                </asp:DropDownList>
                <asp:TextBox ID="TextBoxFornitore" runat="server" Enabled="False" ReadOnly="True"
                    Visible="False" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DropDownListFornitori"
                    Enabled="False" EnableTheming="True" ErrorMessage="Selezionare un fornitore"
                    ValidationGroup="Conferma"></asp:RequiredFieldValidator></td>
        </tr>
        
        <tr>
            <td style="width: 120px" align="left">
                <asp:Label ID="Label2" runat="server" Text="Descrizione:"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="TextBoxDescrizione" runat="server" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxDescrizione"
                    ErrorMessage="RequiredFieldValidator" ValidationGroup="Conferma">Fornire una descrizione</asp:RequiredFieldValidator></td>
            
        </tr>
        <tr>
            <td style="width: 120px" align="left">
                <asp:Label ID="Label1" runat="server" Text="Codice fattura:"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="TextBoxCodiceFattura" runat="server" Width="250px" Enabled="False" ReadOnly="True"></asp:TextBox>&nbsp;</td>
            
        </tr>
        <tr>
            <td style="width: 120px" align="left">
                <asp:Label ID="Label4" runat="server" Text="Caricato:"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="TextBoxFileName" runat="server" Enabled="False" ReadOnly="True"
                    Width="431px"></asp:TextBox></td>
        </tr>
        
    </table>
    <br />
    <asp:Label ID="LabelTitoloErrori" runat="server" Font-Bold="True" Text="Errori" Visible="False"></asp:Label>
    <br />
    <asp:GridView ID="GridViewErrori" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewErrori_RowDataBound" Width="100%" Visible="False" AllowPaging="True" OnPageIndexChanging="GridViewErrori_PageIndexChanging" PageSize="5">
        <Columns>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa" />
            <asp:BoundField HeaderText="Ragione sociale impresa" DataField="RagioneSociale" />
            <asp:BoundField DataField="IdOrdine" HeaderText="Protocollo ordine" />
            <asp:TemplateField HeaderText="Dettaglio">
                <ItemTemplate>
                    <asp:GridView ID="GridViewDettaglio" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewDettaglio_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="TipoVestiario" HeaderText="Tipo vestiario" />
                            <asp:BoundField DataField="Quantita" HeaderText="Quantit&#224; fattura" />
                            <asp:BoundField HeaderText="C">
                                <ItemStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QuantitaFabbisogno" HeaderText="Quantit&#224; Richiesta" />
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun errore presente<br />
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Stato">
                <ItemTemplate>
                    <asp:GridView ID="GridViewStato" runat="server" AutoGenerateColumns="False"
                        ShowHeader="False" OnRowDataBound="GridViewStato_RowDataBound">
                        <Columns>
                            <asp:BoundField HeaderText="Stato" />
                        </Columns>
                        <EmptyDataTemplate>
                            Corretta
                        </EmptyDataTemplate>
                        <RowStyle ForeColor="Red" />
                        <AlternatingRowStyle ForeColor="Red" />
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessun errore riscontrato
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonInserisci" runat="server" Text="Inserisci" Width="203px" OnClick="ButtonInserisci_Click" Visible="False" ValidationGroup="Conferma"/>
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red" Visible="False"></asp:Label><br />
    &nbsp;
</asp:Content>

