﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeGestioneFabbisogniConfermati.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeGestioneFabbisogniConfermati" %>

<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%--<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione"
    TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini"
    TagPrefix="uc3" %>--%>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%--<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>--%>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe"
        sottoTitolo="Gestione fabbisogni confermati" />
    <br />
    Filtra per Impresa
    <table class="standardTable">
        <tr>
            <td style="width: 149px" align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonTutti" runat="server" Text="Tutti" Checked="True"
                    GroupName="filtro" />
            </td>
            <td style="width: 499px">
            </td>
        </tr>
        <tr>
            <td style="width: 149px" align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonCodice" runat="server" Text="Codice" GroupName="filtro" />
            </td>
            <td style="width: 499px" align="left">
                <asp:TextBox ID="TextBoxCodice" runat="server" Width="277px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 149px" align="left">
                &nbsp;<asp:RadioButton ID="RadioButtonRagioneSociale" runat="server" GroupName="filtro"
                    Text="Ragione sociale" />&nbsp;
            </td>
            <td style="width: 499px" align="left">
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="277px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 149px">
            </td>
            <td style="width: 499px" align="left">
                <asp:Button ID="ButtonFiltro" runat="server" Text="Filtra" Width="81px" OnClick="ButtonFiltro_Click" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:Label ID="LabelElencoImprese" runat="server" Font-Bold="True" Text="Elenco imprese con fabbisogno non inserito in un ordine"></asp:Label>
    <asp:GridView ID="GridViewElencoImprese" runat="server" Width="100%" AutoGenerateColumns="False"
        AllowPaging="True" OnRowDataBound="GridViewElencoImprese_RowDataBound"
        PageSize="5" OnPageIndexChanging="GridViewElencoImprese_PageIndexChanging" 
        DataKeyNames="IdImpresa,IdFabbisognoComplessivo,RagioneSocialeImpresa" 
        onrowcommand="GridViewElencoImprese_RowCommand">
        <Columns>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="RagioneSocialeImpresa" HeaderText="Ragione sociale impresa" />
            <asp:BoundField DataField="IdFabbisognoComplessivo" HeaderText="Codice Fabbisogno Complessivo"
                Visible="False" />
            <asp:BoundField DataField="DataFabbisognoComplessivo" HeaderText="Data proposta di fabbisogno"
                DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Fornitori">
                <ItemTemplate>
                    <asp:GridView ID="GridViewFabbisogni" runat="server" AutoGenerateColumns="False"
                        ShowHeader="False" Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ProgressivoFornitura" HeaderText="Fabbisogno" />
                            <asp:BoundField HeaderText="Fornitore" DataField="CodiceFornitore" />
                        </Columns>
                    </asp:GridView>
                </ItemTemplate>
                <ItemStyle Width="100px" />
            </asp:TemplateField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="Modifica"
                Text="Modifica">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <asp:Label ID="LabelTitolo" runat="server" Font-Bold="True" Height="15px" Text="Fabbisogno dell'impresa:"
        Visible="False"></asp:Label>
    <asp:Label ID="LabelRagioneSocialeImpresa" runat="server" Visible="False"></asp:Label><br />
    <br />
    <asp:Panel ID="PanelIndirizzo" runat="server" Width="100%" Visible="False">
        <asp:Label ID="LabelIndirizzo" runat="server" Text="Indirizzo spedizione:" Font-Bold="True"
            Height="15px" Width="151px"></asp:Label>
        <!-- Tabella con l'indirizzo ALtro -->
        <table class="standardTable">
            <tr>
                <td>
                    Indirizzo:
                </td>
                <td>
                    <!-- Tabellina per l'impostazione dell'indirizzo-->
                    <table class="standardTable">
                        <tr>
                            <td>
                                <asp:Label ID="LabelIdImpresa" runat="server" Visible="False"></asp:Label>
                                <asp:Label ID="LabelIdFabbisognoComplessivo" runat="server" Visible="False"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxIndirizzo" runat="server" Width="360px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBoxIndirizzo"
                                    ErrorMessage="*" ValidationGroup="indirizzo"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                C/O (opzionale):
                            </td>
                            <td>
                                <asp:TextBox ID="TextBoxPresso" runat="server" Width="360px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxProvincia" runat="server" Width="247px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxProvincia"
                        ErrorMessage="*" ValidationGroup="indirizzo"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="DropDownListProvincia" runat="server" OnSelectedIndexChanged="DropDownListPreIndirizzo_SelectedIndexChanged"
                        Width="150px" AutoPostBack="True" Visible="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Comune:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxComune" runat="server" Width="247px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxComune"
                        ErrorMessage="*" ValidationGroup="indirizzo"></asp:RequiredFieldValidator>
                    <asp:DropDownList ID="DropDownListComuni" runat="server" OnSelectedIndexChanged="DropDownListComuni_SelectedIndexChanged"
                        AutoPostBack="True" Width="400px" Visible="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    CAP:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCap" runat="server" MaxLength="5" Width="247px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxCap"
                        ErrorMessage="*" ValidationGroup="indirizzo"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxCap"
                        ErrorMessage="Formato errato" ValidationExpression="\d{5}?" ValidationGroup="indirizzo"></asp:RegularExpressionValidator>
                    <asp:DropDownList ID="DropDownListCAP" runat="server" Width="150px" Visible="False">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Telefono:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxTelefono" runat="server" Width="150px" MaxLength="15">
                    </asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorTelefono" runat="server"
                        ControlToValidate="TextBoxTelefono" ErrorMessage="Telefono non corretto" ValidationExpression="^\+?[\d]{3,14}$"
                        ValidationGroup="indirizzo"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Cellulare:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxCellulare" runat="server" Width="150px" MaxLength="15">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorCellulare" runat="server" ControlToValidate="TextBoxCellulare"
                        ErrorMessage="Cellulare obbligatorio" ValidationGroup="indirizzo"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorCellulare" runat="server"
                        ControlToValidate="TextBoxCellulare" ErrorMessage="Cellulare non corretto" ValidationExpression="^\+?[\d]{3,14}$"
                        ValidationGroup="indirizzo"></asp:RegularExpressionValidator>
                </td>
            </tr>
        </table>
        <asp:Button ID="ButtonSalvaIndirizzo" runat="server" Text="Modifica indirizzo" ValidationGroup="indirizzo"
            Width="121px" OnClick="ButtonSalvaIndirizzo_Click" />
        <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label></asp:Panel>
    &nbsp;
    <br />
    <table class="standardTable">
        <tr>
            <td align="left" style="width: 168px">
                <asp:Label ID="LabelIdLavoratore" runat="server" Text="Id lavoratore:" Visible="False"></asp:Label>
            </td>
            <td align="left" style="width: 280px">
                <asp:TextBox ID="TextBoxIdLavoratore" runat="server" Visible="False" Width="263px"></asp:TextBox>
            </td>
            <td style="width: 108px" align="left">
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBoxIdLavoratore"
                    ErrorMessage="Codice intero" MaximumValue="2147483647" MinimumValue="0" 
                    Type="Integer" ValidationGroup="filtroFabbisogno"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 168px" align="left">
                <asp:Label ID="LabelCognomeNome" runat="server" Text="Nome lavoratore:" Visible="False"></asp:Label>
            </td>
            <td style="width: 280px" align="left">
                <asp:TextBox ID="TextBoxCognomeNome" runat="server" Visible="False" Width="263px"></asp:TextBox>
            </td>
            <td style="width: 108px">
            </td>
        </tr>
        <tr>
            <td style="width: 168px">
            </td>
            <td style="width: 280px" align="left">
                <asp:Button ID="ButtonFiltro2" runat="server" OnClick="ButtonFiltro2_Click" Text="Filtra"
                    Visible="False" Width="119px" ValidationGroup="filtroFabbisogno" />
            </td>
            <td style="width: 108px">
            </td>
        </tr>
    </table>
    <asp:GridView ID="GridViewFabbisognoImpresa" runat="server" Visible="False" Width="100%"
        AutoGenerateColumns="False" 
        OnRowDataBound="GridViewFabbisognoImpresa_RowDataBound" DataKeyNames="idFabbisognoConfermatoDettaglio"
        AllowPaging="True" 
        OnPageIndexChanging="GridViewFabbisognoImpresa_PageIndexChanging" 
        onrowcommand="GridViewFabbisognoImpresa_RowCommand">
        <Columns>
            <asp:BoundField HeaderText="Codice lavoratore">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Nome lavoratore" />
            <asp:BoundField DataField="Indumento" HeaderText="Indumento">
                <ItemStyle Width="150px" />
            </asp:BoundField>
            <asp:BoundField DataField="Quantita" HeaderText="Quantit&#224;">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Taglia">
                <ItemTemplate>
                    <asp:DropDownList ID="DropDownListTaglie" runat="server" Width="86px">
                    </asp:DropDownList>
                </ItemTemplate>
                <ItemStyle Width="50px" />
            </asp:TemplateField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="Salva"
                Text="Salva">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dato estratto
        </EmptyDataTemplate>
    </asp:GridView>
    &nbsp;&nbsp;&nbsp;<br />
    <b>Ogni singola modifica necessita di una conferma.</b><br />
    <br />
</asp:Content>

