﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DettaglioStatoConsegna.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.DettaglioStatoConsegna" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>

    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    
    <div>
    <table>
        <tr>
            <td>Numero Ordine</td>
            <td><asp:Label runat="server" ID="LabelNumeroOrdine"/></td>
        </tr>
        <tr>
            <td>Codice Impresa</td>
            <td><asp:Label runat="server" ID="LabelCodiceImpresa"/></td>
        </tr>
    </table>
      <telerik:RadGrid
        ID="RadGridDettaglioStatoConsegna"
        runat="server" GridLines="None" AllowPaging="True" 
        onpageindexchanged="RadGridDettaglioStatoConsegna_PageIndexChanged" 
        PageSize="5">
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

        <MasterTableView>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Numero Spedizione" 
                    DataField="NumeroSpedizione">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Data evento" 
                    DataField="DataEvento" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Descrizione evento" 
                    DataField="Stato">
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    </div>
    </form>
</body>
</html>

