﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeGestioneFatture.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeGestioneFatture" %>

<%--<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione" TagPrefix="uc1" %>--%>

<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Gestione fatture" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Dal (gg/mm/aaaa)"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="10"></asp:TextBox></td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDal"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxDal"
                    ErrorMessage="Immettere una data"></asp:RequiredFieldValidator></td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Al (gg/mm/aaaa)"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="10"></asp:TextBox></td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxAl"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxAl"
                    ErrorMessage="Immettere una data"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Correttezza"></asp:Label></td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonCorretteTutte" runat="server" GroupName="correttezza" Text="Tutte" Checked="True" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonCorrette" runat="server" GroupName="correttezza" Text="Corrette" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonNonCorrette" runat="server" GroupName="correttezza" Text="Non corrette" /></td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Forzatura"></asp:Label></td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonForzateTutte" runat="server" GroupName="forzatura" Text="Tutte" Checked="True" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonForzate" runat="server" GroupName="forzatura" Text="Forzate" /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonNonForzate" runat="server" GroupName="forzatura" Text="Non forzate" /></td>
                    </tr>
                </table>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonCerca" runat="server" OnClick="ButtonCerca_Click" Text="Cerca"
                    Width="101px" /></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br />
    <asp:Label ID="LabelFiltro" runat="server" Font-Bold="True" Text="Filtro impresa:" Visible="False"></asp:Label><br />
    <asp:Label ID="LabelRagSoc" runat="server" Text="Ragione sociale:" Visible="False"></asp:Label><asp:TextBox
        ID="TextBoxRagSoc" runat="server" Width="451px" Visible="False"></asp:TextBox><asp:Button ID="ButtonFiltra"
            runat="server" OnClick="ButtonFiltra_Click" Text="Cerca" Visible="False" Width="71px" /><br />
    <br />
    <asp:Label ID="LabelElencoFatture" runat="server" Font-Bold="True" Text="Elenco Fatture:" Visible="False"></asp:Label>&nbsp;
    <asp:GridView ID="GridViewFatture" runat="server" Width="100%"
        AutoGenerateColumns="False" DataKeyNames="IdFattura,CodiceFattura"
        OnRowUpdating="GridViewFatture_RowUpdating" AllowPaging="True"
        OnPageIndexChanging="GridViewFatture_PageIndexChanging" PageSize="8"
        OnRowDeleting="GridViewFatture_RowDeleting"
        OnRowDataBound="GridViewFatture_RowDataBound"
        OnRowCommand="GridViewFatture_RowCommand">
        <Columns>
            <asp:BoundField DataField="CodiceFattura" HeaderText="Codice fattura" />
            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Fornitore" />
            <asp:BoundField DataField="DataUpload" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data upload"
                HtmlEncode="False" />
            <asp:TemplateField HeaderText="Corretta">
                <ItemTemplate>
                    <asp:Image ID="ImageStato" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Forzata">
                <ItemStyle HorizontalAlign="Center" />
                <ItemTemplate>
                    <asp:Image ID="ImageForzata" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button"
                CommandName="Forza" Text="Forza">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="Delete" Text="Elimina">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                <ItemStyle HorizontalAlign="Center" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button"
                CommandName="Visualizza" Text="Visualizza file">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button"
                CommandName="Update" Text="Dettagli">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna fattura con le caratteristiche richieste
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Label ID="LabelDettagliFattura" runat="server" Font-Bold="True" Text="Dettagli fattura" Visible="False"></asp:Label>
    <asp:Label ID="LabelCodiceFattura" runat="server" Visible="False"></asp:Label>&nbsp;<asp:Label
        ID="LabelIdFattura" runat="server" Visible="False"></asp:Label><br />
    <br />
    <asp:Label ID="LabelRiassunto" runat="server" Text="Riassunto" Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewRiassunto" runat="server" AutoGenerateColumns="False" Width="100%">
        <Columns>
            <asp:BoundField DataField="CategoriaVestiario" HeaderText="Categoria" />
            <asp:BoundField DataField="Quantita" HeaderText="Quantita">
                <ItemStyle Width="100px" />
            </asp:BoundField>
        </Columns>
    </asp:GridView>
    <br />
    <asp:Label ID="LabelDettagli" runat="server" Text="Dettagli" Visible="False"></asp:Label><br />
    <asp:GridView ID="GridViewDettaglioFattura" runat="server" AutoGenerateColumns="False"
        OnRowDataBound="GridViewDettaglioFattura_RowDataBound" PageSize="5"
        Width="100%" AllowPaging="True" OnPageIndexChanging="GridViewDettaglioFattura_PageIndexChanging">
        <Columns>
            <asp:BoundField DataField="IdImpresa" HeaderText="Codice Impresa">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField HeaderText="Ragione sociale impresa" DataField="RagioneSociale" />
            <asp:BoundField DataField="IdOrdine" HeaderText="Protocollo ordine">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Dettaglio">
                <ItemTemplate>
                    <asp:GridView ID="GridViewDettaglio" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewDettaglio_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="TipoVestiario" HeaderText="Tipo vestiario" />
                            <asp:BoundField DataField="Quantita" HeaderText="Quantit&#224; fattura" />
                            <asp:BoundField HeaderText="C">
                                <ItemStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QuantitaFabbisogno" HeaderText="Quantit&#224; Richiesta" />
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun errore presente<br />
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Stato">
                <ItemTemplate>
                    <asp:GridView ID="GridViewStato" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewStato_RowDataBound"
                        ShowHeader="False">
                        <Columns>
                            <asp:BoundField HeaderText="Stato" />
                        </Columns>
                        <RowStyle ForeColor="Red" />
                        <EmptyDataTemplate>
                            Corretta
                        </EmptyDataTemplate>
                        <AlternatingRowStyle ForeColor="Red" />
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            Nessun dettaglio disponibile
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
</asp:Content>