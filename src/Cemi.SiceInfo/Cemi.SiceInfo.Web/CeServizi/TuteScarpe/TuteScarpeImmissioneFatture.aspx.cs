﻿using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
//using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Fornitore = TBridge.Cemi.Type.Entities.GestioneUtenti.Fornitore;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeImmissioneFatture : System.Web.UI.Page
    {
        private readonly TSBusiness tsBiz = new TSBusiness();
        private string codiceFornitore = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeImmissioneFatture);

            if (GestioneUtentiBiz.IsFornitore())
            {
                //TBridge.Cemi.GestioneUtenti.Type.Entities.Fornitore fornitore =
                //    ((Fornitore) HttpContext.Current.User.Identity).Entity;
                Fornitore fornitore =
                    (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                codiceFornitore = fornitore.Codice;

                TextBoxFornitore.Text = codiceFornitore;
                TextBoxFornitore.Visible = true;
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    FornitoreList listaFornitori = tsBiz.GetFornitori();
                    DropDownListFornitori.DataSource = listaFornitori;
                    DropDownListFornitori.DataTextField = "CodiceFornitore";
                    DropDownListFornitori.DataValueField = "CodiceFornitore";
                    DropDownListFornitori.DataBind();

                    if (DropDownListFornitori.Items.FindByValue("OPEN") != null)
                    {
                        DropDownListFornitori.SelectedValue = "OPEN";
                    }

                    RequiredFieldValidator3.Enabled = true;
                    DropDownListFornitori.Visible = true;
                }
            }

            // Per prevenire click multipli
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            //sb.Append("if (Page_ClientValidate() == false) { return false; }} ");
            //sb.Append("this.value = 'Attendere...';");
            //sb.Append("this.disabled = true;");
            //sb.Append(this.Page.GetPostBackEventReference(this.ButtonInserisci));
            //sb.Append(";");
            //this.ButtonInserisci.Attributes.Add("onclick", sb.ToString());
            ButtonInserisci.Attributes.Add("onclick",
                                           "this.value='Attendere...';this.disabled = true;" +
                                           ClientScript.GetPostBackEventReference(ButtonInserisci, null));
        }

        protected void ButtonUpload_Click(object sender, EventArgs e)
        {
            LabelRisultato.Visible = false;

            if (!GestioneUtentiBiz.IsFornitore())
            {
                codiceFornitore = DropDownListFornitori.Text;
            }

            if ((FileUploadFattura.HasFile) && (!string.IsNullOrEmpty(codiceFornitore)))
            {
                if (Path.GetExtension(FileUploadFattura.PostedFile.FileName).ToUpper() == ".TXT")
                {
                    LabelTXT.Visible = false;
                    TextBoxFileName.Text = Path.GetFileName(FileUploadFattura.PostedFile.FileName);

                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(FileUploadFattura.PostedFile.FileName);
                        string[] splittedFileName = fileName.Split(' ');

                        int numeroFattura = Int32.Parse(splittedFileName[1]);
                        TextBoxCodiceFattura.Text = numeroFattura.ToString();

                        if (!tsBiz.EsisteFattura(numeroFattura, codiceFornitore))
                        {
                            Fattura fattura =
                                tsBiz.CreaFattura(FileUploadFattura.PostedFile.InputStream, numeroFattura,
                                                  TextBoxDescrizione.Text, codiceFornitore);
                            tsBiz.VerificaFattura(fattura);

                            ViewState["Fattura"] = fattura;

                            GridViewErrori.DataSource = fattura.Bolle;
                            GridViewErrori.DataBind();

                            if (fattura.Stato())
                            {
                                GridViewErrori.DataSource = null;
                                GridViewErrori.DataBind();
                                ButtonInserisci.Text = "Conferma inserimento";
                            }
                            else
                                ButtonInserisci.Text = "Forza inserimento";

                            VisualizzaDettagli(true, false);
                        }
                        else
                        {
                            LabelErrore.Text = "Il codice della fattura fornita è già presente";
                            VisualizzaDettagli(false, true);
                        }
                    }
                    catch (ArgumentException exc)
                    {
                        LabelErrore.Text = exc.Message;
                        LabelErrore.Visible = true;

                        GridViewErrori.DataSource = null;
                        GridViewErrori.DataBind();

                        VisualizzaDettagli(false, true);
                    }
                    catch (Exception)
                    {
                        LabelErrore.Text = "Formato file non corretto";
                        LabelErrore.Visible = true;

                        GridViewErrori.DataSource = null;
                        GridViewErrori.DataBind();

                        VisualizzaDettagli(false, true);
                    }
                }
                else
                {
                    LabelTXT.Visible = true;
                    TextBoxFileName.Text = string.Empty;

                    GridViewErrori.DataSource = null;
                    GridViewErrori.DataBind();

                    VisualizzaDettagli(false, false);
                }
            }
        }

        private void VisualizzaDettagli(bool visualizza, bool visualizzaErrore)
        {
            GridViewErrori.Visible = visualizza;
            LabelTitoloErrori.Visible = visualizza;
            ButtonInserisci.Visible = visualizza;
            LabelErrore.Visible = visualizzaErrore;
        }

        protected void GridViewErrori_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Bolla bolla = (Bolla)e.Row.DataItem;

                // Bind del grid view interno
                GridView gvInterno = (GridView)e.Row.FindControl("GridViewDettaglio");
                gvInterno.DataSource = bolla.VociBolla;
                gvInterno.DataBind();

                // Bind dello stato
                GridView gvStato = (GridView)e.Row.FindControl("GridViewStato");
                gvStato.DataSource = bolla.Stato;
                gvStato.DataBind();

                if ((bolla.Stato != null) && (bolla.Stato.Count > 0))
                {
                    foreach (StatoBollaClass statoBolla in bolla.Stato)
                    {
                        switch (statoBolla.Stato)
                        {
                            case StatoBolla.GiaGestita:
                                e.Row.Cells[0].ForeColor = Color.Red;
                                e.Row.Cells[1].ForeColor = Color.Red;
                                e.Row.Cells[2].ForeColor = Color.Red;
                                break;
                            case StatoBolla.ImpresaNonPresente:
                                e.Row.Cells[0].ForeColor = Color.Red;
                                e.Row.Cells[1].ForeColor = Color.Red;
                                break;
                            case StatoBolla.OrdineAltroFornitore:
                                e.Row.Cells[2].ForeColor = Color.Red;
                                break;
                            case StatoBolla.OrdineNonPresente:
                                e.Row.Cells[2].ForeColor = Color.Red;
                                break;
                        }
                    }
                }
                else
                    e.Row.Visible = false;

                // Visualizzazione del gridview interno
                //bool vis = false;
                //for (int i = 0; i < gvInterno.Rows.Count; i++)
                //{
                //    if (gvInterno.Rows[i].Cells[2].Text != "=")
                //    {
                //        vis = true;
                //        break;
                //    }
                //}
                //e.Row.Visible = vis;
            }
        }

        protected void GridViewDettaglio_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                VoceBolla voce = (VoceBolla)e.Row.DataItem;

                if (voce.Quantita == voce.QuantitaFabbisogno)
                {
                    e.Row.Cells[2].Text = "=";
                    //e.Row.ForeColor = Color.Green;
                    //e.Row.Visible = false;
                }
                if (voce.Quantita > voce.QuantitaFabbisogno)
                {
                    e.Row.Cells[2].Text = ">";
                    e.Row.ForeColor = Color.Red;
                }
                if (voce.Quantita < voce.QuantitaFabbisogno)
                {
                    e.Row.Cells[2].Text = "<";
                    e.Row.ForeColor = Color.Red;
                }
            }
        }

        protected void ButtonInserisci_Click(object sender, EventArgs e)
        {
            if ((ViewState["Fattura"] != null) && (!string.IsNullOrEmpty(TextBoxDescrizione.Text)))
            {
                Fattura fattura = (Fattura)ViewState["Fattura"];
                fattura.Descrizione = TextBoxDescrizione.Text;
                fattura.CodiceFattura = Int32.Parse(TextBoxCodiceFattura.Text);

                if (tsBiz.InserisciFattura(fattura))
                {
                    ButtonInserisci.Enabled = false;
                    LabelRisultato.Text = "Fattura inserita correttamente";
                }
                else
                    LabelRisultato.Text = "Errore nell'inserimento";
                LabelRisultato.Visible = true;
            }
        }

        protected void GridViewErrori_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (ViewState["Fattura"] != null)
            {
                Fattura fattura = (Fattura)ViewState["Fattura"];

                GridViewErrori.DataSource = fattura.Bolle;
                GridViewErrori.PageIndex = e.NewPageIndex;
                GridViewErrori.DataBind();
            }
        }

        protected void GridViewStato_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                StatoBollaClass stato = (StatoBollaClass)e.Row.DataItem;
                e.Row.Cells[0].Text = stato.GetTesto();
            }
        }
    }
}