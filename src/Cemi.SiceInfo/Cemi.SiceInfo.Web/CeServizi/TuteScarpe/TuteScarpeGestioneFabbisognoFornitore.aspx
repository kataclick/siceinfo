﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeGestioneFabbisognoFornitore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeGestioneFabbisognoFornitore" %>

<%@ Register Src="WebControls/TuteScarpeInfoSpedizioneImpresa.ascx" TagName="TuteScarpeInfoSpedizioneImpresa"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/TuteScarpeFornitoreImpresaSelezionaImpresa.ascx" TagName="TuteScarpeFornitoreImpresaSelezionaImpresa"
    TagPrefix="uc4" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione fabbisogni"
        titolo="Tute e Scarpe" />
    <asp:Label ID="LabelMessaggio" runat="server" ForeColor="Red"></asp:Label><br />
    <br />
    <uc4:TuteScarpeFornitoreImpresaSelezionaImpresa ID="TuteScarpeFornitoreImpresaSelezionaImpresa1"
        runat="server" />
</asp:Content>