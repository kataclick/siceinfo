﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeStatisticheOrdiniEffettuati.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeStatisticheOrdiniEffettuati" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Tute e Scarpe Statistiche"
        sottoTitolo="Ordini predisposti" />
    <br />
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td class="centermain">
                    Dal:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDa" Width="100%" runat="server" />
                </td>
                <td class="centermain">
                    Al:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataA" Width="100%" runat="server" />
                </td>
            </tr>
        </table>
        <div style="padding: 10px;">
            <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
                Text="Visualizza report" ValidationGroup="VisualizzazioneReport" />
            <br />
            <asp:CompareValidator ID="CompareValidatorData" ControlToValidate="RadDatePickerDataA"
                        ControlToCompare="RadDatePickerDataDa" Operator="GreaterThanEqual" ErrorMessage="La data di fine non può essere precedente a quella di inizio"
                        ValidationGroup="VisualizzazioneReport" runat="server" Type="Date" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataDa" ControlToValidate="RadDatePickerDataDa" ErrorMessage="Data inizio ricerca assente." ValidationGroup="VisualizzazioneReport" runat="server" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorA" ControlToValidate="RadDatePickerDataA" ErrorMessage="Data fine ricerca assente." ValidationGroup="VisualizzazioneReport" runat="server" />
        </div>
    </div>
    <table class="standardTable">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerOrdiniEffettuati" runat="server" Height="500px"
                    OnInit="ReportViewerOrdiniEffettuati_Init" Width="100%" ProcessingMode="Remote"
                    ShowPageNavigationControls="False">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>

