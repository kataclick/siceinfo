﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeStatisticheRiassunto.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeStatisticheRiassunto" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc2" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione tute e scarpe"
        sottoTitolo="Statistiche riassunto" />
    <br />
    <div class="borderedDiv">
        <table class="standardTable">
            <tr>
                <td class="centermain">
                    Dal:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataDa" Width="100%" runat="server" />
                </td>
                <td class="centermain">
                    Al:
                </td>
                <td>
                    <telerik:RadDatePicker ID="RadDatePickerDataA" Width="100%" runat="server" />
                </td>
            </tr>
        </table>
        <div style="padding: 10px;">
            <asp:Button ID="ButtonVisualizzaReport" runat="server" OnClick="ButtonVisualizzaReport_Click"
                Text="Visualizza report" ValidationGroup="VisualizzazioneReport" />
            <br />
            <asp:CompareValidator ID="CompareValidatorData" ControlToValidate="RadDatePickerDataA"
                        ControlToCompare="RadDatePickerDataDa" Operator="GreaterThanEqual" ErrorMessage="La data di fine non può essere precedente a quella di inizio"
                        ValidationGroup="VisualizzazioneReport" runat="server" Type="Date" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataDa" ControlToValidate="RadDatePickerDataDa" ErrorMessage="Data inizio ricerca assente." ValidationGroup="VisualizzazioneReport" runat="server" />
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorA" ControlToValidate="RadDatePickerDataA" ErrorMessage="Data fine ricerca assente." ValidationGroup="VisualizzazioneReport" runat="server" />
        </div>
    </div>
    <table style="height: 600pt; width: 100%;">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerRiassunto" runat="server" OnInit="ReportViewerRiassunto_Init"
                    ProcessingMode="Remote" ShowPageNavigationControls="False" Width="100%" Height="550pt">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>

