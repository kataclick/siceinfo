﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe {
    
    
    public partial class TuteScarpeGestioneFabbisognoFornitore {
        
        /// <summary>
        /// TuteScarpeMenu1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.MenuTuteScarpeContenitore TuteScarpeMenu1;
        
        /// <summary>
        /// TitoloSottotitolo1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.TitoloSottotitolo TitoloSottotitolo1;
        
        /// <summary>
        /// LabelMessaggio control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelMessaggio;
        
        /// <summary>
        /// TuteScarpeFornitoreImpresaSelezionaImpresa1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.TuteScarpeFornitoreImpresaSelezionaImpresa TuteScarpeFornitoreImpresaSelezionaImpresa1;
    }
}
