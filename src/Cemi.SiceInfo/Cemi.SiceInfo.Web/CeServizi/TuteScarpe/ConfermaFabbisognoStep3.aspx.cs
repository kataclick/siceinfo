﻿using System;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class ConfermaFabbisognoStep3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);
            #endregion

            bool salvataggioOk;
            Boolean.TryParse(Request.QueryString["ok"], out salvataggioOk);

            if (salvataggioOk)
            {
                LabelRisultato.Text = "Il salvataggio dell'ordine è avvenuto correttamente.";
                PanelRecapiti.Visible = true;
            }
            else
            {
                LabelRisultato.Text = "Il salvataggio dell'ordine non è andato a buon fine. Si prega di riprovare.";
            }
        }
    }
}