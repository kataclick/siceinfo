﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>
                                                         {
                                                             FunzionalitaPredefinite.
                                                                 TuteScarpeGestioneFabbisogniConfermati,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneOrdini,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneConsulenti,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneSms,
                                                             FunzionalitaPredefinite.TuteScarpeSelezioneTaglie,
                                                             FunzionalitaPredefinite.TuteScarpeGestioneScadenze
                                                         };

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion
        }
    }
}