﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class ReportStampa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["titolo"] != null)
            {
                string titolo = Request.QueryString["titolo"];
                Title = titolo;

                LabelTitolo.Text = titolo;
            }
        }
    }
}