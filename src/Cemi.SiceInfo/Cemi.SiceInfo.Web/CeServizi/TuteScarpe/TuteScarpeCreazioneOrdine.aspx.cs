﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
//using TBridge.Cemi.GestioneUtenti.Type;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeCreazioneOrdine : System.Web.UI.Page
    {
        private readonly TSBusiness tsBiz = new TSBusiness();
        //private static object bloccoOrdine = new object();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneOrdini);
            if (!Page.IsPostBack)
            {
                FabbisognoComplessivoList listaFabbisogni = tsBiz.GetFabbisogniComplessivi();
                DropDownListElencoFabbisogni.DataSource = listaFabbisogni;
                DropDownListElencoFabbisogni.DataTextField = "Data";
                DropDownListElencoFabbisogni.DataValueField = "IdFabbisognoComplessivo";
                DropDownListElencoFabbisogni.DataBind();

                CaricaFabbisogni();

                CaricaOrdineTemporaneo();
            }

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonCreaOrdine, null) + ";");
            sb.Append("return true;");
            ButtonCreaOrdine.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonCreaOrdine);

        }

        protected void ButtonCreaOrdine_Click(object sender, EventArgs e)
        {
            //1: Controllare che qualcosa sia stato inserito nell'ordine
            FabbisognoList listaFab = CaricaOrdineTemporaneo();
            if ((listaFab != null) && (listaFab.Count > 0))
            {
                //2: Creare l'ordine (insert nella tabella TuteScarpeOrdini e update del campo idTuteScarpeOrdine di tutti i fabbisogni completati 
                //inseriti nell'ordine)

                // USP_TuteScarpeOrdiniInsert
                // USP_TuteScarpeFabbisogniConfermatiUpdateSetOrdine

                //3: dare un feedback all'utente
                tsBiz.CreaOrdine(TextBoxDescrizioneOrdine.Text, listaFab, CheckBoxConfermato.Checked);
                LabelRisultato.Text = "Ordine creato correttamente";

                TextBoxDescrizioneOrdine.Text = string.Empty;
                CheckBoxConfermato.Checked = false;

                SvuotaOrdine();
            }
            else
                LabelRisultato.Text = "Inserire almeno un fabbisogno nell'ordine";
        }

        protected void DropDownListElencoFabbisogni_SelectedIndexChanged(object sender, EventArgs e)
        {
            CaricaFabbisogni();

            SvuotaOrdine();

            //CaricaOrdineTemporaneo();
        }

        private FabbisognoList CaricaFabbisogni()
        {
            FabbisognoList listaFabbisogni = new FabbisognoList();

            if (!String.IsNullOrEmpty(DropDownListElencoFabbisogni.SelectedValue))
            {
                ButtonCreaOrdine.Enabled = true;

                int idFabbCompl = Int32.Parse(DropDownListElencoFabbisogni.SelectedValue);

                listaFabbisogni = tsBiz.GetFabbisogniConfermatiByComplessivo(idFabbCompl);
                GridViewFabbisogniConfermati.DataSource = listaFabbisogni;
                GridViewFabbisogniConfermati.DataBind();
            }
            else
                ButtonCreaOrdine.Enabled = false;

            if (listaFabbisogni.Count > 0)
                ButtonAggiungiTutti.Visible = true;
            else
                ButtonAggiungiTutti.Visible = false;

            return listaFabbisogni;
        }

        private void SvuotaOrdine()
        {
            //IUtente
            //    utente = ApplicationInstance.GetUtenteSistema();
            //int idUtente = utente.IdUtente;
            int idUtente = GestioneUtentiBiz.GetIdUtente();

            tsBiz.PulisciOrdineTemporaneo(idUtente);
            CaricaFabbisogni();
            CaricaOrdineTemporaneo();
        }

        private FabbisognoList CaricaOrdineTemporaneo()
        {
            //IUtente
            //    utente = ApplicationInstance.GetUtenteSistema();
            //int idUtente = utente.IdUtente;
            int idUtente = GestioneUtentiBiz.GetIdUtente();

            FabbisognoList listaOrdineTemporaneo = tsBiz.SelectOrdineTemporaneo(idUtente);
            GridViewOrdine.DataSource = listaOrdineTemporaneo;
            GridViewOrdine.DataBind();

            if (listaOrdineTemporaneo.Count > 0)
                ButtonRimuoviTutti.Visible = true;
            else
                ButtonRimuoviTutti.Visible = false;

            return listaOrdineTemporaneo;
        }

        protected void GridViewFabbisogniConfermati_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CaricaFabbisogni();
            CaricaOrdineTemporaneo();

            GridViewFabbisogniConfermati.PageIndex = e.NewPageIndex;
            GridViewFabbisogniConfermati.DataBind();
        }

        protected void GridViewOrdine_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CaricaFabbisogni();
            CaricaOrdineTemporaneo();

            GridViewOrdine.PageIndex = e.NewPageIndex;
            GridViewOrdine.DataBind();
        }

        protected void ButtonAggiungiTutti_Click(object sender, EventArgs e)
        {
            FabbisognoList listaFabbisogni = CaricaFabbisogni();
            //IUtente
            //    utente = ApplicationInstance.GetUtenteSistema();
            //int idUtente = utente.IdUtente;
            int idUtente = GestioneUtentiBiz.GetIdUtente();

            for (int i = 0; i < listaFabbisogni.Count; i++)
            {
                int idFab = listaFabbisogni[i].IdFabbisogno;

                if (tsBiz.InsertVoceOrdineTemporaneo(idUtente, idFab))
                    LabelErrore.Visible = false;
                else
                    LabelErrore.Visible = true;
            }

            CaricaFabbisogni();
            CaricaOrdineTemporaneo();
        }

        protected void ButtonRimuoviTutti_Click(object sender, EventArgs e)
        {
            FabbisognoList ordineTemporaneo = CaricaOrdineTemporaneo();
            //IUtente
            //    utente = ApplicationInstance.GetUtenteSistema();
            //int idUtente = utente.IdUtente;
            int idUtente = GestioneUtentiBiz.GetIdUtente();

            for (int i = 0; i < ordineTemporaneo.Count; i++)
            {
                int idFab = ordineTemporaneo[i].IdFabbisogno;

                tsBiz.DeleteVoceOrdineTemporaneo(idUtente, idFab);
            }

            CaricaFabbisogni();
            CaricaOrdineTemporaneo();
        }
        protected void GridViewFabbisogniConfermati_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Aggiungi")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int idFab = (int)GridViewFabbisogniConfermati.DataKeys[index].Value;

                //IUtente
                //    utente = ApplicationInstance.GetUtenteSistema();
                //int idUtente = utente.IdUtente;
                int idUtente = GestioneUtentiBiz.GetIdUtente();

                if (tsBiz.InsertVoceOrdineTemporaneo(idUtente, idFab))
                    LabelErrore.Visible = false;
                else
                    LabelErrore.Visible = true;

                CaricaOrdineTemporaneo();

                CaricaFabbisogni();
            }
        }
        protected void GridViewOrdine_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Rimuovi")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                //IUtente
                //    utente = ApplicationInstance.GetUtenteSistema();
                //int idUtente = utente.IdUtente;
                int idUtente = GestioneUtentiBiz.GetIdUtente();

                CaricaFabbisogni();
                CaricaOrdineTemporaneo();

                int idFab = (int)GridViewOrdine.DataKeys[index].Value;

                tsBiz.DeleteVoceOrdineTemporaneo(idUtente, idFab);

                CaricaFabbisogni();
                CaricaOrdineTemporaneo();

            }
        }
    }
}