﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls {
    
    
    public partial class TuteScarpeInfoSpedizioneImpresa {
        
        /// <summary>
        /// LabelRagioneSociale control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelRagioneSociale;
        
        /// <summary>
        /// LabelSedeLegaleIndirizzo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeLegaleIndirizzo;
        
        /// <summary>
        /// LabelSedeLegaleTelefono control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeLegaleTelefono;
        
        /// <summary>
        /// LabelSedeLegaleFax control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeLegaleFax;
        
        /// <summary>
        /// LabelSedeLegaleEmail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeLegaleEmail;
        
        /// <summary>
        /// LabelSedeAmministrativaIndirizzo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeAmministrativaIndirizzo;
        
        /// <summary>
        /// LabelSedeAmministrativaPresso control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeAmministrativaPresso;
        
        /// <summary>
        /// LabelSedeAmministrativaTelefono control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeAmministrativaTelefono;
        
        /// <summary>
        /// LabelSedeAmministrativaFax control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeAmministrativaFax;
        
        /// <summary>
        /// LabelSedeAmministrativaEmail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelSedeAmministrativaEmail;
    }
}
