﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stampaGruppi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.stampaGruppi" %>

<br />
<asp:GridView ID="GridView1" runat="server" OnDataBound="GridView1_DataBound" AutoGenerateColumns="False">
    <Columns>
        <asp:BoundField DataField="idLavoratore" HeaderText="Codice" />
        <asp:BoundField DataField="nomeLavoratore" HeaderText="Nome" />
        <asp:BoundField DataField="codiceFiscale" HeaderText="Codice fiscale" Visible="False" />
        <asp:BoundField DataField="dataNascita" HeaderText="Data di nascita" DataFormatString="{0:d}" HtmlEncode="False" />
        <asp:BoundField DataField="nomeGruppo" HeaderText="Gruppo" />
        <asp:BoundField DataField="descrizione" HeaderText="Elemento" />
        <asp:CheckBoxField DataField="selezionato" HeaderText="Selez." />
        <asp:BoundField DataField="quantita" HeaderText="Quantit&#224;" />
        <asp:BoundField DataField="tagliaSel" HeaderText="Taglia" />
    </Columns>
</asp:GridView>
<br />
<b>Legenda:</b>
<br />
&nbsp;<asp:GridView ID="GridView2" runat="server" OnDataBound="GridView2_DataBound" Visible="False">
</asp:GridView>