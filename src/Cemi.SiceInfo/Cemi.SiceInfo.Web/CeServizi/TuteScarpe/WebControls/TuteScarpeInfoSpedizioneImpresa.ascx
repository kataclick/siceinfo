﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TuteScarpeInfoSpedizioneImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.TuteScarpeInfoSpedizioneImpresa" %>
<table class="borderedTable">
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelRagioneSociale" runat="server" Font-Bold="true"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <!-- Sede Legale -->
    <tr>
        <td colspan="2">
            <b>Sede Legale</b>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo:
        </td>
        <td>
            <asp:Label ID="LabelSedeLegaleIndirizzo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <asp:Label ID="LabelSedeLegaleTelefono" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <asp:Label ID="LabelSedeLegaleFax" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Email:
        </td>
        <td>
            <asp:Label ID="LabelSedeLegaleEmail" runat="server"></asp:Label>
        </td>
    </tr>
    <!-- Sede Legale -->
    <tr>
        <td colspan="2">
            <b>Sede Amministrativa</b>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo:
        </td>
        <td>
            <asp:Label ID="LabelSedeAmministrativaIndirizzo" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Presso:
        </td>
        <td>
            <asp:Label ID="LabelSedeAmministrativaPresso" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <asp:Label ID="LabelSedeAmministrativaTelefono" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <asp:Label ID="LabelSedeAmministrativaFax" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Email:
        </td>
        <td>
            <asp:Label ID="LabelSedeAmministrativaEmail" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
</table>
