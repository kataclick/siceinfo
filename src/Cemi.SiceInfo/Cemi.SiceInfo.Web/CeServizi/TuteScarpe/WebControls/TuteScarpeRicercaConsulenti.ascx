﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TuteScarpeRicercaConsulenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.TuteScarpeRicercaConsulenti" %>
<asp:Panel ID="PanelRicercaConsulenti" runat="server" DefaultButton="ButtonVisualizza" Width="100%">
    <table class="filledtable">
        <tr>
            <td>
                Ricerca Consulente
            </td>
        </tr>
    </table>
    <table class="borderedTable" >
        <tr>
            <td>
                Ragione sociale
            </td>
            <td>
                Indirizzo
            </td>
            <td>
                Comune
            </td>
            <td>
                Codice fiscale
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
            </td>
            <td >
                <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
            </td>
            <td >
                <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
            </td >
            <td>
                <asp:TextBox ID="TextBoxFiscale" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Codice<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="TextBoxCodice" ErrorMessage="*" ValidationExpression="^\d*$"
                    ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td align="right">
                &nbsp;<asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Ricerca" ValidationGroup="ricerca" />&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GridViewConsulenti" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    DataKeyNames="IdConsulente" OnPageIndexChanging="GridViewConsulenti_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewConsulenti_SelectedIndexChanging" Width="100%"
                    PageSize="5">
                    <Columns>
                        <asp:BoundField DataField="IdConsulente" HeaderText="Codice" >
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
                        <asp:BoundField DataField="Indirizzo" HeaderText="Indirizzo" />
                        <asp:BoundField DataField="Provincia" HeaderText="Prov." >
                            <ItemStyle Width="30px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Comune" HeaderText="Comune" >
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" >
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Telefono" HeaderText="Telefono" >
                            <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                            SelectText="Seleziona" ShowSelectButton="True" >
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun consulente trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
