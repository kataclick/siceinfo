﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelezioneTaglie.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.SelezioneTaglie" %>

<%@ Register src="LiberatoriaTuteScarpeAsfaltatori.ascx" tagname="LiberatoriaTuteScarpeAsfaltatori" tagprefix="uc1" %>

I dati inseriti resteranno memorizzati anche se la gestione dell'ordine relativo a tutti i dipendenti non è stata ultimata. 
Si precisa che per salvare la scheda d'ordine di ogni singolo lavoratore è necessario compilare tutti i campi. 

<br />
<br />

Lavoratore:

<b>
    <asp:Label
        ID="LabelLavoratore"
        runat="server">
    </asp:Label>
</b>

<br />
<br />
<b>Legenda taglie</b>
<br />
S corrisponde alla misura 40/42
<br />
M corrisponde alla misura 44/46
<br />
L corrisponde alla misura 48/50
<br />
XL corrisponde alla misura 52/54
<br />
XXL corrisponde alla misura 56/58
<br />
3XL corrisponde alla misura 60/62
<br />
4XL corrisponde alla misura 64/66
<br />
<br />

<asp:CustomValidator
    ID="CustomValidatorSelezioneGruppi"
    runat="server"
    ValidationGroup="salvataggioTaglie"
    ErrorMessage="Selezionare correttamente i gruppi vestiario e le relative taglie prima di salvare" 
    onservervalidate="CustomValidatorSelezioneGruppi_ServerValidate"
    ForeColor="Red">
</asp:CustomValidator>

<telerik:RadGrid
    ID="RadGridSelezioneTaglie0"
    runat="server"
    AutoGenerateColumns="False" 
    GridLines="None"
    ShowHeader="False" 
    AllowMultiRowSelection="False" 
    onitemdatabound="RadGridSelezioneTaglie_ItemDataBound">
<ClientSettings EnableRowHoverStyle="false">
    <Selecting AllowRowSelect="true" />
</ClientSettings>
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>
<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <NoRecordsTemplate>
        &nbsp;
    </NoRecordsTemplate>
    <Columns>
        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
            <ItemStyle Width="50px" />
        </telerik:GridClientSelectColumn>
        <telerik:GridBoundColumn UniqueName="Gruppo" DataField="Descrizione" ItemStyle-CssClass="testoUpperCase">
            <ItemStyle Width="200px" />
        </telerik:GridBoundColumn>
        <telerik:GridTemplateColumn HeaderText="Indumenti" UniqueName="Indumenti">
            <ItemTemplate>
                <telerik:RadGrid
                    ID="RadGridIndumenti"
                    runat="server"
                    AutoGenerateColumns="False" 
                    GridLines="None"
                    ShowHeader="False" 
                    onitemdatabound="RadGridIndumenti_ItemDataBound">
                    <ClientSettings EnableRowHoverStyle="false">
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                    <HeaderContextMenu EnableAutoScroll="True">
                    </HeaderContextMenu>
                    <MasterTableView DataKeyNames="IdIndumento">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Indumento" UniqueName="Indumento" DataField="Descrizione" ItemStyle-CssClass="testoUpperCase">
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Quantita" UniqueName="Quantita" DataField="Quantita" DataFormatString="Qta: {0}">
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Taglia" UniqueName="Taglia">
                                <ItemTemplate>
                                    <telerik:RadComboBox
                                        ID="RadComboBoxTaglia"
                                        runat="server"
                                        AppendDataBoundItems="true"
                                        Width="95%">
                                    </telerik:RadComboBox>
                                    <asp:CustomValidator
                                        ID="CustomValidatorTaglia"
                                        runat="server"
                                        ErrorMessage="Selezionare la taglia corrispondente"
                                        ValidationGroup="salvataggioTaglie" 
                                        onservervalidate="CustomValidatorTaglia_ServerValidate"
                                        ForeColor="Red"
                                        Font-Bold="true">
                                        *
                                    </asp:CustomValidator>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </ItemTemplate>
        </telerik:GridTemplateColumn>
    </Columns>
</MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid
    ID="RadGridSelezioneTaglie1"
    runat="server"
    AutoGenerateColumns="False" 
    GridLines="None"
    ShowHeader="False" 
    AllowMultiRowSelection="False" 
    onitemdatabound="RadGridSelezioneTaglie_ItemDataBound">
<ClientSettings EnableRowHoverStyle="false">
    <Selecting AllowRowSelect="True" />
</ClientSettings>
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>
<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <NoRecordsTemplate>
        &nbsp;
    </NoRecordsTemplate>
    <Columns>
        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
            <ItemStyle Width="50px" />
        </telerik:GridClientSelectColumn>
        <telerik:GridBoundColumn UniqueName="Gruppo" DataField="Descrizione" ItemStyle-CssClass="testoUpperCase">
            <ItemStyle Width="200px" />
        </telerik:GridBoundColumn>
        <telerik:GridTemplateColumn HeaderText="Indumenti" UniqueName="Indumenti">
            <ItemTemplate>
                <telerik:RadGrid
                    ID="RadGridIndumenti"
                    runat="server"
                    AutoGenerateColumns="False" 
                    GridLines="None"
                    ShowHeader="False"
                    onitemdatabound="RadGridIndumenti_ItemDataBound">
                    <ClientSettings EnableRowHoverStyle="false">
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                    <HeaderContextMenu EnableAutoScroll="True">
                    </HeaderContextMenu>
                    <MasterTableView DataKeyNames="IdIndumento">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Indumento" UniqueName="Indumento" DataField="Descrizione" ItemStyle-CssClass="testoUpperCase">
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Quantita" UniqueName="Quantita" DataField="Quantita" DataFormatString="Qta: {0}">
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Taglia" UniqueName="Taglia">
                                <ItemTemplate>
                                    <telerik:RadComboBox
                                        ID="RadComboBoxTaglia"
                                        runat="server"
                                        AppendDataBoundItems="true"
                                        Width="95%">
                                    </telerik:RadComboBox>
                                    <asp:CustomValidator
                                        ID="CustomValidatorTaglia"
                                        runat="server"
                                        ErrorMessage="Selezionare la taglia corrispondente"
                                        ValidationGroup="salvataggioTaglie" 
                                        onservervalidate="CustomValidatorTaglia_ServerValidate"
                                        ForeColor="Red"
                                        Font-Bold="true">
                                        *
                                    </asp:CustomValidator>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </ItemTemplate>
        </telerik:GridTemplateColumn>
    </Columns>
</MasterTableView>
</telerik:RadGrid>

<br />

<telerik:RadGrid
    ID="RadGridSelezioneTaglie2"
    runat="server"
    AutoGenerateColumns="False" 
    GridLines="None"
    ShowHeader="False" 
    AllowMultiRowSelection="False" 
    onitemdatabound="RadGridSelezioneTaglie_ItemDataBound">
<ClientSettings EnableRowHoverStyle="false">
    <Selecting AllowRowSelect="True" />
</ClientSettings>
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

<MasterTableView>
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>
<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <NoRecordsTemplate>
        &nbsp;
    </NoRecordsTemplate>
    <Columns>
        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
            <ItemStyle Width="50px" />
        </telerik:GridClientSelectColumn>
        <telerik:GridBoundColumn UniqueName="Gruppo" DataField="Descrizione" ItemStyle-CssClass="testoUpperCase">
            <ItemStyle Width="200px" />
        </telerik:GridBoundColumn>
        <telerik:GridTemplateColumn HeaderText="Indumenti" UniqueName="Indumenti">
            <ItemTemplate>
                <telerik:RadGrid
                    ID="RadGridIndumenti"
                    runat="server"
                    AutoGenerateColumns="False" 
                    GridLines="None"
                    ShowHeader="False"
                    onitemdatabound="RadGridIndumenti_ItemDataBound">
                    <ClientSettings EnableRowHoverStyle="false">
                        <Selecting AllowRowSelect="false" />
                    </ClientSettings>
                    <HeaderContextMenu EnableAutoScroll="True">
                    </HeaderContextMenu>
                    <MasterTableView DataKeyNames="IdIndumento">
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Indumento" UniqueName="Indumento" DataField="Descrizione" ItemStyle-CssClass="testoUpperCase">
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Quantita" UniqueName="Quantita" DataField="Quantita" DataFormatString="Qta: {0}">
                                <ItemStyle Width="50px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Taglia" UniqueName="Taglia">
                                <ItemTemplate>
                                    <telerik:RadComboBox
                                        ID="RadComboBoxTaglia"
                                        runat="server"
                                        AppendDataBoundItems="true"
                                        Width="95%"
                                        EmptyMessage="Selezionare la taglia">
                                    </telerik:RadComboBox>
                                    <asp:CustomValidator
                                        ID="CustomValidatorTaglia"
                                        runat="server"
                                        ErrorMessage="Selezionare la taglia corrispondente"
                                        ValidationGroup="salvataggioTaglie" 
                                        onservervalidate="CustomValidatorTaglia_ServerValidate"
                                        ForeColor="Red"
                                        Font-Bold="true">
                                        *
                                    </asp:CustomValidator>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                </telerik:RadGrid>
            </ItemTemplate>
        </telerik:GridTemplateColumn>
    </Columns>
</MasterTableView>
</telerik:RadGrid>

<asp:ValidationSummary
    ID="ValidationSummaryErrori"
    runat="server"
    ValidationGroup="salvataggioTaglie"
    CssClass="messaggiErrore" />

<br />

<asp:Button
    ID="ButtonSalvaProssimo"
    runat="server"
    Text="Salva e passa al prossimo" 
    onclick="ButtonSalvaProssimo_Click"
    ValidationGroup="salvataggioTaglie" />
    
<asp:Button
    ID="ButtonPrecedente"
    runat="server"
    Text="Precedente" onclick="ButtonPrecedente_Click" />
    
<asp:Button
    ID="ButtonSuccessivo"
    runat="server"
    Text="Successivo" onclick="ButtonSuccessivo_Click" />
    
<asp:Button
    ID="ButtonTornaElenco"
    runat="server"
    Text="Torna all'elenco" onclick="ButtonTornaElenco_Click" />
    
<br />
<br />
<uc1:LiberatoriaTuteScarpeAsfaltatori ID="LiberatoriaTuteScarpeAsfaltatori" 
    runat="server" />
<center>
    <asp:Button ID="ButtonConfermaAsfaltatori" runat="server" OnClick="ButtonConfermaAsfaltatori_Click"
    Text="Conferma" Width="164px" />
</center>