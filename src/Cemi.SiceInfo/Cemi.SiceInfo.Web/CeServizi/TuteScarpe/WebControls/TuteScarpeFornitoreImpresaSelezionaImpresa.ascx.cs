﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Telerik.Web.UI;
using Fornitore = TBridge.Cemi.Type.Entities.GestioneUtenti.Fornitore;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls
{
    public partial class TuteScarpeFornitoreImpresaSelezionaImpresa : System.Web.UI.UserControl
    {
        private readonly TSBusiness biz = new TSBusiness();
        private const int COMPILA = 6;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (GestioneUtentiBiz.IsFornitore())
            {
                if (!Page.IsPostBack)
                {
                    CaricaFabbisogni();
                    //Primo ciclo non visualizziamo il dettaglio
                    TuteScarpeInfoSpedizioneImpresa1.Visible = false;
                }
            }
        }

        private void CaricaFabbisogni()
        {
            int? codice = null;
            string ragioneSociale = null;

            if (!string.IsNullOrEmpty(TextBoxCodice.Text))
                codice = Int32.Parse(TextBoxCodice.Text);
            ragioneSociale = TextBoxRagioneSociale.Text;

            Fornitore fornitore =
                (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            RapportoFornitoreImpresaCollection rapporti = biz.GetFornitoreRapportiImpresa(fornitore.IdFornitore, true,
                                                                                          codice, ragioneSociale);

            //TODO da modificare assolutamente! filtra in memoria invece che su sql
            RapportoFornitoreImpresaCollection rappportiFiltrati = FiltraRapporti(rapporti);
            RadGridImpreseFornitore.DataSource = rappportiFiltrati;
            RadGridImpreseFornitore.DataBind();
        }

        protected void RadGridImpreseFornitore_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                RapportoFornitoreImpresa rapporto = (RapportoFornitoreImpresa)e.Item.DataItem;
                Label labelStato = (Label)e.Item.FindControl("LabelStato");

                //Controlliamo se esiste un fabbisogno per l'impresa. Controlliamo l'esistenza di almeno una riga ordine
                if (biz.EsisteFabbisogno(rapporto.IdImpresa))
                {
                    if (rapporto.NumeroLavoratori <= 0)
                    {
                        e.Item.Cells[COMPILA].Enabled = false;
                        labelStato.Text = "L'ordine di fabbisogno del mese corrente è già stato confermato.";
                    }
                }
                else
                {
                    e.Item.Cells[COMPILA].Enabled = false;
                    labelStato.Text = "Non risultano soddisfatti i requisiti di accantonamento orario previsti.";
                }
            }
        }

        /// <summary>
        /// Storico
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void RadGridImpreseFornitore_EditCommand(object source, GridCommandEventArgs e)
        {
            Int32 idImpresa = (Int32)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
            Int32 idFornitore = (Int32)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdFornitore"];

            biz.SetImpresaSelezionataFornitore(idFornitore, idImpresa);
            Response.Redirect("~/CeServizi/TuteScarpe/VisualizzaStorico.aspx");
        }
        /// <summary>
        /// Compila Ordine
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void RadGridImpreseFornitore_DeleteCommand(object source, GridCommandEventArgs e)
        {
            Int32 idImpresa = (Int32)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
            Int32 idFornitore = (Int32)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdFornitore"];

            biz.SetImpresaSelezionataFornitore(idFornitore, idImpresa);
            Response.Redirect("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
        }

        protected void RadGridImpreseFonitore_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaFabbisogni();
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            TuteScarpeInfoSpedizioneImpresa1.Reset();
            TuteScarpeInfoSpedizioneImpresa1.Visible = false;
            CaricaFabbisogni();
        }

        //TODO da valutare cosa fa questo metodo e come migliorarlo
        private RapportoFornitoreImpresaCollection FiltraRapporti(RapportoFornitoreImpresaCollection rapporti)
        {
            RapportoFornitoreImpresaCollection rapportiFiltrati = new RapportoFornitoreImpresaCollection();

            if (DropDownListStato.SelectedValue == "TUTTE")
            {
                rapportiFiltrati = rapporti;
            }

            switch (DropDownListStato.SelectedValue)
            {
                case "TUTTE":
                    rapportiFiltrati = rapporti;
                    break;
                default:
                    foreach (RapportoFornitoreImpresa rapporto in rapporti)
                    {
                        if (biz.EsisteFabbisogno(rapporto.IdImpresa))
                        {
                            // Purtroppo era così
                            DataTable dtLavoratori = biz.ElencoLavoratoriImpresa(rapporto.IdImpresa, null);
                            if (dtLavoratori != null && dtLavoratori.Rows.Count > 0)
                            {
                                if (DropDownListStato.SelectedValue == "DACOMPILARE" ||
                                    DropDownListStato.SelectedValue == "TUTTE")
                                    rapportiFiltrati.Add(rapporto);
                            }
                            else
                            {
                                if (DropDownListStato.SelectedValue == "GIACONFERMATE" ||
                                    DropDownListStato.SelectedValue == "TUTTE")
                                    rapportiFiltrati.Add(rapporto);
                            }
                        }
                        else
                        {
                            if (DropDownListStato.SelectedValue == "NOORE" || DropDownListStato.SelectedValue == "TUTTE")
                                rapportiFiltrati.Add(rapporto);
                        }
                    }
                    break;
            }

            return rapportiFiltrati;
        }

        protected void RadGridImpreseFornitore_ItemCommand(object source, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Dettagli":
                    Int32 idImpresa = (Int32)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
                    String ragioneSociale = (String)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["RagioneSocialeImpresa"];
                    Indirizzo indirizzoSedeLegale = (Indirizzo)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["SedeLegale"];
                    Indirizzo indirizzoSedeAmministrativa = (Indirizzo)RadGridImpreseFornitore.MasterTableView.DataKeyValues[e.Item.ItemIndex]["SedeAmministrativa"];

                    TuteScarpeInfoSpedizioneImpresa1.Visible = true;
                    TuteScarpeInfoSpedizioneImpresa1.CaricaDatiImpresa(ragioneSociale, indirizzoSedeLegale, indirizzoSedeAmministrativa);
                    break;
            }
        }
        protected void RadGridImpreseFornitore_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaFabbisogni();
        }
    }
}