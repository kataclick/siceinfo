﻿using System;
using System.Data;
using System.Web.UI;
using TBridge.Cemi.TuteScarpe.Business;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls
{
    public partial class stampaGruppi : System.Web.UI.UserControl
    {
        private readonly TSBusiness tsBusiness = new TSBusiness();
        //private List<GruppiRadio> grpRadio;

        protected void Page_Load(object sender, EventArgs e)
        {
            //int impresa = Int32.Parse(Request.QueryString["idImpresa"]);

            if (Context.Items["IdImpresa"] == null)
            {
                Response.Redirect("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
            }
            Int32 impresa = (Int32)Context.Items["IdImpresa"];

            //stampaReport(impresa);
            AnteprimaReport(impresa);

        }

        private void AnteprimaReport(int impresa)
        {
            DataTable gruppi = tsBusiness.getGruppiForPrint(impresa);
            GridView1.DataSource = gruppi;
            GridView1.DataBind();

            DataTable legendaTaglie = tsBusiness.getLegendaTaglie();
            //GridView2.DataSource = legendaTaglie;
            //GridView2.DataBind();
            //GridView2.Visible = true;
            StampaLegendaOrizzontale(legendaTaglie);
        }

        private void StampaLegendaOrizzontale(DataTable legenda)
        {
            string nomeVest = string.Empty;

            for (int j = 0; j < legenda.Rows.Count; j++)
            {
                DataRow row = legenda.Rows[j];

                if (row.ItemArray[0].ToString() != nomeVest)
                {
                    if (j != 0)
                    {
                        Controls.Add(new LiteralControl("</tr>"));
                        Controls.Add(new LiteralControl("</table>"));
                        //this.Controls.Add(new LiteralControl("<br>"));
                    }

                    nomeVest = row.ItemArray[0].ToString();
                    Controls.Add(new LiteralControl("<table border=\"1px\">"));
                    Controls.Add(new LiteralControl("<tr>"));
                    Controls.Add(new LiteralControl("<td width=\"200px\">" + nomeVest.Trim() + "</td>"));
                }

                Controls.Add(new LiteralControl("<td>"));
                Controls.Add(new LiteralControl(row.ItemArray[1].ToString()));
                Controls.Add(new LiteralControl("</td>"));
            }

            Controls.Add(new LiteralControl("</tr>"));
            Controls.Add(new LiteralControl("</table>"));
        }

        /*
            private void stampaReport(int impresa)
            {
                DataTable gruppi = tsBusiness.getGruppi(impresa);
                string lastUtente = "";
                string lastGruppo = "";
                string lastIndumento = "";

                //bool opentable = false;
                //bool openrow = false;

                Response.Write(
                    "<b><table width=\"100%\" border=0><tr><td width=\"50px\">Cod lav.</td><td width=\"100px\">Nome Lavoratore</td><td width=\"120px\">Codice Fiscale</td><td width=\"50px\">Data Nascita</td><td width=\"150px\">Tipo gruppo</td><td width=\"10px\">Gr.Sel.</td><td width=\"150px\">Indumento</td><td width=\"20px\">Tg.</td><td width=\"10px\"></td></tr></table></b>");

                for (int i = 0; i < gruppi.Rows.Count; i++)
                {
                    if (lastUtente != gruppi.Rows[i]["idLAvoratore"].ToString())
                    {
                        if (i != 0)
                        {
                            //
                            Response.Write("</table>");
                        }

                        Response.Write("<table width=\"100%\" border=1 cellpadding=1 cellspacing=1>");
                        Response.Write("<tr>");

                        Response.Write("<td width=\"50px\">");
                        lastUtente = gruppi.Rows[i]["idLAvoratore"].ToString();
                        lastGruppo = "";
                        lastIndumento = "";
                        Response.Write(lastUtente);
                        Response.Write("</td>");

                        Response.Write("<td width=\"100px\">");
                        Response.Write(gruppi.Rows[i]["nomeLavoratore"].ToString());
                        Response.Write("</td>");

                        //Response.Write("<td width=\"120px\">");
                        //Response.Write(gruppi.Rows[i]["codiceFiscale"].ToString());
                        //Response.Write("</td>");

                        Response.Write("<td width=\"50px\">");
                        Response.Write(gruppi.Rows[i]["dataNascita"].ToString());
                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<tr>");

                        Response.Write("<td>");
                        Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("</td>");

                        //Response.Write("<td>");
                        //Response.Write("</td>");

                        Response.Write("<td>");
                        Response.Write("</td>");
                    }

                    if (lastGruppo != gruppi.Rows[i]["nomeGruppo"].ToString())
                    {
                        Response.Write("<td width=\"150px\">");
                        lastGruppo = gruppi.Rows[i]["nomeGruppo"].ToString();
                        lastIndumento = "";
                        Response.Write(lastGruppo);
                        Response.Write("</td>");

                        Response.Write("<td width=\"10px\">");
                        if (gruppi.Rows[i]["selezionato"].ToString() == "True")
                        {
                            Response.Write("X");
                        }
                        else
                            Response.Write("");

                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<td width=\"150px\">");
                        Response.Write("</td>");
                        Response.Write("<td width=\"10px\">");
                        Response.Write("</td>");
                    }

                    if (lastIndumento != gruppi.Rows[i]["idIndumento"].ToString())
                    {
                        Response.Write("<td width=\"150px\">");
                        lastIndumento = gruppi.Rows[i]["idIndumento"].ToString();
                        Response.Write(gruppi.Rows[i]["descrizione"].ToString());
                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<td width=\"150px\">");
                        Response.Write("</td>");
                    }

                    if (gruppi.Rows[i]["tagliaSelezionata"].ToString() == "1")
                    {
                        Response.Write("<td width=\"20px\">");
                        Response.Write(gruppi.Rows[i]["taglia"].ToString());
                        Response.Write("</td>");

                        Response.Write("<td width=\"10px\">");
                        Response.Write("X");
                        Response.Write("</td>");
                    }
                    else
                    {
                        Response.Write("<td width=\"20px\">");
                        Response.Write(""); //Response.Write(gruppi.Rows[i]["taglia"].ToString());
                        Response.Write("</td>");

                        Response.Write("<td width=\"10px\">");
                        Response.Write("");
                        Response.Write("</td>");
                    }

                    Response.Write("</tr>");
                }
                Response.Write("</table>");
            }
        */

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            string idLav = GridView1.Rows[0].Cells[0].Text;
            string nomeGruppo = GridView1.Rows[0].Cells[4].Text;

            for (int i = 1; i < GridView1.Rows.Count; i++)
            {
                if (((DataTable)GridView1.DataSource).Rows[i].ItemArray[6].ToString() == "False")
                {
                    GridView1.Rows[i].Cells[8].Text = "";
                }

                if (GridView1.Rows[i].Cells[0].Text == idLav)
                {
                    GridView1.Rows[i].Cells[0].Text = "";
                    GridView1.Rows[i].Cells[1].Text = "";
                    GridView1.Rows[i].Cells[2].Text = "";
                    GridView1.Rows[i].Cells[3].Text = "";
                }
                else
                {
                    idLav = GridView1.Rows[i].Cells[0].Text;
                    nomeGruppo = GridView1.Rows[i].Cells[4].Text;
                    continue;
                }

                if (GridView1.Rows[i].Cells[4].Text == nomeGruppo)
                {
                    GridView1.Rows[i].Cells[4].Text = "";
                }
                else
                {
                    nomeGruppo = GridView1.Rows[i].Cells[4].Text;
                }
            }
        }

        protected void GridView2_DataBound(object sender, EventArgs e)
        {
            string articolo = GridView2.Rows[0].Cells[0].Text;

            for (int i = 1; i < GridView2.Rows.Count; i++)
            {
                if (GridView2.Rows[i].Cells[0].Text == articolo)
                {
                    GridView2.Rows[i].Cells[0].Text = "";
                }
                else
                {
                    articolo = GridView2.Rows[i].Cells[0].Text;
                }
            }
        }
    }
}