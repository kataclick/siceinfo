﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls
{
    public partial class SelezioneTaglie : System.Web.UI.UserControl
    {
        private readonly TSBusiness biz = new TSBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ViewState["IdImpresa"] == null
                    || ViewState["IdLavoratore"] == null)
                {
                    throw new Exception("SelezioneTaglie: Impresa e Lavoratore non selezionati.");
                }
            }
        }

        public void CaricaFabbisognoLavoratore(Int32 idImpresa, Int32 idLavoratore)
        {
            ViewState["IdImpresa"] = idImpresa;
            ViewState["IdLavoratore"] = idLavoratore;
            FabbisognoLavoratore fabbisogno = biz.GetFabbisognoLavoratore(idImpresa, idLavoratore);

            GestisciLiberatoriaAsfaltisti(fabbisogno);

            LabelLavoratore.Text = String.Format("{0} - {1} {2}",
                                                 fabbisogno.IdLavoratore,
                                                 fabbisogno.Cognome,
                                                 fabbisogno.Nome);

            GruppoFabbisognoLavoratoreCollection primoGruppo = new GruppoFabbisognoLavoratoreCollection();
            GruppoFabbisognoLavoratoreCollection secondoGruppo = new GruppoFabbisognoLavoratoreCollection();
            GruppoFabbisognoLavoratoreCollection terzoGruppo = new GruppoFabbisognoLavoratoreCollection();

            biz.SeparaGruppiVestiario(fabbisogno, primoGruppo, secondoGruppo, terzoGruppo,
                                      fabbisogno.ImpresaAsfaltista || fabbisogno.ForzatoAsfaltista);

            if (primoGruppo.Count > 0)
            {
                Presenter.CaricaElementiInGridView(
                    RadGridSelezioneTaglie0,
                    primoGruppo);
            }
            else
            {
                RadGridSelezioneTaglie0.Visible = false;
            }

            if (secondoGruppo.Count > 0)
            {
                Presenter.CaricaElementiInGridView(
                    RadGridSelezioneTaglie1,
                    secondoGruppo);
            }
            else
            {
                RadGridSelezioneTaglie1.Visible = false;
            }

            if (terzoGruppo.Count > 0)
            {
                Presenter.CaricaElementiInGridView(
                    RadGridSelezioneTaglie2,
                    terzoGruppo);
            }
            else
            {
                RadGridSelezioneTaglie2.Visible = false;
            }
        }

        private void GestisciLiberatoriaAsfaltisti(FabbisognoLavoratore fabbisogno)
        {
            LiberatoriaTuteScarpeAsfaltatori.RagioneSocialeImpresa = fabbisogno.RagioneSociale;

            if (fabbisogno.ImpresaAsfaltista || fabbisogno.ForzatoAsfaltista)
            {
                LiberatoriaTuteScarpeAsfaltatori.Visible = false;
                ButtonConfermaAsfaltatori.Visible = false;
            }
        }

        protected void RadGridSelezioneTaglie_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GruppoFabbisognoLavoratore gruppo = (GruppoFabbisognoLavoratore)e.Item.DataItem;
                RadGrid rgSelezioneTaglie = (RadGrid)e.Item.FindControl("RadGridIndumenti");

                if (gruppo.Selezionato)
                {
                    e.Item.Selected = true;
                }

                Presenter.CaricaElementiInGridView(
                    rgSelezioneTaglie,
                    gruppo.Dettagli);
            }
        }

        protected void ButtonTornaElenco_Click(object sender, EventArgs e)
        {
            Server.Transfer("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
        }

        protected void ButtonSuccessivo_Click(object sender, EventArgs e)
        {
            Int32 idImpresa = (Int32)ViewState["IdImpresa"];
            Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];

            Int32 idLavoratoreSuccessivo = biz.GetLavoratoreSuccessivo(idLavoratore, idImpresa);
            Context.Items["IdImpresa"] = idImpresa;
            Context.Items["IdLavoratore"] = idLavoratoreSuccessivo;
            Server.Transfer("~/CeServizi/TuteScarpe/DettaglioLavoratore.aspx");
        }

        protected void ButtonPrecedente_Click(object sender, EventArgs e)
        {
            Int32 idImpresa = (Int32)ViewState["IdImpresa"];
            Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];

            Int32 idLavoratorePrecedente = biz.GetLavoratorePrecedente(idLavoratore, idImpresa);
            Context.Items["IdImpresa"] = idImpresa;
            Context.Items["IdLavoratore"] = idLavoratorePrecedente;
            Server.Transfer("~/CeServizi/TuteScarpe/DettaglioLavoratore.aspx");
        }

        protected void ButtonSalvaProssimo_Click(object sender, EventArgs e)
        {
            Page.Validate("salvataggioTaglie");

            // Se tutto a posto salvo le taglie
            if (Page.IsValid)
            {
                Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
                Int32 idImpresa = (Int32)ViewState["IdImpresa"];

                DettaglioGruppoFabbisognoLavoratoreCollection dettagli = CreaDettagli();
                biz.SalvaFabbisognoLavoratore(idLavoratore, idImpresa, dettagli);

                ButtonSuccessivo_Click(sender, e);
            }
        }

        private DettaglioGruppoFabbisognoLavoratoreCollection CreaDettagli()
        {
            DettaglioGruppoFabbisognoLavoratoreCollection dettagli = new DettaglioGruppoFabbisognoLavoratoreCollection();

            foreach (GridDataItem gruppo in RadGridSelezioneTaglie0.MasterTableView.Items)
            {
                if (gruppo.Selected)
                {
                    RadGrid rgIndumenti = (RadGrid)gruppo.FindControl("RadGridIndumenti");

                    foreach (GridDataItem indumento in rgIndumenti.MasterTableView.Items)
                    {
                        DettaglioGruppoFabbisognoLavoratore dettaglio = new DettaglioGruppoFabbisognoLavoratore();
                        dettagli.Add(dettaglio);

                        dettaglio.IdIndumento = (String)indumento.GetDataKeyValue("IdIndumento");

                        RadComboBox rcbTaglia = (RadComboBox)indumento.FindControl("RadComboBoxTaglia");
                        dettaglio.TagliaSelezionata = rcbTaglia.SelectedItem.Value;
                    }
                }
            }

            foreach (GridDataItem gruppo in RadGridSelezioneTaglie1.MasterTableView.Items)
            {
                if (gruppo.Selected)
                {
                    RadGrid rgIndumenti = (RadGrid)gruppo.FindControl("RadGridIndumenti");

                    foreach (GridDataItem indumento in rgIndumenti.MasterTableView.Items)
                    {
                        DettaglioGruppoFabbisognoLavoratore dettaglio = new DettaglioGruppoFabbisognoLavoratore();
                        dettagli.Add(dettaglio);

                        dettaglio.IdIndumento = (String)indumento.GetDataKeyValue("IdIndumento");

                        RadComboBox rcbTaglia = (RadComboBox)indumento.FindControl("RadComboBoxTaglia");
                        dettaglio.TagliaSelezionata = rcbTaglia.SelectedItem.Value;
                    }
                }
            }

            foreach (GridDataItem gruppo in RadGridSelezioneTaglie2.MasterTableView.Items)
            {
                if (gruppo.Selected)
                {
                    RadGrid rgIndumenti = (RadGrid)gruppo.FindControl("RadGridIndumenti");

                    foreach (GridDataItem indumento in rgIndumenti.MasterTableView.Items)
                    {
                        DettaglioGruppoFabbisognoLavoratore dettaglio = new DettaglioGruppoFabbisognoLavoratore();
                        dettagli.Add(dettaglio);

                        dettaglio.IdIndumento = (String)indumento.GetDataKeyValue("IdIndumento");

                        RadComboBox rcbTaglia = (RadComboBox)indumento.FindControl("RadComboBoxTaglia");
                        dettaglio.TagliaSelezionata = rcbTaglia.SelectedItem.Value;
                    }
                }
            }

            return dettagli;
        }

        protected void ButtonConfermaAsfaltatori_Click(object sender, EventArgs e)
        {
            Int32 idImpresa = (Int32)ViewState["IdImpresa"];
            Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
            //Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            Int32 idUtente = biz.GetIdUtenteByIdImpresa(idImpresa);

            biz.MemorizzaForzaAsfaltista(idUtente);
            CaricaFabbisognoLavoratore(idImpresa, idLavoratore);
        }

        protected void RadGridIndumenti_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                DettaglioGruppoFabbisognoLavoratore dettaglio = (DettaglioGruppoFabbisognoLavoratore)e.Item.DataItem;
                RadComboBox rcbTaglie = (RadComboBox)e.Item.FindControl("RadComboBoxTaglia");

                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    rcbTaglie,
                    dettaglio.Taglie,
                    "",
                    "");

                if (!String.IsNullOrEmpty(dettaglio.TagliaSelezionata))
                {
                    rcbTaglie.SelectedValue = dettaglio.TagliaSelezionata;
                }
            }
        }

        protected void CustomValidatorSelezioneGruppi_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if ((RadGridSelezioneTaglie0.Items.Count > 0
                 && RadGridSelezioneTaglie0.SelectedIndexes.Count == 0)
                ||
                (RadGridSelezioneTaglie1.Items.Count > 0
                 && RadGridSelezioneTaglie1.SelectedIndexes.Count == 0)
                ||
                (RadGridSelezioneTaglie2.Items.Count > 0
                 && RadGridSelezioneTaglie2.SelectedIndexes.Count == 0))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorTaglia_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            // Devo capire se il gruppo di cui fa parte è stato selezionato.
            // Per farlo devo risalire al RadGrid iniziale che lo contiene
            Boolean gruppoSelezionato =
                ((GridDataItem)(((CustomValidator)source).NamingContainer.NamingContainer.NamingContainer.NamingContainer))
                    .Selected;

            // Recupero la combo di selezione della taglia
            RadComboBox rcbTaglia =
                (RadComboBox)((CustomValidator)source).NamingContainer.FindControl("RadComboBoxTaglia");

            if (gruppoSelezionato &&
                (rcbTaglia.SelectedItem == null || String.IsNullOrEmpty(rcbTaglia.SelectedItem.Value)))
            {
                args.IsValid = false;
            }
        }
    }
}