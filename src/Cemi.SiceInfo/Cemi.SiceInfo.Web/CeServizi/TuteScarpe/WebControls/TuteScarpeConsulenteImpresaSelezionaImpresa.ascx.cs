﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using Telerik.Web.UI;


namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls
{
    public partial class TuteScarpeConsulenteImpresaSelezionaImpresa : System.Web.UI.UserControl
    {
        private readonly TSBusiness biz = new TSBusiness();
        private const int COMPILA = 5;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (GestioneUtentiBiz.IsConsulente())
            {
                if (!Page.IsPostBack)
                {
                    CaricaFabbisogni();
                }
            }
        }

        private void CaricaFabbisogni()
        {
            Consulente consulente =
                (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

            RadGridImpreseConsulente.DataSource = biz.GetConsulenteRapportiImpresa(consulente.IdConsulente, true);
            RadGridImpreseConsulente.DataBind();
        }

        protected void RadGridImpreseConsulente_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                RapportoConsulenteImpresa rapporto = (RapportoConsulenteImpresa)e.Item.DataItem;
                Label labelStato = (Label)e.Item.FindControl("LabelStato");

                //Controlliamo se esiste un fabbisogno per l'impresa. Controlliamo l'esistenza di almeno una riga ordine
                if (biz.EsisteFabbisogno(rapporto.IdImpresa))
                {
                    // Purtroppo era così

                    if (rapporto.NumeroLavoratori <= 0)
                    {
                        e.Item.Cells[COMPILA].Enabled = false;
                        labelStato.Text = "L'ordine di fabbisogno del mese corrente è già stato confermato.";
                    }
                }
                else
                {
                    e.Item.Cells[COMPILA].Enabled = false;
                    labelStato.Text = "Non risultano soddisfatti i requisiti di accantonamento orario previsti.";
                }
            }
        }

        /// <summary>
        /// Storico
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void RadGridImpreseConsulente_EditCommand(object source, GridCommandEventArgs e)
        {
            Int32 idImpresa = (Int32)RadGridImpreseConsulente.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
            Int32 idConsulente = (Int32)RadGridImpreseConsulente.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdConsulente"];

            biz.SetImpresaSelezionataConsulente(idConsulente, idImpresa);
            Response.Redirect("~/CeServizi/TuteScarpe/VisualizzaStorico.aspx");
        }
        /// <summary>
        /// Compila Ordine
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void RadGridImpreseConsulente_DeleteCommand(object source, GridCommandEventArgs e)
        {
            Int32 idImpresa = (Int32)RadGridImpreseConsulente.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdImpresa"];
            Int32 idConsulente = (Int32)RadGridImpreseConsulente.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdConsulente"];

            biz.SetImpresaSelezionataConsulente(idConsulente, idImpresa);
            Response.Redirect("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
        }
        protected void RadGridImpreseConsulente_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaFabbisogni();
        }
    }
}