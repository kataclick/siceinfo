﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TuteScarpeConsulenteImpresaSelezionaImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.TuteScarpeConsulenteImpresaSelezionaImpresa" %>
<telerik:RadGrid ID="RadGridImpreseConsulente" runat="server" 
    AutoGenerateColumns="False" GridLines="None" AllowPaging="True" 
    ondeletecommand="RadGridImpreseConsulente_DeleteCommand" 
    oneditcommand="RadGridImpreseConsulente_EditCommand" 
    onitemdatabound="RadGridImpreseConsulente_ItemDataBound" 
    onpageindexchanged="RadGridImpreseConsulente_PageIndexChanged">
<HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>

<MasterTableView DataKeyNames="IdImpresa,IdConsulente">
<RowIndicatorColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn>
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>
    <Columns>
        <telerik:GridBoundColumn DataField="IdImpresa" HeaderText="Cod.Impresa" 
            UniqueName="columnIdImpresa">
            <ItemStyle Width="70px" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="RagioneSocialeImpresa" 
            HeaderText="Ragione sociale" UniqueName="columnRagioneSociale">
        </telerik:GridBoundColumn>
        <telerik:GridTemplateColumn UniqueName="TemplateColumnStato" HeaderText="Stato del fabbisogno" >
            <ItemTemplate>
                <asp:Label ID="LabelStato" runat="server"></asp:Label>
            </ItemTemplate>
        </telerik:GridTemplateColumn>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Delete" 
            Text="Compila" UniqueName="columnCompila" ButtonCssClass="bottoneGrigliaTuteScarpe">
            <ItemStyle Width="60px" />
        </telerik:GridButtonColumn>
        <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Edit" 
            Text="Storico" UniqueName="columnStorico" ButtonCssClass="bottoneGrigliaTuteScarpe">
            <ItemStyle Width="60px" />
        </telerik:GridButtonColumn>
    </Columns>
</MasterTableView>

    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
</telerik:RadGrid>