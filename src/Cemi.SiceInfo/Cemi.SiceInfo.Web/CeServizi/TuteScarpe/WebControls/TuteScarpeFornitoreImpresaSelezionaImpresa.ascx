﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TuteScarpeFornitoreImpresaSelezionaImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.WebControls.TuteScarpeFornitoreImpresaSelezionaImpresa" %>
<%@ Register src="TuteScarpeInfoSpedizioneImpresa.ascx" tagname="TuteScarpeInfoSpedizioneImpresa" tagprefix="uc1" %>
<table class="standardTable">
<tr>
<td colspan="2">
    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Filtro ricerca"></asp:Label>
    <br /></td></tr>
    <tr>
        <td>
            Codice
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodice" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxCodice"
                Operator="GreaterThan" Type="Integer" ValidationGroup="ricerca" ValueToCompare="0">Formato codice errato</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ragione Sociale
        </td>
        <td>
            <asp:TextBox ID="TextBoxRagioneSociale" runat="server" Width="300px" MaxLength="255"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Stato
        </td>
        <td>
            <asp:DropDownList ID="DropDownListStato" runat="server" Width="300px">
                <asp:ListItem Value="TUTTE">Tutte</asp:ListItem>
                <asp:ListItem Value="NOORE">Senza accantonamento orario</asp:ListItem>
                <asp:ListItem Value="GIACONFERMATE">Gi&#224; confermate</asp:ListItem>
                <asp:ListItem Value="DACOMPILARE">Da compilare</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="ButtonRicerca" runat="server" Text="Cerca" ValidationGroup="ricerca"
                OnClick="ButtonRicerca_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="False" Text="Imprese associate al fornitore"></asp:Label>&nbsp;
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridImpreseFornitore" runat="server" AutoGenerateColumns="False"
    GridLines="None" AllowPaging="True" OnDeleteCommand="RadGridImpreseFornitore_DeleteCommand"
    OnEditCommand="RadGridImpreseFornitore_EditCommand" OnItemDataBound="RadGridImpreseFornitore_ItemDataBound"
    OnPageIndexChanged="RadGridImpreseFornitore_PageIndexChanged" 
    onitemcommand="RadGridImpreseFornitore_ItemCommand">
    <HeaderContextMenu EnableAutoScroll="True">
    </HeaderContextMenu>
    <MasterTableView DataKeyNames="IdImpresa,IdFornitore,RagioneSocialeImpresa,SedeLegale,SedeAmministrativa">
        <RowIndicatorColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>
        <ExpandCollapseColumn>
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="IdImpresa" HeaderText="Codice" UniqueName="columnIdImpresa">
                <ItemStyle Width="50px" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="RagioneSocialeImpresa" HeaderText="Ragione sociale"
                UniqueName="columnRagioneSociale">
                <ItemStyle Width="200px" />
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="TemplateColumnStato" HeaderText="Stato del fabbisogno">
                <ItemTemplate>
                    <asp:Label ID="LabelStato" runat="server"></asp:Label>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Dettagli" 
                Text="Contatti" UniqueName="columnDettagli" ButtonCssClass="bottoneGrigliaTuteScarpe">
                <ItemStyle Width="10px" />
            </telerik:GridButtonColumn>
            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Delete" Text="Compila"
                UniqueName="columnCompila" ButtonCssClass="bottoneGrigliaTuteScarpe">
                <ItemStyle Width="10px" />
            </telerik:GridButtonColumn>
            <telerik:GridButtonColumn ButtonType="PushButton" CommandName="Edit" Text="Storico"
                UniqueName="columnStorico" ButtonCssClass="bottoneGrigliaTuteScarpe">
                <ItemStyle Width="10px" />
            </telerik:GridButtonColumn>
        </Columns>
    </MasterTableView>
    <ClientSettings EnableRowHoverStyle="false">
        <Selecting AllowRowSelect="false" />
    </ClientSettings>
</telerik:RadGrid>
<br />
<uc1:TuteScarpeInfoSpedizioneImpresa ID="TuteScarpeInfoSpedizioneImpresa1" 
    runat="server" />
