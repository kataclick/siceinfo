﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeStatisticheFabbisogniConfermati : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeStatistiche);
        }

        protected void ReportViewerFabbisogniConfermati_Init(object sender, EventArgs e)
        {
            ReportViewerFabbisogniConfermati.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            //ReportViewerFabbisogniConfermati.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportFabbisogniConfermati";
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
        }

        protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
        {
            ReportViewerFabbisogniConfermati.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportFabbisogniConfermati";

            List<ReportParameter> listaParam = new List<ReportParameter>();
            if (RadDatePickerDataDa.SelectedDate != null)
            {
                ReportParameter param1 = new ReportParameter("dalPadre",
                                             RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param1);
            }
            if (RadDatePickerDataA.SelectedDate != null)
            {
                ReportParameter param2 = new ReportParameter("alPadre", RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param2);
            }

            ReportViewerFabbisogniConfermati.ServerReport.SetParameters(listaParam.ToArray());
        }
    }
}