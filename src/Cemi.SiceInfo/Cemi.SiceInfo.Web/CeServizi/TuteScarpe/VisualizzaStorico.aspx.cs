﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class VisualizzaStorico : System.Web.UI.Page
    {
        private const int COLONNABOTTONE = 7;
        private readonly TSBusiness tsBiz = new TSBusiness();
        private int idImpresa;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);

            if (GestioneUtentiBiz.IsImpresa())
            {
                Impresa impresaEnt =
                    (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                idImpresa = impresaEnt.IdImpresa;
            }
            else if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente = (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                idImpresa = tsBiz.GetImpresaSelezionataConsulente(consulente.IdConsulente).IdImpresa;
            }
            else if (GestioneUtentiBiz.IsFornitore())
            {
                Fornitore fornitore =
                    (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                //prendiamo da db l'idImpresa selezionata dal fornitore
                idImpresa = tsBiz.GetImpresaSelezionataFornitore(fornitore.IdFornitore).IdImpresa;
            }

            if (!Page.IsPostBack)
            {
                CaricaStorico();
                CaricaFabbisogni();
            }
        }

        private void CaricaStorico()
        {
            Presenter.CaricaElementiInGridView(
                RadGridStorico,
                tsBiz.GetStorico(idImpresa));
        }

        private void CaricaFabbisogni()
        {
            Presenter.CaricaElementiInGridView(
                RadGridFabbisogniNonGestiti,
                tsBiz.GetFabbisogniNonGestiti(idImpresa));
        }

        private void CaricaDettaglioFabbisogno(Int32 idFabbisogno)
        {
            LabelDettagliStorico.Text = "Dettagli fabbisogno confermato";
            LabelDettagliStorico.Visible = true;

            DataTable dt =
                tsBiz.GetFabbisogniConfermatiDettagliComeXML(idFabbisogno, idImpresa);

            if (dt != null && dt.Rows.Count > 0)
            {
                ButtonStampa.Visible = true;
                ButtonExcel.Visible = true;
                ButtonWord.Visible = true;
            }

            Presenter.CaricaElementiInGridView(
                RadGridOrdine,
                dt);
        }

        private void CaricaDettaglioStorico(Int32 idStorico)
        {
            LabelDettagliStorico.Text = "Dettagli storico";
            LabelDettagliStorico.Visible = true;

            DataTable dt =
                tsBiz.GetOrdine(idStorico, Server.MapPath("Ordine.xsd"));

            if (dt != null && dt.Rows.Count > 0)
            {
                ButtonStampa.Visible = true;
                ButtonExcel.Visible = true;
                ButtonWord.Visible = true;
            }

            Presenter.CaricaElementiInGridView(
                RadGridOrdine,
                dt);
        }

        protected void ButtonStampa_Click(object sender, EventArgs e)
        {
            StringWriter sw = PrepareStampa();

            Response.Write(sw.ToString());
            Response.End();
        }

        public static string ShowStatus(bool val)
        {
            if (val == false)
                return "Scaduto";
            else
                return "Confermato";
        }

        #region RadGrid Dettagli

        protected void RadGridOrdine_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            if (ViewState["IdFabbisogno"] != null)
            {
                Int32 idFabbisogno = (Int32)ViewState["IdFabbisogno"];
                CaricaDettaglioFabbisogno(idFabbisogno);
            }

            if (ViewState["IdStorico"] != null)
            {
                Int32 idStorico = (Int32)ViewState["IdStorico"];
                CaricaDettaglioStorico(idStorico);
            }
        }

        protected void RadGridOrdine_DataBound(object sender, EventArgs e)
        {
            if (RadGridOrdine.Items.Count > 0)
            {
                Int32 idLavoratore = (Int32)RadGridOrdine.MasterTableView.DataKeyValues[0]["idLavoratore"];
                String gruppo = (String)RadGridOrdine.MasterTableView.DataKeyValues[0]["nomeGruppo"];

                for (Int32 i = 1; i < RadGridOrdine.Items.Count; i++)
                {
                    if ((Int32)RadGridOrdine.MasterTableView.DataKeyValues[i]["idLavoratore"] == idLavoratore)
                    {
                        RadGridOrdine.MasterTableView.Items[i]["IdLavoratore"].Text = "";
                        RadGridOrdine.MasterTableView.Items[i]["CognomeNome"].Text = "";
                        RadGridOrdine.MasterTableView.Items[i]["CodiceFiscale"].Text = "";
                        RadGridOrdine.MasterTableView.Items[i]["DataNascita"].Text = "";
                    }
                    else
                    {
                        idLavoratore = (Int32)RadGridOrdine.MasterTableView.DataKeyValues[i]["idLavoratore"];
                        gruppo = (String)RadGridOrdine.MasterTableView.DataKeyValues[i]["nomeGruppo"];
                        continue;
                    }

                    if ((String)RadGridOrdine.MasterTableView.DataKeyValues[i]["nomeGruppo"] == gruppo)
                    {
                        RadGridOrdine.MasterTableView.Items[i]["Gruppo"].Text = "";
                    }
                    else
                    {
                        gruppo = (String)RadGridOrdine.MasterTableView.DataKeyValues[i]["nomeGruppo"];
                    }
                }
            }
        }

        #endregion

        #region RadGrid Fabbisogni non gestiti

        protected void RadGridFabbisogniNonGestiti_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                DataRowView dr = (DataRowView)e.Item.DataItem;
                Label lConfermatoDa = (Label)e.Item.FindControl("LabelConfermatoDa");

                if (dr["ragioneSociale"] != DBNull.Value)
                {
                    lConfermatoDa.Text = dr["ragioneSociale"].ToString();
                }
                else
                {
                    if (dr["fornitoreRagioneSociale"] != DBNull.Value)
                    {
                        lConfermatoDa.Text = dr["fornitoreRagioneSociale"].ToString();
                    }
                    else
                    {
                        lConfermatoDa.Text = "Impresa";

                        if (dr["confermaAutomatica"] != DBNull.Value && (Int32)dr["confermaAutomatica"] == 1)
                        {
                            lConfermatoDa.Text = "Cassa Edile";
                        }
                    }
                }
            }
        }

        protected void RadGridFabbisogniNonGestiti_PageIndexChanged(object source, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            CaricaFabbisogni();
        }

        protected void RadGridFabbisogniNonGestiti_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItem item = RadGridFabbisogniNonGestiti.SelectedItems[0];

            Int32 idFabbisogno = (Int32)RadGridFabbisogniNonGestiti.MasterTableView.DataKeyValues[item.ItemIndex]["IdFabbisognoComplessivo"];
            CaricaDettaglioFabbisogno(idFabbisogno);

            ViewState["IdStorico"] = null;
            ViewState["IdFabbisogno"] = idFabbisogno;
            CaricaStorico();
        }

        #endregion

        #region RadGrid Storico

        protected void RadGridStorico_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaStorico();
        }

        protected void RadGridStorico_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                DataRowView dr = (DataRowView)e.Item.DataItem;
                Label lConfermatoDa = (Label)e.Item.FindControl("LabelConfermatoDa");

                if (!(bool)dr["stato"])
                {
                    e.Item.Cells[COLONNABOTTONE].Enabled = false;
                    e.Item.Cells[COLONNABOTTONE + 1].Enabled = false;
                }
                else
                {
                    e.Item.Font.Bold = true;

                    if (ViewState["IdStorico"] != null)
                    {
                        Int32 idStorico = (Int32)ViewState["IdStorico"];
                        if (idStorico == (Int32)dr["idStoricoTuteScarpe"])
                        {
                            e.Item.Selected = true;
                        }
                    }

                    // Confermato da
                    if (dr["ragioneSociale"] != DBNull.Value)
                    {
                        lConfermatoDa.Text = dr["ragioneSociale"].ToString();
                    }
                    else
                    {
                        if (dr["fornitoreRagioneSociale"] != DBNull.Value)
                        {
                            lConfermatoDa.Text = dr["fornitoreRagioneSociale"].ToString();
                        }
                        else
                        {
                            lConfermatoDa.Text = "Impresa";

                            if (dr["confermaAutomatica"] != DBNull.Value && (Int32)dr["confermaAutomatica"] == 1)
                            {
                                lConfermatoDa.Text = "Cassa Edile";
                            }
                        }
                    }
                }

                GridDataItem item = (GridDataItem)e.Item;
                Button buttonVisualizzaDettaglio = (Button)item["StatoConsegnaColumn"].Controls[0];

                string comando = String.Format(
                    "openRadWindow('{0}','{1}'); return false;",
                    item.OwnerTableView.DataKeyValues[item.ItemIndex]["idImpresa"],
                    item.OwnerTableView.DataKeyValues[item.ItemIndex]["idTuteScarpeOrdine"]
                    );

                buttonVisualizzaDettaglio.Attributes.Add("OnClick", comando);

            }
        }

        protected void RadGridStorico_DeleteCommand(object source, GridCommandEventArgs e)
        {
            Int32 idStorico = (Int32)RadGridStorico.MasterTableView.DataKeyValues[e.Item.ItemIndex]["idStoricoTuteScarpe"];
            CaricaDettaglioStorico(idStorico);

            CaricaStorico();
            ViewState["IdStorico"] = idStorico;
            ViewState["IdFabbisogno"] = null;
            e.Item.Selected = true;
        }

        protected void RadGridStorico_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItem item = RadGridStorico.SelectedItems[0];

            Int32 idStorico = (Int32)RadGridStorico.MasterTableView.DataKeyValues[item.ItemIndex]["idStoricoTuteScarpe"];
            CaricaDettaglioStorico(idStorico);

            ViewState["IdStorico"] = idStorico;
            ViewState["IdFabbisogno"] = null;
            CaricaFabbisogni();
        }

        #endregion

        #region Export

        private StringWriter PrepareStampa()
        {
            GridView gv = new GridView();
            gv.ID = "gvProducts";
            gv.AutoGenerateColumns = false;

            BoundField bc1 = new BoundField();
            bc1.HeaderText = "Codice lavoratore";
            bc1.DataField = "idLavoratore";
            BoundField bc2 = new BoundField();
            bc2.HeaderText = "Nome lavoratore";
            bc2.DataField = "nomeLavoratore";
            BoundField bc3 = new BoundField();
            bc3.HeaderText = "Codice fiscale";
            bc3.DataField = "codiceFiscale";
            BoundField bc4 = new BoundField();
            bc4.HeaderText = "Data nascita";
            bc4.DataField = "dataNascita";
            bc4.DataFormatString = "{0:dd/MM/yyyy}";
            bc4.HtmlEncode = false;
            BoundField bc5 = new BoundField();
            bc5.HeaderText = "Gruppo";
            bc5.DataField = "nomeGruppo";
            BoundField bc6 = new BoundField();
            bc6.HeaderText = "Descrizione";
            bc6.DataField = "descrizione";
            BoundField bc7 = new BoundField();
            bc7.HeaderText = "Quantità";
            bc7.DataField = "quantita";
            BoundField bc8 = new BoundField();
            bc8.HeaderText = "Taglia selezionata";
            bc8.DataField = "tagliaSel";

            gv.Columns.Add(bc1);
            gv.Columns.Add(bc2);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            gv.Columns.Add(bc7);
            gv.Columns.Add(bc8);

            if (ViewState["IdFabbisogno"] != null)
            {
                Int32 idFabbisogno = (Int32)ViewState["IdFabbisogno"];
                gv.DataSource =
                    tsBiz.GetFabbisogniConfermatiDettagliComeXML(idFabbisogno, idImpresa);
            }

            if (ViewState["IdStorico"] != null)
            {
                Int32 idStorico = (Int32)ViewState["IdStorico"];
                gv.DataSource =
                    tsBiz.GetOrdine(idStorico, Server.MapPath("Ordine.xsd"));
            }
            gv.DataBind();

            //gv.Columns[0].HeaderText = "Codice lavoratore";
            //gv.Columns[1].HeaderText = "Nome lavoratore";
            //gv.Columns[2].HeaderText = "Codice fiscale";
            //gv.Columns[3].HeaderText = "Data nascita";
            //gv.Columns[4].HeaderText = "Gruppo";
            //gv.Columns[5].HeaderText = "Descrizione";
            //gv.Columns[6].HeaderText = "Quantità";
            //gv.Columns[7].HeaderText = "Taglia selezionata";

            //
            string idLav = gv.Rows[0].Cells[0].Text;
            string nomeGruppo = gv.Rows[0].Cells[4].Text;

            for (int i = 1; i < gv.Rows.Count; i++)
            {
                if (gv.Rows[i].Cells[0].Text == idLav)
                {
                    gv.Rows[i].Cells[0].Text = "";
                    gv.Rows[i].Cells[1].Text = "";
                    gv.Rows[i].Cells[2].Text = "";
                    gv.Rows[i].Cells[3].Text = "";
                }
                else
                {
                    idLav = gv.Rows[i].Cells[0].Text;
                    nomeGruppo = gv.Rows[i].Cells[4].Text;
                    continue;
                }

                if (gv.Rows[i].Cells[4].Text == nomeGruppo)
                {
                    gv.Rows[i].Cells[4].Text = "";
                }
                else
                {
                    nomeGruppo = gv.Rows[i].Cells[4].Text;
                }
            }
            //

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            return sw;
        }


        protected void ButtonExcel_Click(object sender, EventArgs e)
        {
            StringWriter sw = PrepareStampa();

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=Fabbisogno.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void ButtonWord_Click(object sender, EventArgs e)
        {
            StringWriter sw = PrepareStampa();

            Response.ClearContent();
            Response.AppendHeader("content-disposition", "attachment; filename=Fabbisogno.doc");
            Response.ContentType = "application/vnd.ms-word";
            Response.Write(sw.ToString());
            Response.End();
        }

        #endregion
    }
}