﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeConsulenteImprese.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeConsulenteImprese" %>

<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%@ Register Src="../Cantieri/WebControls/CantieriRicercaImpresa.ascx" TagName="CantieriRicercaImpresa"
    TagPrefix="uc4" %>

<%@ Register Src="WebControls/TuteScarpeRicercaConsulenti.ascx" TagName="TuteScarpeRicercaConsulenti"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>

<%--<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione consulenti"
        titolo="Tute e scarpe" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Consulente
            </td>
            <td>
                <asp:TextBox ID="TextBoxConsulente" runat="server" Enabled="false" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="ButtonSelezionaConsulente" runat="server" Text="Seleziona consulente" OnClick="ButtonSelezionaConsulente_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="PanelSelezionaConsulente" runat="server" Visible="false" Width="100%">
                    <uc3:TuteScarpeRicercaConsulenti ID="TuteScarpeRicercaConsulenti1" runat="server" />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Panel ID="PanelDettagli" runat="server" Visible="false" Width="100%">
    <table class="standardTable">
        <tr>
            <td colspan="2">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Imprese associate"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Filtro
            </td>
            <td>
                <asp:DropDownList ID="DropDownListFiltro" runat="server" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownListFiltro_SelectedIndexChanged">
                    <asp:ListItem Value="TUTTI">Tutti</asp:ListItem>
                    <asp:ListItem Value="ATTIVI">Attivi</asp:ListItem>
                    <asp:ListItem Value="NONATTIVI">Non attivi</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewImpreseConsulente" runat="server" width="100%" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridViewImpreseConsulente_PageIndexChanging" DataKeyNames="IdRapportoConsulenteImpresa,DataInizio" OnRowDataBound="GridViewImpreseConsulente_RowDataBound" OnSelectedIndexChanging="GridViewImpreseConsulente_SelectedIndexChanging">
                    <EmptyDataTemplate>
                        Nessuna impresa associata al consulente
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="IdImpresa" HeaderText="Codice" />
                        <asp:BoundField DataField="RagioneSocialeImpresa" HeaderText="Ragione sociale" />
                        <asp:TemplateField HeaderText="Nota">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBoxNota" runat="server" Height="42px" MaxLength="255" TextMode="MultiLine"
                                    Width="200px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DataInizio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Inizio valid."
                            HtmlEncode="False" />
                        <asp:TemplateField HeaderText="Fine valid.">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBoxDataFine" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDataFine"
                                    ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Salva" ShowSelectButton="True" />
                    </Columns>
                </asp:GridView>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Errore durante l'inserimento"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="ButtonSelezionaImpresa" runat="server" OnClick="ButtonSelezionaImpresa_Click" Text="Seleziona impresa" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelSelezioneImpresa" runat="server" Visible="False" Width="100%">
        <br />
        <table class="filledtable">
            <tr>
                <td>
                    <asp:Label id="Label2" runat="server" Text="Selezione impresa" ForeColor="White" Font-Bold="True"></asp:Label> 
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    Impresa
                    <asp:TextBox ID="TextBoxImpresaSelezionata" runat="server" Height="72px" Width="300px" Enabled="False"></asp:TextBox>
                </td>
                <td>
                    Nota
                    <asp:TextBox ID="TextBoxNota" runat="server" Height="72px" Width="300px" MaxLength="255" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="ButtonAggiungiRapporto" runat="server" Text="Salva rapporto" OnClick="ButtonAggiungiRapporto_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Imprese suggerite
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridViewImpreseDenunce" runat="server" AutoGenerateColumns="False" width="100%" AllowPaging="True" DataKeyNames="IdImpresa,RagioneSocialeImpresa" OnPageIndexChanging="GridViewImpreseDenunce_PageIndexChanging" OnSelectedIndexChanging="GridViewImpreseDenunce_SelectedIndexChanging" PageSize="5">
                        <Columns>
                            <asp:BoundField HeaderText="Codice" DataField="IdImpresa" >
                                <ItemStyle Width="50px" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Ragione sociale" DataField="RagioneSocialeImpresa" />
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                                SelectText="Seleziona" ShowSelectButton="True" >
                                <ControlStyle CssClass="bottoneGriglia" />
                                <ItemStyle Width="10px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna impresa associata su SiceNew al consulente
                        </EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="ButtonAltraImpresa" runat="server" Text="Altra impresa" OnClick="ButtonAltraImpresa_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="PanelAltraImpresa" runat="server" Visible="False">
                        <uc4:CantieriRicercaImpresa ID="CantieriRicercaImpresa1" runat="server" Visible="true" />
                        
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
</asp:Content>


