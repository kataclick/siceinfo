﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeDefault" %>

<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe"
        sottoTitolo="Benvenuto nella sezione Tute e Scarpe" />
    <br />
    <p class="DefaultPage">
        Come disposto dalla normativa vigente in materia di prestazioni assistenziali, la Cassa Edile 
		garantisce, una volta all’anno, la fornitura di indumenti da lavoro a tutti i lavoratori che 
		al momento della distribuzione si trovino alle dipendenze di impresa iscritta alla Cassa Edile 
		di Milano, Lodi, Monza e Brianza e che abbiano maturato 1.800 o 500 ore di lavoro ordinario 
		rispettivamente nei 12 o nei 3 mesi precedenti il 1° settembre. 
		<br />
        <br />
		Indipendentemente dalle ore maturate, a tutti gli operai la Cassa Edile fornisce 1 paio di 
		scarpe antinfortunistiche. 
		<br />
        <br />
		La consegna della fornitura avviene direttamente presso l’indirizzo indicato dalle imprese 
		richiedenti.
        <br />
        <br />
        La fornitura annuale gratuita per i lavoratori dipendenti da imprese edili si compone di:
        <ul>
            <li>n. 1 felpa pile</li>
            <li>n. 1 paio di jeans</li>
            <li>n. 1 paio di scarpe antinfortunistiche</li>
        </ul>
        <br />
        Per i lavoratori dipendenti da imprese asfaltiste vengono forniti: 1 giubbino ad alta visibilità, 1 paio di pantaloni ad alta visibilità e 1 paio di scarpe antinfortunistiche. 
		<br />
        <br />
		La fornitura per la categoria imbianchini è composta da: 1 paio di pantaloni bianchi, 1 felpa pile, 1 T-shirt bianca e 1 paio di scarpe antinfortunistiche.
		<br />
        <br /> 
		Il sistema propone in automatico per ogni impresa iscritta, l’elenco dei dipendenti che hanno 
		maturato il diritto alla prestazione; ne consegue che la mancata visualizzazione del nominativo 
		di un dipendente può coincidere con l'avvenuta denuncia dello stesso a posteriori rispetto alla 
		pubblicazione della proposta di fabbisogno.
		<br />
        <br />
		Analogamente la mancata visualizzazione del campo vestiario equivale al mancato soddisfacimento 
		dei requisiti di accantonamento orario previsti, ovvero del numero minimo di ore di lavoro ordinario 
		maturate e pagate.
		I lavoratori che matureranno il diritto in seguito ed i neo-assunti verranno inclusi direttamente 
		dal sistema, senza la necessità da parte dell'impresa di farne specifica richiesta.
		<br />
        <br />
		Qualora l’impresa non dovesse completare e confermare la proposta d’ordine in tempi utili, quest’ultima 
		scadrà e verrà ripresentata dal sistema automaticamente in seguito. 
		<br />
        <br />
		Scarica il manuale utente per la compilazione guidata della richiesta di indumenti e calzature da lavoro.
		<br />
		<a href="http://ww2.cassaedilemilano.it/Portals/0/pdf/Tute%20e%20Scarpe%20-%20Manuale%20utente%2020_09_2017%20M.O.%208.5.3.3.pdf" target="_blank">Clicca Qui</a>		
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>

