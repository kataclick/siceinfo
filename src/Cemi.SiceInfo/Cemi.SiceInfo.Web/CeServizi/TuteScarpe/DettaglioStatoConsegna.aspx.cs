﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class DettaglioStatoConsegna : Page
    {
        private readonly TSBusiness biz = new TSBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idImpresa"]))
            {
                ViewState["IdImpresa"] = Int32.Parse(Request.QueryString["idImpresa"]);
                LabelCodiceImpresa.Text = biz.GetImpresaRagioneSociale((Int32)ViewState["IdImpresa"]);
            }
            if (!String.IsNullOrEmpty(Request.QueryString["idOrdine"]))
            {
                ViewState["IdOrdine"] = Int32.Parse(Request.QueryString["idOrdine"]);
                LabelNumeroOrdine.Text = ViewState["IdOrdine"].ToString();
            }

            CaricaDettaglio();
        }

        private void CaricaDettaglio()
        {
            if (ViewState["IdImpresa"] != null && ViewState["IdOrdine"] != null)
            {
                Presenter.CaricaElementiInGridView(
                    RadGridDettaglioStatoConsegna,
                    biz.GetConsegneOrdini((Int32)ViewState["IdImpresa"], (Int32)ViewState["IdOrdine"]));
            }
        }

        protected void RadGridDettaglioStatoConsegna_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaDettaglio();
        }
    }
}