﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfermaFabbisognoStep1.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.ConfermaFabbisognoStep1" %>

<%--<%@ Register Src="~/WebControls/TuteScarpeMenu.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>--%>
<%--<%@ Register Src="~/WebControls/MenuGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione" TagPrefix="uc4" %>
<%@ Register Src="~/WebControls/MenuGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc3" %>--%>
<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" tagname="TuteScarpeConsulenteImpresaSelezionata" tagprefix="uc6" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Stato salvataggio ordine"/>
    <br />
    <uc6:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1" runat="server" />
    <br />
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="L'ordine non può essere confermato poichè la posizione di alcuni lavoratori non è stata completata."></asp:Label>
    <br /><br />
    &nbsp;<asp:Label ID="LabelLavIncompleti" runat="server" Text="Elenco nominativi lavoratori per i quali non è stato ultimato l'ordine"></asp:Label>
    <br />
    <telerik:RadGrid ID="RadGridLavoratoriIncompleti" runat="server" GridLines="None">
        <HeaderContextMenu EnableAutoScroll="True"></HeaderContextMenu>
        <MasterTableView>
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="IdLavoratore" HeaderText="Codice lavoratore" UniqueName="idLavoratore">
                    <ItemStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Cognome e nome" UniqueName="CognomeNome" DataField="nomeLavoratore">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro" Width="135px" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>

