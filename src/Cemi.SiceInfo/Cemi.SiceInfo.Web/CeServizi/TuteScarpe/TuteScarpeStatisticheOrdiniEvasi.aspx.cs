﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeStatisticheOrdiniEvasi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeStatistiche);
        }

        protected void ReportViewerOrdiniEvasi_Init(object sender, EventArgs e)
        {
            ReportViewerOrdiniEvasi.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            //ReportViewerOrdiniEvasi.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportOrdiniEvasi";
        }

        protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
        {
            ReportViewerOrdiniEvasi.ServerReport.ReportPath = "/TuteScarpeStatistiche/ReportOrdiniEvasi";

            List<ReportParameter> listaParam = new List<ReportParameter>();
            if (RadDatePickerDataDa.SelectedDate != null)
            {
                ReportParameter param1 = new ReportParameter("dalpadre",
                                             RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param1);
            }
            if (RadDatePickerDataA.SelectedDate != null)
            {
                ReportParameter param2 = new ReportParameter("alpadre", RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
                listaParam.Add(param2);
            }

            ReportViewerOrdiniEvasi.ServerReport.SetParameters(listaParam.ToArray());
        }

    }
}