﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.TuteScarpe.Business;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class RipristinaLavoratoreInFabbisogno : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdLavoratore"] != null
                    && Context.Items["IdImpresa"] != null
                    && Context.Items["NomeLavoratore"] != null)
                {
                    ViewState["IdLavoratore"] = Context.Items["IdLavoratore"];
                    ViewState["IdImpresa"] = Context.Items["IdImpresa"];

                    LabelNomeLavoratore.Text = Context.Items["NomeLavoratore"].ToString();
                    LabelIdLavoratore.Text = Context.Items["IdLavoratore"].ToString();
                }
                else
                {
                    ButtonConfermaRipristino.Enabled = false;
                }
            }
        }

        protected void ButtonConfermaRipristino_Click(object sender, EventArgs e)
        {
            if (ViewState["IdLavoratore"] != null
                && ViewState["IdImpresa"] != null)
            {
                Int32 idLavoratore = (Int32)ViewState["IdLavoratore"];
                Int32 idImpresa = (Int32)ViewState["IdImpresa"];

                TSBusiness tsBusiness = new TSBusiness();
                tsBusiness.RipristinaLavoratore(idImpresa, idLavoratore);

                Response.Redirect("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
        }
    }
}