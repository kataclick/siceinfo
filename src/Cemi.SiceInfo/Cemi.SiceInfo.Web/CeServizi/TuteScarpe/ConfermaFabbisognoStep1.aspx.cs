﻿using Cemi.SiceInfo.Web.Helpers;
using System;
using System.Data;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using CommonEntities = TBridge.Cemi.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class ConfermaFabbisognoStep1 : System.Web.UI.Page
    {
        private int idUtente;
        private int idImpresa;
        private TSBusiness tsBiz = new TSBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);
            #endregion

            if (GestioneUtentiBiz.IsImpresa())
            {
                Impresa impresaEnt =
                    (Impresa)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                idImpresa = impresaEnt.IdImpresa;
                idUtente = impresaEnt.IdUtente;
            }
            else if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente = (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                CommonEntities.Impresa impresa = tsBiz.GetImpresaSelezionataConsulente(consulente.IdConsulente);
                idImpresa = impresa.IdImpresa;
                idUtente = impresa.IdUtente;
            }
            else if (GestioneUtentiBiz.IsFornitore())
            {
                Fornitore fornitore =
                    (Fornitore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                //prendiamo da db l'impresa selezionata dal fornitore
                CommonEntities.Impresa impresa = tsBiz.GetImpresaSelezionataFornitore(fornitore.IdFornitore);
                idImpresa = impresa.IdImpresa;
                idUtente = impresa.IdUtente;
            }

            DataTable lavIncompleti = tsBiz.GetLavoratoriIncompleti(idImpresa);

            if (lavIncompleti != null)
            {
                if (lavIncompleti.Rows.Count > 0)
                {
                    //Non si può salvare perchè alcuni lav non sono definiti
                    Presenter.CaricaElementiInGridView(
                        RadGridLavoratoriIncompleti,
                        lavIncompleti);
                }
                else
                {
                    Response.Redirect("~/CeServizi/TuteScarpe/ConfermaFabbisognoStep2.aspx");
                }
            }
        }


        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx");
        }
    }
}