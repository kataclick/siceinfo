﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeGestioneOrdiniConfermati.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeGestioneOrdiniConfermati" %>

<%--<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc3" %>--%>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuTuteScarpeContenitore.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>

<%--<%@ Register Src="~/WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>--%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Gestione ordini confermati" />
    <br />
    <b>Filtra elenco degli ordini confermati:</b>
    <br />
    <table class="standardTable">
        <tr>
            <td>Data:</td>
            <td>Dal (gg/mm/aaaa):
            </td>
            <td>
                <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxDal"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d" ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>

        </tr>
        <tr>
            <td></td>
            <td>Al (gg/mm/aaaa):
            </td>
            <td>
                <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxAl"
                    Display="Dynamic" ErrorMessage="Formato data non valido" ValidationExpression="(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d" ValidationGroup="ricerca"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>Stato:
            </td>
            <td>Scaricati</td>
            <td colspan="2">
                <asp:RadioButton ID="RadioButtonScaricati" runat="server" GroupName="scelta" /></td>
        </tr>
        <tr>
            <td></td>
            <td>Non scaricati</td>
            <td colspan="2">
                <asp:RadioButton ID="RadioButtonNonScaricati" runat="server" GroupName="scelta" /></td>
        </tr>
        <tr>
            <td></td>
            <td>Tutti</td>
            <td colspan="2">
                <asp:RadioButton ID="RadioButtonTutti" runat="server" GroupName="scelta" Checked="True" /></td>
        </tr>
        <tr>
            <td>Codice</td>
            <td>
                <asp:TextBox ID="TextBoxCodice" runat="server"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:CompareValidator ID="CompareValidatorCodice" runat="server" ControlToValidate="TextBoxCodice" ErrorMessage="*" Operator="GreaterThan" Type="Integer" ValidationGroup="ricerca" ValueToCompare="0"></asp:CompareValidator>
                (Filtra gli ordini che contengono l'impresa)
            </td>
        </tr>
        <tr>
            <td>Ragione sociale</td>
            <td>
                <asp:TextBox ID="TextBoxRagSoc" runat="server"></asp:TextBox></td>
            <td colspan="2">(Filtra gli ordini che contengono l'impresa)</td>
        </tr>
    </table>
    <asp:Label ID="LabelErrore" runat="server" Text="La data di inizio ricerca deve essere uguale o inferiore a quella di fine ricerca" ForeColor="Red" Visible="False"></asp:Label><br />
    <asp:Label ID="LabelFormatoDate" runat="server" ForeColor="Red" Text="Controllare il formato delle date"
        Visible="False"></asp:Label><br />
    <br />
    <asp:Button ID="ButtonRicercaOrdiniConfermati" runat="server" OnClick="ButtonRicercaOrdiniConfermati_Click"
        Text="Ricerca" Width="175px" ValidationGroup="ricerca" /><br />
    <br />
    <asp:Label ID="LabelElencoOrdiniConfermati" runat="server" Font-Bold="True" Text="Elenco ordini confermati"></asp:Label><br />
    <asp:GridView ID="GridViewOrdiniConfermati" runat="server" Width="100%" AutoGenerateColumns="False" OnRowEditing="GridViewOrdiniConfermati_RowEditing" DataKeyNames="IdOrdine" OnRowUpdating="GridViewOrdiniConfermati_RowUpdating" AllowPaging="True" OnPageIndexChanging="GridViewOrdiniConfermati_PageIndexChanging">
        <EmptyDataTemplate>
            Non sono presenti ordini confermati
        </EmptyDataTemplate>
        <Columns>
            <asp:BoundField DataField="IdOrdine" HeaderText="Protocollo ordine">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="descrizione" HeaderText="Descrizione" />
            <asp:BoundField DataField="dataCreazione" HeaderText="Data creazione"
                DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="dataConferma" HeaderText="Data conferma"
                DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="False">
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="codiceFornitore" HeaderText="Codice fornitore">
                <ItemStyle Width="50px" />
            </asp:BoundField>
            <asp:BoundField DataField="ScaricatoFornitore" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="File scaricato il" HtmlEncode="False">
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button"
                CommandName="Update" Text="Dettagli">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" Text="Esporta Ordine"
                ButtonType="Button" CommandName="Edit">
                <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content5" runat="server" ContentPlaceHolderID="MainPage2">
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Dettagli dell'ordine:" Visible="False"></asp:Label>
    <asp:Label ID="LabelOrdine" runat="server" Visible="False"></asp:Label>
</asp:Content>
