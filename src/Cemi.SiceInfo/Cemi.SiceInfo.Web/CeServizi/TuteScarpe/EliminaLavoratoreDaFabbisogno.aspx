﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="EliminaLavoratoreDaFabbisogno.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.EliminaLavoratoreDaFabbisogno" %>

<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register src="WebControls/TuteScarpeConsulenteImpresaSelezionata.ascx" tagname="TuteScarpeConsulenteImpresaSelezionata" tagprefix="uc6" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Conferma cancellazione lavoratore" />
    <br />
    <uc6:TuteScarpeConsulenteImpresaSelezionata ID="TuteScarpeConsulenteImpresaSelezionata1" runat="server" />
    <br />
    Il lavoratore <asp:Label ID="LabelNomeLavoratore" runat="server" Font-Bold="True"></asp:Label>, codice <asp:Label ID="LabelIdLavoratore" runat="server" Font-Bold="True"></asp:Label>,
    sta per essere rimosso dall'elenco dei lavoratori aventi diritto al fabbisogno.<br />
    Continuare con la cancellazione?<br />
    <br />
    <center>
    <asp:Button ID="ButtonConfermaCancellazione" runat="server" OnClick="ButtonConfermaCancellazione_Click" Text="Conferma cancellazione" Width="200px" />
    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro" Width="200px" />
    </center>
</asp:Content>

