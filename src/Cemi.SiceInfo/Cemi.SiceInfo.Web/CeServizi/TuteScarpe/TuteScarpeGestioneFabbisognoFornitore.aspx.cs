﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.TuteScarpe.Data.Collections;
using TBridge.Cemi.TuteScarpe.Data.Entities;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Fornitore = TBridge.Cemi.Type.Entities.GestioneUtenti.Fornitore;

namespace Cemi.SiceInfo.Web.CeServizi.TuteScarpe
{
    public partial class TuteScarpeGestioneFabbisognoFornitore : System.Web.UI.Page
    {
        private const int BOTTONEMODIFICA = 4;
        private readonly TSBusiness biz = new TSBusiness();

        private DateTime? scadenzaProposta;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno);

            if (GestioneUtentiBiz.IsFornitore())
            {
                if (!Page.IsPostBack)
                {
                    scadenzaProposta = biz.UltimoFabbisognoValido();
                    if (scadenzaProposta.HasValue && scadenzaProposta < DateTime.Now)
                    {
                        LabelMessaggio.Text =
                            "Il sistema è in fase di aggiornamento. La prossima proposta di fabbisogno verrà pubblicata in seguito.";

                        TuteScarpeFornitoreImpresaSelezionaImpresa1.Visible = false;
                    }
                    else if (!scadenzaProposta.HasValue || scadenzaProposta > DateTime.Now)
                    {
                        if (scadenzaProposta.HasValue)
                        {
                            LabelMessaggio.Text =
                                String.Format(
                                    "L’attuale proposta di fabbisogno scadrà il {0} e verrà riproposta aggiornata in seguito.",
                                    scadenzaProposta.Value.ToShortDateString());
                        }

                        TuteScarpeFornitoreImpresaSelezionaImpresa1.Visible = true;
                    }
                }
            }
        }
    }
}