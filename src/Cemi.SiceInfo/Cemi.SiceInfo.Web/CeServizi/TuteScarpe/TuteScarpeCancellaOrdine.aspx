﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="TuteScarpeCancellaOrdine.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.TuteScarpe.TuteScarpeCancellaOrdine" %>

<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="TuteScarpeMenu" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuTuteScarpeGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%--<%@ Register Src="../WebControls/MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc2" %>--%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
<uc5:TuteScarpeMenu ID="TuteScarpeMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Gestione Tute e Scarpe" sottoTitolo="Cancellazione ordine"/>
    <br />
    Sta per essere eliminato un ordine. Tutti fabbisogni che ne facevano parte dovranno
    nuovamente essere gestiti e inseriti in un un nuovo ordine.<br />
    <table class="standardTable">
        <tr>
            <td style="width: 270px">
            </td>
            <td align="left">
                <asp:TextBox ID="TextBoxIdOrdine" runat="server" Enabled="False" Visible="False"
                    Width="249px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" style="width: 270px; height: 28px">
                Descrizione
            </td>
            <td align="left" style="width: 3px; height: 28px">
                <asp:TextBox ID="TextBoxDescrizione" runat="server" Enabled="False" Width="249px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" style="width: 270px">
                Data creazione
            </td>
            <td align="left" style="width: 3px">
                <asp:TextBox ID="TextBoxDataCreazione" runat="server" Enabled="False" ReadOnly="True"
                    Width="249px"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Dettagli" Width="226px"></asp:Label><br />
    <asp:GridView ID="GridViewDettagli" runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridViewDettagli_PageIndexChanging" Width="701px">
        <Columns>
            <asp:BoundField DataField="IdFabbisogno" HeaderText="Id fabbisogno" Visible="False" />
            <asp:BoundField DataField="IdImpresa" HeaderText="Id Impresa" />
            <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione Sociale" />
            <asp:BoundField DataField="CodiceFornitore" HeaderText="Fornitore" />
            <asp:BoundField DataField="DataConfermaImpresa" DataFormatString="{0:dd/MM/yyyy}"
                HeaderText="Data Conferma" HtmlEncode="False" />
        </Columns>
    </asp:GridView>
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red" Width="506px"></asp:Label><br />
    <br />
    <asp:Button ID="ButtonConfermaCancellazione" runat="server" OnClick="ButtonConfermaCancellazione_Click"
        Text="Conferma eliminazione" Width="165px" />
    <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click"
        Text="Torna indietro" Width="165px" />
</asp:Content>
