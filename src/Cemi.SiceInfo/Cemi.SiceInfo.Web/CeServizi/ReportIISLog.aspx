﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportIISLog.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.ReportIISLog" %>

<%@ Register Src="WebControls/MenuIISLog.ascx" TagName="IISLogMenu" TagPrefix="uc2" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Report Log IIS"
        Visible="true" />
    <br />
    <table height="600px" width="740px">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerIISLog" runat="server" OnInit="ReportViewerIISLog_Init" ProcessingMode="Remote" Width="100%" Height="490px">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:IISLogMenu ID="IISLogMenu1" runat="server" />
</asp:Content>
