﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuIISLog.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuIISLog" %>

<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<table class="menuTable">
    <tr class="menuTable">
        <td >
            Statistiche IIS</td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IISLogStatistiche))
        {%>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=browser">Statistiche browser</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=country">Statistiche geografiche</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=giornoMese">Statistiche giorno-mese</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=giornoSettimana">Statistiche giorno-settimana</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=globali">Statistiche globali</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=ore">Statistiche ore-giorno</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=durata">Statistiche durata</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=OS">Statistiche sistema operativo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=pagine">Statistiche pagine</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=riepilogo">Statistiche di riepilogo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=temporali">Statistiche temporali</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="ReportIISLog.aspx?tipo=visitatori">Statistiche visitatori</a>
        </td>
    </tr>
    <%
        }
%>
</table>
