﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuSOLDO.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuSOLDO" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (//GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente() || 
        TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOImpresa)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOConsulente)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOAmministratoreApplicativo)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOAmministratoreTecnico)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOOperatore))
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            SOLDO
        </td>
    </tr>
    <%
        if (//GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente() || 
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOImpresa)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOConsulente)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOAmministratoreApplicativo)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOAmministratoreTecnico)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SOLDOOperatore))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="../SOLDO/SOLDO.aspx?direzione=SOLDO" runat="server">Accedi a SOLDO</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>