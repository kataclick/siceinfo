﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuSubappalti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuSubappalti" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%

    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.SubappaltiRicerca))
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>Funzioni</td>
    </tr>
    <tr>
        <td>    
            <a href="~/CeServizi/Subappalti/SubappaltiRicercaImprese.aspx" runat="server">Ricerca impresa</a><br />
        </td>
    </tr>
</table>
<%
    }

    if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.SubappaltiStorico) ||
        GestioneUtentiBiz.IsImpresa())
    {
%>

<table class="menuTable">
    <tr class="menuTable" style="BACKGROUND-IMAGE: url(../images/sfondo_sotto_voci_menu_sinistra_new.gif); HEIGHT: 28px">
        <td>Storico</td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa())
        {
%>
    <tr>
        <td>    
            <a href="~/CeServizi/Subappalti/SubappaltiVisualizzaStoricoPropriaImpresa.aspx" runat="server">Chi mi ha cercato</a>
        </td>
    </tr>
    <%
        }
%>
        
    <%
        if (GestioneUtentiBiz.Autorizzato(
            FunzionalitaPredefinite.SubappaltiStorico))
        {
%>
    <tr>
        <td>    
            <a href="~/CeServizi/Subappalti/SubappaltiVisualizzaStorico.aspx" runat="server">Tutti gli storici</a>
        </td>
    </tr>
    <%
        }
%>
</table>

        <%
    }
%>