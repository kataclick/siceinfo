﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.WebControls
{
    public partial class OreCNCECaricamentoManuale : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void ImpostaCassaEdileELavoratore(string idCassaEdile, int idLavoratore)
        {
            TextBoxCassaEdile.Text = idCassaEdile;
            ViewState["IdLavoratore"] = idLavoratore;
        }

        public OreMensiliCNCE CreaOre()
        {
            OreMensiliCNCE ore = new OreMensiliCNCE();

            ore.IdCassaEdile = TextBoxCassaEdile.Text;
            ore.IdLavoratore = (int)ViewState["IdLavoratore"];
            ore.Anno = Int32.Parse(TextBoxAnno.Text);
            ore.Mese = Int32.Parse(TextBoxMese.Text);
            if (!string.IsNullOrEmpty(TextBoxOreLavorate.Text))
                ore.OreLavorate = Int32.Parse(TextBoxOreLavorate.Text);
            if (!string.IsNullOrEmpty(TextBoxOreFerie.Text))
                ore.OreFerie = Int32.Parse(TextBoxOreFerie.Text);
            if (!string.IsNullOrEmpty(TextBoxOreInfortunio.Text))
                ore.OreInfortunio = Int32.Parse(TextBoxOreInfortunio.Text);
            if (!string.IsNullOrEmpty(TextBoxOreMalattia.Text))
                ore.OreMalattia = Int32.Parse(TextBoxOreMalattia.Text);
            if (!string.IsNullOrEmpty(TextBoxOreCassaIntegrazione.Text))
                ore.OreCassaIntegrazione = Int32.Parse(TextBoxOreCassaIntegrazione.Text);
            if (!string.IsNullOrEmpty(TextBoxOrePermessiRetribuiti.Text))
                ore.OrePermessoRetribuito = Int32.Parse(TextBoxOrePermessiRetribuiti.Text);
            if (!string.IsNullOrEmpty(TextBoxOrePermessiNonRetribuiti.Text))
                ore.OrePermessoNonRetribuito = Int32.Parse(TextBoxOrePermessiNonRetribuiti.Text);
            if (!string.IsNullOrEmpty(TextBoxOreAltro.Text))
                ore.OreAltro = Int32.Parse(TextBoxOreAltro.Text);
            if (!string.IsNullOrEmpty(TextBoxLivelloErogazione.Text))
                ore.LivelloErogazione = Int32.Parse(TextBoxLivelloErogazione.Text);
            ore.InseriteManualmente = true;

            return ore;
        }

        protected void CustomValidatorQualcheOraPresente_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (string.IsNullOrEmpty(TextBoxOreLavorate.Text)
                && string.IsNullOrEmpty(TextBoxOreFerie.Text)
                && string.IsNullOrEmpty(TextBoxOreInfortunio.Text)
                && string.IsNullOrEmpty(TextBoxOreMalattia.Text)
                && string.IsNullOrEmpty(TextBoxOreCassaIntegrazione.Text)
                && string.IsNullOrEmpty(TextBoxOrePermessiRetribuiti.Text)
                && string.IsNullOrEmpty(TextBoxOrePermessiNonRetribuiti.Text)
                && string.IsNullOrEmpty(TextBoxOreAltro.Text))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        public void ResetCampi()
        {
            TextBoxCassaEdile.Text = null;
            TextBoxAnno.Text = null;
            TextBoxMese.Text = null;
            TextBoxOreLavorate.Text = null;
            TextBoxOreFerie.Text = null;
            TextBoxOreInfortunio.Text = null;
            TextBoxOreMalattia.Text = null;
            TextBoxOreCassaIntegrazione.Text = null;
            TextBoxOrePermessiRetribuiti.Text = null;
            TextBoxOrePermessiNonRetribuiti.Text = null;
            TextBoxOreAltro.Text = null;
            TextBoxLivelloErogazione.Text = null;
        }

        public void CaricaOre(OreMensiliCNCE ore)
        {
            ResetCampi();

            TextBoxCassaEdile.Text = ore.IdCassaEdile;
            ViewState["IdLavoratore"] = ore.IdLavoratore;
            TextBoxAnno.Text = ore.Anno.ToString();
            TextBoxMese.Text = ore.Mese.ToString();
            if (ore.OreLavorate > 0)
                TextBoxOreLavorate.Text = ore.OreLavorate.ToString();
            if (ore.OreFerie > 0)
                TextBoxOreFerie.Text = ore.OreFerie.ToString();
            if (ore.OreInfortunio > 0)
                TextBoxOreInfortunio.Text = ore.OreInfortunio.ToString();
            if (ore.OreMalattia > 0)
                TextBoxOreMalattia.Text = ore.OreMalattia.ToString();
            if (ore.OreCassaIntegrazione > 0)
                TextBoxOreCassaIntegrazione.Text = ore.OreCassaIntegrazione.ToString();
            if (ore.OrePermessoRetribuito > 0)
                TextBoxOrePermessiRetribuiti.Text = ore.OrePermessoRetribuito.ToString();
            if (ore.OrePermessoNonRetribuito > 0)
                TextBoxOrePermessiNonRetribuiti.Text = ore.OrePermessoNonRetribuito.ToString();
            if (ore.OreAltro > 0)
                TextBoxOreAltro.Text = ore.OreAltro.ToString();
            if (ore.LivelloErogazione > 0)
                TextBoxLivelloErogazione.Text = ore.LivelloErogazione.ToString();
            ore.InseriteManualmente = true;
        }
    }
}