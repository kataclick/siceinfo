﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cemi.SiceInfo.Web.CeServizi.WebControls
{
    public partial class TitoloSottotitolo : UserControl
    {
        private string _sottoTitolo = string.Empty;
        private string _titolo = string.Empty;

        public string titolo
        {
            get { return _titolo; }
            set { _titolo = value; }
        }

        public string sottoTitolo
        {
            get { return _sottoTitolo; }
            set { _sottoTitolo = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}