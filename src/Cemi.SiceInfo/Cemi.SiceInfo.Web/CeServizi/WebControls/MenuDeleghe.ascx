﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuDeleghe.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuDeleghe" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestioneCE)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheConferma)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheSblocco)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheVisioneCE))
    {                  
%>
<table class="menuTable" >
    <tr id="RigaTitolo" class="menuTable" runat="server" >
        <td>Deleghe</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestione))
        {                  
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/Deleghe/InserimentoDelega.aspx" runat="server">Inserimento delega</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/Deleghe/GestioneDelegheSindacato.aspx" runat="server">Gestione deleghe</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/Deleghe/SbloccoDeleghe.aspx" runat="server">Sblocco deleghe</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheConferma))
        {
     %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/Deleghe/ConfermaConfermaDeleghe.aspx" runat="server">Conferma mensile</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/CeServizi/Deleghe/EsportazioneDeleghe.aspx" runat="server">Esportazione</a>
        </td>
    </tr>    
     <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestione))
        {
     %>
    <tr>
        <td>
            <a id="A6" href="~/CeServizi/Deleghe/GestioneDelegheSindacato.aspx?modalita=storico" runat="server">Storico</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestioneCE))
        {
     %>  
    <tr>
        <td>
            <a id="A7" href="~/CeServizi/Deleghe/GestioneDelegheCE.aspx?modalita=storico" runat="server">Gestione deleghe</a>
        </td>
    </tr>  
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheVisioneCE))
        {
     %>  
    <tr>
        <td>
            <a id="A11" href="~/CeServizi/Deleghe/VisualizzaDelegheCE.aspx?modalita=storico" runat="server">Visualizza deleghe</a>
        </td>
    </tr>  
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheSblocco))
        {
    %>
    <tr>
        <td>
            <a id="A8" href="~/CeServizi/Deleghe/DelegheBloccate.aspx" runat="server">Deleghe bloccate</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheGestioneCE))
        {
     %>
    <tr>
        <td>
            <a id="A9" href="~/CeServizi/Deleghe/GenerazioneLettera.aspx" runat="server">Lettere</a>
        </td>
    </tr> 
     <%
        }
            if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.DelegheEstrazioneMetadati))
        {
     %>
    <tr>
        <td>
            <a id="A10" href="~/CeServizi/Deleghe/EstrazioneArchiDoc.aspx" runat="server">Estrazione per ArchiDoc</a>
        </td>
    </tr> 
     <%
        }
    %>
</table>
<%
    }
 %>
