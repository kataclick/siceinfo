﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelezioneIndirizzo.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.SelezioneIndirizzo" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">
        function onSelectedIndexChanged(sender, eventArgs) {
            sender.selectText(0, 1);
        }

        function openRadWindow(latitudine,longitudine,indirizzo,cap,comune,provincia,note) {
            //var oWindow = radopen("../VisualizzazioneMappa.aspx?latitudine="+ latitudine +"&longitudine="+ longitudine +"&cap="+ cap +"&indirizzo="+ indirizzo +"&provincia="+ provincia +"&comune="+ comune +"&note="+ note, null);
            var oWindow = radopen("/Map/?latitudine="+ latitudine +"&longitudine="+ longitudine +"&cap="+ cap +"&indirizzo="+ indirizzo +"&provincia="+ provincia +"&comune="+ comune +"&note="+ note, null);
            oWindow.set_title("Cassa Edile");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 560);
            oWindow.center();
        }
    </script>

</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="~/CeServizi/images/favicon.ico" />
<asp:Panel ID="PanelIndirizzo" runat="server" CssClass="borderedTable" DefaultButton="ButtonGeocodifica">
    <telerik:RadMultiPage ID="RadMultiPageSceltaIndirizzo" runat="server" Width="100%"
        SelectedIndex="0" RenderSelectedPageOnly="true">
        <telerik:RadPageView ID="RadPageViewDatiIndirizzo" runat="server">
            <div class="borderedDiv">
                <table class="standardTable">
                    <tr>
                        <td class="iscrizioneLavoratoriTd">
                            Via/Piazza<b>*</b>:
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxIndirizzo" runat="server" Width="250px">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndirizzo" runat="server" ControlToValidate="RadTextBoxIndirizzo"
                                ErrorMessage="Inserire l'indirizzo" ValidationGroup="geocodifica" ForeColor="Red">
                                     *
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Civico:
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxCivico" runat="server" Width="250px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Provincia<b>*</b>:
                        </td>
                        <td>
                            <telerik:RadComboBox ID="RadComboBoxProvincia" runat="server" Width="250px" OnSelectedIndexChanged="RadComboBoxProvincia_SelectedIndexChanged"
                                AutoPostBack="True" EmptyMessage="- Selezionare la Provincia -" MarkFirstMatch="true"
                                EnableScreenBoundaryDetection="false">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorProvincia" runat="server" ErrorMessage="Selezionare la Provincia"
                                ValidationGroup="geocodifica" OnServerValidate="CustomValidatorProvincia_ServerValidate" ForeColor="Red">
                                     *
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comune<b>*</b>:
                        </td>
                        <td>
                            <telerik:RadComboBox ID="RadComboBoxComune" runat="server" Width="250px" EmptyMessage="- Selezionare il Comune -"
                                AllowCustomText="false" MarkFirstMatch="true" EnableScreenBoundaryDetection="false">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:CustomValidator ID="CustomValidatorComune" runat="server" ErrorMessage="Selezionare il Comune"
                                ValidationGroup="geocodifica" OnServerValidate="CustomValidatorComune_ServerValidate" ForeColor="Red">
                                     *
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cap<b>*</b>:
                        </td>
                        <td class="style2">
                            <telerik:RadTextBox ID="RadTextBoxCap" runat="server" Width="250px" MaxLength="5">
                            </telerik:RadTextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCap" runat="server" ControlToValidate="RadTextBoxCap"
                                ErrorMessage="Inserire il Cap" ValidationGroup="geocodifica" ForeColor="Red">
                                     *
                            </asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="CustomValidatorCap" runat="server" ControlToValidate="RadTextBoxCap"
                                OnServerValidate="CustomValidatorCAP_ServerValidate" ValidationGroup="geocodifica" ForeColor="Red">
                                    *
                            </asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonGeocodifica" runat="server" Text="Geocodifica" ValidationGroup="geocodifica"
                                OnClick="ButtonGeocodifica_Click" Width="150px" />
                        </td>
                        <td colspan="2">
                            <asp:ValidationSummary ID="ValidationSummaryIndirizzo" runat="server" ValidationGroup="geocodifica"
                                CssClass="messaggiErrore" />
                            <asp:Label ID="LabelNoGeocodifica" runat="server" Text="Non è stato possibile geocodificare l'indirizzo, verrà mantenuto quello dichiarato."
                                Visible="False" CssClass="messaggiErrore"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageViewGeocodifica" runat="server">
            <table class="standardTable">
                <tr>
                    <td>
                        <telerik:RadGrid ID="RadGridIndirizzi" runat="server" AutoGenerateColumns="False"
                            GridLines="None" OnItemDataBound="RadGridIndirizzi_ItemDataBound" CellSpacing="0">
                            <ClientSettings EnableRowHoverStyle="true">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <MasterTableView DataKeyNames="Latitudine,Longitudine,Via,Cap,Comune,Provincia">
                                <Columns>
                                    <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                        <ItemStyle Width="25px" />
                                    </telerik:GridClientSelectColumn>
                                    <telerik:GridBoundColumn DataField="Provincia" HeaderText="Provincia" UniqueName="column">
                                        <ItemStyle Width="30px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Comune" HeaderText="Comune" HtmlEncode="true"
                                        UniqueName="column1">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Via" HeaderText="Indirizzo" UniqueName="column2">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Cap" HeaderText="Cap" UniqueName="column3">
                                        <ItemStyle Width="30px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/CeServizi/images/maps.gif" Text="Visualizza sulla mappa" UniqueName="ViewMapColumn">
                                    <ItemStyle Width="30px" />
                                    </telerik:GridButtonColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        Selezionare l'indirizzo corrispondente a quello dichiarato e premere "Conferma selezione",
                        se l'indirizzo non è presente nella tabella cliccare "Mantieni indirizzo dichiarato".
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonConfermaIndirizzo" runat="server" Text="Conferma selezione"
                            Width="200px" OnClick="ButtonConfermaIndirizzo_Click" ValidationGroup="confermaIndirizzo" />
                        <asp:Button ID="ButtonMantieni" runat="server" Text="Mantieni indirizzo dichiarato"
                            Width="200px" OnClick="ButtonMantieni_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ValidationSummary ID="ValidationSummaryGeocodificaIndirizzo" runat="server"
                            ValidationGroup="confermaIndirizzo" CssClass="messaggiErrore" />
                        <asp:Label ID="LabelValidationIndirizzo" runat="server" Text="Scegliere almeno un indirizzo."
                            Visible="False" CssClass="messaggiErrore" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Panel>
