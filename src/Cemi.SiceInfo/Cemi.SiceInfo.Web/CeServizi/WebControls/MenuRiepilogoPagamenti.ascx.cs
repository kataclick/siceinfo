﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

namespace Cemi.SiceInfo.Web.CeServizi.WebControls
{
    public partial class MenuRiepilogoPagamenti : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void SetIdImpresa(Int32 idImpresa)
        {
            ViewState["IdImpresa"] = idImpresa;
        }

        protected void LinkButtonCartella_Click(object sender, EventArgs e)
        {
            if (GestioneUtentiBiz.IsConsulente())
            {
                if (ViewState["IdImpresa"] != null)
                {
                    Context.Items["IdImpresa"] = ViewState["IdImpresa"];
                    Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=cartella");
                }
            }
            else
            {
                Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=cartella");
            }
        }

        //protected void LinkButtonPremioFedelta_Click(object sender, EventArgs e)
        //{
        //    if (GestioneUtentiBiz.IsConsulente())
        //    {
        //        if (ViewState["IdImpresa"] != null)
        //        {
        //            Context.Items["IdImpresa"] = ViewState["IdImpresa"];
        //            Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=premioFedelta");
        //        }
        //    }
        //    else
        //    {
        //        Server.Transfer("~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=premioFedelta");
        //    }
        //}
    }
}