﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuTuteScarpeStatistiche.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuTuteScarpeStatistiche" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeStatistiche)
           )
        {                  
         %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Statistiche</td>
    </tr>  
    <tr>
        <td>
            <a id="a1" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheRiassunto.aspx" runat="server">Riassunto</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a2" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheFabbisogniProposti.aspx" runat="server">Fabbisogni proposti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a3" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheFabbisogniConfermati.aspx" runat="server">Fabbisogni confermati</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a4" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheOrdiniEffettuati.aspx" runat="server">Ordini predisposti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a5" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheOrdiniEvasi.aspx" runat="server">Ordini evasi</a>
        </td>
    </tr>
     <tr>
        <td>
            <a id="a6" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheOrdiniInevasi.aspx" runat="server">Ordini inevasi</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="a7" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheOrdiniConsegnati.aspx" runat="server">Ordini consegnati</a>
        </td>
    </tr>
     <tr>
        <td>
            <a id="a8" href="~/CeServizi/TuteScarpe/TuteScarpeStatisticheOrdiniNonConsegnati.aspx" runat="server">Ordini non consegnati</a>
        </td>
    </tr>
</table>
<%
   } 
    
    %>