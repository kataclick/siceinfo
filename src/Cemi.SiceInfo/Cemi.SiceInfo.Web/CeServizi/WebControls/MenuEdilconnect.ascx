﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuEdilconnect.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuEdilconnect" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%
    //if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
    //{
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Edilconnect
        </td>
    </tr>
    <tr>
        <td>
            <a id="A1" href="http://ww2.cassaedilemilano.it/ImpreseeConsulenti/InformazioniOperative/Edilconnect/tabid/211/language/it-IT/Default.aspx" runat="server">Info su Edilconnect</a>
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa() || GestioneUtentiBiz.IsConsulente())
        {
    %>
    <tr>
        <td>
            <a id="RedirectEdilconnectLink" href="~/CeServizi/Edilconnect/Edilconnect.aspx" runat="server">Accedi a Edilconnect</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    //}
%>