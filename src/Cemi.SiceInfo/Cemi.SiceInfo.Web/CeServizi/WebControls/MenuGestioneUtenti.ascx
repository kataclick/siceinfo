﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuGestioneUtenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuGestioneUtenti" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (
        (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti))
        )
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" runat="server" class="menuTable">
        <td>
            Gestione utenti
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestisciUtenti))
        {
    %>
    <tr>
        <td>
            <a runat="server" id="A1" href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazioneDipendente.aspx">Inserisci
                dipendenti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A2" href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazioneFornitore.aspx">Inserisci
                fornitore</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A3" href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazioneIspettori.aspx">Inserisci
                ispettore</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A10" href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazioneSindacalista.aspx">Inserisci
                sindacalista</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A11" href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazioneCommittente.aspx">Inserisci
                committente</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A12" href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazioneOspiti.aspx">Inserisci
                ospite</a>
        </td>
    </tr>
    <tr>
        <td>
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A4" href="~/CeServizi/GestioneUtenti/GestioneUtentiUtenti_Ruoli.aspx">Gestisci utenti</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A5" href="~/CeServizi/GestioneUtenti/GestioneUtentiCreaRuolo.aspx">Crea ruolo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A6" href="~/CeServizi/GestioneUtenti/GestioneUtentiRuoli_Funzionalita.aspx">Gestisci ruoli</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A7" href="~/CeServizi/GestioneUtenti/ResetPassword.aspx">Reset password</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A14" href="~/CeServizi/GestioneUtenti/InserimentoReport.aspx">Inserisci
                Report</a>
        </td>
    </tr>
    <tr>
        <td>
            <a runat="server" id="A13" href="~/CeServizi/GestioneUtenti/AssociazioneUtentiReport.aspx">Gestisci
                Report</a>
        </td>
    </tr>
    <%
        }

    %>
</table>
<%
    }
%>

