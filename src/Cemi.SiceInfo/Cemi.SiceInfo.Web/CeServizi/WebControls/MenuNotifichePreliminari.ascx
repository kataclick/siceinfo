﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuNotifichePreliminari.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuNotifichePreliminari" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Notifiche preliminari</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariRicercaNotifiche)
            )
    {                  
    %>
    <tr>
        <td>
        <a id="A1" href="~/CeServizi/NotifichePreliminari/RicercaNotifiche.aspx" runat="server">Ricerca notifiche</a>
        </td>
    </tr>
    <%
    }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri))
    {                  
    %>
    <tr>
        <td>
        <a id="A2" href="~/CeServizi/NotifichePreliminari/RicercaCantieri.aspx" runat="server">Ricerca cantieri</a>
        </td>
    </tr>
    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa)
        )
    {                  
    %>
     <tr>
        <td>
        <a id="A3" href="~/CeServizi/NotifichePreliminari/RicercaCantieriGeolocalizzazione.aspx" runat="server">Localizzazione cantieri</a>
        </td>
    </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariEstrazioneBrescia))
    {                  
    %>
        <tr>
            <td>
                <a id="A9" href="~/CeServizi/NotifichePreliminari/EstrazioneBrescia.aspx" runat="server">Estrazione Brescia</a>
            </td>
        </tr>
    <%
    }
     %>
     <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariEstrazioneAssimpredil))
    {                  
    %>
        <tr>
            <td>
                <a id="A10" href="~/CeServizi/NotifichePreliminari/EstrazioneAssimpredil.aspx" runat="server">Estrazione Assimpredil</a>
            </td>
        </tr>
    <%
    }
     %>
</table>
<% 
    }
    %>
