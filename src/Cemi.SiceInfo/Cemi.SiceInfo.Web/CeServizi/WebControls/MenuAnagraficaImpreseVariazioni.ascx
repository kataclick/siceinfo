﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuAnagraficaImpreseVariazioni.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuAnagraficaImpreseVariazioni" %>

<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Variazioni Impresa
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRichiestaVariazioneRecapiti))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/AnagraficaImprese/GestioneRecapiti.aspx" runat="server">Variazione
                Recapiti</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseGestioneStato))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/AnagraficaImprese/GestioneStato.aspx" runat="server">Variazione Stato</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseGestioneRichiesteCambioStato))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/AnagraficaImprese/GestioneRichiesteCambioStato.aspx" runat="server">
                Gestione Variazione Stato</a>
        </td>
    </tr>
    <%
        }
    %>
</table>