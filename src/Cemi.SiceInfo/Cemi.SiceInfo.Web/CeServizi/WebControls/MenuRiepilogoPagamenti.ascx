﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuRiepilogoPagamenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuRiepilogoPagamenti" %>

<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.InfoImpresa))
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>
            Riepilogo pagamenti
        </td>
    </tr>
    <tr>
        <td>
            <asp:LinkButton
                ID="LinkButtonCartella" 
                runat="server"
                Text="Cartella" onclick="LinkButtonCartella_Click">
            </asp:LinkButton>
            <%--<a id="A1" href="ReportImprese.aspx?tipo=cartella">Cartella</a>--%>
        </td>
    </tr>
    <%--<tr>
        <td>
            <asp:LinkButton
                ID="LinkButtonPremioFedelta" 
                runat="server"
                Text="Premio fedeltà" onclick="LinkButtonPremioFedelta_Click">
            </asp:LinkButton>
            <%--<a id="A2" href="ReportImprese.aspx?tipo=premioFedelta">Premio fedeltà</a>--%>
        <%--</td>
    </tr>--%>
    <%--<%
        if (GestioneUtentiBiz.IsImpresa())
        {
    %>--%>
    <%--<tr>
        <td>
            <a id="A3" href="~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=trattamentoCIGO" runat="server">Prestazione sociale trattamento C.I.G.O.</a>
        </td>
    </tr>--%>
    <%--<%
        }
    %>--%>
</table>
<%
    }
%>