﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LiberatoriaPrivacy.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.LiberatoriaPrivacy" %>
<table class="standardTable">
    <tr>
        <td colspan="2">
            <b>CONSENSO ALL’ATTO DI REGISTRAZIONE DELL’UTENTE</b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div style="text-align:justify">
                Dopo aver letto l’informativa di cui alla sezione imprese/lavoratori del presente sito internet 
				prendo atto che il trattamento dei dati personali, di cui al Regolamento Europeo 2016/679, 
				avverrà in piena conformità all’informativa pubblicata ed alla richiamata normativa in materia di tutela dei dati personali. 
				Acconsento, quindi, al trattamento dei dati per le finalità di cui al punto 1) della già citata informativa. 
			</div>
            <br />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <center>
                <asp:RadioButtonList ID="RadioButtonListPrivacy" runat="server" RepeatDirection="Horizontal"
                    Width="300px">
                    <asp:ListItem>Consento</asp:ListItem>
                    <asp:ListItem Selected="True">Non Consento</asp:ListItem>
                </asp:RadioButtonList>
                &nbsp;</center>
        </td>
    </tr>
</table>