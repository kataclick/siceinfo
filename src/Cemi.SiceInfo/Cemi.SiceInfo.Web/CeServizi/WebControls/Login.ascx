﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.WebControls_Login" %>

<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Login <%= ConfigurationManager.AppSettings["Environment"] %>
        </td>
    </tr>
    <tr>
        <td>
            <asp:LoginView
                ID="LoginView1"
                runat="server">
                <AnonymousTemplate>
                    <asp:Login
                        ID="Login1"
                        runat="server" TextLayout="TextOnTop" 
                        FailureText="Credenziali fornite non valide" 
                        PasswordRequiredErrorMessage="Password obbligatoria" 
                        RememberMeText="Ricordati di me" TitleText="" UserNameLabelText="Username:" 
                        UserNameRequiredErrorMessage="Username obbligatorio" Width="100%" 
                        LoginButtonText="Login" onloggedin="Login1_LoggedIn" 
                        onauthenticate="Login1_Authenticate">
                        <TextBoxStyle Width="180px" />
                        <LoginButtonStyle Width="100%" />
                    </asp:Login>
                </AnonymousTemplate>
                <LoggedInTemplate>
                    Benvenuto 
                    <b>
                        <asp:LoginName runat="server" />
                    </b>
                    <br />
                    <a href="~/CeServizi/GestioneUtenti/GestioneUtentiCambiaPassword.aspx" runat="server">Cambia password</a>
                    <br />
                    <br />
                    <asp:LoginStatus
                        ID="LoginStatus1"
                        runat="server" LogoutPageUrl="~/CeServizi/Default.aspx" LogoutText="Disconnetti" 
                        onloggedout="LoginStatus1_LoggedOut" />
                </LoggedInTemplate>
            </asp:LoginView>
        </td>
    </tr>
    <tr>
        <td >
            <asp:LoginView
                ID="LoginView3"
                runat="server">
                <AnonymousTemplate>
                    <center>
                        <a href="~/CeServizi/GestioneUtenti/GestioneUtentiRecuperoPassword.aspx" runat="server">
                                Password dimenticata?
                        </a>
                        <br />
                        <a href="~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazione.aspx" runat="server">
                                Registrazione
                        </a>
                    </center>
                </AnonymousTemplate>
            </asp:LoginView>
        </td>
    </tr>
</table>
