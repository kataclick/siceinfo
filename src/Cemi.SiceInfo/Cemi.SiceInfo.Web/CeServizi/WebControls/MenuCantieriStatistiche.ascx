﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuCantieriStatistiche.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuCantieriStatistiche" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatistichePerIspettore)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatisticheGenerali)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatistichePerIspettoreRUI)
    )
   {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td >Statistiche</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatistichePerIspettore)
            ||
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatistichePerIspettoreRUI)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A1" href="~/CeServizi/Cantieri/StatistichePerIspettore.aspx" runat="server">Statistiche per ispettore</a>
        </td>
    </tr>
       <%
       }
       if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriStatisticheGenerali)
           )
        {                  
         %>
    <tr>       
        <td>
            <a id="A2" href="~/CeServizi/Cantieri/StatisticheGenerali.aspx" runat="server">Statistiche generali</a>
        </td>
    </tr>
     <%
       }
     %>
</table>
<%
    }
%>