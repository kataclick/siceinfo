﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.Footer" %>

<div id="footer">
    <strong>Cassa edile di mutualità e assistenza di Milano, Lodi, Monza e Brianza © 2009</strong>
    <br />
    <strong>C.F.:</strong> 80038030153
    <br />
    <strong>Indirizzo:</strong> Via San Luca 6 20122 - MILANO - MI
    <br />
    <strong>Telefono:</strong> 02/584961
</div>