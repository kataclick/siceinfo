﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuCantieri.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuCantieri" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriProgrammazioneGestione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrev)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestioneGruppi)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriSegnalazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriAssegnazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriEstrazioneLodi)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Cantieri</td>
    </tr>
    <%
        //if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriGestioneZone.ToString())
        //   )
        //{                  
    %>
    <%--<tr>
        <td>
            <a id="A1" href="~/Cantieri/CantieriGestioneZone.aspx" runat="server">Gestione zone</a>
        </td>
    </tr>--%>
    <%
        //}
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestioneGruppi)
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A10" href="~/CeServizi/Cantieri/GestioneGruppi.aspx" runat="server">Gestione gruppi</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestione)
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx" runat="server">Gestione cantieri</a>
        </td>
    </tr>
    <% } %>
    <%--<% 
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione.ToString())
           || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriProgrammazioneGestione.ToString())
          )
      {                  
    %>
    <tr>
        <td>
            <a id="A3" href="~/Cantieri/CantieriProgrammazioneSettimanale.aspx" runat="server">Programmazione
                settimanale</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrev.ToString())
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString())
           )
      {                  
    %>
    <tr>
        <td>
            <a id="A4" href="~/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=preventivo"
                runat="server">Preventivo settimanale</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=consuntivo"
                runat="server">Consuntivo settimanale</a>
        </td>
    </tr>
    <% } %>--%>
    <%
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestione)
           )
      {                  
    %>
    <tr>
        <td>
            <a id="A6" href="~/CeServizi/Cantieri/CantieriLocalizzazioneMultipla.aspx" runat="server">Localizzazione</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrev)
          || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere)
       )
      {                  
    %>
    <tr>
        <td>
            <a id="A7" href="~/CeServizi/Cantieri/CantieriRicercaIspezioni.aspx" runat="server">Ricerca ispezioni</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriModificaIspettori)
            )
        {                  
    %>
    <tr>
        <td>
            <a id="A8" href="~/CeServizi/Cantieri/CantieriModificaIspettore.aspx" runat="server">Modifica ispettori</a>
        </td>
    </tr>
    <%
        }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere)
            )
        {    %>
    <tr>
        <td>
            <a id="A9" href="~/CeServizi/Cantieri/CantieriLogLettere.aspx" runat="server">Log Lettere</a>
        </td>
    </tr>
    <%
        } 
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsIspettore())
        {    %>
    <tr>
        <td>
            <a id="A11" href="~/CeServizi/Cantieri/CalendarioAttivita.aspx" runat="server">Calendario attività</a>
        </td>
    </tr>
    <%
        } if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestioneAttivita))
        {    %>
    <tr>
        <td>
            <a id="A12" href="~/CeServizi/Cantieri/ElencoAttivita.aspx" runat="server">Elenco attività</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A13" href="~/CeServizi/Cantieri/NumeriAttivita.aspx" runat="server">Stato attività</a>
        </td>
    </tr>
    <%
        } 
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriEstrazioneLodi))
        {    %>
    <tr>
        <td>
            <a id="A14" href="~/CeServizi/Cantieri/EstrazioneLodi.aspx" runat="server">Estrazione Lodi</a>
        </td>
    </tr>
    <%
        } 
    %>
</table>
<%
    }
%>
