﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OreCNCEControllo.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.OreCNCEControllo" %>

<%@ Register Src="OreCNCECaricamentoManuale.ascx" TagName="OreCNCECaricamentoManuale"
    TagPrefix="uc3" %>

<table class="standardTable">
    <tr>
        <td>
            <b>Casse Edili di provenienza </b>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewCasseEdili" runat="server" AutoGenerateColumns="False"
                DataKeyNames="IdCassaEdile" OnRowDeleting="GridViewCasseEdili_RowDeleting" Width="100%"
                OnSelectedIndexChanging="GridViewCasseEdili_SelectedIndexChanging" OnRowDataBound="GridViewCasseEdili_RowDataBound"
                OnRowCommand="GridViewCasseEdili_RowCommand">
                <Columns>
                    <asp:BoundField DataField="IdCassaEdile" HeaderText="Codice">
                        <ItemStyle Width="50px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                    <asp:CheckBoxField DataField="Cnce" HeaderText="Presente nel CNCE">
                        <ItemStyle Width="100px" />
                    </asp:CheckBoxField>
                    <asp:CheckBoxField DataField="InseritaManualmente" HeaderText="Inserita man.">
                        <ItemStyle Width="10px" />
                    </asp:CheckBoxField>
                    <%-- <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Carica ore manualmente"
                        ShowDeleteButton="True">
                        <ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                        <ItemStyle Width="10px" />
                    </asp:CommandField>--%>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonCaricaManuale" runat="server" CommandName="carica" Text="Carica ore manualmente" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                         <ItemStyle Width="10px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonRichiediOreCnce" runat="server" Text="Richiedi ore al CNCE"
                Enabled="False" OnClick="ButtonRichiediOreCnce_Click" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <b>Ore caricate </b>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelSegnalazione" runat="server" ForeColor="Red" Text="ATTENZIONE: Le ore segnalate in rosso superano il limite di 200 ore mensili e pertanto non verranno conteggiate. Prendere contatto con la Cassa Edile pertinente per risolvere la situazione."> </asp:Label>
            <asp:GridView ID="GridViewOreCaricate" runat="server" AutoGenerateColumns="False"
                Width="100%" AllowPaging="True" OnPageIndexChanging="GridViewOreCaricate_PageIndexChanging"
                PageSize="5" OnRowDataBound="GridViewOreCaricate_RowDataBound" DataKeyNames="IdLavoratore,IdCassaEdile,Anno,Mese"
                OnRowCommand="GridViewOreCaricate_RowCommand" OnRowDeleting="GridViewOreCaricate_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="Anno" HeaderText="Anno">
                        <ItemStyle Width="40px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Mese" HeaderText="Mese">
                        <ItemStyle Width="40px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdCassaEdile" HeaderText="Cassa Edile">
                        <ItemStyle Width="40px" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="InseriteManualmente" HeaderText="Man." />
                    <asp:BoundField DataField="OreLavorate" HeaderText="Ore lavorate">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OreFerie" HeaderText="Ore ferie">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OreInfortunio" HeaderText="Ore infortunio">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OreMalattia" HeaderText="Ore malattia">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OreCassaIntegrazione" HeaderText="Ore cassa integr.">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OrePermessoRetribuito" HeaderText="Ore perm. retr.">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OrePermessoNonRetribuito" HeaderText="Ore perm. non retr.">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="OreAltro" HeaderText="Ore altro">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LivelloErogazione" HeaderText="Livello erog.">
                        <ItemStyle Width="10px" />
                    </asp:BoundField>
                    <%-- <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Modifica"
                            ShowEditButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                            <ItemStyle Width="10px" />
                        </asp:CommandField>--%>
                    <%--  <asp:ButtonField ButtonType="Button" CommandName="Modifica" Text="Modifica" ControlStyle-CssClass="bottoneGriglia">
                        <ItemStyle Width="10px" />
                    </asp:ButtonField>--%>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonModifica" runat="server" CommandName="Modifica" Text="Modifica" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Button ID="ButtonElimina" runat="server" CommandName="delete" Text="Elimina" />
                                    </td>
                                </tr>
                                <tr id="trConfermaEliminazione" runat="server" visible="false">
                                    <td>
                                        Confermi l'eliminazione?
                                        <asp:Button ID="ButtonConfermaEliminazioneSi" CommandName="confermaDeleteSi" runat="server"
                                            Text="Sì" />
                                        <asp:Button ID="ButtonConfermaEliminazioneNo" CommandName="confermaDeleteNo" runat="server"
                                            Text="No" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna ora presente per il lavoratore
                </EmptyDataTemplate>
            </asp:GridView>
            <br />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="PanelCaricamentoManuale" runat="server" Visible="false">
                <table class="standardTable">
                    <tr>
                        <td>
                            <uc3:OreCNCECaricamentoManuale ID="PrestazioniCaricamentoManualeOre1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryCaricamentoManuale" runat="server" ValidationGroup="inserimentoOre"
                                CssClass="messaggiErrore" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="ButtonCaricaOre" runat="server" Text="Carica ore" ValidationGroup="inserimentoOre"
                                OnClick="ButtonCaricaOre_Click" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>