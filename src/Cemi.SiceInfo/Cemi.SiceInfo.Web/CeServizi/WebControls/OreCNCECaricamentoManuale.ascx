﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OreCNCECaricamentoManuale.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.OreCNCECaricamentoManuale" %>

<table class="standardTable">
    <tr>
        <td colspan="3">
            <b>Inserimento manuale ore </b>
        </td>
    </tr>
    <tr>
        <td>
            Cassa edile:
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBoxCassaEdile" runat="server" Width="300px" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Anno (aaaa):
        </td>
        <td>
            <asp:TextBox ID="TextBoxAnno" runat="server" Width="300px" MaxLength="4"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAnno" runat="server" ErrorMessage="Anno mancante"
                ValidationGroup="inserimentoOre" ControlToValidate="TextBoxAnno">*</asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidatorAnno" runat="server" ErrorMessage="Formato anno errato"
                MaximumValue="2100" MinimumValue="1990" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxAnno">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Mese (mm):
        </td>
        <td>
            <asp:TextBox ID="TextBoxMese" runat="server" Width="300px" MaxLength="2"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorMese" runat="server" ErrorMessage="Mese mancante"
                ValidationGroup="inserimentoOre" ControlToValidate="TextBoxMese">*</asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidatorMese" runat="server" ErrorMessage="Formato mese errato"
                MaximumValue="12" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxMese">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore lavorate:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOreLavorate" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOreLavorate" runat="server" ErrorMessage="Formato ore lavorate errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOreLavorate">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore ferie:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOreFerie" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOreFerie" runat="server" ErrorMessage="Formato ore ferie errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOreFerie">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore infortunio:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOreInfortunio" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOreInfortunio" runat="server" ErrorMessage="Formato ore infortunio errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOreInfortunio">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore malattia:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOreMalattia" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOreMalattia" runat="server" ErrorMessage="Formato ore malattia errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOreMalattia">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore cassa integrazione:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOreCassaIntegrazione" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOreCassaIntegrazione" runat="server" ErrorMessage="Formato ore cassa integrazione errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOreCassaIntegrazione">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore permessi retribuiti:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOrePermessiRetribuiti" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOrePermessiRetribuiti" runat="server" ErrorMessage="Formato ore permessi retribuiti errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOrePermessiRetribuiti">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore permessi non retribuiti:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOrePermessiNonRetribuiti" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOrePermessiNonRetribuiti" runat="server" ErrorMessage="Formato ore permessi non retribuiti errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOrePermessiNonRetribuiti">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ore altro:
        </td>
        <td>
            <asp:TextBox ID="TextBoxOreAltro" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorOreAltro" runat="server" ErrorMessage="Formato ore altro errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxOreAltro">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>
            Livello erogazione:
        </td>
        <td>
            <asp:TextBox ID="TextBoxLivelloErogazione" runat="server" Width="300px" MaxLength="3"></asp:TextBox>
        </td>
        <td>
            <asp:RangeValidator ID="RangeValidatorLivelloErogazione" runat="server" ErrorMessage="Formato livello erogazione errato"
                MaximumValue="744" MinimumValue="1" Type="Integer" ValidationGroup="inserimentoOre"
                ControlToValidate="TextBoxLivelloErogazione">*</asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:CustomValidator ID="CustomValidatorQualcheOraPresente" runat="server" ValidationGroup="inserimentoOre"
                ErrorMessage="Non è stata indicata nessuna ora" OnServerValidate="CustomValidatorQualcheOraPresente_ServerValidate">*</asp:CustomValidator>
        </td>
    </tr>
</table>
