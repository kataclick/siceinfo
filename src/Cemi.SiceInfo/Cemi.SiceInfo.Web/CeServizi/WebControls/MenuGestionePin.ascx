﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuGestionePin.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuGestionePin" %>

<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<table class="menuTable">
    <tr id="RigaTitolo" runat="server" class="menuTable">
        <td>
            Gestione PIN
        </td>
    </tr>
    <%

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePIN))
        {
    %>
    <tr>
        <td>
            <a id="A7" href="~/CeServizi/GestioneUtenti/GestioneUtentiGestionePIN.aspx" runat="server">Gestisci PIN aziende</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINLavoratori))
        {
    %>
    <tr>
        <td>
            <a id="A8" href="~/CeServizi/GestioneUtenti/GestioneUtentiGestionePINLavoratori.aspx" runat="server">Gestisci PIN lavoratori</a>
        </td>
    </tr>
    <%
        }
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.GestioneUtentiGestionePINConsulenti))
        {
    %>
    <tr>
        <td>
            <a id="A9" href="~/CeServizi/GestioneUtenti/GestioneUtentiGestionePINConsulenti.aspx" runat="server">Gestisci PIN consulenti</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
