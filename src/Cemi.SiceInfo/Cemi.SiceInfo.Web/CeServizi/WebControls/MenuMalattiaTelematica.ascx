﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuMalattiaTelematica.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuMalattiaTelematica" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeImpresa)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeBackOffice)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaEstrattoConto)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Malattia / Infortunio
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeImpresa))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/MalattiaTelematica/RicercaAssenze.aspx" runat="server">Gestione assenze</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeBackOffice))
        {
    %>
    <tr>
        <td>
            <a id="A6" href="~/CeServizi/MalattiaTelematica/RicercaAssenzeBackOffice.aspx" runat="server">
                Gestione assenze</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaUploadCertificati))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/MalattiaTelematica/UploadCertificati.aspx" runat="server">Caricamento file XML</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaEstrattoConto))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/MalattiaTelematica/EstrattoConto.aspx" runat="server">Estratto conto</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaMessaggi))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/MalattiaTelematica/RicercaMessaggi.aspx" runat="server">Ricerca messaggi</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaErrori))
        {
    %>
    <tr>
        <td>
            <a id="A8" href="~/CeServizi/MalattiaTelematica/RicercaErrori.aspx" runat="server">Ricerca errori</a>
        </td>
    </tr>
    <%
        }
    %>
	<%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaRicercaAssenzeInAttesa))
        {
    %>
    <tr>
        <td>
            <a id="A9" href="~/CeServizi/MalattiaTelematica/RicercaAssenzaInAttesa.aspx" runat="server">Ricerca assenze in attesa</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaStatisticaLiquidazioni))
        {
    %>
    <tr>
        <td>
            <a id="A5" href="~/CeServizi/MalattiaTelematica/StatisticaLiquidazioni.aspx" runat="server">Statistica
                liquidazioni</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.MalattiaTelematicaStatisticaEvase))
        {
    %>
    <tr>
        <td>
            <a id="A7" href="~/CeServizi/MalattiaTelematica/StatisticaEvase.aspx" runat="server">Statistica
                evase</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
