﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuPrestazioni.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuPrestazioni" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniCompilazioneDomandaFast)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniGestioneDomande)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore)
    )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Prestazioni</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda))
    {                  
    %>
    <tr>
        <td>
        <a id="A1" href="~/CeServizi/Prestazioni/CompilazioneDomanda.aspx" runat="server">Compilazione domanda</a>
        </td>
    </tr>
    <%
    }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniCompilazioneDomandaFast))
    {                  
    %>
    <tr>
        <td>
        <a id="A5" href="~/CeServizi/Prestazioni/CompilazioneDomandaFast.aspx" runat="server">Compilazione domanda FAST</a>
        </td>
    </tr>
    <%
    }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniGestioneDomande))
    {                  
    %>
    <tr>
        <td>
        <a id="A2" href="~/CeServizi/Prestazioni/GestioneDomande.aspx?menu=si" runat="server">Gestione domande</a>
        </td>
    </tr>
    <%
    }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore()
            && TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore))
    {                  
    %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/Prestazioni/GestioneDomandeNonConfermate.aspx" runat="server">Domande non confermate</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/Prestazioni/GestioneDomandePersonali.aspx" runat="server">Storico domande</a>
        </td>
    </tr>
    <%
    }
    %>
</table>
<% 
    }
    %>
