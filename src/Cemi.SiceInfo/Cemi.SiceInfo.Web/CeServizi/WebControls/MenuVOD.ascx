﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuVOD.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuVOD" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.VodElencoImpreseConsulenti)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.VodElencoImpreseMedie))
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            VCOD
        </td>
    </tr>
    <%

        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.VodElencoImpreseMedie))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/Vcod/ReportVOD.aspx?tipo=ImpreseMedie" runat="server">Elenco Imprese con fasce</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
