﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuBackToConenuti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuBackToConenuti" %>

<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Home page sito
        </td>
    </tr>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/Default.aspx" runat="server">Torna al sito servizi</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="http://www.cassaedilemilano.it">Torna al sito istituzionale</a>
        </td>
    </tr>
</table>