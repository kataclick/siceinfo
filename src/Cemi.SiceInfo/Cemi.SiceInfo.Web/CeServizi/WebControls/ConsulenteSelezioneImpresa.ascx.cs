﻿using System;
using System.Drawing;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Delegates;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.WebControls
{
    public partial class ConsulenteSelezioneImpresa : System.Web.UI.UserControl
    {
        private readonly Common commonBiz = new Common();
        public event ImpresaSelectedEventHandler OnImpresaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (GestioneUtentiBiz.IsConsulente())
                {
                    Consulente consulente =
                        (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    Int32 idConsulente = consulente.IdConsulente;

                    CaricaImpreseAssociateAlConsulente(idConsulente);
                    CaricaImpresaSelezionata(idConsulente, false);
                }
                //else
                //{
                //    throw new Exception("ConsulenteSelezioneImpresa: il controllo può essere utilizzato solo dai consulenti");
                //}
            }
        }

        private void CaricaImpresaSelezionata(Int32 idConsulente, Boolean lanciaEvento)
        {
            Int32 idImpresa;
            String ragioneSociale;

            commonBiz.ConsulenteImpresaSelezionata(idConsulente, out idImpresa, out ragioneSociale);

            if (idImpresa > 0)
            {
                LabelImpresaSelezionata.Text = String.Format("{0} - {1}", idImpresa, ragioneSociale);
                LabelImpresaSelezionata.ForeColor = Color.Black;
            }
            else
            {
                LabelImpresaSelezionata.Text = "NESSUNA";
                LabelImpresaSelezionata.ForeColor = Color.Red;
            }

            if (lanciaEvento)
            {
                if (OnImpresaSelected != null)
                {
                    OnImpresaSelected(idImpresa, ragioneSociale);
                }
            }
        }

        protected void ButtonSeleziona_Click(object sender, EventArgs e)
        {
            Consulente consulente =
                (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();


            if (!string.IsNullOrEmpty(RadComboBoxImprese.SelectedValue))
            {
                commonBiz.ConsulenteSelezionaImpresa(consulente.IdConsulente,
                                                     Int32.Parse(RadComboBoxImprese.SelectedValue));
            }
            else
            {
                commonBiz.ConsulenteSelezionaImpresa(consulente.IdConsulente, null);
            }

            CaricaImpresaSelezionata(consulente.IdConsulente, true);

            RadComboBoxImprese.ClearSelection();
            RadComboBoxImprese.Text = String.Empty;
        }

        private void CaricaImpreseAssociateAlConsulente(int idConsulente)
        {
            Presenter.CaricaElementiInDropDown(
                RadComboBoxImprese,
                commonBiz.ImpreseConsulente(idConsulente),
                "NomeComposto",
                "IdImpresa");
        }

        public Int32 GetIdImpresaSelezionata()
        {
            Int32 idImpresa;

            if (GestioneUtentiBiz.IsConsulente())
            {
                Consulente consulente =
                    (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                Int32 idConsulente = consulente.IdConsulente;
                String ragioneSociale;

                commonBiz.ConsulenteImpresaSelezionata(idConsulente, out idImpresa, out ragioneSociale);
            }
            else
            {
                throw new Exception(
                    "GetIdImpresaSelezionata: si può utilizzare questo metodo solo se l'utente loggato è un consulente.");
            }

            return idImpresa;
        }
    }
}