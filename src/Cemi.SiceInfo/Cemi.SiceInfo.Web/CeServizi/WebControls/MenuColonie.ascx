﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuColonie.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuColonie" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneDomande)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneExport)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneMatrice)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneVacanze)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieSchedaPartecipante)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieCalcoloCosti)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieInserimentoDomandeACE)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieComunicazioniACE)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale)
    )
   {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server" >
        <td>Colonie</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneVacanze)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A1" href="~/CeServizi/Colonie/ColonieGestioneDestinazioni.aspx" runat="server">Gestione destinazioni</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneVacanze)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A2" href="~/CeServizi/Colonie/ColonieGestioneVacanze.aspx" runat="server">Gestione vacanze</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneMatrice)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A3" href="~/CeServizi/Colonie/ColonieMatriceRichiestaDisponibilita.aspx" runat="server">Matrice R/D</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneDomande)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A4" href="~/CeServizi/Colonie/ColonieGestioneDomande.aspx" runat="server">Gestione domande</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneExport)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A5" href="~/CeServizi/Colonie/ColonieEsportaDomande.aspx" runat="server">Esportazione domande</a>
        </td>
    </tr>
    <%}
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieCalcoloCosti)
           )
        {                  
         %>
    <tr>
        <td>
        <a id="A6" href="~/CeServizi/Colonie/ColonieCalcoloCosti.aspx" runat="server">Calcolo costi penalità</a>
        </td>
    </tr>
    <%} %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestionePrenotazioni)
          )
        {                  
         %>
    <tr>
        <td>
        <a id="A7" href="~/CeServizi/Colonie/ColonieGestionePrenotazioni.aspx" runat="server">Gestione prenotazioni</a>
        </td>
    </tr>
    <%} %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneDomandeACE)
          )
        {                  
         %>
    <tr>
        <td>
        <a id="A8" href="~/CeServizi/Colonie/ColonieGestioneDomandeACE.aspx" runat="server">Gestione domande ACE</a>
        </td>
    </tr>
    <% } %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieComunicazioniACE)
          )
        {                  
         %>
    <tr>
        <td>
        <a id="A9" href="~/CeServizi/Colonie/ColonieGestioneComunicazioniACE.aspx" runat="server">Gestione comunicazioni ACE</a>
        </td>
    </tr>
    <%} %>
    <%
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieInserimentoDomandeACE)
          && TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsCassaEdile() 
          )
        {                  
         %>
    <tr>
        <td>
        <a id="A10" href="~/CeServizi/Colonie/ColonieInserimentoPrenotazione.aspx" runat="server">Prenotazioni</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A11" href="~/CeServizi/Colonie/ColonieIscrizioneAltreCasseEdili.aspx" runat="server">Inserimento richieste</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A12" href="~/CeServizi/Colonie/ColonieIscrizioneAltreCasseEdiliGestione.aspx" runat="server">Gestione richieste</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A13" href="~/CeServizi/Colonie/ColonieIscrizioneAltreCasseEdiliGestioneTurniEffettivi.aspx" runat="server">Visualizza turni effettivi</a>
        </td>
    </tr>
    <%} %>
    <%
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieGestioneRichiestePersonale)
          )
        {                  
         %>
    <tr>
        <td>
        <a id="A14" href="~/CeServizi/Colonie/ColonieGestioneRichieste.aspx" runat="server">Gestione candidature</a>
        </td>
    </tr>
    <tr>
        <td>
        <a id="A15" href="~/CeServizi/Colonie/ColonieCalendarioColloqui.aspx" runat="server">Calendario colloqui</a>
        </td>
    </tr>
    <%} %>
    <%
      if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ColonieEstrazioneINAZ)
          )
        {                  
         %>
    <tr>
        <td>
        <a id="A16" href="~/CeServizi/Colonie/ColonieEstrazioneInaz.aspx" runat="server">Estrazione INAZ</a>
        </td>
    </tr>
    <%} %>
</table>
<%} %>