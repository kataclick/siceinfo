﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

namespace Cemi.SiceInfo.Web.CeServizi.WebControls
{
    public partial class WebControls_Login : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void LinkButtonRegistrati_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/GestioneUtenti/GestioneUtentiRegistrazione.aspx");
        }

        protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Default.aspx");
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            if (Request.QueryString["ReturnUrl"] != null)
            {
                Response.Redirect(Request.QueryString["ReturnUrl"]);
            }
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            string username = ((Login)LoginView1.FindControl("Login1")).UserName;
            string password = ((Login)LoginView1.FindControl("Login1")).Password;

            if (Membership.ValidateUser(username, password))
            {
                //controllo se password scaduta
                DateTime dataScadenza = new GestioneUtentiBiz().GetScadenzaPasswordFromUsername(username);

                if (DateTime.Today > dataScadenza)
                {
                    Response.Redirect(String.Format("~/CeServizi/GestioneUtenti/GestioneUtentiCambiaPassword.aspx?username={0}&scaduta=true", Server.UrlEncode(username)));
                }
                else
                {
                    e.Authenticated = true;
                }
            }
            else
            {
                e.Authenticated = false;
            }
        }
    }
}