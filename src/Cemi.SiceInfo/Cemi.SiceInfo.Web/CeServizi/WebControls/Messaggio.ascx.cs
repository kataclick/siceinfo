﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cemi.SiceInfo.Web.CeServizi.WebControls
{
    public partial class Messaggio : System.Web.UI.UserControl
    {
        private string _label = "";
        private string _titolo = "Messaggio";
        
        public string TestoMessaggio
        {
            get { return _label; }
            set
            {
                _label = value;
                Label1.Text = _label;
            }
        }

        public string Titolo
        {
            get { return _titolo; }
            set { _titolo = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}