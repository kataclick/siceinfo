﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuIscrizioneLavoratori.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuIscrizioneLavoratori" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE)
		|| TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Notifiche lavoratori
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/IscrizioneLavoratori/IscrizioneLavoratore.aspx" runat="server">Iscrizione</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/IscrizioneLavoratori/IscrizioneLavoratoreSintesiInterfaccia.aspx" runat="server">Iscrizione Sintesi</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
         if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizione)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriIscrizioneSintesiInterfaccia)
            )
        {
    %>
    <tr>
        <td>
            <a id="A5" href="~/CeServizi/IscrizioneLavoratori/Ricerca.aspx" runat="server">Ricerca</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneLavoratoriGestioneCE))
        {
    %>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/IscrizioneLavoratori/GestioneCE.aspx" runat="server">Gestione</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>