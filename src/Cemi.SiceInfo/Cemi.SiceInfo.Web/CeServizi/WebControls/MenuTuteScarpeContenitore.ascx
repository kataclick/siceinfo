﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuTuteScarpeContenitore.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuTuteScarpeContenitore" %>

<%@ Register Src="MenuTuteScarpeStatistiche.ascx" TagName="TuteScarpeMenuStatistiche" TagPrefix="uc4" %>
<%@ Register Src="MenuTuteScarpe.ascx" TagName="MenuTuteScarpe" TagPrefix="uc1" %>
<%@ Register Src="MenuTuteScarpeGestioneOrdini.ascx" TagName="MenuGestioneOrdini" TagPrefix="uc2" %>
<%@ Register Src="MenuTuteScarpeGestioneFatturazione.ascx" TagName="MenuGestioneFatturazione" TagPrefix="uc3" %>

<uc1:MenuTuteScarpe ID="MenuTuteScarpe1" runat="server" />
<uc2:MenuGestioneOrdini ID="MenuGestioneOrdini1" runat="server" />
<uc3:MenuGestioneFatturazione ID="MenuGestioneFatturazione1" runat="server" />
<uc4:TuteScarpeMenuStatistiche ID="TuteScarpeMenuStatistiche1" runat="server" />