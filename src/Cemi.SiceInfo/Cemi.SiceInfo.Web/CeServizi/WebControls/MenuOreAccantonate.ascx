﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuOreAccantonate.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuOreAccantonate" %>

<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.InfoImpresa) && GestioneUtentiBiz.IsImpresa())
    {
%>
<table class="menuTable">
    <tr class="menuTable">
        <td>
            Ore accantonate
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.IsImpresa())
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=orePeriodo" runat="server">Per periodo</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/RendicontiImprese/ReportImprese.aspx?tipo=oreLavoratori" runat="server">Per lavoratore</a>
        </td>
    </tr>
    <%
        }
    %>
    <!--<tr>
        <td>
            <asp:LinkButton
                ID="A3" 
                runat="server"
                Text="EVR" 
                onclick="A3_Click">
            </asp:LinkButton>
            <%--<a id="A3" href="ReportImprese.aspx?tipo=evr" runat="server">EVR</a>--%>
        </td>
    </tr>-->
</table>
<%
    }
%>