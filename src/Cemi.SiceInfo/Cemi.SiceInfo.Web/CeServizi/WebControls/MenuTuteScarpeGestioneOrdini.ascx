﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuTuteScarpeGestioneOrdini.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuTuteScarpeGestioneOrdini" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
     if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneOrdini)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati))
    {
    %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Gestione Ordini</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneOrdini))
    {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/TuteScarpe/TuteScarpeCreazioneOrdine.aspx" runat="server">Crea nuovo ordine</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/TuteScarpe/TuteScarpeGestioneOrdini.aspx" runat="server">Gestione ordini</a>
        </td>
    </tr>

    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneOrdiniConfermati))
    {
    %>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/TuteScarpe/TuteScarpeGestioneOrdiniConfermati.aspx" runat="server">Gestione ordini confermati</a>
        </td>
    </tr>
    <%
    } 
    %>
</table>

    <%  
    } 
    %>