﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuCorsi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuCorsi" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiGestione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiVisualizzazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiIscrizioneImpresa)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiEstrazioneFormedil)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiIscrizioneConsulente)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiStatisticheAttestati)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiGestioneDomandePrestazione)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiPrestazioniDTA)
        )
    {                  
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Iscrizione Corsi
        </td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiGestione))
        {
    %>
        <tr>
            <td>
                <a id="A1" href="~/CeServizi/Corsi/AnagraficaLocazioni.aspx" runat="server">Anagrafica locazioni</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A2" href="~/CeServizi/Corsi/AnagraficaCorsi.aspx" runat="server">Anagrafica corsi</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A3" href="~/CeServizi/Corsi/PianificazioneCorsi.aspx" runat="server">Pianificazione</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A4" href="~/CeServizi/Corsi/IscrizioneCorsi.aspx" runat="server">Iscrizione</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A5" href="~/CeServizi/Corsi/GestionePartecipantiCorsi.aspx" runat="server">Gestione partecipanti</a>
            </td>
        </tr>
        <tr>
            <td>
                <a id="A8" href="~/CeServizi/Corsi/RicercaLavoratoreImpresa.aspx" runat="server">Ricerca lavoratore/impresa</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiVisualizzazione)
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore())
            )
        {
    %>
        <tr>
            <td>
                <a id="A6" href="~/CeServizi/Corsi/LavoratoreCorsi.aspx" runat="server">Corsi svolti/programmati</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiIscrizioneImpresa)
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
            )
        {
    %>
        <tr>
            <td>
                <a id="A9" href="~/CeServizi/Corsi/IscrizioneCorsiImpresa.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiIscrizioneConsulente)
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            )
        {
    %>
        <tr>
            <td>
                <a id="A11" href="~/CeServizi/Corsi/IscrizioneCorsiConsulente.aspx" runat="server">Iscrizione lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiVisualizzazione)
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa())
            )
        {
    %>
        <tr>
            <td>
                <a id="A7" href="~/CeServizi/Corsi/ImpresaCorsi.aspx" runat="server">Corsi dei lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiVisualizzazione)
            && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente())
            )
        {
    %>
        <tr>
            <td>
                <a id="A12" href="~/CeServizi/Corsi/ConsulenteCorsi.aspx" runat="server">Corsi dei lavoratori</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiEstrazioneFormedil)
            )
        {
    %>
        <tr>
            <td>
                <a id="A10" href="~/CeServizi/Corsi/EstrazioneFormedil.aspx" runat="server">Estrazione per Formedil</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiStatisticheAttestati)
            )
        {
    %>
        <tr>
            <td>
                <a id="A13" href="~/CeServizi/Corsi/StatisticheAttestati.aspx" runat="server">Statistiche attestati</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiGestioneDomandePrestazione)
            )
        {
    %>
        <tr>
            <td>
                <a id="A14" href="~/CeServizi/Corsi/GestioneCorsiPrestazioniDomande.aspx" runat="server">Gestione prestazioni</a>
            </td>
        </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CorsiPrestazioniDTA)
            )
        {
    %>
        <tr>
            <td>
                <a id="A15" href="~/CeServizi/Corsi/PrestazioniDTA.aspx" runat="server">Prestazioni DTA</a>
            </td>
        </tr>
    <%
        }
    %>
    
</table>
<%
    }
%>
