﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuImpreseRegolari.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuImpreseRegolari" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business"%>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type"%>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (true
        /*TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariModificaLista.ToString())
        || TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariGestisciAnomalie.ToString())
        || TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariSegnalaAnomalie.ToString())
        || TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Type.FunzionalitaPredefinite.ImpreseRegolariImpostaParametri.ToString())
         */
        )
    {%>
<table class="menuTable" >
    <tr id="RigaTitolo" class="menuTable" runat="server" >
        <td >Imprese Adempienti</td>
    </tr>
    <tr>
        <td>
            <a href="~/CeServizi/ImpreseAdempienti/ImpreseAdempientiReport.aspx" runat="server">Visualizza lista imprese adempienti</a>
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariModificaLista))
        {%>
     <tr>
        <td>
            <a href="~/CeServizi/ImpreseAdempienti/ImpreseRegolariForzaInLista.aspx" runat="server">Modifica lista</a>
        </td>
    </tr>
    <%
        }
%>

    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariImpostaParametri))
        {%>
    <tr>
        <td>
            <a href="~/CeServizi/ImpreseAdempienti/configura.aspx" runat="server">Configura i parametri</a>
        </td>
    </tr>
    <%
        }
%>
     
     <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariGestisciAnomalie))
        {%>
    <tr>
        <td><a href="~/CeServizi/ImpreseAdempienti/Anomalie.aspx" runat="server">Visualizza anomalie</a>
        </td>
    </tr>
    <%
        }
%>
     <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.ImpreseRegolariSegnalaAnomalie))
        {%>
     <tr>
        <td>
            <a href="~/CeServizi/ImpreseAdempienti/ImpreseRegolariSegnalaAnomalie.aspx" runat="server">Segnala anomalia</a>
        </td>
    </tr>
    <%
        }
%>
 
</table>
<%
    }
%>
