﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuAccessoCantieri.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuAccessoCantieri" %>
<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Business" %>
<%--<%@ Import Namespace="TBridge.Cemi.GestioneUtenti.Type" %>--%>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerImpresa)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione)
        || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriControlli)
        || (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerCommittente) && GestioneUtentiBiz.IsCommittente())
        )
    {%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>
            Accesso ai Cantieri
        </td>
    </tr>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere))
        {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/AccessoCantieri/GestioneCantiere.aspx" runat="server">Inserimento
                cantiere</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriGestioneCantiere)
            || (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerCommittente) && GestioneUtentiBiz.IsCommittente()))
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/AccessoCantieri/RiepilogoCantieri.aspx" runat="server">Riepilogo
                cantieri</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A11" href="~/CeServizi/AccessoCantieri/RicercaLavoratore.aspx" runat="server">Ricerca lavoratore</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/AccessoCantieri/GestioneTimbrature.aspx" runat="server">Gestione
                timbrature </a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A23" href="~/CeServizi/AccessoCantieri/Presenze.aspx" runat="server">Presenze</a>
        </td>
    </tr>
    <%-- <tr>
        <td>
            <a id="A5" href="~/AccessoCantieri/InserimentoTimbrature.aspx" runat="server">Inserimento timbrature</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A6" href="~/AccessoCantieri/ProvaImportazioneTimbrature.aspx" runat="server">Prova Importazione timbrature</a>
        </td>
    </tr>--%>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriControlli))
        {
    %>
    <tr>
        <td>
            <a id="A6" href="~/CeServizi/AccessoCantieri/ControlliCexChange.aspx" runat="server">Controlli
            </a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriPerImpresa))
        {
    %>
    <tr>
        <td>
            <a id="A7" href="~/CeServizi/AccessoCantieri/AccessoCantieriReportTimbratureImpresa.aspx" runat="server">
                Situazione timbrature</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A10" href="~/CeServizi/AccessoCantieri/ControlliCEMIImprese.aspx" runat="server">
                Situazione controlli</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.AccessoCantieriAmministrazione))
        {
    %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/AccessoCantieri/GestioneLettori.aspx" runat="server">Gestione rilevatori
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A9" href="~/CeServizi/AccessoCantieri/GestioneCommittenti.aspx" runat="server">Gestione
                committenti </a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/CeServizi/AccessoCantieri/Statistiche.aspx" runat="server">Statistiche</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A8" href="~/CeServizi/AccessoCantieri/StampaBadge.aspx" runat="server">Stampa Badge</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
