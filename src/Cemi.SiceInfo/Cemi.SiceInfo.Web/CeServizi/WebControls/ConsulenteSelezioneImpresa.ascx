﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsulenteSelezioneImpresa.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.ConsulenteSelezioneImpresa" %>

<div class="borderedDiv">
    <b>
        Selezionare l'impresa per cui si sta operando
    </b>
    <div >
        <telerik:RadComboBox
            ID="RadComboBoxImprese" 
            runat="server"
            Width="350px" 
            EmptyMessage="Selezionare un'impresa"
            Filter="Contains" 
            MarkFirstMatch="true" 
            ChangeTextOnKeyBoardNavigation="false"
            AllowCustomText="true">
        </telerik:RadComboBox>
        <asp:Button ID="ButtonSeleziona"  CausesValidation=false runat="server" OnClick="ButtonSeleziona_Click" Text="Seleziona" />
        
    </div>
    <br />
    <div >
        Impresa selezionata:
        &nbsp;
        <b>
            <asp:Label ID="LabelImpresaSelezionata" runat="server">Nessuna</asp:Label>
        </b>
    </div>
</div>