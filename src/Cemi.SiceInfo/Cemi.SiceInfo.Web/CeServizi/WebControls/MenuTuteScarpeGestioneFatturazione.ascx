﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuTuteScarpeGestioneFatturazione.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuTuteScarpeGestioneFatturazione" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeImmissioneFatture)
        || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFatture))
    {
%>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Gestione Fatture</td>
    </tr>

    <%
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeImmissioneFatture))
    {
    %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/TuteScarpe/TuteScarpeImmissioneFatture.aspx" runat="server">Immissione nuova fattura</a>
        </td>
    </tr>
    <%
    }
    if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFatture))
    {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/TuteScarpe/TuteScarpeGestioneFatture.aspx" runat="server">Gestione fatture</a>
        </td>
    </tr>
    <%
    }     
%>
</table>
<%
    }     
%>