﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuIscrizioneCE.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuIscrizioneCE" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>

<%
if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneCEIscrizione)
    || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneCEGestioneDomande))
   {                  
%>
<table class="menuTable">
    <tr class="menuTable" id="RigaTitolo" runat="server">
        <td >Iscrizione CE</td>
    </tr>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneCEIscrizione))
        {                  
         %>
    <tr>
        <td>
            <a id="A1" href="~/CeServizi/IscrizioneCE/Requisiti.aspx" runat="server">Iscrizione</a>
        </td>
    </tr>
    <%
        }
     %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente() 
            && TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneCEIscrizione)
            )
        {
    %>
    <tr>
        <td>
            <a id="A2" href="~/CeServizi/IscrizioneCE/GestioneDomandeConsulente.aspx?modalita=daConfermare" runat="server">Domande da confermare</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/IscrizioneCE/GestioneDomandeConsulente.aspx?modalita=confermate" runat="server">Domande confermate</a>
        </td>
    </tr>
    <%
        }
    %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.IscrizioneCEGestioneDomande))
        {                  
    %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/IscrizioneCE/GestioneDomandeCassaEdile.aspx" runat="server">Gestione domande</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A5" href="~/CeServizi/IscrizioneCE/ReportImpreseIscritte.aspx" runat="server">Imprese iscritte</a>
        </td>
    </tr>
    <%
        }
     %>
</table>
<%
    }
 %>