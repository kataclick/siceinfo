﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuTuteScarpe.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.WebControls.MenuTuteScarpe" %>
<%@ Import Namespace="TBridge.Cemi.Type.Enums.GestioneUtenti" %>
<%
    if (
            TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogniConfermati)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneConsulenti)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneSms)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeSelezioneTaglie)
            || TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneScadenze)
        )
        {                  
         %>
<table class="menuTable">
    <tr id="RigaTitolo" class="menuTable" runat="server">
        <td>Tute e Scarpe</td>
    </tr>
    <%--
        <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtenti.Type.FunzionalitaPredefinite.TuteScarpeSelezioneTaglie)
           && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsLavoratore()))
        {                  
         %>
    <tr>
        <td>
            <a id="A9" href="~/TuteScarpe/SelezioneTaglie.aspx" runat="server">Selezione taglie</a>
        </td>
    </tr>
    
    <%
        } 
        %>
        --%>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno)
           && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsImpresa()))
        {                  
         %>
    <tr>
        <td>
            <a id="A10" href="~/CeServizi/TuteScarpe/GestioneFabbisogno.aspx" runat="server">Gestione ultimo fabbisogno</a>
        </td>
    </tr>
    <tr>
        <td>
            <a id="A12" href="~/CeServizi/TuteScarpe/VisualizzaStorico.aspx" runat="server">Visualizza Storico fabbisogni</a>
        </td>
    </tr>
    
    <%
        } 
        %>
    <%
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno)
       && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsFornitore()))
        {                  
     %>
    <tr>
        <td>
            <a id="A3" href="~/CeServizi/TuteScarpe/TuteScarpeGestioneFabbisognoFornitore.aspx" runat="server">Gestione fabbisogni</a>
        </td>
    </tr>
    <%
    }
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogno)
       && (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.IsConsulente()))
    {                  
     %>
    <tr>
        <td>
            <a id="A4" href="~/CeServizi/TuteScarpe/TuteScarpeGestioneFabbisognoConsulenti.aspx" runat="server">Gestione fabbisogni</a>
        </td>
    </tr>
    <%
        } 
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneFabbisogniConfermati))
        {                  
         %>    
    <tr>
        <td>
        <A id="A5" HREF="~/CeServizi/TuteScarpe/TuteScarpeGestioneFabbisogniConfermati.aspx" runat="server">Gestione fabbisogni confermati</A>
        </td>
    </tr>
    <%
        } 
         if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneScadenze))
        {                  
         %>
    <tr>
        <td>
        <a id="A6" href="~/CeServizi/TuteScarpe/TuteScarpeScadenzaFabbisogni.aspx" runat="server">Gestione scadenze</a>
        </td>
    </tr>
    <%
        } 
         if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneConsulenti))
        {                  
         %> 
    <tr>
        <td>
            <A id="A7" HREF="~/CeServizi/TuteScarpe/TuteScarpeConsulenteImprese.aspx" runat="server">Gestione consulenti</A>
        </td>
    </tr>
    <%
        } 
        if (TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.TuteScarpeGestioneSms))
        {                  
         %> 
    <!--<tr>
        <td>
            <A id="A8" HREF="~/TuteScarpe/TuteScarpeVisualizzaRichiesteSms.aspx" runat="server">Visualizza richieste SMS</A>
        </td>
    </tr>-->
    <%
        } %>
</table>
<%
   } 
    
    %>