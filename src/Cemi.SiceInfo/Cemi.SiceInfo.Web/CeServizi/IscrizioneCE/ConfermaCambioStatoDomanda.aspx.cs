﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Crm;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.IscrizioneCE.Business;
using TBridge.Cemi.Type.Entities.IscrizioneCe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Enums.IscrizioneCe;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE
{
    public partial class ConfermaCambioStatoDomanda : System.Web.UI.Page
    {
        private readonly IscrizioniManager biz = new IscrizioniManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEGestioneDomande);

            #region Click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonConferma, null) + ";");
            sb.Append("return true;");
            ButtonConferma.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonConferma);
            #endregion

            if (!Page.IsPostBack)
            {
                int idDomanda = Int32.Parse(Request.QueryString["idDomanda"]);
                ModuloIscrizione domanda = biz.GetDomanda(idDomanda);
                CaricaDomanda(domanda);

                switch (Request.QueryString["modalita"])
                {
                    case "accetta":
                        TitoloSottotitolo1.sottoTitolo = "Accettazione domanda";
                        LabelModalita.Text = "ACCETTARE";
                        //CaricaTipiIscrizione(domanda.CodiceTipoImpresa);
                        break;
                    case "rifiuta":
                        TitoloSottotitolo1.sottoTitolo = "Rifiuto domanda";
                        LabelModalita.Text = "RIFIUTARE";
                        break;
                    case "attesaDocumentazione":
                        TitoloSottotitolo1.sottoTitolo = "Sospensione per attesa integrazione";
                        LabelModalita.Text = "SOSPENDERE PER ATTESA INTEGRAZIONE";
                        break;
                    case "situazioneDebitoria":
                        TitoloSottotitolo1.sottoTitolo = "Sospensione per debiti";
                        LabelModalita.Text = "SOSPENDERE PER DEBITI";
                        break;
                }
            }
        }

        private void CaricaTipiIscrizione(string idTipoImpresa)
        {
            trTipoIscrizione.Visible = true;
            DropDownListTipoIscrizione.Items.Clear();
            DropDownListTipoIscrizione.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListTipoIscrizione.DataSource = biz.GetTipiIscrizione(idTipoImpresa);
            DropDownListTipoIscrizione.DataTextField = "Descrizione";
            DropDownListTipoIscrizione.DataValueField = "IdTipoIscrizione";
            DropDownListTipoIscrizione.DataBind();
        }

        private void CaricaDomanda(ModuloIscrizione domanda)
        {
            LabelRagioneSociale.Text = domanda.Impresa.RagioneSociale;
            LabelPartitaIVA.Text = domanda.Impresa.PartitaIVA;
            LabelCodiceFiscale.Text = domanda.Impresa.CodiceFiscale;
            LabelTipoImpresa.Text = domanda.DescrizioneTipoImpresa;
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int idDomanda = Int32.Parse(Request.QueryString["idDomanda"]);
                //int idUtente = ((UtenteAbilitato)
                //                HttpContext.Current.User.Identity).IdUtente;
                int idUtente = GestioneUtentiBiz.GetIdUtente();

                ModuloIscrizione moduloIscrizione = GetDomanda(idDomanda);
                string mail = biz.GetMailValidaPerInvio(moduloIscrizione);

                StringBuilder urlGestioneDomande = new StringBuilder();
                urlGestioneDomande.Append("~/CeServizi/IscrizioneCE/GestioneDomandeCassaEdile.aspx?modalita=indietro");

                // Passo alla pagina di ricerca i filtri che erano stati impostati
                if (Request.QueryString["filtroStato"] != null)
                    urlGestioneDomande.Append(String.Format("&filtroStato={0}", Request.QueryString["filtroStato"]));
                if (Request.QueryString["filtroRagSoc"] != null)
                    urlGestioneDomande.Append(String.Format("&filtroRagSoc={0}", Request.QueryString["filtroRagSoc"]));
                if (Request.QueryString["filtroDal"] != null)
                    urlGestioneDomande.Append(String.Format("&filtroDal={0}", Request.QueryString["filtroDal"]));
                if (Request.QueryString["filtroAl"] != null)
                    urlGestioneDomande.Append(String.Format("&filtroAl={0}", Request.QueryString["filtroAl"]));

                switch (Request.QueryString["modalita"])
                {
                    case "rifiuta":
                        if (!biz.RifiutaDomanda(idDomanda, idUtente))
                            LabelErrore.Text = "Errore durante l'aggiornamento";
                        else
                        {
                            IscrizioneCeManager.GestisciSR(moduloIscrizione.RagioneSociale, mail, moduloIscrizione.IdImpresa,
                                                           moduloIscrizione.PartitaIva, moduloIscrizione.IdConsulente,
                                                           moduloIscrizione.CodiceFiscaleConsulente,
                                                           moduloIscrizione.RagioneSocialeConsulente, StatoDomanda.Rifiutata);
                            Response.Redirect(urlGestioneDomande.ToString());
                        }
                        break;
                    case "accetta":
                        if (!biz.AccettaDomanda(idDomanda, "0", idUtente))
                            LabelErrore.Text = "Errore durante l'aggiornamento";
                        else
                        {
                            IscrizioneCeManager.GestisciSR(moduloIscrizione.RagioneSociale, mail, moduloIscrizione.IdImpresa,
                                                           moduloIscrizione.PartitaIva, moduloIscrizione.IdConsulente,
                                                           moduloIscrizione.CodiceFiscaleConsulente,
                                                           moduloIscrizione.RagioneSocialeConsulente, StatoDomanda.Accettata);
                            Response.Redirect(urlGestioneDomande.ToString());
                        }
                        break;
                    case "attesaDocumentazione":
                        if (!biz.AttendiDocumentazioneDomanda(idDomanda, idUtente))
                            LabelErrore.Text = "Errore durante l'aggiornamento";
                        else
                        {
                            IscrizioneCeManager.GestisciSR(moduloIscrizione.RagioneSociale, mail, moduloIscrizione.IdImpresa,
                                                           moduloIscrizione.PartitaIva, moduloIscrizione.IdConsulente,
                                                           moduloIscrizione.CodiceFiscaleConsulente,
                                                           moduloIscrizione.RagioneSocialeConsulente,
                                                           StatoDomanda.SospesaAttesaIntegrazione);
                            Response.Redirect(urlGestioneDomande.ToString());
                        }
                        break;
                    case "situazioneDebitoria":
                        if (!biz.SituazioneDebitoriaDomanda(idDomanda, idUtente))
                            LabelErrore.Text = "Errore durante l'aggiornamento";
                        else
                        {
                            IscrizioneCeManager.GestisciSR(moduloIscrizione.RagioneSociale, mail, moduloIscrizione.IdImpresa,
                                                           moduloIscrizione.PartitaIva, moduloIscrizione.IdConsulente,
                                                           moduloIscrizione.CodiceFiscaleConsulente,
                                                           moduloIscrizione.RagioneSocialeConsulente,
                                                           StatoDomanda.SospesaDebiti);
                            Response.Redirect(urlGestioneDomande.ToString());
                        }
                        break;
                }
            }
        }

        private string GetPivaByIdDomanda(int idDomanda)
        {
            IscrizioniManager manager = new IscrizioniManager();
            return manager.GetPivaByIdDomanda(idDomanda);
        }

        private ModuloIscrizione GetDomanda(int idDomanda)
        {
            IscrizioniManager manager = new IscrizioniManager();
            return manager.GetDomanda(idDomanda);
        }
    }
}