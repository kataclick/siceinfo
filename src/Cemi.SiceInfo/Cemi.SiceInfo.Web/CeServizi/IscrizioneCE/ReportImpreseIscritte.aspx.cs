﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE
{
    public partial class ReportImpreseIscritte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEGestioneDomande);
        }

        protected void ReportViewerImpreseIscritte_Init(object sender, EventArgs e)
        {
            ReportViewerImpreseIscritte.ServerReport.ReportServerUrl =
                   new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerImpreseIscritte.ServerReport.ReportPath = "/ReportIscrizioneCE/ReportImpreseIscrittePeriodo";

            RadDatePickerDataDa.SelectedDate = DateTime.Today.AddDays(-1);
            RadDatePickerDataA.SelectedDate = DateTime.Today;

            SetReportParameters();
        }

        protected void ButtonVisualizzaReport_Click(object sender, EventArgs e)
        {
            //ReportViewerImpreseIscritte.ServerReport.ReportPath = "/ReportIscrizioneCE/ReportImpreseIscrittePeriodo";

            string nothing = null;

            ReportViewerImpreseIscritte.Reset();
            ReportViewerImpreseIscritte.ProcessingMode = ProcessingMode.Remote;
            ReportViewerImpreseIscritte.ServerReport.ReportServerUrl =
                new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportViewerImpreseIscritte.ServerReport.ReportPath = "/ReportIscrizioneCE/ReportImpreseIscrittePeriodo";
            ReportViewerImpreseIscritte.ServerReport.Refresh();

            SetReportParameters();
        }

        private void SetReportParameters()
        {
            const string nothing = null;
            List<ReportParameter> listaParam = new List<ReportParameter>();

            ReportParameter param1 = new ReportParameter("dal");
            ReportParameter param2 = new ReportParameter("al");
            ReportParameter param3 = new ReportParameter("ragioneSociale");
            ReportParameter param4 = new ReportParameter("idImpresa");

            if (RadDatePickerDataDa.SelectedDate != null)
            {
                param1.Values.Add(RadDatePickerDataDa.SelectedDate.Value.ToShortDateString());

            }
            else
            {
                param1.Values.Add(nothing);
            }
            if (RadDatePickerDataA.SelectedDate != null)
            {
                param2.Values.Add(RadDatePickerDataA.SelectedDate.Value.ToShortDateString());
            }
            else
            {
                param2.Values.Add(nothing);
            }
            if (!String.IsNullOrEmpty(RadTextBoxRagioneSociale.Text))
            {
                param3.Values.Add(RadTextBoxRagioneSociale.Text);
            }
            else
            {
                param3.Values.Add(nothing);
            }
            if (!String.IsNullOrEmpty(RadTextBoxCodiceImpresa.Text))
            {
                param4.Values.Add(RadTextBoxCodiceImpresa.Text);
            }
            else
            {
                param4.Values.Add(nothing);
            }

            listaParam.Add(param1);
            listaParam.Add(param2);
            listaParam.Add(param3);
            listaParam.Add(param4);

            ReportViewerImpreseIscritte.ServerReport.SetParameters(listaParam.ToArray());
        }
    }
}