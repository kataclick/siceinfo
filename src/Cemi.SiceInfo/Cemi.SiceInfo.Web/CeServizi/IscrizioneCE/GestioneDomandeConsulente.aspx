﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneDomandeConsulente.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.GestioneDomandeConsulente" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuIscrizioneCE.ascx" TagName="IscrizioneCEMenu" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione domande"
        titolo="Iscrizione imprese" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                Ragione sociale
            </td>
            <td>
                <asp:TextBox ID="TextBoxRicercaRagioneSociale" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="ButtonRicerca" runat="server" Text="Ricerca" OnClick="ButtonRicerca_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelTitolo" runat="server" Text="Domande presenti" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    OnRowDataBound="GridViewDomande_RowDataBound" AllowPaging="True" DataKeyNames="IdDomanda,TipoModulo"
                    OnPageIndexChanging="GridViewDomande_PageIndexChanging" OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging"
                    OnRowDeleting="GridViewDomande_RowDeleting" OnRowCommand="GridViewDomande_RowCommand">
                    <Columns>
                        <asp:BoundField HeaderText="Ragione sociale" DataField="RagioneSociale" >
                        <ItemStyle Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdImpresa" HeaderText="Cod">
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Partita IVA" DataField="PartitaIva" >
                        <ItemStyle Width="70px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Codice fiscale" DataField="CodiceFiscale" >
                        <ItemStyle Width="110px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TipoModulo" HeaderText="Tipo" >
                        <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:CheckBoxField DataField="Confermata" HeaderText="Conferm." >
                        <ItemStyle Width="10px" />
                        </asp:CheckBoxField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Modifica"
                            ShowSelectButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina"
                            ShowDeleteButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="modulo"
                            Text="Modulo">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label><br />
            </td>
        </tr>
    </table>
</asp:Content>
