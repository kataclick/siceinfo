﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneDomandeCassaEdile.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.GestioneDomandeCassaEdile" %>

<%@ Register Src="../WebControls/MenuIscrizioneCE.ascx" TagName="IscrizioneCEMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione domande"
        titolo="Iscrizione imprese" />
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 150px">
                Stato
            </td>
            <td>
                <asp:DropDownList ID="DropDownListRicercaStato" runat="server" Width="300px">
                    <asp:ListItem Value="DAVALUTARE">Da valutare</asp:ListItem>
                    <asp:ListItem Value="ACCETTATE">Accettate</asp:ListItem>
                    <asp:ListItem Value="RESPINTE">Respinte</asp:ListItem>
                    <asp:ListItem Value="ATTESADOCUMENTAZIONE">Sospese per attesa integrazione</asp:ListItem>
                    <asp:ListItem Value="SITUAZIONEDEBITORIA">Sospese per debiti</asp:ListItem>
                    <asp:ListItem Value="TUTTE">Tutte</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">
                Ragione sociale
            </td>
            <td>
                <asp:TextBox ID="TextBoxRicercaRagioneSociale" runat="server" Width="300px"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">
                Data inserimento - Dal (gg/mm/aaaa)
            </td>
            <td style="width: 350px">
                <asp:TextBox ID="TextBoxDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorDal" runat="server"
                    ControlToValidate="TextBoxDal" ValidationGroup="ricerca" ErrorMessage="Formato errato"
                    ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">
                Data inserimento - Al (gg/mm/aaaa)
            </td>
            <td style="width: 350px">
                <asp:TextBox ID="TextBoxAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorAl" runat="server"
                    ControlToValidate="TextBoxAl" ValidationGroup="ricerca" ErrorMessage="Formato errato"
                    ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"></asp:RegularExpressionValidator>
                <asp:CustomValidator ID="CustomValidatorDate" runat="server" ErrorMessage='"Dal" deve essere inferiore ad "Al"'
                    OnServerValidate="CustomValidatorDate_ServerValidate" ValidationGroup="ricerca"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">
                Data richiesta iscr. - Dal (gg/mm/aaaa)
            </td>
            <td style="width: 350px">
                <asp:TextBox ID="TextBoxDataRichiestaDal" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxDataRichiestaDal"
                    ErrorMessage="Formato errato" Operator="DataTypeCheck" Type="Date" ValidationGroup="ricerca"></asp:CompareValidator>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 150px">
                Data richiesta iscr. - Al (gg/mm/aaaa)
            </td>
            <td style="width: 350px">
                <asp:TextBox ID="TextBoxDataRichiestaAl" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
            </td>
            <td>
                &nbsp;<asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBoxAl"
                    ErrorMessage="Formato errato" Operator="DataTypeCheck" Type="Date" ValidationGroup="ricerca"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToCompare="TextBoxDataRichiestaAl"
                    ControlToValidate="TextBoxDataRichiestaDal" ErrorMessage='"Dal" deve essere inferiore ad "Al"'
                    Operator="LessThanEqual" Type="Date" ValidationGroup="ricerca"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>
                Ordinamento:
            </td>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonDataRichiestaIscrizioneAsc" runat="server" GroupName="ricercaDomande"
                                Text="Data richiesta iscrizione crescente" />
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonDataRichiestaIscrizioneDesc" runat="server" GroupName="ricercaDomande"
                                Text="Data richiesta iscrizione decrescente" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonDataInserimentoAsc" runat="server" GroupName="ricercaDomande"
                                Text="Data inserimento crescente" Checked="true" />
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonDataInserimentoDesc" runat="server" GroupName="ricercaDomande"
                                Text="Data inserimento decrescente" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="RadioButtonRagioneSocialeAsc" runat="server" GroupName="ricercaDomande"
                                Text="Ragione sociale crescente" />
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButtonRagioneSocialeDesc" runat="server" GroupName="ricercaDomande"
                                Text="Ragione sociale decrescente" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="ButtonRicerca" runat="server" Text="Ricerca" OnClick="ButtonRicerca_Click"
                    ValidationGroup="ricerca" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="LabelTitolo" runat="server" Text="Domande presenti" Font-Bold="True"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    OnRowDataBound="GridViewDomande_RowDataBound" DataKeyNames="IdDomanda,RagioneSociale,TipoModulo"
                    OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging" OnRowCommand="GridViewDomande_RowCommand"
                    AllowPaging="True" OnPageIndexChanging="GridViewDomande_PageIndexChanging" PageSize="5"
                    OnRowUpdating="GridViewDomande_RowUpdating">
                    <Columns>
                        <asp:TemplateField HeaderText="Ragione sociale">
                            <ItemTemplate>
                                <asp:Label ID="LabelRagioneSociale" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RagioneSociale").ToString() %>'></asp:Label>
                                <hr />
                                <asp:Button ID="ButtonModulo" runat="server" Text="Modulo" CommandName="Modulo" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="IdImpresa" HeaderText="Cod">
                            <ItemStyle Width="20px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Partita IVA" DataField="PartitaIva" />
                        <asp:BoundField HeaderText="Codice fiscale" DataField="CodiceFiscale" />
                        <asp:BoundField DataField="UtenteCompilazione" HeaderText="Utente compilazione" />
                        <asp:BoundField DataField="DataPresentazione" HeaderText="Data inserimento" />
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TipoModulo" HeaderText="Tipo modulo" />
                        <asp:TemplateField HeaderText="Problemi riscontrati">
                            <ItemTemplate>
                                <asp:Panel ID="PanelColonnaProblemi" runat="server">
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                <asp:Label ID="LabelProblemi" runat="server" ForeColor="Red">Sono presenti dei problemi</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ButtonDettagliProblemi" runat="server" Text="Dettagli" CommandName="Update"
                                                    CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonModifica" runat="server" CommandName="Select" Text="Modifica"
                                    Width="150px" />
                                <asp:Panel ID="PanelStatiIntermedi" runat="server" Enabled="false">
                                    <hr />
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                <asp:Button ID="ButtonAttesaDocumentazione" runat="server" CommandName="attesaDocumentazione"
                                                    Text="Sosp. attesa integr." Width="150px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="ButtonSituazioneDebitoria" runat="server" CommandName="situazioneDebitoria"
                                                    Text="Sosp. per debiti" Width="150px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="accetta"
                            Text="Accetta" />
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" CommandName="rifiuta"
                            Text="Rifiuta" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presente
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelDettagli" runat="server" Width="100%" Visible="False">
        <table class="standardTable">
            <tr>
                <td>
                    Problemi riscontrati per la domanda
                    <asp:Label ID="LabelDomanda" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Bloccanti
                </td>
            </tr>
            <tr>
                <td>
                    <asp:BulletedList ID="BulletedListProblemiBloccanti" runat="server">
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td>
                    Non bloccanti
                </td>
            </tr>
            <tr>
                <td>
                    <asp:BulletedList ID="BulletedProblemiNonBloccanti" runat="server">
                    </asp:BulletedList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
</asp:Content>
