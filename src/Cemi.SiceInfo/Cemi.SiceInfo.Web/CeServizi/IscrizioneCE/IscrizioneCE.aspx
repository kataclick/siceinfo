﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IscrizioneCE.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.IscrizioneCE" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="WebControls/IscrizioneCEIscrizione.ascx" TagName="IscrizioneCEIscrizione" TagPrefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Iscrizione"
        titolo="Iscrizione imprese" />
    <br />
    <div>
        <uc2:IscrizioneCEIscrizione ID="IscrizioneCEIscrizione1" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
</asp:Content>