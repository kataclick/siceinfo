﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.Type.Entities.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls
{
    public partial class IscrizioneCEImpiegati : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void CaricaDomanda(ModuloIscrizione domanda)
        {
            // CANTIERE
            TextBoxLavoratoriOccupatiPrevedi.Text = domanda.NumeroOpe.ToString();
        }

        public void CompletaDomandaConImpiegati(ModuloIscrizione modulo)
        {
            if (!string.IsNullOrEmpty(TextBoxLavoratoriOccupatiPrevedi.Text))
                modulo.NumeroOpe = Int32.Parse(TextBoxLavoratoriOccupatiPrevedi.Text);
        }

        #region Custom Validators
        protected void CustomValidatorCantiereNumeroImpiegati_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Controllo che se valorizzato, il numero degli impiegati sia > 0
            int impOcc;
            if (!string.IsNullOrEmpty(TextBoxLavoratoriOccupatiPrevedi.Text) &&
                Int32.TryParse(TextBoxLavoratoriOccupatiPrevedi.Text, out impOcc) && impOcc < 1)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        #endregion
    }
}