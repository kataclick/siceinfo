﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneCERicercaConsulenti.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls.IscrizioneCERicercaConsulenti" %>

<asp:Panel ID="PanelRicercaConsulenti" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td style="height: 16px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="White" Text="Ricerca Consulente"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td>
                Ragione sociale
            </td>
            <td>
                Indirizzo
            </td>
            <td>
                Comune
            </td>
            <td>
                Codice fiscale
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxRagioneSociale" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="TextBoxFiscale" runat="server" MaxLength="16" Width="95%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Codice<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="TextBoxCodice" ErrorMessage="*" ValidationExpression="^\d*$"
                    ValidationGroup="ricerca"></asp:RegularExpressionValidator>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBoxCodice" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Ricerca" ValidationGroup="ricerca" />
            </td>
            <td>
            </td>
            <td>
            </td>
            <td align="right">
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GridViewConsulenti" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    DataKeyNames="IdConsulente" OnPageIndexChanging="GridViewConsulenti_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewConsulenti_SelectedIndexChanging" Width="100%"
                    PageSize="5">
                    <Columns>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" SelectText="Seleziona" ShowSelectButton="True" />
                        <asp:BoundField DataField="IdConsulente" HeaderText="Cod." />
                        <asp:BoundField DataField="RagioneSociale" HeaderText="Ragione sociale" />
                        <asp:BoundField DataField="Indirizzo" HeaderText="Indirizzo" />
                        <asp:BoundField DataField="Provincia" HeaderText="Prov." />
                        <asp:BoundField DataField="Comune" HeaderText="Comune" />
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice fiscale" />
                        <asp:BoundField DataField="Telefono" HeaderText="Telefono" />
                    </Columns>
                    <EmptyDataTemplate>
                        Nessun consulente trovato
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
