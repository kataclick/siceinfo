﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.IscrizioneCE.Business;
using TBridge.Cemi.Business;
using System.Collections.Specialized;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Entities.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls
{
    public partial class IscrizioneCESedi : System.Web.UI.UserControl
    {
        private readonly IscrizioniManager biz = new IscrizioniManager();
        private readonly Common bizCommon = new Common();

        public String EmailSedeLegale
        {
            get
            {
                return TextBoxSedeLegaleEmail.Text;
            }
        }

        public String EmailSedeAmministrativa
        {
            get
            {
                return TextBoxSedeAmmEmail.Text;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ViewState["MODIFICA"] == null)
                {
                    CaricaCombo();
                }
            }
        }

        private void CaricaCombo()
        {
            // PreIndirizzi
            ListDictionary preIndirizzi = bizCommon.GetPreIndirizzi();

            DropDownListSedeAmmPreIndirizzo.Items.Clear();
            DropDownListSedeAmmPreIndirizzo.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListSedeAmmPreIndirizzo.DataSource = preIndirizzi;
            DropDownListSedeAmmPreIndirizzo.DataTextField = "Value";
            DropDownListSedeAmmPreIndirizzo.DataValueField = "Key";
            DropDownListSedeAmmPreIndirizzo.DataBind();

            DropDownListSedeLegalePreIndirizzo.Items.Clear();
            DropDownListSedeLegalePreIndirizzo.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListSedeLegalePreIndirizzo.DataSource = preIndirizzi;
            DropDownListSedeLegalePreIndirizzo.DataTextField = "Value";
            DropDownListSedeLegalePreIndirizzo.DataValueField = "Key";
            DropDownListSedeLegalePreIndirizzo.DataBind();

            // Province
            StringCollection province = bizCommon.GetProvinceSiceNew();

            DropDownListSedeLegaleProvincia.Items.Clear();
            DropDownListSedeLegaleProvincia.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListSedeLegaleProvincia.DataSource = province;
            DropDownListSedeLegaleProvincia.DataBind();

            DropDownListSedeAmmProvincia.Items.Clear();
            DropDownListSedeAmmProvincia.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListSedeAmmProvincia.DataSource = province;
            DropDownListSedeAmmProvincia.DataBind();
        }

        public void CaricaDomanda(ModuloIscrizione domanda)
        {
            ViewState["MODIFICA"] = true;
            CaricaCombo();

            TextBoxSedeLegalePEC.Text = domanda.PEC;

            // SEDE LEGALE
            ViewState["IdIndirizzo1"] = domanda.SedeLegale.IdIndirizzo.Value;
            if (!string.IsNullOrEmpty(domanda.SedeLegale.PreIndirizzo) &&
                DropDownListSedeLegalePreIndirizzo.Items.FindByValue(domanda.SedeLegale.PreIndirizzo) != null)
                DropDownListSedeLegalePreIndirizzo.SelectedValue = domanda.SedeLegale.PreIndirizzo;
            else
                DropDownListSedeLegalePreIndirizzo.Text = domanda.SedeLegale.PreIndirizzo;

            // Provincia
            if (!string.IsNullOrEmpty(domanda.SedeLegale.Provincia) &&
                DropDownListSedeLegaleProvincia.Items.FindByValue(domanda.SedeLegale.Provincia) != null)
            {
                DropDownListSedeLegaleProvincia.SelectedValue = domanda.SedeLegale.Provincia;
                biz.CaricaComuni(DropDownListSedeLegaleLocalita, domanda.SedeLegale.Provincia, null);

                // Comune
                if (!string.IsNullOrEmpty(domanda.SedeLegale.Comune))
                {
                    DropDownListSedeLegaleLocalita.SelectedIndex =
                        IscrizioniManager.IndiceComuneInDropDown(DropDownListSedeLegaleLocalita, domanda.SedeLegale.Comune);
                    biz.CaricaFrazioni(DropDownListSedeLegaleFrazione, domanda.SedeLegale.Comune);

                    if (!string.IsNullOrEmpty(domanda.SedeLegale.Frazione))
                        DropDownListSedeLegaleFrazione.SelectedIndex =
                            IscrizioniManager.IndiceFrazioneInDropDown(DropDownListSedeLegaleFrazione, domanda.SedeLegale.Frazione);
                }
            }

            TextBoxSedeLegaleIndirizzo.Text = domanda.SedeLegale.Indirizzo;
            TextBoxSedeLegaleCap.Text = domanda.SedeLegale.Cap;
            TextBoxSedeLegaleTel.Text = domanda.SedeLegale.Telefono;
            TextBoxSedeLegaleFax.Text = domanda.SedeLegale.Fax;
            TextBoxSedeLegaleEmail.Text = domanda.SedeLegale.EMail;

            // SEDE AMMINISTRATIVA
            ViewState["IdIndirizzo2"] = domanda.SedeAmministrativa.IdIndirizzo.Value;
            if (!string.IsNullOrEmpty(domanda.SedeAmministrativa.PreIndirizzo) &&
                DropDownListSedeAmmPreIndirizzo.Items.FindByValue(domanda.SedeAmministrativa.PreIndirizzo) != null)
                DropDownListSedeAmmPreIndirizzo.SelectedValue = domanda.SedeLegale.PreIndirizzo;
            else
                DropDownListSedeAmmPreIndirizzo.Text = domanda.SedeAmministrativa.PreIndirizzo;
            if (!string.IsNullOrEmpty(domanda.SedeAmministrativa.Provincia) &&
                DropDownListSedeAmmProvincia.Items.FindByValue(domanda.SedeAmministrativa.Provincia) != null)
            {
                DropDownListSedeAmmProvincia.SelectedValue = domanda.SedeAmministrativa.Provincia;
                biz.CaricaComuni(DropDownListSedeAmmLocalita, domanda.SedeAmministrativa.Provincia, null);

                if (!string.IsNullOrEmpty(domanda.SedeAmministrativa.Comune))
                {
                    DropDownListSedeAmmLocalita.SelectedIndex =
                        IscrizioniManager.IndiceComuneInDropDown(DropDownListSedeAmmLocalita, domanda.SedeAmministrativa.Comune);
                    biz.CaricaFrazioni(DropDownListSedeAmmFrazione, domanda.SedeAmministrativa.Comune);

                    if (!string.IsNullOrEmpty(domanda.SedeAmministrativa.Frazione))
                        DropDownListSedeAmmFrazione.SelectedIndex =
                            IscrizioniManager.IndiceFrazioneInDropDown(DropDownListSedeAmmFrazione, domanda.SedeAmministrativa.Frazione);
                }
            }

            TextBoxSedeAmmIndirizzo.Text = domanda.SedeAmministrativa.Indirizzo;
            TextBoxSedeAmmCap.Text = domanda.SedeAmministrativa.Cap;
            TextBoxSedeAmmTel.Text = domanda.SedeAmministrativa.Telefono;
            TextBoxSedeAmmFax.Text = domanda.SedeAmministrativa.Fax;
            TextBoxSedeAmmEmail.Text = domanda.SedeAmministrativa.EMail;
        }

        public void CompletaDomandaConSedi(ModuloIscrizione modulo)
        {
            modulo.PEC = TextBoxSedeLegalePEC.Text;

            // Sedi
            // Legale (obbligatoria)
            Sede sedeLegale = modulo.SedeLegale;
            if (ViewState["IdIndirizzo1"] != null)
            {
                sedeLegale.IdIndirizzo = (Int32)ViewState["IdIndirizzo1"];
            }
            sedeLegale.PreIndirizzo = DropDownListSedeLegalePreIndirizzo.SelectedValue;
            sedeLegale.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxSedeLegaleIndirizzo.Text);
            sedeLegale.Comune = biz.GetCodiceCatastaleDaCombo(DropDownListSedeLegaleLocalita.SelectedValue);
            sedeLegale.Frazione = biz.GetFrazioneDaCombo(DropDownListSedeLegaleFrazione.SelectedValue);
            sedeLegale.Provincia = DropDownListSedeLegaleProvincia.SelectedValue;
            sedeLegale.Cap = TextBoxSedeLegaleCap.Text.ToUpper();
            sedeLegale.Telefono = TextBoxSedeLegaleTel.Text.ToUpper();
            sedeLegale.Fax = TextBoxSedeLegaleFax.Text.ToUpper();
            if (!string.IsNullOrEmpty(TextBoxSedeLegaleEmail.Text))
                sedeLegale.EMail = TextBoxSedeLegaleEmail.Text;

            // Amministrativa
            Sede sedeAmministrativa = modulo.SedeAmministrativa;
            if (ViewState["IdIndirizzo2"] != null)
            {
                sedeAmministrativa.IdIndirizzo = (Int32)ViewState["IdIndirizzo2"];
            }
            sedeAmministrativa.PreIndirizzo = DropDownListSedeAmmPreIndirizzo.SelectedValue;
            sedeAmministrativa.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxSedeAmmIndirizzo.Text);
            sedeAmministrativa.Comune = biz.GetCodiceCatastaleDaCombo(DropDownListSedeAmmLocalita.SelectedValue);
            sedeAmministrativa.Frazione = biz.GetFrazioneDaCombo(DropDownListSedeAmmFrazione.SelectedValue);
            sedeAmministrativa.Provincia = DropDownListSedeAmmProvincia.SelectedValue;
            sedeAmministrativa.Cap = TextBoxSedeAmmCap.Text.ToUpper();
            sedeAmministrativa.Telefono = TextBoxSedeAmmTel.Text.ToUpper();
            sedeAmministrativa.Fax = TextBoxSedeAmmFax.Text.ToUpper();
            if (!string.IsNullOrEmpty(TextBoxSedeAmmEmail.Text))
                sedeAmministrativa.EMail = TextBoxSedeAmmEmail.Text;
        }

        public Boolean EsisteGeocodificaIndirizzoSedeLegale()
        {
            String indirizzo = Presenter.NormalizzaCampoTesto(TextBoxSedeLegaleIndirizzo.Text);
            if (!string.IsNullOrEmpty(indirizzo))
            {
                String comune = DropDownListSedeLegaleLocalita.Text;
                String provincia = DropDownListSedeLegaleProvincia.Text;

                String indirizzoGeocoder = String.Format("{0} {1} {2}, Italy", indirizzo, comune, provincia);
                TBridge.Cemi.Cpt.Type.Collections.IndirizzoCollection indirizzi = CantieriGeocoding.GeoCodeGoogleMultiplo(indirizzoGeocoder);
                if (indirizzi == null || indirizzi.Count != 1)
                {
                    return false;
                }
            }

            return true;
        }

        public Boolean EsisteGeocodificaIndirizzoSedeAmministrativa()
        {
            String indirizzo = TextBoxSedeAmmIndirizzo.Text;
            if (!string.IsNullOrEmpty(indirizzo))
            {
                String comune = DropDownListSedeAmmLocalita.Text;
                String provincia = DropDownListSedeAmmProvincia.Text;

                String indirizzoGeocoder = String.Format("{0} {1} {2}, Italy", indirizzo, comune, provincia);
                TBridge.Cemi.Cpt.Type.Collections.IndirizzoCollection indirizzi = CantieriGeocoding.GeoCodeGoogleMultiplo(indirizzoGeocoder);
                if (indirizzi == null || indirizzi.Count != 1)
                {
                    return false;
                }
            }

            return true;
        }

        #region Funzione per caricare dei dati di prova

        public void CaricaDatiProva()
        {
            // SEDE LEGALE
            DropDownListSedeLegaleProvincia.SelectedIndex = 1;
            DropDownListSedeLegaleLocalita.SelectedIndex = 1;
            DropDownListSedeLegalePreIndirizzo.SelectedIndex = 5;
            TextBoxSedeLegaleIndirizzo.Text = "della Vittoria";
            TextBoxSedeLegaleCap.Text = "16100";
            TextBoxSedeLegaleTel.Text = "05255588";
            TextBoxSedeLegaleFax.Text = "05255588";
            TextBoxSedeLegaleEmail.Text = "marco.catalano@cassaedilemilano.it";

            // SEDE AMMINISTRATIVA
            DropDownListSedeAmmProvincia.SelectedIndex = 1;
            DropDownListSedeAmmLocalita.SelectedIndex = 1;
            DropDownListSedeAmmPreIndirizzo.SelectedIndex = 1;
            TextBoxSedeAmmIndirizzo.Text = "XX Settembre";
            TextBoxSedeAmmCap.Text = "16100";
            TextBoxSedeAmmTel.Text = "05255588";
            TextBoxSedeAmmFax.Text = "05255588";
            TextBoxSedeAmmEmail.Text = "marco.catalano@cassaedilemilano.it";
        }

        #endregion

        #region Custom Validators
        /// <summary>
        /// Viene controllato che non sia inserito un CAP generico
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void CustomValidatorCAP_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator customValidatorCAP = (CustomValidator)source;
            TextBox textBoxCAP = (TextBox)this.FindControl(customValidatorCAP.ControlToValidate);
            int iCAP;

            if (textBoxCAP.Text.Length == 5 && Int32.TryParse(textBoxCAP.Text, out iCAP))
            {
                string cap = textBoxCAP.Text;

                string codiceComboComune = null;
                switch (textBoxCAP.ID)
                {
                    case "TextBoxSedeLegaleCap":
                        codiceComboComune = DropDownListSedeLegaleLocalita.SelectedValue;
                        break;
                    case "TextBoxSedeAmmCap":
                        codiceComboComune = DropDownListSedeAmmLocalita.SelectedValue;
                        break;
                }
                string[] codiceSplittato = codiceComboComune.Split('|');

                if (biz.IsGrandeCitta(codiceSplittato[0]) && cap[2] == '1' && cap[3] == '0' && cap[4] == '0')
                    args.IsValid = false;
            }
        }

        protected void CustomValidatorSedeAmministrativa_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Controllo che vengono immessi alcuni dati per la sede amministrativa, devono essere immessi tutti
            if (string.IsNullOrEmpty(DropDownListSedeAmmPreIndirizzo.SelectedValue) ||
                string.IsNullOrEmpty(TextBoxSedeAmmIndirizzo.Text)
                || string.IsNullOrEmpty(DropDownListSedeAmmProvincia.SelectedValue) ||
                string.IsNullOrEmpty(DropDownListSedeAmmLocalita.SelectedValue)
                || string.IsNullOrEmpty(TextBoxSedeAmmCap.Text) || string.IsNullOrEmpty(TextBoxSedeAmmTel.Text) ||
                string.IsNullOrEmpty(TextBoxSedeAmmFax.Text))
            {
                if (
                    !(string.IsNullOrEmpty(DropDownListSedeAmmPreIndirizzo.SelectedValue) &&
                      string.IsNullOrEmpty(TextBoxSedeAmmIndirizzo.Text)
                      && string.IsNullOrEmpty(DropDownListSedeAmmProvincia.SelectedValue) &&
                      string.IsNullOrEmpty(DropDownListSedeAmmLocalita.SelectedValue)
                      && string.IsNullOrEmpty(TextBoxSedeAmmCap.Text) && string.IsNullOrEmpty(TextBoxSedeAmmTel.Text) &&
                      string.IsNullOrEmpty(TextBoxSedeAmmFax.Text)))
                {
                    args.IsValid = false;
                }
            }
        }
        #endregion

        #region Eventi DropDown
        protected void DropDownListSedeLegaleProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxSedeLegaleCap.Text = string.Empty;
            biz.CaricaComuni(DropDownListSedeLegaleLocalita, DropDownListSedeLegaleProvincia.SelectedValue,
                         DropDownListSedeLegaleFrazione);
        }

        protected void DropDownListSedeAmmProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxSedeAmmCap.Text = string.Empty;
            biz.CaricaComuni(DropDownListSedeAmmLocalita, DropDownListSedeAmmProvincia.SelectedValue,
                         DropDownListSedeAmmFrazione);
        }

        protected void DropDownListSedeLegaleLocalita_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxSedeLegaleCap.Text = string.Empty;
            biz.CaricaCapOFrazioni(DropDownListSedeLegaleLocalita, DropDownListSedeLegaleFrazione, TextBoxSedeLegaleCap);
        }

        protected void DropDownListSedeAmmLocalita_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxSedeAmmCap.Text = string.Empty;
            biz.CaricaCapOFrazioni(DropDownListSedeAmmLocalita, DropDownListSedeAmmFrazione, TextBoxSedeAmmCap);
        }

        protected void DropDownListSedeLegaleFrazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxSedeLegaleCap.Text = string.Empty;
            biz.CaricaCap(DropDownListSedeLegaleFrazione.SelectedValue, TextBoxSedeLegaleCap);
        }

        protected void DropDownListSedeAmmFrazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxSedeAmmCap.Text = string.Empty;
            biz.CaricaCap(DropDownListSedeAmmFrazione.SelectedValue, TextBoxSedeAmmCap);
        }
        #endregion
    }
}