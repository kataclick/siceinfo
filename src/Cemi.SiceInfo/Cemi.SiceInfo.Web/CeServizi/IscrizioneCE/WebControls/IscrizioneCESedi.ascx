﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneCESedi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls.IscrizioneCESedi" %>

<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelPec" runat="server" Text="PEC" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo PEC:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeLegalePEC" runat="server" MaxLength="50" Width="350px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPECSedeLegale" runat="server"
                ErrorMessage="Indirizzo PEC mancante" ControlToValidate="TextBoxSedeLegalePEC"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPECSedeLegale" runat="server" ControlToValidate="TextBoxSedeLegalePEC"
                ErrorMessage="Email sede legale non corretta" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelSedeLegale" runat="server" Text="Sede legale" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        </td>
    </tr>
    <tr>
        <td>
            Provincia:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeLegaleProvincia" runat="server" Width="350px"
                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListSedeLegaleProvincia_SelectedIndexChanged" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorProvinciaSedeLegale" runat="server"
                ErrorMessage="Provincia sede legale mancante" ControlToValidate="DropDownListSedeLegaleProvincia"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Località:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeLegaleLocalita" runat="server" Width="350px"
                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListSedeLegaleLocalita_SelectedIndexChanged" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLocalitaSedeLegale" runat="server"
                ErrorMessage="Localit&#224; sede legale mancante" ControlToValidate="DropDownListSedeLegaleLocalita"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Frazione:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeLegaleFrazione" runat="server" Width="350px"
                AppendDataBoundItems="True" OnSelectedIndexChanged="DropDownListSedeLegaleFrazione_SelectedIndexChanged"
                AutoPostBack="True" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            CAP:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeLegaleCap" runat="server" MaxLength="5" Width="350px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCapSedeLegale" runat="server"
                ErrorMessage="Cap sede legale mancante" ControlToValidate="TextBoxSedeLegaleCap"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCapSedeLegale" ControlToValidate="TextBoxSedeLegaleCap"
                ValidationGroup="stop" runat="server" ErrorMessage="Cap sede legale non corretto"
                ValidationExpression="^\d{5}$">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorSedeLegaleCAP" runat="server" ControlToValidate="TextBoxSedeLegaleCap"
                ErrorMessage="Non pu&#242; essere utilizzato un CAP generico per la sede legale"
                OnServerValidate="CustomValidatorCAP_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeLegalePreIndirizzo" runat="server" Width="100px"
                AppendDataBoundItems="True" />
            &nbsp;
            <asp:TextBox ID="TextBoxSedeLegaleIndirizzo" runat="server" MaxLength="40" Width="240px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIndirizzoSedeLegale" runat="server"
                ErrorMessage="Indirizzo sede legale mancante" ControlToValidate="TextBoxSedeLegaleIndirizzo"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPreIndirizzoSedeLegale" runat="server"
                ControlToValidate="DropDownListSedeLegalePreIndirizzo" ErrorMessage="Pre indirizzo sede legale mancante"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeLegaleTel" runat="server" MaxLength="20" Width="350px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTelSedeLegale" runat="server"
                ErrorMessage="Telefono sede legale mancante" ControlToValidate="TextBoxSedeLegaleTel"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorTelefonoSedeLegale"
                runat="server" ControlToValidate="TextBoxSedeLegaleTel" ErrorMessage="Telefono sede legale non corretto"
                ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeLegaleFax" runat="server" MaxLength="20" Width="350px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorFaxSedeLegale" runat="server"
                ErrorMessage="Fax sede legale mancante" ControlToValidate="TextBoxSedeLegaleFax"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorFaxSedeLegale" runat="server"
                ControlToValidate="TextBoxSedeLegaleFax" ErrorMessage="Fax sede legale non corretto"
                ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            E-mail:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeLegaleEmail" runat="server" MaxLength="50" Width="350px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmailSedeLegale" runat="server"
                ErrorMessage="E-mail sede legale mancante" ControlToValidate="TextBoxSedeLegaleEmail"
                ValidationGroup="sedi">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBoxSedeLegaleEmail"
                ErrorMessage="Email sede legale non corretta" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelSedeAmministrativa" runat="server" Text="Sede amministrativa"
                Font-Bold="True"></asp:Label>
            (da completare solo se differente dalla sede legale)
            <asp:CustomValidator ID="CustomValidatorSedeAmministrativa" runat="server" ErrorMessage="Mancano alcuni dati della sede amministrativa"
                ValidationGroup="sedi" OnServerValidate="CustomValidatorSedeAmministrativa_ServerValidate">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
        </td>
    </tr>
    <tr>
        <td>
            Provincia:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeAmmProvincia" runat="server" Width="350px"
                AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListSedeAmmProvincia_SelectedIndexChanged" />
        </td>
    </tr>
    <tr>
        <td>
            Località:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeAmmLocalita" runat="server" Width="350px" AppendDataBoundItems="True"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownListSedeAmmLocalita_SelectedIndexChanged" />
        </td>
    </tr>
    <tr>
        <td>
            Frazione:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeAmmFrazione" runat="server" Width="350px" AppendDataBoundItems="True"
                OnSelectedIndexChanged="DropDownListSedeAmmFrazione_SelectedIndexChanged" AutoPostBack="True" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            CAP:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeAmmCap" runat="server" MaxLength="5" Width="350px" />
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorSedeAmmCap" runat="server"
                ControlToValidate="TextBoxSedeAmmCap" ErrorMessage="Cap sede amministrativa non corretto"
                ValidationExpression="^\d{5}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <asp:CustomValidator ID="CustomValidatorSedeAmmCAP" runat="server" ControlToValidate="TextBoxSedeAmmCap"
                ErrorMessage="Non pu&#242; essere utilizzato un CAP generico per la sede amministrativa"
                OnServerValidate="CustomValidatorCAP_ServerValidate" ValidationGroup="stop">*</asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSedeAmmPreIndirizzo" runat="server" Width="100px"
                AppendDataBoundItems="True" />
            &nbsp;
            <asp:TextBox ID="TextBoxSedeAmmIndirizzo" runat="server" MaxLength="40" Width="240px" />
        </td>
    </tr>
    <tr>
        <td>
            Telefono:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeAmmTel" runat="server" MaxLength="20" Width="350px" />
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorSedeAmmTelefono" runat="server"
                ControlToValidate="TextBoxSedeAmmTel" ErrorMessage="Telefono sede amministrativa non corretto"
                ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            Fax:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeAmmFax" runat="server" MaxLength="20" Width="350px" />
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorSedeAmmFax" runat="server"
                ControlToValidate="TextBoxSedeAmmFax" ErrorMessage="Fax sede amministrativa non corretto"
                ValidationExpression="^\+?[\d]{3,19}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            E-mail:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSedeAmmEmail" runat="server" MaxLength="50" Width="350px" />
        </td>
        <td>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBoxSedeAmmEmail"
                ErrorMessage="Email sede amministrativa non corretta" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                ValidationGroup="stop">*</asp:RegularExpressionValidator>
        </td>
    </tr>
</table>
