﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Net;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Crm;
using TBridge.Cemi.Business.EmailClient;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.Cpt.Type.Collections;
using TBridge.Cemi.Cpt.Type.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneCE.Business;
using Telerik.Web.UI;
using System.Diagnostics;
using TBridge.Cemi.Business.Cpt;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Entities.IscrizioneCe;
using TBridge.Cemi.Type.Enums.IscrizioneCe;
using TBridge.Cemi.Type.Exceptions.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls
{
    public partial class IscrizioneCEIscrizione : System.Web.UI.UserControl
    {
        private const string cssCampoDisabilitato = "campoDisabilitato";
        private const int INDICEBANCA = 3;
        private const int INDICECANTIERE = 5;
        private const int INDICECONFERMA = 6;
        private const int INDICECONSULENTE = 1;
        private const int INDICEDATIGENERALI = 0;
        private const int INDICELEGALERAPPRESENTANTE = 4;
        private const int INDICESEDI = 2;

        private readonly IscrizioniManager biz = new IscrizioniManager();
        private readonly Common bizCommon = new Common();
        private readonly CptBusiness bizCpt = new CptBusiness();
        private readonly CodiceFiscaleManager bizCodFisc = new CodiceFiscaleManager();

        private bool modalitaPrevedi;
        private bool modalitaCantiere;

        #region Proprietà da file di configurazione

        private String CassaEdile
        {
            get
            {
                String res = "MI00";

                if (ConfigurationManager.AppSettings["CassaEdile"] != null)
                {
                    res = ConfigurationManager.AppSettings["CassaEdile"];
                }

                return res;
            }
        }

        private Boolean InvioEmail
        {
            get
            {
                Boolean res = true;

                if (ConfigurationManager.AppSettings["InvioEmail"] != null)
                {
                    Boolean tempRes = false;

                    if (Boolean.TryParse(ConfigurationManager.AppSettings["InvioEmail"], out tempRes))
                    {
                        res = tempRes;
                    }
                }

                return res;
            }
        }

        private String UrlConfermaInserimentoDomanda
        {
            get
            {
                String res = "~/CeServizi/IscrizioneCE/IscrizioneCompletata.aspx";
                //String res = "~/ReportIscrizioneCE.aspx";

                //if (ConfigurationManager.AppSettings["UrlConfermaInserimentoDomande"] != null)
                //{
                //    res = ConfigurationManager.AppSettings["UrlConfermaInserimentoDomande"];
                //}

                return res;
            }
        }

        private String ReportIscrizioneNormale
        {
            get
            {
                String res = "/ReportIscrizioneCE/ReportModuloIscrizione";

                if (ConfigurationManager.AppSettings["ReportIscrizioneOperai"] != null)
                {
                    res = ConfigurationManager.AppSettings["ReportIscrizioneOperai"];
                }

                return res;
            }
        }

        private String ReportIscrizionePrevedi
        {
            get
            {
                String res = "/ReportIscrizioneCE/ReportModuloIscrizionePrevedi";

                if (ConfigurationManager.AppSettings["ReportIscrizionePrevedi"] != null)
                {
                    res = ConfigurationManager.AppSettings["ReportIscrizionePrevedi"];
                }

                return res;
            }
        }

        private String FirmaEmail
        {
            get
            {
                String res = "Cassa Edile di Milano, Lodi, Monza e Brianza";

                if (ConfigurationManager.AppSettings["FirmaEmail"] != null)
                {
                    res = ConfigurationManager.AppSettings["FirmaEmail"];
                }

                return res;
            }
        }

        #endregion

        #region Page Init e Page Load

        protected void Page_Init(object sender, EventArgs e)
        {
            // Non visualizzo lo step "Consulente" se l'utente connesso è un consulente
            // oppure l'utente pubblico.
            // Devo farlo per forza nell'Init altrimenti ci sono problemi con il ViewState



            Utente utente = Membership.GetUser() as Utente;
            if (utente != null)
            {
                if (GestioneUtentiBiz.IsConsulente())
                {
                    WizardInserimento.WizardSteps.Remove(WizardStepConsulente);
                    rigaRiassuntoConsulente.Visible = false;
                }
                else
                {
                    IscrizioneCEDatiGenerali1.NascondiDatiConsulente(true);
                }
            }
            else
            {
                WizardInserimento.WizardSteps.Remove(WizardStepConsulente);
                rigaRiassuntoConsulente.Visible = false;
            }

            // Non visualizzo lo step Cantiere e Unico se il modulo da utilizzare è quello solo Prevedi
            if (Request.QueryString["tipoModulo"] != null && Request.QueryString["tipoModulo"] == "prevedi")
            {
                //WizardInserimento.WizardSteps.Remove(WizardStepCantiere);
                ModalitaPrevedi();
            }

            // Non visualizzo lo step Prevedi e Unico se il modulo da utilizzare è quello con Cantiere
            if (Request.QueryString["tipoModulo"] != null && (Request.QueryString["tipoModulo"] == "cantiere" || Request.QueryString["tipoModulo"] == "normale"))
            {
                //WizardInserimento.WizardSteps.Remove(WizardStepCantiere);
                ModalitaCantiere();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            TuteScarpeRicercaConsulenti1.OnConsulenteSelected += TuteScarpeRicercaConsulenti1_OnConsulenteSelected;

            Utente utente = Membership.GetUser() as Utente;

            if (GestioneUtentiBiz.IsConsulente())
            {
                ButtonSalva.Visible = true;
            }

            if (!Page.IsPostBack)
            {
                ViewState["indiceWizard"] = 0;
                ViewState["geocodificaOkSedeLegale"] = false;
                ViewState["geocodificaOkSedeAmministrativa"] = false;
                ViewState["geocodificaOkCantiere"] = false;
                ViewState["cantiereInNotifiche"] = false;
                ViewState["guid"] = Guid.NewGuid();

                if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "modifica" &&
                    Session["IdDomanda"] != null)
                {
                    int idDomanda = (int)Session["IdDomanda"];
                    CaricaDomanda(idDomanda);
                }
                else
                {
                    //CaricaDatiProva();
                }
            }

            #region Cambio colore link della sidebar

            // Per far funzionare il cambiamento di colore dei link della sidebar
            //Hashtable buttonsColl = new Hashtable();
            Control lst1 = WizardInserimento.FindControl("SideBarContainer");
            DataList lst = (DataList)lst1.FindControl("SideBarList");
            lst.ItemCreated += lst_ItemCreated;

            #endregion

            #region Click multipli

            // Per prevenire click multipli
            Button bConferma =
                (Button)
                WizardInserimento.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append(
                "if (Page_ClientValidate('stop') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(bConferma, null) + ";");
            sb.Append("return true;");
            bConferma.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(bConferma);

            #endregion
        }

        #endregion

        private void ModalitaPrevedi()
        {
            modalitaPrevedi = true;

            PanelCantiereNormale.Visible = false;
            PanelLavoratoriUnico.Visible = false;
            PanelCantierePrevedi.Visible = true;
            WizardStepCantiere.Title = "- Impiegati";
            LabelCantiereValidation.Text = "Impiegati";
        }

        private void ModalitaCantiere()
        {
            modalitaCantiere = true;

            PanelCantiereNormale.Visible = true;
            PanelLavoratoriUnico.Visible = false;
            PanelCantierePrevedi.Visible = false;
            WizardStepCantiere.Title = "- Cantiere";
            LabelCantiereValidation.Text = "Cantiere";
        }

        private void TuteScarpeRicercaConsulenti1_OnConsulenteSelected(Consulente consulente)
        {
            ViewState["IdConsulente"] = consulente.IdConsulente;
            ViewState["EmailConsulente"] = consulente.EMail;
            TextBoxConsulenteSelezionato.Text =
                String.Format("{0}\n{1}\n{2} {3} ({4})", consulente.RagioneSociale, consulente.CodiceFiscale,
                              consulente.Indirizzo, consulente.Comune, consulente.Provincia);
            PanelSelezionaConsulente.Visible = false;
        }

        private void CaricaDomanda(int idDomanda)
        {
            ModuloIscrizione domanda = biz.GetDomanda(idDomanda);

            if (domanda.TipoModulo == TipoModulo.Impiegati)
            {
                IscrizioneCEImpiegati1.CaricaDomanda(domanda);
                ModalitaPrevedi();
            }

            if (domanda.TipoModulo == TipoModulo.Operai)
            {
                if (domanda.Cantiere != null)
                {
                    //IscrizioneCELavoratori1.CaricaDomanda(domanda);
                    IscrizioneCECantiere1.CaricaDomanda(domanda);
                }
                ModalitaCantiere();
            }


            // Se la domanda è confermata non permettiamo la modifica della partita IVA
            //if (domanda.Confermata)
            //{
            //    TextBoxPIVA.Enabled = false;
            //    TextBoxPIVA.CssClass = "campoDisabilitato";
            //}

            // Se la domanda è compilata dal consulente non permettiamo la modifica del consulente
            if (domanda.CompilazioneConsulente && domanda.Confermata)
            {
                ButtonConsulenteSeleziona.Enabled = false;
            }

            // Caricamento di tutti dati nella form
            ViewState["IdConsulente"] = domanda.IdConsulente;
            ViewState["IdDomanda"] = domanda.IdDomanda.Value;

            IscrizioneCEDatiGenerali1.CaricaDomanda(domanda);
            IscrizioneCESedi1.CaricaDomanda(domanda);
            IscrizioneCEContoCorrente1.CaricaDomanda(domanda);
            IscrizioneCELegaleRappresentante1.CaricaDomanda(domanda);
            IscrizioneCELavoratori1.CaricaDomanda(domanda);

            /*
            if (domanda.Cantiere != null)
            {
                //IscrizioneCELavoratori1.CaricaDomanda(domanda);
                IscrizioneCECantiere1.CaricaDomanda(domanda);
            }
            else
            {
                IscrizioneCEImpiegati1.CaricaDomanda(domanda);
            }
            */

            // CONSULENTE
            if (domanda.Consulente != null)
            {
                ViewState["IdConsulente"] = domanda.Consulente.IdConsulente;
                TextBoxConsulenteSelezionato.Text = String.Format("{0}\n{1}\n{2} {3} ({4})",
                                                                  domanda.Consulente.RagioneSociale,
                                                                  domanda.Consulente.CodiceFiscale,
                                                                  domanda.Consulente.Indirizzo,
                                                                  domanda.Consulente.Comune, domanda.Consulente.Provincia);
                PanelSelezionaConsulente.Visible = false;

                if (domanda.CompilazioneConsulente)
                    PanelConsulenteSelezione.Enabled = false;
            }
        }

        private void lst_ItemCreated(object sender, DataListItemEventArgs e)
        {
            // Per far funzionare il cambiamento di colore dei link della sidebar
            int indiceWizard = (int)ViewState["indiceWizard"];
            LinkButton button = (LinkButton)e.Item.FindControl("SideBarButton");

            if (e.Item.ItemIndex > indiceWizard)
                button.ForeColor = Color.LightGray;
        }

        protected void ValidaPagina(String validationGroup, BulletedList listaErrori)
        {
            listaErrori.Items.Clear();

            foreach (IValidator validator in Page.Validators)
            {
                BaseValidator bValid = validator as BaseValidator;
                if (bValid != null && bValid.ValidationGroup == validationGroup)
                {
                    bValid.Validate();

                    if (!bValid.IsValid)
                    {
                        listaErrori.Items.Add(bValid.ErrorMessage);
                    }
                }
            }
        }

        private void InviaModuloIscrizione(ModuloIscrizione iscrizione, string wsReporting2005, string wsExecution2005,
                                           byte[] moduli)
        {
            // Devo capire a che indirizzo spedire la mail..
            string destinatario = biz.GetMailValidaPerInvio(iscrizione);

            // Per fare delle prove
            //destinatario = "a.mosto@tbridge.it";

            if (!string.IsNullOrEmpty(destinatario))
            {
                string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
                string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

                StringBuilder sbPlain = new StringBuilder();
                StringBuilder sbHtml = new StringBuilder();

                EmailInfoService service = new EmailInfoService();
                EmailMessageSerializzabile email = new EmailMessageSerializzabile();

                #region Text Plain
                sbPlain.AppendFormat("Spettabile impresa {0},", iscrizione.RagioneSociale);
                sbPlain.Append("\n\n");
                sbPlain.Append("si allega la domanda di iscrizione da Voi compilata. ");
                sbPlain.Append("Al fine di considerare ultimata la procedura di iscrizione, la domanda dovrà essere inviata firmata dal legale rappresentante all'indirizzo di Posta Elettronica Certificata iscrizioneimprese@cassaedilemilano.legalmail.it");
                sbPlain.Append("\n\n");
                sbPlain.Append("Per ogni ulteriore informazione è possibile contattare il seguente numero telefonico: 02-584961, tasto \"1\" Servizi per le imprese, tasto \"4\" Iscrizioni e variazioni anagrafiche.");
                sbPlain.Append("\n\n");
                sbPlain.Append("Cordiali saluti.");
                sbPlain.Append("\n\n");
                sbPlain.Append("Ufficio Servizi alle Imprese\n");
                sbPlain.Append(FirmaEmail);
                email.BodyPlain = sbPlain.ToString();
                #endregion

                #region Text Html
                sbHtml.AppendFormat("Spettabile impresa {0},", iscrizione.RagioneSociale);
                sbHtml.Append("<br /><br />");
                sbHtml.Append("si allega la domanda di iscrizione da Voi compilata. ");
                sbHtml.Append("Al fine di considerare ultimata la procedura di iscrizione, la domanda dovrà essere inviata firmata dal legale rappresentante all'indirizzo di Posta Elettronica Certificata <a href=\"mailto:iscrizioneimprese@cassaedilemilano.legalmail.it\">iscrizioneimprese@cassaedilemilano.legalmail.it</a>");
                sbHtml.Append("<br /><br />");
                sbHtml.Append("Per ogni ulteriore informazione è possibile contattare il seguente numero telefonico: 02-584961, tasto \"1\" Servizi per le imprese, tasto \"4\" Iscrizioni e variazioni anagrafiche.");
                sbHtml.Append("<br /><br />");
                sbHtml.Append("Cordiali saluti.");
                sbHtml.Append("<br /><br />");
                sbHtml.Append("Ufficio Servizi alle Imprese");
                sbHtml.Append("<br />");
                sbHtml.Append(FirmaEmail);
                email.BodyHTML = sbHtml.ToString();
                #endregion

                email.Destinatari = new List<EmailAddress>();
                email.Destinatari.Add(new EmailAddress(iscrizione.RagioneSociale, destinatario));
                //email.Destinatari[0] = new EmailAddress();

                //email.Destinatari[0].Indirizzo = destinatario;
                //email.Destinatari[0].Nome = iscrizione.RagioneSociale;

                email.Mittente = new EmailAddress();
                email.Mittente.Indirizzo = "iscrizioneimprese@cassaedilemilano.legalmail.it";
                email.Mittente.Nome = "Cassa Edile di Milano - Ufficio Servizi alle Imprese";

                email.ReplyTo.Indirizzo = "iscrizioneimprese@cassaedilemilano.legalmail.it";
                email.ReplyTo.Nome = "Cassa Edile di Milano - Ufficio Servizi alle Imprese";

                email.Oggetto = "Avvenuto invio telematico domanda di iscrizione alla Cassa Edile di Milano, Lodi, Monza e Brianza";

                email.Allegati = new List<EmailAttachment>();
                email.Allegati.Add(new EmailAttachment("moduli.pdf", moduli));
                //email.Allegati[0] = new EmailAttachment();
                //email.Allegati[0].File = moduli;
                //email.Allegati[0].NomeFile = "moduli.pdf";

                email.DataSchedulata = DateTime.Now;
                email.Priorita = MailPriority.High;

                // per CRM
                email.DestinatariBCC = new List<EmailAddress>();
                email.DestinatariBCC.Add(new EmailAddress(String.Empty, ConfigurationManager.AppSettings["emailCrmIscrizioneCE"]));
                //email.DestinatariBCC[0] = new EmailAddress();
                //email.DestinatariBCC[0].Indirizzo =
                //    ConfigurationManager.AppSettings["emailCrmIscrizioneCE"];

                NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
                service.Credentials = credentials;

                service.InviaEmail(email);
            }
        }

        protected void ImpostaRiassunto(int index)
        {
            bool stepConsulentePresente = false;

            Utente utente = Membership.GetUser() as Utente;
            if (!(utente == null)
                && !GestioneUtentiBiz.IsConsulente())
                stepConsulentePresente = true;

            if ((stepConsulentePresente && (index == INDICECONFERMA)) ||
                (!stepConsulentePresente && (index == (INDICECONFERMA - 1))))
            {
                bool permettiInserimento = true;
                BulletedListAvvertimenti.Items.Clear();
                BulletedListConsulente.Items.Clear();

                #region Altri controlli Dati Generali

                //// Controllo che se il tipo di impresa è Artigiana allora sia fornito un numero di iscrizione all'albo delle imprese artigiane
                //if (DropDownListTipoImpresa.SelectedValue == "A" && string.IsNullOrEmpty(TextBoxNumImpreseArtigiane.Text))
                //    if (BulletedListDatiGenerali.Items.FindByValue("0") == null)
                //        BulletedListDatiGenerali.Items.Add(
                //            new ListItem("Deve essere specificato il N° di iscrizione all'albo delle imprese artigiane", "0"));

                // Controllo che se il codice fiscale inserito per l'impresa è del formato per le persone sia quello del legale rappresentante
                String codiceFiscale = IscrizioneCEDatiGenerali1.CodiceFiscale;
                String codiceFiscaleLegaleRappresentante = IscrizioneCELegaleRappresentante1.CodiceFiscale;
                if (codiceFiscale.Length == 16 && codiceFiscale != codiceFiscaleLegaleRappresentante)
                    if (BulletedListDatiGenerali.Items.FindByValue("1") == null)
                        BulletedListDatiGenerali.Items.Add(
                            new ListItem("Il codice fiscale non può essere diverso da quello del legale rappresentante", "1"));

                String NumeroCameraCommercio = IscrizioneCEDatiGenerali1.NumeroCameraCommercio;
                String NumeroCameraCommercioArtigiani = IscrizioneCEDatiGenerali1.NumeroCameraCommercioArtigiani;
                if (String.IsNullOrEmpty(NumeroCameraCommercio) && String.IsNullOrEmpty(NumeroCameraCommercioArtigiani))
                {
                    if (BulletedListDatiGenerali.Items.FindByValue("2") == null)
                        BulletedListDatiGenerali.Items.Add(
                            new ListItem("I campi \"N° iscrizione al Registro delle Imprese\" e \"N° iscrizione al Registro delle Imprese - sezione speciale\" non possono essere entrambi vuoti", "2"));
                }

                #endregion

                #region Controlli Consulente

                String tipoInvioDenuncia = IscrizioneCEDatiGenerali1.TipoInvioDenuncia;
                if (utente != null &&
                    !GestioneUtentiBiz.IsConsulente()
                    && tipoInvioDenuncia == "C"
                    && ViewState["IdConsulente"] == null)
                    if (BulletedListConsulente.Items.FindByValue("1") == null)
                        BulletedListConsulente.Items.Add(
                            new ListItem(
                                "Deve essere selezionato un consulente perchè è stato scelto Consulente come Invio denuncia",
                                "1"));

                #endregion

                #region Altri controlli Sedi

                //// Controllo che vengono immessi alcuni dati per la sede amministrativa, devono essere immessi tutti
                //if (string.IsNullOrEmpty(DropDownListSedeAmmPreIndirizzo.SelectedValue) ||
                //    string.IsNullOrEmpty(TextBoxSedeAmmIndirizzo.Text)
                //    || string.IsNullOrEmpty(DropDownListSedeAmmProvincia.SelectedValue) ||
                //    string.IsNullOrEmpty(DropDownListSedeAmmLocalita.SelectedValue)
                //    || string.IsNullOrEmpty(TextBoxSedeAmmCap.Text) || string.IsNullOrEmpty(TextBoxSedeAmmTel.Text) ||
                //    string.IsNullOrEmpty(TextBoxSedeAmmFax.Text))
                //{
                //    if (
                //        !(string.IsNullOrEmpty(DropDownListSedeAmmPreIndirizzo.SelectedValue) &&
                //          string.IsNullOrEmpty(TextBoxSedeAmmIndirizzo.Text)
                //          && string.IsNullOrEmpty(DropDownListSedeAmmProvincia.SelectedValue) &&
                //          string.IsNullOrEmpty(DropDownListSedeAmmLocalita.SelectedValue)
                //          && string.IsNullOrEmpty(TextBoxSedeAmmCap.Text) && string.IsNullOrEmpty(TextBoxSedeAmmTel.Text) &&
                //          string.IsNullOrEmpty(TextBoxSedeAmmFax.Text)))
                //    {
                //        if (BulletedListSedi.Items.FindByValue("0") == null)
                //            BulletedListSedi.Items.Add(new ListItem("Mancano alcuni dati della sede amministrativa", "0"));
                //    }
                //}

                String emailConsulente = IscrizioneCEDatiGenerali1.EmailConsulente;
                String emailSedeLegale = IscrizioneCESedi1.EmailSedeLegale;
                String emailSedeAmministrativa = IscrizioneCESedi1.EmailSedeAmministrativa;


                // Controlliamo che l'email del consulente (indicato o selezionato) non sia uguale alla sede legale o
                // amministrativa
                if ((!String.IsNullOrEmpty(emailSedeLegale)
                    && tipoInvioDenuncia == "C"
                    && !String.IsNullOrEmpty(emailConsulente)
                    && emailConsulente == emailSedeLegale)
                    ||
                    (!String.IsNullOrEmpty(emailSedeLegale)
                    && ViewState["EmailConsulente"] != null
                    && ViewState["EmailConsulente"].ToString() == emailSedeLegale))
                {
                    BulletedListAvvertimenti.Items.Add(
                                "L'indirizzo email della sede legale deve appartenere all'impresa e non al consulente");
                }

                if (!String.IsNullOrEmpty(emailSedeAmministrativa)
                    && tipoInvioDenuncia == "C"
                    && !String.IsNullOrEmpty(emailConsulente)
                    && emailConsulente == emailSedeAmministrativa
                    ||
                    (!String.IsNullOrEmpty(emailSedeAmministrativa)
                    && ViewState["EmailConsulente"] != null
                    && ViewState["EmailConsulente"].ToString() == emailSedeAmministrativa))
                {
                    BulletedListAvvertimenti.Items.Add(
                                "L'indirizzo email della sede amministrativa deve appartenere all'impresa e non al consulente");
                }

                #endregion

                #region Altri controlli Legale Rappresentante

                // Controllo la correttezza del codice fiscale del legale rappresentante
                try
                {
                    String rappCognome = IscrizioneCELegaleRappresentante1.Cognome;
                    String rappNome = IscrizioneCELegaleRappresentante1.Nome;
                    String rappCodiceFiscale = IscrizioneCELegaleRappresentante1.CodiceFiscale;
                    String rappDataNascita = IscrizioneCELegaleRappresentante1.DataNascita;
                    String rappComuneNascita = IscrizioneCELegaleRappresentante1.ComuneNascita;
                    String rappSesso = IscrizioneCELegaleRappresentante1.Sesso;

                    if (!String.IsNullOrEmpty(rappDataNascita) &&
                        !String.IsNullOrEmpty(rappComuneNascita))
                    {
                        String codiceCatastale = biz.GetCodiceCatastaleDaCombo(rappComuneNascita);
                        ;

                        if (!CodiceFiscaleManager.VerificaPrimi11CaratteriCodiceFiscale(rappNome,
                                                                             rappCognome,
                                                                             rappSesso,
                                                                             DateTime.Parse(
                                                                                 rappDataNascita),
                                                                             rappCodiceFiscale))

                            /*if (!bizCommon.VerificaPrimi11CaratteriCodiceFiscale(rappNome,
                                                                                 rappCognome,
                                                                                 rappSesso,
                                                                                 DateTime.Parse(
                                                                                     rappDataNascita),
                                                                                 rappCodiceFiscale))*/
                            BulletedListAvvertimenti.Items.Add(
                                "Il codice fiscale del legale rappresentante potrebbe non essere corretto");
                    }
                }
                catch
                {
                }

                //// Controllo la correttezza del codice fiscale del legale rappresentante (primi 11 caratteri, bloccante)
                //try
                //{
                //    if (!string.IsNullOrEmpty(TextBoxRappLegaleDataNascita.Text))
                //    {
                //        string sesso;
                //        if (RadioButtonRappLegaleSessoM.Checked)
                //            sesso = "M";
                //        else
                //            sesso = "F";

                //        if (!bizCommon.VerificaPrimi11CaratteriCodiceFiscale(TextBoxRappLegaleNome.Text.Trim().ToUpper(),
                //                                                             TextBoxRappLegaleCognome.Text.Trim().ToUpper(),
                //                                                             sesso,
                //                                                             DateTime.Parse(
                //                                                                 TextBoxRappLegaleDataNascita.Text),
                //                                                             TextBoxRappLegaleCF.Text.Trim().ToUpper()))
                //            if (BulletedListLegaleRappresentante.Items.FindByValue("1") == null)
                //                BulletedListLegaleRappresentante.Items.Add(
                //                    "Il codice fiscale del legale rappresentante non è corretto");
                //    }
                //}
                //catch
                //{
                //}

                #endregion

                #region Altri controlli Cantiere

                // Controllo che almeno un lavoratore sia stato inserito se non siamo in presenza di Impresa cooperativa o interinale
                String numOpe = IscrizioneCELavoratori1.NumeroOperai;
                String numImp = IscrizioneCELavoratori1.NumeroImpiegati;
                int opeNum = 0;
                int impNum = 0;

                if (!String.IsNullOrEmpty(IscrizioneCELavoratori1.NumeroOperai))
                    opeNum = Convert.ToInt32(IscrizioneCELavoratori1.NumeroOperai);

                if (!String.IsNullOrEmpty(IscrizioneCELavoratori1.NumeroImpiegati))
                    impNum = Convert.ToInt32(IscrizioneCELavoratori1.NumeroImpiegati);

                if (((opeNum + impNum) < 1) && (/*IscrizioneCEDatiGenerali1.FormaGiuridica != "01" && */ IscrizioneCEDatiGenerali1.CodiceAttivita != "78.20.00"))
                    if (BulletedListCantiere.Items.FindByValue("2") == null)
                        BulletedListCantiere.Items.Add(new ListItem("Inserire almeno un operaio o un impiegato", "2"));


                //// Controllo che se valorizzato, il numero dei lavoratori occupati sia > 0
                //int lavOcc;
                //if (!string.IsNullOrEmpty(TextBoxLavoratoriOccupati.Text) &&
                //    Int32.TryParse(TextBoxLavoratoriOccupati.Text, out lavOcc) && lavOcc < 1)
                //    if (BulletedListCantiere.Items.FindByValue("0") == null)
                //        BulletedListCantiere.Items.Add(
                //            new ListItem("Il numero dei lavoratori occupati deve essere maggiore di 0", "0"));

                // Controllo che se valorizzato, il numero degli impiegati sia > 0
                //int impOcc;
                //if (!string.IsNullOrEmpty(TextBoxLavoratoriOccupatiPrevedi.Text) &&
                //    Int32.TryParse(TextBoxLavoratoriOccupatiPrevedi.Text, out impOcc) && impOcc < 1)
                //    if (BulletedListCantiere.Items.FindByValue("1") == null)
                //        BulletedListCantiere.Items.Add(
                //            new ListItem("Il numero degli impiegati deve essere maggiore di 0", "1"));

                #endregion

                #region Controlli per visualizzazioni

                // Dati generali
                if (BulletedListDatiGenerali.Items.Count == 0)
                    LabelRiassuntoDatiGenerali.Visible = true;
                else
                {
                    LabelRiassuntoDatiGenerali.Visible = false;
                    permettiInserimento = false;
                }

                // Consulente
                if (BulletedListConsulente.Items.Count == 0)
                    LabelRiassuntoConsulente.Visible = true;
                else
                {
                    LabelRiassuntoConsulente.Visible = false;
                    permettiInserimento = false;
                }

                // Sedi
                if (BulletedListSedi.Items.Count == 0)
                    LabelRiassuntoSedi.Visible = true;
                else
                {
                    LabelRiassuntoSedi.Visible = false;
                    permettiInserimento = false;
                }

                // Banca
                if (BulletedListBanca.Items.Count == 0)
                    LabelRiassuntoBanca.Visible = true;
                else
                {
                    LabelRiassuntoBanca.Visible = false;
                    permettiInserimento = false;
                }

                // Legale rappresentante
                if (BulletedListLegaleRappresentante.Items.Count == 0)
                    LabelRiassuntoLegaleRappresentante.Visible = true;
                else
                {
                    LabelRiassuntoLegaleRappresentante.Visible = false;
                    permettiInserimento = false;
                }

                // Cantiere
                if (BulletedListCantiere.Items.Count == 0)
                    LabelRiassuntoCantiere.Visible = true;
                else
                {
                    LabelRiassuntoCantiere.Visible = false;
                    permettiInserimento = false;
                }

                #endregion

                ControllaDatiDB(ref permettiInserimento);

                Button bConferma =
                    (Button)
                    WizardInserimento.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
                bConferma.Enabled = permettiInserimento;
            }
        }

        private void ControllaDatiDB(ref bool permettiInserimento)
        {
            // Effettuo i controlli obbligatori che vanno effettuati sui dati del DB

            #region Controlli bloccanti

            if (CassaEdile == "MI00")
            {
                string partitaIva = IscrizioneCEDatiGenerali1.PartitaIva;
                string codiceFiscale = IscrizioneCEDatiGenerali1.CodiceFiscale;

                int? idDomanda = null;
                if (Session["IdDomanda"] != null)
                    idDomanda = (int)Session["IdDomanda"];

                // CF e partita IVA
                if (!string.IsNullOrEmpty(partitaIva) &&
                    (biz.PartitaIvaEsistente(partitaIva) || biz.PartitaIvaEsistenteDomande(partitaIva, idDomanda)))
                {
                    LabelErroreBloccante.Text = "Partita IVA non valida per la registrazione, contattare Cassa Edile";
                    permettiInserimento = false;
                }
                else
                {
                    if (!string.IsNullOrEmpty(codiceFiscale) &&
                        (biz.CodiceFiscaleEsistente(codiceFiscale) ||
                         biz.CodiceFiscaleEsistenteDomande(codiceFiscale, idDomanda)))
                    {
                        LabelErroreBloccante.Text = "Codice fiscale non valido per la registrazione, contattare Cassa Edile";
                        permettiInserimento = false;
                    }
                    else
                    {
                        LabelErroreBloccante.Text = string.Empty;
                    }
                }
            }

            #endregion

            #region Controlli non bloccanti

            // Correttezza indirizzi sede

            // Sede legale (obbligatoria)
            Boolean esisteGeocodificaSedeLegale = IscrizioneCESedi1.EsisteGeocodificaIndirizzoSedeLegale();
            ViewState["geocodificaOkSedeLegale"] = esisteGeocodificaSedeLegale;
            if (!esisteGeocodificaSedeLegale)
            {
                BulletedListAvvertimenti.Items.Add(
                    "L'indirizzo utilizzato per la sede legale non è stato riconosciuto dal sistema");
            }

            // Sede amministrativa
            Boolean esisteGeocodificaSedeAmministrativa = IscrizioneCESedi1.EsisteGeocodificaIndirizzoSedeAmministrativa();
            ViewState["geocodificaOkSedeAmministrativa"] = esisteGeocodificaSedeAmministrativa;
            if (!esisteGeocodificaSedeAmministrativa)
            {
                BulletedListAvvertimenti.Items.Add(
                    "L'indirizzo utilizzato per la sede amministrativa non è stato riconosciuto dal sistema");
            }

            /*

            if (CassaEdile == "MI00")
            {
                // Presenza indirizzo cantiere in notifiche
                String indirizzo = IscrizioneCECantiere1.Indirizzo;
                if (!string.IsNullOrEmpty(indirizzo))
                {
                    String comune = IscrizioneCECantiere1.Comune;
                    String provincia = IscrizioneCECantiere1.Provincia;

                    String indirizzoGeocoder = String.Format("{0} {1} {2}, Italy", indirizzo, comune, provincia);
                    IndirizzoCollection indirizzi = CantieriGeocoding.GeoCodeGoogleMultiplo(indirizzoGeocoder);
                    if (indirizzi == null || indirizzi.Count != 1)
                    {
                        // Controllo correttezza indirizzo
                        BulletedListAvvertimenti.Items.Add(
                            "L'indirizzo utilizzato per il cantiere non è stato riconosciuto dal sistema");
                        ViewState["geocodificaOkCantiere"] = false;
                    }
                    else
                    {
                        ViewState["geocodificaOkCantiere"] = true;

                        // Controllo presenza in notifica
                        NotificaFilter filtro = new NotificaFilter();
                        filtro.Indirizzo = indirizzi[0].Indirizzo1;
                        filtro.IndirizzoComune = indirizzi[0].Comune;
                        NotificaCollection notifiche =
                            bizCpt.RicercaNotifiche(filtro);
                        if (notifiche == null || notifiche.Count == 0)
                        {
                            BulletedListAvvertimenti.Items.Add(
                                "L'indirizzo utilizzato per il cantiere non è presente nelle notifiche preliminari");
                            ViewState["cantiereInNotifiche"] = false;
                        }
                        else
                        {
                            ViewState["cantiereInNotifiche"] = true;
                        }
                    }
                }
            }
            */

            #endregion

            if (BulletedListAvvertimenti.Items.Count > 0)
            {
                if (permettiInserimento)
                    LabelErroreNonBloccante.Text =
                        @"Attenzione, ci sono alcuni dati che potrebbero essere stati inseriti in modo errato. Siete pregati di verificarne l'esattezza. Se siete sicuri della correttezza dei dati cliccate sul tasto ""Conferma domanda""";
                else
                    LabelErroreNonBloccante.Text =
                        @"Attenzione, ci sono alcuni dati che potrebbero essere stati inseriti in modo errato. Siete pregati di verificarne l'esattezza.";
            }
            else
            {
                LabelErroreNonBloccante.Text = string.Empty;
            }
        }

        protected ModuloIscrizione CreaModuloIscrizione(bool confermata)
        {
            ModuloIscrizione modulo = new ModuloIscrizione();
            modulo.Confermata = confermata;
            modulo.guid = (Guid)ViewState["guid"];

            if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "modifica" &&
                ViewState["IdDomanda"] != null)
            {
                // Sono in modalita modifica, sicuramente esistono gli Id nel ViewState..
                modulo.IdDomanda = (int)ViewState["IdDomanda"];
                //modulo.SedeLegale.IdIndirizzo = (int)ViewState["IdIndirizzo1"];
                //modulo.SedeAmministrativa.IdIndirizzo = (int)ViewState["IdIndirizzo2"];
                //modulo.LegaleRappresentante.IdLavoratore = (int)ViewState["IdLegaleRappresentante"];
                //modulo.Cantiere.IdCantiere = (int)ViewState["IdCantiere"];
                modulo.IdConsulente = (int?)ViewState["IdConsulente"];
            }
            else
            {
                if (GestioneUtentiBiz.IsConsulente())
                {
                    Consulente consulente =
                        (Consulente)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    modulo.IdConsulente = consulente.IdConsulente;
                    modulo.CompilazioneConsulente = true;
                }
                else if (ViewState["IdConsulente"] != null)
                    modulo.IdConsulente = (int)ViewState["IdConsulente"];
            }

            IscrizioneCEDatiGenerali1.CompletaDomandaConDatiGenerali(modulo);
            IscrizioneCESedi1.CompletaDomandaConSedi(modulo);
            IscrizioneCEContoCorrente1.CompletaDomandaConContoCorrente(modulo);
            IscrizioneCELegaleRappresentante1.CompletaDomandaConLegaleRappresentante(modulo);

            // Cantiere
            if (modalitaPrevedi)
            {
                IscrizioneCEImpiegati1.CompletaDomandaConImpiegati(modulo);
            }
            else if (modalitaCantiere)
            {
                IscrizioneCECantiere1.CompletaDomandaConCantiere(modulo);
            }
            else
            {
                IscrizioneCELavoratori1.CompletaDomanda(modulo);
            }

            // Geocodifica
            modulo.GeocodificaOkSedeLegale = (bool)ViewState["geocodificaOkSedeLegale"];
            modulo.GeocodificaOkSedeAmministrativa = (bool)ViewState["geocodificaOkSedeAmministrativa"];
            modulo.GeocodificaOkCantiere = (bool)ViewState["geocodificaOkCantiere"];
            modulo.CantiereInNotifiche = (bool)ViewState["cantiereInNotifiche"];

            // Utente che ha effettuato l'inserimento
            Utente utente = Membership.GetUser() as Utente;
            if (utente != null)
            {
                if (Request.QueryString["modalita"] == null)
                    modulo.IdUtente = utente.IdUtente;
            }

            // Tipo modulo
            if (modalitaPrevedi)
                modulo.TipoModulo = TipoModulo.Impiegati;
            else if (modalitaCantiere)
                modulo.TipoModulo = TipoModulo.Operai;
            else
                modulo.TipoModulo = TipoModulo.Unico;

            return modulo;
        }

        protected void ButtonSalva_Click(object sender, EventArgs e)
        {
            LabelCodiceFiscalePartitaIvaPresente.Visible = false;
            Page.Validate("stop");

            if (Page.IsValid)
            {
                string wsReporting2005 = ConfigurationManager.AppSettings["WSReportingServices2005"];
                string wsExecution2005 = ConfigurationManager.AppSettings["WSReportingExecution2005"];

                ModuloIscrizione iscrizione = CreaModuloIscrizione(true);
                iscrizione.Confermata = false;
                try
                {
                    if (biz.InsertIscrizione(iscrizione, wsReporting2005, wsExecution2005, true))
                    {
                        LabelErrore.Visible = false;
                        Response.Redirect("~/CeServizi/IscrizioneCE/ConfermaSalvataggioDomanda.aspx");
                    }
                    else
                    {
                        LabelErrore.Visible = true;
                    }
                }
                catch (PartitaIvaCodiceFiscaleGiaPresenteException)
                {
                    LabelCodiceFiscalePartitaIvaPresente.Visible = true;
                }
            }
        }

        protected void ButtonConsulenteSeleziona_Click(object sender, EventArgs e)
        {
            PanelSelezionaConsulente.Visible = true;
        }

        #region Eventi del Wizard

        protected void WizardInserimento_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
        {
            // Dalla SideBar non si può andare avanti, ma solo indietro.
            // In questo modo risultano obbligate le validazioni fatte di pagina in pagina
            if (e.NextStepIndex >= e.CurrentStepIndex)
                e.Cancel = true;
            else
            {
                ViewState["indiceWizard"] = e.NextStepIndex;
                ImpostaRiassunto(e.NextStepIndex);
            }
        }

        protected void WizardInserimento_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            bool stepConsulentePresente = false;

            Page.Validate("stop");

            // Se c'è 
            //if (HttpContext.Current.User != null)
            //{
            //    if (!(HttpContext.Current.User.Identity.GetType() == typeof (UtentePubblico))
            //        && !GestioneUtentiBiz.IsConsulente())
            //        stepConsulentePresente = true;
            //}
            Utente utente = Membership.GetUser() as Utente;
            if (utente != null)
            {
                if (!GestioneUtentiBiz.IsConsulente())
                    stepConsulentePresente = true;
            }

            if (stepConsulentePresente)
            {
                switch (e.CurrentStepIndex)
                {
                    case INDICEDATIGENERALI:
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryDatiGenerali.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryDatiGenerali.Visible = false;
                            ValidaPagina("datiGenerali", BulletedListDatiGenerali);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case INDICECONSULENTE:
                        //TODO bisogna capire cosa fare in questo step: chiedere a mura
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummarySedi.Visible = true;
                        }
                        else
                        {
                            //ValidaPagina(BulletedList);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case INDICESEDI:
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummarySedi.Visible = true;
                        }
                        else
                        {
                            //PanelSummarySedi.Visible = false;
                            ValidaPagina("sedi", BulletedListSedi);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case INDICEBANCA:
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryBanca.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryBanca.Visible = false;
                            ValidaPagina("banca", BulletedListBanca);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case INDICELEGALERAPPRESENTANTE:
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryLegaleRappresentante.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryLegaleRappresentante.Visible = false;
                            ValidaPagina("legaleRappresentante", BulletedListLegaleRappresentante);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case INDICECANTIERE:
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryCantiere.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryCantiere.Visible = false;
                            ValidaPagina("cantiere", BulletedListCantiere);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                }
            }
            else
            {
                switch (e.CurrentStepIndex)
                {
                    case INDICEDATIGENERALI:
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryDatiGenerali.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryDatiGenerali.Visible = false;
                            ValidaPagina("datiGenerali", BulletedListDatiGenerali);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case (INDICESEDI - 1):
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummarySedi.Visible = true;
                        }
                        else
                        {
                            //PanelSummarySedi.Visible = false;
                            ValidaPagina("sedi", BulletedListSedi);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case (INDICEBANCA - 1):
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryBanca.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryBanca.Visible = false;
                            ValidaPagina("banca", BulletedListBanca);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case (INDICELEGALERAPPRESENTANTE - 1):
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryLegaleRappresentante.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryLegaleRappresentante.Visible = false;
                            ValidaPagina("legaleRappresentante", BulletedListLegaleRappresentante);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                    case (INDICECANTIERE - 1):
                        if (!Page.IsValid)
                        {
                            e.Cancel = true;
                            //PanelSummaryCantiere.Visible = true;
                        }
                        else
                        {
                            //PanelSummaryCantiere.Visible = false;
                            ValidaPagina("cantiere", BulletedListCantiere);
                            ViewState["indiceWizard"] = e.NextStepIndex;
                            ImpostaRiassunto(e.NextStepIndex);
                        }
                        break;
                }
            }
        }

        protected void WizardInserimento_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
        {
            ViewState["indiceWizard"] = e.NextStepIndex;
        }

        protected void WizardInserimento_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            LabelDomandaGiaInserita.Visible = false;
            LabelCodiceFiscalePartitaIvaPresente.Visible = false;

            Boolean modificaAbilitata = false;
            if (!String.IsNullOrEmpty(Request.QueryString["op"]) && Request.QueryString["op"] == "cemi")
            {
                modificaAbilitata = true;
            }

            // Conferma della domanda
            // I controlli sulla correttezza della domanda sono stati fatti in precedenza.
            // Se è possibile cliccare sul bottone di fine sono sicuro che la domanda sia corretta

            string wsReporting2005 = ConfigurationManager.AppSettings["WSReportingServices2005"];
            string wsExecution2005 = ConfigurationManager.AppSettings["WSReportingExecution2005"];
            bool mailDaInviare = false;

            ModuloIscrizione iscrizione = CreaModuloIscrizione(true);
            iscrizione.Confermata = true;
            iscrizione.Stato = StatoDomanda.DaValutare;

            Utente utente = Membership.GetUser() as Utente;
            if (!iscrizione.IdDomanda.HasValue &&
                ((utente == null)
                 ||
                 (GestioneUtentiBiz.IsConsulente())))
            {
                mailDaInviare = true;
            }
            mailDaInviare = mailDaInviare && InvioEmail;
            //mailDaInviare = true;

            try
            {
                using (EventLog appLog = new EventLog())
                {
                    appLog.Source = "SiceInfo";
                    appLog.WriteEntry(String.Format("Invio Email Iscrizione Bool: {0}",
                                                    mailDaInviare));
                }
            }
            catch { }

            try
            {
                if (biz.InsertIscrizione(iscrizione, wsReporting2005, wsExecution2005, modificaAbilitata))
                {
                    byte[] moduli = null;

                    try
                    {
                        moduli = biz.CreaModuloPdfIscrizioneCE(
                            iscrizione.IdDomanda.Value, iscrizione.TipoModulo, wsReporting2005, wsExecution2005,
                            ReportIscrizioneNormale, ReportIscrizionePrevedi);

                        biz.UpdateIscrizioneSalvaDocumento(iscrizione.IdDomanda.Value, moduli);
                    }
                    catch
                    {
                    }

                    if (mailDaInviare)
                    {
                        try
                        {
                            InviaModuloIscrizione(iscrizione, wsReporting2005, wsExecution2005, moduli);
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                using (EventLog appLog = new EventLog())
                                {
                                    appLog.Source = "SiceInfo";
                                    appLog.WriteEntry(String.Format("Eccezione Invio Email Iscrizione: {0} - {1} - {2}",
                                                                    iscrizione.RagioneSociale,
                                                                    ex.Message,
                                                                    ex.InnerException != null ? ex.InnerException.Message : String.Empty));
                                }
                            }
                            catch { }
                        }
                    }

                    if (CassaEdile == "MI00")
                    {
                        // per CRM
                        string mail = biz.GetMailValidaPerInvio(iscrizione);

                        try
                        {
                            IscrizioneCeManager.GestisciSR(iscrizione.RagioneSociale, mail, iscrizione.IdImpresa,
                                                           iscrizione.PartitaIva, iscrizione.IdConsulente,
                                                           iscrizione.CodiceFiscaleConsulente,
                                                           iscrizione.RagioneSocialeConsulente, iscrizione.Stato);
                        }
                        catch { }
                    }

                    Session["IdDomanda"] = iscrizione.IdDomanda;
                    LabelErrore.Visible = false;
                    LabelDomandaGiaInserita.Visible = false;

                    StringBuilder urlDestinazione = new StringBuilder();
                    urlDestinazione.Append(UrlConfermaInserimentoDomanda);
                    urlDestinazione.Append("?tipoModulo=");
                    if (modalitaPrevedi)
                        urlDestinazione.Append("prevedi");
                    else if (modalitaCantiere)
                        urlDestinazione.Append("normale");
                    else
                        urlDestinazione.Append("unico");

                    // Redireziono su pagina di avvenuta comunicazione
                    Response.Redirect(urlDestinazione.ToString());
                }
                else
                    LabelErrore.Visible = true;
            }
            catch (DomandaGiaInseritaException)
            {
                LabelDomandaGiaInserita.Visible = true;
            }
            catch (PartitaIvaCodiceFiscaleGiaPresenteException)
            {
                LabelCodiceFiscalePartitaIvaPresente.Visible = true;
            }
        }

        #endregion

        #region Funzione per caricare dei dati di prova

        private void CaricaDatiProva()
        {
            IscrizioneCEDatiGenerali1.CaricaDatiProva();
            IscrizioneCESedi1.CaricaDatiProva();
            IscrizioneCEContoCorrente1.CaricaDatiProva();
            IscrizioneCELegaleRappresentante1.CaricaDatiProva();
            //IscrizioneCECantiere1.CaricaDatiProva();
        }

        #endregion
    }
}