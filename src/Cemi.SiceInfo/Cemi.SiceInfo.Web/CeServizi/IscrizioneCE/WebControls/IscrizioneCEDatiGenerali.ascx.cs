﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Utility;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.IscrizioneCE.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Entities.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls
{
    public partial class IscrizioneCEDatiGenerali : System.Web.UI.UserControl
    {
        private const string cssCampoDisabilitato = "campoDisabilitato";

        private readonly IscrizioniManager biz = new IscrizioniManager();
        private readonly Common bizCommon = new Common();

        private Boolean moduloNormale;

        private String CassaEdile
        {
            get
            {
                String res = "MI00";

                if (ConfigurationManager.AppSettings["CassaEdile"] != null)
                {
                    res = ConfigurationManager.AppSettings["CassaEdile"];
                }

                return res;
            }
        }

        public String CodiceFiscale
        {
            get { return Presenter.NormalizzaCampoTesto(TextBoxCF.Text); }
        }

        public String PartitaIva
        {
            get { return TextBoxPIVA.Text; }
        }

        public String TipoInvioDenuncia
        {
            get { return DropDownListTipoInvioDenuncia.SelectedValue; }
        }

        public String EmailConsulente
        {
            get { return TextBoxEmailConsulente.Text; }
        }

        public String FormaGiuridica
        {
            get { return DropDownListFormaGiuridica.SelectedValue; }
        }

        public String CodiceAttivita
        {
            get { return DropDownListCodiceAttivita.SelectedValue; }
        }

        public String NumeroCameraCommercio
        {
            get { return TextBoxNumCameraCommercio.Text; }
        }

        public String NumeroCameraCommercioArtigiani
        {
            get { return TextBoxNumImpreseArtigiane.Text; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utente utente = Membership.GetUser() as Utente;
            if (utente != null)
            {
                RadioButtonConsulente.Enabled = true;
            }

            if (GestioneUtentiBiz.IsConsulente())
            {
                PanelImmissioneDatiConsulente.Visible = false;
            }

            moduloNormale = (Request.QueryString["tipoModulo"] == "normale");

            if (!Page.IsPostBack)
            {
                if (ViewState["MODIFICA"] == null)
                {
                    CaricaCombo();
                }

                if (CassaEdile != "MI00")
                {
                    trAssociazioneImprenditoriale.Visible = false;
                }
            }
        }

        public void CaricaDomanda(ModuloIscrizione domanda)
        {
            ViewState["MODIFICA"] = true;
            CaricaCombo();

            // DATI GENERALI

            TextBoxRagioneSociale.Text = domanda.Impresa.RagioneSociale;
            TextBoxCF.Text = domanda.Impresa.CodiceFiscale;
            TextBoxPIVA.Text = domanda.Impresa.PartitaIVA;
            TextBoxMatricolaInps.Text = domanda.Impresa.CodiceINPS;
            TextBoxMatricolaInail.Text = domanda.Impresa.CodiceINAIL;
            TextBoxControCodiceInail.Text = domanda.Impresa.ControCodiceINAIL;
            if (domanda.DataRichiestaIscrizione.HasValue)
                TextBoxDataRichiestaIscrizione.Text = domanda.DataRichiestaIscrizione.Value.ToShortDateString();
            if (!string.IsNullOrEmpty(domanda.CodiceOrganizzazioneImprenditoriale))
                DropDownListAssociazioneImprenditoriale.SelectedValue = domanda.CodiceOrganizzazioneImprenditoriale;
            else
                DropDownListAssociazioneImprenditoriale.Text = domanda.DescrizioneOrganizzazioneImprenditoriale;
            TextBoxPosAssociazioneImprenditoriale.Text = domanda.PosizioneOrganizzazioneImprenditoriale;
            TextBoxNumCameraCommercio.Text = domanda.NumeroIscrizioneCameraCommercio;
            if (!string.IsNullOrEmpty(domanda.NumeroIscrizioneImpreseArtigiane))
            {
                TextBoxNumImpreseArtigiane.Text = domanda.NumeroIscrizioneImpreseArtigiane;
                AbilitaNumeroImpreseArtigiane(true);
            }
            if (domanda.DataIscrizioneCommercioArtigiane.HasValue)
                TextBoxIscrittaDal.Text = domanda.DataIscrizioneCommercioArtigiane.Value.ToShortDateString();
            if (!string.IsNullOrEmpty(domanda.CodiceTipoImpresa))
                DropDownListTipoImpresa.SelectedValue = domanda.CodiceTipoImpresa;
            else
                DropDownListTipoImpresa.Text = domanda.DescrizioneTipoImpresa;
            if (!string.IsNullOrEmpty(domanda.CodiceNaturaGiuridica))
                DropDownListFormaGiuridica.SelectedValue = domanda.CodiceNaturaGiuridica;
            else
                DropDownListFormaGiuridica.Text = domanda.DescrizioneNaturaGiuridica;
            if (!string.IsNullOrEmpty(domanda.CodiceAttivitaISTAT))
                DropDownListCodiceAttivita.SelectedValue = domanda.CodiceAttivitaISTAT;
            else
                DropDownListCodiceAttivita.Text = domanda.DescrizioneAttivitaISTAT;
            if (!string.IsNullOrEmpty(domanda.CorrispondenzaPresso))
            {
                switch (domanda.CorrispondenzaPresso)
                {
                    case "L":
                        RadioButtonSedeLegale.Checked = true;
                        RadioButtonSedeAmministrativa.Checked = false;
                        RadioButtonConsulente.Checked = false;
                        break;
                    case "A":
                        RadioButtonSedeLegale.Checked = false;
                        RadioButtonSedeAmministrativa.Checked = true;
                        RadioButtonConsulente.Checked = false;
                        break;
                    case "C":
                        RadioButtonSedeLegale.Checked = false;
                        RadioButtonSedeAmministrativa.Checked = false;
                        RadioButtonConsulente.Checked = true;
                        break;
                }
            }
            if (!string.IsNullOrEmpty(domanda.IdTipoInvioDenuncia))
            {
                DropDownListTipoInvioDenuncia.SelectedValue = domanda.IdTipoInvioDenuncia;

                if (domanda.IdTipoInvioDenuncia == "C")
                {
                    TextBoxCodiceConsulente.Text = domanda.CodiceConsulente.ToString();
                    TextBoxRagioneSocialeConsulente.Text = domanda.RagSocConsulente;
                    TextBoxComuneConsulente.Text = domanda.ComuneConsulente;
                    TextBoxCodiceFiscaleConsulente.Text = domanda.CodiceFiscaleConsulente;
                    if (!string.IsNullOrEmpty(domanda.ProvinciaConsulente))
                    {
                        DropDownListProvinciaConsulente.SelectedValue = domanda.ProvinciaConsulente;
                        biz.CaricaComuni(DropDownListComuneConsulente, domanda.ProvinciaConsulente, null);

                        if (!string.IsNullOrEmpty(domanda.CodiceCatastaleConsulente))
                            DropDownListComuneConsulente.SelectedIndex = IscrizioniManager.IndiceComuneInDropDown(
                                DropDownListComuneConsulente, domanda.CodiceCatastaleConsulente);
                    }
                    TextBoxIndirizzoConsulente.Text = domanda.IndirizzoConsulente;
                    TextBoxCAPConsulente.Text = domanda.CapConsulente;
                    TextBoxTelefonoConsulente.Text = domanda.TelefonoConsulente;
                    TextBoxFaxConsulente.Text = domanda.FaxConsulente;
                    TextBoxEmailConsulente.Text = domanda.EmailConsulente;
                    TextBoxPECConsulente.Text = domanda.PecConsulente;
                    AbilitaDatiConsulente(true);
                }
            }
        }

        private void CaricaCombo()
        {
            // Organizzazioni imprenditoriali
            ListDictionary associazioni = bizCommon.GetAssociazioniImprenditoriali();
            DropDownListAssociazioneImprenditoriale.Items.Clear();
            DropDownListAssociazioneImprenditoriale.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListAssociazioneImprenditoriale.DataSource = associazioni;
            DropDownListAssociazioneImprenditoriale.DataTextField = "Value";
            DropDownListAssociazioneImprenditoriale.DataValueField = "Key";
            DropDownListAssociazioneImprenditoriale.DataBind();

            // Province
            StringCollection province = bizCommon.GetProvinceSiceNew();
            DropDownListProvinciaConsulente.Items.Clear();
            DropDownListProvinciaConsulente.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListProvinciaConsulente.DataSource = province;
            DropDownListProvinciaConsulente.DataBind();

            // Natura giuridica
            DropDownListFormaGiuridica.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListFormaGiuridica.DataSource = bizCommon.GetNatureGiuridiche();
            DropDownListFormaGiuridica.DataTextField = "Value";
            DropDownListFormaGiuridica.DataValueField = "Key";
            DropDownListFormaGiuridica.DataBind();
            DropDownListFormaGiuridica.SelectedIndex = 0;

            // Attvità Istat
            DropDownListCodiceAttivita.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListCodiceAttivita.DataSource = bizCommon.GetAttivitaIstat();
            DropDownListCodiceAttivita.DataTextField = "Value";
            DropDownListCodiceAttivita.DataValueField = "Key";
            DropDownListCodiceAttivita.DataBind();
            DropDownListCodiceAttivita.SelectedIndex = 0;

            // Tipi impresa
            CaricaTipiImpresa();

            // Attvità Istat
            DropDownListTipoInvioDenuncia.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListTipoInvioDenuncia.DataSource = bizCommon.GetTipiInvioDenuncia();
            DropDownListTipoInvioDenuncia.DataTextField = "Value";
            DropDownListTipoInvioDenuncia.DataValueField = "Key";
            DropDownListTipoInvioDenuncia.DataBind();
            DropDownListTipoInvioDenuncia.SelectedIndex = 0;
        }

        private void AbilitaNumeroImpreseArtigiane(bool abilita)
        {
            PanelNumImpreseArtigiane.Enabled = abilita;

            if (!abilita)
            {
                TextBoxNumImpreseArtigiane.Text = null;
                TextBoxNumImpreseArtigiane.CssClass = cssCampoDisabilitato;
            }
            else
            {
                TextBoxNumImpreseArtigiane.CssClass = null;
            }
        }

        private void CaricaTipiImpresa()
        {
            DropDownListTipoImpresa.Items.Clear();

            DropDownListTipoImpresa.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListTipoImpresa.DataSource = bizCommon.GetTipiImpresa();
            DropDownListTipoImpresa.DataTextField = "Value";
            DropDownListTipoImpresa.DataValueField = "Key";
            DropDownListTipoImpresa.DataBind();
            DropDownListTipoImpresa.SelectedIndex = 0;
        }

        private void CaricaTipiImpresaSoloArtigianaEIndustria()
        {
            DropDownListTipoImpresa.Items.Clear();

            DropDownListTipoImpresa.Items.Add(new ListItem(string.Empty, string.Empty));
            ListDictionary tipiImpresa = bizCommon.GetTipiImpresa();
            ListDictionary tipiImpresaFiltrato = new ListDictionary();

            foreach (DictionaryEntry coppia in tipiImpresa)
            {
                if (((string)coppia.Key) == "A" || ((string)coppia.Key) == "I")
                    tipiImpresaFiltrato.Add(coppia.Key, coppia.Value);
            }

            DropDownListTipoImpresa.DataSource = tipiImpresaFiltrato;
            DropDownListTipoImpresa.DataTextField = "Value";
            DropDownListTipoImpresa.DataValueField = "Key";
            DropDownListTipoImpresa.DataBind();
        }

        private void CaricaTipiImpresaSoloArtigianaCooperativaEIndustria()
        {
            DropDownListTipoImpresa.Items.Clear();

            DropDownListTipoImpresa.Items.Add(new ListItem(string.Empty, string.Empty));
            ListDictionary tipiImpresa = bizCommon.GetTipiImpresa();
            ListDictionary tipiImpresaFiltrato = new ListDictionary();

            foreach (DictionaryEntry coppia in tipiImpresa)
            {
                if (((string)coppia.Key) == "A" || ((string)coppia.Key) == "I" || ((string)coppia.Key) == "C")
                    tipiImpresaFiltrato.Add(coppia.Key, coppia.Value);
            }

            DropDownListTipoImpresa.DataSource = tipiImpresaFiltrato;
            DropDownListTipoImpresa.DataTextField = "Value";
            DropDownListTipoImpresa.DataValueField = "Key";
            DropDownListTipoImpresa.DataBind();
        }

        private void CaricaTipiImpresaSoloArtigianaIndustriaEInterinale()
        {
            DropDownListTipoImpresa.Items.Clear();

            DropDownListTipoImpresa.Items.Add(new ListItem(string.Empty, string.Empty));
            ListDictionary tipiImpresa = bizCommon.GetTipiImpresa();
            ListDictionary tipiImpresaFiltrato = new ListDictionary();

            foreach (DictionaryEntry coppia in tipiImpresa)
            {
                if (((string)coppia.Key) == "A" || ((string)coppia.Key) == "I" || ((string)coppia.Key) == "T")
                    tipiImpresaFiltrato.Add(coppia.Key, coppia.Value);
            }

            DropDownListTipoImpresa.DataSource = tipiImpresaFiltrato;
            DropDownListTipoImpresa.DataTextField = "Value";
            DropDownListTipoImpresa.DataValueField = "Key";
            DropDownListTipoImpresa.DataBind();
        }

        public void CompletaDomandaConDatiGenerali(ModuloIscrizione modulo)
        {
            // Dati generali
            Impresa impresa = modulo.Impresa;
            modulo.IdCassaEdile = CassaEdile;
            impresa.RagioneSociale = Presenter.NormalizzaCampoTesto(TextBoxRagioneSociale.Text);
            impresa.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCF.Text);
            impresa.PartitaIVA = TextBoxPIVA.Text.ToUpper();
            if (!string.IsNullOrEmpty(TextBoxMatricolaInps.Text))
                impresa.CodiceINPS = Presenter.NormalizzaCampoTesto(TextBoxMatricolaInps.Text);
            if (!string.IsNullOrEmpty(TextBoxMatricolaInail.Text))
                impresa.CodiceINAIL = Presenter.NormalizzaCampoTesto(TextBoxMatricolaInail.Text);
            if (!string.IsNullOrEmpty(TextBoxControCodiceInail.Text))
                impresa.ControCodiceINAIL = Presenter.NormalizzaCampoTesto(TextBoxControCodiceInail.Text);
            if (!string.IsNullOrEmpty(TextBoxDataRichiestaIscrizione.Text))
                modulo.DataRichiestaIscrizione = DateTime.Parse(TextBoxDataRichiestaIscrizione.Text.Replace('.', '/'));
            if (!string.IsNullOrEmpty(DropDownListAssociazioneImprenditoriale.SelectedValue))
            {
                modulo.CodiceOrganizzazioneImprenditoriale = DropDownListAssociazioneImprenditoriale.SelectedValue;
                modulo.DescrizioneOrganizzazioneImprenditoriale = null;
            }
            else
            {
                modulo.CodiceOrganizzazioneImprenditoriale = null;
                modulo.DescrizioneOrganizzazioneImprenditoriale = DropDownListAssociazioneImprenditoriale.Text.ToUpper();
            }
            modulo.PosizioneOrganizzazioneImprenditoriale =
                Presenter.NormalizzaCampoTesto(TextBoxPosAssociazioneImprenditoriale.Text);
            modulo.NumeroIscrizioneCameraCommercio = Presenter.NormalizzaCampoTesto(TextBoxNumCameraCommercio.Text);
            modulo.NumeroIscrizioneImpreseArtigiane = Presenter.NormalizzaCampoTesto(TextBoxNumImpreseArtigiane.Text);
            if (!string.IsNullOrEmpty(TextBoxIscrittaDal.Text))
                modulo.DataIscrizioneCommercioArtigiane = DateTime.Parse(TextBoxIscrittaDal.Text.Replace('.', '/'));
            if (!string.IsNullOrEmpty(DropDownListTipoImpresa.SelectedValue))
            {
                modulo.CodiceTipoImpresa = DropDownListTipoImpresa.SelectedValue;
                modulo.DescrizioneTipoImpresa = null;
            }
            else
            {
                modulo.CodiceTipoImpresa = null;
                modulo.DescrizioneTipoImpresa = DropDownListTipoImpresa.Text.ToUpper();
            }
            if (!string.IsNullOrEmpty(DropDownListFormaGiuridica.SelectedValue))
            {
                modulo.CodiceNaturaGiuridica = DropDownListFormaGiuridica.SelectedValue;
                modulo.DescrizioneNaturaGiuridica = null;
            }
            else
            {
                modulo.CodiceNaturaGiuridica = null;
                modulo.DescrizioneNaturaGiuridica = DropDownListFormaGiuridica.Text.ToUpper();
            }
            if (!string.IsNullOrEmpty(DropDownListCodiceAttivita.SelectedValue))
            {
                modulo.CodiceAttivitaISTAT = DropDownListCodiceAttivita.SelectedValue;
                modulo.DescrizioneAttivitaISTAT = null;
            }
            else
            {
                modulo.CodiceAttivitaISTAT = null;
                modulo.DescrizioneAttivitaISTAT = DropDownListCodiceAttivita.Text.ToUpper();
            }
            if (RadioButtonSedeLegale.Checked)
                modulo.CorrispondenzaPresso = "L";
            else if (RadioButtonSedeAmministrativa.Checked)
                modulo.CorrispondenzaPresso = "A";
            else if (RadioButtonConsulente.Checked)
                modulo.CorrispondenzaPresso = "C";

            if (!string.IsNullOrEmpty(DropDownListTipoInvioDenuncia.SelectedValue))
            {
                modulo.IdTipoInvioDenuncia = DropDownListTipoInvioDenuncia.SelectedValue;
                modulo.DescrizioneTipoInvioDenuncia = null;

                if (modulo.IdTipoInvioDenuncia == "C")
                {
                    if (!string.IsNullOrEmpty(TextBoxCodiceConsulente.Text))
                        modulo.CodiceConsulente = Int32.Parse(TextBoxCodiceConsulente.Text);
                    modulo.RagSocConsulente = Presenter.NormalizzaCampoTesto(TextBoxRagioneSocialeConsulente.Text);
                    modulo.ComuneConsulente = Presenter.NormalizzaCampoTesto(TextBoxComuneConsulente.Text);
                    modulo.CodiceFiscaleConsulente = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscaleConsulente.Text);
                    modulo.ProvinciaConsulente = DropDownListProvinciaConsulente.SelectedValue;
                    modulo.CodiceCatastaleConsulente = DropDownListComuneConsulente.SelectedValue;
                    modulo.CapConsulente = TextBoxCAPConsulente.Text;
                    modulo.IndirizzoConsulente = Presenter.NormalizzaCampoTesto(TextBoxIndirizzoConsulente.Text);
                    modulo.TelefonoConsulente = TextBoxTelefonoConsulente.Text;
                    modulo.FaxConsulente = TextBoxFaxConsulente.Text;
                    modulo.EmailConsulente = TextBoxEmailConsulente.Text;
                    modulo.PecConsulente = TextBoxPECConsulente.Text;
                }
            }
            else
            {
                modulo.IdTipoInvioDenuncia = null;
                modulo.DescrizioneTipoInvioDenuncia = null;
            }
        }

        /// <summary>
        ///   Metodo per visualizzare l'intera stringa se ci si sofferma con il mouse sulla descrizione
        /// </summary>
        /// <param name = "sender"></param>
        /// <param name = "e"></param>
        protected void DropDownListCodiceAttivita_DataBound(object sender, EventArgs e)
        {
            for (Int32 i = 0; i < DropDownListCodiceAttivita.Items.Count; i++)
            {
                DropDownListCodiceAttivita.Items[i].Attributes.Add("Title", DropDownListCodiceAttivita.Items[i].Text);
            }
        }

        #region Custom Validators

        protected void CustomValidatorPartitaIVA_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!string.IsNullOrEmpty(TextBoxPIVA.Text.Trim()) && !PartitaIvaManager.VerificaPartitaIva(TextBoxPIVA.Text))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorSmallDateTime_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (bizCommon.IsCampoDataSmallDateTime(TextBoxDataRichiestaIscrizione.Text))
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        protected void CustomValidatorPosizioneOrganizzazioneImprenditoriale_ServerValidate(object source,
                                                                                            ServerValidateEventArgs args)
        {
            if (!string.IsNullOrEmpty(DropDownListAssociazioneImprenditoriale.SelectedValue)
                && DropDownListAssociazioneImprenditoriale.SelectedValue != "ANCE"
                && string.IsNullOrEmpty(TextBoxPosAssociazioneImprenditoriale.Text))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorIscrittaDal_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (bizCommon.IsCampoDataSmallDateTimeMinoreDiOggi(TextBoxIscrittaDal.Text))
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        protected void CustomValidatorInvioDenuncia_ServerValidate(object source, ServerValidateEventArgs args)
        {
            Utente utente = Membership.GetUser() as Utente;

            if (utente == null)
            {
                if (DropDownListTipoInvioDenuncia.SelectedValue == "C" &&
                    (string.IsNullOrEmpty(TextBoxRagioneSocialeConsulente.Text.Trim())
                     || string.IsNullOrEmpty(DropDownListComuneConsulente.SelectedValue)
                     || string.IsNullOrEmpty(TextBoxCAPConsulente.Text)
                     || string.IsNullOrEmpty(TextBoxCodiceFiscaleConsulente.Text.Trim())
                     || string.IsNullOrEmpty(TextBoxPECConsulente.Text.Trim()))
                    )
                    args.IsValid = false;
                else
                    args.IsValid = true;
            }
            else
                args.IsValid = true;
        }

        protected void CustomValidatorTipoImpresa_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Controllo che se il tipo di impresa è Artigiana allora sia fornito un numero di iscrizione all'albo delle imprese artigiane
            if (DropDownListTipoImpresa.SelectedValue == "A" && string.IsNullOrEmpty(TextBoxNumImpreseArtigiane.Text))
            {
                args.IsValid = false;
            }
        }

        #endregion

        #region Funzione per caricare dei dati di prova

        public void CaricaDatiProva()
        {
            // DATI GENERALI
            TextBoxRagioneSociale.Text = "Prova ins";
            TextBoxCF.Text = "MRULSS81L25D969A";
            TextBoxPIVA.Text = "80938030154";
            TextBoxMatricolaInps.Text = "5646452";
            TextBoxMatricolaInail.Text = "8897426";
            TextBoxControCodiceInail.Text = "313115";
            TextBoxDataRichiestaIscrizione.Text = "19/09/2007";
            DropDownListAssociazioneImprenditoriale.SelectedIndex = 1;
            TextBoxPosAssociazioneImprenditoriale.Text = "1234";
            TextBoxNumCameraCommercio.Text = "1254";
            //TextBoxNumImpreseArtigiane.Text = "54698";
            TextBoxIscrittaDal.Text = "10/01/2016";
            DropDownListTipoImpresa.SelectedIndex = 2;
            DropDownListFormaGiuridica.SelectedIndex = 1;
            DropDownListCodiceAttivita.SelectedIndex = 1;
        }

        #endregion

        #region Selezioni DropDown

        protected void DropDownListAssociazioneImprenditoriale_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(DropDownListAssociazioneImprenditoriale.SelectedValue))
                TextBoxPosAssociazioneImprenditoriale.Text = null;
        }

        protected void DropDownListTipoImpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListTipoImpresa.SelectedValue == "A")
                AbilitaNumeroImpreseArtigiane(true);
            else
                AbilitaNumeroImpreseArtigiane(false);
        }

        protected void DropDownListFormaGiuridica_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownListTipoImpresa.Enabled = false;

            if (!string.IsNullOrEmpty(DropDownListFormaGiuridica.SelectedValue))
            {
                DropDownListTipoImpresa.Enabled = true;

                if (!moduloNormale)
                {
                    if (DropDownListFormaGiuridica.SelectedValue == "01" ||
                        DropDownListFormaGiuridica.SelectedValue == "02" ||
                        DropDownListFormaGiuridica.SelectedValue == "03" ||
                        DropDownListFormaGiuridica.SelectedValue == "06")
                    {
                        CaricaTipiImpresaSoloArtigianaEIndustria();
                    }
                    else
                    {
                        CaricaTipiImpresaSoloArtigianaCooperativaEIndustria();
                    }
                }
                else
                {
                    if (DropDownListFormaGiuridica.SelectedValue == "01" ||
                    DropDownListFormaGiuridica.SelectedValue == "02" ||
                    DropDownListFormaGiuridica.SelectedValue == "03")
                    {
                        CaricaTipiImpresaSoloArtigianaEIndustria();
                    }
                    else
                        if (DropDownListFormaGiuridica.SelectedValue == "06")
                    {
                        CaricaTipiImpresaSoloArtigianaIndustriaEInterinale();
                    }
                    else
                    {
                        if (DropDownListFormaGiuridica.SelectedValue == "07"
                            || DropDownListFormaGiuridica.SelectedValue == "08"
                            || DropDownListFormaGiuridica.SelectedValue == "09")
                        {
                            CaricaTipiImpresaSoloArtigianaCooperativaEIndustria();
                        }
                        else
                        {
                            CaricaTipiImpresa();
                        }
                    }
                }
            }
            else
                CaricaTipiImpresa();
        }

        protected void DropDownListTipoInvioDenuncia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListTipoInvioDenuncia.SelectedValue == "C")
            {
                AbilitaDatiConsulente(true);
            }
            else
            {
                ResetDatiConsulente();
                AbilitaDatiConsulente(false);
            }
        }

        protected void DropDownListProvinciaConsulente_SelectedIndexChanged(object sender, EventArgs e)
        {
            biz.CaricaComuni(DropDownListComuneConsulente, DropDownListProvinciaConsulente.SelectedValue,
                             null);
        }

        protected void DropDownListComuneConsulente_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxCAPConsulente.Text = string.Empty;
            biz.CaricaCap(DropDownListComuneConsulente.SelectedValue, TextBoxCAPConsulente);
        }

        #endregion

        #region Metodi per gestione dati aggiuntivi consulente

        private void AbilitaDatiConsulente(bool abilita)
        {
            if (PanelImmissioneDatiConsulente.Enabled)
            {
                PanelDatiConsulenteImpresa.Enabled = abilita;
                if (!abilita)
                {
                    TextBoxCodiceConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxRagioneSocialeConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxComuneConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxCodiceFiscaleConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxIndirizzoConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxCAPConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxTelefonoConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxFaxConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxEmailConsulente.CssClass = cssCampoDisabilitato;
                    TextBoxPECConsulente.CssClass = cssCampoDisabilitato;

                    TextBoxCodiceConsulente.Text = null;
                    TextBoxRagioneSocialeConsulente.Text = null;
                    TextBoxComuneConsulente.Text = null;
                    TextBoxCodiceFiscaleConsulente.Text = null;
                    TextBoxIndirizzoConsulente.Text = null;
                    DropDownListProvinciaConsulente.SelectedValue = string.Empty;
                    biz.CaricaComuni(DropDownListComuneConsulente, DropDownListProvinciaConsulente.SelectedValue, null);
                    DropDownListComuneConsulente.SelectedValue = string.Empty;
                    TextBoxCAPConsulente.Text = null;
                    TextBoxTelefonoConsulente.Text = null;
                    TextBoxFaxConsulente.Text = null;
                    TextBoxEmailConsulente.Text = null;
                    TextBoxPECConsulente.Text = null;
                }
                else
                {
                    TextBoxCodiceConsulente.CssClass = null;
                    TextBoxRagioneSocialeConsulente.CssClass = null;
                    TextBoxComuneConsulente.CssClass = null;
                    TextBoxCodiceFiscaleConsulente.CssClass = null;
                    TextBoxIndirizzoConsulente.CssClass = null;
                    TextBoxCAPConsulente.CssClass = null;
                    TextBoxTelefonoConsulente.CssClass = null;
                    TextBoxFaxConsulente.CssClass = null;
                    TextBoxEmailConsulente.CssClass = null;
                    TextBoxPECConsulente.CssClass = null;
                }
            }
            else
            {
                TextBoxCodiceConsulente.CssClass = cssCampoDisabilitato;
                TextBoxRagioneSocialeConsulente.CssClass = cssCampoDisabilitato;
                TextBoxComuneConsulente.CssClass = cssCampoDisabilitato;
                TextBoxCodiceFiscaleConsulente.CssClass = cssCampoDisabilitato;
                TextBoxIndirizzoConsulente.CssClass = cssCampoDisabilitato;
                TextBoxCAPConsulente.CssClass = cssCampoDisabilitato;
                TextBoxTelefonoConsulente.CssClass = cssCampoDisabilitato;
                TextBoxFaxConsulente.CssClass = cssCampoDisabilitato;
                TextBoxEmailConsulente.CssClass = cssCampoDisabilitato;
                TextBoxPECConsulente.CssClass = cssCampoDisabilitato;
                PanelDatiConsulenteImpresa.Enabled = false;
            }

            Utente utente = Membership.GetUser() as Utente;
            if (utente == null)
            {
                if (abilita)
                {
                    RadioButtonConsulente.Enabled = true;
                }
                else
                {
                    if (RadioButtonConsulente.Checked)
                    {
                        RadioButtonSedeLegale.Checked = true;
                        RadioButtonConsulente.Checked = false;
                    }
                    RadioButtonConsulente.Enabled = false;
                }
            }
        }

        private void ResetDatiConsulente()
        {
            TextBoxCodiceConsulente.Text = null;
            TextBoxRagioneSocialeConsulente.Text = null;
        }

        public void NascondiDatiConsulente(bool nascondi)
        {
            PanelImmissioneDatiConsulente.Enabled = !nascondi;
        }

        #endregion

        protected void TextBoxNumCameraCommercio_TextChanged(object sender, EventArgs e)
        {
            if (TextBoxNumCameraCommercio.Text.Length > 0)
            {
                TextBoxNumImpreseArtigiane.Text = null;
                TextBoxNumImpreseArtigiane.DataBind();
            }
        }

        protected void TextBoxNumImpreseArtigiane_TextChanged(object sender, EventArgs e)
        {
            if (TextBoxNumImpreseArtigiane.Text.Length > 0)
            {
                TextBoxNumCameraCommercio.Text = null;
                TextBoxNumCameraCommercio.DataBind();
            }
        }
    }
}