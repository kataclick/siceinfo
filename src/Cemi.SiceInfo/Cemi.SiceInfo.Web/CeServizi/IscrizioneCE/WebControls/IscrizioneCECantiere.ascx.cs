﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TBridge.Cemi.IscrizioneCE.Business;
using System.Collections.Specialized;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Entities.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls
{
    public partial class IscrizioneCECantiere : System.Web.UI.UserControl
    {
        private readonly IscrizioniManager biz = new IscrizioniManager();
        private readonly Common bizCommon = new Common();

        public String Indirizzo
        {
            get
            {
                return Presenter.NormalizzaCampoTesto(TextBoxCantiereIndirizzo.Text);
            }
        }

        public String Provincia
        {
            get
            {
                return DropDownListCantiereProvincia.Text;
            }
        }

        public String Comune
        {
            get
            {
                return DropDownListCantiereComune.Text;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (ViewState["MODIFICA"] == null)
                {
                    CaricaCombo();
                }
            }
        }

        private void CaricaCombo()
        {
            // PreIndirizzi
            ListDictionary preIndirizzi = bizCommon.GetPreIndirizzi();

            DropDownListCantierePreIndirizzo.Items.Clear();
            DropDownListCantierePreIndirizzo.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListCantierePreIndirizzo.DataSource = preIndirizzi;
            DropDownListCantierePreIndirizzo.DataTextField = "Value";
            DropDownListCantierePreIndirizzo.DataValueField = "Key";
            DropDownListCantierePreIndirizzo.DataBind();

            // Province
            StringCollection province = bizCommon.GetProvinceSiceNew();

            DropDownListCantiereProvincia.Items.Clear();
            DropDownListCantiereProvincia.Items.Add(new ListItem(string.Empty, string.Empty));
            DropDownListCantiereProvincia.DataSource = province;
            DropDownListCantiereProvincia.DataBind();
        }

        public void CaricaDomanda(ModuloIscrizione domanda)
        {
            ViewState["MODIFICA"] = true;
            CaricaCombo();

            // CANTIERE
            ViewState["IdCantiere"] = domanda.Cantiere.IdCantiere.Value;
            if (!string.IsNullOrEmpty(domanda.Cantiere.Provincia) &&
                DropDownListCantiereProvincia.Items.FindByValue(domanda.Cantiere.Provincia) != null)
            {
                DropDownListCantiereProvincia.SelectedValue = domanda.Cantiere.Provincia;
                biz.CaricaComuni(DropDownListCantiereComune, domanda.Cantiere.Provincia, null);

                if (!string.IsNullOrEmpty(domanda.Cantiere.Comune))
                {
                    DropDownListCantiereComune.SelectedIndex =
                        IscrizioniManager.IndiceComuneInDropDown(DropDownListCantiereComune, domanda.Cantiere.Comune);
                    biz.CaricaFrazioni(DropDownListCantiereFrazione, domanda.Cantiere.Comune);

                    if (!string.IsNullOrEmpty(domanda.Cantiere.Frazione))
                        DropDownListCantiereFrazione.SelectedIndex =
                            IscrizioniManager.IndiceFrazioneInDropDown(DropDownListCantiereFrazione, domanda.Cantiere.Frazione);
                }
            }

            if (!string.IsNullOrEmpty(domanda.Cantiere.PreIndirizzo) &&
                DropDownListCantierePreIndirizzo.Items.FindByValue(domanda.Cantiere.PreIndirizzo) != null)
                DropDownListCantierePreIndirizzo.SelectedValue = domanda.Cantiere.PreIndirizzo;
            else
                DropDownListCantierePreIndirizzo.Text = domanda.Cantiere.PreIndirizzo;
            TextBoxCantiereIndirizzo.Text = domanda.Cantiere.Indirizzo;
            TextBoxCantiereCap.Text = domanda.Cantiere.Cap;
            TextBoxLavoratoriOccupati.Text = domanda.NumeroOpe.ToString();
            TextBoxCantiereCommittente.Text = domanda.Cantiere.CommittenteTrovato;
        }

        public void CompletaDomandaConCantiere(ModuloIscrizione modulo)
        {
            Cantiere cantiere = modulo.Cantiere;
            if (ViewState["IdCantiere"] != null)
            {
                cantiere.IdCantiere = (Int32)ViewState["IdCantiere"];
            }
            cantiere.PreIndirizzo = DropDownListCantierePreIndirizzo.SelectedValue;
            cantiere.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxCantiereIndirizzo.Text);
            cantiere.Comune = biz.GetCodiceCatastaleDaCombo(DropDownListCantiereComune.SelectedValue);
            cantiere.Frazione = biz.GetFrazioneDaCombo(DropDownListCantiereFrazione.SelectedValue);
            cantiere.Provincia = DropDownListCantiereProvincia.SelectedValue;
            cantiere.Cap = Presenter.NormalizzaCampoTesto(TextBoxCantiereCap.Text);
            cantiere.CommittenteTrovato = Presenter.NormalizzaCampoTesto(TextBoxCantiereCommittente.Text);
            if (!string.IsNullOrEmpty(TextBoxLavoratoriOccupati.Text))
                modulo.NumeroOpe = Int32.Parse(TextBoxLavoratoriOccupati.Text);
        }

        public void CaricaDatiProva()
        {
            // CANTIERE
            DropDownListCantiereProvincia.SelectedIndex = 1;
            DropDownListCantiereComune.SelectedIndex = 1;
            DropDownListCantierePreIndirizzo.SelectedIndex = 1;
            TextBoxCantiereIndirizzo.Text = "cantiere 43";
            TextBoxCantiereCap.Text = "16100";
            TextBoxLavoratoriOccupati.Text = "10";
            TextBoxCantiereCommittente.Text = "Committente Gino";
        }

        #region Eventi DropDown
        protected void DropDownListCantiereProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxCantiereCap.Text = string.Empty;
            biz.CaricaComuni(DropDownListCantiereComune, DropDownListCantiereProvincia.SelectedValue,
                         DropDownListCantiereFrazione);
        }

        protected void DropDownListCantiereComune_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxCantiereCap.Text = string.Empty;
            biz.CaricaCapOFrazioni(DropDownListCantiereComune, DropDownListCantiereFrazione, TextBoxCantiereCap);
        }

        protected void DropDownListCantiereFrazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxCantiereCap.Text = string.Empty;
            biz.CaricaCap(DropDownListCantiereFrazione.SelectedValue, TextBoxCantiereCap);
        }
        #endregion

        #region Custom Validators
        protected void CustomValidatorCAP_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator customValidatorCAP = (CustomValidator)source;
            TextBox textBoxCAP = (TextBox)this.FindControl(customValidatorCAP.ControlToValidate);
            int iCAP;

            if (textBoxCAP.Text.Length == 5 && Int32.TryParse(textBoxCAP.Text, out iCAP))
            {
                string cap = textBoxCAP.Text;

                string codiceComboComune = null;
                switch (textBoxCAP.ID)
                {
                    case "TextBoxCantiereCap":
                        codiceComboComune = DropDownListCantiereComune.SelectedValue;
                        break;
                }
                string[] codiceSplittato = codiceComboComune.Split('|');

                if (biz.IsGrandeCitta(codiceSplittato[0]) && cap[2] == '1' && cap[3] == '0' && cap[4] == '0')
                    args.IsValid = false;
            }
        }

        protected void CustomValidatorCantiereLavoratoriOccupati_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Controllo che se valorizzato, il numero dei lavoratori occupati sia > 0
            int lavOcc;
            if (!string.IsNullOrEmpty(TextBoxLavoratoriOccupati.Text) &&
                Int32.TryParse(TextBoxLavoratoriOccupati.Text, out lavOcc) && lavOcc < 1)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        #endregion
    }
}