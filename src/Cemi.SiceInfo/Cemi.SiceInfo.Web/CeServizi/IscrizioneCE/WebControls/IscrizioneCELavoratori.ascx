﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneCELavoratori.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls.IscrizioneCELavoratori" %>

<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelLavoratoriUnico" runat="server" Text="Lavoratori in forza" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Indicare il numero di operai / impiegati per i quali si richiede l’iscrizione presso Cassa Edile di Milano, Lodi, Monza e Brianza.
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            Numero operai occupati all’atto dell’iscrizione:
        </td>
        <td>
            <asp:TextBox ID="TextBoxLavoratoriOccupatiOperai" runat="server" Width="300px" MaxLength="10" />
        </td>
        <td>
            <!-- <% /*
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorLavoratoriOccupatiOperai"
                runat="server" ErrorMessage="Numero Operai mancante" ControlToValidate="TextBoxLavoratoriOccupatiOperai"
                ValidationGroup="cantiere">*</asp:RequiredFieldValidator>
                 */    %>
            -->
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorLavoratoriOccupatiOperai"
                runat="server" ControlToValidate="TextBoxLavoratoriOccupatiOperai" ErrorMessage="Numero Operai non corretto"
                ValidationExpression="^\d{0,4}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <!-- <% /*
                <asp:CustomValidator ID="CustomValidatorCantiereNumeroOperai" runat="server" ControlToValidate="TextBoxLavoratoriOccupatiOperai"
                ErrorMessage="Il numero degli operai deve essere positivo" OnServerValidate="CustomValidatorCantiereNumeroOperai_ServerValidate"
                ValidationGroup="stop">*</asp:CustomValidator>
                */ %>
            -->
        </td>
    </tr>
    <tr>
        <td>
            Numero impiegati occupati all’atto dell’iscrizione:
        </td>
        <td>
            <asp:TextBox ID="TextBoxLavoratoriOccupatiPrevedi" runat="server" Width="300px" MaxLength="10" />
        </td>
        <td>            
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorLavoratoriOccupatiPrevedi"
                runat="server" ControlToValidate="TextBoxLavoratoriOccupatiPrevedi" ErrorMessage="Numero impiegati non corretto"
                ValidationExpression="^\d{0,4}$" ValidationGroup="stop">*</asp:RegularExpressionValidator>
            <!-- <% /*
                <asp:CustomValidator ID="CustomValidatorCantiereNumeroImpiegati" runat="server" ControlToValidate="TextBoxLavoratoriOccupatiPrevedi"
                ErrorMessage="Il numero degli impiegati deve essere positivo" OnServerValidate="CustomValidatorCantiereNumeroImpiegati_ServerValidate"
                ValidationGroup="stop">*</asp:CustomValidator>
                */ %>
            -->
        </td>
    </tr>
    
</table>
