﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IscrizioneCEContoCorrente.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls.IscrizioneCEContoCorrente" %>

<table class="standardTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelBanca" runat="server" Font-Bold="True" Text="Conto corrente"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            IBAN
        </td>
        <td>
            <asp:TextBox ID="TextBoxBancaIBAN" runat="server" MaxLength="34" TabIndex="20" Width="350px"></asp:TextBox>
        </td>
        <td>
            <asp:CustomValidator ID="CustomValidatorBancaIBAN" runat="server" ControlToValidate="TextBoxBancaIBAN"
                ErrorMessage="Codice IBAN non corretto" ValidationGroup="stop" OnServerValidate="CustomValidatorBancaIBAN_ServerValidate">*</asp:CustomValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBancaIBAN" runat="server" ControlToValidate="TextBoxBancaIBAN"
                ErrorMessage="Codice IBAN mancante" ValidationGroup="banca">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Intestato a<b>*</b>:
        </td>
        <td>
            <asp:TextBox ID="TextBoxBancaIntestatario" runat="server" Width="350px" MaxLength="60"
                TabIndex="29"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBancaIntestatario" runat="server"
                ControlToValidate="TextBoxBancaIntestatario" ErrorMessage="Intestatario CC mancante"
                ValidationGroup="banca">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <b>*</b>
            Inserire cognome e nome della persona fisica o giuridica intestataria del conto corrente
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            Esempio IBAN italiano
            <table class="standardTable">
                <tr>
                    <td class="tdIban">
                        ITPPCAAAAABBBBBNNNNNNNNNNNN
                    </td>
                </tr>
                <tr>
                    <td class="tdIban">
                        <b>IT73J0615513000000000012345 </b>
                    </td>
                </tr>
            </table>
            <table class="standardTable">
                <tr>
                    <td align="left">
                        IT
                    </td>
                    <td>
                        Codice paese (IT)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        PP
                    </td>
                    <td>
                        Cifra di controllo (73)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        C
                    </td>
                    <td>
                        CIN (J)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        AAAAA
                    </td>
                    <td>
                        ABI (06155)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        BBBBB
                    </td>
                    <td>
                        CAB (13000)
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        NNNNNNNNNNNN
                    </td>
                    <td>
                        CONTO (000000012345)
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
