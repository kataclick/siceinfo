﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Utility;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Entities.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.WebControls
{
    public partial class IscrizioneCEContoCorrente : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDomanda(ModuloIscrizione domanda)
        {
            TextBoxBancaIBAN.Text = domanda.IBAN;
            TextBoxBancaIntestatario.Text = domanda.IntestatarioConto;
        }

        public void CompletaDomandaConContoCorrente(ModuloIscrizione modulo)
        {
            // Conto corrente
            modulo.IBAN = Presenter.NormalizzaCampoTesto(TextBoxBancaIBAN.Text);
            modulo.IntestatarioConto = Presenter.NormalizzaCampoTesto(TextBoxBancaIntestatario.Text);
        }

        public void CaricaDatiProva()
        {
            TextBoxBancaIBAN.Text = "IT59F0529673970CC0000033409";
            TextBoxBancaIntestatario.Text = "pippo stock";
        }

        #region Custom Validators

        protected void CustomValidatorBancaIBAN_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string IBAN = Presenter.NormalizzaCampoTesto(TextBoxBancaIBAN.Text);

            bool ibanCorretto = false;

            try
            {
                ibanCorretto = IbanManager.VerificaCodiceIban(IBAN);
            }
            catch
            {
            }

            // Controllo che il codice IBAN sia corretto
            if (!string.IsNullOrEmpty(IBAN) && (!ibanCorretto || IBAN.ToUpper() == "IT73J0615513000000000012345"))
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        #endregion
    }
}