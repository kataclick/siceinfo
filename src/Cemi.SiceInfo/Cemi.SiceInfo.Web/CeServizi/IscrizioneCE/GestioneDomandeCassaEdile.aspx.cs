﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.IscrizioneCE.Business;
using TBridge.Cemi.IscrizioneCE.Type.Filters;
using TBridge.Cemi.Type.Entities.IscrizioneCe;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Enums.IscrizioneCe;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE
{
    public partial class GestioneDomandeCassaEdile : System.Web.UI.Page
    {
        private const int INDICEACCETTA = 10;
        private const int INDICEMODIFICA = 9;
        private const int INDICERIFIUTA = 11;
        private readonly IscrizioniManager biz = new IscrizioniManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEGestioneDomande);

            //Modifichiamo la larghezza dei div per fare in modo che la pagina venga visualizzata correttamente. ai dive sono stati aggiunti 220px
            //((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("maindiv")).Attributes.Add("style", "width: 1040px;");
            //((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("outerdiv")).Attributes.Add("style", "width: 1300px;");

            if (!Page.IsPostBack)
            {
                if (Session["IscrizionePagina"] != null && Session["IscrizioneFiltro"] != null)
                {
                    TipoOrdinamento? ordinamento = null;

                    if (Session["IscrizioneOrdinamento"] != null)
                        ordinamento = (TipoOrdinamento)Session["IscrizioneOrdinamento"];

                    GridViewDomande.PageIndex = (int)Session["IscrizionePagina"];
                    CaricaFiltro((ModuloIscrizioneFilter)Session["IscrizioneFiltro"]);
                    CaricaOrdinamento(ordinamento);
                    CaricaDomande((ModuloIscrizioneFilter)Session["IscrizioneFiltro"], ordinamento);
                }

                // Se sto tornando dall'accettazione o da un rifiuto di una domanda verifico se 
                // devo caricare i filtri impostati precedentemente
                if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "indietro")
                {
                    if (Request.QueryString["filtroStato"] != null)
                    {
                        DropDownListRicercaStato.SelectedValue = Request.QueryString["filtroStato"];
                    }

                    if (Request.QueryString["filtroRagSoc"] != null)
                    {
                        TextBoxRicercaRagioneSociale.Text = Request.QueryString["filtroRagSoc"];
                    }

                    if (Request.QueryString["filtroDal"] != null)
                    {
                        TextBoxDal.Text = Request.QueryString["filtroDal"];
                    }

                    if (Request.QueryString["filtroAl"] != null)
                    {
                        TextBoxAl.Text = Request.QueryString["filtroAl"];
                    }

                    CaricaDomande(null, null);
                }
            }
        }

        private void CaricaOrdinamento(TipoOrdinamento? ordinamento)
        {
            if (ordinamento.HasValue)
            {
                RadioButtonDataInserimentoAsc.Checked = false;
                RadioButtonDataInserimentoDesc.Checked = false;
                RadioButtonDataRichiestaIscrizioneAsc.Checked = false;
                RadioButtonDataRichiestaIscrizioneDesc.Checked = false;
                RadioButtonRagioneSocialeAsc.Checked = false;
                RadioButtonRagioneSocialeDesc.Checked = false;

                switch (ordinamento.Value)
                {
                    case TipoOrdinamento.DataInserimentoCrescente:
                        RadioButtonDataInserimentoAsc.Checked = true;
                        break;
                    case TipoOrdinamento.DataInserimentoDecrescente:
                        RadioButtonDataInserimentoDesc.Checked = true;
                        break;
                    case TipoOrdinamento.DataRichiestaIscrizioneCrescente:
                        RadioButtonDataRichiestaIscrizioneAsc.Checked = true;
                        break;
                    case TipoOrdinamento.DataRichiestaIscrizioneDecrescente:
                        RadioButtonDataRichiestaIscrizioneDesc.Checked = true;
                        break;
                    case TipoOrdinamento.RagioneSocialeCrescente:
                        RadioButtonRagioneSocialeAsc.Checked = true;
                        break;
                    case TipoOrdinamento.RagioneSocialeDecrescente:
                        RadioButtonRagioneSocialeDesc.Checked = true;
                        break;
                }
            }
        }

        private void CaricaFiltro(ModuloIscrizioneFilter filtro)
        {
            if (filtro.StatoDomanda.HasValue)
                DropDownListRicercaStato.SelectedValue = filtro.StatoDomanda.ToString();

            TextBoxRicercaRagioneSociale.Text = filtro.RagioneSociale;

            if (filtro.Dal.HasValue)
                TextBoxDal.Text = filtro.Dal.Value.ToShortDateString();
            if (filtro.Al.HasValue)
                TextBoxAl.Text = filtro.Al.Value.ToShortDateString();

            if (filtro.DataRichiestaDal.HasValue)
                TextBoxDataRichiestaDal.Text = filtro.DataRichiestaDal.Value.ToShortDateString();
            if (filtro.DataRichiestaAl.HasValue)
                TextBoxDataRichiestaAl.Text = filtro.DataRichiestaAl.Value.ToShortDateString();
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GridViewDomande.PageIndex = 0;
                Session["IscrizionePagina"] = 0;
                LabelTitolo.Visible = true;
                SvuotaProblemi();
                CaricaDomande(null, null);
            }
        }

        protected void SvuotaProblemi()
        {
            BulletedListProblemiBloccanti.Items.Clear();
            BulletedProblemiNonBloccanti.Items.Clear();
            LabelDomanda.Text = string.Empty;
            PanelDettagli.Visible = false;
            LabelErrore.Text = string.Empty;
        }

        protected void CaricaDomande(ModuloIscrizioneFilter filtroP, TipoOrdinamento? ordinamento)
        {
            ModuloIscrizioneFilter filtro = null;
            TipoOrdinamento? tipoOrd = null;

            if (filtro == null)
            {
                filtro = CreaFiltro();
            }
            else
            {
                filtro = filtroP;
            }

            if (tipoOrd == null)
            {
                tipoOrd = CreaOrdinamento();
            }
            else
            {
                tipoOrd = ordinamento;
            }

            GridViewDomande.DataSource = biz.GetDomande(filtro, tipoOrd);
            GridViewDomande.DataBind();
        }

        private TipoOrdinamento CreaOrdinamento()
        {
            TipoOrdinamento tipoOrdinamento = TipoOrdinamento.DataInserimentoCrescente;

            if (RadioButtonDataInserimentoAsc.Checked)
                tipoOrdinamento = TipoOrdinamento.DataInserimentoCrescente;
            if (RadioButtonDataInserimentoDesc.Checked)
                tipoOrdinamento = TipoOrdinamento.DataInserimentoDecrescente;
            if (RadioButtonDataRichiestaIscrizioneAsc.Checked)
                tipoOrdinamento = TipoOrdinamento.DataRichiestaIscrizioneCrescente;
            if (RadioButtonDataRichiestaIscrizioneDesc.Checked)
                tipoOrdinamento = TipoOrdinamento.DataRichiestaIscrizioneDecrescente;
            if (RadioButtonRagioneSocialeAsc.Checked)
                tipoOrdinamento = TipoOrdinamento.RagioneSocialeCrescente;
            if (RadioButtonRagioneSocialeDesc.Checked)
                tipoOrdinamento = TipoOrdinamento.RagioneSocialeDecrescente;

            Session["IscrizioneOrdinamento"] = tipoOrdinamento;

            return tipoOrdinamento;
        }

        private ModuloIscrizioneFilter CreaFiltro()
        {
            ModuloIscrizioneFilter filtro = new ModuloIscrizioneFilter();

            switch (DropDownListRicercaStato.SelectedValue)
            {
                case "ACCETTATE":
                    filtro.StatoDomanda = StatoDomanda.Accettata;
                    break;
                case "DAVALUTARE":
                    filtro.StatoDomanda = StatoDomanda.DaValutare;
                    break;
                case "RESPINTE":
                    filtro.StatoDomanda = StatoDomanda.Rifiutata;
                    break;
                case "ATTESADOCUMENTAZIONE":
                    filtro.StatoDomanda = StatoDomanda.SospesaAttesaIntegrazione;
                    break;
                case "SITUAZIONEDEBITORIA":
                    filtro.StatoDomanda = StatoDomanda.SospesaDebiti;
                    break;
            }

            if (!string.IsNullOrEmpty(TextBoxRicercaRagioneSociale.Text))
            {
                filtro.RagioneSociale = TextBoxRicercaRagioneSociale.Text;
            }

            if (!string.IsNullOrEmpty(TextBoxDal.Text))
            {
                filtro.Dal = DateTime.Parse(TextBoxDal.Text);
            }

            if (!string.IsNullOrEmpty(TextBoxAl.Text))
            {
                filtro.Al = DateTime.Parse(TextBoxAl.Text);
            }

            if (!string.IsNullOrEmpty(TextBoxDataRichiestaDal.Text))
            {
                filtro.DataRichiestaDal = DateTime.Parse(TextBoxDataRichiestaDal.Text);
            }

            if (!string.IsNullOrEmpty(TextBoxDataRichiestaAl.Text))
            {
                filtro.DataRichiestaAl = DateTime.Parse(TextBoxDataRichiestaAl.Text);
            }

            filtro.Confermate = true;

            Session["IscrizioneFiltro"] = filtro;

            return filtro;
        }

        protected void GridViewDomande_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                ModuloIscrizione modulo = (ModuloIscrizione)e.Row.DataItem;
                Label lStato = (Label)e.Row.FindControl("LabelStato");
                Panel pColonnaProblemi = (Panel)e.Row.FindControl("PanelColonnaProblemi");
                Panel pStatiIntermedi = (Panel)e.Row.FindControl("PanelStatiIntermedi");
                Button bInAttesaDocumentazione = (Button)e.Row.FindControl("ButtonAttesaDocumentazione");
                Button bSituazioneDebitoria = (Button)e.Row.FindControl("ButtonSituazioneDebitoria");
                Button bModulo = (Button)e.Row.FindControl("ButtonModulo");

                bModulo.CommandArgument = e.Row.RowIndex.ToString();
                bInAttesaDocumentazione.CommandArgument = e.Row.RowIndex.ToString();
                bSituazioneDebitoria.CommandArgument = e.Row.RowIndex.ToString();

                if (modulo.Stato.HasValue)
                {
                    switch (modulo.Stato)
                    {
                        case StatoDomanda.Accettata:
                            lStato.Text = "Accettata";
                            DisabilitaModifiche(pColonnaProblemi, e.Row);
                            break;
                        case StatoDomanda.Rifiutata:
                            lStato.Text = "Rifiutata";
                            DisabilitaModifiche(pColonnaProblemi, e.Row);
                            break;
                        case StatoDomanda.DaValutare:
                        case StatoDomanda.SospesaAttesaIntegrazione:
                        case StatoDomanda.SospesaDebiti:
                            pStatiIntermedi.Enabled = true;
                            lStato.Text = modulo.Stato.ToString();

                            // Verifica dei problemi presenti nella domanda
                            StringCollection problemiBloc = new StringCollection();
                            StringCollection problemiNonBloc = new StringCollection();
                            bool blocco = biz.ControllaDomanda(modulo.IdDomanda.Value, problemiBloc, problemiNonBloc);
                            if (blocco)
                            {
                                e.Row.Cells[INDICEACCETTA].Enabled = false;
                            }

                            if (problemiBloc.Count == 0 && problemiNonBloc.Count == 0)
                            {
                                pColonnaProblemi.Visible = false;
                            }

                            break;
                    }
                }
            }
        }

        protected static void DisabilitaModifiche(Panel panelColonnaProblemi, GridViewRow row)
        {
            panelColonnaProblemi.Visible = false;
            row.Cells[INDICEMODIFICA].Enabled = false;
            row.Cells[INDICEACCETTA].Enabled = false;
            row.Cells[INDICERIFIUTA].Enabled = false;
        }

        protected void GridViewDomande_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int? idDomanda = (int?)GridViewDomande.DataKeys[e.NewSelectedIndex].Values["IdDomanda"];
            TipoModulo tipoModulo = (TipoModulo)GridViewDomande.DataKeys[e.NewSelectedIndex].Values["TipoModulo"];

            if (idDomanda.HasValue)
            {
                Session["IdDomanda"] = idDomanda.Value;

                StringBuilder urlRedir = new StringBuilder();
                urlRedir.Append("~/CeServizi/IscrizioneCE/IscrizioneCE.aspx?op=cemi&modalita=modifica&tipoModulo=");
                if (tipoModulo == TipoModulo.Operai)
                {
                    urlRedir.Append("cantiere");
                }
                else if (tipoModulo == TipoModulo.Impiegati)
                {
                    urlRedir.Append("prevedi");
                }
                else
                {
                    urlRedir.Append("unico");
                }

                Response.Redirect(urlRedir.ToString());
            }
        }

        protected void GridViewDomande_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "accetta" || e.CommandName == "rifiuta"
                || e.CommandName == "attesaDocumentazione" || e.CommandName == "situazioneDebitoria")
            {
                LabelErrore.Text = string.Empty;
                int indice;
                int idDomanda;

                indice = Int32.Parse(e.CommandArgument.ToString());
                idDomanda = ((int?)GridViewDomande.DataKeys[indice].Values["IdDomanda"]).Value;

                StringBuilder urlPaginaCambioStato = new StringBuilder();
                urlPaginaCambioStato.Append(
                    String.Format("~/CeServizi/IscrizioneCE/ConfermaCambioStatoDomanda.aspx?idDomanda={0}", idDomanda));

                // Se utilizzati memorizzo i filtri nella query string
                if (!string.IsNullOrEmpty(DropDownListRicercaStato.SelectedValue))
                {
                    urlPaginaCambioStato.Append(String.Format("&filtroStato={0}", DropDownListRicercaStato.SelectedValue));
                }

                if (!string.IsNullOrEmpty(TextBoxRicercaRagioneSociale.Text))
                {
                    urlPaginaCambioStato.Append(String.Format("&filtroRagSoc={0}", TextBoxRicercaRagioneSociale.Text));
                }

                if (!string.IsNullOrEmpty(TextBoxDal.Text))
                {
                    urlPaginaCambioStato.Append(String.Format("&filtroDal={0}", TextBoxDal.Text));
                }

                if (!string.IsNullOrEmpty(TextBoxAl.Text))
                {
                    urlPaginaCambioStato.Append(String.Format("&filtroAl={0}", TextBoxAl.Text));
                }

                switch (e.CommandName)
                {
                    case "rifiuta":
                        urlPaginaCambioStato.Append(String.Format("&modalita=rifiuta"));
                        Response.Redirect(urlPaginaCambioStato.ToString());
                        break;
                    case "accetta":
                        urlPaginaCambioStato.Append(String.Format("&modalita=accetta"));
                        Response.Redirect(urlPaginaCambioStato.ToString());
                        break;
                    case "attesaDocumentazione":
                        urlPaginaCambioStato.Append(String.Format("&modalita=attesaDocumentazione"));
                        Response.Redirect(urlPaginaCambioStato.ToString());
                        break;
                    case "situazioneDebitoria":
                        urlPaginaCambioStato.Append(String.Format("&modalita=situazioneDebitoria"));
                        Response.Redirect(urlPaginaCambioStato.ToString());
                        break;
                }
            }

            if (e.CommandName == "Modulo")
            {
                int indice;
                int idDomanda;

                indice = Int32.Parse(e.CommandArgument.ToString());
                idDomanda = ((int?)GridViewDomande.DataKeys[indice].Values["IdDomanda"]).Value;
                ModuloIscrizione modulo = biz.GetDomanda(idDomanda);
                Session["IdDomanda"] = idDomanda;

                Response.Redirect(String.Format("~/CeServizi/IscrizioneCE/ReportIscrizioneCE.aspx?tipoModulo={0}", modulo.TipoModulo.ToString().ToLower()));
            }
        }

        protected void GridViewDomande_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Page.Validate("ricerca");

            if (Page.IsValid)
            {
                GridViewDomande.PageIndex = e.NewPageIndex;
                Session["IscrizionePagina"] = e.NewPageIndex;
                CaricaDomande(null, null);
            }
        }

        protected void CustomValidatorDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (!string.IsNullOrEmpty(TextBoxDal.Text) && !string.IsNullOrEmpty(TextBoxAl.Text))
            {
                DateTime dal = DateTime.Parse(TextBoxDal.Text);
                DateTime al = DateTime.Parse(TextBoxAl.Text);

                if (dal > al)
                {
                    args.IsValid = false;
                }
            }
        }

        protected void GridViewDomande_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            PanelDettagli.Visible = true;
            BulletedListProblemiBloccanti.Items.Clear();
            BulletedProblemiNonBloccanti.Items.Clear();

            int idDomanda = (int)GridViewDomande.DataKeys[e.RowIndex].Value;
            StringCollection listaErroriBloc = new StringCollection();
            StringCollection listaErroriNonBloc = new StringCollection();

            biz.ControllaDomanda(idDomanda, listaErroriBloc, listaErroriNonBloc);
            LabelDomanda.Text = (string)GridViewDomande.DataKeys[e.RowIndex].Values["RagioneSociale"];

            foreach (string errore in listaErroriBloc)
            {
                BulletedListProblemiBloccanti.Items.Add(errore);
            }

            foreach (string errore in listaErroriNonBloc)
            {
                BulletedProblemiNonBloccanti.Items.Add(errore);
            }

            if (BulletedListProblemiBloccanti.Items.Count == 0)
            {
                BulletedListProblemiBloccanti.Items.Add("Nessuno");
            }

            if (BulletedProblemiNonBloccanti.Items.Count == 0)
            {
                BulletedProblemiNonBloccanti.Items.Add("Nessuno");
            }
        }
    }
}