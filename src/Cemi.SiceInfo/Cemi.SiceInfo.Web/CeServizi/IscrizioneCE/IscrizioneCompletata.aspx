﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IscrizioneCompletata.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.IscrizioneCompletata" %>

<%@ Register Src="../WebControls/MenuIscrizioneCE.ascx" TagName="IscrizioneCEMenu" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Stampa modulo"
        titolo="Iscrizione imprese" />
    <br />
    La procedura di richiesta di iscrizione è stata <b>completata correttamente</b>.
    E' possibile stampare direttamente la domanda cliccando su "<b>Stampa</b>".
    <br />
    <br />
    In aggiunta, la domanda verrà inviata all'indirizzo e-mail specificato.
    <br />
    <br />
    <asp:Button 
        ID="ButtonStampa" 
        runat="server" 
        OnClick="ButtonStampa_Click" 
        Text="Stampa"
        Width="150px" />
    <br />
</asp:Content>