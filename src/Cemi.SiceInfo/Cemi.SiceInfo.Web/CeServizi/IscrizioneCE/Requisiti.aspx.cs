﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE
{
    public partial class Requisiti : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.IscrizioneCEIscrizione);
        }

        protected void ButtonProsegui_Click(object sender, EventArgs e)
        {
            string tipoModulo;
            /*
            if (RadioButtonModuloNormale.Checked)
                tipoModulo = "normale";
            else
                tipoModulo = "prevedi";
            */

            tipoModulo = "unico";

            Response.Redirect(String.Format("~/CeServizi/IscrizioneCE/IscrizioneCE.aspx?tipoModulo={0}", tipoModulo));
        }
    }
}