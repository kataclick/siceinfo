﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IscrizioneCEDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IscrizioneCE.IscrizioneCEDefault" %>

<%@ Register Src="../WebControls/MenuIscrizioneCE.ascx" TagName="IscrizioneCEMenu"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Iscrizione imprese"
        sottoTitolo="Iscrizione" />
    <br />
    <p class="DefaultPage">
        In questa sezione è possibile effettuare l'iscrizione presso Cassa Edile di Milano.
        E' prevista la compilazione obbligatoria di tutti i campi, senza la quale non potrà
        essere portata a termine con successo la procedura.
    </p>
    <p class="DefaultPage">
        Per le imprese non è richiesta alcuna registrazione al sito internet e possono,
        quindi, procedere direttamente alla compilazione della domanda di iscrizione cliccando
        sulla voce "Iscrizione" riportata nel menù verticale alla sinistra del video.
    </p>
    <p class="DefaultPage">
        <b>L'operazione può essere eseguita anche da un consulente del lavoro che deve essersi
            preventivamente registrato in quest'area del sito.
            <br />
            Inserendo username e password nella finestra "Login", l'utente potrà
            procedere alla compilazione della domanda di iscrizione in nome e per conto dell'impresa
            richiedente. </b>
    </p>
    <p class="DefaultPage">
        <b><span style="color: red">I DATI INSERITI NELLA DOMANDA DI ISCRIZIONE TELEMATICA NON
            CONFERMATA ANDRANNO IRRIMEDIABILMENTE PERSI.</span></b>
    </p>
    <p class="DefaultPage">
        <b>Una volta confermata la domanda si ricorda che la stessa verrà considerata completata
            al ricevimento dell’originale firmato dal legale rappresentante dell’impresa richiedente.</b>
    </p>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:IscrizioneCEMenu ID="IscrizioneCEMenu1" runat="server" />
</asp:Content>

