﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cemi.SiceInfo.Web.CeServizi.IscrizioneCE
{
    public partial class IscrizioneCompletata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ButtonStampa_Click(object sender, EventArgs e)
        {
            string tipoModulo = Request.QueryString["tipoModulo"];
            Server.Transfer(String.Format("~/CeServizi/IscrizioneCE/ReportIscrizioneCE.aspx?tipoModulo={0}", tipoModulo));
        }
    }
}