﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriConfermaCancellazioneAttivita : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita
                = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            //controlliamo se è una risposta
            if (!string.IsNullOrEmpty(Request.QueryString["mod"]))
            {
                //
                switch (Request.QueryString["mod"])
                {
                    case "cancellato":
                        LabelTesto.Text = "La cancellazione è andata a buon fine.";
                        ButtonAnnulla.Text = "Indietro";
                        ButtonConfermaEliminazioneAttivita.Visible = false;
                        break;

                    case "errore":
                        LabelTesto.Text = "La cancellazione non è andata a buon fine.";
                        ButtonAnnulla.Text = "Indietro";
                        ButtonConfermaEliminazioneAttivita.Visible = false;
                        break;
                }
            }
        }

        protected void ButtonConfermaEliminazioneAttivita_Click(object sender, EventArgs e)
        {
            int idAttivita;

            if (int.TryParse(Request.QueryString["idAttivita"], out idAttivita))
            {
                biz.DeleteIspezione(idAttivita);
                biz.DeleteAttivita(idAttivita);

                Response.Redirect("~/CeServizi/Cantieri/CantieriConfermaCancellazioneAttivita.aspx?mod=cancellato&o=" +
                                  Request.QueryString["o"] + "&idIspettore=" + Request.QueryString["idIspettore"] + "&dal=" +
                                  Request.QueryString["dal"] + "&modalita=" + Request.QueryString["modalita"]);
            }

            Response.Redirect("~/CeServizi/Cantieri/CantieriConfermaCancellazioneAttivita.aspx?mod=errore&o=" +
                              Request.QueryString["o"] + "&idIspettore=" + Request.QueryString["idIspettore"] + "&dal=" +
                              Request.QueryString["dal"] + "&modalita=" + Request.QueryString["modalita"]);
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            //controlliamo se è una risposta
            if (!string.IsNullOrEmpty(Request.QueryString["o"]))
            {
                //
                switch (Request.QueryString["o"])
                {
                    case "prog":
                        Response.Redirect("~/CeServizi/Cantieri/CantieriProgrammazioneSettimanale.aspx?idIspettore=" +
                                          Request.QueryString["idIspettore"] + "&dal=" + Request.QueryString["dal"] +
                                          "&modalita=" + Request.QueryString["modalita"]);
                        break;

                    case "precon":
                        Response.Redirect("~/CeServizi/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?idIspettore=" +
                                          Request.QueryString["idIspettore"] + "&dal=" + Request.QueryString["dal"] +
                                          "&modalita=" + Request.QueryString["modalita"]);
                        break;
                }
            }
        }
    }
}