﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
//using TBridge.Cemi.GestioneUtenti.Type.Entities;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriInserimentoModificaZona : System.Web.UI.Page
    {
        private readonly Common _commonBiz = new Common();
        private readonly CantieriBusiness biz = new CantieriBusiness();

        private bool aggiornamento;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);

            #region Impostiamo gli eventi JS per la gestione dei click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();

            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append(
                "if (Page_ClientValidate('generale') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonOperazione, null) + ";");
            sb.Append("return true;");
            ButtonOperazione.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonOperazione);

            #endregion

            if (!Page.IsPostBack)
            {
                string modalita = Request.QueryString["modalita"];

                if (modalita == null || modalita != "modifica")
                {
                    ModalitaInserimento();
                    ViewState["listaCap"] = new List<CAP>();
                    GridViewCap.DataSource = ViewState["listaCap"];
                    GridViewCap.DataBind();
                }
                else
                {
                    string idZonaS = Request.QueryString["idZona"];
                    int idZona;

                    if (Int32.TryParse(idZonaS, out idZona))
                    {
                        ModalitaAggiornamento();
                        CaricaZona(idZona);
                    }
                    else
                    {
                        ModalitaInserimento();
                        ViewState["listaCap"] = new List<CAP>();
                    }

                    GridViewCap.DataSource = ViewState["listaCap"];
                    GridViewCap.DataBind();
                }

                CaricaProvince();

                //DropDownListProvincia.SelectedItem.Text = "MI";
                //DropDownListProvincia.DataBind();
                // Porcheria
                DropDownListProvincia.SelectedValue = "13";

                int idProvincia;
                if ((DropDownListProvincia.SelectedValue != null) &&
                    (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                    CaricaComuni(idProvincia);

                //DropDownListComuni.SelectedItem.Text = "MILANO";
                //DropDownListComuni.DataBind();
                // Porcheria
                DropDownListComuni.SelectedValue = "1772";

                Int64 idComune;
                if ((DropDownListComuni.SelectedValue != null) &&
                    (Int64.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                    CaricaCAP(idComune);

                ViewState["aggiornamento"] = aggiornamento;
            }
        }

        private void CaricaZona(int idZona)
        {
            ZonaCollection listaZone = biz.GetZone(idZona);
            if (listaZone.Count == 1)
            {
                Zona zona = listaZone[0];

                TextBoxIdZona.Text = zona.IdZona.ToString();
                TextBoxNome.Text = zona.Nome;
                TextBoxDescrizione.Text = zona.Descrizione;

                ViewState["listaCap"] = zona.ListaCap;
            }
        }

        private void ModalitaAggiornamento()
        {
            TitoloSottotitolo1.sottoTitolo = "Modifica zona";
            ButtonOperazione.Text = "Aggiorna zona";
            aggiornamento = true;
        }

        private void ModalitaInserimento()
        {
            TitoloSottotitolo1.sottoTitolo = "Inserimento nuova zona";
            ButtonOperazione.Text = "Crea zona";
            aggiornamento = false;
            PanelDatiAggiornamento.Visible = false;
        }

        protected void ButtonAggiungiCap_Click(object sender, EventArgs e)
        {
            AggiungiAListaCap();
        }

        private void AggiungiAListaCap()
        {
            if (ViewState["listaCap"] != null)
            {
                List<CAP> listaCap = (List<CAP>)ViewState["listaCap"];

                CAP cap;

                //if (!string.IsNullOrEmpty(TextBoxCap.Text))
                //{   
                //    if (Regex.IsMatch(TextBoxCap.Text, @"^\d{5}$"))
                //        cap = TextBoxCap.Text;
                //}
                //else
                cap = new CAP(DropDownListCAP.SelectedValue);
                cap.ListaComuni.Add(
                    new CAP(DropDownListCAP.SelectedValue, DropDownListComuni.SelectedItem.Text,
                            DropDownListProvincia.SelectedItem.Text));

                if (cap != null)
                {
                    if (!listaCap.Contains(cap))
                        listaCap.Add(cap);
                }

                ViewState["listaCap"] = listaCap;
                CaricaListaCap();
            }
        }

        protected void ButtonAggiungiCapManuale_Click(object sender, EventArgs e)
        {
            if (ViewState["listaCap"] != null)
            {
                List<CAP> listaCap = (List<CAP>)ViewState["listaCap"];

                CAP cap = null;

                if (!string.IsNullOrEmpty(TextBoxCap.Text))
                {
                    if (Regex.IsMatch(TextBoxCap.Text, @"^\d{5}$"))
                    {
                        cap = new CAP(TextBoxCap.Text);
                        cap.ListaComuni.Add(new CAP(DropDownListCAP.SelectedValue, null, null));
                    }
                }
                //else
                //    cap = DropDownListCAP.SelectedValue;

                if (cap != null)
                {
                    if (!listaCap.Contains(cap))
                        listaCap.Add(cap);
                }

                ViewState["listaCap"] = listaCap;
                CaricaListaCap();
            }
        }

        private void CaricaListaCap()
        {
            GridViewCap.DataSource = ViewState["listaCap"];
            GridViewCap.DataBind();
        }

        protected void ButtonOperazione_Click(object sender, EventArgs e)
        {
            aggiornamento = (bool)ViewState["aggiornamento"];

            if (ControlloCampiServer())
            {
                Zona zona = CreaZona();
                bool res = false;
                bool doppia = false;

                // Eseguo l'operazione
                if (aggiornamento)
                {
                    int idZona;
                    Int32.TryParse(TextBoxIdZona.Text, out idZona);
                    zona.IdZona = idZona;

                    // Aggiorno il record
                    res = biz.UpdateZona(zona);
                    CaricaZona(zona.IdZona.Value);
                    CaricaListaCap();
                }
                else
                {
                    // Inserisco il record
                    try
                    {
                        biz.InsertZona(zona);
                        if (zona.IdZona.HasValue && zona.IdZona > 0)
                        {
                            res = true;
                            SvuotaCampi();
                        }
                    }
                    catch (ArgumentException aExc)
                    {
                        doppia = true;
                    }
                }

                // Gestisco il risultato dell'operazione
                if (res)
                {
                    // Tutto ok
                    if (aggiornamento)
                        LabelRisultato.Text = "Aggiornamento effettuato correttamente";
                    else
                        LabelRisultato.Text = "Inserimento effettuato correttamente";
                }
                else
                {
                    if (doppia)
                        LabelRisultato.Text = "Il nome della zona è già presente";
                    else
                        LabelRisultato.Text = "Si è verificato un errore durante l'operazione";
                }
            }
        }

        private void SvuotaCampi()
        {
            TextBoxNome.Text = string.Empty;
            TextBoxDescrizione.Text = string.Empty;

            ViewState["listaCap"] = new List<CAP>();
            GridViewCap.DataSource = ViewState["listaCap"];
            GridViewCap.DataBind();
        }

        private Zona CreaZona()
        {
            // Creazione oggetto Zona
            string nome;
            string descrizione = null;
            List<CAP> listaCap;

            nome = TextBoxNome.Text;
            if (!string.IsNullOrEmpty(TextBoxDescrizione.Text))
                descrizione = TextBoxDescrizione.Text;

            if (ViewState["listaCap"] != null)
                listaCap = (List<CAP>)ViewState["listaCap"];
            else
                listaCap = new List<CAP>();

            Zona zona = new Zona(null, nome, descrizione, listaCap);

            return zona;
        }

        /// <summary>
        ///   Effettua il controllo dei campi lato server
        /// </summary>
        /// <returns></returns>
        private bool ControlloCampiServer()
        {
            // CAMPI CONTROLLATI
            // Nome

            bool res = true;
            StringBuilder errori = new StringBuilder();

            if (string.IsNullOrEmpty(TextBoxNome.Text))
            {
                res = false;
                errori.Append("Campo nome vuoto" + Environment.NewLine);
            }

            if (!res)
                LabelRisultato.Text = errori.ToString();
            return res;
        }

        protected void GridViewCap_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            CaricaListaCap();

            GridViewCap.PageIndex = e.NewSelectedIndex;
            GridViewCap.DataBind();
        }

        protected void GridViewCap_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (ViewState["listaCap"] != null)
            {
                List<CAP> listaCap = (List<CAP>)ViewState["listaCap"];
                int indiceDaCancellare = e.RowIndex + (GridViewCap.PageIndex * GridViewCap.PageCount);
                listaCap.RemoveAt(indiceDaCancellare);

                ViewState["listaCap"] = listaCap;
                GridViewCap.DataSource = listaCap;
                GridViewCap.DataBind();
            }
        }

        private void CaricaProvince()
        {
            DataTable dtProvince = _commonBiz.GetProvince();

            DropDownListProvincia.DataSource = dtProvince;
            DropDownListProvincia.DataTextField = "sigla";
            DropDownListProvincia.DataValueField = "idProvincia";

            DropDownListProvincia.DataBind();
        }

        private void CaricaComuni(int idProvincia)
        {
            DataTable dtComuni = _commonBiz.GetComuniDellaProvincia(idProvincia);

            DropDownListComuni.DataSource = dtComuni;
            DropDownListComuni.DataTextField = "denominazione";
            DropDownListComuni.DataValueField = "idComune";

            DropDownListComuni.DataBind();
        }

        private void CaricaCAP(Int64 idComune)
        {
            DataTable dtCAP = _commonBiz.GetCAPDelComune(idComune);

            DropDownListCAP.DataSource = dtCAP;
            DropDownListCAP.DataTextField = "cap";
            DropDownListCAP.DataValueField = "cap";

            DropDownListCAP.DataBind();
        }

        protected void DropDownListProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idProvincia;

            if ((DropDownListProvincia.SelectedValue != null) &&
                (Int32.TryParse(DropDownListProvincia.SelectedValue, out idProvincia)))
                CaricaComuni(idProvincia);

            int idComune;

            if ((DropDownListComuni.SelectedValue != null) &&
                (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                CaricaCAP(idComune);
        }

        protected void DropDownListComuni_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idComune;

            if ((DropDownListComuni.SelectedValue != null) &&
                (Int32.TryParse(DropDownListComuni.SelectedValue, out idComune)))
                CaricaCAP(idComune);
        }

        protected void GridViewCap_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CAP cap = (CAP)e.Row.DataItem;
                GridView gvComuni = (GridView)e.Row.FindControl("GridViewComuni");

                gvComuni.DataSource = cap.ListaComuni;
                gvComuni.DataBind();
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneZone.aspx");
        }
    }
}