﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Filters.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class EstrazioneLodi : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        private const String carattereSeparatore = "|";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonEstrazione_Click(object sender, EventArgs e)
        {
            IspezioniFilter filtro = new IspezioniFilter();
            filtro.Stato = StatoIspezione.Conclusa;
            filtro.Provincia = "LO";
            RapportoIspezioneCollection listaIspezioni =
                biz.GetIspezioni(filtro);

            GeneraFile(listaIspezioni);
        }

        private void GeneraFile(RapportoIspezioneCollection listaIspezioni)
        {
            StringBuilder sbFile = new StringBuilder();

            foreach (RapportoIspezione ispezione in listaIspezioni)
            {
                sbFile.AppendLine(
                    String.Format("120{0}{1}{0}{0}{2}{0}{0}{0}{8}{0}{9}{0}{0}{0}{0}{0}{4}{0}{5}{0}{6}{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}{7}",
                        carattereSeparatore,
                        ispezione.IdIspezione,
                        ispezione.Giorno.ToString("ddMMyyyy"),
                        Presenter.NormalizzaCampoTesto(Presenter.StampaStringaExcel(String.Format("{0} {1}, {2} {3} {4}", ispezione.Cantiere.Indirizzo, ispezione.Cantiere.Civico, ispezione.Cantiere.Cap, ispezione.Cantiere.Comune, ispezione.Cantiere.Provincia))),
                        Presenter.NormalizzaCampoTesto(Presenter.StampaStringaExcel(ispezione.Cantiere.Provincia)) == "LODI" ? "LO" : Presenter.NormalizzaCampoTesto(Presenter.StampaStringaExcel(ispezione.Cantiere.Provincia)),
                        Presenter.NormalizzaCampoTesto(Presenter.StampaStringaExcel(ispezione.Cantiere.Comune)),
                        Presenter.NormalizzaCampoTesto(Presenter.StampaStringaExcel(String.Format("{0} {1}", ispezione.Cantiere.Indirizzo, ispezione.Cantiere.Civico))),
                        ispezione.ProtocolloNotificaRegionale,
                        ispezione.Cantiere.ImpresaAppaltatrice != null ? !String.IsNullOrWhiteSpace(ispezione.Cantiere.ImpresaAppaltatrice.CodiceFiscale) ? ispezione.Cantiere.ImpresaAppaltatrice.CodiceFiscale : !String.IsNullOrWhiteSpace(ispezione.Cantiere.ImpresaAppaltatrice.PartitaIva) ? ispezione.Cantiere.ImpresaAppaltatrice.PartitaIva : String.Empty : String.Empty,
                        ispezione.Cantiere.ImpresaAppaltatrice != null ? Presenter.NormalizzaCampoTesto(Presenter.StampaStringaExcel(ispezione.Cantiere.ImpresaAppaltatrice.RagioneSociale)) : String.Empty)
                    );
            }

            Response.AddHeader("content-disposition",
                                   String.Format("attachment; filename=cassaedile_{0}.txt", DateTime.Now.ToString("yyyyMMdd")));
            Response.ContentType = "text/plain";
            Response.Write(sbFile.ToString());
            Response.Flush();
            Response.End();
        }
    }
}