﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriInserisciModificaProgrammazioneAttivitaGiornaliera : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneGestione);
            funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            //ButtonIndietro.Attributes.Add("onclick", "history.back(); return false;");

            #region Impostiamo gli eventi JS per la gestione dei click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciModifica, null) + ";");
            sb.Append("return true;");
            ButtonInserisciModifica.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciModifica);

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb2.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb2.Append("if (Page_ClientValidate() == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb2.Append("this.value = 'Attendere...';");
            sb2.Append("this.disabled = true;");
            sb2.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciContinua, null) + ";");
            sb2.Append("return true;");
            ButtonInserisciContinua.Attributes.Add("onclick", sb2.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciContinua);

            #endregion

            CantieriRicercaCantiere1.OnCantiereSelected += CantieriRicercaCantiere1_OnCantiereSelected;
            CantieriRicercaCantiere1.SoloSelezione();

            if (!Page.IsPostBack)
            {
                // Carico dati attivita
                CaricaTipologieAttivita();

                if (Request.QueryString["idIspettore"] != null)
                {
                    int idIspettore = Int32.Parse(Request.QueryString["idIspettore"]);
                    Ispettore ispettore = biz.GetIspettori(idIspettore, null)[0];
                    ViewState["ispettore"] = ispettore;

                    // Carico dati giorno e ispettore
                    TextBoxIspettore.Text = ispettore.NomeCompleto;
                    TextBoxZona.Text = ispettore.Zona.Nome;
                }

                if (Request.QueryString["giorno"] != null)
                {
                    DateTime giorno = DateTime.Parse(Request.QueryString["giorno"]);
                    ViewState["giorno"] = giorno;
                    TextBoxGiorno.Text = giorno.ToShortDateString();
                }

                // Se arrivo alla pagina con un nomeAttivita lo seleziono
                if (Request.QueryString["nomeAttivita"] != null)
                {
                    //DropDownListAttivita.Enabled = false;
                    DropDownListAttivita.DataBind();
                    DropDownListAttivita.SelectedValue = Request.QueryString["nomeAttivita"];
                }

                // Sono arrivato nella pagina dalla programmazione settimanale
                // Il RUI inserisce solo le visite ai cantieri
                if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "prog")
                {
                    DropDownListAttivita.SelectedValue = "Cantieri";
                    DropDownListAttivita.Enabled = false;
                    DropDownListAttivita.DataBind();
                }
            }
        }

        private void CantieriRicercaCantiere1_OnCantiereSelected(Cantiere cantiere)
        {
            TextBoxCantiere.Text = cantiere.Indirizzo + " " + cantiere.Civico + Environment.NewLine;
            TextBoxCantiere.Text += cantiere.Comune + Environment.NewLine;
            TextBoxCantiere.Text += cantiere.Provincia + Environment.NewLine;

            ViewState["cantiere"] = cantiere;

            CantieriRicercaCantiere1.Visible = false;
            LabelRicercaCantieri.Visible = false;
        }

        private void CaricaTipologieAttivita()
        {
            TipologiaAttivitaCollection listaAttivita = biz.GetTipologieAttivita();
            DropDownListAttivita.DataSource = listaAttivita;
            DropDownListAttivita.DataTextField = "Nome";
            DropDownListAttivita.DataValueField = "Nome";
            DropDownListAttivita.DataBind();
        }

        protected void ButtonInserisciModifica_Click(object sender, EventArgs e)
        {
            if (ControlloCampiServer())
            {
                // Inserimento della nuova attivita
                DateTime giorno = (DateTime)ViewState["giorno"];
                Ispettore ispettore = (Ispettore)ViewState["ispettore"];
                string nomeAttivita = DropDownListAttivita.SelectedItem.Text;
                string descrizione = null;
                Cantiere cantiere = null;
                bool programmato = true;
                bool preventivato = false;
                bool consuntivato = false;

                if (!string.IsNullOrEmpty(TextBoxDecrizione.Text))
                    descrizione = TextBoxDecrizione.Text;

                if (ViewState["cantiere"] != null)
                    cantiere = (Cantiere)ViewState["cantiere"];

                if (Request.QueryString["modalita"] != null)
                {
                    switch (Request.QueryString["modalita"])
                    {
                        case "prev":
                            programmato = false;
                            preventivato = true;
                            break;
                        case "cons":
                            programmato = false;
                            consuntivato = true;
                            break;
                    }
                }

                Attivita nuovaAttivita =
                    new Attivita(null, giorno, DateTime.Now, nomeAttivita, descrizione, ispettore, cantiere, programmato,
                                 preventivato, consuntivato);

                // Se tutto a posto redirect su pagina della programmazione
                if (biz.InsertAttivita(nuovaAttivita))
                {
                    string pagina = "CantieriProgrammazioneSettimanale.aspx?modalita=programma";

                    if (Request.QueryString["modalita"] != null)
                    {
                        switch (Request.QueryString["modalita"])
                        {
                            case "prev":
                                pagina = "~/CeServizi/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=preventivo";
                                break;
                            case "cons":
                                pagina = "~/CeServizi/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=consuntivo";
                                break;
                        }
                    }

                    if (Request.QueryString["idIspettore"] != null && Request.QueryString["dal"] != null)
                        Response.Redirect(
                            string.Format("{0}&idIspettore={1}&dal={2}", pagina, ispettore.IdIspettore,
                                          Request.QueryString["dal"]));
                    else
                        Response.Redirect(pagina);
                }
                else
                    LabelRisultato.Text = "Errore durante l'inserimento";
            }
        }

        /// <summary>
        /// Effettua il controllo dei campi lato server
        /// </summary>
        /// <returns></returns>
        private bool ControlloCampiServer()
        {
            // CAMPI CONTROLLATI

            bool res = true;
            StringBuilder errori = new StringBuilder();

            if (string.IsNullOrEmpty(DropDownListAttivita.SelectedItem.Text))
            {
                res = false;
                errori.Append("Selezionare una tipologia di attività" + Environment.NewLine);
            }
            else if (DropDownListAttivita.SelectedItem.Text == "Cantieri" && string.IsNullOrEmpty(TextBoxCantiere.Text))
            {
                res = false;
                errori.Append("Selezionare un cantiere" + Environment.NewLine);
            }

            if (ViewState["ispettore"] == null || ViewState["giorno"] == null)
            {
                res = false;
                errori.Append("Mancano riferimenti necessari" + Environment.NewLine);
            }

            if (string.IsNullOrEmpty(TextBoxDecrizione.Text) && string.IsNullOrEmpty(TextBoxCantiere.Text))
            {
                res = false;
                errori.Append("E' necessario selezionare un cantiere o immettere una descrizione" + Environment.NewLine);
            }

            if (!res)
                LabelRisultato.Text = errori.ToString();
            return res;
        }

        protected void ButtonSelezionaCantiere_Click(object sender, EventArgs e)
        {
            if (ViewState["ispettore"] != null)
            {
                Ispettore ispettore = (Ispettore)ViewState["ispettore"];
                CantieriRicercaCantiere1.ispettore = ispettore;
                CantieriRicercaCantiere1.Carica();
            }

            LabelRicercaCantieri.Visible = true;
            CantieriRicercaCantiere1.Visible = true;
        }

        protected void ButtonInserisciContinua_Click(object sender, EventArgs e)
        {
            if (ControlloCampiServer())
            {
                // Inserimento della nuova attivita
                DateTime giorno = (DateTime)ViewState["giorno"];
                Ispettore ispettore = (Ispettore)ViewState["ispettore"];
                string nomeAttivita = DropDownListAttivita.SelectedItem.Text;
                string descrizione = null;
                Cantiere cantiere = null;
                bool programmato = true;
                bool preventivato = false;
                bool consuntivato = false;

                if (!string.IsNullOrEmpty(TextBoxDecrizione.Text))
                    descrizione = TextBoxDecrizione.Text;

                if (ViewState["cantiere"] != null)
                    cantiere = (Cantiere)ViewState["cantiere"];

                if (Request.QueryString["modalita"] != null)
                {
                    switch (Request.QueryString["modalita"])
                    {
                        case "prev":
                            programmato = false;
                            preventivato = true;
                            break;
                        case "cons":
                            programmato = false;
                            consuntivato = true;
                            break;
                    }
                }

                Attivita nuovaAttivita =
                    new Attivita(null, giorno, DateTime.Now, nomeAttivita, descrizione, ispettore, cantiere, programmato,
                                 preventivato, consuntivato);

                if (biz.InsertAttivita(nuovaAttivita))
                {
                    LabelRisultato.Text = "Attività aggiunta";
                    TextBoxDecrizione.Text = String.Empty;
                }
                else
                {
                    LabelRisultato.Text = "Errore durante l'inserimento";
                }
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            string pagina = "CantieriProgrammazioneSettimanale.aspx?modalita=programma";
            Ispettore ispettore = (Ispettore)ViewState["ispettore"];

            if (Request.QueryString["modalita"] != null)
            {
                switch (Request.QueryString["modalita"])
                {
                    case "prev":
                        pagina = "~/CeServizi/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=preventivo";
                        break;
                    case "cons":
                        pagina = "~/CeServizi/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?modalita=consuntivo";
                        break;
                }
            }

            if (Request.QueryString["idIspettore"] != null && Request.QueryString["dal"] != null)
                Response.Redirect(
                    string.Format("{0}&idIspettore={1}&dal={2}", pagina, ispettore.IdIspettore, Request.QueryString["dal"]));
            else
                Response.Redirect(pagina);
        }
    }
}