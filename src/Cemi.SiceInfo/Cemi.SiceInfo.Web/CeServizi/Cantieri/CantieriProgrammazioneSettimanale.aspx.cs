﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Ispettore = TBridge.Cemi.Type.Entities.GestioneUtenti.Ispettore;


namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriProgrammazioneSettimanale : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        private bool gestioneProgrammazioneAbilitata;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione);
            funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneGestione);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            //Modifichiamo la larghezza dei div per fare in modo che la pagina venga visualizzata correttamente. ai dive sono stati aggiunti 220px
            //((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("maindiv")).Attributes.Add("style", "width: 1040px;");
            //((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("outerdiv")).Attributes.Add("style", "width: 1300px;");

            //WebControls_MenuDx menudx = (WebControls_MenuDx)Master.FindControl("MenuDx1");
            //if (menudx != null)
            //    menudx.Visible = false;

            if (!Page.IsPostBack)
            {
                //Carichiamo l'abilitazione alla gestione della programmazione
                gestioneProgrammazioneAbilitata =
                    GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriProgrammazioneGestione);

                GridViewAttivita.Enabled = gestioneProgrammazioneAbilitata;

                //Carichiamo la lista degli ispettori
                IspettoreCollection listaIspettori = biz.GetIspettori(null, true);
                DropDownListIspettore.DataSource = listaIspettori;
                DropDownListIspettore.DataTextField = "NomeCompleto";
                DropDownListIspettore.DataValueField = "IdIspettore";
                DropDownListIspettore.DataBind();

                //Se l'utente è ispettore e non è rui
                if (GestioneUtentiBiz.IsIspettore() && !gestioneProgrammazioneAbilitata)
                {
                    //TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore ispettore =
                    //    ((Ispettore) HttpContext.Current.User.Identity).
                    //        Entity;
                    Ispettore ispettore =
                        (Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    DropDownListIspettore.SelectedValue = ispettore.IdIspettore.ToString();
                    DropDownListIspettore.Enabled = false;
                }

                // Torno dall'inserimento di una nuova attività
                if (Request.QueryString["idIspettore"] != null && Request.QueryString["dal"] != null)
                {
                    DateTime dal = DateTime.Parse(Request.QueryString["dal"]);
                    for (int i = 0; i < 7; i++)
                    {
                        CalendarSettimana.SelectedDates.Add(dal);
                        dal = dal.AddDays(1);
                    }
                    CalendarSettimana.VisibleDate = dal;
                    CalendarSettimana.DataBind();

                    DropDownListIspettore.SelectedValue = Request.QueryString["idIspettore"];

                    VisualizzaProgramma();
                }
            }
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            VisualizzaProgramma();
        }

        private void VisualizzaProgramma()
        {
            if (DropDownListIspettore.SelectedValue != null)
                if (CalendarSettimana.SelectedDates.Count == 7) // controllo sulla settimana
                {
                    LabelErrore.Text = string.Empty;

                    DateTime dal = CalendarSettimana.SelectedDates[0];
                    DateTime al = CalendarSettimana.SelectedDates[6];
                    int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);

                    int i = 2;
                    for (DateTime dSet = dal; dSet <= al; dSet = dSet.AddDays(1))
                    {
                        if (dSet.DayOfWeek != DayOfWeek.Saturday && dSet.DayOfWeek != DayOfWeek.Sunday)
                        {
                            GridViewAttivita.Columns[i].HeaderText = dSet.ToShortDateString();
                            i++;
                        }
                    }

                    ProgrammazioneGiornaliereIspettoreCollection programmazione =
                        biz.GetProgrammazioneSettimanale(dal, al, idIspettore, true, false, false);
                    ProgrammazioneSettimanaleIspettore settimana = new ProgrammazioneSettimanaleIspettore();
                    settimana.Add(programmazione);
                    GridViewAttivita.DataSource = settimana;
                    GridViewAttivita.DataBind();

                    ButtonReport.Enabled = true;
                }
                else
                    LabelErrore.Text = "Selezionare una settimana";
            else
                LabelErrore.Text = "Selezionare un ispettore";
        }

        protected void GridViewAttivita_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ProgrammazioneGiornaliereIspettoreCollection programmazione =
                    (ProgrammazioneGiornaliereIspettoreCollection)e.Row.DataItem;

                if (programmazione != null && programmazione.Count == 5)
                {
                    Label labelIsp = (Label)e.Row.FindControl("LabelIspettore");
                    labelIsp.Text = DropDownListIspettore.SelectedItem.Text;
                    //labelIsp.DataBind();

                    // Popolo i 7 giorni della settimana
                    GridView gvAttivita1 = (GridView)e.Row.FindControl("GridViewGiorno1");
                    gvAttivita1.DataSource = programmazione[0].ListaAttivita;
                    gvAttivita1.DataBind();

                    GridView gvAttivita2 = (GridView)e.Row.FindControl("GridViewGiorno2");
                    gvAttivita2.DataSource = programmazione[1].ListaAttivita;
                    gvAttivita2.DataBind();

                    GridView gvAttivita3 = (GridView)e.Row.FindControl("GridViewGiorno3");
                    gvAttivita3.DataSource = programmazione[2].ListaAttivita;
                    gvAttivita3.DataBind();

                    GridView gvAttivita4 = (GridView)e.Row.FindControl("GridViewGiorno4");
                    gvAttivita4.DataSource = programmazione[3].ListaAttivita;
                    gvAttivita4.DataBind();

                    GridView gvAttivita5 = (GridView)e.Row.FindControl("GridViewGiorno5");
                    gvAttivita5.DataSource = programmazione[4].ListaAttivita;
                    gvAttivita5.DataBind();

                    //GridView gvAttivita6 = (GridView)e.Row.FindControl("GridViewGiorno6");
                    //gvAttivita6.DataSource = programmazione[5].ListaAttivita;
                    //gvAttivita6.DataBind();

                    //GridView gvAttivita7 = (GridView)e.Row.FindControl("GridViewGiorno7");
                    //gvAttivita7.DataSource = programmazione[6].ListaAttivita;
                    //gvAttivita7.DataBind();
                }
            }
        }

        protected void ButtonAggiungiAttivita_Click(object sender, EventArgs e)
        {
            if (DropDownListIspettore.SelectedValue != null)
            {
                int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
                DateTime dal = CalendarSettimana.SelectedDates[0];
                DateTime giorno = DateTime.Now;

                // Recupero il giorno scelto
                Button bottone = (Button)sender;
                switch (bottone.ID)
                {
                    case "ButtonAggiungiAttivita1":
                        giorno = DateTime.Parse(GridViewAttivita.Columns[2].HeaderText);
                        break;
                    case "ButtonAggiungiAttivita2":
                        giorno = DateTime.Parse(GridViewAttivita.Columns[3].HeaderText);
                        break;
                    case "ButtonAggiungiAttivita3":
                        giorno = DateTime.Parse(GridViewAttivita.Columns[4].HeaderText);
                        break;
                    case "ButtonAggiungiAttivita4":
                        giorno = DateTime.Parse(GridViewAttivita.Columns[5].HeaderText);
                        break;
                    case "ButtonAggiungiAttivita5":
                        giorno = DateTime.Parse(GridViewAttivita.Columns[6].HeaderText);
                        break;
                }

                Response.Redirect(
                    string.Format(
                        "~/CeServizi/Cantieri/CantieriInserisciModificaProgrammazioneAttivitaGiornaliera.aspx?idIspettore={0}&giorno={1}&dal={2}&modalita=prog",
                        idIspettore, giorno.ToShortDateString(), dal.ToShortDateString()));
            }
        }

        protected void DropDownListIspettore_SelectedIndexChanged(object sender, EventArgs e)
        {
            SvuotaAttivita();
        }

        protected void CalendarSettimana_SelectionChanged(object sender, EventArgs e)
        {
            DateTime giornoSel = CalendarSettimana.SelectedDate;
            CalendarSettimana.SelectedDates.Clear();
            for (int i = 0; i < 7; i++)
            {
                CalendarSettimana.SelectedDates.Add(giornoSel.AddDays(i));
            }
            CalendarSettimana.DataBind();

            SvuotaAttivita();
        }

        private void SvuotaAttivita()
        {
            ButtonReport.Enabled = false;

            GridViewAttivita.DataSource = null;
            GridViewAttivita.DataBind();
        }

        protected void GridViewGiorno1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView attivita = (GridView)sender;
            int idAttivita = (int)attivita.DataKeys[e.RowIndex].Value;
            int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
            DateTime dal = CalendarSettimana.SelectedDates[0];

            //biz.DeleteAttivita(idAttivita);
            Response.Redirect(
                string.Format(
                    "~/CeServizi/Cantieri/CantieriConfermaCancellazioneAttivita.aspx?idAttivita={0}&o=prog&idIspettore={1}&dal={2}",
                    idAttivita, idIspettore, dal.ToShortDateString()));
            //VisualizzaProgramma();
        }

        protected void ButtonReport_Click(object sender, EventArgs e)
        {
            int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);
            DateTime dal = CalendarSettimana.SelectedDates[0];

            Response.Redirect(
                string.Format("~/CeServizi/Cantieri/ReportCantieriAttivita.aspx?tipo=programmazione&idIspettore={0}&dal={1}", idIspettore,
                              dal.ToShortDateString()));
        }

        protected void GridViewGiorno_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Attivita attivita = (Attivita)e.Row.DataItem;
                Label labelImpresa = (Label)e.Row.FindControl("LabelImpresa");

                if (attivita.Cantiere.ImpresaAppaltatrice != null)
                    labelImpresa.Text = attivita.Cantiere.ImpresaAppaltatrice.RagioneSociale;
                else if (!string.IsNullOrEmpty(attivita.Cantiere.ImpresaAppaltatriceTrovata))
                    labelImpresa.Text = attivita.Cantiere.ImpresaAppaltatriceTrovata;
            }
        }
    }
}