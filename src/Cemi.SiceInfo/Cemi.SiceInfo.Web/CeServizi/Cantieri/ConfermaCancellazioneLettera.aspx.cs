﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class ConfermaCancellazioneLettera : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriLettere);
            ButtonAnnulla.Attributes.Add("onclick", "hystory.back();");

            if (!Page.IsPostBack)
            {
                LabelProtocollo.Text = Context.Items["protocollo"].ToString();
            }
        }

        protected void ButtonConferma_Click(object sender, EventArgs e)
        {
            string protocollo = LabelProtocollo.Text;

            if (biz.DeleteLettera(protocollo))
            {
                LabelErrore.Visible = false;
                Server.Transfer("~/CeServizi/Cantieri/CantieriLogLettere.aspx");
            }
            else
                LabelErrore.Visible = true;
        }
    }
}