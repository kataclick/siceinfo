﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using System.Drawing;
using System.Web.UI.HtmlControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Delegates.Cantieri;
using TBridge.Cemi.Type.Filters.Cantieri;
using Cantiere = TBridge.Cemi.Type.Entities.Cantieri.Cantiere;
using Ispettore = TBridge.Cemi.Type.Entities.GestioneUtenti.Ispettore;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CalendarioAttivita : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly BusinessEF bizEF = new BusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            RicercaCantieriPerIspettori1.OnCantiereSelected += new CantieriSelectedEventHandler(RicercaCantieriPerIspettori1_OnCantiereSelected);

            if (!Page.IsPostBack)
            {
                DataBind();

                RadSchedulerAttivita.SelectedDate = DateTime.Now;
                CaricaCalendarioAttivita();
                RicercaCantieriPerIspettori1.SoloVisualizzazioneAttivitaNonPreseInCarico();
            }
            else
            {
                // Verifico se è un Refresh fatto con Javascript
                String eventArg = Request["__EVENTARGUMENT"];

                if (eventArg != null)
                {
                    int offset = eventArg.IndexOf("@@@@@");

                    if (offset > -1)
                    {
                        RicercaCantieriPerIspettori1.SoloVisualizzazioneAttivitaNonPreseInCarico();
                        CaricaCalendarioAttivita();
                    }
                }
            }
        }

        void RicercaCantieriPerIspettori1_OnCantiereSelected(Cantiere cantiere)
        {
            // Devo far partire la creazione di un'attività selezionando il cantiere..
            Int32 i = 0;
        }

        private void CaricaCalendarioAttivita()
        {
            DateTime inizio = RadSchedulerAttivita.VisibleRangeStart;
            DateTime fine = RadSchedulerAttivita.VisibleRangeEnd;

            Int32 idIspettore = ((Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdIspettore;
            List<CantieriCalendarioAttivita> calendarioAttivita = bizEF.GetCalendarioAttivita(idIspettore, inizio, fine);

            RadSchedulerAttivita.DataSource = calendarioAttivita;
            RadSchedulerAttivita.DataBind();
        }

        protected String RecuperaPostBackCode()
        {
            return this.Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
        }

        protected void RadSchedulerAttivita_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            Boolean inCarico = false;
            CantieriCalendarioAttivita attivita = (CantieriCalendarioAttivita)e.Appointment.DataItem;

            Int32 idIspettoreCorrente = ((Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdIspettore;
            TBridge.Cemi.Type.Entities.Cantieri.Ispettore ispettoreCorrente = new TBridge.Cemi.Type.Entities.Cantieri.Ispettore() { IdIspettore = idIspettoreCorrente };
            inCarico = attivita.InCarico(idIspettoreCorrente);

            if (!inCarico)
            {
                e.Appointment.AllowDelete = false;
                e.Appointment.AllowEdit = false;
            }

            switch (attivita.TipologiaAttivita.Id)
            {
                case 1:
                    // Ispezione

                    // Se esiste rapporto permetto la stampa e la modifica, altrimenti l'inserimento
                    IspezioniFilter filtro = new IspezioniFilter();
                    filtro.IdCantiere = attivita.CantieriCantieri.First().IdCantiere;
                    RapportoIspezioneCollection ispezioni = biz.GetIspezioniConGruppo(filtro);

                    if (ispezioni != null && ispezioni.Count == 1)
                    {
                        e.Appointment.BackColor = ColoriAttivita.SfondoIspezioneConsiderata;
                        e.Appointment.AllowDelete = false;
                        e.Appointment.AllowEdit = false;

                        if (inCarico)
                        {
                            e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioniModificaStampa";
                            if (ispezioni[0].Stato.HasValue)
                            {
                                e.Appointment.Description =
                                    attivita.DescrizioneAttivitaConRapportoIspezione(ispezioni[0].Stato.ToString());
                            }
                        }
                        else
                        {
                            // Se esiste rapporto permetto la stampa
                            e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioniStampa";

                            if (ispezioni[0].GruppoIspezione != null && !ispezioni[0].GruppoIspezione.Contains(ispettoreCorrente))
                            {
                                e.Appointment.BackColor = ColoriAttivita.SfondoIspezioneFantasma;
                                e.Appointment.ForeColor = Color.LightGray;

                                e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioni";
                            }
                        }
                    }
                    else
                    {
                        if (attivita.Rifiuto == null)
                        {
                            e.Appointment.BackColor = ColoriAttivita.SfondoIspezione;

                            if (inCarico)
                            {
                                e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioniModifica";
                            }
                            else
                            {
                                e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioni";
                            }
                        }
                        else
                        {
                            e.Appointment.BackColor = ColoriAttivita.SfondoIspezioneConsiderata;

                            e.Appointment.AllowDelete = false;
                            e.Appointment.AllowEdit = false;

                            e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioni";

                            e.Appointment.Description =
                                    attivita.DescrizioneAttivitaConRifiuto(attivita.Rifiuto.Descrizione);
                        }
                    }

                    break;
                case 2:
                    // Appuntamenti
                    e.Appointment.BackColor = ColoriAttivita.SfondoAppuntamento;
                    e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioniApriElimina";
                    break;
                case 3:
                    // Attività di Back office
                    e.Appointment.BackColor = ColoriAttivita.SfondoBackOffice;
                    e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioniApriElimina";
                    break;
                case 4:
                    // Varie
                    e.Appointment.BackColor = ColoriAttivita.SfondoVarie;
                    e.Appointment.ContextMenuID = "RadSchedulerContextMenuIspezioniApriElimina";
                    break;
            }
        }

        protected void RadSchedulerAttivita_AppointmentDelete(object sender, Telerik.Web.UI.SchedulerCancelEventArgs e)
        {
            LabelErrore.Visible = false;

            try
            {
                bizEF.DeleteAttivita((Int32)e.Appointment.ID);
                //CaricaCalendarioAttivita();
                RicercaCantieriPerIspettori1.SoloVisualizzazioneAttivitaNonPreseInCarico();
            }
            catch
            {
                LabelErrore.Visible = true;
                e.Cancel = true;
            }
        }

        protected void RadSchedulerAttivita_AppointmentUpdate(object sender, Telerik.Web.UI.AppointmentUpdateEventArgs e)
        {
            LabelErrore.Visible = false;

            Int32 idAttivita = (Int32)e.Appointment.ID;
            DateTime inizio = e.ModifiedAppointment.Start;
            Int32 durata = e.ModifiedAppointment.Duration.Hours;

            try
            {
                bizEF.UpdateAttivita(idAttivita, inizio, durata);
                CaricaCalendarioAttivita();
            }
            catch
            {
                LabelErrore.Visible = true;
                e.Cancel = true;
            }
        }

        protected void RadSchedulerAttivita_AppointmentContextMenuItemClicked(object sender, Telerik.Web.UI.AppointmentContextMenuItemClickedEventArgs e)
        {
            if (e.MenuItem.Value != "CommandDelete")
            {
                Int32 idAttivita = (Int32)e.Appointment.ID;
                CantieriCalendarioAttivita attivita = bizEF.GetAttivita(idAttivita);
                RapportoIspezioneCollection ispezioni = null;
                Int32? idIspezione = null;
                Int32? idCantiere = null;

                if (attivita.CantieriCantieri != null && attivita.CantieriCantieri.Count == 1)
                {
                    IspezioniFilter filtro = new IspezioniFilter();
                    idCantiere = attivita.CantieriCantieri.Single().IdCantiere;
                    filtro.IdCantiere = idCantiere;
                    ispezioni = biz.GetIspezioni(filtro);

                    if (ispezioni != null && ispezioni.Count == 1)
                    {
                        idIspezione = ispezioni[0].IdIspezione;
                    }
                }

                switch (e.MenuItem.Value)
                {
                    case "APRI":
                        break;
                    case "STAMPA":
                        Response.Redirect(String.Format("~/CeServizi/Cantieri/CantieriIspezioneStampaReport.aspx?idIspezione={0}", idIspezione));
                        break;
                    case "MODIFICA":
                        if (!idIspezione.HasValue)
                        {
                            // Nuovo rapporto di ispezione
                            Response.Redirect(String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep1.aspx?idIspettore={0}&giorno={1}&idCantiere={2}",
                                            ((Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdIspettore,
                                            attivita.Data.ToString("dd/MM/yyyy"),
                                            idCantiere));
                        }
                        else
                        {
                            // Modifica di un rapporto esistente
                            Response.Redirect(String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep1.aspx?idIspezione={0}",
                                            idIspezione));
                        }
                        break;
                }
            }
        }

        protected void RadSchedulerAttivita_NavigationComplete(object sender, Telerik.Web.UI.SchedulerNavigationCompleteEventArgs e)
        {
            CaricaCalendarioAttivita();
        }
    }
}