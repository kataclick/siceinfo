﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class SegnalaCantiere : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly BusinessEF bizEF = new BusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriSegnalazione);
            #endregion

            if (!Page.IsPostBack)
            {
                //CaricaMotivazioni();
                //CaricaPriorita();

                if (Request.QueryString["idCantiere"] != null)
                {
                    Int32 idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
                    Boolean modifica = Request.QueryString["modalita"] != null;

                    CaricaDatiCantiere(idCantiere, modifica);
                }
                else
                {
                    ButtonSegnala.Enabled = false;
                }
            }
        }

        //private void CaricaPriorita()
        //{
        //    Presenter.CaricaElementiInDropDownConElementoVuoto(
        //        RadComboBoxPriorita,
        //        bizEF.GetSegnalazioniPriorita(),
        //        "Descrizione",
        //        "Id");
        //}

        //private void CaricaMotivazioni()
        //{
        //    Presenter.CaricaElementiInDropDownConElementoVuoto(
        //        RadComboBoxMotivazione,
        //        bizEF.GetSegnalazioneMotivazioni(),
        //        "Descrizione",
        //        "Id");
        //}

        private void CaricaDatiCantiere(Int32 idCantiere, Boolean modifica)
        {
            Cantiere cantiere = biz.GetCantiere(idCantiere);
            ViewState["IdCantiere"] = idCantiere;

            LabelIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
            if (!String.IsNullOrEmpty(cantiere.Cap))
            {
                LabelCap.Text = String.Format("{0} ", cantiere.Cap);
            }
            LabelComune.Text = cantiere.Comune;
            LabelProvincia.Text = cantiere.Provincia;

            if (modifica)
            {
                CaricaSegnalazione(cantiere);
            }
            else
            {
                //LabelData.Text = DateTime.Now.ToString("dd/MM/yyyy");
                //LabelUtente.Text = GestioneUtentiBiz.GetNomeUtente();
                SegnalazioneDati1.CaricaDatiIniziali();
            }
        }

        private void CaricaSegnalazione(Cantiere cantiere)
        {
            ButtonElimina.Visible = true;

            //ViewState["IdSegnalazione"] = cantiere.Segnalazione.IdSegnalazione;
            //LabelData.Text = cantiere.Segnalazione.Data.ToString("dd/MM/yyyy");
            //LabelUtente.Text = cantiere.Segnalazione.NomeUtente;
            //CheckBoxRicorrente.Checked = cantiere.Segnalazione.Ricorrente;
            //RadComboBoxMotivazione.SelectedValue = cantiere.Segnalazione.Motivazione.Id.ToString();
            //RadComboBoxPriorita.SelectedValue = cantiere.Segnalazione.Priorita.Id.ToString();
            //RadTextBoxNote.Text = cantiere.Segnalazione.Note;
            //ButtonSegnala.Text = "Modifica la segnalazione";
            SegnalazioneDati1.CaricaSegnalazione(cantiere);
            ViewState["IdSegnalazione"] = cantiere.Segnalazione.IdSegnalazione;

            // Se il cantiere è stato assegnato non permetto la modifica della segnalazione
            if (cantiere.Assegnazione != null || cantiere.Segnalazione.IdUtente != GestioneUtentiBiz.GetIdUtente())
            {
                //CheckBoxRicorrente.Enabled = false;
                //RadComboBoxMotivazione.Enabled = false;
                //RadComboBoxPriorita.Enabled = false;
                //RadTextBoxNote.Enabled = false;
                //ButtonSegnala.Visible = false;
                //ButtonElimina.Visible = false;
                SegnalazioneDati1.Abilita(false);
            }
        }

        protected void ButtonSegnala_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Segnalazione segnalazione = CreaSegnalazione();

                if (segnalazione.IdSegnalazione <= 0)
                {
                    if (biz.InsertSegnalazione(segnalazione))
                    {
                        ConfermaInserimento();
                    }
                    else
                    {
                        ErroreNellInserimento();
                    }
                }
                else
                {
                    try
                    {
                        bizEF.UpdateSegnalazione(segnalazione);
                        ConfermaInserimento();
                    }
                    catch
                    {
                        ErroreNellInserimento();
                    }
                }
            }
        }

        private void ErroreNellInserimento()
        {
            LabelMessaggio.Text = "Errore durante l'inserimento/modifica della segnalazione.";
        }

        private void ConfermaInserimento()
        {
            //CheckBoxRicorrente.Enabled = false;
            //RadComboBoxMotivazione.Enabled = false;
            //RadComboBoxPriorita.Enabled = false;
            //RadTextBoxNote.Enabled = false;
            SegnalazioneDati1.Abilita(false);
            ButtonSegnala.Enabled = false;
            ButtonElimina.Enabled = false;

            LabelMessaggio.Text = "Segnalazione effettuata correttamente.";
        }

        private Segnalazione CreaSegnalazione()
        {
            //Segnalazione segnalazione = new Segnalazione();

            //if (ViewState["IdSegnalazione"] != null)
            //{
            //    segnalazione.IdSegnalazione = (Int32)ViewState["IdSegnalazione"];
            //}
            //else
            //{
            //    segnalazione.IdSegnalazione = -1;
            //}
            //segnalazione.IdCantiere = (Int32)ViewState["IdCantiere"];
            //segnalazione.Data = DateTime.Now;
            //segnalazione.Ricorrente = CheckBoxRicorrente.Checked;
            //segnalazione.Motivazione = new TBridge.Cemi.Type.Domain.CantiereSegnalazioneMotivazione();
            //segnalazione.Motivazione.Id = Int32.Parse(RadComboBoxMotivazione.SelectedItem.Value);
            //segnalazione.Motivazione.Descrizione = RadComboBoxMotivazione.SelectedItem.Text;
            //segnalazione.Priorita = new TBridge.Cemi.Type.Domain.CantiereSegnalazionePriorita();
            //segnalazione.Priorita.Id = Int32.Parse(RadComboBoxPriorita.SelectedItem.Value);
            //segnalazione.Priorita.Descrizione = RadComboBoxPriorita.SelectedItem.Text;
            //segnalazione.Note = Presenter.NormalizzaCampoTesto(RadTextBoxNote.Text);

            //// Utente che effettua la segnalazione
            //segnalazione.IdUtente = GestioneUtentiBiz.GetIdUtente();

            //return segnalazione;
            Segnalazione segnalazione = SegnalazioneDati1.CreaSegnalazione();
            segnalazione.IdCantiere = (Int32)ViewState["IdCantiere"];

            return segnalazione;
        }

        protected void ButtonElimina_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 idSegnalazione = (Int32)ViewState["IdSegnalazione"];

                bizEF.DeleteSegnalazione(idSegnalazione);
                ConfermaInserimento();
            }
            catch
            {
                ErroreNellInserimento();
            }
        }
    }
}