﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriGestioneZone.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriGestioneZone" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Gestione zone" />
    <br />
    <asp:Button ID="ButtonNuova" runat="server" OnClick="ButtonNuova_Click" Text="Crea nuova zona" />
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco zone"></asp:Label><br />
    
    <asp:GridView ID="GridViewZone" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewZone_RowDataBound" DataKeyNames="IdZona" OnRowEditing="GridViewZone_RowEditing" Width="100%" OnRowDeleting="GridViewZone_RowDeleting" AllowPaging="True" OnPageIndexChanging="GridViewZone_PageIndexChanging" PageSize="1">
        <Columns>
            <asp:BoundField DataField="Nome" HeaderText="Nome" />
            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
            <asp:TemplateField HeaderText="Cap">
                <ItemTemplate>
                    <asp:GridView ID="GridViewCap" runat="server" AutoGenerateColumns="False" BorderColor="BlanchedAlmond"
                        BorderStyle="Solid" BorderWidth="1px" OnRowDataBound="GridViewCap_RowDataBound"
                        ShowHeader="False" Width="250px">
                        <Columns>
                            <asp:BoundField DataField="Cap" HeaderText="CAP" />
                            <asp:TemplateField HeaderText="CAP">
                                <ItemTemplate>
                                    <asp:GridView ID="GridViewComuni" runat="server" AutoGenerateColumns="False"
                                        ShowHeader="False" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="Comune" HeaderText="Comune">
                                                <ItemStyle Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Provincia" HeaderText="Provincia">
                                                <ItemStyle Width="10px" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessun CAP nella lista
                        </EmptyDataTemplate>
                    </asp:GridView>
                    &nbsp;
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Modifica" ShowEditButton="True" >
                <ItemStyle Width="70px" />
            </asp:CommandField>
            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina" ShowDeleteButton="True" >
                <ItemStyle Width="70px" />
            </asp:CommandField>
        </Columns>
        <EmptyDataTemplate>
            Nessuna zona presente
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    E' possibile eliminare solamente le zone che non sono associate ad alcun ispettore<br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label><br />
</asp:Content>

