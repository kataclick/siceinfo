﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="AttivitaClass.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.AttivitaClass" %>

<%@ Register src="WebControls/RicercaCantieriPerIspettori.ascx" tagname="RicercaCantieriPerIspettori" tagprefix="uc1" %>

<%@ Register src="WebControls/InformazioniBaseCantiere.ascx" tagname="InformazioniBaseCantiere" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1 {
            width: 39px;
        }
        .style2
        {
            width: 40px;
        }
        .style3
        {
            width: 110px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager ID="RadScriptManagerMain" Runat="server">
        </telerik:RadScriptManager>
        <table class="standardTable">
            <tr>
                <td>
                    Ispettore
                </td>
                <td colspan="2">
                    <asp:Label
                        ID="LabelIspettore"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Data
                </td>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDateTimePickerInizio"
                        runat="server"
                        Width="250px">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <asp:CustomValidator
                        ID="CustomValidatorInizio"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="Selezionare l'inizio dell'attività" 
                        onservervalidate="CustomValidatorInizio_ServerValidate"
                        ForeColor="Red">
                        *
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Orario
                </td>
                <td>
                    <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <table class="standardTable">
                            <tr>
                                <td class="style3">
                                    <asp:RadioButtonList
                                        ID="RadioButtonListPeriodo"
                                        runat="server"
                                        AutoPostBack="true" 
                                        onselectedindexchanged="RadioButtonListPeriodo_SelectedIndexChanged">
                                        <asp:ListItem Text="Tutto il giorno" Value="G">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Mattino" Value="M">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Pomeriggio" Value="P">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Personalizzato" Value="E">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td class="style2">
                                    <table class="standardTable">
                                        <tr>
                                            <td>
                                                Dalle
                                            </td>
                                            <td>
                                                <telerik:RadTimePicker
                                                    ID="RadTimePickerDalle"
                                                    runat="server"
                                                    Width="100px"
                                                    MinDate="1/1/1900 0:0"
                                                    MaxDate="1/1/2100 23:59"
                                                    SelectedDate="1/1/1900 8:30" AutoPostBack="True" 
                                                    onselecteddatechanged="RadTimePicker_SelectedDateChanged"
                                                    Enabled="false">
                                                </telerik:RadTimePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Alle
                                            </td>
                                            <td>
                                                <telerik:RadTimePicker
                                                    ID="RadTimePickerAlle"
                                                    runat="server"
                                                    Width="100px"
                                                    MinDate="1/1/1900 0:0"
                                                    MaxDate="1/1/2100 23:59"
                                                    SelectedDate="1/1/1900 17:30"
                                                    onselecteddatechanged="RadTimePicker_SelectedDateChanged"
                                                    Enabled="false">
                                                </telerik:RadTimePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:CustomValidator
                        ID="CustomValidatorOrario"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="Selezionare un orario" 
                        onservervalidate="CustomValidatorOrario_ServerValidate"
                        ForeColor="Red" >
                        *
                    </asp:CustomValidator>
                    <asp:CustomValidator
                        ID="CustomValidatorFineMaggioreInizio"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="La fine dell'attività deve essere successiva all'inizio" 
                        onservervalidate="CustomValidatorFineMaggioreInizio_ServerValidate"
                        ForeColor="Red">
                        *
                    </asp:CustomValidator>
                </td>
            </tr>
            <%--<tr>
                <td>
                    Fine
                </td>
                <td>
                    <telerik:RadDateTimePicker
                        ID="RadDateTimePickerFine"
                        runat="server"
                        Width="250px">
                    </telerik:RadDateTimePicker>
                </td>
                <td>
                    <asp:CustomValidator
                        ID="CustomValidatorFine"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="Selezionare la fine dell'attività" 
                        onservervalidate="CustomValidatorFine_ServerValidate">
                        *
                    </asp:CustomValidator>
                    <asp:CustomValidator
                        ID="CustomValidatorFineMaggioreInizio"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="La fine dell'attività deve essere successiva all'inizio" 
                        onservervalidate="CustomValidatorFineMaggioreInizio_ServerValidate">
                        *
                    </asp:CustomValidator>
                    <asp:CustomValidator
                        ID="CustomValidatorInizioFineStessoGiorno"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="L'inizio e la fine dell'attività devono avvenire lo stesso giorno" 
                        onservervalidate="CustomValidatorInizioFineStessoGiorno_ServerValidate">
                        *
                    </asp:CustomValidator>
                </td>
            </tr>--%>
            <tr>
                <td>
                    Tipo Attività
                </td>
                <td>
                    <telerik:RadComboBox
                        ID="RadComboBoxTipoAttivita"
                        AutoPostBack="true"
                        runat="server"
                        Width="250px"
                        EmptyMessage="Seleziona il tipo di attività" 
                        AppendDataBoundItems="True" 
                        onselectedindexchanged="RadComboBoxTipoAttivita_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator
                        ID="RequiredFieldValidatorTipoAttivita"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="Selezionare la tipologia di attività"
                        ControlToValidate="RadComboBoxTipoAttivita"
                        ForeColor="Red">
                        *
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Descrizione
                </td>
                <td>
                    <telerik:RadTextBox
                        ID="RadTextBoxDescrizione"
                        runat="server"
                        Width="250px"
                        MaxLength="500" Height="80px" TextMode="MultiLine">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <asp:CustomValidator
                        ID="CustomValidatorDescrizione"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="Immettere una descrizione" 
                        onservervalidate="CustomValidatorDescrizione_ServerValidate"
                        ForeColor="Red" >
                        *
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Cantiere
                </td>
                <td>
                    <uc2:InformazioniBaseCantiere ID="InformazioniBaseCantiere1" runat="server" />
                    <br />
                    <asp:Button
                        ID="ButtonRicercaCantieri"
                        runat="server"
                        Text="Ricerca cantieri"
                        Width="150px" onclick="ButtonRicercaCantieri_Click" Enabled="False" />
                </td>
                <td>
                    <asp:CustomValidator
                        ID="CustomValidatorCantiere"
                        runat="server"
                        ValidationGroup="attivita"
                        ErrorMessage="Selezionare un cantiere" 
                        onservervalidate="CustomValidatorCantiere_ServerValidate"
                        ForeColor="Red" >
                        *
                    </asp:CustomValidator>
                </td>
            </tr>
            <tr id="trRicercaCantieri" runat="server" visible="false">
                <td colspan="3">
                    <uc1:RicercaCantieriPerIspettori ID="RicercaCantieriPerIspettori1" 
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Button
                        ID="ButtonSalva"
                        runat="server"
                        Text="Salva"
                        Width="150px"
                        ValidationGroup="attivita" onclick="ButtonSalva_Click" />
                    <br />        
                    <asp:ValidationSummary
                        ID="ValidationSummaryAttivita"
                        runat="server"
                        ValidationGroup="attivita"
                        CssClass="messaggiErrore" />
                    <br />
                    <asp:Label
                        ID="LabelMessaggio"
                        runat="server"
                        CssClass="messaggiErrore">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
