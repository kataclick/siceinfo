﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriSchedaCantiere.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriSchedaCantiere" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Scheda cantiere">
    </uc1:TitoloSottotitolo>
    <br />
    <asp:Panel ID="PanelDatiComuni" runat="server">
        <table class="standardTable">
            <tr>
                <td>
                    Indirizzo:
                </td>
                <td>
                    <asp:Label ID="LabelIndirizzo" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Civico:
                </td>
                <td>
                    <asp:Label ID="LabelCivico" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Comune:
                </td>
                <td>
                    <asp:Label ID="LabelComune" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia:
                </td>
                <td>
                    <asp:Label ID="LabelProvincia" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Cap:
                </td>
                <td>
                    <asp:Label ID="LabelCap" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    N°permesso costruire/DIA:
                </td>
                <td>
                    <asp:Label ID="LabelPermessoCostruire" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Attivo:
                </td>
                <td>
                    <asp:CheckBox ID="CheckBoxAttivo" runat="server" Enabled="False" />
                </td>
            </tr>
            <tr>
                <td>
                    Importo (€):
                </td>
                <td>
                    <asp:Label ID="LabelImporto" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Data inizio lavori:
                </td>
                <td>
                    <asp:Label ID="LabelDataInizioLavori" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Data fine lavori:
                </td>
                <td>
                    <asp:Label ID="LabelDataFineLavori" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Tipologia appalto:
                </td>
                <td>
                    <asp:Label ID="LabelTipologiaAppalto" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Latitudine:
                </td>
                <td>
                    <asp:Label ID="LabelLatitudine" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Longitudine:
                </td>
                <td>
                    <asp:Label ID="LabelLongitudine" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Committente:
                </td>
                <td>
                    <asp:Label ID="LabelCommittente" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Impresa:
                </td>
                <td>
                    <asp:Label ID="LabelImpresa" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Direzione lavori:
                </td>
                <td>
                    <asp:Label ID="LabelDirezioneLavori" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Responsabile procedimento:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileProcedimento" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Responsabile cantiere:
                </td>
                <td>
                    <asp:Label ID="LabelResponsabileCantiere" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Descrizione lavori:
                </td>
                <td>
                    <asp:Label ID="LabelDescrizioneLavori" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    &nbsp;
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            &nbsp;<asp:LinkButton ID="LinkButtonLegenda" runat="server" OnClick="LinkButtonLegenda_Click">Mostra legenda</asp:LinkButton>
            <asp:Panel ID="PanelLegenda" runat="server" Width="600px" Visible="False">
                <table class="borderedTable">
                    <tr>
                        <td style="background-color: Red">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            Impresa/Committente letta da fonte ma non ancora associata ad una entità del sistema
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: Gray">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            Impresa associata all’anagrafica temporanea dei cantieri
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #022a64">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td>
                            Impresa presente in SiceNew / Committente presente in anagrafica
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    &nbsp;&nbsp;<br />
    <b></b>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Ispezioni"></asp:Label><br />
    <asp:GridView ID="GridViewIspezioni" runat="server" AutoGenerateColumns="False" Width="100%">
        <Columns>
            <asp:BoundField DataField="Giorno" HeaderText="Giorno" DataFormatString="{0:dd/MM/yyyy}"
                HtmlEncode="False" />
            <asp:BoundField DataField="NomeIspettore" HeaderText="Ispettore" />
            <asp:BoundField DataField="Esito" HeaderText="Esito" />
        </Columns>
        <EmptyDataTemplate>
            Nessuna ispezione presente
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" Width="187px" /><br />
</asp:Content>

