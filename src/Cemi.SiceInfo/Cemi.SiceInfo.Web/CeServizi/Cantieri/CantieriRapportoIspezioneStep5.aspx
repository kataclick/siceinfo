﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriRapportoIspezioneStep5.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriRapportoIspezioneStep5" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<asp:Content ID="Content9" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Rapporto di ispezione - Completato"
        titolo="Cantieri" />
    <br />
    Il rapporto di ispezione è stato inserito correttamente nel sistema.<br />
    <br />
    Utilizzare il menu cantieri presente sulla sinistra per eseguire una'altra attività.<br />
</asp:Content>
