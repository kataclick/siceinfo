﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriLogLettere.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriLogLettere" %>

<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc4" %>
<%@ Register Src="WebControls/CantieriRicercaLettere.ascx" TagName="CantieriRicercaLettere" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Log lettere generate" titolo="Cantieri" />
    <br />
    <uc2:CantieriRicercaLettere ID="CantieriRicercaLettere1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>


