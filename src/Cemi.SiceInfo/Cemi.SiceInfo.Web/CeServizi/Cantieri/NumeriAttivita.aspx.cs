﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class NumeriAttivita : System.Web.UI.Page
    {
        private CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestioneAttivita);
            #endregion

            if (!Page.IsPostBack)
            {
                DateTime now = DateTime.Now;

                RadDateInputDal.SelectedDate = new DateTime(now.Year, 1, 1);
                RadDateInputAl.SelectedDate = now;
            }
        }

        protected void ButtonRicerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (RadDateInputDal.SelectedDate.HasValue
                    && RadDateInputAl.SelectedDate.HasValue)
                {
                    CaricaStatistiche(RadDateInputDal.SelectedDate.Value,
                                      RadDateInputAl.SelectedDate.Value);
                }
            }
        }

        private void CaricaStatistiche(DateTime dal, DateTime al)
        {
            CantieriStatistiche statistiche = biz.GetStatisticheCantieri(dal, al);

            PanelDati.Visible = true;

            LabelDal.Text = dal.ToShortDateString();
            LabelAl.Text = al.ToShortDateString();

            // Segnalazioni
            LabelSegnalazioniTotale.Text = statistiche.Segnalazioni.Totale.ToString();
            LabelSegnalazioniAssegnateRifiutate.Text = statistiche.Segnalazioni.TotaleAssegnazioniRifiuti.ToString();

            // Assegnazioni
            LabelAssegnazioniTotale.Text = statistiche.Assegnazioni.Totale.ToString();
            LabelAssegnazioniSegnalatiAssegnati.Text = statistiche.Assegnazioni.TotaleAssegnati.ToString();
            LabelAssegnazioniSegnalatiRifiutati.Text = statistiche.Assegnazioni.TotaleRifiutati.ToString();
            LabelAssegnazioniPresiInCarico.Text = statistiche.Assegnazioni.TotalePresiInCarico.ToString();

            Presenter.CaricaElementiInListView(
                ListViewAssegnazioniPerIspettore,
                statistiche.AssegnazioniPerIspettore);

            // Prese In Carico
            LabelPreseInCaricoTotale.Text = statistiche.PreseInCarico.TotalePresiInCarico.ToString();
            LabelPreseInCaricoConIspezione.Text = statistiche.PreseInCarico.TotaleIspezioni.ToString();
            LabelPreseInCaricoConIspezioneInVerifica.Text = statistiche.PreseInCarico.TotaleIspezioniInVerifica.ToString();
            LabelPreseInCaricoConIspezioneConcluse.Text = statistiche.PreseInCarico.TotaleIspezioniConcluse.ToString();

            Presenter.CaricaElementiInListView(
                ListViewPreseInCaricoPerIspettore,
                statistiche.PreseInCaricoPerIspettore);
        }

        #region Custom Validators
        protected void CustomValidatoreDal_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (!RadDateInputDal.SelectedDate.HasValue)
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatoreAl_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (!RadDateInputAl.SelectedDate.HasValue)
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatoreDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (RadDateInputDal.SelectedDate.HasValue && RadDateInputAl.SelectedDate.HasValue)
            {
                if (RadDateInputDal.SelectedDate.Value > RadDateInputAl.SelectedDate.Value)
                {
                    args.IsValid = false;
                }
            }
        }
        #endregion
    }
}