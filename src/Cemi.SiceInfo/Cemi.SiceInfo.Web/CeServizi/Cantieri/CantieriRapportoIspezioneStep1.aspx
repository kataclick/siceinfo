﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriRapportoIspezioneStep1.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriRapportoIspezioneStep1" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc8" %>
<%@ Register src="../WebControls/SelezioneIndirizzoLibero.ascx" tagname="IscrizioneLavoratoreIndirizzo" tagprefix="uc10" %>
<%@ Register Src="WebControls/CantieriTestataIspezione.ascx" TagName="CantieriTestataIspezione" TagPrefix="uc2" %>
<%@ Register Src="WebControls/CantieriRicercaCommittente.ascx" TagName="CantieriRicercaCommittente" TagPrefix="uc3" %>
<%@ Register Src="WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte" TagPrefix="uc4" %>
<%@ Register Src="WebControls/CantieriCommittente.ascx" TagName="CantieriCommittente" TagPrefix="uc6" %>
<%@ Register Src="WebControls/CantieriImpresa.ascx" TagName="CantieriImpresa" TagPrefix="uc7" %>
<%@ Register Src="WebControls/CantieriGruppoIspezione.ascx" TagName="CantieriGruppoIspezione" TagPrefix="uc9" %>


<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc5:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc8:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Rapporto di ispezione - Dati cantiere"
        titolo="Cantiere" />
    &nbsp;<br />

    <script type="text/javascript"> 

        function onEndRequest(sender, e) 
        { 
            var err = e.get_error();
            e.set_errorHandled(true);
            if (err != null)
            {
                if ((err.name == 'Sys.WebForms.PageRequestManagerTimeoutException'
                     || err.name == 'Sys.WebForms.PageRequestManagerServerErrorException' )) 
                { 
                    window.alert('Attenzione, il server non risponde o la connessione è caduta');
                }
                else
                {
                    window.alert('Attenzione, errore generico');
                }
            }
        } 

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest); 

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="standardTable">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Rapporto di ispezione"></asp:Label>
                        <uc9:CantieriGruppoIspezione ID="CantieriGruppoIspezione1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Segnalazione pervenuta da
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListSegnalazionePervenuta" runat="server" Width="300px"
                            AppendDataBoundItems="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Stato dell'ispezione*
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListStatoIspezione" runat="server" Width="300px" AppendDataBoundItems="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator
                            ID="RequiredFieldValidatorStatoIspezione"
                            runat="server"
                            ErrorMessage="Selezionare lo stato dell'ispezione"
                            ValidationGroup="Step1" 
                            CssClass="messaggiErrore"
                            ControlToValidate="DropDownListStatoIspezione">
                            *
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Avanzamento lavori alla data dell'ispezione %
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxAvanzamentoLavori" runat="server" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:CompareValidator
                            ID="CompareValidatorAvanzamento"
                            runat="server"
                            ErrorMessage="L'avanzamento lavori deve essere un numero"
                            ValidationGroup="Step1" 
                            CssClass="messaggiErrore"
                            ControlToValidate="TextBoxAvanzamentoLavori"
                            Operator="DataTypeCheck"
                            Type="Integer">
                            *
                        </asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <center>
                            <asp:Label ID="Label1" runat="server" Text="Anagrafica Cantiere" Font-Bold="True"></asp:Label>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Indirizzo</b>*
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc10:IscrizioneLavoratoreIndirizzo ID="IscrizioneLavoratoreIndirizzo1" 
                            runat="server" />
                    </td>
                    <td>
                        <asp:CustomValidator
                            ID="CustomValidatorIndirizzo"
                            runat="server"
                            ErrorMessage="Inserire un indirizzo"
                            ValidationGroup="Step1" 
                            onservervalidate="CustomValidatorIndirizzo_ServerValidate"
                            CssClass="messaggiErrore">
                            *
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Date e numeri</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Data inizio lavori (gg/mm/aaaa)
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereDataInizioLavori" runat="server" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizioLavori" runat="server"
                            ControlToValidate="TextBoxCantiereDataInizioLavori" ErrorMessage="Data inizio lavori non corretta"
                            ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                            ValidationGroup="Step1">
                            *
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Data fine lavori (gg/mm/aaaa)
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereDataFineLavori" runat="server" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFineLavori" runat="server"
                            ControlToValidate="TextBoxCantiereDataFineLavori" ErrorMessage="Data fine lavori non corretta"
                            ValidationExpression="(((0[1-9]|[12][0-9]|3[01])([/])(0[13578]|10|12)([/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([/])(0[469]|11)([/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([/])(02)([/])(\d{4}))|((29)(\.|-|\/)(02)([/])([02468][048]00))|((29)([/])(02)([/])([13579][26]00))|((29)([/])(02)([/])([0-9][0-9][0][48]))|((29)([/])(02)([/])([0-9][0-9][2468][048]))|((29)([/])(02)([/])([0-9][0-9][13579][26])))"
                            ValidationGroup="Step1">
                            *
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Importo
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereImporto" runat="server" Width="300px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorImporto" runat="server"
                            ControlToValidate="TextBoxCantiereImporto" ErrorMessage="Formato importo non valido"
                            ValidationExpression="^[.][0-9]+$|[0-9]*[,]*[0-9]{0,2}$" ValidationGroup="Step1">
                            *
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Permesso costruire / DIA
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantierePermessoCostruire" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <b>Altro</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tipologia appalto
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListCantiereTipologiaAppalto" runat="server" Width="300px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:CustomValidator
                            ID="CustomValidatorTipologiaAppalto"
                            runat="server"
                            ValidationGroup="Step1"
                            ErrorMessage="Selezionare la tipologia di appalto" 
                            onservervalidate="CustomValidatorTipologiaAppalto_ServerValidate">
                            *
                        </asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Attivo
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBoxCantiereAttivo" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Direzione lavori
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereDirezioneLavori" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Responsabile procedimento
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereResponsabileProcedimento" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Responsabile cantiere
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereResponsabileCantiere" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Descrizione lavori
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxCantiereDescrizioneLavori" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <center>
                            <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Anagrafica committente"></asp:Label>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td>
                        Committente
                    </td>
                    <td>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:TextBox ID="TextBoxCommittente" runat="server" Text="" Width="353px" Height="90px"
                                        TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaCommittente" runat="server" Text="Modifica dati" OnClick="ButtonModificaCommittente_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="ButtonSelezionaCommittente" runat="server" Text="Cambia committente"
                                        OnClick="ButtonSelezionaCommittente_Click" Width="200px" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdateProgress ID="UpdateProgressCommittente" runat="server">
                            <ProgressTemplate>
                                <center>
                                    <asp:Image ID="ImageCommittenteProgress" runat="server" ImageUrl="../images/loading6b.gif" /><asp:Label
                                        ID="LabelCommittenteProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                                        Text="In elaborazione...."></asp:Label>
                                </center>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel ID="UpdatePanelCommittente" runat="server">
                            <ContentTemplate>
                                <uc3:CantieriRicercaCommittente ID="CantieriRicercaCommittente1" runat="server" Visible="false" />
                                <br />
                                <asp:Panel ID="PanelInserimentoCommittente" runat="server" Width="100%"
                                    Visible="False">
                                    <table class="filledtable">
                                        <tr>
                                            <td >
                                                <asp:Label ID="LabelTitoloInsComm" runat="server" Font-Bold="True" ForeColor="White"
                                                    Text="Inserisci Committente"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="borderedTable">
                                        <tr>
                                            <td>
                                                <uc6:CantieriCommittente ID="CantieriCommittente1" runat="server" />
                                                <asp:Button ID="ButtonInserisciCommittente" runat="server" Text="Inserisci committente"
                                                    OnClick="ButtonInserisciCommittente_Click" Width="170px" />
                                                <asp:Button ID="ButtonAnnullaInserimentoCommittente" runat="server" Text="Annulla"
                                                    Width="170px" OnClick="ButtonAnnullaInserimentoCommittente_Click" CausesValidation="False" /><br />
                                                <asp:Label ID="LabelInserimentoCommittenteRes" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <center>
                            <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Anagrafica impresa appaltatrice"></asp:Label>
                        </center>
                    </td>
                </tr>
                <tr>
                    <td>
                        Impresa appaltatrice
                    </td>
                    <td>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:RadioButton ID="RadioButtonImpresaSiceInfo" runat="server" Text="SiceNew" Enabled="False"
                                        GroupName="scelta" />
                                    <asp:RadioButton ID="RadioButtonImpresaCantieri" runat="server" Text="Cantieri" Enabled="False"
                                        GroupName="scelta" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="TextBoxImpresaAppaltatrice" runat="server" Text="" Width="353px"
                                        Height="90px" TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                </td>
                                <td>
                                    <asp:Button ID="ButtonModificaImpresa" runat="server" Text="Modifica dati" Enabled="False"
                                        OnClick="ButtonModificaImpresa_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    &nbsp;<asp:Button ID="ButtonSelezionaAppaltatrice" runat="server" Text="Modifica impresa appaltatrice"
                                        OnClick="ButtonSelezionaAppaltatrice_Click" Width="200px" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdateProgress ID="UpdateProgressImpresa" runat="server">
                            <ProgressTemplate>
                                <center>
                                    <asp:Image ID="ImageImpresaProgress" runat="server" ImageUrl="../images/loading6b.gif" /><asp:Label
                                        ID="LabelImpresaProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                                        Text="In elaborazione...."></asp:Label>
                                </center>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:UpdatePanel ID="UpdatePanelImpresa" runat="server">
                            <ContentTemplate>
                                <uc4:CantieriRicercaImpresaUnicaFonte ID="CantieriRicercaImpresa1" runat="server"
                                    Visible="false" />
                                <br />
                                <asp:Panel ID="PanelInserimentoImpresa" runat="server" Width="100%"
                                    Visible="False">
                                    <table class="filledtable">
                                        <tr>
                                            <td >
                                                <asp:Label ID="LabelTitoloInserimentoImpresa" runat="server" Font-Bold="True" ForeColor="White"
                                                    Text="Inserisci Impresa"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="borderedTable">
                                        <tr>
                                            <td>
                                                <uc7:CantieriImpresa ID="CantieriImpresa1" runat="server" />
                                                <asp:Button ID="ButtonInserisciImpresa" runat="server" OnClick="ButtonInserisciImpresa_Click"
                                                    Text="Inserisci impresa" Width="170px" />
                                                <asp:Button ID="ButtonAnnullaInserimentoImpresa" runat="server" OnClick="ButtonAnnullaInserimentoImpresa_Click"
                                                    Text="Annulla" Width="170px" CausesValidation="False" />
                                                <asp:Label ID="LabelInserimentoImpresaRes" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        Tipo
                    </td>
                    <td>
                        <table class="standardTable">
                            <tr>
                                <td>
                                    <asp:RadioButton ID="RadioButtonDefault" runat="server" GroupName="scelta" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownListTipoImpresa" runat="server" Width="300px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="RadioButtonAltro" runat="server" GroupName="scelta" Text="Altro" />
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxTipoImpresa" runat="server" MaxLength="50" Width="300px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:Button ID="ButtonSalva" runat="server" OnClick="ButtonSalva_Click" Text="Salva e vai al passo 2"
        Width="170px" ValidationGroup="Step1" />
    <asp:Button ID="ButtonAnnulla" runat="server" CausesValidation="False" OnClick="ButtonAnnulla_Click"
        Text="Annulla" Width="170px" /><br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="Step1" CssClass="messaggiErrore" />
    <br />
    <asp:Label ID="LabelRisultato" runat="server" ForeColor="Red"></asp:Label><br />
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainPage2">
    <uc2:CantieriTestataIspezione ID="CantieriTestataIspezione1" runat="server" />
</asp:Content>

