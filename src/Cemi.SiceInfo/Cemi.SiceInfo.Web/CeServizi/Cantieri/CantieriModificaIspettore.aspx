﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriModificaIspettore.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriModificaIspettore" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc4" %>

<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="WebControls/RegistrazioneIspettoreModifica.ascx" TagName="RegistrazioneIspettoreModifica"
    TagPrefix="uc3" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Modifica ispettori"/>
    <br />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco ispettori"></asp:Label><br />
    <asp:GridView ID="GridViewIspettori" runat="server" AutoGenerateColumns="False" DataKeyNames="IdUtente"
        OnRowUpdating="GridViewIspettori_RowUpdating"
        Width="100%">
        <Columns>
            <asp:BoundField DataField="username" HeaderText="Username" />
            <asp:BoundField DataField="cognome" HeaderText="Cognome" />
            <asp:BoundField DataField="nome" HeaderText="Nome" />
            <asp:BoundField DataField="nomeZona" HeaderText="Zona" />
            <asp:CheckBoxField DataField="operativo" HeaderText="Operativo" >
                <ItemStyle Width="50px" />
            </asp:CheckBoxField>
            <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" 
                CommandName="Update" Text="Modifica" >
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>
                <ItemStyle Width="10px" />
            </asp:ButtonField>
        </Columns>
        <EmptyDataTemplate>
            <br />
            Nessun ispettore presente nel sistema.
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    
    <asp:Label ID="LabelModificaIspettore" runat="server" Font-Bold="True" Text="Modifica ispettore:"></asp:Label>
    <uc3:RegistrazioneIspettoreModifica id="RegistrazioneIspettoreModifica1" runat="server" Visible="false">
    </uc3:RegistrazioneIspettoreModifica>
    <br />
    &nbsp;<asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
        Width="179px" />
</asp:Content>

