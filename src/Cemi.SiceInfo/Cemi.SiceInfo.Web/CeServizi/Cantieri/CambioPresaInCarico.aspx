﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="CambioPresaInCarico.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CambioPresaInCarico" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="standardDiv">
        <telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
        </telerik:RadScriptManager>
        <table class="standardTable">
            <tr>
                <td>
                    Titolare:
                </td>
                <td>
                    <b>
                        <asp:Label 
                            ID="LabelTitolare"
                            runat="server" />
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    Altri ispettori:
                </td>
                <td>
                    <telerik:RadGrid
                        ID="RadGridIspettori"
                        runat="server"
                        AutoGenerateColumns="False"
                        Width="100%" GridLines="None" ShowHeader="False" 
                        onitemcommand="RadGridIspettori_ItemCommand">
                        <MasterTableView DataKeyNames="IdIspettore">
                            <NoRecordsTemplate>
                                Nessun altro ispettore correlato
                            </NoRecordsTemplate>
                            <Columns>
                                <telerik:GridBoundColumn DataField="NomeCompleto" UniqueName="nomecompleto">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="promuovi" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Button ID="ButtonPromuovi" runat="server" CausesValidation="false" CommandName="PROMUOVI" 
                                            Text="Rendi titolare" Width="100px" />
                                    </ItemTemplate>
                                    <ItemStyle Width="50px"></ItemStyle>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

