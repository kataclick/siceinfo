﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriGestioneCantieri.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriGestioneCantieri"  %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc4" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="WebControls/CantieriRicercaCantiere.ascx" TagName="CantieriRicercaCantiere" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register src="WebControls/LegendaGestioneCantieri.ascx" tagname="LegendaGestioneCantieri" tagprefix="uc5" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc4:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione cantieri"
        titolo="Cantieri" />
    <br />
    Imposta i filtri di ricerca e premi "Ricerca" per trovare i cantieri già inseriti
    oppure inserisci
    <asp:Button ID="ButtonNuovoCantiere" runat="server" OnClick="ButtonNuovoCantiere_Click"
        Text="Nuovo cantiere" />.<br />
    <br />
    <asp:Panel ID="PanelRicerca" runat="server" Width="1000px">
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Ricerca Cantieri"></asp:Label><br />
    <asp:UpdateProgress ID="UpdateProgressRicercaCantiere" runat="server" AssociatedUpdatePanelID="UpdatePanelRicercaCantiere"
        DisplayAfter="100">
        <ProgressTemplate>
            <center>
                <asp:Image ID="ImageCommittenteProgress" runat="server" ImageUrl="../images/loading6b.gif" />
                <asp:Label ID="LabelCommittenteProgress" runat="server" Font-Bold="True" Font-Size="Medium"
                    Text="In elaborazione...."></asp:Label>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanelRicercaCantiere" runat="server">
        <ContentTemplate>
            <uc2:CantieriRicercaCantiere ID="CantieriRicercaCantiere1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <uc5:LegendaGestioneCantieri ID="LegendaGestioneCantieri1" runat="server" />
    <br />
    </asp:Panel>
</asp:Content>
