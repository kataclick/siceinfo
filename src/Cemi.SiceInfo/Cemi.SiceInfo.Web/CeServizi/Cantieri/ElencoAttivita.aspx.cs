﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Domain;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class ElencoAttivita : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly BusinessEF bizEF = new BusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestioneAttivita);
            #endregion

            if (!Page.IsPostBack)
            {
                RadSchedulerAttivita.SelectedDate = DateTime.Now;
                CaricaCalendarioAttivita();
                CaricaIspettori();
            }
        }

        protected void CaricaIspettori()
        {
            IspettoreCollection ispettori = biz.GetIspettori(null, true);

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                RadComboBoxIspettore,
                ispettori,
                "NomeCompleto",
                "IdIspettore");
        }

        private void CaricaCalendarioAttivita()
        {
            Int32? idIspettore = null;

            if (!String.IsNullOrEmpty(RadComboBoxIspettore.SelectedValue))
            {
                idIspettore = Int32.Parse(RadComboBoxIspettore.SelectedValue);
            }

            DateTime inizio = RadSchedulerAttivita.VisibleRangeStart;
            DateTime fine = RadSchedulerAttivita.VisibleRangeEnd;
            List<CantieriCalendarioAttivita> calendarioAttivita = bizEF.GetCalendarioAttivita(idIspettore, inizio, fine);

            RadSchedulerAttivita.DataSource = calendarioAttivita;
            RadSchedulerAttivita.DataBind();
        }

        protected void RadSchedulerAttivita_AppointmentDataBound(object sender, Telerik.Web.UI.SchedulerEventArgs e)
        {
            CantieriCalendarioAttivita attivita = (CantieriCalendarioAttivita)e.Appointment.DataItem;

            e.Appointment.AllowDelete = false;
            e.Appointment.AllowEdit = false;

            e.Appointment.ToolTip = attivita.DescrizioneAttivitaPerElencoCompletoNonHtml;

            switch (attivita.TipologiaAttivita.Id)
            {
                case 1:
                    // Ispezione

                    // Se esiste rapporto visualizzo stato
                    if (attivita.CantieriCantieri != null && attivita.CantieriCantieri.Count == 1)
                    {
                        IspezioniFilter filtro = new IspezioniFilter();
                        filtro.IdCantiere = attivita.CantieriCantieri.Single().IdCantiere;
                        RapportoIspezioneCollection ispezioni =
                            biz.GetIspezioni(filtro);

                        if (ispezioni != null && ispezioni.Count == 1)
                        {
                            e.Appointment.BackColor = ColoriAttivita.SfondoIspezioneConsiderata;

                            if (ispezioni[0].Stato.HasValue)
                            {
                                e.Appointment.Description =
                                    attivita.DescrizioneAttivitaPerElencoCompletoConRapportoIspezione(
                                        ispezioni[0].Stato.ToString());
                            }
                        }
                        else
                        {
                            if (attivita.Rifiuto != null)
                            {
                                e.Appointment.BackColor = ColoriAttivita.SfondoIspezioneConsiderata;

                                e.Appointment.Description =
                                    attivita.DescrizioneAttivitaPerElencoCompletoConRifiuto(attivita.Rifiuto.Descrizione);
                            }
                            else
                            {
                                e.Appointment.BackColor = ColoriAttivita.SfondoIspezione;
                            }
                        }
                    }
                    break;
                case 2:
                    // Appuntamenti
                    e.Appointment.BackColor = ColoriAttivita.SfondoAppuntamento;
                    break;
                case 3:
                    // Attività di Back office
                    e.Appointment.BackColor = ColoriAttivita.SfondoBackOffice;
                    break;
                case 4:
                    // Varie
                    e.Appointment.BackColor = ColoriAttivita.SfondoVarie;
                    break;
            }
        }

        protected void RadSchedulerAttivita_NavigationComplete(object sender, Telerik.Web.UI.SchedulerNavigationCompleteEventArgs e)
        {
            CaricaCalendarioAttivita();
        }

        protected void RadComboBoxIspettore_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CaricaCalendarioAttivita();
        }
    }
}