﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Domain;
using System.Collections.Generic;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Cantiere = TBridge.Cemi.Type.Entities.Cantieri.Cantiere;


namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriConfermaEliminazioneCantiere : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);

            if (Request.QueryString["idCantiere"] != null)
            {
                int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);
                Cantiere cantiere = biz.GetCantiere(idCantiere);

                AttivitaCollection attivita = biz.GetProgrammazionePerCantiere(idCantiere);

                Presenter.CaricaElementiInGridView(
                    GridViewAttivita,
                    attivita,
                    0);

                if (cantiere.PresaInCarico != null)
                {
                    List<CantieriCalendarioAttivita> nuoveAttivita = new List<CantieriCalendarioAttivita>();
                    nuoveAttivita.Add(cantiere.PresaInCarico);

                    Presenter.CaricaElementiInGridView(
                        GridViewAttivitaNuove,
                        nuoveAttivita,
                        0);

                    if (nuoveAttivita != null && nuoveAttivita.Count > 0)
                    {
                        ButtonCancella.Enabled = false;
                    }
                }

                if (attivita != null && attivita.Count > 0)
                {
                    ButtonCancella.Enabled = false;
                }
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx");
        }

        protected void ButtonCancella_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["idCantiere"] != null)
            {
                int idCantiere = Int32.Parse(Request.QueryString["idCantiere"]);

                if (biz.DeleteCantiere(idCantiere))
                    Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx");
                else
                    LabelErrore.Text = "Errore durante la cancellazione";
            }
        }
    }
}