﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class Subappalti : System.Web.UI.Page
    {
        private CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);
            #endregion

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdCantiere"] != null)
                {
                    Int32 idCantiere = (Int32)Context.Items["IdCantiere"];
                    SubappaltiCantiere1.CaricaCantiere(idCantiere);
                }
                else
                {
                    Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx");
                }
            }
        }

        protected void ButtonSalvaAppalti_Click(object sender, EventArgs e)
        {
            SubappaltoCollection subappalti = SubappaltiCantiere1.GetSubappalti();

            if (biz.InsertSubappalti(subappalti))
            {
                Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx");
            }
        }
    }
}