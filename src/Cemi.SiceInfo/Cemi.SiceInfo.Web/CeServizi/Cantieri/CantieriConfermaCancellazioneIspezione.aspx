﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriConfermaCancellazioneIspezione.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriConfermaCancellazioneIspezione" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Gestione cancellazione rapporto"/>
    <br />
    <asp:Label ID="LabelTesto" runat="server" Text="E' stata richiesta la cancellazione di un rapporto di ispezione. Se si conferma tutti i dati inseriti relativamente al rapporto di ispezione verranno cancellati. Si conferma l'eliminazione?"></asp:Label><br />
    <br />
    <asp:Button ID="ButtonConfermaEliminazione" runat="server" OnClick="ButtonConfermaEliminazione_Click"
        Text="Conferma eliminazione" Width="200px" />
    <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla"
        Width="200px" /></asp:Content>


