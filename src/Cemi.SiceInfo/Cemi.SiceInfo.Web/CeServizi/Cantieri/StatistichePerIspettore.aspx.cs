﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class StatistichePerIspettore : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriStatistichePerIspettore);
            funzionalita.Add(FunzionalitaPredefinite.CantieriStatistichePerIspettoreRUI);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            // Fix report viewer bug in IE7
            string userAgent = Request.ServerVariables.Get("HTTP_USER_AGENT");
            if (userAgent.Contains("MSIE 7.0"))
            {
                ReportViewerAttivita.Style.Add("margin-bottom", "70px");
                //ReportViewerAttivita.Attributes.Add("style", "overflow:auto;");
            }

            if (!Page.IsPostBack)
            {
                CaricaIspettori();

                //Se l'utente è ispettore e non è rui blocchiamo la dropdown
                if (GestioneUtentiBiz.IsIspettore()
                    &&
                    !GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI))
                {
                    //Ispettore ispettore =
                    //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Ispettore) HttpContext.Current.User.Identity).
                    //        Entity;
                    Ispettore ispettore =
                        (Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    DropDownListIspettore.SelectedValue = ispettore.IdIspettore.ToString();
                    DropDownListIspettore.Enabled = false;
                }
            }
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                ReportViewerAttivita.ServerReport.ReportServerUrl =
                    new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportParameter[] listaParam;

                //string[] nomiParametri = new string[2] {"mese", "idIspettore"};

                if (RadioButtonIspezioni.Checked)
                    ReportViewerAttivita.ServerReport.ReportPath =
                        "/ReportCantieri/StatisticheRiepilogoIspezioniMensileIspettore";
                else
                    ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/StatisticheRiepilogoMensileIspettore";

                DateTime mese = DateTime.ParseExact(TextBoxMese.Text, "MM/yyyy", null);
                int idIspettore = Int32.Parse(DropDownListIspettore.SelectedValue);

                listaParam = new ReportParameter[2];
                listaParam[0] = new ReportParameter("mese", mese.ToShortDateString());
                listaParam[1] = new ReportParameter("idIspettore", idIspettore.ToString());

                ReportViewerAttivita.ServerReport.SetParameters(listaParam);
            }
        }

        private void CaricaIspettori()
        {
            DropDownListIspettore.Items.Clear();
            DropDownListIspettore.Items.Add(new ListItem(string.Empty, string.Empty));

            //Carichiamo la lista degli ispettori
            IspettoreCollection listaIspettori = biz.GetIspettori(null, true);
            DropDownListIspettore.DataSource = listaIspettori;
            DropDownListIspettore.DataTextField = "NomeCompleto";
            DropDownListIspettore.DataValueField = "IdIspettore";
            DropDownListIspettore.DataBind();
        }

        protected void CustomValidatorMese_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime data;
            if (DateTime.TryParseExact(TextBoxMese.Text, "MM/yyyy", null, System.Globalization.DateTimeStyles.None, out data))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
    }
}