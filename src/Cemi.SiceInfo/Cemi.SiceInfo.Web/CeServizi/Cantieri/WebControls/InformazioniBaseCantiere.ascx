﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InformazioniBaseCantiere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.InformazioniBaseCantiere" %>
<asp:MultiView
    ID="MultiViewCantiere"
    runat="server"
    ActiveViewIndex="0">
    <asp:View
        ID="ViewNessunCantiere"
        runat="server">
        Nessun cantiere selezionato
    </asp:View>
    <asp:View
        ID="ViewDatiCantiere"
        runat="server">
        <table class="standardTable">
            <tr>
                <td colspan="3">
                    <b>
                        <asp:Label
                            ID="LabelIndirizzo"
                            runat="server">
                        </asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label
                        ID="LabelCap"
                        runat="server">
                    </asp:Label>
                    <b>
                        <asp:Label
                            ID="LabelComune"
                            runat="server">
                        </asp:Label>
                    </b>
                    <br />
                    <asp:Label
                        ID="LabelProvincia"
                        runat="server">
                    </asp:Label>
                </td>
            </tr>
        </table>
    </asp:View>
</asp:MultiView>
