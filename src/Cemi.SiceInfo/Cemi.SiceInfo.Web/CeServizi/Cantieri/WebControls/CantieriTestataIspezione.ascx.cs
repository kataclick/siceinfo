﻿using System;
using System.Web.UI;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class CantieriTestataIspezione : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void ImpostaTestata(RapportoIspezione ispezione)
        {
            TextBoxGiorno.Text = ispezione.Giorno.ToShortDateString();
            TextBoxCantiere.Text = ispezione.Cantiere.IndirizzoMappa;
            TextBoxIspettore.Text = ispezione.Ispettore.Cognome + " " + ispezione.Ispettore.Nome;
        }
    }
}