﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using Telerik.Web.UI;
using System.Drawing;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Delegates.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Cantieri;
using Ispettore = TBridge.Cemi.Type.Entities.GestioneUtenti.Ispettore;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class CantieriRicercaIspezioni : System.Web.UI.UserControl
    {
        private const int idACCEDIRAPPORTO = 5;
        private const int idACCEDIRAPPORTOFINTO = 6;
        private const int idACCEDISTAMPARAPPORTO = 7;
        private const int idGENERAZIONELETTERE = 8;
        private const int idELIMINA = 9;
        private readonly CantieriBusiness biz = new CantieriBusiness();

        private int giorniScadenza;

        private string ispettore = null;
        private string stato = null;
        public event IspezioneSelectedEventHandler OnIspezioneSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(GridViewIspezioni);

            if (!GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere)
                && !GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrev)
                )
            {
                GridViewIspezioni.Columns[idGENERAZIONELETTERE].Visible = false;
            }

            giorniScadenza = biz.GetScadenza();

            if (!Page.IsPostBack && Request.QueryString["modalita"] != "indietro")
            {
                CaricaIspettori();
                CaricaEsiti();
            }
        }

        public void ImpostaIspettore(int idIspettore)
        {
            DropDownListIspettori.SelectedValue = idIspettore.ToString();

            if (!GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere))
            {
                DropDownListIspettori.Enabled = false;
            }
        }

        private void CaricaEsiti()
        {
            DropDownListStato.Items.Clear();
            DropDownListStato.Items.Insert(0, new ListItem(" Tutti", "0"));

            DropDownListStato.DataSource = Enum.GetNames(typeof(StatoIspezione));
            DropDownListStato.DataBind();

            if (!string.IsNullOrEmpty(stato))
                DropDownListStato.SelectedValue = stato;
        }

        private void CaricaIspettori()
        {
            DropDownListIspettori.Items.Clear();
            DropDownListIspettori.Items.Insert(0, new ListItem(" Tutti", "0"));

            IspettoreCollection listaIspettori = biz.GetIspettori(null, true);
            DropDownListIspettori.DataSource = listaIspettori;
            DropDownListIspettori.DataTextField = "NomeCompleto";
            DropDownListIspettori.DataValueField = "IdIspettore";
            DropDownListIspettori.DataBind();

            if (!String.IsNullOrEmpty(ispettore))
                DropDownListIspettori.SelectedValue = ispettore;
        }

        public void CaricaIspezioni(Int32 pagina)
        {
            IspezioniFilter filtro = new IspezioniFilter();

            if (DropDownListStato.SelectedValue != "0")
                filtro.Stato = (StatoIspezione)Enum.Parse(typeof(StatoIspezione), DropDownListStato.SelectedValue);

            if (DropDownListIspettori.SelectedValue != "0")
            {
                int idIspettoreI = Int32.Parse(DropDownListIspettori.SelectedValue);
                filtro.IdIspettore = idIspettoreI;
            }

            if (!String.IsNullOrEmpty(TextBoxDal.Text))
                filtro.Dal = DateTime.Parse(TextBoxDal.Text);
            if (!String.IsNullOrEmpty(TextBoxAl.Text))
                filtro.Al = DateTime.Parse(TextBoxAl.Text);

            filtro.Appaltatrice = Presenter.NormalizzaCampoTesto(TextBoxAppaltatrice.Text);
            filtro.SubAppaltatrice = Presenter.NormalizzaCampoTesto(TextBoxSubappaltatrice.Text);

            if (!String.IsNullOrEmpty(TextBoxIdImpresa.Text))
            {
                filtro.IdImpresa = Int32.Parse(TextBoxIdImpresa.Text);
            }

            RapportoIspezioneCollection listaIspezioni =
                biz.GetIspezioni(filtro);

            Presenter.CaricaElementiInGridView(
                GridViewIspezioni,
                listaIspezioni,
                pagina);
        }

        protected void GridViewIspezioni_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idIspezione = (int)GridViewIspezioni.DataKeys[e.NewSelectedIndex].Values["IdIspezione"];
            IspezioniFilter filtro = new IspezioniFilter();
            filtro.IdIspezione = idIspezione;
            RapportoIspezione ispezione = biz.GetIspezioni(filtro)[0];

            if (OnIspezioneSelected != null)
                OnIspezioneSelected(ispezione);
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaIspezioni(0);
            }
        }

        protected void GridViewIspezioni_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CaricaIspezioni(e.NewPageIndex);
        }

        protected void ButtonChiudi_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        protected void GridViewIspezioni_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int idIspezione = (int)GridViewIspezioni.DataKeys[e.RowIndex].Value;

            string indirizzo = string.Format(
                "CantieriIspezioneStampaReport.aspx?idIspezione={0}&modalita=ricercaIspezioni", idIspezione);
            indirizzo = ComponiUrlConFiltriRicerca(indirizzo);

            Response.Redirect(indirizzo);
        }

        protected void GridViewIspezioni_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            bool isIspettoreStandard = GestioneUtentiBiz.IsIspettore() &&
                                       GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrev.ToString())
                                       &&
                                       !GestioneUtentiBiz.Autorizzato(
                                            FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString());

            Int32 idIspettore = -1;
            if (isIspettoreStandard)
            {
                Ispettore isp = (Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
                idIspettore = isp.IdIspettore;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RapportoIspezione ispezione = (RapportoIspezione)e.Row.DataItem;
                Label lAppaltatrice = (Label)e.Row.FindControl("LabelAppaltatrice");
                System.Web.UI.WebControls.Image iBusta = (System.Web.UI.WebControls.Image)e.Row.FindControl("ImageBusta");

                if (ispezione.DataLetteraVerificaInCorso.HasValue)
                {
                    iBusta.ImageUrl = "~/CeServizi/images/icona_busta.png";
                    iBusta.ToolTip = String.Format("Lettera di \"Verifica in Corso\" generata il {0:dd/MM/yyyy}", ispezione.DataLetteraVerificaInCorso.Value);
                }
                else
                {
                    iBusta.ImageUrl = "~/CeServizi/images/icona_bustaBN.png";
                }

                //controlliamo che sia ispettore e che sia passato il limite temporale entro cui un ispettore può modificare il rapporto
                bool scaduto = isIspettoreStandard &&
                               (((TimeSpan)DateTime.Today.Subtract(ispezione.Giorno)).Days > giorniScadenza);

                if (scaduto || (isIspettoreStandard && idIspettore != ispezione.Ispettore.IdIspettore))
                {
                    GestioneCompilaIspezione(e.Row, false);

                    if (isIspettoreStandard && idIspettore != ispezione.Ispettore.IdIspettore)
                    {
                        e.Row.Cells[idACCEDISTAMPARAPPORTO].Visible = false;
                    }
                }
                else
                {
                    GestioneCompilaIspezione(e.Row, true);
                }

                if (ispezione.Cantiere.ImpresaAppaltatrice != null)
                {
                    lAppaltatrice.Text = ispezione.Cantiere.ImpresaAppaltatrice.RagioneSociale;

                    if (ispezione.Cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                        lAppaltatrice.ForeColor = Color.Gray;
                }
                else if (!string.IsNullOrEmpty(ispezione.Cantiere.ImpresaAppaltatriceTrovata))
                {
                    lAppaltatrice.Text = ispezione.Cantiere.ImpresaAppaltatriceTrovata;
                    lAppaltatrice.ForeColor = Color.Red;
                }

                if (!GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriGestione) || !GestioneUtentiBiz.IsIspettore())
                {
                    e.Row.Cells[idACCEDIRAPPORTO].Visible = false;
                    e.Row.Cells[idACCEDIRAPPORTOFINTO].Visible = false;
                    e.Row.Cells[idACCEDISTAMPARAPPORTO].Visible = false;
                    e.Row.Cells[idELIMINA].Visible = false;
                }

                if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriLettere))
                {
                    e.Row.Cells[idACCEDISTAMPARAPPORTO].Visible = true;
                }
            }
        }

        private static void GestioneCompilaIspezione(GridViewRow row, bool abilita)
        {
            row.Cells[idACCEDIRAPPORTO].Visible = abilita;
            row.Cells[idACCEDIRAPPORTO].Enabled = abilita;

            row.Cells[idACCEDIRAPPORTOFINTO].Visible = !abilita;
            row.Cells[idACCEDIRAPPORTOFINTO].Enabled = false;

            row.Cells[idELIMINA].Visible = abilita;
            row.Cells[idELIMINA].Enabled = abilita;
        }

        protected void GridViewIspezioni_RowEditing(object sender, GridViewEditEventArgs e)
        {
            int idIspezione = (int)GridViewIspezioni.DataKeys[e.NewEditIndex].Value;

            string indirizzo = string.Format("CantieriSelezioneLettera.aspx?idIspezione={0}&modalita=ricercaIspezioni",
                                             idIspezione);
            indirizzo = ComponiUrlConFiltriRicerca(indirizzo);

            Response.Redirect(indirizzo);
        }

        private string ComponiUrlConFiltriRicerca(string url)
        {
            StringBuilder urlRes = new StringBuilder();

            string dal = TextBoxDal.Text;
            string al = TextBoxAl.Text;
            string ispettore = DropDownListIspettori.SelectedValue;
            string stato = DropDownListStato.SelectedValue;
            string detAppalto = TextBoxAppaltatrice.Text;
            string subApp = TextBoxSubappaltatrice.Text;

            urlRes.Append(url);
            if (!string.IsNullOrEmpty(dal))
                urlRes.Append(String.Format("&dal={0}", dal));
            if (!string.IsNullOrEmpty(al))
                urlRes.Append(String.Format("&al={0}", al));
            if (!string.IsNullOrEmpty(ispettore))
                urlRes.Append(String.Format("&ispettore={0}", ispettore));
            if (!string.IsNullOrEmpty(stato))
                urlRes.Append(String.Format("&stato={0}", stato));
            if (!string.IsNullOrEmpty(detAppalto))
                urlRes.Append(String.Format("&detAppalto={0}", detAppalto));
            if (!string.IsNullOrEmpty(subApp))
                urlRes.Append(String.Format("&subApp={0}", subApp));
            urlRes.Append(String.Format("&pagina={0}", GridViewIspezioni.PageIndex.ToString()));

            return urlRes.ToString();
        }

        public void CaricaFiltri(string dal, string al, string ispettore, string stato, string detAppalto, string subApp,
                                 int? pagina)
        {
            if (!string.IsNullOrEmpty(dal))
                TextBoxDal.Text = dal;
            if (!string.IsNullOrEmpty(al))
                TextBoxAl.Text = al;
            if (!string.IsNullOrEmpty(ispettore))
                this.ispettore = ispettore;
            if (!string.IsNullOrEmpty(stato))
                this.stato = stato;
            if (!string.IsNullOrEmpty(detAppalto))
                TextBoxAppaltatrice.Text = detAppalto;
            if (!string.IsNullOrEmpty(subApp))
                TextBoxSubappaltatrice.Text = subApp;

            if (pagina.HasValue)
                GridViewIspezioni.PageIndex = pagina.Value;

            CaricaIspettori();
            CaricaEsiti();
        }

        public void ForzaRicerca()
        {
            CaricaIspezioni(GridViewIspezioni.PageIndex);
        }

        protected void GridViewIspezioni_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Int32 indice = -1;

            switch (e.CommandName)
            {
                case "elimina":
                    indice = Int32.Parse(e.CommandArgument.ToString());
                    Context.Items["IdIspezione"] = (Int32)GridViewIspezioni.DataKeys[indice].Value;
                    Server.Transfer("~/CeServizi/Cantieri/ConfermaCancellazioneIspezione.aspx");
                    break;
            }
        }
    }
}