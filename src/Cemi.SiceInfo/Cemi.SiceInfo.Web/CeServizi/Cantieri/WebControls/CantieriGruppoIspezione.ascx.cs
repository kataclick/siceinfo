﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class CantieriGruppoIspezione : System.Web.UI.UserControl
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        public IspettoreCollection GruppoIspezione
        {
            get { return RecuperaGruppoDaViewState(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                CaricaGruppoDaViewState();
            }
        }

        public void CaricaGruppoIspezione(IspettoreCollection gruppo, int idIspettore)
        {
            GridViewGruppoIspezione.DataSource = gruppo;
            GridViewGruppoIspezione.DataBind();
            ViewState["gruppoIspezione"] = gruppo;

            CaricaIspettoriDropDown(gruppo, idIspettore);
        }

        private void CaricaGruppoDaViewState()
        {
            IspettoreCollection gruppo = RecuperaGruppoDaViewState();
            GridViewGruppoIspezione.DataSource = gruppo;
            GridViewGruppoIspezione.DataBind();
        }

        private IspettoreCollection RecuperaGruppoDaViewState()
        {
            IspettoreCollection gruppo = null;

            if (ViewState["gruppoIspezione"] == null)
                gruppo = new IspettoreCollection();
            else
                gruppo = (IspettoreCollection)ViewState["gruppoIspezione"];

            return gruppo;
        }

        private void CaricaIspettoriDropDown(IspettoreCollection gruppo, int idIspettore)
        {
            IspettoreCollection ispettori = biz.GetIspettori(null, true);
            IspettoreCollection ispettoriDaBindare = new IspettoreCollection();

            foreach (Ispettore ispettore in ispettori)
            {
                if (ispettore.IdIspettore.Value != idIspettore
                    && !gruppo.Contains(ispettore))
                    ispettoriDaBindare.Add(ispettore);
            }

            DropDownListIspettori.DataTextField = "NomeCompleto";
            DropDownListIspettori.DataValueField = "IdIspettore";
            DropDownListIspettori.DataSource = ispettoriDaBindare;
            DropDownListIspettori.DataBind();
        }

        protected void ButtonAggiungi_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DropDownListIspettori.SelectedValue))
            {
                IspettoreCollection gruppo = RecuperaGruppoDaViewState();

                Ispettore ispettore = new Ispettore();
                ispettore.IdIspettore = Int32.Parse(DropDownListIspettori.SelectedItem.Value);
                ispettore.Cognome = DropDownListIspettori.SelectedItem.Text;
                if (!gruppo.Contains(ispettore))
                {
                    gruppo.Add(ispettore);
                }

                ViewState["gruppoIspezione"] = gruppo;
                CaricaGruppoDaViewState();
            }
        }

        protected void GridViewGruppoIspezione_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            IspettoreCollection gruppo = RecuperaGruppoDaViewState();
            gruppo.RemoveAt(e.RowIndex);
            ViewState["gruppoIspezione"] = gruppo;
            CaricaGruppoDaViewState();
        }
    }
}