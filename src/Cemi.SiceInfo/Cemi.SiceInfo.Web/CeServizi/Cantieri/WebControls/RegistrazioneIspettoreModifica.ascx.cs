﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class RegistrazioneIspettoreModifica : System.Web.UI.UserControl
    {
        private readonly CantieriBusiness bizCA = new CantieriBusiness();
        private GestioneUtentiBiz gu;

        //private Ispettore ispettore = null;

        public Ispettore Ispettore
        {
            set
            {
                //ispettore = value;
                ViewState["ispettore"] = value;

                if (value != null)
                {
                    ButtonRegistraIspettore.Enabled = true;
                    CheckBoxAttivo.Enabled = true;
                    DropDownListZonaCantiere.Enabled = true;
                    TextBoxCellulare.Enabled = true;

                    LabelCognome.Text = value.Cognome;
                    LabelNome.Text = value.Nome;
                    LabelLogin.Text = value.UserName;
                    TextBoxCellulare.Text = value.Cellulare;
                    CheckBoxAttivo.Checked = value.Operativo;
                    DropDownListZonaCantiere.SelectedValue = value.IdCantieriZona.ToString();
                }
                else
                {
                    //
                    LabelResult.Text = string.Empty;

                    LabelCognome.Text = string.Empty;
                    LabelNome.Text = string.Empty;
                    LabelLogin.Text = string.Empty;
                    CheckBoxAttivo.Checked = true;

                    ButtonRegistraIspettore.Enabled = false;
                    CheckBoxAttivo.Enabled = false;
                    DropDownListZonaCantiere.Enabled = false;
                    TextBoxCellulare.Enabled = false;
                }
            }
            get { return (Ispettore)ViewState["ispettore"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ButtonRegistraIspettore.Enabled = false;

            gu = new GestioneUtentiBiz();

            if (!Page.IsPostBack)
                CaricaZone();
        }

        private void CaricaZone()
        {
            ZonaCollection listaZone = bizCA.GetZone(null);

            DropDownListZonaCantiere.Items.Clear();
            DropDownListZonaCantiere.Items.Insert(0, new ListItem(" Tutti", "0"));

            DropDownListZonaCantiere.DataSource = listaZone;
            DropDownListZonaCantiere.DataTextField = "Nome";
            DropDownListZonaCantiere.DataValueField = "IdZona";

            DropDownListZonaCantiere.DataBind();
        }

        protected void ButtonModificaIspettore_Click(object sender, EventArgs e)
        {
            if (DropDownListZonaCantiere.SelectedValue != string.Empty)
            {
                Ispettore ispettore = Ispettore;
                if (ispettore != null)
                {
                    ispettore.IdCantieriZona = int.Parse(DropDownListZonaCantiere.SelectedValue);
                    ispettore.Operativo = CheckBoxAttivo.Checked;
                    ispettore.Cellulare = TextBoxCellulare.Text.Trim();

                    //Inseriamo il dipendente nel sistema

                    //switch (gu.ModificaIspettore(ispettore))
                    //{
                    //    case ErroriRegistrazione.RegistrazioneEffettuata:
                    //        LabelNome.Text = string.Empty;
                    //        LabelCognome.Text = string.Empty;
                    //        CheckBoxAttivo.Checked = true;
                    //        LabelLogin.Text = string.Empty;
                    //        TextBoxCellulare.Text = string.Empty;

                    //        LabelResult.Text = "Registrazione completata";

                    //        Response.Redirect("GestioneUtentiRegistrazioneAvvenuta.aspx");
                    //        break;
                    //    case ErroriRegistrazione.ChallengeNonPassato:
                    //        LabelResult.Text = "I dati inseriti non sono corretti. Correggerli e riprovare";
                    //        break;
                    //    case ErroriRegistrazione.LoginPresente:
                    //        LabelResult.Text =
                    //            "La login scelta è già presente nel sistema, sceglierne un'altra e riprovare.";
                    //        break;
                    //    case ErroriRegistrazione.RegistrazioneGiaPresente:
                    //        LabelResult.Text = "La registrazione risulta come già effettuata.";
                    //        break;
                    //    case ErroriRegistrazione.Errore:
                    //        LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare più tardi.";
                    //        break;
                    //    case ErroriRegistrazione.LoginNonCorretta:
                    //        LabelResult.Text =
                    //            "La username fornita contiene caratteri non validi o la sua lunghezza non è compresa fra 5 e 15 caratteri.";
                    //        break;
                    //    case ErroriRegistrazione.PasswordNonCorretta:
                    //        LabelResult.Text =
                    //            "La password deve contenere almeno una lettera e un numero e la sua lunghezza deve essere compresa tra 8 e 15 caratteri.";
                    //        break;
                    //    default:
                    //        LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare più tardi.";
                    //        break;
                    //}

                    #region
                    if (gu.ModificaIspettore(ispettore))
                    {
                        LabelNome.Text = string.Empty;
                        LabelCognome.Text = string.Empty;
                        CheckBoxAttivo.Checked = true;
                        LabelLogin.Text = string.Empty;
                        TextBoxCellulare.Text = string.Empty;

                        Response.Redirect("CantieriModificaIspettore.aspx");
                        //this.Visible = false;
                    }
                    else
                    {
                        LabelResult.Text = "La registrazione non è andata a buon fine. Riprovare.";
                    }
                    #endregion
                }
                else
                {
                    LabelResult.Text = "Errore durante l'aggiornamento.";
                }
            }
            else
            {
                LabelResult.Text = "Correggi i campi contrassegnati da un asterisco";
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            LabelResult.Text = string.Empty;
            Ispettore = null;
            Visible = false;
        }
    }
}