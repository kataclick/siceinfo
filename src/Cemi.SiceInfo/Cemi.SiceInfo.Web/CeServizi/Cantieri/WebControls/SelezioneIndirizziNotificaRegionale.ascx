﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelezioneIndirizziNotificaRegionale.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.SelezioneIndirizziNotificaRegionale" %>

<div class="standardDiv">
    <b>
        Indirizzo su cui creare il cantiere
    </b>
    <asp:RadioButtonList ID="RadioButtonListIndirizzi" runat="server">
    </asp:RadioButtonList>
</div>