﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class SegnalazioneDati : System.Web.UI.UserControl
    {
        private readonly BusinessEF bizEF = new BusinessEF();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CaricaMotivazioni();
                CaricaPriorita();
            }
        }

        private void CaricaPriorita()
        {
            if (RadComboBoxPriorita.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    RadComboBoxPriorita,
                    bizEF.GetSegnalazioniPriorita(),
                    "Descrizione",
                    "Id");
            }
        }

        private void CaricaMotivazioni()
        {
            if (RadComboBoxMotivazione.Items.Count == 0)
            {
                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    RadComboBoxMotivazione,
                    bizEF.GetSegnalazioneMotivazioni(),
                    "Descrizione",
                    "Id");
            }
        }

        public void Abilita(Boolean abilita)
        {
            CheckBoxRicorrente.Enabled = abilita;
            RadComboBoxMotivazione.Enabled = abilita;
            RadComboBoxPriorita.Enabled = abilita;
            RadTextBoxNote.Enabled = abilita;
        }

        public void CaricaSegnalazione(Cantiere cantiere)
        {
            CaricaPriorita();
            CaricaMotivazioni();

            ViewState["IdSegnalazione"] = cantiere.Segnalazione.IdSegnalazione;
            LabelData.Text = cantiere.Segnalazione.Data.ToString("dd/MM/yyyy");
            LabelUtente.Text = cantiere.Segnalazione.NomeUtente;
            CheckBoxRicorrente.Checked = cantiere.Segnalazione.Ricorrente;
            RadComboBoxMotivazione.SelectedValue = cantiere.Segnalazione.Motivazione.Id.ToString();
            RadComboBoxPriorita.SelectedValue = cantiere.Segnalazione.Priorita.Id.ToString();
            RadTextBoxNote.Text = cantiere.Segnalazione.Note;
        }

        public void CaricaDatiIniziali()
        {
            LabelData.Text = DateTime.Now.ToString("dd/MM/yyyy");
            LabelUtente.Text = GestioneUtentiBiz.GetNomeUtente();
        }

        public Segnalazione CreaSegnalazione()
        {
            Segnalazione segnalazione = new Segnalazione();

            if (ViewState["IdSegnalazione"] != null)
            {
                segnalazione.IdSegnalazione = (Int32)ViewState["IdSegnalazione"];
            }
            else
            {
                segnalazione.IdSegnalazione = -1;
            }
            segnalazione.Data = DateTime.Now;
            segnalazione.Ricorrente = CheckBoxRicorrente.Checked;
            segnalazione.Motivazione = new TBridge.Cemi.Type.Domain.CantiereSegnalazioneMotivazione();
            segnalazione.Motivazione.Id = Int32.Parse(RadComboBoxMotivazione.SelectedItem.Value);
            segnalazione.Motivazione.Descrizione = RadComboBoxMotivazione.SelectedItem.Text;
            segnalazione.Priorita = new TBridge.Cemi.Type.Domain.CantiereSegnalazionePriorita();
            segnalazione.Priorita.Id = Int32.Parse(RadComboBoxPriorita.SelectedItem.Value);
            segnalazione.Priorita.Descrizione = RadComboBoxPriorita.SelectedItem.Text;
            segnalazione.Note = Presenter.NormalizzaCampoTesto(RadTextBoxNote.Text);

            // Utente che effettua la segnalazione
            segnalazione.IdUtente = GestioneUtentiBiz.GetIdUtente();

            return segnalazione;
        }

    }
}