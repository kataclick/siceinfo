﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegendaColoriAttivita.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.LegendaColoriAttivita" %>
<table class="standardTable">
    <tr>
        <td colspan="4">
            <b>
                Legenda
            </b>
        </td>
    </tr>
    <tr>
        <td style="width:25%;">
            <asp:Panel
                ID="PanelIspezioniDaConsiderare"
                runat="server"
                Height="20px"
                Width="20px">
            </asp:Panel>
            Ispezioni da considerare
        </td>
        <td style="width:25%">
            <asp:Panel
                ID="PanelAppuntamenti"
                runat="server"
                Height="20px"
                Width="20px">
            </asp:Panel>
            Appuntamenti
        </td>
        <td style="width:25%">
            <asp:Panel
                ID="PanelAttivita"
                runat="server"
                Height="20px"
                Width="20px">
            </asp:Panel>
            Attività di Back Office
        </td>
        <td style="width:25%">
            <asp:Panel
                ID="PanelVarie"
                runat="server"
                Height="20px"
                Width="20px">
            </asp:Panel>
            Varie
        </td>
    </tr>
    <tr>
        <td style="width:25%;">
            <asp:Panel
                ID="PanelIspezioniConsiderate"
                runat="server"
                Height="20px"
                Width="20px">
            </asp:Panel>
            Ispezioni già considerate
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
    </tr>
    <tr>
        <td style="width:25%;">
            <asp:Panel
                ID="PanelIspezioniFantasma"
                runat="server"
                Height="20px"
                Width="20px">
            </asp:Panel>
            Ispezioni assegnate ma di cui non si fa parte
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
        <td style="width:25%">
        </td>
    </tr>
</table>