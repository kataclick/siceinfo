﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CantiereDaNotificaRegionale.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.CantiereDaNotificaRegionale" %>
<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openRadWindowGeneraCantiere(protocolloRegionale, segnalazione) {
            var oWindow = radopen("../Cantieri/GeneraCantiereRegionale.aspx?protocolloRegionale=" + protocolloRegionale + "&segnalazione=" + segnalazione, null);
            oWindow.set_title("Generazione cantiere");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(500, 450);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="../../images/favicon.ico" />

<asp:Panel
    ID="PanelGlobale"
    runat="server"
    BorderStyle="Solid"
    BorderWidth="1px"
    BorderColor="Gray"
    Width="100%">
    <b>
        Cantieri generati
    </b>
    <div class="standardDiv">
        <asp:Button
            ID="ButtonGenera"
            runat="server"
            Text="Genera"
            Width="120px"
            CommandName="generaCantiere" />
        &nbsp;
        <asp:Button
            ID="ButtonGeneraSegnala"
            runat="server"
            Text="Genera e segnala"
            Width="120px"
            CommandName="generaSegnalaCantiere" />
    </div>
    <div class="standardDiv">
        <asp:GridView
            ID="GridViewCantieri"
            runat="server"
            Width="100%" 
            AutoGenerateColumns="False" 
            DataKeyNames="IdCantiere" 
            ShowHeader="False">
            <Columns>
                <asp:BoundField HeaderText="Indirizzo" DataField="Indirizzo" />
                <asp:BoundField HeaderText="Comune" DataField="Comune">
                <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Provincia" DataField="Provincia">
                <ItemStyle Width="50px" />
                </asp:BoundField>
            </Columns>
            <EmptyDataTemplate>
                Nessun cantiere generato
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Panel>