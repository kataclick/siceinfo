﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.SiceInfo.Web.Helpers;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class SelezioneIndirizziNotificaRegionale : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void CaricaNotifica(NotificaPreliminare notifica)
        {
            Presenter.CaricaElementiInRadioButtonList(
                RadioButtonListIndirizzi,
                notifica.Indirizzi,
                "IndirizzoCompleto",
                "IdIndirizzo");

            // Nel caso in cui sia presente 
            if (RadioButtonListIndirizzi.Items.Count == 1)
            {
                RadioButtonListIndirizzi.Items[0].Selected = true;
            }
        }

        public Int32? GetIndirizzoSelezionato()
        {
            Int32? idIndirizzo = null;

            if (!String.IsNullOrEmpty(RadioButtonListIndirizzi.SelectedValue))
            {
                idIndirizzo = Int32.Parse(RadioButtonListIndirizzi.SelectedValue);
            }

            return idIndirizzo;
        }

        public void Abilita(Boolean abilita)
        {
            RadioButtonListIndirizzi.Enabled = abilita;
        }

    }
}