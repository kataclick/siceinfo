﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegendaGestioneCantieri.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.LegendaGestioneCantieri" %>


<div class="standardDiv">
<div style="float:left;">
<table width="300px">
    <tr>
        <td colspan="2">
            <b>
                Legenda
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/segnalazione.png" id="imageSegnala" runat="server" />
        </td>
        <td>
            Segnala il cantiere
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/assegnato.png" id="imageAssegna" runat="server" />
        </td>
        <td>
            Assegna il cantiere ad un gruppo di ispezione
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/programmato.png" id="imageProgramma" runat="server" />
        </td>
        <td>
            Presa in carico dell'ispezione
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/ispezione.png" id="imageIspezione" runat="server" />
        </td>
        <td>
            Ispezioni nel cantiere
        </td>
    </tr>
</table>
</div>

<div style="float:right;">
<table width="300px">
    <tr>
        <td colspan="2">
            <b>
                Legenda
            </b>
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/edit.png" id="imageModifica" runat="server" />
        </td>
        <td>
            Modifica i dati del cantiere
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/maps.gif" id="imageMappa" runat="server" />
        </td>
        <td>
            Visualizza il cantiere sulla mappa
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/pdf24.png" id="Img1" runat="server" />
        </td>
        <td>
            Esporta la scheda cantiere
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/associa.png" id="imageAssocia" runat="server" />
        </td>
        <td>
            Accorpa il cantiere ad altri cantieri
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/subappalti.png" id="imageSubappalti" runat="server" />
        </td>
        <td>
            Gestisci i subappalti del cantiere
        </td>
    </tr>
    <tr>
        <td>
            <img src="../../images/pallinoX.png" id="imageElimina" runat="server" />
        </td>
        <td>
            Elimina il cantiere
        </td>
    </tr>
</table>
</div>
</div>