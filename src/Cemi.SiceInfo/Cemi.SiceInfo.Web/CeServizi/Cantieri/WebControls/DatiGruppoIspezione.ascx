﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatiGruppoIspezione.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.DatiGruppoIspezione" %>

<table class="standardTable">
    <tr>
        <td>
            Descrizione
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxDescrizione"
                runat="server"
                MaxLength="50"
                Width="200px">
            </telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorDescrizione"
                runat="server"
                ControlToValidate="RadTextBoxDescrizione"
                ErrorMessage="Immettere una descrizione"
                ValidationGroup="datiGruppoIspezione">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Ispettori
        </td>
        <td colspan="2">
            <telerik:RadComboBox
                ID="RadComboBoxIspettori"
                runat="server"
                EmptyMessage="Selezionare un ispettore"
                AppendDataBoundItems="true"
                Width="200px">
            </telerik:RadComboBox>
            <asp:Button
                ID="ButtonAggiungiIspettore"
                runat="server"
                Text="Aggiungi ispettore"
                Width="120px" onclick="ButtonAggiungiIspettore_Click" />
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            <telerik:RadListView 
                ID="RadListViewIspettori"
                runat="server"
                onitemdatabound="RadListViewIspettori_ItemDataBound">
                <EmptyDataTemplate>
                    Nessun ispettore nel gruppo
                </EmptyDataTemplate>
                <ItemTemplate>
                    <asp:Label 
                        ID="LabelIspettore" 
                        runat="server">
                    </asp:Label>
                    <br />
                </ItemTemplate>
            </telerik:RadListView>
            <br />
        </td>
        <td>
            <asp:CustomValidator
                ID="CustomValidatorIspettori"
                runat="server"
                ErrorMessage="Selezionare almeno un ispettore"
                ValidationGroup="datiGruppoIspezione" 
                onservervalidate="CustomValidatorIspettori_ServerValidate">
                *
            </asp:CustomValidator>
        </td>
    </tr>
</table>