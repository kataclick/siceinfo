﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SegnalazioneDati.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.SegnalazioneDati" %>

<style type="text/css">
    .style1
    {
        width: 150px;
    }
</style>

<table class="standardTable">
    <tr>
        <td class="style1">
            Data:
        </td>
        <td colspan="2">
            <b>
                <asp:Label
                    ID="LabelData"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Utente:
        </td>
        <td colspan="2">
            <b>
                <asp:Label
                    ID="LabelUtente"
                    runat="server">
                </asp:Label>
            </b>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Ricorrente:
        </td>
        <td>
            <b>
                <asp:CheckBox
                    ID="CheckBoxRicorrente"
                    runat="server" />
            </b>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Motivazione:
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxMotivazione"
                runat="server"
                Width="250px"
                EmptyMessage="Seleziona la motivazione" AppendDataBoundItems="True">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorMotivazione"
                runat="server"
                ErrorMessage="Selezionare una motivazione"
                ValidationGroup="segnalaCantiere"
                ControlToValidate="RadComboBoxMotivazione"
                CssClass="messaggiErrore">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Priorità:
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxPriorita"
                runat="server"
                Width="250px"
                EmptyMessage="Seleziona la priorità" AppendDataBoundItems="True">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidatorPriorita"
                runat="server"
                ErrorMessage="Selezionare una priorità"
                ValidationGroup="segnalaCantiere"
                ControlToValidate="RadComboBoxPriorita"
                CssClass="messaggiErrore">
                *
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="style1">
            Note:
        </td>
        <td>
            <telerik:RadTextBox
                ID="RadTextBoxNote"
                runat="server"
                Width="250px"
                MaxLength="100">
            </telerik:RadTextBox>
        </td>
        <td>
        </td>
    </tr>
</table>