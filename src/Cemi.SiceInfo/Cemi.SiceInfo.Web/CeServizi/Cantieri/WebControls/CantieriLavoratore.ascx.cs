﻿using System;
using System.Text;
using System.Web.UI;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class CantieriLavoratore : System.Web.UI.UserControl
    {
        private string errore;
        public string Nome { get; set; }

        public string Cognome { get; set; }

        public DateTime DataNascita { get; set; }

        public Lavoratore Lavoratore
        {
            get
            {
                if (ControlloCampiServer())
                {
                    return CreaLavoratore();
                }
                else return null;
            }
        }

        public string Errore
        {
            get { return errore; }
            set { errore = value; }
        }

        /// <summary>
        /// Effettua il controllo dei campi lato server
        /// </summary>
        /// <returns></returns>
        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            if (string.IsNullOrEmpty(TextBoxCognome.Text))
            {
                res = false;
                errori.Append("Campo cognome vuoto" + Environment.NewLine);
            }

            if (string.IsNullOrEmpty(TextBoxNome.Text))
            {
                res = false;
                errori.Append("Campo nome vuoto" + Environment.NewLine);
            }

            if (string.IsNullOrEmpty(TextBoxDataNascita.Text))
            {
                res = false;
                errori.Append("Campo data di nascita vuoto" + Environment.NewLine);
            }

            DateTime dataNascita;
            if (!string.IsNullOrEmpty(TextBoxDataNascita.Text) &&
                !DateTime.TryParse(TextBoxDataNascita.Text, out dataNascita))
            {
                res = false;
                errori.Append("Formato data nascita errato" + Environment.NewLine);
            }

            if (!res)
                errore = errori.ToString();
            return res;
        }

        private Lavoratore CreaLavoratore()
        {
            // Creazione oggetto Lavoratore
            string cognome = null;
            string nome = null;
            DateTime dataNascita;

            cognome = TextBoxCognome.Text;
            nome = TextBoxNome.Text;
            dataNascita = DateTime.Parse(TextBoxDataNascita.Text);

            Lavoratore lavoratore = new Lavoratore(TipologiaLavoratore.Cantieri, null, cognome, nome, dataNascita);

            return lavoratore;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void SetValidationGroup(string validationGroup)
        {
            RegularExpressionValidator1.ValidationGroup = validationGroup;
            RequiredFieldValidator1.ValidationGroup = validationGroup;
            RequiredFieldValidator2.ValidationGroup = validationGroup;
            RequiredFieldValidator3.ValidationGroup = validationGroup;
        }
    }
}