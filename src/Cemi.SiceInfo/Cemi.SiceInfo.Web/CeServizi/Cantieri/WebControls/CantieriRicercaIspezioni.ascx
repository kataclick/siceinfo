﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CantieriRicercaIspezioni.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.CantieriRicercaIspezioni" %>

<asp:Panel ID="PanelRicercaIspezioni" runat="server" DefaultButton="ButtonVisualizza">
    <table class="filledtable">
        <tr>
            <td>
                Ricerca Ispezioni
            </td>
        </tr>
    </table>
    <table class="borderedTable">
        <tr>
            <td style="height: 76px">
                <table class="standardTable">
                    <tr>
                        <td>
                            Dal (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorDal" ControlToValidate="TextBoxDal" runat="server" ForeColor="Red"
                                Type="Date" Operator="DataTypeCheck" ErrorMessage="*" ValidationGroup="ricercaIspezioni"></asp:CompareValidator>
                        </td>
                        <td>
                            Al (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorAl" ControlToValidate="TextBoxAl" runat="server" ForeColor="Red"
                                Type="Date" Operator="DataTypeCheck" ErrorMessage="*" ValidationGroup="ricercaIspezioni"></asp:CompareValidator>
                        </td>
                        <td>
                            Ispettore
                        </td>
                        <td>
                            Stato
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListIspettori" runat="server" AppendDataBoundItems="True"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListStato" runat="server" AppendDataBoundItems="True"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cod.Impresa
                            <asp:RangeValidator ID="RangeValidatorIdImpresa" ControlToValidate="TextBoxIdImpresa" runat="server" ForeColor="Red"
                                Type="Integer" MinimumValue="1" MaximumValue="99999999" ErrorMessage="*" ValidationGroup="ricercaIspezioni"></asp:RangeValidator>
                        </td>
                        <td>
                            Detentrice appalto
                        </td>
                        <td>
                            Subappaltatrice
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIdImpresa" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxAppaltatrice" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxSubappaltatrice" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            &nbsp;<asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                                Text="Ricerca" ValidationGroup="ricercaIspezioni" />
                            <asp:Button ID="ButtonChiudi" runat="server" Font-Size="XX-Small" Height="20px" OnClick="ButtonChiudi_Click"
                                Text="X" Visible="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewIspezioni" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" DataKeyNames="IdIspezione" OnPageIndexChanging="GridViewIspezioni_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewIspezioni_SelectedIndexChanging" Width="100%"
                    OnRowDeleting="GridViewIspezioni_RowDeleting" OnRowDataBound="GridViewIspezioni_RowDataBound"
                    OnRowEditing="GridViewIspezioni_RowEditing" 
                    onrowcommand="GridViewIspezioni_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="Giorno">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td style="text-align:center;">
                                            <asp:Label ID="Label1" runat="server" 
                                                Text='<%# Bind("Giorno", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">
                                            <asp:Image
                                                ID="ImageBusta"
                                                runat="server"
                                                Width="16px"
                                                ImageUrl="../../images/icona_busta.png" />
                                        </td>
                                    </tr>
                                </table>
                                
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="NomeIspettore" HeaderText="Ispettore">
                        <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Stato" HeaderText="Stato" >
                        <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Cantiere">
                            <ItemTemplate>
                                <asp:Label ID="LabelCantiere" runat="server" Text='<%# Bind("NomeCantiere") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Font-Bold="True" Width="250px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Det. appalto">
                            <ItemTemplate>
                                <asp:Label ID="LabelAppaltatrice" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectText="Seleziona"
                            ShowSelectButton="True" SelectImageUrl="../../images/edit.png">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="24px" />
                        </asp:CommandField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" ImageUrl="../../images/PallinoXrosso.png"
                            Text="Ispezione non accessibile">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="24px" />
                        </asp:ButtonField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="Report"
                            ShowDeleteButton="True" DeleteImageUrl="../../images/printer.png">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="24px" />
                        </asp:CommandField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" EditText="Lettere"
                            ShowEditButton="True" EditImageUrl="../../images/LetteraWord_Icon.png" >
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="24px" />
                        </asp:CommandField>
                        <asp:ButtonField ButtonType="Image" CommandName="elimina" 
                            ImageUrl="../../images/editdelete.png" Text="Elimina">
                        <ItemStyle Width="24px" />
                        </asp:ButtonField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna ispezione trovata
                    </EmptyDataTemplate>
                </asp:GridView>
                &nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
