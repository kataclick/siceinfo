﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CantieriTestataIspezione.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.CantieriTestataIspezione" %>

<table class="borderedTable">
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelTitolo" runat="server" Text="Riassunto dati ispezione" Font-Bold="True"></asp:Label>    
        </td>
    </tr>
    <tr>
        <td>
            Giorno
        </td>
        <td>
            <asp:TextBox ID="TextBoxGiorno" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Cantiere
        </td>
        <td>
            <asp:TextBox ID="TextBoxCantiere" runat="server" Height="53px" TextMode="MultiLine" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Ispettore
        </td>
        <td>
            <asp:TextBox ID="TextBoxIspettore" runat="server" Width="300px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
</table>