﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CantieriRicercaLettere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.CantieriRicercaLettere" %>

<table class="filledtable">
    <tr>
        <td>
            Ricerca Lettere
        </td>
    </tr>
</table>
<table class="borderedTable">
    <tr>
        <td>
            <table class="standardTable">
                <tr>
                    <td>
                        Dal (gg/mm/aaaa)
                        <asp:CompareValidator 
                            ID="CompareValidatorDal"
                            runat="server" 
                            ControlToValidate="TextBoxDal" 
                            ErrorMessage="*" 
                            Type="Date" 
                            Operator="DataTypeCheck"
                            ValidationGroup="ricercaLettere">
                        </asp:CompareValidator>
                    </td>
                    <td>
                        Al (gg/mm/aaaa)
                        <asp:CompareValidator 
                            ID="CompareValidatorAl"
                            runat="server" 
                            ControlToValidate="TextBoxAl" 
                            ErrorMessage="*" 
                            Type="Date" 
                            Operator="DataTypeCheck"
                            ValidationGroup="ricercaLettere">
                        </asp:CompareValidator>
                    </td>
                    <td>
                        Tipo lettera
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="10" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="10" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListTipoLettera" runat="server" Width="180px" 
                            AppendDataBoundItems="True"></asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Indirizzo cantiere
                    </td>
                    <td>
                        Comune cantiere
                    </td>
                    <td>
                        Protocollo
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="255" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxProtocollo" runat="server" MaxLength="50" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Codice impresa
                        <asp:RangeValidator 
                            ID="RangeValidatorIdImpresa"
                            runat="server" 
                            ControlToValidate="TextBoxIdImpresa" 
                            ErrorMessage="*" 
                            Type="Integer" 
                            MinimumValue="1"
                            MaximumValue="2147483647"
                            ValidationGroup="ricercaLettere">
                        </asp:RangeValidator>
                    </td>
                    <td>
                        Rag.soc. impresa
                    </td>
                    <td>
                        Fisc./Iva impresa
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBoxIdImpresa" runat="server" MaxLength="10" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxRagioneSocialeImpresa" runat="server" MaxLength="255" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxFiscIvaImpresa" runat="server" MaxLength="16" Width="180px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click" ValidationGroup="ricercaLettere" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewLettere" runat="server" DataKeyNames="Protocollo" AllowPaging="True"
                AutoGenerateColumns="False" Width="100%" OnRowEditing="GridViewLettere_RowEditing"
                OnPageIndexChanging="GridViewLettere_PageIndexChanging" OnRowDataBound="GridViewLettere_RowDataBound"
                OnRowDeleting="GridViewLettere_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="Giorno" HeaderText="Data gen." DataFormatString="{0:dd/MM/yyyy}"
                        HtmlEncode="False">
                        <ItemStyle Width="60px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Protocollo" HeaderText="Prot.">
                        <ItemStyle Width="50px" Font-Bold="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IspezioneGiorno" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Giorno isp."
                        HtmlEncode="False">
                        <ItemStyle Width="60px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Cantiere">
                        <ItemTemplate>
                            <asp:Label ID="LabelCantiere" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TipoLettera" HeaderText="Tipo lettera">
                        <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Visualizza"
                        ShowEditButton="True" HeaderText="Lettera">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                        <ItemStyle Width="30px" />
                    </asp:CommandField>
                    <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Elimina"
                        ShowDeleteButton="True">
<ControlStyle CssClass="bottoneGriglia"></ControlStyle>

                        <ItemStyle Width="10px" />
                    </asp:CommandField>
                </Columns>
                <EmptyDataTemplate>
                    Nessuna lettera trovata
                </EmptyDataTemplate>
            </asp:GridView>
            &nbsp;
        </td>
    </tr>
</table>
