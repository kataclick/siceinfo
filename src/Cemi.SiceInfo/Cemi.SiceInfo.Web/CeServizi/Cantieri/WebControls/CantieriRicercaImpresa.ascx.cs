﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Delegates.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class CantieriRicercaImpresa : System.Web.UI.UserControl
    {
        private const int INDICECODICE = 0;

        private readonly CantieriBusiness biz = new CantieriBusiness();

        private string impresaTrovata;
        private bool modalitaNotifiche;

        public string ImpresaTrovata
        {
            get { return impresaTrovata; }
            set
            {
                impresaTrovata = value;
                TextBoxRagioneSociale.Text = "%" + impresaTrovata;
            }
        }

        public string RagioneSociale
        {
            get { return TextBoxRagioneSociale.Text; }
        }

        public event EventHandler OnNuovaImpresaSelected;
        public event ImpreseSelectedEventHandler OnImpresaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!modalitaNotifiche)
            //    GridViewImprese.Columns[INDICECODICE].Visible = false;
        }

        public void ModalitaNotifiche()
        {
            ButtonChiudi.Visible = false;
            RadioButtonCantieri.Text = "Anagrafica Notifiche";
            modalitaNotifiche = true;
            GridViewImprese.Columns[INDICECODICE].Visible = true;
        }

        public void ModalitaTuteScarpe()
        {
            //RadioButtonSiceNew.Enabled = false;
            //RadioButtonCantieri.Enabled = false;
            RadioButtonSiceNew.Visible = false;
            RadioButtonCantieri.Visible = false;
            ButtonNuovo.Visible = false;
            ButtonChiudi.Visible = false;
        }

        public void ModalitaImpreseRegolari()
        {
            //RadioButtonSiceNew.Enabled = false;
            //RadioButtonCantieri.Enabled = false;
            RadioButtonSiceNew.Visible = false;
            RadioButtonCantieri.Visible = false;
            ButtonNuovo.Visible = false;
            ButtonChiudi.Visible = false;
        }

        public void Reset()
        {
            GridViewImprese.DataSource = null;
            GridViewImprese.DataBind();
            //GridViewImprese.

            TextBoxRagioneSociale.Text = string.Empty;
            TextBoxIndirizzo.Text = string.Empty;
            TextBoxComune.Text = string.Empty;
            TextBoxIvaFiscale.Text = string.Empty;
        }

        public void CaricaImprese()
        {
            if (string.IsNullOrEmpty(TextBoxRagioneSociale.Text.Trim()) &&
                string.IsNullOrEmpty(TextBoxIndirizzo.Text.Trim()) && string.IsNullOrEmpty(TextBoxComune.Text.Trim()) &&
                string.IsNullOrEmpty(TextBoxIvaFiscale.Text.Trim()) && string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
            {
                LabelErrore.Text = "Digitare un filtro";
            }
            else
            {
                LabelErrore.Text = string.Empty;
                int? codice = null;
                string ragioneSociale = null;
                string comune = null;
                string indirizzo = null;
                string ivaCodFisc = null;
                TipologiaImpresa tipoImpresa = TipologiaImpresa.SiceNew;

                string ragSocRic = TextBoxRagioneSociale.Text.Trim();
                if (!string.IsNullOrEmpty(ragSocRic))
                    ragioneSociale = ragSocRic;

                string comRic = TextBoxComune.Text.Trim();
                if (!string.IsNullOrEmpty(comRic))
                    comune = comRic;

                string indRic = TextBoxIndirizzo.Text.Trim();
                if (!string.IsNullOrEmpty(indRic))
                    indirizzo = indRic;

                string ivaRic = TextBoxIvaFiscale.Text;
                if (!string.IsNullOrEmpty(ivaRic))
                    ivaCodFisc = ivaRic;

                if (!string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
                    codice = Int32.Parse(TextBoxCodice.Text);

                if (RadioButtonCantieri.Checked)
                    tipoImpresa = TipologiaImpresa.Cantieri;

                ImpresaCollection listaImprese =
                    biz.GetimpreseOrdinate(tipoImpresa, codice, ragioneSociale, comune, indirizzo, ivaCodFisc, null, null,
                                           modalitaNotifiche);
                GridViewImprese.DataSource = listaImprese;
                GridViewImprese.DataBind();
            }
        }

        private void CaricaImprese(string sortExpression)
        {
            int? codice = null;
            string ragioneSociale = null;
            string comune = null;
            string indirizzo = null;
            string ivaCodFisc = null;
            TipologiaImpresa tipoImpresa = TipologiaImpresa.SiceNew;

            string ragSocRic = TextBoxRagioneSociale.Text.Trim();
            if (!string.IsNullOrEmpty(ragSocRic))
                ragioneSociale = ragSocRic;

            string comRic = TextBoxComune.Text.Trim();
            if (!string.IsNullOrEmpty(comRic))
                comune = comRic;

            string indRic = TextBoxIndirizzo.Text.Trim();
            if (!string.IsNullOrEmpty(indRic))
                indirizzo = indRic;

            string ivaRic = TextBoxIvaFiscale.Text;
            if (!string.IsNullOrEmpty(ivaRic))
                ivaCodFisc = ivaRic;

            if (!string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
                codice = Int32.Parse(TextBoxCodice.Text);

            if (RadioButtonCantieri.Checked)
                tipoImpresa = TipologiaImpresa.Cantieri;

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "DESC";
                else
                    direct = "ASC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            ImpresaCollection listaImprese =
                biz.GetimpreseOrdinate(tipoImpresa, codice, ragioneSociale, comune, indirizzo, ivaCodFisc, sortExpression,
                                       direct, modalitaNotifiche);
            GridViewImprese.DataSource = listaImprese;
            GridViewImprese.PageIndex = 0;
            GridViewImprese.DataBind();
        }

        private void CaricaImpresePreservaOrdine(string sortExpression)
        {
            int? codice = null;
            string ragioneSociale = null;
            string comune = null;
            string indirizzo = null;
            string ivaCodFisc = null;
            TipologiaImpresa tipoImpresa = TipologiaImpresa.SiceNew;

            string ragSocRic = TextBoxRagioneSociale.Text.Trim();
            if (!string.IsNullOrEmpty(ragSocRic))
                ragioneSociale = ragSocRic;

            string comRic = TextBoxComune.Text.Trim();
            if (!string.IsNullOrEmpty(comRic))
                comune = comRic;

            string indRic = TextBoxIndirizzo.Text.Trim();
            if (!string.IsNullOrEmpty(indRic))
                indirizzo = indRic;

            string ivaRic = TextBoxIvaFiscale.Text;
            if (!string.IsNullOrEmpty(ivaRic))
                ivaCodFisc = ivaRic;

            if (!string.IsNullOrEmpty(TextBoxCodice.Text.Trim()))
                codice = Int32.Parse(TextBoxCodice.Text);

            if (RadioButtonCantieri.Checked)
                tipoImpresa = TipologiaImpresa.Cantieri;

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "ASC";
                else
                    direct = "DESC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            ImpresaCollection listaImprese =
                biz.GetimpreseOrdinate(tipoImpresa, codice, ragioneSociale, comune, indirizzo, ivaCodFisc, sortExpression,
                                       direct, modalitaNotifiche);
            GridViewImprese.DataSource = listaImprese;
            GridViewImprese.PageIndex = 0;
            GridViewImprese.DataBind();
        }

        protected void GridViewImprese_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idImpresa = (int)GridViewImprese.DataKeys[e.NewSelectedIndex]["IdImpresa"];
            TipologiaImpresa tipoImpresa = (TipologiaImpresa)GridViewImprese.DataKeys[e.NewSelectedIndex]["TipoImpresa"];
            Impresa impresa =
                biz.GetimpreseOrdinate(tipoImpresa, idImpresa, null, null, null, null, null, null, modalitaNotifiche)[0];

            if (OnImpresaSelected != null)
                OnImpresaSelected(impresa);
        }

        /// <summary>
        /// Evento di ricerca
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            //controlliamo il tipo di ricerca che stiamo effettuando
            if (RadioButtonSiceNew.Checked)
            {
                //se è sull'anagrafica di sicenew ha senso visualizzare il codice impresa
                GridViewImprese.Columns[INDICECODICE].Visible = true;
            }
            else
                //se è sull'anagrafica cantieri non visualizziamo il codice
                //GridViewImprese.Columns[INDICECODICE].Visible = false;
                if (!modalitaNotifiche)
                GridViewImprese.Columns[INDICECODICE].Visible = false;

            CaricaImprese();
        }

        protected void GridViewImprese_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');

                if (ord[1] == "DESC")
                    CaricaImpresePreservaOrdine(ord[0]);
                else
                    CaricaImpresePreservaOrdine(ord[0]);
            }
            else
                CaricaImprese();

            GridViewImprese.PageIndex = e.NewPageIndex;
            GridViewImprese.DataBind();
        }

        protected void GridViewImprese_Sorting(object sender, GridViewSortEventArgs e)
        {
            //SortDirection dir = SortDirection.Descending;
            //if (e.SortDirection == SortDirection.Descending)
            //    dir = SortDirection.Ascending;

            CaricaImprese(e.SortExpression);
        }

        protected void ButtonChiudi_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        protected void ButtonNuovo_Click(object sender, EventArgs e)
        {
            if (OnNuovaImpresaSelected != null)
                OnNuovaImpresaSelected(this, null);
        }

        protected void GridViewImprese_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label labelSedeLegale = (Label)e.Row.FindControl("LabelSedeLegale");
                Label labelSedeAmministrativa = (Label)e.Row.FindControl("LabelSedeAmministrativa");
                Label labelLegale = (Label)e.Row.FindControl("LabelLegale");
                Label labelAmministrativa = (Label)e.Row.FindControl("LabelAmministrativa");

                Impresa impresa = (Impresa)e.Row.DataItem;

                labelSedeLegale.Text = impresa.IndirizzoCompleto;
                labelSedeAmministrativa.Text = impresa.AmmiIndirizzoCompleto;

                if (impresa.TipoImpresa == TipologiaImpresa.Cantieri)
                {
                    labelLegale.Visible = false;
                    labelAmministrativa.Visible = false;
                }
                else if (impresa.TipoImpresa == TipologiaImpresa.SiceNew &&
                         impresa.IndirizzoCompleto == impresa.AmmiIndirizzoCompleto)
                {
                    labelLegale.Visible = false;
                    labelAmministrativa.Visible = false;
                    labelSedeAmministrativa.Visible = false;
                }
            }
        }
    }
}