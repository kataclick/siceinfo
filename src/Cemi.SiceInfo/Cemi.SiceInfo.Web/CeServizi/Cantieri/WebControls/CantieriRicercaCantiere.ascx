﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CantieriRicercaCantiere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.CantieriRicercaCantiere" %>

<telerik:RadScriptBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function openRadWindowMappa(latitudine, longitudine, indirizzo, cap, comune, provincia, note) {
            //var oWindow = radopen("../VisualizzazioneMappa.aspx?latitudine=" + latitudine + "&longitudine=" + longitudine + "&cap=" + cap + "&indirizzo=" + indirizzo + "&provincia=" + provincia + "&comune=" + comune + "&note=" + note, null);
            var oWindow = radopen("/Map/?latitudine=" + latitudine + "&longitudine=" + longitudine + "&cap=" + cap + "&indirizzo=" + indirizzo + "&provincia=" + provincia + "&comune=" + comune + "&note=" + note, null);
            oWindow.set_title("Mappa");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(800, 560);
            oWindow.center();
        }

        function OnClientClose(oWnd, args) {
            <%# RecuperaPostBackCode() %>;
        }

        function openRadWindowSegnalazione(idCantiere) {
            var oWindow = radopen("SegnalaCantiere.aspx?idCantiere=" + idCantiere, null);
            oWindow.set_title("Nuova Segnalazione");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(400, 360);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openRadWindowSegnalazioneModifica(idCantiere) {
            var oWindow = radopen("SegnalaCantiere.aspx?modalita=modifica&idCantiere=" + idCantiere, null);
            oWindow.set_title("Modifica Segnalazione");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(400, 360);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openRadWindowAssegnazione(idCantiere) {
            var oWindow = radopen("AssegnaCantiere.aspx?idCantiere=" + idCantiere, null);
            oWindow.set_title("Nuova Assegnazione");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(500, 500);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openRadWindowAssegnazioneModifica(idCantiere) {
            var oWindow = radopen("AssegnaCantiere.aspx?modalita=modifica&idCantiere=" + idCantiere, null);
            oWindow.set_title("Modifica Assegnazione");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(500, 500);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }

        function openRadWindowCambioPresaInCarico(idPresaInCarico) {
            var oWindow = radopen("CambioPresaInCarico.aspx?idPresaInCarico=" + idPresaInCarico, null);
            oWindow.set_title("Cambio presa in carico");
            oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close);
            oWindow.setSize(300, 200);
            oWindow.center();
            oWindow.add_close(OnClientClose);
        }
    </script>
</telerik:RadScriptBlock>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" VisibleTitlebar="true"
    VisibleStatusbar="false" Modal="true" IconUrl="../../images/favicon.ico" />
<asp:Panel ID="PanelRicercaCantieri" runat="server" DefaultButton="ButtonVisualizza"
    Width="950px">
    <div class="standardDiv">
        <table class="standardTable">
            <tr>
                <td colspan="4">
                    <small>
                    <b>
                        Impresa
                    </b>
                    </small>
                </td>
            </tr>
            <tr>
                <td>
                    Codice
                </td>
                <td>
                    Rag. soc.
                </td>
                <td>
                    Cod.fisc/IVA
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadNumericTextBox ID="RadTextBoxIdImpresa" runat="server" MaxLength="10"
                        Width="230px" MinValue="1" MaxValue="2147483647" Type="Number" DataType="System.Int32">
                        <NumberFormat DecimalDigits="0" GroupSeparator="" />
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxImpresa" runat="server" MaxLength="255" Width="230px">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxCodiceFiscale" runat="server" MaxLength="16" Width="230px">
                    </telerik:RadTextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <small>
                    <b>
                        Indirizzo
                    </b>
                    </small>
                </td>
            </tr>
            <tr>
                <td>
                    Provincia
                </td>
                <td>
                    Comune
                </td>
                <td>
                    Indirizzo
                </td>
                <td>
                    Cap
                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorCap" runat="server"
                        ErrorMessage="*" ControlToValidate="RadTextBoxCap" ValidationGroup="ricercaCantieri"
                        ValidationExpression="^\d{5}$">
                    </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="TextBoxProvincia" runat="server" Width="230px" MaxLength="2">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxComune" runat="server" Width="230px" MaxLength="100">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxIndirizzo" runat="server" Width="230px" MaxLength="255">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxCap" runat="server" Width="230px" MaxLength="5">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <small>
                    <b>
                        Data segnalazione
                    </b>
                    </small>
                    <asp:CustomValidator 
                        ID="CustomValidatorValidatorDataSegnalazione" 
                        runat="server"
                        ErrorMessage="*" 
                        ValidationGroup="ricercaCantieri" 
                        onservervalidate="CustomValidatorValidatorDataSegnalazione_ServerValidate">
                    </asp:CustomValidator>
                </td>
                <td>
                </td>
                <td>
                    <small>
                    <b>
                        Data assegnazione
                    </b>
                    </small>
                    <asp:CustomValidator 
                        ID="CustomValidatorValidatorDataAssegnazione" 
                        runat="server"
                        ErrorMessage="*" 
                        ValidationGroup="ricercaCantieri" 
                        onservervalidate="CustomValidatorValidatorDataAssegnazione_ServerValidate">
                    </asp:CustomValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Dal
                </td>
                <td>
                    Al
                </td>
                <td>
                    Dal
                </td>
                <td>
                    Al
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDatePickerDataSegnalazioneDal"
                        runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDatePickerDataSegnalazioneAl"
                        runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDatePickerDataAssegnazioneDal"
                        runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDatePickerDataAssegnazioneAl"
                        runat="server">
                    </telerik:RadDatePicker>
                </td>
            </tr>
            <tr>
                <td>
                    <small>
                    <b>
                        Data programmazione
                    </b>
                    </small>
                    <asp:CustomValidator 
                        ID="CustomValidatorValidatorDataIspezione" 
                        runat="server"
                        ErrorMessage="*" 
                        ValidationGroup="ricercaCantieri" 
                        onservervalidate="CustomValidatorValidatorDataIspezione_ServerValidate">
                    </asp:CustomValidator>
                </td>
                <td>
                </td>
                <td>
                    <small>
                    <b>
                        Altro
                    </b>
                    </small>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Dal
                </td>
                <td>
                    Al
                </td>
                <td>
                    Importo&gt;
                </td>
                <td>
                    Committente
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDatePickerDataIspezioneDal"
                        runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadDatePicker
                        ID="RadDatePickerDataIspezioneAl"
                        runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxImporto" runat="server" Width="230px"
                        MaxLength="20" Type="Currency" MaxValue="100000000" MinValue="0">
                    </telerik:RadNumericTextBox>
                </td>
                <td>
                    <telerik:RadTextBox ID="TextBoxCommittente" runat="server" MaxLength="255" Width="230px">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <small>
                    <b>
                        Stati
                    </b>
                    </small>
                </td>
            </tr>
            <tr>
                <td>
                    Stato segnalazione
                </td>
                <td>
                    Stato assegnazione
                </td>
                <td>
                    Stato presa in carico
                </td>
                <td>
                    Stato rapporto ispezione
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadComboBox ID="DropDownListStatoSegnalazione" runat="server" Width="230px">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="TUTTI" />
                            <telerik:RadComboBoxItem Text="Segnalati" Value="SEGNALATI" />
                            <telerik:RadComboBoxItem Text="Non segnalati" Value="NONSEGNALATI" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadComboBox ID="DropDownListStatoAssegnazione" runat="server" Width="230px"
                        AutoPostBack="True" OnSelectedIndexChanged="DropDownListStatoAssegnazione_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="Tutti" />
                            <telerik:RadComboBoxItem Text="Assegnati" Value="Assegnati" />
                            <telerik:RadComboBoxItem Text="Rifiutati" Value="Rifiutati" />
                            <telerik:RadComboBoxItem Text="Non assegnati" Value="NonAssegnati" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadComboBox ID="DropDownListStatoPresaCarico" runat="server" Width="230px"
                        AutoPostBack="True" OnSelectedIndexChanged="DropDownListStatoPresaCarico_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="Tutti" />
                            <telerik:RadComboBoxItem Text="Presi in carico" Value="PresiCarico" />
                            <telerik:RadComboBoxItem Text="Rifiutati" Value="Rifiutati" />
                            <telerik:RadComboBoxItem Text="Non presi in carico" Value="NonPresiCarico" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadComboBox ID="DropDownListStatoRapportoIspezione" runat="server" Width="230px">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="Tutti" />
                            <telerik:RadComboBoxItem Text="Rapporto ispezione creato" Value="Ispezione" />
                            <telerik:RadComboBoxItem Text="Rapporto ispezione non creato" Value="NoIspezione" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <telerik:RadComboBox ID="DropDownListAssegnazioneIspettore" runat="server" Width="230px"
                        AppendDataBoundItems="True" Enabled="False">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                    <telerik:RadComboBox ID="DropDownListPresaInCaricoIspettore" runat="server" Width="230px"
                        AppendDataBoundItems="True" Enabled="False">
                        <Items>
                            <telerik:RadComboBoxItem Text="Tutti" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="text-align:right" colspan="4">
                    <asp:Button ID="ButtonNuovo" runat="server" OnClick="ButtonNuovo_Click" Text="Nuovo"
                        Width="70px" />
                    <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click"
                        Width="100px" ValidationGroup="ricercaCantieri" />
                    <asp:Button ID="ButtonEstrazioneExcel" runat="server" Text="Estraz. Excel" Width="100px"
                        ValidationGroup="ricercaCantieri" OnClick="ButtonEstrazioneExcel_Click" />
                    <asp:Button ID="ButtonEstrazionePDF" runat="server" Text="Estraz. PDF" Width="100px"
                        ValidationGroup="ricercaCantieri" OnClick="ButtonEstrazionePDF_Click" />
                </td>
            </tr>
        </table>
        <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Il cantiere ha già delle programmazioni, non è possibile fonderlo"
            Visible="False"></asp:Label>
    </div>
    <div class="standardDiv">
        <asp:Label ID="LabelElencoCantieri" runat="server" Font-Bold="True" Text="Elenco cantieri"
            Visible="False"></asp:Label>
        <br />
        <telerik:RadGrid ID="RadGridCantieri" runat="server" Width="100%" GridLines="None"
            AllowPaging="True" OnItemDataBound="RadGridCantieri_ItemDataBound" OnPageIndexChanged="RadGridCantieri_PageIndexChanged"
            AllowSorting="True" OnItemCommand="RadGridCantieri_ItemCommand" OnSortCommand="RadGridCantieri_SortCommand"
            PageSize="5" Visible="False">
            <MasterTableView DataKeyNames="IdCantiere, IndirizzoMappa">
                <Columns>
                    <telerik:GridTemplateColumn HeaderText="Indirizzo" UniqueName="indirizzo">
                        <ItemTemplate>
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="LabelIndirizzo" runat="server">
                                            </asp:Label>
                                            <br />
                                            <asp:Label ID="LabelCap" runat="server">
                                            </asp:Label>
                                            <asp:Label ID="LabelComune" runat="server">
                                            </asp:Label>
                                            <asp:Label ID="LabelProvincia" runat="server">
                                            </asp:Label>
                                        </b>
                                    </td>
                                    <td align="right">
                                        <asp:ImageButton ID="ImageButtonMappa" runat="server" ImageUrl="../../images/maps.gif"
                                            CommandName="mappa" CommandArgument="<%# Container.RowIndex %>" ToolTip="Vedi su Mappa" />
                                        <br />
                                        <asp:ImageButton ID="ImageButtonSchedaCantiere" runat="server" ImageUrl="../../images/pdf24.png"
                                            CommandName="scheda" CommandArgument="<%# Container.RowIndex %>" ToolTip="Scheda Cantiere" />
                                        <br />
                                        <asp:ImageButton ID="ImageButtonNotifica" runat="server" ImageUrl="../../images/pdf24.png"
                                            CommandName="notifica" CommandArgument="<%# Container.RowIndex %>" ToolTip="Notifica Preliminare" />
                                    </td>
                                </tr>
                                <tr id="trNote" runat="server">
                                    <td colspan="2">
                                        <table class="borderedTable">
                                            <tr>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonSegnalazione" runat="server" ImageUrl="../../images/segnalazioneBN.png"
                                                        CommandName="segnala" CommandArgument="<%# Container.RowIndex %>" ToolTip="Segnalazione" />
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonAssegnazione" runat="server" ImageUrl="../../images/assegnatoBN.png"
                                                        CommandName="assegna" CommandArgument="<%# Container.RowIndex %>" ToolTip="Assegnazione" />
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonProgrammazione" runat="server" ImageUrl="../../images/programmatoBN.png"
                                                        CommandName="programma" CommandArgument="<%# Container.RowIndex %>" ToolTip="Programmazione"
                                                        Enabled="false" />
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:ImageButton ID="ImageButtonIspezione" runat="server" ImageUrl="../../images/ispezioneBN.png"
                                                        CommandName="ispeziona" CommandArgument="<%# Container.RowIndex %>" ToolTip="Ispezione"
                                                        Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataSegnalazione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataAssegnazione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataProgrammazione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                                <td class="tdNoteRicercaCantieri">
                                                    <asp:Label ID="LabelDataIspezione" runat="server" CssClass="campoMoltoPiccolo">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trAssegnazioneMultipla" runat="server" visible="false">
                                    <td>
                                        <asp:CheckBox
                                            ID="CheckBoxAssegnazioneMultipla"
                                            runat="server"
                                            Text="Assegnazione multipla" />
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <ItemStyle Width="250px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="Importo(€)" DataField="Importo" SortExpression="Importo"
                        DataFormatString="{0:C}" HtmlEncode="False" UniqueName="importo">
                        <ItemStyle HorizontalAlign="Right" Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="impresa">
                        <ItemStyle Width="200px" />
                        <ItemTemplate>
                            <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Committente" UniqueName="committente">
                        <ItemStyle Width="200px" />
                        <ItemTemplate>
                            <asp:Label ID="LabelCommittente" runat="server"></asp:Label>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Fonti" HeaderText="Fonti" UniqueName="fonti">
                        <ItemStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridTemplateColumn HeaderText="Ispezioni" UniqueName="ispezioni">
                                <ItemTemplate>
                                    <asp:GridView ID="GridViewIspezioni" runat="server" AutoGenerateColumns="False" DataKeyNames="IdIspezione"
                                        ShowHeader="False" OnRowDataBound="GridViewIspezioni_RowDataBound" Width="100%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelIspezione" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            Nessuna ispezione
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </telerik:GridTemplateColumn>--%>
                    <telerik:GridButtonColumn CommandName="modifica" ImageUrl="../../images/edit.png" Text="Modifica"
                        ButtonType="ImageButton" UniqueName="modifica">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="fondi" ButtonCssClass="bottoneGriglia"
                        ImageUrl="../../images/associa.png" Text="Associa" UniqueName="associa">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="subappalti" UniqueName="subappalti"
                        ImageUrl="../../images/subappalti.png" Text="Subappalti">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                    <telerik:GridButtonColumn CommandName="elimina" ButtonType="ImageButton" ImageUrl="../../images/pallinoX.png"
                        Text="" UniqueName="elimina" ButtonCssClass="bottoneGriglia">
                        <ItemStyle Width="10px" />
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
        </telerik:RadGrid>
        <br />
        <asp:Button 
            ID="ButtonAssegnazioneMultipla"
            runat="server"
            Text="Assegnazione Multipla"
            Width="150px"
            Visible="false"
            ValidationGroup="assegnazioneMultipla"
            CausesValidation="true"
            onclick="ButtonAssegnazioneMultipla_Click" />
        <asp:CustomValidator
            ID="CustomValidatorAssegnazioneMultipla"
            runat="server"
            ValidationGroup="assegnazioneMultipla"
            ErrorMessage="Selezionare almeno un cantiere"
            ForeColor="Red" 
            onservervalidate="CustomValidatorAssegnazioneMultipla_ServerValidate">
        </asp:CustomValidator>
    </div>
</asp:Panel>
