﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class LegendaColoriAttivita : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PanelIspezioniDaConsiderare.BackColor = ColoriAttivita.SfondoIspezione;
                PanelIspezioniConsiderate.BackColor = ColoriAttivita.SfondoIspezioneConsiderata;
                PanelIspezioniFantasma.BackColor = ColoriAttivita.SfondoIspezioneFantasma;
                PanelAppuntamenti.BackColor = ColoriAttivita.SfondoAppuntamento;
                PanelAttivita.BackColor = ColoriAttivita.SfondoBackOffice;
                PanelVarie.BackColor = ColoriAttivita.SfondoVarie;
            }
        }
    }
}