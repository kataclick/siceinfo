﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrazioneIspettoreModifica.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.RegistrazioneIspettoreModifica" %>
<table width="600">
    <tr>
        <td align="right">
            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Nome:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelNome" runat="server"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Cognome:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelCognome" runat="server"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelUsername" runat="server" Font-Bold="True" Text="Username:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelLogin" runat="server"></asp:Label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelCellulare" runat="server" Font-Bold="True" Text="Cellulare:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxCellulare" runat="server" MaxLength="15"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelZonaCantiere" runat="server" Font-Bold="True" Text="Zona cantiere:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="DropDownListZonaCantiere" runat="server">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="right">
            <asp:Label ID="LabelAttivo" runat="server" Font-Bold="True" Text="Attivo:"></asp:Label>
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxAttivo" runat="server" Checked="True" />
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3" height="5">
        </td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Button ID="ButtonRegistraIspettore" runat="server" Text="Modifica ispettore"
                OnClick="ButtonModificaIspettore_Click" />
            <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla"
                Width="130px" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <asp:Label ID="LabelResult" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>
