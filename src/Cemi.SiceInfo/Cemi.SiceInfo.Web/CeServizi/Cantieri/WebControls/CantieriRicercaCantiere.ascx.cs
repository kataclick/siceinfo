﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Text;
using System.IO;
using TBridge.Cemi.Cpt.Business;
using System.Collections.Generic;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Business.Cpt;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Delegates.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Filters.Cantieri;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls
{
    public partial class CantieriRicercaCantiere : System.Web.UI.UserControl
    {
        private const int COLONNAELIMINA = 8;
        private const int COLONNAFONDI = 6;
        private const int COLONNASUBAPPALTI = 7;
        private const int COLONNASELEZIONA = 5;

        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly BusinessEF bizEF = new BusinessEF();

        public Ispettore ispettore;
        public event CantieriSelectedEventHandler OnCantiereSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (DropDownListZone.Items.Count == 0)
            //{
            //    CaricaZone();
            //}

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(RadGridCantieri);

            if (!Page.IsPostBack)
            {
                this.DataBind();

                NascondiFiltri();
                CaricaIspettori();

                if (GestioneUtentiBiz.IsIspettore())
                {
                    ViewState["IdIspettore"] = ((TBridge.Cemi.Type.Entities.GestioneUtenti.Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente()).IdIspettore;
                }
            }
            else
            {
                // Verifico se è un Refresh fatto con Javascript

                String eventArg = Request["__EVENTARGUMENT"];

                if (eventArg != null)
                {
                    int offset = eventArg.IndexOf("@@@@@");

                    if (offset > -1)
                    {
                        ButtonAssegnazioneMultipla.Attributes.Clear();

                        if (ViewState["ordina"] != null)
                        {
                            string[] ord = ((string)ViewState["ordina"]).Split('|');

                            if (ord[1] == "DESC")
                                CaricaCantieriPreservaOrdine(ord[0]);
                            else
                                CaricaCantieriPreservaOrdine(ord[0]);
                        }
                        else
                        {
                            CaricaCantieri();
                        }
                    }
                }
            }
        }

        private void CaricaIspettori()
        {
            IspettoreCollection ispettori = biz.GetIspettori(null, true);

            Presenter.CaricaElementiInDropDown(
                DropDownListAssegnazioneIspettore,
                ispettori,
                "NomeCompleto",
                "IdIspettore");

            Presenter.CaricaElementiInDropDown(
                DropDownListPresaInCaricoIspettore,
                ispettori,
                "NomeCompleto",
                "IdIspettore");
        }

        private void NascondiFiltri()
        {
            //if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriSegnalazione.ToString()))
            //{
            //    PanelStatoSegnalazioneDescrizione.Visible = true;
            //    PanelStatoSegnalazioneCombo.Visible = true;
            //}
            //else
            //{
            //    PanelStatoSegnalazioneDescrizione.Visible = false;
            //    PanelStatoSegnalazioneCombo.Visible = false;
            //}

            //if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriAssegnazione.ToString()))
            //{
            //    PanelStatoProgrammazioneDescrizione.Visible = true;
            //    PanelStatoProgrammazioneCombo.Visible = true;
            //    DropDownListAssegnazioneIspettore.Visible = true;
            //}
            //else
            //{
            //    PanelStatoProgrammazioneDescrizione.Visible = false;
            //    PanelStatoProgrammazioneCombo.Visible = false;
            //    DropDownListAssegnazioneIspettore.Visible = false;
            //}

            //if (GestioneUtentiBiz.IsIspettore() && !GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI))
            //{
            //    DropDownListPresaInCaricoIspettore.Visible = false;
            //    DropDownListAssegnazioneIspettore.Visible = false;
            //}
        }

        public void SoloSelezione()
        {
            RadGridCantieri.MasterTableView.Columns[COLONNAELIMINA].Visible = false;
            RadGridCantieri.MasterTableView.Columns[COLONNAFONDI].Visible = false;
            RadGridCantieri.MasterTableView.Columns[COLONNASUBAPPALTI].Visible = false;
            ViewState["SoloSelezione"] = true;
        }

        public void Carica()
        {
            //CaricaZone();
        }

        private CantieriFilter CreaFiltro()
        {
            CantieriFilter filtro = new CantieriFilter();

            filtro.Provincia = Presenter.NormalizzaCampoTesto(TextBoxProvincia.Text);
            filtro.Comune = Presenter.NormalizzaCampoTesto(TextBoxComune.Text);
            filtro.Indirizzo = Presenter.NormalizzaCampoTesto(TextBoxIndirizzo.Text);
            if (RadTextBoxIdImpresa.Value.HasValue)
            {
                filtro.IdImpresa = (Int32)RadTextBoxIdImpresa.Value.Value;
            }
            filtro.Impresa = Presenter.NormalizzaCampoTesto(TextBoxImpresa.Text);
            filtro.CodiceFiscaleImpresa = Presenter.NormalizzaCampoTesto(RadTextBoxCodiceFiscale.Text);
            filtro.Committente = Presenter.NormalizzaCampoTesto(TextBoxCommittente.Text);
            //filtro.ImpresaSubappalto = TextBoxSubappaltatrice.Text.Trim();
            filtro.Importo = RadNumericTextBoxImporto.Value;
            filtro.Cap = RadTextBoxCap.Text.Trim();

            // Date
            // Segnalazione
            if (RadDatePickerDataSegnalazioneDal.SelectedDate.HasValue)
            {
                filtro.DataSegnalazioneDal = RadDatePickerDataSegnalazioneDal.SelectedDate.Value;
            }
            if (RadDatePickerDataSegnalazioneAl.SelectedDate.HasValue)
            {
                filtro.DataSegnalazioneAl = RadDatePickerDataSegnalazioneAl.SelectedDate.Value;
            }
            // Assegnazione
            if (RadDatePickerDataAssegnazioneDal.SelectedDate.HasValue)
            {
                filtro.DataAssegnazioneDal = RadDatePickerDataAssegnazioneDal.SelectedDate.Value;
            }
            if (RadDatePickerDataAssegnazioneAl.SelectedDate.HasValue)
            {
                filtro.DataAssegnazioneAl = RadDatePickerDataAssegnazioneAl.SelectedDate.Value;
            }
            // Presa in carico
            if (RadDatePickerDataIspezioneDal.SelectedDate.HasValue)
            {
                filtro.DataPresaCaricoDal = RadDatePickerDataIspezioneDal.SelectedDate.Value;
            }
            if (RadDatePickerDataIspezioneAl.SelectedDate.HasValue)
            {
                filtro.DataPresaCaricoAl = RadDatePickerDataIspezioneAl.SelectedDate.Value;
            }

            //if (!string.IsNullOrEmpty(DropDownListZone.SelectedValue) && DropDownListZone.SelectedValue != "0")
            //{
            //    filtro.IdZona = Int32.Parse(DropDownListZone.SelectedValue);
            //}

            switch (DropDownListStatoSegnalazione.SelectedValue)
            {
                case "SEGNALATI":
                    filtro.Segnalati = true;
                    break;
                case "NONSEGNALATI":
                    filtro.Segnalati = false;
                    break;
            }

            switch (DropDownListStatoPresaCarico.SelectedValue)
            {
                case "Tutti":
                    filtro.PresaInCarico = StatoPresaInCaricoFiltro.Tutti;
                    break;
                case "PresiCarico":
                    filtro.PresaInCarico = StatoPresaInCaricoFiltro.PresiInCarico;
                    break;
                case "NonPresiCarico":
                    filtro.PresaInCarico = StatoPresaInCaricoFiltro.NonPresiInCarico;
                    break;
                case "Rifiutati":
                    filtro.PresaInCarico = StatoPresaInCaricoFiltro.Rifiutati;
                    break;
            }

            if (!String.IsNullOrEmpty(DropDownListAssegnazioneIspettore.SelectedValue))
            {
                filtro.IdIspettoreAssegnato = Int32.Parse(DropDownListAssegnazioneIspettore.SelectedValue);
            }

            if (!String.IsNullOrEmpty(DropDownListPresaInCaricoIspettore.SelectedValue))
            {
                filtro.IdIspettorePresoInCarico = Int32.Parse(DropDownListPresaInCaricoIspettore.SelectedValue);
            }

            if (!String.IsNullOrEmpty(DropDownListStatoAssegnazione.SelectedValue))
            {
                filtro.Assegnati = (StatoAssegnazioneFiltro)Enum.Parse(typeof(StatoAssegnazioneFiltro), DropDownListStatoAssegnazione.SelectedValue);
            }

            switch (DropDownListStatoRapportoIspezione.SelectedValue)
            {
                case "Tutti":
                    filtro.RapportoIspezione = StatoRapportoIspezioneFiltro.Tutti;
                    break;
                case "Ispezione":
                    filtro.RapportoIspezione = StatoRapportoIspezioneFiltro.ConRapportoIspezione;
                    break;
                case "NoIspezione":
                    filtro.RapportoIspezione = StatoRapportoIspezioneFiltro.SenzaRapportoIspezione;
                    break;
            }

            return filtro;
        }

        private CantiereCollection CaricaCantieri()
        {
            CantieriFilter filtro = CreaFiltro();

            ViewState["ordina"] = null;

            CantiereCollection listaCantieri =
                biz.GetCantieri(filtro);

            Presenter.CaricaElementiInGridView(
                RadGridCantieri,
                listaCantieri);

            if (listaCantieri.Count > 0 && GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriAssegnazione))
            {
                ButtonAssegnazioneMultipla.Visible = true;
            }
            else
            {
                ButtonAssegnazioneMultipla.Visible = false;
            }

            return listaCantieri;
        }

        private void CaricaCantieri(string sortExpression)
        {
            CantieriFilter filtro = CreaFiltro();

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "DESC";
                else
                    direct = "ASC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            CantiereCollection listaCantieri =
                biz.GetCantieriOrdinati(filtro, sortExpression, direct);

            Presenter.CaricaElementiInGridView(
                RadGridCantieri,
                listaCantieri);
        }

        private void CaricaCantieriPreservaOrdine(string sortExpression)
        {
            CantieriFilter filtro = CreaFiltro();

            string direct = "ASC";
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');
                if (ord[0] == sortExpression && ord[1] == "ASC")
                    direct = "ASC";
                else
                    direct = "DESC";
            }
            ViewState["ordina"] = sortExpression + "|" + direct;

            CantiereCollection listaCantieri =
                biz.GetCantieriOrdinati(filtro, sortExpression, direct);

            Presenter.CaricaElementiInGridView(
                RadGridCantieri,
                listaCantieri);
        }

        protected void ButtonVisualizza_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                LabelElencoCantieri.Visible = true;
                RadGridCantieri.Visible = true;
                RadGridCantieri.CurrentPageIndex = 0;
                CaricaCantieri();
            }
        }

        protected void ButtonEstrazioneExcel_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CantiereCollection listaCantieri = CaricaCantieri();

                StringWriter sw = PreparaStampa(listaCantieri);

                Response.ClearContent();
                Response.AddHeader("content-disposition", String.Format(@"attachment; filename=Cantieri - {0}.xls", DateTime.Now.ToString("dd.MM.yyyy")));
                Response.ContentType = "application/vnd.ms-excel";
                Response.Write(sw.ToString());
                Response.End();
            }
        }

        protected void ButtonEstrazionePDF_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Context.Items["Filtro"] = CreaFiltro();
                Server.Transfer("~/CeServizi/Cantieri/EstrazioneCantieri.aspx");
            }
        }

        private StringWriter PreparaStampa(CantiereCollection listaCantieri)
        {
            // Purtroppo questo è l'unico modo per recuperare le ispezioni
            foreach (Cantiere cantiere in listaCantieri)
            {
                IspezioniFilter filtro = new IspezioniFilter();
                filtro.IdCantiere = cantiere.IdCantiere;
                cantiere.Ispezioni = biz.GetIspezioni(filtro);
            }

            var gv = new GridView();
            gv.ID = "gvCantieri";
            gv.AutoGenerateColumns = false;
            //gv.RowDataBound += gv_RowDataBound;

            // Indirizzo
            var bc0 = new BoundField();
            bc0.HeaderText = "Indirizzo";
            bc0.DataField = "Indirizzo";
            var bc01 = new BoundField();
            bc01.HeaderText = "Civico";
            bc01.DataField = "Civico";
            var bc02 = new BoundField();
            bc02.HeaderText = "Cap";
            bc02.DataField = "Cap";
            var bc03 = new BoundField();
            bc03.HeaderText = "Località";
            bc03.DataField = "Comune";
            var bc04 = new BoundField();
            bc04.HeaderText = "Provincia";
            bc04.DataField = "Provincia";
            var bc20 = new BoundField();
            bc20.HeaderText = "Segnalazione";
            bc20.DataField = "SegnalazioneStringa";
            var bc21 = new BoundField();
            bc21.HeaderText = "Motivazione";
            bc21.DataField = "SegnalazioneMotivazione";
            var bc21bis = new BoundField();
            bc21bis.HeaderText = "Note";
            bc21bis.DataField = "SegnalazioneNote";
            var bc22 = new BoundField();
            bc22.HeaderText = "Priorita";
            bc22.DataField = "SegnalazionePriorita";
            var bc3 = new BoundField();
            bc3.HeaderText = "Assegnazione";
            bc3.DataField = "AssegnazioneStringa";
            var bc4 = new BoundField();
            bc4.HeaderText = "Presa in Carico";
            bc4.DataField = "PresaInCaricoStringa";
            var bc5 = new BoundField();
            bc5.HeaderText = "Ispezione";
            bc5.DataField = "IspezioneStringa";
            var bc6 = new BoundField();
            bc6.HeaderText = "Importo (euro)";
            bc6.DataField = "Importo";
            var bc7 = new BoundField();
            bc7.HeaderText = "Impresa";
            bc7.DataField = "ImpresaAppaltatriceStringa";
            var bc8 = new BoundField();
            bc8.HeaderText = "Committente";
            bc8.DataField = "CommittenteStringa";
            //bc4.DataFormatString = "{0:dd/MM/yyyy}";
            //bc4.HtmlEncode = false;
            //var bc4bis = new BoundField();
            //bc4bis.HeaderText = "Cellulare";
            //bc4bis.DataField = "LavoratoreCellulare";
            //var bc5 = new BoundField();
            //bc5.HeaderText = "Codice impresa";
            //bc5.DataField = "IdImpresaLavoratore";
            //var bc6 = new BoundField();
            //bc6.HeaderText = "Impresa";
            //bc6.DataField = "ImpresaLavoratore";
            //var bc7 = new BoundField();
            //bc7.HeaderText = "Residenza";
            //bc7.DataField = "ResidenzaLavoratore";
            //var bc8 = new BoundField();
            //bc8.HeaderText = "Cantiere";
            //bc8.DataField = "IndirizzoCantiere";
            //var bc9 = new BoundField();
            //bc9.HeaderText = "Delega da";
            //bc9.DataField = "OperatoreTerritorio";
            //var bc7_1 = new BoundField { HeaderText = "Indirizzo", DataField = "IndirizzoDenominazioneLavoratore" };
            //var bc7_2 = new BoundField { HeaderText = "Comune", DataField = "ComuneLavoratore" };
            //var bc7_3 = new BoundField { HeaderText = "CAP", DataField = "CapLavoratore" };
            //var bc7_4 = new BoundField { HeaderText = "Provincia", DataField = "ProvinciaLavoratore" };

            gv.Columns.Add(bc0);
            gv.Columns.Add(bc01);
            gv.Columns.Add(bc02);
            gv.Columns.Add(bc03);
            gv.Columns.Add(bc04);
            gv.Columns.Add(bc20);
            gv.Columns.Add(bc21);
            gv.Columns.Add(bc21bis);
            gv.Columns.Add(bc22);
            gv.Columns.Add(bc3);
            gv.Columns.Add(bc4);
            gv.Columns.Add(bc5);
            gv.Columns.Add(bc6);
            gv.Columns.Add(bc7);
            gv.Columns.Add(bc8);
            ////if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
            ////{
            //gv.Columns.Add(bc7_1);
            //gv.Columns.Add(bc7_2);
            //gv.Columns.Add(bc7_3);
            //gv.Columns.Add(bc7_4);
            ////}
            ////else
            ////{
            ////    gv.Columns.Add(bc7);
            ////}
            //gv.Columns.Add(bc8);
            //gv.Columns.Add(bc9);

            //if (Request.QueryString["modalita"] != null && Request.QueryString["modalita"] == "storico")
            //{
            //    var bc10 = new BoundField();
            //    bc10.HeaderText = "Mese conferma";
            //    bc10.DataField = "dataConferma";
            //    bc10.HtmlEncode = false;
            //    bc10.DataFormatString = "{0:MM/yyyy}";

            //    gv.Columns.Add(bc10);
            //}

            gv.DataSource = listaCantieri;
            gv.DataBind();

            var sw = new StringWriter();
            var htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            return sw;
        }

        //private void CaricaZone()
        //{
        //    ZonaCollection listaZone = biz.GetZone(null);

        //    DropDownListZone.Items.Clear();
        //    DropDownListZone.Items.Insert(0, new RadComboBoxItem(" Tutti", "0"));
        //    DropDownListZone.DataSource = listaZone;
        //    DropDownListZone.DataTextField = "Nome";
        //    DropDownListZone.DataValueField = "IdZona";

        //    DropDownListZone.DataBind();
        //}

        protected void ButtonNuovo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriInserimentoModificaCantiere.aspx");
        }

        #region Vecchi metodi GridView


        //protected void GridViewIspezioni_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        RapportoIspezione isp = (RapportoIspezione) e.Row.DataItem;
        //        Label lIspezione = (Label) e.Row.FindControl("LabelIspezione");

        //        lIspezione.Text = String.Format("{0}\n{1}", isp.Giorno.ToShortDateString(), isp.Ispettore.Cognome);
        //    }
        //}

        //protected void GridViewCantieri_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    int idCantiere = (int)GridViewCantieri.DataKeys[e.RowIndex].Value;

        //    Response.Redirect("CantieriConfermaEliminazioneCantiere.aspx?idCantiere=" + idCantiere);
        //}

        //protected void GridViewCantieri_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    Int32 idCantiere = -1;

        //    switch (e.CommandName)
        //    {
        //        case "fondi":
        //            idCantiere = (Int32)GridViewCantieri.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Value;

        //            if (biz.NumeroDiProgrammazioniPerCantiere(idCantiere) == 0)
        //            {
        //                LabelErrore.Visible = false;
        //                Response.Redirect("CantieriFusioneCantieri.aspx?idCantiere=" + idCantiere);
        //            }
        //            else
        //                LabelErrore.Visible = true;
        //            break;
        //        case "subappalti":
        //            idCantiere = (Int32)GridViewCantieri.DataKeys[Int32.Parse(e.CommandArgument.ToString())].Value;

        //            Context.Items["IdCantiere"] = idCantiere;
        //            Server.Transfer("~/Cantieri/Subappalti.aspx");
        //            break;
        //    }
        //}

        //protected void GridViewCantieri_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    if (ViewState["ordina"] != null)
        //    {
        //        string[] ord = ((string)ViewState["ordina"]).Split('|');

        //        if (ord[1] == "DESC")
        //            CaricaCantieriPreservaOrdine(ord[0]);
        //        else
        //            CaricaCantieriPreservaOrdine(ord[0]);
        //    }
        //    else
        //        CaricaCantieri();

        //    GridViewCantieri.PageIndex = e.NewPageIndex;
        //    GridViewCantieri.DataBind();
        //}

        //protected void GridViewCantieri_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    //SortDirection dir = SortDirection.Ascending;
        //    //if (e.SortDirection == SortDirection.Ascending)
        //    //    dir = SortDirection.Descending;

        //    CaricaCantieri(e.SortExpression);
        //}

        //protected void GridViewCantieri_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    int idCantiere = (int)GridViewCantieri.DataKeys[e.NewEditIndex].Value;
        //    Cantiere cantiere = biz.GetCantieri(idCantiere, null, null, null, null, null, null, null, null)[0];

        //    Response.Redirect("CantieriLocalizzazione.aspx?idCantiere=" + cantiere.IdCantiere + "&indirizzoCantiere=" +
        //                      cantiere.IndirizzoMappa);
        //}

        //protected void GridViewCantieri_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        Cantiere cantiere = (Cantiere)e.Row.DataItem;

        //        ImageButton ibMappa = (ImageButton)e.Row.FindControl("ImageButtonMappa");

        //        if (cantiere.Latitudine.HasValue && cantiere.Longitudine.HasValue)
        //        {
        //            ibMappa.Enabled = true;
        //            ibMappa.ImageUrl = "~/images/map.png";
        //        }
        //        else
        //        {
        //            ibMappa.Enabled = false;
        //            ibMappa.ImageUrl = "~/images/mapBN.png";
        //        }

        //        // Indirizzo
        //        Label lIndirizzo = (Label)e.Row.FindControl("LabelIndirizzo");
        //        Label lCap = (Label)e.Row.FindControl("LabelCap");
        //        Label lComune = (Label)e.Row.FindControl("LabelComune");
        //        Label lProvincia = (Label)e.Row.FindControl("LabelProvincia");
        //        lIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
        //        lCap.Text = cantiere.Cap;
        //        lComune.Text = cantiere.Comune;
        //        lProvincia.Text = cantiere.Provincia;

        //        // Ispezioni
        //        IspezioniFilter filtro = new IspezioniFilter();
        //        filtro.IdCantiere = cantiere.IdCantiere;
        //        cantiere.Ispezioni = biz.GetIspezioni(filtro);
        //        GridView gvIspezioni = (GridView)e.Row.FindControl("GridViewIspezioni");
        //        gvIspezioni.DataSource = cantiere.Ispezioni;
        //        gvIspezioni.DataBind();

        //        // Impresa
        //        if (cantiere.ImpresaAppaltatrice != null)
        //        {
        //            Label labelImpresa = (Label)e.Row.FindControl("LabelImpresa");
        //            labelImpresa.Text = cantiere.ImpresaAppaltatrice.RagioneSociale;

        //            if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
        //                labelImpresa.ForeColor = Color.Gray;
        //        }
        //        else
        //        {
        //            if (cantiere.ImpresaAppaltatriceTrovata != null)
        //            {
        //                Label labelImpresa = (Label)e.Row.FindControl("LabelImpresa");
        //                labelImpresa.Text = cantiere.ImpresaAppaltatriceTrovata;
        //                labelImpresa.ForeColor = Color.Red;
        //            }
        //        }

        //        // Committente
        //        Label lCommittente = (Label)e.Row.FindControl("LabelCommittente");
        //        if (cantiere.Committente != null)
        //        {
        //            lCommittente.Text = cantiere.Committente.RagioneSociale;
        //        }
        //        else
        //        {
        //            lCommittente.Text = cantiere.CommittenteTrovato;
        //            lCommittente.ForeColor = Color.Red;
        //        }
        //    }
        //}

        //protected void GridViewCantieri_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        //{
        //    int idCantiere = (int)GridViewCantieri.DataKeys[e.NewSelectedIndex].Value;
        //    Cantiere cantiere = biz.GetCantieri(idCantiere, null, null, null, null, null, null, null, null)[0];

        //    if (OnCantiereSelected != null)
        //        OnCantiereSelected(cantiere);
        //}

        #endregion

        //protected void GridViewIspezioni_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        RapportoIspezione isp = (RapportoIspezione)e.Row.DataItem;
        //        Label lIspezione = (Label)e.Row.FindControl("LabelIspezione");

        //        lIspezione.Text = String.Format("{0}\n{1}", isp.Giorno.ToShortDateString(), isp.Ispettore.Cognome);
        //    }
        //}

        protected void RadGridCantieri_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                Cantiere cantiere = (Cantiere)e.Item.DataItem;

                ImageButton ibMappa = (ImageButton)e.Item.FindControl("ImageButtonMappa");
                ImageButton ibNotifica = (ImageButton)e.Item.FindControl("ImageButtonNotifica");

                // Mappa
                if (cantiere.Latitudine.HasValue && cantiere.Longitudine.HasValue)
                {
                    ibMappa.Enabled = true;
                    ibMappa.ImageUrl = "~/CeServizi/images/maps.gif";

                    string comando = String.Format(
                        "openRadWindowMappa('{0}','{1}','{2}','{3}','{4}','{5}','{6}'); return false;",
                        cantiere.Latitudine,
                        cantiere.Longitudine,
                        String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico),
                        cantiere.Cap,
                        cantiere.Comune,
                        cantiere.Provincia,
                        String.Empty);

                    ibMappa.Attributes.Add("OnClick", comando);
                }
                else
                {
                    ibMappa.Enabled = false;
                    ibMappa.ImageUrl = "~/CeServizi/images/mapsBN.gif";
                }

                // Notifica
                if (cantiere.Fonti.ToUpper().Contains("NOTIFICHE"))
                {
                    ibNotifica.Enabled = true;
                    ibNotifica.ImageUrl = "~/CeServizi/images/pdf24.png";
                }
                else
                {
                    ibNotifica.Enabled = false;
                    ibNotifica.ImageUrl = "~/CeServizi/images/pdf24BN.png";
                }

                // Indirizzo
                Label lIndirizzo = (Label)e.Item.FindControl("LabelIndirizzo");
                Label lCap = (Label)e.Item.FindControl("LabelCap");
                Label lComune = (Label)e.Item.FindControl("LabelComune");
                Label lProvincia = (Label)e.Item.FindControl("LabelProvincia");
                lIndirizzo.Text = String.Format("{0} {1}", cantiere.Indirizzo, cantiere.Civico);
                lCap.Text = cantiere.Cap;
                lComune.Text = cantiere.Comune;
                lProvincia.Text = cantiere.Provincia;

                HtmlTableRow trNote = (HtmlTableRow)e.Item.FindControl("trNote");
                if (ViewState["SoloSelezione"] != null)
                {
                    trNote.Visible = false;
                }
                else
                {
                    trNote.Visible = true;

                    // Ispezioni
                    IspezioniFilter filtro = new IspezioniFilter();
                    filtro.IdCantiere = cantiere.IdCantiere;
                    cantiere.Ispezioni = biz.GetIspezioni(filtro);
                    ImageButton ibIspezione = (ImageButton)e.Item.FindControl("ImageButtonIspezione");
                    ibIspezione.Enabled = false;
                    if (cantiere.Ispezioni != null && cantiere.Ispezioni.Count > 0)
                    {
                        ibIspezione.ImageUrl = "~/CeServizi/images/ispezione.png";
                        Label lDataIspezione = (Label)e.Item.FindControl("LabelDataIspezione");
                        StringBuilder stringaIspezione = new StringBuilder();

                        foreach (RapportoIspezione ispezione in cantiere.Ispezioni)
                        {
                            stringaIspezione.AppendLine(String.Format("{0} {1}", ispezione.Giorno.ToString("dd/MM/yyyy"),
                                                                      ispezione.Ispettore.NomeCompleto));
                            lDataIspezione.Text = ispezione.Giorno.ToString("dd/MM/yyyy");
                        }
                        ibIspezione.ToolTip = stringaIspezione.ToString();

                        // Permetto la compilazione o la modifica del rapporto di ispezione
                        // solo se l'ispettore ha precedentemente preso in carico la visita
                        if (cantiere.Ispezioni != null && cantiere.Ispezioni.Count == 1)
                        {
                            if (ViewState["IdIspettore"] != null && (Int32)ViewState["IdIspettore"] == cantiere.Ispezioni[0].Ispettore.IdIspettore)
                            {
                                ibIspezione.PostBackUrl = String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep1.aspx?idIspezione={0}",
                                        cantiere.Ispezioni[0].IdIspezione);
                                ibIspezione.Enabled = true;
                            }
                            else
                            {
                                // Altrimenti faccio andare alla stampa del report se si tratta del rui o altri
                                if (ViewState["IdIspettore"] == null)
                                {
                                    ibIspezione.PostBackUrl = String.Format("~/CeServizi/Cantieri/CantieriIspezioneStampaReport.aspx?idIspezione={0}",
                                        cantiere.Ispezioni[0].IdIspezione);
                                    ibIspezione.Enabled = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        // Permetto la compilazione o la modifica del rapporto di ispezione
                        // solo se l'ispettore ha precedentemente preso in carico la visita
                        if (cantiere.PresaInCarico != null)
                        {
                            if (ViewState["IdIspettore"] != null && (Int32)ViewState["IdIspettore"] == cantiere.PresaInCarico.Ispettore.IdIspettore)
                            {
                                if (cantiere.Ispezioni == null || cantiere.Ispezioni.Count == 0)
                                {
                                    ibIspezione.PostBackUrl = String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep1.aspx?idIspettore={0}&giorno={1}&idCantiere={2}",
                                        (Int32)ViewState["IdIspettore"],
                                        cantiere.PresaInCarico.Data.ToString("dd/MM/yyyy"),
                                        cantiere.IdCantiere);

                                    ibIspezione.ToolTip = "Compila il rapporto di ispezione";
                                    ibIspezione.Enabled = true;
                                }
                            }
                        }
                    }

                    // Segnalazioni
                    ImageButton ibSegnalazione = (ImageButton)e.Item.FindControl("ImageButtonSegnalazione");
                    ibSegnalazione.Enabled = false;
                    if (cantiere.Segnalazione != null)
                    {
                        ibSegnalazione.ImageUrl = "~/CeServizi/images/segnalazione.png";

                        Label lDataSegnalazione = (Label)e.Item.FindControl("LabelDataSegnalazione");
                        lDataSegnalazione.Text = cantiere.Segnalazione.Data.ToString("dd/MM/yyyy");

                        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriSegnalazione.ToString()))
                        {
                            ibSegnalazione.Enabled = true;
                        }
                        ibSegnalazione.ToolTip =
                            String.Format("Cantiere segnalato da {0} in data {1}. Motivazione: {2}. Priorità: {3}. Note: {4}",
                                          cantiere.Segnalazione.NomeUtente,
                                          lDataSegnalazione.Text, cantiere.Segnalazione.Motivazione,
                                          cantiere.Segnalazione.Priorita,
                                          cantiere.Segnalazione.Note);

                        string comando = String.Format(
                                "openRadWindowSegnalazioneModifica('{0}'); return false;",
                                cantiere.IdCantiere);

                        ibSegnalazione.Attributes.Add("OnClick", comando);
                    }
                    else
                    {
                        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriSegnalazione.ToString()))
                        {
                            ibSegnalazione.Enabled = true;

                            ibSegnalazione.ToolTip = "Segnala il cantiere";

                            string comando = String.Format(
                                "openRadWindowSegnalazione('{0}'); return false;",
                                cantiere.IdCantiere);

                            ibSegnalazione.Attributes.Add("OnClick", comando);
                        }
                    }

                    // Gestione dell'assegnazione multipla
                    HtmlTableRow trAssegnazioneMultipla = (HtmlTableRow)e.Item.FindControl("trAssegnazioneMultipla");

                    // Assegnazioni
                    ImageButton ibAssegnazione = (ImageButton)e.Item.FindControl("ImageButtonAssegnazione");
                    ibAssegnazione.Enabled = false;
                    if (cantiere.Assegnazione != null)
                    {
                        Label lDataAssegnazione = (Label)e.Item.FindControl("LabelDataAssegnazione");
                        lDataAssegnazione.Text = cantiere.Assegnazione.Data.ToString("dd/MM/yyyy");

                        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriAssegnazione.ToString()))
                        {
                            ibAssegnazione.Enabled = true;
                        }

                        if (cantiere.Assegnazione.Priorita != null)
                        {
                            // Assegnato
                            ibAssegnazione.ImageUrl = "~/CeServizi/images/assegnato.png";
                            ibAssegnazione.ToolTip = String.Format("Cantiere assegnato in data {0}. Priorità: {1}",
                                                                   lDataAssegnazione.Text, cantiere.Assegnazione.Priorita);

                            TBridge.Cemi.Type.Domain.CantieriAssegnazione assegnazione = bizEF.GetAssegnazione(cantiere.Assegnazione.IdAssegnazione);
                            foreach (TBridge.Cemi.Type.Domain.Ispettore isp in assegnazione.Ispettori)
                            {
                                ibAssegnazione.ToolTip = String.Format("{0}\n{1}", ibAssegnazione.ToolTip, isp.NomeCompleto);
                            }
                        }
                        else
                        {
                            // Rifiutato
                            ibAssegnazione.ImageUrl = "~/CeServizi/images/assegnatoNessuno.png";
                            ibAssegnazione.ToolTip = String.Format("Cantiere rifiutato in data {0}. Motivazione: {1}",
                                                                   lDataAssegnazione.Text,
                                                                   cantiere.Assegnazione.MotivazioneRifiuto);
                        }

                        string comando = String.Format(
                                "openRadWindowAssegnazioneModifica('{0}'); return false;",
                                cantiere.IdCantiere);

                        ibAssegnazione.Attributes.Add("OnClick", comando);
                    }
                    else
                    {
                        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriAssegnazione.ToString()))
                        {
                            ibAssegnazione.Enabled = true;
                            trAssegnazioneMultipla.Visible = true;

                            ibAssegnazione.ToolTip = "Assegna il cantiere";

                            string comando = String.Format(
                                "openRadWindowAssegnazione('{0}'); return false;",
                                cantiere.IdCantiere);

                            ibAssegnazione.Attributes.Add("OnClick", comando);
                        }
                    }

                    // Presa in carico
                    ImageButton ibPresaInCarico = (ImageButton)e.Item.FindControl("ImageButtonProgrammazione");
                    ibPresaInCarico.Enabled = false;
                    if (cantiere.PresaInCarico != null)
                    {
                        ibPresaInCarico.ImageUrl = "~/CeServizi/images/programmato.png";
                        Label lDataProgrammazione = (Label)e.Item.FindControl("LabelDataProgrammazione");

                        lDataProgrammazione.Text = cantiere.PresaInCarico.DataPresaInCarico.ToString("dd/MM/yyyy");
                        if (cantiere.PresaInCarico.Rifiuto == null)
                        {
                            if (String.IsNullOrEmpty(cantiere.PresaInCarico.Descrizione))
                            {
                                ibPresaInCarico.ToolTip = String.Format("Ispezione programmata per il {0}, in carico a {1} {2}",
                                    cantiere.PresaInCarico.Data.ToString("dd/MM/yyyy"),
                                    cantiere.PresaInCarico.Ispettore.Cognome,
                                    cantiere.PresaInCarico.Ispettore.Nome);
                            }
                            else
                            {
                                ibPresaInCarico.ToolTip = String.Format("Ispezione programmata per il {0}, in carico a {1} {2}. Nota: {3}",
                                    cantiere.PresaInCarico.Data.ToString("dd/MM/yyyy"),
                                    cantiere.PresaInCarico.Ispettore.Cognome,
                                    cantiere.PresaInCarico.Ispettore.Nome,
                                    cantiere.PresaInCarico.Descrizione);
                            }
                        }
                        else
                        {
                            ibPresaInCarico.ImageUrl = "~/CeServizi/images/programmatoRifiutato.png";

                            if (String.IsNullOrEmpty(cantiere.PresaInCarico.Descrizione))
                            {
                                ibPresaInCarico.ToolTip = String.Format("Ispezione programmata per il {0}, in carico a {1} {2}. Rifiutata per: {3}",
                                    cantiere.PresaInCarico.Data.ToString("dd/MM/yyyy"),
                                    cantiere.PresaInCarico.Ispettore.Cognome,
                                    cantiere.PresaInCarico.Ispettore.Nome,
                                    cantiere.PresaInCarico.Rifiuto.Descrizione);
                            }
                            else
                            {
                                ibPresaInCarico.ToolTip = String.Format("Ispezione programmata per il {0}, in carico a {1} {2}. Nota: {3}. Rifiutata per: {4}",
                                    cantiere.PresaInCarico.Data.ToString("dd/MM/yyyy"),
                                    cantiere.PresaInCarico.Ispettore.Cognome,
                                    cantiere.PresaInCarico.Ispettore.Nome,
                                    cantiere.PresaInCarico.Descrizione,
                                    cantiere.PresaInCarico.Rifiuto.Descrizione);
                            }
                        }

                        if (GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString()))
                        {
                            ibPresaInCarico.Enabled = true;

                            string comando = String.Format(
                                "openRadWindowCambioPresaInCarico('{0}'); return false;",
                                cantiere.PresaInCarico.Id);

                            ibPresaInCarico.Attributes.Add("OnClick", comando);
                        }
                    }

                    // Impresa
                    if (cantiere.ImpresaAppaltatrice != null)
                    {
                        Label labelImpresa = (Label)e.Item.FindControl("LabelImpresa");
                        labelImpresa.Text = cantiere.ImpresaAppaltatrice.RagioneSociale;

                        if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                            labelImpresa.ForeColor = Color.Gray;
                    }
                    else
                    {
                        if (cantiere.ImpresaAppaltatriceTrovata != null)
                        {
                            Label labelImpresa = (Label)e.Item.FindControl("LabelImpresa");
                            labelImpresa.Text = cantiere.ImpresaAppaltatriceTrovata;
                            labelImpresa.ForeColor = Color.Red;
                        }
                    }
                }

                // Committente
                Label lCommittente = (Label)e.Item.FindControl("LabelCommittente");
                if (cantiere.Committente != null)
                {
                    lCommittente.Text = cantiere.Committente.RagioneSociale;
                }
                else
                {
                    lCommittente.Text = cantiere.CommittenteTrovato;
                    lCommittente.ForeColor = Color.Red;
                }
            }
        }

        protected void RadGridCantieri_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            if (ViewState["ordina"] != null)
            {
                string[] ord = ((string)ViewState["ordina"]).Split('|');

                if (ord[1] == "DESC")
                    CaricaCantieriPreservaOrdine(ord[0]);
                else
                    CaricaCantieriPreservaOrdine(ord[0]);
            }
            else
                CaricaCantieri();
        }

        protected void RadGridCantieri_SortCommand(object source, GridSortCommandEventArgs e)
        {
            CaricaCantieri(e.SortExpression);
        }

        protected void RadGridCantieri_ItemCommand(object source, GridCommandEventArgs e)
        {
            Int32 idCantiere = -1;

            switch (e.CommandName)
            {
                case "fondi":
                    idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];

                    if (biz.NumeroDiProgrammazioniPerCantiere(idCantiere) == 0)
                    {
                        LabelErrore.Visible = false;
                        Response.Redirect("~/CeServizi/Cantieri/CantieriFusioneCantieri.aspx?idCantiere=" + idCantiere);
                    }
                    else
                        LabelErrore.Visible = true;
                    break;
                case "subappalti":
                    idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];

                    Context.Items["IdCantiere"] = idCantiere;
                    Server.Transfer("~/CeServizi/Cantieri/Subappalti.aspx");
                    break;
                case "mappa":
                    // Gestito via JavaScript
                    break;
                case "modifica":
                    idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];
                    Cantiere cantiere = biz.GetCantiere(idCantiere);

                    if (OnCantiereSelected != null)
                        OnCantiereSelected(cantiere);
                    break;
                case "elimina":
                    idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];
                    Response.Redirect(String.Format("~/CeServizi/Cantieri/CantieriConfermaEliminazioneCantiere.aspx?idCantiere={0}", idCantiere));
                    break;
                case "segnala":
                    // Gestito via JavaScript
                    break;
                case "scheda":
                    idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];
                    Context.Items["IdCantiere"] = idCantiere;
                    Server.Transfer("~/CeServizi/Cantieri/SchedaCantiere.aspx");
                    break;
                case "notifica":
                    idCantiere = (Int32)RadGridCantieri.MasterTableView.DataKeyValues[e.Item.ItemIndex]["IdCantiere"];
                    Cantiere cantiereNot = biz.GetCantiere(idCantiere);

                    if (cantiereNot.NotificaRiferimento.HasValue)
                    {
                        CptBusiness cptBiz = new CptBusiness();
                        TBridge.Cemi.Cpt.Type.Entities.NotificaTelematica notifica = cptBiz.GetNotificaTelematicaUltimaVersione(cantiereNot.NotificaRiferimento.Value);

                        Server.Transfer(String.Format("~/CeServizi/Cantieri/ReportCpt.aspx?idNotifica={0}", notifica.IdNotifica));
                    }
                    else
                    {
                        Server.Transfer(
                            String.Format("~/CeServizi/NotifichePreliminari/ReportNotifica.aspx?idNotifica={0}&visualizzaAltriLavoratori={1}",
                                Server.UrlEncode(cantiereNot.ProtocolloRegionaleNotifica),
                                TBridge.Cemi.GestioneUtenti.Business.GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.NotifichePreliminariDatiPersone)));
                    }
                    break;
            }
        }

        protected String RecuperaPostBackCode()
        {
            return this.Page.GetPostBackEventReference(this, "@@@@@buttonPostBackRefresh");
        }

        protected void DropDownListStatoAssegnazione_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value == "Assegnati")
            {
                DropDownListAssegnazioneIspettore.Enabled = true;
            }
            else
            {
                DropDownListAssegnazioneIspettore.ClearSelection();
                DropDownListAssegnazioneIspettore.Enabled = false;
            }
        }

        protected void DropDownListStatoPresaCarico_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (e.Value == "PresiCarico")
            {
                DropDownListPresaInCaricoIspettore.Enabled = true;
            }
            else
            {
                DropDownListPresaInCaricoIspettore.ClearSelection();
                DropDownListPresaInCaricoIspettore.Enabled = false;
            }
        }

        protected void CustomValidatorValidatorDataSegnalazione_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadDatePickerDataSegnalazioneDal.SelectedDate.HasValue
                && RadDatePickerDataSegnalazioneAl.SelectedDate.HasValue)
            {
                if (RadDatePickerDataSegnalazioneDal.SelectedDate.Value > RadDatePickerDataSegnalazioneAl.SelectedDate.Value)
                {
                    args.IsValid = false;
                }
            }
        }

        protected void CustomValidatorValidatorDataAssegnazione_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadDatePickerDataAssegnazioneDal.SelectedDate.HasValue
                && RadDatePickerDataAssegnazioneAl.SelectedDate.HasValue)
            {
                if (RadDatePickerDataAssegnazioneDal.SelectedDate.Value > RadDatePickerDataAssegnazioneAl.SelectedDate.Value)
                {
                    args.IsValid = false;
                }
            }
        }

        protected void CustomValidatorValidatorDataIspezione_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadDatePickerDataIspezioneDal.SelectedDate.HasValue
                && RadDatePickerDataIspezioneAl.SelectedDate.HasValue)
            {
                if (RadDatePickerDataIspezioneDal.SelectedDate.Value > RadDatePickerDataIspezioneAl.SelectedDate.Value)
                {
                    args.IsValid = false;
                }
            }
        }

        protected void ButtonAssegnazioneMultipla_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                List<Int32> cantieri = new List<Int32>();

                foreach (GridDataItem it in RadGridCantieri.Items)
                {
                    CheckBox cbAssegnazioneMultiple = (CheckBox)it.FindControl("CheckBoxAssegnazioneMultipla");

                    if (cbAssegnazioneMultiple.Checked)
                    {
                        Int32 idCantiere = (Int32)it.GetDataKeyValue("IdCantiere");
                        cantieri.Add(idCantiere);
                    }
                }

                ButtonAssegnazioneMultipla.Attributes.Add("onclick", String.Format("var oWindow = radopen(\"../Cantieri/AssegnaCantiere.aspx?idCantiere={0}\", null); oWindow.set_title(\"Nuova Assegnazione\"); oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close); oWindow.setSize(500, 500); oWindow.center(); oWindow.add_close(OnClientClose); return false;", ListaCantieri(cantieri)));
                // Per lanciare automaticamente la finestra dell'assegnazione (per quella multipla)
                //ClientScriptManager cs = Page.ClientScript;
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "auto_postback", "window.onload = function() { " + String.Format("var oWindow = radopen(\"../Cantieri/AssegnaCantiere.aspx?idCantiere={0}\", null); oWindow.set_title(\"Nuova Assegnazione\"); oWindow.set_behaviors(Telerik.Web.UI.WindowBehaviors.Close); oWindow.setSize(500, 500); oWindow.center(); oWindow.add_close(OnClientClose); return false;", ListaCantieri(cantieri)) + "; }; ", true);

                //Telerik.Web.UI.RadWindow newWindow = new Telerik.Web.UI.RadWindow();
                //newWindow.NavigateUrl = String.Format("../Cantieri/AssegnaCantiere.aspx?idCantiere={0}", ListaCantieri(cantieri));
                ////Set OpenerElementId - the id (ClientID if a runat=server is used) of a html element, which, when clicked, will automatically open/show the Telerik RadWindow
                //newWindow.OpenerElementID = ButtonAssegnazioneMultipla.ClientID;
                ////Set OffsetElementID - the id (ClientID if a runat=server is used) of a html element, whose left and top position will be used as 0,0 of the Telerik RadWindow object when it is first shown
                //newWindow.OffsetElementID = ButtonAssegnazioneMultipla.ClientID;
                //newWindow.CenterIfModal = true;
                //newWindow.Width = new Unit(500, UnitType.Pixel);
                //newWindow.Height = new Unit(500, UnitType.Pixel);
                //newWindow.Behaviors = Telerik.Web.UI.WindowBehaviors.Close;
                //newWindow.Title = "Nuova Assegnazione";
                //newWindow.Modal = true;
                //newWindow.OnClientClose = "OnClientClose";
                //RadWindowManager1.Windows.Add(newWindow);
            }
        }

        private String ListaCantieri(List<Int32> cantieri)
        {
            StringBuilder lista = new StringBuilder();

            foreach (Int32 cantiere in cantieri)
            {
                lista.Append(String.Format("{0},", cantiere));
            }

            String listString = lista.ToString();
            return listString.Substring(0, listString.Length - 1);
        }

        protected void CustomValidatorAssegnazioneMultipla_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadGridCantieri.Items != null)
            {
                Boolean unoCheckato = false;

                foreach (GridDataItem it in RadGridCantieri.Items)
                {
                    CheckBox cbAssegnazioneMultiple = (CheckBox)it.FindControl("CheckBoxAssegnazioneMultipla");

                    if (cbAssegnazioneMultiple.Checked)
                    {
                        unoCheckato = true;
                        break;
                    }
                }

                if (!unoCheckato)
                {
                    args.IsValid = false;
                }
            }
        }
    }
}