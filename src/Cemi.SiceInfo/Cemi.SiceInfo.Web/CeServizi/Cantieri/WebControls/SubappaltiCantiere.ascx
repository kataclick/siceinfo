﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubappaltiCantiere.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.WebControls.SubappaltiCantiere" %>
    
<%@ Register Src="../WebControls/CantieriImpresa.ascx" TagName="CantieriImpresa" TagPrefix="uc5" %>
<%@ Register Src="../WebControls/CantieriRicercaImpresaUnicaFonte.ascx" TagName="CantieriRicercaImpresaUnicaFonte" TagPrefix="uc4" %>


    <script type="text/javascript">

    function onEndRequest(sender, e) {
        var err = e.get_error();
        e.set_errorHandled(true);
        if (err != null) {
            if ((err.name == 'Sys.WebForms.PageRequestManagerTimeoutException'
                     || err.name == 'Sys.WebForms.PageRequestManagerServerErrorException')) {
                window.alert('Attenzione, il server non risponde o la connessione è caduta');
            }
            else {
                window.alert('Attenzione, errore generico');
            }
        }
    }

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onEndRequest); 

    </script>

    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
        <div>
            <table class="standardTable">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Lista Appalti" Font-Bold="True"></asp:Label>
                        <telerik:RadGrid
                            ID="RadGridSubappalti"
                            runat="server"
                            Width="100%" GridLines="None" 
                            onitemdatabound="RadGridSubappalti_ItemDataBound" 
                            onitemcommand="RadGridSubappalti_ItemCommand" >
                            <MasterTableView DataKeyNames="IdSubappalto">
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Subappaltatrice" 
                                        UniqueName="subappaltatrice">
                                        <ItemTemplate>
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <b>
                                                            <asp:Label ID="LabelSubappaltatriceRagioneSociale" runat="server" ForeColor="Black">
                                                            </asp:Label>
                                                        </b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><asp:Label ID="LabelSubappaltatriceLegale" runat="server" Text="Leg:"></asp:Label></b>
                                                        <asp:Label ID="LabelSubappaltatriceIndirizzo" runat="server" Font-Size="XX-Small"
                                                            ForeColor="Black"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><asp:Label ID="LabelSubappaltatriceAmministrativo" runat="server" Text="Ammi:"></asp:Label></b>
                                                        <asp:Label ID="LabelSubappaltatriceIndirizzoAmmi" runat="server" Font-Size="XX-Small"
                                                            ForeColor="Black">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="350px" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Appaltata da" 
                                        UniqueName="appaltataDa">
                                        <ItemTemplate>
                                            <table class="standardTable">
                                                <tr>
                                                    <td>
                                                        <b>
                                                            <asp:Label ID="LabelSubappaltataRagioneSociale" runat="server" ForeColor="Black"></asp:Label>
                                                        </b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><asp:Label ID="LabelSubappaltataLegale" runat="server" Text="Leg:"></asp:Label></b>
                                                        <asp:Label ID="LabelSubappaltataIndirizzo" runat="server" Font-Size="XX-Small" ForeColor="Black">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b><asp:Label ID="LabelSubappaltataAmministrativo" runat="server" Text="Ammi:"></asp:Label></b>
                                                        <asp:Label ID="LabelSubappaltataIndirizzoAmmi" runat="server" Font-Size="XX-Small"
                                                            ForeColor="Black">
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="350px" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Tipologia contratto" 
                                        UniqueName="tipologiaContratto">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DropDownListTipoContratto" runat="server" Width="150px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle Width="260px" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="" 
                                        UniqueName="modifica">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButtonModifica" runat="server" CommandName="modifica" ImageUrl="../../images/edit.png" />
                                        </ItemTemplate>
                                        <ItemStyle Width="25px" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="" 
                                        UniqueName="elimina">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButtonDelete" runat="server" CommandName="elimina" ImageUrl="../../images/pallinoX.png" />
                                        </ItemTemplate>
                                        <ItemStyle Width="25px" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                        <asp:Button ID="ButtonVisualizzaAppalto" runat="server" OnClick="ButtonVisualizzaAppalto_Click"
                            Text="Crea nuovo appalto" Width="150px" CausesValidation="False" /><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="PanelAppalti" runat="server" Width="100%" Visible="False">
                            <table class="filledtable">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Crea appalto"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table class="borderedTable">
                                <tr>
                                    <td>
                                        Appaltata da
                                    </td>
                                    <td>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltatriceSiceInfo" runat="server" Text="Anagrafica SiceNew"
                                                        Enabled="False" GroupName="scelta" />
                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltatriceCantieri" runat="server" Text="Anagrafica Cantieri"
                                                        Enabled="False" GroupName="scelta" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelAppaltataDa" runat="server" Visible="true" Width="100%">
                                                        <table class="standardTable">
                                                            <tr>
                                                                <td class="cantieriTd">
                                                                    <asp:TextBox ID="TextBoxImpresaAppaltatrice" runat="server" Text="" 
                                                                        Width="300px" Height="57px"
                                                                        TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="ButtonModificaAppaltatrice" runat="server" Text="Modifica dati" CausesValidation="False"
                                                                        Enabled="False" OnClick="ButtonModificaAppaltatrice_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="iscrizioneLavoratoriTd">
                                                                    <asp:Button ID="ButtonSelezionaAppaltatrice" runat="server" Text="Seleziona" OnClick="ButtonSelezionaAppaltatrice_Click"
                                                                        CausesValidation="False" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <asp:Panel ID="PanelAppaltataDaModifica" runat="server" Visible="false" Width="100%">
                                                        <table class="borderedTable">
                                                            <tr>
                                                                <td>
                                                                    <uc5:CantieriImpresa ID="CantieriImpresaModificaSubappaltatrice" runat="server" />
                                                                    <br />
                                                                    <asp:Button ID="ButtonModificaImpresaSubappaltatrice" runat="server" Text="Modifica impresa"
                                                                        Width="170" OnClick="ButtonModificaImpresaSubappaltatrice_Click" />
                                                                    <asp:Button ID="ButtonModificaImpresaSubappaltatriceAnnulla" runat="server" Text="Annulla"
                                                                        Width="170" CausesValidation="False" OnClick="ButtonModificaImpresaSubappaltatriceAnnulla_Click" />
                                                                    <br />
                                                                    <asp:Label ID="LabelModificaImpresaSubappaltatriceErrore" runat="server" ForeColor="Red"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelImpresaDropDown" runat="server" Width="100%" Visible="false">
                                                        <asp:DropDownList ID="DropDownListImpresa" runat="server" Width="300px">
                                                        </asp:DropDownList>
                                                        <asp:Button ID="ButtonSelezionaImpresaDaDropDownList" runat="server" CausesValidation="False"
                                                            OnClick="ButtonSelezionaImpresaDaDropDownList_Click" Text="Seleziona" Width="125px" /><br />
                                                        Se l'impresa a cui il lavoratore ha dichiarato di appartenere non è nella lista,
                                                        premere:&nbsp;
                                                        <asp:Button ID="ButtonAltro" runat="server" Text="Altra impresa" Width="102px" OnClick="ButtonAltro_Click" />
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Panel ID="PanelRicercaImpresaNuovaImpresa" runat="server" Width="100%" Visible="false">
                                                        <uc4:CantieriRicercaImpresaUnicaFonte ID="CantieriRicercaImpresaAppaltataDa" runat="server"
                                                            Visible="false" />
                                                        <br />
                                                        <asp:Panel ID="PanelInserisciImpresaAppaltataDa" runat="server" Width="100%" Visible="false">
                                                            <table class="filledtable">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="borderedTable">
                                                                <tr>
                                                                    <td>
                                                                        <uc5:CantieriImpresa ID="CantieriImpresaAppaltataDa" runat="server" />
                                                                        <asp:Button ID="ButtonInserisciNuovaImpresaAppaltataDa" runat="server" Text="Inserisci nuova impresa"
                                                                            Width="170" OnClick="ButtonInserisciNuovaImpresaAppaltataDa_Click" />
                                                                        <asp:Button ID="ButtonAnnullaNuovaImpresaAppaltataDa" runat="server" Text="Annulla"
                                                                            Width="170" OnClick="ButtonAnnullaNuovaImpresaAppaltataDa_Click" CausesValidation="False" />
                                                                        <br />
                                                                        <asp:Label ID="LabelNuovaAppaltataDaRes" runat="server" ForeColor="Red"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <br />
                                                    </asp:Panel>
                                                    <br />
                                                    &nbsp; &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Subappaltatrice
                                    </td>
                                    <td>
                                        <table class="standardTable">
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltataSiceInfo" runat="server" Text="Anagrafica SiceNew"
                                                        Enabled="False" GroupName="scelta" />
                                                    <asp:RadioButton ID="RadioButtonImpresaAppaltataCantieri" runat="server" Text="Anagrafica Cantieri"
                                                        Enabled="False" GroupName="scelta" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="standardTable">
                                                        <tr>
                                                            <td>
                                                                <asp:Panel ID="PanelAppaltata" runat="server" Visible="true" Width="100%">
                                                                    <table class="standardTable">
                                                                        <tr>
                                                                            <td class="cantieriTd">
                                                                                <asp:TextBox ID="TextBoxImpresaAppaltata" runat="server" Text="" Width="300px" Height="57px"
                                                                                    TextMode="MultiLine" Enabled="False" ReadOnly="True" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="ButtonModificaImpresaAppaltata" runat="server" Text="Modifica dati"
                                                                                    CausesValidation="False" Enabled="False" OnClick="ButtonModificaImpresaAppaltata_Click" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="LabelTipologiaContratto" runat="server" Text="Tipologia contratto"
                                                                                    Width="40%"></asp:Label>
                                                                                <asp:DropDownList ID="DropDownListTipoContratto" runat="server" Width="58%">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="ButtonSelezionaAppaltata" runat="server" Text="Seleziona" OnClick="ButtonSelezionaAppaltata_Click"
                                                                                    CausesValidation="False" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBoxImpresaAppaltata"
                                                                                    ErrorMessage="*" ValidationGroup="aggiungiAppalto"></asp:RequiredFieldValidator>&nbsp;
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                                <asp:Panel ID="PanelAppaltataModifica" runat="server" Visible="false" Width="100%">
                                                                    <table class="borderedTable">
                                                                        <tr>
                                                                            <td>
                                                                                <uc5:CantieriImpresa ID="CantieriImpresaModificaAppaltata" runat="server" />
                                                                                <br />
                                                                                <asp:Button ID="ButtonModificaImpresaSubappaltata" runat="server" Text="Modifica impresa"
                                                                                    Width="170" OnClick="ButtonModificaImpresaSubappaltata_Click" />
                                                                                <asp:Button ID="ButtonModificaImpresaSubappaltataAnnulla" runat="server" Text="Annulla"
                                                                                    Width="170" CausesValidation="False" OnClick="ButtonModificaImpresaSubappaltataAnnulla_Click" />
                                                                                <br />
                                                                                <asp:Label ID="LabelModificaImpresaSubappaltataErrore" runat="server" ForeColor="Red"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc4:CantieriRicercaImpresaUnicaFonte ID="CantieriRicercaImpresaSubappaltatrice"
                                                        runat="server" Visible="false" />
                                                    <br />
                                                    <asp:Panel ID="PanelInserisciImpresaSubappaltata" runat="server" Width="100%"
                                                        Visible="false">
                                                        <table class="filledtable">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="Inserimento nuova impresa"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table class="borderedTable">
                                                            <tr>
                                                                <td>
                                                                    <uc5:CantieriImpresa ID="CantieriImpresaSubappaltatrice" runat="server" />
                                                                    <br />
                                                                    <asp:Button ID="ButtonInserisciNuovaImpresaSubappaltatrice" runat="server" Text="Inserisci nuova impresa"
                                                                        Width="170" OnClick="ButtonInserisciNuovaImpresaSubappaltatrice_Click" />
                                                                    <asp:Button ID="ButtonAnnullaNuovaImpresaSubappaltatrice" runat="server" Text="Annulla"
                                                                        Width="170" OnClick="ButtonAnnullaNuovaImpresaSubappaltatrice_Click" />
                                                                    <br />
                                                                    <asp:Label ID="LabelNuovaSubappaltatriceRes" runat="server" ForeColor="Red"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CustomValidator
                                            ID="CustomValidatorImpresaSubappaltatriceSelezionata"
                                            runat="server"
                                            ValidationGroup="aggiungiAppalto" 
                                            onservervalidate="CustomValidatorImpresaSubappaltatriceSelezionata_ServerValidate">
                                            &nbsp;
                                        </asp:CustomValidator>
                                        <asp:ValidationSummary
                                            ID="ValidationSummaryNuovoAppalto"
                                            runat="server"
                                            CssClass="messaggiErrore"
                                            ValidationGroup="aggiungiAppalto" />
                                        <br />
                                        <asp:Button ID="ButtonAggiungiAppalto" runat="server" OnClick="ButtonAggiungiAppalto_Click"
                                            Text="Salva appalto" ValidationGroup="aggiungiAppalto" Width="150px" />
                                        <asp:Button ID="ButtonAnnulla" runat="server" OnClick="ButtonAnnulla_Click" Text="Annulla"
                                            Width="150px" CausesValidation="False" />
                                        <asp:Label ID="LabelRisultatoAppalto" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadAjaxPanel>