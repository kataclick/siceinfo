﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;

using Telerik.Web.UI;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using CassaEdile = TBridge.Cemi.Type.Entities.CassaEdile;
using CassaEdileCollection = TBridge.Cemi.Type.Collections.CassaEdileCollection;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriRapportoIspezioneStep4 : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly Common commonBiz = new Common();
        private RapportoIspezione ispezione;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            //((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("maindiv")).Attributes.Add("style", "width: 1340px;");
            //((System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("outerdiv")).Attributes.Add("style", "width: 1600px;");

            CantieriRicercaImpresaUnicaFonteAggiungiImpresa.OnImpresaSelected +=
                CantieriRicercaImpresaUnicaFonteAggiungiImpresa_OnImpresaSelected;

            #region Impostiamo gli eventi JS per la gestione dei click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('Step4') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalva, null) + ";");
            sb.Append("return true;");
            ButtonSalva.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaDati();

                GridViewOSS.DataSource = biz.GetOSS();
                GridViewOSS.DataBind();

                GridViewRAC.DataSource = biz.GetRAC();
                GridViewRAC.DataBind();
            }

            ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);
            ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonPassoPrecedente);
        }

        private void CaricaDati()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idAttivita"]) || !String.IsNullOrEmpty(Request.QueryString["idIspezione"]))
            {
                CaricaEsiti();
                CaricaComunicatoA();
                //CaricaCasseEdili();
                //CaricaContratti();

                if (!String.IsNullOrEmpty(Request.QueryString["idAttivita"]))
                {
                    int idAttivita = Int32.Parse(Request.QueryString["idAttivita"]);
                    ispezione = biz.GetIspezione(idAttivita);
                }
                else
                {
                    int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
                    ispezione = biz.GetIspezioneByKey(idIspezione);
                }

                if (ispezione != null)
                {
                    CantieriTestataIspezione1.ImpostaTestata(ispezione);

                    ispezione.RapportiImpresa =
                        biz.GetIspezioniImprese(ispezione.IdIspezione.Value, ispezione.Cantiere.IdCantiere.Value);

                    //ispezione.CasseEdili = biz.GetCasseEdiliIspezione(ispezione.IdIspezione.Value);
                    //ispezione.Contratti = biz.GetContrattiIspezione(ispezione.IdIspezione.Value);

                    CaricaDatiIspezione();

                    ispezione.Cantiere.PresaInCarico = null;
                    ViewState["Ispezione"] = ispezione;
                }
            }
        }

        private void CaricaDatiIspezione()
        {
            DropDownListEsito.Text = ispezione.Esito.ToString();
            DropDownListComunicato.Text = ispezione.ComunicatoA.ToString();
            TextBoxNumeroRilievi.Text = ispezione.NumRilievi.ToString();
            TextBoxNumeroOsservazioni.Text = ispezione.NumOsservazioni.ToString();
            TextBoxNumeroRAC.Text = ispezione.NumRAC.ToString();
            TextBoxNote.Text = ispezione.Note;
            if (ispezione.ComunicatoIl.HasValue)
                TextBoxComunicatoIl.Text = ispezione.ComunicatoIl.Value.ToShortDateString();

            if (ispezione.NoteIspettore1 != null)
                TextBoxNoteIspettore1.Text = ispezione.NoteIspettore1;
            if (ispezione.Audit1Dalle.HasValue)
                TextBoxNote1Dalle.Text = ispezione.Audit1Dalle.Value.ToString("HH:mm");
            if (ispezione.Audit1Alle.HasValue)
                TextBoxNote1Alle.Text = ispezione.Audit1Alle.Value.ToString("HH:mm");
            if (ispezione.Audit1Data.HasValue)
                TextBoxNote1Data.Text = ispezione.Audit1Data.Value.ToShortDateString();
            if (ispezione.NoteIspettore2 != null)
                TextBoxNoteIspettore2.Text = ispezione.NoteIspettore2;
            if (ispezione.Audit2Dalle.HasValue)
                TextBoxNote2Dalle.Text = ispezione.Audit2Dalle.Value.ToString("HH:mm");
            if (ispezione.Audit2Alle.HasValue)
                TextBoxNote2Alle.Text = ispezione.Audit2Alle.Value.ToString("HH:mm");
            if (ispezione.Audit2Data.HasValue)
                TextBoxNote2Data.Text = ispezione.Audit2Data.Value.ToShortDateString();

            GridViewRapportiImpresa.DataSource = ispezione.RapportiImpresa;
            GridViewRapportiImpresa.DataBind();

            //ViewState["CasseEdili"] = ispezione.CasseEdili;
            //ViewState["Contratti"] = ispezione.Contratti;

            //CaricaContrattiSelezionati();
            //CaricaCasseEdiliSelezionate();
        }

        private void CaricaEsiti()
        {
            DropDownListEsito.DataSource = Enum.GetNames(typeof(EsitoIspezione));
            DropDownListEsito.DataBind();
        }

        private void CaricaComunicatoA()
        {
            DropDownListComunicato.DataSource = Enum.GetNames(typeof(ComunicazioneRapporto));
            DropDownListComunicato.DataBind();
        }

        protected void GridViewRapportiImpresa_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RapportoIspezioneImpresa ispImpresa = (RapportoIspezioneImpresa)e.Row.DataItem;

                DropDownList ddOSS = (DropDownList)e.Row.FindControl("DropDownListOSS");
                ddOSS.DataSource = biz.GetOSS();
                ddOSS.DataTextField = "IdOSS";
                ddOSS.DataValueField = "IdOSS";
                ddOSS.DataBind();
                GridView gvOSS = (GridView)e.Row.FindControl("GridViewOSS");
                ViewState["oss" + e.Row.RowIndex] = ispImpresa.Oss;
                gvOSS.DataSource = ViewState["oss" + e.Row.RowIndex];
                gvOSS.DataBind();

                DropDownList ddRAC = (DropDownList)e.Row.FindControl("DropDownListRAC");
                ddRAC.DataSource = biz.GetRAC();
                ddRAC.DataTextField = "IdRAC";
                ddRAC.DataValueField = "IdRAC";
                ddRAC.DataBind();
                GridView gvRAC = (GridView)e.Row.FindControl("GridViewRAC");
                ViewState["rac" + e.Row.RowIndex] = ispImpresa.Rac;
                gvRAC.DataSource = ViewState["rac" + e.Row.RowIndex];
                gvRAC.DataBind();

                DropDownList ddCasseEdili = (DropDownList)e.Row.FindControl("DropDownListCassaEdile");
                CassaEdileCollection casseEdili = commonBiz.GetCasseEdiliSenzaMilano();
                Presenter.CaricaElementiInDropDown(
                    ddCasseEdili,
                    casseEdili,
                    "Descrizione",
                    "IdCassaEdile");
                GridView gvCasseEdili = (GridView)e.Row.FindControl("GridViewCasseEdili");
                ViewState["casseedili" + e.Row.RowIndex] = ispImpresa.CasseEdili;
                gvCasseEdili.DataSource = ViewState["casseedili" + e.Row.RowIndex];
                gvCasseEdili.DataBind();

                DropDownList ddContratti = (DropDownList)e.Row.FindControl("DropDownListContratto");
                ContrattoCollection contratti = biz.GetContratti();
                Presenter.CaricaElementiInDropDown(
                    ddContratti,
                    contratti,
                    "Descrizione",
                    "Id");
                GridView gvContratti = (GridView)e.Row.FindControl("GridViewContratti");
                ViewState["contratti" + e.Row.RowIndex] = ispImpresa.Contratti;
                gvContratti.DataSource = ViewState["contratti" + e.Row.RowIndex];
                gvContratti.DataBind();

                if (ispImpresa.DaAttuare.HasValue)
                {
                    TextBox tbDaAttuare = (TextBox)e.Row.FindControl("TextBoxDaAttuare");
                    tbDaAttuare.Text = ispImpresa.DaAttuare.Value.ToShortDateString();
                }
                if (ispImpresa.DataChiusura.HasValue)
                {
                    TextBox tbDataChiusura = (TextBox)e.Row.FindControl("TextBoxDataChiusura");
                    tbDataChiusura.Text = ispImpresa.DataChiusura.Value.ToShortDateString();
                }

                TextBox tbOperaiRegolari = (TextBox)e.Row.FindControl("TextBoxOperaiRegolari");
                tbOperaiRegolari.Text = ispImpresa.OperaiRegolari.ToString();
                TextBox tbOperaiIntegrati = (TextBox)e.Row.FindControl("TextBoxOperaiIntegrati");
                tbOperaiIntegrati.Text = ispImpresa.OperaiIntegrati.ToString();
                TextBox tbOperaiTrasformati = (TextBox)e.Row.FindControl("TextBoxOperaiTrasformati");
                tbOperaiTrasformati.Text = ispImpresa.OperaiTrasformati.ToString();
                TextBox tbOperaiRegolarizzati = (TextBox)e.Row.FindControl("TextBoxOperaiRegolarizzati");
                tbOperaiRegolarizzati.Text = ispImpresa.OperaiRegolarizzati.ToString();
                //TextBox tbDimensioneAziendale = (TextBox) e.Row.FindControl("TextBoxDimensioneAziendale");
                //tbDimensioneAziendale.Text = ispImpresa.DimensioneAziendale.ToString();
                TextBox tbOperaiTrasferta = (TextBox)e.Row.FindControl("TextBoxOperaiTrasferta");
                tbOperaiTrasferta.Text = ispImpresa.OperaiTrasferta.ToString();
                TextBox tbOperaiNoAltraCE = (TextBox)e.Row.FindControl("TextBoxOperaiNoAltreCE");
                tbOperaiNoAltraCE.Text = ispImpresa.OperaiNoAltreCE.ToString();
                TextBox tbOperaiDistacco = (TextBox)e.Row.FindControl("TextBoxOperaiDistacco");
                tbOperaiDistacco.Text = ispImpresa.OperaiDistacco.ToString();
                TextBox tbOperaiAltroCCNL = (TextBox)e.Row.FindControl("TextBoxOperaiAltroCcnl");
                tbOperaiAltroCCNL.Text = ispImpresa.OperaiAltroCCNL.ToString();

                if (ispImpresa.CaschiCE.HasValue)
                {
                    CheckBox cbCaschi = (CheckBox)e.Row.FindControl("CheckBoxCaschi");
                    cbCaschi.Checked = ispImpresa.CaschiCE.Value;
                }
                if (ispImpresa.TuteCE.HasValue)
                {
                    CheckBox cbTute = (CheckBox)e.Row.FindControl("CheckBoxTute");
                    cbTute.Checked = ispImpresa.TuteCE.Value;
                }
                if (ispImpresa.ScarpeCE.HasValue)
                {
                    CheckBox cbScarpe = (CheckBox)e.Row.FindControl("CheckBoxScarpe");
                    cbScarpe.Checked = ispImpresa.ScarpeCE.Value;
                }

                CheckBox cbNuovaIscritta = (CheckBox)e.Row.FindControl("CheckBoxNuovaIscritta");
                cbNuovaIscritta.Checked = ispImpresa.NuovaIscritta;

                if (ispImpresa.ElenchiIntegrativi.HasValue)
                {
                    TextBox tbElenchiIntegrativi = (TextBox)e.Row.FindControl("TextBoxElenchiIntegrativi");
                    tbElenchiIntegrativi.Text = ispImpresa.ElenchiIntegrativi.Value.ToString("F2");
                }

                if (ispImpresa.ContributiRecuperati.HasValue)
                {
                    TextBox tbContributiRecuperati = (TextBox)e.Row.FindControl("TextBoxContributiRecuperati");
                    tbContributiRecuperati.Text = ispImpresa.ContributiRecuperati.Value.ToString("F2");
                }

                CheckBox cbStatistiche = (CheckBox)e.Row.FindControl("CheckBoxStatistiche");
                cbStatistiche.Checked = ispImpresa.Statistiche;

                ImageButton iBCancella = (ImageButton)e.Row.FindControl("ImageButtonCancella");
                if (!ispImpresa.IdRapportoIspezioneImpresa.HasValue)
                    iBCancella.Visible = false;
            }
        }

        protected void ButtonSalva_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ViewState["Ispezione"] != null)
                {
                    RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

                    // Recupero i campi dell'ispezione
                    if (ControlloCampiServer())
                    {
                        ispezione.Esito = (EsitoIspezione)Enum.Parse(typeof(EsitoIspezione), DropDownListEsito.Text);
                        ispezione.ComunicatoA =
                            (ComunicazioneRapporto)Enum.Parse(typeof(ComunicazioneRapporto), DropDownListComunicato.Text);

                        if (!string.IsNullOrEmpty(TextBoxNumeroRilievi.Text))
                            ispezione.NumRilievi = Int32.Parse(TextBoxNumeroRilievi.Text);
                        if (!string.IsNullOrEmpty(TextBoxNumeroOsservazioni.Text))
                            ispezione.NumOsservazioni = Int32.Parse(TextBoxNumeroOsservazioni.Text);
                        if (!string.IsNullOrEmpty(TextBoxNumeroRAC.Text))
                            ispezione.NumRAC = Int32.Parse(TextBoxNumeroRAC.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote.Text))
                            ispezione.Note = TextBoxNote.Text;
                        if (!string.IsNullOrEmpty(TextBoxComunicatoIl.Text))
                            ispezione.ComunicatoIl = DateTime.Parse(TextBoxComunicatoIl.Text);

                        if (!string.IsNullOrEmpty(TextBoxNoteIspettore1.Text))
                            ispezione.NoteIspettore1 = TextBoxNoteIspettore1.Text;
                        if (!string.IsNullOrEmpty(TextBoxNote1Dalle.Text))
                            ispezione.Audit1Dalle = DateTime.Parse(TextBoxNote1Dalle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote1Alle.Text))
                            ispezione.Audit1Alle = DateTime.Parse(TextBoxNote1Alle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote1Data.Text))
                            ispezione.Audit1Data = DateTime.Parse(TextBoxNote1Data.Text);

                        if (!string.IsNullOrEmpty(TextBoxNoteIspettore2.Text))
                            ispezione.NoteIspettore2 = TextBoxNoteIspettore2.Text;
                        if (!string.IsNullOrEmpty(TextBoxNote2Dalle.Text))
                            ispezione.Audit2Dalle = DateTime.Parse(TextBoxNote2Dalle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote2Alle.Text))
                            ispezione.Audit2Alle = DateTime.Parse(TextBoxNote2Alle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote2Data.Text))
                            ispezione.Audit2Data = DateTime.Parse(TextBoxNote2Data.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote2Presso.Text))
                            ispezione.Audit2Presso = TextBoxNote2Presso.Text;

                        ispezione.RapportiImpresa = CreaRapportiImpresa();

                        //ispezione.CasseEdili = (CassaEdileCollection) ViewState["CasseEdili"];
                        //ispezione.Contratti = (ContrattoCollection) ViewState["Contratti"];

                        if (ispezione.RapportiImpresa != null)
                        {
                            if (biz.InsertUpdateIspezione(ispezione))
                                Response.Redirect("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep5.aspx");
                            else
                            {
                                LabelRisultato.Text = "Errore nell'inserimento";
                                CaricaDati();
                            }
                        }
                    }
                }
            }
        }

        private bool ControlloCampiServer()
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            DateTime audit1Dalle;
            if (!string.IsNullOrEmpty(TextBoxNote1Dalle.Text) && !DateTime.TryParse(TextBoxNote1Dalle.Text, out audit1Dalle))
            {
                res = false;
                errori.Append("Audit1 dalle errato" + Environment.NewLine);
            }

            DateTime audit1Alle;
            if (!string.IsNullOrEmpty(TextBoxNote1Alle.Text) && !DateTime.TryParse(TextBoxNote1Alle.Text, out audit1Alle))
            {
                res = false;
                errori.Append("Audit1 alle errato" + Environment.NewLine);
            }

            DateTime audit1Data;
            if (!string.IsNullOrEmpty(TextBoxNote1Data.Text) && !DateTime.TryParse(TextBoxNote1Data.Text, out audit1Data))
            {
                res = false;
                errori.Append("Audit1 data errato" + Environment.NewLine);
            }

            DateTime audit2Dalle;
            if (!string.IsNullOrEmpty(TextBoxNote2Dalle.Text) && !DateTime.TryParse(TextBoxNote2Dalle.Text, out audit2Dalle))
            {
                res = false;
                errori.Append("Audit2 dalle errato" + Environment.NewLine);
            }

            DateTime audit2Alle;
            if (!string.IsNullOrEmpty(TextBoxNote2Alle.Text) && !DateTime.TryParse(TextBoxNote2Alle.Text, out audit2Alle))
            {
                res = false;
                errori.Append("Audit2 alle errato" + Environment.NewLine);
            }

            DateTime audit2Data;
            if (!string.IsNullOrEmpty(TextBoxNote2Data.Text) && !DateTime.TryParse(TextBoxNote2Data.Text, out audit2Data))
            {
                res = false;
                errori.Append("Audit2 data errato" + Environment.NewLine);
            }

            int numRilievi;
            if (!string.IsNullOrEmpty(TextBoxNumeroRilievi.Text) &&
                !Int32.TryParse(TextBoxNumeroRilievi.Text, out numRilievi))
            {
                res = false;
                errori.Append("Numero rilievi errato" + Environment.NewLine);
            }

            int numOsservazioni;
            if (!string.IsNullOrEmpty(TextBoxNumeroOsservazioni.Text) &&
                !Int32.TryParse(TextBoxNumeroOsservazioni.Text, out numOsservazioni))
            {
                res = false;
                errori.Append("Numero osservazioni errato" + Environment.NewLine);
            }

            int numRAC;
            if (!string.IsNullOrEmpty(TextBoxNumeroRAC.Text) && !Int32.TryParse(TextBoxNumeroRAC.Text, out numRAC))
            {
                res = false;
                errori.Append("Numero RAC errato" + Environment.NewLine);
            }

            DateTime comunicatoIl;
            if (!string.IsNullOrEmpty(TextBoxComunicatoIl.Text) &&
                !DateTime.TryParse(TextBoxComunicatoIl.Text, out comunicatoIl))
            {
                res = false;
                errori.Append("Data comunicazione errata" + Environment.NewLine);
            }

            if (!res)
                LabelRisultato.Text = errori.ToString();
            return res;
        }

        private RapportoIspezioneImpresaCollection CreaRapportiImpresa()
        {
            RapportoIspezioneImpresaCollection rapportiImpresa = new RapportoIspezioneImpresaCollection();

            foreach (GridViewRow row in GridViewRapportiImpresa.Rows)
            {
                if (ControlloCampiServerTabella(row.RowIndex))
                {
                    int? idRapportoIspezioneImpresa = null;
                    Impresa impresa = null;
                    DateTime? daAttuare = null;
                    DateTime daAtt;
                    DateTime? dataChiusura = null;
                    DateTime datChiu;
                    int? operaiRegolari = null;
                    int opRe = -1;
                    int? operaiIntegrati = null;
                    int opIn = -1;
                    int? operaiTrasformati = null;
                    int opTr = -1;
                    int? operaiRegolarizzati = null;
                    int opRi = -1;
                    int? dimensioneAziendale = null;
                    int dimAz = -1;
                    bool caschi = false;
                    bool tute = false;
                    bool scarpe = false;
                    bool nuovaIscritta = false;
                    decimal? contributiRecuperati = null;

                    idRapportoIspezioneImpresa =
                        (int?)GridViewRapportiImpresa.DataKeys[row.RowIndex].Values["IdRapportoIspezioneImpresa"];
                    impresa = (Impresa)GridViewRapportiImpresa.DataKeys[row.RowIndex].Values["Impresa"];

                    TextBox tbDaAttuare = (TextBox)row.FindControl("TextBoxDaAttuare");
                    if (!string.IsNullOrEmpty(tbDaAttuare.Text) && DateTime.TryParse(tbDaAttuare.Text, out daAtt))
                        daAttuare = daAtt;
                    else
                        daAttuare = null;

                    TextBox tbDataChiusura = (TextBox)row.FindControl("TextBoxDataChiusura");
                    if (!string.IsNullOrEmpty(tbDataChiusura.Text) && DateTime.TryParse(tbDataChiusura.Text, out datChiu))
                        dataChiusura = datChiu;
                    else
                        dataChiusura = null;

                    TextBox tbOperaiRegolari = (TextBox)row.FindControl("TextBoxOperaiRegolari");
                    if (!string.IsNullOrEmpty(tbOperaiRegolari.Text) && Int32.TryParse(tbOperaiRegolari.Text, out opRe))
                        operaiRegolari = opRe;
                    else
                        operaiRegolari = null;

                    TextBox tbOperaiIntegrati = (TextBox)row.FindControl("TextBoxOperaiIntegrati");
                    if (!string.IsNullOrEmpty(tbOperaiIntegrati.Text) && Int32.TryParse(tbOperaiIntegrati.Text, out opIn))
                        operaiIntegrati = opIn;
                    else
                        operaiIntegrati = null;

                    TextBox tbOperaiTrasformati = (TextBox)row.FindControl("TextBoxOperaiTrasformati");
                    if (!string.IsNullOrEmpty(tbOperaiTrasformati.Text) &&
                        Int32.TryParse(tbOperaiTrasformati.Text, out opTr))
                        operaiTrasformati = opTr;
                    else
                        operaiTrasformati = null;

                    TextBox tbOperaiRegolarizzati = (TextBox)row.FindControl("TextBoxOperaiRegolarizzati");
                    if (!string.IsNullOrEmpty(tbOperaiRegolarizzati.Text) &&
                        Int32.TryParse(tbOperaiRegolarizzati.Text, out opRi))
                        operaiRegolarizzati = opRi;
                    else
                        operaiRegolarizzati = null;

                    //TextBox tbDimensioneAziendale = (TextBox) row.FindControl("TextBoxDimensioneAziendale");
                    //if (!string.IsNullOrEmpty(tbDimensioneAziendale.Text) &&
                    //    Int32.TryParse(tbDimensioneAziendale.Text, out dimAz))
                    //    dimensioneAziendale = dimAz;
                    //else
                    //    dimensioneAziendale = null;

                    CheckBox cbCaschi = (CheckBox)row.FindControl("CheckBoxCaschi");
                    caschi = cbCaschi.Checked;

                    CheckBox cbTute = (CheckBox)row.FindControl("CheckBoxTute");
                    tute = cbTute.Checked;

                    CheckBox cbScarpe = (CheckBox)row.FindControl("CheckBoxScarpe");
                    scarpe = cbScarpe.Checked;

                    CheckBox cbNuovaIscritta = (CheckBox)row.FindControl("CheckBoxNuovaIscritta");
                    nuovaIscritta = cbNuovaIscritta.Checked;

                    TextBox tbContributiRecuperati = (TextBox)row.FindControl("TextBoxContributiRecuperati");
                    if (!string.IsNullOrEmpty(tbContributiRecuperati.Text))
                        contributiRecuperati = decimal.Parse(tbContributiRecuperati.Text);

                    CheckBox cbStatistiche = (CheckBox)row.FindControl("CheckBoxStatistiche");

                    RapportoIspezioneImpresa rappImp =
                        new RapportoIspezioneImpresa(idRapportoIspezioneImpresa, impresa, daAttuare,
                                                     dataChiusura, operaiRegolari, operaiIntegrati, operaiTrasformati,
                                                     operaiRegolarizzati, dimensioneAziendale,
                                                     caschi, tute, scarpe);

                    rappImp.NuovaIscritta = nuovaIscritta;
                    TextBox tbElenchiIntegrativi = (TextBox)row.FindControl("TextBoxElenchiIntegrativi");
                    if (!String.IsNullOrEmpty(tbElenchiIntegrativi.Text))
                    {
                        rappImp.ElenchiIntegrativi = Decimal.Parse(tbElenchiIntegrativi.Text);
                    }
                    rappImp.ContributiRecuperati = contributiRecuperati;
                    rappImp.Statistiche = cbStatistiche.Checked;

                    TextBox tbOperaiTrasferta = (TextBox)row.FindControl("TextBoxOperaiTrasferta");
                    if (!String.IsNullOrEmpty(tbOperaiTrasferta.Text))
                    {
                        rappImp.OperaiTrasferta = Int32.Parse(tbOperaiTrasferta.Text);
                    }
                    TextBox tbOperaiNoAltreCE = (TextBox)row.FindControl("TextBoxOperaiNoAltreCE");
                    if (!String.IsNullOrEmpty(tbOperaiNoAltreCE.Text))
                    {
                        rappImp.OperaiNoAltreCE = Int32.Parse(tbOperaiNoAltreCE.Text);
                    }
                    TextBox tbOperaiDistacco = (TextBox)row.FindControl("TextBoxOperaiDistacco");
                    if (!String.IsNullOrEmpty(tbOperaiDistacco.Text))
                    {
                        rappImp.OperaiDistacco = Int32.Parse(tbOperaiDistacco.Text);
                    }
                    TextBox tbOperaiAltroCCNL = (TextBox)row.FindControl("TextBoxOperaiAltroCcnl");
                    if (!String.IsNullOrEmpty(tbOperaiAltroCCNL.Text))
                    {
                        rappImp.OperaiAltroCCNL = Int32.Parse(tbOperaiAltroCCNL.Text);
                    }

                    // OSS
                    rappImp.Oss = CreaOSS(row.RowIndex);

                    // RAC
                    rappImp.Rac = CreaRAC(row.RowIndex);

                    // Contratti
                    rappImp.Contratti = CreaContratti(row.RowIndex);

                    // Casse Edili
                    rappImp.CasseEdili = CreaCasseEdili(row.RowIndex);

                    rapportiImpresa.Add(rappImp);
                }
                else
                {
                    rapportiImpresa = null;
                    break;
                }
            }

            return rapportiImpresa;
        }

        private CassaEdileCollection CreaCasseEdili(int indice)
        {
            return (CassaEdileCollection)ViewState["casseedili" + indice];
        }

        private ContrattoCollection CreaContratti(int indice)
        {
            return (ContrattoCollection)ViewState["contratti" + indice];
        }

        private RACCollection CreaRAC(int indice)
        {
            return (RACCollection)ViewState["rac" + indice];
        }

        private OSSCollection CreaOSS(int indice)
        {
            return (OSSCollection)ViewState["oss" + indice];
        }

        private bool ControlloCampiServerTabella(int indice)
        {
            bool res = true;
            StringBuilder errori = new StringBuilder();

            TextBox tbDaAttuare = (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxDaAttuare");
            DateTime daAttuare;
            if (!string.IsNullOrEmpty(tbDaAttuare.Text) && !DateTime.TryParse(tbDaAttuare.Text, out daAttuare))
            {
                res = false;
                errori.Append("Formato da attuare errato" + Environment.NewLine);
            }

            TextBox tbDataChiusura = (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxDataChiusura");
            DateTime dataChiusura;
            if (!string.IsNullOrEmpty(tbDataChiusura.Text) && !DateTime.TryParse(tbDataChiusura.Text, out dataChiusura))
            {
                res = false;
                errori.Append("Formato data chiusura errato" + Environment.NewLine);
            }

            TextBox tbOperaiRegolari = (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxOperaiRegolari");
            int operaiRegolari;
            if (!string.IsNullOrEmpty(tbOperaiRegolari.Text) && !Int32.TryParse(tbOperaiRegolari.Text, out operaiRegolari))
            {
                res = false;
                errori.Append("Formato operai regolari errato" + Environment.NewLine);
            }

            TextBox tbOperaiIntegrati = (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxOperaiIntegrati");
            int operaiIntegrati;
            if (!string.IsNullOrEmpty(tbOperaiIntegrati.Text) &&
                !Int32.TryParse(tbOperaiIntegrati.Text, out operaiIntegrati))
            {
                res = false;
                errori.Append("Formato operai integrati errato" + Environment.NewLine);
            }

            TextBox tbOperaiRegolarizzati =
                (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxOperaiRegolarizzati");
            int operaiRegolarizzati;
            if (!string.IsNullOrEmpty(tbOperaiRegolarizzati.Text) &&
                !Int32.TryParse(tbOperaiRegolarizzati.Text, out operaiRegolarizzati))
            {
                res = false;
                errori.Append("Formato operai regolarizzati errato" + Environment.NewLine);
            }

            TextBox tbElenchiIntegrativi = (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxElenchiIntegrativi");
            decimal elenchiIntegrativi;
            if (!string.IsNullOrEmpty(tbElenchiIntegrativi.Text) &&
                !decimal.TryParse(tbElenchiIntegrativi.Text, out elenchiIntegrativi))
            {
                res = false;
                errori.Append("Formato elenchi integrativi errato" + Environment.NewLine);
            }

            TextBox tbContributiRecuperati =
                (TextBox)GridViewRapportiImpresa.Rows[indice].FindControl("TextBoxContributiRecuperati");
            decimal contributiRecuperati;
            if (!string.IsNullOrEmpty(tbContributiRecuperati.Text) &&
                !decimal.TryParse(tbContributiRecuperati.Text, out contributiRecuperati))
            {
                res = false;
                errori.Append("Formato contributi recuperati errato" + Environment.NewLine);
            }

            if (!res)
                LabelRisultato.Text = errori.ToString();
            return res;
        }

        protected void ButtonAggiungiOSS_Click(object sender, EventArgs e)
        {
            Button bAggiungi = sender as Button;
            GridViewRow row = (GridViewRow)(((Control)(sender)).Parent.Parent);

            if (ViewState["oss" + row.RowIndex] != null)
            {
                DropDownList ddOSS = (DropDownList)bAggiungi.Parent.FindControl("DropDownListOSS");
                OSS os = new OSS(Int32.Parse(ddOSS.SelectedValue), ddOSS.SelectedItem.Text);
                GridView gvOSS = (GridView)bAggiungi.Parent.FindControl("GridViewOSS");

                OSSCollection oss = (OSSCollection)ViewState["oss" + row.RowIndex];

                if (!oss.Contains(os))
                    oss.Add(os);

                gvOSS.DataSource = ViewState["oss" + row.RowIndex];
                gvOSS.DataBind();
            }
        }

        protected void ButtonAggiungiRAC_Click(object sender, EventArgs e)
        {
            Button bAggiungi = sender as Button;
            GridViewRow row = (GridViewRow)(((Control)(sender)).Parent.Parent);

            if (ViewState["rac" + row.RowIndex] != null)
            {
                DropDownList ddRAC = (DropDownList)bAggiungi.Parent.FindControl("DropDownListRAC");
                RAC ra = new RAC(ddRAC.SelectedValue, ddRAC.SelectedItem.Text);
                GridView gvRAC = (GridView)bAggiungi.Parent.FindControl("GridViewRAC");

                RACCollection rac = (RACCollection)ViewState["rac" + row.RowIndex];

                if (!rac.Contains(ra))
                    rac.Add(ra);

                gvRAC.DataSource = ViewState["rac" + row.RowIndex];
                gvRAC.DataBind();
            }
        }

        protected void GridViewRAC_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Recupero l'indice dell'ispezione impresa
            int indice = ((GridViewRow)(((((GridView)sender).Parent)).Parent)).RowIndex;

            if (ViewState["rac" + indice] != null)
            {
                GridView gvRAC = sender as GridView;

                RACCollection rac = (RACCollection)ViewState["rac" + indice];
                rac.RemoveAt(e.RowIndex);
                gvRAC.DataSource = ViewState["rac" + indice];
                gvRAC.DataBind();
            }
        }

        protected void GridViewOSS_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Recupero l'indice dell'ispezione impresa
            int indice = ((GridViewRow)(((((GridView)sender).Parent)).Parent)).RowIndex;

            if (ViewState["oss" + indice] != null)
            {
                GridView gvOSS = sender as GridView;

                OSSCollection oss = (OSSCollection)ViewState["oss" + indice];
                oss.RemoveAt(e.RowIndex);
                gvOSS.DataSource = ViewState["oss" + indice];
                gvOSS.DataBind();
            }
        }

        //protected void ButtonVisualizzaLegendaOSSRAC_Click(object sender, EventArgs e)
        //{
        //    this.PanelLegendaOSSRAC.Visible = !this.PanelLegendaOSSRAC.Visible;
        //    if (this.PanelLegendaOSSRAC.Visible)
        //        this.ButtonVisualizzaLegendaOSSRAC.Text = "Nascondi legenda";
        //    else
        //        this.ButtonVisualizzaLegendaOSSRAC.Text = "Mostra legenda";
        //}
        protected void LinkButtonMostraNascondiLegenda_Click(object sender, EventArgs e)
        {
            bool visualizza = !PanelLegenda.Visible;

            PanelLegenda.Visible = visualizza;

            if (visualizza)
                LinkButtonMostraNascondiLegenda.Text = "Nascondi legenda";
            else
                LinkButtonMostraNascondiLegenda.Text = "Mostra legenda";
        }

        protected void ButtonPassoPrecedente_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (ViewState["Ispezione"] != null)
                {
                    RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

                    // Recupero i campi dell'ispezione
                    if (ControlloCampiServer())
                    {
                        ispezione.Esito = (EsitoIspezione)Enum.Parse(typeof(EsitoIspezione), DropDownListEsito.Text);
                        ispezione.ComunicatoA =
                            (ComunicazioneRapporto)Enum.Parse(typeof(ComunicazioneRapporto), DropDownListComunicato.Text);

                        if (!string.IsNullOrEmpty(TextBoxNumeroRilievi.Text))
                            ispezione.NumRilievi = Int32.Parse(TextBoxNumeroRilievi.Text);
                        if (!string.IsNullOrEmpty(TextBoxNumeroOsservazioni.Text))
                            ispezione.NumOsservazioni = Int32.Parse(TextBoxNumeroOsservazioni.Text);
                        if (!string.IsNullOrEmpty(TextBoxNumeroRAC.Text))
                            ispezione.NumRAC = Int32.Parse(TextBoxNumeroRAC.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote.Text))
                            ispezione.Note = TextBoxNote.Text;
                        if (!string.IsNullOrEmpty(TextBoxComunicatoIl.Text))
                            ispezione.ComunicatoIl = DateTime.Parse(TextBoxComunicatoIl.Text);

                        if (!string.IsNullOrEmpty(TextBoxNoteIspettore1.Text))
                            ispezione.NoteIspettore1 = TextBoxNoteIspettore1.Text;
                        if (!string.IsNullOrEmpty(TextBoxNote1Dalle.Text))
                            ispezione.Audit1Dalle = DateTime.Parse(TextBoxNote1Dalle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote1Alle.Text))
                            ispezione.Audit1Alle = DateTime.Parse(TextBoxNote1Alle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote1Data.Text))
                            ispezione.Audit1Data = DateTime.Parse(TextBoxNote1Data.Text);

                        if (!string.IsNullOrEmpty(TextBoxNoteIspettore2.Text))
                            ispezione.NoteIspettore2 = TextBoxNoteIspettore2.Text;
                        if (!string.IsNullOrEmpty(TextBoxNote2Dalle.Text))
                            ispezione.Audit2Dalle = DateTime.Parse(TextBoxNote2Dalle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote2Alle.Text))
                            ispezione.Audit2Alle = DateTime.Parse(TextBoxNote2Alle.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote2Data.Text))
                            ispezione.Audit2Data = DateTime.Parse(TextBoxNote2Data.Text);
                        if (!string.IsNullOrEmpty(TextBoxNote2Presso.Text))
                            ispezione.Audit2Presso = TextBoxNote2Presso.Text;

                        ispezione.RapportiImpresa = CreaRapportiImpresa();

                        //ispezione.CasseEdili = (CassaEdileCollection) ViewState["CasseEdili"];
                        //ispezione.Contratti = (ContrattoCollection) ViewState["Contratti"];

                        if (ispezione.RapportiImpresa != null)
                        {
                            if (biz.InsertUpdateIspezione(ispezione))
                            {
                                String redir = String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep3.aspx?idAttivita={0}&idCantiere={1}&giorno={2}&idIspettore={3}&idIspezione={4}",
                                    Request.QueryString["idAttivita"],
                                    Request.QueryString["idCantiere"],
                                    Request.QueryString["giorno"],
                                    Request.QueryString["idIspettore"],
                                    Request.QueryString["idIspezione"]);
                                Response.Redirect(redir);
                            }
                            else
                            {
                                LabelRisultato.Text = "Errore nell'inserimento";
                                CaricaDati();
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep3.aspx?idCantiere=" +
                                      Request.QueryString["idCantiere"]
                                      + "&giorno=" + Request.QueryString["giorno"]
                                      + "&idIspettore=" + Request.QueryString["idIspettore"]
                                      + "&idAttivita=" + Request.QueryString["idAttivita"]);
                }

                //Response.Redirect("CantieriRapportoIspezioneStep3.aspx?idAttivita=" + Request.QueryString["idAttivita"]);
                //string paginaPrecedente = this.Page.Request.UrlReferrer.LocalPath.ToString();
                //Response.Redirect(paginaPrecedente);
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriDefault.aspx");
        }

        protected void GridViewRapportiImpresa_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (GridViewRapportiImpresa.DataKeys[e.RowIndex].Values["IdRapportoIspezioneImpresa"] != null)
            {
                int idRapportoIspezioneImpresa =
                    (int)GridViewRapportiImpresa.DataKeys[e.RowIndex].Values["IdRapportoIspezioneImpresa"];

                if (!biz.DeleteIspezioneImpresa(idRapportoIspezioneImpresa))
                    LabelErroreCancellazione.Visible = true;
                else
                {
                    LabelErroreCancellazione.Visible = false;
                    CaricaDati();
                }
            }
        }

        protected void ButtonAggiungiImpresa_Click(object sender, EventArgs e)
        {
            PanelAggiungiImpresa.Visible = true;
            CantieriRicercaImpresaUnicaFonteAggiungiImpresa.Visible = true;
        }

        private void CantieriRicercaImpresaUnicaFonteAggiungiImpresa_OnImpresaSelected(Impresa impresa)
        {
            if (ViewState["Ispezione"] != null)
            {
                bool inserisci = true;
                ispezione = (RapportoIspezione)ViewState["Ispezione"];

                if (ispezione.RapportiImpresa != null)
                {
                    foreach (RapportoIspezioneImpresa rapImp in ispezione.RapportiImpresa)
                    {
                        if (rapImp.Impresa.TipoImpresa == impresa.TipoImpresa &&
                            rapImp.Impresa.IdImpresa == impresa.IdImpresa)
                        {
                            inserisci = false;
                            break;
                        }
                    }
                }
                else
                    ispezione.RapportiImpresa = new RapportoIspezioneImpresaCollection();

                if (inserisci)
                {
                    RapportoIspezioneImpresa rapImpNuovo = new RapportoIspezioneImpresa(null, impresa, null,
                                                                                        null, null, null, null, null, null,
                                                                                        null, null, null);
                    ispezione.RapportiImpresa.Add(rapImpNuovo);

                    PanelAggiungiImpresa.Visible = false;
                    ViewState["Ispezione"] = ispezione;
                    CaricaDatiIspezione();
                }
            }
        }

        protected void ButtonAggiungiCassaEdile_Click(object sender, EventArgs e)
        {
            Button bAggiungi = sender as Button;
            GridViewRow row = (GridViewRow)(((Control)(sender)).Parent.Parent);

            if (ViewState["casseedili" + row.RowIndex] != null)
            {
                DropDownList ddCassaEdile = (DropDownList)bAggiungi.Parent.FindControl("DropDownListCassaEdile");
                CassaEdile ce = new CassaEdile()
                {
                    IdCassaEdile = ddCassaEdile.SelectedItem.Value,
                    Descrizione = ddCassaEdile.SelectedItem.Text
                };
                GridView gvCassEdili = (GridView)bAggiungi.Parent.FindControl("GridViewCasseEdili");

                CassaEdileCollection casseEdili = (CassaEdileCollection)ViewState["casseedili" + row.RowIndex];

                if (!casseEdili.PresenteNellaLista(ce.IdCassaEdile))
                {
                    casseEdili.Add(ce);
                }

                gvCassEdili.DataSource = ViewState["casseedili" + row.RowIndex];
                gvCassEdili.DataBind();
            }

            //if (!String.IsNullOrEmpty(DropDownListCassaEdile.SelectedValue))
            //{
            //    CassaEdileCollection casseEdili = (CassaEdileCollection) ViewState["CasseEdili"];

            //    if (casseEdili == null)
            //    {
            //        casseEdili = new CassaEdileCollection();
            //    }

            //    CassaEdile ce = new CassaEdile()
            //    {
            //        IdCassaEdile = DropDownListCassaEdile.SelectedItem.Value,
            //        Descrizione = DropDownListCassaEdile.SelectedItem.Text
            //    };

            //    if (!casseEdili.PresenteNellaLista(ce.IdCassaEdile))
            //    {
            //        casseEdili.Add(ce);
            //    }

            //    ViewState["CasseEdili"] = casseEdili;

            //    CaricaCasseEdiliSelezionate();
            //}
        }

        protected void ButtonAggiungiContratto_Click(object sender, EventArgs e)
        {
            Button bAggiungi = sender as Button;
            GridViewRow row = (GridViewRow)(((Control)(sender)).Parent.Parent);

            if (ViewState["contratti" + row.RowIndex] != null)
            {
                DropDownList ddContratto = (DropDownList)bAggiungi.Parent.FindControl("DropDownListContratto");
                Contratto co = new Contratto()
                {
                    Id = Int32.Parse(ddContratto.SelectedItem.Value),
                    Descrizione = ddContratto.SelectedItem.Text
                };
                GridView gvContratti = (GridView)bAggiungi.Parent.FindControl("GridViewContratti");

                ContrattoCollection contratti = (ContrattoCollection)ViewState["contratti" + row.RowIndex];

                if (!contratti.PresenteNellaLista(co.Id))
                {
                    contratti.Add(co);
                }

                gvContratti.DataSource = ViewState["contratti" + row.RowIndex];
                gvContratti.DataBind();
            }

            //if (!String.IsNullOrEmpty(DropDownListContratto.SelectedValue))
            //{
            //    ContrattoCollection contratti = (ContrattoCollection) ViewState["Contratti"];

            //    if (contratti == null)
            //    {
            //        contratti = new ContrattoCollection();
            //    }

            //    Contratto co = new Contratto()
            //    {
            //        Id = Int32.Parse(DropDownListContratto.SelectedItem.Value),
            //        Descrizione = DropDownListContratto.SelectedItem.Text
            //    };

            //    if (!contratti.PresenteNellaLista(co.Id))
            //    {
            //        contratti.Add(co);
            //    }

            //    ViewState["Contratti"] = contratti;

            //    CaricaContrattiSelezionati();
            //}
        }

        //private void CaricaCasseEdiliSelezionate()
        //{
        //    CassaEdileCollection casseEdili = (CassaEdileCollection) ViewState["CasseEdili"];

        //    Presenter.CaricaElementiInGridView(
        //        GridViewCasseEdili,
        //        casseEdili,
        //        0);
        //}

        //private void CaricaContrattiSelezionati()
        //{
        //    ContrattoCollection contratti = (ContrattoCollection) ViewState["Contratti"];

        //    Presenter.CaricaElementiInGridView(
        //        GridViewContratti,
        //        contratti,
        //        0);
        //}

        protected void GridViewCasseEdili_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Recupero l'indice dell'ispezione impresa
            int indice = ((GridViewRow)(((((GridView)sender).Parent)).Parent)).RowIndex;

            if (ViewState["casseedili" + indice] != null)
            {
                GridView gvCasseEdili = sender as GridView;

                CassaEdileCollection casseEdili = (CassaEdileCollection)ViewState["casseedili" + indice];
                casseEdili.RemoveAt(e.RowIndex);
                gvCasseEdili.DataSource = ViewState["casseedili" + indice];
                gvCasseEdili.DataBind();
            }

            //CassaEdileCollection casseEdili = (CassaEdileCollection) ViewState["CasseEdili"];
            //casseEdili.RemoveAt(e.RowIndex);
            //ViewState["CasseEdili"] = casseEdili;

            //CaricaCasseEdiliSelezionate();
        }

        protected void GridViewContratti_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // Recupero l'indice dell'ispezione impresa
            int indice = ((GridViewRow)(((((GridView)sender).Parent)).Parent)).RowIndex;

            if (ViewState["contratti" + indice] != null)
            {
                GridView gvContratti = sender as GridView;

                ContrattoCollection contratti = (ContrattoCollection)ViewState["contratti" + indice];
                contratti.RemoveAt(e.RowIndex);
                gvContratti.DataSource = ViewState["contratti" + indice];
                gvContratti.DataBind();
            }

            //ContrattoCollection contratti = (ContrattoCollection) ViewState["Contratti"];
            //contratti.RemoveAt(e.RowIndex);
            //ViewState["Contratti"] = contratti;

            //CaricaContrattiSelezionati();
        }

        protected void CustomValidatorCasseEdili_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator cvCasseEdiliPresenti = (CustomValidator)source;
            GridViewRow rowCasseEdili = (GridViewRow)cvCasseEdiliPresenti.NamingContainer;
            TextBox tbOperaiTrasferta = (TextBox)rowCasseEdili.FindControl("TextBoxOperaiTrasferta");

            Int32? numeroOpTrasferta = null;
            Int32 notTemp;
            if (Int32.TryParse(tbOperaiTrasferta.Text, out notTemp))
            {
                numeroOpTrasferta = notTemp;
            }

            CassaEdileCollection casseEdili = (CassaEdileCollection)ViewState[String.Format("casseedili{0}", rowCasseEdili.RowIndex)];

            if ((numeroOpTrasferta.HasValue
                && numeroOpTrasferta.Value > 0
                && (casseEdili == null || casseEdili.Count == 0))
                ||
                ((!numeroOpTrasferta.HasValue || numeroOpTrasferta.Value == 0)
                && casseEdili != null && casseEdili.Count > 0))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorContratti_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator cvContrattiPresenti = (CustomValidator)source;
            GridViewRow rowContratti = (GridViewRow)cvContrattiPresenti.NamingContainer;
            TextBox tbOperaiAltroCcnl = (TextBox)rowContratti.FindControl("TextBoxOperaiAltroCcnl");

            Int32? numeroOpAltroCcnl = null;
            Int32 notTemp;
            if (Int32.TryParse(tbOperaiAltroCcnl.Text, out notTemp))
            {
                numeroOpAltroCcnl = notTemp;
            }

            ContrattoCollection contratti = (ContrattoCollection)ViewState[String.Format("contratti{0}", rowContratti.RowIndex)];

            if ((numeroOpAltroCcnl.HasValue
                && numeroOpAltroCcnl.Value > 0
                && (contratti == null || contratti.Count == 0))
                ||
                ((!numeroOpAltroCcnl.HasValue || numeroOpAltroCcnl.Value == 0)
                && contratti != null && contratti.Count > 0))
            {
                args.IsValid = false;
            }
        }

    }
}