﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Collections.GestioneUtenti;
using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriModificaIspettore : System.Web.UI.Page
    {
        private readonly GestioneUtentiBiz gu =
                new GestioneUtentiBiz();

        private IspettoriCollection ispettori;

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriModificaIspettori);

            CaricaIspettori();
        }

        private void CaricaIspettori()
        {
            ispettori = gu.GetUtentiIspettori();

            GridViewIspettori.DataSource = ispettori;
            GridViewIspettori.DataBind();
        }

        protected void GridViewIspettori_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int idUtente = ((int)GridViewIspettori.DataKeys[e.RowIndex].Value);
            CaricaIspettore(idUtente);
        }

        private void CaricaIspettore(int idUtente)
        {
            Ispettore ispettore = (Ispettore)GestioneUtentiBiz.GetIdentitaUtente(idUtente);
            RegistrazioneIspettoreModifica1.Ispettore = ispettore;

            RegistrazioneIspettoreModifica1.Visible = true;
            LabelModificaIspettore.Visible = true;
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriDefault.aspx");
        }
    }
}