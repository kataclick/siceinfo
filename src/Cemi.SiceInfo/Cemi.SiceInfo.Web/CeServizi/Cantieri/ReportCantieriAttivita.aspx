﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportCantieriAttivita.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.ReportCantieriAttivita" %>

<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<%--<%@ Register Src="../WebControls/MenuImprese.ascx" TagName="MenuImprese" TagPrefix="uc2" %>--%>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc3:MenuCantieri ID="MenuCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Attivita" />
    <br />
    <table height="600px" width="740px">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewerAttivita" runat="server"
                    ProcessingMode="Remote" Width="100%" Height="490px">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>

