﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriProgrammazioneSettimanale.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriProgrammazioneSettimanale"  %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc2:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Programmazione settimanale" />
    &nbsp;
    <br />


    <table class="cantieriSelezionePeriodi">
        <tr>
            <td>Ispettore<br />
                <asp:DropDownList ID="DropDownListIspettore" runat="server" Width="260px" AutoPostBack="True" OnSelectedIndexChanged="DropDownListIspettore_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>Settimana&nbsp;<asp:Calendar ID="CalendarSettimana" runat="server" OnSelectionChanged="CalendarSettimana_SelectionChanged" CellPadding="1" Height="96px" Width="150px"></asp:Calendar>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red"></asp:Label></td>
        </tr>
    </table>
    &nbsp;
        
                <asp:Button ID="ButtonVisualizza" runat="server" OnClick="ButtonVisualizza_Click"
                    Text="Visualizza programmazione settimanale" Width="288px" />&nbsp;
    &nbsp;<asp:Button ID="ButtonReport" runat="server" Enabled="False" OnClick="ButtonReport_Click"
        Text="Visualizza Report" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="MainPage2" runat="Server">
    <asp:GridView ID="GridViewAttivita" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridViewAttivita_RowDataBound" Width="980px" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
        <Columns>
            <asp:TemplateField HeaderText="Id ispettore" Visible="False"></asp:TemplateField>
            <asp:TemplateField HeaderText="Ispettore">
                <ItemTemplate>
                    <asp:Label ID="LabelIspettore" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Giorno1">
                <ItemTemplate>
                    <center>
                        <asp:Button ID="ButtonAggiungiAttivita1" runat="server" Text="Nuova attivita" OnClick="ButtonAggiungiAttivita_Click" /></center>
                    <hr />
                    <asp:GridView ID="GridViewGiorno1" runat="server" AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdAttivita" OnRowDeleting="GridViewGiorno1_RowDeleting" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" OnRowDataBound="GridViewGiorno_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="NomeAttivita" HeaderText="Attivit&#224;" Visible="False" />
                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                            <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Cantiere" />
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="X" ShowDeleteButton="True" DeleteImageUrl="../images/pallinoX.png" ShowCancelButton="False">
                                <ControlStyle Height="16px" Width="16px" />
                                <ItemStyle Wrap="True" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna attività presente
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Giorno2">
                <ItemTemplate>
                    <center>
                        <asp:Button ID="ButtonAggiungiAttivita2" runat="server" Text="Nuova attivita" OnClick="ButtonAggiungiAttivita_Click" /></center>
                    <hr />
                    <asp:GridView ID="GridViewGiorno2" runat="server" AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdAttivita" OnRowDeleting="GridViewGiorno1_RowDeleting" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" OnRowDataBound="GridViewGiorno_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="NomeAttivita" HeaderText="Attivit&#224;" Visible="False" />
                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                            <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Cantiere" />
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="X" ShowDeleteButton="True" DeleteImageUrl="../images/pallinoX.png">
                                <ControlStyle Height="16px" Width="16px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna attività presente
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Giorno3">
                <ItemTemplate>
                    <center>
                        <asp:Button ID="ButtonAggiungiAttivita3" runat="server" Text="Nuova attivita" OnClick="ButtonAggiungiAttivita_Click" /></center>
                    <hr />
                    <asp:GridView ID="GridViewGiorno3" runat="server" AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdAttivita" OnRowDeleting="GridViewGiorno1_RowDeleting" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" OnRowDataBound="GridViewGiorno_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="NomeAttivita" HeaderText="Attivit&#224;" Visible="False" />
                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                            <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Cantiere" />
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="X" ShowDeleteButton="True" DeleteImageUrl="../images/pallinoX.png">
                                <ControlStyle Height="16px" Width="16px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna attività presente
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Giorno4">
                <ItemTemplate>
                    <center>
                        <asp:Button ID="ButtonAggiungiAttivita4" runat="server" Text="Nuova attivita" OnClick="ButtonAggiungiAttivita_Click" /></center>
                    <hr />
                    <asp:GridView ID="GridViewGiorno4" runat="server" AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdAttivita" OnRowDeleting="GridViewGiorno1_RowDeleting" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" OnRowDataBound="GridViewGiorno_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="NomeAttivita" HeaderText="Attivit&#224;" Visible="False" />
                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                            <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Cantiere" />
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="X" ShowDeleteButton="True" DeleteImageUrl="../images/pallinoX.png">
                                <ControlStyle Height="16px" Width="16px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna attività presente
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Giorno5">
                <ItemTemplate>
                    <center>
                        <asp:Button ID="ButtonAggiungiAttivita5" runat="server" Text="Nuova attivita" OnClick="ButtonAggiungiAttivita_Click" /></center>
                    <hr />
                    <asp:GridView ID="GridViewGiorno5" runat="server" AutoGenerateColumns="False" ShowHeader="False" DataKeyNames="IdAttivita" OnRowDeleting="GridViewGiorno1_RowDeleting" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" OnRowDataBound="GridViewGiorno_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="NomeAttivita" HeaderText="Attivit&#224;" Visible="False" />
                            <asp:BoundField DataField="Descrizione" HeaderText="Descrizione" />
                            <asp:BoundField DataField="NomeCompletoCantiere" HeaderText="Cantiere" />
                            <asp:TemplateField HeaderText="Impresa">
                                <ItemTemplate>
                                    <asp:Label ID="LabelImpresa" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" DeleteText="X" ShowDeleteButton="True" DeleteImageUrl="../images/pallinoX.png">
                                <ControlStyle Height="16px" Width="16px" />
                            </asp:CommandField>
                        </Columns>
                        <EmptyDataTemplate>
                            Nessuna attività presente
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <br />
</asp:Content>
