﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfermaCancellazioneLettera.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.ConfermaCancellazioneLettera" %>

<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche" TagPrefix="uc2" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc2:MenuCantieriStatistiche ID="MenuCantieriStatistiche1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Conferma cancellazione lettera" titolo="Cantieri" />
    <br />
    Hai scelto di cancellare la lettera con protocollo
    <asp:Label ID="LabelProtocollo" runat="server" Font-Bold="True"></asp:Label>. Premi
    "Conferma" per eliminarla.<br />
    <br />
    <asp:Button ID="ButtonConferma" runat="server" OnClick="ButtonConferma_Click" Text="Conferma" />&nbsp;
    <asp:Button ID="ButtonAnnulla" runat="server" Text="Annulla" />
    <br />
    <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante l'operazione" Visible="False"></asp:Label>
</asp:Content>
