﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Ispettore = TBridge.Cemi.Type.Entities.GestioneUtenti.Ispettore;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriRicercaIspezioni : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);
            funzionalita.Add(FunzionalitaPredefinite.CantieriLettere);
            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            CantieriRicercaIspezioni1.OnIspezioneSelected += CantieriRicercaIspezioni1_OnIspezioneSelected;

            //Se non è RUI controllare TEMPO!!!!!
            //TBridge.Cemi.GestioneUtentiBiz.Business.GestioneUtentiBiz.Autorizzato(TBridge.Cemi.GestioneUtentiBiz.Data.FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString());

            if (!Page.IsPostBack)
            {
                //Se l'utente è ispettore e non è rui blocchiamo la dropdown
                if (GestioneUtentiBiz.IsIspettore() &&
                    GestioneUtentiBiz.Autorizzato(
                        FunzionalitaPredefinite.CantieriConsuPrev.ToString())
                    &&
                    !GestioneUtentiBiz.Autorizzato(
                         FunzionalitaPredefinite.CantieriConsuPrevRUI.ToString())
                    )
                {
                    //TBridge.Cemi.GestioneUtenti.Type.Entities.Ispettore ispettore =
                    //    ((Ispettore) HttpContext.Current.User.Identity).Entity;
                    Ispettore ispettore =
                        (Ispettore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                    CantieriRicercaIspezioni1.ImpostaIspettore(ispettore.IdIspettore);
                }

                if (Request.QueryString["modalita"] == "indietro")
                {
                    int? pagina = null;

                    int tempPagina;
                    if (Int32.TryParse(Request.QueryString["pagina"], out tempPagina))
                        pagina = tempPagina;

                    CantieriRicercaIspezioni1.CaricaFiltri(Request.QueryString["dal"], Request.QueryString["al"],
                                                           Request.QueryString["ispettore"], Request.QueryString["stato"],
                                                           Request.QueryString["detAppalto"], Request.QueryString["subApp"],
                                                           pagina);
                    CantieriRicercaIspezioni1.ForzaRicerca();
                }
            }
        }

        private void CantieriRicercaIspezioni1_OnIspezioneSelected(RapportoIspezione ispezione)
        {
            //Response.Redirect(
            //    string.Format(
            //        "~/Cantieri/CantieriRapportoIspezioneStep1.aspx?idCantiere={0}&giorno={1}&idIspettore={2}&idAttivita={3}",
            //        ispezione.Cantiere.IdCantiere, ispezione.Giorno.ToShortDateString(), ispezione.Ispettore.IdIspettore,
            //        ispezione.IdAttivita));
            Response.Redirect(
                string.Format(
                    "~/CeServizi/Cantieri/CantieriRapportoIspezioneStep1.aspx?idIspezione={0}",
                    ispezione.IdIspezione));
        }
    }
}