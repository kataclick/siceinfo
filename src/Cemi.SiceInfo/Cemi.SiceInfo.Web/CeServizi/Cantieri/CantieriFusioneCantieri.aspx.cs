﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriFusioneCantieri : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);

            CantieriRicercaCantiere1.SoloSelezione();
            CantieriRicercaCantiere1.OnCantiereSelected += CantieriRicercaCantiere1_OnCantiereSelected;

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["idCantiere"] != null)
                {
                    int idCantiere = int.Parse(Request.QueryString["idCantiere"]);

                    ViewState["CantieriAssimilabili"] = new CantiereCollection();

                    Cantiere cantiere =
                        biz.GetCantiere(idCantiere);

                    ViewState["Cantiere"] = cantiere;

                    LabelIndirizzoCantiere.Text = cantiere.Indirizzo;
                    LabelCivico.Text = cantiere.Civico;
                    LabelProvincia.Text = cantiere.Provincia;
                    LabelComune.Text = cantiere.Comune;
                    LabelCAP.Text = cantiere.Cap;
                    LabelImporto.Text = String.Format("{0:n}", cantiere.Importo); //cantiere.Importo.ToString();

                    // Impresa appaltatrice
                    if (cantiere.ImpresaAppaltatrice != null)
                    {
                        LabelImpresa.Text = cantiere.ImpresaAppaltatrice.RagioneSociale;
                    }
                    else if (cantiere.ImpresaAppaltatriceTrovata != null)
                    {
                        LabelImpresa.Text = cantiere.ImpresaAppaltatriceTrovata;
                    }
                }
            }

            CaricaCantieriAssimilabili();
        }

        private void CaricaCantieriAssimilabili()
        {
            GridViewCantieriAssimilabili.DataSource = ViewState["CantieriAssimilabili"];
            GridViewCantieriAssimilabili.DataBind();
        }

        private void CantieriRicercaCantiere1_OnCantiereSelected(Cantiere cantiere)
        {
            CantiereCollection cantieriAssimilabili = (CantiereCollection)ViewState["CantieriAssimilabili"];

            if (!cantieriAssimilabili.Contains(cantiere))
                cantieriAssimilabili.Add(cantiere);

            CaricaCantieriAssimilabili();
        }

        protected void ButtonCantieriMerge_Click(object sender, EventArgs e)
        {
            CantiereCollection cantieriAssimilabili = (CantiereCollection)ViewState["CantieriAssimilabili"];
            Cantiere cantiere = (Cantiere)ViewState["Cantiere"];

            if (biz.FusioneCantieri(cantiere, cantieriAssimilabili))
            {
                // Probabilmente un redirect...
                //LabelRisultato.Text = "Cantieri fusi";
                Response.Redirect(
                    string.Format("~/CeServizi/Cantieri/CantieriSchedaCantiere.aspx?idCantiere={0}", cantiere.IdCantiere));
            }
            else
                LabelRisultato.Text = "Errore durante la fusione dei cantieri";
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx");
        }
    }
}