﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;

using System.Configuration;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class SchedaCantiere : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);
            #endregion

            if (Context.Items["IdCantiere"] != null)
            {
                Int32 idCantiere = (Int32)Context.Items["IdCantiere"];

                ReportViewerSchedaCantiere.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewerSchedaCantiere.ServerReport.ReportPath = "/ReportCantieri/SchedaCantiere";

                ReportParameter[] listaParam = new ReportParameter[1];
                listaParam[0] = new ReportParameter("idCantiere", idCantiere.ToString());
                ReportViewerSchedaCantiere.ServerReport.SetParameters(listaParam);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                //PDF

                byte[] bytes = ReportViewerSchedaCantiere.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", "attachment;filename=SchedaCantiere.pdf");
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }
    }
}