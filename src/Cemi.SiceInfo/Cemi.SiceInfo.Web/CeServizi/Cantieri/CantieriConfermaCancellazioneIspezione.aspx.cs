﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriConfermaCancellazioneIspezione : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            //controlliamo se è una risposta
            if (!string.IsNullOrEmpty(Request.QueryString["mod"]))
            {
                //
                switch (Request.QueryString["mod"])
                {
                    case "cancellato":
                        LabelTesto.Text = "La cancellazione è andata a buon fine.";
                        ButtonAnnulla.Text = "Indietro";
                        ButtonConfermaEliminazione.Visible = false;
                        break;

                    case "errore":
                        LabelTesto.Text = "La cancellazione non è andata a buon fine.";
                        ButtonAnnulla.Text = "Indietro";
                        ButtonConfermaEliminazione.Visible = false;
                        break;
                }
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            //controlliamo se è una risposta
            if (!string.IsNullOrEmpty(Request.QueryString["o"]))
            {
                //
                switch (Request.QueryString["o"])
                {
                    case "prog":
                        Response.Redirect("~/CeServizi/Cantieri/CantieriProgrammazioneSettimanale.aspx");
                        break;

                    case "precon":
                        Response.Redirect("~/CeServizi/Cantieri/CantieriPreventivoConsuntivoSettimanale.aspx?idIspettore=" +
                                          Request.QueryString["idIspettore"] + "&dal=" + Request.QueryString["dal"]);
                        break;
                }
            }
        }

        protected void ButtonConfermaEliminazione_Click(object sender, EventArgs e)
        {
            int idAttivita;

            if (int.TryParse(Request.QueryString["idAttivita"], out idAttivita))
            {
                if (biz.DeleteIspezione(idAttivita))
                {
                    Response.Redirect("~/CeServizi/Cantieri/CantieriConfermaCancellazioneIspezione.aspx?mod=cancellato&o=" +
                                      Request.QueryString["o"] + "&idIspettore=" + Request.QueryString["idIspettore"] +
                                      "&dal=" + Request.QueryString["dal"]);
                }
            }

            Response.Redirect("~/CeServizi/Cantieri/CantieriConfermaCancellazioneIspezione.aspx?mod=errore&o=" +
                              Request.QueryString["o"] + "&idIspettore=" + Request.QueryString["idIspettore"] + "&dal=" +
                              Request.QueryString["dal"]);
        }
    }
}