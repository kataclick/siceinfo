﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriGestioneCantieri : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriGestione);
            #endregion

            CantieriRicercaCantiere1.OnCantiereSelected += CantieriRicercaCantiere1_OnCantiereSelected;

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(CantieriRicercaCantiere1);
        }

        private void CantieriRicercaCantiere1_OnCantiereSelected(Cantiere cantiere)
        {
            Response.Redirect(
                string.Format("~/CeServizi/Cantieri/CantieriInserimentoModificaCantiere.aspx?idCantiere={0}&modalita=modifica",
                              cantiere.IdCantiere));
        }

        protected void LinkButtonNuovoCantiere_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriInserimentoModificaCantiere.aspx?modalita=inserisci");
        }

        protected void ButtonNuovoCantiere_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriInserimentoModificaCantiere.aspx?modalita=inserisci");
        }
    }
}