﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.TuteScarpe.Business;
using Telerik.Web.UI;
using Impresa = TBridge.Cemi.Type.Entities.Cantieri.Impresa;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Entities.Geocode;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriInserimentoModificaCantiere : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly TSBusiness tsbiz = new TSBusiness();
        private bool aggiornamento;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.CantieriModifica);
            #endregion

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonAnnulla);

            #region Impostiamo eventi e logiche ai controlli

            //Registriamo gli eventi di click sul controllo ricerca committente per sapere quando
            //viene selzionato un committente e quando viene richiesto l'inserimento di un nuovo committente
            CantieriRicercaCommittente1.OnCommittenteSelected += CantieriRicercaCommittente1_OnCommittenteSelected;
            CantieriRicercaCommittente1.OnNuovoCommittenteSelected += CantieriRicercaCommittente1_OnNuovoCommittenteSelected;

            //Registriamo gli eventi di click sul controllo ricerca impersa per sapere quando
            //viene selezionata una impresa e quando viene richiesto l'inserimento di una nuova impresa
            CantieriRicercaImpresa1.OnImpresaSelected += CantieriRicercaImpresa1_OnImpresaSelected;
            CantieriRicercaImpresa1.OnNuovaImpresaSelected += CantieriRicercaImpresa1_OnNuovaImpresaSelected;

            //CantieriRicercaCommittente1.OnCommittenteSelected += new TBridge.Cemi.Cantieri.Type.Delegates.CommittentiSelectedEventHandler(CantieriRicercaCommittente1_OnCommittenteSelected);
            //CantieriRicercaImpresa1.OnImpresaSelected += new TBridge.Cemi.Cantieri.Type.Delegates.ImpreseSelectedEventHandler(CantieriRicercaImpresa1_OnImpresaSelected);

            CantieriCommittente1.CantieriBiz = biz;
            CantieriImpresa1.CantieriBiz = biz;
            CantieriImpresa1.TsBiz = tsbiz;

            #endregion

            #region Impostiamo i Validatiuon group

            //Settiamo il validation group cross-control
            string validationGroupImpresa = "validationGroupImpresa";
            ButtonInserisciImpresa.ValidationGroup = validationGroupImpresa;
            CantieriImpresa1.SetValidationGroup(validationGroupImpresa);

            //Settiamo il validation group cross-control
            string validationGroupCommittente = "validationGroupCommittente";
            ButtonInserisciCommittente.ValidationGroup = validationGroupCommittente;
            CantieriCommittente1.SetValidationGroup(validationGroupCommittente);

            #endregion

            #region Impostiamo gli eventi JS per la gestione dei click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();

            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('validationGroupCommittente') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciCommittente, null) + ";");
            sb.Append("return true;");
            ButtonInserisciCommittente.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciCommittente);

            ////resettiamo
            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('validationGroupImpresa') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciImpresa, null) + ";");
            sb.Append("return true;");
            ButtonInserisciImpresa.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciImpresa);


            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('InsModCantiere') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonOperazione, null) + ";");
            sb.Append("return true;");
            ButtonOperazione.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonOperazione);


            #endregion

            if (!Page.IsPostBack)
            {
                CaricaTipologieAppalto();
                CaricaTipiImpresaAppaltatrice();

                string modalita = Request.QueryString["modalita"];
                if (modalita == null || modalita != "modifica")
                {
                    ModalitaInserimento();
                }
                else
                {
                    string idCantiereS = Request.QueryString["idCantiere"];
                    int idCantiere;

                    if (Int32.TryParse(idCantiereS, out idCantiere))
                    {
                        ModalitaAggiornamento();
                        CaricaCantiere(idCantiere);
                    }
                    else
                        ModalitaInserimento();
                }
            }
        }

        /// <summary>
        /// Evento bottone inserimento nuova impresa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CantieriRicercaImpresa1_OnNuovaImpresaSelected(object sender, EventArgs e)
        {
            LabelInserimentoImpresaRes.Text = string.Empty;
            CantieriRicercaImpresa1.Visible = false;
            PanelInserimentoImpresa.Visible = true;
        }

        /// <summary>
        /// Evento selezione nuova impresa da controllo ricerca impresa
        /// </summary>
        /// <param name="impresa"></param>
        private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
        {
            ImpresaSelezionata(impresa);
        }

        /// <summary>
        /// Memorizza la seleziona di un impresa
        /// </summary>
        /// <param name="impresa"></param>
        private void ImpresaSelezionata(Impresa impresa)
        {
            RadioButtonImpresaCantieri.Checked = false;
            RadioButtonImpresaSiceInfo.Checked = false;

            TextBoxImpresaAppaltatrice.BackColor = Color.White;

            if (impresa.TipoImpresa == TipologiaImpresa.SiceNew)
            {
                RadioButtonImpresaSiceInfo.Checked = true;
                TextBoxImpresaAppaltatrice.Text =
                    String.Format("{0} - {1}", impresa.IdImpresa,
                                  impresa.NomeCompleto);
            }
            else
            {
                RadioButtonImpresaCantieri.Checked = true;
                TextBoxImpresaAppaltatrice.Text = impresa.NomeCompleto;
            }
            ViewState["impresa"] = impresa;

            CantieriRicercaImpresa1.Visible = false;
            PanelInserimentoImpresa.Visible = false;

            if (impresa.FonteNotifica && !impresa.Modificato)
                ButtonModificaImpresa.Enabled = true;
            else
                ButtonModificaImpresa.Enabled = false;
        }

        protected void ButtonSelezionaAppaltatrice_Click(object sender, EventArgs e)
        {
            CantieriRicercaImpresa1.Visible = true;
            PanelInserimentoImpresa.Visible = false;
        }

        protected void ButtonOperazione_Click(object sender, EventArgs e)
        {
            Page.Validate("InsModCantiere");

            if (Page.IsValid)
            {
                Cantiere cantiere = CreaCantiere();
                bool res = false;

                if (Request.QueryString["modalita"] == "modifica")
                    aggiornamento = true;

                // Eseguo l'operazione
                if (aggiornamento)
                {
                    int idCantiere;
                    Int32.TryParse(TextBoxIdCantiere.Text, out idCantiere);
                    cantiere.IdCantiere = idCantiere;

                    // Aggiorno il record
                    res = biz.UpdateCantiere(cantiere);
                }
                else
                {
                    // Inserisco il record
                    biz.InsertCantiere(cantiere);
                    if (cantiere.IdCantiere > 0)
                        res = true;
                }

                // Gestisco il risultato dell'operazione
                if (res)
                {
                    // Tutto ok
                    if (aggiornamento)
                        LabelRisultato.Text = "Aggiornamento effettuato correttamente";
                    else
                    {
                        LabelRisultato.Text = "Inserimento effettuato correttamente";
                        SvuotaCampi();
                    }
                }
                else
                {
                    LabelRisultato.Text = "Si è verificato un errore durante l'operazione";
                }
            }
        }

        private void SvuotaCampi()
        {
            IscrizioneLavoratoreIndirizzo1.Reset();
            TextBoxPermessoCostruire.Text = string.Empty;
            TextBoxImporto.Text = string.Empty;
            TextBoxDataInizioLavori.Text = string.Empty;
            TextBoxDataFineLavori.Text = string.Empty;
            TextBoxDirezioneLavori.Text = string.Empty;
            TextBoxResponsabileProcedimento.Text = string.Empty;
            TextBoxResponsabileCantiere.Text = string.Empty;
            TextBoxDescrizioneLavori.Text = string.Empty;
            DropDownListTipologiaAppalto.SelectedIndex = 0;
            CheckBoxAttivo.Checked = true;

            ViewState["committente"] = null;
            TextBoxCommittente.Text = string.Empty;
            ViewState["impresa"] = null;
            TextBoxImpresaAppaltatrice.Text = string.Empty;

            RadioButtonDefault.Checked = true;
            DropDownListTipoImpresa.SelectedIndex = 0;
        }

        private Cantiere CreaCantiere()
        {
            Cantiere cantiere = new Cantiere();

            Indirizzo indirizzo = IscrizioneLavoratoreIndirizzo1.GetIndirizzo();

            cantiere.Indirizzo = Presenter.NormalizzaCampoTesto(indirizzo.NomeVia);
            cantiere.Civico = Presenter.NormalizzaCampoTesto(indirizzo.Civico);
            cantiere.Provincia = Presenter.NormalizzaCampoTesto(indirizzo.Provincia);
            cantiere.Comune = Presenter.NormalizzaCampoTesto(indirizzo.Comune);
            cantiere.Cap = Presenter.NormalizzaCampoTesto(indirizzo.Cap);
            cantiere.Latitudine = indirizzo.Latitudine.HasValue ? (Double?)Decimal.ToDouble(indirizzo.Latitudine.Value) : null;
            cantiere.Longitudine = indirizzo.Longitudine.HasValue ? (Double?)Decimal.ToDouble(indirizzo.Longitudine.Value) : null;

            if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxPermessoCostruire.Text)))
                cantiere.PermessoCostruire = Presenter.NormalizzaCampoTesto(TextBoxPermessoCostruire.Text);
            if (!string.IsNullOrEmpty(TextBoxImporto.Text))
                cantiere.Importo = double.Parse(TextBoxImporto.Text);
            if (!string.IsNullOrEmpty(TextBoxDataInizioLavori.Text))
                cantiere.DataInizioLavori = DateTime.Parse(TextBoxDataInizioLavori.Text);
            if (!string.IsNullOrEmpty(TextBoxDataFineLavori.Text))
                cantiere.DataFineLavori = DateTime.Parse(TextBoxDataFineLavori.Text);
            if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxDirezioneLavori.Text)))
                cantiere.DirezioneLavori = Presenter.NormalizzaCampoTesto(TextBoxDirezioneLavori.Text);
            if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxResponsabileProcedimento.Text)))
                cantiere.ResponsabileProcedimento = Presenter.NormalizzaCampoTesto(TextBoxResponsabileProcedimento.Text);
            if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxResponsabileCantiere.Text)))
                cantiere.ResponsabileCantiere = Presenter.NormalizzaCampoTesto(TextBoxResponsabileCantiere.Text);
            if (!string.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxDescrizioneLavori.Text)))
                cantiere.DescrizioneLavori = Presenter.NormalizzaCampoTesto(TextBoxDescrizioneLavori.Text);

            cantiere.TipologiaAppalto =
                (TipologiaAppalto)
                Enum.Parse(typeof(TipologiaAppalto), DropDownListTipologiaAppalto.SelectedItem.ToString());
            cantiere.Attivo = CheckBoxAttivo.Checked;

            cantiere.Committente = ViewState["committente"] as Committente;
            cantiere.ImpresaAppaltatrice = ViewState["impresa"] as Impresa;

            if (cantiere.ImpresaAppaltatrice != null)
            {
                if (RadioButtonDefault.Checked && !string.IsNullOrEmpty(DropDownListTipoImpresa.SelectedValue))
                {
                    cantiere.TipoImpresaAppaltatrice = DropDownListTipoImpresa.SelectedValue;
                }
                else if (RadioButtonAltro.Checked && !string.IsNullOrEmpty(TextBoxTipoImpresa.Text))
                {
                    cantiere.TipoImpresaAppaltatrice = TextBoxTipoImpresa.Text;
                }
            }

            // Non cancellare il committente trovato e l'impresa trovata
            //Cantiere cantiere = new Cantiere(null, provincia, comune, cap, indirizzo, civico,
            //                                 impresaAppaltatrice, permessoCostruire, importo, dataInizioLavori,
            //                                 dataFineLavori,
            //                                 tipologiaAppalto, attivo, committente, null, impresa, null, null,
            //                                 direzioneLavori,
            //                                 responsabileProcedimento, responsabileCantiere, descrizioneLavori,
            //                                 tipoImpresaAppaltatrice, null);

            return cantiere;
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriGestioneCantieri.aspx");
        }

        protected void ButtonModificaCommittente_Click(object sender, EventArgs e)
        {
            if (ViewState["committente"] != null)
            {
                Committente committente = (Committente)ViewState["committente"];
                CantieriCommittente1.ImpostaCommittente(committente);

                //committente.Modificato = true;
                //ViewState["committente"] = committente;

                LabelInserimentoCommittenteRes.Text = string.Empty;
                CantieriRicercaCommittente1.Visible = false;
                PanelInserimentoCommittente.Visible = true;
                LabelTitoloInserimentoCommittente.Text = "Modifica committente";
                ButtonInserisciCommittente.Text = "Modifica committente";
            }
        }

        protected void ButtonModificaImpresa_Click(object sender, EventArgs e)
        {
            if (ViewState["impresa"] != null)
            {
                Impresa impresa = (Impresa)ViewState["impresa"];
                CantieriImpresa1.ImpostaImpresa(impresa);

                //committente.Modificato = true;
                //ViewState["committente"] = committente;

                LabelInserimentoImpresaRes.Text = string.Empty;
                CantieriRicercaImpresa1.Visible = false;
                PanelInserimentoImpresa.Visible = true;
                LabelInserimentoNuovaImpresa.Text = "Modifica committente";
                ButtonInserisciImpresa.Text = "Modifica impresa";
            }
        }

        #region Eventi legati al committente

        protected void ButtonSelezionaCommittente_Click(object sender, EventArgs e)
        {
            PanelInserimentoCommittente.Visible = false;
            CantieriRicercaCommittente1.Visible = true;
        }

        private void CantieriRicercaCommittente1_OnNuovoCommittenteSelected(object sender, EventArgs e)
        {
            LabelInserimentoCommittenteRes.Text = string.Empty;
            CantieriRicercaCommittente1.Visible = false;
            PanelInserimentoCommittente.Visible = true;
            CantieriCommittente1.Reset();
        }

        private void CantieriRicercaCommittente1_OnCommittenteSelected(Committente committente)
        {
            CommittenteSelezionato(committente);
        }

        private void CommittenteSelezionato(Committente committente)
        {
            TextBoxCommittente.Text = committente.NomeCompleto;
            TextBoxCommittente.BackColor = Color.White;
            ViewState["committente"] = committente;

            CantieriRicercaCommittente1.Visible = false;
            PanelInserimentoCommittente.Visible = false;

            if (committente.FonteNotifica && !committente.Modificato)
                ButtonModificaCommittente.Enabled = true;
            else
                ButtonModificaCommittente.Enabled = false;
        }

        #endregion

        #region Eventi inserimento committente

        protected void ButtonInserisciCommittente_Click(object sender, EventArgs e)
        {
            Page.Validate("ValidaInserimentoCommittente");

            if (Page.IsValid)
            {
                Committente committente = CantieriCommittente1.Committente;
                bool res = true;

                if (committente != null)
                {
                    if (!committente.IdCommittente.HasValue)
                        // Inserisco il record
                        biz.InsertCommittente(committente);
                    else
                    {
                        if (committente.FonteNotifica && !committente.Modificato)
                        {
                            res = biz.UpdateCommittenteIspettore(committente);
                        }
                    }

                    if (committente.IdCommittente > 0 && res)
                    {
                        CommittenteSelezionato(committente);
                        CantieriCommittente1.Reset();
                    }
                    else
                        LabelInserimentoCommittenteRes.Text = "Errore durante l'inserimento";
                }
                else
                    LabelInserimentoCommittenteRes.Text = CantieriCommittente1.Errore;
            }
        }

        protected void ButtonAnnullaInserimentoCommittente_Click(object sender, EventArgs e)
        {
            PanelInserimentoCommittente.Visible = false;
            LabelInserimentoCommittenteRes.Text = string.Empty;
        }

        #endregion

        #region Eventi inserimento impresa

        protected void ButtonInserisciImpresa_Click(object sender, EventArgs e)
        {
            Page.Validate("ValidaInserimentoImpresa");

            if (Page.IsValid)
            {
                Impresa impresa = CantieriImpresa1.impresa;
                bool res = true;

                if (impresa != null)
                {
                    // Inserisco il record
                    if (!impresa.IdImpresa.HasValue)
                        biz.InsertImpresa(impresa);
                    else
                    {
                        if (impresa.FonteNotifica && !impresa.Modificato)
                        {
                            res = biz.UpdateImpresaIspettore(impresa);
                        }
                    }

                    if (impresa.IdImpresa > 0 && res)
                    {
                        ImpresaSelezionata(impresa);
                        CantieriImpresa1.Reset();
                    }
                    else
                        LabelInserimentoImpresaRes.Text = "Si è verificato un errore durante l'inserimento";
                }
                else
                    LabelInserimentoImpresaRes.Text = CantieriImpresa1.Errore;
            }
        }

        protected void ButtonAnnullaInserimentoImpresa_Click(object sender, EventArgs e)
        {
            PanelInserimentoImpresa.Visible = false;
            LabelInserimentoImpresaRes.Text = string.Empty;
        }

        #endregion

        #region Gestione Modalità

        private void ModalitaAggiornamento()
        {
            TitoloSottotitolo1.sottoTitolo = "Modifica cantiere";
            ButtonOperazione.Text = "Aggiorna";
            aggiornamento = true;
        }

        private void ModalitaInserimento()
        {
            TitoloSottotitolo1.sottoTitolo = "Inserimento nuovo cantiere";
            ButtonOperazione.Text = "Inserisci";
            aggiornamento = false;
            LabelIdCantiere.Visible = false;
            TextBoxIdCantiere.Visible = false;
        }

        #endregion

        #region Caricamenti

        private void CaricaTipiImpresaAppaltatrice()
        {
            DropDownListTipoImpresa.Items.Clear();
            DropDownListTipoImpresa.Items.Add(string.Empty);
            DropDownListTipoImpresa.Items.Add(new ListItem("Singola", "Singola"));
            DropDownListTipoImpresa.Items.Add(new ListItem("A.T.I.", "A.T.I."));
            DropDownListTipoImpresa.SelectedValue = string.Empty;
        }

        private void CaricaTipologieAppalto()
        {
            DropDownListTipologiaAppalto.DataSource = Enum.GetNames(typeof(TipologiaAppalto));
            DropDownListTipologiaAppalto.DataBind();
        }

        private void CaricaCantiere(int idCantiere)
        {
            Cantiere cantiere = biz.GetCantiere(idCantiere);

            TextBoxIdCantiere.Text = cantiere.IdCantiere.ToString();

            Indirizzo indirizzo = new Indirizzo
            {
                NomeVia = cantiere.Indirizzo,
                Civico = cantiere.Civico,
                Provincia = cantiere.Provincia,
                Comune = cantiere.Comune,
                Cap = cantiere.Cap,
                Latitudine = (Decimal?)cantiere.Latitudine,
                Longitudine = (Decimal?)cantiere.Longitudine
            };

            IscrizioneLavoratoreIndirizzo1.CaricaDatiIndirizzo(indirizzo);

            if (cantiere.DataInizioLavori.HasValue)
                TextBoxDataInizioLavori.Text = cantiere.DataInizioLavori.Value.ToShortDateString();
            if (cantiere.DataFineLavori.HasValue)
                TextBoxDataFineLavori.Text = cantiere.DataFineLavori.Value.ToShortDateString();

            TextBoxPermessoCostruire.Text = cantiere.PermessoCostruire;
            TextBoxImporto.Text = cantiere.Importo.ToString();
            DropDownListTipologiaAppalto.SelectedValue = cantiere.TipologiaAppalto.ToString();
            CheckBoxAttivo.Checked = cantiere.Attivo;

            TextBoxResponsabileProcedimento.Text = cantiere.ResponsabileProcedimento;
            TextBoxResponsabileCantiere.Text = cantiere.ResponsabileCantiere;
            TextBoxDirezioneLavori.Text = cantiere.DirezioneLavori;
            TextBoxDescrizioneLavori.Text = cantiere.DescrizioneLavori;

            // Tipo impresa appaltatrice
            if (!string.IsNullOrEmpty(cantiere.TipoImpresaAppaltatrice))
            {
                if (DropDownListTipoImpresa.Items.FindByText(cantiere.TipoImpresaAppaltatrice) != null)
                {
                    RadioButtonDefault.Checked = true;
                    DropDownListTipoImpresa.SelectedValue = cantiere.TipoImpresaAppaltatrice;
                }
                else
                {
                    RadioButtonAltro.Checked = true;
                    TextBoxTipoImpresa.Text = cantiere.TipoImpresaAppaltatrice;
                }
            }

            // Committente
            if (cantiere.Committente != null)
            {
                TextBoxCommittente.Text = cantiere.Committente.NomeCompleto;
                ViewState["committente"] = cantiere.Committente;

                if (cantiere.Committente.FonteNotifica && !cantiere.Committente.Modificato)
                    ButtonModificaCommittente.Enabled = true;
                else
                    ButtonModificaCommittente.Enabled = false;
            }
            else if (cantiere.CommittenteTrovato != null)
            {
                TextBoxCommittente.BackColor = Color.LightCoral;
                TextBoxCommittente.Text = cantiere.CommittenteTrovato + Environment.NewLine +
                                            "[Committente non presente]";

                CantieriRicercaCommittente1.CommittenteTrovato = cantiere.CommittenteTrovato.Trim();
                CantieriRicercaCommittente1.CaricaCommittenti();
                CantieriRicercaCommittente1.Visible = true;

                CantieriCommittente1.ImpostaRagioneSociale(cantiere.CommittenteTrovato);
            }

            // Impresa appaltatrice
            if (cantiere.ImpresaAppaltatrice != null)
            {
                RadioButtonImpresaCantieri.Checked = false;
                RadioButtonImpresaSiceInfo.Checked = false;

                if (cantiere.ImpresaAppaltatrice.FonteNotifica && !cantiere.ImpresaAppaltatrice.Modificato)
                    ButtonModificaImpresa.Enabled = true;
                else
                    ButtonModificaImpresa.Enabled = false;

                if (cantiere.ImpresaAppaltatrice.TipoImpresa == TipologiaImpresa.Cantieri)
                {
                    RadioButtonImpresaCantieri.Checked = true;
                    TextBoxImpresaAppaltatrice.Text = cantiere.ImpresaAppaltatrice.NomeCompleto;
                }
                else
                {
                    RadioButtonImpresaSiceInfo.Checked = true;
                    TextBoxImpresaAppaltatrice.Text =
                        String.Format("{0} - {1}", cantiere.ImpresaAppaltatrice.IdImpresa,
                                        cantiere.ImpresaAppaltatrice.NomeCompleto);
                }
                ViewState["impresa"] = cantiere.ImpresaAppaltatrice;
            }
            else if (cantiere.ImpresaAppaltatriceTrovata != null)
            {
                TextBoxImpresaAppaltatrice.BackColor = Color.LightCoral;
                TextBoxImpresaAppaltatrice.Text = cantiere.ImpresaAppaltatriceTrovata + Environment.NewLine +
                                                    "[Impresa non presente]";

                CantieriRicercaImpresa1.ImpresaTrovata = cantiere.ImpresaAppaltatriceTrovata.Trim();
                CantieriRicercaImpresa1.CaricaImprese();
                CantieriRicercaImpresa1.Visible = true;

                CantieriImpresa1.ImpostaRagioneSociale(cantiere.ImpresaAppaltatriceTrovata);
            }

            ButtonModificaIndirizzo.Enabled = true;
        }

        #endregion

        protected void CustomValidatorIndirizzo_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (IscrizioneLavoratoreIndirizzo1.IndirizzoConfermato())
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void ButtonModificaIndirizzo_Click(object sender, EventArgs e)
        {
            ButtonModificaIndirizzo.Enabled = false;
        }
    }
}