﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class ReportCantieriAttivita : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);
            funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneVisualizzazione);
            funzionalita.Add(FunzionalitaPredefinite.CantieriProgrammazioneGestione);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            ReportViewerAttivita.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
            ReportParameter[] listaParam = null;

            string tipoReport = Request.QueryString["tipo"];
            if (tipoReport == "programmazione")
            {
                string[] nomiParametri =
                    new string[5] { "primoGiorno", "secondoGiorno", "terzoGiorno", "quartoGiorno", "quintoGiorno" };

                ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/ReportProgrammazione";
                TitoloSottotitolo1.sottoTitolo = "Programmazione settimanale";

                DateTime dal = DateTime.Parse(Request.QueryString["dal"]);
                int idIspettore = Int32.Parse(Request.QueryString["idIspettore"]);

                listaParam = new ReportParameter[6];
                listaParam[0] = new ReportParameter("idIspettore", idIspettore.ToString());

                int i = 1;
                for (DateTime giorno = dal; giorno <= dal.AddDays(6); giorno = giorno.AddDays(1))
                {
                    if (giorno.DayOfWeek != DayOfWeek.Saturday && giorno.DayOfWeek != DayOfWeek.Sunday)
                    {
                        listaParam[i] = new ReportParameter(nomiParametri[i - 1], giorno.ToShortDateString());
                        i++;
                    }
                }
            }
            else if (tipoReport == "preventivo" || tipoReport == "consuntivo")
            {
                ReportViewerAttivita.ServerReport.ReportPath = "/ReportCantieri/ReportPreventivoConsuntivoIspettore";
                if (tipoReport == "preventivo")
                    TitoloSottotitolo1.sottoTitolo = "Preventivo settimanale";
                else
                    TitoloSottotitolo1.sottoTitolo = "Consuntivo settimanale";

                DateTime dal = DateTime.Parse(Request.QueryString["dal"]);
                int idIspettore = Int32.Parse(Request.QueryString["idIspettore"]);

                listaParam = new ReportParameter[4];
                listaParam[0] = new ReportParameter("idIspettore", idIspettore.ToString());
                listaParam[1] = new ReportParameter("dataDa", dal.ToShortDateString());
                listaParam[2] = new ReportParameter("dataA", dal.AddDays(6).ToShortDateString());

                if (tipoReport == "preventivo")
                {
                    listaParam[3] = new ReportParameter("preventivato", true.ToString());
                    //listaParam[4] = new ReportParameter("consuntivato", false.ToString());
                }
                else
                {
                    //listaParam[3] = new ReportParameter("preventivato", false.ToString());
                    listaParam[3] = new ReportParameter("consuntivato", true.ToString());
                }
            }

            ReportViewerAttivita.ServerReport.SetParameters(listaParam);
        }
    }
}