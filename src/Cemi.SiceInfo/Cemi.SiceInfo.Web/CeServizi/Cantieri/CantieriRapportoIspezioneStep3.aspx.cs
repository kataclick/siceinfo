﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business.Cantieri;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.TuteScarpe.Business;
using TBridge.Cemi.Type.Collections.Cantieri;
using TBridge.Cemi.Type.Entities.Cantieri;
using TBridge.Cemi.Type.Enums.Cantieri;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using Telerik.Web.UI;


namespace Cemi.SiceInfo.Web.CeServizi.Cantieri
{
    public partial class CantieriRapportoIspezioneStep3 : System.Web.UI.Page
    {
        private readonly CantieriBusiness biz = new CantieriBusiness();
        private readonly TSBusiness tsbiz = new TSBusiness();
        private RapportoIspezione ispezione;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrev);
            funzionalita.Add(FunzionalitaPredefinite.CantieriConsuPrevRUI);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            #region biz ed eventi

            CantieriRicercaLavoratore1.OnLavoratoreSelected +=
                CantieriRicercaLavoratore1_OnLavoratoreSelected;
            CantieriRicercaLavoratore1.OnNuovoLavoratoreSelected +=
                CantieriRicercaLavoratore1_OnNuovoLavoratoreSelected;

            CantieriRicercaImpresa1.OnImpresaSelected +=
                CantieriRicercaImpresa1_OnImpresaSelected;
            CantieriRicercaImpresa1.OnNuovaImpresaSelected +=
                CantieriRicercaImpresa1_OnNuovaImpresaSelected;

            //Impostiamo le biz al controllo
            CantieriImpresa1.CantieriBiz = biz;
            CantieriImpresa1.TsBiz = tsbiz;

            #endregion

            #region set validation group

            //Impostiamo i validation group
            string validationGroupImpresa = "validationGroupImpresa";
            ButtonInserisciImpresa.ValidationGroup = validationGroupImpresa;
            CantieriImpresa1.SetValidationGroup(validationGroupImpresa);

            //Impostiamo i validation group
            //string validationGroupLavoratore = "validationGroupLavoratore";
            ButtonInserisciLavoratore.ValidationGroup = validationGroupImpresa;
            CantieriLavoratore1.SetValidationGroup(validationGroupImpresa);

            #endregion

            #region Impostiamo gli eventi JS per la gestione dei click multipli

            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();

            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('validationGroupImpresa') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciImpresa, null) + ";");
            sb.Append("return true;");
            ButtonInserisciImpresa.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciImpresa);

            ////resettiamo
            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('validationGroupLavoratore') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciNuovoLavoratore, null) + ";");
            sb.Append("return true;");
            ButtonInserisciNuovoLavoratore.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciNuovoLavoratore);

            ////resettiamo
            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('InsLav') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonInserisciLavoratore, null) + ";");
            sb.Append("return true;");
            ButtonInserisciLavoratore.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonInserisciLavoratore);

            sb.Remove(0, sb.Length);
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append("if (Page_ClientValidate('Step3') == false) { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(ButtonSalva, null) + ";");
            sb.Append("return true;");
            ButtonSalva.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);

            #endregion

            if (!Page.IsPostBack)
            {
                CaricaDati();
            }

            ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonSalva);
            ((RadScriptManager)Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(ButtonPassoPrecedente);
        }


        private void CaricaDati()
        {
            if (!String.IsNullOrEmpty(Request.QueryString["idAttivita"]) || !String.IsNullOrEmpty(Request.QueryString["idIspezione"]))
            {
                if (!String.IsNullOrEmpty(Request.QueryString["idAttivita"]))
                {
                    int idAttivita = Int32.Parse(Request.QueryString["idAttivita"]);
                    ispezione = biz.GetIspezione(idAttivita);
                }
                else
                {
                    int idIspezione = Int32.Parse(Request.QueryString["idIspezione"]);
                    ispezione = biz.GetIspezioneByKey(idIspezione);
                }

                if (ispezione != null)
                {
                    CantieriTestataIspezione1.ImpostaTestata(ispezione);

                    ispezione.RapportiImpresa =
                        biz.GetIspezioniImprese(ispezione.IdIspezione.Value, ispezione.Cantiere.IdCantiere.Value);
                    ispezione.Lavoratori = biz.GetLavoratoriIspezione(ispezione.IdIspezione);

                    CaricaDatiIspezione();

                    CaricaImprese(ispezione.RapportiImpresa);

                    ispezione.Cantiere.PresaInCarico = null;
                    ViewState["Ispezione"] = ispezione;
                }
            }
        }

        private void CaricaImprese(IEnumerable<RapportoIspezioneImpresa> rapportiImpresa)
        {
            if (rapportiImpresa != null)
            {
                DropDownListImpresa.Items.Clear();
                foreach (RapportoIspezioneImpresa rapImp in rapportiImpresa)
                {
                    DropDownListImpresa.Items.Add(
                        new ListItem(rapImp.NomeImpresa,
                                     string.Format("{0}|{1}", ((int)rapImp.Impresa.TipoImpresa), rapImp.Impresa.IdImpresa)));
                }
            }
        }

        private void CaricaDatiIspezione()
        {
            GridViewIspezioneLavoratori.DataSource = ispezione.Lavoratori;
            GridViewIspezioneLavoratori.DataBind();
        }

        protected void ButtonSalva_Click(object sender, EventArgs e)
        {
            RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

            if (biz.InsertIspezioneLavoratori(ispezione))
            {
                String redir = String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep4.aspx?idAttivita={0}&idCantiere={1}&giorno={2}&idIspettore={3}&idIspezione={4}",
                    Request.QueryString["idAttivita"],
                    Request.QueryString["idCantiere"],
                    Request.QueryString["giorno"],
                    Request.QueryString["idIspettore"],
                    Request.QueryString["idIspezione"]);
                Response.Redirect(redir);
            }
            else
                LabelRisultato.Text = "Errore durante l'inserimento dei lavoratori";
        }

        protected void ButtonPassoPrecedente_Click(object sender, EventArgs e)
        {
            RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

            if (biz.InsertIspezioneLavoratori(ispezione))
            {
                String redir = String.Format("~/CeServizi/Cantieri/CantieriRapportoIspezioneStep2.aspx?idAttivita={0}&idCantiere={1}&giorno={2}&idIspettore={3}&idIspezione={4}",
                    Request.QueryString["idAttivita"],
                    Request.QueryString["idCantiere"],
                    Request.QueryString["giorno"],
                    Request.QueryString["idIspettore"],
                    Request.QueryString["idIspezione"]);
                Response.Redirect(redir);
            }
            else
                LabelRisultato.Text = "Errore durante l'inserimento dei lavoratori";
        }

        protected void GridViewIspezioneLavoratori_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RapportoIspezioneLavoratore ispLav = (RapportoIspezioneLavoratore)e.Row.DataItem;
                Label lImpresa = (Label)e.Row.FindControl("LabelImpresaLavoratore");
                Label lLavoratore = (Label)e.Row.FindControl("LabelLavoratore");
                GridView gvLavoratoriSiceNew = (GridView)e.Row.FindControl("GridViewLavSiceNew");

                if (ispLav.Lavoratore != null)
                {
                    if (ispLav.Lavoratore.TipoLavoratore == TipologiaLavoratore.Cantieri ||
                        ispLav.Lavoratore.TipoLavoratore == TipologiaLavoratore.Notifiche)
                    {
                        lLavoratore.Text = ispLav.NomeLavoratore;

                        if (ispLav.IdIspezioneLavoratore.HasValue)
                        {
                            LavoratoreCollection lavoratori =
                                biz.GetLavoratoriConUltimoImpiego(ispLav.Lavoratore.Cognome, ispLav.Lavoratore.Nome,
                                                                  ispLav.Lavoratore.DataNascita.Value);
                            if (lavoratori != null && lavoratori.Count >= 1)
                            {
                                gvLavoratoriSiceNew.DataSource = lavoratori;
                                gvLavoratoriSiceNew.DataBind();
                            }
                        }
                    }
                    else
                        lLavoratore.Text = string.Format("{0} - {1}", ispLav.Lavoratore.IdLavoratore, ispLav.NomeLavoratore);
                }
                else
                    lLavoratore.Text = ispLav.NumeroLavoratori.ToString();

                if (ispLav.Impresa.TipoImpresa == TipologiaImpresa.Cantieri)
                    lImpresa.Text = ispLav.Impresa.RagioneSociale;
                else
                    lImpresa.Text =
                        String.Format("{0} - {1}", ispLav.Impresa.IdImpresa, ispLav.Impresa.RagioneSociale);
                lImpresa.Visible = true;
            }
        }

        protected void GridViewIspezioneLavoratori_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

            if (GridViewIspezioneLavoratori.DataKeys[e.RowIndex].Value != null)
            {
                int idIspezioneLavoratore = (int)GridViewIspezioneLavoratori.DataKeys[e.RowIndex].Value;
                if (biz.DeleteIspezioneLavoratore(idIspezioneLavoratore))
                    ispezione.Lavoratori.RemoveAt(e.RowIndex);
            }
            else
            {
                ispezione.Lavoratori.RemoveAt(e.RowIndex);
            }

            this.ispezione = ispezione;
            CaricaDatiIspezione();
        }

        protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
        {
            CantieriRicercaLavoratore1.Visible = true;
        }

        protected void ButtonInserisciLavoratore_Click(object sender, EventArgs e)
        {
            if ((string)ViewState["Modalita"] == "Singolo")
            {
                if (ViewState["Lavoratore"] != null && ViewState["Impresa"] != null)
                {
                    Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
                    Impresa impresa = (Impresa)ViewState["Impresa"];
                    RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

                    // Altro costruttore per inserire numero lavoratori           
                    ispezione.Lavoratori.Add(new RapportoIspezioneLavoratore(null, lavoratore, impresa));

                    this.ispezione = ispezione;
                    CaricaDatiIspezione();
                    PanelAggiungiLavoratore.Visible = false;
                }
                else
                {
                    LabelResInserimentoLavInLista.Text =
                        "E' necessario selezionare sia il lavoratore che l'impresa a cui ha dichiarato di appartenere";
                }
            }
            else if ((string)ViewState["Modalita"] == "Gruppo")
            {
                int numeroLavoratori;

                if (ViewState["Impresa"] != null && int.TryParse(TextBoxLavoratore.Text, out numeroLavoratori))
                {
                    Impresa impresa = (Impresa)ViewState["Impresa"];
                    RapportoIspezione ispezione = (RapportoIspezione)ViewState["Ispezione"];

                    // Altro costruttore per inserire numero lavoratori
                    ispezione.Lavoratori.Add(new RapportoIspezioneLavoratore(null, numeroLavoratori, impresa));

                    this.ispezione = ispezione;
                    CaricaDatiIspezione();
                    PanelAggiungiLavoratore.Visible = false;
                }
                else
                {
                    LabelResInserimentoLavInLista.Text =
                        "E' necessario impostare correttamente sia il numero dei lavoratori che l'impresa a cui appartengono";
                }
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            ResetCampi();

            PanelAggiungiLavoratore.Visible = false;
        }

        private void ResetCampi()
        {
            //resettiamo un po' di cose...
            ViewState["Lavoratore"] = null;
            ViewState["Impresa"] = null;

            TextBoxImpresa.Text = string.Empty;
            TextBoxLavoratore.Text = string.Empty;

            LabelResInserimentoLavInLista.Text = string.Empty;
            LabelRisultato.Text = string.Empty;
        }

        //protected void DropDownListImpresa_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    SelezionaImpresaDaDropDowList();
        //}


        protected void ButtonaggiungiGruppo_Click(object sender, EventArgs e)
        {
            ResetCampi();

            ViewState["Modalita"] = "Gruppo";
            PanelAggiungiLavoratore.Visible = true;
            ButtonVisualizzaRicercaLavoratori.Visible = false;
            TextBoxLavoratore.Enabled = true;
            CantieriRicercaLavoratore1.Visible = false;
            PanelRicercaInserisciLavoratore.Visible = false;

            LabelModalitaLavInserimento.Text = "Numero lavoratori da inserire";
        }

        protected void ButtonaggiungiLavoratore_Click(object sender, EventArgs e)
        {
            ResetCampi();

            ViewState["Modalita"] = "Singolo";
            PanelAggiungiLavoratore.Visible = true;
            ButtonVisualizzaRicercaLavoratori.Visible = true;
            TextBoxLavoratore.Enabled = false;

            LabelModalitaLavInserimento.Text = "Lavoratore da inserire";
        }

        protected void ButtonAnnullaPasso3_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Cantieri/CantieriDefault.aspx");
        }

        protected void GridViewLavSiceNew_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridView gvLavoratoriSiceNew = (GridView)sender;

            int? idLavoratore = (int?)gvLavoratoriSiceNew.DataKeys[e.NewSelectedIndex].Value;

            if (idLavoratore.HasValue)
            {
                int indiceLav = ((GridViewRow)(((Control)sender).Parent.Parent)).RowIndex;
                int idIspezioneLav = (int)GridViewIspezioneLavoratori.DataKeys[indiceLav].Value;

                if (biz.UpdateIspezioneLavoratore(idIspezioneLav, idLavoratore.Value))
                    CaricaDati();
            }
        }

        #region Ricerca/ Seleziona Impresa 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonVisualizzaRicercaImprese_Click(object sender, EventArgs e)
        {
            //pannello che contiene la dropdown e il tasto altro
            PanelSelezionaImpresaDaDropDown.Visible = true;
            //premendo il tasto altro viene aperto nu pannello per la ricerca inserimento estesa
        }

        /// <summary>
        /// é stata richiesta la gestione di un impresa fuori lista 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAltro_Click1(object sender, EventArgs e)
        {
            //Visualizziamo il pannello che contiene la gestione altra impresa
            PanelRicercaImpresaEstesa.Visible = true;
            //inizialmetne visualizziamo solo la ricerca sulle anagrafiche
            CantieriRicercaImpresa1.Visible = true;
            PanelInserisciNuovaImpresa.Visible = false;
        }

        protected void ButtonSelezionaImpresaDaDropDownList_Click(object sender, EventArgs e)
        {
            SelezionaImpresaDaDropDowList();
        }

        private void SelezionaImpresaDaDropDowList()
        {
            string[] tipoId = DropDownListImpresa.SelectedValue.Split('|');
            TipologiaImpresa tipoImpresa = (TipologiaImpresa)Int32.Parse(tipoId[0]);

            Impresa impresa = new Impresa();
            impresa.TipoImpresa = tipoImpresa;
            impresa.IdImpresa = Int32.Parse(tipoId[1]);
            impresa.RagioneSociale = DropDownListImpresa.SelectedItem.Text;

            ImpresaSelezionata(impresa);
            //this.PanelSelezionaImpresaDaDropDown.Visible = false;
        }

        private void ImpresaSelezionata(Impresa impresa)
        {
            ViewState["Impresa"] = impresa;
            TextBoxImpresa.Text = impresa.RagioneSociale; //DropDownListImpresa.SelectedItem.Text;

            //NASCONDIAMO tutte le possibilità di selezionare un'impresa
            PanelSelezionaImpresaDaDropDown.Visible = false;
            PanelRicercaImpresaEstesa.Visible = false;
            CantieriRicercaImpresa1.Visible = false;
            PanelInserisciNuovaImpresa.Visible = false;
        }

        private void CantieriRicercaImpresa1_OnImpresaSelected(Impresa impresa)
        {
            ImpresaSelezionata(impresa);
            //ViewState["Impresa"] = impresa;
            //TextBoxImpresa.Text = impresa.RagioneSociale;

            //this.CantieriRicercaImpresa1.Visible = false;
            //this.PanelRicercaImpresa.Visible = false;
        }

        private void CantieriRicercaImpresa1_OnNuovaImpresaSelected(object sender, EventArgs e)
        {
            PanelRicercaImpresaEstesa.Visible = true;
            CantieriRicercaImpresa1.Visible = false;
            PanelInserisciNuovaImpresa.Visible = true;
            //throw new Exception("The method or operation is not implemented.");
        }

        #region eventi inserimento impresa

        protected void ButtonInserisciImpresa_Click(object sender, EventArgs e)
        {
            Impresa impresa = CantieriImpresa1.impresa;

            if (impresa != null)
            {
                // Inserisco il record
                biz.InsertImpresa(impresa);

                if (impresa.IdImpresa > 0)
                {
                    ImpresaSelezionata(impresa);
                    CantieriImpresa1.Reset();
                }
                else
                    LabelInserimentoNuovaImpresaRes.Text = "Si è verificato un errore durante l'inserimento";
            }
            else
                LabelInserimentoNuovaImpresaRes.Text = "Non tutti i campi sono corretti";
        }

        /// <summary>
        /// Annulla Tutto l'inserimento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAnnullaInserimentoImpresa_Click(object sender, EventArgs e)
        {
            //this.PanelSelezionaImpresaDaDropDown.Visible = false;
            PanelRicercaImpresaEstesa.Visible = false;
            CantieriRicercaImpresa1.Visible = false;
            PanelInserisciNuovaImpresa.Visible = false;
            //throw new Exception();
        }

        #endregion

        #endregion

        #region Eventi gestione inserimento ricerca lavoratore

        /// <summary>
        /// Visualizziamo la ricerca dei lavoratori
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonVisualizzaRicercaLavoratori_Click(object sender, EventArgs e)
        {
            PanelRicercaInserisciLavoratore.Visible = true;
            //visualizziamo la ricerca
            CantieriRicercaLavoratore1.Visible = true;
            //nascondiamo l'inserimento di un nuovo lavoratore
            PanelInserisciLavoratore.Visible = false;
        }

        /// <summary>
        /// Evento per l'inserimento di un nuovo lavoratore
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CantieriRicercaLavoratore1_OnNuovoLavoratoreSelected(object sender, EventArgs e)
        {
            PanelRicercaInserisciLavoratore.Visible = true;
            //nascondiamo il controllo di ricerca
            CantieriRicercaLavoratore1.Visible = false;
            //visualizziamo il controllo di inserimento
            PanelInserisciLavoratore.Visible = true;
        }

        private void CantieriRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
        {
            LavoratoreSelezionato(lavoratore);
        }

        /// <summary>
        /// Memorizziamo la selezione di un nuovo lavoratore
        /// </summary>
        /// <param name="lavoratore"></param>
        private void LavoratoreSelezionato(Lavoratore lavoratore)
        {
            ViewState["Lavoratore"] = lavoratore;
            TextBoxLavoratore.Text = lavoratore.Cognome + " " + lavoratore.Nome;

            PanelRicercaInserisciLavoratore.Visible = false;
            CantieriRicercaLavoratore1.Visible = false;
            PanelInserisciLavoratore.Visible = false;
            //this.CantieriRicercaLavoratore1.Visible = false;
        }

        protected void ButtonInserisciNuovoLavoratore_Click(object sender, EventArgs e)
        {
            Lavoratore lavoratore = CantieriLavoratore1.Lavoratore;

            if (lavoratore != null)
            {
                // Inserisco il record
                biz.InsertLavoratore(lavoratore);
                if (lavoratore.IdLavoratore > 0)
                {
                    LavoratoreSelezionato(lavoratore);
                }
                else
                    LabelInserimentoNuovoLavoratoreRes.Text = "Si è verificato un errore durante l'operazione";
            }
            else
                LabelInserimentoNuovoLavoratoreRes.Text = "Non tutti i campi sono corretti";
        }

        protected void ButtonAnnullaInserimentoLavoratore_Click(object sender, EventArgs e)
        {
            PanelRicercaInserisciLavoratore.Visible = false;
            CantieriRicercaLavoratore1.Visible = false;
            PanelInserisciLavoratore.Visible = false;
        }

        #endregion
    }
}