﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CantieriDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.CantieriDefault" %>

<%@ Register Src="../WebControls/MenuCantieriStatistiche.ascx" TagName="MenuCantieriStatistiche"
    TagPrefix="uc3" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%@ Register Src="../WebControls/MenuCantieri.ascx" TagName="MenuCantieri" TagPrefix="uc1" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
    <uc3:MenuCantieriStatistiche id="MenuCantieriStatistiche1" runat="server">
    </uc3:MenuCantieriStatistiche>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Cantieri"/>
    <br />
    <p class="DefaultPage">
    Cantieri è la sezione dalla quale gli ispettori e il responsabile ufficio ispettori
    potranno gestire la programmazione delle loro attività, i preventivi e i consuntivi
    di tali attività e compilare i rapporti di ispezione.</p><br />
</asp:Content>


