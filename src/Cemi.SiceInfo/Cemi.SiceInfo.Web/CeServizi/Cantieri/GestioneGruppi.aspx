﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneGruppi.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Cantieri.GestioneGruppi" %>

<%@ Register src="../WebControls/MenuCantieri.ascx" tagname="MenuCantieri" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/DatiGruppoIspezione.ascx" tagname="DatiGruppoIspezione" tagprefix="uc3" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuCantieri ID="MenuCantieri1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Cantieri" sottoTitolo="Gestione Gruppi" />
    <br />
    <br />
    <b>
        Gruppi presenti
    </b>
    <telerik:RadGrid
        ID="RadGridGruppiIspezione"
        runat="server" 
        GridLines="None"
        AutoGenerateColumns="false" 
        onitemdatabound="RadGridGruppiIspezione_ItemDataBound" 
        onitemcommand="RadGridGruppiIspezione_ItemCommand">
        <MasterTableView DataKeyNames="IdCantieriGruppoIspezione">
        <RowIndicatorColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>
        <ExpandCollapseColumn>
        <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="Descrizione" HeaderText="Descrizione" 
                    UniqueName="descrizione">
                    <ItemStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Ispettori" UniqueName="ispettori">
                    <ItemTemplate>
                        <telerik:RadListView 
                            ID="RadListViewIspettori"
                            runat="server"
                            onitemdatabound="RadListViewIspettori_ItemDataBound">
                            <ItemTemplate>
                                <b>
                                    <asp:Label 
                                        ID="LabelIspettore" 
                                        runat="server">
                                    </asp:Label>
                                </b>
                                <br />
                            </ItemTemplate>
                        </telerik:RadListView>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn CommandName="elimina" ButtonType="ImageButton" 
                    ImageUrl="../images/pallinoX.png" Text="" UniqueName="elimina" >
                <ItemStyle Width="25px" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:Button
        ID="ButtonNuovo"
        runat="server"
        Width="150px" onclick="ButtonNuovo_Click" Text="Nuovo gruppo" />
    <br />
    <br />
</asp:Content>


<asp:Content ID="Content5" runat="server" contentplaceholderid="MainPage2">
    <asp:Panel
        ID="PanelNuovoGruppoIspezione"
        runat="server"
        Visible="false">
        <table class="filledtable">
            <tr>
                <td>
                    Nuovo gruppo
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    <uc3:DatiGruppoIspezione ID="DatiGruppoIspezione1" runat="server" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary
            ID="ValidationSummeryNuovoGruppo"
            runat="server"
            ValidationGroup="datiGruppoIspezione"
            CssClass="messaggiErrore" />
        <br />
        <asp:Button
            ID="ButtonInserisci"
            runat="server"
            Text="Inserisci" 
            ValidationGroup="datiGruppoIspezione"
            onclick="ButtonInserisci_Click" />
    </asp:Panel>
</asp:Content>




