﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportRicevutaFast.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ReportRicevutaFast" %>

<%@ Register src="../WebControls/MenuPrestazioni.ascx" tagname="MenuPrestazioni" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Prestazioni" sottoTitolo="Ricevuta Fast" />
    <br />
    <table style="height: 600px; width: 740px">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewer" runat="server" Height="600px"
                    ProcessingMode="Remote" Width="100%" ShowDocumentMapButton="False" ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False">
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
