﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneDomande.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.GestioneDomande" %>

<%@ Register Src="WebControls/PrestazioniRicercaDomande.ascx" TagName="PrestazioniRicercaDomande" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc2" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuPrestazioni id="MenuPrestazioni1" runat="server">
    </uc2:MenuPrestazioni>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Gestione domande"
        titolo="Prestazioni" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:PrestazioniRicercaDomande ID="PrestazioniRicercaDomande1" runat="server" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>