﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class GestioneDomandeNonConfermate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore);

            #endregion

            PrestazioniRicercaDomandeNonConfermate1.OnDomandaTemporaneaSelected +=
                PrestazioniRicercaDomandeNonConfermate1_OnDomandaTemporaneaSelected;

            if (!Page.IsPostBack)
            {
                //TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore lavoratore =
                //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore)
                //        HttpContext.Current.User.Identity).Entity;
                Lavoratore lavoratore =
                    (Lavoratore)
                    GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                PrestazioniRicercaDomandeNonConfermate1.ImpostaIdLavoratore(lavoratore.IdLavoratore);
            }
        }

        private void PrestazioniRicercaDomandeNonConfermate1_OnDomandaTemporaneaSelected(int idDomanda)
        {
            Context.Items["IdDomandaTemporanea"] = idDomanda;
            Server.Transfer("~/CeServizi/Prestazioni/CompilazioneDomanda.aspx");
        }
    }
}