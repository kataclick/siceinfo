﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ControlloDomandaUnivocita.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ControlloDomandaUnivocita" %>

<%@ Register Src="WebControls/PrestazioniDatiDomanda.ascx" TagName="PrestazioniDatiDomanda" TagPrefix="uc3" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda - Univocità" titolo="Prestazioni" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc3:PrestazioniDatiDomanda ID="PrestazioniDatiDomanda1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" Font-Bold="true" Text="Prestazioni erogate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewPrestazioniErogate" runat="server" AutoGenerateColumns="False" width="100%" AllowPaging="True" OnPageIndexChanging="GridViewPrestazioniErogate_PageIndexChanging" OnRowDataBound="GridViewPrestazioniErogate_RowDataBound" PageSize="5">
                    <Columns>
                        <asp:BoundField HeaderText="Data Domanda" DataField="DataDomanda" DataFormatString="{0:dd/MM/yyyy}" >
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Tipo prestazione" DataField="TipoPrestazione" >
                            <ItemStyle Width="200px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Beneficiario">
                            <ItemTemplate>
                                <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Stato" DataField="IdStatoPrestazione" >
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ImportoLiquidabile" HeaderText="Importo Liquidabile" DataFormatString="{0:0.00}">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna prestazione erogata nell'anagrafica
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>    
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" Font-Bold="true" Text="Domande presentate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewDomandePresentate" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="GridViewDomandePresentate_PageIndexChanging" OnRowDataBound="GridViewDomandePresentate_RowDataBound" PageSize="10" width="100%">
                    <Columns>
                        <asp:BoundField DataField="DataConferma" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data compilazione">
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdTipoPrestazione" HeaderText="Tipo Prestazione">
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Beneficiario">
                            <ItemTemplate>
                                <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presentata nell'anagrafica
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr runat="server" id="SoggiornoEstivoTitle" visible="false">
            <td>
                <asp:Label ID="LabelQuorum" Font-Bold="true" Text="Massimale prestazioni erogate" runat="server" /><asp:Label runat="server" ID="labelQuorumLavoratore" />
            </td>
        </tr>
        <tr runat="server" id="SoggiornoEstivoQuorum" visible="false">
            <td>
                <asp:GridView ID="GridViewSoggiornoEstivoQuorum" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="GridViewSoggiornoEstivoQuorum_PageIndexChanging" OnRowDataBound="GridViewSoggiornoEstivoQuorum_RowDataBound" PageSize="10" width="100%">
                    <Columns>
                        <asp:BoundField HeaderText="Beneficiario" DataField="Beneficiario">                            
                        </asp:BoundField>
                        <asp:BoundField DataField="IdTipoPrestazione" HeaderText="Tipo Prestazione">
                            <ItemStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Importo" HeaderText="Importo erogato" DataFormatString="{0:0.00}">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Massimale disponibile">
                            <ItemTemplate>
                                <asp:Image ID="ImageQuorum" runat="server" ImageUrl="~/CeServizi/images/semaforoVerde.png" />
                            </ItemTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna domanda presentata nell'anagrafica
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonForza" runat="server" Text="Forza controllo" OnClick="ButtonForza_Click" Width="150px" Enabled="False" />&nbsp;
                <asp:Button ID="ButtonIndietro" runat="server" Text="Indietro" OnClick="ButtonIndietro_Click" Width="150px" />
            </td>
        </tr>
    </table>
</asp:Content>

