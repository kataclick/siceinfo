﻿using System;
using System.Web.UI;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ControlloDomandaDatiAnagrafici : System.Web.UI.Page
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            #endregion

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDomanda"] == null)
                {
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
                }

                int idDomanda = (int)Context.Items["IdDomanda"];
                Domanda domanda = biz.GetDomanda(idDomanda);
                biz.ControllaDomandaLavoratore(domanda);

                CaricaDomanda(domanda);

                //In funzione dei ruoli e dell'utente definisce le azioni possibili sui controlli
                AbilitaGestione(domanda);
            }
        }

        /// <summary>
        /// Abilita le azioni che può fare l'utente in funzione del ruolo e della proprietà della domanda 
        /// </summary>
        /// <param name="domanda">Domanda in gestione</param>
        private void AbilitaGestione(Domanda domanda)
        {
            //IUtente utente = ApplicationInstance.GetUtenteSistema();
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            //Se l'utente è chi ha la domanda in carico o ha funzioni amministrative, può getire la domanda
            if (idUtente == domanda.IdUtenteInCarico ||
                GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin.ToString()))
            {
                //Tutto normale
            }
            else
            {
                //Tutto viene disabilitato
                PanelAzioni.Enabled = false;
            }
        }

        private void CaricaDomanda(Domanda domanda)
        {
            ViewState["Domanda"] = domanda;
            LabelAnagraficaIndirizzo.Text = domanda.Lavoratore.Indirizzo.IndirizzoVia;
            LabelAnagraficaProvincia.Text = domanda.Lavoratore.Indirizzo.Provincia;
            LabelAnagraficaComune.Text = domanda.Lavoratore.Indirizzo.Comune;
            LabelAnagraficaFrazione.Text = domanda.Lavoratore.Indirizzo.Frazione;
            LabelAnagraficaCap.Text = domanda.Lavoratore.Indirizzo.Cap;

            LabelComunicatoIndirizzo.Text = domanda.Lavoratore.IndirizzoFornito.IndirizzoVia;
            LabelComunicatoProvincia.Text = domanda.Lavoratore.IndirizzoFornito.Provincia;
            LabelComunicatoComune.Text = domanda.Lavoratore.IndirizzoFornito.Comune;
            LabelComunicatoFrazione.Text = domanda.Lavoratore.IndirizzoFornito.Frazione;
            LabelComunicatoCap.Text = domanda.Lavoratore.IndirizzoFornito.Cap;

            LabelAnagraficaEmail.Text = domanda.Lavoratore.Comunicazioni.Email;
            LabelAnagraficaCellulare.Text = domanda.Lavoratore.Comunicazioni.Cellulare;

            LabelComunicatoEmail.Text = domanda.Lavoratore.ComunicazioniFornite.Email;
            LabelComunicatoCellulare.Text = domanda.Lavoratore.ComunicazioniFornite.Cellulare;

            CaricaSemafori(domanda);

            if (!domanda.Lavoratore.ControlloIndirizzo.HasValue)
                ButtonForzaIndirizzo.Enabled = true;
            else
                ButtonForzaIndirizzo.Enabled = false;

            if (!domanda.Lavoratore.ControlloEmail.HasValue)
                ButtonForzaEmail.Enabled = true;
            else
                ButtonForzaEmail.Enabled = false;

            if (!domanda.Lavoratore.ControlloCellulare.HasValue)
                ButtonForzaCellulare.Enabled = true;
            else
                ButtonForzaCellulare.Enabled = false;

            PrestazioniDatiDomanda1.CaricaDomanda(domanda);
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Domanda domanda = (Domanda)ViewState["Domanda"];
            Context.Items["IdDomanda"] = domanda.IdDomanda.Value;

            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomanda.aspx");
        }

        private void CaricaSemafori(Domanda domanda)
        {
            BulletedListControlli.Items.Clear();

            ImageControlloIndirizzo.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.Lavoratore.ControlloIndirizzo);
            if (domanda.Lavoratore.ControlloIndirizzo.HasValue && !domanda.Lavoratore.ControlloIndirizzo.Value)
                BulletedListControlli.Items.Add("Il lavoratore non ha un indirizzo in anagrafica");

            ImageControlloEmail.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.Lavoratore.ControlloEmail);
            if (domanda.Lavoratore.ControlloEmail.HasValue && !domanda.Lavoratore.ControlloEmail.Value)
                BulletedListControlli.Items.Add("Il lavoratore non ha un indirizzo email anagrafica");

            ImageControlloCellulare.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.Lavoratore.ControlloCellulare);
            if (domanda.Lavoratore.ControlloCellulare.HasValue && !domanda.Lavoratore.ControlloCellulare.Value)
                BulletedListControlli.Items.Add("Il lavoratore non ha un numero di cellulare in anagrafica");
        }

        #region Bottoni forzatura

        protected void ButtonForzaIndirizzo_Click(object sender, EventArgs e)
        {
            Domanda domanda = (Domanda)ViewState["Domanda"];

            if (biz.ForzaLavoratoreIndirizzo(domanda.IdDomanda.Value))
            {
                biz.ControllaDomandaLavoratore(domanda);
                CaricaDomanda(domanda);
            }
        }

        protected void ButtonForzaEmail_Click(object sender, EventArgs e)
        {
            Domanda domanda = (Domanda)ViewState["Domanda"];

            if (biz.ForzaLavoratoreEmail(domanda.IdDomanda.Value))
            {
                biz.ControllaDomandaLavoratore(domanda);
                CaricaDomanda(domanda);
            }
        }

        protected void ButtonForzaCellulare_Click(object sender, EventArgs e)
        {
            Domanda domanda = (Domanda)ViewState["Domanda"];

            if (biz.ForzaLavoratoreCellulare(domanda.IdDomanda.Value))
            {
                biz.ControllaDomandaLavoratore(domanda);
                CaricaDomanda(domanda);
            }
        }

        #endregion
    }
}