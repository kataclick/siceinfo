﻿<%@ Page Title="" EnableEventValidation="false" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfermaSanitarie.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ConfermaSanitarie" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Moduli"
        titolo="Prestazioni" />
    &nbsp;<br />
    La compilazione della domanda è completata.<br />
    Occorre stampare il modulo, <b>firmarlo</b> e spedirlo o consegnarlo <b>IN ORIGINALE</b> 
    alla Cassa Edile insieme ai documenti previsti (elencati sulla domanda di prestazione).<br />
    <br />
    <asp:Button
        ID="ButtonStampaModulo" runat="server" 
        Text="Stampa il modulo" Width="250px" 
        onclick="ButtonStampaModulo_Click" />
    &nbsp;
    <asp:Button
        ID="ButtonStampaRicevutaNascosto" runat="server" 
        Text="Stampa il modulo" Width="250px" Visible="false" 
        onclick="ButtonStampaRicevutaNascosto_Click" />
    <br />
    <br />
    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Clicca per inserire una nuova domanda" Width="250px" />
    &nbsp;
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Clicca per tornare alla Home Page" Width="250px" /><br />
    <br />
</asp:Content>
