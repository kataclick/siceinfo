﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneRicevuta.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.GestioneRicevuta" %>

<%@ Register src="../WebControls/MenuPrestazioni.ascx" tagname="MenuPrestazioni" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/PrestazioniSelezionaDocumentiRicevuta.ascx" tagname="PrestazioniSelezionaDocumentiRicevuta" tagprefix="uc3" %>
<%@ Register src="WebControls/PrestazioniDatiDomanda.ascx" tagname="PrestazioniDatiDomanda" tagprefix="uc4" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="MainPage">
    <p>
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" 
    sottoTitolo="Selezionare i documenti consegnati" titolo="Gestione ricevuta" />
    <br />
    <uc4:PrestazioniDatiDomanda ID="PrestazioniDatiDomanda1" runat="server" />
    <br />
    <uc3:PrestazioniSelezionaDocumentiRicevuta ID="PrestazioniSelezionaDocumentiRicevuta1" 
    runat="server" />
    <br />
    <asp:Button ID="ButtonSalvaStampa" runat="server" Text="Salva e stampa" 
        onclick="ButtonSalvaStampa_Click" Width="150px" />
    <asp:Button ID="ButtonAnnulla" runat="server" Text="Indietro" 
        onclick="ButtonAnnulla_Click" Width="150px"/>
</p>

</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="MenuDettaglio">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" 
        runat="server" />
</asp:Content>