﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ConfermaSalvataggioTemporaneo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda);

            #endregion
        }
    }
}