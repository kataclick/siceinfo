﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfermaSalvataggioTemporaneo.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ConfermaSalvataggioTemporaneo" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Conferma salvataggio temporaneo"
        titolo="Prestazioni" />
    <br />
    <p>
        Il salvataggio temporaneo è avvenuto correttamente. La domanda può essere recuperata dalla
        <a href="GestioneDomandeNonConfermate.aspx" runat="server">Gestione domande non confermate</a>.
    </p>
</asp:Content>