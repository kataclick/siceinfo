﻿using System;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Entities.GestioneUtenti;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class GestioneDomandePersonali : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomandeLavoratore);

            #endregion

            if (!Page.IsPostBack)
            {
                //TBridge.Cemi.GestioneUtenti.Type.Entities.Lavoratore lavoratore =
                //        ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore)
                //        HttpContext.Current.User.Identity).Entity;
                Lavoratore lavoratore =
                    (Lavoratore)
                    GestioneUtentiBiz.GetIdentitaUtenteCorrente();

                PrestazioniRicercaDomandeLavoratore1.ImpostaIdLavoratore(lavoratore.IdLavoratore);
            }
        }
    }
}