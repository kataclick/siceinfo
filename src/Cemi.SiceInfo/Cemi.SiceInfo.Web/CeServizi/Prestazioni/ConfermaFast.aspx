﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfermaFast.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ConfermaFast" %>

<%@ Register src="../WebControls/MenuPrestazioni.ascx" tagname="MenuPrestazioni" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/StampaCopertine.ascx" tagname="StampaCopertine" tagprefix="uc3" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Prestazioni" sottoTitolo="Ricevuta Fast" />
    <br />
    La domanda è stata correttamente inserita.
    <br />
    <b>
        <asp:Label
            ID="LabelRespinta"
            runat="server"
            Text="Come richiesto è stata respinta."
            Visible="false">
        </asp:Label>
    </b>
    <br />
    <br />
    <asp:Button ID="ButtonNuovaDomanda" runat="server" OnClick="ButtonIndietro_Click" 
        Text="Nuova domanda FAST" Width="200px" />
    &nbsp;
    <asp:Button
        ID="ButtonStampaRicevuta" runat="server" 
        Text="Stampa la ricevuta" Width="200px" 
        onclick="ButtonStampaRicevuta_Click" />
    &nbsp;
    <asp:Button
        ID="ButtonStampaModulo" runat="server" OnClick="Button1_Click" 
        Text="Stampa il modulo" Width="200px" />
    <br />
    <asp:Button
        ID="ButtonStampaCopertine" runat="server" 
        Text="Stampa le copertine" Width="200px" Visible="false" 
        onclick="ButtonStampaCopertine_Click" />
    &nbsp;
    <asp:Button
        ID="ButtonStampaRicevutaNascosto" runat="server" 
        Text="Stampa la ricevuta" Width="200px" Visible="false" 
        onclick="ButtonStampaRicevutaNascosto_Click" />
    &nbsp;
    <asp:Button ID="ButtonGestisci" runat="server"
        Text="Gestisci la domanda" Width="200px" onclick="ButtonGestisci_Click" Visible="false" />
    <br />
    <br />
    <uc3:StampaCopertine ID="StampaCopertine1" runat="server" />
</asp:Content>


