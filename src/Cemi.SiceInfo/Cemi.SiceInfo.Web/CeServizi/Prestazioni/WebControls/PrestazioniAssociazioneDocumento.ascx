﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniAssociazioneDocumento.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniAssociazioneDocumento" %>

<table class="standardTable">
    <tr>
        <td colspan="2">
            <b>Documento</b>
        </td>
    </tr>
    <tr>
        <td>
            Tipologia
        </td>
        <td>
            <asp:Label ID="LabelTipoDocumento" runat="server"></asp:Label>
            <asp:Label ID="LabelIdTipoDocumento" runat="server" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Codice documento
        </td>
        <td>
            <asp:Label ID="LabelIdDocumento" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Codice Archidoc
        </td>
        <td>
            <asp:Label ID="LabelIdArchidoc" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <b>Persona documento</b>
        </td>
    </tr>
    <tr>
        <td>
            Persona
        </td>
        <td>
            <asp:RadioButton ID="RadioButtonLavoratore" runat="server" Checked="true" GroupName="persona"
                Text="Lavoratore" Enabled="False" />
            <asp:RadioButton ID="RadioButtonFamiliare" runat="server" Checked="false" GroupName="persona"
                Text="Familiare" Enabled="False" />
            <asp:CheckBox ID="CheckBoxPerDomanda" runat="server" Visible="False" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="ButtonAssocia" runat="server" Text="Associa" OnClick="ButtonAssocia_Click" />
        </td>
    </tr>
</table>
