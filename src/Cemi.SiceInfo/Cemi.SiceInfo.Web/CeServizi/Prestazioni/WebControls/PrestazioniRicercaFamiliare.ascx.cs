﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Delegates.Prestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniRicercaFamiliare : System.Web.UI.UserControl
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        public event FamiliareSelectedEventHandler OnFamiliareSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void GridViewFamiliari_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int idLavoratore = (int)GridViewFamiliari.DataKeys[e.NewSelectedIndex].Values["IdLavoratore"];
            int idFamiliare = (int)GridViewFamiliari.DataKeys[e.NewSelectedIndex].Values["IdFamiliare"];

            Familiare familiare = biz.GetFamiliare(idLavoratore, idFamiliare);


            if (OnFamiliareSelected != null)
                OnFamiliareSelected(familiare);
        }



        public void CaricaFamiliari(FamiliareCollection familiari)
        {
            Presenter.CaricaElementiInGridView(
                GridViewFamiliari,
                familiari,
                0);
        }

        /// <summary>
        /// Carica l'elenco dei familiare a partire da un idLavoratore
        /// </summary>
        /// <param name="idLavoratore"></param>
        public void CaricaFamiliari(int idLavoratore)
        {
            Presenter.CaricaElementiInGridView(
                GridViewFamiliari,
                biz.GetFamiliariPerGradoParentela(idLavoratore, string.Empty, false),
                0);
        }

        protected void GridViewFamiliari_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Familiare familiare = (Familiare)e.Row.DataItem;

                Label lACarico = (Label)e.Row.FindControl("LabelACarico");

                if (!String.IsNullOrEmpty(familiare.ACarico))
                {
                    switch (familiare.ACarico)
                    {
                        case "S":
                            lACarico.Text = "Sì";
                            break;
                        case "N":
                            lACarico.Text = "No";
                            break;
                    }
                }
            }
        }
    }
}