﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniDatiNuovoFamiliare.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniDatiNuovoFamiliare" %>

<table class="borderedTable">
    <tr>
        <td>
            Cognome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognome" runat="server" ControlToValidate="TextBoxCognome"
                ValidationGroup="datiFamiliare">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server" ControlToValidate="TextBoxNome"
                ValidationGroup="datiFamiliare">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data nascita*
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataNascita" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidatorDataNascita" runat="server" ControlToValidate="TextBoxDataNascita"
                Type="Date" Operator="DataTypeCheck" ErrorMessage="Data di nascita non valida"
                ValidationGroup="datiFamiliare">*</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataNascita" runat="server"
                ControlToValidate="TextBoxDataNascita" ValidationGroup="datiFamiliare">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            * campi obbligatori
        </td>
    </tr>
</table>
