﻿using System;
using System.Web.UI;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniDatiLavoratore : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaLavoratore(Lavoratore lavoratore)
        {
            LabelCodice.Text = lavoratore.IdLavoratore.ToString();
            LabelCognome.Text = lavoratore.Cognome;
            LabelNome.Text = lavoratore.Nome;
        }

        private void Reset()
        {
            LabelCodice.Text = null;
            LabelCognome.Text = null;
            LabelNome.Text = null;
        }
    }
}