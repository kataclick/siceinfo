﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Prestazioni;
using Telerik.Web.UI;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class StampaCopertine : System.Web.UI.UserControl
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();
        private Int32 stampaIndex = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Int32 idDomanda = (Int32)Context.Items["IdDomanda"];
                ViewState["IdDomanda"] = idDomanda;

                Domanda domanda = biz.GetDomanda(idDomanda);

                // Etichette
                LabelTipoPrestazione.Text = String.Format("{0} {1}", domanda.TipoPrestazione.IdTipoPrestazione, domanda.TipoPrestazione.Descrizione);
                LabelProtocollo.Text = String.Format("{0}/{1}", domanda.ProtocolloPrestazione, domanda.NumeroProtocolloPrestazione);

                // Documenti
                CaricaDocumenti(idDomanda);
            }
        }

        public void CaricaDocumenti(Int32 idDomanda)
        {
            DocumentoCopertinaCollection documenti = biz.GetDocumentiStampaCopertine(idDomanda);

            LabelDocumentiTotali.Text = String.Format("{0} doc", documenti.Count);
            LabelDocumentiConsegnati.Text = String.Format("{0} doc", documenti.NumeroDocumentiConsegnati);

            Presenter.CaricaElementiInGridView(
                GridViewDocumenti,
                documenti,
                0);
        }

        protected void GridViewDocumentiRichiesti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DocumentoCopertina documento = (DocumentoCopertina)e.Row.DataItem;

                Label lProgressivo = (Label)e.Row.FindControl("LabelProgressivo");
                Label lBeneficiario = (Label)e.Row.FindControl("LabelBeneficiario");
                Label lIdTipoDocumento = (Label)e.Row.FindControl("LabelIdTipoDocumento");
                Label lIdFatturaDichiarata = (Label)e.Row.FindControl("LabelIdFatturaDichiarata");
                CheckBox cbConsegnato = (CheckBox)e.Row.FindControl("CheckBoxConsegnato");
                CustomValidator cvOriginale = (CustomValidator)e.Row.FindControl("CustomValidatorOriginale");
                RadButton buttonOriginale = (RadButton)e.Row.FindControl("RadButtonOriginale");


                lProgressivo.Text = (e.Row.RowIndex + 1).ToString();

                cbConsegnato.Enabled = false;
                cbConsegnato.Checked = documento.Consegnato;

                if (!String.IsNullOrEmpty(documento.GradoParentelaDocumento))
                {
                    lBeneficiario.Text = biz.TrasformaGradoParentelaInDescrizione(documento.GradoParentelaDocumento);
                }
                else
                {
                    lBeneficiario.Text = String.Format("{0:dd/MM/yyyy} - {1}", documento.FatturaData, documento.FatturaNumero);
                }
                lIdTipoDocumento.Text = documento.IdTipoDocumento.ToString();
                lIdFatturaDichiarata.Text = documento.IdFatturaDichiarata.ToString();

                cvOriginale.ErrorMessage = String.Format("Impostare il flag di originalità sul documento #{0}", e.Row.RowIndex + 1);

                // Originale su Archidoc è presente solo perMODULO_RICHIESTA CERTIFICATO e FATTURA
                if (documento.IdTipoDocumentoArchidoc != 1
                    && documento.IdTipoDocumentoArchidoc != 4
                    && documento.IdTipoDocumentoArchidoc != 6)
                {
                    cvOriginale.Enabled = false;
                    buttonOriginale.Visible = false;
                }
            }
        }

        protected void ButtonStampaTutto_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];

            Server.Transfer("~/CeServizi/Prestazioni/ReportCopertine.aspx");
        }

        protected void ButtonStampaConsegnati_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Context.Items["Consegnati"] = true;

            Server.Transfer("~/CeServizi/Prestazioni/ReportCopertine.aspx");
        }

        protected void ButtonStampaSelezionati_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DocumentoStampaCopertinaCollection tipiDocumentoDaStampare = new DocumentoStampaCopertinaCollection();
                DocumentoStampaCopertinaCollection fattureDaStampare = new DocumentoStampaCopertinaCollection();
                foreach (GridViewRow row in GridViewDocumenti.Rows)
                {
                    Label lIdTipoDocumento = (Label)row.FindControl("LabelIdTipoDocumento");
                    Label lIdFatturaDichiarata = (Label)row.FindControl("LabelIdFatturaDichiarata");
                    CheckBox checkBoxStampa = (CheckBox)row.FindControl("CheckBoxStampa");
                    RadButton buttonOriginale = (RadButton)row.FindControl("RadButtonOriginale");
                    Boolean? originale;

                    if (checkBoxStampa.Checked)
                    {
                        switch (buttonOriginale.SelectedToggleState.Value)
                        {
                            case "S":
                                originale = true;
                                break;
                            case "N":
                                originale = false;
                                break;
                            default:
                                originale = null;
                                break;
                        }

                        if (!String.IsNullOrEmpty(lIdTipoDocumento.Text))
                        {
                            tipiDocumentoDaStampare.Add(new DocumentoStampaCopertina(lIdTipoDocumento.Text, originale));
                        }
                        else if (!String.IsNullOrEmpty(lIdFatturaDichiarata.Text))
                        {
                            fattureDaStampare.Add(new DocumentoStampaCopertina(lIdFatturaDichiarata.Text, originale));
                        }
                    }

                }

                if (tipiDocumentoDaStampare.Count > 0 || fattureDaStampare.Count > 0)
                {
                    Context.Items["IdDomanda"] = ViewState["IdDomanda"];
                    Context.Items["TipiDocumentoList"] = tipiDocumentoDaStampare;
                    Context.Items["FattureDichiarateList"] = fattureDaStampare;
                    Server.Transfer("~/CeServizi/Prestazioni/ReportCopertine.aspx");
                }
            }
        }

        protected void GridViewDocumenti_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            //stampaIndex = e.NewSelectedIndex;
            //Page.Validate("stampa");

            //if (Page.IsValid)
            //{
            //    Context.Items["IdDomanda"] = ViewState["IdDomanda"];

            //    Label lIdTipoDocumento = (Label) GridViewDocumenti.Rows[e.NewSelectedIndex].FindControl("LabelIdTipoDocumento");
            //    Label lIdFatturaDichiarata = (Label) GridViewDocumenti.Rows[e.NewSelectedIndex].FindControl("LabelIdFatturaDichiarata");

            //    Context.Items["IdTipiDocumentoList"] = new List<String> { lIdTipoDocumento.Text };

            //    // if (!String.IsNullOrEmpty(lIdFatturaDichiarata.Text) )
            //    // {
            //    Context.Items["IdFattureDichiarateList"] = new List<String> { lIdFatturaDichiarata.Text };
            //    // }

            //    Server.Transfer("~/Prestazioni/ReportCopertine.aspx");
            //}
        }



        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            // Se sono nella pagina di conferma FAST devo mettere "in carico"
            // la domanda
            if (Request.Url.ToString().ToUpper().Contains("FAST"))
            {
                Int32 idDomanda = (Int32)ViewState["IdDomanda"];
                PrestazioniBusiness biz = new PrestazioniBusiness();
                Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

                biz.UpdateDomandaSetPresaInCarico(idDomanda, idUtente, true);
            }

            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomanda.aspx");
        }

        protected void CustomValidatorOriginale_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator cvOriginale = (CustomValidator)source;

            CheckBox cbStampa = (CheckBox)cvOriginale.NamingContainer.FindControl("CheckBoxStampa");
            RadButton rbOriginale = (RadButton)cvOriginale.NamingContainer.FindControl("RadButtonOriginale");

            args.IsValid = true;

            if ((String.IsNullOrEmpty(rbOriginale.SelectedToggleState.Value) || rbOriginale.SelectedToggleState.Value == "?")
                && ((stampaIndex == -1 && cbStampa.Checked) || stampaIndex == ((GridViewRow)cvOriginale.NamingContainer).RowIndex))
            {
                args.IsValid = false;
            }
        }

        protected void CheckBoxSelezionaTutti_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewDocumenti.Rows)
            {
                CheckBox cbStampa = (CheckBox)row.FindControl("CheckBoxStampa");
                cbStampa.Checked = CheckBoxSelezionaTutti.Checked;
            }

            CheckBoxSelezionaConsegnati.Checked = false;

            GestisciTestiCheckBoxSelezione();
        }

        protected void CheckBoxSelezionaConsegnati_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewDocumenti.Rows)
            {
                CheckBox cbStampa = (CheckBox)row.FindControl("CheckBoxStampa");
                CheckBox cbConsegnato = (CheckBox)row.FindControl("CheckBoxConsegnato");

                cbStampa.Checked = CheckBoxSelezionaConsegnati.Checked && cbConsegnato.Checked;
            }

            CheckBoxSelezionaTutti.Checked = false;

            GestisciTestiCheckBoxSelezione();
        }

        protected void GestisciTestiCheckBoxSelezione()
        {
            if (CheckBoxSelezionaTutti.Checked)
            {
                CheckBoxSelezionaTutti.Text = "Deseleziona tutti";
            }
            else
            {
                CheckBoxSelezionaTutti.Text = "Seleziona tutti";
            }

            if (CheckBoxSelezionaConsegnati.Checked)
            {
                CheckBoxSelezionaConsegnati.Text = "Deseleziona consegnati";
            }
            else
            {
                CheckBoxSelezionaConsegnati.Text = "Seleziona consegnati";
            }
        }

        protected void CheckBoxSelezionaTuttiOriginali_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewDocumenti.Rows)
            {
                CheckBox cbStampa = (CheckBox)row.FindControl("CheckBoxStampa");
                RadButton rbOriginale = (RadButton)row.FindControl("RadButtonOriginale");

                if (rbOriginale.Visible)
                {
                    if (CheckBoxSelezionaTuttiOriginali.Checked)
                    {
                        rbOriginale.SelectedToggleStateIndex = 2;
                    }
                    else
                    {
                        rbOriginale.SelectedToggleStateIndex = 0;
                    }
                }

                CheckBoxSelezionaTuttiNonOriginali.Checked = false;
            }
        }

        protected void CheckBoxSelezionaTuttiNonOriginali_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridViewDocumenti.Rows)
            {
                CheckBox cbStampa = (CheckBox)row.FindControl("CheckBoxStampa");
                RadButton rbOriginale = (RadButton)row.FindControl("RadButtonOriginale");

                if (rbOriginale.Visible)
                {
                    if (CheckBoxSelezionaTuttiNonOriginali.Checked)
                    {
                        rbOriginale.SelectedToggleStateIndex = 1;
                    }
                    else
                    {
                        rbOriginale.SelectedToggleStateIndex = 0;
                    }
                }

                CheckBoxSelezionaTuttiOriginali.Checked = false;
            }
        }
    }
}