﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniDatiFattura : System.Web.UI.UserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void ImpostaValidatorPerDataFattura(int limiteFattureMesi)
        {
            CustomValidatorFattureNuovaDataLimite.ErrorMessage =
                String.Format(
                    "La data della fattura non può essere antecedente il {0}° mese rispetto alla data della domanda",
                    limiteFattureMesi);
            TextBoxLimiteFatturaAntecedente.Text = DateTime.Now.AddMonths(-limiteFattureMesi).ToShortDateString();
            CompareValidatorFattureNuovaDataLimiteOggi.ValueToCompare = DateTime.Today.ToShortDateString();
        }

        //public void ImpostaValidatorPerRangeDataFattura(DateTime dateMin, DateTime dateMax)
        //{
        //    CustomValidatorFattureRangeSoggiornoEstivo.ErrorMessage =
        //        String.Format(
        //            "La data della fattura deve essere compresa tra il 01/06/{0} e il 30/09/{0}",
        //            DateTime.Now.Year);
        //    CustomValidatorFattureRangeSoggiornoEstivo.MinimumValue = dateMin.ToShortDateString();
        //    CustomValidatorFattureRangeSoggiornoEstivo.MaximumValue = dateMax.ToShortDateString();
        //}

        

        public FatturaDichiarata CreaFattura()
        {
            FatturaDichiarata fattura = new FatturaDichiarata();

            fattura.Data = DateTime.Parse(TextBoxFattureNuovaData.Text.Replace('.', '/'));
            fattura.Numero = TextBoxFattureNuovaNumero.Text.Trim().ToUpper();
            fattura.ImportoTotale = decimal.Parse(TextBoxFattureNuovaImporto.Text);
            //fattura.Saldo = RadioButtonAnticipoNo.Checked;
            fattura.Saldo = true;

            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            if (idUtente > 0)
            {
                fattura.IdUtenteInserimento = idUtente;
            }

            return fattura;
        }

        public void ResetCampi()
        {
            TextBoxFattureNuovaData.Text = null;
            TextBoxFattureNuovaNumero.Text = null;
            TextBoxFattureNuovaImporto.Text = null;
        }

        protected void CustomValidatorFattureNuovaDataLimite_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            // Faccio uil controllo Data fattura - Data domanda solo sulla fattura a saldo
            if (RadioButtonAnticipoNo.Checked)
            {
                DateTime dataFattura = DateTime.Parse(TextBoxFattureNuovaData.Text.Replace('.', '/'));
                DateTime dataLimite = DateTime.Parse(TextBoxLimiteFatturaAntecedente.Text.Replace('.', '/'));

                if (dataFattura < dataLimite)
                    args.IsValid = false;
            }
        }
        protected void CustomValidatorFattureRangeSoggiornoEstivo_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (ViewState["SoggiornoEstivo"] != null && (bool)ViewState["SoggiornoEstivo"] == true)
            {
                if (DateTime.Parse(TextBoxFattureNuovaData.Text).Year != DateTime.Now.Year)
                {
                    args.IsValid = false;
                }
            }

        }

    }
}