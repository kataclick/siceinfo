﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniDatiNuovoFamiliare : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public Familiare GetFamiliare()
        {
            Familiare familiare = null;

            Page.Validate("datiFamiliare");
            if (Page.IsValid)
            {
                familiare = new Familiare();

                familiare.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
                familiare.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
                familiare.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
            }

            return familiare;
        }

        public void ResetCampi()
        {
            Presenter.SvuotaCampo(TextBoxCognome);
            Presenter.SvuotaCampo(TextBoxNome);
            Presenter.SvuotaCampo(TextBoxDataNascita);
        }
    }
}