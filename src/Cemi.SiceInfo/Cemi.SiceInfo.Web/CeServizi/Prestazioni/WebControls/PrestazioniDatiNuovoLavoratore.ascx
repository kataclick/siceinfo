﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniDatiNuovoLavoratore.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniDatiNuovoLavoratore" %>

<table  class="borderedTable" >
    <tr>
        <td>
            Cognome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCognome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCognome" runat="server"
                ControlToValidate="TextBoxCognome" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Nome*
        </td>
        <td>
            <asp:TextBox ID="TextBoxNome" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNome" runat="server"
                ControlToValidate="TextBoxNome" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Data nascita*
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataNascita" runat="server" Width="300px" MaxLength="10"></asp:TextBox>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidatorDataNascita" runat="server" ControlToValidate="TextBoxDataNascita"
                Type="Date" Operator="DataTypeCheck" ErrorMessage="Data di nascita non valida" ValidationGroup="datiLavoratore">*</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDataNascita" runat="server"
                ControlToValidate="TextBoxDataNascita" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Codice fiscale*
        </td>
        <td>
            <asp:TextBox ID="TextBoxCodiceFiscale" runat="server" Width="300px" MaxLength="16"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodiceFiscale" runat="server"
                ControlToValidate="TextBoxCodiceFiscale" ValidationGroup="datiLavoratore">*</asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionCF" runat="server" ControlToValidate="TextBoxCodiceFiscale"
                ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}|\d{11}$"
                ValidationGroup="datiLavoratore">*</asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            * campi obbligatori
        </td>
    </tr>
</table>