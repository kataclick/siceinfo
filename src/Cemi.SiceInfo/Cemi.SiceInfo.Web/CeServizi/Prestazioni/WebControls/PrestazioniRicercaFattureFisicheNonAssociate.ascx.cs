﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Delegates.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniRicercaFattureFisicheNonAssociate : System.Web.UI.UserControl
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        public event FatturaSelectedEventHandler OnFatturaSelected;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void ButtonFatturaCerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                GridViewFattureRicevute.Visible = true;
                GridViewFattureRicevute.PageIndex = 0;
                CaricaFatture();
            }
        }

        private void CaricaFatture()
        {
            decimal? importo = null;
            DateTime? data = null;

            if (!string.IsNullOrEmpty(TextBoxFatturaImporto.Text))
                importo = decimal.Parse(TextBoxFatturaImporto.Text);

            if (!string.IsNullOrEmpty(TextBoxFatturaData.Text))
                data = DateTime.Parse(TextBoxFatturaData.Text.Replace('.', '/'));

            FatturaCollection fatture = biz.getFattureFisicheAssociabili(data, TextBoxFatturaNumero.Text, importo, false);
            GridViewFattureRicevute.DataSource = fatture;
            GridViewFattureRicevute.DataBind();
        }

        protected void GridViewFattureRicevute_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            string idArchidoc = (string)GridViewFattureRicevute.DataKeys[e.NewSelectedIndex].Values["IdArchidoc"];

            // Caricare il documento tramite il Web Service Archidoc
            IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
            byte[] file = servizioArchidoc.GetDocument(idArchidoc);
            if (file != null)
            {
                Presenter.RestituisciFileArchidoc(idArchidoc, file, Page);
            }
        }

        protected void GridViewFattureRicevute_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridViewFattureRicevute.PageIndex = e.NewPageIndex;
            CaricaFatture();
        }

        public void Reset()
        {
            GridViewFattureRicevute.Visible = false;
            GridViewFattureRicevute.DataSource = null;
            GridViewFattureRicevute.DataBind();
        }
        protected void GridViewFattureRicevute_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Associa")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                int idFattura = (int)GridViewFattureRicevute.DataKeys[index].Values["IdPrestazioniFattura"];

                if (OnFatturaSelected != null)
                    OnFatturaSelected(idFattura);
            }
        }
    }
}