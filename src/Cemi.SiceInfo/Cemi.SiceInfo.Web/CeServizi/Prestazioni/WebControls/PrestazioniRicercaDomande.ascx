﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniRicercaDomande.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniRicercaDomande" %>

<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonVisualizza">
    <table class="standardTable">
        <tr>
            <td>
                <table class="borderedTable">
                    <tr>
                        <td>
                            Data dom. - Dal (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorDataDomandaDal" runat="server" ControlToValidate="TextBoxDataDomandaDal"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date" ValidationGroup="ricerca" ForeColor="Red"></asp:CompareValidator>
                        </td>
                        <td>
                            Data dom. - Al (gg/mm/aaaa)
                            <asp:CompareValidator ID="CompareValidatorDataDomandaAl" runat="server" ControlToValidate="TextBoxDataDomandaAl"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date" ValidationGroup="ricerca" ForeColor="Red"></asp:CompareValidator>
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox
                                ID="TextBoxDataDomandaDal"
                                runat="server"
                                Width="100%"
                                MaxLength="10">
                            </asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox
                                ID="TextBoxDataDomandaAl"
                                runat="server"
                                Width="100%"
                                MaxLength="10">
                            </asp:TextBox>
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            Tipo prestazione
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <%--<asp:DropDownList ID="DropDownListTipoPrestazione" runat="server" AppendDataBoundItems="true"
                                Width="540px">
                            </asp:DropDownList>--%>
                            <telerik:RadComboBox ID="DropDownListTipoPrestazione" runat="server" AppendDataBoundItems="true"
                                Width="540px" EmptyMessage="Selezionare il tipo prestazione">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Codice lav.
                            <asp:CompareValidator ID="CompareValidatorIdLavoratore" runat="server" ControlToValidate="TextBoxIdLavoratore"
                                ErrorMessage="*" Operator="GreaterThan" Type="Integer" ValidationGroup="ricerca"
                                ValueToCompare="0" ForeColor="Red"></asp:CompareValidator>
                        </td>
                        <td>
                            Cognome lav.
                        </td>
                        <td>
                            Nome lav.
                        </td>
                        <td>
                            Data nascita lav.
                            <asp:CompareValidator ID="CompareValidatorDataNascita" runat="server" ControlToValidate="TextBoxDataNascitaLavoratore"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date" ValidationGroup="ricerca" ForeColor="Red"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxIdLavoratore" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxCognomeLavoratore" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxNomeLavoratore" runat="server" MaxLength="30" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxDataNascitaLavoratore" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Stato
                        </td>
                        <td>
                        </td>
                        <td>
                            Tipo Inserimento
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownListStato" runat="server" AppendDataBoundItems="true"
                                Width="100%">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListInCarico" runat="server" Width="180px">
                                <asp:ListItem>Tutte</asp:ListItem>
                                <asp:ListItem Value="InCarico">In carico</asp:ListItem>
                                <asp:ListItem Value="NonInCarico">Non in carico</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CheckBox ID="CheckBoxInCarico" runat="server" Text="In carico" Width="70px"
                                Visible="False" />
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownListTipoInserimento" runat="server" Width="180px">
                                <asp:ListItem>Tutte</asp:ListItem>
                                <asp:ListItem Value="Normale">Normali</asp:ListItem>
                                <asp:ListItem Value="Fast">Fast</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Anno (aaaa)
                            <asp:CompareValidator ID="CompareValidatorAnno1" runat="server" ControlToValidate="TextBoxAnno"
                                ErrorMessage="*" Operator="GreaterThanEqual" Type="Integer" ValueToCompare="1900" ForeColor="Red"></asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidatorAnno2" runat="server" ControlToValidate="TextBoxAnno"
                                ErrorMessage="*" Operator="LessThanEqual" Type="Integer" ValueToCompare="2100" ForeColor="Red"></asp:CompareValidator>
                        </td>
                        <td>
                            Prot. prestazione
                            <asp:CompareValidator ID="CompareValidatorProtocollo" runat="server" ControlToValidate="TextBoxProtocollo"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Integer" ForeColor="Red"></asp:CompareValidator>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxAnno" runat="server" MaxLength="4" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxProtocollo" runat="server" MaxLength="10" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Width="100%"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right">
                            &nbsp;
                            <asp:Button ID="ButtonVisualizza" runat="server" Text="Ricerca" OnClick="ButtonVisualizza_Click"
                                ValidationGroup="ricerca" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="LabelErrore" runat="server" ForeColor="Red" Text="Si è verificato un errore durante l'operazione"
                    Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco domande"></asp:Label><br />
                <asp:GridView ID="GridViewDomande" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowPaging="True" DataKeyNames="IdDomanda,IdUtenteInCarico" OnPageIndexChanging="GridViewDomande_PageIndexChanging"
                    OnSelectedIndexChanging="GridViewDomande_SelectedIndexChanging" OnRowDataBound="GridViewDomande_RowDataBound"
                    OnRowDeleting="GridViewDomande_RowDeleting">
                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                    <EmptyDataTemplate>
                        Nessuna domanda trovata
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Prestazione">
                            <ItemTemplate>
                                <table class="standardTable">
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelTipoPrestazione" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="LabelProtocollo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <ItemStyle Width="80px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DataConferma" DataFormatString="{0:dd/MM/yyyy}" 
                            HeaderText="Data comp.">
                            <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TipoInserimento" HeaderText="Tipo Ins." 
                            ItemStyle-Width="30px">
                            <ItemStyle Width="40px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Cod. lav.">
                            <ItemTemplate>
                                <asp:Label ID="LabelIdLavoratore" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cognome">
                            <ItemTemplate>
                                <asp:Label ID="LabelCognome" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Font-Bold="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nome">
                            <ItemTemplate>
                                <asp:Label ID="LabelNome" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Font-Bold="True" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stato">
                            <ItemTemplate>
                                <asp:Label ID="LabelStato" runat="server"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="60px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="LoginInCarico" HeaderText="In carico">
                            <ItemStyle Width="60px" />
                        </asp:BoundField>
                        <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Image" SelectText="Visualizza"
                            ShowSelectButton="True" SelectImageUrl="../../images/edit.png">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:CommandField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="ButtonAzione" runat="server" Text="In carico" CommandName="Delete"
                                    CommandArgument='<%# Container.DataItemIndex %>' Width="80px" />
                            </ItemTemplate>
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
