﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniDatiFattura.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniDatiFattura" %>

<asp:TextBox ID="TextBoxLimiteFatturaAntecedente" runat="server" Visible="false"></asp:TextBox>
<table class="borderedTable">
    <tr>
        <td colspan="3">
            <asp:Label ID="LabelDatiFatturaTitolo" runat="server" Font-Bold="true" Text="Nuova fattura"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Data (gg/mm/aaaa)
        </td>
        <td>
            <asp:TextBox ID="TextBoxFattureNuovaData" runat="server" MaxLength="10" Width="350px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorFattureNuovaData" runat="server"
                ControlToValidate="TextBoxFattureNuovaData" ErrorMessage="Data fattura mancante"  ValidationGroup="nuovaFattura">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorFattureNuovaData" runat="server" ControlToValidate="TextBoxFattureNuovaData" ErrorMessage="Formato data errato" Operator="DataTypeCheck" Type="Date" ValidationGroup="nuovaFattura">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidatorFattureNuovaDataLimiteOggi" runat="server" ControlToValidate="TextBoxFattureNuovaData"
                ErrorMessage="La data della fattura non pu&#242; essere successiva al giorno di inserimento della domanda"
                Operator="LessThanEqual" Type="Date" ValidationGroup="nuovaFattura">*</asp:CompareValidator>
            <asp:CustomValidator ID="CustomValidatorFattureNuovaDataLimite" runat="server" ControlToValidate="TextBoxFattureNuovaData" OnServerValidate="CustomValidatorFattureNuovaDataLimite_ServerValidate" ValidationGroup="nuovaFattura">*</asp:CustomValidator>
            <asp:CustomValidator ID="CustomValidatorFattureRangeSoggiornoEstivo" runat="server" ControlToValidate="TextBoxFattureNuovaData" OnServerValidate="CustomValidatorFattureRangeSoggiornoEstivo_ServerValidate" ValidationGroup="nuovaFattura">*</asp:CustomValidator>
            <%--<asp:RangeValidator ID="CustomValidatorFattureRangeSoggiornoEstivo"  runat="server" ControlToValidate="TextBoxFattureNuovaData" ValidationGroup="nuovaFattura">*</asp:RangeValidator>--%>
        </td>
    </tr>
    <tr>
        <td>
            Numero
        </td>
        <td>
            <asp:TextBox ID="TextBoxFattureNuovaNumero" runat="server" MaxLength="20" Width="350px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorFattureNuovaNumero" runat="server" ErrorMessage="Numero fattura mancante" ControlToValidate="TextBoxFattureNuovaNumero" ValidationGroup="nuovaFattura">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Importo
        </td>
        <td>
            <asp:TextBox ID="TextBoxFattureNuovaImporto" runat="server" MaxLength="20" Width="350px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorFattureNuovaImporto" runat="server"
                ControlToValidate="TextBoxFattureNuovaImporto" ErrorMessage="Importo fattura mancante" ValidationGroup="nuovaFattura">*</asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidatorFattureNuovaImporto" runat="server" ControlToValidate="TextBoxFattureNuovaImporto" ErrorMessage="Formato dell'importo errato" Operator="DataTypeCheck" Type="Currency" ValidationGroup="nuovaFattura">*</asp:CompareValidator>
            <asp:CompareValidator ID="CompareValidatorFattureNuovaImportoMaggioreZero" runat="server"
                ControlToValidate="TextBoxFattureNuovaImporto" ErrorMessage="L'importo deve essere maggiore di 0"
                Operator="GreaterThan" Type="Currency" ValidationGroup="nuovaFattura" ValueToCompare="0">*</asp:CompareValidator></td>
    </tr>
    <tr id="trAcconto" runat="server" visible="false">
        <td>
            E' un acconto?&nbsp;</td>
        <td>
            &nbsp;<asp:RadioButton ID="RadioButtonAnticipoSi" runat="server" GroupName="anticipo"
                Text="Sì" />
            <asp:RadioButton ID="RadioButtonAnticipoNo" runat="server" Checked="True" GroupName="anticipo"
                Text="No" /></td>
        <td>
        </td>
    </tr>
</table>