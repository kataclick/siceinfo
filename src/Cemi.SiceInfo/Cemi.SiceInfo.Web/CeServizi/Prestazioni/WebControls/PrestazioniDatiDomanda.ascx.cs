﻿using System;
using System.Web.UI;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniDatiDomanda : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void CaricaDomanda(Domanda domanda)
        {
            LabelTipoPrestazione.Text = String.Format("{0} - {1}", domanda.TipoPrestazione.IdTipoPrestazione, domanda.TipoPrestazione.Descrizione);
            LabelCodicePrestazione.Text = domanda.ProtocolloPrestazioneCompleto;

            LabelLavoratore.Text = String.Format("{0} - {1} {2}",
                                                 domanda.Lavoratore.IdLavoratore,
                                                 domanda.Lavoratore.Cognome,
                                                 domanda.Lavoratore.Nome);

            if (domanda.Beneficiario == "L")
            {
                LabelBeneficiario.Text = "Lavoratore";
            }
            else
            {
                if (domanda.Familiare != null)
                    LabelBeneficiario.Text = domanda.Familiare.NomeCompleto;
                else if (domanda.FamiliareFornito != null)
                    LabelBeneficiario.Text = domanda.FamiliareFornito.NomeCompleto;
            }
        }
    }
}