﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Type.Entities.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniDatiNuovoLavoratore : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public Lavoratore GetLavoratore()
        {
            Lavoratore lavoratore = null;

            Page.Validate("datiLavoratore");
            if (Page.IsValid)
            {
                lavoratore = new Lavoratore();

                if (ViewState["IdLavoratore"] != null)
                {
                    lavoratore.IdLavoratore = (Int32)ViewState["IdLavoratore"];
                }

                lavoratore.Cognome = Presenter.NormalizzaCampoTesto(TextBoxCognome.Text);
                lavoratore.Nome = Presenter.NormalizzaCampoTesto(TextBoxNome.Text);
                lavoratore.DataNascita = DateTime.Parse(TextBoxDataNascita.Text.Replace('.', '/'));
                lavoratore.CodiceFiscale = Presenter.NormalizzaCampoTesto(TextBoxCodiceFiscale.Text);
            }

            return lavoratore;
        }

        public void ResetCampi()
        {
            Presenter.SvuotaCampo(TextBoxCognome);
            Presenter.SvuotaCampo(TextBoxNome);
            Presenter.SvuotaCampo(TextBoxCodiceFiscale);
            Presenter.SvuotaCampo(TextBoxDataNascita);
        }
    }
}