﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.Business;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Entities;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls
{
    public partial class PrestazioniSelezioneCasseEdili : System.Web.UI.UserControl
    {
        private readonly Common commonBiz = new Common();

        protected void Page_Load(object sender, EventArgs e)
        {
            bool forzatura = false;

            if (ViewState["Forzatura"] != null)
                forzatura = true;

            if (!Page.IsPostBack && !forzatura)
            {
                CassaEdileCollection casseEdiliSelezionate = new CassaEdileCollection();
                ViewState["CasseEdili"] = casseEdiliSelezionate;
                CaricaCasseEdili();
                CaricaCasseEdiliSelezionate();
            }
        }

        private void CaricaCasseEdiliSelezionate()
        {
            GridViewCasseEdili.DataSource = ViewState["CasseEdili"];
            GridViewCasseEdili.DataBind();
        }

        private void CaricaCasseEdili()
        {
            DropDownListCasseEdili.Items.Clear();
            DropDownListCasseEdili.Items.Add(new ListItem(string.Empty, string.Empty));

            CassaEdileCollection casseEdili = commonBiz.GetCasseEdiliSenzaMilano();
            DropDownListCasseEdili.DataSource = casseEdili;
            DropDownListCasseEdili.DataTextField = "Descrizione";
            DropDownListCasseEdili.DataValueField = "IdComposito";
            DropDownListCasseEdili.DataBind();
            DropDownListCasseEdili.Text = string.Empty;
        }

        protected void ButtonAggiungiCassaEdile_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CassaEdileCollection casseEdiliSelezionate = (CassaEdileCollection)ViewState["CasseEdili"];
                CassaEdile cassaEdile = new CassaEdile();

                char[] separator = new char[1];
                separator[0] = CassaEdile.IDCOMPOSTOSEPARATOR;
                string[] IdComposito = DropDownListCasseEdili.SelectedItem.Value.Split(separator);

                cassaEdile.IdCassaEdile = IdComposito[0];
                cassaEdile.Cnce = bool.Parse(IdComposito[1]);
                cassaEdile.Descrizione = DropDownListCasseEdili.SelectedItem.Text;

                if (!IsCassaEdilePresente(casseEdiliSelezionate, cassaEdile))
                {
                    casseEdiliSelezionate.Add(cassaEdile);
                    ViewState["CasseEdili"] = casseEdiliSelezionate;
                    CaricaCasseEdiliSelezionate();
                }
            }
        }

        private bool IsCassaEdilePresente(CassaEdileCollection casseEdili, CassaEdile cassaEdile)
        {
            if (casseEdili != null && cassaEdile != null && !string.IsNullOrEmpty(cassaEdile.IdCassaEdile))
            {
                foreach (CassaEdile cassa in casseEdili)
                {
                    if (cassa.IdCassaEdile == cassaEdile.IdCassaEdile)
                        return true;
                }
            }

            return false;
        }

        public CassaEdileCollection GetCasseEdili()
        {
            return (CassaEdileCollection)ViewState["CasseEdili"];
        }

        protected void GridViewCasseEdili_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            CassaEdileCollection casseEdili = (CassaEdileCollection)ViewState["CasseEdili"];
            casseEdili.RemoveAt(e.RowIndex);
            CaricaCasseEdiliSelezionate();
        }

        public void CaricaCasseEdili(CassaEdileCollection casseEdili)
        {
            ViewState["CasseEdili"] = casseEdili;
            ViewState["Forzatura"] = true;
            CaricaCasseEdili();
            CaricaCasseEdiliSelezionate();
        }
    }
}