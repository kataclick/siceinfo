﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniSelezionaDocumentiRicevuta.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniSelezionaDocumentiRicevuta" %>

<asp:Label ID="LabelErroreVisualizzazioneDocumento" runat="server" Text="Documento non trovato o errore durante la lettura da Archidoc"
                    Visible="False" ForeColor="Red">
                </asp:Label>
<asp:GridView runat="server" AutoGenerateColumns="False" ID="GridViewDocumentiRichiesti"
    OnRowDataBound="GridViewDocumentiRichiesti_RowDataBound" 
    DataKeyNames="TipoDocumento,IdArchidoc" Width="95%" 
    onrowcommand="GridViewDocumentiRichiesti_RowCommand">
    <Columns>
        <asp:BoundField DataField="TipoDocumento" HeaderText="Tipo documento">
        <ItemStyle Font-Bold="True" Width="250px" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="Relativo a">
            <ItemTemplate>
                <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
            </ItemTemplate>
            <ItemStyle Width="150px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Stato">
            <ItemTemplate>
                <table style="padding:0px; border-spacing:0px;" cellpadding="0px" cellspacing="0px">
                    <tr>
                        <td style="text-align:left;">
                            <asp:Label ID="LabelStato" runat="server"></asp:Label>
                        </td>
                        <td style="text-align:right;">
                            <asp:Button 
                                ID="ButtonVisualizza"
                                runat="server"
                                Text="Vis."
                                CommandName="visualizzadocumento"
                                CommandArgument="<%# Container.DataItemIndex %>" />
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Consegnato">
            <ItemTemplate>
                <asp:CheckBox ID="CheckBoxConsegnato" runat="server" Checked="True" />
            </ItemTemplate>
            <ItemStyle Width="10px" />
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        Nessuno
    </EmptyDataTemplate>
</asp:GridView>