﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StampaCopertine.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.StampaCopertine" %>

<b>
    Stampa copertine
</b>
<div class="borderedDiv">
<script type="text/javascript">
    function CheckSelezionati() {
        var table = document.getElementById('<%= GridViewDocumenti.ClientID %>');
        var selected = false;
        for (var i = 0; (i < table.rows.length && selected == false); i++) {
            var columnStampa = table.rows[i].cells[1];
            var inputs = columnStampa.getElementsByTagName('input');
            for (var j = 0; j < inputs.length; j++) {

                if (inputs[j].type == 'checkbox' && inputs[j].checked == true) {
                    selected = true;
                    break;
                }
            }

        }

        if (selected == false) {
            alert('Nessun documento selezionato');
            return false;
        }
        else {
            return true;
        }
    }
    </script>
    <br />
    <table class="borderedTable">
        <tr>
            <td style="width: 207px">
                Tipo prestazione
            </td>
            <td colspan="2">
                <b>
                    <asp:Label ID="LabelTipoPrestazione" runat="server"></asp:Label>
                </b>
            </td>
        </tr>
        <tr>
            <td style="width: 207px">
                Protocollo
            </td>
            <td colspan="2">
                <b>
                    <asp:Label ID="LabelProtocollo" runat="server"></asp:Label>
                </b>
            </td>
        </tr>
    </table>
    <br />
    <asp:Button
        ID="ButtonIndietro"
        runat="server"
        Text="Gestione domanda"
        Width="200px" onclick="ButtonIndietro_Click" />
    <br />
    <br />
    <asp:Button
        ID="ButtonStampaTutto"
        runat="server"
        Text="Stampa tutto"
        Width="200px" 
        onclick="ButtonStampaTutto_Click"
        Visible="false" />
    Totali:
    <asp:Label
        ID="LabelDocumentiTotali"
        runat="server">
    </asp:Label>
    <br />
    <asp:Button
        ID="ButtonStampaConsegnati"
        runat="server"
        Text="Stampa i consegnati"
        Width="200px" 
        onclick="ButtonStampaConsegnati_Click"
        Visible="false" />
    Consegnati: 
    <asp:Label
        ID="LabelDocumentiConsegnati"
        runat="server">
    </asp:Label>
    <br />
    <%--<br />
    <asp:Button
        ID="ButtonStampaSelezionati"
        runat="server"
        Text="Stampa i selezionati"
        Width="200px" 
        ValidationGroup="stampa"
        CausesValidation="true"
        OnClientClick="return CheckSelezionati()"
        onclick="ButtonStampaSelezionati_Click" />
    <br />--%>
    <br />
    <table class="standardTable">
        <tr>
            <td style="width: 165px">
                <asp:CheckBox
                    ID="CheckBoxSelezionaTutti"
                    runat="server"
                    AutoPostBack="true"
                    Text="Seleziona tutti" 
                    oncheckedchanged="CheckBoxSelezionaTutti_CheckedChanged" />
                <br />
                <asp:CheckBox
                    ID="CheckBoxSelezionaConsegnati"
                    runat="server"
                    AutoPostBack="true"
                    Text="Seleziona consegnati" 
                    oncheckedchanged="CheckBoxSelezionaConsegnati_CheckedChanged" />
            </td>
            <td>
                <asp:CheckBox
                    ID="CheckBoxSelezionaTuttiOriginali"
                    runat="server"
                    AutoPostBack="true"
                    Text="Imposta tutti originali" 
                    oncheckedchanged="CheckBoxSelezionaTuttiOriginali_CheckedChanged" />
                <br />
                <asp:CheckBox
                    ID="CheckBoxSelezionaTuttiNonOriginali"
                    runat="server"
                    AutoPostBack="true"
                    Text="Imposta tutti non originali" 
                    oncheckedchanged="CheckBoxSelezionaTuttiNonOriginali_CheckedChanged" />
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" AutoGenerateColumns="False" ID="GridViewDocumenti" 
        Width="100%" OnRowDataBound="GridViewDocumentiRichiesti_RowDataBound" 
        onselectedindexchanging="GridViewDocumenti_SelectedIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="#">
                <ItemTemplate>
                    <asp:Label
                        ID="LabelProgressivo"
                        runat="server">
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10px" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Stampa"  >
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxStampa" runat="server" Checked="false" />
                </ItemTemplate>
                <ItemStyle Width="20px" HorizontalAlign="Center"/>
            </asp:TemplateField >
            <asp:TemplateField HeaderText="Originale">
                <ItemTemplate>
                    <telerik:RadButton ID="RadButtonOriginale" runat="server" ToggleType="CustomToggle" ButtonType="ToggleButton">
                     <ToggleStates>
                      <telerik:RadButtonToggleState Text="??" Value="?" PrimaryIconCssClass="rbToggleCheckboxFilled" Selected="true" />
                      <telerik:RadButtonToggleState Text="No" Value="N" PrimaryIconCssClass="rbToggleCheckbox" />
                      <telerik:RadButtonToggleState Text="Sì" Value="S" PrimaryIconCssClass="rbToggleCheckboxChecked"
                               CssClass="rbSkinnedButtonChecked" />
                     </ToggleStates>
                    </telerik:RadButton>
                    <asp:CustomValidator
                        ID="CustomValidatorOriginale"
                        runat="server"
                        ValidationGroup="stampa"
                        ForeColor="Red" onservervalidate="CustomValidatorOriginale_ServerValidate">
                        *
                    </asp:CustomValidator>
                </ItemTemplate>
                <ItemStyle Width="40px" HorizontalAlign="Left"/>
            </asp:TemplateField >
            <asp:TemplateField HeaderText="Relativo a" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="LabelIdTipoDocumento" runat="server"></asp:Label>
                    <asp:Label ID="LabelIdFatturaDichiarata" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </asp:TemplateField>
            <asp:BoundField DataField="DescrizioneDocumento" HeaderText="Tipo documento">
            <ItemStyle Font-Bold="True" Width="200px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Relativo a">
                <ItemTemplate>
                    <asp:Label ID="LabelBeneficiario" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="150px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cons.">
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBoxConsegnato" runat="server" Checked="True" />
                </ItemTemplate>
                <ItemStyle Width="10px" />
            </asp:TemplateField>
            <%--<asp:CommandField ButtonType="Button" SelectText="Stampa" 
                ShowSelectButton="True">
            <ItemStyle Width="10px" />
            </asp:CommandField>--%>
        </Columns>
        <EmptyDataTemplate>
            Nessuno
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <br />
    <asp:Button
        ID="ButtonStampaSelezionatiBis"
        runat="server"
        Text="Stampa i selezionati"
        Width="200px" 
        ValidationGroup="stampa"
        CausesValidation="true" OnClientClick="return CheckSelezionati()"
        onclick="ButtonStampaSelezionati_Click" />
    <asp:ValidationSummary
        ID="ValidationSummaryStampa"
        runat="server"
        ValidationGroup="stampa"
        CssClass="messaggiErrore" />
</div>