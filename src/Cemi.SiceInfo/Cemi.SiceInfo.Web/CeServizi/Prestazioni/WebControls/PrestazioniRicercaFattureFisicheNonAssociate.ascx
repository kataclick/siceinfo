﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniRicercaFattureFisicheNonAssociate.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniRicercaFattureFisicheNonAssociate" %>

<asp:Panel ID="PanelRicercaDomande" runat="server" DefaultButton="ButtonFatturaCerca">
    <table class="standardTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                        <td>
                            Data (gg/mm/aaaa):
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBoxFatturaData"
                                ErrorMessage="*" Operator="DataTypeCheck" Type="Date" ValidationGroup="RicercaFatturaFisica"></asp:CompareValidator>
                        </td>
                        <td>
                            Numero:
                        </td>
                        <td>
                            Importo:
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBoxFatturaData" runat="server" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxFatturaNumero" runat="server" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxFatturaImporto" runat="server" Width="100%"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelPadding" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 24px">
                        </td>
                        <td style="height: 24px">
                        </td>
                        <td style="height: 24px">
                        </td>
                        <td style="height: 24px">
                            <asp:Button ID="ButtonFatturaCerca" runat="server" Text="Cerca" OnClick="ButtonFatturaCerca_Click"
                                ValidationGroup="RicercaFatturaFisica" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Elenco fatture non associate"></asp:Label><br />
                <asp:GridView ID="GridViewFattureRicevute" runat="server" AutoGenerateColumns="False"
                    Width="100%" DataKeyNames="IdPrestazioniFattura,IdArchidoc" OnSelectedIndexChanging="GridViewFattureRicevute_SelectedIndexChanging"
                    AllowPaging="True" OnPageIndexChanging="GridViewFattureRicevute_PageIndexChanging"
                    PageSize="5" OnRowCommand="GridViewFattureRicevute_RowCommand">
                    <Columns>
                        <asp:BoundField DataField="Data" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy}">
                            <ItemStyle Width="80px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Numero" HeaderText="Numero" />
                        <asp:BoundField DataField="ImportoTotale" HeaderText="Importo" DataFormatString="{0:0.00}" />
                        <%--<asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" EditText="Associa"
                            ShowEditButton="True">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:CommandField>--%>
                        <asp:ButtonField ButtonType="Button" CommandName="Associa" Text="Associa">
                            <ControlStyle CssClass="bottoneGriglia" />
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                        <asp:CommandField ButtonType="Button" ControlStyle-CssClass="bottoneGriglia" 
                            SelectText="Visualizza" ShowSelectButton="True">
                        <ControlStyle CssClass="bottoneGriglia" />
                        <ItemStyle Width="10px" />
                        </asp:CommandField>
                    </Columns>
                    <EmptyDataTemplate>
                        Nessuna fattura fisica trovata
                    </EmptyDataTemplate>
                </asp:GridView>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
