﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniDatiLavoratore.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniDatiLavoratore" %>

<table class="borderedTable">
    <tr>
        <td colspan="2">
            <b>Dati riassuntivi lavoratore</b>
        </td>
    </tr>
    <tr>
        <td style="width: 156px">
            Codice lavoratore:</td>
        <td>
            <asp:Label ID="LabelCodice" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 156px">
            Cognome:</td>
        <td>
            <asp:Label ID="LabelCognome" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="width: 156px">
            Nome:</td>
        <td>
            <asp:Label ID="LabelNome" runat="server"></asp:Label>
        </td>
    </tr>
</table>