﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrestazioniDatiFamiliare.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniDatiFamiliare" %>

<asp:Panel ID="PrestazioniDatiFamiliarePanel" runat="server">
    <table class="standardTable">
        <tr>
            <td colspan="3">
                Dichiarazione di familiare a carico a fini IRPEF
            </td>
        </tr>
        <tr>
            <td>
                Cognome:
            </td>
            <td>
                <asp:TextBox ID="TextBoxDatiFamiliareCognome" runat="server" MaxLength="30" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiFamiliareCognome" runat="server"
                    ErrorMessage="Cognome mancante" ControlToValidate="TextBoxDatiFamiliareCognome"
                    ValidationGroup="datiFamiliare">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Nome:
            </td>
            <td>
                <asp:TextBox ID="TextBoxDatiFamiliareNome" runat="server" MaxLength="30" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiFamiliareNome" runat="server"
                    ControlToValidate="TextBoxDatiFamiliareNome" ErrorMessage="Nome mancante" ValidationGroup="datiFamiliare">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Data di nascita (gg/mm/aaaa):
            </td>
            <td>
                <asp:TextBox ID="TextBoxDatiFamiliareDataNascita" runat="server" MaxLength="10" Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiFamiliareDataNascita" runat="server"
                    ControlToValidate="TextBoxDatiFamiliareDataNascita" ErrorMessage="Data di nascita mancante"
                    ValidationGroup="datiFamiliare">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidatorDatiFamiliareDataNascita" runat="server"
                    ControlToValidate="TextBoxDatiFamiliareDataNascita" ErrorMessage="Formato data di nascita non corretto"
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="datiFamiliare">*</asp:CompareValidator>
                    <asp:CustomValidator ID="CustomValidatorDataNascita" runat="server"
                    ErrorMessage="La data di nascita del familiare &#232; troppo antecedente la data della domanda."
                    ValidationGroup="datiFamiliare" onservervalidate="CustomValidatorDataNascita_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                Sesso:
            </td>
            <td colspan="2">
                <asp:RadioButton ID="RadioButtonSessoM" runat="server" Checked="true" GroupName="sesso"
                    Text="M" />
                <asp:RadioButton ID="RadioButtonSessoF" runat="server" GroupName="sesso" Text="F" />
            </td>
        </tr>
        <tr>
            <td>
                Codice fiscale:
            </td>
            <td>
                <asp:TextBox ID="TextBoxDatiFamiliareCodiceFiscale" runat="server" MaxLength="16"
                    Width="300px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDatiFamiliareCodiceFiscale"
                    runat="server" ControlToValidate="TextBoxDatiFamiliareCodiceFiscale" ErrorMessage="Codice fiscale mancante"
                    ValidationGroup="datiFamiliare" Enabled="False">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="CustomValidatorDatiFamiliareCodiceFiscale" runat="server"
                    ErrorMessage="Codice fiscale non valido" ControlToValidate="TextBoxDatiFamiliareCodiceFiscale"
                    OnServerValidate="CustomValidatorDatiFamiliareCodiceFiscale_ServerValidate" ValidationGroup="datiFamiliare">*</asp:CustomValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorDatiFamiliareCodiceFiscale"
                    runat="server" ControlToValidate="TextBoxDatiFamiliareCodiceFiscale" ErrorMessage="Formato del codice fiscale non valido"
                    ValidationExpression="^[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z\d]{5}$" ValidationGroup="datiFamiliare">*</asp:RegularExpressionValidator>
            </td>
        </tr>
    </table>
</asp:Panel>
