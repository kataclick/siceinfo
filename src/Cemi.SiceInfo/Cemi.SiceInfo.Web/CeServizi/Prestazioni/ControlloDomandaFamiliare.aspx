﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ControlloDomandaFamiliare.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ControlloDomandaFamiliare" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%@ Register Src="WebControls/PrestazioniDatiDomanda.ascx" TagName="PrestazioniDatiDomanda" TagPrefix="uc4" %>
<%@ Register Src="WebControls/PrestazioniRicercaFamiliare.ascx" TagName="PrestazioniRicercaFamiliare" TagPrefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda - Familiare"
        titolo="Prestazioni" />
    <br />
    <uc4:PrestazioniDatiDomanda ID="PrestazioniDatiDomanda1" runat="server" />
    <asp:Panel ID="PanelAzioni" runat="server">
        <table class="standardTable">
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                </td>
                <td style="width: 200px">
                </td>
                <td>
                    <b>Anagrafica</b>
                </td>
                <td>
                    <b>Comunicato</b>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Codice:
                </td>
                <td style="width: 200px">
                    <asp:Image ID="ImageControlloFamiliareSelezionato" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareCodice" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Cognome
                </td>
                <td style="width: 200px">
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareCognome" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelComunicatoFamiliareCognome" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Nome
                </td>
                <td style="width: 200px">
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareNome" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelComunicatoFamiliareNome" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Data di nascita
                </td>
                <td style="width: 200px">
                    <asp:Image ID="ImageControlloFamiliareDataNascita" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                    <asp:Button ID="ButtonForzaDataNascita" runat="server" Text="Forza" Enabled="false"
                        OnClick="ButtonForzaDataNascita_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareDataNascita" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelComunicatoFamiliareDataNascita" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Data decesso
                </td>
                <td style="width: 200px">
                    <asp:Image ID="ImageControlloDataDecesso" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                    <asp:Button ID="ButtonForzaDataDecesso" runat="server" Text="Forza" Enabled="false"
                        OnClick="ButtonForzaDataDecesso_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareDataDecesso" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Codice fiscale
                </td>
                <td style="width: 200px">
                    <asp:Image ID="ImageControlloFamiliareCodiceFiscale" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                    <asp:Button ID="ButtonForzaCodiceFiscale" runat="server" Text="Forza" Enabled="false"
                        OnClick="ButtonForzaCodiceFiscale_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareCodiceFiscale" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelComunicatoFamiliareCodiceFiscale" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Grado parentela
                </td>
                <td style="width: 200px">
                    <asp:Image ID="ImageControlloGradoParentela" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareGradoParentela" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelComunicatoFamiliareGradoParentela" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    A carico
                </td>
                <td style="width: 200px">
                    <asp:Image ID="ImageControlloACarico" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                    <asp:Button ID="ButtonForzaACarico" runat="server" Text="Forza" Enabled="false"
                        OnClick="ButtonForzaACarico_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelAnagraficaFamiliareACarico" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:BulletedList ID="BulletedListControlli" runat="server">
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    Se il familiare &quot;Anagrafica&quot; ha tutti i campi vuoti oppure non è quello
                    corretto, premere &quot;Seleziona familiare&quot; e poi sceglierlo dalla tabella
                    che comparirà premendo &quot;Seleziona&quot;.<br />
                    <asp:Button ID="ButtonSelezionaFamiliare" runat="server" OnClick="ButtonSelezionaFamiliare_Click"
                        Text="Seleziona familiare" Width="150px" />
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table class="standardTable">
        <tr>
            <td colspan="4">
                <uc3:PrestazioniRicercaFamiliare ID="PrestazioniRicercaFamiliare1" runat="server"
                    Visible="false" />
            </td>
        </tr>
    </table>
    <asp:Button ID="ButtonIndietro" runat="server" OnClick="ButtonIndietro_Click" Text="Indietro"
        Width="150px" />
    <br />
</asp:Content>
