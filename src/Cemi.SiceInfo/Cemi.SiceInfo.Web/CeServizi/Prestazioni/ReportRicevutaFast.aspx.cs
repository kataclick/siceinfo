﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ReportRicevutaFast : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda);
            funzionalita.Add(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDomanda"] != null)
                {
                    Int32 idDomanda = (Int32)Context.Items["IdDomanda"];
                    ViewState["IdDomanda"] = Context.Items["IdDomanda"];
                    string idStato = (string)Context.Items["IdStato"];

                    ReportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                    if (idStato != "R")
                        ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportRicevutaFast";
                    else if (idStato == "R")
                        ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportRicevutaFastRifiuto";

                    ReportParameter[] listaParam = new ReportParameter[3];
                    listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());
                    listaParam[1] = new ReportParameter("filtraDaConsegnare", "1");
                    listaParam[2] = new ReportParameter("operatore", "1");

                    ReportViewer.ServerReport.SetParameters(listaParam);

                    Warning[] warnings;
                    string[] streamids;
                    string mimeType;
                    string encoding;
                    string extension;
                    byte[] bytes = null;

                    //PDF
                    bytes = ReportViewer.ServerReport.Render(
                        "PDF", null, out mimeType, out encoding, out extension,
                        out streamids, out warnings);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";

                    Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=Ricevuta {0}.pdf", idDomanda));
                    Response.BinaryWrite(bytes);

                    Response.Flush();
                    Response.End();
                }
                else
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
            }
        }
    }
}