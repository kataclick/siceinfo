﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportRestituzioneFatture.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ReportRestituzioneFatture" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Prestazioni" sottoTitolo="Restituzione Fatture" />
    <br />
    <rsweb:reportviewer id="ReportViewer" runat="server" height="600px"
        processingmode="Remote" width="100%" ShowDocumentMapButton="False" ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:reportviewer>
</asp:Content>