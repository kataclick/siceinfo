﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="StampaCopertine.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.StampaCopertine" %>

<%@ Register src="../WebControls/MenuPrestazioni.ascx" tagname="MenuPrestazioni" tagprefix="uc1" %>
<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc2" %>
<%@ Register src="WebControls/StampaCopertine.ascx" tagname="StampaCopertine" tagprefix="uc3" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Prestazioni" sottoTitolo="Stampa Copertine" />
    <br />
    <uc3:StampaCopertine ID="StampaCopertine1" runat="server" />    
</asp:Content>