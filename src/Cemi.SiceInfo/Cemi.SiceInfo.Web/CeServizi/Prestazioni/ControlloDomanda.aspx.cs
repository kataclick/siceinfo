﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ControlloDomanda : System.Web.UI.Page
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        private int idUtente;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            #endregion

            idUtente = GestioneUtentiBiz.GetIdUtente();

            if (!Page.IsPostBack)
            {
                //Nel caso in cui il contesto non fosse presente, torniamo alla pagina di ricerca
                if (Context.Items["IdDomanda"] == null)
                {
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
                }

                int idDomanda = (int)Context.Items["IdDomanda"];
                ViewState["IdDomanda"] = idDomanda;
                Domanda domanda = CaricaDomanda(idDomanda);

                AbilitaGestione(domanda);

                CaricaCausali(domanda.IdTipoPrestazione);
                
            }
        }

        /// <summary>
        /// Abilita le azioni che può fare l'utente in funzione del ruolo e della proprietà della domanda 
        /// </summary>
        /// <param name="domanda">Domanda in gestione</param>
        private bool AbilitaGestione(Domanda domanda)
        {
            //IUtente utente = ApplicationInstance.GetUtenteSistema();
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            //Se l'utente è chi ha la domanda in carico o ha funzioni amministrative, può getire la domanda
            if (idUtente == domanda.IdUtenteInCarico || GestioneUtentiBiz.Autorizzato(FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin))
            {
                //Tutto normale
                return true;
            }
            else
            {
                //Tutto viene disabilitato
                PanelControllaDomandaAzioni.Enabled = false;
                return false;
            }
        }

        private Domanda CaricaDomanda(int idDomanda)
        {
            Domanda domanda = biz.GetDomanda(idDomanda);

            if (domanda.IdTipoPrestazione == "C004CP")
            {
                trProtesiCure.Visible = true;

                StatiCureProtesi stati = biz.GetStatiCureProtesi(domanda.IdDomanda.Value);
                if (stati != null)
                {
                    if (!String.IsNullOrEmpty(stati.StatoCL))
                    {
                        LabelStatoCL.Text = stati.StatoCL;
                    }
                    if (!String.IsNullOrEmpty(stati.StatoPL))
                    {
                        LabelStatoPL.Text = stati.StatoPL;
                    }
                }
            }

            // Al caricamento lancio i controlli sui dati anagrafici
            biz.ControllaDomandaLavoratore(domanda);
            biz.ControllaDomandaUnivocita(domanda);

            Configurazione configurazione = biz.GetConfigurazionePrestazione(
                domanda.IdTipoPrestazione,
                domanda.Beneficiario,
                domanda.Lavoratore.IdLavoratore.Value);

            ViewState["IdDomanda"] = domanda.IdDomanda;

            LabelDataConferma.Text = domanda.DataDomanda.Value.ToShortDateString();
            LabelDataRiferimento.Text = domanda.DataRiferimento.ToShortDateString();
            if (domanda.DataGestione.HasValue)
                LabelDataGestione.Text = domanda.DataGestione.Value.ToShortDateString();
            LabelTipoPrestazione.Text = domanda.TipoPrestazione.IdTipoPrestazione + " " +
                                        domanda.TipoPrestazione.Descrizione;

            LabelStato.Text = domanda.Stato.Descrizione;
            if (domanda.Stato.IdStato == "R" && domanda.TipoCausale != null)
            {
                LabelTipoCausale.Text = domanda.TipoCausale.Descrizione + "(" + domanda.TipoCausale.IdTipoCausale + ")";
                trTipoCausale.Visible = true;
            }

            LabelNota.Text = domanda.Nota;
            TextBoxNota.Text = domanda.Nota;
            PanelNotaEdit.Visible = false;

            if (domanda.Stato.IdStato != "L" && domanda.Stato.IdStato != "R" && domanda.Stato.IdStato != "A" && domanda.Stato.IdStato != "D")
            {
                ImageButtonEdit.Enabled = true;
                ImageButtonEdit.ImageUrl = "~/CeServizi/images/edit.png";
            }
            else
            {
                ImageButtonEdit.Enabled = false;
                ImageButtonEdit.ImageUrl = "~/CeServizi/images/editBN.png";
            }

            //la descrittiva al momento non viene presa dal DB
            string beneficiarioCompleto = string.Empty;

            switch (domanda.Beneficiario)
            {
                case "L":
                    beneficiarioCompleto = "Lavoratore";
                    break;
                case "F":
                    beneficiarioCompleto = "Figlio";
                    break;
                case "C":
                    beneficiarioCompleto = "Coniuge";
                    break;
                case "G":
                    beneficiarioCompleto = "Genitore non convivente";
                    break;
                case "A":
                    if (domanda.GenitoriConviventi)
                    {
                        beneficiarioCompleto = "Genitore convivente";
                    }
                    else
                    {
                        beneficiarioCompleto = "Coniuge";
                    }
                    break;
            }

            if (domanda.Beneficiario == "L")
            {
                LabelBeneficiario.Text = beneficiarioCompleto + " (" + domanda.Beneficiario + ")";
            }
            else
            {
                if (domanda.Familiare != null)
                    LabelBeneficiario.Text = String.Format("{0} ({1})",
                                                           beneficiarioCompleto + " (" + domanda.Beneficiario + ")",
                                                           domanda.Familiare.NomeCompleto);
                else
                    LabelBeneficiario.Text = String.Format("{0} ({1})",
                                                           beneficiarioCompleto + " (" + domanda.Beneficiario + ")",
                                                           domanda.FamiliareFornito.NomeCompleto);
            }

            //Se è stata inserita come fast permettiamo il recupero della ricevuta
            //TrRecuperaRicevuta.Visible = (domanda.TipoInserimento == TipoInserimento.Fast);
            TrRecuperaModulo.Visible = true;

            // Abilitazione aree bottoni controlli
            if (domanda.Beneficiario == "L")
                tdControlloFamiliare.Visible = false;

            //if (domanda.CasseEdili.Count == 0)
            //    tdControlloOreCNCE.Visible = false;
            if (!configurazione.RichiestaFattura)
            {
                trControlloFatture.Visible = false;
                trRestituzioneFatture.Visible = false;
            }

            // Per la prima fase di test abilitiamo la stampa delle
            // copertine solo per le C007 e C010
            //if (configurazione.IdTipoPrestazione == "C007-1"
            //    || configurazione.IdTipoPrestazione == "C007-2"
            //    || configurazione.IdTipoPrestazione == "C007-3"
            //    || configurazione.IdTipoPrestazione == "C010"
            //    || configurazione.IdTipoPrestazione == "C010-F")
            //{
            trCopertine.Visible = true;
            //}
            //else
            //{
            //    trCopertine.Visible = false;
            //}

            // per le prestazioni Scolastiche, funerarie o handicap mostriamo il tasto per la gestione
            if (configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione == 2 ||
                configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione == 3
                || configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione == 4)
                trControlloScolastiche.Visible = true;
            else
                trControlloScolastiche.Visible = false;

            LabelProtocollo.Text = String.Format("{0}/{1}", domanda.ProtocolloPrestazione,
                                                 domanda.NumeroProtocolloPrestazione);

            // Lavoratore
            LabelLavoratoreCodice.Text = domanda.Lavoratore.IdLavoratore.ToString();
            LabelLavoratoreCognome.Text = domanda.Lavoratore.Cognome;
            LabelLavoratoreNome.Text = domanda.Lavoratore.Nome;

            LabelNumeroFatture.Text = domanda.NumeroFatture.Value.ToString();
            LabelTipoInserimento.Text = domanda.TipoInserimento.ToString();

            ImageControlloIndirizzo.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloLavoratore);
            ImageControlloFamiliare.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloFamiliare);
            ImageControlloPresenzaDocumenti.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloPresenzaDocumenti);
            ImageControlloFatture.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloFatture);
            ImageControlloUnivocita.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloUnivocitaPrestazione);
            ImageControlloOreCNCE.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloOreCnce);
            ImageControlloScolastiche.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.ControlloScolastiche);

            ControllaStatoDomanda(domanda);
            AbilitaBottoniCambioStato(domanda);

            return domanda;
        }

        private void ControllaStatoDomanda(Domanda domanda)
        {
            if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
            {
                PanelControllaDomandaAzioni.Enabled = true;
            }
            else
            {
                PanelControllaDomandaAzioni.Enabled = false;
            }
        }

        private void AbilitaBottoniCambioStato(Domanda domanda)
        {
            // Bottone per accogliere la domanda
            if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
            {
                ButtonAccogli.Enabled = biz.IsDomandaAccoglibile(domanda);
            }
            else
            {
                ButtonAccogli.Enabled = false;
            }

            // Bottone per annullare la domanda
            if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
            {
                ButtonAnnulla.Enabled = true;
            }
            else
            {
                ButtonAnnulla.Enabled = false;
            }

            // Bottone per mettere la domanda in stato attesa documenti
            if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T")
            {
                ButtonAttesa.Enabled = true;
                ButtonAttesaDenuncia.Enabled = true;
                //ButtonEsame.Enabled = true;
            }
            else
            {
                ButtonAttesa.Enabled = false;
                ButtonAttesaDenuncia.Enabled = false;
                //ButtonEsame.Enabled = false;
            }

            // Bottone per respingere la domanda (modifica: non occorre che sia in stato "Attesa documenti")
            if (domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O" || domanda.Stato.IdStato == "N" || domanda.Stato.IdStato == "E")
            {
                ButtonRespingi.Enabled = true;
            }
            else
            {
                ButtonRespingi.Enabled = false;
            }
        }

        /// <summary>
        /// Metodo per recuperare il modulo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButtonRecuperaModulo_Click(object sender, ImageClickEventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Domanda d = biz.GetDomanda((int)ViewState["IdDomanda"]);

            Context.Items["TipoModulo"] = d.TipoModulo.Modulo;

            Configurazione conf = biz.GetConfigurazionePrestazione(
                d.TipoPrestazione.IdTipoPrestazione,
                d.Beneficiario,
                d.Lavoratore.IdLavoratore.Value);

            //switch (d.TipoModulo.IdTipoModulo)
            switch (conf.TipoMacroPrestazione.IdTipoMacroPrestazione)
            {
                // Prestazioni Sanitarie
                case 1:
                    Server.Transfer("~/CeServizi/Prestazioni/ReportPrestazioniSanitarie.aspx");
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    Server.Transfer("~/CeServizi/Prestazioni/ReportPrestazioniScolastiche.aspx");
                    break;
            }
        }

        /// <summary>
        /// Evento per recuperare la ricevuta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButtonRecuperaRicevuta_Click(object sender, ImageClickEventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Domanda d = biz.GetDomanda((int)ViewState["IdDomanda"]);

            Context.Items["IdDomanda"] = d.IdDomanda.Value;
            Context.Items["IdStato"] = d.Stato.IdStato;
            Server.Transfer("~/CeServizi/Prestazioni/ReportRicevutaFast.aspx");
        }

        /// <summary>
        /// Carichiamo le causali
        /// </summary>
        private void CaricaCausali(string idTipoPrestazione)
        {
            TipoCausaleCollection tipiCausale = biz.GetTipiCausali(false, idTipoPrestazione);

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCausaleRifiuto,
                tipiCausale,
                "Descrizione",
                "IdTipoCausale");
        }

        /// <summary>
        /// Permettiamo la gestione della ricevuta a posteriori
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonGestioneRicevuta_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/GestioneRicevuta.aspx");
        }

        #region Eventi bottoni per i vari controlli

        protected void ButtonControlloIndirizzo_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaDatiAnagrafici.aspx");
        }

        protected void ButtonControlloFamiliare_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaFamiliare.aspx");
        }

        protected void ButtonControlloFatture_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaFatture.aspx");
        }

        protected void ButtonControlloOreCNCE_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaOreCNCE.aspx");
        }

        protected void ButtonControlloPresenzaDocumenti_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaDocumenti.aspx");
        }

        protected void ButtonControlloUnivocitaPrestazione_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaUnivocita.aspx");
        }

        protected void ButtonControlloScolastiche_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomandaScolastiche.aspx");
        }

        #endregion

        #region Eventi bottoni per le azioni

        protected void ButtonAccogli_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];

            Domanda domanda = biz.GetDomanda(idDomanda);
            //in questo primo if facciamo dei controlli di logica che non avrebbe elaborare tramite validator (controllare la data di nascita del familiare)
            if ((domanda.IdTipoPrestazione == "C-ANID"
                 &&
                 (domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati >= 6 &&
                  (DateTime.Now.Subtract(domanda.Familiare.DataNascita.Value)).TotalDays <= 8 * 365))
                || domanda.IdTipoPrestazione != "C-ANID")
            {
                if (biz.UpdateDomandaAccogli(idDomanda, idUtente, domanda.IdTipoPrestazione))
                {
                    LabelMessaggio.Text = "Operazione effettuata";
                    CaricaDomanda(idDomanda);

                    if (!Presenter.IsSviluppo)
                    {
                        biz.CallCrmWsPrestazioni(idDomanda);
                    }
                }
                else
                {
                    LabelMessaggio.Text =
                        "Operazione NON effettuata. Il contesto di validazione è cambiato (controllare l'univocità).";
                }
            }
            else
            {
                LabelMessaggio.Text =
                    "La domanda NON può essere accolta per un problema sui mesi frequenza o sull'età del familiare.";
            }
        }

        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];

            if (biz.UpdateDomandaAnnulla(idDomanda, idUtente))
            {
                LabelMessaggio.Text = "Operazione effettuata";
                CaricaDomanda(idDomanda);

                //CrmWsCall(idDomanda); l'annullamento sono domande NON VALIDE E NON DEVONO ESSERE GESTITE IN ALTRI SISTEMI
                if (!Presenter.IsSviluppo)
                {
                    biz.CallCrmWsPrestazioni(idDomanda);
                }
            }
            else
            {
                LabelMessaggio.Text = "Operazione NON effettuata";
            }
        }
        
        protected void ButtonAttesa_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];

            if (biz.UpdateDomandaAttesa(idDomanda))
            {
                LabelMessaggio.Text = "Operazione effettuata";
                CaricaDomanda(idDomanda);

                //gli errori non vengono gestiti lato utente
                if (!Presenter.IsSviluppo)
                {
                    biz.CallCrmWsPrestazioni(idDomanda);
                }
            }
            else
            {
                LabelMessaggio.Text = "Operazione NON effettuata";
            }
        }

        protected void ButtonAttesaDenuncia_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];

            if (biz.UpdateDomandaAttesaDenuncia(idDomanda))
            {
                LabelMessaggio.Text = "Operazione effettuata";
                CaricaDomanda(idDomanda);

                //gli errori non vengono gestiti lato utente
                if (!Presenter.IsSviluppo)
                {
                    biz.CallCrmWsPrestazioni(idDomanda);
                }
            }
            else
            {
                LabelMessaggio.Text = "Operazione NON effettuata";
            }
        }

        protected void ButtonEsame_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];

            if (biz.UpdateDomandaEsame(idDomanda))
            {
                LabelMessaggio.Text = "Operazione effettuata";
                CaricaDomanda(idDomanda);

                //gli errori non vengono gestiti lato utente
                if (!Presenter.IsSviluppo)
                {
                    biz.CallCrmWsPrestazioni(idDomanda);
                }
            }
            else
            {
                LabelMessaggio.Text = "Operazione NON effettuata";
            }
        }

        protected void ButtonRespingi_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int idDomanda = (int)ViewState["IdDomanda"];

                if (biz.UpdateDomandaRespingi(idDomanda, DropDownListCausaleRifiuto.SelectedValue))
                {
                    LabelMessaggio.Text = "Operazione effettuata";
                    CaricaDomanda(idDomanda);

                    //gli errori non vengono gestiti lato utente
                    if (!Presenter.IsSviluppo)
                    {
                        biz.CallCrmWsPrestazioni(idDomanda);
                    }
                }
                else
                {
                    LabelMessaggio.Text = "Operazione NON effettuata";
                }
            }
        }

        #endregion

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Prestazioni/GestioneDomande.aspx");
        }

        protected void ImageButtonRestituzioneFatture_Click(object sender, ImageClickEventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];
            Context.Items["IdDomanda"] = idDomanda;

            Server.Transfer("~/CeServizi/Prestazioni/ReportRestituzioneFatture.aspx");
        }

        protected void ImageButtonCopertine_Click(object sender, ImageClickEventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];
            Context.Items["IdDomanda"] = idDomanda;

            Server.Transfer("~/CeServizi/Prestazioni/StampaCopertine.aspx");
        }

        protected void ImageButtonEdit_Click(object sender, ImageClickEventArgs e)
        {
            PanelNotaEdit.Visible = true;
        }

        protected void ButtonNotaAnnulla_Click(object sender, EventArgs e)
        {
            PanelNotaEdit.Visible = false;
        }

        protected void ButtonNotaSalva_Click(object sender, EventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            String nota = Presenter.NormalizzaCampoTesto(TextBoxNota.Text);

            biz.UpdateDomandaNota(idDomanda, idUtente, nota);
            CaricaDomanda(idDomanda);
        }
    }
}