﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni {
    
    
    public partial class ControlloDomandaFamiliare {
        
        /// <summary>
        /// MenuPrestazioni1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.MenuPrestazioni MenuPrestazioni1;
        
        /// <summary>
        /// TitoloSottotitolo1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.TitoloSottotitolo TitoloSottotitolo1;
        
        /// <summary>
        /// PrestazioniDatiDomanda1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniDatiDomanda PrestazioniDatiDomanda1;
        
        /// <summary>
        /// PanelAzioni control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PanelAzioni;
        
        /// <summary>
        /// ImageControlloFamiliareSelezionato control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageControlloFamiliareSelezionato;
        
        /// <summary>
        /// LabelAnagraficaFamiliareCodice control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareCodice;
        
        /// <summary>
        /// LabelAnagraficaFamiliareCognome control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareCognome;
        
        /// <summary>
        /// LabelComunicatoFamiliareCognome control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelComunicatoFamiliareCognome;
        
        /// <summary>
        /// LabelAnagraficaFamiliareNome control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareNome;
        
        /// <summary>
        /// LabelComunicatoFamiliareNome control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelComunicatoFamiliareNome;
        
        /// <summary>
        /// ImageControlloFamiliareDataNascita control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageControlloFamiliareDataNascita;
        
        /// <summary>
        /// ButtonForzaDataNascita control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonForzaDataNascita;
        
        /// <summary>
        /// LabelAnagraficaFamiliareDataNascita control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareDataNascita;
        
        /// <summary>
        /// LabelComunicatoFamiliareDataNascita control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelComunicatoFamiliareDataNascita;
        
        /// <summary>
        /// ImageControlloDataDecesso control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageControlloDataDecesso;
        
        /// <summary>
        /// ButtonForzaDataDecesso control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonForzaDataDecesso;
        
        /// <summary>
        /// LabelAnagraficaFamiliareDataDecesso control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareDataDecesso;
        
        /// <summary>
        /// ImageControlloFamiliareCodiceFiscale control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageControlloFamiliareCodiceFiscale;
        
        /// <summary>
        /// ButtonForzaCodiceFiscale control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonForzaCodiceFiscale;
        
        /// <summary>
        /// LabelAnagraficaFamiliareCodiceFiscale control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareCodiceFiscale;
        
        /// <summary>
        /// LabelComunicatoFamiliareCodiceFiscale control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelComunicatoFamiliareCodiceFiscale;
        
        /// <summary>
        /// ImageControlloGradoParentela control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageControlloGradoParentela;
        
        /// <summary>
        /// LabelAnagraficaFamiliareGradoParentela control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareGradoParentela;
        
        /// <summary>
        /// LabelComunicatoFamiliareGradoParentela control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelComunicatoFamiliareGradoParentela;
        
        /// <summary>
        /// ImageControlloACarico control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageControlloACarico;
        
        /// <summary>
        /// ButtonForzaACarico control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonForzaACarico;
        
        /// <summary>
        /// LabelAnagraficaFamiliareACarico control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelAnagraficaFamiliareACarico;
        
        /// <summary>
        /// BulletedListControlli control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.BulletedList BulletedListControlli;
        
        /// <summary>
        /// ButtonSelezionaFamiliare control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonSelezionaFamiliare;
        
        /// <summary>
        /// PrestazioniRicercaFamiliare1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.Prestazioni.WebControls.PrestazioniRicercaFamiliare PrestazioniRicercaFamiliare1;
        
        /// <summary>
        /// ButtonIndietro control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonIndietro;
    }
}
