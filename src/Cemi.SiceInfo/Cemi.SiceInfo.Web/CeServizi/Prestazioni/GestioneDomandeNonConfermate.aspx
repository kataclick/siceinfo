﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="GestioneDomandeNonConfermate.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.GestioneDomandeNonConfermate" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc2" %>
<%@ Register Src="WebControls/PrestazioniRicercaDomandeNonConfermate.ascx" TagName="PrestazioniRicercaDomandeNonConfermate" TagPrefix="uc3" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Domande non confermate"
        titolo="Prestazioni" />
    <br />
    <uc3:PrestazioniRicercaDomandeNonConfermate ID="PrestazioniRicercaDomandeNonConfermate1" runat="server" />
</asp:Content>