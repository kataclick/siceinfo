﻿using System;
using System.Web.UI;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Delegates.Prestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ControlloDomandaScolastiche : System.Web.UI.Page
    {
        private PrestazioniBusiness biz = new PrestazioniBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            #endregion

            if (!Page.IsPostBack)
            {
                //Nel caso in cui il contesto non fosse presente, torniamo alla pagina di ricerca
                if (Context.Items["IdDomanda"] == null)
                {
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
                }

                Int32 idDomanda = (Int32)Context.Items["IdDomanda"];
                ViewState["IdDomanda"] = idDomanda;
                Domanda domanda = biz.GetDomanda(idDomanda);
                ViewState["IdTipoPrestazione"] = domanda.IdTipoPrestazione;
                ViewState["IdLavoratore"] = domanda.Lavoratore.IdLavoratore;

                CaricaTipiScuola(domanda.TipoPrestazione.IdTipoPrestazione);
                CaricaTipiDecesso();
                CaricaDomanda(domanda);

                ControllaStatoDomanda(domanda);
            }

            PrestazioniRicercaFamiliareErede.OnFamiliareSelected +=
                new FamiliareSelectedEventHandler(PrestazioniRicercaFamiliareErede_OnFamiliareSelected);
        }


        /// <summary>
        /// in funzione dello stato della domanda decidiamo quali azioni lasciare all'utente
        /// </summary>
        /// <param name="domanda"></param>
        private void ControllaStatoDomanda(Domanda domanda)
        {
            if (StatoDomanda.StatoDomandaModificabile(domanda.Stato.IdStato))
            {
                PanelAzioni.Enabled = true;
            }
            else
            {
                PanelAzioni.Enabled = false;
                ButtonSalvataggio.Enabled = false;
            }
        }

        private void CaricaTipiScuola(String idTipoPrestazione)
        {
            TipoScuolaCollection tipiScuola = biz.GetTipiScuola(idTipoPrestazione);
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipoScuolaC0031, tipiScuola, "Descrizione", "IdTipoScuola");
        }

        private void CaricaTipiPromozione(Int32 idTipoScuola)
        {
            TipoPromozioneCollection tipiPromozione = biz.GetTipiPromozione(idTipoScuola);
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipoPromozioneC0031, tipiPromozione,
                                                               "Descrizione", "IdTipoPromozione");
        }

        private void CaricaDomanda(Domanda domanda)
        {
            LabelTipoPrestazione.Text = domanda.TipoPrestazione.ToString();
            LabelBeneficiario.Text = domanda.Beneficiario;

            if (domanda.DatiAggiuntiviScolastiche != null)
            {
                ViewState["IdDatiAggiuntivi"] = domanda.DatiAggiuntiviScolastiche.IdDatiScolastiche;
            }

            // Visualizzo il pannello per la prestazione corrente
            switch (domanda.TipoPrestazione.IdTipoPrestazione)
            {
                case "C003-1":
                    PanelC0031.Visible = true;
                    PanelC003123.Visible = true;

                    if (domanda.DatiAggiuntiviScolastiche != null)
                    {
                        if (domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.HasValue)
                        {
                            TextBoxAnnoFrequenzaC0031.Text = domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.ToString();
                        }

                        if (domanda.DatiAggiuntiviScolastiche.TipoScuola != null)
                        {
                            DropDownListTipoScuolaC0031.SelectedValue =
                                domanda.DatiAggiuntiviScolastiche.TipoScuola.IdTipoScuola.ToString();
                            CaricaTipiPromozione(domanda.DatiAggiuntiviScolastiche.TipoScuola.IdTipoScuola);
                        }

                        if (domanda.DatiAggiuntiviScolastiche.TipoPromozione != null)
                        {
                            DropDownListTipoPromozioneC0031.SelectedValue =
                                domanda.DatiAggiuntiviScolastiche.TipoPromozione.IdTipoPromozione.ToString();
                        }

                        if (domanda.DatiAggiuntiviScolastiche.ClasseConclusa != null)
                        {
                            TextBoxClasseConclusaC0031.Text = domanda.DatiAggiuntiviScolastiche.ClasseConclusa.ToString();
                        }

                        if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione))
                        {
                            DropDownListApplicaDeduzioneC0031.SelectedValue =
                                domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione;
                        }

                        if (domanda.DatiAggiuntiviScolastiche.RedditoSi.HasValue)
                        {
                            CheckBoxC003123RedditoSi.Checked = domanda.DatiAggiuntiviScolastiche.RedditoSi.Value;
                            CheckBoxC003123RedditoNo.Checked = !domanda.DatiAggiuntiviScolastiche.RedditoSi.Value;
                        }
                    }
                    break;
                case "C006":
                    PanelC006.Visible = true;

                    if (domanda.DatiAggiuntiviScolastiche != null)
                    {
                        LabelAnnoScolasticoC006.Text = domanda.DatiAggiuntiviScolastiche.AnnoScolastico;
                        LabelIstitutoDenominazioneC006.Text = domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione;
                        LabelIstitutoIndirizzoC006.Text = domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo;
                        if (!String.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.IstitutoLocalita))
                        {
                            LabelIstitutoLocalitaC006.Text = String.Format("{0} ({1})", domanda.DatiAggiuntiviScolastiche.IstitutoLocalita, domanda.DatiAggiuntiviScolastiche.IstitutoProvincia);
                        }

                        if (domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.HasValue)
                        {
                            TextBoxAnnoFrequenzaC006.Text = domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.ToString();
                        }
                    }
                    break;
                case "C003-2":
                case "C003-3":
                    PanelC00323.Visible = true;
                    PanelC003123.Visible = true;

                    if (domanda.DatiAggiuntiviScolastiche != null)
                    {
                        if (domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.HasValue)
                            TextBoxAnnoFrequenzaC00323.Text = domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.AnnoImmatricolazione.HasValue)
                            TextBoxAnnoImmatricolazioneC00323.Text =
                                domanda.DatiAggiuntiviScolastiche.AnnoImmatricolazione.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.NumeroAnniFrequentati.HasValue)
                            TextBoxAnniFrequentatiC00323.Text =
                                domanda.DatiAggiuntiviScolastiche.NumeroAnniFrequentati.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.NumeroAnniFuoriCorso.HasValue)
                            TextBoxAnniFuoriCorsoC00323.Text =
                                domanda.DatiAggiuntiviScolastiche.NumeroAnniFuoriCorso.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.MediaVoti.HasValue)
                            TextBoxMediaVotoC00323.Text = domanda.DatiAggiuntiviScolastiche.MediaVoti.Value.ToString("0,00");

                        if (domanda.DatiAggiuntiviScolastiche.NumeroEsamiSostenuti.HasValue)
                            TextBoxEsamiSostenutiC00323.Text =
                                domanda.DatiAggiuntiviScolastiche.NumeroEsamiSostenuti.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.CfuPrevisti.HasValue)
                            TextBoxCFUPrevistiC00323.Text = domanda.DatiAggiuntiviScolastiche.CfuPrevisti.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.CfuConseguiti.HasValue)
                            TextBoxCFUConseguitiC00323.Text = domanda.DatiAggiuntiviScolastiche.CfuConseguiti.ToString();

                        if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione))
                            DropDownListApplicaDeduzioneC00323.SelectedValue =
                                domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione;

                        if (domanda.DatiAggiuntiviScolastiche.RedditoSi.HasValue)
                        {
                            CheckBoxC003123RedditoSi.Checked = domanda.DatiAggiuntiviScolastiche.RedditoSi.Value;
                            CheckBoxC003123RedditoNo.Checked = !domanda.DatiAggiuntiviScolastiche.RedditoSi.Value;
                        }
                    }
                    break;
                case "C-ANID":
                    PanelCANID.Visible = true;

                    if (domanda.DatiAggiuntiviScolastiche != null)
                    {
                        if (domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.HasValue)
                            TextBoxAnnoFrequenzaCANID.Text = domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati.HasValue)
                            TextBoxNumeroMesiFrequentatiCANID.Text =
                                domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.PeriodoDal.HasValue)
                        {
                            TextBoxPeriodoDal.Text = domanda.DatiAggiuntiviScolastiche.PeriodoDal.Value.ToString("MM/yyyy");
                        }

                        if (domanda.DatiAggiuntiviScolastiche.PeriodoAl.HasValue)
                        {
                            TextBoxPeriodoAl.Text = domanda.DatiAggiuntiviScolastiche.PeriodoAl.Value.ToString("MM/yyyy");
                        }

                        CaricaSemafori(domanda);
                    }
                    break;
                case "C002":
                case "C002-L":
                    PanelC002.Visible = true;

                    if (domanda.TipoPrestazione.IdTipoPrestazione == "C002-L")
                    {
                        //impostiamo il valore di default di applica deduzione
                        DropDownListApplicaDeduzioneC002.SelectedValue = "S";
                        trFamiliareErede.Visible = true;
                    }
                    else trFamiliareErede.Visible = false;

                    if (domanda.DatiAggiuntiviScolastiche != null)
                    {
                        //if (domanda.DatiAggiuntiviScolastiche.NumeroFigliCarico.HasValue)
                        //    TextBoxNumeroFigli.Text = domanda.DatiAggiuntiviScolastiche.NumeroFigliCarico.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.IdFamiliareErede.HasValue)
                        {
                            //todo dovrei pescare il familiare dall'nanagrafica per visualizzarne il nome...
                            Familiare familiare = biz.GetFamiliare(domanda.Lavoratore.IdLavoratore.Value,
                                                                   domanda.DatiAggiuntiviScolastiche.IdFamiliareErede.Value);
                            LabelFamiliareErede.Text = familiare.NomeCompleto;
                        }
                        else
                            LabelFamiliareErede.Text = "Familiare non ancora selezionato";

                        if (domanda.DatiAggiuntiviScolastiche.DataDecesso.HasValue)
                            TextBoxDataDecesso.Text =
                                domanda.DatiAggiuntiviScolastiche.DataDecesso.Value.ToShortDateString();

                        DropDownListTipoDecesso.SelectedValue = domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso;

                        if (!string.IsNullOrEmpty(domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione))
                            DropDownListApplicaDeduzioneC002.SelectedValue =
                                domanda.DatiAggiuntiviScolastiche.ApplicaDeduzione;
                    }
                    break;

                case "C014":
                case "C014-C":
                case "C014-F":
                    PanelC014.Visible = true;


                    if (domanda.DatiAggiuntiviScolastiche != null)
                    {
                        if (domanda.DatiAggiuntiviScolastiche.AnnoDaErogare.HasValue)
                            TextBoxAnnoDaErogare.Text = domanda.DatiAggiuntiviScolastiche.AnnoDaErogare.ToString();

                        if (domanda.DatiAggiuntiviScolastiche.TipoDaErogare != null)
                        {
                            if (domanda.DatiAggiuntiviScolastiche.TipoDaErogare.HasValue)
                                DropDownListTipoErogazione.SelectedValue =
                                    domanda.DatiAggiuntiviScolastiche.TipoDaErogare.Value.ToString();
                        }
                    }
                    break;
            }
        }

        private void CaricaSemafori(Domanda domanda)
        {
            BulletedListControlli.Items.Clear();

            if (domanda.DatiAggiuntiviScolastiche != null)
            {
                if (domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.HasValue)
                {
                    Boolean annoFrequenzaMaggioreData = domanda.DatiAggiuntiviScolastiche.AnnoFrequenza.Value == domanda.DataRiferimento.Year;

                    ImageControlloAnnoFrequenza.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.DatiAggiuntiviScolastiche.ForzaturaAnnoFrequenza || annoFrequenzaMaggioreData);

                    if (!annoFrequenzaMaggioreData && !domanda.DatiAggiuntiviScolastiche.ForzaturaAnnoFrequenza)
                    {
                        ButtonForzaAnnoFrequenza.Enabled = true;
                    }

                    if (!annoFrequenzaMaggioreData)
                    {
                        BulletedListControlli.Items.Add("L'anno di frequenza è diverso dall'anno della data di riferimento della domanda");
                    }
                }

                if (domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati.HasValue)
                {
                    Boolean mesiMaggiore6 = domanda.DatiAggiuntiviScolastiche.NumeroMesiFrequentati >= 6;

                    ImageControlloMesiFrequentati.ImageUrl = biz.ConvertiBoolInSemaforo(mesiMaggiore6);

                    if (!mesiMaggiore6)
                    {
                        BulletedListControlli.Items.Add("I mesi frequentati sono minori di 6");
                    }
                }

                if (domanda.DatiAggiuntiviScolastiche.PeriodoAl.HasValue)
                {
                    Int32 differenzaGiorniDomanda = (domanda.DataDomanda.Value - domanda.DatiAggiuntiviScolastiche.PeriodoAl.Value.AddMonths(1).AddDays(-1)).Days;
                    Boolean differenzaDomanda = differenzaGiorniDomanda <= 180;
                    ImageControlloPeriodoFrequenza180Giorni.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza180Giorni || differenzaDomanda);

                    if (!differenzaDomanda && !domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza180Giorni)
                    {
                        ButtonForzaPeriodoFrequenza180Giorni.Enabled = true;
                    }

                    if (!differenzaDomanda)
                    {
                        BulletedListControlli.Items.Add("La domanda è stata presentata dopo 180 giorni dall'ultimo mese di frequenza");
                    }

                    if (domanda.Familiare != null)
                    {
                        Boolean periodoFrequenza = domanda.DatiAggiuntiviScolastiche.PeriodoAl.Value <= domanda.Familiare.DataNascita.Value.AddYears(3);
                        ImageControlloPeriodoFrequenza3Anni.ImageUrl = biz.ConvertiBoolInSemaforo(domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza3Anni || periodoFrequenza);

                        if (!periodoFrequenza && !domanda.DatiAggiuntiviScolastiche.ForzaturaPeriodoFrequenza3Anni)
                        {
                            ButtonForzaPeriodoFrequenza3Anni.Enabled = true;
                        }

                        if (!periodoFrequenza)
                        {
                            BulletedListControlli.Items.Add("Il periodo di frequenza ricade oltre il compimento del 3° anno di età del beneficiario");
                        }
                    }
                    else
                    {
                        BulletedListControlli.Items.Add("Non è stato selezionato il familiare");
                    }
                }
            }
            else
            {
                BulletedListControlli.Items.Add("Non è possibile effettuare i controlli, inserire i dati aggiuntivi");
            }
        }

        protected void DropDownListTipoScuolaC0031_SelectedIndexChanged(Object sender, EventArgs e)
        {
            CaricaTipiPromozione(Int32.Parse(DropDownListTipoScuolaC0031.SelectedValue));
        }

        protected void ButtonSalvataggio_Click(Object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Int32 idDomanda = (Int32)ViewState["IdDomanda"];
                String idTipoPrestazione = (String)ViewState["IdTipoPrestazione"];

                SalvaDati(idDomanda, idTipoPrestazione);
            }
        }

        private void SalvaDati(Int32 idDomanda, String idTipoPrestazione)
        {
            DatiScolastiche datiScolastiche = new DatiScolastiche();
            datiScolastiche.IdDatiScolastiche = (Int32?)ViewState["IdDatiAggiuntivi"];
            datiScolastiche.IdTipoPrestazione = (string)ViewState["IdTipoPrestazione"];

            switch (idTipoPrestazione)
            {
                case "C003-1":
                    datiScolastiche.AnnoFrequenza = Int16.Parse(TextBoxAnnoFrequenzaC0031.Text);
                    datiScolastiche.TipoScuola = new TipoScuola();
                    datiScolastiche.TipoScuola.IdTipoScuola = Int32.Parse(DropDownListTipoScuolaC0031.SelectedValue);
                    datiScolastiche.TipoPromozione = new TipoPromozione();
                    datiScolastiche.TipoPromozione.IdTipoPromozione =
                        Int32.Parse(DropDownListTipoPromozioneC0031.SelectedValue);
                    datiScolastiche.ClasseConclusa = Int16.Parse(TextBoxClasseConclusaC0031.Text);
                    datiScolastiche.ApplicaDeduzione = DropDownListApplicaDeduzioneC0031.SelectedValue;
                    break;
                case "C006":
                    datiScolastiche.AnnoFrequenza = Int16.Parse(TextBoxAnnoFrequenzaC006.Text);
                    break;
                case "C003-2":
                case "C003-3":
                    datiScolastiche.AnnoFrequenza = Int16.Parse(TextBoxAnnoFrequenzaC00323.Text);
                    datiScolastiche.AnnoImmatricolazione = Int16.Parse(TextBoxAnnoImmatricolazioneC00323.Text);
                    datiScolastiche.NumeroAnniFrequentati = Int16.Parse(TextBoxAnniFrequentatiC00323.Text);
                    datiScolastiche.NumeroAnniFuoriCorso = Int16.Parse(TextBoxAnniFuoriCorsoC00323.Text);
                    datiScolastiche.MediaVoti = Decimal.Parse(TextBoxMediaVotoC00323.Text);
                    datiScolastiche.NumeroEsamiSostenuti = Int16.Parse(TextBoxEsamiSostenutiC00323.Text);
                    datiScolastiche.CfuPrevisti = Int16.Parse(TextBoxCFUPrevistiC00323.Text);
                    datiScolastiche.CfuConseguiti = Int16.Parse(TextBoxCFUConseguitiC00323.Text);
                    datiScolastiche.ApplicaDeduzione = DropDownListApplicaDeduzioneC00323.SelectedValue;
                    break;
                case "C-ANID":
                    datiScolastiche.AnnoFrequenza = Int16.Parse(TextBoxAnnoFrequenzaCANID.Text);
                    datiScolastiche.NumeroMesiFrequentati = Int16.Parse(TextBoxNumeroMesiFrequentatiCANID.Text);
                    datiScolastiche.PeriodoDal = DateTime.ParseExact(TextBoxPeriodoDal.Text, "MM/yyyy", null);
                    datiScolastiche.PeriodoAl = DateTime.ParseExact(TextBoxPeriodoAl.Text, "MM/yyyy", null);
                    break;
                case "C002":
                case "C002-L":
                    datiScolastiche.IdFamiliareErede = (Int32?)ViewState["IdFamiliareErede"];
                    datiScolastiche.DataDecesso = DateTime.Parse(TextBoxDataDecesso.Text);
                    datiScolastiche.ApplicaDeduzione = DropDownListApplicaDeduzioneC002.SelectedValue;
                    datiScolastiche.IdTipoPrestazioneDecesso = DropDownListTipoDecesso.SelectedValue;
                    break;
                case "C014":
                case "C014-C":
                case "C014-F":
                    datiScolastiche.TipoDaErogare = Int16.Parse(DropDownListTipoErogazione.SelectedValue);
                    datiScolastiche.AnnoDaErogare = int.Parse(TextBoxAnnoDaErogare.Text);
                    break;
            }

            if (!biz.InsertUpdateDatiAggiuntiviScolastiche(idDomanda, datiScolastiche))
            {
                LabelErrore.Visible = true;
                LabelOk.Visible = false;
            }
            else
            {
                LabelErrore.Visible = false;
                LabelOk.Visible = true;
                Domanda domanda = biz.GetDomanda(idDomanda);
                CaricaDomanda(domanda);
            }
        }

        protected void ButtonIndietro_Click(Object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomanda.aspx");
        }

        private void CaricaTipiDecesso()
        {
            TipoDecessoCollection tipiDecesso = biz.GetTipiDecesso();
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipoDecesso, tipiDecesso, "Descrizione",
                                                               "IdTipoDecesso");
        }

        protected void ButtonSelezionaFamiliareErede_Click(object sender, EventArgs e)
        {
            PanelFamiliareErede.Visible = true;
            PrestazioniRicercaFamiliareErede.CaricaFamiliari(
                (int)ViewState["IdLavoratore"]);
        }

        private void PrestazioniRicercaFamiliareErede_OnFamiliareSelected(Familiare familiare)
        {
            PanelFamiliareErede.Visible = false;

            LabelFamiliareErede.Text = familiare.NomeCompleto;
            ViewState["IdFamiliareErede"] = familiare.IdFamiliare;
        }

        protected void CustomValidatorPeriodo_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            String dataDa = Presenter.NormalizzaCampoTesto(TextBoxPeriodoDal.Text);
            String dataA = Presenter.NormalizzaCampoTesto(TextBoxPeriodoAl.Text);

            DateTime dal;
            DateTime al;

            if (DateTime.TryParseExact(dataDa, "MM/yyyy", null, System.Globalization.DateTimeStyles.None, out dal)
                && DateTime.TryParseExact(dataA, "MM/yyyy", null, System.Globalization.DateTimeStyles.None, out al))
            {
                if (dal > al)
                {
                    args.IsValid = false;
                }
            }
        }

        protected void ButtonForzaPeriodoFrequenza180Giorni_Click(object sender, EventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];

            if (biz.ForzaControlloPeriodoFrequenza180Giorni(idDomanda))
            {
                Domanda domanda = biz.GetDomanda(idDomanda);
                biz.ControllaDomandaScolastiche(domanda);

                CaricaDomanda(domanda);
            }
        }

        protected void ButtonForzaPeriodoFrequenza3Anni_Click(object sender, EventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];

            if (biz.ForzaControlloPeriodoFrequenza3Anni(idDomanda))
            {
                Domanda domanda = biz.GetDomanda(idDomanda);
                biz.ControllaDomandaScolastiche(domanda);

                CaricaDomanda(domanda);
            }
        }

        protected void ButtonForzaAnnoFrequenza_Click(object sender, EventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];

            if (biz.ForzaControlloAnnoFrequenza(idDomanda))
            {
                Domanda domanda = biz.GetDomanda(idDomanda);
                biz.ControllaDomandaScolastiche(domanda);

                CaricaDomanda(domanda);
            }
        }
    }
}