﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Utility;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.EmailClient;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Enums.Prestazioni;
using TBridge.Cemi.Type.Exceptions.Prestazioni;
using Telerik.Web.UI;
using Indirizzo = TBridge.Cemi.Type.Entities.Prestazioni.Indirizzo;
using UtenteLavoratore = TBridge.Cemi.Type.Entities.GestioneUtenti.Lavoratore;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class CompilazioneDomanda : System.Web.UI.Page
    {
        private const int INDICECASSEEDILI = 3;
        private const int INDICECOMUNICAZIONI = 2;
        private const int INDICECONFERMA = 6;
        private const int INDICEFATTURE = 5;
        private const int INDICEIDENTIFICAZIONE = 0;
        private const int INDICEINDIRIZZO = 1;
        private const int INDICETIPOPRESTAZIONE = 4;
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();
        private readonly Common commonBiz = new Common();

        private int idUtente;
        private bool isUtenteLavoratore;
        private UtenteLavoratore lavoratore;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                WizardInserimento.ActiveStepIndex = INDICEIDENTIFICAZIONE;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda);

            idUtente = GestioneUtentiBiz.GetIdUtente();

            #endregion

            #region Identificazione lavoratore, se ha effettuato login

            isUtenteLavoratore = GestioneUtentiBiz.IsLavoratore();
            if (isUtenteLavoratore)
            {
                // Se l'utente è un lavoratore allora posso riempire i dati iniziali in automatico
                //lavoratore =
                //    ((TBridge.Cemi.GestioneUtenti.Business.Identities.Lavoratore)HttpContext.Current.User.Identity).
                //        Entity;
                lavoratore =
                    (UtenteLavoratore)GestioneUtentiBiz.GetIdentitaUtenteCorrente();
            }

            #endregion

            #region Eventi da ascoltare

            PrestazioniRicercaFamiliare1.OnFamiliareSelected += PrestazioniRicercaFamiliare1_OnFamiliareSelected;

            #endregion

            if (!Page.IsPostBack)
            {
                ViewState["indiceWizard"] = 0;
                ViewState["GuidId"] = Guid.NewGuid();
                CaricaCombo();
                CaricaAnniScolastici();

                if (isUtenteLavoratore)
                {
                    // Se l'utente è un lavoratore allora posso riempire i dati iniziali in automatico
                    CaricaDatiLavoratore(lavoratore);

                    trDatiInizialiMessaggio.Visible = true;
                    trDatiLavoratoreMessaggio.Visible = true;

                    // Abilito anche la selezione del familiare (se la prestazione lo richiede)
                    AbilitazioneSelezioneFamiliare(true);
                }
                else
                {
                    //CaricaDatiProva();
                }

                if (Context.Items["IdDomandaTemporanea"] != null)
                {
                    int idDomandaTemporanea = (int)Context.Items["IdDomandaTemporanea"];

                    CaricaDatiDomandaTemporanea(idDomandaTemporanea);
                }
                else
                {
                    ViewState["Fatture"] = new FatturaDichiarataCollection();
                }

                CaricaFatture();
            }

            #region Click multipli

            // Per prevenire click multipli
            Button bConferma =
                (Button)
                WizardInserimento.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");
            // Per prevenire click multipli
            StringBuilder sb = new StringBuilder();
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            sb.Append("var oldPage_IsValid = Page_IsValid; var oldPage_BlockSubmit = Page_BlockSubmit;");
            sb.Append(
                "if (Page_ClientValidate('stop') == false)  { Page_IsValid = oldPage_IsValid; Page_BlockSubmit = oldPage_BlockSubmit; return false; }} ");
            sb.Append("this.value = 'Attendere...';");
            sb.Append("this.disabled = true;");
            sb.Append(Page.ClientScript.GetPostBackEventReference(bConferma, null) + ";");
            sb.Append("return true;");
            bConferma.Attributes.Add("onclick", sb.ToString());

            ((RadScriptManager)Page.Master.FindControl("RadScriptManagerMain")).RegisterPostBackControl(bConferma);

            #endregion

            #region Cambio colore link della sidebar

            // Per far funzionare il cambiamento di colore dei link della sidebar
            //Hashtable buttonsColl = new Hashtable();
            Control lst1 = WizardInserimento.FindControl("SideBarContainer");
            DataList lst = (DataList)lst1.FindControl("SideBarList");
            lst.ItemCreated += lst_ItemCreated;

            #endregion
        }

        private void CaricaAnniScolastici()
        {
            DateTime adesso = DateTime.Now;
            Int32 annoCorr = adesso.Year;

            if (adesso.Month < 6)
            {
                ListItem liAnno1 = new ListItem(String.Format("{0}/{1}", annoCorr - 1, annoCorr), "1");
                RadioButtonListAnnoScolastico.Items.Add(liAnno1);
            }

            ListItem liAnno2 = new ListItem(String.Format("{0}/{1}", annoCorr, annoCorr + 1), "1");
            RadioButtonListAnnoScolastico.Items.Add(liAnno2);

            if (RadioButtonListAnnoScolastico.Items.Count == 1)
            {
                RadioButtonListAnnoScolastico.Items[0].Selected = true;
            }
        }

        private void CaricaDatiDomandaTemporanea(int idDomandaTemporanea)
        {
            ViewState["IdDomandaTemporanea"] = idDomandaTemporanea;
            Domanda domandaTemporanea = biz.GetDomandaTemporanea(idDomandaTemporanea);

            // Indirizzo
            if (domandaTemporanea.Lavoratore.Indirizzo != null)
            {
                TextBoxDatiLavoratoreIndirizzo.Text = domandaTemporanea.Lavoratore.Indirizzo.IndirizzoVia;
                DropDownListDatiLavoratoreProvincia.SelectedValue = domandaTemporanea.Lavoratore.Indirizzo.Provincia;
                CaricaComuni(DropDownListDatiLavoratoreLocalita, domandaTemporanea.Lavoratore.Indirizzo.Provincia,
                             DropDownListDatiLavoratoreFrazione);
                DropDownListDatiLavoratoreLocalita.SelectedValue = domandaTemporanea.Lavoratore.Indirizzo.Comune;
                CaricaCapOFrazioni(DropDownListDatiLavoratoreLocalita, DropDownListDatiLavoratoreFrazione,
                                   TextBoxDatiLavoratoreCap);
                DropDownListDatiLavoratoreFrazione.SelectedValue = domandaTemporanea.Lavoratore.Indirizzo.Frazione;
                TextBoxDatiLavoratoreCap.Text = domandaTemporanea.Lavoratore.Indirizzo.Cap;

                PanelDatiLavoratore.Enabled = true;
                ButtonDatiLavoratoreModifica.Enabled = false;
            }

            // Comunicazioni
            if (domandaTemporanea.Lavoratore.Comunicazioni != null)
            {
                // Carico i dati forniti
                TextBoxDatiComunicazioniCellulare.Text = domandaTemporanea.Lavoratore.Comunicazioni.Cellulare;
                TextBoxDatiComunicazioniEmail.Text = domandaTemporanea.Lavoratore.Comunicazioni.Email;

                if (!string.IsNullOrEmpty(domandaTemporanea.Lavoratore.Comunicazioni.IdTipoPagamento))
                {
                    if (domandaTemporanea.Lavoratore.Comunicazioni.IdTipoPagamento == "B" || domandaTemporanea.Lavoratore.Comunicazioni.IdTipoPagamento == "A")
                    {
                        RadioButtonContoCorrente.Checked = true;
                        PanelDatiComunicazioniTipoPagamentoContoCorrenteDettagli.Enabled = true;
                        TextBoxDatiComunicazioniTipoPagamentoIBAN.Text = domandaTemporanea.Lavoratore.Comunicazioni.Iban;
                    }
                    else
                    {
                        RadioButtonCartaPrepagata.Checked = true;
                        PanelDatiComunicazioniTipoPagamentoCartaPrepagataDettagli.Enabled = true;
                        DropDownListDatiComunicazioniTipoCarta.SelectedValue =
                            domandaTemporanea.Lavoratore.Comunicazioni.IdTipoPagamento;
                    }
                }
            }

            // Tipo prestazione
            if (!string.IsNullOrEmpty(domandaTemporanea.Beneficiario))
            {
                DropDownListTipoPrestazioneBeneficiario.SelectedValue = domandaTemporanea.Beneficiario;
                CaricaTipiPrestazione(domandaTemporanea.Beneficiario);
                if (domandaTemporanea.TipoPrestazione != null)
                    DropDownListTipoPrestazioneTipoPrestazione.SelectedValue =
                        domandaTemporanea.TipoPrestazione.IdTipoPrestazione;

                if (domandaTemporanea.Familiare != null)
                {
                    PanelDatiFamiliare.Enabled = true;
                    ViewState["Familiare"] = domandaTemporanea.Familiare;
                    CaricaDatiFamiliare(domandaTemporanea.Familiare);

                    if (!domandaTemporanea.Familiare.IdFamiliare.HasValue)
                    {
                        PrestazioniDatiFamiliare1.StatoCampi(true);
                        PrestazioniDatiFamiliare1.ForzaStatoCampi();
                    }
                }
            }

            // Fatture
            ViewState["Fatture"] = domandaTemporanea.FattureDichiarate;
            CaricaFatture();

            // Casse edili
            PrestazioniSelezioneCasseEdili1.CaricaCasseEdili(domandaTemporanea.CasseEdili);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (WizardInserimento.ActiveStepIndex > INDICEIDENTIFICAZIONE && isUtenteLavoratore)
            {
                ButtonSalvaTemporaneamente.Visible = true;
            }
            else
            {
                ButtonSalvaTemporaneamente.Visible = false;
            }
        }

        private void CaricaCombo()
        {
            // Province
            StringCollection province = commonBiz.GetProvinceSiceNew();
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListDatiLavoratoreProvincia, province, null, null);
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListIstitutoScolasticoProvincia, province, null, null);
        }

        private void CaricaDatiProva()
        {
        }

        private void CaricaDatiLavoratore(UtenteLavoratore lavoratore)
        {
            // Nel caso in cui questa funzione venga chiamata disabilito la possibilità di
            // inserimento dei dati iniziali da parte dell'utente

            PanelDatiIniziali.Enabled = false;
            PanelDatiLavoratore.Enabled = false;
            //PanelDatiComunicazioni.Enabled = false;
            ButtonDatiLavoratoreModifica.Visible = true;
            //ButtonDatiComunicazioniModifica.Visible = true;

            // Dati iniziali
            TextBoxDatiInizialiCodiceFiscale.Text = lavoratore.CodiceFiscale;
            TextBoxDatiInizialiCognome.Text = lavoratore.Cognome;
            TextBoxDatiInizialiNome.Text = lavoratore.Nome;
            TextBoxDatiInizialiDataNascita.Text = lavoratore.DataNascita.ToShortDateString();

            // Dati lavoratore
            //if (!string.IsNullOrEmpty(lavoratore.IndirizzoPreIndirizzo))
            //    DropDownListDatiLavoratorePreIndirizzo.SelectedItem.Text = lavoratore.IndirizzoPreIndirizzo;
            TextBoxDatiLavoratoreIndirizzo.Text = lavoratore.IndirizzoDenominazione;
            TextBoxDatiLavoratoreCap.Text = lavoratore.IndirizzoCAP;
            if (!string.IsNullOrEmpty(lavoratore.IndirizzoProvincia))
            {
                DropDownListDatiLavoratoreProvincia.SelectedIndex =
                    DropDownListDatiLavoratoreProvincia.Items.IndexOf(
                        DropDownListDatiLavoratoreProvincia.Items.FindByText(lavoratore.IndirizzoProvincia));
                CaricaComuni(DropDownListDatiLavoratoreLocalita, lavoratore.IndirizzoProvincia, null);

                if (!string.IsNullOrEmpty(lavoratore.IndirizzoComune))
                {
                    DropDownListDatiLavoratoreLocalita.SelectedIndex =
                        DropDownListDatiLavoratoreLocalita.Items.IndexOf(
                            DropDownListDatiLavoratoreLocalita.Items.FindByText(lavoratore.IndirizzoComune));
                    string codiceComboComune = DropDownListDatiLavoratoreLocalita.SelectedValue;
                    string[] codiceSplittato = codiceComboComune.Split('|');
                    CaricaFrazioni(DropDownListDatiLavoratoreFrazione, codiceSplittato[0]);

                    if (!string.IsNullOrEmpty(lavoratore.IndirizzoFrazione))
                        DropDownListDatiLavoratoreFrazione.SelectedItem.Text =
                            lavoratore.IndirizzoFrazione;
                }
            }

            // Dati comunicazioni
            TextBoxDatiComunicazioniCellulare.Text = lavoratore.Cellulare;
            TextBoxDatiComunicazioniEmail.Text = lavoratore.Email;
            if (!string.IsNullOrEmpty(lavoratore.IdTipoPagamento))
            {
                //DropDownListDatiComunicazioniTipoPagamento.SelectedValue = lavoratore.IdTipoPagamento;
                //DropDownListDatiComunicazioniTipoPagamento.Enabled = false;
                trTipoPagamento.Visible = false;
            }

            // Se tutti i dati comunicazioni sono disponibili disabilito il pannello e abilito il bottone per la modifica
            if (!string.IsNullOrEmpty(lavoratore.Cellulare)
                && !string.IsNullOrEmpty(lavoratore.Email)
                && !string.IsNullOrEmpty(lavoratore.IdTipoPagamento))
            {
                PanelDatiComunicazioni.Enabled = false;
                ButtonDatiComunicazioniModifica.Visible = true;
            }
        }

        protected void WizardInserimento_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
        {
            // Dalla SideBar non si può andare avanti, ma solo indietro.
            // In questo modo risultano obbligate le validazioni fatte di pagina in pagina
            if (e.NextStepIndex >= e.CurrentStepIndex)
                e.Cancel = true;
            else
            {
                if (!Page.IsValid || !ValidaStep(e.CurrentStepIndex))
                {
                    e.Cancel = true;
                }
                else
                {
                    ViewState["indiceWizard"] = e.NextStepIndex;

                    if (e.NextStepIndex == INDICECONFERMA)
                        ValidaDomanda();
                }

                if (e.CurrentStepIndex == INDICEIDENTIFICAZIONE)
                {
                    PrestazioniDatiLavoratore1.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                    PrestazioniDatiLavoratore2.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                    PrestazioniDatiLavoratore3.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                    PrestazioniDatiLavoratore4.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                    PrestazioniDatiLavoratore5.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                }
            }
        }

        protected void WizardInserimento_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
        {
            //if (!Page.IsValid || !ValidaStep(e.CurrentStepIndex))
            //{
            //    e.Cancel = true;
            //}
            //else
            //{
            //    ViewState["indiceWizard"] = e.NextStepIndex;

            //    if (e.NextStepIndex == INDICECONFERMA)
            //        ValidaDomanda();
            //}

            ViewState["indiceWizard"] = e.NextStepIndex;
        }

        protected void WizardInserimento_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (!Page.IsValid || !ValidaStep(e.CurrentStepIndex))
            {
                e.Cancel = true;
            }
            else
            {
                ViewState["indiceWizard"] = e.NextStepIndex;

                if (e.NextStepIndex == INDICECONFERMA)
                    ValidaDomanda();
            }

            if (e.CurrentStepIndex == INDICEIDENTIFICAZIONE)
            {
                PrestazioniDatiLavoratore1.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                PrestazioniDatiLavoratore2.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                PrestazioniDatiLavoratore3.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                PrestazioniDatiLavoratore4.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
                PrestazioniDatiLavoratore5.CaricaLavoratore((Lavoratore)ViewState["Lavoratore"]);
            }
        }

        private void ValidaDomanda()
        {
            Configurazione configurazione = (Configurazione)ViewState["Configurazione"];

            CaricaRiassunto();

            // Elenco i documenti necessari
            if (!string.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue)
                && !string.IsNullOrEmpty(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue)
                && ViewState["Lavoratore"] != null)
            {
                string idTipoPrestazione = DropDownListTipoPrestazioneTipoPrestazione.SelectedValue;
                string beneficiario = DropDownListTipoPrestazioneBeneficiario.SelectedValue;
                int idLavoratore = ((Lavoratore)ViewState["Lavoratore"]).IdLavoratore.Value;
                int? idFamiliare = null;

                if (ViewState["Familiare"] != null && ((Familiare)ViewState["Familiare"]).IdFamiliare.HasValue)
                    idFamiliare = ((Familiare)ViewState["Familiare"]).IdFamiliare.Value;

                GridViewDocumentiRichiesti.DataSource = biz.GetDocumentiNecessari(idTipoPrestazione, beneficiario,
                                                                                  idLavoratore, idFamiliare);
                GridViewDocumentiRichiesti.DataBind();
            }

            // Controllo che siano stati completate tutte le informazioni necessarie
            BullettedListResoconto.Items.Clear();

            if (ViewState["Lavoratore"] == null)
                BullettedListResoconto.Items.Add("Mancano i dati per l'identificazione");
            else
            {
                Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
                if (lavoratore.Indirizzo == null)
                    BullettedListResoconto.Items.Add("Mancano i dati dell'indirizzo");
                if (lavoratore.Comunicazioni == null)
                    BullettedListResoconto.Items.Add("Mancano i dati delle comunicazioni");
            }

            if (DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L" && ViewState["Familiare"] == null)
                BullettedListResoconto.Items.Add("Mancano i dati del familiare");

            FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
            if (configurazione.RichiestaFattura)
            {
                if (fatture.Count == 0)
                    BullettedListResoconto.Items.Add("Non è stata caricata nessuna fattura");
                else if (!fatture.IsPresenteFatturaSaldo())
                    BullettedListResoconto.Items.Add("Non è stata caricata nessuna fattura a saldo");
            }

            if (ViewState["Configurazione"] == null)
                BullettedListResoconto.Items.Add("Non è stata selezionata la prestazione");

            Button bConferma =
                (Button)
                WizardInserimento.FindControl("FinishNavigationTemplateContainerID").FindControl("btnSuccessivoFinish");


            if (BullettedListResoconto.Items.Count > 0 || !ControlliSupplementari())
            {
                bConferma.Enabled = false;
            }
            else
            {
                bConferma.Enabled = true;
            }
        }

        //Sono stati implementati dei controlli supplementari per la gestione dei figli
        private bool ControlliSupplementari()
        {
            bool res = true;

            //controllo se in caso di asilo nido il familiare ha più di 8 anni
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text == "C-ANID" &&
                DateTime.Now.Subtract(DateTime.Parse(TextBoxDatiInizialiDataNascita.Text)).TotalDays >= 8 * 365)
            {
                res = false;
            }

            //controllo in caso di prima media che il familiare non abbia più di 14 anni
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text == "C006" &&
                DateTime.Now.Subtract(DateTime.Parse(TextBoxDatiInizialiDataNascita.Text)).TotalDays >= 14 * 365)
            {
                res = false;
            }

            //controllo che, in caso di Soggiorno Estivo, il familiare abbia tra i 5 e i 18 anni
            if ((DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text == "C011-1" || DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text == "C011-2") &&
                (DateTime.Parse(TextBoxDatiInizialiDataNascita.Text).Year < (DateTime.Now.Year - 18) || DateTime.Parse(TextBoxDatiInizialiDataNascita.Text).Year > (DateTime.Now.Year - 5)))
            {
                res = false;
            }

            return res;
        }

        private void CaricaRiassunto()
        {
            LabelRiassuntoCodiceFiscale.Text = TextBoxDatiInizialiCodiceFiscale.Text;
            LabelRiassuntoCognome.Text = TextBoxDatiInizialiCognome.Text.Trim().ToUpper();
            LabelRiassuntoNome.Text = TextBoxDatiInizialiNome.Text.Trim().ToUpper();
            LabelRiassuntoDataNascita.Text = TextBoxDatiInizialiDataNascita.Text;
            LabelRiassuntoCellulare.Text = TextBoxDatiComunicazioniCellulare.Text.Trim().ToUpper();
            LabelRiassuntoEmail.Text = TextBoxDatiComunicazioniEmail.Text;

            LabelRiassuntoIndirizzo.Text = TextBoxDatiLavoratoreIndirizzo.Text.Trim().ToUpper();
            LabelRiassuntoProvincia.Text = DropDownListDatiLavoratoreProvincia.SelectedItem.Text;
            LabelRiassuntoLocalita.Text = DropDownListDatiLavoratoreLocalita.SelectedItem.Text;
            LabelRiassuntoFrazione.Text = DropDownListDatiLavoratoreFrazione.SelectedItem.Text;
            LabelRiassuntoCAP.Text = TextBoxDatiLavoratoreCap.Text;

            LabelRiassuntoBeneficiario.Text = DropDownListTipoPrestazioneBeneficiario.SelectedItem.Text;
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedItem != null)
                LabelRiassuntoTipoPrestazione.Text = DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text;

            Familiare familiare = PrestazioniDatiFamiliare1.CreaFamiliare();
            if (!string.IsNullOrEmpty(familiare.Cognome))
            {
                LabelRiassuntoBeneficiarioCognome.Text = familiare.Cognome;
                LabelRiassuntoBeneficiarioNome.Text = familiare.Nome;
                LabelRiassuntoBeneficiarioDataNascita.Text = familiare.DataNascita.Value.ToShortDateString();
                LabelRiassuntoBeneficiarioCodiceFiscale.Text = familiare.CodiceFiscale;
            }
            else
            {
                LabelRiassuntoBeneficiarioCognome.Text = TextBoxDatiInizialiCognome.Text.Trim().ToUpper();
                ;
                LabelRiassuntoBeneficiarioNome.Text = TextBoxDatiInizialiNome.Text.Trim().ToUpper();
                LabelRiassuntoBeneficiarioDataNascita.Text = TextBoxDatiInizialiDataNascita.Text;
                LabelRiassuntoBeneficiarioCodiceFiscale.Text = TextBoxDatiInizialiCodiceFiscale.Text;
            }

            if (ViewState["Fatture"] != null)
            {
                FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
                LabelRiassuntoNumeroFatture.Text = fatture.Count.ToString();

                GridViewRiassuntoFatture.DataSource = ViewState["Fatture"];
                GridViewRiassuntoFatture.DataBind();
            }
        }

        protected void WizardInserimento_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
            Domanda domanda = CreaDomanda();
            domanda.Confermata = true;

            int? idDomandaTemporanea = null;
            if (ViewState["IdDomandaTemporanea"] != null)
            {
                idDomandaTemporanea = (int)ViewState["IdDomandaTemporanea"];
            }

            try
            {
                string loginCNCE = ConfigurationManager.AppSettings["CNCEUserName"];
                string passwordCNCE = ConfigurationManager.AppSettings["CNCEPassword"];
                string urlCNCE = ConfigurationManager.AppSettings["CNCEUrl"];

                if (biz.InsertDomanda(domanda, loginCNCE, passwordCNCE, urlCNCE, idDomandaTemporanea, idUtente))
                {
                    if (!string.IsNullOrEmpty(domanda.Lavoratore.Comunicazioni.Email))
                    {
                        try
                        {
                            string wsReporting2005 = ConfigurationManager.AppSettings["WSReportingServices2005"];
                            string wsExecution2005 = ConfigurationManager.AppSettings["WSReportingExecution2005"];
                            byte[] moduli = biz.CreaModuloPdfDomanda(domanda, configurazione.TipoModulo, wsReporting2005,
                                                                     wsExecution2005);

                            InviaModulo(domanda, moduli);
                        }
                        catch
                        {
                        }
                    }

                    //Notifichiamo al CRM un inserimento della prestazioni 
                    //le eccezioni del WS non vengono gestire a questo livello
                    if (!Presenter.IsSviluppo)
                    {
                        biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                    }

                    Context.Items["IdDomanda"] = domanda.IdDomanda;

                    switch (configurazione.TipoMacroPrestazione.IdTipoMacroPrestazione)
                    {
                        // Prestazioni Sanitarie
                        case 1:
                            Server.Transfer("~/CeServizi/Prestazioni/ConfermaSanitarie.aspx");
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            Context.Items["TipoModulo"] = configurazione.TipoModulo.Modulo;
                            Server.Transfer("~/CeServizi/Prestazioni/ConfermaScolastiche.aspx");
                            break;
                    }
                }
            }
            catch (UnivocitaViolataException univExc)
            {
                e.Cancel = true;
                BullettedListResoconto.Items.Clear();
                BullettedListResoconto.Items.Add(
                    "La domanda non può essere presentata perchè viola i vincoli di univocità della prestazione. Contattare Cassa Edile di Milano");
            }
            catch (DomandaGiaInseritaException giaInsExc)
            {
                e.Cancel = true;
                BullettedListResoconto.Items.Clear();
                BullettedListResoconto.Items.Add(
                    "La domanda non può essere presentata perchè è già presente una domanda con gli stessi dati");
            }
        }

        private void InviaModulo(Domanda domanda, byte[] moduli)
        {
            string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
            string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

            EmailInfoService service = new EmailInfoService();
            EmailMessageSerializzabile email = new EmailMessageSerializzabile();

            // Il testo della mail va memorizzato sul DB
            StringBuilder testoPlain = new StringBuilder();
            testoPlain.Append("Trasmettiamo in allegato la domanda di prestazione compilata ed inoltrata telematicamente");
            testoPlain.Append(
                " a Cassa Edile di Milano, Lodi, Monza e Brianza tramite la funzione informatica 'Prestazioni'");
            testoPlain.Append(" del sito internet www.cassaedilemilano.it");
            testoPlain.AppendLine();
            testoPlain.AppendLine();
            testoPlain.Append("Cordiali saluti.");
            testoPlain.AppendLine();
            testoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            email.BodyPlain = testoPlain.ToString();

            StringBuilder testoHTML = new StringBuilder();
            testoHTML.Append("Trasmettiamo in allegato la domanda di prestazione compilata ed inoltrata telematicamente");
            testoHTML.Append(
                " a Cassa Edile di Milano, Lodi, Monza e Brianza tramite la funzione informatica '<b>Prestazioni</b>'");
            testoHTML.Append(" del sito internet <a href='http://www.cassaedilemilano.it'>www.cassaedilemilano.it</a>");
            testoHTML.Append("<BR />");
            testoHTML.Append("<BR />");
            testoHTML.Append("Cordiali saluti.");
            testoHTML.Append("<BR />");
            testoHTML.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
            email.BodyHTML = testoHTML.ToString();
            // Fine testo mail


            email.Destinatari = new List<EmailAddress>(1);
            email.Destinatari[0] = new EmailAddress();

            // Sostituire con indirizzo lavoratore
            email.Destinatari[0].Indirizzo = domanda.Lavoratore.Comunicazioni.Email;
            email.Destinatari[0].Nome = String.Format("{0} {1}", domanda.Lavoratore.Nome, domanda.Lavoratore.Cognome);

            //email.DestinatariBCC = new List<EmailAddress>(1);
            //email.DestinatariBCC[0] = new EmailAddress();
            //email.DestinatariBCC[0].Indirizzo = "g.trifoglio@tbridge.it";
            //email.DestinatariBCC[0].Nome = "Giuliano Trifoglio";

            email.Mittente = new EmailAddress();
            email.Mittente.Indirizzo = "servizioprestazioni@cassaedilemilano.it";
            email.Mittente.Nome = "Cassa Edile Milano, servizio prestazioni";

            email.Oggetto = "Cassa Edile di Milano, Modulistica";

            email.Allegati = new List<EmailAttachment>(1);
            email.Allegati[0] = new EmailAttachment();
            email.Allegati[0].File = moduli;
            email.Allegati[0].NomeFile = "moduli.pdf";

            email.DataSchedulata = DateTime.Now;
            email.Priorita = MailPriority.High;

            NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
            service.Credentials = credentials;

            service.InviaEmail(email);
        }

        private Domanda CreaDomanda()
        {
            Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
            Domanda domanda = new Domanda();

            domanda.Lavoratore = (Lavoratore)ViewState["Lavoratore"];
            if (ViewState["Familiare"] != null)
                domanda.Familiare = (Familiare)ViewState["Familiare"];
            domanda.FattureDichiarate = (FatturaDichiarataCollection)ViewState["Fatture"];
            domanda.CasseEdili = PrestazioniSelezioneCasseEdili1.GetCasseEdili();
            domanda.DataDomanda = DateTime.Now;
            domanda.TipoInserimento = TipoInserimento.Normale;

            domanda.TipoPrestazione = new TipoPrestazione();
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedItem != null)
            {
                domanda.TipoPrestazione.IdTipoPrestazione = DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Value;
                domanda.TipoPrestazione.Descrizione = DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text;
            }
            // Il grado di parentela Genitori conviventi è solo di facciata ma si comporta
            // come i parenti affini conviventi
            if (DropDownListTipoPrestazioneBeneficiario.SelectedValue == "O")
            {
                domanda.Beneficiario = "A";
                domanda.GenitoriConviventi = true;
            }
            else
            {
                domanda.Beneficiario = DropDownListTipoPrestazioneBeneficiario.SelectedValue;
            }
            domanda.Stato = new StatoDomanda("I");

            if (!string.IsNullOrEmpty(DropDownListTipologiaScuola.SelectedValue))
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                domanda.DatiAggiuntiviScolastiche.TipoScuola =
                    new TipoScuola(Int32.Parse(DropDownListTipologiaScuola.SelectedValue));
            }

            if (!string.IsNullOrEmpty(DropDownListTipologiaPromozione.SelectedValue))
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                domanda.DatiAggiuntiviScolastiche.TipoPromozione =
                    new TipoPromozione(Int32.Parse(DropDownListTipologiaPromozione.SelectedValue));
            }

            //Domanda di prestazione funeraria
            if (domanda.TipoPrestazione.IdTipoPrestazione == "C002")
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                domanda.DatiAggiuntiviScolastiche.DataDecesso = DateTime.Parse(TextBoxDataDecesso.Text);
                domanda.DatiAggiuntiviScolastiche.IdTipoPrestazioneDecesso = DropDownListTipoDecesso.SelectedValue;
            }

            //Domanda per superiori e univeristà
            if ((domanda.TipoPrestazione.IdTipoPrestazione == "C003-1"
                || domanda.TipoPrestazione.IdTipoPrestazione == "C003-2"
                || domanda.TipoPrestazione.IdTipoPrestazione == "C003-3")
                && domanda.Beneficiario != "L")
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                domanda.DatiAggiuntiviScolastiche.RedditoSi = RadioButtonRedditoSi.Checked;
            }

            // Domanda per 1a media
            if (domanda.TipoPrestazione.IdTipoPrestazione == "C006")
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                domanda.DatiAggiuntiviScolastiche.AnnoScolastico = RadioButtonListAnnoScolastico.SelectedItem.Text;
                domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione = Presenter.NormalizzaCampoTesto(TextBoxIstitutoScolastico.Text);
                domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo = Presenter.NormalizzaCampoTesto(TextBoxIstitutoScolasticoIndirizzo.Text);
                domanda.DatiAggiuntiviScolastiche.IstitutoProvincia = DropDownListIstitutoScolasticoProvincia.SelectedValue;
                domanda.DatiAggiuntiviScolastiche.IstitutoLocalita = DropDownListIstitutoScolasticoLocalita.SelectedItem.Text;
            }

            domanda.Guid = (Guid)ViewState["GuidId"];

            //DateTime dataTemp = new DateTime();
            //se è richiesta la fattura, la data min o max è la data evento e quindi la data di riferimento della prestazione
            if (configurazione != null && configurazione.RichiestaFattura)
            {
                //// Imposto come data di riferimento della domanda la data della prima fattura a saldo caricata
                //foreach (FatturaDichiarata fattura in domanda.FattureDichiarate)
                //{
                //    if (fattura.Saldo && (domanda.DataRiferimento == dataTemp || domanda.DataRiferimento > fattura.Data))
                //    {
                //        domanda.DataRiferimento = fattura.Data;
                //    }
                //}

                //Nel caso di asilo nido prendiamo l'ultima data fattura
                if (domanda.TipoPrestazione.IdTipoPrestazione == "C-ANID")
                {
                    //siamo sicuri che almeno una fattura è valorizzata quindi possiamo prendere il valore del tipo nullable senza controlli
                    domanda.DataRiferimento = domanda.FattureDichiarate.MaxDataFattura.Value;
                }
                // per tutte le altre prendiamo la data fattura minima
                else
                {
                    domanda.DataRiferimento = domanda.FattureDichiarate.MinDataFattura.Value;
                }
            }
            //se la domanda è di tipo scolastico c006 c003-1 la data di riferimento è il primo ottobre, il rolling poi genera la "data calcolo" portandola indietro di un anno
            else if
                (
                domanda.TipoPrestazione.IdTipoPrestazione == "C006" ||
                domanda.TipoPrestazione.IdTipoPrestazione == "C003-1"
                )
            {
                //mettiamo ottobre di un anno ipotetico, poi l'operatore selezione l'anno corretto ed il sistema di generazione del rolling porta indietro di un mese
                //if (DateTime.Now.Month >= 10 && domanda.TipoPrestazione.IdTipoPrestazione == "C006")
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year,10,1);
                //else
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year-1, 10, 1);

                //if (DateTime.Now.Month >= 6 && DateTime.Now.Month <= 12 && domanda.TipoPrestazione.IdTipoPrestazione == "C003-1")
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year-1, 10, 1);
                //else
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year - 2, 10, 1);

                //inutile fare tante ipotesi tanto devono per forza definire l'anno di frequenza, in base a quello viene calcolato l'inizio anno o il fine anno scolastico
                domanda.DataRiferimento = DateTime.Now;
            }
            else
            {
                // Imposto come data di riferimento la data di inserimento della domanda
                domanda.DataRiferimento = DateTime.Now;
            }

            domanda.CertificatoFamiglia = RadioButtonCertificatoFamigliaSi.Checked;
            domanda.ModuloDentista = RadioButtonModuloDentistaSi.Checked;

            return domanda;
        }

        private bool ValidaStep(int stepCorrente)
        {
            bool res = true;

            switch (stepCorrente)
            {
                case INDICEINDIRIZZO:
                    IntegraLavoratoreConIndirizzo();
                    break;
                case INDICECOMUNICAZIONI:
                    IntegraLavoratoreConComunicazioni();
                    break;
                case INDICETIPOPRESTAZIONE:

                    if (DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L")
                    {
                        Page.Validate("datiFamiliare");

                        if (!Page.IsValid)
                            return false;
                    }

                    // Nel caso in cui il lavoratore non sia autenticato verifico se il familiare
                    // è presente nell'anagrafica e nel caso se è deceduto
                    if (!isUtenteLavoratore || ViewState["Familiare"] == null)
                    {
                        Familiare familiare = CreaFamiliare();
                        biz.IsFamiliareInAnagrafica(familiare);
                        ViewState["Familiare"] = familiare;
                    }

                    // Se il tipo di prestazione selezionata è per le lenti visualizzo il pannellino per l'importo lenti
                    if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C007-1"
                        || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C007-2"
                        || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C007-3")
                    {
                        PanelLenti.Visible = true;
                    }
                    else
                    {
                        PanelLenti.Visible = false;
                        TextBoxImportoLenti.Text = null;
                    }

                    // Se il tipo di prestazione selezionata è per le protesi e cure dentarie visualizzo il pannellino per il modulo per il dentista
                    if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C004"
                        || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C004-1"
                        || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C004CP")
                    {
                        trModuloDentista.Visible = true;
                    }
                    else
                    {
                        trModuloDentista.Visible = false;
                    }

                    Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
                    Configurazione configurazione = biz.GetConfigurazionePrestazione(
                        DropDownListTipoPrestazioneTipoPrestazione.SelectedValue,
                        biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue),
                        lavoratore.IdLavoratore.Value);
                    ViewState["Configurazione"] = configurazione;

                    #region Visualizzazione del pannello per i dati funerari

                    bool richiestaFunerario;
                    // Se il tipo di prestazione selezionata è per le lenti visualizzo il pannellino per l'importo lenti
                    if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C002")
                    {
                        CaricaTipiDecesso();
                        richiestaFunerario = true;
                        PanelFunerario.Visible = true;

                        //se l'utente non è il lavoratore impostiamo con un range più ampio il controllo della data decesso
                        if (!isUtenteLavoratore)
                        {
                            CompareValidatorDataDecessoMinima.ValueToCompare =
                                DateTime.Now.AddMonths(-24).ToShortDateString();
                        }
                        else
                        {
                            CompareValidatorDataDecessoMinima.ValueToCompare =
                                DateTime.Now.AddMonths((-1) * configurazione.DifferenzaMesiFatturaDomanda).ToShortDateString();
                        }
                        CompareValidatorDataDecessoMassima.ValueToCompare = DateTime.Now.ToShortDateString();
                    }
                    else
                    {
                        richiestaFunerario = false;
                        PanelFunerario.Visible = false;
                    }

                    #endregion

                    #region Calcolo della data limite per le fatture

                    //se un dipendente CE aumentiamo il margine a 18 mesi
                    if (!isUtenteLavoratore && GestioneUtentiBiz.GetIdUtente() != -1)
                    {
                        PrestazioniDatiFattura1.ImpostaValidatorPerDataFattura(24);
                    }
                    //se non è un dipendente usiamo la data configurata
                    else
                    {
                        PrestazioniDatiFattura1.ImpostaValidatorPerDataFattura(configurazione.DifferenzaMesiFatturaDomanda);
                    }

                    if ((configurazione.IdTipoPrestazione == "C011-1") || (configurazione.IdTipoPrestazione == "C011-2"))
                    {
                        ViewState["SoggiornoEstivo"] = true;
                    }
                    else
                    {
                        ViewState["SoggiornoEstivo"] = false;
                    }

                    #endregion

                    #region Visualizzazione del pannello per le fatture

                    AbilitaDisabilitaFatture(configurazione.RichiestaFattura);

                    #endregion

                    #region Visualizzazione del pannello nessun dato aggiuntivo

                    if (!configurazione.RichiestaFattura && !configurazione.RichiestaTipoScuola && !richiestaFunerario)
                    {
                        PanelNessunDato.Visible = true;
                    }
                    else
                    {
                        PanelNessunDato.Visible = false;
                    }

                    #endregion

                    #region Visualizzazione del pannello per le scolastiche

                    if (configurazione.RichiestaTipoScuola)
                    {
                        CaricaTipiScuola(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);
                        PanelScolastiche.Visible = true;
                    }
                    else
                    {
                        PanelScolastiche.Visible = false;
                        DropDownListTipologiaPromozione.Items.Clear();
                        if (DropDownListTipologiaScuola.Items.Count > 0)
                            DropDownListTipologiaScuola.SelectedIndex = 0;
                    }

                    #region Scolastiche medie

                    if ((DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C006"))
                    {
                        PanelScolasticheSecondarie1.Visible = true;
                        PanelNessunDato.Visible = false;
                    }
                    else
                    {
                        PanelScolasticheSecondarie1.Visible = false;
                    }

                    #endregion

                    #region Scolastiche superiori e universitarie

                    if ((DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C003-1"
                        || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C003-2"
                        || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C003-3")
                        && DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L")
                    {
                        PanelScolasticheUnico.Visible = true;
                        PanelNessunDato.Visible = false;
                    }
                    else
                    {
                        PanelScolasticheUnico.Visible = false;
                    }

                    #endregion

                    #endregion

                    break;
            }

            return res;
        }

        private void AbilitaDisabilitaFatture(bool abilitato)
        {
            PanelFatture.Visible = abilitato;
            CustomValidatorAlmenoUnaFattura.Enabled = abilitato;
            CustomValidatorFatture.Enabled = abilitato;

            trFatture1.Visible = abilitato;
            trFatture2.Visible = abilitato;
            trFatture3.Visible = abilitato;
        }

        private Familiare CreaFamiliare()
        {
            Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
            Familiare familiare = PrestazioniDatiFamiliare1.CreaFamiliare();
            familiare.GradoParentela = DropDownListTipoPrestazioneBeneficiario.SelectedValue;
            familiare.IdLavoratore = lavoratore.IdLavoratore.Value;
            return familiare;
        }

        private Lavoratore CreaLavoratore()
        {
            Lavoratore lavoratore = new Lavoratore();

            lavoratore.CodiceFiscale = TextBoxDatiInizialiCodiceFiscale.Text;
            lavoratore.Cognome = TextBoxDatiInizialiCognome.Text;
            lavoratore.Nome = TextBoxDatiInizialiNome.Text;
            lavoratore.DataNascita = DateTime.Parse(TextBoxDatiInizialiDataNascita.Text);

            ViewState["Lavoratore"] = lavoratore;

            return lavoratore;
        }

        private void IntegraLavoratoreConIndirizzo()
        {
            Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];

            lavoratore.Indirizzo = new Indirizzo();
            lavoratore.Indirizzo.IndirizzoVia = TextBoxDatiLavoratoreIndirizzo.Text.Trim().ToUpper();
            lavoratore.Indirizzo.Provincia = DropDownListDatiLavoratoreProvincia.SelectedValue;
            lavoratore.Indirizzo.Comune = DropDownListDatiLavoratoreLocalita.SelectedValue;
            lavoratore.Indirizzo.Frazione = DropDownListDatiLavoratoreFrazione.SelectedValue;
            lavoratore.Indirizzo.Cap = TextBoxDatiLavoratoreCap.Text;
        }

        private void IntegraLavoratoreConComunicazioni()
        {
            Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];

            lavoratore.Comunicazioni = new Comunicazioni();
            lavoratore.Comunicazioni.Cellulare = CellulareHelper.PulisciNumeroCellulare(TextBoxDatiComunicazioniCellulare.Text);
            lavoratore.Comunicazioni.Email = TextBoxDatiComunicazioniEmail.Text;

            if (RadioButtonContoCorrente.Checked)
            {
                lavoratore.Comunicazioni.IdTipoPagamento = "B";
                lavoratore.Comunicazioni.Iban = TextBoxDatiComunicazioniTipoPagamentoIBAN.Text.Trim().ToUpper();
            }

            if (RadioButtonCartaPrepagata.Checked)
            {
                lavoratore.Comunicazioni.IdTipoPagamento = DropDownListDatiComunicazioniTipoCarta.SelectedValue;
            }
        }

        protected void ButtonDatiLavoratoreModifica_Click(object sender, EventArgs e)
        {
            PanelDatiLavoratore.Enabled = true;
            ButtonDatiLavoratoreModifica.Enabled = false;
        }

        protected void ButtonDatiComunicazioniModifica_Click(object sender, EventArgs e)
        {
            PanelDatiComunicazioni.Enabled = true;
            ButtonDatiComunicazioniModifica.Enabled = false;
        }

        protected void DropDownListTipoPrestazioneBeneficiario_SelectedIndexChanged(object sender, EventArgs e)
        {
            // In base al beneficiario carico le prestazioni corrispondenti e l'eventuale
            // selezione/inserimento del familiare

            PrestazioniDatiFamiliare1.ResetCampi();
            PrestazioniDatiFamiliare1.StatoCampi(false);
            ViewState["Familiare"] = null;

            if (!string.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue))
            {
                CaricaTipiPrestazione(biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue));
                if (isUtenteLavoratore)
                    CaricaFamiliari(biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue));

                if (DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L")
                {
                    PanelDatiFamiliare.Enabled = true;
                    if (!isUtenteLavoratore)
                    {
                        PrestazioniDatiFamiliare1.StatoCampi(true);
                    }
                }
                else
                {
                    PanelDatiFamiliare.Enabled = false;
                    ResetSelezioneFamiliare();
                }
            }
            else
            {
                DropDownListTipoPrestazioneTipoPrestazione.Items.Clear();
                ResetSelezioneFamiliare();
            }
        }

        private void CaricaFamiliari(string gradoParentela)
        {
            ButtonDatiFamiliareSelezionaFamiliare.Enabled = false;

            PrestazioniRicercaFamiliare1.Visible = true;
            FamiliareCollection familiari = biz.GetFamiliariPerGradoParentela(lavoratore.IdLavoratore, gradoParentela, DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "PRENAT" || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C011-1" || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C011-2" ? true : false);
            PrestazioniRicercaFamiliare1.CaricaFamiliari(familiari);
        }

        private void ResetSelezioneFamiliare()
        {
            ButtonDatiFamiliareSelezionaFamiliare.Enabled = true;

            PrestazioniDatiFamiliare1.ResetCampi();
            ResetGridViewFamiliari();
            ViewState["Familiare"] = null;
        }

        private void ResetGridViewFamiliari()
        {
            PrestazioniRicercaFamiliare1.Visible = false;
            PrestazioniRicercaFamiliare1.CaricaFamiliari(null);
        }

        private void CaricaTipiPrestazione(string beneficiario)
        {
            DropDownListTipoPrestazioneTipoPrestazione.Items.Clear();
            DropDownListTipoPrestazioneTipoPrestazione.Items.Add(new ListItem(string.Empty, string.Empty));

            TipoPrestazioneCollection tipiPrestazione = biz.GetTipiPrestazionePerGradoParentela(beneficiario);
            DropDownListTipoPrestazioneTipoPrestazione.DataSource = tipiPrestazione;
            DropDownListTipoPrestazioneTipoPrestazione.DataTextField = "Descrizione";
            DropDownListTipoPrestazioneTipoPrestazione.DataValueField = "IdTipoPrestazione";
            DropDownListTipoPrestazioneTipoPrestazione.DataBind();
        }

        private void PrestazioniRicercaFamiliare1_OnFamiliareSelected(Familiare familiare)
        {
            LabelDataNascitaFiglio.Visible = false;
            bool chkData = true;
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue != "" && DropDownListTipoPrestazioneTipoPrestazione.SelectedValue != null)
            {
                Configurazione configurazione = biz.GetConfigurazionePrestazione(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue as string, "F", familiare.IdLavoratore); //(Configurazione)ViewState["Configurazione"];

                //if ( DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "PRENAT")
                //{
                //    Configurazione conf = biz.GetConfigurazionePrestazione(ViewState["idTipoPrestazione"] as string, "F", familiare.IdLavoratore);

                //    if (!familiare.DataNascita.HasValue || familiare.DataNascita < DateTime.Now.AddMonths(-conf.DifferenzaMesiFatturaDomanda) )
                //    {
                //        LabelDataNascitaFiglio.Visible = true;
                //        chkData = false;
                //    }
                //}

                if (configurazione.IdTipoPrestazione == "C011-1" || configurazione.IdTipoPrestazione == "C011-2")
                {
                    if (familiare.DataNascita.HasValue)
                    {
                        if (familiare.DataNascita.Value.Year < (DateTime.Now.Year - 18) || familiare.DataNascita.Value.Year > (DateTime.Now.Year - 5))
                        {
                            LabelDataNascitaFiglio.Text = $"La data di nascita del familiare deve essere compresa tra 01/01/{DateTime.Now.Year - 18} e 31/12/{DateTime.Now.Year - 5}";
                            LabelDataNascitaFiglio.Visible = true;
                            chkData = false;
                        }
                    }
                }

                if (chkData)
                {
                    ViewState["Familiare"] = familiare;
                    CaricaDatiFamiliare(familiare);
                }
            }
            else
            {
                LabelDataNascitaFiglio.Text = $"Selezionare una tipologia prestazione.";
                LabelDataNascitaFiglio.Visible = true;
            }
        }

        private void CaricaDatiFamiliare(Familiare familiare)
        {
            PrestazioniDatiFamiliare1.CaricaDatiFamiliare(familiare);
            ResetGridViewFamiliari();
            ButtonDatiFamiliareSelezionaFamiliare.Enabled = true;
        }

        private void AbilitazioneSelezioneFamiliare(bool abilita)
        {
            ButtonDatiFamiliareSelezionaFamiliare.Visible = abilita;
            ButtonDatiFamiliareNuovo.Visible = abilita;
            PrestazioniRicercaFamiliare1.Visible = abilita;
        }

        protected void ButtonDatiFamiliareSelezionaFamiliare_Click(object sender, EventArgs e)
        {
            CaricaFamiliari(DropDownListTipoPrestazioneBeneficiario.SelectedValue);
        }

        private void CaricaFatture()
        {
            FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
            GridViewFatture.DataSource = fatture;
            GridViewFatture.DataBind();
        }

        protected void ButtonFattureNuovaInserisci_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                FatturaDichiarata fattura = CreaFattura();
                Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];

                if (!biz.FatturaGiaUtilizzata(fattura, lavoratore.IdLavoratore.Value))
                {
                    LabelFatturaGiaPresente.Visible = false;
                    FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
                    Configurazione configurazione = (Configurazione)ViewState["Configurazione"];

                    if (configurazione.IdTipoPrestazione != "C011-1" && configurazione.IdTipoPrestazione != "C011-2")
                    {
                        if (!fatture.IsGiaPresente(fattura))
                        {
                            fatture.Add(fattura);
                            ViewState["Fatture"] = fatture;

                            ResetNuovaFattura();
                            CaricaFatture();

                            LabelFatturaGiaCaricata.Visible = false;
                            LabelFatturaGiaPresente.Visible = false;
                            LabelFatturaNonCorrettaGenerico.Visible = false;
                        }
                        else
                        {
                            LabelFatturaGiaCaricata.Visible = true;
                        }
                    }
                    else
                    {
                        if (fattura.Data.Year != DateTime.Now.Year)
                        {
                            LabelFatturaNonCorrettaGenerico.Text = $"La data della fattura deve essere compresa tra 01/01/{DateTime.Now.Year} e 31/12/{DateTime.Now.Year}.";
                            LabelFatturaNonCorrettaGenerico.Visible = true;
                        }
                        else
                        {
                            fatture.Add(fattura);
                            ViewState["Fatture"] = fatture;

                            ResetNuovaFattura();
                            CaricaFatture();

                            LabelFatturaGiaCaricata.Visible = false;
                            LabelFatturaGiaPresente.Visible = false;
                            LabelFatturaNonCorrettaGenerico.Visible = false;
                        }
                    }
                }
                else
                {
                    LabelFatturaGiaPresente.Visible = true;
                }
            }
        }

        private void ResetNuovaFattura()
        {
            PrestazioniDatiFattura1.ResetCampi();
        }

        private FatturaDichiarata CreaFattura()
        {
            FatturaDichiarata fattura = PrestazioniDatiFattura1.CreaFattura();
            return fattura;
        }

        protected void GridViewFatture_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
            fatture.RemoveAt(e.RowIndex);
            ViewState["Fatture"] = fatture;

            CaricaFatture();
        }

        protected void ButtonDatiFamiliareNuovo_Click(object sender, EventArgs e)
        {
            ResetSelezioneFamiliare();
            PrestazioniDatiFamiliare1.ResetCampi();
            //PanelDatiFamiliareCampi.Enabled = true;
            PrestazioniDatiFamiliare1.StatoCampi(true);
            ViewState["Familiare"] = null;
        }

        protected void GridViewFatture_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                FatturaDichiarata fattura = (FatturaDichiarata)e.Row.DataItem;

                Label lTipoFattura = (Label)e.Row.FindControl("LabelTipoFattura");

                if (fattura.Saldo)
                    lTipoFattura.Text = "A saldo";
                else
                    lTipoFattura.Text = "Anticipo";
            }
        }

        protected void GridViewDocumentiRichiesti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Documento documento = (Documento)e.Row.DataItem;

                Label lBeneficiario = (Label)e.Row.FindControl("LabelBeneficiario");
                Label lStato = (Label)e.Row.FindControl("LabelStato");

                lBeneficiario.Text = biz.TrasformaGradoParentelaInDescrizione(documento.RiferitoA);

                if (!String.IsNullOrEmpty(documento.IdArchidoc)) //prima, quando idarch era int, il controllo era .HasValue
                {
                    if (documento.DataScansione.HasValue)
                    {
                        lStato.Text = String.Format("Presente in Cassa Edile. Ricevuto il {0}",
                                                    documento.DataScansione.Value.ToShortDateString());
                    }
                    else
                    {
                        lStato.Text = String.Format("Presente in Cassa Edile");
                    }
                }
                else
                {
                    lStato.Text = String.Format("Da allegare alla domanda");
                }
            }
        }

        protected void GridViewRiassuntoFatture_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                FatturaDichiarata fattura = (FatturaDichiarata)e.Row.DataItem;

                Label lTipoFattura = (Label)e.Row.FindControl("LabelTipoFattura");

                if (fattura.Saldo)
                    lTipoFattura.Text = "A saldo";
                else
                    lTipoFattura.Text = "Anticipo";
            }
        }

        protected void RadioButtonContoCorrente_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButtonContoCorrente.Checked)
            {
                PanelDatiComunicazioniTipoPagamentoContoCorrenteDettagli.Enabled = true;
                PanelDatiComunicazioniTipoPagamentoCartaPrepagataDettagli.Enabled = false;

                DropDownListDatiComunicazioniTipoCarta.SelectedIndex = 0;
            }
            else
            {
                //hanno richiesto di svuotare il dato nel caso di inserimento e poi selezione di Carta
                TextBoxDatiComunicazioniTipoPagamentoIBAN.Text = string.Empty;

                PanelDatiComunicazioniTipoPagamentoContoCorrenteDettagli.Enabled = false;
                PanelDatiComunicazioniTipoPagamentoContoCorrenteDettagli.Enabled = false;
                PanelDatiComunicazioniTipoPagamentoCartaPrepagataDettagli.Enabled = true;
            }
        }

        protected void ButtonSalvaTemporaneamente_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Domanda domanda = CreaDomanda();
                IntegraLavoratoreConIndirizzo();
                IntegraLavoratoreConComunicazioni();

                domanda.DataConferma = DateTime.Now;

                if (ViewState["IdDomandaTemporanea"] != null)
                    domanda.IdDomanda = (int)ViewState["IdDomandaTemporanea"];

                if (biz.InsertOrUpdateDomandaTemporanea(domanda))
                    Response.Redirect("~/CeServizi/Prestazioni/ConfermaSalvataggioTemporaneo.aspx");
            }
        }

        protected void DropDownListTipologiaScuola_SelectedIndexChanged(object sender, EventArgs e)
        {
            CaricaTipiPromozione(DropDownListTipologiaScuola.SelectedValue);
        }

        private void CaricaTipiPromozione(string idTipoScuola)
        {
            DropDownListTipologiaPromozione.Items.Clear();

            if (!string.IsNullOrEmpty(idTipoScuola))
            {
                TipoPromozioneCollection tipiPromozione = biz.GetTipiPromozione(Int32.Parse(idTipoScuola));
                Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipologiaPromozione, tipiPromozione,
                                                                   "Descrizione", "IdTipoPromozione");
            }
        }

        protected void DropDownListTipoPrestazioneTipoPrestazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            PrestazioniDatiFamiliare1.GestioneControlloCodiceFiscale(
                DropDownListTipoPrestazioneTipoPrestazione.SelectedValue != "C002");

            PrestazioniDatiFamiliare1.CaricaTipoPrestazione(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);
            //PrestazioniRicercaFamiliare1.CaricaTipoPrestazione(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);

            //if (!string.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue))
            //{
            //    //CaricaTipiPrestazione(biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue));
            //    if (isUtenteLavoratore)
            //        CaricaFamiliari(biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue));

            //    if (DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L")
            //    {
            //        PanelDatiFamiliare.Enabled = true;
            //        if (!isUtenteLavoratore)
            //        {
            //            PrestazioniDatiFamiliare1.StatoCampi(true);
            //        }
            //    }
            //    else
            //    {
            //        PanelDatiFamiliare.Enabled = false;
            //        ResetSelezioneFamiliare();
            //    }
            //}
            //else
            //{
            //    DropDownListTipoPrestazioneTipoPrestazione.Items.Clear();
            //    ResetSelezioneFamiliare();
            //}
        }

        #region Custom Validators

        protected void CustomValidatorDatiInizialiLavoratorePresenteAnagrafica_ServerValidate(object source,
                                                                                              ServerValidateEventArgs args)
        {
            // Questo validator viene utilizzato per i lavoratori non autenticati.
            // Viene verificata la presenza in anagrafica a parità di Codice fiscale, 
            // Cognome, Nome e Data di nascita

            DateTime dataNascita;
            if (!string.IsNullOrEmpty(TextBoxDatiInizialiDataNascita.Text)
                && DateTime.TryParse(TextBoxDatiInizialiDataNascita.Text, out dataNascita))
            {
                Lavoratore lavoratore = CreaLavoratore();

                if (biz.IsLavoratoreInAnagrafica(lavoratore))
                {
                    args.IsValid = true;

                    if (!string.IsNullOrEmpty(lavoratore.IdTipoPagamento))
                        trTipoPagamento.Visible = false;
                    PrestazioniDatiFamiliare1.CaricaIdLavoratore(lavoratore);
                }
                else
                    args.IsValid = false;
            }
        }

        protected void CustomValidatorCAP_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int iCAP;

            if (TextBoxDatiLavoratoreCap.Text.Length == 5 && Int32.TryParse(TextBoxDatiLavoratoreCap.Text, out iCAP))
            {
                string cap = TextBoxDatiLavoratoreCap.Text;

                string codiceComboComune = DropDownListDatiLavoratoreLocalita.SelectedValue;
                string[] codiceSplittato = codiceComboComune.Split('|');

                if (commonBiz.IsGrandeCitta(codiceSplittato[0]) && cap[2] == '1' && cap[3] == '0' && cap[4] == '0')
                    args.IsValid = false;
            }
        }

        protected void CustomValidatorTipoPagamento_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!RadioButtonCartaPrepagata.Checked && !RadioButtonContoCorrente.Checked)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorRequiredDatiComunicazioniTipoPagamentoIBAN_ServerValidate(object source,
                                                                                                ServerValidateEventArgs args)
        {
            if (RadioButtonContoCorrente.Checked && string.IsNullOrEmpty(TextBoxDatiComunicazioniTipoPagamentoIBAN.Text))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void RequiredDatiComunicazioniTipoPagamentoTipoCarta_ServerValidate(object source,
                                                                                      ServerValidateEventArgs args)
        {
            if (RadioButtonCartaPrepagata.Checked &&
                string.IsNullOrEmpty(DropDownListDatiComunicazioniTipoCarta.SelectedValue))
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CustomValidatorDatiComunicazioniTipoPagamentoIBAN_ServerValidate(object source,
                                                                                        ServerValidateEventArgs args)
        {
            string IBAN = TextBoxDatiComunicazioniTipoPagamentoIBAN.Text.Trim().ToUpper();
            bool ibanCorretto = false;

            try
            {
                ibanCorretto = IbanManager.VerificaCodiceIban(IBAN);
            }
            catch
            {
            }

            // Controllo che il codice IBAN sia corretto
            if (!string.IsNullOrEmpty(IBAN) && !ibanCorretto)
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        protected void CustomValidatorFatture_ServerValidate(object source, ServerValidateEventArgs args)
        {
            // Viene controllato che tutte le fatture a saldo facciano riferimento allo stesso anno
            bool res = true;

            if (ViewState["Fatture"] != null && ViewState["Configurazione"] != null)
            {
                FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
                Configurazione configurazione = (Configurazione)ViewState["Configurazione"];

                DateTime dataInizioPeriodo = DateTime.Now;
                DateTime dataFinePeriodo = DateTime.Now;
                bool dateInizializzate = false;

                for (int i = 0; i < fatture.Count; i++)
                {
                    if (fatture[i].Saldo)
                    {
                        if (!dateInizializzate)
                        {
                            // Calcolo del periodo di riferimento della prima fattura
                            dataInizioPeriodo = configurazione.PeriodoRiferimentoInizio.AddYears(fatture[0].Data.Year - 1900);
                            if (dataInizioPeriodo > fatture[0].Data)
                                dataInizioPeriodo = dataInizioPeriodo.AddYears(-1);

                            dataFinePeriodo = configurazione.PeriodoRiferimentoFine.AddYears(dataInizioPeriodo.Year - 1900);
                        }
                        else
                        {
                            if (fatture[i].Data < dataInizioPeriodo || fatture[i].Data > dataFinePeriodo)
                            {
                                res = false;
                                break;
                            }
                        }
                    }
                }
            }

            args.IsValid = res;
        }

        protected void CustomValidatorAlmenoUnaFattura_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = false;

            if (ViewState["Fatture"] != null)
            {
                FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];

                if (fatture.Count > 0)
                    args.IsValid = true;
            }
        }

        protected void CustomValidatorDatiComunicazioniCellulare_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (isUtenteLavoratore && String.IsNullOrWhiteSpace(TextBoxDatiComunicazioniCellulare.Text))
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorReddito_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!RadioButtonRedditoSi.Checked
                && !RadioButtonRedditoNo.Checked)
            {
                args.IsValid = false;
            }
        }

        protected void RequiredFieldValidatorAnnoScolastico_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (RadioButtonListAnnoScolastico.SelectedItem == null)
            {
                args.IsValid = false;
            }
        }
        #endregion

        #region Funzioni per il caricamento dei comuni e delle frazioni

        private static int IndiceComuneInDropDown(DropDownList dropDownListComuni, string codiceCatastale)
        {
            int res = 0;

            for (int i = 0; i < dropDownListComuni.Items.Count; i++)
            {
                string valoreCombo = dropDownListComuni.Items[i].Value;
                string[] splitted = valoreCombo.Split('|');
                if (splitted.Length > 1)
                {
                    if (splitted[0] == codiceCatastale)
                    {
                        res = i;
                        break;
                    }
                }
            }

            return res;
        }

        private static int IndiceFrazioneInDropDown(DropDownList dropDownListFrazioni, string frazione)
        {
            int res = 0;

            for (int i = 0; i < dropDownListFrazioni.Items.Count; i++)
            {
                string valoreCombo = dropDownListFrazioni.Items[i].Value;
                string[] splitted = valoreCombo.Split('|');
                if (splitted.Length > 1)
                {
                    if (splitted[0] == frazione)
                    {
                        res = i;
                        break;
                    }
                }
            }

            return res;
        }

        private void CaricaCapOFrazioni(DropDownList DropDownListLocalita, DropDownList DropDownListFrazione,
                                        TextBox textBoxCap)
        {
            string codiceComboComune = DropDownListLocalita.SelectedValue;
            string[] codiceSplittato = codiceComboComune.Split('|');

            if (codiceSplittato.Length > 1 && codiceSplittato[1] != null && codiceSplittato[1] != "0")
            {
                textBoxCap.Text = VerificaCapValido(codiceSplittato[0], codiceSplittato[1]);
            }
            else
            {
                textBoxCap.Text = string.Empty;
            }

            CaricaFrazioni(DropDownListFrazione, codiceSplittato[0]);
        }

        private void CaricaCap(string selectedValue, TextBox textBoxCap)
        {
            string[] codiceSplittato = selectedValue.Split('|');

            if (codiceSplittato.Length > 1 && codiceSplittato[1] != null && codiceSplittato[1] != "0")
                textBoxCap.Text = VerificaCapValido(codiceSplittato[0], codiceSplittato[1]);
        }

        private string VerificaCapValido(string codiceCatastale, string cap)
        {
            string retCap = cap;

            if (cap.Length == 5)
            {
                // Caso grande città
                if (commonBiz.IsGrandeCitta(codiceCatastale) && cap[2] == '1')
                {
                    if (cap[3] == '0' && cap[4] == '0')
                        retCap = retCap.Substring(0, 3);
                }
            }

            return retCap;
        }

        private void CaricaComuni(DropDownList dropDownListComuni, string provincia, DropDownList dropDownListFrazioni)
        {
            //svuotiamo e aggiumgiamo l'item vuoto
            dropDownListComuni.Items.Clear();
            dropDownListComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (dropDownListFrazioni != null)
                dropDownListFrazioni.Items.Clear();

            if (!string.IsNullOrEmpty(provincia))
            {
                List<ComuneSiceNew> comuni = commonBiz.GetComuniSiceNew(provincia);
                dropDownListComuni.DataSource = comuni;
                dropDownListComuni.DataTextField = "Comune";
                dropDownListComuni.DataValueField = "CodicePerCombo";
            }

            dropDownListComuni.DataBind();
        }

        private void CaricaFrazioni(DropDownList dropDownListFrazioni, string codiceCatastale)
        {
            dropDownListFrazioni.Items.Clear();
            dropDownListFrazioni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (!string.IsNullOrEmpty(codiceCatastale))
            {
                List<FrazioneSiceNew> frazioni = commonBiz.GetFrazioniSiceNew(codiceCatastale);
                dropDownListFrazioni.DataSource = frazioni;
                dropDownListFrazioni.DataTextField = "Frazione";
                dropDownListFrazioni.DataValueField = "CodicePerCombo";
            }

            dropDownListFrazioni.DataBind();
        }

        #endregion

        #region Funzioni per la gestione della selezione delle province, località, cap

        protected void DropDownListDatiLavoratoreProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxDatiLavoratoreCap.Text = string.Empty;
            CaricaComuni(DropDownListDatiLavoratoreLocalita,
                         DropDownListDatiLavoratoreProvincia.SelectedValue,
                         DropDownListDatiLavoratoreFrazione);
        }

        protected void DropDownListIstitutoScolasticoProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            CaricaComuni(DropDownListIstitutoScolasticoLocalita,
                         DropDownListIstitutoScolasticoProvincia.SelectedValue,
                         null);
        }

        protected void DropDownListDatiLavoratoreLocalita_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxDatiLavoratoreCap.Text = string.Empty;
            CaricaCapOFrazioni(DropDownListDatiLavoratoreLocalita,
                               DropDownListDatiLavoratoreFrazione,
                               TextBoxDatiLavoratoreCap);
        }

        protected void DropDownListDatiLavoratoreFrazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBoxDatiLavoratoreCap.Text = string.Empty;
            CaricaCap(DropDownListDatiLavoratoreFrazione.SelectedValue, TextBoxDatiLavoratoreCap);
        }

        #endregion

        #region Caricamento tipi dropdown

        private void CaricaTipiScuola(string idTipoPrestazione)
        {
            TipoScuolaCollection tipiScuola = biz.GetTipiScuola(idTipoPrestazione);
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipologiaScuola, tipiScuola, "Descrizione",
                                                               "IdTipoScuola");
        }

        private void CaricaTipiDecesso()
        {
            TipoDecessoCollection tipiDecesso = biz.GetTipiDecesso();
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListTipoDecesso, tipiDecesso, "Descrizione",
                                                               "IdTipoDecesso");
        }

        #endregion

        #region Cambio colore link della sidebar

        private void lst_ItemCreated(object sender, DataListItemEventArgs e)
        {
            // Per far funzionare il cambiamento di colore dei link della sidebar
            int indiceWizard = (int)ViewState["indiceWizard"];
            LinkButton button = (LinkButton)e.Item.FindControl("SideBarButton");

            if (e.Item.ItemIndex > indiceWizard)
                button.ForeColor = Color.LightGray;
        }

        #endregion
    }
}