﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ControlloDomandaDocumenti.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ControlloDomandaDocumenti" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc2" %>
<%@ Register Src="WebControls/PrestazioniDatiDomanda.ascx" TagName="PrestazioniDatiDomanda" TagPrefix="uc5" %>
<%@ Register Src="WebControls/PrestazioniAssociazioneDocumento.ascx" TagName="PrestazioniAssociazioneDocumento" TagPrefix="uc4" %>
<%@ Register Src="WebControls/PrestazioniRicercaDocumenti.ascx" TagName="PrestazioniRicercaDocumenti" TagPrefix="uc3" %>


<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc2:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Controllo domanda - Documenti"
        titolo="Prestazioni" />
    <br />
    <table class="standardTable">
        <tr>
            <td>
                <uc5:PrestazioniDatiDomanda ID="PrestazioniDatiDomanda1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label
                    ID="LabelErroreVisualizzazioneDocumento"
                    runat="server"
                    Text="Documento non trovato o errore durante la lettura da Archidoc"
                    Visible="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <b>
                    Documenti
                </b>
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridViewDocumenti" runat="server" AutoGenerateColumns="False" width="100%" DataKeyNames="PerPrestazione,RiferitoA,TipoDocumento,IdDocumento,IdArchidoc" OnRowCommand="GridViewDocumenti_RowCommand" OnRowDataBound="GridViewDocumenti_RowDataBound">
                    <EmptyDataTemplate>
                        Nessun documento mancante
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Presente" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Image ID="ImagePresente" runat="server" ImageUrl="../images/semaforoGiallo.png" />
                                <br />
                                <small><asp:Label ID="LabelDataScansione" runat="server"></asp:Label></small>
                            </ItemTemplate>
                            <ItemStyle Width="10px" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Documento" DataField="TipoDocumento" ItemStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="Relativo a" DataField="RiferitoA" />
                        <asp:BoundField DataField="LavoratoreCognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="LavoratoreNome" HeaderText="Nome" />
                        <asp:TemplateField HeaderText="Originale">
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBoxOriginale" runat="server" Enabled="False" 
                                    Visible="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" Text="Visualizza" CommandName="visualizza" >
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" Text="Ricerca" CommandName="ricerca" >
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                        <asp:ButtonField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" Text="Annulla" CommandName="annulla" >
                            <ItemStyle Width="10px" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:Panel ID="PanelControllaDomandaAzioni" runat="server">
        <asp:Button ID="ButtonAccettaDocumentazione" runat="server" Text="Accetta  la documentazione anche se mancante"
        Width="332px" OnClick="ButtonAccettaDocumentazione_Click" />
        <br />
        Se tutti i semafori sono verdi l&#39;accettazione è automatica
    </asp:Panel>
    <br />
    <asp:Button ID="ButtonIndietro"
            runat="server" OnClick="ButtonIndietro_Click" Text="Indietro" Width="200px" />
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MainPage2">
    <asp:Panel ID="PanelRicercaDocumenti" runat="server" Visible="false">
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Ricerca documenti"></asp:Label><br />
        <uc3:PrestazioniRicercaDocumenti ID="PrestazioniRicercaDocumenti1" runat="server" />
        
        <asp:Panel ID="PanelAssociazione" runat="server" Visible="false" >
            <br />
            <uc4:PrestazioniAssociazioneDocumento id="PrestazioniAssociazioneDocumento1" runat="server">
            </uc4:PrestazioniAssociazioneDocumento>
            
        </asp:Panel>
    </asp:Panel>
    <br />
</asp:Content>

