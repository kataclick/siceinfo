﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportPrestazioniSanitarie.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.ReportPrestazioniSanitarie" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Moduli"
        titolo="Prestazioni" />
    <rsweb:reportviewer id="ReportViewer" runat="server" height="600px"
        processingmode="Remote" width="100%" ShowDocumentMapButton="False" ShowFindControls="False" ShowRefreshButton="False" ShowZoomControl="False"></rsweb:reportviewer>
</asp:Content>