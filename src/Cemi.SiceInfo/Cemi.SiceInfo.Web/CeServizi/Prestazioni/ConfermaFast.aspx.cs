﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ConfermaFast : System.Web.UI.Page
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda);
            funzionalita.Add(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDomanda"] != null)
                {
                    Int32 idDomanda = (Int32)Context.Items["IdDomanda"];
                    ViewState["IdDomanda"] = Context.Items["IdDomanda"];
                    String idStato = (String)Context.Items["IdStato"];
                    ViewState["IdStato"] = Context.Items["IdStato"];

                    if (idStato == "R")
                    {
                        LabelRespinta.Visible = true;
                    }

                    //ReportViewer.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                    //if (idStato != "R")
                    //    ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportRicevutaFast";
                    //else if (idStato == "R")
                    //    ReportViewer.ServerReport.ReportPath = "/ReportPrestazioni/ReportRicevutaFastRifiuto";

                    //ReportParameter[] listaParam = new ReportParameter[3];
                    //listaParam[0] = new ReportParameter("idDomanda", idDomanda.ToString());
                    //listaParam[1] = new ReportParameter("filtraDaConsegnare", "1");
                    //listaParam[2] = new ReportParameter("operatore", "1");

                    //ReportViewer.ServerReport.SetParameters(listaParam);

                    // Download automatico ricevuta
                    //ClientScriptManager cs = Page.ClientScript;
                    //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "auto_postback", "window.onload = function() { " + cs.GetPostBackClientHyperlink(ButtonStampaRicevutaNascosto, "first", false) + "; }; ", true);


                    //Domanda domanda = biz.GetDomanda(idDomanda);
                    // Per la prima fase di test abilitiamo la stampa delle
                    // copertine solo per le C007 e C010
                    //if (domanda.IdTipoPrestazione == "C007-1"
                    //    || domanda.IdTipoPrestazione == "C007-2"
                    //    || domanda.IdTipoPrestazione == "C007-3"
                    //    || domanda.IdTipoPrestazione == "C010"
                    //    || domanda.IdTipoPrestazione == "C010-F")
                    //{
                    //    ButtonStampaCopertine.Visible = true;
                    //}
                    //else
                    //{
                    //    ButtonStampaCopertine.Visible = false;
                    //}
                }
                else
                {
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
                }
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/CeServizi/Prestazioni/CompilazioneDomandaFast.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            CaricaModulo();
        }

        protected void ButtonGestisci_Click(object sender, EventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];
            PrestazioniBusiness biz = new PrestazioniBusiness();
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();

            biz.UpdateDomandaSetPresaInCarico(idDomanda, idUtente, true);

            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomanda.aspx");
        }

        private void CaricaModulo()
        {
            //Il tipo modulo non è un parametro passato nel context, poichè la stampa del modulo non sarà frequente, è accettabile fare unq query per avere il dato
            PrestazioniBusiness biz = new PrestazioniBusiness();
            Domanda d = biz.GetDomanda((Int32)ViewState["IdDomanda"]);

            Context.Items["IdDomanda"] = d.IdDomanda;
            Context.Items["TipoModulo"] = d.TipoModulo.Modulo;

            Configurazione conf = biz.GetConfigurazionePrestazione(
                d.TipoPrestazione.IdTipoPrestazione,
                d.Beneficiario,
                d.Lavoratore.IdLavoratore.Value);

            //switch (d.TipoModulo.IdTipoModulo)
            switch (conf.TipoMacroPrestazione.IdTipoMacroPrestazione)
            {
                // Prestazioni Sanitarie
                case 1:
                    Server.Transfer("~/CeServizi/Prestazioni/ReportPrestazioniSanitarie.aspx");
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                    Server.Transfer("~/CeServizi/Prestazioni/ReportPrestazioniScolastiche.aspx");
                    break;
            }
        }

        private void CaricaRicevuta()
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Context.Items["IdStato"] = ViewState["IdStato"];
            Server.Transfer("~/CeServizi/Prestazioni/ReportRicevutaFast.aspx");
        }

        protected void ButtonStampaRicevuta_Click(object sender, EventArgs e)
        {
            CaricaRicevuta();
        }

        protected void ButtonStampaRicevutaNascosto_Click(object sender, EventArgs e)
        {
            CaricaRicevuta();
        }

        protected void ButtonStampaCopertine_Click(object sender, EventArgs e)
        {
            Int32 idDomanda = (Int32)ViewState["IdDomanda"];
            Context.Items["IdDomanda"] = idDomanda;

            Server.Transfer("~/CeServizi/Prestazioni/StampaCopertine.aspx?sorgente=conferma");
        }
    }
}