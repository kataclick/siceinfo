﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class GestioneRicevuta : System.Web.UI.Page
    {
        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();
            funzionalita.Add(FunzionalitaPredefinite.PrestazioniGestioneDomande);
            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion

            if (!Page.IsPostBack)
            {
                //Nel caso in cui il contesto non fosse presente, torniamo alla pagina di ricerca
                if (Context.Items["IdDomanda"] == null)
                {
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
                }

                ViewState["IdDomanda"] = Context.Items["IdDomanda"];
                Domanda domanda = biz.GetDomanda((int)ViewState["IdDomanda"]);

                Int32? idFamiliare = null;

                if (domanda.Familiare != null)
                    idFamiliare = domanda.Familiare.IdFamiliare.Value;

                PrestazioniSelezionaDocumentiRicevuta1.CaricaDocumenti(domanda.IdTipoPrestazione,
                                                                       domanda.Beneficiario,
                                                                       domanda.Lavoratore.IdLavoratore.Value,
                                                                       idFamiliare, true, domanda.IdDomanda.Value);

                PrestazioniDatiDomanda1.CaricaDomanda(domanda);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonSalvaStampa_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];
            biz.InserisciDocumentiConsegnati(idDomanda,
                                             PrestazioniSelezionaDocumentiRicevuta1.GetDocumenti(idDomanda));

            Context.Items["IdDomanda"] = idDomanda;
            Domanda d = biz.GetDomanda((int)ViewState["IdDomanda"]);

            //Context.Items["IdDomanda"] = d.IdDomanda.Value;
            Context.Items["IdStato"] = d.Stato.IdStato;
            Server.Transfer("~/CeServizi/Prestazioni/ReportRicevutaFast.aspx");
        }

        /// <summary>
        /// Ritorniamo al controllodomanda
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAnnulla_Click(object sender, EventArgs e)
        {
            Context.Items["IdDomanda"] = ViewState["IdDomanda"];
            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomanda.aspx");
        }
    }
}