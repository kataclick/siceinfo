﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class PrestazioniDefault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.PrestazioniCompilazioneDomanda);
            funzionalita.Add(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);

            #endregion
        }
    }
}