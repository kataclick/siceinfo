﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Archidoc;
using TBridge.Cemi.Business.Archidoc.Interfaces;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Prestazioni.Business;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Delegates.Prestazioni;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class ControlloDomandaDocumenti : System.Web.UI.Page
    {
        private const int INDICEANNULLA = 8;
        private const int INDICEORIGINALE = 5;
        private const int INDICERICERCA = 7;
        private const int INDICEVISUALIZZA = 6;

        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniGestioneDomande);

            #endregion

            PrestazioniRicercaDocumenti1.OnDocumentoSelected += PrestazioniRicercaDocumenti1_OnDocumentoSelected;
            PrestazioniAssociazioneDocumento1.OnAssociazioneEffettuata +=
                new AssociazioneEffettuataEventHandler(PrestazioniAssociazioneDocumento1_OnAssociazioneEffettuata);

            if (!Page.IsPostBack)
            {
                if (Context.Items["IdDomanda"] != null)
                {
                    int idDomanda = (int)Context.Items["IdDomanda"];
                    ViewState["IdDomanda"] = idDomanda;

                    Domanda domanda = biz.GetDomanda(idDomanda);
                    ViewState["stato"] = domanda.Stato.IdStato;

                    //Verifichiamo se la domanda può essere gestita
                    ViewState.Add("permettiGestioneDomanda", AbilitaGestione(domanda));

                    CaricaDocumenti(domanda);

                    ControllaStatoDomanda(domanda);

                    ViewState.Add("stato", domanda.Stato.IdStato);

                    //Verifichiamo se la domanda può essere gestita
                    //TODO non è detto che qui serva
                    ViewState.Add("permettiGestioneDomanda", AbilitaGestione(domanda));
                }
                else
                {
                    Server.Transfer("~/CeServizi/Prestazioni/GestioneDomande.aspx");
                }
            }
        }

        /// <summary>
        /// Abilita le azioni che può fare l'utente in funzione del ruolo e della proprietà della domanda 
        /// </summary>
        /// <param name="domanda">Domanda in gestione</param>
        private bool AbilitaGestione(Domanda domanda)
        {
            //IUtente utente = ApplicationInstance.GetUtenteSistema();
            Int32 idUtente = GestioneUtentiBiz.GetIdUtente();
            //Se l'utente è chi ha la domanda in carico o ha funzioni amministrative, può getire la domanda
            if (idUtente == domanda.IdUtenteInCarico ||
                GestioneUtentiBiz.Autorizzato(
                    FunzionalitaPredefinite.PrestazioniGestioneDomandeAdmin.ToString()))
            {
                //Tutto normale
                return true;
            }
            else
            {
                //Tutto viene disabilitato
                PanelControllaDomandaAzioni.Enabled = false;
                //GridViewDocumenti.Enabled = false; 
                //Non va bene i documenti devono poter essere visualizzabili, quindi non si può disabilitare tutto il gridview
                //in alternativa viene usato il ViewState["permettiGestioneDomanda"].
                return false;
            }
        }

        /// <summary>
        /// in funzione dello stato della domanda decidiamo quali azioni lasciare all'utente5
        /// </summary>
        /// <param name="domanda"></param>
        private void ControllaStatoDomanda(Domanda domanda)
        {
            if (StatoDomanda.StatoDomandaModificabile(domanda.Stato.IdStato))
            //domanda.Stato.IdStato == "I" || domanda.Stato.IdStato == "T" || domanda.Stato.IdStato == "O"
            {
                PanelControllaDomandaAzioni.Enabled = true;
            }
            else
            {
                PanelControllaDomandaAzioni.Enabled = false;
            }
        }

        private void CaricaDocumenti(Domanda domanda)
        {
            DocumentoCollection documentiNecessari = biz.GetDocumentiNecessariPiuRecenti(domanda.IdDomanda.Value);

            if (documentiNecessari.Count == 0)
                ButtonAccettaDocumentazione.Enabled = false;

            if (domanda.ControlloPresenzaDocumenti.HasValue && domanda.ControlloPresenzaDocumenti.Value)
                ButtonAccettaDocumentazione.Enabled = false;

            GridViewDocumenti.DataSource = documentiNecessari;
            GridViewDocumenti.DataBind();

            PrestazioniDatiDomanda1.CaricaDomanda(domanda);
        }

        protected void ButtonAccettaDocumentazione_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];
            bool ok = biz.ForzaPresenzaDocumenti(idDomanda);

            Domanda domanda = biz.GetDomanda(idDomanda);

            biz.ControllaDomandaDocumenti(domanda);

            CaricaDocumenti(domanda);
        }

        protected void GridViewDocumenti_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Documento documento = (Documento)e.Row.DataItem;

                Image iPresente = (Image)e.Row.FindControl("ImagePresente");
                Label lDataScansione = (Label)e.Row.FindControl("LabelDataScansione");
                CheckBox checkBoxOriginale = (CheckBox)e.Row.FindControl("CheckBoxOriginale");

                //se il documento ha un valore "Originale" visualizziamo il templatefield e il valore
                if (documento.Originale.HasValue)
                {
                    checkBoxOriginale.Visible = true;
                    checkBoxOriginale.Checked = documento.Originale.Value;
                }

                if (documento.DataScansione.HasValue)
                {
                    lDataScansione.Text = documento.DataScansione.Value.ToString("dd/MM/yyyy");
                }

                //Prima disabilitiamo tutti i tasti poi in funzione delle variabili modifichiamo
                e.Row.Cells[INDICEVISUALIZZA].Enabled = false;
                e.Row.Cells[INDICEANNULLA].Enabled = false;
                e.Row.Cells[INDICERICERCA].Enabled = false;

                //Se il documento richiesto non ha un documento associato permetttiamo l'associazione
                if (string.IsNullOrEmpty(documento.IdArchidoc))
                {
                    e.Row.Cells[INDICERICERCA].Enabled = true;
                }
                else
                {
                    //altrimenti permettiamo la visualizzazione e l'annullamento dell'associazione
                    e.Row.Cells[INDICEVISUALIZZA].Enabled = true;
                    e.Row.Cells[INDICEANNULLA].Enabled = true;
                }

                //se la domanda è già stata accolta o l'utente che sta visualizzando la pagina non ha in carico la domanda 
                //permettiamo solo la visualizzazione
                //Partiamo dal risultato degli stati dei bottoni precedente ed applichiamo altre regole più restrittive
                if (StatoDomanda.StatoDomandaModificabile((string)ViewState["stato"])
                    && (bool)ViewState["permettiGestioneDomanda"])
                //(string)ViewState["stato"] == "I" || (string)ViewState["stato"] == "T" || (string)ViewState["stato"] == "O")
                {
                    //Se la domanda non è in uno stato definito lasciamo tutto com'è
                }
                else
                {
                    //disabilitiamo ricerca e annullamento del documento
                    e.Row.Cells[INDICERICERCA].Enabled = false;
                    e.Row.Cells[INDICEANNULLA].Enabled = false;
                    //la visualizzazione rimane condizionata dalla presenza dell'idarchidoc
                }

                //Gestiamo il semaforo del documento
                iPresente.ImageUrl = biz.ConvertiBoolInSemaforo(!string.IsNullOrEmpty(documento.IdArchidoc));
                //prima, quando idarch era int, il controllo era .HasValue;
            }
        }

        protected void GridViewDocumenti_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            LabelErroreVisualizzazioneDocumento.Visible = false;
            int indice = Int32.Parse(e.CommandArgument.ToString());
            bool perPrestazione = (bool)GridViewDocumenti.DataKeys[indice].Values["PerPrestazione"];
            string relativoA = (string)GridViewDocumenti.DataKeys[indice].Values["RiferitoA"];
            TipoDocumento tipoDocumento = (TipoDocumento)GridViewDocumenti.DataKeys[indice].Values["TipoDocumento"];

            switch (e.CommandName)
            {
                case "visualizza":
                    string idArchidoc = (string)GridViewDocumenti.DataKeys[indice].Values["IdArchidoc"];
                    // Caricare il documento tramite il Web Service Archidoc
                    try
                    {
                        IArchidocService servizioArchidoc = ArchidocConnector.GetIstance();
                        byte[] file = servizioArchidoc.GetDocument(idArchidoc);
                        if (file != null)
                        {
                            Presenter.RestituisciFileArchidoc(idArchidoc, file, this);
                        }
                    }
                    catch
                    {
                        LabelErroreVisualizzazioneDocumento.Visible = true;
                    }
                    break;
                case "ricerca":
                    PanelRicercaDocumenti.Visible = true;
                    PrestazioniRicercaDocumenti1.PreImpostaTipoDocumento(tipoDocumento, perPrestazione, relativoA);

                    PrestazioniAssociazioneDocumento1.ResetCampiAssociazione();
                    PanelAssociazione.Visible = false;
                    break;
                case "annulla":
                    int idDocumento = (int)GridViewDocumenti.DataKeys[indice].Values["IdDocumento"];

                    biz.DisassociaDocumento(tipoDocumento.IdTipoDocumento, idDocumento);
                    int idDomanda = (int)ViewState["IdDomanda"];
                    Domanda domanda = biz.GetDomanda(idDomanda);

                    biz.ControllaDomandaDocumenti(domanda);
                    CaricaDocumenti(domanda);

                    //Notifichiamo al CRM un cambio nella documentazione (in questo caso disassociazione di un documento)
                    //le eccezioni del WS non vengono gestire a questo livello
                    if (!Presenter.IsSviluppo)
                    {
                        biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                    }
                    break;
            }
        }

        protected void ButtonIndietro_Click(object sender, EventArgs e)
        {
            int idDomanda = (int)ViewState["IdDomanda"];
            Context.Items["IdDomanda"] = idDomanda;

            Server.Transfer("~/CeServizi/Prestazioni/ControlloDomanda.aspx");
        }

        private void PrestazioniRicercaDocumenti1_OnDocumentoSelected(Documento documento)
        {
            PanelAssociazione.Visible = true;
            int idDomanda = (int)ViewState["IdDomanda"];

            Domanda domanda = biz.GetDomanda(idDomanda);
            PrestazioniAssociazioneDocumento1.ImpostaAssociazione(documento, domanda);
        }

        private void PrestazioniAssociazioneDocumento1_OnAssociazioneEffettuata()
        {
            int idDomanda = (int)ViewState["IdDomanda"];
            Domanda domanda = biz.GetDomanda(idDomanda);

            PanelRicercaDocumenti.Visible = false;
            biz.ControllaDomandaDocumenti(domanda);
            CaricaDocumenti(domanda);
        }
    }
}