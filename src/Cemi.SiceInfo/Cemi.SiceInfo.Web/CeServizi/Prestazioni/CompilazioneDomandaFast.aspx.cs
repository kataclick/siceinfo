﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using TBridge.Cemi.GestioneUtenti.Business;
using TBridge.Cemi.Prestazioni.Business;
using System.Drawing;
using TBridge.Cemi.Business;
using System.Collections.Specialized;
using Cemi.SiceInfo.Web.Helpers;
using TBridge.Cemi.Business.Prestazioni;
using TBridge.Cemi.Type.Collections;
using TBridge.Cemi.Type.Collections.Prestazioni;
using TBridge.Cemi.Type.Delegates.Prestazioni;
using TBridge.Cemi.Type.Entities;
using TBridge.Cemi.Type.Entities.Prestazioni;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Enums.Prestazioni;
using TBridge.Cemi.Type.Exceptions.Prestazioni;

namespace Cemi.SiceInfo.Web.CeServizi.Prestazioni
{
    public partial class CompilazioneDomandaFast : System.Web.UI.Page
    {
        private const Int32 INDICENUOVOFAMILIARE = 1;
        private const Int32 INDICENUOVOLAVORATORE = 1;
        private const Int32 INDICESELEZIONEFAMILIARE = 0;
        private const Int32 INDICESELEZIONELAVORATORE = 0;

        private readonly PrestazioniBusiness biz = new PrestazioniBusiness();
        private readonly Common commonBiz = new Common();

        private Int32 idUtente;

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autenticazione

            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.PrestazioniCompilazioneDomandaFast);

            idUtente = GestioneUtentiBiz.GetIdUtente();

            #endregion

            #region Eventi dei controlli custom

            PrestazioniRicercaLavoratore1.OnLavoratoreSelected += PrestazioniRicercaLavoratore1_OnLavoratoreSelected;
            PrestazioniRicercaLavoratore1.OnLavoratoreNuovo += PrestazioniRicercaLavoratore1_OnLavoratoreNuovo;

            PrestazioniRicercaFamiliare1.OnFamiliareSelected +=
                new FamiliareSelectedEventHandler(PrestazioniRicercaFamiliare1_OnFamiliareSelected);

            #endregion

            if (!Page.IsPostBack)
            {
                ViewState["Fatture"] = new FatturaDichiarataCollection();
                ViewState["GuidId"] = Guid.NewGuid();
                ViewState["paginaValidaFatture"] = 0;
                ViewState["paginaValidaFamiliare"] = true;
                TextBoxDataDomanda.Text = DateTime.Now.ToShortDateString();

                CaricaCausali();
                CaricaProvince();
                CaricaAnniScolastici();
            }

            PrestazioniDatiFattura1.ImpostaValidatorPerDataFattura(24);

            Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
            if (configurazione != null)
            { 
                if ((configurazione.IdTipoPrestazione == "C011-1") || (configurazione.IdTipoPrestazione == "C011-2"))
                {
                    ViewState["SoggiornoEstivo"] = true;
                }
                else
                {
                    ViewState["SoggiornoEstivo"] = false;
                }
            }
        }

        private void CaricaProvince()
        {
            StringCollection province = commonBiz.GetProvinceSiceNew();
            Presenter.CaricaElementiInDropDownConElementoVuoto(DropDownListIstitutoScolasticoProvincia, province, null, null);
        }

        private void CaricaCausali()
        {
            TipoCausaleCollection tipiCausale = biz.GetTipiCausaliFast();

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListCausaleRifiuto,
                tipiCausale,
                "Descrizione",
                "IdTipoCausale");
        }

        private void CaricaAnniScolastici()
        {
            DateTime adesso = DateTime.Now;
            Int32 annoCorr = adesso.Year;

            if (adesso.Month < 6)
            {
                ListItem liAnno1 = new ListItem(String.Format("{0}/{1}", annoCorr - 1, annoCorr), "1");
                RadioButtonListAnnoScolastico.Items.Add(liAnno1);
            }

            ListItem liAnno2 = new ListItem(String.Format("{0}/{1}", annoCorr, annoCorr + 1), "1");
            RadioButtonListAnnoScolastico.Items.Add(liAnno2);

            if (RadioButtonListAnnoScolastico.Items.Count == 1)
            {
                RadioButtonListAnnoScolastico.Items[0].Selected = true;
            }
        }

        protected void ButtonSelezionaLavoratore_Click(object sender, EventArgs e)
        {
            MultiViewLavoratore.Visible = true;
            MultiViewLavoratore.ActiveViewIndex = INDICESELEZIONELAVORATORE;
            ButtonSelezionaLavoratore.Enabled = false;
        }

        protected void ButtonSelezionaFamiliare_Click(object sender, EventArgs e)
        {
            LabelFamiliareTroppoVecchio.Visible = false;
            MultiViewFamiliare.Visible = true;
            MultiViewFamiliare.ActiveViewIndex = INDICESELEZIONEFAMILIARE;
            ButtonSelezionaFamiliare.Enabled = false;
        }

        protected void ButtonNuovoLavoratore_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Lavoratore lavoratore = PrestazioniDatiNuovoLavoratore1.GetLavoratore();

                SelezionaLavoratore(lavoratore);
                PrestazioniDatiNuovoLavoratore1.ResetCampi();
            }
        }

        private void PrestazioniRicercaLavoratore1_OnLavoratoreNuovo()
        {
            MultiViewLavoratore.ActiveViewIndex = INDICENUOVOLAVORATORE;
        }

        private void PrestazioniRicercaLavoratore1_OnLavoratoreSelected(Lavoratore lavoratore)
        {
            SelezionaLavoratore(lavoratore);
            CaricaFamiliari(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);
        }

        private void PrestazioniRicercaFamiliare1_OnFamiliareSelected(Familiare familiare)
        {
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue != "" && DropDownListTipoPrestazioneTipoPrestazione.SelectedValue != null)
            {
                //Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
                //DateTime dataDomanda = DateTime.Parse(TextBoxDataDomanda.Text);

                //if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "PRENAT"
                //    && familiare.DataNascita < dataDomanda.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda)
                //    && !LabelFamiliareTroppoVecchio.Visible)
                //{
                //    LabelFamiliareTroppoVecchio.Visible = true;
                //}
                //else
                //{
                //    SelezionaFamiliare(familiare);
                //    CaricaDocumentiRichiesti();
                //    LabelFamiliareTroppoVecchio.Visible = false;
                //}


                if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C011-1" || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C011-2")
                {
                    if (familiare.DataNascita.HasValue)
                    {
                        ViewState["paginaValidaFamiliare"] = true;
                        TextBoxFamiliare.ForeColor = Color.Empty;

                        if (familiare.DataNascita.Value.Year < (DateTime.Now.Year - 18) || familiare.DataNascita.Value.Year > (DateTime.Now.Year - 5))
                        {
                            LabelFamiliareTroppoVecchio.Text = $"La data di nascita del familiare deve essere compresa tra 01/01/{DateTime.Now.Year - 18} e 31/12/{DateTime.Now.Year - 5}";
                            LabelFamiliareTroppoVecchio.Visible = true;
                            TextBoxFamiliare.ForeColor = Color.Red;
                            ViewState["paginaValidaFamiliare"] = false;
                        }                      
                        SelezionaFamiliare(familiare);
                        CaricaDocumentiRichiesti();                        
                    }
                }
                else
                {
                    SelezionaFamiliare(familiare);
                    CaricaDocumentiRichiesti();
                    LabelFamiliareTroppoVecchio.Visible = false;
                    TextBoxFamiliare.ForeColor = Color.Empty;
                }
            }
            else
            {
                LabelFamiliareTroppoVecchio.Text = $"Selezionare una tipologia prestazione.";
                LabelFamiliareTroppoVecchio.Visible = true;
            }
        }

        private void CaricaFamiliari(string idPrestazione)
        {
            if (ViewState["Lavoratore"] != null &&
                !String.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue)
                && DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L")
            {
                Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];

                FamiliareCollection familiari =
                    biz.GetFamiliariPerGradoParentela(
                        lavoratore.IdLavoratore.Value,
                        biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue),
                        idPrestazione == "PRENAT" ? true : false);
                PrestazioniRicercaFamiliare1.CaricaFamiliari(familiari);

                PrestazioniRicercaFamiliare1.Visible = true;
            }
        }

        private void SelezionaLavoratore(Lavoratore lavoratore)
        {
            ViewState["Lavoratore"] = lavoratore;

            MultiViewLavoratore.Visible = false;
            ButtonSelezionaLavoratore.Enabled = true;

            CaricaDocumentiRichiesti();

            if (lavoratore != null)
            {
                TextBoxLavoratore.Text =
                    String.Format("{0} - {1} {2}\nData nascita: {3}\nCF: {4}\n{5}\nCell: {6}\nEMail: {7}\nTipo Pagamento: {8}",
                                  lavoratore.IdLavoratore,
                                  lavoratore.Cognome,
                                  lavoratore.Nome,
                                  lavoratore.DataNascita.ToShortDateString(),
                                  lavoratore.CodiceFiscale,
                                  lavoratore.Indirizzo,
                                  lavoratore.Cellulare,
                                  lavoratore.Email,
                                  lavoratore.IdTipoPagamento);
            }
            else
            {
                TextBoxLavoratore.Text = String.Empty;
            }
        }

        private void SelezionaFamiliare(Familiare familiare)
        {
            ViewState["Familiare"] = familiare;

            MultiViewFamiliare.Visible = false;
            ButtonSelezionaFamiliare.Enabled = true;

            if (familiare != null)
            {
                TextBoxFamiliare.Text = String.Format("{0} {1}\n{2}",
                                                      familiare.Cognome,
                                                      familiare.Nome,
                                                      familiare.DataNascita.HasValue
                                                          ? familiare.DataNascita.Value.ToShortDateString()
                                                          : String.Empty);
            }
            else
            {
                TextBoxFamiliare.Text = String.Empty;
            }
        }

        protected void DropDownListTipoPrestazioneBeneficiario_SelectedIndexChanged(object sender, EventArgs e)
        {
            // In base al beneficiario carico le prestazioni corrispondenti e l'eventuale
            // selezione/inserimento del familiare

            if (!string.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue))
            {
                // Il grado di parentela Genitori conviventi è solo di facciata ma si comporta
                // come i parenti affini conviventi
                CaricaTipiPrestazione(biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue));
                CaricaFamiliari(null);
            }
            else
            {
                DropDownListTipoPrestazioneTipoPrestazione.Items.Clear();
            }

            ImpostaSelezioneFamiliare();
        }

        private void CaricaTipiPrestazione(string beneficiario)
        {
            //richiediamo tutte le prestazioni, non solo quelle attive nel periodo.
            TipoPrestazioneCollection tipiPrestazione = biz.GetTipiPrestazionePerGradoParentela(beneficiario, false);

            Presenter.CaricaElementiInDropDownConElementoVuoto(
                DropDownListTipoPrestazioneTipoPrestazione,
                tipiPrestazione,
                "Descrizione",
                "IdTipoPrestazione"
                );
        }

        protected void DropDownListTipoPrestazioneTipoPrestazione_SelectedIndexChanged(object sender, EventArgs e)
        {
            Configurazione configurazione = null;

            if (!String.IsNullOrEmpty(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue))
            {
                configurazione =
                    biz.GetConfigurazionePrestazione(
                        DropDownListTipoPrestazioneTipoPrestazione.SelectedValue,
                        biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue),
                        -1);
                TipoPrestazione tipoPrestazione =
                    biz.GetTipoPrestazione(biz.TraduciBeneficiariReali(DropDownListTipoPrestazioneBeneficiario.SelectedValue),
                                           DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);

                if (tipoPrestazione != null && tipoPrestazione.ValidaDa.HasValue && tipoPrestazione.ValidaA.HasValue)
                    LabelValiditaPrestazione.Text = "Periodo validità: " + tipoPrestazione.ValidaDa.Value.ToString("dd/MM") +
                                                    " - " +
                                                    tipoPrestazione.ValidaA.Value.ToString("dd/MM");
                else LabelValiditaPrestazione.Text = string.Empty;

                //richiediamo tutte le prestazioni, non solo quelle attive nel periodo.
                TipoCausaleCollection causali = biz.GetTipiCausali(true, DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);

                Presenter.CaricaElementiInDropDownConElementoVuoto(
                    DropDownListCausaleRifiuto,
                    causali,
                    "Descrizione",
                    "IdTipoCausale"
                    );
            }
            else
            {
                LabelValiditaPrestazione.Text = string.Empty;
            }

            ImpostaConfigurazione(configurazione);
            CaricaFamiliari(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue);
        }

        private void ImpostaConfigurazione(Configurazione configurazione)
        {
            ViewState["Configurazione"] = configurazione;

            if (configurazione != null)
            {
                // Abilitazione dati aggiuntivi
                if (configurazione.RichiestaFattura)
                {
                    PanelDatiAggiuntivi.Enabled = true;
                }
                else
                {
                    PanelDatiAggiuntivi.Enabled = false;
                    ResetDatiAggiuntivi();
                }

                PrestazioniDatiFattura1.ImpostaValidatorPerDataFattura(configurazione.DifferenzaMesiFatturaDomanda);
                if ((configurazione.IdTipoPrestazione == "C011-1") || (configurazione.IdTipoPrestazione == "C011-2"))
                {
                    ViewState["SoggiornoEstivo"] = true;
                }
                else
                {
                    ViewState["SoggiornoEstivo"] = false;
                }

                CaricaDocumentiRichiesti();

                if ((configurazione.IdTipoPrestazione == "C003-1" || configurazione.IdTipoPrestazione == "C003-2" || configurazione.IdTipoPrestazione == "C003-3")
                    && configurazione.GradoParentela != "L")
                {
                    PanelScolasticheUnico.Visible = true;
                }
                else
                {
                    PanelScolasticheUnico.Visible = false;
                }

                if (configurazione.IdTipoPrestazione == "C006")
                {
                    PanelScolasticheMedie.Visible = true;
                }
                else
                {
                    PanelScolasticheMedie.Visible = false;
                }
            }
        }

        private void CaricaDocumentiRichiesti()
        {
            if (!String.IsNullOrEmpty(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue)
                && !String.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue)
                )
            {
                String idTipoPrestazione = DropDownListTipoPrestazioneTipoPrestazione.SelectedValue;
                String beneficiario = DropDownListTipoPrestazioneBeneficiario.SelectedValue;
                Int32 idLavoratore = -1;
                Int32? idFamiliare = null;

                if (ViewState["Lavoratore"] != null)
                {
                    Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];
                    if (lavoratore.IdLavoratore.HasValue)
                    {
                        idLavoratore = lavoratore.IdLavoratore.Value;
                    }
                }

                if (ViewState["Familiare"] != null)
                {
                    Familiare familiare = (Familiare)ViewState["Familiare"];
                    idFamiliare = familiare.IdFamiliare;
                }
                PrestazioniSelezionaDocumentiRicevuta1.CaricaDocumenti(
                    idTipoPrestazione, biz.TraduciBeneficiariReali(beneficiario), idLavoratore, idFamiliare, true, null);
            }
        }

        private void ImpostaSelezioneFamiliare()
        {
            // Abilitazione selezione familiare
            if (DropDownListTipoPrestazioneBeneficiario.SelectedValue == "L")
            {
                PanelFamiliare.Enabled = false;
                PrestazioniRicercaFamiliare1.Visible = false;
                ResetFamiliare();
            }
            else
            {
                PanelFamiliare.Enabled = true;
                PrestazioniRicercaFamiliare1.Visible = true;
            }
        }

        private void ResetFamiliare()
        {
            PrestazioniRicercaFamiliare1.Visible = false;
        }

        private void ResetDatiAggiuntivi()
        {
            TextBoxNumeroFatture.Text = null;
        }

        protected void ButtonNuovoFamiliare_Click(object sender, EventArgs e)
        {
            LabelFamiliareTroppoVecchio.Visible = false;
            MultiViewFamiliare.ActiveViewIndex = INDICENUOVOFAMILIARE;
        }

        protected void ButtonSalvaFamiliare_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Familiare familiare = PrestazioniDatiNuovoFamiliare1.GetFamiliare();

                Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
                DateTime dataDomanda = DateTime.Parse(TextBoxDataDomanda.Text);

                if ((DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C011-1" || DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "C011-2")
                    && (familiare.DataNascita.Value.Year < (DateTime.Now.Year - 18) || familiare.DataNascita.Value.Year > (DateTime.Now.Year - 5)))
                {
                    //LabelFamiliareTroppoVecchio.Visible = true;
                    TextBoxFamiliare.ForeColor = Color.Red;
                    ViewState["paginaValidaFamiliare"] = false;
                    SelezionaFamiliare(familiare);
                    PrestazioniDatiNuovoLavoratore1.ResetCampi();

                }
                else
                { 
                    if (DropDownListTipoPrestazioneTipoPrestazione.SelectedValue == "PRENAT"
                        && familiare.DataNascita < dataDomanda.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda)
                        && !LabelFamiliareTroppoVecchio.Visible)
                    {
                        LabelFamiliareTroppoVecchio.Visible = true;
                    }
                    else
                    {
                        SelezionaFamiliare(familiare);
                        PrestazioniDatiNuovoLavoratore1.ResetCampi();
                        LabelFamiliareTroppoVecchio.Visible = false;
                        TextBoxFamiliare.ForeColor = Color.Empty;
                        ViewState["paginaValidaFamiliare"] = true;
                    }
                }
            }
        }


        protected void CustomValidatorLavoratore_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ViewState["Lavoratore"] != null)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorFamiliare_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;

            if (!String.IsNullOrEmpty(DropDownListTipoPrestazioneBeneficiario.SelectedValue)
                && DropDownListTipoPrestazioneBeneficiario.SelectedValue != "L"
                && ViewState["Familiare"] == null)
            {
                args.IsValid = false;
            }
        }

        protected void CustomValidatorReddito_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!RadioButtonRedditoSi.Checked
                && !RadioButtonRedditoNo.Checked)
            {
                args.IsValid = false;
            }
        }

        protected void ButtonInserisciPrestazione_Click(object sender, EventArgs e)
        {
            if (Page.IsValid && (bool)ViewState["paginaValidaFamiliare"] == true && (int)ViewState["paginaValidaFatture"] == 0)
            {
                Domanda domanda = CreaDomanda();
                domanda.Confermata = true;

                try
                {
                    if (biz.InsertDomanda(domanda, String.Empty, String.Empty, String.Empty, null, idUtente))
                    {
                        //Notifichiamo al CRM l'inserimento di una prestazione 
                        //le eccezioni del WS non vengono gestire a questo livello
                        if (!Presenter.IsSviluppo)
                        {
                            biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                        }

                        ResetCampi();

                        Context.Items["IdDomanda"] = domanda.IdDomanda;
                        Context.Items["IdStato"] = domanda.Stato.IdStato;
                        Server.Transfer("~/CeServizi/Prestazioni/ConfermaFast.aspx");
                    }
                }
                catch (DomandaGiaInseritaException)
                {
                    LabelGiaInserita.Visible = true;
                }
            }
        }

        protected void ButtonRifiutaPrestazione_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Page.Validate("inserimentoDomanda");

                if (Page.IsValid)
                {
                    Domanda domanda = CreaDomanda();
                    domanda.Confermata = true;
                    domanda.Stato = new StatoDomanda("R");
                    domanda.TipoCausale = new TipoCausale(DropDownListCausaleRifiuto.SelectedValue);

                    if (biz.InsertDomanda(domanda, String.Empty, String.Empty, String.Empty, null, idUtente))
                    {
                        //Notifichiamo al CRM l'inserimento di una prestazione 
                        //le eccezioni del WS non vengono gestire a questo livello
                        if (!Presenter.IsSviluppo)
                        {
                            biz.CallCrmWsPrestazioni(domanda.IdDomanda.Value);
                        }

                        ResetCampi();

                        Context.Items["IdDomanda"] = domanda.IdDomanda;
                        Context.Items["IdStato"] = domanda.Stato.IdStato;
                        //Server.Transfer("~/Prestazioni/ReportRicevutaFast.aspx");
                        Server.Transfer("~/CeServizi/Prestazioni/ConfermaFast.aspx");
                    }
                }
            }
        }

        private void ResetCampi()
        {
            ViewState["Lavoratore"] = null;
            SelezionaLavoratore(null);

            ViewState["Familiare"] = null;
            SelezionaFamiliare(null);

            Presenter.SvuotaCampo(TextBoxNumeroFatture);
            DropDownListTipoPrestazioneBeneficiario.SelectedIndex = 0;
            DropDownListTipoPrestazioneTipoPrestazione.Items.Clear();
            PrestazioniSelezionaDocumentiRicevuta1.CaricaDocumentiListaVuota();

            TextBoxDataDomanda.Text = DateTime.Now.ToShortDateString();
        }

        private Domanda CreaDomanda()
        {
            Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
            Domanda domanda = new Domanda();

            domanda.Lavoratore = (Lavoratore)ViewState["Lavoratore"];
            if (ViewState["Familiare"] != null)
                domanda.Familiare = (Familiare)ViewState["Familiare"];
            domanda.FattureDichiarate = (FatturaDichiarataCollection)ViewState["Fatture"];
            domanda.CasseEdili = PrestazioniSelezioneCasseEdili1.GetCasseEdili();
            //new TBridge.Cemi.Type.Collections.CassaEdileCollection();
            domanda.Lavoratore.Comunicazioni = new Comunicazioni();
            domanda.DataDomanda = DateTime.Parse(TextBoxDataDomanda.Text);
            domanda.Documenti =
                PrestazioniSelezionaDocumentiRicevuta1.GetDocumenti(DropDownListTipoPrestazioneTipoPrestazione.SelectedValue
                                                                    , DropDownListTipoPrestazioneBeneficiario.SelectedValue);
            domanda.TipoInserimento = TipoInserimento.Fast;

            if (!String.IsNullOrEmpty(TextBoxNumeroFatture.Text))
            {
                domanda.NumeroFatture = Int32.Parse(TextBoxNumeroFatture.Text);
            }
            else if (domanda.FattureDichiarate != null && domanda.FattureDichiarate.Count > 0)
            {
                domanda.NumeroFatture = domanda.FattureDichiarate.Count;
            }

            domanda.TipoPrestazione = new TipoPrestazione();
            if (DropDownListTipoPrestazioneTipoPrestazione.SelectedItem != null)
            {
                domanda.TipoPrestazione.IdTipoPrestazione = DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Value;
                domanda.TipoPrestazione.Descrizione = DropDownListTipoPrestazioneTipoPrestazione.SelectedItem.Text;
            }

            // Il grado di parentela Genitori conviventi è solo di facciata ma si comporta
            // come i parenti affini conviventi
            if (DropDownListTipoPrestazioneBeneficiario.SelectedValue == "O")
            {
                domanda.Beneficiario = "A";
                domanda.GenitoriConviventi = true;
            }
            else
            {
                domanda.Beneficiario = DropDownListTipoPrestazioneBeneficiario.SelectedValue;
            }
            domanda.Stato = new StatoDomanda("I");

            domanda.Guid = (Guid)ViewState["GuidId"];

            //DateTime dataTemp = new DateTime();
            //se è richiesta la fattura, la data min o max è la data evento e quindi la data di riferimento della prestazione
            if (configurazione != null && configurazione.RichiestaFattura)
            {
                //// Imposto come data di riferimento della domanda la data della prima fattura a saldo caricata
                //foreach (FatturaDichiarata fattura in domanda.FattureDichiarate)
                //{
                //    if (fattura.Saldo && (domanda.DataRiferimento == dataTemp || domanda.DataRiferimento > fattura.Data))
                //    {
                //        domanda.DataRiferimento = fattura.Data;
                //    }
                //}

                if (domanda.FattureDichiarate.Count > 0)
                {
                    //Nel caso di asilo nido prendiamo l'ultima data fattura
                    if (domanda.TipoPrestazione.IdTipoPrestazione == "C-ANID")
                    {
                        //siamo sicuri che almeno una fattura è valorizzata quindi possiamo prendere il valore del tipo nullable senza controlli
                        domanda.DataRiferimento = domanda.FattureDichiarate.MaxDataFattura.Value;
                    }
                    // per tutte le altre prendiamo la data fattura minima
                    else
                    {
                        domanda.DataRiferimento = domanda.FattureDichiarate.MinDataFattura.Value;
                    }
                }
                else domanda.DataRiferimento = domanda.DataDomanda.Value;
            }
            //se la domanda è di tipo scolastico c006 c003-1 la data di riferimento è il primo ottobre, il rolling poi genera la "data calcolo" portandola indietro di un anno
            else if
                (
                domanda.TipoPrestazione.IdTipoPrestazione == "C006" ||
                domanda.TipoPrestazione.IdTipoPrestazione == "C003-1"
                )
            {
                //mettiamo ottobre di un anno ipotetico, poi l'operatore selezione l'anno corretto ed il sistema di generazione del rolling porta indietro di un mese
                //if (DateTime.Now.Month >= 10 && domanda.TipoPrestazione.IdTipoPrestazione == "C006")
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year,10,1);
                //else
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year-1, 10, 1);

                //if (DateTime.Now.Month >= 6 && DateTime.Now.Month <= 12 && domanda.TipoPrestazione.IdTipoPrestazione == "C003-1")
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year-1, 10, 1);
                //else
                //    domanda.DataRiferimento = new DateTime(DateTime.Now.Year - 2, 10, 1);

                //inutile fare tante ipotesi tanto devono per forza definire l'anno di frequenza, in base a quello viene calcolato l'inizio anno o il fine anno scolastico
                //domanda.DataRiferimento = DateTime.Now;
                domanda.DataRiferimento = domanda.DataDomanda.Value;
            }
            else
            {
                if (domanda.TipoPrestazione.IdTipoPrestazione == "PRENAT")
                {
                    domanda.DataRiferimento = domanda.Familiare.DataNascita.Value;
                }
                else
                {
                    // Imposto come data di riferimento la data di inserimento della domanda
                    //domanda.DataRiferimento = DateTime.Now;
                    domanda.DataRiferimento = domanda.DataDomanda.Value;
                }
            }

            if ((domanda.TipoPrestazione.IdTipoPrestazione == "C003-1"
                || domanda.TipoPrestazione.IdTipoPrestazione == "C003-2"
                || domanda.TipoPrestazione.IdTipoPrestazione == "C003-3")
                && domanda.Beneficiario != "L")
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                domanda.DatiAggiuntiviScolastiche.RedditoSi = RadioButtonRedditoSi.Checked;
            }

            if (domanda.TipoPrestazione.IdTipoPrestazione == "C006")
            {
                if (domanda.DatiAggiuntiviScolastiche == null)
                    domanda.DatiAggiuntiviScolastiche = new DatiScolastiche();

                if (RadioButtonListAnnoScolastico.SelectedItem != null)
                {
                    domanda.DatiAggiuntiviScolastiche.AnnoScolastico = RadioButtonListAnnoScolastico.SelectedItem.Text;
                }
                if (!String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxIstitutoScolastico.Text)))
                {
                    domanda.DatiAggiuntiviScolastiche.IstitutoDenominazione = Presenter.NormalizzaCampoTesto(TextBoxIstitutoScolastico.Text);
                }
                if (!String.IsNullOrEmpty(Presenter.NormalizzaCampoTesto(TextBoxIstitutoScolasticoIndirizzo.Text)))
                {
                    domanda.DatiAggiuntiviScolastiche.IstitutoIndirizzo = Presenter.NormalizzaCampoTesto(TextBoxIstitutoScolasticoIndirizzo.Text);
                }
                if (!String.IsNullOrEmpty(DropDownListIstitutoScolasticoProvincia.SelectedValue))
                {
                    domanda.DatiAggiuntiviScolastiche.IstitutoProvincia = DropDownListIstitutoScolasticoProvincia.SelectedValue;
                }
                if (DropDownListIstitutoScolasticoLocalita.SelectedItem != null)
                {
                    domanda.DatiAggiuntiviScolastiche.IstitutoLocalita = DropDownListIstitutoScolasticoLocalita.SelectedItem.Text;
                }
            }

            return domanda;
        }


        protected void ButtonFattureNuovaInserisci_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                FatturaDichiarata fattura = CreaFattura();
                Lavoratore lavoratore = (Lavoratore)ViewState["Lavoratore"];

                if (lavoratore != null && !biz.FatturaGiaUtilizzata(fattura, lavoratore.IdLavoratore.Value))
                {
                    LabelFatturaGiaPresente.Visible = false;
                    FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];

                    if (!fatture.IsGiaPresente(fattura))
                    {
                        Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
                        DateTime dataDomanda = DateTime.Parse(TextBoxDataDomanda.Text);

                        if (configurazione.IdTipoPrestazione != "C011-1" && configurazione.IdTipoPrestazione != "C011-2")
                        {
                            if (fattura.Data < dataDomanda.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda)
                            && !LabelFatturaTroppoVecchia.Visible)
                            {
                                LabelFatturaTroppoVecchia.Visible = true;
                            }
                            else
                            {
                                fatture.Add(fattura);
                                ViewState["Fatture"] = fatture;

                                ResetNuovaFattura();
                                CaricaFatture();

                                LabelFatturaGiaCaricata.Visible = false;
                                LabelFatturaGiaPresente.Visible = false;
                                LabelFatturaTroppoVecchia.Visible = false;
                                LabelFatturaNonCorrettaGenerico.Visible = false;
                            }
                        }
                        else
                        {
                            LabelFatturaGiaCaricata.Visible = false;
                            LabelFatturaGiaPresente.Visible = false;
                            LabelFatturaTroppoVecchia.Visible = false;
                            LabelFatturaNonCorrettaGenerico.Visible = false;

                            if (fattura.Data.Year != DateTime.Now.Year)
                            {
                                LabelFatturaNonCorrettaGenerico.Text = $"La data della fattura deve essere compresa tra 01/01/{DateTime.Now.Year} e 31/12/{DateTime.Now.Year}.";
                                LabelFatturaNonCorrettaGenerico.Visible = true;
                                ViewState["paginaValidaFatture"] = (int)ViewState["paginaValidaFatture"] + 1;
                            }
                            
                            fatture.Add(fattura);
                            ViewState["Fatture"] = fatture;

                            ResetNuovaFattura();
                            CaricaFatture();

                            //LabelFatturaGiaCaricata.Visible = false;
                            //LabelFatturaGiaPresente.Visible = false;
                            //LabelFatturaTroppoVecchia.Visible = false;
                            //LabelFatturaNonCorrettaGenerico.Visible = false;
                            
                        }
                        /*
                        if (fattura.Data < dataDomanda.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda)
                            && !LabelFatturaTroppoVecchia.Visible)
                        {
                            LabelFatturaTroppoVecchia.Visible = true;
                        }
                        else
                        {
                            fatture.Add(fattura);
                            ViewState["Fatture"] = fatture;

                            ResetNuovaFattura();
                            CaricaFatture();

                            LabelFatturaGiaCaricata.Visible = false;
                            LabelFatturaGiaPresente.Visible = false;
                            LabelFatturaTroppoVecchia.Visible = false;
                        }
                        */
                    }
                    else
                    {
                        LabelFatturaGiaCaricata.Visible = true;
                    }
                }
                else
                {
                    LabelFatturaGiaPresente.Visible = true;
                }
            }
        }

        private void ResetNuovaFattura()
        {
            PrestazioniDatiFattura1.ResetCampi();
        }

        private FatturaDichiarata CreaFattura()
        {
            FatturaDichiarata fattura = PrestazioniDatiFattura1.CreaFattura();
            return fattura;
        }

        protected void GridViewFatture_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Configurazione configurazione = (Configurazione)ViewState["Configurazione"];
                FatturaDichiarata fattura = (FatturaDichiarata)e.Row.DataItem;
                DateTime dataDomanda = DateTime.Parse(TextBoxDataDomanda.Text);

                if (configurazione.IdTipoPrestazione != "C011-1" && configurazione.IdTipoPrestazione != "C011-2")
                {
                    if (fattura.Data < dataDomanda.AddMonths(-configurazione.DifferenzaMesiFatturaDomanda))
                    {
                        e.Row.ForeColor = Color.Red;
                    }
                }
                else
                {
                    if (fattura.Data.Year != DateTime.Now.Year)
                    {
                        e.Row.ForeColor = Color.Red;
                    }
                }

                Label lTipoFattura = (Label)e.Row.FindControl("LabelTipoFattura");

                if (fattura.Saldo)
                    lTipoFattura.Text = "A saldo";
                else
                    lTipoFattura.Text = "Anticipo";
            }
        }

        protected void GridViewFatture_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            FatturaDichiarataCollection fatture = (FatturaDichiarataCollection)ViewState["Fatture"];
            if (fatture[e.RowIndex].Data.Year != DateTime.Now.Year && (int)ViewState["paginaValidaFatture"] > 0)
            {
                ViewState["paginaValidaFatture"] = (int)ViewState["paginaValidaFatture"] - 1;
            }
            fatture.RemoveAt(e.RowIndex);
            ViewState["Fatture"] = fatture;

            CaricaFatture();
        }

        private void CaricaFatture()
        {
            Presenter.CaricaElementiInGridView(
                GridViewFatture,
                (FatturaDichiarataCollection)ViewState["Fatture"],
                0);
        }

        protected void CustomValidatorPrestazioneSelezionata_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ViewState["Configurazione"] == null)
            {
                args.IsValid = false;
            }
        }

        protected void DropDownListIstitutoScolasticoProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            CaricaComuni(DropDownListIstitutoScolasticoLocalita,
                         DropDownListIstitutoScolasticoProvincia.SelectedValue,
                         null);
        }

        private void CaricaComuni(DropDownList dropDownListComuni, string provincia, DropDownList dropDownListFrazioni)
        {
            //svuotiamo e aggiumgiamo l'item vuoto
            dropDownListComuni.Items.Clear();
            dropDownListComuni.Items.Add(new ListItem(string.Empty, string.Empty));

            if (dropDownListFrazioni != null)
                dropDownListFrazioni.Items.Clear();

            if (!string.IsNullOrEmpty(provincia))
            {
                List<ComuneSiceNew> comuni = commonBiz.GetComuniSiceNew(provincia);
                dropDownListComuni.DataSource = comuni;
                dropDownListComuni.DataTextField = "Comune";
                dropDownListComuni.DataValueField = "CodicePerCombo";
            }

            dropDownListComuni.DataBind();
        }
    }
}