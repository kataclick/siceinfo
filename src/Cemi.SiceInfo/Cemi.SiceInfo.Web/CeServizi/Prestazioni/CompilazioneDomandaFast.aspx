﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="CompilazioneDomandaFast.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.Prestazioni.CompilazioneDomandaFast" %>

<%@ Register Src="../WebControls/MenuPrestazioni.ascx" TagName="MenuPrestazioni" TagPrefix="uc1" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%@ Register Src="WebControls/PrestazioniRicercaLavoratore.ascx" TagName="PrestazioniRicercaLavoratore" TagPrefix="uc3" %>
<%@ Register Src="WebControls/PrestazioniDatiNuovoLavoratore.ascx" TagName="PrestazioniDatiNuovoLavoratore" TagPrefix="uc4" %>
<%@ Register Src="WebControls/PrestazioniRicercaFamiliare.ascx" TagName="PrestazioniRicercaFamiliare" TagPrefix="uc5" %>
<%@ Register Src="WebControls/PrestazioniDatiFamiliare.ascx" TagName="PrestazioniDatiFamiliare" TagPrefix="uc6" %>
<%@ Register Src="WebControls/PrestazioniDatiNuovoFamiliare.ascx" TagName="PrestazioniDatiNuovoFamiliare" TagPrefix="uc7" %>
<%@ Register Src="WebControls/PrestazioniDatiFattura.ascx" TagName="PrestazioniDatiFattura" TagPrefix="uc8" %>
<%@ Register Src="WebControls/PrestazioniSelezioneCasseEdili.ascx" TagName="PrestazioniSelezioneCasseEdili" TagPrefix="uc9" %>
<%@ Register Src="WebControls/PrestazioniSelezionaDocumentiRicevuta.ascx" TagName="PrestazioniSelezionaDocumentiRicevuta" TagPrefix="uc10" %>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    <uc1:MenuPrestazioni ID="MenuPrestazioni1" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Prestazioni"
        sottoTitolo="Compilazione domanda Fast" />
    <br />
    <div>
        <table class="standardTable">
            <tr>
                <td>
                    Data domanda:
                    <asp:TextBox ID="TextBoxDataDomanda" runat="server" MaxLength="10" Width="110px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CompareValidator ID="CompareValidatorDataDomanda" runat="server" ValidationGroup="inserimentoDomanda"
                        ControlToValidate="TextBoxDataDomanda" Type="Date" Operator="DataTypeCheck" ErrorMessage="Formato data domanda errato">*</asp:CompareValidator>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td>
                    Lavoratore
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    Lavoratore selezionato
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLavoratore" runat="server" TextMode="MultiLine" Width="439px"
                        Height="105px" Enabled="False"></asp:TextBox>
                    <asp:Button ID="ButtonSelezionaLavoratore" runat="server" Text="Seleziona lavoratore"
                        Width="144px" Enabled="False" OnClick="ButtonSelezionaLavoratore_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:MultiView ID="MultiViewLavoratore" runat="server" ActiveViewIndex="0">
                        <asp:View ID="ViewLavoratoreSelezione" runat="server">
                            <uc3:PrestazioniRicercaLavoratore ID="PrestazioniRicercaLavoratore1" runat="server" />
                        </asp:View>
                        <asp:View ID="ViewLavoratoreNuovo" runat="server">
                            <uc4:PrestazioniDatiNuovoLavoratore ID="PrestazioniDatiNuovoLavoratore1" runat="server" />
                            <asp:ValidationSummary ID="ValidationSummaryDatiLavoratore" runat="server" CssClass="messaggiErrore"
                                ValidationGroup="datiLavoratore" />
                            <asp:Button ID="ButtonNuovoLavoratore" runat="server" Text="Salva lavoratore" Width="200px"
                                ValidationGroup="datiLavoratore" OnClick="ButtonNuovoLavoratore_Click" />
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="White" Text="Prestazione"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    Per chi viene richiesta la prestazione?:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTipoPrestazioneBeneficiario" runat="server" Width="450px"
                        AutoPostBack="True" OnSelectedIndexChanged="DropDownListTipoPrestazioneBeneficiario_SelectedIndexChanged">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="L">Per il lavoratore</asp:ListItem>
                        <asp:ListItem Value="C">Per la moglie</asp:ListItem>
                        <asp:ListItem Value="F">Per il figlio/a</asp:ListItem>
                        <asp:ListItem Value="O">Genitori conviventi</asp:ListItem>
                        <asp:ListItem Value="G">Genitori non conviventi</asp:ListItem>
                        <asp:ListItem Value="A">Parenti affini fino al 2° grado</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    (in caso di contributo per spese funerarie, indicare il grado di parentela della
                    persona deceduta)
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoPrestazioneBeneficiario"
                        runat="server" ControlToValidate="DropDownListTipoPrestazioneBeneficiario" ErrorMessage="Beneficiario non selezionato"
                        ValidationGroup="stop">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Tipo prestazione:
                </td>
                <td>
                    <asp:DropDownList ID="DropDownListTipoPrestazioneTipoPrestazione" runat="server"
                        Width="450px" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="DropDownListTipoPrestazioneTipoPrestazione_SelectedIndexChanged">
                    </asp:DropDownList>
                    <br />
                    <asp:Label ID="LabelValiditaPrestazione" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoPrestazioneTipoPrestazione"
                        runat="server" ControlToValidate="DropDownListTipoPrestazioneTipoPrestazione"
                        ErrorMessage="Tipologia di prestazione non selezionata" ValidationGroup="stop">*</asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="LabelCE" runat="server" Font-Bold="True" ForeColor="White" Text="Casse edili"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td colspan="2">
                    <asp:Panel ID="PanelDatiCasseEdili" runat="server">
                        <uc9:PrestazioniSelezioneCasseEdili ID="PrestazioniSelezioneCasseEdili1" runat="server" />
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <asp:Panel ID="PanelFamiliare" runat="server" Enabled="false">
            <table class="filledtable">
                <tr>
                    <td style="height: 16px">
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="White" Text="Familiare"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td style="width: 100px">
                        Familiare selezionato
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxFamiliare" runat="server" TextMode="MultiLine" Width="400px"
                            Height="80px" Enabled="False"></asp:TextBox>
                        <asp:Button ID="ButtonSelezionaFamiliare" runat="server" Text="Seleziona familiare"
                            Width="200px" Enabled="False" OnClick="ButtonSelezionaFamiliare_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:MultiView ID="MultiViewFamiliare" runat="server" ActiveViewIndex="0">
                            <asp:View ID="ViewFamiliareSelezionato" runat="server">
                                <asp:Button ID="ButtonNuovoFamiliare" runat="server" Width="150px" OnClick="ButtonNuovoFamiliare_Click"
                                    Text="Nuovo" />
                                <uc5:PrestazioniRicercaFamiliare ID="PrestazioniRicercaFamiliare1" runat="server" />
                            </asp:View>
                            <asp:View ID="ViewFamiliareNuovo" runat="server">
                                <uc7:PrestazioniDatiNuovoFamiliare ID="PrestazioniDatiNuovoFamiliare1" runat="server" />
                                <asp:ValidationSummary ID="ValidationSummaryFamiliare" runat="server" CssClass="messaggiErrore"
                                    ValidationGroup="datiFamiliare" />
                                <asp:Button ID="ButtonSalvaFamiliare" runat="server" Text="Salva familiare" Width="200px"
                                    ValidationGroup="datiFamiliare" OnClick="ButtonSalvaFamiliare_Click" />
                                &nbsp;
                                
                            </asp:View>
                        </asp:MultiView>
                        <asp:Label ID="LabelFamiliareTroppoVecchio" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <div>
        <asp:Panel ID="PanelDatiAggiuntivi" runat="server" Enabled="false">
            <table class="filledtable">
                <tr>
                    <td style="height: 16px">
                        <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="White" Text="Dati aggiuntivi"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td colspan="3">
                        Le fatture possono essere inserite indicandone solamente quantità o dichiarate singolarmente.
                    </td>
                </tr>
                <tr>
                    <td>
                        Quantità fatture:
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxNumeroFatture" runat="server" MaxLength="2" Width="150px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RangeValidator ID="RangeValidatorNumeroFatture" runat="server" ControlToValidate="TextBoxNumeroFatture"
                            MinimumValue="0" MaximumValue="99" ErrorMessage="Formato numero fatture non valido">*</asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="PanelFatture" runat="server" Visible="true">
                            <table class="standardTable">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelTitoloListaFatture" runat="server" Font-Bold="true" Text="Fatture caricate"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelFattureNuova" runat="server">
                                            <table class="standardTable">
                                                <tr>
                                                    <td colspan="2">
                                                        <uc8:PrestazioniDatiFattura ID="PrestazioniDatiFattura1" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LabelFatturaGiaPresente" runat="server" ForeColor="Red" Text="La fattura &#232; gi&#224; stata utilizzata in un'altra domanda. Non &#232; possibile caricarla."
                                                            Visible="False"></asp:Label>
                                                        <asp:Label ID="LabelFatturaGiaCaricata" runat="server" ForeColor="Red" Text="La fattura &#232; gi&#224; stata caricata."
                                                            Visible="False"></asp:Label>
                                                        <asp:Label ID="LabelFatturaNonCorrettaGenerico" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                                                        <asp:Label ID="LabelFatturaTroppoVecchia" runat="server" ForeColor="Red" Text="La fattura &#232; troppo antecedente la data della domanda. Per caricarla lo stesso premere nuovamente il pulsante &quot;Aggiungi la fattura alla lista&quot;."
                                                            Visible="False"></asp:Label>
                                                        <asp:CustomValidator 
                                                            ID="CustomValidatorPrestazioneSelezionata" 
                                                            runat="server" 
                                                            ErrorMessage="Selezionare prima una tipologia di prestazione"
                                                            ValidationGroup="nuovaFattura" 
                                                            onservervalidate="CustomValidatorPrestazioneSelezionata_ServerValidate">&nbsp;</asp:CustomValidator>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Button ID="ButtonFattureNuovaInserisci" runat="server" Text="Aggiungi la fattura alla lista"
                                                            ValidationGroup="nuovaFattura" OnClick="ButtonFattureNuovaInserisci_Click" Width="200px" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:ValidationSummary ID="ValidationSummaryFattureNuova" runat="server" ValidationGroup="nuovaFattura"
                                                            CssClass="messaggiErrore" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridViewFatture" runat="server" AutoGenerateColumns="False" Width="100%"
                                            OnRowDeleting="GridViewFatture_RowDeleting" OnRowDataBound="GridViewFatture_RowDataBound">
                                            <Columns>
                                                <asp:BoundField DataField="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Data">
                                                    <ItemStyle Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Numero" HeaderText="Numero" />
                                                <asp:BoundField DataField="ImportoTotale" HeaderText="Importo" />
                                                <asp:TemplateField HeaderText="Tipo" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelTipoFattura" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" />
                                                </asp:TemplateField>
                                                <asp:CommandField ControlStyle-CssClass="bottoneGriglia" ButtonType="Button" DeleteText="Rimuovi"
                                                    ShowDeleteButton="True">
                                                    <ItemStyle Width="10px" />
                                                </asp:CommandField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                Nessuna fattura caricata<br />
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PanelLenti" runat="server" Visible="false">
                                            <table class="standardTable">
                                                <tr>
                                                    <td style="width: 150px">
                                                        Importo delle lenti:
                                                    </td>
                                                    <td style="width: 203px">
                                                        <asp:TextBox ID="TextBoxImportoLenti" runat="server" MaxLength="6"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:CompareValidator ID="CompareValidatorImportoLenti" runat="server" ControlToValidate="TextBoxImportoLenti"
                                                            ErrorMessage="Formato dell'importo non valido" Operator="DataTypeCheck" Type="Currency"
                                                            ValidationGroup="stop">*</asp:CompareValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <div>
        <asp:Panel ID="PanelScolasticheMedie" runat="server" Visible="false">
            <table class="filledtable">
                <tr>
                    <td style="height: 16px">
                        <asp:Label ID="LabelScolastiche1" runat="server" Font-Bold="True" ForeColor="White" Text="Dati scolastiche"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td>
                        Anno scolastico:
                    </td>
                    <td>
                        <asp:RadioButtonList
                            ID="RadioButtonListAnnoScolastico"
                            runat="server">
                        </asp:RadioButtonList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Denominazione istituto scolastico
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxIstitutoScolastico" runat="server" MaxLength="50"
                            Width="350px">
                        </asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Indirizzo
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxIstitutoScolasticoIndirizzo" runat="server" MaxLength="50"
                            Width="350px">
                        </asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Provincia:
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListIstitutoScolasticoProvincia" runat="server" Width="350px"
                            AppendDataBoundItems="True" AutoPostBack="True" 
                            OnSelectedIndexChanged="DropDownListIstitutoScolasticoProvincia_SelectedIndexChanged"  />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Località:
                    </td>
                    <td>
                        <asp:DropDownList ID="DropDownListIstitutoScolasticoLocalita" runat="server" Width="350px"
                            AppendDataBoundItems="True" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorIstitutoScolasticoLocalita" runat="server"
                            ErrorMessage="Localit&#224; non selezionata" ControlToValidate="DropDownListIstitutoScolasticoLocalita"
                            ValidationGroup="stop">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="PanelScolasticheUnico" runat="server" Visible="false">
            <table class="filledtable">
                <tr>
                    <td style="height: 16px">
                        <asp:Label ID="LabelScolastiche2" runat="server" Font-Bold="True" ForeColor="White" Text="Dati scolastiche"></asp:Label>
                    </td>
                </tr>
            </table>
            <table class="borderedTable">
                <tr>
                    <td>
                        <b>
                            Dichiarazione dello studente ai fini delle detrazioni di imposta spettanti (art. 22, D.P.R. 29/09/1973, n. 600 e s.m.i.)
                        </b>
                    </td>
                </tr>
                <tr>
                    <td>
                        Lo studente dichiara di aver diritto per l'anno in corso alle ALTRE DETRAZIONI (art.13 TUIR)
                        <asp:CustomValidator 
                                ID="CustomValidatorReddito" 
                                runat="server" 
                                ErrorMessage="Selezionare se si ha diritto o meno alle detrazioni"
                                ValidationGroup="inserimentoDomanda" 
                            OnServerValidate="CustomValidatorReddito_ServerValidate">
                                *
                            </asp:CustomValidator>:
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton
                            ID="RadioButtonRedditoSi"
                            runat="server"
                            GroupName="reddito"
                            Text="Sì" />
                        <br />
                        <asp:RadioButton
                            ID="RadioButtonRedditoNo"
                            runat="server"
                            GroupName="reddito"
                            Text="No" />
                    </td>
                </tr>
                <tr>
                    <td>
                        (barrare "Sì" nel caso in cui lo studente NON abbia rapporti di lavoro o di collaborazione nell'anno di presentazione della domanda; in caso contrario barrare "No").
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <br />
    <div>
        <table class="filledtable">
            <tr>
                <td style="height: 16px">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="White" Text="Documenti richiesti"></asp:Label>
                </td>
            </tr>
        </table>
        <table class="borderedTable">
            <tr>
                <td>
                    <uc10:PrestazioniSelezionaDocumentiRicevuta ID="PrestazioniSelezionaDocumentiRicevuta1"
                        runat="server" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorBeneficiario" runat="server"
            ControlToValidate="DropDownListTipoPrestazioneBeneficiario" ErrorMessage="Selezionare il beneficiario"
            ValidationGroup="inserimentoDomanda">&nbsp;</asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorTipoPrestazione" runat="server"
            ControlToValidate="DropDownListTipoPrestazioneTipoPrestazione" ErrorMessage="Selezionare il tipo di prestazione"
            ValidationGroup="inserimentoDomanda">&nbsp;</asp:RequiredFieldValidator>
        <asp:CustomValidator ID="CustomValidatorLavoratore" runat="server" ErrorMessage="Lavoratore non selezionato"
            ValidationGroup="inserimentoDomanda" OnServerValidate="CustomValidatorLavoratore_ServerValidate">&nbsp;</asp:CustomValidator>
        <asp:CustomValidator ID="CustomValidatorFamiliare" runat="server" ErrorMessage="Familiare non selezionato"
            ValidationGroup="inserimentoDomanda" OnServerValidate="CustomValidatorFamiliare_ServerValidate">&nbsp;</asp:CustomValidator>
        <asp:ValidationSummary ID="ValidationSummaryIns" runat="server" ValidationGroup="inserimentoDomanda"
            CssClass="messaggiErrore" />
        <asp:ValidationSummary ID="ValidationSummaryResp" runat="server" ValidationGroup="respingimentoDomanda"
            CssClass="messaggiErrore" />
        <table class="standardTable">
            <tr>
                <td colspan="2">
                    Se i dati sono corretti la domanda può essere inserita come Immessa premendo:<br />
                    <asp:Button ID="Button1" runat="server" Text="Inserisci prestazione" ValidationGroup="inserimentoDomanda"
                        Width="200px" OnClick="ButtonInserisciPrestazione_Click" />
                    <asp:Label
                        ID="LabelGiaInserita"
                        runat="server"
                        ForeColor="Red"
                        Text="La domanda è già stata inserita"
                        Visible="false">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Nel caso la domanda non sia corretta, selezionare prima la causale:<br />
                    <asp:DropDownList ID="DropDownListCausaleRifiuto" runat="server" Width="300px" AppendDataBoundItems="True"
                        ValidationGroup="respingimentoDomanda">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="DropDownListCausaleRifiuto"
                        ErrorMessage="Impostare la causale" ValidationGroup="respingimentoDomanda">*</asp:RequiredFieldValidator>
                    <br />
                    e poi premere il tasto "Respingi":<br />
                    <asp:Button ID="ButtonRifiutaPrestazione" runat="server" Text="Respingi prestazione"
                        ValidationGroup="respingimentoDomanda" Width="200px" OnClick="ButtonRifiutaPrestazione_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
