﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="InformativaSindacatiLavoratoriCemiDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.InformativaSindacati.InformativaSindacatiLavoratoriAttiviDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Download Elenco Lavoratori Cemi"
        titolo="InfoSindacati" />
    <br />
    <p>In questa pagina è possibile scaricare l'archivio contentente la base dati InfoSindacati con i lavoratori CEMI (presenti in denuncia negli ultimi 5 anni).</p>
    <p>
    <asp:Label ID="LabelUltimoFile" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownload" runat="server" OnClick="ButtonDownload_Click" Text="Scarica" />
    </p>
    <p>
    Nel caso in cui, soprattutto su vecchi sistemi operativi, il programma non dovesse funzionare, mettiamo a disposizione la precedente versione dell'applicativo (Con i dati aggiornati).
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="ButtonOldDownload_Click" Text="Scarica vecchia versione" />
    </p>
    
</asp:Content>
