﻿using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Linq;
using System.IO;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Collections.GestioneUtenti;


namespace Cemi.SiceInfo.Web.CeServizi.InformativaSindacati
{    
    public partial class InformativaSindacatiLavoratoriAttiviDefault : System.Web.UI.Page
    {
        GestioneUtentiBiz gu = new GestioneUtentiBiz();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DownloadLavoratoriAttivi);
            #endregion

            if (!Page.IsPostBack)
            {
                ButtonDownload.Enabled = false;

                try
                {                    
                    FileInfo file = new FileInfo(ConfigurationManager.AppSettings["InfoSindacatiDownload"]+"LavoratoriCemi.zip");
                    if (file.Exists)
                    {
                        LabelUltimoFile.Text = String.Format("File aggiornato al: <b>{0}</b>", file.LastWriteTime.ToShortDateString());
                        ButtonDownload.Enabled = true;
                    }
                    else
                    {
                        LabelUltimoFile.Text = "File non disponibile";
                    }
                }
                catch (Exception exc)
                {
                    LabelUltimoFile.Text = String.Format("Errore durante il recupero del file: {0}", exc.Message);
                }
            }
        }

        


        protected void ButtonDownload_Click(object sender, EventArgs e)
        {
            string filePath = ConfigurationManager.AppSettings["InfoSindacatiDownload"];

            string nomeFile = "LavoratoriCemi.zip";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}",nomeFile));
            Response.ContentType = "application/zip";
            Response.TransmitFile(filePath+nomeFile);
            //TransmitFile invece di WriteFile perchè file grosso e transmit non carica in memoria
            Response.Flush();
            Response.End();
        }

        protected void ButtonOldDownload_Click(object sender, EventArgs e)
        {
            string filePath = ConfigurationManager.AppSettings["InfoSindacatiDownload"];

            string nomeFile = "LavoratoriCemi_VecchiaVersione.zip";

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", nomeFile));
            Response.ContentType = "application/zip";
            Response.TransmitFile(filePath + nomeFile);
            //TransmitFile invece di WriteFile perchè file grosso e transmit non carica in memoria
            Response.Flush();
            Response.End();
        }
    }
}