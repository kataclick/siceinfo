﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="InformativaSindacatiDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.InformativaSindacati.InformativaSindacatiDefault" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Download dell'applicativo"
        titolo="InfoSindacati" />
    <br />
    <asp:Label ID="LabelUltimoFile" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownload" runat="server" OnClick="ButtonDownload_Click" Text="Scarica" />
    <br />
    <br />
    <strong>ATTENZIONE!!!
    <br />
    E' disponibile una nuova versione dell'applicativo "Informativa ai sindacati"</strong>
    <br />
    Scaricare il manuale operativo cliccando <strong><a href="InfoSindacati - manuale utente Ver.3.50.docx">QUI</a></strong>.
    <br />
    <br />
    <strong>N.B.:</strong> Il file zip disponibile al download contiene:
    <ul>
        <li><b>La nuova versione dell'applicativo Informativa ai Sindacati (cartella "App")</b></li>
        <%--<li>Il componente aggiuntivo da installare nel caso non si riesca ad aprire correttamente l'applicativo (Cartella "Runtime". <a href="Installazione InfoSindacati.doc">Vedi istuzioni per l'installazione</a>)</li>--%>
        <%--<li>L'export in formato Microsoft Excel dell'elenco dei Lavoratori</li>--%>
        <li>L'export in formato Microsoft Access dell'elenco dei Lavoratori</li>
        <li>Il file della base dati per l'applicativo Informativa ai Sindacati</li> 
		<!--<li><b>La nuova versione dell'applicativo Informativa ai Sindacati</b></li>-->
    </ul>
    <br />
    Nel caso in cui, soprattutto su vecchi sistemi operativi, il programma non dovesse funzionare, mettiamo a disposizione la precedente versione dell'applicativo (Con i dati aggiornati).
    <br />
    <asp:Button ID="ButtonOld" runat="server" OnClick="ButtonOldDownload_Click" Text="Scarica vecchia versione" />
    <!--Va pertanto <b>sostituita la precedente versione dell'applicativo con quella contenuta nel file zip (cartella "Programma")</b><br />-->
    <br />
    <br />
    Per il corretto funzionamento dell'applicativo è necessario il Microsoft .NET Framework 4,
    disponibile tra gli aggiornamenti automatici di Windows.<br />
    Qualora non fosse già installato lo si può scaricare direttamente da Internet: <a href="https://www.microsoft.com/it-it/download/details.aspx?id=17718" target="_blank">
        Microsoft .NET Framework 4 (programma di installazione autonomo)</a>
		<!--
	<p>
		<b>Cantieri Attivi</b></br>
		E' possibile scaricare un elenco dei cantieri attivi negli ultimi tre mesi nelle province di Milano, Lodi, Monza e Brianza.</br>
		Per effettuare il download, <strong><a href="Cantieri_Attivi.xls">cliccare QUI.</a></strong>
	</p>
		-->
</asp:Content>
