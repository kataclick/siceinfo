﻿using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Linq;
using System.IO;
using TBridge.Cemi.Type.Enums.GestioneUtenti;
using TBridge.Cemi.Type.Collections.GestioneUtenti;


namespace Cemi.SiceInfo.Web.CeServizi.InformativaSindacati
{    
    public partial class InformativaSindacatiDefault : System.Web.UI.Page
    {
        GestioneUtentiBiz gu = new GestioneUtentiBiz();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.DownloadInfoSindacati);
            #endregion

            if (!Page.IsPostBack)
            {
                ButtonDownload.Enabled = false;

                try
                {
                    string nomeSindacato = FindSindacatoName();
                    FileInfo file = new FileInfo(ConfigurationManager.AppSettings["InfoSindacatiDownload"]+ $"infosindacati_{nomeSindacato}.zip");
                    if (file.Exists)
                    {
                        LabelUltimoFile.Text = String.Format("File aggiornato al: <b>{0}</b>", file.LastWriteTime.ToShortDateString());
                        ButtonDownload.Enabled = true;
                    }
                    else
                    {
                        LabelUltimoFile.Text = "File non disponibile";
                    }
                }
                catch (Exception exc)
                {
                    LabelUltimoFile.Text = String.Format("Errore durante il recupero del file: {0}", exc.Message);
                }
            }
        }

        protected string FindSindacatoName()
        {
            int IdUtente = GestioneUtentiBiz.GetIdUtente();

            SindacalistiCollection sindacalisti = new SindacalistiCollection();

            sindacalisti = gu.GetUtentiSindacalisti();

            //string nomeFile;
            string nomeSindacato;

            TBridge.Cemi.Type.Entities.GestioneUtenti.Sindacalista sindacalista = new TBridge.Cemi.Type.Entities.GestioneUtenti.Sindacalista();

            sindacalista = (from TBridge.Cemi.Type.Entities.GestioneUtenti.Sindacalista sin in sindacalisti
                            where sin.IdUtente == IdUtente
                            select sin).SingleOrDefault();


            /*
                idSindacato	| descrizione
                01	        | CGIL
                02	        | CISL
                03	        | UIL
            */

            switch (sindacalista.Sindacato.Id)
            {
                case "01":
                    nomeSindacato = "cgil";
                    break;
                case "02":
                    nomeSindacato = "cisl";
                    break;
                case "03":
                    nomeSindacato = "uil";
                    break;
                default:
                    nomeSindacato = "";
                    break;
            }

            return nomeSindacato;

            /*
            switch (sindacalista.Sindacato.Id)
            {
                case "01":
                    nomeFile = "InfoSindacati_cgil.zip";
                    break;
                case "02":
                    nomeFile = "InfoSindacati_cisl.zip";
                    break;
                case "03":
                    nomeFile = "InfoSindacati_uil.zip";
                    break;
                default:
                    nomeFile = "InfoSindacati.zip";
                    break;
            }

            return nomeFile;
            */
        }


        protected void ButtonDownload_Click(object sender, EventArgs e)
        {
            string filePath = ConfigurationManager.AppSettings["InfoSindacatiDownload"];

            string nomeSindacato = FindSindacatoName();
            //string nomeFile = FindFileName();

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}",$"infosindacati_{nomeSindacato}.zip"));
            Response.ContentType = "application/zip";
            Response.TransmitFile($"{filePath}infosindacati_{nomeSindacato}.zip");
            //TransmitFile invece di WriteFile perchè file grosso e transmit non carica in memoria
            Response.Flush();
            Response.End();
        }

        protected void ButtonOldDownload_Click(object sender, EventArgs e)
        {
            string filePath = ConfigurationManager.AppSettings["InfoSindacatiDownload"];

            string nomeSindacato = FindSindacatoName();
            //string nomeFile = FindFileName();

            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", $"infosindacati_VecchiaVersione_{nomeSindacato}.zip"));
            Response.ContentType = "application/zip";
            Response.TransmitFile($"{filePath}infosindacati_VecchiaVersione_{nomeSindacato}.zip");
            //TransmitFile invece di WriteFile perchè file grosso e transmit non carica in memoria
            Response.Flush();
            Response.End();
        }
    }
}