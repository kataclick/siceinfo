﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="DelegheArchivioCartaceo.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.InformativaSindacati.DelegheArchivioCartaceo" %>
<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Download Elenco Lavoratori Cemi"
        titolo="InfoSindacati" />
    <br />
    <p>In questa pagina è possibile scaricare l'archivio contentente le immagini delle deleghe presentate il mese precedente.</p>
    <strong>ISTRUZIONI</strong>
    <ul>
        <li>Scaricare il file zip "deleghe-<i>[SiglaSindacato]</i>.zip"</li>
        <li>Cliccare con il tasto destro del mouse sopra il file e selezionare dal menu a tendina che verrà visualizzato la voce "Estrai tutto"</li>
        <li>Verrà estratto un ulteriore file zip con nome "deleghe-<i>[SiglaSindacato]-anno-mese</i>.zip"</li>
        <li>Cliccare con il tasto destro del mouse sopra il file e selezionare dal menu a tendina che verrà visualizzato la voce "Estrai tutto"</li>
        <li>Fare doppio click con il tasto sinistro del mouse sul file DELEGHE</li>
        <li>Apparirà la maschera di ricerca con i possibili filtri applicabili. Per visualizzare tutti i cartacei, cliccare su accetta senza inserire alcun filtro.</li>
    </ul>    
    <p>
        <strong>N.B.</strong>: è possibile ricercare solo le deleghe del proprio sindacato, del mese specificato nel nome del file zip ("deleghe-[SiglaSindacato]-<strong>anno-mese</strong>.zip").
    </p>
    <p>&nbsp;</p>
    <p>
    <asp:Label ID="LabelUltimoFile" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownload" runat="server" OnClick="ButtonDownload_Click" Text="Scarica" />
    </p>
    
</asp:Content>