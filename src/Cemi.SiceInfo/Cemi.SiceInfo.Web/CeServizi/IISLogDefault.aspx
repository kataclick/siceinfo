﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="IISLogDefault.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.IISLogDefault" %>

<%@ Register Src="WebControls/MenuIISLog.ascx" TagName="IISLogMenu" TagPrefix="uc1" %>
<%@ Register Src="WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc1:IISLogMenu id="IISLogMenu1" runat="server">
    </uc1:IISLogMenu>
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc2:TitoloSottotitolo id="TitoloSottotitolo1" runat="server">
    </uc2:TitoloSottotitolo>
    <br />
    Sono presentate le statistiche ricavate dai log del web server IIS</asp:Content>