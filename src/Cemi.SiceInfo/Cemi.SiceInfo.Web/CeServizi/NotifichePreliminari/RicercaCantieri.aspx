﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RicercaCantieri.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.RicercaCantieri" %>

<%@ Register src="../WebControls/TitoloSottotitolo.ascx" tagname="TitoloSottotitolo" tagprefix="uc1" %>
<%@ Register src="WebControls/NotifichePreliminariRicercaNotificheFilter.ascx" tagname="NotifichePreliminariRicercaNotificheFilter" tagprefix="uc2" %>
<%@ Register src="../WebControls/MenuNotifichePreliminari.ascx" tagname="MenuNotifichePreliminari" tagprefix="uc3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MenuDettaglio" Runat="Server">
    <uc3:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainPage" Runat="Server">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" titolo="Notifiche preliminari" sottoTitolo="Ricerca cantieri" />
    <br />
    <br />
    <div class="standardDiv">
        <uc2:NotifichePreliminariRicercaNotificheFilter ID="NotifichePreliminariRicercaNotificheFilter1" 
        runat="server" />
    </div>
    <div class="standardDiv">
        <table class="standardTable">
            <tr>
                <td>
                    <b>Ordinamento</b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButtonList
                        ID="RadioButtonListOrdinamento"
                        runat="server"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Text="per committente" Value="C" Selected="True">
                        </asp:ListItem>
                        <asp:ListItem Text="per impresa" Value="I" Selected="False">
                        </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td style="text-align:right">
                    <asp:Button
                        ID="ButtonCerca"
                        runat="server"
                        Width="100px"
                        OnClientClick="javascript :if ( Page_ClientValidate('ricercaNotifiche') ) {this.disabled = true; this.value = 'Attendi...';}"
                        UseSubmitBehavior="false"
                        Text="Cerca" 
                        onclick="ButtonCerca_Click" />
                    &nbsp;
                    <asp:Button
                        ID="ButtonEstrazione"
                        runat="server"
                        ValidationGroup="ricercaNotifiche"
                        Width="100px"
                        Text="Estraz. Excel" onclick="ButtonEstrazione_Click" Enabled="false"/>
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <telerik:RadGrid ID="RadGridCantieriPerCommittente" runat="server" Width="100%" AllowPaging="True"
        OnPageIndexChanged="RadGridCantieriPerCommittente_PageIndexChanged" GridLines="None" Visible="False"
        OnItemDataBound="RadGridCantieriPerCommittente_ItemDataBound" 
        OnItemCommand="RadGridCantieriPerCommittente_ItemCommand" CellSpacing="0">
        <ExportSettings>
            <Pdf PageWidth="" />
        </ExportSettings>
        <MasterTableView DataKeyNames="NumeroNotifica">
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Committente" UniqueName="Committente">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelCommittenteCognomeNome" runat="server" />
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <asp:Label ID="LabelCommittenteCodiceFiscale" runat="server" />
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="100px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa" Visible="false">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelImpresaRagioneSociale" runat="server" />
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <asp:Label ID="LabelImpresaCodiceFiscale" runat="server" />
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="100px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Prot." UniqueName="Protocollo" DataField="NumeroNotifica">
                    <ItemStyle Width="60px" VerticalAlign="Top" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Indirizzo" UniqueName="Indirizzo">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelIndirizzo" runat="server" />
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelComune" runat="server" />
                                    </b>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="150px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Date" UniqueName="Date">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelDataInizio" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <asp:Label ID="LabelDurata" runat="server" />
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="50px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Natura opera" UniqueName="NaturaOpera">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNaturaOperaCategoria" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNaturaOperaTipologia" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNaturaOpera3" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="50px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Select" Text="Dettaglio"
                    UniqueName="column" ImageUrl="../images/lente.png">
                    <ItemStyle Width="10px" VerticalAlign="Top" />
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
            <PagerStyle PageSizeControlType="RadComboBox" />
        </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>
    <telerik:RadGrid ID="RadGridCantieriPerImpresa" runat="server" Width="100%" AllowPaging="True"
        OnPageIndexChanged="RadGridCantieriPerImpresa_PageIndexChanged" GridLines="None" Visible="False"
        OnItemDataBound="RadGridCantieriPerImpresa_ItemDataBound" 
        OnItemCommand="RadGridCantieriPerImpresa_ItemCommand" CellSpacing="0">
        <ExportSettings>
            <Pdf PageWidth="" />
        </ExportSettings>
        <MasterTableView DataKeyNames="NumeroNotifica">
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridTemplateColumn HeaderText="Impresa" UniqueName="Impresa">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelImpresaRagioneSociale" runat="server" />
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <asp:Label ID="LabelImpresaCodiceFiscale" runat="server" />
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="100px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Committente" UniqueName="Committente">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelCommittenteCognomeNome" runat="server" />
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <asp:Label ID="LabelCommittenteCodiceFiscale" runat="server" />
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="100px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Prot." UniqueName="Protocollo" DataField="NumeroNotifica">
                    <ItemStyle Width="60px" VerticalAlign="Top" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Indirizzo" UniqueName="Indirizzo">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelIndirizzo" runat="server" />
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="LabelComune" runat="server" />
                                    </b>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="150px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Date" UniqueName="Date">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelDataInizio" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <asp:Label ID="LabelDurata" runat="server" />
                                    </small>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="50px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Natura opera" UniqueName="NaturaOpera">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNaturaOperaCategoria" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNaturaOperaTipologia" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LabelNaturaOpera3" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="50px" VerticalAlign="Top" />
                </telerik:GridTemplateColumn>
                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Select" Text="Dettaglio"
                    UniqueName="column" ImageUrl="../images/lente.png">
                    <ItemStyle Width="10px" VerticalAlign="Top" />
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
            <PagerStyle PageSizeControlType="RadComboBox" />
        </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>
</asp:Content>



