﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Entities;
using TBridge.Cemi.Cpt.Business;
using Cemi.NotifichePreliminari.Types.Filters;
using TBridge.Cemi.GestioneUtenti.Business;
using System.Configuration;
using TBridge.Cemi.Business;
using TBridge.Cemi.Business.Cpt;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari
{
    public partial class RicercaCantieriGeolocalizzazione : System.Web.UI.Page
    {
        private readonly NotifichePreliminariManager _biz = new NotifichePreliminariManager();
        private readonly CptBusiness _oldBiz = new CptBusiness();
        private readonly int _maxPushpin = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPushpin"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            if (!Page.IsPostBack)
            {
                CaricaMappa();
            }
        }

        protected void ButtonNotificheCerca_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                CaricaCantieri();
            }
        }


        private string normalizzaStringa(string stringa, int lunghezza)
        {
            string res = string.Empty;

            if (stringa != null)
            {
                if (stringa.Length <= lunghezza)
                    res = Server.HtmlEncode(stringa.Replace('\r', ' ').Replace('\n', ' '));
                else
                    res =
                        Server.HtmlEncode(String.Format("{0}..",
                                                        stringa.Substring(0, lunghezza).Replace('\r', ' ').Replace('\n', ' ')));
            }

            return res;
        }

        private void CaricaMappa(CantiereCollection listaCantieri)
        {

            StringBuilder script = new StringBuilder();
            script.Append("LoadMap();");

            if (listaCantieri.Count <= _maxPushpin)
            {
                try
                {
                    int i = 0;
                    foreach (Cantiere cantiere in listaCantieri)
                    {
                        var descrizione = new StringBuilder();
                        if (cantiere.Indirizzo.Latitudine.HasValue && cantiere.Indirizzo.Longitudine.HasValue)
                        {
                            // Indirizzo del cantiere
                            descrizione.Append(normalizzaStringa(cantiere.Indirizzo.NomeVia, 40));
                            //descrizione.Append(" ");
                            //descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Civico, 10));
                            //if (!string.IsNullOrEmpty(cantiere.Indirizzo.InfoAggiuntiva))
                            //{
                            //    descrizione.Append(" (");
                            //    descrizione.Append(normalizzaStringa(cantiere.Indirizzo.InfoAggiuntiva, 20));
                            //    descrizione.Append(")");
                            //}
                            descrizione.Append("<br />");
                            descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Comune, 30));
                            descrizione.Append(" ");
                            if (!string.IsNullOrEmpty(cantiere.Indirizzo.Provincia))
                            {
                                descrizione.Append(" (");
                                descrizione.Append(normalizzaStringa(cantiere.Indirizzo.Provincia, 20));
                                descrizione.Append(")");
                            }

                            // Informazioni aggiuntive
                            descrizione.Append("<br />");
                            descrizione.Append("<br />");
                            descrizione.Append("Categoria:");
                            descrizione.Append("<br />");
                            // descrizione.Append("<b>");
                            descrizione.Append(normalizzaStringa(cantiere.Categoria, 150));
                            //  descrizione.Append("</b>");
                            descrizione.Append("<br />");
                            descrizione.Append("<br />");
                            descrizione.Append("Committente:");
                            descrizione.Append("<br />");
                            descrizione.Append(normalizzaStringa(cantiere.Committente, 50));
                            descrizione.Append("<br />");
                            //descrizione.Append("<br />");



                            // Link alla notifica
                            //descrizione.Append("<br />");
                            descrizione.Append("<br />");
                            descrizione.Append("<a href=DettaglioNotifica.aspx?NumeroNotifica=" + cantiere.NumeroNotifica +
                                               ">Visualizza notifica</a>");

                            string immagineCantiere = "../images/cantiere24.gif";

                            script.Append(String.Format("AddShape('{0}','{1}','{2}','{3}','{4}');", i,
                                                               cantiere.Indirizzo.Latitudine.ToString().Replace(',', '.'),
                                                               cantiere.Indirizzo.Longitudine.ToString().Replace(',', '.'), descrizione,
                                                               immagineCantiere));

                            i++;


                        }
                    }
                }
                catch (Exception exc)
                {
                }
            }
            else
            {
                //CustomValidatorMaxPushpin.ErrorMessage = "I cantieri trovati sono troppi e non possono essere visualizzati sulla mappa, filtrare maggiormemte";
                CustomValidatorMaxPushpin.IsValid = false;
            }

            RadAjaxPanel1.ResponseScripts.Add(script.ToString());
        }

        private void CaricaMappa()
        {
            RadAjaxPanel1.ResponseScripts.Add("LoadMap();");
        }

        private void CaricaCantieri()
        {
            NotificaPreliminareFilter filter = UserControlRicercaNotificheFilter1.GetFilter();
            if (filter != null)
            {
                filter.CantiereIndirizzoGeolocalizzato = true;  //Cerco solo quelli geolocalizzati

                TBridge.Cemi.Cpt.Type.Entities.LogRicerca log = new TBridge.Cemi.Cpt.Type.Entities.LogRicerca();
                log.IdUtente = GestioneUtentiBiz.GetIdUtente();
                log.XmlFiltro = _oldBiz.GetPropertiesDelFiltro(filter);
                log.Sezione = TBridge.Cemi.Cpt.Type.Enums.SezioneLogRicerca.LocalizzazioneCantieri;
                _oldBiz.InsertLogRicerca(log);

                CantiereCollection cantieri = _biz.RicercaCantieriPerCommittenteByFilter(filter);
                CaricaMappa(cantieri);
                LabelCantieriTrovati.Visible = true;
                LabelCantieriTrovati.Text = String.Format("Cantieri trovati: {0}", cantieri.Count);


            }
        }
    }
}