﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Cemi.NotifichePreliminari.Types.Collections;
using Cemi.NotifichePreliminari.Types.Entities;
using Cemi.SiceInfo.Web.Helpers;

namespace Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.WebControls
{
    public partial class GridImprese : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadGridImprese_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {
            CaricaImprese();
        }



        protected void RadGridImprese_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                // Dichiarazione dichiarazione = (Dichiarazione)e.Item.DataItem;

                ImpresaNotifichePreliminari impresa = (ImpresaNotifichePreliminari)e.Item.DataItem;

                Label labRagSoc = (Label)e.Item.FindControl("labelRagioneSociale");
                Label labCf = (Label)e.Item.FindControl("labelCodiceFiscale");
                Label labPIva = (Label)e.Item.FindControl("labelPartitaIva");

                labRagSoc.Text = impresa.RagioneSociale;
                labCf.Text = impresa.CodiceFiscale;
                labPIva.Text = impresa.PartitaIva;
            }
        }

        private void CaricaImprese()
        {
            throw new NotImplementedException();
        }

        public void CaricaImprese(ImpresaNotifichePreliminariCollection imprese)
        {
            Presenter.CaricaElementiInGridView(RadGridImprese, imprese);
        }

    }
}