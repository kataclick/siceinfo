﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotifichePreliminariRicercaNotificheFilter.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.WebControls.NotifichePreliminariRicercaNotificheFilter" %>
<style type="text/css">
    .styleCol1
    {
        width: 30%;
    }
    .styleCol2
    {
        width: 30%;
    }
    .styleCol3
    {
        width: 20%;
    }
    .styleCol4
    {
        width: 20%;
    }
</style>

<table class="standardTable">
    <tr>
        <td colspan="4">
            <b>Notifica</b>
        </td>
    </tr>
    <tr>
        <td class="styleCol1">
            Dal (ggmmaaaa)
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDal" runat="server"
                ControlToValidate="TextBoxDal" ErrorMessage="Campo Dal deve essere una data nel formato ggmmaaaa" 
                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
        </td>
        <td class="styleCol2">
            Al (ggmmaaaa)
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorAl" runat="server"
                ControlToValidate="TextBoxAl" ErrorMessage="Campo Al deve essere una data nel formato ggmmaaaa" 
                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
        </td>

        <td class="styleCol3">
            Natura opera
        </td>
        <td class="styleCol4">
            Ammontare (&gt;)
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ErrorMessage="Campo Ammontare deve essere un numero"
                ValidationExpression="^[.][0-9]+$|[0-9]*[,]*[0-9]{0,2}$" ControlToValidate="TextBoxAmmontare"
                ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
        </td>               
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxDal" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxAl" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
        </td>

        <td>
            <asp:TextBox ID="TextBoxNaturaOpera" runat="server" Width="100%" MaxLength="255" />
        </td>
        <td>
            <asp:TextBox ID="TextBoxAmmontare" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
        </td>
                        
    </tr>
    <tr>
        <td>
            Inizio lavori (&gt;)(ggmmaaaa)
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorInizioLavDal" runat="server"
                ControlToValidate="TextBoxInizioLavoriDal" ErrorMessage="Campo Inizio lavori > deve essere una data nel formato ggmmaaaa" 
                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
        </td>      
        <td>
            Inizio lavori (&lt;)(ggmmaaaa)
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorInizioLavAl" runat="server"
                ControlToValidate="TextBoxInizioLavoriAl" 
                ErrorMessage="Campo Inizio lavori < deve essere una data nel formato ggmmaaaa" 
                ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
        </td>
         <td>
            Protocollo regione
        </td>
         <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxInizioLavoriDal" runat="server" Width="100%" MaxLength="8" />
        </td>
        <td>
            <asp:TextBox ID="TextBoxInizioLavoriAl" runat="server" Width="100%" MaxLength="8" />
        </td>
         <td>
            <asp:TextBox ID="TextBoxNumeroNotifica" runat="server" Width="100%" MaxLength="50" />
        </td>
         <td>
        </td>
    </tr>

    <tr>
        <td colspan="4">
            <b>Cantiere</b>
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo
        </td>
        <td>
            Comune
        </td>
        <td>
            Provincia
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxIndirizzoIndirizzo" Width="100%" runat="server" MaxLength="250"/>
        </td>
        <td>
            <asp:TextBox ID="TextBoxIndirizzoComune" Width="100%" runat="server" MaxLength="50"/>
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxIndirizzoProvincia"
                runat="server"
               
                Width="100%"
                EmptyMessage="-" 
                    MarkFirstMatch="true" AllowCustomText="false"  EnableScreenBoundaryDetection="false">
            </telerik:RadComboBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="4">
        <b>Committente</b>
        </td>
    </tr>
    <tr>
        <td>
            Nome
        </td>
        <td>
            Cognome
        </td>
        <td>
            Codice fiscale
        </td>
        <td>

        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="textBoxCommittenteNome" runat="server" Width="100%" MaxLength="50" />
        </td> 
         <td>
            <asp:TextBox ID="textBoxCommittenteCognome" runat="server" Width="100%" MaxLength="50" />
        </td>  
         <td>
            <asp:TextBox ID="textBoxCommittenteCodFiscale" runat="server" Width="100%" MaxLength="16" />
        </td> 
        <td>
        </td>
    </tr>

<%--    <tr>
        <td>
            Indirizzo
        </td>
        <td>
            Comune
        </td>
        <td>
            Provincia
        </td>
        <td>
            Cap
            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBoxCap"
                ErrorMessage="*" ValidationExpression="^\d{5}$" ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxIndirizzo" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxComune" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:DropDownList
                ID="DropDownListProvincia"
                runat="server"
                AppendDataBoundItems="true"
                Width="100%">
            </asp:DropDownList>
        </td>  
        <td>
            <asp:TextBox ID="TextBoxCap" runat="server" MaxLength="5" Width="100%"></asp:TextBox>
        </td>
    </tr>--%>
    
    
   <%-- <tr>
                        
        <td>
            <asp:Label ID="Label3" runat="server" Text="Inizio lavori (>)(ggmmaaaa)"></asp:Label>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataInizio" runat="server"
                ControlToValidate="TextBoxDataInizioLavori" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
        </td>
        <td>
            <asp:Label ID="Label4" runat="server" Text="Fine lavori (<)(ggmmaaaa)"></asp:Label>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDataFine" runat="server"
                ControlToValidate="TextBoxDataFineLavori" ErrorMessage="*" ValidationExpression="(((0[1-9]|[12][0-9]|3[01])(0[13578]|10|12)(\d{4}))|(([0][1-9]|[12][0-9]|30)(0[469]|11)(\d{4}))|((0[1-9]|1[0-9]|2[0-8])(02)(\d{4}))|((29)(\.|-|\/)(02)([02468][048]00))|((29)(02)([13579][26]00))|((29)(02)([0-9][0-9][0][48]))|((29)(02)([0-9][0-9][2468][048]))|((29)(02)([0-9][0-9][13579][26])))"
                ValidationGroup="ricercaNotifiche"></asp:RegularExpressionValidator>
        </td>
        <td>
            Numero appalto
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBoxDataInizioLavori" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxDataFineLavori" runat="server" MaxLength="8" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxNumeroAppalto" runat="server" MaxLength="20" Width="100%"></asp:TextBox>
        </td>
        <td>
            
        </td>
    </tr>--%>

    <tr>
        <td colspan = "4">
            <b>Impresa</b>
        </td>
    </tr>
    <tr>
        <td>
            Rag.soc.impresa
        </td>
        <td>
            P.IVA/Cod.fisc.impresa
        </td>
        <td>
            Provincia impresa
        </td>
        <td>
            CAP impresa
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCapImpresa" runat="server" ControlToValidate="TextBoxImpresaCap"
                ErrorMessage="CAP impresa non valido" 
                ValidationExpression="^\d{5}$" ValidationGroup="ricercaNotifiche">*</asp:RegularExpressionValidator>
        </td>
    </tr>

    <tr>
        <td>
            <asp:TextBox ID="TextBoxImpresaRagSoc" runat="server" MaxLength="255" Width="100%"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxImpresaIvaFisc" runat="server" MaxLength="16" Width="100%"></asp:TextBox>
        </td>
        <td>
            <telerik:RadComboBox
                ID="RadComboBoxImpresaProvincia"
                runat="server"
               
                Width="100%"
                EmptyMessage="-" 
                    MarkFirstMatch="true" AllowCustomText="false"  EnableScreenBoundaryDetection="false">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:TextBox ID="TextBoxImpresaCap" runat="server" MaxLength="5" Width="100%"></asp:TextBox>
        </td>
    </tr>
</table>
