﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GridIndirizzi.ascx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.WebControls.GridIndirizzi" %>

<style type="text/css"> 
.RadGrid .Row50 td 
{ 
    padding-top:10px; 
    padding-bottom:10px; 
    height:auto;
    vertical-align:middle; 
} 
 
</style> 
 <telerik:RadGrid ID="RadGridIndizziCantieri" runat="server" Width="100%"
        OnPageIndexChanged="RadGridIndirizzi_PageIndexChanged" GridLines="None"
        OnItemDataBound="RadGridIndirizzi_ItemDataBound" CellSpacing="0" 
    >
        <MasterTableView >
           <%-- <ItemStyle  CssClass="Row50" />   
            <AlternatingItemStyle  CssClass="Row50" />--%>
            <RowIndicatorColumn>
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <%--<telerik:GridBoundColumn HeaderText="ID" UniqueName="NumeroNotifica" DataField="NumeroNotifica">
                    <ItemStyle Width="40px" />
                </telerik:GridBoundColumn>--%>
                <%--   <telerik:GridBoundColumn HeaderText="Attività" UniqueName="Attività" 
                    DataField="Attivita">
                    <ItemStyle Width="50px" />
                </telerik:GridBoundColumn>--%>
               
                <telerik:GridTemplateColumn HeaderText="Indirizzo" UniqueName="Indirizzo" >
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                     <asp:Label ID="LabelIndirizzo" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <ItemStyle Width="40%" Font-Bold="true"/>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Inizio lavori" UniqueName="Iniziolavori" DataField="DataInizioLavori" DataType="System.DateTime" 
                                            DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Width="20%" />
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn HeaderText="Durata lavori" UniqueName="Duratalavori" DataField="DurataLavoriVisualizzazione"   >
                    <ItemStyle Width="20%"/>                
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Numero lavoratori" UniqueName="Lavoratori" DataField="NumeroMassimoLavoratori"  >
                    <ItemStyle Width="20%" />                
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
