﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RicercaNotifiche.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.RicercaNotifiche" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc2" %>
<%@ Register Src="WebControls/NotifichePreliminariRicercaNotificheFilter.ascx" TagPrefix="uc3" TagName="RicercaNotificheFilter" %>
<%@ Register src="../Cantieri/WebControls/CantiereDaNotificaRegionale.ascx" tagname="CantiereDaNotificaRegionale" tagprefix="uc1" %>
<%@ Register src="../WebControls/MenuNotifichePreliminari.ascx" tagname="MenuNotifichePreliminari" tagprefix="uc4" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Ricerca notifiche"
        titolo="Notifiche preliminari" />
    <br />
    <table class="borderedTable">
        <tr>
            <td>
                <table class="standardTable">
                    <tr>
                     <td colspan="2">
                         <uc3:RicercaNotificheFilter ID="UserControlRicercaNotificheFilter1" runat="server" />
                         <br />
                     </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="ValidationSummaryRicercaNotifiche" runat="server" 
                                                ValidationGroup="ricercaNotifiche" ForeColor="Red" />
                        </td>
                        <td align="right" valign="top">
                            <asp:Button ID="ButtonNotificheCerca" runat="server" Text="Cerca" Width="100px"
                                        OnClick="ButtonNotificheCerca_Click" 
                                        OnClientClick="javascript :if ( Page_ClientValidate('ricercaNotifiche') ) {this.disabled = true; this.value = 'Attendi...';}"
                                        UseSubmitBehavior="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td>
                <br />
                <br />
                <telerik:RadGrid ID="RadGridNotifiche" runat="server" Width="100%" AllowPaging="True"
                        OnPageIndexChanged="RadGridNotifiche_PageIndexChanged" 
                    GridLines="None" Visible="False"
                        OnItemDataBound="RadGridNotifiche_ItemDataBound" 
                        OnItemCommand="RadGridNotifiche_ItemCommand" CellSpacing="0">
                        <ExportSettings>
                            <Pdf PageWidth="" />
                        </ExportSettings>
                        <MasterTableView DataKeyNames="NumeroNotifica">
                            <CommandItemSettings ExportToPdfText="Export to PDF" />
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
<%--                                <telerik:GridBoundColumn HeaderText="ID" UniqueName="NumeroNotifica" DataField="NumeroNotifica">
                                    <ItemStyle Width="40px" />
                                </telerik:GridBoundColumn>--%>
                                <%--   <telerik:GridBoundColumn HeaderText="Attività" UniqueName="Attività" 
                                    DataField="Attivita">
                                    <ItemStyle Width="50px" />
                                </telerik:GridBoundColumn>--%>
                               <%-- <telerik:GridBoundColumn HeaderText="Committenti" UniqueName="Committenti"  ItemStyle-VerticalAlign="Top"
                                    DataField="CommittentiVisualizzazione">
                                    <ItemStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Indirizzi" UniqueName="Indirizzi" ItemStyle-VerticalAlign="Top"
                                    DataField="IndirizziVisualizzazione">
                                    <ItemStyle Width="100px" />
                                </telerik:GridBoundColumn>--%>
                                <telerik:GridTemplateColumn HeaderText="Notifica" UniqueName ="Notifica">
                                    <ItemTemplate>
                                        <table class="standardTable">
                                            <tr>
                                                <td style="width: 90px">
                                                    Prot.Regione:
                                                </td>
                                                <td>
                                                    <b>
                                                        <asp:Label  ID="LabelNumeroNotifica" runat="server" />
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 90px">
                                                    Data:
                                                </td>

                                                <td>
                                                    <asp:Label  ID="LabelDataComunicazione" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 90px">
                                                    Ultimo agg.:
                                                </td>

                                                <td>
                                                    <asp:Label  ID="LabelDataAggiornamento" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" style="width: 90px">
                                                    Categoria:
                                                </td>
                                                <td>
                                                    <asp:Label  ID="LabelCategoria" runat="server" />
                                                </td>
                                             </tr>
                                            <tr>
                                                 <td valign="top" style="width: 90px">
                                                    Tipologia:
                                                </td>
                                                <td>
                                                    <asp:Label  ID="LabelTipologia" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td style="width: 90px" >
                                                    Ammontare:
                                                </td>
                                                <td>
                                                    <asp:Label  ID="LabelAmmontare" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <uc1:CantiereDaNotificaRegionale ID="CantiereDaNotificaRegionale1" 
                                                        runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <ItemStyle Width="40%" VerticalAlign="Top" />
                                </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Indirizzi" UniqueName="Indirizzi">
                                    <ItemTemplate>
                                       <asp:BulletedList ID="BulletListIndirizzi" runat="server"  />
                                    </ItemTemplate>
                                    <ItemStyle Width="30%" VerticalAlign="Top"/>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Committenti" UniqueName="Committenti" >
                                    <ItemTemplate>
                                        <asp:ListView ID="ListViewDatiGeneraliIndirizzi" runat="server" OnItemDataBound="listViewCommittenti_DataBound">
                                            <LayoutTemplate>
                                                <table class="standardTable">
                                                    <asp:Panel ID="itemPlaceholder" runat="server" />
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <b>
                                                          <asp:Label ID="LabelCommittenteNome" runat="server" />
                                                        </b>
                                                        <br />
                                                        <asp:Label ID="LabelCommittanteCf" runat="server" />
                                                    </td>
                                                </tr>
                                                
                                            </ItemTemplate>
                                            <EmptyDataTemplate>
                                                ----
                                            </EmptyDataTemplate>
                                        </asp:ListView>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" VerticalAlign="Top" />
                                </telerik:GridTemplateColumn>  
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="GoToDettaglio" Text="Dettaglio"
                                                        UniqueName="buttonDettaglio" ImageUrl="../images/lente.png">
                                    <ItemStyle Width="10%" VerticalAlign="Top"  />
                                </telerik:GridButtonColumn>
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                            <PagerStyle PageSizeControlType="RadComboBox" />
                        </MasterTableView>

<PagerStyle PageSizeControlType="RadComboBox"></PagerStyle>

<FilterMenu EnableImageSprites="False"></FilterMenu>
                    </telerik:RadGrid>
            </td>
        </tr>
    </table>

   
</asp:Content >
<asp:Content ID="Content7" runat="server" contentplaceholderid="MenuDettaglio">
    <uc4:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>


