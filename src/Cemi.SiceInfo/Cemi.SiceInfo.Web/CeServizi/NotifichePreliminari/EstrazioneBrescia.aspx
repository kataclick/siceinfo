﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="EstrazioneBrescia.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.EstrazioneBrescia" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc1" %>
<%@ Register src="../WebControls/MenuNotifichePreliminari.ascx" tagname="MenuNotifichePreliminari" tagprefix="uc3" %>
<%--  <%@ Register src="../WebControls/MenuCpt.ascx" tagname="MenuCpt" tagprefix="uc2" %> --%>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="MainPage">
    <uc1:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Estrazione Brescia"
        titolo="Notifiche Preliminari" />
    <br />
    <asp:Label ID="LabelUltimoFile" runat="server"></asp:Label>
    <br />
    <asp:Button ID="ButtonDownload" runat="server" OnClick="ButtonDownload_Click" Text="Download" />
</asp:Content>
<asp:Content ID="Content5" runat="server" contentplaceholderid="MenuDettaglio">
    <uc3:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>
