﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReportNotifica.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.ReportNotifica" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo"
    TagPrefix="uc2" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<%@ Register src="../WebControls/MenuNotifichePreliminari.ascx" tagname="MenuNotifichePreliminari" tagprefix="uc3" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MenuDettaglio" runat="Server">
    
    <uc3:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc2:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Report notifica"
        titolo="Notifiche preliminari" />
    <br />
    <table style="height: 600pt; width: 550pt;">
        <tr>
            <td>
                <rsweb:reportviewer ID="ReportViewerNotifica" runat="server" ProcessingMode="Remote" ShowParameterPrompts="False"
                    Height="550pt" Width="550pt" />
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
