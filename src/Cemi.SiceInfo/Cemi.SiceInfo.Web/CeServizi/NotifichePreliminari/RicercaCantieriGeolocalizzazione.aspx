﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CeServizi/MasterPage.Master" AutoEventWireup="true" CodeBehind="RicercaCantieriGeolocalizzazione.aspx.cs" Inherits="Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.RicercaCantieriGeolocalizzazione" %>

<%@ Register Src="../WebControls/TitoloSottotitolo.ascx" TagName="TitoloSottotitolo" TagPrefix="uc3" %>
<%@ Register Src="WebControls/NotifichePreliminariRicercaNotificheFilter.ascx" TagPrefix="uc1" TagName="RicercaNotificheFilter" %>
<%@ Register Src="../WebControls/MenuNotifichePreliminari.ascx" TagName="MenuNotifichePreliminari" TagPrefix="uc2" %>

<asp:Content ID="Content6" ContentPlaceHolderID="MainPage" runat="Server">
    <uc3:TitoloSottotitolo ID="TitoloSottotitolo1" runat="server" sottoTitolo="Localizzazione cantieri"
        titolo="Notifiche preliminari" />
    <br />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1" Width="100%">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script src="https://www.bing.com/api/maps/mapcontrol?callback=LoadMap" async defer></script>
            <script type="text/javascript">
                //Mappa
                var map = null;

                //Parametri della mappa
                var latitudine = 45.464044;
                var longitudine = 9.191567;
                var zoom = 10;

                function LoadMap() {
                    map = new Microsoft.Maps.Map('#myMap', {
                        credentials: 'AkAg3s1r8wAGdbJns9DCog3AcDYwwWmFgQCE0P-O6-kIxD_NjIGWmx47miyUlQLF',
                        center: new Microsoft.Maps.Location(latitudine, longitudine),
                        mapTypeId: Microsoft.Maps.MapTypeId.road,
                        zoom: zoom
                    });
                }

                function AddShape(id, lat, lon, descrizione, img) {
                    var pin;
                    var position = new Microsoft.Maps.Location(lat, lon);
                    if (img)
                        pin = new Microsoft.Maps.Pushpin(position,
                            {
                                icon: img,
                                //anchor: new Microsoft.Maps.Point(12, 39)
                            });
                    else
                        pin = new Microsoft.Maps.Pushpin(position,
                            {
                                //icon: img
                                //anchor: new Microsoft.Maps.Point(12, 39)
                            });

                    var infobox = new Microsoft.Maps.Infobox(position, {
                        title: 'Dettaglio',
                        description: descrizione, visible: false
                    });
                    infobox.setMap(map);
                    Microsoft.Maps.Events.addHandler(pin, 'click', function () {
                        infobox.setOptions({ visible: true });
                    });

                    //Add the pushpin to the map
                    map.entities.push(pin);
                }
            </script>
        </telerik:RadCodeBlock>

        <table class="borderedTable" width="100%">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <uc1:RicercaNotificheFilter ID="UserControlRicercaNotificheFilter1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummaryRicercaNotifiche" runat="server"
                                    ValidationGroup="ricercaNotifiche" ForeColor="Red" />
                            </td>
                            <td align="right">
                                <asp:CustomValidator ID="CustomValidatorMaxPushpin" runat="server"
                                    Display="None" EnableClientScript="False" ValidationGroup="ricercaNotifiche"
                                    ErrorMessage="I cantieri trovati sono troppi e non possono essere visualizzati sulla mappa, filtrare maggiormemte" />
                                <asp:Button ID="ButtonNotificheCerca" runat="server" Text="Cerca" Width="100px"
                                    OnClick="ButtonNotificheCerca_Click" ValidationGroup="ricercaNotifiche" />
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelCantieriTrovati" Font-Size="Small" Font-Bold="true" runat="server" Text="Cantieri trovati:" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>

                    <div id='myMap' style="position: relative; float: inherit; width: 100%; height: 500px;">
                    </div>
                </td>
            </tr>
        </table>

    </telerik:RadAjaxPanel>
</asp:Content>

<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="MenuDettaglio">
    <uc2:MenuNotifichePreliminari ID="MenuNotifichePreliminari1" runat="server" />
</asp:Content>



