﻿using System;
using System.Configuration;
using System.Web.UI;
using TBridge.Cemi.GestioneUtenti.Business;

using System.IO;
using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari
{
    public partial class EstrazioneAssimpredil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazione
            GestioneAutorizzazionePagine.PaginaAutorizzata(FunzionalitaPredefinite.NotifichePreliminariEstrazioneAssimpredil);
            #endregion

            if (!Page.IsPostBack)
            {
                ButtonDownloadMilano.Enabled = false;
                ButtonDownloadLodi.Enabled = false;
                ButtonDownloadMonzaBrianza.Enabled = false;

                #region Milano
                try
                {
                    FileInfo file = new FileInfo(ConfigurationManager.AppSettings["NotificheAssimpredilMilanoDownload"]);
                    if (file.Exists)
                    {
                        LabelUltimoFileMilano.Text = String.Format("File aggiornato al: <b>{0}</b>", file.LastWriteTime.ToShortDateString());
                        ButtonDownloadMilano.Enabled = true;
                    }
                    else
                    {
                        LabelUltimoFileMilano.Text = "File non disponibile";
                    }
                }
                catch (Exception exc)
                {
                    LabelUltimoFileMilano.Text = String.Format("Errore durante il recupero del file: {0}", exc.Message);
                }
                #endregion

                #region Lodi
                try
                {
                    FileInfo file = new FileInfo(ConfigurationManager.AppSettings["NotificheAssimpredilLodiDownload"]);
                    if (file.Exists)
                    {
                        LabelUltimoFileLodi.Text = String.Format("File aggiornato al: <b>{0}</b>", file.LastWriteTime.ToShortDateString());
                        ButtonDownloadLodi.Enabled = true;
                    }
                    else
                    {
                        LabelUltimoFileLodi.Text = "File non disponibile";
                    }
                }
                catch (Exception exc)
                {
                    LabelUltimoFileLodi.Text = String.Format("Errore durante il recupero del file: {0}", exc.Message);
                }
                #endregion

                #region Monza Brianza
                try
                {
                    FileInfo file = new FileInfo(ConfigurationManager.AppSettings["NotificheAssimpredilMonzaBrianzaDownload"]);
                    if (file.Exists)
                    {
                        LabelUltimoFileMonzaBrianza.Text = String.Format("File aggiornato al: <b>{0}</b>", file.LastWriteTime.ToShortDateString());
                        ButtonDownloadMonzaBrianza.Enabled = true;
                    }
                    else
                    {
                        LabelUltimoFileMonzaBrianza.Text = "File non disponibile";
                    }
                }
                catch (Exception exc)
                {
                    LabelUltimoFileMonzaBrianza.Text = String.Format("Errore durante il recupero del file: {0}", exc.Message);
                }
                #endregion
            }
        }

        protected void ButtonDownloadMilano_Click(object sender, EventArgs e)
        {
            RestituisciFile("NotificheAssimpredilMilanoDownload");
        }

        protected void ButtonDownloadLodi_Click(object sender, EventArgs e)
        {
            RestituisciFile("NotificheAssimpredilLodiDownload");
        }

        protected void ButtonDownloadMonzaBrianza_Click(object sender, EventArgs e)
        {
            RestituisciFile("NotificheAssimpredilMonzaBrianzaDownload");
        }

        protected void RestituisciFile(String parametroFile)
        {
            String filePath = ConfigurationManager.AppSettings[parametroFile];

            Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", Path.GetFileName(filePath)));
            Response.ContentType = "text/plain";
            Response.TransmitFile(filePath);
            //TransmitFile invece di WriteFile perchè file grosso e transmit non carica in memoria
            Response.Flush();
            Response.End();
        }
    }
}