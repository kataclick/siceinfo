﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari {
    
    
    public partial class RicercaCantieri {
        
        /// <summary>
        /// MenuNotifichePreliminari1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.MenuNotifichePreliminari MenuNotifichePreliminari1;
        
        /// <summary>
        /// TitoloSottotitolo1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.WebControls.TitoloSottotitolo TitoloSottotitolo1;
        
        /// <summary>
        /// NotifichePreliminariRicercaNotificheFilter1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari.WebControls.NotifichePreliminariRicercaNotificheFilter NotifichePreliminariRicercaNotificheFilter1;
        
        /// <summary>
        /// RadioButtonListOrdinamento control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList RadioButtonListOrdinamento;
        
        /// <summary>
        /// ButtonCerca control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonCerca;
        
        /// <summary>
        /// ButtonEstrazione control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ButtonEstrazione;
        
        /// <summary>
        /// RadGridCantieriPerCommittente control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid RadGridCantieriPerCommittente;
        
        /// <summary>
        /// RadGridCantieriPerImpresa control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid RadGridCantieriPerImpresa;
    }
}
