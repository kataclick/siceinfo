﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Microsoft.Reporting.WebForms;
using TBridge.Cemi.Business.Cpt;
using TBridge.Cemi.Cpt.Business;
using TBridge.Cemi.GestioneUtenti.Business;

using TBridge.Cemi.Type.Enums.GestioneUtenti;

namespace Cemi.SiceInfo.Web.CeServizi.NotifichePreliminari
{
    public partial class ReportNotifica : System.Web.UI.Page
    {
        private readonly CptBusiness biz = new CptBusiness();

        protected void Page_Load(object sender, EventArgs e)
        {
            #region Autorizzazioni
            List<FunzionalitaPredefinite> funzionalita = new List<FunzionalitaPredefinite>();

            funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaNotifiche);
            funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaCantieri);
            funzionalita.Add(FunzionalitaPredefinite.NotifichePreliminariRicercaMappa);

            GestioneAutorizzazionePagine.PaginaAutorizzata(funzionalita);
            #endregion

            if (Request.QueryString["idNotifica"] != null && Request.QueryString["visualizzaAltriLavoratori"] != null)
            {
                //String numeroNotifica = Context.Items["NumeroNotifica"].ToString();
                String numeroNotifica = Server.UrlDecode(Request.QueryString["idNotifica"]);
                String visualizzaAltriLavoratori = Request.QueryString["visualizzaAltriLavoratori"];

                ReportViewerNotifica.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUrl"]);
                ReportViewerNotifica.ServerReport.ReportPath = "/ReportNotifichePreliminari/ReportNotificaPreliminare";

                ReportParameter[] listaParam = new ReportParameter[2];
                listaParam[0] = new ReportParameter("numeroNotifica", numeroNotifica.ToString());
                listaParam[1] = new ReportParameter("visualizzaAltrePersone", visualizzaAltriLavoratori);

                ReportViewerNotifica.ServerReport.SetParameters(listaParam);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;
                byte[] bytes = null;

                //PDF
                bytes = ReportViewerNotifica.ServerReport.Render(
                    "PDF", null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";

                Response.AppendHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", numeroNotifica.Replace("/", "-")));
                Response.BinaryWrite(bytes);

                Response.Flush();
                Response.End();
            }
        }
    }
}