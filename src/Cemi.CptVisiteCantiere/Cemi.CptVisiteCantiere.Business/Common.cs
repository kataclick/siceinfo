﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Cemi.CptVisiteCantiere.Data;
using Cemi.CptVisiteCantiere.Type.Collection;
using Cemi.CptVisiteCantiere.Type.Domain;
using System.IO;


namespace Cemi.CptVisiteCantiere.Business
{
    public class Common
    {
        Data.Common data = new Data.Common();

        public Int32 InsertVisiteCollection(VisitaCptCollection vc)
        {
            return data.InsertVisiteCollection(vc);
        }

        public Int32 InsertVisiteCollectionEF(List<CptVisitaCantiere> vc)
        {
            Int32 inserted = 0;

            using (SICEEntities context = new SICEEntities())
            {

                foreach (CptVisitaCantiere v in vc)
                {
                        try
                        {
                            context.CptVisiteCantiere.AddObject(v);
                            Int32 res = context.SaveChanges();
                            inserted = inserted + res;
                        }

                        catch (Exception exc)
                        {
                            context.CptVisiteCantiere.DeleteObject(v);
                            if (exc.InnerException != null)
                            {
                                Console.WriteLine(String.Format("Errore inserimento visita al cantiere {0}\n per il seguente errore generico: {1}", v.IdCantiereCpt, exc.InnerException.Message));
                                Log(String.Format("Errore inserimento visita al cantiere \"{0}\", Indirizzo \"{1} {2} {3} {4}\", data visita \"{5:dd/MM/yyyy}\", numero visita \"{6}\", impresa \"{7}\" partita Iva \"{8}\", \n per il seguente errore generico: {9}", v.IdCantiereCpt, v.Indirizzo, v.Civico, v.Comune, v.CAP, v.DataVisita, v.NumeroVisita, v.RagioneSocialeImpresa, v.PIvaImpresa, exc.InnerException.Message));
                            }
                            else
                            {
                                Console.WriteLine(String.Format("Errore inserimento visita al cantiere {0}\n per il seguente errore generico: {1}", v.IdCantiereCpt, exc.Message));
                                Log(String.Format("Errore inserimento visita al cantiere \"{0}\", Indirizzo \"{1} {2} {3} {4}\", data visita \"{5:dd/MM/yyyy}\", numero visita \"{6}\", impresa \"{7}\" partita Iva \"{8}\", \n per il seguente errore generico: {9}", v.IdCantiereCpt, v.Indirizzo, v.Civico, v.Comune, v.CAP, v.DataVisita, v.NumeroVisita, v.RagioneSocialeImpresa, v.PIvaImpresa, exc.Message));
                            }
                            
                        }

                    }
                 
            }
            Console.WriteLine(String.Format("Inserite {0} visite", inserted.ToString()));
            return vc.Count;
        }

        public bool CheckInserted(String codice, Int32 numVisita, String impresa, String tipo)
        {
            return data.CheckInserted(codice, numVisita, impresa, tipo);
        }

        public void Log(String message)
        {
            String msg = String.Format("{0} ---- {1}", DateTime.Now, message);
            using (StreamWriter writer = new StreamWriter("Log.txt", true))
            {
                writer.WriteLine();
                writer.WriteLine(msg);
            }
            Console.WriteLine();
            int maxlenght = msg.Length;
            if (maxlenght > 255)
            {
                maxlenght = 255;
            }
            Console.WriteLine(msg.Substring(0, maxlenght));
        }
    }
}
