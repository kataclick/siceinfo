﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Collections.Specialized;
using System.Data.Common;
using System.Xml.Serialization;


using Cemi.CptVisiteCantiere.Type.Entities;
using Cemi.CptVisiteCantiere.Type.Collection;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;



namespace Cemi.CptVisiteCantiere.Data
{
    public class Common
    {
        //System.Configuration.ConnectionStringSettings constr = ConfigurationManager.ConnectionStrings["CEMI"];

        
        public Common()
        {
            DatabaseCemi = DatabaseFactory.CreateDatabase("CEMI");
        }

        public Database DatabaseCemi { get; set; }




        public Int32 InsertVisiteCollection(VisitaCptCollection vc)
        {
            //Int32 cont = 0;
            Int32 inserted = 0;


            foreach (VisitaCpt v in vc)
            {
                    using (DbCommand comm = DatabaseCemi.GetStoredProcCommand("dbo.Marco_CptVisiteCantiere_Insert"))
                    {
                        DatabaseCemi.AddInParameter(comm, "@idCantiereCpt", DbType.String, String.IsNullOrEmpty(v.IdCantiereCpt) ? String.Empty : v.IdCantiereCpt);
                        DatabaseCemi.AddInParameter(comm, "@indirizzo", DbType.String, v.Indirizzo);
                        DatabaseCemi.AddInParameter(comm, "@comune", DbType.String, v.Comune);
                        DatabaseCemi.AddInParameter(comm, "@CAP", DbType.String, v.CAP);
                        DatabaseCemi.AddInParameter(comm, "@Civico", DbType.String, String.IsNullOrEmpty(v.Civico) ? String.Empty : v.Civico);
                        DatabaseCemi.AddInParameter(comm, "@numeroVisita", DbType.Int32, v.NumeroVisita);
                        DatabaseCemi.AddInParameter(comm, "@dataVisita", DbType.DateTime, v.DataVisita);
                        DatabaseCemi.AddInParameter(comm, "@ragioneSocialeImpresa", DbType.String, v.RagioneSocialeImpresa);
                        DatabaseCemi.AddInParameter(comm, "@pIvaImpresa", DbType.String, v.PIvaImpresa);
                        DatabaseCemi.AddInParameter(comm, "@tipo", DbType.String, v.Tipo);

                        /*
                        command.Parameters.AddWithValue("@idCantiereCpt", String.IsNullOrEmpty(v.CODICE) ? String.Empty : v.CODICE);
                        command.Parameters.AddWithValue("@indirizzo", v.INDIRIZZO);
                        command.Parameters.AddWithValue("@comune", v.COMUNE);
                        command.Parameters.AddWithValue("@CAP", v.CAP);
                        command.Parameters.AddWithValue("@civico", String.IsNullOrEmpty(v.CIVICO) ? String.Empty : v.CIVICO);
                        command.Parameters.AddWithValue("@numeroVisita", v.NUMVISITA);
                        command.Parameters.AddWithValue("@dataVisita", v.DATAVISITA);
                        command.Parameters.AddWithValue("@ragioneSocialeImpresa", v.impresa);
                        command.Parameters.AddWithValue("@pIvaImpresa", v.pivanumero);
                        command.Parameters.AddWithValue("@tipo", v.TIPO);
                        */

                        try
                        {
                            Int32 res = DatabaseCemi.ExecuteNonQuery(comm);
                            inserted = inserted + res;
                        }
                        catch (SqlException exc)
                        {
                            if (exc.Number == 2601)
                            {
                                Console.WriteLine(String.Format("Errore inserimento visita al cantiere {0}\n per il seguente errore (2601): {1}", v.IdCantiereCpt, exc.Message));
                            }
                            else
                            {
                                Console.WriteLine(String.Format("Errore inserimento visita al cantiere {0}\n per il seguente errore: {1}", v.IdCantiereCpt, exc.Message));
                            }
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(String.Format("Errore inserimento visita al cantiere {0}\n per il seguente errore generico: {1}", v.IdCantiereCpt, exc.Message));
                        }

                    }
                        

                //cont = cont + 1;
            }
            
            Console.WriteLine(String.Format("Inserite {0} visite", inserted.ToString()));
            return vc.Count;
        }

             


        public bool CheckInserted(String codice, Int32 numVisita, String impresa, String tipo)
        {
            bool res = false;
                    
            using (DbCommand command = DatabaseCemi.GetStoredProcCommand("Marco_CptVisiteCantiere_SelectSingola"))
            {
                DatabaseCemi.AddInParameter(command,"@CODICE", DbType.String, codice);
                DatabaseCemi.AddInParameter(command,"@NUMVISITA", DbType.Int32,numVisita);
                DatabaseCemi.AddInParameter(command,"@impresa",DbType.String, impresa);
                DatabaseCemi.AddInParameter(command,"@TIPO",DbType.String, tipo);

                using (IDataReader dr = DatabaseCemi.ExecuteReader(command))
                {
                    if (dr.Read())
                    {
                        res = true;
                    }
                }
            }

            return res;
        }
    }





        /* DbCommand
            using (DbCommand comm = DatabaseCemi.GetStoredProcCommand("dbo.Marco_CptVisiteCantiere_Insert"))
            {
                foreach (VisitaCpt v in vc)
                {
                    DatabaseCemi.AddInParameter(comm, "@idCantiereCpt", DbType.String, v.CODICE);
                    DatabaseCemi.AddInParameter(comm, "@indirizzo", DbType.String, v.INDIRIZZO);
                    DatabaseCemi.AddInParameter(comm, "@comune", DbType.String, v.COMUNE);
                    DatabaseCemi.AddInParameter(comm, "@CAP", DbType.String, v.CAP);
                    DatabaseCemi.AddInParameter(comm, "@Civico", DbType.String, v.CIVICO);
                    DatabaseCemi.AddInParameter(comm, "@numeroVisita", DbType.Int32, v.NUMVISITA);
                    DatabaseCemi.AddInParameter(comm, "@dataVisita", DbType.DateTime, v.DATAVISITA);
                    DatabaseCemi.AddInParameter(comm, "@ragioneSocialeImpresa", DbType.String, v.impresa);
                    DatabaseCemi.AddInParameter(comm, "@pIvaImpresa", DbType.String, v.pivanumero);
                    DatabaseCemi.AddInParameter(comm, "@tipo", DbType.String, v.TIPO);

                    DatabaseCemi.ExecuteNonQuery(comm);
                    cont = cont+1;
                }
            }
         */


}
