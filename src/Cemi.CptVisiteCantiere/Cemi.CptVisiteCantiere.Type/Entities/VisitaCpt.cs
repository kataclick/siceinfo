﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.CptVisiteCantiere.Type.Entities
{
    public class VisitaCpt
    {
        public String IdCantiereCpt { get; set; }

        public String Indirizzo { get; set; }

        public String Comune { get; set; }

        public String CAP { get; set; }

        public String Civico { get; set; }

        public Int32? NumeroVisita { get; set; }

        public DateTime? DataVisita { get; set; }

        public String RagioneSocialeImpresa { get; set; }

        public String PIvaImpresa { get; set; }

        public String Tipo { get; set; }
    }
}
