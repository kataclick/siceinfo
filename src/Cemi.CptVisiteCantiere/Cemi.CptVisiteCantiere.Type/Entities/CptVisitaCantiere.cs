﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.CptVisiteCantiere.Type.Entities
{
    public partial class CptVisitaCantierelll
    {
        #region Primitive Properties

        public virtual int IdCptVisiteCantiere
        {
            get;
            set;
        }

        public virtual string IdCantiereCpt
        {
            get;
            set;
        }

        public virtual string Indirizzo
        {
            get;
            set;
        }

        public virtual string Comune
        {
            get;
            set;
        }

        public virtual string CAP
        {
            get;
            set;
        }

        public virtual string Civico
        {
            get;
            set;
        }

        public virtual Nullable<int> NumeroVisita
        {
            get;
            set;
        }

        public virtual Nullable<System.DateTime> DataVisita
        {
            get;
            set;
        }

        public virtual string RagioneSocialeImpresa
        {
            get;
            set;
        }

        public virtual string PivaImpresa
        {
            get;
            set;
        }

        public virtual string Tipo
        {
            get;
            set;
        }

        public virtual Nullable<System.DateTime> DataInserimentoRecord
        {
            get;
            set;
        }

        public virtual Nullable<System.DateTime> DataModificaRecord
        {
            get;
            set;
        }

        #endregion
    }
}
