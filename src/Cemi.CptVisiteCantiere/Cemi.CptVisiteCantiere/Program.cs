﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Configuration;
using System.IO;
using System.Data.SqlClient;
using Cemi.CptVisiteCantiere.CptVisiteCantiereService;
using Cemi.CptVisiteCantiere.Type.Collection;
using Cemi.CptVisiteCantiere.Type.Entities;
using Cemi.CptVisiteCantiere.Business;
using Cemi.CptVisiteCantiere.Type.Domain;


namespace Cemi.CptVisiteCantiere.ConsoleApp
{
    public class Program
    {
        wsServicesCptSoapClient cptService = new wsServicesCptSoapClient();
        static private Common biz = new Common();
        

        static void Main()
        {
            Program prg = new Program();

            Console.WriteLine("-- START --");
            Console.WriteLine(" ");
            Console.WriteLine(" ");

            VisitaCptCollection cptVisite = prg.CaricaVisite();
            //List<CptVisitaCantiere> cptVisite = prg.CaricaVisiteEF();
            biz.InsertVisiteCollection(cptVisite);
            //biz.InsertVisiteCollectionEF(cptVisite);

            //Console.WriteLine(" ");
            //Console.WriteLine(" ");
            //Console.WriteLine("PREMERE INVIO");
            //Console.ReadKey();            

        }

        public VisitaCptCollection CaricaVisite()
        {
            //DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime startDate = new DateTime(2014, 1, 1);
            DateTime endDate = DateTime.Now;
            

            Console.WriteLine("-- ACCESSO AL WS --");
            VisitaCptCollection vc = new VisitaCptCollection();
            DataSet ds = new DataSet();
            ds = cptService.CptMi_DatiVisita(startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"), null, null, "CptOspite9_", "BlvtVgRUab51FSGWZGxhpw==");
            
            DataTable dt = ds.Tables[0];

            Console.WriteLine("-- Numero righe recuperate da CPT: {0} --", dt.Rows.Count.ToString());

            DataTable dtNew = new DataTable("CptVisite");
            //DataView dv = new DataView(dt);
            //dv.RowFilter = "CODICE LIKE '"+row["CODICE"].ToString()+"' AND NUMVISITA = "+row["NUMVISITA"].ToString()+" AND impresa LIKE '"+row["impresa"].ToString().Replace("'","''")+"' AND TIPO LIKE '"+row["TIPO"].ToString()+"'";
            dtNew = dt.DefaultView.ToTable(true);   //.DefaultView.ToTable(true);


            foreach (DataRow row in dtNew.Rows)
            {                
                if (!biz.CheckInserted(row["CODICE"].ToString(), (Int32)row["NUMVISITA"], row["impresa"].ToString(), row["TIPO"].ToString()))
                {
                    VisitaCpt vs = new VisitaCpt();

                    vs.IdCantiereCpt = row["CODICE"].ToString().Trim();
                    if (row["CAP"].ToString().Length > 5)
                    {
                        vs.CAP = row["CAP"].ToString().Substring(0, 5);
                    }
                    {
                        vs.CAP = row["CAP"].ToString().Trim();
                    }
                    vs.Comune = row["COMUNE"].ToString().Trim();
                    vs.Indirizzo = row["INDIRIZZO"].ToString().Trim().Replace("/", " "); //.Replace("'","''");
                    vs.Civico = row["CIVICO"].ToString().Trim();
                    vs.NumeroVisita = (Int32)row["NUMVISITA"];
                    vs.DataVisita = DateTime.Parse(row["DATAVISITA"].ToString());
                    vs.RagioneSocialeImpresa = row["impresa"].ToString().Trim();
                    if (row["pivanumero"].ToString().Length > 16)
                    {
                        vs.PIvaImpresa = row["pivanumero"].ToString().Substring(0, 16);
                    }
                    else
                    {
                        vs.PIvaImpresa = row["pivanumero"].ToString().Trim();
                    }
                    vs.Tipo = row["TIPO"].ToString().Trim();

                    /*
                    vs.IdCantiereCpt = row["CODICE"].ToString();
                    vs.CAP = row["CAP"].ToString();
                    vs.Comune = row["COMUNE"].ToString();
                    vs.Indirizzo = row["INDIRIZZO"].ToString();
                    vs.Civico = row["CIVICO"].ToString();
                    vs.NumeroVisita = (Int32)row["NUMVISITA"];
                    vs.DataVisita = DateTime.Parse(row["DATAVISITA"].ToString());
                    vs.RagioneSocialeImpresa = row["impresa"].ToString();
                    vs.PIvaImpresa = row["pivanumero"].ToString();
                    vs.Tipo = row["TIPO"].ToString();
                    */
                    vc.Add(vs);
                }
                
            }
            Console.WriteLine("-- Numero righe nuove da inserire: {0} --", vc.Count.ToString());            
            return vc;
        }

        public List<CptVisitaCantiere> CaricaVisiteEF()
        {
            //DateTime startDate = DateTime.Now.AddMonths(-1);
            DateTime startDate = new DateTime(2014, 1, 1);
            DateTime endDate = DateTime.Now;


            Console.WriteLine("-- ACCESSO AL WS --");
            List<CptVisitaCantiere> vc = new List<CptVisitaCantiere>();
            DataSet ds = new DataSet();
            ds = cptService.CptMi_DatiVisita(startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"), null, null, "CptOspite9_", "BlvtVgRUab51FSGWZGxhpw==");

            DataTable dt = ds.Tables[0];

            Console.WriteLine("-- Numero righe recuperate da CPT: {0} --", dt.Rows.Count.ToString());

            DataTable dtNew = new DataTable("CptVisite");
            //DataView dv = new DataView(dt);
            //dv.RowFilter = "CODICE LIKE '"+row["CODICE"].ToString()+"' AND NUMVISITA = "+row["NUMVISITA"].ToString()+" AND impresa LIKE '"+row["impresa"].ToString().Replace("'","''")+"' AND TIPO LIKE '"+row["TIPO"].ToString()+"'";
            dtNew = dt.DefaultView.ToTable(true);   //.DefaultView.ToTable(true);


            foreach (DataRow row in dtNew.Rows)
            {
                if (!biz.CheckInserted(row["CODICE"].ToString(), (Int32)row["NUMVISITA"], row["impresa"].ToString(), row["TIPO"].ToString()))
                {
                    CptVisitaCantiere vs = new CptVisitaCantiere();
                    vs.IdCantiereCpt = row["CODICE"].ToString().Trim();
                    if (row["CAP"].ToString().Length > 5)
                    {
                        vs.CAP = row["CAP"].ToString().Substring(0, 5);
                    }
                    {
                        vs.CAP = row["CAP"].ToString().Trim();
                    }
                    vs.Comune = row["COMUNE"].ToString().Trim();
                    vs.Indirizzo = row["INDIRIZZO"].ToString().Trim().Replace("/", " "); //.Replace("'","''");
                    vs.Civico = row["CIVICO"].ToString().Trim();
                    vs.NumeroVisita = (Int32)row["NUMVISITA"];
                    vs.DataVisita = DateTime.Parse(row["DATAVISITA"].ToString());
                    vs.RagioneSocialeImpresa = row["impresa"].ToString().Trim();
                    if (row["pivanumero"].ToString().Length > 16)
                    {
                        vs.PIvaImpresa = row["pivanumero"].ToString().Substring(0, 16);
                    }
                    else
                    {
                        vs.PIvaImpresa = row["pivanumero"].ToString().Trim();
                    }
                    vs.Tipo = row["TIPO"].ToString().Trim();
                    vs.DataInserimentoRecord = DateTime.Now;

                    vc.Add(vs);
                }
            }
            Console.WriteLine("-- Numero righe nuove da inserire: {0} --", vc.Count.ToString());
            return vc;
        }

    }
}
