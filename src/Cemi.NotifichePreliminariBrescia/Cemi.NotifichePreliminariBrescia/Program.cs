﻿using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Cemi.NotifichePreliminariBrescia
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string provincia;
            string path;

            #region Controllo args
            if (args != null && args.Length > 0)
            {
                provincia = args[0];
                path = args[1];

            }
            else
            {
                //Impostiamo il default
                provincia = "BS";
                path = ConfigurationManager.AppSettings["Path"];
            }
            #endregion

            using (SICEEntities context = new SICEEntities())
            {
                var query = from notificheTelematiche in context.CptNotificheTelematiche
                            join notificaIndirizzi in context.CptNotificaIndirizzi on notificheTelematiche.idCptNotifica
                                equals notificaIndirizzi.idCptNotifica
                            join indirizzi in context.CptIndirizzi on notificaIndirizzi.idCptIndirizzo equals
                                indirizzi.idCptIndirizzo
                            join notificaTelematicaCommittenti in context.CptNotificaTelematicaCommittenti on
                                notificheTelematiche.idCptNotificaTelematicaCommittente equals
                                notificaTelematicaCommittenti.idCptNotificaTelematicaCommittente
                            join pcp in context.CptPersone on notificheTelematiche.idCptPersonaCoordinatoreProgettazione
                                equals pcp.idCptPersona into persona1
                            from pcp in persona1.DefaultIfEmpty()
                            join pcr in context.CptPersone on notificheTelematiche.idCptPersonaCoordinatoreRealizzazione
                                equals pcr.idCptPersona into persona2
                            from pcr in persona2.DefaultIfEmpty()
                            join pdl in context.CptPersone on notificheTelematiche.idCptPersonaDirettoreLavori equals
                                pdl.idCptPersona into persona3
                            from pdl in persona3.DefaultIfEmpty()
                            join notificaTelematicaSubappalti in context.CptNotificaTelematicaSubappalti on
                                notificheTelematiche.idCptNotifica equals notificaTelematicaSubappalti.idCptNotifica
                                into sub
                            from notificaTelematicaSubappalti in sub.DefaultIfEmpty()
                            join ntis in context.CptNotificaTelematicaImprese on
                                notificaTelematicaSubappalti.idCptNotificaTelematicaImpresaSelezionata equals
                                ntis.idCptNotificaTelematicaImpresa into sub2
                            from ntis in sub2.DefaultIfEmpty()
                            join ntia in context.CptNotificaTelematicaImprese on
                                notificaTelematicaSubappalti.idCptNotificaTelematicaImpresaAppaltataDa equals
                                ntia.idCptNotificaTelematicaImpresa into sub3
                            from ntia in sub3.DefaultIfEmpty()
                            where indirizzi.provincia == provincia
                            //join notificaTelematicaImprese2 in context.CptNotificaTelematicaImprese on notificaTelematicaSubappalti.idCptNotificaTelematicaImpresaAppaltataDa equals notificaTelematicaImprese2.idCptNotificaTelematicaImpresa into garba2
                            //from notificaTelematicaImprese2 in garba2.DefaultIfEmpty()
                            select
                                new
                                    {
                                        indirizzi,
                                        notificheTelematiche,
                                        notificaTelematicaCommittenti,
                                        pcp,
                                        pcr,
                                        pdl,
                                        ntis,
                                        ntia
                                    };

                StringBuilder sb = new StringBuilder();

                bool primo = true;

                foreach (var v in query)
                {
                    #region intestazione
                    if (primo)
                    {
                        sb.AppendFormat("indirizzo;");
                        sb.AppendFormat("civico;");
                        sb.AppendFormat("comune;");
                        sb.AppendFormat("provincia;");
                        sb.AppendFormat("cap;");
                        sb.AppendFormat("latitudine;");
                        sb.AppendFormat("longitudine;");
                        sb.AppendFormat("dataInizioLavori;");

                        sb.AppendFormat("descrizioneDurata;");
                        sb.AppendFormat("numeroDurata;");
                        sb.AppendFormat("numeroMassimoLavoratori;");

                        sb.AppendFormat("protocolloRegione;");
                        sb.AppendFormat("data;");
                        sb.AppendFormat("naturaOpera;");
                        sb.AppendFormat("tipoOpera;");
                        sb.AppendFormat("numeroAppalto;");
                        sb.AppendFormat("numeroPrevistoImprese;");
                        sb.AppendFormat("numeroLavoratoriAutonomi;");
                        sb.AppendFormat("ammontareComplessivo;");
                        sb.AppendFormat("responsabileLavoriCommittente;");
                        sb.AppendFormat("note;");

                        sb.AppendFormat("CommittenteCognome;");
                        sb.AppendFormat("CommittenteNome;");
                        sb.AppendFormat("CommittenteCodiceFiscale;");
                        sb.AppendFormat("CommittenteIndirizzo;");
                        sb.AppendFormat("CommittenteComune;");
                        sb.AppendFormat("CommittenteProvincia;");
                        sb.AppendFormat("CommittenteEmail;");

                        sb.AppendFormat("CoordinatoreProgettazioneCognome;");
                        sb.AppendFormat("CoordinatoreProgettazioneNome;");
                        sb.AppendFormat("CoordinatoreProgettazioneCodiceFiscale;");
                        sb.AppendFormat("CoordinatoreProgettazioneIndirizzo;");
                        sb.AppendFormat("CoordinatoreProgettazioneComune;");
                        sb.AppendFormat("CoordinatoreProgettazioneProvincia;");
                        sb.AppendFormat("CoordinatoreProgettazioneEmail;");

                        sb.AppendFormat("CoordinatoreRealizzazioneCognome;");
                        sb.AppendFormat("CoordinatoreRealizzazioneNome;");
                        sb.AppendFormat("CoordinatoreRealizzazioneCodiceFiscale;");
                        sb.AppendFormat("CoordinatoreRealizzazioneIndirizzo;");
                        sb.AppendFormat("CoordinatoreRealizzazioneComune;");
                        sb.AppendFormat("CoordinatoreRealizzazioneProvincia;");
                        sb.AppendFormat("CoordinatoreRealizzazioneEmail;");

                        sb.AppendFormat("DirettoreLavoriCognome;");
                        sb.AppendFormat("DirettoreLavoriNome;");
                        sb.AppendFormat("DirettoreLavoriCodiceFiscale;");
                        sb.AppendFormat("DirettoreLavoriIndirizzo;");
                        sb.AppendFormat("DirettoreLavoriComune;");
                        sb.AppendFormat("DirettoreLavoriProvincia;");
                        sb.AppendFormat("DirettoreLavoriEmail;");

                        sb.AppendFormat("ImpresaSelezionataRagioneSociale;");
                        sb.AppendFormat("ImpresaSelezionataPartitaIva;");
                        sb.AppendFormat("ImpresaSelezionataCodiceFiscale;");
                        sb.AppendFormat("ImpresaSelezionataMatricolaINAIL;");
                        sb.AppendFormat("ImpresaSelezionataMatricolaINPS;");
                        sb.AppendFormat("ImpresaSelezionataIndirizzo;");
                        sb.AppendFormat("ImpresaSelezionataProvincia;");
                        sb.AppendFormat("ImpresaSelezionataComune;");
                        sb.AppendFormat("ImpresaSelezionataTelefono;");

                        sb.AppendFormat("ImpresaAppaltataDaRagioneSociale;");
                        sb.AppendFormat("ImpresaAppaltataDaPartitaIva;");
                        sb.AppendFormat("ImpresaAppaltataDaCodiceFiscale;");
                        sb.AppendFormat("ImpresaAppaltataDaMatricolaINAIL;");
                        sb.AppendFormat("ImpresaAppaltataDaMatricolaINPS;");
                        sb.AppendFormat("ImpresaAppaltataDaIndirizzo;");
                        sb.AppendFormat("ImpresaAppaltataDaProvincia;");
                        sb.AppendFormat("ImpresaAppaltataDaComune;");
                        sb.AppendFormat("ImpresaAppaltataDaTelefono;");

                        sb.AppendLine();
                        primo = false;
                    }

                    #endregion

                    sb.AppendFormat("{0};", v.indirizzi.indirizzo);
                    sb.AppendFormat("{0};", v.indirizzi.civico);
                    sb.AppendFormat("{0};", v.indirizzi.comune);
                    sb.AppendFormat("{0};", v.indirizzi.provincia);
                    sb.AppendFormat("{0};", v.indirizzi.cap);
                    sb.AppendFormat("{0};", v.indirizzi.latitudine);
                    sb.AppendFormat("{0};", v.indirizzi.longitudine);
                    sb.AppendFormat("{0};",
                                    v.indirizzi.dataInizioLavori.HasValue
                                        ? v.indirizzi.dataInizioLavori.Value.ToShortDateString()
                                        : string.Empty);
                    sb.AppendFormat("{0};", v.indirizzi.descrizioneDurata);
                    sb.AppendFormat("{0};", v.indirizzi.numeroDurata);
                    sb.AppendFormat("{0};", v.indirizzi.numeroMassimoLavoratori);

                    sb.AppendFormat("{0};", v.notificheTelematiche.protocolloRegione);
                    sb.AppendFormat("{0};", v.notificheTelematiche.data.Date.ToShortDateString());
                    if (string.IsNullOrEmpty(v.notificheTelematiche.naturaOpera))
                        sb.AppendFormat("{0};", v.notificheTelematiche.naturaOpera);
                    else
                        sb.AppendFormat("{0};", v.notificheTelematiche.naturaOpera.Replace("\n", " "));
                    sb.AppendFormat("{0};",
                                    v.notificheTelematiche.operaPubblica.HasValue
                                    ? (v.notificheTelematiche.operaPubblica.Value ? "Pubblica" : "Privata")
                                        : string.Empty);
                    sb.AppendFormat("{0};", v.notificheTelematiche.numeroAppalto);
                    sb.AppendFormat("{0};", v.notificheTelematiche.numeroPrevistoImprese);
                    sb.AppendFormat("{0};", v.notificheTelematiche.numeroLavoratoriAutonomi);
                    sb.AppendFormat("{0};", v.notificheTelematiche.ammontareComplessivo);
                    sb.AppendFormat("{0};", v.notificheTelematiche.responsabileLavoriCommittente);
                    if (string.IsNullOrEmpty(v.notificheTelematiche.note))
                        sb.AppendFormat("{0};", v.notificheTelematiche.note);
                    else
                        sb.AppendFormat("{0};", v.notificheTelematiche.note.Replace("\n", " "));
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaCognome);
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaNome);
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaCodiceFiscale);
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaIndirizzo);
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaComune);
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaProvincia);
                    sb.AppendFormat("{0};", v.notificaTelematicaCommittenti.personaEmail);

                    if (v.pcp != null)
                    {
                        sb.AppendFormat("{0};", v.pcp.cognome);
                        sb.AppendFormat("{0};", v.pcp.nome);
                        sb.AppendFormat("{0};", v.pcp.codiceFiscale);
                        sb.AppendFormat("{0};", v.pcp.indirizzo);
                        sb.AppendFormat("{0};", v.pcp.comune);
                        sb.AppendFormat("{0};", v.pcp.provincia);
                        sb.AppendFormat("{0};", v.pcp.email);
                    }
                    else
                    {
                        sb.AppendFormat("{0};{0};{0};{0};{0};{0};{0};", string.Empty);
                    }

                    if (v.pcr != null)
                    {
                        sb.AppendFormat("{0};", v.pcr.cognome);
                        sb.AppendFormat("{0};", v.pcr.nome);
                        sb.AppendFormat("{0};", v.pcr.codiceFiscale);
                        sb.AppendFormat("{0};", v.pcr.indirizzo);
                        sb.AppendFormat("{0};", v.pcr.comune);
                        sb.AppendFormat("{0};", v.pcr.provincia);
                        sb.AppendFormat("{0};", v.pcr.email);
                    }

                    else
                    {
                        sb.AppendFormat("{0};{0};{0};{0};{0};{0};{0};", string.Empty);
                    }

                    if (v.pdl != null)
                    {
                        sb.AppendFormat("{0};", v.pdl.cognome);
                        sb.AppendFormat("{0};", v.pdl.nome);
                        sb.AppendFormat("{0};", v.pdl.codiceFiscale);
                        sb.AppendFormat("{0};", v.pdl.indirizzo);
                        sb.AppendFormat("{0};", v.pdl.comune);
                        sb.AppendFormat("{0};", v.pdl.provincia);
                        sb.AppendFormat("{0};", v.pdl.email);
                    }

                    else
                    {
                        sb.AppendFormat("{0};{0};{0};{0};{0};{0};{0};", string.Empty);
                    }

                    if (v.ntis != null)
                    {
                        sb.AppendFormat("{0};", v.ntis.ragioneSociale);
                        sb.AppendFormat("{0};", v.ntis.partitaIva);
                        sb.AppendFormat("{0};", v.ntis.codiceFiscale);
                        sb.AppendFormat("{0};", v.ntis.matricolaINAIL);
                        sb.AppendFormat("{0};", v.ntis.matricolaINPS);
                        sb.AppendFormat("{0};", v.ntis.indirizzo);
                        sb.AppendFormat("{0};", v.ntis.provincia);
                        sb.AppendFormat("{0};", v.ntis.comune);
                        sb.AppendFormat("{0};", v.ntis.telefono);
                    }

                    else
                    {
                        sb.AppendFormat("{0};{0};{0};{0};{0};{0};{0};{0};{0};", string.Empty);
                    }

                    if (v.ntia != null)
                    {
                        sb.AppendFormat("{0};", v.ntia.ragioneSociale);
                        sb.AppendFormat("{0};", v.ntia.partitaIva);
                        sb.AppendFormat("{0};", v.ntia.codiceFiscale);
                        sb.AppendFormat("{0};", v.ntia.matricolaINAIL);
                        sb.AppendFormat("{0};", v.ntia.matricolaINPS);
                        sb.AppendFormat("{0};", v.ntia.indirizzo);
                        sb.AppendFormat("{0};", v.ntia.provincia);
                        sb.AppendFormat("{0};", v.ntia.comune);
                        sb.AppendFormat("{0};", v.ntia.telefono);
                    }

                    else
                    {
                        sb.AppendFormat("{0};{0};{0};{0};{0};{0};{0};{0};{0};", string.Empty);
                    }


                    sb.AppendLine();
                }

                using (StreamWriter file =
                    new StreamWriter(path))
                {
                    file.Write(sb.ToString());
                }
            }
        }
    }
}