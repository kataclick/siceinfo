//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.NotifichePreliminariBrescia
{
    public partial class CptIndirizzo
    {
        #region Primitive Properties
    
        public virtual int idCptIndirizzo
        {
            get;
            set;
        }
    
        public virtual string indirizzo
        {
            get;
            set;
        }
    
        public virtual string civico
        {
            get;
            set;
        }
    
        public virtual string comune
        {
            get;
            set;
        }
    
        public virtual string provincia
        {
            get;
            set;
        }
    
        public virtual string cap
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> latitudine
        {
            get;
            set;
        }
    
        public virtual Nullable<decimal> longitudine
        {
            get;
            set;
        }
    
        public virtual string infoAggiuntiva
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> dataInserimentoRecord
        {
            get;
            set;
        }
    
        public virtual Nullable<int> idCantiere
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> dataInizioLavori
        {
            get;
            set;
        }
    
        public virtual string descrizioneDurata
        {
            get;
            set;
        }
    
        public virtual Nullable<int> numeroDurata
        {
            get;
            set;
        }
    
        public virtual Nullable<int> numeroMassimoLavoratori
        {
            get;
            set;
        }
    
        public virtual string indirizzoGeocoder
        {
            get;
            set;
        }
    
        public virtual string civicoGeocoder
        {
            get;
            set;
        }
    
        public virtual string provinciaGeocoder
        {
            get;
            set;
        }
    
        public virtual string comuneGeocoder
        {
            get;
            set;
        }
    
        public virtual string capGeocoder
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<CptNotificaIndirizzo> CptNotificaIndirizzi
        {
            get
            {
                if (_cptNotificaIndirizzi == null)
                {
                    var newCollection = new FixupCollection<CptNotificaIndirizzo>();
                    newCollection.CollectionChanged += FixupCptNotificaIndirizzi;
                    _cptNotificaIndirizzi = newCollection;
                }
                return _cptNotificaIndirizzi;
            }
            set
            {
                if (!ReferenceEquals(_cptNotificaIndirizzi, value))
                {
                    var previousValue = _cptNotificaIndirizzi as FixupCollection<CptNotificaIndirizzo>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCptNotificaIndirizzi;
                    }
                    _cptNotificaIndirizzi = value;
                    var newValue = value as FixupCollection<CptNotificaIndirizzo>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCptNotificaIndirizzi;
                    }
                }
            }
        }
        private ICollection<CptNotificaIndirizzo> _cptNotificaIndirizzi;

        #endregion
        #region Association Fixup
    
        private void FixupCptNotificaIndirizzi(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CptNotificaIndirizzo item in e.NewItems)
                {
                    item.CptIndirizzi = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CptNotificaIndirizzo item in e.OldItems)
                {
                    if (ReferenceEquals(item.CptIndirizzi, this))
                    {
                        item.CptIndirizzi = null;
                    }
                }
            }
        }

        #endregion
    }
}
