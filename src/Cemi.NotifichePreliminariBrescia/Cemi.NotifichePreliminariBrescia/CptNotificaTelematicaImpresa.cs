//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Cemi.NotifichePreliminariBrescia
{
    public partial class CptNotificaTelematicaImpresa
    {
        #region Primitive Properties
    
        public virtual int idCptNotificaTelematicaImpresa
        {
            get;
            set;
        }
    
        public virtual string ragioneSociale
        {
            get;
            set;
        }
    
        public virtual bool lavoratoreAutonomo
        {
            get;
            set;
        }
    
        public virtual string partitaIva
        {
            get;
            set;
        }
    
        public virtual string codiceFiscale
        {
            get;
            set;
        }
    
        public virtual string attivitaPrevalente
        {
            get;
            set;
        }
    
        public virtual Nullable<int> idImpresa
        {
            get;
            set;
        }
    
        public virtual Nullable<int> idCptNotificaTelematicaImpresaAnagrafica
        {
            get;
            set;
        }
    
        public virtual string idCassaEdile
        {
            get;
            set;
        }
    
        public virtual string matricolaINAIL
        {
            get;
            set;
        }
    
        public virtual string matricolaINPS
        {
            get;
            set;
        }
    
        public virtual string matricolaCCIAA
        {
            get;
            set;
        }
    
        public virtual string indirizzo
        {
            get;
            set;
        }
    
        public virtual string comune
        {
            get;
            set;
        }
    
        public virtual string provincia
        {
            get;
            set;
        }
    
        public virtual string cap
        {
            get;
            set;
        }
    
        public virtual string telefono
        {
            get;
            set;
        }
    
        public virtual string fax
        {
            get;
            set;
        }

        #endregion
        #region Navigation Properties
    
        public virtual ICollection<CptNotificaTelematicaSubappalto> CptNotificaTelematicaSubappalti
        {
            get
            {
                if (_cptNotificaTelematicaSubappalti == null)
                {
                    var newCollection = new FixupCollection<CptNotificaTelematicaSubappalto>();
                    newCollection.CollectionChanged += FixupCptNotificaTelematicaSubappalti;
                    _cptNotificaTelematicaSubappalti = newCollection;
                }
                return _cptNotificaTelematicaSubappalti;
            }
            set
            {
                if (!ReferenceEquals(_cptNotificaTelematicaSubappalti, value))
                {
                    var previousValue = _cptNotificaTelematicaSubappalti as FixupCollection<CptNotificaTelematicaSubappalto>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCptNotificaTelematicaSubappalti;
                    }
                    _cptNotificaTelematicaSubappalti = value;
                    var newValue = value as FixupCollection<CptNotificaTelematicaSubappalto>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCptNotificaTelematicaSubappalti;
                    }
                }
            }
        }
        private ICollection<CptNotificaTelematicaSubappalto> _cptNotificaTelematicaSubappalti;
    
        public virtual ICollection<CptNotificaTelematicaSubappalto> CptNotificaTelematicaSubappalti1
        {
            get
            {
                if (_cptNotificaTelematicaSubappalti1 == null)
                {
                    var newCollection = new FixupCollection<CptNotificaTelematicaSubappalto>();
                    newCollection.CollectionChanged += FixupCptNotificaTelematicaSubappalti1;
                    _cptNotificaTelematicaSubappalti1 = newCollection;
                }
                return _cptNotificaTelematicaSubappalti1;
            }
            set
            {
                if (!ReferenceEquals(_cptNotificaTelematicaSubappalti1, value))
                {
                    var previousValue = _cptNotificaTelematicaSubappalti1 as FixupCollection<CptNotificaTelematicaSubappalto>;
                    if (previousValue != null)
                    {
                        previousValue.CollectionChanged -= FixupCptNotificaTelematicaSubappalti1;
                    }
                    _cptNotificaTelematicaSubappalti1 = value;
                    var newValue = value as FixupCollection<CptNotificaTelematicaSubappalto>;
                    if (newValue != null)
                    {
                        newValue.CollectionChanged += FixupCptNotificaTelematicaSubappalti1;
                    }
                }
            }
        }
        private ICollection<CptNotificaTelematicaSubappalto> _cptNotificaTelematicaSubappalti1;

        #endregion
        #region Association Fixup
    
        private void FixupCptNotificaTelematicaSubappalti(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CptNotificaTelematicaSubappalto item in e.NewItems)
                {
                    item.CptNotificaTelematicaImprese = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CptNotificaTelematicaSubappalto item in e.OldItems)
                {
                    if (ReferenceEquals(item.CptNotificaTelematicaImprese, this))
                    {
                        item.CptNotificaTelematicaImprese = null;
                    }
                }
            }
        }
    
        private void FixupCptNotificaTelematicaSubappalti1(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (CptNotificaTelematicaSubappalto item in e.NewItems)
                {
                    item.CptNotificaTelematicaImprese1 = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (CptNotificaTelematicaSubappalto item in e.OldItems)
                {
                    if (ReferenceEquals(item.CptNotificaTelematicaImprese1, this))
                    {
                        item.CptNotificaTelematicaImprese1 = null;
                    }
                }
            }
        }

        #endregion
    }
}
