using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Linq;
using System.Windows.Forms;
using Cemi.InfoSindacati.Type.Entities;
using Cemi.InfoSindacati.Type.Enums;

namespace Cemi.InfoSindacati.Data
{
    public class DataProvider
    {
        private static string ConnectionStringCompact => ConfigurationManager.ConnectionStrings["SiceCompact"].ConnectionString.Replace(
            "|DataDirectory|", Application.StartupPath);

        #region Lavoratori

        public List<Lavoratore> GetLavoratori(LavoratoreFilter filter)
        {
            List<Lavoratore> lavoratori = new List<Lavoratore>();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                string commandText = "SELECT idLavoratore, sindacato, cognome, nome, codiceFiscale, "
                                     + "dataNascita, indirizzoDenominazione, indirizzoComune, indirizzoCap, "
                                     + "indirizzoProvincia, idImpresa, ragioneSociale, tipoFineRapporto, tipoContratto "
                                     + "FROM LavoratoriDeleghe "
                                     + "WHERE 1 = 1";

                if (filter.IdLavoratore.HasValue)
                {
                    string idLavOp;
                    switch (filter.IdLavoratoreOperatore)
                    {
                        case Operatori.Uguale:
                            idLavOp = "=";
                            break;
                        case Operatori.Maggiore:
                            idLavOp = ">";
                            break;
                        case Operatori.Minore:
                            idLavOp = "<";
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    commandText += $" AND idLavoratore {idLavOp} {filter.IdLavoratore}";
                }
                if (!String.IsNullOrEmpty(filter.Cognome))
                    commandText += " AND cognome LIKE '%" + filter.Cognome + "%'";
                if (!String.IsNullOrEmpty(filter.Nome))
                    commandText += " AND nome LIKE '%" + filter.Nome + "%'";
                if (!String.IsNullOrEmpty(filter.CodiceFiscale))
                    commandText += " AND codiceFiscale = '" + filter.CodiceFiscale + "'";
                if (filter.DataNascita.HasValue)
                    commandText += " AND dataNascita = @dataNascita";
                if (!String.IsNullOrEmpty(filter.IndirizzoDenominazione))
                    commandText += " AND indirizzoDenominazione LIKE '%" + filter.IndirizzoDenominazione + "%'";
                if (!String.IsNullOrEmpty(filter.IndirizzoComune))
                    commandText += " AND indirizzoComune LIKE '%" + filter.IndirizzoComune + "%'";
                if (!String.IsNullOrEmpty(filter.IndirizzoCap))
                    commandText += " AND indirizzoCap = '" + filter.IndirizzoCap + "'";
                if (!String.IsNullOrEmpty(filter.IndirizzoProvincia))
                    commandText += " AND indirizzoProvincia = '" + filter.IndirizzoProvincia + "'";
                if (filter.IdImpresa.HasValue)
                {
                    string idImpOp;
                    switch (filter.IdImpresaOperatore)
                    {
                        case Operatori.Uguale:
                            idImpOp = "=";
                            break;
                        case Operatori.Maggiore:
                            idImpOp = ">";
                            break;
                        case Operatori.Minore:
                            idImpOp = "<";
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    commandText += $" AND idImpresa {idImpOp} {filter.IdImpresa}";
                }
                if (!String.IsNullOrEmpty(filter.RagioneSociale))
                    commandText += " AND ragioneSociale LIKE '%" + filter.RagioneSociale + "%'";
                if (filter.Sindacato != "Tutti")
                {
                    if (filter.Sindacato == "Non iscritto")
                        commandText += " AND sindacato is null";
                    else
                        commandText += " AND sindacato = '" + filter.Sindacato + "'";
                }
                if (filter.TipoFineRapporto != "Tutti")
                    commandText += " AND tipoFineRapporto = '" + filter.TipoFineRapporto + "'";
                if (filter.ComprensorioSindacale != "Tutti")
                    commandText += " AND comprensorioSindacale = '" + filter.ComprensorioSindacale + "'";
                switch (filter.Attivo)
                {
                    case 1:
                        commandText += " AND dataFineValiditaRapporto > getdate()";
                        break;
                    case 0:
                        commandText += " AND (dataFineValiditaRapporto <= getdate() OR (dataFineValiditaRapporto IS NULL))";
                        break;
                }
                switch (filter.ConIban)
                {
                    case 1:
                        commandText += " AND ibanPresente = 1";
                        break;
                    case 0:
                        commandText += " AND ibanPresente = 0";
                        break;
                }

                switch (filter.Importi)
                {
                    case 1:
                        commandText += " AND conImporti = 1";
                        break;
                    case 0:
                        commandText += " AND conImporti = 0";
                        break;
                }

                switch (filter.ConDenuncia)
                {
                    case 1:
                        commandText += " AND oreInDenuncia = 1";
                        break;
                    case 0:
                        commandText += " AND oreInDenuncia = 0";
                        break;
                }

                switch (filter.ConUtenza)
                {
                    case 1:
                        commandText += " AND utenteRegistrato = 1";
                        break;
                    case 0:
                        commandText += " AND utenteRegistrato = 0";
                        break;
                }

                commandText += " ORDER BY cognome, nome";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    command.Parameters.AddWithValue("@dataNascita", filter.DataNascita);
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        int indexIdLavoratore = reader.GetOrdinal("idLavoratore");
                        int indexCognome = reader.GetOrdinal("cognome");
                        int indexNome = reader.GetOrdinal("nome");
                        int indexCodiceFiscale = reader.GetOrdinal("codiceFiscale");
                        int indexDataNascita = reader.GetOrdinal("dataNascita");
                        int indexIndirizzoDenominazione = reader.GetOrdinal("indirizzoDenominazione");
                        int indexIndirizzoComune = reader.GetOrdinal("indirizzoComune");
                        int indexIndirizzoCap = reader.GetOrdinal("indirizzoCap");
                        int indexIndirizzoProvincia = reader.GetOrdinal("indirizzoProvincia");
                        int indexIdImpresa = reader.GetOrdinal("idImpresa");
                        int indexRagioneSociale = reader.GetOrdinal("ragioneSociale");
                        int indexTipoFineRapporto = reader.GetOrdinal("tipoFineRapporto");
                        int indexSindacato = reader.GetOrdinal("sindacato");
                        int indexTipoContratto = reader.GetOrdinal("tipoContratto");

                        while (reader.Read())
                        {
                            Lavoratore lav = new Lavoratore();

                            lav.IdLavoratore = reader.GetInt32(indexIdLavoratore);

                            if (reader[indexCognome] != DBNull.Value)
                                lav.Cognome = reader.GetString(indexCognome);
                            if (reader[indexNome] != DBNull.Value)
                                lav.Nome = reader.GetString(indexNome);
                            if (reader[indexCodiceFiscale] != DBNull.Value)
                                lav.CodiceFiscale = reader.GetString(indexCodiceFiscale);
                            if (reader[indexDataNascita] != DBNull.Value)
                                lav.DataNascita = reader.GetDateTime(indexDataNascita);
                            if (reader[indexIndirizzoDenominazione] != DBNull.Value)
                                lav.IndirizzoDenominazione = reader.GetString(indexIndirizzoDenominazione);
                            if (reader[indexIndirizzoComune] != DBNull.Value)
                                lav.IndirizzoComune = reader.GetString(indexIndirizzoComune);
                            if (reader[indexIndirizzoCap] != DBNull.Value)
                                lav.IndirizzoCAP = reader.GetString(indexIndirizzoCap);
                            if (reader[indexIndirizzoProvincia] != DBNull.Value)
                                lav.IndirizzoProvincia = reader.GetString(indexIndirizzoProvincia);
                            if (reader[indexIdImpresa] != DBNull.Value)
                                lav.IdImpresa = reader.GetInt32(indexIdImpresa);
                            if (reader[indexRagioneSociale] != DBNull.Value)
                                lav.RagioneSociale = reader.GetString(indexRagioneSociale);
                            if (reader[indexTipoFineRapporto] != DBNull.Value)
                                lav.TipoFineRapporto = reader.GetString(indexTipoFineRapporto);
                            if (reader[indexSindacato] != DBNull.Value)
                                lav.Sindacato = reader.GetString(indexSindacato);
                            if (reader[indexTipoContratto] != DBNull.Value)
                                lav.TipoContrattoLavoratore = reader.GetString(indexTipoContratto);

                            lavoratori.Add(lav);
                        }
                    }
                }

                conn.Close();
            }

            return lavoratori;
        }

        public LavoratoreDettaglio GetLavoratore(int idLavoratore)
        {
            LavoratoreDettaglio lav = null;

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                const string commandText = "SELECT * FROM LavoratoriDeleghe WHERE idLavoratore = @idLavoratore";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    command.Parameters.AddWithValue("@idLavoratore", idLavoratore);
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            lav = new LavoratoreDettaglio();

                            lav.IdLavoratore = (int) reader["idLavoratore"];

                            if (reader["cognome"] != DBNull.Value)
                                lav.Cognome = (string) reader["cognome"];
                            if (reader["nome"] != DBNull.Value)
                                lav.Nome = (string) reader["nome"];
                            if (reader["codiceFiscale"] != DBNull.Value)
                                lav.CodiceFiscale = (string) reader["codiceFiscale"];
                            if (reader["dataNascita"] != DBNull.Value)
                                lav.DataNascita = (DateTime) reader["dataNascita"];
                            if (reader["indirizzoDenominazione"] != DBNull.Value)
                                lav.IndirizzoDenominazione = (string) reader["indirizzoDenominazione"];
                            if (reader["indirizzoComune"] != DBNull.Value)
                                lav.IndirizzoComune = (string) reader["indirizzoComune"];
                            if (reader["indirizzoCap"] != DBNull.Value)
                                lav.IndirizzoCAP = (string) reader["indirizzoCap"];
                            if (reader["indirizzoProvincia"] != DBNull.Value)
                                lav.IndirizzoProvincia = (string) reader["indirizzoProvincia"];
                            if (reader["idImpresa"] != DBNull.Value)
                                lav.IdImpresa = (int) reader["idImpresa"];
                            if (reader["ragioneSociale"] != DBNull.Value)
                                lav.RagioneSociale = (string) reader["ragioneSociale"];
                            if (reader["sindacato"] != DBNull.Value)
                                lav.Sindacato = (string) reader["sindacato"];


                            if (reader["anno"] != DBNull.Value)
                                lav.Anno = (int) reader["anno"];
                            if (reader["comprensorioSindacale"] != DBNull.Value)
                                lav.ComprensorioSindacale = (string) reader["comprensorioSindacale"];
                            if (reader["dataIscrizioneSindacato"] != DBNull.Value)
                                lav.DataIscrizioneSindacato = (DateTime) reader["dataIscrizioneSindacato"];
                            if (reader["ultimoNrErogazione"] != DBNull.Value)
                                lav.UltimoNrErogazione = (string) reader["ultimoNrErogazione"];
                            if (reader["annoUltimaErogazioneAPE"] != DBNull.Value)
                                lav.AnnoUltimaErogazioneApe = (string) reader["annoUltimaErogazioneAPE"];
                            if (reader["dataRevocaTesseraSindacale"] != DBNull.Value)
                                lav.DataRevocaTesseraSindacale = (DateTime) reader["dataRevocaTesseraSindacale"];
                            if (reader["sindacatoPrecedente"] != DBNull.Value)
                                lav.SindacatoPrecedente = (string) reader["sindacatoPrecedente"];
                            if (reader["inizioPeriodoLavoro"] != DBNull.Value)
                                lav.InizioPeriodo = (DateTime) reader["inizioPeriodoLavoro"];
                            if (reader["finePeriodoLavoro"] != DBNull.Value)
                                lav.FinePeriodo = (DateTime) reader["finePeriodoLavoro"];
                            if (reader["imponibileSalariale"] != DBNull.Value)
                                lav.ImponibileSalariale = (decimal) reader["imponibileSalariale"];
                            if (reader["importoCartella"] != DBNull.Value)
                                lav.ImportoCartella = (decimal) reader["importoCartella"];
                            if (reader["importoDelega"] != DBNull.Value)
                                lav.ImportoDelega = (decimal) reader["importoDelega"];
                            if (reader["luogoNascita"] != DBNull.Value)
                                lav.LuogoNascita = (string) reader["luogoNascita"];
                            if (reader["sesso"] != DBNull.Value)
                                lav.Sesso = (string) reader["sesso"];
                            if (reader["codiceFiscaleImpresa"] != DBNull.Value)
                                lav.CodiceFiscaleImpresa = (string) reader["codiceFiscaleImpresa"];
                            if (reader["partitaIVA"] != DBNull.Value)
                                lav.PartitaIva = (string) reader["partitaIVA"];
                            if (reader["tipoImpresa"] != DBNull.Value)
                                lav.TipoImpresa = (string) reader["tipoImpresa"];
                            if (reader["codiceINAIL"] != DBNull.Value)
                                lav.CodiceInail = (string) reader["codiceINAIL"];
                            if (reader["codiceINPS"] != DBNull.Value)
                                lav.CodiceInps = (string) reader["codiceINPS"];
                            if (reader["attivitaIstat"] != DBNull.Value)
                                lav.AttivitaIstat = (string) reader["attivitaIstat"];
                            if (reader["naturaGiuridica"] != DBNull.Value)
                                lav.NaturaGiuridica = (string) reader["naturaGiuridica"];
                            if (reader["indirizzoSedeLegale"] != DBNull.Value)
                                lav.IndirizzoSedeLegale = (string) reader["indirizzoSedeLegale"];
                            if (reader["capSedeLegale"] != DBNull.Value)
                                lav.CapSedeLegale = (string) reader["capSedeLegale"];
                            if (reader["localitaSedeLegale"] != DBNull.Value)
                                lav.LocalitaSedeLegale = (string) reader["localitaSedeLegale"];
                            if (reader["provinciaSedeLegale"] != DBNull.Value)
                                lav.ProvinciaSedeLegale = (string) reader["provinciaSedeLegale"];
                            if (reader["indirizzoSedeAmministrazione"] != DBNull.Value)
                                lav.IndirizzoSedeAmministrazione = (string) reader["indirizzoSedeAmministrazione"];
                            if (reader["capSedeAmministrazione"] != DBNull.Value)
                                lav.CapSedeAmministrazione = (string) reader["capSedeAmministrazione"];
                            if (reader["localitaSedeAmministrazione"] != DBNull.Value)
                                lav.LocalitaSedeAmministrazione = (string) reader["localitaSedeAmministrazione"];
                            if (reader["provinciaSedeAmministrazione"] != DBNull.Value)
                                lav.ProvinciaSedeAmministrazione = (string) reader["provinciaSedeAmministrazione"];
                            if (reader["telefonoSedeLegale"] != DBNull.Value)
                                lav.TelefonoSedeLegale = (string) reader["telefonoSedeLegale"];
                            if (reader["faxSedeLegale"] != DBNull.Value)
                                lav.FaxSedeLegale = (string) reader["faxSedeLegale"];
                            if (reader["eMailSedeLegale"] != DBNull.Value)
                                lav.EMailSedeLegale = (string) reader["eMailSedeLegale"];
                            if (reader["telefonoSedeAmministrazione"] != DBNull.Value)
                                lav.TelefonoSedeAmministrazione = (string) reader["telefonoSedeAmministrazione"];
                            if (reader["faxSedeAmministrazione"] != DBNull.Value)
                                lav.FaxSedeAmministrazione = (string) reader["faxSedeAmministrazione"];
                            if (reader["eMailSedeAmministrazione"] != DBNull.Value)
                                lav.EMailSedeAmministrazione = (string) reader["eMailSedeAmministrazione"];
                            if (reader["dataInizioValiditaRapporto"] != DBNull.Value)
                                lav.DataInizioValiditaRapporto = (DateTime) reader["dataInizioValiditaRapporto"];
                            if (reader["dataFineValiditaRapporto"] != DBNull.Value)
                                lav.DataFineValiditaRapporto = (DateTime) reader["dataFineValiditaRapporto"];
                            if (reader["tipoContratto"] != DBNull.Value)
                                lav.TipoContratto = (string) reader["tipoContratto"];
                            if (reader["tipoCategoria"] != DBNull.Value)
                                lav.TipoCategoria = (string) reader["tipoCategoria"];
                            if (reader["tipoQualifica"] != DBNull.Value)
                                lav.TipoQualifica = (string) reader["tipoQualifica"];
                            if (reader["tipoMansione"] != DBNull.Value)
                                lav.TipoMansione = (string) reader["tipoMansione"];
                            //if (reader["tipoInizioRapporto"] != DBNull.Value)
                            //{
                            //    string tipoInizioRapporto = (string) reader["tipoInizioRapporto"];
                            //    if (tipoInizioRapporto.Trim() == "3")
                            //        lav.PartTime = true;
                            //}
                            if (reader["tipoFineRapporto"] != DBNull.Value)
                                lav.TipoFineRapporto = (string) reader["tipoFineRapporto"];
                            if (reader["partTime"] != DBNull.Value)
                                lav.PartTime = (bool) reader["partTime"];
                            if (reader["tipoRapporto"] != DBNull.Value)
                                lav.TipoRapporto = (string) reader["tipoRapporto"];
                            if (reader["ibanPresente"] != DBNull.Value)
                                lav.IbanPresente = (bool) reader["ibanPresente"];
                            if (reader["possibileDoppione"] != DBNull.Value)
                                lav.PossibileDoppione = (bool) reader["possibileDoppione"];
                        }
                    }
                }

                conn.Close();
            }

            return lav;
        }

        public DataSet GetLavoratori()
        {
            DataSet lavoratori = new DataSet();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                const string commandText = "SELECT * FROM LavoratoriDeleghe";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    using (SqlCeDataAdapter adapter = new SqlCeDataAdapter(command))
                    {
                        adapter.Fill(lavoratori);
                    }
                }

                conn.Close();
            }

            return lavoratori;
        }

        public DataSet GetLavoratori(int idImpresa)
        {
            DataSet lavoratori = new DataSet();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                string commandText = $"SELECT * FROM LavoratoriDeleghe WHERE idImpresa = {idImpresa}";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    using (SqlCeDataAdapter adapter = new SqlCeDataAdapter(command))
                    {
                        adapter.Fill(lavoratori);
                    }
                }

                conn.Close();
            }

            return lavoratori;
        }

        public List<string> GetElencoSindacati()
        {
            List<string> ret = new List<string>();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                const string commandText = "SELECT sindacato FROM Sindacati";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader["sindacato"] != DBNull.Value)
                                ret.Add((string) reader["sindacato"]);
                        }
                    }
                }

                conn.Close();
            }

            return ret;
        }

        public List<string> GetElencoTipoFineRapporto()
        {
            List<string> ret = new List<string>();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                const string commandText = "SELECT tipoFineRapporto FROM TipiCessazione";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader["tipoFineRapporto"] != DBNull.Value)
                                ret.Add((string) reader["tipoFineRapporto"]);
                        }
                    }
                }

                conn.Close();
            }

            return ret;
        }

        #endregion

        #region Imprese

        public List<Impresa> GetImprese(ImpresaFilter filter)
        {
            List<Impresa> imprese = new List<Impresa>();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                string commandText = "SELECT idImpresa, ragioneSociale, indirizzoSedeLegale, localitaSedeLegale, capSedeLegale, provinciaSedeLegale, codiceFiscaleImpresa, partitaIVA, numeroDipendenti, dataDenuncia "
                                     + "FROM ImpreseDeleghe "
                                     + "WHERE 1 = 1";
                if (filter.IdImpresa.HasValue)
                {
                    string idImpOp;
                    switch (filter.IdImpresaOperatore)
                    {
                        case Operatori.Uguale:
                            idImpOp = "=";
                            break;
                        case Operatori.Maggiore:
                            idImpOp = ">";
                            break;
                        case Operatori.Minore:
                            idImpOp = "<";
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    commandText += $" AND idImpresa {idImpOp} {filter.IdImpresa}";
                }
                if (!String.IsNullOrEmpty(filter.RagioneSociale))
                    commandText += " AND ragioneSociale LIKE '%" + filter.RagioneSociale + "%'";
                if (!String.IsNullOrEmpty(filter.LocalitaSedeLegale))
                    commandText += " AND localitaSedeLegale LIKE '%" + filter.LocalitaSedeLegale + "%'";
                if (!String.IsNullOrEmpty(filter.IndirizzoSedeLegale))
                    commandText += " AND indirizzoSedeLegale LIKE '%" + filter.IndirizzoSedeLegale + "%'";
                if (!String.IsNullOrEmpty(filter.CapSedeLegale))
                    commandText += " AND capSedeLegale = '" + filter.CapSedeLegale + "'";
                if (!String.IsNullOrEmpty(filter.ProvinciaSedeLegale))
                    commandText += " AND provinciaSedeLegale = '" + filter.ProvinciaSedeLegale + "'";
                if (!String.IsNullOrEmpty(filter.CodiceFiscaleImpresa))
                    commandText += " AND codiceFiscaleImpresa = '" + filter.CodiceFiscaleImpresa + "'";
                if (!String.IsNullOrEmpty(filter.PartitaIVA))
                    commandText += " AND partitaIVA = '" + filter.PartitaIVA + "'";

                commandText += " ORDER BY ragioneSociale";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Impresa impresa = new Impresa();

                            impresa.IdImpresa = (int) reader["idImpresa"];

                            if (reader["ragioneSociale"] != DBNull.Value)
                                impresa.RagioneSociale = (string) reader["ragioneSociale"];
                            if (reader["indirizzoSedeLegale"] != DBNull.Value)
                                impresa.IndirizzoSedeLegale = (string) reader["indirizzoSedeLegale"];
                            if (reader["localitaSedeLegale"] != DBNull.Value)
                                impresa.LocalitaSedeLegale = (string) reader["localitaSedeLegale"];
                            if (reader["capSedeLegale"] != DBNull.Value)
                                impresa.CapSedeLegale = (string) reader["capSedeLegale"];
                            if (reader["provinciaSedeLegale"] != DBNull.Value)
                                impresa.ProvinciaSedeLegale = (string) reader["provinciaSedeLegale"];
                            if (reader["codiceFiscaleImpresa"] != DBNull.Value)
                                impresa.CodiceFiscaleImpresa = (string) reader["codiceFiscaleImpresa"];
                            if (reader["dataDenuncia"] != DBNull.Value)
                                impresa.DataDenuncia = (DateTime) reader["dataDenuncia"];
                            if (reader["numeroDipendenti"] != DBNull.Value)
                                impresa.NumeroDipendenti = (int) reader["numeroDipendenti"];
                            if (reader["partitaIVA"] != DBNull.Value)
                                impresa.PartitaIVA = (string) reader["partitaIVA"];

                            imprese.Add(impresa);
                        }
                    }
                }

                conn.Close();
            }

            return imprese;
        }

        public ImpresaDettaglio GetImpresa(int idImpresa)
        {
            ImpresaDettaglio impresa = null;

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                const string commandText = "SELECT * FROM ImpreseDeleghe WHERE idImpresa = @idImpresa";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    command.Parameters.AddWithValue("@idImpresa", idImpresa);
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            impresa = new ImpresaDettaglio();

                            impresa.IdImpresa = (int) reader["idImpresa"];

                            if (reader["ragioneSociale"] != DBNull.Value)
                                impresa.RagioneSociale = (string) reader["ragioneSociale"];
                            if (reader["indirizzoSedeLegale"] != DBNull.Value)
                                impresa.IndirizzoSedeLegale = (string) reader["indirizzoSedeLegale"];
                            if (reader["localitaSedeLegale"] != DBNull.Value)
                                impresa.LocalitaSedeLegale = (string) reader["localitaSedeLegale"];
                            if (reader["capSedeLegale"] != DBNull.Value)
                                impresa.CapSedeLegale = (string) reader["capSedeLegale"];
                            if (reader["provinciaSedeLegale"] != DBNull.Value)
                                impresa.ProvinciaSedeLegale = (string) reader["provinciaSedeLegale"];
                            if (reader["codiceFiscaleImpresa"] != DBNull.Value)
                                impresa.CodiceFiscaleImpresa = (string) reader["codiceFiscaleImpresa"];
                            if (reader["dataDenuncia"] != DBNull.Value)
                                impresa.DataDenuncia = (DateTime) reader["dataDenuncia"];
                            if (reader["numeroDipendenti"] != DBNull.Value)
                                impresa.NumeroDipendenti = (int) reader["numeroDipendenti"];
                            if (reader["partitaIVA"] != DBNull.Value)
                                impresa.PartitaIVA = (string) reader["partitaIVA"];


                            if (reader["statoAmministrativo"] != DBNull.Value)
                                impresa.StatoAmministrativo = reader["statoAmministrativo"] as string;
                            if (reader["dataSospensioneAttivita"] != DBNull.Value)
                                impresa.DataSospensioneAttivita = (DateTime) reader["dataSospensioneAttivita"];
                            if (reader["dataRipresaAttivita"] != DBNull.Value)
                                impresa.DataRipresaAttivita = (DateTime) reader["dataRipresaAttivita"];
                            if (reader["dataCessazioneAttivita"] != DBNull.Value)
                                impresa.DataCessazioneAttivita = (DateTime) reader["dataCessazioneAttivita"];
                            if (reader["tipoImpresa"] != DBNull.Value)
                                impresa.TipoImpresa = reader["tipoImpresa"] as string;
                            if (reader["codiceINAIL"] != DBNull.Value)
                                impresa.CodiceINAIL = reader["codiceINAIL"] as string;
                            if (reader["codiceINPS"] != DBNull.Value)
                                impresa.CodiceINPS = reader["codiceINPS"] as string;
                            if (reader["attivitaIstat"] != DBNull.Value)
                                impresa.AttivitaIstat = reader["attivitaIstat"] as string;
                            if (reader["naturaGiuridica"] != DBNull.Value)
                                impresa.NaturaGiuridica = reader["naturaGiuridica"] as string;
                            if (reader["indirizzoSedeAmministrazione"] != DBNull.Value)
                                impresa.IndirizzoSedeAmministrazione = reader["indirizzoSedeAmministrazione"] as string;
                            if (reader["capSedeAmministrazione"] != DBNull.Value)
                                impresa.CapSedeAmministrazione = reader["capSedeAmministrazione"] as string;
                            if (reader["localitaSedeAmministrazione"] != DBNull.Value)
                                impresa.LocalitaSedeAmministrazione = reader["localitaSedeAmministrazione"] as string;
                            if (reader["provinciaSedeAmministrazione"] != DBNull.Value)
                                impresa.ProvinciaSedeAmministrazione = reader["provinciaSedeAmministrazione"] as string;
                            if (reader["telefonoSedeAmministrazione"] != DBNull.Value)
                                impresa.TelefonoSedeAmministrazione = reader["telefonoSedeAmministrazione"] as string;
                            if (reader["faxSedeAmministrazione"] != DBNull.Value)
                                impresa.FaxSedeAmministrazione = reader["faxSedeAmministrazione"] as string;
                            if (reader["eMailSedeAmministrazione"] != DBNull.Value)
                                impresa.EMailSedeAmministrazione = reader["eMailSedeAmministrazione"] as string;
                            if (reader["telefonoSedeLegale"] != DBNull.Value)
                                impresa.TelefonoSedeLegale = reader["telefonoSedeLegale"] as string;
                            if (reader["faxSedeLegale"] != DBNull.Value)
                                impresa.FaxSedeLegale = reader["faxSedeLegale"] as string;
                            if (reader["eMailSedeLegale"] != DBNull.Value)
                                impresa.EMailSedeLegale = reader["eMailSedeLegale"] as string;
                            if (reader["dataIscrizione"] != DBNull.Value)
                                impresa.DataIscrizione = (DateTime) reader["dataIscrizione"];
                            if (reader["idConsulente"] != DBNull.Value)
                                impresa.IdConsulente = (int) reader["idConsulente"];
                            if (reader["ragioneSocialeConsulente"] != DBNull.Value)
                                impresa.RagioneSocialeConsulente = reader["ragioneSocialeConsulente"] as string;
                            if (reader["idLavoratore"] != DBNull.Value)
                                impresa.IdLavoratore = (int) reader["idLavoratore"];
                            if (reader["rappresentanteLegale"] != DBNull.Value)
                                impresa.RappresentanteLegale = reader["rappresentanteLegale"] as string;
                            if (reader["telefonoConsulente"] != DBNull.Value)
                                impresa.TelefonoConsulente = reader["telefonoConsulente"] as string;
                            if (reader["associazioneImprenditoriale"] != DBNull.Value)
                                impresa.AssociazioneImprenditoriale = reader["associazioneImprenditoriale"] as string;
                            if (reader["numeroDipendentiPartTime"] != DBNull.Value)
                                impresa.NumeroDipendentiPartTime = (int) reader["numeroDipendentiPartTime"];

                            if (reader["idClasseAssegnazione"] != DBNull.Value)
                                impresa.IdClasseAssegnazione = (Int32)reader["idClasseAssegnazione"];
                        }
                    }
                }

                conn.Close();
            }

            return impresa;
        }

        #endregion

        #region Riepilogo

        public List<string> GetElencoComprensori()
        {
            List<string> ret = new List<string>();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                const string commandText = "SELECT comprensorioSindacale FROM ComprensoriSindacali";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader["comprensorioSindacale"] != DBNull.Value)
                                ret.Add((string) reader["comprensorioSindacale"]);
                        }
                    }
                }

                conn.Close();
            }

            return ret;
        }

        public Riepilogo GetRiepilogo(RiepilogoFilter filter)
        {
            Riepilogo riepilogo = new Riepilogo();

            string connectionString = ConnectionStringCompact;
            using (SqlCeConnection conn = new SqlCeConnection(connectionString))
            {
                conn.Open();

                string commandText =
                    "SELECT sindacato, SUM(imponibileSalariale) AS salario, SUM(importoCartella) AS cartella, SUM(importoDelega) AS delega, count(*) AS numLav  FROM LavoratoriDeleghe WHERE dataIscrizioneSindacato BETWEEN @dataIscrizioneDa AND @dataIscrizioneA ";

                if (filter.Comprensorio != "Tutti")
                    commandText += $" AND comprensorioSindacale = '{filter.Comprensorio}'";

                commandText += " GROUP BY sindacato";

                using (SqlCeCommand command = new SqlCeCommand(commandText, conn))
                {
                    command.Parameters.AddWithValue("@dataIscrizioneDa", filter.DataIscrizioneDa);
                    command.Parameters.AddWithValue("@dataIscrizioneA", filter.DataIscrizioneA);

                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string sindacato = reader["sindacato"] as string;
                            switch (sindacato)
                            {
                                case "CGIL":
                                    if (reader["salario"] != DBNull.Value)
                                        riepilogo.Cgil.ImponibileSalariale = (decimal) reader["salario"];
                                    if (reader["cartella"] != DBNull.Value)
                                        riepilogo.Cgil.ImportoCartella = (decimal) reader["cartella"];
                                    if (reader["delega"] != DBNull.Value)
                                        riepilogo.Cgil.ImportoDelega = (decimal) reader["delega"];
                                    if (reader["numLav"] != DBNull.Value)
                                        riepilogo.Cgil.NumeroLavoratori = (int) reader["numLav"];
                                    break;
                                case "CISL":
                                    if (reader["salario"] != DBNull.Value)
                                        riepilogo.Cisl.ImponibileSalariale = (decimal) reader["salario"];
                                    if (reader["cartella"] != DBNull.Value)
                                        riepilogo.Cisl.ImportoCartella = (decimal) reader["cartella"];
                                    if (reader["delega"] != DBNull.Value)
                                        riepilogo.Cisl.ImportoDelega = (decimal) reader["delega"];
                                    if (reader["numLav"] != DBNull.Value)
                                        riepilogo.Cisl.NumeroLavoratori = (int) reader["numLav"];
                                    break;
                                case "UIL":
                                    if (reader["salario"] != DBNull.Value)
                                        riepilogo.Uil.ImponibileSalariale = (decimal) reader["salario"];
                                    if (reader["cartella"] != DBNull.Value)
                                        riepilogo.Uil.ImportoCartella = (decimal) reader["cartella"];
                                    if (reader["delega"] != DBNull.Value)
                                        riepilogo.Uil.ImportoDelega = (decimal) reader["delega"];
                                    if (reader["numLav"] != DBNull.Value)
                                        riepilogo.Uil.NumeroLavoratori = (int) reader["numLav"];
                                    break;
                                case null:
                                    if (reader["salario"] != DBNull.Value)
                                        riepilogo.NonIscritti.ImponibileSalariale = (decimal) reader["salario"];
                                    if (reader["cartella"] != DBNull.Value)
                                        riepilogo.NonIscritti.ImportoCartella = (decimal) reader["cartella"];
                                    if (reader["delega"] != DBNull.Value)
                                        riepilogo.NonIscritti.ImportoDelega = (decimal) reader["delega"];
                                    if (reader["numLav"] != DBNull.Value)
                                        riepilogo.NonIscritti.NumeroLavoratori = (int) reader["numLav"];
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }

                riepilogo.Totali.ImponibileSalariale = riepilogo.Cgil.ImponibileSalariale +
                                                       riepilogo.Cisl.ImponibileSalariale +
                                                       riepilogo.Uil.ImponibileSalariale +
                                                       riepilogo.NonIscritti.ImponibileSalariale;

                riepilogo.Totali.ImportoCartella = riepilogo.Cgil.ImportoCartella +
                                                   riepilogo.Cisl.ImportoCartella +
                                                   riepilogo.Uil.ImportoCartella +
                                                   riepilogo.NonIscritti.ImportoCartella;

                riepilogo.Totali.ImportoDelega = riepilogo.Cgil.ImportoDelega +
                                                 riepilogo.Cisl.ImportoDelega +
                                                 riepilogo.Uil.ImportoDelega +
                                                 riepilogo.NonIscritti.ImportoDelega;

                riepilogo.Totali.NumeroLavoratori = riepilogo.Cgil.NumeroLavoratori +
                                                    riepilogo.Cisl.NumeroLavoratori +
                                                    riepilogo.Uil.NumeroLavoratori +
                                                    riepilogo.NonIscritti.NumeroLavoratori;
            }

            return riepilogo;
        }

        #endregion

        public int CopyData(string sourceConnString, string destConnString, string selectSource, string selectDestination, string selectDistinctDest = null, int? indexDistinctDest = null)
        {
            int ret;

            try
            {
                SqlCeConnection connectionSource = new SqlCeConnection(sourceConnString);
                SqlCeConnection connectionDest = new SqlCeConnection(destConnString);

                
                List<string> valPresenti = new List<string>();

                if (!string.IsNullOrEmpty(selectDistinctDest) && indexDistinctDest.HasValue)
                {
                    using (SqlCeCommand destCommand = new SqlCeCommand(selectDistinctDest, connectionDest))
                    {
                        connectionDest.Open();
                        using (var destReader = destCommand.ExecuteReader())
                        {
                            while (destReader.Read())
                            {
                                valPresenti.Add(destReader[0].ToString());
                            }
                        }
                        connectionDest.Close();
                    }
                }

                //string lavoratoriIdPresentiString = string.Join(",", lavoratoriIdPresenti);
                //selectSource = $"{selectSource} WHERE idLavoratore NOT IN ({lavoratoriIdPresentiString})";
                

                SqlCeCommand commandSource = new SqlCeCommand(selectSource, connectionSource);
                SqlCeDataAdapter source = new SqlCeDataAdapter(commandSource);
                DataSet data = new DataSet();
                source.Fill(data);
                for (int index = 0; index < data.Tables[0].Rows.Count; index++)
                {
                    if (valPresenti.Count > 0 && indexDistinctDest.HasValue)
                    {
                        string val = data.Tables[0].Rows[index][indexDistinctDest.Value].ToString();
                        if (!valPresenti.Contains(val))
                        {
                            data.Tables[0].Rows[index].SetAdded();
                        }
                        else
                        {
                            data.Tables[0].Rows[index].SetModified();
                        }
                    }
                    else
                    {
                        data.Tables[0].Rows[index].SetAdded();
                    }
                }

                SqlCeCommand commandDest = new SqlCeCommand(selectDestination, connectionDest);
                SqlCeDataAdapter dest = new SqlCeDataAdapter(commandDest);
                SqlCeCommandBuilder builder = new SqlCeCommandBuilder(dest);
                //builder.ConflictOption = ConflictOption.OverwriteChanges;
                dest.InsertCommand = builder.GetInsertCommand();
                //dest.UpdateCommand = builder.GetUpdateCommand();
                //dest.DeleteCommand = builder.GetDeleteCommand();
                ret = dest.Update(data);
            }
            catch (Exception ex)
            {
                ret = -1;
            }

            return ret;
        }
    }
}