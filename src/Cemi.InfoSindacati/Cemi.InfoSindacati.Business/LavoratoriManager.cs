using System.Collections.Generic;
using System.Data;
using Cemi.InfoSindacati.Data;
using Cemi.InfoSindacati.Type.Entities;

//using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.Business
{
    public class LavoratoriManager
    {
        private readonly DataProvider _dataProvider = new DataProvider();

        public List<Lavoratore> GetLavoratori(LavoratoreFilter filter)
        {
            return _dataProvider.GetLavoratori(filter);
        }

        public LavoratoreDettaglio GetLavoratore(int idLavoratore)
        {
            return _dataProvider.GetLavoratore(idLavoratore);
        }

        public List<string> GetElencoSindacati()
        {
            List<string> ret = new List<string> {"Tutti"};
            List<string> comp = _dataProvider.GetElencoSindacati();
            ret.AddRange(comp);
            ret.Add("Non iscritto");
            return ret;
        }

        public List<string> GetElencoTipoFineRapporto()
        {
            List<string> ret = new List<string> {"Tutti"};
            List<string> comp = _dataProvider.GetElencoTipoFineRapporto();
            ret.AddRange(comp);
            return ret;
        }

        public DataSet GetLavoratori()
        {
            return _dataProvider.GetLavoratori();
        }

        public DataSet GetLavoratori(int idImpresa)
        {
            return _dataProvider.GetLavoratori(idImpresa);
        }

        public int AccodaDati(string sourceConnString, string destConnString)
        {
            string selectSource = string.Empty;
            string selectDest = string.Empty;
            string selecetDistinctDest = string.Empty;
            int? indexDistinctDest = null;


            selectSource = "SELECT * FROM Sindacati";
            selectDest = "SELECT * FROM Sindacati";
            selecetDistinctDest = "SELECT sindacato FROM Sindacati";
            indexDistinctDest = 0;
            _dataProvider.CopyData(sourceConnString, destConnString, selectSource, selectDest, selecetDistinctDest, indexDistinctDest);

            selectSource = "SELECT * FROM TipiCessazione";
            selectDest = "SELECT * FROM TipiCessazione";
            selecetDistinctDest = "SELECT tipoFineRapporto FROM TipiCessazione";
            indexDistinctDest = 0;
            _dataProvider.CopyData(sourceConnString, destConnString, selectSource, selectDest, selecetDistinctDest, indexDistinctDest);

            selectSource = "SELECT * FROM ComprensoriSindacali";
            selectDest = "SELECT * FROM ComprensoriSindacali";
            selecetDistinctDest = "SELECT comprensorioSindacale FROM ComprensoriSindacali";
            indexDistinctDest = 0;
            _dataProvider.CopyData(sourceConnString, destConnString, selectSource, selectDest, selecetDistinctDest, indexDistinctDest);

            selectSource = "SELECT * FROM LavoratoriDeleghe";
            selectDest = "SELECT * FROM LavoratoriDeleghe";
            selecetDistinctDest = "SELECT idLavoratore FROM LavoratoriDeleghe";
            indexDistinctDest = 1;
            return _dataProvider.CopyData(sourceConnString, destConnString, selectSource, selectDest, selecetDistinctDest, indexDistinctDest);
        }
    }
}