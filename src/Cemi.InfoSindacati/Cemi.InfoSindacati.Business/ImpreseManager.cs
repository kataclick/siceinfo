using System.Collections.Generic;
using Cemi.InfoSindacati.Data;
using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.Business
{
    public class ImpreseManager
    {
        private readonly DataProvider _dataProvider = new DataProvider();

        public List<Impresa> GetImprese(ImpresaFilter filter)
        {
            return _dataProvider.GetImprese(filter);
        }

        public ImpresaDettaglio GetImpresa(int idImpresa)
        {
            return _dataProvider.GetImpresa(idImpresa);
        }
    }
}