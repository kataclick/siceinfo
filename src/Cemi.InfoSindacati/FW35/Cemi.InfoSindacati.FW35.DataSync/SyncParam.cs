namespace Cemi.InfoSindacati.FW35.DataSync
{
    internal class SyncParam
    {
        public string ConnStringSource { get; set; }

        public string ConnStringDestCGIL { get; set; }
        public string ConnStringDestCISL { get; set; }
        public string ConnStringDestUIL { get; set; }
        public string ConnStringDestTutti { get; set; }
    }
}