using System;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;

namespace Cemi.InfoSindacati.FW35.DataSync
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            InitializeBackgroundWorker();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadConnString();
        }

        private void LoadConnString()
        {
            textBoxSource.Text = ConfigurationManager.ConnectionStrings["SiceSource"].ConnectionString;
            textBoxDestCGIL.Text = ConfigurationManager.ConnectionStrings["SiceCompactDestCGIL"].ConnectionString;
            textBoxDestCISL.Text = ConfigurationManager.ConnectionStrings["SiceCompactDestCISL"].ConnectionString;
            textBoxDestUIL.Text = ConfigurationManager.ConnectionStrings["SiceCompactDestUIL"].ConnectionString;
            textBoxDestTutti.Text = ConfigurationManager.ConnectionStrings["SiceCompactDestTutti"].ConnectionString;
        }

        private void buttonSync_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxSource.Text) || String.IsNullOrEmpty(textBoxDestCGIL.Text))
            {
                MessageBox.Show("Connection string non definite!", "Attenzione", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            else
            {
                textBoxLog.Text = string.Empty; // svuoto la textbox del log
                textBoxLog.AppendText(DateTime.Now.ToShortTimeString() + ": Inizio procedura di allineamento");
                buttonSync.Enabled = false;
                SyncData(textBoxSource.Text, textBoxDestCGIL.Text, textBoxDestCISL.Text, textBoxDestUIL.Text, textBoxDestTutti.Text);
            }
        }

        private void SyncData(string connStringSource, string connStringDestCGIL, string connStringDestCISL, string connStringDestUIL, string connStringDestTutti)
        {
            SyncParam param = new SyncParam
            {
                ConnStringSource = connStringSource,
                ConnStringDestCGIL = connStringDestCGIL,
                ConnStringDestCISL = connStringDestCISL,
                ConnStringDestUIL = connStringDestUIL,
                ConnStringDestTutti = connStringDestTutti
            };

            backgroundWorkerSync.RunWorkerAsync(param);
        }

        #region BackgroundWorkers

        private void InitializeBackgroundWorker()
        {
            backgroundWorkerSync.RunWorkerCompleted += backgroundWorkerSync_RunWorkerCompleted;
            backgroundWorkerSync.ProgressChanged += backgroundWorkerSync_ProgressChanged;
            backgroundWorkerSync.DoWork += backgroundWorkerSync_DoWork;
        }

        private void backgroundWorkerSync_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker) sender;
            SyncParam param = (SyncParam) e.Argument;

            worker.ReportProgress(1, $"{Environment.NewLine}{DateTime.Now.ToShortTimeString()}: Gestione CGIL");
            SyncValues(worker, param.ConnStringSource, param.ConnStringDestCGIL, "CGIL");

            worker.ReportProgress(1, $"{Environment.NewLine}{DateTime.Now.ToShortTimeString()}: Gestione CISL");
            SyncValues(worker, param.ConnStringSource, param.ConnStringDestCISL, "CISL");

            worker.ReportProgress(1, $"{Environment.NewLine}{DateTime.Now.ToShortTimeString()}: Gestione UIL");
            SyncValues(worker, param.ConnStringSource, param.ConnStringDestUIL, "UIL");

            worker.ReportProgress(1, $"{Environment.NewLine}{DateTime.Now.ToShortTimeString()}: Gestione TUTTI");
            SyncValues(worker, param.ConnStringSource, param.ConnStringDestTutti, string.Empty);
        }

        private static void SyncValues(BackgroundWorker worker, string connStringSource, string connStringDest, string sindacato)
        {
            Syncronizer syncronizer = new Syncronizer(sindacato);

            syncronizer.SvuotaTabelle(connStringDest);

            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Inizio Sincronizzazione Lavoratori");
            int lavRet = syncronizer.SyncLavoratori(connStringSource, connStringDest);
            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Sincronizzati {lavRet} Lavoratori");

            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Inizio Sincronizzazione Imprese");
            int impRet = syncronizer.SyncImprese(connStringSource, connStringDest);
            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Sincronizzate {impRet} Imprese");

            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Inizio Sincronizzazione Sindacati");
            int sinRet = syncronizer.SyncSindacati(connStringSource, connStringDest);
            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Sincronizzati {sinRet} Sindacati");

            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Inizio Sincronizzazione Cessazioni");
            int cesRet = syncronizer.SyncTipiCessazione(connStringSource, connStringDest);
            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Sincronizzati {cesRet} Cessazioni");

            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Inizio Sincronizzazione Comprensori");
            int comRet = syncronizer.SyncComprensori(connStringSource, connStringDest);
            worker.ReportProgress(1, $"{DateTime.Now.ToShortTimeString()}: Sincronizzati {comRet} Comprensori");
        }

        private void backgroundWorkerSync_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if ((string) e.UserState != null)
                textBoxLog.AppendText(Environment.NewLine + (string) e.UserState /* + Environment.NewLine*/);
            progressBarSync.PerformStep();
            if (progressBarSync.Value == progressBarSync.Maximum)
                progressBarSync.Value = 0;
        }

        private void backgroundWorkerSync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonSync.Enabled = true;
            if (e.Error != null)
            {
                textBoxLog.AppendText(
                    $"{Environment.NewLine}{Environment.NewLine}{DateTime.Now.ToShortTimeString()}: Errore durante la procedura di allineamento! - {e.Error.Message}");
            }
            //else if (e.Cancelled)
            //{
            //    textBoxLog.AppendText(Environment.NewLine + "Procedura di import annullata!");
            //}
            else
            {
                progressBarSync.Value = progressBarSync.Maximum;
                textBoxLog.AppendText($"{Environment.NewLine}{Environment.NewLine}{DateTime.Now.ToShortTimeString()}: Procedura di allineamento completata");
            }
        }

        #endregion
    }
}