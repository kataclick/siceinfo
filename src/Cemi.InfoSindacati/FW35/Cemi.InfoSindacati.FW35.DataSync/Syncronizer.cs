using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlServerCe;

namespace Cemi.InfoSindacati.FW35.DataSync
{
    internal class Syncronizer
    {
        private readonly string _sindacato;

        public Syncronizer(string sindacato)
        {
            _sindacato = sindacato;
        }

        internal int SyncImprese(string connStringSource, string connStringDest)
        {
            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            int ret = CopyData(sqlConn, sqlCeConn, "SELECT * FROM _ImpreseDelegheTemp", "SELECT * FROM ImpreseDeleghe");

            return ret;
        }

        internal int SyncLavoratori(string connStringSource, string connStringDest)
        {
            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            var selectSource = !string.IsNullOrEmpty(_sindacato)
                ? $"SELECT * FROM _LavoratoriDelegheTemp WHERE sindacato = '{_sindacato}'"
                : "SELECT anno, idLavoratore, NULL AS comprensorioSindacale, NULL AS sindacato, NULL AS dataIscrizioneSindacato, NULL AS ultimoNrErogazione, " +
                  "NULL AS annoUltimaErogazioneAPE, NULL AS dataRevocaTesseraSindacale, NULL AS sindacatoPrecedente, NULL AS inizioPeriodoLavoro, NULL AS finePeriodoLavoro, " + 
                  "NULL AS imponibileSalariale, NULL AS importoCartella, NULL AS importoDelega, cognome,  nome, NULL AS codiceFiscale, NULL AS dataNascita, " + 
                  "NULL AS luogoNascita, NULL AS sesso, NULL AS indirizzoDenominazione, NULL AS indirizzoCAP, NULL AS indirizzoProvincia, NULL AS indirizzoComune, " + 
                  "idImpresa, ragioneSociale, codiceFiscaleImpresa, partitaIVA, tipoImpresa, codiceINAIL, codiceINPS, attivitaIstat, naturaGiuridica, indirizzoSedeLegale, " + 
                  "capSedeLegale, localitaSedeLegale, provinciaSedeLegale, indirizzoSedeAmministrazione, capSedeAmministrazione, localitaSedeAmministrazione, " + 
                  "provinciaSedeAmministrazione, telefonoSedeAmministrazione, faxSedeAmministrazione, eMailSedeAmministrazione, telefonoSedeLegale, faxSedeLegale, " +
                  "eMailSedeLegale, dataInizioValiditaRapporto AS dataInizioValiditaRapporto, dataFineValiditaRapporto AS dataFineValiditaRapporto, tipoInizioRapporto AS tipoInizioRapporto, tipoFineRapporto AS tipoFineRapporto, " + 
                  "NULL AS tipoContratto, NULL AS tipoCategoria, NULL AS tipoQualifica, NULL AS tipoMansione, NULL AS partTime, NULL AS tipoRapporto, NULL AS ibanPresente, " +
                  "NULL AS possibileDoppione, [oreInDenuncia], NULL AS [conImporti], [utenteRegistrato] FROM _LavoratoriDelegheTemp";

            var ret = CopyData(sqlConn, sqlCeConn, selectSource, "SELECT * FROM LavoratoriDeleghe");

            return ret;
        }

        internal int SyncSindacati(string connStringSource, string connStringDest)
        {
            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            var selectSource = !string.IsNullOrEmpty(_sindacato)
                ? $"SELECT DISTINCT(sindacato) FROM _LavoratoriDelegheTemp WHERE sindacato = '{_sindacato}'"
                : "SELECT DISTINCT(sindacato) FROM _LavoratoriDelegheTemp WHERE sindacato IS NOT NULL";

            var ret = CopyData(sqlConn, sqlCeConn, selectSource, "SELECT * FROM Sindacati");

            return ret;
        }

        internal int SyncTipiCessazione(string connStringSource, string connStringDest)
        {
            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            var selectSource = !string.IsNullOrEmpty(_sindacato)
                ? $"SELECT DISTINCT(tipoFineRapporto) FROM _LavoratoriDelegheTemp WHERE tipoFineRapporto IS NOT NULL AND sindacato = '{_sindacato}'"
                : "SELECT DISTINCT(tipoFineRapporto) FROM _LavoratoriDelegheTemp WHERE tipoFineRapporto IS NOT NULL";

            var ret = CopyData(sqlConn, sqlCeConn, selectSource, "SELECT * FROM TipiCessazione");

            return ret;
        }

        internal int SyncComprensori(string connStringSource, string connStringDest)
        {
            SqlConnection sqlConn = new SqlConnection(connStringSource);
            SqlCeConnection sqlCeConn = new SqlCeConnection(connStringDest);

            var selectSource = !string.IsNullOrEmpty(_sindacato)
                ? $"SELECT DISTINCT(comprensorioSindacale) FROM _LavoratoriDelegheTemp WHERE comprensorioSindacale IS NOT NULL AND sindacato = '{_sindacato}'"
                : "SELECT DISTINCT(comprensorioSindacale) FROM _LavoratoriDelegheTemp WHERE comprensorioSindacale IS NOT NULL";

            var ret = CopyData(sqlConn, sqlCeConn, selectSource, "SELECT * FROM ComprensoriSindacali");

            return ret;
        }

        internal int CopyData(SqlConnection connectionSource, SqlCeConnection connectionDest, string selectSource,
            string selectDestination)
        {
            int ret;

            try
            {
                SqlCommand commandSource = new SqlCommand(selectSource, connectionSource);
                SqlDataAdapter source = new SqlDataAdapter(commandSource);
                DataSet data = new DataSet();
                source.Fill(data);
                for (int index = 0; index < data.Tables[0].Rows.Count; index++)
                {
                    data.Tables[0].Rows[index].SetAdded();
                }

                SqlCeCommand commandDest = new SqlCeCommand(selectDestination, connectionDest);
                SqlCeDataAdapter dest = new SqlCeDataAdapter(commandDest);
                SqlCeCommandBuilder builder = new SqlCeCommandBuilder(dest);
                dest.InsertCommand = builder.GetInsertCommand();
                ret = dest.Update(data);
            }
            catch (Exception ex)
            {
                ret = -1;
            }

            return ret;
        }

        internal void SvuotaTabelle(string connString)
        {
            const string deleteLav = "DELETE FROM LavoratoriDeleghe";
            const string deleteImp = "DELETE FROM ImpreseDeleghe";
            const string deleteSin = "DELETE FROM Sindacati";
            const string deleteCom = "DELETE FROM ComprensoriSindacali";
            const string deleteCes = "DELETE FROM TipiCessazione";

            using (SqlCeConnection sqlCeConn = new SqlCeConnection(connString))
            {
                sqlCeConn.Open();

                using (SqlCeCommand commandLav = new SqlCeCommand(deleteLav, sqlCeConn))
                {
                    commandLav.ExecuteNonQuery();
                }

                using (SqlCeCommand commandImp = new SqlCeCommand(deleteImp, sqlCeConn))
                {
                    commandImp.ExecuteNonQuery();
                }

                using (SqlCeCommand commandSin = new SqlCeCommand(deleteSin, sqlCeConn))
                {
                    commandSin.ExecuteNonQuery();
                }

                using (SqlCeCommand commandCom = new SqlCeCommand(deleteCom, sqlCeConn))
                {
                    commandCom.ExecuteNonQuery();
                }

                using (SqlCeCommand commandCes = new SqlCeCommand(deleteCes, sqlCeConn))
                {
                    commandCes.ExecuteNonQuery();
                }

                sqlCeConn.Close();
            }
        }
    }
}