namespace Cemi.InfoSindacati.Type.Enums
{
    public enum Operatori
    {
        Uguale = 0,
        Maggiore,
        Minore
    }
}