using System;
using Cemi.InfoSindacati.Type.Enums;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class LavoratoreFilter
    {
        public LavoratoreFilter()
        {
            ComprensorioSindacale = "Tutti";
            TipoFineRapporto = "Tutti";
            IdImpresaOperatore = Operatori.Uguale;
            Sindacato = "Tutti";
            IdLavoratoreOperatore = Operatori.Uguale;
        }

        public int? IdLavoratore { get; set; }

        public Operatori IdLavoratoreOperatore { get; set; }

        public string Sindacato { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public DateTime? DataNascita { get; set; }

        public string IndirizzoDenominazione { get; set; }

        public string IndirizzoCap { get; set; }

        public string IndirizzoProvincia { get; set; }

        public string IndirizzoComune { get; set; }

        public int? IdImpresa { get; set; }

        public Operatori IdImpresaOperatore { get; set; }

        public string RagioneSociale { get; set; }

        public string TipoFineRapporto { get; set; }

        public int? Attivo { get; set; }

        public string ComprensorioSindacale { get; set; }

        public int? ConIban { get; set; }

        public int? Importi { get; set; }

        public int? ConDenuncia { get; set; }

        public int? ConUtenza { get; set; }
    }
}