using System;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class Lavoratore
    {
        private string indirizzoCAP;
        private string indirizzoComune;
        private string indirizzoDenominazione;
        private string indirizzoProvincia;
        private string tipoFineRapporto;
        private char sesso;
        private string straniero;

        public int IdLavoratore { get; set; }

        public string Sindacato { get; set; }

        public string Cognome { get; set; }

        public string Nome { get; set; }

        public string CodiceFiscale { get; set; }

        public DateTime DataNascita { get; set; }

        public string IndirizzoDenominazione
        {
            get { return indirizzoDenominazione; }
            set { indirizzoDenominazione = value; }
        }

        public string IndirizzoCAP
        {
            get { return indirizzoCAP; }
            set { indirizzoCAP = value; }
        }

        public string IndirizzoProvincia
        {
            get { return indirizzoProvincia; }
            set { indirizzoProvincia = value; }
        }

        public string IndirizzoComune
        {
            get { return indirizzoComune; }
            set { indirizzoComune = value; }
        }

        public string IndirizzoCompleto
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoDenominazione) && !String.IsNullOrEmpty(indirizzoComune) &&
                    !String.IsNullOrEmpty(indirizzoCAP) && !String.IsNullOrEmpty(indirizzoProvincia))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoDenominazione, indirizzoComune, indirizzoCAP,
                                      indirizzoProvincia);
                return ret;
            }
        }

        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string TipoFineRapporto
        {
            get
            {
                string ret = tipoFineRapporto;
                if (!string.IsNullOrEmpty(tipoFineRapporto))
                    ret = tipoFineRapporto.Substring(0, 3);
                return ret;
            }
            set { tipoFineRapporto = value; }
        }

        public char Sesso
        {
            get
            {
                char ret = sesso;
                if (!string.IsNullOrEmpty(CodiceFiscale))
                {
                    if (Convert.ToInt32(CodiceFiscale.Substring(9, 2)) <= 31)
                        ret = 'M';
                    else
                        ret = 'F';
                }

                return ret;
            }
            set { sesso = value; }
        }

        public string Straniero
        {
            get
            {
                string ret = straniero;
                if (!string.IsNullOrEmpty(CodiceFiscale))
                {
                    if (CodiceFiscale.Substring(11, 1) == "Z")
                        ret = "S�";
                    else
                        ret = DBNull.Value.ToString();
                }

                return ret;
            }
            set { straniero = value; }
        }

        public string TipoContrattoLavoratore { get; set; }
    }
}