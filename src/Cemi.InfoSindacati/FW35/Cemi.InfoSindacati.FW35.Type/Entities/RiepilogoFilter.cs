using System;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class RiepilogoFilter
    {
        public string Comprensorio { get; set; }

        public DateTime DataIscrizioneDa { get; set; }

        public DateTime DataIscrizioneA { get; set; }
    }
}