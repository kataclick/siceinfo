using System;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class ImpresaDettaglio : Impresa
    {
        private string capSedeAmministrazione;
        private string indirizzoSedeAmministrazione;
        private string localitaSedeAmministrazione;
        private string provinciaSedeAmministrazione;
        private int idClasseAssegnazione;

        public string StatoAmministrativo { get; set; }

        public DateTime DataSospensioneAttivita { get; set; }

        public DateTime DataRipresaAttivita { get; set; }

        public DateTime DataCessazioneAttivita { get; set; }

        public string TipoImpresa { get; set; }

        public string CodiceINAIL { get; set; }

        public string CodiceINPS { get; set; }

        public string AttivitaIstat { get; set; }

        public string NaturaGiuridica { get; set; }

        public string IndirizzoSedeAmministrazione
        {
            get { return indirizzoSedeAmministrazione; }
            set { indirizzoSedeAmministrazione = value; }
        }

        public string CapSedeAmministrazione
        {
            get { return capSedeAmministrazione; }
            set { capSedeAmministrazione = value; }
        }

        public string LocalitaSedeAmministrazione
        {
            get { return localitaSedeAmministrazione; }
            set { localitaSedeAmministrazione = value; }
        }

        public string ProvinciaSedeAmministrazione
        {
            get { return provinciaSedeAmministrazione; }
            set { provinciaSedeAmministrazione = value; }
        }

        public string TelefonoSedeAmministrazione { get; set; }

        public string IndirizzoCompletoSedeAmministrativa
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoSedeAmministrazione) &&
                    !String.IsNullOrEmpty(localitaSedeAmministrazione) && !String.IsNullOrEmpty(capSedeAmministrazione) &&
                    !String.IsNullOrEmpty(provinciaSedeAmministrazione))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoSedeAmministrazione, localitaSedeAmministrazione,
                                      capSedeAmministrazione, provinciaSedeAmministrazione);
                return ret;
            }
        }

        public string FaxSedeAmministrazione { get; set; }

        public string EMailSedeAmministrazione { get; set; }

        public string TelefonoSedeLegale { get; set; }

        public string FaxSedeLegale { get; set; }

        public string EMailSedeLegale { get; set; }

        public DateTime DataIscrizione { get; set; }

        public int IdConsulente { get; set; }

        public string RagioneSocialeConsulente { get; set; }

        public int IdLavoratore { get; set; }

        public string RappresentanteLegale { get; set; }

        public string TelefonoConsulente { get; set; }

        public string AssociazioneImprenditoriale { get; set; }

        public int NumeroDipendentiPartTime { get; set; }

        public Int32 IdClasseAssegnazione 
         {
            get { return idClasseAssegnazione; }
            set { idClasseAssegnazione = value; }
        }

        public string ClasseAssegnazione
        {
            get
            {
                string ret = string.Empty;
                if (idClasseAssegnazione == 2) ret = "B";
                
                return ret;
            }
        }
    }
}