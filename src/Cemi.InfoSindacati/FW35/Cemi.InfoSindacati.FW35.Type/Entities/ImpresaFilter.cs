using Cemi.InfoSindacati.Type.Enums;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class ImpresaFilter
    {
        private Operatori idImpresaOperatore = Operatori.Uguale;

        public int? IdImpresa { get; set; }

        public Operatori IdImpresaOperatore
        {
            get { return idImpresaOperatore; }
            set { idImpresaOperatore = value; }
        }

        public string RagioneSociale { get; set; }

        public string CodiceFiscaleImpresa { get; set; }

        public string PartitaIVA { get; set; }

        public string IndirizzoSedeLegale { get; set; }

        public string CapSedeLegale { get; set; }

        public string LocalitaSedeLegale { get; set; }

        public string ProvinciaSedeLegale { get; set; }
    }
}