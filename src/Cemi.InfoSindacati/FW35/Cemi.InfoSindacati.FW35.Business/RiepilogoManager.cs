using System.Collections.Generic;
using Cemi.InfoSindacati.FW35.Data;
using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.FW35.Business
{
    public class RiepilogoManager
    {
        private readonly DataProvider _dataProvider = new DataProvider();

        public List<string> GetElencoComprensori()
        {
            List<string> ret = new List<string>();
            ret.Add("Tutti");
            List<string> comp = _dataProvider.GetElencoComprensori();
            ret.AddRange(comp);
            return ret;
        }

        public Riepilogo GetRiepilogo(RiepilogoFilter filter)
        {
            return _dataProvider.GetRiepilogo(filter);
        }
    }
}