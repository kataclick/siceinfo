using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Cemi.InfoSindacati.FW35.Business;
using Cemi.InfoSindacati.Type;
using Cemi.InfoSindacati.Type.Entities;
using Cemi.InfoSindacati.Type.Enums;

namespace Cemi.InfoSindacati.FW35.WinApp
{
    public partial class FormMain : FormBase
    {
        private readonly ImpreseManager _impreseManager;
        private readonly LavoratoriManager _lavoratoriManager;
        private readonly RiepilogoManager _riepilogoManager;

        public FormMain()
        {
            InitializeComponent();

            InitializeMyComponent();
            InitializeBackgroundWorkers();

            _lavoratoriManager = new LavoratoriManager();
            _impreseManager = new ImpreseManager();
            _riepilogoManager = new RiepilogoManager();

            LoadSindacati();
            LoadCauseCessazioni();
            LoadComprensori();
        }

        #region BackgroundWorkers

        private void InitializeBackgroundWorkers()
        {
            backgroundWorkerElencoLavoratori.DoWork += backgroundWorkerElencoLavoratori_DoWork;
            backgroundWorkerElencoLavoratori.ProgressChanged += backgroundWorkerElencoLavoratori_ProgressChanged;
            backgroundWorkerElencoLavoratori.RunWorkerCompleted += backgroundWorkerElencoLavoratori_RunWorkerCompleted;

            backgroundWorkerElencoImprese.DoWork += backgroundWorkerElencoImprese_DoWork;
            backgroundWorkerElencoImprese.ProgressChanged += backgroundWorkerElencoImprese_ProgressChanged;
            backgroundWorkerElencoImprese.RunWorkerCompleted += backgroundWorkerElencoImprese_RunWorkerCompleted;

            backgroundWorkerExport.DoWork += backgroundWorkerExport_DoWork;
            backgroundWorkerExport.ProgressChanged += backgroundWorkerExport_ProgressChanged;
            backgroundWorkerExport.RunWorkerCompleted += backgroundWorkerExport_RunWorkerCompleted;

            backgroundWorkerAccodaDb.DoWork += BackgroundWorkerAccodaDbOnDoWork;
            backgroundWorkerAccodaDb.ProgressChanged += BackgroundWorkerAccodaDbOnProgressChanged;
            backgroundWorkerAccodaDb.RunWorkerCompleted += BackgroundWorkerAccodaDbOnRunWorkerCompleted;
        }

        #region Export

        private void backgroundWorkerExport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabelOperation.Text = "Ready";

            if (e.Error != null)
            {
                MessageBox.Show(
                    $"Errore durante la procedura di export{Environment.NewLine}Dettagli: {e.Error.Message}", "Errore",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Export completato", "Export", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void backgroundWorkerExport_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
                toolStripStatusLabelOperation.Text = (string) e.UserState;
        }

        private void backgroundWorkerExport_DoWork(object sender, DoWorkEventArgs e)
        {
            if (sender is BackgroundWorker worker)
            {
                worker.ReportProgress(1, "Exporting data...");
            }

            switch (e.Argument)
            {
                case LavoratoriExportParameter parameter:
                    DataSet dsLav = ConvertLavoratoriToDataSet(parameter.Lavoratori);
                    string fileNameLav = parameter.FileName;
                    ExportToExcel(dsLav, fileNameLav);
                    break;
                case ImpreseExportParameter exportParameter:
                    DataSet dsImp = ConvertImpreseToDataSet(exportParameter.Imprese);
                    string fileNameImp = exportParameter.FileName;
                    ExportToExcel(dsImp, fileNameImp);
                    break;
            }
        }

        #endregion

        #region Imprese

        private void backgroundWorkerElencoImprese_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabelOperation.Text = "Ready";
            groupBoxFiltroImp.Enabled = true;
            groupBoxFiltroLav.Enabled = true;
            dataGridViewElencoImp.Enabled = true;
            dataGridViewElencoLav.Enabled = true;

            if (e.Error != null)
                MessageBox.Show(
                    $"Errore durante il recupero delle informazioni richieste{Environment.NewLine}Dettagli: {e.Error.Message}", "Errore", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            else
            {
                CreateDataGridViewStructImpresa();
                dataGridViewElencoImp.DataSource = e.Result;
                toolStripStatusLabelRow.Text = $"Record: {((List<Impresa>) e.Result).Count}";
            }
        }

        private void backgroundWorkerElencoImprese_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
                toolStripStatusLabelOperation.Text = (string) e.UserState;
        }

        private void backgroundWorkerElencoImprese_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            ImpresaFilter filter = (ImpresaFilter) e.Argument;

            worker?.ReportProgress(1, "Data retrieving");

            List<Impresa> imprese = _impreseManager.GetImprese(filter);

            e.Result = imprese;
        }

        #endregion

        #region Lavoratori

        private void backgroundWorkerElencoLavoratori_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabelOperation.Text = "Ready";
            groupBoxFiltroImp.Enabled = true;
            groupBoxFiltroLav.Enabled = true;
            dataGridViewElencoImp.Enabled = true;
            dataGridViewElencoLav.Enabled = true;

            if (e.Error != null)
                MessageBox.Show(
                    "Errore durante il recupero delle informazioni richieste" + Environment.NewLine + "Dettagli: " +
                    e.Error.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                CreateDataGridViewStructLavoratore();
                dataGridViewElencoLav.DataSource = e.Result;
                toolStripStatusLabelRow.Text = $"Record: {((List<Lavoratore>) e.Result).Count}";
            }
        }

        private void backgroundWorkerElencoLavoratori_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                toolStripStatusLabelOperation.Text = (string) e.UserState;
            }
        }

        private void backgroundWorkerElencoLavoratori_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker) sender;
            LavoratoreFilter filter = (LavoratoreFilter) e.Argument;

            worker.ReportProgress(1, "Data retrieving");
            List<Lavoratore> lavoratori = _lavoratoriManager.GetLavoratori(filter);

            e.Result = lavoratori;
        }

        #endregion

        #region Accoda Db

        private void BackgroundWorkerAccodaDbOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripStatusLabelOperation.Text = "Ready";

            if (e.Error != null)
            {
                MessageBox.Show(
                    "Errore durante il caricamento dei dati" + Environment.NewLine + "Dettagli: " +
                    e.Error.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Aggiornamento eseguito correttamente.", "Db update",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                groupBoxFiltroImp.Enabled = true;
                groupBoxFiltroLav.Enabled = true;
                dataGridViewElencoImp.Enabled = true;
                dataGridViewElencoLav.Enabled = true;

                groupBoxFiltroImp.Enabled = true;
                groupBoxFiltroLav.Enabled = true;
                dataGridViewElencoImp.Enabled = true;
                dataGridViewElencoLav.Enabled = true;

                groupBoxParametri.Enabled = true;

                fileToolStripMenuItem.Enabled = true;
                helpToolStripMenuItem.Enabled = true;

                dataGridViewElencoLav.DataSource = null;
                dataGridViewElencoImp.DataSource = null;
                ClearDettagli();

                LoadSindacati();
                LoadCauseCessazioni();
                LoadComprensori();

                PulisciFiltroImprese();
                PulisciFiltroLavoratori();

                toolStripStatusLabelRow.Text = $"Record importati: {e.Result}";
            }
        }

        private void BackgroundWorkerAccodaDbOnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState != null)
            {
                toolStripStatusLabelOperation.Text = (string)e.UserState;
            }
        }

        private void BackgroundWorkerAccodaDbOnDoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            AccodaDatiParam param = (AccodaDatiParam)e.Argument;

            worker.ReportProgress(1, "Data import...");
            int recordLavImportati = _lavoratoriManager.AccodaDati(param.ConnStringSource, param.ConnStringDest);
            _lavoratoriManager.AccodaDati(param.ConnStringSource, param.ConnStringDest);



            e.Result = recordLavImportati;
        }

        #endregion

        #endregion

        #region Events

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutBox = new AboutBox1();
            aboutBox.ShowDialog();
        }

        private void caricaDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CaricaNuovoDb();
        }

        private void accodaDBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccodaDb();
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonFiltraLav_Click(object sender, EventArgs e)
        {
            CaricaListaLavoratori();
        }

        private void buttonFiltraImp_Click(object sender, EventArgs e)
        {
            CaricaListaImprese();
        }

        private void dataGridViewElencoLav_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewElencoLav.Columns[e.ColumnIndex] == ColumnSelectLavoratore && e.RowIndex >= 0)
            {
                Lavoratore selectedItem = (Lavoratore) dataGridViewElencoLav.Rows[e.RowIndex].DataBoundItem;
                //MessageBox.Show(selectedItem.CognomeNome);
                FormLavoratore frmLav = new FormLavoratore(selectedItem.IdLavoratore);
                frmLav.Show();
            }
        }

        private void dataGridViewElencoImp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewElencoImp.Columns[e.ColumnIndex] == ColumnSelectImpresa)
            {
                Impresa selectedItem = (Impresa) dataGridViewElencoImp.Rows[e.RowIndex].DataBoundItem;
                //MessageBox.Show(selectedItem.RagioneSociale);
                FormImpresa frmImp = new FormImpresa(selectedItem.IdImpresa);
                frmImp.Show();
            }
        }

        private void dataGridViewElencoLav_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(String.Format("Colonna:{0}; Riga:{1}; valore:{2}", e.ColumnIndex, e.RowIndex, dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value));
            if (e.RowIndex >= 0)
            {
                PulisciFiltroLavoratori();

                if (dataGridViewElencoLav.Columns[e.ColumnIndex] == codiceDataGridViewTextBoxColumnLavoratore)
                    maskedTextBoxIdLavoratore.Text = dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] == cognomeDataGridViewTextBoxColumnLavoratore)
                    textBoxCognome.Text = dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] == nomeDataGridViewTextBoxColumnLavoratore)
                    textBoxNome.Text = dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] == sindacatoDataGridViewTextBoxColumnLavoratore)
                    comboBoxSindacato.SelectedItem = dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] == dataNascitaDataGridViewTextBoxColumnLavoratore)
                    maskedTextBoxDataNascita.Text = dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] ==
                         ragioneSocialeDataGridViewTextBoxColumnLavoratore)
                    textBoxRagioneSociale.Text = dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] ==
                         codiceDittaDataGridViewTextBoxColumnLavoratore)
                    maskedTextBoxIdImpresa.Text =
                        dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();
                else if (dataGridViewElencoLav.Columns[e.ColumnIndex] ==
                         codiceFiscaleDataGridViewTextBoxColumnLavoratore)
                    textBoxCodiceFiscale.Text =
                        dataGridViewElencoLav[e.ColumnIndex, e.RowIndex].Value.ToString();

                buttonFiltraLav.Select();
            }
        }

        private void PulisciFiltroLavoratori()
        {
            foreach (Control control in groupBoxFiltroLav.Controls)
            {
                if (control is TextBox || control is MaskedTextBox)
                {
                    control.ResetText();
                }

                if (control is ComboBox box)
                {
                    box.SelectedIndex = 0;
                }
            }

            //radioButtonDenunciaNo.Checked = false;
            //radioButtonDenunciaSi.Checked = false;
            radioButtonDenunciaTutti.Checked = true;

            radioButtonIbanTutti.Checked = true;

            radioButtonRdlTutti.Checked = true;

            radioButtonUtenzaTutti.Checked = true;

            radioButtonImportiTutti.Checked = true;


            //checkBoxAttivo.Checked = false;
            //checkBoxSenzaIban.Checked = false;
        }

        private void dataGridViewElencoImp_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                PulisciFiltroImprese();

                if (dataGridViewElencoImp.Columns[e.ColumnIndex] == codiceDataGridViewTextBoxColumnImpresa)
                    maskedTextBoxImpIdImpresa.Text = dataGridViewElencoImp[e.ColumnIndex, e.RowIndex].Value.ToString();
                if (dataGridViewElencoImp.Columns[e.ColumnIndex] == ragioneSocialeDataGridViewTextBoxColumnImpresa)
                    textBoxImpRagioneSociale.Text = dataGridViewElencoImp[e.ColumnIndex, e.RowIndex].Value.ToString();
                if (dataGridViewElencoImp.Columns[e.ColumnIndex] == codiceFiscaleDataGridViewTextBoxColumnImpresa)
                    textBoxImpCodiceFiscale.Text = dataGridViewElencoImp[e.ColumnIndex, e.RowIndex].Value.ToString();
                if (dataGridViewElencoImp.Columns[e.ColumnIndex] == partitaIvaDataGridViewTextBoxColumnImpresa)
                    textBoxImpPartitaIva.Text = dataGridViewElencoImp[e.ColumnIndex, e.RowIndex].Value.ToString();

                buttonFiltraImp.Select();
            }
        }

        private void PulisciFiltroImprese()
        {
            foreach (Control control in groupBoxFiltroImp.Controls)
            {
                if (control is TextBox || control is MaskedTextBox)
                {
                    control.ResetText();
                }

                if (control is ComboBox box)
                {
                    box.SelectedIndex = 0;
                }
            }
        }

        private void groupBoxFiltroImp_Enter(object sender, EventArgs e)
        {
            AcceptButton = buttonFiltraImp;
        }

        private void groupBoxFiltroLav_Enter(object sender, EventArgs e)
        {
            AcceptButton = buttonFiltraLav;
        }

        private void buttonDettagli_Click(object sender, EventArgs e)
        {
            CaricaDettagli();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tabName = tabControlMain.SelectedTab.Name;

            switch (tabName)
            {
                case "tabPageLavoratori":
                    string fileNameLav = SaveFilePath();
                    if (!string.IsNullOrEmpty(fileNameLav))
                    {
                        LavoratoriExportParameter paramLav = new LavoratoriExportParameter();
                        paramLav.FileName = fileNameLav;
                        paramLav.Lavoratori = (List<Lavoratore>) dataGridViewElencoLav.DataSource;
                        backgroundWorkerExport.RunWorkerAsync(paramLav);
                    }

                    break;
                case "tabPageImprese":
                    string fileNameImp = SaveFilePath();
                    if (!string.IsNullOrEmpty(fileNameImp))
                    {
                        ImpreseExportParameter paramImp = new ImpreseExportParameter();
                        paramImp.FileName = fileNameImp;
                        paramImp.Imprese = (List<Impresa>) dataGridViewElencoImp.DataSource;
                        backgroundWorkerExport.RunWorkerAsync(paramImp);
                    }

                    break;
                default:
                    MessageBox.Show("Operazione non supportata");
                    break;
            }
        }

        private void toolStripMenuItemReport_Click(object sender, EventArgs e)
        {
            string tabName = tabControlMain.SelectedTab.Name;

            switch (tabName)
            {
                case "tabPageLavoratori":
                    if (dataGridViewElencoLav.DataSource != null)
                    {
                        int idImpresa = ((List<Lavoratore>) dataGridViewElencoLav.DataSource)[0].IdImpresa;
                        FormReport rep = new FormReport(idImpresa);
                        rep.Show();
                    }
                    else
                        MessageBox.Show("Lista vuota", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                    break;
                default:
                    MessageBox.Show("Operazione non supportata");
                    break;
            }
        }

        private void buttonResetLav_Click(object sender, EventArgs e)
        {
            PulisciFiltroLavoratori();
        }

        private void buttonResetImp_Click(object sender, EventArgs e)
        {
            PulisciFiltroImprese();
        }

        #endregion

        #region Load Lavoratori List

        private void CaricaListaLavoratori()
        {
            groupBoxFiltroImp.Enabled = false;
            groupBoxFiltroLav.Enabled = false;
            dataGridViewElencoImp.Enabled = false;
            dataGridViewElencoLav.Enabled = false;
            LavoratoreFilter filter = CreateFilterLavoratore();
            backgroundWorkerElencoLavoratori.RunWorkerAsync(filter);
        }

        private LavoratoreFilter CreateFilterLavoratore()
        {
            LavoratoreFilter filter = new LavoratoreFilter();

            if (!string.IsNullOrEmpty(maskedTextBoxIdLavoratore.Text))
            {
                int idLavoratore;
                if (int.TryParse(maskedTextBoxIdLavoratore.Text, out idLavoratore))
                    filter.IdLavoratore = idLavoratore;

                string idLavOp = comboBoxIdLavOp.Text;
                switch (idLavOp)
                {
                    case "=":
                        filter.IdLavoratoreOperatore = Operatori.Uguale;
                        break;
                    case ">":
                        filter.IdLavoratoreOperatore = Operatori.Maggiore;
                        break;
                    case "<":
                        filter.IdLavoratoreOperatore = Operatori.Minore;
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(textBoxCognome.Text))
                filter.Cognome = textBoxCognome.Text;

            if (!string.IsNullOrEmpty(textBoxNome.Text))
                filter.Nome = textBoxNome.Text;

            if (!string.IsNullOrEmpty(textBoxCodiceFiscale.Text))
                filter.CodiceFiscale = textBoxCodiceFiscale.Text;

            if (maskedTextBoxDataNascita.MaskCompleted)
                filter.DataNascita = DateTime.Parse(maskedTextBoxDataNascita.Text);

            if (!string.IsNullOrEmpty(textBoxIndirizzo.Text))
                filter.IndirizzoDenominazione = textBoxIndirizzo.Text;

            if (!string.IsNullOrEmpty(textBoxComune.Text))
                filter.IndirizzoComune = textBoxComune.Text;

            if (maskedTextBoxCap.MaskCompleted)
                filter.IndirizzoCap = maskedTextBoxCap.Text;

            if (!string.IsNullOrEmpty(textBoxProvincia.Text))
                filter.IndirizzoProvincia = textBoxProvincia.Text;

            if (!string.IsNullOrEmpty(maskedTextBoxIdImpresa.Text))
            {
                int idImpresa;
                if (int.TryParse(maskedTextBoxIdImpresa.Text, out idImpresa))
                    filter.IdImpresa = idImpresa;

                string idImpOp = comboBoxIdImpOp.Text;
                switch (idImpOp)
                {
                    case "=":
                        filter.IdImpresaOperatore = Operatori.Uguale;
                        break;
                    case ">":
                        filter.IdImpresaOperatore = Operatori.Maggiore;
                        break;
                    case "<":
                        filter.IdImpresaOperatore = Operatori.Minore;
                        break;
                    default:
                        break;
                }
            }
            //filter.IdImpresa = Int32.Parse(maskedTextBoxIdImpresa.Text);

            if (!string.IsNullOrEmpty(textBoxRagioneSociale.Text))
                filter.RagioneSociale = textBoxRagioneSociale.Text;

            filter.Sindacato = comboBoxSindacato.SelectedItem.ToString();

            filter.TipoFineRapporto = comboBoxTipoCessazione.SelectedItem.ToString();

            filter.ComprensorioSindacale = comboBoxComprensori.SelectedItem.ToString();

            filter.Attivo = null;
            if (radioButtonRdlSi.Checked)
                filter.Attivo = 1;
            if (radioButtonRdlNo.Checked)
                filter.Attivo = 0;

            filter.ConIban = null;
            if (radioButtonIbanSi.Checked)
                filter.ConIban = 1;
            if (radioButtonIbanNo.Checked)
                filter.ConIban = 0;

            filter.Importi = null;
            if (radioButtonImportiSi.Checked)
                filter.Importi = 1;
            if (radioButtonImportiNo.Checked)
                filter.Importi = 0;

            filter.ConDenuncia = null;
            if (radioButtonDenunciaSi.Checked)
                filter.ConDenuncia = 1;
            if (radioButtonDenunciaNo.Checked)
                filter.ConDenuncia = 0;

            filter.ConUtenza = null;
            if (radioButtonUtenzaSi.Checked)
                filter.ConUtenza = 1;
            if (radioButtonUtenzaNo.Checked)
                filter.ConUtenza = 0;

            return filter;
        }

        private void CreateDataGridViewStructLavoratore()
        {
            dataGridViewElencoLav.AllowUserToAddRows = false;
            dataGridViewElencoLav.AllowUserToDeleteRows = false;
            dataGridViewElencoLav.AutoGenerateColumns = false;
            dataGridViewElencoLav.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader);

            dataGridViewElencoLav.Columns.Clear();
            dataGridViewElencoLav.Columns.AddRange(ColumnSelectLavoratore, codiceDataGridViewTextBoxColumnLavoratore,
                cognomeDataGridViewTextBoxColumnLavoratore, nomeDataGridViewTextBoxColumnLavoratore,
                sindacatoDataGridViewTextBoxColumnLavoratore, tipoCessazioneDataGridViewTextBoxColumnLavoratore,
                dataNascitaDataGridViewTextBoxColumnLavoratore, ragioneSocialeDataGridViewTextBoxColumnLavoratore,
                codiceDittaDataGridViewTextBoxColumnLavoratore, indirizzoDataGridViewTextBoxColumnLavoratore,
                codiceFiscaleDataGridViewTextBoxColumnLavoratore);
        }

        private void LoadSindacati()
        {
            comboBoxSindacato.DataSource = _lavoratoriManager.GetElencoSindacati();
        }

        private void LoadCauseCessazioni()
        {
            comboBoxTipoCessazione.DataSource = _lavoratoriManager.GetElencoTipoFineRapporto();
        }

        #endregion

        #region Load Imprese List

        private void CaricaListaImprese()
        {
            groupBoxFiltroImp.Enabled = false;
            groupBoxFiltroLav.Enabled = false;
            dataGridViewElencoImp.Enabled = false;
            dataGridViewElencoLav.Enabled = false;
            ImpresaFilter filter = CreateFilterImpresa();
            backgroundWorkerElencoImprese.RunWorkerAsync(filter);
        }

        private ImpresaFilter CreateFilterImpresa()
        {
            ImpresaFilter filter = new ImpresaFilter();

            if (!string.IsNullOrEmpty(maskedTextBoxImpIdImpresa.Text))
            {
                int idImpresa;
                if (int.TryParse(maskedTextBoxImpIdImpresa.Text, out idImpresa))
                    filter.IdImpresa = idImpresa;

                string idImpOp = comboBoxIdImpDittaOp.Text;
                switch (idImpOp)
                {
                    case "=":
                        filter.IdImpresaOperatore = Operatori.Uguale;
                        break;
                    case ">":
                        filter.IdImpresaOperatore = Operatori.Maggiore;
                        break;
                    case "<":
                        filter.IdImpresaOperatore = Operatori.Minore;
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(textBoxImpRagioneSociale.Text))
                filter.RagioneSociale = textBoxImpRagioneSociale.Text;

            if (!string.IsNullOrEmpty(textBoxImpCodiceFiscale.Text))
                filter.CodiceFiscaleImpresa = textBoxImpCodiceFiscale.Text;

            if (!string.IsNullOrEmpty(textBoxImpPartitaIva.Text))
                filter.PartitaIVA = textBoxImpPartitaIva.Text;

            if (!string.IsNullOrEmpty(textBoxImpIndirizzo.Text))
                filter.IndirizzoSedeLegale = textBoxImpIndirizzo.Text;

            if (!string.IsNullOrEmpty(textBoxImpComune.Text))
                filter.LocalitaSedeLegale = textBoxImpComune.Text;

            if (maskedTextBoxImpCap.MaskCompleted)
                filter.CapSedeLegale = maskedTextBoxImpCap.Text;

            if (!string.IsNullOrEmpty(textBoxImpProvincia.Text))
                filter.ProvinciaSedeLegale = textBoxImpProvincia.Text;

            return filter;
        }

        private void CreateDataGridViewStructImpresa()
        {
            dataGridViewElencoImp.AllowUserToAddRows = false;
            dataGridViewElencoImp.AllowUserToDeleteRows = false;
            dataGridViewElencoImp.AutoGenerateColumns = false;
            dataGridViewElencoImp.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;

            dataGridViewElencoImp.Columns.Clear();
            dataGridViewElencoImp.Columns.AddRange(ColumnSelectImpresa, codiceDataGridViewTextBoxColumnImpresa,
                ragioneSocialeDataGridViewTextBoxColumnImpresa, codiceFiscaleDataGridViewTextBoxColumnImpresa,
                partitaIvaDataGridViewTextBoxColumnImpresa, indirizzoSedeLegaleDataGridViewTextBoxColumnImpresa,
                numeroDipendentiDataGridViewTextBoxColumnImpresa,
                dataDenunciaDipendentiDataGridViewTextBoxColumnImpresa);
        }

        #endregion

        #region Riepilogo

        private void LoadComprensori()
        {
            comboBoxComprensorio.DataSource = _riepilogoManager.GetElencoComprensori();
            comboBoxComprensori.DataSource = _riepilogoManager.GetElencoComprensori();
        }

        private void CaricaDettagli()
        {
            groupBoxParametri.Enabled = false;

            RiepilogoFilter filter = CreateFilterDettagli();

            Riepilogo riepilogo = _riepilogoManager.GetRiepilogo(filter);

            textBoxSalariale.Text = riepilogo.Totali.ImponibileSalariale.ToString("c");
            textBoxCartella.Text = riepilogo.Totali.ImportoCartella.ToString("c");
            textBoxDelega.Text = riepilogo.Totali.ImportoDelega.ToString("c");
            textBoxIscritti.Text = riepilogo.Totali.NumeroLavoratori.ToString();

            textBoxSalarialeCgil.Text = riepilogo.Cgil.ImponibileSalariale.ToString("c");
            textBoxCartellaCgil.Text = riepilogo.Cgil.ImportoCartella.ToString("c");
            textBoxDelegaCgil.Text = riepilogo.Cgil.ImportoDelega.ToString("c");
            textBoxIscrittiCgil.Text = riepilogo.Cgil.NumeroLavoratori.ToString();

            textBoxSalarialeCisl.Text = riepilogo.Cisl.ImponibileSalariale.ToString("c");
            textBoxCartellaCisl.Text = riepilogo.Cisl.ImportoCartella.ToString("c");
            textBoxDelegaCisl.Text = riepilogo.Cisl.ImportoDelega.ToString("c");
            textBoxIscrittiCisl.Text = riepilogo.Cisl.NumeroLavoratori.ToString();

            textBoxSalarialeUil.Text = riepilogo.Uil.ImponibileSalariale.ToString("c");
            textBoxCartellaUil.Text = riepilogo.Uil.ImportoCartella.ToString("c");
            textBoxDelegaUil.Text = riepilogo.Uil.ImportoDelega.ToString("c");
            textBoxIscrittiUil.Text = riepilogo.Uil.NumeroLavoratori.ToString();

            groupBoxParametri.Enabled = true;
        }

        private RiepilogoFilter CreateFilterDettagli()
        {
            RiepilogoFilter filter = new RiepilogoFilter();

            filter.Comprensorio = comboBoxComprensorio.SelectedItem.ToString();
            filter.DataIscrizioneDa = dateTimePickerDataIscrizioneDa.Value;
            filter.DataIscrizioneA = dateTimePickerDataIscrizioneA.Value;

            return filter;
        }

        private void ClearDettagli()
        {
            textBoxSalariale.ResetText();
            textBoxCartella.ResetText();
            textBoxDelega.ResetText();
            textBoxIscritti.ResetText();

            textBoxSalarialeCgil.ResetText();
            textBoxCartellaCgil.ResetText();
            textBoxDelegaCgil.ResetText();
            textBoxIscrittiCgil.ResetText();

            textBoxSalarialeCisl.ResetText();
            textBoxCartellaCisl.ResetText();
            textBoxDelegaCisl.ResetText();
            textBoxIscrittiCisl.ResetText();

            textBoxSalarialeUil.ResetText();
            textBoxCartellaUil.ResetText();
            textBoxDelegaUil.ResetText();
            textBoxIscrittiUil.ResetText();
        }

        #endregion

        #region Update Db

        private void CaricaNuovoDb()
        {
            string fileNameOld = Application.StartupPath + @"\SICE.sdf";

            try
            {
                string fileNameNew = LoadNewDbPath();
                if (!string.IsNullOrEmpty(fileNameNew) && File.Exists(fileNameNew))
                {
                    File.Copy(fileNameNew, fileNameOld, true);

                    MessageBox.Show("Aggiornamento eseguito correttamente.", "Db update",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    dataGridViewElencoLav.DataSource = null;
                    dataGridViewElencoImp.DataSource = null;
                    ClearDettagli();

                    LoadSindacati();
                    LoadCauseCessazioni();
                    LoadComprensori();

                    PulisciFiltroImprese();
                    PulisciFiltroLavoratori();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    "Errore durante l'aggiornamento della base di dati" + Environment.NewLine + "Dettaglio: " +
                    ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AccodaDb()
        {
            try
            {
                string fileNameNew = LoadNewDbPath();
                if (!string.IsNullOrEmpty(fileNameNew) && File.Exists(fileNameNew))
                {
                    AccodaDatiParam param = new AccodaDatiParam
                    {
                        ConnStringDest = ConfigurationManager.ConnectionStrings["SiceCompact"].ConnectionString
                            .Replace("|DataDirectory|", Application.StartupPath),
                        ConnStringSource = $"Data Source = {fileNameNew};Password=tbridge2007"
                    };



                    groupBoxFiltroImp.Enabled = false;
                    groupBoxFiltroLav.Enabled = false;
                    dataGridViewElencoImp.Enabled = false;
                    dataGridViewElencoLav.Enabled = false;

                    groupBoxFiltroImp.Enabled = false;
                    groupBoxFiltroLav.Enabled = false;
                    dataGridViewElencoImp.Enabled = false;
                    dataGridViewElencoLav.Enabled = false;

                    groupBoxParametri.Enabled = false;

                    fileToolStripMenuItem.Enabled = false;
                    helpToolStripMenuItem.Enabled = false;


                    backgroundWorkerAccodaDb.RunWorkerAsync(param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    "Errore durante l'aggiornamento della base di dati" + Environment.NewLine + "Dettaglio: " +
                    ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static string LoadNewDbPath()
        {
            string fileNameNew = null;

            OpenFileDialog openFileDialogDb = new OpenFileDialog
            {
                Filter = "Compact Database files (*.sdf)|*.sdf",
                FilterIndex = 0,
                Multiselect = false,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };

            if (openFileDialogDb.ShowDialog() == DialogResult.OK)
            {
                fileNameNew = openFileDialogDb.FileName;
            }

            return fileNameNew;
        }

        #endregion

        #region Export Excel

        private static string SaveFilePath()
        {
            string fileNameNew = null;

            SaveFileDialog saveFileDialogExcel = new SaveFileDialog();

            saveFileDialogExcel.Filter = "Excel file (*.xls)|*.xls";
            saveFileDialogExcel.FilterIndex = 0;
            saveFileDialogExcel.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (saveFileDialogExcel.ShowDialog() == DialogResult.OK)
                fileNameNew = saveFileDialogExcel.FileName;

            return fileNameNew;
        }

        private static DataSet ConvertImpreseToDataSet(ICollection<Impresa> imprese)
        {
            DataSet ret = new DataSet();

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id Impresa"));
            dt.Columns.Add(new DataColumn("Ragione Sociale"));
            dt.Columns.Add(new DataColumn("Codice Fiscale"));
            dt.Columns.Add(new DataColumn("P.IVA"));
            dt.Columns.Add(new DataColumn("Indirizzo S.L."));
            dt.Columns.Add(new DataColumn("Comune S.L."));
            dt.Columns.Add(new DataColumn("CAP S.L."));
            dt.Columns.Add(new DataColumn("Provincia S.L."));
            dt.Columns.Add(new DataColumn("Num. dipendenti"));
            dt.Columns.Add(new DataColumn("Data ultima denuncia"));

            if (imprese != null && imprese.Count > 0)
            {
                foreach (Impresa impresa in imprese)
                {
                    DataRow dr = dt.NewRow();

                    dr["Id Impresa"] = impresa.IdImpresa;
                    dr["Ragione Sociale"] = impresa.RagioneSociale;
                    dr["Codice Fiscale"] = impresa.CodiceFiscaleImpresa;
                    dr["P.IVA"] = impresa.PartitaIVA;
                    dr["Indirizzo S.L."] = impresa.IndirizzoSedeLegale;
                    dr["Comune S.L."] = impresa.LocalitaSedeLegale;
                    dr["CAP S.L."] = impresa.CapSedeLegale;
                    dr["Provincia S.L."] = impresa.ProvinciaSedeLegale;
                    dr["Num. dipendenti"] = impresa.NumeroDipendenti;
                    dr["Data ultima denuncia"] = impresa.DataDenuncia;

                    dt.Rows.Add(dr);
                }
            }

            ret.Tables.Add(dt);

            return ret;
        }

        private static DataSet ConvertLavoratoriToDataSet(ICollection<Lavoratore> lavoratori)
        {
            DataSet ret = new DataSet();

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Cod Lav"));
            dt.Columns.Add(new DataColumn("Cognome"));
            dt.Columns.Add(new DataColumn("Nome"));
            dt.Columns.Add(new DataColumn("Sindacato"));
            dt.Columns.Add(new DataColumn("Data di Nascita"));
            dt.Columns.Add(new DataColumn("Ragione Sociale"));
            dt.Columns.Add(new DataColumn("Id Impresa"));
            dt.Columns.Add(new DataColumn("Indirizzo Lavoratore"));
            dt.Columns.Add(new DataColumn("Comune Lavoratore"));
            dt.Columns.Add(new DataColumn("CAP Lavoratore"));
            dt.Columns.Add(new DataColumn("Provincia Lavoratore"));
            dt.Columns.Add(new DataColumn("Codice Fiscale"));
            dt.Columns.Add(new DataColumn("Sesso"));
            dt.Columns.Add(new DataColumn("Straniero"));
            dt.Columns.Add(new DataColumn("Contratto"));
            dt.Columns.Add(new DataColumn("Cessazione"));

            if (lavoratori != null && lavoratori.Count > 0)
            {
                foreach (Lavoratore lavoratore in lavoratori)
                {
                    DataRow dr = dt.NewRow();

                    dr["Cod Lav"] = lavoratore.IdLavoratore;
                    dr["Cognome"] = lavoratore.Cognome;
                    dr["Nome"] = lavoratore.Nome;
                    dr["Sindacato"] = lavoratore.Sindacato;
                    dr["Data di Nascita"] = lavoratore.DataNascita;
                    dr["Ragione Sociale"] = lavoratore.RagioneSociale;
                    dr["Id Impresa"] = lavoratore.IdImpresa;
                    dr["Indirizzo Lavoratore"] = lavoratore.IndirizzoDenominazione;
                    dr["Comune Lavoratore"] = lavoratore.IndirizzoComune;
                    dr["CAP Lavoratore"] = lavoratore.IndirizzoCAP;
                    dr["Provincia Lavoratore"] = lavoratore.IndirizzoProvincia;
                    dr["Codice Fiscale"] = lavoratore.CodiceFiscale;
                    dr["Sesso"] = lavoratore.Sesso;
                    dr["Straniero"] = lavoratore.Straniero;
                    dr["Contratto"] = lavoratore.TipoContrattoLavoratore;
                    dr["Cessazione"] = lavoratore.TipoFineRapporto;

                    dt.Rows.Add(dr);
                }
            }

            ret.Tables.Add(dt);

            return ret;
        }

        private static void ExportToExcel(DataSet source, string fileName)
        {
            StreamWriter excelDoc = new StreamWriter(fileName);

            const string startExcelXml = "<xml version>\r\n<Workbook " +
                                         "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                                         " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n " +
                                         "xmlns:x=\"urn:schemas-    microsoft-com:office:" +
                                         "excel\"\r\n xmlns:ss=\"urn:schemas-microsoft-com:" +
                                         "office:spreadsheet\">\r\n <Styles>\r\n " +
                                         "<Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n " +
                                         "<Alignment ss:Vertical=\"Bottom\"/>\r\n <Borders/>" +
                                         "\r\n <Font/>\r\n <Interior/>\r\n <NumberFormat/>" +
                                         "\r\n <Protection/>\r\n </Style>\r\n " +
                                         "<Style ss:ID=\"BoldColumn\">\r\n <Font " +
                                         "x:Family=\"Swiss\" ss:Bold=\"1\"/>\r\n </Style>\r\n " +
                                         "<Style     ss:ID=\"StringLiteral\">\r\n <NumberFormat" +
                                         " ss:Format=\"@\"/>\r\n </Style>\r\n <Style " +
                                         "ss:ID=\"Decimal\">\r\n <NumberFormat " +
                                         "ss:Format=\"0.0000\"/>\r\n </Style>\r\n " +
                                         "<Style ss:ID=\"Integer\">\r\n <NumberFormat " +
                                         "ss:Format=\"0\"/>\r\n </Style>\r\n <Style " +
                                         "ss:ID=\"DateLiteral\">\r\n <NumberFormat " +
                                         "ss:Format=\"dd/mm/yyyy;@\"/>\r\n </Style>\r\n " +
                                         "</Styles>\r\n ";
            const string endExcelXml = "</Workbook>";

            int rowCount = 0;
            int sheetCount = 1;
            /*
           <xml version>
           <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
           xmlns:o="urn:schemas-microsoft-com:office:office"
           xmlns:x="urn:schemas-microsoft-com:office:excel"
           xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
           <Styles>
           <Style ss:ID="Default" ss:Name="Normal">
             <Alignment ss:Vertical="Bottom"/>
             <Borders/>
             <Font/>
             <Interior/>
             <NumberFormat/>
             <Protection/>
           </Style>
           <Style ss:ID="BoldColumn">
             <Font x:Family="Swiss" ss:Bold="1"/>
           </Style>
           <Style ss:ID="StringLiteral">
             <NumberFormat ss:Format="@"/>
           </Style>
           <Style ss:ID="Decimal">
             <NumberFormat ss:Format="0.0000"/>
           </Style>
           <Style ss:ID="Integer">
             <NumberFormat ss:Format="0"/>
           </Style>
           <Style ss:ID="DateLiteral">
             <NumberFormat ss:Format="mm/dd/yyyy;@"/>
           </Style>
           </Styles>
           <Worksheet ss:Name="Sheet1">
           </Worksheet>
           </Workbook>
           */
            excelDoc.Write(startExcelXml);
            excelDoc.Write("<Worksheet ss:Name=\"Sheet" + sheetCount + "\">");
            excelDoc.Write("<Table>");
            excelDoc.Write("<Row>");
            for (int x = 0; x < source.Tables[0].Columns.Count; x++)
            {
                excelDoc.Write("<Cell ss:StyleID=\"BoldColumn\"><Data ss:Type=\"String\">");
                excelDoc.Write(source.Tables[0].Columns[x].ColumnName);
                excelDoc.Write("</Data></Cell>");
            }

            excelDoc.Write("</Row>");
            foreach (DataRow x in source.Tables[0].Rows)
            {
                rowCount++;
                //if the number of rows is > 64000 create a new page to continue output
                if (rowCount == 64000)
                {
                    rowCount = 0;
                    sheetCount++;
                    excelDoc.Write("</Table>");
                    excelDoc.Write(" </Worksheet>");
                    excelDoc.Write("<Worksheet ss:Name=\"Sheet" + sheetCount + "\">");
                    excelDoc.Write("<Table>");
                }

                excelDoc.Write("<Row>"); //ID=" + rowCount + "
                for (int y = 0; y < source.Tables[0].Columns.Count; y++)
                {
                    System.Type rowType = x[y].GetType();
                    switch (rowType.ToString())
                    {
                        case "System.String":
                            string xmLstring = x[y].ToString();
                            xmLstring = xmLstring.Trim();
                            xmLstring = xmLstring.Replace("&", "&");
                            xmLstring = xmLstring.Replace(">", ">");
                            xmLstring = xmLstring.Replace("<", "<");
                            excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                           "<Data ss:Type=\"String\">");
                            excelDoc.Write(xmLstring);
                            excelDoc.Write("</Data></Cell>");
                            break;
                        case "System.DateTime":
                            //Excel has a specific Date Format of YYYY-MM-DD followed by  
                            //the letter 'T' then hh:mm:sss.lll Example 2005-01-31T24:01:21.000
                            //The Following Code puts the date stored in XMLDate 
                            //to the format above
                            DateTime xmlDate = (DateTime) x[y];
                            string xmlDatetoString = xmlDate.Year +
                                                     "-" +
                                                     (xmlDate.Month < 10
                                                         ? "0" +
                                                           xmlDate.Month
                                                         : xmlDate.Month.ToString()) +
                                                     "-" +
                                                     (xmlDate.Day < 10
                                                         ? "0" +
                                                           xmlDate.Day
                                                         : xmlDate.Day.ToString()) +
                                                     "T" +
                                                     (xmlDate.Hour < 10
                                                         ? "0" +
                                                           xmlDate.Hour
                                                         : xmlDate.Hour.ToString()) +
                                                     ":" +
                                                     (xmlDate.Minute < 10
                                                         ? "0" +
                                                           xmlDate.Minute
                                                         : xmlDate.Minute.ToString()) +
                                                     ":" +
                                                     (xmlDate.Second < 10
                                                         ? "0" +
                                                           xmlDate.Second
                                                         : xmlDate.Second.ToString()) +
                                                     ".000";
                            excelDoc.Write("<Cell ss:StyleID=\"DateLiteral\">" +
                                           "<Data ss:Type=\"DateTime\">");
                            excelDoc.Write(xmlDatetoString);
                            excelDoc.Write("</Data></Cell>");
                            break;
                        case "System.Boolean":
                            excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                           "<Data ss:Type=\"String\">");
                            excelDoc.Write(x[y].ToString());
                            excelDoc.Write("</Data></Cell>");
                            break;
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Byte":
                            excelDoc.Write("<Cell ss:StyleID=\"Integer\">" +
                                           "<Data ss:Type=\"Number\">");
                            excelDoc.Write(x[y].ToString());
                            excelDoc.Write("</Data></Cell>");
                            break;
                        case "System.Decimal":
                        case "System.Double":
                            excelDoc.Write("<Cell ss:StyleID=\"Decimal\">" +
                                           "<Data ss:Type=\"Number\">");
                            excelDoc.Write(x[y].ToString());
                            excelDoc.Write("</Data></Cell>");
                            break;
                        case "System.DBNull":
                            excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                           "<Data ss:Type=\"String\">");
                            excelDoc.Write("");
                            excelDoc.Write("</Data></Cell>");
                            break;
                        default:
                            throw new Exception(rowType + " not handled.");
                    }
                }

                excelDoc.Write("</Row>");
            }

            excelDoc.Write("</Table>");
            excelDoc.Write(" </Worksheet>");
            excelDoc.Write(endExcelXml);
            excelDoc.Close();
        }

        #endregion
    }
}