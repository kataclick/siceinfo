using Microsoft.VisualBasic.ApplicationServices;

namespace Cemi.InfoSindacati.FW35.WinApp
{
    public class MyApplicationStart : WindowsFormsApplicationBase
    {
        protected override void OnCreateMainForm()
        {
            MainForm = new FormMain();
        }

        protected override void OnCreateSplashScreen()
        {
            SplashScreen = new SplashForm();
        }
    }
}