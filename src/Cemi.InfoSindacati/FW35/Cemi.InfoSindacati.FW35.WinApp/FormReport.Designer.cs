namespace Cemi.InfoSindacati.FW35.WinApp
{
	partial class FormReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.reportViewerLav = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // reportViewerLav
            // 
            this.reportViewerLav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewerLav.LocalReport.ReportEmbeddedResource = "TBridge.Cemi.InfoSindacati.WinApp.ReportLavoratori.rdlc";
            this.reportViewerLav.Location = new System.Drawing.Point(0, 0);
            this.reportViewerLav.Name = "reportViewerLav";
            this.reportViewerLav.Size = new System.Drawing.Size(609, 430);
            this.reportViewerLav.TabIndex = 0;
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 430);
            this.Controls.Add(this.reportViewerLav);
            this.Name = "FormReport";
            this.Text = "Report";
            this.Load += new System.EventHandler(this.FormReport_Load);
            this.ResumeLayout(false);

		}

		#endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewerLav;

    }
}