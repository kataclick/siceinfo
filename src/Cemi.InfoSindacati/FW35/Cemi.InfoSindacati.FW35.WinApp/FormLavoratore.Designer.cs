namespace Cemi.InfoSindacati.FW35.WinApp
{
    partial class FormLavoratore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLavoratore));
            this.textBoxInizioPeriodo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxFinePeriodo = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxAnnoCE = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxIban = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.checkBoxDoppione = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxIndirizzo = new System.Windows.Forms.TextBox();
            this.textBoxCodiceFiscale = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSesso = new System.Windows.Forms.TextBox();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.textBoxCognome = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxLuogoNascita = new System.Windows.Forms.TextBox();
            this.textBoxDataNascita = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxTipoRapporto = new System.Windows.Forms.TextBox();
            this.checkBoxPartTime = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxTipoFineRapporto = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxCategoria = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxQualifica = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxMansione = new System.Windows.Forms.TextBox();
            this.textBoxDataFineRapporto = new System.Windows.Forms.TextBox();
            this.textBoxContratto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDataInizioRapporto = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxNaturaGiuridica = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxPartitaIva = new System.Windows.Forms.TextBox();
            this.textBoxCodiceFiscaleImp = new System.Windows.Forms.TextBox();
            this.textBoxAttivitaIstat = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxTipoImp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxIdImp = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxIndirizzoSedeAmm = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxIndirizzoSedeLegale = new System.Windows.Forms.TextBox();
            this.textBoxRagioneSociale = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDataRevocaTessera = new System.Windows.Forms.TextBox();
            this.textBoxDataIscrizioneSindacato = new System.Windows.Forms.TextBox();
            this.textBoxSindacatoPrecedente = new System.Windows.Forms.TextBox();
            this.textBoxSindacato = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxDeleghe = new System.Windows.Forms.TextBox();
            this.textBoxSalario = new System.Windows.Forms.TextBox();
            this.textBoxApe = new System.Windows.Forms.TextBox();
            this.textBoxComprensorioSindacale = new System.Windows.Forms.TextBox();
            this.textBoxCartella = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxIdLav = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.printDocumentLav = new System.Drawing.Printing.PrintDocument();
            this.printDialogLav = new System.Windows.Forms.PrintDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonStampa = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxInizioPeriodo
            // 
            this.textBoxInizioPeriodo.BackColor = System.Drawing.Color.White;
            this.textBoxInizioPeriodo.Location = new System.Drawing.Point(278, 18);
            this.textBoxInizioPeriodo.Name = "textBoxInizioPeriodo";
            this.textBoxInizioPeriodo.ReadOnly = true;
            this.textBoxInizioPeriodo.Size = new System.Drawing.Size(148, 20);
            this.textBoxInizioPeriodo.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Da:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(455, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "A:";
            // 
            // textBoxFinePeriodo
            // 
            this.textBoxFinePeriodo.BackColor = System.Drawing.Color.White;
            this.textBoxFinePeriodo.Location = new System.Drawing.Point(481, 18);
            this.textBoxFinePeriodo.Name = "textBoxFinePeriodo";
            this.textBoxFinePeriodo.ReadOnly = true;
            this.textBoxFinePeriodo.Size = new System.Drawing.Size(148, 20);
            this.textBoxFinePeriodo.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxAnnoCE);
            this.groupBox1.Controls.Add(this.textBoxFinePeriodo);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.textBoxInizioPeriodo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(635, 47);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Periodo";
            // 
            // textBoxAnnoCE
            // 
            this.textBoxAnnoCE.BackColor = System.Drawing.Color.White;
            this.textBoxAnnoCE.Location = new System.Drawing.Point(68, 18);
            this.textBoxAnnoCE.Name = "textBoxAnnoCE";
            this.textBoxAnnoCE.ReadOnly = true;
            this.textBoxAnnoCE.Size = new System.Drawing.Size(148, 20);
            this.textBoxAnnoCE.TabIndex = 4;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(51, 12);
            this.label23.TabIndex = 3;
            this.label23.Text = "Anno CE";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxIban);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.checkBoxDoppione);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBoxIndirizzo);
            this.groupBox2.Controls.Add(this.textBoxCodiceFiscale);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBoxSesso);
            this.groupBox2.Controls.Add(this.textBoxNome);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.textBoxCognome);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.textBoxLuogoNascita);
            this.groupBox2.Controls.Add(this.textBoxDataNascita);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(635, 122);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dati anagrafici";
            // 
            // checkBoxIban
            // 
            this.checkBoxIban.AutoSize = true;
            this.checkBoxIban.Enabled = false;
            this.checkBoxIban.Location = new System.Drawing.Point(614, 46);
            this.checkBoxIban.Name = "checkBoxIban";
            this.checkBoxIban.Size = new System.Drawing.Size(15, 14);
            this.checkBoxIban.TabIndex = 9;
            this.checkBoxIban.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(573, 46);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(31, 12);
            this.label40.TabIndex = 8;
            this.label40.Text = "IBAN";
            // 
            // checkBoxDoppione
            // 
            this.checkBoxDoppione.AutoSize = true;
            this.checkBoxDoppione.Enabled = false;
            this.checkBoxDoppione.Location = new System.Drawing.Point(614, 21);
            this.checkBoxDoppione.Name = "checkBoxDoppione";
            this.checkBoxDoppione.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDoppione.TabIndex = 7;
            this.checkBoxDoppione.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(546, 21);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 12);
            this.label39.TabIndex = 6;
            this.label39.Text = "Doppione";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 4;
            this.label6.Text = "Indirizzo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "Codice Fiscale";
            // 
            // textBoxIndirizzo
            // 
            this.textBoxIndirizzo.BackColor = System.Drawing.Color.White;
            this.textBoxIndirizzo.Location = new System.Drawing.Point(100, 93);
            this.textBoxIndirizzo.MaxLength = 16;
            this.textBoxIndirizzo.Name = "textBoxIndirizzo";
            this.textBoxIndirizzo.ReadOnly = true;
            this.textBoxIndirizzo.Size = new System.Drawing.Size(529, 20);
            this.textBoxIndirizzo.TabIndex = 3;
            // 
            // textBoxCodiceFiscale
            // 
            this.textBoxCodiceFiscale.BackColor = System.Drawing.Color.White;
            this.textBoxCodiceFiscale.Location = new System.Drawing.Point(100, 68);
            this.textBoxCodiceFiscale.MaxLength = 16;
            this.textBoxCodiceFiscale.Name = "textBoxCodiceFiscale";
            this.textBoxCodiceFiscale.ReadOnly = true;
            this.textBoxCodiceFiscale.Size = new System.Drawing.Size(148, 20);
            this.textBoxCodiceFiscale.TabIndex = 3;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(320, 46);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(12, 12);
            this.label25.TabIndex = 2;
            this.label25.Text = "A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nato il";
            // 
            // textBoxSesso
            // 
            this.textBoxSesso.BackColor = System.Drawing.Color.White;
            this.textBoxSesso.Location = new System.Drawing.Point(341, 68);
            this.textBoxSesso.Name = "textBoxSesso";
            this.textBoxSesso.ReadOnly = true;
            this.textBoxSesso.Size = new System.Drawing.Size(148, 20);
            this.textBoxSesso.TabIndex = 1;
            // 
            // textBoxNome
            // 
            this.textBoxNome.BackColor = System.Drawing.Color.White;
            this.textBoxNome.Location = new System.Drawing.Point(341, 18);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.ReadOnly = true;
            this.textBoxNome.Size = new System.Drawing.Size(148, 20);
            this.textBoxNome.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(294, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 12);
            this.label26.TabIndex = 0;
            this.label26.Text = "Sesso";
            // 
            // textBoxCognome
            // 
            this.textBoxCognome.BackColor = System.Drawing.Color.White;
            this.textBoxCognome.Location = new System.Drawing.Point(100, 18);
            this.textBoxCognome.Name = "textBoxCognome";
            this.textBoxCognome.ReadOnly = true;
            this.textBoxCognome.Size = new System.Drawing.Size(148, 20);
            this.textBoxCognome.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(295, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(37, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "Nome";
            // 
            // textBoxLuogoNascita
            // 
            this.textBoxLuogoNascita.BackColor = System.Drawing.Color.White;
            this.textBoxLuogoNascita.Location = new System.Drawing.Point(341, 43);
            this.textBoxLuogoNascita.Name = "textBoxLuogoNascita";
            this.textBoxLuogoNascita.ReadOnly = true;
            this.textBoxLuogoNascita.Size = new System.Drawing.Size(148, 20);
            this.textBoxLuogoNascita.TabIndex = 1;
            // 
            // textBoxDataNascita
            // 
            this.textBoxDataNascita.BackColor = System.Drawing.Color.White;
            this.textBoxDataNascita.Location = new System.Drawing.Point(100, 43);
            this.textBoxDataNascita.Name = "textBoxDataNascita";
            this.textBoxDataNascita.ReadOnly = true;
            this.textBoxDataNascita.Size = new System.Drawing.Size(148, 20);
            this.textBoxDataNascita.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cognome";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.textBoxDataRevocaTessera);
            this.groupBox3.Controls.Add(this.textBoxDataIscrizioneSindacato);
            this.groupBox3.Controls.Add(this.textBoxSindacatoPrecedente);
            this.groupBox3.Controls.Add(this.textBoxSindacato);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.textBoxDeleghe);
            this.groupBox3.Controls.Add(this.textBoxSalario);
            this.groupBox3.Controls.Add(this.textBoxApe);
            this.groupBox3.Controls.Add(this.textBoxComprensorioSindacale);
            this.groupBox3.Controls.Add(this.textBoxCartella);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.textBoxIdLav);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(12, 205);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(635, 478);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dati CE";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.textBoxNaturaGiuridica);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.textBoxPartitaIva);
            this.groupBox4.Controls.Add(this.textBoxCodiceFiscaleImp);
            this.groupBox4.Controls.Add(this.textBoxAttivitaIstat);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.textBoxTipoImp);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.textBoxIdImp);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.textBoxIndirizzoSedeAmm);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.textBoxIndirizzoSedeLegale);
            this.groupBox4.Controls.Add(this.textBoxRagioneSociale);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(8, 145);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(621, 324);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Ditta";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.textBoxTipoRapporto);
            this.groupBox5.Controls.Add(this.checkBoxPartTime);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.textBoxTipoFineRapporto);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.textBoxCategoria);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.textBoxQualifica);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.textBoxMansione);
            this.groupBox5.Controls.Add(this.textBoxDataFineRapporto);
            this.groupBox5.Controls.Add(this.textBoxContratto);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.textBoxDataInizioRapporto);
            this.groupBox5.Location = new System.Drawing.Point(6, 198);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(609, 120);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Rapporto di lavoro";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(21, 97);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(56, 12);
            this.label38.TabIndex = 7;
            this.label38.Text = "Rapporto";
            // 
            // textBoxTipoRapporto
            // 
            this.textBoxTipoRapporto.BackColor = System.Drawing.Color.White;
            this.textBoxTipoRapporto.Location = new System.Drawing.Point(87, 94);
            this.textBoxTipoRapporto.Name = "textBoxTipoRapporto";
            this.textBoxTipoRapporto.ReadOnly = true;
            this.textBoxTipoRapporto.Size = new System.Drawing.Size(148, 20);
            this.textBoxTipoRapporto.TabIndex = 6;
            // 
            // checkBoxPartTime
            // 
            this.checkBoxPartTime.AutoSize = true;
            this.checkBoxPartTime.Enabled = false;
            this.checkBoxPartTime.Location = new System.Drawing.Point(320, 97);
            this.checkBoxPartTime.Name = "checkBoxPartTime";
            this.checkBoxPartTime.Size = new System.Drawing.Size(15, 14);
            this.checkBoxPartTime.TabIndex = 5;
            this.checkBoxPartTime.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(252, 97);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 12);
            this.label36.TabIndex = 2;
            this.label36.Text = "Part Time";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(22, 71);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 12);
            this.label35.TabIndex = 2;
            this.label35.Text = "Mansione";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 46);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 12);
            this.label33.TabIndex = 2;
            this.label33.Text = "Contratto";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(59, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 12);
            this.label21.TabIndex = 2;
            this.label21.Text = "Da";
            // 
            // textBoxTipoFineRapporto
            // 
            this.textBoxTipoFineRapporto.BackColor = System.Drawing.Color.White;
            this.textBoxTipoFineRapporto.Location = new System.Drawing.Point(456, 93);
            this.textBoxTipoFineRapporto.Name = "textBoxTipoFineRapporto";
            this.textBoxTipoFineRapporto.ReadOnly = true;
            this.textBoxTipoFineRapporto.Size = new System.Drawing.Size(148, 20);
            this.textBoxTipoFineRapporto.TabIndex = 4;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(353, 96);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(94, 12);
            this.label37.TabIndex = 3;
            this.label37.Text = "Tipo cessazione";
            // 
            // textBoxCategoria
            // 
            this.textBoxCategoria.BackColor = System.Drawing.Color.White;
            this.textBoxCategoria.Location = new System.Drawing.Point(456, 68);
            this.textBoxCategoria.Name = "textBoxCategoria";
            this.textBoxCategoria.ReadOnly = true;
            this.textBoxCategoria.Size = new System.Drawing.Size(148, 20);
            this.textBoxCategoria.TabIndex = 4;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(387, 71);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 12);
            this.label34.TabIndex = 3;
            this.label34.Text = "Categoria";
            // 
            // textBoxQualifica
            // 
            this.textBoxQualifica.BackColor = System.Drawing.Color.White;
            this.textBoxQualifica.Location = new System.Drawing.Point(456, 43);
            this.textBoxQualifica.Name = "textBoxQualifica";
            this.textBoxQualifica.ReadOnly = true;
            this.textBoxQualifica.Size = new System.Drawing.Size(148, 20);
            this.textBoxQualifica.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(394, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 12);
            this.label22.TabIndex = 3;
            this.label22.Text = "Qualifica";
            // 
            // textBoxMansione
            // 
            this.textBoxMansione.BackColor = System.Drawing.Color.White;
            this.textBoxMansione.Location = new System.Drawing.Point(88, 68);
            this.textBoxMansione.Name = "textBoxMansione";
            this.textBoxMansione.ReadOnly = true;
            this.textBoxMansione.Size = new System.Drawing.Size(148, 20);
            this.textBoxMansione.TabIndex = 1;
            // 
            // textBoxDataFineRapporto
            // 
            this.textBoxDataFineRapporto.BackColor = System.Drawing.Color.White;
            this.textBoxDataFineRapporto.Location = new System.Drawing.Point(456, 18);
            this.textBoxDataFineRapporto.Name = "textBoxDataFineRapporto";
            this.textBoxDataFineRapporto.ReadOnly = true;
            this.textBoxDataFineRapporto.Size = new System.Drawing.Size(148, 20);
            this.textBoxDataFineRapporto.TabIndex = 4;
            // 
            // textBoxContratto
            // 
            this.textBoxContratto.BackColor = System.Drawing.Color.White;
            this.textBoxContratto.Location = new System.Drawing.Point(88, 43);
            this.textBoxContratto.Name = "textBoxContratto";
            this.textBoxContratto.ReadOnly = true;
            this.textBoxContratto.Size = new System.Drawing.Size(148, 20);
            this.textBoxContratto.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(435, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "A";
            // 
            // textBoxDataInizioRapporto
            // 
            this.textBoxDataInizioRapporto.BackColor = System.Drawing.Color.White;
            this.textBoxDataInizioRapporto.Location = new System.Drawing.Point(88, 18);
            this.textBoxDataInizioRapporto.Name = "textBoxDataInizioRapporto";
            this.textBoxDataInizioRapporto.ReadOnly = true;
            this.textBoxDataInizioRapporto.Size = new System.Drawing.Size(148, 20);
            this.textBoxDataInizioRapporto.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(36, 121);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 12);
            this.label29.TabIndex = 4;
            this.label29.Text = "Sede Amm.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(33, 96);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 12);
            this.label17.TabIndex = 4;
            this.label17.Text = "Sede Legale";
            // 
            // textBoxNaturaGiuridica
            // 
            this.textBoxNaturaGiuridica.BackColor = System.Drawing.Color.White;
            this.textBoxNaturaGiuridica.Location = new System.Drawing.Point(116, 168);
            this.textBoxNaturaGiuridica.Name = "textBoxNaturaGiuridica";
            this.textBoxNaturaGiuridica.ReadOnly = true;
            this.textBoxNaturaGiuridica.Size = new System.Drawing.Size(148, 20);
            this.textBoxNaturaGiuridica.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 171);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 12);
            this.label20.TabIndex = 2;
            this.label20.Text = "Natura giuridica";
            // 
            // textBoxPartitaIva
            // 
            this.textBoxPartitaIva.BackColor = System.Drawing.Color.White;
            this.textBoxPartitaIva.Location = new System.Drawing.Point(469, 68);
            this.textBoxPartitaIva.Name = "textBoxPartitaIva";
            this.textBoxPartitaIva.ReadOnly = true;
            this.textBoxPartitaIva.Size = new System.Drawing.Size(148, 20);
            this.textBoxPartitaIva.TabIndex = 1;
            // 
            // textBoxCodiceFiscaleImp
            // 
            this.textBoxCodiceFiscaleImp.BackColor = System.Drawing.Color.White;
            this.textBoxCodiceFiscaleImp.Location = new System.Drawing.Point(116, 68);
            this.textBoxCodiceFiscaleImp.Name = "textBoxCodiceFiscaleImp";
            this.textBoxCodiceFiscaleImp.ReadOnly = true;
            this.textBoxCodiceFiscaleImp.Size = new System.Drawing.Size(148, 20);
            this.textBoxCodiceFiscaleImp.TabIndex = 1;
            // 
            // textBoxAttivitaIstat
            // 
            this.textBoxAttivitaIstat.BackColor = System.Drawing.Color.White;
            this.textBoxAttivitaIstat.Location = new System.Drawing.Point(469, 143);
            this.textBoxAttivitaIstat.Name = "textBoxAttivitaIstat";
            this.textBoxAttivitaIstat.ReadOnly = true;
            this.textBoxAttivitaIstat.Size = new System.Drawing.Size(148, 20);
            this.textBoxAttivitaIstat.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(377, 146);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 12);
            this.label18.TabIndex = 2;
            this.label18.Text = "Attivita ISTAT";
            // 
            // textBoxTipoImp
            // 
            this.textBoxTipoImp.BackColor = System.Drawing.Color.White;
            this.textBoxTipoImp.Location = new System.Drawing.Point(116, 143);
            this.textBoxTipoImp.Name = "textBoxTipoImp";
            this.textBoxTipoImp.ReadOnly = true;
            this.textBoxTipoImp.Size = new System.Drawing.Size(148, 20);
            this.textBoxTipoImp.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 146);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "Tipo Impresa";
            // 
            // textBoxIdImp
            // 
            this.textBoxIdImp.BackColor = System.Drawing.Color.White;
            this.textBoxIdImp.Location = new System.Drawing.Point(116, 18);
            this.textBoxIdImp.Name = "textBoxIdImp";
            this.textBoxIdImp.ReadOnly = true;
            this.textBoxIdImp.Size = new System.Drawing.Size(148, 20);
            this.textBoxIdImp.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(33, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 12);
            this.label15.TabIndex = 2;
            this.label15.Text = "Codice Ditta";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(394, 71);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 12);
            this.label32.TabIndex = 0;
            this.label32.Text = "Partita IVA";
            // 
            // textBoxIndirizzoSedeAmm
            // 
            this.textBoxIndirizzoSedeAmm.BackColor = System.Drawing.Color.White;
            this.textBoxIndirizzoSedeAmm.Location = new System.Drawing.Point(116, 118);
            this.textBoxIndirizzoSedeAmm.MaxLength = 16;
            this.textBoxIndirizzoSedeAmm.Name = "textBoxIndirizzoSedeAmm";
            this.textBoxIndirizzoSedeAmm.ReadOnly = true;
            this.textBoxIndirizzoSedeAmm.Size = new System.Drawing.Size(501, 20);
            this.textBoxIndirizzoSedeAmm.TabIndex = 3;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(22, 71);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(83, 12);
            this.label31.TabIndex = 0;
            this.label31.Text = "Codice Fiscale";
            // 
            // textBoxIndirizzoSedeLegale
            // 
            this.textBoxIndirizzoSedeLegale.BackColor = System.Drawing.Color.White;
            this.textBoxIndirizzoSedeLegale.Location = new System.Drawing.Point(116, 93);
            this.textBoxIndirizzoSedeLegale.MaxLength = 16;
            this.textBoxIndirizzoSedeLegale.Name = "textBoxIndirizzoSedeLegale";
            this.textBoxIndirizzoSedeLegale.ReadOnly = true;
            this.textBoxIndirizzoSedeLegale.Size = new System.Drawing.Size(501, 20);
            this.textBoxIndirizzoSedeLegale.TabIndex = 3;
            // 
            // textBoxRagioneSociale
            // 
            this.textBoxRagioneSociale.BackColor = System.Drawing.Color.White;
            this.textBoxRagioneSociale.Location = new System.Drawing.Point(116, 43);
            this.textBoxRagioneSociale.Name = "textBoxRagioneSociale";
            this.textBoxRagioneSociale.ReadOnly = true;
            this.textBoxRagioneSociale.Size = new System.Drawing.Size(261, 20);
            this.textBoxRagioneSociale.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "Ragione Sociale";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(387, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(83, 12);
            this.label30.TabIndex = 5;
            this.label30.Text = "Comprensorio";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(376, 96);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 12);
            this.label12.TabIndex = 5;
            this.label12.Text = "Importo cartella";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(352, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(116, 12);
            this.label28.TabIndex = 4;
            this.label28.Text = "Data revoca tessera";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(449, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "Dal";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(125, 12);
            this.label27.TabIndex = 4;
            this.label27.Text = "Sindacato precedente";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "Iscritto al sindacato";
            // 
            // textBoxDataRevocaTessera
            // 
            this.textBoxDataRevocaTessera.BackColor = System.Drawing.Color.White;
            this.textBoxDataRevocaTessera.Location = new System.Drawing.Point(481, 68);
            this.textBoxDataRevocaTessera.MaxLength = 16;
            this.textBoxDataRevocaTessera.Name = "textBoxDataRevocaTessera";
            this.textBoxDataRevocaTessera.ReadOnly = true;
            this.textBoxDataRevocaTessera.Size = new System.Drawing.Size(148, 20);
            this.textBoxDataRevocaTessera.TabIndex = 3;
            // 
            // textBoxDataIscrizioneSindacato
            // 
            this.textBoxDataIscrizioneSindacato.BackColor = System.Drawing.Color.White;
            this.textBoxDataIscrizioneSindacato.Location = new System.Drawing.Point(481, 43);
            this.textBoxDataIscrizioneSindacato.MaxLength = 16;
            this.textBoxDataIscrizioneSindacato.Name = "textBoxDataIscrizioneSindacato";
            this.textBoxDataIscrizioneSindacato.ReadOnly = true;
            this.textBoxDataIscrizioneSindacato.Size = new System.Drawing.Size(148, 20);
            this.textBoxDataIscrizioneSindacato.TabIndex = 3;
            // 
            // textBoxSindacatoPrecedente
            // 
            this.textBoxSindacatoPrecedente.BackColor = System.Drawing.Color.White;
            this.textBoxSindacatoPrecedente.Location = new System.Drawing.Point(154, 68);
            this.textBoxSindacatoPrecedente.MaxLength = 16;
            this.textBoxSindacatoPrecedente.Name = "textBoxSindacatoPrecedente";
            this.textBoxSindacatoPrecedente.ReadOnly = true;
            this.textBoxSindacatoPrecedente.Size = new System.Drawing.Size(148, 20);
            this.textBoxSindacatoPrecedente.TabIndex = 3;
            // 
            // textBoxSindacato
            // 
            this.textBoxSindacato.BackColor = System.Drawing.Color.White;
            this.textBoxSindacato.Location = new System.Drawing.Point(154, 43);
            this.textBoxSindacato.MaxLength = 16;
            this.textBoxSindacato.Name = "textBoxSindacato";
            this.textBoxSindacato.ReadOnly = true;
            this.textBoxSindacato.Size = new System.Drawing.Size(148, 20);
            this.textBoxSindacato.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(339, 121);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(131, 12);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ultima erogazione APE";
            // 
            // textBoxDeleghe
            // 
            this.textBoxDeleghe.BackColor = System.Drawing.Color.White;
            this.textBoxDeleghe.Location = new System.Drawing.Point(154, 118);
            this.textBoxDeleghe.Name = "textBoxDeleghe";
            this.textBoxDeleghe.ReadOnly = true;
            this.textBoxDeleghe.Size = new System.Drawing.Size(148, 20);
            this.textBoxDeleghe.TabIndex = 1;
            // 
            // textBoxSalario
            // 
            this.textBoxSalario.BackColor = System.Drawing.Color.White;
            this.textBoxSalario.Location = new System.Drawing.Point(154, 93);
            this.textBoxSalario.Name = "textBoxSalario";
            this.textBoxSalario.ReadOnly = true;
            this.textBoxSalario.Size = new System.Drawing.Size(148, 20);
            this.textBoxSalario.TabIndex = 1;
            // 
            // textBoxApe
            // 
            this.textBoxApe.BackColor = System.Drawing.Color.White;
            this.textBoxApe.Location = new System.Drawing.Point(481, 118);
            this.textBoxApe.Name = "textBoxApe";
            this.textBoxApe.ReadOnly = true;
            this.textBoxApe.Size = new System.Drawing.Size(148, 20);
            this.textBoxApe.TabIndex = 1;
            // 
            // textBoxComprensorioSindacale
            // 
            this.textBoxComprensorioSindacale.BackColor = System.Drawing.Color.White;
            this.textBoxComprensorioSindacale.Location = new System.Drawing.Point(481, 18);
            this.textBoxComprensorioSindacale.Name = "textBoxComprensorioSindacale";
            this.textBoxComprensorioSindacale.ReadOnly = true;
            this.textBoxComprensorioSindacale.Size = new System.Drawing.Size(148, 20);
            this.textBoxComprensorioSindacale.TabIndex = 1;
            // 
            // textBoxCartella
            // 
            this.textBoxCartella.BackColor = System.Drawing.Color.White;
            this.textBoxCartella.Location = new System.Drawing.Point(481, 93);
            this.textBoxCartella.Name = "textBoxCartella";
            this.textBoxCartella.ReadOnly = true;
            this.textBoxCartella.Size = new System.Drawing.Size(148, 20);
            this.textBoxCartella.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(53, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 12);
            this.label13.TabIndex = 0;
            this.label13.Text = "Importo delega";
            // 
            // textBoxIdLav
            // 
            this.textBoxIdLav.BackColor = System.Drawing.Color.White;
            this.textBoxIdLav.Location = new System.Drawing.Point(154, 18);
            this.textBoxIdLav.Name = "textBoxIdLav";
            this.textBoxIdLav.ReadOnly = true;
            this.textBoxIdLav.Size = new System.Drawing.Size(148, 20);
            this.textBoxIdLav.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 96);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 12);
            this.label11.TabIndex = 0;
            this.label11.Text = "Imponibile salariale";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 12);
            this.label10.TabIndex = 0;
            this.label10.Text = "Codice Lavoratore CE";
            // 
            // printDocumentLav
            // 
            this.printDocumentLav.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentLav_PrintPage);
            // 
            // printDialogLav
            // 
            this.printDialogLav.UseEXDialog = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonStampa});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(664, 25);
            this.toolStrip1.TabIndex = 9;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonStampa
            // 
            this.toolStripButtonStampa.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStampa.Image")));
            this.toolStripButtonStampa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStampa.Name = "toolStripButtonStampa";
            this.toolStripButtonStampa.Size = new System.Drawing.Size(67, 22);
            this.toolStripButtonStampa.Text = "Stampa";
            this.toolStripButtonStampa.Click += new System.EventHandler(this.toolStripButtonStampa_Click);
            // 
            // FormLavoratore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(0, 600);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(664, 686);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(680, 724);
            this.MinimumSize = new System.Drawing.Size(500, 260);
            this.Name = "FormLavoratore";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CEMI - Dettaglio Lavoratore";
            this.Load += new System.EventHandler(this.FormLavoratore_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxInizioPeriodo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFinePeriodo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxIndirizzo;
        private System.Windows.Forms.TextBox textBoxCodiceFiscale;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCognome;
        private System.Windows.Forms.TextBox textBoxDataNascita;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDataIscrizioneSindacato;
        private System.Windows.Forms.TextBox textBoxSindacato;
        private System.Windows.Forms.TextBox textBoxIdLav;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxDeleghe;
        private System.Windows.Forms.TextBox textBoxSalario;
        private System.Windows.Forms.TextBox textBoxApe;
        private System.Windows.Forms.TextBox textBoxCartella;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxIdImp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxRagioneSociale;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxNaturaGiuridica;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxTipoImp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxIndirizzoSedeLegale;
        private System.Drawing.Printing.PrintDocument printDocumentLav;
        private System.Windows.Forms.PrintDialog printDialogLav;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonStampa;
        private System.Windows.Forms.TextBox textBoxAnnoCE;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBoxLuogoNascita;
        private System.Windows.Forms.TextBox textBoxSesso;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxDataRevocaTessera;
        private System.Windows.Forms.TextBox textBoxSindacatoPrecedente;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBoxIndirizzoSedeAmm;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxComprensorioSindacale;
        private System.Windows.Forms.TextBox textBoxPartitaIva;
        private System.Windows.Forms.TextBox textBoxCodiceFiscaleImp;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBoxAttivitaIstat;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxCategoria;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBoxQualifica;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxMansione;
        private System.Windows.Forms.TextBox textBoxDataFineRapporto;
        private System.Windows.Forms.TextBox textBoxContratto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxDataInizioRapporto;
        private System.Windows.Forms.CheckBox checkBoxPartTime;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBoxTipoFineRapporto;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxTipoRapporto;
        private System.Windows.Forms.CheckBox checkBoxIban;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox checkBoxDoppione;
        private System.Windows.Forms.Label label39;
    }
}