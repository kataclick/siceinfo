using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Windows.Forms;
using Cemi.InfoSindacati.FW35.Business;
using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.FW35.WinApp
{
    public partial class FormLavoratore : FormBase
    {
        private readonly LavoratoreDettaglio lavoratore;
        private readonly LavoratoriManager lavoratoriManager;
        private Bitmap memoryImage;

        public FormLavoratore(int idLavoratore)
        {
            InitializeComponent();

            lavoratoriManager = new LavoratoriManager();
            lavoratore = lavoratoriManager.GetLavoratore(idLavoratore);
        }

        #region Events

        private void FormLavoratore_Load(object sender, EventArgs e)
        {
            if (lavoratore.Anno != 0)
                textBoxAnnoCE.Text = lavoratore.Anno.ToString();
            if (lavoratore.InizioPeriodo.ToShortDateString() != "01/01/0001")
                textBoxInizioPeriodo.Text = lavoratore.InizioPeriodo.ToShortDateString();
            if (lavoratore.FinePeriodo.ToShortDateString() != "01/01/0001")
                textBoxFinePeriodo.Text = lavoratore.FinePeriodo.ToShortDateString();
            textBoxCognome.Text = lavoratore.Cognome;
            textBoxNome.Text = lavoratore.Nome;
            if (lavoratore.DataNascita != DateTime.MinValue)
                textBoxDataNascita.Text = lavoratore.DataNascita.ToShortDateString();
            textBoxCodiceFiscale.Text = lavoratore.CodiceFiscale;
            textBoxIndirizzo.Text = lavoratore.IndirizzoCompleto;
            textBoxLuogoNascita.Text = lavoratore.LuogoNascita;
            textBoxSesso.Text = lavoratore.Sesso;
            textBoxIdLav.Text = lavoratore.IdLavoratore.ToString();
            textBoxSindacato.Text = lavoratore.Sindacato;
            if (lavoratore.DataIscrizioneSindacato != DateTime.MinValue)
                textBoxDataIscrizioneSindacato.Text = lavoratore.DataIscrizioneSindacato.ToShortDateString();
            textBoxSindacatoPrecedente.Text = lavoratore.SindacatoPrecedente;
            if (!string.IsNullOrEmpty(lavoratore.UltimoNrErogazione) &&
                !string.IsNullOrEmpty(lavoratore.AnnoUltimaErogazioneApe))
                textBoxApe.Text =
                    String.Format("{0} / {1}", lavoratore.UltimoNrErogazione, lavoratore.AnnoUltimaErogazioneApe);
            textBoxCartella.Text = lavoratore.ImportoCartella.ToString("c");
            textBoxDeleghe.Text = lavoratore.ImportoDelega.ToString("c");
            textBoxSalario.Text = lavoratore.ImponibileSalariale.ToString("c");
            textBoxComprensorioSindacale.Text = lavoratore.ComprensorioSindacale;
            if (lavoratore.IdImpresa != 0)
                textBoxIdImp.Text = lavoratore.IdImpresa.ToString();
            textBoxRagioneSociale.Text = lavoratore.RagioneSociale;
            textBoxCodiceFiscaleImp.Text = lavoratore.CodiceFiscaleImpresa;
            textBoxPartitaIva.Text = lavoratore.PartitaIva;
            textBoxIndirizzoSedeAmm.Text = lavoratore.IndirizzoCompletoSedeAmministrativa;
            textBoxIndirizzoSedeLegale.Text = lavoratore.IndirizzoCompletoSedeLegale;
            textBoxTipoImp.Text = lavoratore.TipoImpresa;
            textBoxAttivitaIstat.Text = lavoratore.AttivitaIstat;
            textBoxNaturaGiuridica.Text = lavoratore.NaturaGiuridica;
            if ((lavoratore.DataInizioValiditaRapporto != DateTime.MinValue) &&
                (lavoratore.DataInizioValiditaRapporto != DateTime.ParseExact("19000101", "yyyyMMdd", null, DateTimeStyles.None)))
                textBoxDataInizioRapporto.Text = lavoratore.DataInizioValiditaRapporto.ToShortDateString();
            if ((lavoratore.DataFineValiditaRapporto != DateTime.MinValue) &&
                (lavoratore.DataFineValiditaRapporto != DateTime.ParseExact("20790606", "yyyyMMdd", null, DateTimeStyles.None)))
                textBoxDataFineRapporto.Text = lavoratore.DataFineValiditaRapporto.ToShortDateString();
            textBoxCategoria.Text = lavoratore.TipoCategoria;
            textBoxContratto.Text = lavoratore.TipoContratto;
            textBoxQualifica.Text = lavoratore.TipoQualifica;
            textBoxMansione.Text = lavoratore.TipoMansione;
            checkBoxPartTime.Checked = lavoratore.PartTime;
            textBoxTipoFineRapporto.Text = lavoratore.TipoFineRapporto;
            textBoxTipoRapporto.Text = lavoratore.TipoRapporto;
            checkBoxIban.Checked = lavoratore.IbanPresente;
            checkBoxDoppione.Checked = lavoratore.PossibileDoppione;
            if ((lavoratore.DataRevocaTesseraSindacale != DateTime.MinValue) &&
                (lavoratore.DataRevocaTesseraSindacale != DateTime.ParseExact("19000101", "yyyyMMdd", null, DateTimeStyles.None)))
                textBoxDataRevocaTessera.Text = lavoratore.DataRevocaTesseraSindacale.ToShortDateString();
        }

        private void printDocumentLav_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        private void toolStripButtonStampa_Click(object sender, EventArgs e)
        {
            StampaForm();
        }

        #endregion

        #region Stampa

        private void StampaForm()
        {
            CaptureScreen();

            printDialogLav.Document = printDocumentLav;

            if (printDialogLav.ShowDialog() == DialogResult.OK)
            {
                printDocumentLav.Print();
            }
        }

        private void CaptureScreen()
        {
            memoryImage = new Bitmap(Size.Width, Size.Height);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(Location.X, Location.Y, 0, 0, Size, CopyPixelOperation.SourceCopy);
        }

        #endregion
    }
}