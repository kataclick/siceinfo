using System.Collections.Generic;
using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.Type
{
    public class LavoratoriExportParameter
    {
        public List<Lavoratore> Lavoratori { get; set; }
        public string FileName { get; set; }
    }
}