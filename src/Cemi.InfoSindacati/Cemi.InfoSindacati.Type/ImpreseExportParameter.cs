using System.Collections.Generic;
using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.Type
{
    public class ImpreseExportParameter
    {
        public List<Impresa> Imprese { get; set; }
        public string FileName { get; set; }
    }
}