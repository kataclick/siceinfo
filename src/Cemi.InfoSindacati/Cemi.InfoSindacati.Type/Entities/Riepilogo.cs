namespace Cemi.InfoSindacati.Type.Entities
{
    public class Riepilogo
    {
        public Riepilogo()
        {
            Totali = new RiepilogoDettaglio();
            Cgil = new RiepilogoDettaglio();
            Cisl = new RiepilogoDettaglio();
            Uil = new RiepilogoDettaglio();
            NonIscritti = new RiepilogoDettaglio();
        }

        public RiepilogoDettaglio Totali { get; set; }

        public RiepilogoDettaglio Cgil { get; set; }

        public RiepilogoDettaglio Cisl { get; set; }

        public RiepilogoDettaglio Uil { get; set; }

        public RiepilogoDettaglio NonIscritti { get; set; }
    }
}