﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class AccodaDatiParam
    {
        public string ConnStringSource { get; set; }
        public string ConnStringDest { get; set; }
    }
}
