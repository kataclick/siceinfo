using System;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class LavoratoreDettaglio : Lavoratore
    {
        public int Anno { get; set; }

        public string ComprensorioSindacale { get; set; }

        public DateTime DataIscrizioneSindacato { get; set; }

        public string UltimoNrErogazione { get; set; }

        public string AnnoUltimaErogazioneApe { get; set; }

        public DateTime DataRevocaTesseraSindacale { get; set; }

        public string SindacatoPrecedente { get; set; }

        public DateTime InizioPeriodo { get; set; }

        public DateTime FinePeriodo { get; set; }

        public decimal ImponibileSalariale { get; set; }

        public decimal ImportoCartella { get; set; }

        public decimal ImportoDelega { get; set; }

        public string LuogoNascita { get; set; }

        public string Sesso { get; set; }

        public string CodiceFiscaleImpresa { get; set; }

        public string PartitaIva { get; set; }

        public string TipoImpresa { get; set; }

        public string CodiceInail { get; set; }

        public string CodiceInps { get; set; }

        public string AttivitaIstat { get; set; }

        public string NaturaGiuridica { get; set; }

        public string IndirizzoSedeLegale { get; set; }

        public string CapSedeLegale { get; set; }

        public string LocalitaSedeLegale { get; set; }

        public string ProvinciaSedeLegale { get; set; }

        public string IndirizzoCompletoSedeLegale
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(IndirizzoSedeLegale) && !String.IsNullOrEmpty(LocalitaSedeLegale) &&
                    !String.IsNullOrEmpty(CapSedeLegale) && !String.IsNullOrEmpty(ProvinciaSedeLegale))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", IndirizzoSedeLegale, LocalitaSedeLegale, CapSedeLegale,
                                      ProvinciaSedeLegale);
                return ret;
            }
        }

        public string IndirizzoSedeAmministrazione { get; set; }

        public string CapSedeAmministrazione { get; set; }

        public string LocalitaSedeAmministrazione { get; set; }

        public string ProvinciaSedeAmministrazione { get; set; }

        public string IndirizzoCompletoSedeAmministrativa
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(IndirizzoSedeAmministrazione) &&
                    !String.IsNullOrEmpty(LocalitaSedeAmministrazione) && !String.IsNullOrEmpty(CapSedeAmministrazione) &&
                    !String.IsNullOrEmpty(ProvinciaSedeAmministrazione))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", IndirizzoSedeAmministrazione, LocalitaSedeAmministrazione,
                                      CapSedeAmministrazione, ProvinciaSedeAmministrazione);
                return ret;
            }
        }

        public string TelefonoSedeAmministrazione { get; set; }

        public string FaxSedeAmministrazione { get; set; }

        public string EMailSedeAmministrazione { get; set; }

        public string TelefonoSedeLegale { get; set; }

        public string FaxSedeLegale { get; set; }

        public string EMailSedeLegale { get; set; }

        public DateTime DataInizioValiditaRapporto { get; set; }

        public DateTime DataFineValiditaRapporto { get; set; }

        public string TipoContratto { get; set; }

        public string TipoCategoria { get; set; }

        public string TipoQualifica { get; set; }

        public string TipoMansione { get; set; }

        public bool PartTime { get; set; }

        public string TipoRapporto { get; set; }

        public bool IbanPresente { get; set; }

        public bool PossibileDoppione { get; set; }
    }
}