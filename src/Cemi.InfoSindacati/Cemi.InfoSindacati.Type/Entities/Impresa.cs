using System;

namespace Cemi.InfoSindacati.Type.Entities
{
    public class Impresa
    {
        private string capSedeLegale;
        private string indirizzoSedeLegale;
        private string localitaSedeLegale;
        private string provinciaSedeLegale;

        public int IdImpresa { get; set; }

        public string RagioneSociale { get; set; }

        public string CodiceFiscaleImpresa { get; set; }

        public string PartitaIVA { get; set; }

        public string IndirizzoSedeLegale
        {
            get { return indirizzoSedeLegale; }
            set { indirizzoSedeLegale = value; }
        }

        public string CapSedeLegale
        {
            get { return capSedeLegale; }
            set { capSedeLegale = value; }
        }

        public string LocalitaSedeLegale
        {
            get { return localitaSedeLegale; }
            set { localitaSedeLegale = value; }
        }

        public string ProvinciaSedeLegale
        {
            get { return provinciaSedeLegale; }
            set { provinciaSedeLegale = value; }
        }

        public string IndirizzoCompletoSedeLegale
        {
            get
            {
                string ret = string.Empty;
                if (!String.IsNullOrEmpty(indirizzoSedeLegale) && !String.IsNullOrEmpty(localitaSedeLegale) &&
                    !String.IsNullOrEmpty(capSedeLegale) && !String.IsNullOrEmpty(provinciaSedeLegale))
                    ret =
                        String.Format("{0}, {1}, {2} ({3})", indirizzoSedeLegale, localitaSedeLegale, capSedeLegale,
                                      provinciaSedeLegale);
                return ret;
            }
        }

        public int NumeroDipendenti { get; set; }

        public DateTime DataDenuncia { get; set; }
    }
}