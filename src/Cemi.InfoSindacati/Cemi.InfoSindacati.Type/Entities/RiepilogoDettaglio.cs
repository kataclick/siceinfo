﻿namespace Cemi.InfoSindacati.Type.Entities
{
    public class RiepilogoDettaglio
    {
        public decimal ImponibileSalariale { get; set; }

        public decimal ImportoCartella { get; set; }

        public decimal ImportoDelega { get; set; }

        public int NumeroLavoratori { get; set; }
    }
}