namespace Cemi.InfoSindacati.WinApp
{
    partial class FormImpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormImpresa));
            this.printDocumentImp = new System.Drawing.Printing.PrintDocument();
            this.printDialogImp = new System.Windows.Forms.PrintDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxClasseAssegnazione = new System.Windows.Forms.TextBox();
            this.textBoxOperaiPartTime = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxInps = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxDataCessazione = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxDataSospensione = new System.Windows.Forms.TextBox();
            this.textBoxTelConsulente = new System.Windows.Forms.TextBox();
            this.textBoxConsulente = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxDataRipresa = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxDataIscrizione = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxEMailSedeAmm = new System.Windows.Forms.TextBox();
            this.textBoxEMailSedeLegale = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textBoxInail = new System.Windows.Forms.TextBox();
            this.textBoxFaxSedeAmm = new System.Windows.Forms.TextBox();
            this.textBoxFaxSedeLegale = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxNaturaGiuridica = new System.Windows.Forms.TextBox();
            this.textBoxTelSedeAmm = new System.Windows.Forms.TextBox();
            this.textBoxTelSedeLegale = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxAttivitaIstat = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTipoImp = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxRappresentanteLegale = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxOperai = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxUltimoPeriodoDenunciato = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxCodiceFiscale = new System.Windows.Forms.TextBox();
            this.textBoxAssociazioneImp = new System.Windows.Forms.TextBox();
            this.textBoxPartitaIva = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxStatoAmm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxIndirizzoSedeAmm = new System.Windows.Forms.TextBox();
            this.textBoxIndirizzoSedeLegale = new System.Windows.Forms.TextBox();
            this.textBoxRagioneSociale = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxIdImp = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonStampa = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // printDocumentImp
            // 
            this.printDocumentImp.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocumentImp_PrintPage);
            // 
            // printDialogImp
            // 
            this.printDialogImp.UseEXDialog = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.textBoxClasseAssegnazione);
            this.groupBox1.Controls.Add(this.textBoxOperaiPartTime);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.textBoxInps);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxDataCessazione);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.textBoxDataSospensione);
            this.groupBox1.Controls.Add(this.textBoxTelConsulente);
            this.groupBox1.Controls.Add(this.textBoxConsulente);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.textBoxDataRipresa);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBoxDataIscrizione);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxEMailSedeAmm);
            this.groupBox1.Controls.Add(this.textBoxEMailSedeLegale);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.textBoxInail);
            this.groupBox1.Controls.Add(this.textBoxFaxSedeAmm);
            this.groupBox1.Controls.Add(this.textBoxFaxSedeLegale);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.textBoxNaturaGiuridica);
            this.groupBox1.Controls.Add(this.textBoxTelSedeAmm);
            this.groupBox1.Controls.Add(this.textBoxTelSedeLegale);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.textBoxAttivitaIstat);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxTipoImp);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.textBoxRappresentanteLegale);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxOperai);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.textBoxUltimoPeriodoDenunciato);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.textBoxCodiceFiscale);
            this.groupBox1.Controls.Add(this.textBoxAssociazioneImp);
            this.groupBox1.Controls.Add(this.textBoxPartitaIva);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxStatoAmm);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.textBoxIndirizzoSedeAmm);
            this.groupBox1.Controls.Add(this.textBoxIndirizzoSedeLegale);
            this.groupBox1.Controls.Add(this.textBoxRagioneSociale);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.textBoxIdImp);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(699, 477);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dati Ditta";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 161);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(140, 12);
            this.label30.TabIndex = 29;
            this.label30.Text = "Gruppo di Appartenenza";
            // 
            // textBoxClasseAssegnazione
            // 
            this.textBoxClasseAssegnazione.BackColor = System.Drawing.Color.White;
            this.textBoxClasseAssegnazione.Location = new System.Drawing.Point(153, 158);
            this.textBoxClasseAssegnazione.Name = "textBoxClasseAssegnazione";
            this.textBoxClasseAssegnazione.ReadOnly = true;
            this.textBoxClasseAssegnazione.Size = new System.Drawing.Size(173, 20);
            this.textBoxClasseAssegnazione.TabIndex = 28;
            // 
            // textBoxOperaiPartTime
            // 
            this.textBoxOperaiPartTime.BackColor = System.Drawing.Color.White;
            this.textBoxOperaiPartTime.Location = new System.Drawing.Point(510, 446);
            this.textBoxOperaiPartTime.Name = "textBoxOperaiPartTime";
            this.textBoxOperaiPartTime.ReadOnly = true;
            this.textBoxOperaiPartTime.Size = new System.Drawing.Size(172, 20);
            this.textBoxOperaiPartTime.TabIndex = 26;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(400, 450);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 12);
            this.label29.TabIndex = 25;
            this.label29.Text = "Operai Part Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(442, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 24;
            this.label3.Text = "Ass. Imp.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(436, 89);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 12);
            this.label22.TabIndex = 24;
            this.label22.Text = "Partita IVA";
            // 
            // textBoxInps
            // 
            this.textBoxInps.BackColor = System.Drawing.Color.White;
            this.textBoxInps.Location = new System.Drawing.Point(510, 182);
            this.textBoxInps.Name = "textBoxInps";
            this.textBoxInps.ReadOnly = true;
            this.textBoxInps.Size = new System.Drawing.Size(172, 20);
            this.textBoxInps.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(465, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "INPS";
            // 
            // textBoxDataCessazione
            // 
            this.textBoxDataCessazione.BackColor = System.Drawing.Color.White;
            this.textBoxDataCessazione.Location = new System.Drawing.Point(510, 374);
            this.textBoxDataCessazione.Name = "textBoxDataCessazione";
            this.textBoxDataCessazione.ReadOnly = true;
            this.textBoxDataCessazione.Size = new System.Drawing.Size(172, 20);
            this.textBoxDataCessazione.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(402, 378);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 12);
            this.label11.TabIndex = 21;
            this.label11.Text = "Data cessazione";
            // 
            // textBoxDataSospensione
            // 
            this.textBoxDataSospensione.BackColor = System.Drawing.Color.White;
            this.textBoxDataSospensione.Location = new System.Drawing.Point(510, 350);
            this.textBoxDataSospensione.Name = "textBoxDataSospensione";
            this.textBoxDataSospensione.ReadOnly = true;
            this.textBoxDataSospensione.Size = new System.Drawing.Size(172, 20);
            this.textBoxDataSospensione.TabIndex = 22;
            // 
            // textBoxTelConsulente
            // 
            this.textBoxTelConsulente.BackColor = System.Drawing.Color.White;
            this.textBoxTelConsulente.Location = new System.Drawing.Point(510, 398);
            this.textBoxTelConsulente.Name = "textBoxTelConsulente";
            this.textBoxTelConsulente.ReadOnly = true;
            this.textBoxTelConsulente.Size = new System.Drawing.Size(172, 20);
            this.textBoxTelConsulente.TabIndex = 22;
            // 
            // textBoxConsulente
            // 
            this.textBoxConsulente.BackColor = System.Drawing.Color.White;
            this.textBoxConsulente.Location = new System.Drawing.Point(154, 398);
            this.textBoxConsulente.Name = "textBoxConsulente";
            this.textBoxConsulente.ReadOnly = true;
            this.textBoxConsulente.Size = new System.Drawing.Size(172, 20);
            this.textBoxConsulente.TabIndex = 22;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(408, 402);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 12);
            this.label28.TabIndex = 21;
            this.label28.Text = "Tel. Consulente";
            // 
            // textBoxDataRipresa
            // 
            this.textBoxDataRipresa.BackColor = System.Drawing.Color.White;
            this.textBoxDataRipresa.Location = new System.Drawing.Point(154, 374);
            this.textBoxDataRipresa.Name = "textBoxDataRipresa";
            this.textBoxDataRipresa.ReadOnly = true;
            this.textBoxDataRipresa.Size = new System.Drawing.Size(172, 20);
            this.textBoxDataRipresa.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(75, 402);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 12);
            this.label12.TabIndex = 21;
            this.label12.Text = "Consulente";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(395, 354);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 12);
            this.label9.TabIndex = 21;
            this.label9.Text = "Data sospensione";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(72, 378);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 12);
            this.label10.TabIndex = 21;
            this.label10.Text = "Data ripresa";
            // 
            // textBoxDataIscrizione
            // 
            this.textBoxDataIscrizione.BackColor = System.Drawing.Color.White;
            this.textBoxDataIscrizione.Location = new System.Drawing.Point(154, 350);
            this.textBoxDataIscrizione.Name = "textBoxDataIscrizione";
            this.textBoxDataIscrizione.ReadOnly = true;
            this.textBoxDataIscrizione.Size = new System.Drawing.Size(172, 20);
            this.textBoxDataIscrizione.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(58, 354);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 12);
            this.label8.TabIndex = 21;
            this.label8.Text = "Data iscrizione";
            // 
            // textBoxEMailSedeAmm
            // 
            this.textBoxEMailSedeAmm.BackColor = System.Drawing.Color.White;
            this.textBoxEMailSedeAmm.Location = new System.Drawing.Point(154, 326);
            this.textBoxEMailSedeAmm.Name = "textBoxEMailSedeAmm";
            this.textBoxEMailSedeAmm.ReadOnly = true;
            this.textBoxEMailSedeAmm.Size = new System.Drawing.Size(172, 20);
            this.textBoxEMailSedeAmm.TabIndex = 22;
            // 
            // textBoxEMailSedeLegale
            // 
            this.textBoxEMailSedeLegale.BackColor = System.Drawing.Color.White;
            this.textBoxEMailSedeLegale.Location = new System.Drawing.Point(154, 254);
            this.textBoxEMailSedeLegale.Name = "textBoxEMailSedeLegale";
            this.textBoxEMailSedeLegale.ReadOnly = true;
            this.textBoxEMailSedeLegale.Size = new System.Drawing.Size(172, 20);
            this.textBoxEMailSedeLegale.TabIndex = 22;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(43, 329);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(103, 12);
            this.label27.TabIndex = 21;
            this.label27.Text = "eMail Sede Amm.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(36, 257);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 12);
            this.label24.TabIndex = 21;
            this.label24.Text = "eMail Sede Legale";
            // 
            // textBoxInail
            // 
            this.textBoxInail.BackColor = System.Drawing.Color.White;
            this.textBoxInail.Location = new System.Drawing.Point(154, 182);
            this.textBoxInail.Name = "textBoxInail";
            this.textBoxInail.ReadOnly = true;
            this.textBoxInail.Size = new System.Drawing.Size(172, 20);
            this.textBoxInail.TabIndex = 22;
            // 
            // textBoxFaxSedeAmm
            // 
            this.textBoxFaxSedeAmm.BackColor = System.Drawing.Color.White;
            this.textBoxFaxSedeAmm.Location = new System.Drawing.Point(510, 302);
            this.textBoxFaxSedeAmm.Name = "textBoxFaxSedeAmm";
            this.textBoxFaxSedeAmm.ReadOnly = true;
            this.textBoxFaxSedeAmm.Size = new System.Drawing.Size(172, 20);
            this.textBoxFaxSedeAmm.TabIndex = 22;
            // 
            // textBoxFaxSedeLegale
            // 
            this.textBoxFaxSedeLegale.BackColor = System.Drawing.Color.White;
            this.textBoxFaxSedeLegale.Location = new System.Drawing.Point(510, 230);
            this.textBoxFaxSedeLegale.Name = "textBoxFaxSedeLegale";
            this.textBoxFaxSedeLegale.ReadOnly = true;
            this.textBoxFaxSedeLegale.Size = new System.Drawing.Size(172, 20);
            this.textBoxFaxSedeLegale.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(107, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "INAIL";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(408, 306);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(94, 12);
            this.label26.TabIndex = 21;
            this.label26.Text = "Fax Sede Amm.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(401, 234);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(98, 12);
            this.label23.TabIndex = 21;
            this.label23.Text = "Fax Sede Legale";
            // 
            // textBoxNaturaGiuridica
            // 
            this.textBoxNaturaGiuridica.BackColor = System.Drawing.Color.White;
            this.textBoxNaturaGiuridica.Location = new System.Drawing.Point(510, 134);
            this.textBoxNaturaGiuridica.Name = "textBoxNaturaGiuridica";
            this.textBoxNaturaGiuridica.ReadOnly = true;
            this.textBoxNaturaGiuridica.Size = new System.Drawing.Size(172, 20);
            this.textBoxNaturaGiuridica.TabIndex = 22;
            // 
            // textBoxTelSedeAmm
            // 
            this.textBoxTelSedeAmm.BackColor = System.Drawing.Color.White;
            this.textBoxTelSedeAmm.Location = new System.Drawing.Point(154, 302);
            this.textBoxTelSedeAmm.Name = "textBoxTelSedeAmm";
            this.textBoxTelSedeAmm.ReadOnly = true;
            this.textBoxTelSedeAmm.Size = new System.Drawing.Size(172, 20);
            this.textBoxTelSedeAmm.TabIndex = 23;
            // 
            // textBoxTelSedeLegale
            // 
            this.textBoxTelSedeLegale.BackColor = System.Drawing.Color.White;
            this.textBoxTelSedeLegale.Location = new System.Drawing.Point(154, 230);
            this.textBoxTelSedeLegale.Name = "textBoxTelSedeLegale";
            this.textBoxTelSedeLegale.ReadOnly = true;
            this.textBoxTelSedeLegale.Size = new System.Drawing.Size(172, 20);
            this.textBoxTelSedeLegale.TabIndex = 23;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(55, 306);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 12);
            this.label25.TabIndex = 20;
            this.label25.Text = "Tel Sede Amm.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(408, 138);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 12);
            this.label20.TabIndex = 21;
            this.label20.Text = "Natura giuridica";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(48, 234);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 12);
            this.label21.TabIndex = 20;
            this.label21.Text = "Tel Sede Legale";
            // 
            // textBoxAttivitaIstat
            // 
            this.textBoxAttivitaIstat.BackColor = System.Drawing.Color.White;
            this.textBoxAttivitaIstat.Location = new System.Drawing.Point(154, 134);
            this.textBoxAttivitaIstat.Name = "textBoxAttivitaIstat";
            this.textBoxAttivitaIstat.ReadOnly = true;
            this.textBoxAttivitaIstat.Size = new System.Drawing.Size(172, 20);
            this.textBoxAttivitaIstat.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(62, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 12);
            this.label5.TabIndex = 20;
            this.label5.Text = "Attivit� ISTAT";
            // 
            // textBoxTipoImp
            // 
            this.textBoxTipoImp.BackColor = System.Drawing.Color.White;
            this.textBoxTipoImp.Location = new System.Drawing.Point(154, 109);
            this.textBoxTipoImp.Name = "textBoxTipoImp";
            this.textBoxTipoImp.ReadOnly = true;
            this.textBoxTipoImp.Size = new System.Drawing.Size(172, 20);
            this.textBoxTipoImp.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(65, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 12);
            this.label19.TabIndex = 20;
            this.label19.Text = "Tipo Impresa";
            // 
            // textBoxRappresentanteLegale
            // 
            this.textBoxRappresentanteLegale.BackColor = System.Drawing.Color.White;
            this.textBoxRappresentanteLegale.Location = new System.Drawing.Point(154, 422);
            this.textBoxRappresentanteLegale.Name = "textBoxRappresentanteLegale";
            this.textBoxRappresentanteLegale.ReadOnly = true;
            this.textBoxRappresentanteLegale.Size = new System.Drawing.Size(172, 20);
            this.textBoxRappresentanteLegale.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 426);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "Rappresentante legale";
            // 
            // textBoxOperai
            // 
            this.textBoxOperai.BackColor = System.Drawing.Color.White;
            this.textBoxOperai.Location = new System.Drawing.Point(153, 446);
            this.textBoxOperai.Name = "textBoxOperai";
            this.textBoxOperai.ReadOnly = true;
            this.textBoxOperai.Size = new System.Drawing.Size(172, 20);
            this.textBoxOperai.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(101, 450);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(42, 12);
            this.label18.TabIndex = 14;
            this.label18.Text = "Operai";
            // 
            // textBoxUltimoPeriodoDenunciato
            // 
            this.textBoxUltimoPeriodoDenunciato.BackColor = System.Drawing.Color.White;
            this.textBoxUltimoPeriodoDenunciato.Location = new System.Drawing.Point(510, 422);
            this.textBoxUltimoPeriodoDenunciato.Name = "textBoxUltimoPeriodoDenunciato";
            this.textBoxUltimoPeriodoDenunciato.ReadOnly = true;
            this.textBoxUltimoPeriodoDenunciato.Size = new System.Drawing.Size(172, 20);
            this.textBoxUltimoPeriodoDenunciato.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(364, 426);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(136, 12);
            this.label14.TabIndex = 12;
            this.label14.Text = "Ultima denuncia inviata";
            // 
            // textBoxCodiceFiscale
            // 
            this.textBoxCodiceFiscale.BackColor = System.Drawing.Color.White;
            this.textBoxCodiceFiscale.Location = new System.Drawing.Point(154, 85);
            this.textBoxCodiceFiscale.Name = "textBoxCodiceFiscale";
            this.textBoxCodiceFiscale.ReadOnly = true;
            this.textBoxCodiceFiscale.Size = new System.Drawing.Size(172, 20);
            this.textBoxCodiceFiscale.TabIndex = 13;
            // 
            // textBoxAssociazioneImp
            // 
            this.textBoxAssociazioneImp.BackColor = System.Drawing.Color.White;
            this.textBoxAssociazioneImp.Location = new System.Drawing.Point(510, 109);
            this.textBoxAssociazioneImp.Name = "textBoxAssociazioneImp";
            this.textBoxAssociazioneImp.ReadOnly = true;
            this.textBoxAssociazioneImp.Size = new System.Drawing.Size(172, 20);
            this.textBoxAssociazioneImp.TabIndex = 13;
            // 
            // textBoxPartitaIva
            // 
            this.textBoxPartitaIva.BackColor = System.Drawing.Color.White;
            this.textBoxPartitaIva.Location = new System.Drawing.Point(510, 85);
            this.textBoxPartitaIva.Name = "textBoxPartitaIva";
            this.textBoxPartitaIva.ReadOnly = true;
            this.textBoxPartitaIva.Size = new System.Drawing.Size(172, 20);
            this.textBoxPartitaIva.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "Codice Fiscale";
            // 
            // textBoxStatoAmm
            // 
            this.textBoxStatoAmm.BackColor = System.Drawing.Color.White;
            this.textBoxStatoAmm.Location = new System.Drawing.Point(510, 38);
            this.textBoxStatoAmm.Name = "textBoxStatoAmm";
            this.textBoxStatoAmm.ReadOnly = true;
            this.textBoxStatoAmm.Size = new System.Drawing.Size(172, 20);
            this.textBoxStatoAmm.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(432, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "Stato Amm.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 281);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(122, 12);
            this.label13.TabIndex = 9;
            this.label13.Text = "Indirizzo Sede Amm.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 209);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(126, 12);
            this.label17.TabIndex = 9;
            this.label17.Text = "Indirizzo Sede Legale";
            // 
            // textBoxIndirizzoSedeAmm
            // 
            this.textBoxIndirizzoSedeAmm.BackColor = System.Drawing.Color.White;
            this.textBoxIndirizzoSedeAmm.Location = new System.Drawing.Point(154, 278);
            this.textBoxIndirizzoSedeAmm.MaxLength = 16;
            this.textBoxIndirizzoSedeAmm.Name = "textBoxIndirizzoSedeAmm";
            this.textBoxIndirizzoSedeAmm.ReadOnly = true;
            this.textBoxIndirizzoSedeAmm.Size = new System.Drawing.Size(528, 20);
            this.textBoxIndirizzoSedeAmm.TabIndex = 8;
            // 
            // textBoxIndirizzoSedeLegale
            // 
            this.textBoxIndirizzoSedeLegale.BackColor = System.Drawing.Color.White;
            this.textBoxIndirizzoSedeLegale.Location = new System.Drawing.Point(154, 206);
            this.textBoxIndirizzoSedeLegale.MaxLength = 16;
            this.textBoxIndirizzoSedeLegale.Name = "textBoxIndirizzoSedeLegale";
            this.textBoxIndirizzoSedeLegale.ReadOnly = true;
            this.textBoxIndirizzoSedeLegale.Size = new System.Drawing.Size(528, 20);
            this.textBoxIndirizzoSedeLegale.TabIndex = 8;
            // 
            // textBoxRagioneSociale
            // 
            this.textBoxRagioneSociale.BackColor = System.Drawing.Color.White;
            this.textBoxRagioneSociale.Location = new System.Drawing.Point(154, 62);
            this.textBoxRagioneSociale.Name = "textBoxRagioneSociale";
            this.textBoxRagioneSociale.ReadOnly = true;
            this.textBoxRagioneSociale.Size = new System.Drawing.Size(528, 20);
            this.textBoxRagioneSociale.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(48, 66);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 12);
            this.label16.TabIndex = 6;
            this.label16.Text = "Ragione Sociale";
            // 
            // textBoxIdImp
            // 
            this.textBoxIdImp.BackColor = System.Drawing.Color.White;
            this.textBoxIdImp.Location = new System.Drawing.Point(154, 38);
            this.textBoxIdImp.Name = "textBoxIdImp";
            this.textBoxIdImp.ReadOnly = true;
            this.textBoxIdImp.Size = new System.Drawing.Size(172, 20);
            this.textBoxIdImp.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(71, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 12);
            this.label15.TabIndex = 4;
            this.label15.Text = "Codice Ditta";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonStampa});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(699, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonStampa
            // 
            this.toolStripButtonStampa.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonStampa.Image")));
            this.toolStripButtonStampa.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStampa.Name = "toolStripButtonStampa";
            this.toolStripButtonStampa.Size = new System.Drawing.Size(67, 22);
            this.toolStripButtonStampa.Text = "Stampa";
            this.toolStripButtonStampa.Click += new System.EventHandler(this.toolStripButtonStampa_Click);
            // 
            // FormImpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMinSize = new System.Drawing.Size(690, 450);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(699, 477);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(715, 515);
            this.MinimumSize = new System.Drawing.Size(500, 260);
            this.Name = "FormImpresa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CEMI - Dettaglio Impresa";
            this.Load += new System.EventHandler(this.FormImpresa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Drawing.Printing.PrintDocument printDocumentImp;
        private System.Windows.Forms.PrintDialog printDialogImp;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxIdImp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxStatoAmm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxIndirizzoSedeLegale;
        private System.Windows.Forms.TextBox textBoxRagioneSociale;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxPartitaIva;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRappresentanteLegale;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxNaturaGiuridica;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxAttivitaIstat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTipoImp;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxInps;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxInail;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxDataCessazione;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxDataSospensione;
        private System.Windows.Forms.TextBox textBoxDataRipresa;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxDataIscrizione;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxConsulente;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxOperai;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxUltimoPeriodoDenunciato;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxIndirizzoSedeAmm;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonStampa;
        private System.Windows.Forms.TextBox textBoxCodiceFiscale;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxEMailSedeAmm;
        private System.Windows.Forms.TextBox textBoxEMailSedeLegale;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBoxFaxSedeAmm;
        private System.Windows.Forms.TextBox textBoxFaxSedeLegale;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxTelSedeAmm;
        private System.Windows.Forms.TextBox textBoxTelSedeLegale;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTelConsulente;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBoxAssociazioneImp;
        private System.Windows.Forms.TextBox textBoxOperaiPartTime;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBoxClasseAssegnazione;
    }
}