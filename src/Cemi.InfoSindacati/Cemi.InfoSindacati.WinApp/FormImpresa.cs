using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using Cemi.InfoSindacati.Business;
using Cemi.InfoSindacati.Type.Entities;

namespace Cemi.InfoSindacati.WinApp
{
    public partial class FormImpresa : FormBase
    {
        private readonly ImpresaDettaglio impresa;
        private readonly ImpreseManager impreseManager;
        private Bitmap memoryImage;

        public FormImpresa(int idImpresa)
        {
            InitializeComponent();

            impreseManager = new ImpreseManager();
            impresa = impreseManager.GetImpresa(idImpresa);
        }

        #region Events

        private void FormImpresa_Load(object sender, EventArgs e)
        {
            textBoxIdImp.Text = impresa.IdImpresa.ToString();
            textBoxStatoAmm.Text = impresa.StatoAmministrativo;
            textBoxRagioneSociale.Text = impresa.RagioneSociale;
            textBoxCodiceFiscale.Text = impresa.CodiceFiscaleImpresa;
            textBoxPartitaIva.Text = impresa.PartitaIVA;
            textBoxTipoImp.Text = impresa.TipoImpresa;
            textBoxAttivitaIstat.Text = impresa.AttivitaIstat;
            textBoxNaturaGiuridica.Text = impresa.NaturaGiuridica;
            textBoxInail.Text = impresa.CodiceINAIL;
            textBoxInps.Text = impresa.CodiceINPS;
            textBoxIndirizzoSedeLegale.Text = impresa.IndirizzoCompletoSedeLegale;
            textBoxIndirizzoSedeAmm.Text = impresa.IndirizzoCompletoSedeAmministrativa;
            textBoxTelSedeLegale.Text = impresa.TelefonoSedeLegale;
            textBoxFaxSedeLegale.Text = impresa.FaxSedeLegale;
            textBoxEMailSedeLegale.Text = impresa.EMailSedeLegale;
            textBoxTelSedeAmm.Text = impresa.TelefonoSedeAmministrazione;
            textBoxFaxSedeAmm.Text = impresa.FaxSedeAmministrazione;
            textBoxEMailSedeAmm.Text = impresa.EMailSedeAmministrazione;
            if (impresa.DataIscrizione != DateTime.MinValue)
                textBoxDataIscrizione.Text = impresa.DataIscrizione.ToShortDateString();
            if (impresa.DataCessazioneAttivita != DateTime.MinValue)
                textBoxDataCessazione.Text = impresa.DataCessazioneAttivita.ToShortDateString();
            if (impresa.DataSospensioneAttivita != DateTime.MinValue)
                textBoxDataSospensione.Text = impresa.DataSospensioneAttivita.ToShortDateString();
            if (impresa.DataRipresaAttivita != DateTime.MinValue)
                textBoxDataRipresa.Text = impresa.DataRipresaAttivita.ToShortDateString();
            textBoxConsulente.Text = impresa.RagioneSocialeConsulente;
            textBoxRappresentanteLegale.Text = impresa.RappresentanteLegale;
            if (impresa.DataDenuncia != DateTime.MinValue)
            {
                textBoxUltimoPeriodoDenunciato.Text = impresa.DataDenuncia.ToShortDateString();
                textBoxOperai.Text = impresa.NumeroDipendenti.ToString();
                textBoxOperaiPartTime.Text = impresa.NumeroDipendentiPartTime.ToString();
            }
            textBoxAssociazioneImp.Text = impresa.AssociazioneImprenditoriale;
            textBoxTelConsulente.Text = impresa.TelefonoConsulente;
            textBoxClasseAssegnazione.Text = impresa.ClasseAssegnazione;
        }

        private void toolStripButtonStampa_Click(object sender, EventArgs e)
        {
            StampaForm();
        }

        private void printDocumentImp_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(memoryImage, 0, 0);
        }

        #endregion

        #region Stampa

        private void StampaForm()
        {
            CaptureScreen();

            printDialogImp.Document = printDocumentImp;

            if (printDialogImp.ShowDialog() == DialogResult.OK)
            {
                printDocumentImp.Print();
            }
        }

        private void CaptureScreen()
        {
            memoryImage = new Bitmap(Size.Width, Size.Height);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(Location.X, Location.Y, 0, 0, Size, CopyPixelOperation.SourceCopy);
        }

        #endregion

    }
}