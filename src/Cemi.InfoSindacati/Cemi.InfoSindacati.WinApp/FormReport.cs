using System;
using System.Data;
using Cemi.InfoSindacati.Business;
using Microsoft.Reporting.WinForms;

namespace Cemi.InfoSindacati.WinApp
{
    public partial class FormReport : FormBase
    {
        private readonly LavoratoriManager lavoratoriManager;
        private readonly int idImpresa;

        public FormReport(int _idImpresa)
        {
            InitializeComponent();

            idImpresa = _idImpresa;
            lavoratoriManager = new LavoratoriManager();
            DataSet ds = lavoratoriManager.GetLavoratori(idImpresa);
            DataSetLavoratori d = new DataSetLavoratori();

            ReportDataSource dsl = new ReportDataSource("DataSetLavoratori_LavoratoriDeleghe", ds.Tables[0]);

            reportViewerLav.LocalReport.DataSources.Add(dsl);
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            reportViewerLav.RefreshReport();
        }
    }
}