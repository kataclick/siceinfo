﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.IO
Imports TuteScarpe.Test.ConsoleAppVb.TuteScarpeServiceReference

Module Module1

    Sub Main()
        Try
            Dim filename As String = ConfigurationManager.AppSettings("Filename")

            Using reader As New StreamReader(filename)
                Dim line As [String]
                Dim informazioniConsegne As New List(Of InformazioniConsegna)()

                While (InlineAssignHelper(line, reader.ReadLine())) IsNot Nothing
                    Dim informazioniConsegna As New InformazioniConsegna()

                    informazioniConsegna.CodiceImpresa = Int32.Parse(line.Substring(10, 10))
                    informazioniConsegna.DataEvento = DateTime.Parse(line.Substring(35, 10))
                    informazioniConsegna.NumeroOrdine = Int32.Parse(line.Substring(0, 10))
                    informazioniConsegna.NumeroSpedizione = line.Substring(20, 15)
                    informazioniConsegna.Stato = line.Substring(45)

                    informazioniConsegne.Add(informazioniConsegna)
                End While

                Dim serviceClient As New UploadServiceClient()
                serviceClient.UploadConsegne(informazioniConsegne.ToArray())
            End Using

        Catch exception As Exception
            Console.WriteLine(exception.Message)
            Console.ReadLine()
        End Try

    End Sub

    Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
        target = value
        Return value
    End Function

End Module
