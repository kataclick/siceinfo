﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using TuteScarpe.Test.ConsoleApp.TuteScarpeServiceReference;

namespace TuteScarpe.Test.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                string filename = ConfigurationManager.AppSettings["Filename"];

                using (StreamReader reader = new StreamReader(filename))
                {
                    String line;
                    List<InformazioniConsegna> informazioniConsegne = new List<InformazioniConsegna>();

                    while ((line = reader.ReadLine()) != null)
                    {
                        InformazioniConsegna informazioniConsegna = new InformazioniConsegna();

                        informazioniConsegna.CodiceImpresa = Int32.Parse(line.Substring(10, 10));
                        informazioniConsegna.DataEvento = DateTime.Parse(line.Substring(35, 10));
                        informazioniConsegna.NumeroOrdine = Int32.Parse(line.Substring(0, 10));
                        informazioniConsegna.NumeroSpedizione = line.Substring(20, 15);
                        informazioniConsegna.Stato = line.Substring(45);

                        informazioniConsegne.Add(informazioniConsegna);
                    }

                    UploadServiceClient serviceClient = new UploadServiceClient();
                    serviceClient.UploadConsegne(informazioniConsegne.ToArray());
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadLine();
            }
            
        }
    }
}