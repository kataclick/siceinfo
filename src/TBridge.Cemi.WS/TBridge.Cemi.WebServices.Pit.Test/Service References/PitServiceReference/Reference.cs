﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace TBridge.Cemi.WebServices.Pit.Test.PitServiceReference
{
    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "AuthenticationResponse",
        Namespace = "http://schemas.datacontract.org/2004/07/TBridge.Cemi.WebServices.Pit")]
    [Serializable]
    public class AuthenticationResponse : object, IExtensibleDataObject, INotifyPropertyChanged
    {
        [OptionalField] private bool AutenticatoField;

        [OptionalField] private string CodiceFiscaleField;

        [OptionalField] private string PartitaIvaField;

        [OptionalField] private int TipoUtenteField;
        [NonSerialized] private ExtensionDataObject extensionDataField;

        [DataMember]
        public bool Autenticato
        {
            get { return AutenticatoField; }
            set
            {
                if ((AutenticatoField.Equals(value) != true))
                {
                    AutenticatoField = value;
                    RaisePropertyChanged("Autenticato");
                }
            }
        }

        [DataMember]
        public string CodiceFiscale
        {
            get { return CodiceFiscaleField; }
            set
            {
                if ((ReferenceEquals(CodiceFiscaleField, value) != true))
                {
                    CodiceFiscaleField = value;
                    RaisePropertyChanged("CodiceFiscale");
                }
            }
        }

        [DataMember]
        public string PartitaIva
        {
            get { return PartitaIvaField; }
            set
            {
                if ((ReferenceEquals(PartitaIvaField, value) != true))
                {
                    PartitaIvaField = value;
                    RaisePropertyChanged("PartitaIva");
                }
            }
        }

        [DataMember]
        public int TipoUtente
        {
            get { return TipoUtenteField; }
            set
            {
                if ((TipoUtenteField.Equals(value) != true))
                {
                    TipoUtenteField = value;
                    RaisePropertyChanged("TipoUtente");
                }
            }
        }

        #region IExtensibleDataObject Members

        [Browsable(false)]
        public ExtensionDataObject ExtensionData
        {
            get { return extensionDataField; }
            set { extensionDataField = value; }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    [ServiceContract(ConfigurationName = "PitServiceReference.IAuthenticationService")]
    public interface IAuthenticationService
    {
        [OperationContract(Action = "http://tempuri.org/IAuthenticationService/VerifyUser",
            ReplyAction = "http://tempuri.org/IAuthenticationService/VerifyUserResponse")]
        AuthenticationResponse VerifyUser(string username, string password);
    }

    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public interface IAuthenticationServiceChannel : IAuthenticationService, IClientChannel
    {
    }

    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class AuthenticationServiceClient : ClientBase<IAuthenticationService>, IAuthenticationService
    {
        public AuthenticationServiceClient()
        {
        }

        public AuthenticationServiceClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public AuthenticationServiceClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public AuthenticationServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public AuthenticationServiceClient(Binding binding, EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        #region IAuthenticationService Members

        public AuthenticationResponse VerifyUser(string username, string password)
        {
            return base.Channel.VerifyUser(username, password);
        }

        #endregion
    }
}