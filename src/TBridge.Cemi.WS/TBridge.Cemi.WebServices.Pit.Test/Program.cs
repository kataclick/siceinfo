using System;
using TBridge.Cemi.WebServices.Pit.Test.PitServiceReference;

namespace TBridge.Cemi.WebServices.Pit.Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                string username = "ricoed90";
                string password = "salvo101264";

                Console.WriteLine("Test Pit");
                Console.WriteLine();
                Console.WriteLine(String.Format("username: {0}  -  password: {1}", username, password));

                AuthenticationServiceClient client = new AuthenticationServiceClient();
                AuthenticationResponse response = client.VerifyUser(username, password);

                Console.WriteLine("Risposta:");
                Console.WriteLine(String.Format("autenticato: {0}", response.Autenticato));
                Console.WriteLine(String.Format("tipo utente: {0}", response.TipoUtente));
                Console.WriteLine(String.Format("CF: {0}", response.CodiceFiscale));
                Console.WriteLine(String.Format("p. IVA: {0}", response.PartitaIva));


                username = "SCHGRL64C12G264I";
                password = "clistre42___";

                Console.WriteLine();
                Console.WriteLine(String.Format("username: {0}  -  password: {1}", username, password));

                response = client.VerifyUser(username, password);

                Console.WriteLine("Risposta:");
                Console.WriteLine(String.Format("autenticato: {0}", response.Autenticato));
                Console.WriteLine(String.Format("tipo utente: {0}", response.TipoUtente));
                Console.WriteLine(String.Format("CF: {0}", response.CodiceFiscale));
                Console.WriteLine(String.Format("p. IVA: {0}", response.PartitaIva));


                username = "SCHGRL64C12G264I";
                password = "clistre42";

                Console.WriteLine();
                Console.WriteLine(String.Format("username: {0}  -  password: {1}", username, password));

                response = client.VerifyUser(username, password);

                Console.WriteLine("Risposta:");
                Console.WriteLine(String.Format("autenticato: {0}", response.Autenticato));
                Console.WriteLine(String.Format("tipo utente: {0}", response.TipoUtente));
                Console.WriteLine(String.Format("CF: {0}", response.CodiceFiscale));
                Console.WriteLine(String.Format("p. IVA: {0}", response.PartitaIva));
            }
            catch (Exception exception)
            {
                Console.WriteLine("Eccezione!!");
                Console.WriteLine(exception.Message);
            }

            Console.Read();
        }
    }
}