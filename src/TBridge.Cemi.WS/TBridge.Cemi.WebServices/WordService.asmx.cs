using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web.Services;
using System.Xml;
using DocumentFormat.OpenXml.CustomProperties;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml.Wordprocessing;

namespace TBridge.Cemi.WebServices
{
    public enum PropertyTypes : int
    {
        YesNo,
        Text,
        DateTime,
        NumberInteger,
        NumberDouble
    }

    public class PropHelper
    {
        public static string SetCustomProperty(
            WordprocessingDocument document,
            string propertyName,
            object propertyValue,
            PropertyTypes propertyType)
        {
            // Given a document name, a property name/value, and the property type, 
            // add a custom property to a document. The method returns the original
            // value, if it existed.

            string returnValue = null;

            var newProp = new CustomDocumentProperty();
            bool propSet = false;

            // Calculate the correct type.
            switch (propertyType)
            {
                case PropertyTypes.DateTime:

                    // Be sure you were passed a real date, 
                    // and if so, format in the correct way. 
                    // The date/time value passed in should 
                    // represent a UTC date/time.
                    if ((propertyValue) is DateTime)
                    {
                        newProp.VTFileTime =
                            new VTFileTime(string.Format("{0:s}Z",
                                Convert.ToDateTime(propertyValue)));
                        propSet = true;
                    }

                    break;

                case PropertyTypes.NumberInteger:
                    if ((propertyValue) is int)
                    {
                        newProp.VTInt32 = new VTInt32(propertyValue.ToString());
                        propSet = true;
                    }

                    break;

                case PropertyTypes.NumberDouble:
                    if (propertyValue is double)
                    {
                        newProp.VTFloat = new VTFloat(propertyValue.ToString());
                        propSet = true;
                    }

                    break;

                case PropertyTypes.Text:
                    newProp.VTLPWSTR = new VTLPWSTR(propertyValue.ToString());
                    propSet = true;

                    break;

                case PropertyTypes.YesNo:
                    if (propertyValue is bool)
                    {
                        // Must be lowercase.
                        newProp.VTBool = new VTBool(
                          Convert.ToBoolean(propertyValue).ToString().ToLower());
                        propSet = true;
                    }
                    break;
            }

            if (!propSet)
            {
                // If the code was not able to convert the 
                // property to a valid value, throw an exception.
                throw new InvalidDataException("propertyValue");
            }

            // Now that you have handled the parameters, start
            // working on the document.
            newProp.FormatId = "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}";
            newProp.Name = propertyName;

            var customProps = document.CustomFilePropertiesPart;
            if (customProps == null)
            {
                // No custom properties? Add the part, and the
                // collection of properties now.
                customProps = document.AddCustomFilePropertiesPart();
                customProps.Properties =
                    new DocumentFormat.OpenXml.CustomProperties.Properties();
            }

            var props = customProps.Properties;
            if (props != null)
            {
                // This will trigger an exception if the property's Name 
                // property is null, but if that happens, the property is damaged, 
                // and probably should raise an exception.
                var prop =
                    props.Where(
                    p => ((CustomDocumentProperty) p).Name.Value
                        == propertyName).FirstOrDefault();

                // Does the property exist? If so, get the return value, 
                // and then delete the property.
                if (prop != null)
                {
                    returnValue = prop.InnerText;
                    prop.Remove();
                }

                // Append the new property, and 
                // fix up all the property ID values. 
                // The PropertyId value must start at 2.
                props.AppendChild(newProp);
                int pid = 2;
                foreach (CustomDocumentProperty item in props)
                {
                    item.PropertyId = pid++;
                }
                props.Save();
            }
            return returnValue;
        }
    }


    /// <summary>
    ///   Summary description for WordService
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WordService : WebService
    {
        [WebMethod]
        public string CreateWord(string templatePath, List<WordField> properties, string pathGenerazioneDocumenti)
        {
            Dictionary<string, string> ds = GenerateDictionary(properties);
            //string docPath = String.Format("{0}{1}", pathGenerazioneDocumenti, GeneraNomeFileWord());
            String templateExt = Path.GetExtension(templatePath);
            String docPath = String.Empty;
            switch (Path.GetExtension(templatePath.ToUpper()))
            {
                case ".DOCM":
                    docPath = String.Format("{0}{1}", pathGenerazioneDocumenti, GeneraNomeFileWordConMacro());
                    break;
                case ".DOCX":
                    docPath = String.Format("{0}{1}", pathGenerazioneDocumenti, GeneraNomeFileWord());
                    break;
            }

            File.Copy(templatePath, docPath, true);

            using (WordprocessingDocument doc = WordprocessingDocument.Open(docPath, true))
            {
                // DICO AL DOCUMENTO CHE DEVE FARE REFRESH DELLE PROPRIETA' ALL'APERTURA
                DocumentSettingsPart settingsPart =
                doc.MainDocumentPart.GetPartsOfType<DocumentSettingsPart>().First();

                // Create object to update fields on open
                UpdateFieldsOnOpen updateFields = new UpdateFieldsOnOpen();
                updateFields.Val = new DocumentFormat.OpenXml.OnOffValue(true);

                // Insert object into settings part.
                settingsPart.Settings.PrependChild<UpdateFieldsOnOpen>(updateFields);
                settingsPart.Settings.Save();
                /////////////////////////////////////////////////////////////////////////

                // MODIFICA PROPERTY PER ANTEPRIMA DOCUMENTO
                MainDocumentPart mainPart = doc.MainDocumentPart;

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(mainPart.GetStream(FileMode.Open, FileAccess.ReadWrite));
                XmlNamespaceManager xmlnsManager = CreateWordProcessingNamespaceManager(xmlDoc);


                //MODIFICA PROPERTYU FISICA DEL FILE
                //CustomFilePropertiesPart cf = doc.CustomFilePropertiesPart;

                //var xmlProp = new XmlDocument();
                //xmlProp.Load(cf.GetStream(FileMode.Open, FileAccess.ReadWrite));
                //XmlNamespaceManager xmlnsManagerProp = CreateWordCustomProcessingNamespaceManager(xmlProp);

                //const string strPropExpression = "//ns:property/@name";
                ////"/w:document/w:body/*/w:fldSimple/*/w:t";

                //XmlNodeList NodeListProp = xmlProp.SelectNodes(strPropExpression, xmlnsManagerProp);
                //if (NodeListProp != null)
                //{
                //    for (int j = 0; j < NodeListProp.Count; j++)
                //    {
                //        string filtro = "//ns:property[@name= '" + NodeListProp[j].InnerText + "']/vt:lpwstr";
                //        XmlNodeList valuePropNodes = xmlProp.SelectNodes(filtro, xmlnsManagerProp);
                //        if (valuePropNodes != null)
                //        {
                //            for (int i = 0; i < valuePropNodes.Count; i++)
                //            {
                //                string nomeCampo =
                //                    NodeListProp[j].InnerText.Replace(@" DOCPROPERTY  ", "").Replace(
                //                        @"  \* MERGEFORMAT ", "");
                //                if (ds.ContainsKey(nomeCampo))
                //                    valuePropNodes[i].InnerText = ds[nomeCampo];
                //            }
                //        }
                //    }
                //}

                //xmlProp.Save(cf.GetStream(FileMode.Open, FileAccess.ReadWrite));
                foreach (String nomeProp in ds.Keys)
                {
                    PropHelper.SetCustomProperty(doc, nomeProp, ds[nomeProp], PropertyTypes.Text);
                }

                //const string strExpression = "//w:fldSimple/@w:instr";
                ////"/w:document/w:body/*/w:fldSimple/*/w:t";

                //XmlNodeList NodeList = xmlDoc.SelectNodes(strExpression, xmlnsManager);
                //if (NodeList != null)
                //{
                //    for (int j = 0; j < NodeList.Count; j++)
                //    {
                //        string filtro = "//w:fldSimple[@w:instr = '" + NodeList[j].InnerText + "']/*/w:t";
                //        XmlNodeList valueNodes = xmlDoc.SelectNodes(filtro, xmlnsManager);
                //        if (valueNodes != null)
                //        {
                //            for (int i = 0; i < valueNodes.Count; i++)
                //            {
                //                string nomeCampo =
                //                    NodeList[j].InnerText.Replace(@" DOCPROPERTY  ", "").Replace(@"  \* MERGEFORMAT ",
                //                                                                                 "");
                //                if (ds.ContainsKey(nomeCampo))
                //                    valueNodes[i].InnerText = ds[nomeCampo];
                //            }
                //        }
                //    }
                //}
                //xmlDoc.Save(mainPart.GetStream(FileMode.Open, FileAccess.ReadWrite));
            }

            return docPath;
        }

        private static Dictionary<string, string> GenerateDictionary(IEnumerable<WordField> properties)
        {
            var ret = new Dictionary<string, string>();

            foreach (WordField field in properties)
            {
                if (!ret.ContainsKey(field.Campo))
                    ret.Add(field.Campo, field.Valore);
            }

            return ret;
        }

        private static XmlNamespaceManager CreateWordProcessingNamespaceManager(XmlDocument xmlDoc)
        {
            // Define all the namespaces 
            var nmNamespaceMgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nmNamespaceMgr.AddNamespace("ve", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            nmNamespaceMgr.AddNamespace("o", "urn:schemas-microsoft-com:office:office");
            nmNamespaceMgr.AddNamespace("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            nmNamespaceMgr.AddNamespace("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            nmNamespaceMgr.AddNamespace("v", "urn:schemas-microsoft-com:vml");
            nmNamespaceMgr.AddNamespace("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            nmNamespaceMgr.AddNamespace("w10", "urn:schemas-microsoft-com:office:word");
            nmNamespaceMgr.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            nmNamespaceMgr.AddNamespace("ds", "http://schemas.openxmlformats.org/officeDocument/2006/customXml");
            nmNamespaceMgr.AddNamespace("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            nmNamespaceMgr.AddNamespace("vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
            nmNamespaceMgr.AddNamespace("", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties");
            //nmNamespaceMgr.AddNamespace("", "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties");
            /* These namespaces are not required however can be added if required at a later stage. 
            nsNamespaceMgr.AddNamespace("o12", "http://schemas.microsoft.com/office/2004/7/core"); 
            nsNamespaceMgr.AddNamespace("m", "http://schemas.microsoft.com/office/omml/2004/12/core"); 
            nsNamespaceMgr.AddNamespace("wp", "http://schemas.openxmlformats.org/drawingml/2006/3/wordprocessingDrawing"); 
            nsNamespaceMgr.AddNamespace("a", "http://schemas.openxmlformats.org/drawingml/2006/3/main"); 
            nsNamespaceMgr.AddNamespace("pic", "http://schemas.openxmlformats.org/drawingml/2006/3/picture"); 
            */
            return nmNamespaceMgr;
        }

        private static XmlNamespaceManager CreateWordCustomProcessingNamespaceManager(XmlDocument xmlProp)
        {
            // Define all the namespaces 
            var nmNamespaceMgr = new XmlNamespaceManager(xmlProp.NameTable);
            nmNamespaceMgr.AddNamespace("ns", "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties");
            nmNamespaceMgr.AddNamespace("vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
            return nmNamespaceMgr;
        }

        private static string GeneraNomeFileWord()
        {
            var random = new Random();

            return String.Format("{0}-{1}.docx", DateTime.Now.ToString("yyyyMMddhhmmssfffffff"), random.Next());
        }

        private static string GeneraNomeFileWordConMacro()
        {
            var random = new Random();

            return String.Format("{0}-{1}.docm", DateTime.Now.ToString("yyyyMMddhhmmssfffffff"), random.Next());
        }
    }
}