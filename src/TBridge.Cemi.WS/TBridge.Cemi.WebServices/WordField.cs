using System;

namespace TBridge.Cemi.WebServices
{
    [Serializable]
    public class WordField
    {
        public string Campo { get; set; }
        public string Valore { get; set; }
    }
}