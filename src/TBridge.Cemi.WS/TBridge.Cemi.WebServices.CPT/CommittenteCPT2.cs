﻿using System;

namespace TBridge.Cemi.WebServices.CPT
{
    [Serializable]
    public class CommittenteCPT2
    {
        public String Cognome { get; set; }

        public String Nome { get; set; }

        public String CodiceFiscale { get; set; }
    }
}