using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.WebServices.CPT
{
    /// <summary>
    ///   Summary description for CPTService
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/CPT/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class CPTService : WebService
    {
        private readonly Database databaseCemi = DatabaseFactory.CreateDatabase("CEMI");

        [WebMethod]
        public List<CantiereCPT> GetCantieri()
        {
            List<CantiereCPT> ret = new List<CantiereCPT>();

            try
            {
                using (DbCommand dbCommandCantiere = databaseCemi.GetStoredProcCommand("dbo.USP_CptCantieriSelectAll"))
                {
                    dbCommandCantiere.CommandTimeout = 900;
                    //ret = databaseCemi.ExecuteDataSet(dbCommand);
                    using (IDataReader drCantiere = databaseCemi.ExecuteReader(dbCommandCantiere))
                    {
                        int ammontareLavoriIndex = drCantiere.GetOrdinal("ammontareLavori");
                        int cantiereCapIndex = drCantiere.GetOrdinal("cantiereCap");
                        int cantiereCivicoIndex = drCantiere.GetOrdinal("cantiereCivico");
                        int cantiereComuneIndex = drCantiere.GetOrdinal("cantiereComune");
                        int cantiereIndirizzoIndex = drCantiere.GetOrdinal("cantiereIndirizzo");
                        int cantiereInfoAggiuntivaIndex = drCantiere.GetOrdinal("cantiereInfoAggiuntiva");
                        int cantiereLatitudineIndex = drCantiere.GetOrdinal("cantiereLatitudine");
                        int cantiereLongitudineIndex = drCantiere.GetOrdinal("cantiereLongitudine");
                        int cantiereProvinciaIndex = drCantiere.GetOrdinal("cantiereProvincia");
                        int codiceCantiereIndex = drCantiere.GetOrdinal("codiceCantiere");
                        int committenteRagioneSocialeIndex = drCantiere.GetOrdinal("committenteRagioneSociale");
                        int coordinatoreEsecuzioneNominativoIndex =
                            drCantiere.GetOrdinal("coordinatoreEsecuzioneNominativo");
                        int coordinatoreEsecuzioneRagioneSocialeIndex =
                            drCantiere.GetOrdinal("coordinatoreEsecuzioneRagioneSociale");
                        int dataFineLavoriIndex = drCantiere.GetOrdinal("dataFineLavori");
                        int dataInizioLavoriIndex = drCantiere.GetOrdinal("dataInizioLavori");
                        int durataLavoriIndex = drCantiere.GetOrdinal("durataLavori");
                        int naturaLavoriIndex = drCantiere.GetOrdinal("naturaLavori");
                        int numeroAppaltoIndex = drCantiere.GetOrdinal("numeroAppalto");
                        int protocolloRegioneIndex = drCantiere.GetOrdinal("protocolloRegione");

                        while (drCantiere.Read())
                        {
                            CantiereCPT cantiere = new CantiereCPT();

                            cantiere.CodiceCantiere = drCantiere.GetInt32(codiceCantiereIndex);

                            if (!drCantiere.IsDBNull(ammontareLavoriIndex))
                                cantiere.AmmontareLavori = drCantiere.GetDecimal(ammontareLavoriIndex);
                            if (!drCantiere.IsDBNull(cantiereCapIndex))
                                cantiere.CantiereCap = drCantiere.GetString(cantiereCapIndex);
                            if (!drCantiere.IsDBNull(cantiereCivicoIndex))
                                cantiere.CantiereCivico = drCantiere.GetString(cantiereCivicoIndex);
                            if (!drCantiere.IsDBNull(cantiereComuneIndex))
                                cantiere.CantiereComune = drCantiere.GetString(cantiereComuneIndex);
                            if (!drCantiere.IsDBNull(cantiereIndirizzoIndex))
                                cantiere.CantiereIndirizzo = drCantiere.GetString(cantiereIndirizzoIndex);
                            if (!drCantiere.IsDBNull(cantiereInfoAggiuntivaIndex))
                                cantiere.CantiereInfoAggiuntiva = drCantiere.GetString(cantiereInfoAggiuntivaIndex);
                            if (!drCantiere.IsDBNull(cantiereLatitudineIndex))
                                cantiere.CantiereLatitudine = drCantiere.GetDecimal(cantiereLatitudineIndex);
                            if (!drCantiere.IsDBNull(cantiereLongitudineIndex))
                                cantiere.CantiereLongitudine = drCantiere.GetDecimal(cantiereLongitudineIndex);
                            if (!drCantiere.IsDBNull(cantiereProvinciaIndex))
                                cantiere.CantiereProvincia = drCantiere.GetString(cantiereProvinciaIndex);
                            if (!drCantiere.IsDBNull(committenteRagioneSocialeIndex))
                                cantiere.CommittenteRagioneSociale = drCantiere.GetString(committenteRagioneSocialeIndex);
                            if (!drCantiere.IsDBNull(coordinatoreEsecuzioneNominativoIndex))
                                cantiere.CoordinatoreEsecuzioneNominativo =
                                    drCantiere.GetString(coordinatoreEsecuzioneNominativoIndex);
                            if (!drCantiere.IsDBNull(coordinatoreEsecuzioneRagioneSocialeIndex))
                                cantiere.CoordinatoreEsecuzioneRagioneSociale =
                                    drCantiere.GetString(coordinatoreEsecuzioneRagioneSocialeIndex);
                            if (!drCantiere.IsDBNull(dataFineLavoriIndex))
                                cantiere.DataFineLavori = drCantiere.GetDateTime(dataFineLavoriIndex);
                            if (!drCantiere.IsDBNull(dataInizioLavoriIndex))
                                cantiere.DataInizioLavori = drCantiere.GetDateTime(dataInizioLavoriIndex);
                            if (!drCantiere.IsDBNull(durataLavoriIndex))
                                cantiere.DurataLavori = drCantiere.GetInt32(durataLavoriIndex);
                            if (!drCantiere.IsDBNull(naturaLavoriIndex))
                                cantiere.NaturaLavori = drCantiere.GetString(naturaLavoriIndex);
                            if (!drCantiere.IsDBNull(numeroAppaltoIndex))
                                cantiere.NumeroAppalto = drCantiere.GetString(numeroAppaltoIndex);
                            if (!drCantiere.IsDBNull(protocolloRegioneIndex))
                                cantiere.ProtocolloRegione = drCantiere.GetString(protocolloRegioneIndex);

                            using (
                                DbCommand dbCommandSubappalto =
                                    databaseCemi.GetStoredProcCommand("dbo.USP_CptSubappaltiSelectByIdCantiere"))
                            {
                                databaseCemi.AddInParameter(dbCommandSubappalto, "@idCantiere", DbType.Int32,
                                                            cantiere.CodiceCantiere);

                                dbCommandSubappalto.CommandTimeout = 900;

                                using (IDataReader drSubappalto = databaseCemi.ExecuteReader(dbCommandSubappalto))
                                {
                                    int impresaAppaltataDacodiceSubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDacodice");
                                    int impresaAppaltataDaPartitaIVASubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDaPartitaIVA");
                                    int impresaAppaltataDaCodiceFiscaleSubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDaCodiceFiscale");
                                    int impresaAppaltataDaRagioneSocialeSubIndex =
                                        drSubappalto.GetOrdinal("impresaAppaltataDaRagioneSociale");
                                    int impresaSelezionatacodiceSubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionatacodice");
                                    int impresaSelezionataPartitaIVASubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionataPartitaIVA");
                                    int impresaSelezionataCodiceFiscaleSubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionataCodiceFiscale");
                                    int impresaSelezionataRagioneSocialeSubIndex =
                                        drSubappalto.GetOrdinal("impresaSelezionataRagioneSociale");

                                    while (drSubappalto.Read())
                                    {
                                        SubappaltoCPT subappalto = new SubappaltoCPT();

                                        subappalto.CodiceCantiere = cantiere.CodiceCantiere;

                                        if (!drSubappalto.IsDBNull(impresaAppaltataDacodiceSubIndex))
                                            subappalto.ImpresaAppaltataDaCodice =
                                                drSubappalto.GetInt32(impresaAppaltataDacodiceSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaAppaltataDaPartitaIVASubIndex))
                                            subappalto.ImpresaAppaltataDaPartitaIVA =
                                                drSubappalto.GetString(impresaAppaltataDaPartitaIVASubIndex);
                                        if (!drSubappalto.IsDBNull(impresaAppaltataDaCodiceFiscaleSubIndex))
                                            subappalto.ImpresaAppaltataDaCodiceFiscale =
                                                drSubappalto.GetString(impresaAppaltataDaCodiceFiscaleSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaAppaltataDaRagioneSocialeSubIndex))
                                            subappalto.ImpresaAppaltataDaRagioneSociale =
                                                drSubappalto.GetString(impresaAppaltataDaRagioneSocialeSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionatacodiceSubIndex))
                                            subappalto.ImpresaSelezionataCodice =
                                                drSubappalto.GetInt32(impresaSelezionatacodiceSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionataPartitaIVASubIndex))
                                            subappalto.ImpresaSelezionataPartitaIVA =
                                                drSubappalto.GetString(impresaSelezionataPartitaIVASubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionataCodiceFiscaleSubIndex))
                                            subappalto.ImpresaSelezionataCodiceFiscale =
                                                drSubappalto.GetString(impresaSelezionataCodiceFiscaleSubIndex);
                                        if (!drSubappalto.IsDBNull(impresaSelezionataRagioneSocialeSubIndex))
                                            subappalto.ImpresaSelezionataRagioneSociale =
                                                drSubappalto.GetString(impresaSelezionataRagioneSocialeSubIndex);

                                        cantiere.Subappalti.Add(subappalto);
                                    }
                                }
                            }

                            ret.Add(cantiere);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }

            return ret;
        }

        [WebMethod]
        public List<CantiereCPT2> GetCantieri2()
        {
            List<CantiereCPT2> ret = new List<CantiereCPT2>();

            try
            {
                using (DbCommand dbCommandCantiere = databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariCantieriSelectAllPerCpt"))
                {
                    dbCommandCantiere.CommandTimeout = 900;
                    //ret = databaseCemi.ExecuteDataSet(dbCommand);
                    using (IDataReader drCantiere = databaseCemi.ExecuteReader(dbCommandCantiere))
                    {
                        int ammontareLavoriIndex = drCantiere.GetOrdinal("ammontareLavori");
                        int cantiereComuneIndex = drCantiere.GetOrdinal("cantiereComune");
                        int cantiereIndirizzoIndex = drCantiere.GetOrdinal("cantiereIndirizzo");
                        int cantiereLatitudineIndex = drCantiere.GetOrdinal("cantiereLatitudine");
                        int cantiereLongitudineIndex = drCantiere.GetOrdinal("cantiereLongitudine");
                        int cantiereProvinciaIndex = drCantiere.GetOrdinal("cantiereProvincia");
                        int codiceCantiereIndex = drCantiere.GetOrdinal("codiceCantiere");
                        int dataInizioLavoriIndex = drCantiere.GetOrdinal("dataInizioLavori");
                        int durataLavoriIndex = drCantiere.GetOrdinal("durataLavori");
                        int descrizioneDurataLavoriIndex = drCantiere.GetOrdinal("descrizioneDurataLavori");
                        int protocolloRegioneIndex = drCantiere.GetOrdinal("protocolloRegione");
                        int tipoOperaIndex = drCantiere.GetOrdinal("descrizioneTipoOpera");
                        int tipoCategoriaIndex = drCantiere.GetOrdinal("descrizioneTipoCategoria");
                        int tipoTipologiaIndex = drCantiere.GetOrdinal("descrizioneTipoTipologia");
                        int altraTipologiaIndex = drCantiere.GetOrdinal("descrizioneAltraTipologia");
                        int altraCategoriaIndex = drCantiere.GetOrdinal("descrizioneAltraCategoria");

                        while (drCantiere.Read())
                        {
                            CantiereCPT2 cantiere = new CantiereCPT2();

                            cantiere.CodiceCantiere = drCantiere.GetInt32(codiceCantiereIndex);

                            if (!drCantiere.IsDBNull(ammontareLavoriIndex))
                                cantiere.AmmontareLavori = drCantiere.GetDecimal(ammontareLavoriIndex);
                            if (!drCantiere.IsDBNull(cantiereComuneIndex))
                                cantiere.CantiereComune = drCantiere.GetString(cantiereComuneIndex);
                            if (!drCantiere.IsDBNull(cantiereIndirizzoIndex))
                                cantiere.CantiereIndirizzo = drCantiere.GetString(cantiereIndirizzoIndex);
                            if (!drCantiere.IsDBNull(cantiereLatitudineIndex))
                                cantiere.CantiereLatitudine = drCantiere.GetDecimal(cantiereLatitudineIndex);
                            if (!drCantiere.IsDBNull(cantiereLongitudineIndex))
                                cantiere.CantiereLongitudine = drCantiere.GetDecimal(cantiereLongitudineIndex);
                            if (!drCantiere.IsDBNull(cantiereProvinciaIndex))
                                cantiere.CantiereProvincia = drCantiere.GetString(cantiereProvinciaIndex);
                            if (!drCantiere.IsDBNull(dataInizioLavoriIndex))
                                cantiere.DataInizioLavori = drCantiere.GetDateTime(dataInizioLavoriIndex);
                            if (!drCantiere.IsDBNull(durataLavoriIndex))
                                cantiere.DurataLavori = drCantiere.GetInt32(durataLavoriIndex);
                            if (!drCantiere.IsDBNull(descrizioneDurataLavoriIndex))
                                cantiere.DescrizioneDurataLavori = drCantiere.GetString(descrizioneDurataLavoriIndex);

                            cantiere.ProtocolloRegione = drCantiere.GetString(protocolloRegioneIndex);

                            if (!drCantiere.IsDBNull(tipoOperaIndex))
                                cantiere.TipoOpera = drCantiere.GetString(tipoOperaIndex);
                            if (!drCantiere.IsDBNull(tipoCategoriaIndex))
                                cantiere.TipoCategoria = drCantiere.GetString(tipoCategoriaIndex);
                            if (!drCantiere.IsDBNull(tipoTipologiaIndex))
                                cantiere.TipoTipologia = drCantiere.GetString(tipoTipologiaIndex);
                            if (!drCantiere.IsDBNull(altraTipologiaIndex))
                                cantiere.AltraTipologia = drCantiere.GetString(altraTipologiaIndex);
                            if (!drCantiere.IsDBNull(altraCategoriaIndex))
                                cantiere.AltraCategoria = drCantiere.GetString(altraCategoriaIndex);

                            using (
                                DbCommand dbCommandCommittenti =
                                    databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariPersoneSelectByNumeroNotifica"))
                            {
                                databaseCemi.AddInParameter(dbCommandCommittenti, "@numeroNotifica", DbType.String,
                                                            cantiere.ProtocolloRegione);
                                databaseCemi.AddInParameter(dbCommandCommittenti, "@ruolo", DbType.String,
                                                            "COMMITTENTE");

                                dbCommandCommittenti.CommandTimeout = 900;

                                using (IDataReader drCommittenti = databaseCemi.ExecuteReader(dbCommandCommittenti))
                                {
                                    Int32 cognomeIndex = drCommittenti.GetOrdinal("cognome");
                                    Int32 nomeIndex = drCommittenti.GetOrdinal("nome");
                                    Int32 codiceFiscaleIndex = drCommittenti.GetOrdinal("codiceFiscale");

                                    while (drCommittenti.Read())
                                    {
                                        CommittenteCPT2 comm = new CommittenteCPT2();
                                        cantiere.Committenti.Add(comm);

                                        comm.Cognome = drCommittenti.GetString(cognomeIndex);
                                        comm.Nome = drCommittenti.GetString(nomeIndex);
                                        comm.CodiceFiscale = drCommittenti.GetString(codiceFiscaleIndex);
                                    }
                                }
                            }

                            using (
                                DbCommand dbCommandSubappalto =
                                    databaseCemi.GetStoredProcCommand("dbo.USP_NotifichePreliminariImpreseSelectByNumeroNotifica"))
                            {
                                databaseCemi.AddInParameter(dbCommandSubappalto, "@numeroNotifica", DbType.String,
                                                            cantiere.ProtocolloRegione);

                                dbCommandSubappalto.CommandTimeout = 900;

                                using (IDataReader drSubappalto = databaseCemi.ExecuteReader(dbCommandSubappalto))
                                {
                                    Int32 impresaRagioneSocialeIndex = drSubappalto.GetOrdinal("ragioneSociale");
                                    Int32 impresaCodiceFiscaleIndex = drSubappalto.GetOrdinal("codiceFiscale");
                                    Int32 tipoIncaricoIndex = drSubappalto.GetOrdinal("descrizioneTipoIncaricoAppalto");

                                    while (drSubappalto.Read())
                                    {
                                        SubappaltoCPT2 subappalto = new SubappaltoCPT2();
                                        cantiere.Subappalti.Add(subappalto);

                                        subappalto.CodiceCantiere = cantiere.CodiceCantiere;

                                        if (!drSubappalto.IsDBNull(impresaRagioneSocialeIndex))
                                            subappalto.ImpresaSelezionataRagioneSociale =
                                                drSubappalto.GetString(impresaRagioneSocialeIndex);
                                        if (!drSubappalto.IsDBNull(impresaCodiceFiscaleIndex))
                                            subappalto.ImpresaSelezionataCodiceFiscale =
                                                drSubappalto.GetString(impresaCodiceFiscaleIndex);
                                        if (!drSubappalto.IsDBNull(tipoIncaricoIndex))
                                            subappalto.TipoIncarico =
                                                drSubappalto.GetString(tipoIncaricoIndex);
                                    }
                                }
                            }

                            ret.Add(cantiere);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;

                //CantiereCPT2 cant = new CantiereCPT2();
                //cant.CantiereIndirizzo = ex.Message;
                //ret.Add(cant);
            }

            return ret;
        }
    }
}