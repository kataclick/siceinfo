using System;

namespace TBridge.Cemi.WebServices.CPT
{
    [Serializable]
    public class SubappaltoCPT2
    {
        public Int32 CodiceCantiere { get; set; }

        public String TipoIncarico { get; set; }

        public String ImpresaSelezionataRagioneSociale { get; set; }

        public String ImpresaSelezionataCodiceFiscale { get; set; }
    }
}