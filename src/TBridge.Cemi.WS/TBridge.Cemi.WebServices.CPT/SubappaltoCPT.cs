using System;

namespace TBridge.Cemi.WebServices.CPT
{
    [Serializable]
    public class SubappaltoCPT
    {
        private int codiceCantiere;
        private int impresaAppaltataDaCodice;
        private string impresaAppaltataDaPartitaIVA;
        private string impresaAppaltataDaCodiceFiscale;
        private string impresaAppaltataDaRagioneSociale;
        private int impresaSelezionataCodice;
        private string impresaSelezionataPartitaIVA;
        private string impresaSelezionataCodiceFiscale;
        private string impresaSelezionataRagioneSociale;

        public int CodiceCantiere
        {
            get
            {
                return codiceCantiere;
            }
            set
            {
                codiceCantiere = value;
            }
        }

        public int ImpresaSelezionataCodice
        {
            get
            {
                return impresaSelezionataCodice;
            }
            set
            {
                impresaSelezionataCodice = value;
            }
        }

        public string ImpresaSelezionataRagioneSociale
        {
            get
            {
                return impresaSelezionataRagioneSociale;
            }
            set
            {
                impresaSelezionataRagioneSociale = value;
            }
        }

        public string ImpresaSelezionataPartitaIVA
        {
            get
            {
                return impresaSelezionataPartitaIVA;
            }
            set
            {
                impresaSelezionataPartitaIVA = value;
            }
        }

        public string ImpresaSelezionataCodiceFiscale
        {
            get
            {
                return impresaSelezionataCodiceFiscale;
            }
            set
            {
                impresaSelezionataCodiceFiscale = value;
            }
        }

        public int ImpresaAppaltataDaCodice
        {
            get
            {
                return impresaAppaltataDaCodice;
            }
            set
            {
                impresaAppaltataDaCodice = value;
            }
        }

        public string ImpresaAppaltataDaRagioneSociale
        {
            get
            {
                return impresaAppaltataDaRagioneSociale;
            }
            set
            {
                impresaAppaltataDaRagioneSociale = value;
            }
        }

        public string ImpresaAppaltataDaPartitaIVA
        {
            get
            {
                return impresaAppaltataDaPartitaIVA;
            }
            set
            {
                impresaAppaltataDaPartitaIVA = value;
            }
        }

        public string ImpresaAppaltataDaCodiceFiscale
        {
            get
            {
                return impresaAppaltataDaCodiceFiscale;
            }
            set
            {
                impresaAppaltataDaCodiceFiscale = value;
            }
        }
    }
}