using System;
using System.Collections.Generic;

namespace TBridge.Cemi.WebServices.CPT
{
    [Serializable]
    public class CantiereCPT2
    {
        public Decimal AmmontareLavori { get; set; }

        public String CantiereIndirizzo { get; set; }

        public String CantiereComune { get; set; }

        public String CantiereProvincia { get; set; }

        public Decimal CantiereLatitudine { get; set; }

        public Decimal CantiereLongitudine { get; set; }

        public Int32 CodiceCantiere { get; set; }

        public DateTime DataInizioLavori { get; set; }

        public Int32 DurataLavori { get; set; }

        public String DescrizioneDurataLavori { get; set; }

        public String TipoOpera { get; set; }

        public String TipoCategoria { get; set; }

        public String TipoTipologia { get; set; }

        public String AltraCategoria { get; set; }

        public String AltraTipologia { get; set; }

        public String ProtocolloRegione { get; set; }

        public List<CommittenteCPT2> Committenti { get; set; }

        public List<SubappaltoCPT2> Subappalti { get; set; }

        public CantiereCPT2()
        {
            this.Subappalti = new List<SubappaltoCPT2>();
            this.Committenti = new List<CommittenteCPT2>();
        }
    }
}