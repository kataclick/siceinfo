using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.WebService
{
    /// <summary>
    ///   Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/assimpredil/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class AssimpredilService : System.Web.Services.WebService
    {
        private readonly Database databaseCemi = DatabaseFactory.CreateDatabase("CEMI");

        [WebMethod]
        public DataSet GetImprese()
        {
            DataSet ret = null;

            try
            {
                using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_AssimpredilImpreseSelect"))
                {
                    dbCommand.CommandTimeout = 900;
                    ret = databaseCemi.ExecuteDataSet(dbCommand);
                }
            }
            catch (Exception ex)
            {
                //
            }

            return ret;
        }
    }
}