using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace TBridge.Cemi.WebServices.CGIA
{
    /// <summary>
    ///   Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/CGIA/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class CGIAService : WebService
    {
        private readonly Database databaseCemi = DatabaseFactory.CreateDatabase("CEMI");

        [WebMethod]
        public List<Impresa> GetImprese()
        {
            List<Impresa> ret = new List<Impresa>();

            try
            {
                using (DbCommand dbCommand = databaseCemi.GetStoredProcCommand("dbo.USP_CGIAImpreseSelect"))
                {
                    //dbCommand.CommandTimeout = 900;
                    //ret = databaseCemi.ExecuteDataSet(dbCommand);
                    using (IDataReader dr = databaseCemi.ExecuteReader(dbCommand))
                    {
                        int attivitaIstatIndex = dr.GetOrdinal("attivitaIstat");
                        int codiceDittaIndex = dr.GetOrdinal("codiceDitta");
                        int codiceFiscaleIndex = dr.GetOrdinal("codiceFiscale");
                        int dataCessazioneCEIndex = dr.GetOrdinal("dataCessazioneCE");
                        int dataIscrizioneCEIndex = dr.GetOrdinal("dataIscrizioneCE");
                        int dataRipresaCEIndex = dr.GetOrdinal("dataRipresaCE");
                        int dataSospensioneCEIndex = dr.GetOrdinal("dataSospensioneCE");
                        int emailSedeAmministrativaIndex = dr.GetOrdinal("emailSedeAmministrativa");
                        int emailSedeLegaleIndex = dr.GetOrdinal("emailSedeLegale");
                        int faxSedeAmministrativaIndex = dr.GetOrdinal("faxSedeAmministrativa");
                        int faxSedeLegaleIndex = dr.GetOrdinal("faxSedeLegale");
                        int indirizzoSedeAmministrativaIndex = dr.GetOrdinal("indirizzoSedeAmministrativa");
                        int indirizzoSedeLegaleIndex = dr.GetOrdinal("indirizzoSedeLegale");
                        int naturaGiuridicaIndex = dr.GetOrdinal("naturaGiuridica");
                        int operaiIndex = dr.GetOrdinal("operai");
                        int partitaIvaIndex = dr.GetOrdinal("partitaIva");
                        int ragioneSocialeIndex = dr.GetOrdinal("ragioneSociale");
                        int telSedeAmministrativaIndex = dr.GetOrdinal("telSedeAmministrativa");
                        int telSedeLegaleIndex = dr.GetOrdinal("telSedeLegale");
                        int tipoImpresaIndex = dr.GetOrdinal("tipoImpresa");
                        int ultimaDenunciaInviataIndex = dr.GetOrdinal("ultimaDenunciaInviata");

                        while (dr.Read())
                        {
                            Impresa impresa = new Impresa();

                            impresa.CodiceDitta = dr.GetInt32(codiceDittaIndex);
                            impresa.RagioneSociale = dr.GetString(ragioneSocialeIndex);

                            if (!dr.IsDBNull(attivitaIstatIndex))
                                impresa.AttivitaIstat = dr.GetString(attivitaIstatIndex);
                            if (!dr.IsDBNull(codiceFiscaleIndex))
                                impresa.CodiceFiscale = dr.GetString(codiceFiscaleIndex);
                            if (!dr.IsDBNull(dataCessazioneCEIndex))
                                impresa.DataCessazioneCE = dr.GetDateTime(dataCessazioneCEIndex);
                            if (!dr.IsDBNull(dataIscrizioneCEIndex))
                                impresa.DataIscrizioneCE = dr.GetDateTime(dataIscrizioneCEIndex);
                            if (!dr.IsDBNull(dataRipresaCEIndex))
                                impresa.DataRipresaCE = dr.GetDateTime(dataRipresaCEIndex);
                            if (!dr.IsDBNull(dataSospensioneCEIndex))
                                impresa.DataSospensioneCE = dr.GetDateTime(dataSospensioneCEIndex);
                            if (!dr.IsDBNull(emailSedeAmministrativaIndex))
                                impresa.EmailSedeAmministrativa = dr.GetString(emailSedeAmministrativaIndex);
                            if (!dr.IsDBNull(emailSedeLegaleIndex))
                                impresa.EmailSedeLegale = dr.GetString(emailSedeLegaleIndex);
                            if (!dr.IsDBNull(faxSedeAmministrativaIndex))
                                impresa.FaxSedeAmministrativa = dr.GetString(faxSedeAmministrativaIndex);
                            if (!dr.IsDBNull(faxSedeLegaleIndex))
                                impresa.FaxSedeLegale = dr.GetString(faxSedeLegaleIndex);
                            if (!dr.IsDBNull(indirizzoSedeAmministrativaIndex))
                                impresa.IndirizzoSedeAmministrativa = dr.GetString(indirizzoSedeAmministrativaIndex);
                            if (!dr.IsDBNull(indirizzoSedeLegaleIndex))
                                impresa.IndirizzoSedeLegale = dr.GetString(indirizzoSedeLegaleIndex);
                            if (!dr.IsDBNull(naturaGiuridicaIndex))
                                impresa.NaturaGiuridica = dr.GetString(naturaGiuridicaIndex);
                            if (!dr.IsDBNull(operaiIndex))
                                impresa.Operai = dr.GetInt32(operaiIndex);
                            if (!dr.IsDBNull(partitaIvaIndex))
                                impresa.PartitaIva = dr.GetString(partitaIvaIndex);
                            if (!dr.IsDBNull(telSedeAmministrativaIndex))
                                impresa.TelSedeAmministrativa = dr.GetString(telSedeAmministrativaIndex);
                            if (!dr.IsDBNull(telSedeLegaleIndex))
                                impresa.TelSedeLegale = dr.GetString(telSedeLegaleIndex);
                            if (!dr.IsDBNull(tipoImpresaIndex))
                                impresa.TipoImpresa = dr.GetString(tipoImpresaIndex);
                            if (!dr.IsDBNull(ultimaDenunciaInviataIndex))
                                impresa.UltimaDenunciaInviata = dr.GetDateTime(ultimaDenunciaInviataIndex);

                            ret.Add(impresa);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }

            return ret;
        }
    }
}