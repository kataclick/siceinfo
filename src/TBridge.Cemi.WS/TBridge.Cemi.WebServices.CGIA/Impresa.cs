using System;

namespace TBridge.Cemi.WebServices.CGIA
{
    [Serializable]
    public class Impresa
    {
        private string attivitaIstat;
        private int codiceDitta;
        private string codiceFiscale;
        private DateTime dataCessazioneCE;
        private DateTime dataIscrizioneCE;
        private DateTime dataRipresaCE;
        private DateTime dataSospensioneCE;
        private string emailSedeAmministrativa;
        private string emailSedeLegale;
        private string faxSedeAmministrativa;
        private string faxSedeLegale;
        private string indirizzoSedeAmministrativa;
        private string indirizzoSedeLegale;
        private string naturaGiuridica;
        private int operai;
        private string partitaIva;
        private string ragioneSociale;
        private string telSedeAmministrativa;
        private string telSedeLegale;
        private string tipoImpresa;
        private DateTime ultimaDenunciaInviata;

        public int CodiceDitta
        {
            get { return codiceDitta; }
            set { codiceDitta = value; }
        }

        public string RagioneSociale
        {
            get { return ragioneSociale; }
            set { ragioneSociale = value; }
        }

        public string CodiceFiscale
        {
            get { return codiceFiscale; }
            set { codiceFiscale = value; }
        }

        public string PartitaIva
        {
            get { return partitaIva; }
            set { partitaIva = value; }
        }

        public string TipoImpresa
        {
            get { return tipoImpresa; }
            set { tipoImpresa = value; }
        }

        public string AttivitaIstat
        {
            get { return attivitaIstat; }
            set { attivitaIstat = value; }
        }

        public string NaturaGiuridica
        {
            get { return naturaGiuridica; }
            set { naturaGiuridica = value; }
        }

        public string IndirizzoSedeLegale
        {
            get { return indirizzoSedeLegale; }
            set { indirizzoSedeLegale = value; }
        }

        public string TelSedeLegale
        {
            get { return telSedeLegale; }
            set { telSedeLegale = value; }
        }

        public string FaxSedeLegale
        {
            get { return faxSedeLegale; }
            set { faxSedeLegale = value; }
        }

        public string EmailSedeLegale
        {
            get { return emailSedeLegale; }
            set { emailSedeLegale = value; }
        }

        public string IndirizzoSedeAmministrativa
        {
            get { return indirizzoSedeAmministrativa; }
            set { indirizzoSedeAmministrativa = value; }
        }

        public string TelSedeAmministrativa
        {
            get { return telSedeAmministrativa; }
            set { telSedeAmministrativa = value; }
        }

        public string FaxSedeAmministrativa
        {
            get { return faxSedeAmministrativa; }
            set { faxSedeAmministrativa = value; }
        }

        public string EmailSedeAmministrativa
        {
            get { return emailSedeAmministrativa; }
            set { emailSedeAmministrativa = value; }
        }

        public DateTime DataIscrizioneCE
        {
            get { return dataIscrizioneCE; }
            set { dataIscrizioneCE = value; }
        }

        public DateTime DataSospensioneCE
        {
            get { return dataSospensioneCE; }
            set { dataSospensioneCE = value; }
        }

        public DateTime DataRipresaCE
        {
            get { return dataRipresaCE; }
            set { dataRipresaCE = value; }
        }

        public DateTime DataCessazioneCE
        {
            get { return dataCessazioneCE; }
            set { dataCessazioneCE = value; }
        }

        public DateTime UltimaDenunciaInviata
        {
            get { return ultimaDenunciaInviata; }
            set { ultimaDenunciaInviata = value; }
        }

        public int Operai
        {
            get { return operai; }
            set { operai = value; }
        }
    }
}