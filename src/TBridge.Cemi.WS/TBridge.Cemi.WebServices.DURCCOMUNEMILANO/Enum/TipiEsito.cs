﻿using System;

namespace TBridge.Cemi.WebServices.DURCCOMUNEMILANO.Enum
{
    [Serializable]
    public enum TipiEsito
    {
        Positivo = 0,
        ProtocolloAssente,
        ProtocolloNonCorrispondente,
        TipologiaErrata,
        NonRegolare
    }
}