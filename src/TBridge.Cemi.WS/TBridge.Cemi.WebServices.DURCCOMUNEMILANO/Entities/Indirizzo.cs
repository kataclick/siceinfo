﻿using System;

namespace TBridge.Cemi.WebServices.DURCCOMUNEMILANO.Entities
{
    [Serializable]
    public class Indirizzo
    {
        public String Via { get; set; }

        public String Civico { get; set; }

        public String Cap { get; set; }
    }
}