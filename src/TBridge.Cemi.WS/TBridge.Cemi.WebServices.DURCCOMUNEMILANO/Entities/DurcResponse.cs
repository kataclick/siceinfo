﻿using System;
using TBridge.Cemi.WebServices.DURCCOMUNEMILANO.Enum;

namespace TBridge.Cemi.WebServices.DURCCOMUNEMILANO.Entities
{
    [Serializable]
    public class DurcResponse
    {
        public TipiEsito Esito { get; set; }

        public DateTime? DataEmissione { get; set; }
    }
}