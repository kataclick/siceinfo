﻿using System;

namespace TBridge.Cemi.WebServices.DURCCOMUNEMILANO.Entities
{
    [Serializable]
    public class DurcRequest
    {
        public String NumeroProtocollo { get; set; }

        public String CodiceFiscaleEsecutore { get; set; }

        public String PartitaIvaEsecutore { get; set; }

        public String RagioneSocialeEsecutore { get; set; }

        public String NumeroAnnoProtocolloComunale { get; set; }

        public String NumeroAnnoPraticaEdilizia { get; set; }

        public Indirizzo Indirizzo { get; set; }

        public DateTime? DataInizioLavori { get; set; }
    }
}