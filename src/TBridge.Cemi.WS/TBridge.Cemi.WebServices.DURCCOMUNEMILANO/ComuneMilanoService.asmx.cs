﻿using System.ComponentModel;
using System.Web.Services;
using TBridge.Cemi.WebServices.DURCCOMUNEMILANO.Entities;

namespace TBridge.Cemi.WebServices.DURCCOMUNEMILANO
{
    /// <summary>
    ///   Web Service per l'interazione con il comune di Milano
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/ComuneMilano")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
        // [System.Web.Script.Services.ScriptService]
    public class ComuneMilanoService : WebService
    {
        [WebMethod]
        public DurcResponse VerificaDurc(DurcRequest durc)
        {
            return new DurcResponse();
        }
    }
}