﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TBridge.Cemi.WebServices.Pit
{
    // NOTE: If you change the class name "AuthenticationService" here, you must also update the reference to "AuthenticationService" in Web.config.
    public class AuthenticationService : IAuthenticationService
    {
        #region IAuthenticationService Members

        public AuthenticationResponse VerifyUser(string username, string password)
        {
            AuthenticationResponse response = new AuthenticationResponse();

            string connectionString = ConfigurationManager.ConnectionStrings["Sice"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.USP_WsPitVerificaCredenziali", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@username", username);
                    command.Parameters.AddWithValue("@password", password);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                response.Autenticato = (bool) reader["autenticato"];
                                response.TipoUtente = (int) reader["tipoUtente"];
                                response.CodiceFiscale = reader["codiceFiscale"] as string;
                                response.PartitaIva = reader["partitaIva"] as string;
                            }
                        }
                    }
                }
            }

            return response;
        }

        #endregion
    }
}