using System;
using System.ServiceModel;

namespace TBridge.Cemi.WebServices.Pit
{
    // NOTE: If you change the interface name "IAuthenticationService" here, you must also update the reference to "IAuthenticationService" in Web.config.
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        AuthenticationResponse VerifyUser(String username, String password);
    }
}