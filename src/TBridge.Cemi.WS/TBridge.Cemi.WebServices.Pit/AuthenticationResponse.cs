using System;
using System.Runtime.Serialization;

namespace TBridge.Cemi.WebServices.Pit
{
    [DataContract]
    public class AuthenticationResponse
    {
        public AuthenticationResponse()
        {
            Autenticato = false;
            TipoUtente = 0;
        }

        [DataMember]
        public Boolean Autenticato { get; set; }

        [DataMember]
        public Int32 TipoUtente { get; set; }

        [DataMember]
        public String CodiceFiscale { get; set; }

        [DataMember]
        public String PartitaIva { get; set; }
    }
}