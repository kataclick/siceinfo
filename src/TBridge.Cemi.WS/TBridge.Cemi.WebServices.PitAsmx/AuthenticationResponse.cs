﻿using System;

namespace TBridge.Cemi.WebServices.PitAsmx
{
    [Serializable]
    public class AuthenticationResponse
    {
        public AuthenticationResponse()
        {
            Autenticato = false;
            TipoUtente = 0;
        }

        public Boolean Autenticato { get; set; }

        public Int32 TipoUtente { get; set; }

        public String CodiceFiscale { get; set; }

        public String PartitaIva { get; set; }
    }
}