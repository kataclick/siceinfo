﻿using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;

namespace TBridge.Cemi.WebServices.PitAsmx
{
    /// <summary>
    ///   Summary description for AuthenticationService
    /// </summary>
    [WebService(Namespace = "http://www.cassaedilemilano.it/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
        // [System.Web.Script.Services.ScriptService]
    public class AuthenticationService : WebService
    {
        [WebMethod]
        public AuthenticationResponse VerifyUser(string username, string password)
        {
            AuthenticationResponse response = new AuthenticationResponse();

            string connectionString = ConfigurationManager.ConnectionStrings["Sice"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("dbo.USP_WsPitVerificaCredenziali", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@username", username);
                    command.Parameters.AddWithValue("@password", password);
                    connection.Open();
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            if (reader.Read())
                            {
                                response.Autenticato = (bool) reader["autenticato"];
                                response.TipoUtente = (int) reader["tipoUtente"];
                                response.CodiceFiscale = reader["codiceFiscale"] as string;
                                response.PartitaIva = reader["partitaIva"] as string;
                            }
                        }
                    }
                }
            }

            return response;
        }
    }
}