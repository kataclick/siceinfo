﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using Cemi.PianiRientro.Scadenzario.ConsoleApp.EmailService;
using System.Threading;
using System.Globalization;

namespace Cemi.PianiRientro.Scadenzario.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                int giorniPreavviso = Convert.ToInt32(ConfigurationManager.AppSettings["GiorniPreavviso"]);
                DateTime dataConfronto = DateTime.Today.AddDays(giorniPreavviso);

                using (SICEEntities context = new SICEEntities())
                {
                    var pianiRientroQuery = from pianoRientro in context.PianiRientro
                                            join impresa in context.Imprese on pianoRientro.IdImpresa equals impresa.Id
                                            join impresaConsulente in context.ImpreseConsulenti on impresa.Id equals
                                                impresaConsulente.IdImpresa
                                            join consulente in context.Consulenti on impresaConsulente.IdConsulente
                                                equals consulente.Id
                                            where
                                                pianoRientro.Valido == "S" && pianoRientro.RataVersato == "N" &&
                                                pianoRientro.TipoFase != "FI" &&
                                                pianoRientro.RataDataScadenza == dataConfronto
                                            select new { pianoRientro, impresa, consulente };

                    foreach (var piano in pianiRientroQuery)
                    {
                        InvioEMail(piano.pianoRientro, piano.impresa, piano.consulente);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }

                Console.ReadLine();
            }
        }

        private static void InvioEMail(PianoRientro pianoRientro, Impresa impresa, Consulente consulente)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");

            //verifico vi sia almeno un indirizzo a cui spedire la e-mail
            if (!String.IsNullOrEmpty(impresa.EMailSedeLegale) ||
                !String.IsNullOrEmpty(impresa.EMailSedeAmministrazione) ||
                !String.IsNullOrEmpty(impresa.EMailCorrispondenza) ||
                (consulente != null && !String.IsNullOrEmpty(consulente.EMail)))
            {
                string emailUserName = ConfigurationManager.AppSettings["EmailUserName"];
                string emailPassword = ConfigurationManager.AppSettings["EmailPassword"];

                bool usaMailTest = true;
                string usaMailTestString = ConfigurationManager.AppSettings["UsaMailTest"];
                if (usaMailTestString == "false")
                    usaMailTest = false;

                EmailMessageSerializzabile email = new EmailMessageSerializzabile();

                email.Mittente = new EmailAddress
                                     {
                                         Nome = "Cassa Edile di Milano",
                                         Indirizzo = "noreply-contenzioso@cassaedilemilano.it"
                                     };


                email.Oggetto = String.Format("Promemoria scadenza rata piano di rientro impresa {1} - {0}", impresa.Id,
                                              impresa.RagioneSociale);

                StringBuilder sbTestoHtml = new StringBuilder();
                StringBuilder sbTestoPlain = new StringBuilder();

                #region Testo 1

                if (pianoRientro.TipoFase == "AS" || pianoRientro.TipoFase == "APA" || pianoRientro.TipoFase == "AIS" ||
                    pianoRientro.TipoFase == "ARC")
                {
                    #region Testo HTML

                    sbTestoHtml = new StringBuilder();
                    sbTestoHtml.Append("<font face=\"Arial\" size=2>");

                    if (usaMailTest)
                    {
                        sbTestoHtml.AppendFormat("Identificativo: {0}<br/>", pianoRientro.Identificativo);
                        sbTestoHtml.AppendFormat("Data: {0}<br/>", pianoRientro.RataDataScadenza);
                        sbTestoHtml.AppendFormat("Importo rata: {0}<br/>", pianoRientro.RataImporto);

                        sbTestoHtml.Append("Destinatari:<br/>");
                        if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                            sbTestoHtml.AppendFormat("email sede legale: {0}<br/>", impresa.EMailSedeLegale);
                        if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                            sbTestoHtml.AppendFormat("email sede amministrativa: {0}<br/>",
                                                     impresa.EMailSedeAmministrazione);
                        if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                            sbTestoHtml.AppendFormat("email corrispondenza: {0}<br/>", impresa.EMailCorrispondenza);
                        if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                            sbTestoHtml.AppendFormat("email consulente: {0}<br/>", consulente.EMail);
                    }

                    sbTestoHtml.Append("Buongiorno,<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.AppendFormat(
                        "si ricorda che in data {0} scadrà la rata n. {1} di importo pari a € {2} del piano di rientro concordato.<br/>",
                        pianoRientro.RataDataScadenza.Value.ToShortDateString(), pianoRientro.RataNumeroRata,
                        pianoRientro.RataImporto);
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append(
                        "Qualora abbiate già provveduto al pagamento della suddetta rata, vi chiediamo di non considerare valido il promemoria.<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append(
                        "Si invita a non rispondere al presente messaggio; per informazioni contattare il seguente recapito telefonico: tel. 02.584961, tasto 1 \"Servizi alle imprese\", tasto 7 \"Regolarizzazione pagamenti e contenzioso imprese\".<br />");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append("Cogliamo l'occasione per porgere cordiali saluti.<br/>");
                    sbTestoHtml.Append("<br />");
                    sbTestoHtml.Append("<br />");
                    sbTestoHtml.Append("Ufficio Servizi alle Imprese<br/>");
                    sbTestoHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
                    sbTestoHtml.Append("</font>");

                    #endregion

                    #region Testo Plain

                    sbTestoPlain = new StringBuilder();

                    if (usaMailTest)
                    {
                        sbTestoPlain.AppendFormat("Identificativo: {0}<br/>", pianoRientro.Identificativo);
                        sbTestoPlain.AppendFormat("Data: {0}<br/>", pianoRientro.RataDataScadenza);
                        sbTestoPlain.AppendFormat("Importo rata: {0}<br/>", pianoRientro.RataImporto);

                        sbTestoPlain.Append("Destinatari:<br/>");
                        if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                            sbTestoPlain.AppendFormat("email sede legale: {0}<br/>", impresa.EMailSedeLegale);
                        if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                            sbTestoPlain.AppendFormat("email sede amministrativa: {0}<br/>",
                                                      impresa.EMailSedeAmministrazione);
                        if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                            sbTestoPlain.AppendFormat("email corrispondenza: {0}<br/>", impresa.EMailCorrispondenza);
                        if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                            sbTestoPlain.AppendFormat("email consulente: {0}<br/>", consulente.EMail);
                    }

                    sbTestoPlain.Append("Buongiorno,<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.AppendFormat(
                        "si ricorda che in data {0} scadrà la rata n. {1} di importo pari a € {2} del piano di rientro concordato.<br/>",
                        pianoRientro.RataDataScadenza.Value.ToShortDateString(), pianoRientro.RataNumeroRata,
                        pianoRientro.RataImporto);
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append(
                        "Qualora abbiate già provveduto al pagamento della suddetta rata, vi chiediamo di non considerare valido il promemoria.<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append(
                        "Si invita a non rispondere al presente messaggio; per informazioni contattare il seguente recapito telefonico: tel. 02.584961, tasto 1 \"Servizi alle imprese\", tasto 7 \"Regolarizzazione pagamenti e contenzioso imprese\".<br />");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append("Cogliamo l'occasione per porgere cordiali saluti.<br/>");
                    sbTestoPlain.Append("<br />");
                    sbTestoPlain.Append("<br />");
                    sbTestoPlain.Append("Ufficio Servizi alle Imprese<br/>");
                    sbTestoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

                    #endregion
                }

                #endregion

                #region Testo 2

                if (pianoRientro.TipoFase == "APL" || pianoRientro.TipoFase == "PR1")
                {
                    #region Testo HTML

                    sbTestoHtml = new StringBuilder();
                    sbTestoHtml.Append("<font face=\"Arial\" size=2>");

                    if (usaMailTest)
                    {
                        sbTestoHtml.AppendFormat("Identificativo: {0}<br/>", pianoRientro.Identificativo);
                        sbTestoHtml.AppendFormat("Data: {0}<br/>", pianoRientro.RataDataScadenza);
                        sbTestoHtml.AppendFormat("Importo rata: {0}<br/>", pianoRientro.RataImporto);

                        sbTestoHtml.Append("Destinatari:<br/>");
                        if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                            sbTestoHtml.AppendFormat("email sede legale: {0}<br/>", impresa.EMailSedeLegale);
                        if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                            sbTestoHtml.AppendFormat("email sede amministrativa: {0}<br/>",
                                                     impresa.EMailSedeAmministrazione);
                        if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                            sbTestoHtml.AppendFormat("email corrispondenza: {0}<br/>", impresa.EMailCorrispondenza);
                        if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                            sbTestoHtml.AppendFormat("email consulente: {0}<br/>", consulente.EMail);
                    }

                    sbTestoHtml.Append("Buongiorno,<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.AppendFormat(
                        "si ricorda che in data {0} scadrà la rata n. {1} di importo pari a € {2} del piano di rientro accordatoVi.<br/>",
                        pianoRientro.RataDataScadenza.Value.ToShortDateString(), pianoRientro.RataNumeroRata,
                        pianoRientro.RataImporto);
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append(
                        "Qualora abbiate già provveduto al pagamento della suddetta rata, Vi chiediamo di non considerare il promemoria valido.<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append(
                        "Non rispondere al presente messaggio; in caso di necessità o per ulteriori informazioni contattare l’ufficio Servizi alle Imprese al seguente recapito telefonico: 02.584961, tasto 1 \"Servizi alle Imprese\", tasto 7 \"Regolarizzazione pagamenti e contenzioso imprese\".<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append("Cordiali saluti.<br/>");
                    sbTestoHtml.Append("<br />");
                    sbTestoHtml.Append("<br />");
                    sbTestoHtml.Append("Ufficio Servizi alle Imprese<br/>");
                    sbTestoHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
                    sbTestoHtml.Append("</font>");

                    #endregion

                    #region Testo Plain

                    sbTestoPlain = new StringBuilder();

                    if (usaMailTest)
                    {
                        sbTestoPlain.AppendFormat("Identificativo: {0}<br/>", pianoRientro.Identificativo);
                        sbTestoPlain.AppendFormat("Data: {0}<br/>", pianoRientro.RataDataScadenza);
                        sbTestoPlain.AppendFormat("Importo rata: {0}<br/>", pianoRientro.RataImporto);

                        sbTestoPlain.Append("Destinatari:<br/>");
                        if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                            sbTestoPlain.AppendFormat("email sede legale: {0}<br/>", impresa.EMailSedeLegale);
                        if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                            sbTestoPlain.AppendFormat("email sede amministrativa: {0}<br/>",
                                                      impresa.EMailSedeAmministrazione);
                        if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                            sbTestoPlain.AppendFormat("email corrispondenza: {0}<br/>", impresa.EMailCorrispondenza);
                        if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                            sbTestoPlain.AppendFormat("email consulente: {0}<br/>", consulente.EMail);
                    }

                    sbTestoPlain.Append("Buongiorno,<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.AppendFormat(
                        "si ricorda che in data {0} scadrà la rata n. {1} di importo pari a € {2} del piano di rientro accordatoVi.<br/>",
                        pianoRientro.RataDataScadenza.Value.ToShortDateString(), pianoRientro.RataNumeroRata,
                        pianoRientro.RataImporto);
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append(
                        "Qualora abbiate già provveduto al pagamento della suddetta rata, Vi chiediamo di non considerare il promemoria valido.<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append(
                        "Non rispondere al presente messaggio; in caso di necessità o per ulteriori informazioni contattare l’ufficio Servizi alle Imprese al seguente recapito telefonico: 02.584961, tasto 1 \"Servizi alle Imprese\", tasto 7 \"Regolarizzazione pagamenti e contenzioso imprese\".<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append("Cordiali saluti.<br/>");
                    sbTestoPlain.Append("<br />");
                    sbTestoPlain.Append("<br />");
                    sbTestoPlain.Append("Ufficio Servizi alle Imprese<br/>");
                    sbTestoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

                    #endregion
                }

                #endregion

                #region Testo 3

                if (pianoRientro.TipoFase == "AB" || pianoRientro.TipoFase == "APC" || pianoRientro.TipoFase == "ATR")
                {
                    #region Testo HTML

                    sbTestoHtml = new StringBuilder();
                    sbTestoHtml.Append("<font face=\"Arial\" size=2>");

                    if (usaMailTest)
                    {
                        sbTestoHtml.AppendFormat("Identificativo: {0}<br/>", pianoRientro.Identificativo);
                        sbTestoHtml.AppendFormat("Data: {0}<br/>", pianoRientro.RataDataScadenza);
                        sbTestoHtml.AppendFormat("Importo rata: {0}<br/>", pianoRientro.RataImporto);

                        sbTestoHtml.Append("Destinatari:<br/>");
                        if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                            sbTestoHtml.AppendFormat("email sede legale: {0}<br/>", impresa.EMailSedeLegale);
                        if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                            sbTestoHtml.AppendFormat("email sede amministrativa: {0}<br/>",
                                                     impresa.EMailSedeAmministrazione);
                        if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                            sbTestoHtml.AppendFormat("email corrispondenza: {0}<br/>", impresa.EMailCorrispondenza);
                        if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                            sbTestoHtml.AppendFormat("email consulente: {0}<br/>", consulente.EMail);
                    }

                    sbTestoHtml.Append("Buongiorno,<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.AppendFormat(
                        "si ricorda che in data {0} andrà all’incasso l’importo di € {1} relativo alla rata n. {2} del piano di rientro accordatoVi.<br/>",
                        pianoRientro.RataDataScadenza.Value.ToShortDateString(), pianoRientro.RataImporto,
                        pianoRientro.RataNumeroRata);
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append(
                        "Si chiede cortesemente di inviare la relativa contabile di addebito, non appena disponibile, a mezzo fax al n.° 02.58316406.<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append(
                        "Non rispondere al presente messaggio; per informazioni contattare il seguente recapito telefonico: tel. 02.584961, tasto 1 “Servizi alle imprese”, tasto 7 “Regolarizzazione pagamenti e contenzioso imprese”.<br/>");
                    sbTestoHtml.Append("<br/>");
                    sbTestoHtml.Append("Cogliamo l'occasione per porgere cordiali saluti.<br/>");
                    sbTestoHtml.Append("<br />");
                    sbTestoHtml.Append("<br />");
                    sbTestoHtml.Append("Ufficio Servizi alle Imprese<br/>");
                    sbTestoHtml.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");
                    sbTestoHtml.Append("</font>");

                    #endregion

                    #region Testo Plain

                    sbTestoPlain = new StringBuilder();

                    if (usaMailTest)
                    {
                        sbTestoPlain.AppendFormat("Identificativo: {0}<br/>", pianoRientro.Identificativo);
                        sbTestoPlain.AppendFormat("Data: {0}<br/>", pianoRientro.RataDataScadenza);
                        sbTestoPlain.AppendFormat("Importo rata: {0}<br/>", pianoRientro.RataImporto);

                        sbTestoPlain.Append("Destinatari:<br/>");
                        if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                            sbTestoPlain.AppendFormat("email sede legale: {0}<br/>", impresa.EMailSedeLegale);
                        if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                            sbTestoPlain.AppendFormat("email sede amministrativa: {0}<br/>",
                                                      impresa.EMailSedeAmministrazione);
                        if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                            sbTestoPlain.AppendFormat("email corrispondenza: {0}<br/>", impresa.EMailCorrispondenza);
                        if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                            sbTestoPlain.AppendFormat("email consulente: {0}<br/>", consulente.EMail);
                    }

                    sbTestoPlain.Append("Buongiorno,<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.AppendFormat(
                        "si ricorda che in data {0} andrà all’incasso l’importo di € {1} relativo alla rata n. {2} del piano di rientro accordatoVi.<br/>",
                        pianoRientro.RataDataScadenza.Value.ToShortDateString(), pianoRientro.RataImporto,
                        pianoRientro.RataNumeroRata);
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append(
                        "Si chiede cortesemente di inviare la relativa contabile di addebito, non appena disponibile, a mezzo fax al n.° 02.58316406.<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append(
                        "Non rispondere al presente messaggio; per informazioni contattare il seguente recapito telefonico: tel. 02.584961, tasto 1 “Servizi alle imprese”, tasto 7 “Regolarizzazione pagamenti e contenzioso imprese”.<br/>");
                    sbTestoPlain.Append("<br/>");
                    sbTestoPlain.Append("Cogliamo l'occasione per porgere cordiali saluti.<br/>");
                    sbTestoPlain.Append("<br />");
                    sbTestoPlain.Append("<br />");
                    sbTestoPlain.Append("Ufficio Servizi alle Imprese<br/>");
                    sbTestoPlain.Append("Cassa Edile di Milano, Lodi, Monza e Brianza");

                    #endregion
                }

                #endregion

                email.BodyPlain = sbTestoPlain.ToString();
                email.BodyHTML = sbTestoHtml.ToString();
                email.Priorita = MailPriority.Normal;
                email.DataSchedulata = DateTime.Now;

                List<EmailAddress> destinatari = new List<EmailAddress>();

                if (usaMailTest)
                {
                    destinatari.Add(new EmailAddress
                    {
                        Indirizzo = "marco.catalano@cassaedilemilano.it",
                        Nome = impresa.RagioneSociale
                    });
                    destinatari.Add(new EmailAddress
                                        {
                                            Indirizzo = "massimo.sonzini@cassaedilemilano.it",
                                            Nome = impresa.RagioneSociale
                                        });
                }
                else
                {
                    if (!String.IsNullOrEmpty(impresa.EMailSedeLegale))
                    {
                        destinatari.Add(new EmailAddress { Indirizzo = impresa.EMailSedeLegale, Nome = impresa.RagioneSociale });
                    }

                    if (!String.IsNullOrEmpty(impresa.EMailSedeAmministrazione))
                    {
                        destinatari.Add(new EmailAddress
                                            {
                                                Indirizzo = impresa.EMailSedeAmministrazione,
                                                Nome = impresa.RagioneSociale
                                            });
                    }

                    if (!String.IsNullOrEmpty(impresa.EMailCorrispondenza))
                    {
                        destinatari.Add(new EmailAddress { Indirizzo = impresa.EMailCorrispondenza, Nome = impresa.RagioneSociale });
                    }

                    if (consulente != null && !String.IsNullOrEmpty(consulente.EMail))
                    {
                        destinatari.Add(new EmailAddress { Indirizzo = consulente.EMail, Nome = consulente.RagioneSociale });
                    }
                }

                var destinatariUnici = destinatari.GroupBy(d => d.Indirizzo).Select(g => g.First()).ToList();

                email.Destinatari = destinatariUnici.ToArray();

                EmailInfoService client = new EmailInfoService();
                NetworkCredential credentials = new NetworkCredential(emailUserName, emailPassword);
                client.Credentials = credentials;
                client.InviaEmail(email);
            }
        }
    }
}