﻿
using System;
namespace Cemi.SOLDO.ConsoleAppTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---- INIZIO");

            try
            {
                
                SOLDOSiceInfoService.SOLDOSiceInfoClient client = new SOLDOSiceInfoService.SOLDOSiceInfoClient();

                //
                // Test CFP INSERIMENTO
                //
                /*
                SOLDOSiceInfoService.RecuperoCreditiDenuncia denuncia1 = new SOLDOSiceInfoService.RecuperoCreditiDenuncia();
                denuncia1.IdImpresa = 666;
                denuncia1.AnnoCompetenza = 2015;
                denuncia1.MeseCompetenza = 9;
                denuncia1.SequenzaDenuncia = 0;

                denuncia1.ImportoDenuncia = 0;
                denuncia1.ImportoConguaglioMalattia = 0;
                denuncia1.ImportoDovutoNetto = 500;
                denuncia1.ImportoExtraDenuncia = 0;

                Console.WriteLine("-- Inserimento CFP --");
                Console.ReadLine();
                Console.WriteLine(client.SalvaRecuperoCreditiDenuncia(denuncia1));
                Console.ReadLine();
                */


                //
                // Test CFP CANCELLAZIONE
                //
                SOLDOSiceInfoService.RecuperoCreditiDenuncia denuncia3 = new SOLDOSiceInfoService.RecuperoCreditiDenuncia();
                denuncia3.IdImpresa = 666;
                denuncia3.AnnoCompetenza = 2015;
                denuncia3.MeseCompetenza = 9;
                denuncia3.SequenzaDenuncia = 0;

                denuncia3.ImportoDenuncia = 0;
                denuncia3.ImportoConguaglioMalattia = 0;
                denuncia3.ImportoDovutoNetto = 500;
                denuncia3.ImportoExtraDenuncia = 0;

                Console.WriteLine("-- Cancellazione CFP --");
                Console.ReadLine();
                Console.WriteLine(client.CancellaRecuperoCreditiDenuncia(denuncia3));
                Console.ReadLine();

                //
                // Test generici
                //
                /*
                SOLDOSiceInfoService.RecuperoCreditiDenuncia denuncia1 = new SOLDOSiceInfoService.RecuperoCreditiDenuncia();
                denuncia1.IdImpresa = 21;
                denuncia1.AnnoCompetenza = 2015;
                denuncia1.MeseCompetenza = 1;
                denuncia1.SequenzaDenuncia = 1;

                denuncia1.ImportoDenuncia = 100;
                denuncia1.ImportoConguaglioMalattia = 200;
                denuncia1.ImportoDovutoNetto = 300;
                denuncia1.ImportoExtraDenuncia = 400;
                
                Console.WriteLine("-- Inserimento 1 --");
                Console.ReadLine();
                Console.WriteLine(client.SalvaRecuperoCreditiDenuncia(denuncia1));
                Console.ReadLine();

                SOLDOSiceInfoService.RecuperoCreditiDenuncia denuncia2 = new SOLDOSiceInfoService.RecuperoCreditiDenuncia();
                denuncia2.IdImpresa = 45;
                denuncia2.AnnoCompetenza = 2015;
                denuncia2.MeseCompetenza = 3;
                denuncia2.SequenzaDenuncia = 1;

                denuncia2.ImportoDenuncia = Convert.ToDecimal(400.5);
                denuncia2.ImportoConguaglioMalattia = Convert.ToDecimal(300.5);
                denuncia2.ImportoDovutoNetto = Convert.ToDecimal(300.5);
                denuncia2.ImportoExtraDenuncia = Convert.ToDecimal(400.5);

                Console.WriteLine("-- Inserimento 2 --");
                Console.ReadLine();
                Console.WriteLine(client.SalvaRecuperoCreditiDenuncia(denuncia2));
                Console.ReadLine();


                denuncia1.ImportoDovutoNetto = denuncia1.ImportoDovutoNetto + Convert.ToDecimal(200);

                Console.WriteLine("-- Aggiornamento 1 --");
                Console.ReadLine();
                Console.WriteLine(client.SalvaRecuperoCreditiDenuncia(denuncia1));
                Console.ReadLine();


                SOLDOSiceInfoService.RecuperoCreditiDenuncia denuncia3 = new SOLDOSiceInfoService.RecuperoCreditiDenuncia();
                denuncia3.IdImpresa = 21;
                denuncia3.AnnoCompetenza = 2015;
                denuncia3.MeseCompetenza = 1;
                denuncia3.SequenzaDenuncia = 1;

                denuncia3.ImportoDenuncia = 100;
                denuncia3.ImportoConguaglioMalattia = 200;
                denuncia3.ImportoDovutoNetto = 300;
                denuncia3.ImportoExtraDenuncia = 400;

                Console.WriteLine("-- Cancellazione 1 --");
                Console.ReadLine();
                Console.WriteLine(client.CancellaRecuperoCreditiDenuncia(denuncia3));
                Console.ReadLine();
                */
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);

                if (exc.InnerException != null)
                {
                    Console.WriteLine("-- INNER --");
                    Console.WriteLine(exc.InnerException.Message);
                }
            }

            Console.WriteLine("---- FINE");
            Console.ReadLine();
        }
    }
}
