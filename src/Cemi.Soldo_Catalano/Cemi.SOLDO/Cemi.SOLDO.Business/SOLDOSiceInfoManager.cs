﻿using System;
using System.Linq;
using Cemi.SOLDO.Type.Domain;
using Cemi.SOLDO.Data;

namespace Cemi.SOLDO.Business
{
    public class SOLDOSiceInfoManager
    {
        public Boolean SaveRecuperoCreditiDenuncia(RecuperoCreditiDenuncia recuperoCreditiDenuncia)
        {
            Boolean res = false;

            using (SICEEntities context = new SICEEntities())
            {
                RecuperoCreditiDenuncia findRecuperoCreditiDenuncia = (from rcd in context.RecuperoCreditiDenunce
                                                                       where rcd.IdImpresa == recuperoCreditiDenuncia.IdImpresa
                                                                           && rcd.AnnoCompetenza == recuperoCreditiDenuncia.AnnoCompetenza
                                                                           && rcd.MeseCompetenza == recuperoCreditiDenuncia.MeseCompetenza
                                                                           && rcd.SequenzaDenuncia == recuperoCreditiDenuncia.SequenzaDenuncia
                                                                       select rcd).FirstOrDefault();

                if (findRecuperoCreditiDenuncia != null)
                {
                    findRecuperoCreditiDenuncia.ImportoDenuncia = recuperoCreditiDenuncia.ImportoDenuncia;
                    findRecuperoCreditiDenuncia.ImportoConguaglioMalattia = recuperoCreditiDenuncia.ImportoConguaglioMalattia;
                    findRecuperoCreditiDenuncia.ImportoExtraDenuncia = recuperoCreditiDenuncia.ImportoExtraDenuncia;
                    findRecuperoCreditiDenuncia.ImportoDovutoNetto = recuperoCreditiDenuncia.ImportoDovutoNetto;
                    findRecuperoCreditiDenuncia.DataModificaRecord = DateTime.Now;
                }
                else
                {
                    recuperoCreditiDenuncia.DataInserimentoRecord = DateTime.Now;
                    context.RecuperoCreditiDenunce.AddObject(recuperoCreditiDenuncia);
                }

                if (context.SaveChanges() == 1)
                {
                    res = true;
                }
            }

            return res;
        }

        public Boolean DeleteRecuperoCreditiDenuncia(RecuperoCreditiDenuncia recuperoCreditiDenuncia)
        {
            Boolean res = false;

            using (SICEEntities context = new SICEEntities())
            {
                RecuperoCreditiDenuncia findRecuperoCreditiDenuncia = (from rcd in context.RecuperoCreditiDenunce
                                                                       where rcd.IdImpresa == recuperoCreditiDenuncia.IdImpresa
                                                                           && rcd.AnnoCompetenza == recuperoCreditiDenuncia.AnnoCompetenza
                                                                           && rcd.MeseCompetenza == recuperoCreditiDenuncia.MeseCompetenza
                                                                           && rcd.SequenzaDenuncia == recuperoCreditiDenuncia.SequenzaDenuncia
                                                                       select rcd).FirstOrDefault();

                if (findRecuperoCreditiDenuncia != null)
                {
                    context.RecuperoCreditiDenunce.DeleteObject(findRecuperoCreditiDenuncia);
                    if (context.SaveChanges() == 1)
                    {
                        res = true;
                    }
                }
                
            }

            return res;
        }
    }
}
