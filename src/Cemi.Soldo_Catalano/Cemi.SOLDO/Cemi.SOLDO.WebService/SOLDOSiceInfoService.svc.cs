﻿using System;
using Cemi.SOLDO.Business;
using Cemi.SOLDOSiceInfo;

namespace Cemi.SOLDO
{
    public class SOLDOSiceInfoService : ISOLDOSiceInfo
    {
        public Boolean SalvaRecuperoCreditiDenuncia(RecuperoCreditiDenuncia recuperoCreditiDenuncia)
        {
            SOLDOSiceInfoManager manager = new SOLDOSiceInfoManager();
            
            // Conversione dell'oggetto I/O in oggetto di dominio
            Cemi.SOLDO.Type.Domain.RecuperoCreditiDenuncia rcd = new Type.Domain.RecuperoCreditiDenuncia();
            rcd.IdImpresa = recuperoCreditiDenuncia.IdImpresa;
            rcd.AnnoCompetenza = recuperoCreditiDenuncia.AnnoCompetenza;
            rcd.MeseCompetenza = recuperoCreditiDenuncia.MeseCompetenza;

            // Gestione sequenza denuncia uguale a 0. Alla creazione del report viene effettuata una sottrazione di 1 al numero sequenza. COsì evitiamo il valore "-1"
            short sequenzaSOLDO = 0;
            if (recuperoCreditiDenuncia.SequenzaDenuncia != null)
            {
                sequenzaSOLDO = 1;
                sequenzaSOLDO += recuperoCreditiDenuncia.SequenzaDenuncia;
            }
            rcd.SequenzaDenuncia = sequenzaSOLDO;
            // fine gestione numero sequenza

            rcd.ImportoDenuncia = recuperoCreditiDenuncia.ImportoDenuncia;
            rcd.ImportoConguaglioMalattia = recuperoCreditiDenuncia.ImportoConguaglioMalattia;
            rcd.ImportoExtraDenuncia = recuperoCreditiDenuncia.ImportoExtraDenuncia;
            rcd.ImportoDovutoNetto = recuperoCreditiDenuncia.ImportoDovutoNetto;

            return manager.SaveRecuperoCreditiDenuncia(rcd);
        }

        public Boolean CancellaRecuperoCreditiDenuncia(RecuperoCreditiDenuncia recuperoCreditiDenuncia)
        {
            SOLDOSiceInfoManager manager = new SOLDOSiceInfoManager();

            // Conversione dell'oggetto I/O in oggetto di dominio
            Cemi.SOLDO.Type.Domain.RecuperoCreditiDenuncia rcd = new Type.Domain.RecuperoCreditiDenuncia();
            rcd.IdImpresa = recuperoCreditiDenuncia.IdImpresa;
            rcd.AnnoCompetenza = recuperoCreditiDenuncia.AnnoCompetenza;
            rcd.MeseCompetenza = recuperoCreditiDenuncia.MeseCompetenza;
            
            // Gestione sequenza denuncia uguale a 0. Nel DB la sequenza è salvata con valore minimo 1. Da SOLDO il valore minimo arriva come 0
            short sequenzaSOLDO = 0;
            if (recuperoCreditiDenuncia.SequenzaDenuncia != null)
            {
                sequenzaSOLDO = 1;
                sequenzaSOLDO += recuperoCreditiDenuncia.SequenzaDenuncia;
            }
            rcd.SequenzaDenuncia = sequenzaSOLDO;
            // fine gestione numero sequenza

            rcd.ImportoDenuncia = recuperoCreditiDenuncia.ImportoDenuncia;
            rcd.ImportoConguaglioMalattia = recuperoCreditiDenuncia.ImportoConguaglioMalattia;
            rcd.ImportoExtraDenuncia = recuperoCreditiDenuncia.ImportoExtraDenuncia;
            rcd.ImportoDovutoNetto = recuperoCreditiDenuncia.ImportoDovutoNetto;

            return manager.DeleteRecuperoCreditiDenuncia(rcd);
        }

    }
}
