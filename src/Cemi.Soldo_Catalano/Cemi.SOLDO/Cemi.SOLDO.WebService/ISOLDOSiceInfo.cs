﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Cemi.SOLDOSiceInfo
{
    [ServiceContract(Namespace = "ww3.cassaedilemilano.it/SOLDOSiceInfo")]
    public interface ISOLDOSiceInfo
    {
        [OperationContract]
        Boolean SalvaRecuperoCreditiDenuncia(RecuperoCreditiDenuncia recuperoCreditiDenuncia);

        [OperationContract]
        Boolean CancellaRecuperoCreditiDenuncia(RecuperoCreditiDenuncia recuperoCreditiDenuncia);
    }


    [DataContract]
    public class RecuperoCreditiDenuncia
    {
        [DataMember]
        public Int32 IdImpresa { get; set; }

        [DataMember]
        public Int32 AnnoCompetenza { get; set; }

        [DataMember]
        public Int32 MeseCompetenza { get; set; }

        [DataMember]
        public Int16 SequenzaDenuncia { get; set; }

        [DataMember]
        public Decimal? ImportoDenuncia { get; set; }

        [DataMember]
        public Decimal? ImportoConguaglioMalattia { get; set; }

        [DataMember]
        public Decimal? ImportoExtraDenuncia { get; set; }

        [DataMember]
        public Decimal ImportoDovutoNetto { get; set; }
    }
}
