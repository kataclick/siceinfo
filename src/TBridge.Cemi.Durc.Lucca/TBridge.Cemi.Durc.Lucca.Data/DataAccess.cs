﻿//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DurcClass = TBridge.Cemi.Durc.Lucca.Type.Durc;

namespace TBridge.Cemi.Durc.Lucca.Data
{
    public class DataAccess
    {
        private readonly string _connectionStringCemi = ConfigurationManager.ConnectionStrings["CEMI"].ConnectionString;
        private readonly string _connectionStringLucca = ConfigurationManager.ConnectionStrings["Lucca"].ConnectionString;

        public int CountDurc()
        {
            int ret;

            const string query = "select count(*) as num from dmView where data_durc is not null";

            using (SqlConnection connection = new SqlConnection(_connectionStringLucca))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    ret = Int32.Parse(command.ExecuteScalar().ToString());
                }
            }

            #region MySql

            //using (MySqlConnection connection = new MySqlConnection(connectionStringLucca))
            //{
            //    using (MySqlCommand command = new MySqlCommand(query, connection))
            //    {
            //        connection.Open();

            //        ret = Int32.Parse(command.ExecuteScalar().ToString());
            //    }
            //}

            #endregion

            return ret;
        }

        public List<DurcClass> ReadDurc(int durcToRead)
        {
            List<DurcClass> durcList = new List<DurcClass>();

            using (SqlConnection connection = new SqlConnection(_connectionStringLucca))
            {
                const string query =
                    "select impresa_esecutrice as ragionesociale, protocollo, cip, data_pratica as datarichiesta, data_durc as dataemissione, codice_fiscale as codiceFiscale, tipo_pratica as tipoPratica from dmView where data_durc is not null ";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int ragioneSociale = reader.GetOrdinal("ragioneSociale");
                        int protocollo = reader.GetOrdinal("protocollo");
                        int cip = reader.GetOrdinal("cip");
                        int dataRichiesta = reader.GetOrdinal("dataRichiesta");
                        int dataEmissione = reader.GetOrdinal("dataEmissione");
                        int tipoPratica = reader.GetOrdinal("tipoPratica");
                        int codiceFiscale = reader.GetOrdinal("codiceFiscale");

                        while (reader.Read())
                        {
                            DurcClass durc = new DurcClass();

                            if (!reader.IsDBNull(ragioneSociale))
                                durc.RagioneSociale = reader.GetString(ragioneSociale);
                            if (!reader.IsDBNull(protocollo))
                                durc.Protocollo = reader.GetString(protocollo);
                            if (!reader.IsDBNull(cip))
                                durc.Cip = reader.GetString(cip);
                            if (!reader.IsDBNull(dataRichiesta))
                                durc.DataRichiesta = reader.GetDateTime(dataRichiesta);
                            if (!reader.IsDBNull(dataEmissione))
                                durc.DataEmissione = reader.GetDateTime(dataEmissione);
                            if (!reader.IsDBNull(codiceFiscale))
                                durc.CodiceFiscale = reader.GetString(codiceFiscale);
                            if (!reader.IsDBNull(tipoPratica))
                                durc.TipoPratica = reader.GetInt32(tipoPratica);

                            durcList.Add(durc);
                        }
                    }
                }
            }

            #region MySql

            //int div = durcToRead/100;
            //int mod = durcToRead%100;

            //if (mod != 0)
            //    div++;

            //for (int i = 0; i < div; i++)
            //{
            //    using (MySqlConnection connection = new MySqlConnection(connectionStringLucca))
            //    {
            //        int start = 100*i;
            //        const int seed = 100;

            //        string query =
            //            String.Format(
            //                "select impresa_esecutrice as ragionesociale, protocollo, cip, data_pratica as datarichiesta, data_durc as dataemissione from dmView where data_durc is not null limit {0},{1}",
            //                start, seed);

            //        using (MySqlCommand command = new MySqlCommand(query, connection))
            //        {
            //            connection.Open();

            //            using (MySqlDataReader reader = command.ExecuteReader())
            //            {
            //                int ragioneSociale = reader.GetOrdinal("ragioneSociale");
            //                int protocollo = reader.GetOrdinal("protocollo");
            //                int cip = reader.GetOrdinal("cip");
            //                int dataRichiesta = reader.GetOrdinal("dataRichiesta");
            //                int dataEmissione = reader.GetOrdinal("dataEmissione");

            //                while (reader.Read())
            //                {
            //                    DurcClass durc = new DurcClass();

            //                    if (!reader.IsDBNull(ragioneSociale))
            //                        durc.RagioneSociale = reader.GetString(ragioneSociale);
            //                    if (!reader.IsDBNull(protocollo)) durc.Protocollo = reader.GetString(protocollo);
            //                    if (!reader.IsDBNull(cip)) durc.Cip = reader.GetString(cip);
            //                    if (!reader.IsDBNull(dataRichiesta))
            //                        durc.DataRichiesta = reader.GetDateTime(dataRichiesta);
            //                    if (!reader.IsDBNull(dataEmissione))
            //                        durc.DataEmissione = reader.GetDateTime(dataEmissione);

            //                    durcList.Add(durc);
            //                }
            //            }
            //        }
            //    }
            //}

            #endregion

            return durcList;
        }

        public int WriteDurcTmp(List<DurcClass> durcList)
        {
            int ret;

            const string queryDelete = "delete from DurcLuccaTmp";
            const string queryInsert =
                "Insert Into dbo.DurcLuccaTmp([ragioneSociale],[protocollo],[CIP],[dataRichiesta],[dataEmissione],[codiceFiscale],[tipoPratica]) Values (@ragioneSociale,@protocollo,@CIP,@dataRichiesta,@dataEmissione,@codiceFiscale,@tipoPratica)";
            const string queryCount = "select count(*) from DurcLuccaTmp";

            using (SqlConnection connection = new SqlConnection(_connectionStringCemi))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(queryDelete, connection))
                {
                    command.ExecuteNonQuery();
                }

                foreach (DurcClass durc in durcList)
                {
                    using (SqlCommand command = new SqlCommand(queryInsert, connection))
                    {
                        command.Parameters.AddWithValue("@ragioneSociale", durc.RagioneSociale);
                        command.Parameters.AddWithValue("@protocollo", durc.Protocollo);
                        command.Parameters.AddWithValue("@CIP", durc.Cip);
                        command.Parameters.AddWithValue("@dataRichiesta", durc.DataRichiesta);
                        command.Parameters.AddWithValue("@dataEmissione", durc.DataEmissione);
                        command.Parameters.AddWithValue("@codiceFiscale", durc.CodiceFiscale);
                        command.Parameters.AddWithValue("@tipoPratica", durc.TipoPratica);

                        command.ExecuteNonQuery();
                    }
                }

                using (SqlCommand command = new SqlCommand(queryCount, connection))
                {
                    ret = Int32.Parse(command.ExecuteScalar().ToString());
                }
            }

            return ret;
        }

        public int WriteDurcDef()
        {
            int ret;

            const string queryDelete = "delete from DurcLucca";
            const string queryInsert =
                "Insert Into dbo.DurcLucca([ragioneSociale],[protocollo],[CIP],[dataRichiesta],[dataEmissione],[codiceFiscale],[tipoPratica]) select [ragioneSociale],[protocollo],[CIP],[dataRichiesta],[dataEmissione],[codiceFiscale],[tipoPratica] from dbo.DurcLuccaTmp";

            using (SqlConnection connection = new SqlConnection(_connectionStringCemi))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(queryDelete, connection))
                {
                    command.ExecuteNonQuery();
                }

                using (SqlCommand command = new SqlCommand(queryInsert, connection))
                {
                    ret = command.ExecuteNonQuery();
                }
            }

            return ret;
        }
    }
}