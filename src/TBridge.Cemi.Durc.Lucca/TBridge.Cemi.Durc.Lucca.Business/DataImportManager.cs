﻿using System;
using System.Collections.Generic;
using System.Configuration;
using TBridge.Cemi.Durc.Lucca.Data;
using DurcClass = TBridge.Cemi.Durc.Lucca.Type.Durc;

namespace TBridge.Cemi.Durc.Lucca.Business
{
    public class DataImportManager
    {
        private readonly DataAccess _dataAccess = new DataAccess();
        private readonly string _readErrorMessage = ConfigurationManager.AppSettings["ReadErrorMessage"];
        private readonly string _writeErrorMessage = ConfigurationManager.AppSettings["WriteErrorMessage"];

        private int DurcQuantityControl { get; set; }
        private List<DurcClass> DurcList { get; set; }

        public void ImportData()
        {
            DurcQuantityControl = CountDurc();
            DurcList = ReadDurc(DurcQuantityControl);

            if (DurcList.Count != DurcQuantityControl)
            {
                //ERRORE
                string detailExceptioMessage = String.Format("Nr. Durc esposti: {0}; nr. Durc letti: {1}",
                                                             DurcQuantityControl, DurcList.Count);
                throw new Exception(_readErrorMessage, new Exception(detailExceptioMessage));
            }

            int durcSavedCount = WriteDurcTmp(DurcList);

            if (durcSavedCount != DurcQuantityControl)
            {
                //ERRORE
                string detailExceptioMessage = String.Format("Nr. Durc esposti: {0}; nr. Durc scritti: {1}",
                                                             DurcQuantityControl, durcSavedCount);
                throw new Exception(_writeErrorMessage, new Exception(detailExceptioMessage));
            }

            WriteDurcDef();
        }

        private int WriteDurcTmp(List<DurcClass> durcList)
        {
            return _dataAccess.WriteDurcTmp(durcList);
        }

        private int WriteDurcDef()
        {
            return _dataAccess.WriteDurcDef();
        }

        private List<DurcClass> ReadDurc(int durcToRead)
        {
            return _dataAccess.ReadDurc(durcToRead);
        }

        private int CountDurc()
        {
            return _dataAccess.CountDurc();
        }
    }
}