﻿using System;

namespace TBridge.Cemi.Durc.Lucca.Type
{
    public class Durc
    {
        public Durc()
        {
            DataEmissione = null;
            DataRichiesta = null;
        }

        public string RagioneSociale { get; set; }
        public string Protocollo { get; set; }
        public string Cip { get; set; }
        public DateTime? DataRichiesta { get; set; }
        public DateTime? DataEmissione { get; set; }

        public String CodiceFiscale { get; set; }
        public Int32 TipoPratica { get; set; }
    }
}