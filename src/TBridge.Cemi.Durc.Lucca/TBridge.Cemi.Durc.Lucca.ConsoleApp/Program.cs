﻿using System;
using System.Configuration;
using System.Diagnostics;
using TBridge.Cemi.Durc.Lucca.Business;

namespace TBridge.Cemi.Durc.Lucca.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string logDurc = ConfigurationManager.AppSettings["LogDurc"];

            try
            {
                DataImportManager dataImportManager = new DataImportManager();
                dataImportManager.ImportData();
            }
            catch (Exception exception)
            {
                if (logDurc == "true")
                {
                    using (EventLog appLog = new EventLog())
                    {
                        appLog.Source = "SiceInfo";
                        appLog.WriteEntry(String.Format("Eccezione Durc: {0}", exception.Message));
                    }
                }
            }
        }
    }
}